rename_classes = {  # dws names map
        "TRichViewEdit": "TReport",
        "TCustomRichViewEdit": "TReportBase",
        "TRichView": "TReportView",
        "TCustomRichView": "TReportViewBase",
    }
input_classes = [  # classes to parse
        "TRichViewEdit",
        "TCustomRichViewEdit",
        "TRichView",
        "TCustomRichView",
        "TRVTableItemInfo",

        #"TCustomRVData",
        #'TRVLength',
        #'TRVSaveComponentToFileEvent',
        #'TRVAnsiString',
        #'TRVOleDragOverEvent',
        #'TRVFlags',
        #'PRVScreenAndDevice',
        #'TBasicAction',
        #'TCPEventKind',
        #'TRVOleDragEnterEvent',
        #'TRVBiDiMode',
        #'Cardinal',
        #'TPersistent',
        #'TBackgroundStyle',
        #'TList',
        #'TStream',
        #'TRVSpellingCheckEvent',
        #'TRect',
        #'TRVStrProperty',
        #'TRVList',
        #'TFontInfo',
        #'TRVLongOperation',
        #'TRVItemHintEvent',
        #'PRVStyleUnits',
        #'TRVFReaderStyleMode',
        #'TRVYesNoAuto',
        #'TRVProgressEvent',
        #'TControl',
        #'Byte',
        #'TRVOleDropEvent',
        #'TRVStoreSubRVData',
        #'TRVStickingItemsEvent',
        #'TColor',
        #'TRVMouseEvent',
        #'TRVBackground',
        #'TRVRightClickEvent',
        #'TRVTabNavigationType',
        #'TRVDrawCustomCaretEvent',
        #'TRVImportPictureEvent',
        #'TRVSaveOptions',
        #'TRVEditorOptions',
        #'TRVStyleTemplateCollection',
        #'IRVScaleRichViewInterface',
        #'TRVDropSourceClass',
        #'TRVUnicodeString',
        #'TRVSaveRTFExtraEvent',
        #'TRVStyleConversionEvent',
        #'TRVIntegerList',
        #'TRVSaveImageEvent2',
        #'TRVSaveFormat',
        #'TRVDocRotation',
        #'TRVLayoutInfo',
        #'TRVHTMLListCSSList',
        #'TRVLiveSpellingMode',
        #'TRVFWarnings',
        #'TRVGetItemCursorEvent',
        #'TRVDblClickEvent',
        #'TRVControlActionEvent',
        #'TRVFloatProperty',
        #'TRVHTMLSaveImageEvent',
        #'TPoint',
        #'TCustomImageList',
        #'TRVURLNeededEvent',
        #'TRVRTFErrorCode',
        #'TRVMarkerList',
        #'TRVVAlign',
        #'TRVItemActionEvent',
        #'TRVDragDropFormats',
        #'TRVReadHyperlink',
        #'TCursor',
        #'TJumpEvent',
        #'TRVPaletteAction',
        #'TRVUndoType',
        #'TRVCheckpointVisibleEvent',
        #'TRVIntProperty',
        #'TRVCodePage',
        #'TCustomRVItemInfo',
        #'TRVDocXSavingData',
        #'TRVTag',
        #'TCustomRVInfo',
        #'TRVSeqList',
        #'TRVItemTextEditEvent',
        #'TRVStyle',
        #'TParaInfo',
        #
        #'TRVStyleUnits',
        #'TRVWriteHyperlink',
        #'TRVRTFReaderProperties',
        #'TRVFControlNeededEvent',
        #'TRVSaveParaToHTMLEvent',
        #'TGraphic',
        #'TRVItemOptions',
        #'TRVFImageListNeededEvent',
        #'TRVSaveItemToFileEvent',
        #'TRVFOptions',
        #'TRVSearchOptions',
        #'TRVOnCaretGetOutEvent',
        #'TRVSaveHTMLExtraEvent',
        #'TBitmap',
        #'PLogPalette',
        #'TRVScroller',
        #'TRVFSaveScope',
        #'TTextLayout',
        #'TCheckpointData',
        #'TWinControl',
        #'TRVRawByteString',
        #'TRVStyleTemplateInsertMode',
        #'TRVItemList',
        #'TRVGetSRVInterfaceEvent',
        #'TCustomRVControl',
        #'HPALETTE',
        #'TStrings',
        #'TRVCPInfo',
        #'TRVDropFilesEvent',
        #'TNotifyEvent',
        #'TRVItemResizeEvent',
        #'TRVThumbnailCache',
        #'TRVSmartPopupClickEvent',
        #'TObject',
        #
        #'TComponent',
        #'TRVFPictureNeededEvent',
        #'TRVRTFOptions',
        #'TStringList',
        #'TRVAddStyleEvent',
        #'TRVSmartPopupProperties',
        #'TRVSpellingCheckExEvent',
        #'TRVSaveDocXExtraEvent',
        #'TRVMouseMoveEvent',
        #'TRVDeletingEvent',
        #'TRVMeasureCustomCaretEvent',
        #'TRVDocParameters',
        #'TRVAnimationMode',
        #'TRVOptions',
        #'TRVUnicodeChar',
        #'TRVESearchOptions',
        #'TRVDeleteUnusedStylesData',
        #'TCanvas',
        #'TCustomControl',
        # 'TRVFontInfoProperties',
        # 'TRVApplyStyleEvent',
        # 'TRVDocXPart',
        # 'TFontCharset',
        # 'TPicture',
        # 'TRVStyleTemplateName',
        # 'TRVSmartPopupType',
        # 'TPenStyle',
        # 'TRVItemBackgroundStyle',
        # 'TRVStyleTemplateId',
        # 'TRVListInfos',
        # 'TRVSubRVDataPos',
        # 'TShortCut',
        # 'TRVReaderStyleMode',
        # 'TListSortCompare',
        # 'TRVTextDrawStates',
        # 'TRVDrawParaRectEvent',
        # 'TPopupMenu',
        # 'TRVZoomMode',
        # 'TRVMarkerItemInfo',
        # 'TRVDrawCheckpointEvent',
        # 'TRVSaveCSSOptions',
        # 'TRVGraphicInterface',
        # 'TRVTextBackgroundKind',
        # 'TRVHFType',
        # 'TRVLineWrapMode',
        # 'TRVFontInfoClass',
        # 'TRVDrawTextBackEvent',
        # 'TRVPaintItemPart',
        # 'TRVEStyleConversionType',
        # 'TRVFieldHighlightType',
        # 'TRVRTFHighlight',
        # 'TRVSelectionMode',
        # 'TRVParaInfoProperties',
        # 'TRVRTFStyleComparer',
        # 'TRVLoadFormat',
        # 'TRVUnits',
        # 'TRVSelectionStyle',
        # 'TCustomRVFontInfo',
        # 'TRVListInfoClass',
        # 'Extended',
        # 'TRVStyleTemplate',
        # 'TFontQuality',
        # 'TRVReaderUnicode',
        # 'TRVSmartPopupPosition',
        # 'TFontInfos',
        # 'TRVStyleLength',
        # 'TRVStyleHoverSensitiveEvent',
        # 'TRVReformatType',
        # 'TCollectionItem',
        # 'TRVSeqItemInfo',
        # 'TCollection',
        # 'TRVRTFReader',
        # 'TRVApplyStyleColorEvent',
        # 'TRVItemBoolProperty',
        # 'TRVDrawLineInfo',
        # 'TRVRTFHeaderFooterType',
        # 'TRVDocXRelationCatalog',
        # 'TRVRTFReaderClass',
        # 'TRVAfterApplyStyleEvent',
        # 'TRVParaInfoClass',
        # 'TRVZipPacker',
        # 'TRVSelectionHandleKind',
        # 'TCustomRVParaInfo',
        # 'TRVColorMode',
        # 'TPrinterOrientation',
        # 'TParaInfos',
        # 'TRVDrawStyleTextEvent',
        # 'TRVScreenAndDevice',
        # 'TRVSeqType',
        # 'TRVDrawPageBreakEvent',
        #'TCustomFontOrParaRVInfo', 'TFontName', 'PSize', 'TRVRTFNewObjectEvent', 'TRVRTFNewUnicodeTextEvent', 'TRVRTFTranslateKeywordEvent', 'TRVRTFListTable97', 'TRVBookmarkEvent', 'TRVRTFDocProperties', 'TRVRTFNewTextEvent', 'ByteBool', 'TRVRTFListOverrideTable97', 'TRVLineSpacingType', 'TRVRTFStyleSheet', 'TRVListInfo', 'TRVRTFFontList', 'TRVStyleTemplateKind', 'PPoint', 'TRVTabInfos', 'TRVBackgroundRect', 'TRVParaOptions', 'TRVAlignment', 'TRVRTFNewSeqEvent', 'TRVRTFNewPictureEvent', 'TRVRTFImportPictureEvent', 'TRVRTFNoteEvent', 'TTextMetric', 'POutlineTextmetric', 'TRVHandle', 'PInteger', 'TAlignment', 'TRVBorder', 'TRVSubSuperScriptType', 'TRVRTFTableEvent', 'TRVLineSpacingValue', 'TLogFont', 'TFont', 'TRVRTFReaderState', 'TXForm', 'TRVRTFCharProperties', 'TCustomRVInfos', 'TRVFontStyles', 'TRVRTFHighlightConvert', 'TRVRTFColorList', 'TRVSTFontInfo', 'TRVListLevel', 'TRVRTFProgressEvent', 'PRect', 'TFontStyles', 'TRVUnderlineType', 'TRVTextOptions', 'TRVHoverEffects', 'TRVSimpleRectItemInfo', 'TRVProtectOptions', 'array of TPoint', 'TSize', 'TRVRTFHeaderFooterEvent', 'TRVLabelItemInfo', 'TRVSTParaInfo',
        #'TRVRectItemInfo', 'TRVListLevelCollection', 'TRVRTFParaProperties', 'TRVRTFRowProperties', 'TRVRTFFont', 'TRVListType', 'TRVRTFSScriptType', 'TRVRTFSectionProperties', 'TRVMarkerAlignment', 'TRVRTFZoomKind', 'TRVBorderStyle', 'TRVMarkerFormatString', 'TRVListLevelOptions', 'TRVRTFBiDiMode', 'TRVRTFList97', 'TRVRTFStyleSheetEntry', 'TRVRTFUnderlineType', 'TRVBooleanRect', 'TRVTabInfo', 'TRVRTFFontStylesEx', 'TRVMarkerFormatStringW', 'TRVRect', 'TRVRTFListOverride97', 'TRVMarkerFont', 'TRVNonTextItemInfo',
        #'TRVRTFListLevel97', 'TRVTabAlign', 'TRVRTFParaBorder', 'TRVRTFListOverrideLevel97', 'TRVRTFAlignment', 'TRVRTFSectionBreakType', 'TRVRTFPageNumberFormat', 'TRVRTFMarkerProperties', 'TRVRTFCellPropsList', 'TRVRTFTabList', 'TRVRTFStyleSheetType',

    ]

# legacy funcs


    def parse_standalone_procedures(self):
        sources_path = []
        for path in self.search_path:
                files = os.listdir(path)
                for file in files:
                    if file[-3:] == "pas":
                        sources_path.append(path+file)
        for proc in self.standalone_procedures:
            for source_filename in sources_path:
                source_file = open(source_filename, 'r')
                source_code = source_file.read()
                source_file.close()
                parsed_proc = self.parse_standalone_procedure_old(source_code, proc, source_filename)
                if parsed_proc is not None:
                    self.syntax_tree['procedures'].append(parsed_proc)
                    self.parsed_procedures.append(parsed_proc['name'])
                    break
        return

    def parse_standalone_procedure_old(self, source_code, proc_name, source_filename):
        self.code = source_code
        self.code = self.sanitize()
        self.cursor = 0  # due to search from cursor
        while True:
            self.cursor = self.code.find(proc_name, self.cursor)
            if self.cursor == -1:
                return None
            name = proc_name
            self.cursor += len(proc_name)
            self.get_valuable_lex()
            lexeme = self.lex
            if lexeme != "=":
                continue
            self.get_valuable_lex()
            lexeme = self.lex
            if lexeme != "procedure":
                continue
            self.get_valuable_lex()
            args = self.parse_args()
            sign = self.parse_signature()
            item = {
                'name': name,
                'args': args,
                'signature': sign,
                'filename': source_filename,
            }
            return item

        return True


    def parse_classes_old(self):
        sources_path = []
        for path in self.search_path:
                files = os.listdir(path)
                for file in files:
                    if file[-3:] == "pas":
                        sources_path.append(path+file)
        for c in self.input_classes:
            for source in sources_path:
                source_file = open(source, 'r')
                source_code = source_file.read()
                source_file.close()
                parsed_class = self.parse_class_old(source_code, c, source)
                if parsed_class is not None:
                    self.syntax_tree['classes'].append(parsed_class)
                    self.parsed_classes.append(parsed_class['name'])
                    break


    def parse_class_old(self, source_code, name, file_name):
        self.code = source_code
        self.code = self.sanitize()
        self.cursor = 0  # due to search from cursor
        while True:
            self.cursor = self.code.find(name, self.cursor)
            if self.cursor == -1:
                return None
            class_name = name
            self.cursor += len(name)

            self.get_valuable_lex()
            lexeme = self.lex
            if lexeme != "=":
                continue
            self.get_valuable_lex()
            lexeme = self.lex
            if lexeme != "class":
                continue
            self.get_valuable_lex()
            lexeme = self.lex
            if lexeme == "of":
                continue
            class_ancestors = self.parse_ancestors()

            lexeme = self.lex
            if lexeme == ';':
                continue
            if lexeme == "end":
                item = {
                    'name': class_name,
                    'ancestors': class_ancestors,
                    'public': [],
                    'published': [],
                    'filename': file_name,
                }

                return item

            class_end = self.code.find("end;", self.cursor)
            if class_end == -1:
                raise Exception("Expected end; not found"+" recieved "+self.lex+"!"+self.code+str(self.cursor))


            self.code = lexeme+" "+self.code[self.cursor:class_end]
            self.cursor = 0
            self.get_lex()

            #print(self.code)  # DEBUG

            self.parse_items()  # skip non private/public/protected/published items
            self.parse_private()  # skip private
            self.parse_protected()  #skip protected
            public_items = self.parse_public()
            published_items = self.parse_published()

            item = {
                'name': class_name,
                'ancestors': class_ancestors,
                'public': public_items,
                'published': published_items,
                'filename': file_name,
            }

            return item