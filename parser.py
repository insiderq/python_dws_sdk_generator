#!/usr/local/bin/python3.3
import re
import os
import json

# todo Parser
# todo: filename to syntax tree
# todo: 'array of' is parsed into simple string
# todo: fields are parsed without read,write,default values and without parameters
# todo: add 'is var param' feature
# todo: procedure of object is not just a signature, it is Method Pointer
# todo: packed array
# todo: set of (Club, Diamond, Heart, Spade)
# todo: array[indexType1, ..., indexTypen] of baseType;
# todo: case inside of a struct http://docwiki.embarcadero.com/RADStudio/XE3/en/Structured_Types
# todo: file of type

# todo Generator
# todo: get done all types of items not only procedures and functions
# todo: add 'is var param' feature
# todo: gen declarations and implementations with preprocessor directives
# todo: fake class generation

# todo: do anything with this awful multiline strings and lists in headers


class Parser:
    search_path = [
        '/Users/insider/Dropbox/mac/projectos/HSE/CW_2014/python_sdk_generator/TRichView/',
        #'/Users/insider/Dropbox/mac/projectos/HSE/CW_2014/python_sdk_generator/test/',
        #'/Users/insider/Dropbox/mac/projectos/HSE/CW_2014/FCART_CW_OneDrive/',
    ]
    syntax_tree = {}  # result

    code = ""     # current analysed source code
    cursor = 0    # current caret position
    lex = ""  # current lexeme

    def parse_files(self):
        sources_path = []
        self.syntax_tree["files"] = []
        for path in self.search_path:
            files = os.listdir(path)
            for file in files:
                if file[-3:] == "pas":
                    sources_path.append(path + file)
        for file_name in sources_path:
            self.syntax_tree["files"].append(self.parse_file(file_name))

    def parse_file(self, file_name):
        source_file = open(file_name, 'r', encoding='utf8')
        source_code = source_file.read()
        source_file.close()
        self.code = source_code
        self.cursor = 0
        cut_off_to = -1
        while True:
            lexeme = self.get_valuable_lex()
            if lexeme == "implementation":
                cut_off_to = self.cursor - len(lexeme)
                break
        if cut_off_to == 1:
            raise Exception("The end of declaration part is not found in " + file_name)

        #cut_off_to = source_code.find("implementation")
        #if cut_off_to == -1:
        #    cut_off_to = source_code.find("end.")
        #    if cut_off_to == -1:
        #        raise Exception("The end of declaration part is not found in "+file_name)

        self.code = source_code[:cut_off_to]
        self.code = self.sanitize()
        self.cursor = 0

        lexeme = self.get_valuable_lex()
        if lexeme.lower() != "unit":
            return None

        unit = {}

        self.get_valuable_lex()
        unit["name"] = self.parse_name()
        lexeme = self.parse_semicolon()

        if lexeme.lower() != "interface":
            raise Exception("The interface part of unit is not found in " + file_name)

        unit["uses"] = []
        lexeme = self.get_valuable_lex()
        if lexeme.lower() == "uses":
            self.get_valuable_lex()
            unit["uses"] = self.parse_names()
            lexeme = self.parse_semicolon()

        unit['items'] = []

        # hardcode in case absence of 'type'
        if lexeme.lower() in ['procedure', 'function']:
            unit['items'] += self.parse_items()
            return unit

        while True:
            lexeme = self.lex
            if lexeme is None:
                return unit

            #if lexeme.lower() in ["const", "var", "resourcestring"]:  # todo: handle and delete that hard
            #    self.get_valuable_lex()
            #    continue

            #if lexeme.lower() in ["type", "const", "var"]:  # todo: replace
            if lexeme.lower() in ["type"]:
                self.get_valuable_lex()
                unit['items'] += self.parse_items()
            else:
                self.get_valuable_lex()

        return unit

    def parse_items(self):
        items = []
        while True:
            item = self.parse_item()
            if item is None:
                break
            else:
                if isinstance(item, list):  # parse_item can return list of items in some cases
                    items += item
                else:
                    items.append(item)
        return items

    def parse_item(self):
        lexeme = self.lex
        if lexeme is None:
            return None
        if lexeme.lower() in ["type", "var", "const"]:
            return None  # items from this part are done

        # check if comment or something
        if len(lexeme) >= 2:
            if lexeme[0:2] == "//" or lexeme[0:2] == "(*":
                item = {
                    'type': 'comment',
                    'node': dict(text=lexeme),
                }
                self.get_lex()
                return item

        if lexeme.lower() == "[":  # temporary skip that construction. todo: delete
            self.parse_square_parenthesis()
            lexeme = self.lex

        simple_item_keywords = [
            #"[",
            "{",
            "function",
            "procedure",
        ]

        can_have_signature = {
            "function",
            "procedure",
        }

        switch_parsing_function = {
            #"[":            self.parse_square_parenthesis(),
            "{": self.parse_pre_directive,
            "function": self.parse_function,
            "procedure": self.parse_procedure,
        }

        if lexeme.lower() in simple_item_keywords:
            self.get_valuable_lex()
            item = {
                'type': lexeme,
                'node': switch_parsing_function[lexeme](),
            }
            if lexeme.lower() in can_have_signature:
                item['node']['sign'] = self.parse_signature()
                #self.parse_semicolon()
            if item['type'] == "{":
                item['type'] = "preprocessor"
            return item

        #  no keywords found
        item_name = lexeme
        lexeme = self.get_valuable_lex()
        if lexeme.lower() != "=":
            raise Exception("Expected = after name " + item_name + " ,but recieved " + lexeme + "!"
                            + self.code[self.cursor - 30:self.cursor + 30])
        lexeme = self.get_valuable_lex()
        if lexeme.lower() == "(":
            item = {
                'type': "enum",
                'node': self.parse_enum(),
            }
            item['node']['name'] = item_name
            return item

        if lexeme.lower() == "class":
            self.get_valuable_lex()
            lexeme = self.lex
            item = {}
            if lexeme.lower() == "of":
                self.get_valuable_lex()
                item = {
                    'type': 'class_reference',
                    'node': {
                        'type': self.parse_name(),
                        'name': item_name,
                    }
                }
                self.parse_semicolon()
                return item
            else:
                item['node'] = self.parse_class()
                item['node']['name'] = item_name
                item['type'] = 'class'
                return item

        if lexeme.lower() == "interface":
            item = {}
            self.get_valuable_lex()
            item['node'] = self.parse_interface()
            item['node']['name'] = item_name
            item['type'] = "interface"
            return item

        if lexeme.lower() == "procedure":
            item = {}
            self.get_valuable_lex()
            item['node'] = self.parse_standalone_procedure()
            item['node']["name"] = item_name
            item["type"] = "procedural_proc"  # myproc1 = procedure(Index:Integer);
            return item

        if lexeme.lower() == "function":
            item = {}
            self.get_valuable_lex()
            item['node'] = self.parse_standalone_function()
            item['node']["name"] = item_name
            item["type"] = "procedural_func"  # myproc1 = function(Index:Integer):Boolean;
            return item

        if lexeme.lower() == "set":
            lexeme = self.get_valuable_lex()
            if lexeme.lower() == "of":
                self.get_valuable_lex()
                obj = self.parse_name()
                self.parse_semicolon()
                item = {
                    'type': "set",
                    'node': dict(type=obj, name=item_name),
                }
                return item
            else:
                raise Exception('Expected \'of\' to declare set')
        if lexeme.lower() == "^":
            self.get_valuable_lex()
            pointer_to = self.parse_name()
            self.parse_semicolon()
            item = {
                'type': 'pointer',
                'node': dict(type=pointer_to, name=item_name),
            }
            return item

        if lexeme.lower() == "packed":
            lexeme = self.get_valuable_lex()

        if lexeme.lower() == "record":
            self.get_valuable_lex()
            members = []
            while True:
                lexeme = self.lex
                if lexeme is None:
                    raise Exception("end of record is not found")
                if lexeme.lower() == "end":
                    self.get_valuable_lex()
                    self.parse_semicolon()
                    break
                else:
                    fields = self.parse_field()
                    members += fields
            item = {
                'type': 'record',
                'node': dict(members=members, name=item_name),
            }
            return item

        if lexeme.lower() == "array":
            item = {
                'type': 'array',
                'node': dict(type=self.parse_type_array_of(), name=item_name)  # todo correct handle of arrays
            }
            self.parse_semicolon()
            return item

        if lexeme.lower() == "type":
            self.get_valuable_lex()
            item = {
                'type': 'distinct_synonym', # myclass1 = type Integer;
                'node': dict(type=self.parse_name(), name=item_name)
            }
            self.parse_semicolon()
            return item

        #  all pass means construction is like "myclass1 = myclass2;"
        node = self.parse_non_delim()
        self.parse_semicolon()
        item = {
            'type': 'synonym',
            'node': dict(type=node, name=item_name)
        }
        return item

        #raise Exception("Unknown keyword after = "+lexeme)

    def parse_class(self):
        item = dict(ancestors=[], decl_info=None, members=[], private=[],
                    protected=[], public=[], published=[], )

        visibilities = ["private", "protected", "public", "published"]

        item['ancestors'] = self.parse_ancestors()
        lexeme = self.lex
        if lexeme.lower() == ";":
            self.parse_semicolon()
            if not item["ancestors"]:
                item["decl_info"] = "forward"
            return item

        visibility = 'members'
        while True:
            item[visibility] = self.parse_class_items()
            lexeme = self.lex
            if lexeme.lower() in visibilities:
                visibility = lexeme
                self.get_valuable_lex()
                continue
            elif lexeme.lower() == "end":
                self.get_valuable_lex()
                self.parse_semicolon()
                return item
            else:
                raise Exception("Expected visibility(public,private) or end, but recieved " + lexeme + "!")

    def parse_interface(self):
        return self.parse_class()

    def parse_ancestors(self):
        lexeme = self.lex
        if lexeme != '(':
            return []
        self.get_valuable_lex()
        names = self.parse_arg_names()
        lexeme = self.lex
        if lexeme != ')':
            raise Exception("Expected )" + " recieved " + self.lex + "!" + self.code + str(self.cursor))
        self.get_valuable_lex()
        return names

    def parse_scope(self, scope):
        lexeme = self.lex
        if lexeme != scope:
            return []
        self.get_lex()
        return self.parse_items()

    def parse_public(self):
        return self.parse_scope("public")

    def parse_published(self):
        return self.parse_scope("published")

    def parse_private(self):
        return self.parse_scope("private")

    def parse_protected(self):
        return self.parse_scope("protected")

    def parse_class_items(self):
        items = []
        while True:
            item = self.parse_class_item()
            # print(pprint.pprint(item))  # debug
            if item is None:
                break
            else:
                if isinstance(item, list):  # parse_item can return list of items in some cases
                    items += item
                else:
                    items.append(item)
        return items

    def parse_class_item(self):
        lexeme = self.lex
        if lexeme is None:
            return None
        if lexeme.lower() in ["public", "private", "protected", "published", "end"]:
            return None  # items from current visibility are done

        if lexeme.lower() == "class":  # class function
            temp_lex = lexeme
            lexeme = self.get_valuable_lex()
            if lexeme.lower() != "function":
                raise Exception(
                    "Expected 'function' to declare static method! " + self.code[self.cursor - 30:self.cursor + 30])
            lexeme = temp_lex + " " + self.lex

        if lexeme[0:2] == "//" or lexeme[0:2] == "(*":
            item = {
                'type': 'comment',
                'node': dict(text=lexeme),
            }
            self.get_lex()
            return item

        switch_class_item_parser = {
            "{": self.parse_pre_directive,
            "constructor": self.parse_construct_destruct,
            "destructor": self.parse_construct_destruct,
            "function": self.parse_function,
            "class function": self.parse_function,
            "procedure": self.parse_procedure,
            "property": self.parse_property,
        }
        can_have_signature = {
            "constructor",
            "destructor",
            "function",
            "procedure",
            "class function",
        }

        if lexeme.lower() in switch_class_item_parser:
            self.get_lex()
            item = {
                'type': lexeme,
                'node': switch_class_item_parser[lexeme](), # switch and call parse function based on last lexeme
            }

        else:
            items = self.parse_field()
            return items

        if lexeme.lower() in can_have_signature:
            sign = self.parse_signatures()
            item['node']['signature'] = sign

        return item

    def parse_standalone_procedure(self):
        args = self.parse_args()
        lexeme = self.lex
        if lexeme.lower() == ";":
            self.parse_semicolon()
            temp_sign = self.parse_signature()
        elif lexeme.lower() == "of":
            temp_sign = self.parse_signature()
        else:
            raise Exception("Expected ';' or 'of', but recieved " + lexeme)
        item = {
            'args': args,
            'sing': temp_sign,
        }
        return item

    def parse_standalone_function(self):
        args = self.parse_args()
        lexeme = self.lex
        result_type = None
        if lexeme.lower() == ":":
            self.parse_colon()
            result_type = self.parse_type()
        self.parse_semicolon()
        temp_sign = self.parse_signature()
        item = {
            'args': args,
            'sign': temp_sign,
            'result_type': result_type,
        }

        return item

    def parse_semicolon(self):
        if not self.lex == ';':
            raise Exception('Expected ;' + " recieved " + self.lex + "!" + self.code[self.cursor - 30:self.cursor + 30])
            #raise Exception('Expected ;'+" recieved "+self.lex+"!"+self.code+str(self.cursor))
        return self.get_valuable_lex()

    def parse_colon(self):
        if not self.lex == ':':
            raise Exception('Expected :' + " recieved " + self.lex + "!" + self.code[self.cursor - 30:self.cursor + 30])
            #raise Exception('Expected :'+" recieved "+self.lex+"!"+self.code+str(self.cursor))
        self.get_valuable_lex()

    def parse_non_delim(self):
        lexeme = self.lex
        if lexeme.lower() in ",:;{}()=$":
            #raise Exception('Expected non delimiter'+" recieved "+self.lex+"!"+self.code+str(self.cursor))
            raise Exception(
                'Expected non delimiter' + " recieved " + self.lex + "!" + self.code[self.cursor - 30:self.cursor + 30])
        if len(lexeme) >= 2:
            if lexeme[0:2] == "//" or lexeme[0:2] == "(*":
                raise Exception(
                    'Expected non delimiter' + " recieved " + self.lex + self.code[self.cursor - 30:self.cursor + 30])
                #raise Exception('Expected non delimiter'+" recieved "+self.lex+"!"+self.code+str(self.cursor))

        self.get_valuable_lex()
        return lexeme

    def parse_type(self):
        t = ""
        lexeme = self.lex

        if lexeme.lower() == "^":
            self.get_valuable_lex()
            lexeme = self.lex
            t = "^" + lexeme
            self.get_valuable_lex()
            return t

        if lexeme.lower() == "array":
            t += lexeme + " "
            self.get_valuable_lex()
            t += self.parse_square_parenthesis()
            lexeme = self.lex
            if lexeme.lower() == "of":
                t += lexeme + " "
                self.get_valuable_lex()
                t += self.parse_non_delim()
                return t
            else:
                t += lexeme + " "
                self.get_valuable_lex()
                lexeme = self.lex
                if lexeme.lower() == "of":
                    t += lexeme + " "
                    self.get_valuable_lex()
                    t += self.parse_non_delim()
                    return t
                else:
                    raise Exception('Expected of' + " recieved " + self.lex + "!" + self.code + str(self.cursor))
        else:
            if lexeme.lower() in ["read", 'write', "default", "stored", ";"]:
                return None
            return self.parse_non_delim()

    def parse_type_array_of(self):
        #array [TRVDocXPart] of TRVDocXRelationCatalog;
        #array of Integer;
        #array[0..1000] of String;
        #array of array of Integer
        return self.parse_type()  # todo: fix

    def parse_name(self):
        name = self.parse_non_delim()
        return name

    def parse_square_parenthesis(self):
        item = ""
        lexeme = self.lex
        if lexeme.lower() == "[":
            item += lexeme
            while True:
                self.get_valuable_lex()
                lexeme = self.lex
                if lexeme.lower() == "]":
                    item += lexeme
                    self.get_valuable_lex()
                    break
                else:
                    item += lexeme
        return item

    def parse_value(self):
        return self.parse_non_delim()

    def parse_signatures(self):
        items = []
        while True:
            item = self.parse_signature()
            if item is None:
                break
            else:
                items.append(item)
        return items

    def parse_signature(self):
        lexeme = self.lex
        item = None
        if lexeme is None:
            return None
        if lexeme.lower() in [
            "override",
            "dynamic",
            "virtual",
            "abstract",
            "stdcall",
            "inline",
            "overload",
            "reintroduce",
            "assembler",
        ]:
            self.get_valuable_lex()
            self.parse_semicolon()

            item = {
                'type': lexeme,
                'node': None,
            }
            return item
        if lexeme.lower() == "message":
            self.get_valuable_lex()
            lexeme = self.lex
            self.get_valuable_lex()
            self.parse_semicolon()
            item = {
                'type': "message",
                'node': lexeme,
            }
            return item
        if lexeme.lower() == "deprecated":
            lexeme = self.get_valuable_lex()
            if lexeme != ";":
                hint = lexeme
                self.get_valuable_lex()
            self.parse_semicolon()

            item = {
                'type': "deprecated",
                'node': lexeme,
            }
            return item

        if lexeme.lower() == "of":
            item = lexeme
            self.get_valuable_lex()
            lexeme = self.lex
            item += " " + lexeme
            item = {
                'type': item,
                'node': None,
            }
            self.get_valuable_lex()
            self.parse_semicolon()
            return item
        return item

    def parse_enum(self):
        lexeme = self.lex
        if lexeme.lower() != "(":
            return []
        self.get_valuable_lex()
        items = self.parse_names()
        lexeme = self.lex
        if lexeme.lower() != ")":
            raise Exception("Expected ) to end enum declaration")
        self.get_valuable_lex()
        self.parse_semicolon()
        return dict(members=items)

    def parse_string(self):
        s = ""
        init_lexeme = self.lex
        if init_lexeme.lower() in "'\"":
            s += init_lexeme + ' '
            while True:
                self.get_valuable_lex()
                lexeme = self.lex
                s += lexeme + ' '
                if lexeme == init_lexeme:
                    self.get_valuable_lex()
                    return s
        return None

    def parse_pre_directive(self):
        item = "{"
        lexeme = self.lex
        if lexeme != "{":
            return None
        while True:  # compile directive from lexemes separating with ' '
            sym = self.get_sym()
            if sym is None:
                #+str(self.cursor))
                raise Exception('Expected }' + " recieved None !" + str(self.code))
            item += sym
            if sym == "}":
                self.get_lex()
                break

        return item

    def parse_field(self):
        names = self.parse_names()
        self.parse_colon()
        field_type = self.parse_type()
        self.parse_semicolon()
        items = []
        for name in names:
            item = dict(type='field', node={})
            item['node'] = {
                'name': name,
                'field_type': field_type,
            }
            items.append(item)
        return items

    def parse_construct_destruct(self):
        name = self.parse_name()

        item = {
            'name': name,
            'args': self.parse_args()
        }
        self.parse_semicolon()
        return item

    def parse_function(self):
        name = self.parse_name()

        item = {
            'name': name,
            'args': self.parse_args()
        }

        self.parse_colon()
        item['result_type'] = self.parse_type()
        self.parse_semicolon()
        return item

    def parse_procedure(self):
        name = self.parse_name()

        item = {
            'name': name,
            'args': self.parse_args()
        }
        self.parse_semicolon()
        return item

    def parse_property_args(self):
        items = []
        lexeme = self.lex
        if lexeme.lower() != "[":
            return items
        self.get_valuable_lex()
        while True:
            param_names = self.parse_names()
            lexeme = self.lex
            if lexeme.lower() == ":":
                self.parse_colon()

            param_type = self.parse_type()
            for name in param_names:
                item = {
                    'name': name,
                    'type': param_type,
                }
                items.append(item)
            lexeme = self.lex
            if lexeme.lower() == "]":
                self.get_valuable_lex()
                return items

        return items

    def parse_property(self):
        name = self.parse_name()
        args = self.parse_property_args()
        lexeme = self.lex
        prop_type = None
        if lexeme.lower() == ":":
            self.parse_colon()
            prop_type = self.parse_type()
        prop_sign = self.parse_prop_sign()
        while True:  # todo: deal with properties
            lexeme = self.lex
            if lexeme.lower() == ";":
                break
            self.get_valuable_lex()

        item = {
            'name': name,
            'args': args,
            'type': prop_type,
            'sign': prop_sign,
        }
        self.parse_semicolon()
        lexeme = self.lex
        if lexeme.lower() == "default":
            self.get_valuable_lex()
            self.parse_semicolon()
        return item

    def parse_prop_sign(self):
        lexeme = self.lex
        item = {}
        if lexeme.lower() == "read":
            self.get_valuable_lex()
            lexeme = self.lex
            item['read'] = lexeme
            self.get_valuable_lex()
            lexeme = self.lex
        if lexeme.lower() == "write":
            self.get_valuable_lex()
            lexeme = self.lex
            item['write'] = lexeme
            self.get_valuable_lex()
            lexeme = self.lex
        if lexeme.lower() == "stored":
            self.get_valuable_lex()
            lexeme = self.lex
            item['stored'] = lexeme
            self.get_valuable_lex()
            lexeme = self.lex
        if lexeme.lower() == "default":
            self.get_valuable_lex()
            value = ""
            while True:
                lexeme = self.lex
                if lexeme.lower() == ";":
                    item["default"] = value
                    return item
                else:
                    value += lexeme
                    self.get_valuable_lex()
        return item

    def parse_args(self):
        args = []
        lexeme = self.lex
        if lexeme.lower() != "(":
            return args

        self.get_valuable_lex()
        while True:
            arg_names = self.parse_arg_names()
            arg_type = self.parse_arg_type()
            arg_default = self.parse_arg_default_value()
            for name in arg_names:  # in case of a,b,c:Integer declaration
                args.append({
                    'name': name,
                    'type': arg_type,
                    'default': arg_default,
                })
            lexeme = self.lex
            if lexeme.lower() == ")":
                break
            self.get_valuable_lex()

        self.get_valuable_lex()
        return args

    def parse_arg_names(self):
        names = []

        while True:
            names.append(self.parse_arg_name())
            lexeme = self.lex
            if lexeme.lower() != ",":
                break
            else:
                self.get_valuable_lex()
                continue
        return names

    def parse_names(self):
        names = []

        while True:
            names.append(self.parse_name())
            lexeme = self.lex
            if lexeme.lower() != ",":
                break
            else:
                self.get_valuable_lex()
                continue
        return names

    def parse_arg_name(self):
        # todo: do not skip 'const' and 'var' implementations
        lexeme = self.lex
        if lexeme.lower() in ["var", "const", "out"]:
            self.get_valuable_lex()
        return self.parse_name()

    def parse_arg_type(self):
        lexeme = self.lex
        if lexeme == ";" or lexeme.lower() == ")":
            return None
        self.parse_colon()
        return self.parse_type()

    def parse_arg_default_value(self):
        lexeme = self.lex
        if lexeme.lower() == "=":
            self.get_valuable_lex()
            default = self.parse_value()
        else:
            default = None
        return default

    def sanitize(self):  # strip repeating white-space symbols
        def space_or_newline(match_obj):
            group = match_obj.group(0)
            if "\n" in group:
                return "\n"
            else:
                return " "

        return re.sub(r"(\s+)", space_or_newline, self.code)

    def get_sym(self):
        if self.cursor >= len(self.code) - 1:  # next is out of range
            return None
        else:
            self.cursor += 1
            return self.code[self.cursor - 1]

    def get_lex(self):
        # todo: redesign to get symbol after
        lex = ""
        while True:
            sym = self.get_sym()
            if sym is None:
                if lex:
                    self.lex = lex
                    return self.lex
                else:
                    self.lex = None
                    return self.lex
            if sym in " \t\r\v\f\n":
                if not len(lex) == 0:  # lexem exists
                    self.lex = lex
                    return self.lex
                else:  # lexem is empty, just skip delimiter
                    continue

            if sym == "(":  # context sensitive lexem if it is comment (care hard-code).
                if len(lex) == 0:
                    next_sym = self.get_sym()
                    if next_sym == "*":
                        lex += sym
                        lex += next_sym
                        while True:
                            next_sym = self.get_sym()
                            if next_sym is None:
                                self.lex = lex
                                return self.lex
                            if next_sym == "*":
                                lex += next_sym
                                next_sym = self.get_sym()
                                if next_sym == ")":
                                    lex += next_sym
                                    self.lex = lex
                                    return self.lex
                                else:
                                    self.cursor -= 1
                                    continue
                            lex += next_sym
                    else:  # just ( return self.lex
                        self.cursor -= 1
                        self.lex = "("
                        return self.lex
                else:
                    self.lex = lex
                    self.cursor -= 1  # retake (
                    return self.lex

            if sym in "^,:;[]{}()=":  # single symbolic lexemes
                if not len(lex) == 0:  # lexem exists
                    self.cursor -= 1  # move back cursor to retake delimiter
                    self.lex = lex
                    return self.lex
                else:
                    self.lex = sym
                    return self.lex

            if sym == "'":
                if not len(lex) == 0:  # lexem exists
                    self.cursor -= 1  # move back cursor to retake delimiter
                    self.lex = lex
                    return self.lex
                else:
                    lex = sym
                    while True:
                        sym = self.get_sym()
                        if sym is None:
                            self.lex = lex
                            return self.lex
                        if sym == "'":
                            self.lex = lex + sym
                            return self.lex
                        else:
                            lex += sym

            if sym == '"':
                if not len(lex) == 0:  # lexem exists
                    self.cursor -= 1  # move back cursor to retake delimiter
                    self.lex = lex
                    return self.lex
                else:
                    lex = sym
                    while True:
                        sym = self.get_sym()
                        if sym == '"':
                            self.lex = lex + sym
                            return self.lex
                        else:
                            lex += sym

            if sym == "/":  # context sensitive lexem (care hard-code).
                if len(lex) == 0:
                    lex += sym
                    continue
                if lex[-1] == "/":
                    if len(lex) == 1:
                        lex += sym
                        while True:  # read symbols while not facing '\n' or EOF
                            sym = self.get_sym()
                            if sym is None:
                                if lex:
                                    self.lex = lex
                                    return self.lex
                            if sym == "\n":
                                self.lex = lex
                                return self.lex
                            else:
                                lex += sym
                                continue
                else:
                    self.cursor -= 1  # end reading lexem and move cursor back for retake '/'
                    self.lex = lex
                    return self.lex
            lex += sym

    def get_valuable_lex(self):
        self.get_lex()
        while True:
            lexeme = self.lex
            if lexeme is None:
                break
            if lexeme.lower() == "{":
                self.parse_pre_directive()
            elif lexeme[0:2] == "//" or lexeme[0:2] == "(*":
                self.get_lex()
            else:
                break
        return self.lex

###############################################################################
###############################################################################
###############################################################################
###############################################################################


class Generator:

    dws_rename = {  # dws exist classes names map
        "trichviewedit": "TReport",
        "tctlattice": "TLattice",
        "tctsubcontext": "TSubContext",
        "tctconcept": "TConcept",
    }

    dws_exist_classes = [  # define classes exist in dws
        "Boolean",
        "Integer",
        "Char",
        "String",
        "Float",
        "TReport",
        "TLattice",
        "TConcept",
        "TContext",
    ]

    dws_func_decl = \
        "procedure {class_name}{unit_obj_name}Functions{dws_name}Eval(info: TProgramInfo);\n"
    dws_func_impl = \
        """
{declaration}{var}
begin
{body}
end;
"""

    dws_gen_script_obj_decl = \
        """
function {class_name}Gen{type}ScriptObj(info: TProgramInfo; Obj: {type}):Variant;
"""
    dws_gen_script_obj_impl = \
        """
{declaration}begin
{body}
end
"""
    dws_gen_script_obj_impl_body = \
        "Result:=info.Vars['{dws_type}'].GetConstructor('GenCreate', Obj).Call.Value;"

    dws_funcs_form = \
        """
Functions = <
{proc_list}>"""
    dws_func_form = \
        """item
\tName = '{dws_name}'
\tParameters = <
{param_list}>
\tResultType = {dws_type}
\tOnEval = {unit_obj_name}Functions{dws_name}Eval
end"""
    dws_proc_form = \
        """item
\tName = '{dws_name}'
\tParameters = <
{param_list}>
\tOnEval = {unit_obj_name}Functions{dws_name}Eval
end
"""

    dws_param_form = \
        """item
\tName = '{dws_name}'
\tDataType = '{dws_type}'
\tIsVarParam = {bool}
end
"""

    default_types = [
        'Integer',
        'Float',
        'Boolean',
        'String',
        'TString',
    ]

    def __init__(self, parser, unit_obj_name, unit_name, class_name):
        self.parser = parser
        self.dws_unit_obj_name = unit_obj_name
        self.dws_unit_name = unit_name
        self.dws_class_name = class_name
        self.dws_tree = {}  # syntax tree sorted by item type
        self.unit_form = ""  # result form
        self.unit_declaration = ""  # result declaration part
        self.unit_implementation = ""  # result implementation part

    def gen_func_declaration(self, func_node):
        return self.dws_func_decl.format(
            class_name="",
            unit_obj_name=self.dws_unit_obj_name,
            dws_name=self.rename(func_node['name']),
        )

    def gen_func_implementation(self, func_node):
        func_declaration = self.dws_func_decl.format(
            class_name=self.dws_class_name+".",
            unit_obj_name=self.dws_unit_obj_name,
            dws_name=self.rename(func_node['name']),
        )
        var_text = self.gen_impl_var(func_node)
        body_text = self.gen_func_body(func_node)
        return self.dws_func_impl.format(
            declaration=func_declaration,
            var=var_text,
            body=body_text,
        )

    def gen_func_body(self, func_node):
        body = ""
        args_to_call = ""
        for arg in func_node['args']:
            args_to_call += arg['name'] + ','
            if arg['type'] in self.default_types:
                line = "\t" + arg['name'] + " := info.ValueAs{type}['{dws_name}']; \n"
            else:
                line = "\t" + arg['name'] + " := (info.ValueAsObject['{dws_name}'] as {type}); \n"
            body += line.format(
                dws_name=self.rename(arg['name']),
                name=arg['name'],
                type=arg['type'],
                )
        args_to_call = args_to_call[0:-1]  # slice the last comma
        if func_node['result_type'] in self.default_types:
            line = "\tinfo.ResultAs{type} := {func_name}({args}); \n"
        else:
            line = "\tinfo.ResultAsVariant := Gen{type}ScriptObj({func_name}({args}));"
        body += line.format(
            func_name=func_node['name'],
            args=args_to_call,
            type=func_node['result_type'],
        )
        return body

    def gen_func_list_text(self, func_nodes):
        func_list = ""
        for func_node in func_nodes:
            func_list += self.gen_func_text(func_node)
        return func_list

    def gen_func_text(self, func_node):
        params_text = self.gen_param_list_text(func_node['args'])
        return self.dws_func_form.format(
            param_list=indent_multiline(params_text, 2),
            unit_obj_name=self.dws_unit_obj_name,
            dws_name=self.rename(func_node['name']),
            dws_type=self.rename(func_node['result_type']),
        )

    def gen_proc_declaration(self, proc_node):
        return self.dws_func_decl.format(
            class_name="",
            unit_obj_name=self.dws_unit_obj_name,
            dws_name=self.rename(proc_node['name']),
        )

    def gen_proc_implementation(self, proc_node):
        proc_declaration = self.dws_func_decl.format(
            class_name=self.dws_class_name+".",
            unit_obj_name=self.dws_unit_obj_name,
            dws_name=self.rename(proc_node['name']),
        )
        var_text = self.gen_impl_var(proc_node)
        body_text = self.gen_proc_body(proc_node)
        return self.dws_func_impl.format(
            declaration=proc_declaration,
            var=var_text,
            body=body_text,
        )

    def gen_proc_body(self, proc_node):
        body = ""
        args_to_call = ""
        for arg in proc_node['args']:
            args_to_call += arg['name'] + ','
            if arg['type'] in self.default_types:
                line = "\t" + arg['name'] + " := info.ValueAs{type}['{dws_name}']; \n"
            else:
                line = "\t" + arg['name'] + " := (info.ValueAsObject['{dws_name}'] as {type}); \n"
            body += line.format(
                dws_name=self.rename(arg['name']),
                name=arg['name'],
                type=arg['type'],
                )
        args_to_call = args_to_call[0:-1]  # slice the last comma
        line = "\t{proc_name}({args}); "
        body += line.format(
            proc_name=proc_node['name'],
            args=args_to_call,
        )
        return body

    #noinspection PyMethodMayBeStatic
    def gen_impl_var(self, node):
        if node['args']:
            var = "var\n"
        else:
            return ""
        for arg in node['args']:
            var += "\t" + arg['name'] + ": " + arg['type'] + ";\n"
        return var[0:-1]  # slice the last \n

    def gen_procs_form(self, proc_nodes, func_nodes):
        proc_list = self.gen_proc_list_text(proc_nodes)
        proc_list += self.gen_func_list_text(func_nodes)
        return self.dws_funcs_form.format(
            proc_list=indent_multiline(proc_list)
        )

    def gen_proc_list_text(self, proc_nodes):
        proc_list = ""
        for proc_node in proc_nodes:
            proc_list += self.gen_proc_text(proc_node)
        return proc_list

    def gen_proc_text(self, proc_node):
        params_text = self.gen_param_list_text(proc_node['args'])
        return self.dws_proc_form.format(
            param_list=indent_multiline(params_text, 2),
            unit_obj_name=self.dws_unit_obj_name,
            dws_name=self.rename(proc_node['name']),
        )

    def gen_param_list_text(self, args):
        param_list_text = ""
        for arg in args:
            param_list_text += self.gen_param_text(arg)
        return param_list_text

    def gen_param_text(self, arg):
        return self.dws_param_form.format(
            dws_name=self.rename(arg['name']),
            dws_type=self.rename(arg['type']),
            bool="False",
        )

    def gen_gen_script_obj_decl(self, obj_type):
        return self.dws_gen_script_obj_decl.format(
            type=obj_type,
            class_name="",
        )

    def gen_gen_script_obj_impl(self, obj_type):
        decl = self.dws_gen_script_obj_decl.format(
            type=obj_type,
            class_name=self.dws_class_name,
        )
        body = self.dws_gen_script_obj_impl_body.format(
            type=obj_type,
        )
        impl = self.dws_gen_script_obj_impl.format(
            declaration=decl,
            body=body,
        )
        return impl

    def rename(self, name):
        if name.lower() in self.dws_rename:
            return self.dws_rename[name.lower()]
        else:
            return name

    def parse(self):
        self.parser.parse_files()

    def sort_parsed_items(self):
        self.dws_tree = {
            'procedure_list': [],
            'function_list': [],
            'class_list': [],
            'array_list': [],
            'enum_list': [],
            'forward_list': [],
            'interface_list': [],
            'record_list': [],
            'set_list': [],
            'synonym_list': [],
            'distinct_synonym_list': [],
            'procedural_func_list': [],
            'procedural_proc_list': [],

        }
        if not self.parser.syntax_tree:
            raise Exception('Syntax tree is empty')
        for file in self.parser.syntax_tree['files']:
            for item in file['items']:
                self.dws_tree[item['type'] + '_list'].append(item['node'])

    def gen_unit_form(self):
        #header
        unit_form = "object {unit_obj_name}: TdwsUnit\n".format(unit_obj_name=self.dws_unit_obj_name)
        unit_form += "\tScript = WScript"
        # parse all types of items
        procedures = self.gen_procs_form(self.dws_tree['procedure_list'],
                                         self.dws_tree['function_list'])

        #add parsed items
        unit_form += indent_multiline(procedures, 1) + "\n"
        #footer
        unit_form += (
            "\tUnitName = '{unit_name}'\n"
            "\tStaticSymbols = {bool}\n"
            "\tLeft = {left}\n"
            "\tTop = {top}\n"
            "end"
        ).format(
            unit_name=self.dws_unit_name,
            bool="False",  # todo: don't place just False for Static symbols
            left=216,  # todo: so hardcode
            top=24,  # todo: so hardcode
        )
        self.unit_form = unit_form
        return unit_form

    def gen_unit_declaration(self):
        declaration = ""
        for proc_node in self.dws_tree['procedure_list']:
            declaration += self.gen_proc_declaration(proc_node)
        for func_node in self.dws_tree['function_list']:
            declaration += self.gen_func_declaration(func_node)
        self.unit_declaration = declaration
        return declaration

    def gen_unit_implementation(self):
        implementation = ""
        for proc_node in self.dws_tree['procedure_list']:
            implementation += self.gen_proc_implementation(proc_node)
        for func_node in self.dws_tree['function_list']:
            implementation += self.gen_func_implementation(func_node)
        self.unit_implementation = implementation
        return implementation

    def gen_unit(self):
        if not self.parser.syntax_tree:
            self.parser.parse_files()
        if not self.dws_tree:
            self.sort_parsed_items()
        self.gen_unit_form()
        self.gen_unit_declaration()
        self.gen_unit_implementation()


def indent_multiline(block, tabs=1):
    indent = "\t" * tabs
    block = indent + block
    return block.replace("\n", "\n" + indent)


def parse_files_print_tree():
    parser_obj = Parser()
    parser_obj.parse_files()
    print(json.dumps(parser_obj.syntax_tree))


def parse_print_dws():
    g = Generator(Parser(), 'WSUnitReport', 'UReport', 'TDMDWSReport')
    g.gen_unit()
    print(g.unit_form)
    print(g.unit_declaration)
    print(g.unit_implementation)

parse_files_print_tree()  # run
#parse_print_dws()  # run!

