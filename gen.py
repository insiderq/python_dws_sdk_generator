#!/usr/local/bin/python3.3


decs = open('decs.txt', 'w')
impls = open('impls.txt', 'w')
methods = open('methods.txt', 'w')
code = open('code.txt')
for a in code:
    #a = "function SRVGetActiveEditor(MainDoc: Boolean): TCustomRVControl;"
    end1 = a.find(" ")
    if a[0:end1] == "property":
        continue
    start2 = a.find("(")
    params_split = []
    result_type = None
    if not start2 == -1:
        end2 = a.find(")", start2+1)
        params = a[start2+1:end2]
        params = params.split(";")
        for param in params:
            params_split.append(param.replace(" ", "").split(":"))
    else:
        start2 = a.find(":")
        end2 = start2
        if start2 == -1:
            start2 = a.find(";")
            end2 = start2

    method_name = a[end1+1:start2]
    decs.write("procedure TDMDWSReport.WSUnitRepClassesTReportMethods"
               + method_name + "(Info: TProgramInfo; var ExtObject: TObject);\n")
    if a[0:end1] == "function":

        result_type = a[end2+2:-2].replace(" ", "")
        if result_type in ['Integer', 'Boolean', 'Float', 'String']:
            impls.write("procedure TDMDWSReport.WSUnitRepClassesTReportMethods"
                        + method_name + "(Info: TProgramInfo; var ExtObject: TObject);\n"
                        + "begin\n" + "\tInfo.ResultAs"+result_type+" := Rep."+ method_name
                        + "("+"".join([ "Info.ValueAs"+param[1]+"['"+param[0]+"']," for param in params_split ])[0:-1]+");\nend;\n"
            )
        else:
            impls.write("procedure TDMDWSReport.WSUnitRepClassesTReportMethods"
                        + method_name + "(Info: TProgramInfo; var ExtObject: TObject);\n"
                        + "begin\n" + "\t(ExtObject As "+result_type+") := Rep."+ method_name
                        + "("+"".join([ "Info.ValueAs"+param[1]+"['"+param[0]+"']," for param in params_split ])[0:-1]+");\nend;\n"
            )

    elif a[0:end1] == "procedure":
        impls.write("procedure TDMDWSReport.WSUnitRepClassesTReportMethods"
                    + method_name + "(Info: TProgramInfo; var ExtObject: TObject);\n"
                    + "begin\n" + "\tRep."+ method_name
                    + "("+"".join([ "Info.ValueAs"+param[1]+"['"+param[0]+"']," for param in params_split ])[0:-1]+");\nend;\n"
        )
    methods.write(
        "item\n\tName = '"+method_name+"'\n"
    )
    if len(params_split):
        methods.write("\tParameters = <\n")
        for param in params_split:
            methods.write(
                "\t\titem\n\t\t\tName = '"+param[0]+"'\n\t\t\tDataType = '"+param[1]+"'\n\t\tend\n"
            )
        methods.write("\t>\n")
    if result_type is not None:
        methods.write("\tResultType = '"+result_type+"'\n")
    methods.write("\tOnEval = WSUnitRepClassesTReportMethods"+method_name+"'\n\tKind = mkFunction\nend\n")

decs.close()
impls.close()
methods.close()


    #    "procedure TDMDWSReport.WSUnitRepClassesTReportMethods"+method_name+"(Info: TProgramInfo; var ExtObject: TObject);"
#
#"procedure TDMDWSReport.WSUnitRepClassesTReportMethods"+method_name+"(Info: TProgramInfo; var ExtObject: TObject);"
#
#
#"procedure TDMDWSReport.WSUnitRepClassesTReportMethods"+method_name+"(Info: TProgramInfo; var ExtObject: TObject);"
#"""begin
#    Info.ResultAs"""+result_type+""" := Rep."""+method_name+"("+param_names+")"+
#"end;"
#
#
#
##functions
#"""
#item
#    Name = '"""+method_name+"""'
#    Parameters = <
#        item
#            Name = '"""+param_name+"""'
#            DataType = '"""+param_type+"""'
#        end
#        >
#    ResultType = '"""+result_type+"""'
#    OnEval = WSUnitRepClassesTReportMethods"""+method_name+"""
#    Kind = mkFunction
#end
#"""
#
##procedures
#"""
#item
#    Name = '"""+method_name+"""'
#    Parameters = <
#        item
#            Name = '"""+param_name+"""'
#            DataType = '"""+param_type+"""'
#        end
#        >
#    OnEval = WSUnitRepClassesTReportMethods"""+method_name+"""
#    Kind = mkFunction
#end
#"""