unit Unit1;

interface
uses SysUtils, Classes, Generics.Defaults, Generics.Collections, Windows,
  mksetint, CTUtils, RndNum, NativeXml, RVEdit,CTBigFCA, RVTypes, RVStyle,
  Vcl.Graphics,RVGrHandler,RVFuncs,RVTable,RichView,CRVData,CTAAReps,
  CTLLTypes,
  CTMemStr;
type
  TArtifact = class// govno
  private

  public
  function AsBitmap: TBitmap;
  function AsEMF: TMetafile;
  function AsXML: string;
  published
  end;



  procedure InsertObjArr(ACon: TCTSubContext; Rep: TRichViewEdit);
  procedure InsertAttrArr(ACon: TCTSubContext; Rep: TRichViewEdit);
  procedure InsertInfoOfContext(ACon: TCTSubContext; Rep: TRichViewEdit);
  procedure InsertTableOfConcepts(Lat: TCTLattice; Rep: TRichViewEdit);
  procedure InsertInfoOfLattice(Lat: TCTLattice; Rep: TrichViewEdit);
  procedure InsertInfoOfConcept(Con:TCTConcept; Rep: TrichViewEdit);

implementation
function TArtifact.AsBitmap:TBitmap;
begin
  //
end;

function TArtifact.AsEMF:TMetafile;
begin
    //
end;

function TArtifact.AsXML:string;
begin
      //
end;

procedure AsBMP(Art: TArtifact;Rep: TRichViewEdit);
var
bmp: TGraphic;
begin
  bmp := RVGraphicHandler.CreateGraphicByType(rvgtBitmap);
  bmp.Assign(Art.AsBitmap());

  rep.InsertPicture('',bmp,rep.DefaultPictureVAlign);
end;

procedure AsEMF( Art: TArtifact;Rep: TRichViewEdit);
var
emf: TGraphic;
begin
  emf := RVGraphicHandler.CreateGraphicByType(rvgtMetafile);
  emf.Assign(Art.AsEMF());
  rep.InsertPicture('',emf,rep.DefaultPictureVAlign);
end;

procedure AsXML( Art: TArtifact;Rep: TRichViewEdit);
begin
  rep.InsertText(Art.AsXML());
end;


procedure InsertTable(Lat: TCTLattice; Rep: TRichViewEdit);
var
  RowCount: Integer;
  ColumnCount: Integer;
  table: TRVTableItemInfo;
  InContext: TCTContext;
//  ObjArr: TObjsOwnArr;
 // AttrsArr: TAttrsOwnArr;
  i,j:Integer;
begin
  RowCount:=Lat.ObjsCount;
  ColumnCount:=Lat.AttrsCount;
  InContext:=Lat.InitialContext;
 // ObjArr:=InContext.Objects;
  //AttrsArr:=InContext.Attributes;
  table:=TRVTableItemInfo.CreateEx(RowCount,ColumnCount,Rep.RVData);
  for i := 0 to RowCount-1 do  // do arrays srart from 0 or 1?
  begin
    for j := 0 to ColumnCount-1 do
      Begin
        table.cells[i,j].Clear;
        if InContext.Elem[i,j] then  table.Cells[i,j].addNL('1',0,0)
        else table.Cells[i,j].addNL('0',0,0);
      End;
  end;
  rep.InsertItem('concept table',table);
end;


procedure InsertObjArr(ACon: TCTSubContext; Rep: TRichViewEdit);
//����� ������� ��������
var
  table: TRVTableItemInfo;
  i,j,ObjCount: integer;
  ObjArr: TObjsOwnArr;
  IndArr: TCTElementIndexArr;
begin
  ObjCount:=Acon.ObjsCount;
  IndArr:=Acon.SubObjsInds;
  ObjArr:=TObjsOwnArr.Create();
  for i:=0 to objcount-1 do
  begin
    //ObjArr[j]:=Acon.Objects[i];//�������� �������������
    objarr.add(Acon.Objects[i]);
  end;
  table:= TRVTableItemInfo.CreateEx(ObjCount+2,2,rep.RVData);
  SetStdTableStyle(Table);
  Table.HeadingRowCount := 1;
  Table.MergeCells(0, 0, 2, 1, false);
  rep.AddItem('SubContext Objects', Table);
  with table.Cells[0,0] do begin
    Color := RPS.RepCol_TabMainTitle;
    Clear;
    AddNL('SubContext Objects',1,1);
  end;
  with table.Cells[1,0] do begin
    Color := RPS.RepCol_TabTitle;
    Clear;
    AddNL('Indexes',1,1);
  end;
  with table.Cells[1,1] do begin
    Color := RPS.RepCol_TabTitle;
    Clear;
    AddNL('Objects',1,1);
  end;
  j:=0;
  for i:=0 to ObjCount-1 do
  begin
     with table.Cells[j+2, 0] do
     begin
      Color := RPS.RepCol_TabCells;
      Clear;
      AddNL(IntToStr(indarr[i]+1),1,1);
      j:=j+1;
     end;
  end;
  j:=0;
  for i:=0 to ObjCount-1 do
  begin
    with table.Cells[j+2, 1] do
    begin
      Color := RPS.RepCol_TabCells;
      Clear;
      AddNL(ObjArr[i].Name,1,1);
      j:=j+1;
    end;
  end;
  Rep.AddNL(' ', 0, 1);
end;

procedure InsertAttrArr(ACon: TCTSubContext; Rep: TRichViewEdit);
//����� ������� ���������
var
  table: TRVTableItemInfo;
  i,j,AttrCount: integer;
  AttrArr: TAttrsOwnArr;
  IndArr: TCTElementIndexArr;
begin
  AttrCount:=Acon.AttrsCount;
  IndArr:=Acon.SubAttrsInds;
  AttrArr:=TAttrsOwnArr.Create();
  for i:=0 to Attrcount-1 do
  begin
    //ObjArr[j]:=Acon.Objects[i];//�������� �������������
    Attrarr.add(Acon.Attributes[i]);
  end;
  table:= TRVTableItemInfo.CreateEx(AttrCount+2,2,rep.RVData);
  SetStdTableStyle(Table);
  Table.HeadingRowCount := 1;
  Table.MergeCells(0, 0, 2, 1, false);
  rep.AddItem('SubContext Attributes', Table);
  with table.Cells[0,0] do begin
    Color := RPS.RepCol_TabMainTitle;
    Clear;
    AddNL('SubContext Attributes',1,1);
  end;
  with table.Cells[1,0] do begin
    Color := RPS.RepCol_TabTitle;
    Clear;
    AddNL('Indexes',1,1);
  end;
  with table.Cells[1,1] do begin
    Color := RPS.RepCol_TabTitle;
    Clear;
    AddNL('Attributes',1,1);
  end;
  j:=0;
  for i:=0 to AttrCount-1 do
  begin
     with table.Cells[j+2, 0] do
     begin
      Color := RPS.RepCol_TabCells;
      Clear;
      AddNL(IntToStr(indarr[i]+1),1,1);
      j:=j+1;
     end;
  end;
  j:=0;
  for i:=0 to AttrCount-1 do
  begin
    with table.Cells[j+2, 1] do
    begin
      Color := RPS.RepCol_TabCells;
      Clear;
      AddNL(AttrArr[i].Name,1,1);
      j:=j+1;
    end;
  end;
  Rep.AddNL(' ', 0, 1);
end;

procedure InsertInfoOfContext(ACon: TCTSubContext; Rep: TRichViewEdit);
//����� ��� ���� �� binary formal context
var
  table: TRVTableItemInfo;
  ObjsCount,AttrsCount,i,j: Integer;
begin
   ObjsCount:=Acon.ObjsCount;
   AttrsCount:=Acon.AttrsCount;
   Table := TRVTableItemInfo.CreateEx(3,2, rep.RVData);
   SetStdTableStyle(Table);
   Table.HeadingRowCount := 2;
   Table.MergeCells(0, 0, 3, 1, false);
   Rep.AddItem('Additional info', table);
   with table.Cells[0,0] do
   begin
    Color := RPS.RepCol_TabMainTitle;
    Clear;
    AddNL('Additional info of Context',1,1);
   end;
  for i:=0 to 1 do
  begin
    for j := 1 to 2 do
    begin
      with table.Cells[j, i] do
      begin
        if j=1 then Color := RPS.RepCol_TabTitle;
        if j=2 then Color := RPS.RepCol_TabCells;
        Clear;
      end;
    end;
  end;
  table.Cells[1,0].AddNL('Objects',1,1);
  table.Cells[1,1].AddNL('Attributes',1,1);
  table.Cells[2,0].AddNL(IntToStr(ObjsCount),1,1);
  table.Cells[2,1].AddNL(IntToStr(AttrsCount),1,1);
  Rep.AddNL(' ', 0, 1);
end;

procedure InsertTableOfConcepts(Lat: TCTLattice; Rep: TRichViewEdit);
// ����� ������� ���������
var
  table: TRVTableItemInfo;
  i,j,ConcCount,k: Integer;
  ConcArr: TConceptArr;
  Objs,Attrs: Tset;
  str:string;

begin
    ConcArr:=lat.Concepts;
    ConcCount:=lat.VertexCount;
    Table := TRVTableItemInfo.CreateEx(ConcCount+2, 3,  Rep.RVData);
    SetStdTableStyle(Table);
    Table.HeadingRowCount := 2;
    Table.MergeCells(0, 0,3, 1, false);
    Rep.AddItem('Table of Concepts', Table);
    with table.Cells[0,0] do
    begin
      Color := RPS.RepCol_TabMainTitle;
      Clear;
      AddNL('Table of Concepts',1,1);
    end;
    for I := 0 to 2 do
    begin
      with table.Cells[1,i] do
      begin
      Color := RPS.RepCol_TabTitle;
      Clear;
      end;
    end;
    table.Cells[1,0].AddNL('Indexes',1,1);
    table.Cells[1,1].AddNL('Objects',1,1);
    table.Cells[1,2].AddNL('Attributes',1,1);
    for i:=0 to ConcCount-1 do
    begin
    for j := 0 to 2 do
      begin
        with table.Cells[2+i,j] do
        begin
          str:='';
          Color := RPS.RepCol_TabCells;
          Clear;
          //bestwidth:=500;//??? 500?
          if j=0 then AddNL(IntToStr(i+1),1,1);
          if j=1 then
          begin
            Objs:=ConcArr[i].Objs;
            for k in objs do
            begin
              str:=str+IntToStr(k)+',';
              end;
            AddNL(str,1,1);
          end;
          if j=2 then
          begin
            Attrs:=ConcArr[i].Attrs;
            for k in Attrs do
            begin
              str:=str+IntToStr(k)+',';
            end;
            AddNL(str,1,1);
          end;
        end;
      end;
    end;
    Rep.AddNL(' ', 0, 1);
end;

procedure InsertInfoOfLattice(Lat: TCTLattice; Rep: TrichViewEdit);
//����� ���� ��� Formal Concept Lattice
var
  table: TRVTableItemInfo;
  ObjsCount,AttrsCount,ConceptsCount,i,j: integer;
begin
  ObjsCount:=Lat.ObjsCount;
  AttrsCount:=Lat.AttrsCount;
  ConceptsCount:=Lat.VertexCount;
  Table := TRVTableItemInfo.CreateEx(3,3, Rep.RVData);
  SetStdTableStyle(Table);
  Table.HeadingRowCount := 2;
  Table.MergeCells(0, 0,  3, 1, false);
  Rep.AddItem('Additional information of lattice', Table);
  with table.Cells[0,0] do
  begin
    Color := RPS.RepCol_TabMainTitle;
    Clear;
    AddNL('Additional information of lattice',1,1);
  end;
  for I := 1 to 2 do
  begin
    for j := 0 to 2 do
    begin
         if i=1 then table.cells[i,j].Color := RPS.RepCol_TabTitle;
         if i=2 then table.cells[i,j].Color := RPS.RepCol_TabCells;
         table.cells[i,j].clear;
    end;
  end;
  table.Cells[1,0].AddNL('Objects',1,1);
  table.Cells[1,1].AddNL('Attributes',1,1);
  table.Cells[1,2].AddNL('Concepts',1,1);
  table.Cells[2,0].AddNL(IntToStr(ObjsCount),1,1);
  table.Cells[2,1].AddNL(IntToStr(AttrsCount),1,1);
  table.Cells[2,2].AddNL(IntToStr(ConceptsCount),1,1);
  Rep.AddNL(' ', 0, 1);
end;

procedure InsertInfoOfConcept(Con:TCTConcept; Rep: TrichViewEdit);
var
  table: TRVTableItemInfo;
  ObjsCount,AttrsCount,i,j: integer;
begin
  ObjsCount:=con.ObjsCount;
  AttrsCount:=con.AttrsCount;
  Table := TRVTableItemInfo.CreateEx(3,2, Rep.RVData);
  SetStdTableStyle(Table);
  Table.HeadingRowCount := 2;
  Table.MergeCells(0, 0,  3, 1, false);
  Rep.AddItem('Additional information of Concepts', Table);
  with table.Cells[0,0] do 
  begin
    Color := RPS.RepCol_TabMainTitle;
    Clear;
    AddNL('Additional information of Concepts',1,1);
  end;
  for I := 1 to 2 do
  begin
    for j := 0 to 1 do
    begin
         if i=1 then table.cells[i,j].Color := RPS.RepCol_TabCells;
         if i=2 then table.cells[i,j].Color := RPS.RepCol_TabCells;
         table.cells[i,j].clear;
    end;
  end;
  table.Cells[1,0].AddNL('Objects',1,1);
  table.Cells[1,1].AddNL('Attributes',1,1);
  table.Cells[2,0].AddNL(IntToStr(ObjsCount),1,1);
  table.Cells[2,1].AddNL(IntToStr(AttrsCount),1,1);
  Rep.AddNL(' ', 0, 1);
end;

procedure Implications( Context:TCTContext; Rep: TRichViewEdit);
var
  table: TRVTableItemInfo;
  ObjsCount,AttrsCount,i,j: Integer;
begin
  //TImplication
end;

end.
// �������� �����, �������� �������

