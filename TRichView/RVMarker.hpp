﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVMarker.pas' rev: 27.00 (Windows)

#ifndef RvmarkerHPP
#define RvmarkerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvmarker
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVMarkerItemInfo;
class DELPHICLASS TRVMarkerList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMarkerItemInfo : public Rvitem::TRVSimpleRectItemInfo
{
	typedef Rvitem::TRVSimpleRectItemInfo inherited;
	
private:
	int FWidth;
	int FHeight;
	int FDescent;
	int FOverhang;
	int FCachedIndexInList;
	void __fastcall DoPaint(int x, int y, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Rvstyle::TRVStyle* Style, Dlines::TRVDrawLineInfo* dli, Rvstyle::TRVColorMode ColorMode, bool UseCustomPPI);
	
protected:
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	void __fastcall CalcSize(Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, int &Width, int &Height, int &Desc, int &Overhang, Rvstyle::PRVScreenAndDevice sad, bool ForMinWidth, int &HShift, int &SpaceBefore);
	void __fastcall CalcDisplayString(Rvstyle::TRVStyle* RVStyle, TRVMarkerList* List, int Index);
	Rvstyle::TRVListLevel* __fastcall GetLevelInfoEx(Rvstyle::TRVStyle* RVStyle, int LevelNo);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	
public:
	int ListNo;
	int Level;
	int Counter;
	bool Reset;
	int StartFrom;
	System::UnicodeString DisplayString;
	bool NoHTMLImageSize;
	__fastcall TRVMarkerItemInfo(System::Classes::TPersistent* RVData, int AListNo, int ALevel, int AStartFrom, bool AReset);
	__fastcall virtual TRVMarkerItemInfo(System::Classes::TPersistent* RVData);
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall Assign(Rvitem::TCustomRVItemInfo* Source);
	Rvstyle::TRVListLevel* __fastcall GetLevelInfo(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	virtual bool __fastcall GetBoolValue(Rvitem::TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(Rvitem::TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	virtual bool __fastcall PrintToBitmap(Vcl::Graphics::TBitmap* Bkgnd, bool Preview, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	virtual int __fastcall GetImageWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetLeftOverhang(void);
	void __fastcall HTMLOpenOrCloseTags(System::Classes::TStream* Stream, int OldLevelNo, int NewLevelNo, Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvstyle::TRVHTMLListCSSList* ListBullets);
	void __fastcall SaveHTMLSpecial(System::Classes::TStream* Stream, TRVMarkerItemInfo* Prev, Rvstyle::TRVStyle* RVStyle, bool UseCSS, Rvstyle::TRVSaveOptions SaveOptions, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	System::UnicodeString __fastcall GetLICSS(System::Classes::TPersistent* RVData, int ItemNo, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, Rvclasses::TRVList* Bullets);
	DYNAMIC void __fastcall FillRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList, System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	int __fastcall GetIndexInList(System::Classes::TList* List);
	DYNAMIC bool __fastcall SetExtraIntProperty(Rvitem::TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(Rvitem::TRVExtraItemProperty Prop, int &Value);
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVMarkerItemInfo(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMarkerList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	TRVMarkerList* PrevMarkerList;
	int __fastcall InsertAfter(TRVMarkerItemInfo* InsertMe, TRVMarkerItemInfo* AfterMe);
	void __fastcall RecalcCounters(int StartFrom, Rvstyle::TRVStyle* RVStyle, bool ForAllLists);
	bool __fastcall FindParentMarker(int Index, TRVMarkerItemInfo* Marker, TRVMarkerList* &ParentList, int &ParentIndex);
	void __fastcall RecalcDisplayStrings(Rvstyle::TRVStyle* RVStyle);
	void __fastcall SaveToStream(System::Classes::TStream* Stream, int Count, bool IncludeSize);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, bool IncludeSize);
	void __fastcall SaveTextToStream(System::Classes::TStream* Stream, int Count);
	void __fastcall LoadText(const Rvtypes::TRVAnsiString s, System::Classes::TPersistent* RVData);
	void __fastcall LoadBinary(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData);
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVMarkerList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVMarkerList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE Rvstyle::TRVListLevel* __fastcall RVGetLevelInfo(Rvstyle::TRVStyle* RVStyle, int ListNo, int Level);
}	/* namespace Rvmarker */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVMARKER)
using namespace Rvmarker;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvmarkerHPP
