�
 TFORM1 0  TPF0TForm1Form1Left� TopkWidth�Height�CaptionRichViewXML documentationColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight 	TRichViewrvLeft Top!Width�Height�Cursor	crDefaultAlignalClient	PopupMenupmTabOrder TabStop	BackgroundStyle
bsNoBitmapBorderStylebsNoneBottomMarginCPEventKindcpeNone
Delimiters .;,:(){}"/\DoInPaletteModervpaCreateCopiesFirstJumpNo HScrollVisible	
LeftMarginMaxTextWidth MinTextWidth RightMarginStyleRVStyle1	TopMarginTracking	VScrollVisible	OnJumprvJump  	TRichViewrv2Left Top Width�Height!Cursor	crDefaultAlignalTopColorclInfoBk	PopupMenupmTabOrderTabStop	BackgroundStyle
bsNoBitmapBorderStylebsNoneBottomMarginCPEventKindcpeNone
Delimiters .;,:(){}"/\DoInPaletteModervpaCreateCopiesFirstJumpNo HScrollVisible	
LeftMarginMaxTextWidth MinTextWidth OptionsrvoScrollToEndrvoShowPageBreaksrvoAutoCopyTextrvoAutoCopyRVFrvoAutoCopyImagervoAutoCopyRTFrvoFormatInvalidatervoDblClickSelectsWordrvoRClickDeselects RightMarginStyleRVStyle2TabNavigationrvtnNone	TopMarginTracking	VScrollVisible	OnJumprv2Jump  TRichViewXMLxmlDefaultBreakColorclNoneDefaultBreakWidthDefaultParaNo DefaultStyleNo DefaultVAlignrvvaBaselineEncodingwindows-1250InsLoadBackgroundInsStyleLoadingModeslmMergeLoadBackground	LoadTagMode	RichViewrvRootElementbookSaveBackground	SaveImgToFilesSaveSelectionBackground	SaveSelectionImgToFilesSaveSelectionStyles	SaveStyleNames	
SaveStyles	StyleLoadingMode	slmIgnoreUseStyleNamesLeftxTopx  TRVStyleRVStyle2
TextStyles	StyleNamenormalFontNameArial 	StyleNamejumpFontNameArialStylefsUnderline ColorclBlue
HoverColorclRedJump	 	StyleNamesymbolCharsetSYMBOL_CHARSETFontNameSymbol  
ParaStyles	StyleNamecenter	Alignment	rvaCenterOptions   Left� TopX  TRichViewXMLxml2DefaultBreakColorclNoneDefaultBreakWidthDefaultParaNo DefaultStyleNo DefaultVAlignrvvaBaselineEncodingwindows-1250InsLoadBackgroundInsStyleLoadingModeslmMergeLoadBackgroundLoadTagMode	RichViewrv2RootElementbookSaveBackground	SaveImgToFilesSaveSelectionBackground	SaveSelectionImgToFilesSaveSelectionStyles	SaveStyleNames	
SaveStyles	StyleLoadingMode	slmIgnoreUseStyleNamesLeft� Topx  
TPopupMenupmLeft� Top�  	TMenuItemCopy1Caption&CopyShortCutC@OnClick
Copy1Click  	TMenuItemN1Caption-  	TMenuItemProperties1Caption&PropertiesOnClickProperties1Click  	TMenuItemMethods1Caption&MethodsOnClickMethods1Click  	TMenuItemEvents1Caption&EventsOnClickEvents1Click   TRVStyleRVStyle1
TextStyles	StyleNameNormalFontNameArial
JumpCursor	crDefault 	StyleNameHeadingFontNameArialSizeStylefsBold ColorclBlue
JumpCursor	crDefaultNextStyleNo  	StyleName
SubheadingFontNameArialSizeStylefsBold ColorclNavy
JumpCursor	crDefaultNextStyleNo  	StyleNameCodeFontNameCourier NewColorclBlack
JumpCursor	crDefault 	StyleNameKeywordFontNameCourier NewStylefsBold ColorclBlack
JumpCursor	crDefault 	StyleNameJumpFontNameArialStylefsUnderline ColorclBlue
HoverColorclRedJump	 	StyleNameBoldFontNameArialStylefsBold   
ParaStyles	StyleNameLeftFirstIndent
NextParaNo Options  	StyleNameHeading
LeftIndentRightIndentSpaceBefore
SpaceAfter	Alignment	rvaCenterBorder.BorderOffsets.LeftBorder.BorderOffsets.RightBorder.BorderOffsets.TopBorder.BorderOffsets.BottomBackground.ColorclSilver
NextParaNo Options   
HoverColorclRedCheckpointColorclLimeDefUnicodeStyleLeftxTopX   