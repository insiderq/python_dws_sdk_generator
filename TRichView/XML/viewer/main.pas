unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, RichViewXML, RVScroll, RichView, RVStyle, ExtCtrls, Menus, CRVFData;

type
  TForm1 = class(TForm)
    rv: TRichView;
    xml: TRichViewXML;
    rv2: TRichView;
    RVStyle2: TRVStyle;
    xml2: TRichViewXML;
    pm: TPopupMenu;
    Copy1: TMenuItem;
    N1: TMenuItem;
    Properties1: TMenuItem;
    Methods1: TMenuItem;
    Events1: TMenuItem;
    RVStyle1: TRVStyle;
    procedure rvJump(Sender: TObject; id: Integer);
    procedure FormCreate(Sender: TObject);
    procedure rv2Jump(Sender: TObject; id: Integer);
    procedure Copy1Click(Sender: TObject);
    procedure Properties1Click(Sender: TObject);
    procedure Methods1Click(Sender: TObject);
    procedure Events1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  xml.LoadFromFile(ExtractFilePath(Application.ExeName) + 'index.xml');
  xml2.LoadFromFile(ExtractFilePath(Application.ExeName) + 'contents.xml');
  rv.Format;
  rv2.Format;
end;

procedure TForm1.rvJump(Sender: TObject; id: Integer);
var
  Tag: string;
  RVData: TCustomRVFormattedData;
  ItemNo: Integer;
begin
  rv.GetJumpPointLocation(id, RVData, ItemNo);
  Tag := RVData.GetItemTag(ItemNo);
  xml.LoadFromFile(xml.BaseFilePath +Tag + '.xml');
  rv.Format;
end;

procedure TForm1.rv2Jump(Sender: TObject; id: Integer);
var
  Tag: string;
  RVData: TCustomRVFormattedData;
  ItemNo: Integer;
begin
  rv2.GetJumpPointLocation(id, RVData, ItemNo);
  Tag := RVData.GetItemTag(ItemNo);
  xml.LoadFromFile(xml.BaseFilePath +Tag + '.xml');
  rv.Format;
end;

procedure TForm1.Copy1Click(Sender: TObject);
begin
  rv.Copy;
end;

procedure TForm1.Properties1Click(Sender: TObject);
begin
  xml.LoadFromFile(ExtractFilePath(Application.ExeName) + 'properties.xml');
end;

procedure TForm1.Methods1Click(Sender: TObject);
begin
  xml.LoadFromFile(ExtractFilePath(Application.ExeName) + 'methods.xml');
end;

procedure TForm1.Events1Click(Sender: TObject);
begin
  xml.LoadFromFile(ExtractFilePath(Application.ExeName) + 'methods.xml');
end;

end.
