unit RichViewXML;

interface

{$I RV_Defs.inc}

uses SysUtils, Classes, Graphics, RichView, RVStyle, RVEdit,
  RVXMLBase, RVXMLRoutines, CRVData, RVItem, Controls, RVERVData,
  RVUndo, RVRVData, DLines, PtblRV, RVTypes, RVUni;


type

  TRichViewXML = class;

  ERichViewXMLError = class (Exception);

  TRichViewXMLUnknownTagEvent = procedure (Sender: TRichViewXML; Node: TXMLTag;
    RVData: TCustomRVData; ItemNo: Integer) of object;

  TRichViewXMLLoadControlEvent = procedure (Sender: TRichViewXML; const ClassName, ObjectData: string;
    var Control: TControl) of object;

  TRichViewXMLAfterLoadControlEvent = procedure (Sender: TRichViewXML; Control: TControl) of object;

  TRichViewXMLUnknownItemEvent = procedure (Sender: TRichViewXML; RVData: TCustomRVData;
    ItemNo: Integer; Item: TCustomRVItemInfo; Node: TXMLTag) of object;

  TRichViewXMLSaveControlEvent = procedure (Sender: TRichViewXML; Control: TControl; var S: string) of object;

  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRichViewXML = class (TComponent)
  private
    FBaseFilePath: string;
    FDefaultStyle: TRichViewXMLStyle;
    FEncoding: string;
    FInsLoadBackground: Boolean;
    FInsLoadPrint: Boolean;
    FInsStyleLoadingMode: TStyleLoadingMode;
    FLoadBackground: Boolean;
    FLoadPrint: Boolean;
    FLoadTagMode: Boolean;
    FMeta: TXMLTag;
    FOnLoadControl: TRichViewXMLLoadControlEvent;
    FOnAfterLoadControl: TRichViewXMLAfterLoadControlEvent;
    FOnSaveControl: TRichViewXMLSaveControlEvent;
    FOnUnknownItem: TRichViewXMLUnknownItemEvent;
    FOnUnknownTag: TRichViewXMLUnknownTagEvent;
    FRichView, FRVHeader, FRVFooter: TCustomRichView;
    FRootElement: string;
    FRVPrint: TRVPrint;
    FSaveBackground: Boolean;
    FSaveImgToFiles: Boolean;
    FSavePrint: Boolean;
    FSaveSelectionBackground: Boolean;
    FSaveSelectionImgToFiles: Boolean;
    FSaveSelectionPrint: Boolean;
    FSaveSelectionStyles: Boolean;
    FSaveStyleNames: Boolean;
    FSaveStyles: Boolean;
    FStyleLoadingMode: TStyleLoadingMode;
    FUseStyleNames: Boolean;
    FWantTabs: Boolean;
    FSaveLineBreaks: Boolean;
    FCanChangeUnits: Boolean;
    FSaveDocParameters, FLoadDocParameters: Boolean;
    function _OnLoadControl(const ClassName, ObjectData: string): TControl;
    procedure _OnAfterLoadControl(Control: TControl);
    function _OnSaveControl(Control: TControl): string;
    procedure _OnUnknownItem(RVData: TCustomRVData; ItemNo: Integer; Item: TCustomRVItemInfo;
      Node: TXMLTag);
    procedure _OnUnknownTag(Node: TXMLTag; RVData: TCustomRVData; ItemNo: Integer);
    procedure DoLoadFromStream(Stream: TStream; ClearBefore: Boolean);
    procedure DoLoadFromXML(Node: TXMLTag; ClearBefore: Boolean);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure InsertFromFile(FileName: string);
    procedure InsertFromStream(Stream: TStream);
    procedure InsertFromXML(Node: TXMLTag);
    procedure LoadFromFile(FileName: string);
    procedure LoadFromStream(Stream: TStream);
    procedure LoadFromXML(Node: TXMLTag);
    procedure AppendFromFile(FileName: string);
    procedure AppendFromStream(Stream: TStream);
    procedure AppendFromXML(Node: TXMLTag);
    procedure SaveToFile(FileName: string);
    procedure SaveToStream(Stream: TStream);
    procedure SaveToXML(Node: TXMLTag);
    procedure SaveSelectionToFile(FileName: string);
    procedure SaveSelectionToStream(Stream: TStream);
    procedure SaveSelectionToXML(Node: TXMLTag);
    property Meta: TXMLTag read FMeta write FMeta;
  published
    property BaseFilePath: string read FBaseFilePath write FBaseFilePath;
    property DefaultBreakColor: TColor read FDefaultStyle.BreakColor write FDefaultStyle.BreakColor;
    property DefaultBreakWidth: Byte read FDefaultStyle.BreakWidth write FDefaultStyle.BreakWidth;
    property DefaultParaNo: Integer read FDefaultStyle.ParaNo write FDefaultStyle.ParaNo;
    property DefaultStyleNo: Integer read FDefaultStyle.StyleNo write FDefaultStyle.StyleNo;
    property DefaultVAlign: TRVVAlign read FDefaultStyle.VAlign write FDefaultStyle.VAlign;
    property Encoding: string read FEncoding write FEncoding;
    property InsLoadBackground: Boolean read FInsLoadBackground write FInsLoadBackground;
    property InsLoadPrint: Boolean read FInsLoadPrint write FInsLoadPrint;
    property InsStyleLoadingMode: TStyleLoadingMode read FInsStyleLoadingMode write FInsStyleLoadingMode;
    property LoadBackground: Boolean read FLoadBackground write FLoadBackground;
    property LoadPrint: Boolean read FLoadPrint write FLoadPrint;
    property LoadTagMode: Boolean read FLoadTagMode write FLoadTagMode;
    property RichView: TCustomRichView read FRichView write FRichView;
    property RVHeader: TCustomRichView read FRVHeader write FRVHeader;
    property RVFooter: TCustomRichView read FRVFooter write FRVFooter;        
    property RootElement: string read FRootElement write FRootElement;
    property RVPrint: TRVPrint read FRVPrint write FRVPrint;
    property OnLoadControl: TRichViewXMLLoadControlEvent read FOnLoadControl write FOnLoadControl;
    property OnAfterLoadControl: TRichViewXMLAfterLoadControlEvent read FOnAfterLoadControl write FOnAfterLoadControl;    
    property OnSaveControl: TRichViewXMLSaveControlEvent read FOnSaveControl write FOnSaveControl;
    property OnUnknownItem: TRichViewXMLUnknownItemEvent read FOnUnknownItem write FOnUnknownItem;
    property OnUnknownTag: TRichViewXMLUnknownTagEvent read FOnUnknownTag write FOnUnknownTag;
    property SaveBackground: Boolean read FSaveBackground write FSaveBackground;
    property SaveImgToFiles: Boolean read FSaveImgToFiles write FSaveImgToFiles;
    property SavePrint: Boolean read FSavePrint write FSavePrint;
    property SaveSelectionBackground: Boolean read FSaveSelectionBackground write FSaveSelectionBackground;
    property SaveSelectionImgToFiles: Boolean read FSaveSelectionImgToFiles write FSaveSelectionImgToFiles;
    property SaveSelectionPrint: Boolean read FSaveSelectionPrint write FSaveSelectionPrint;
    property SaveSelectionStyles: Boolean read FSaveSelectionStyles write FSaveSelectionStyles;
    property SaveLineBreaks: Boolean read FSaveLineBreaks write FSaveLineBreaks default False;
    property SaveStyleNames: Boolean read FSaveStyleNames write FSaveStyleNames;
    property SaveStyles: Boolean read FSaveStyles write FSaveStyles;
    property StyleLoadingMode: TStyleLoadingMode read FStyleLoadingMode write FStyleLoadingMode;
    property UseStyleNames: Boolean read FUseStyleNames write FUseStyleNames;
    property WantTabs: Boolean read FWantTabs write FWantTabs;
    property CanChangeUnits: Boolean read FCanChangeUnits write FCanChangeUnits default False;
    property SaveDocParameters: Boolean read FSaveDocParameters write FSaveDocParameters default True;
    property LoadDocParameters: Boolean read FLoadDocParameters write FLoadDocParameters default True;
  end;

procedure Register;

implementation

uses CRVFData;

function TRichViewXML._OnLoadControl(const ClassName, ObjectData: string): TControl;
begin
  Result := nil;
  FOnLoadControl(Self, ClassName, ObjectData, Result);
end;

procedure TRichViewXML._OnAfterLoadControl(Control: TControl);
begin
  FOnAfterLoadControl(Self, Control);
end;

function TRichViewXML._OnSaveControl(Control: TControl): string;
begin
  FOnSaveControl(Self, Control, Result);
end;

procedure TRichViewXML._OnUnknownItem(RVData: TCustomRVData; ItemNo: Integer; Item: TCustomRVItemInfo;
  Node: TXMLTag);
begin
  FOnUnknownItem(Self, RVData, ItemNo, Item, Node);
end;

procedure TRichViewXML._OnUnknownTag(Node: TXMLTag; RVData: TCustomRVData; ItemNo: Integer);
begin
  FOnUnknownTag(Self, Node, RVData, ItemNo);
end;

constructor TRichViewXML.Create(AOwner: TComponent);
begin
  inherited;
  FBaseFilePath := '';
  FDefaultStyle := RVXMLRoutines.DefaultXMLStyle;
  FEncoding := 'UTF-8';
  FRootElement := 'rvxml';
  FInsLoadBackground := False;
  FInsStyleLoadingMode := slmMerge;
  FLoadBackground := True;
  FLoadTagMode := True;
  FMeta := TXMLTag.Create('meta', nil, nil);
  FRichView := nil;
  FSaveBackground := True;
  FSaveImgToFiles := False;
  FSaveSelectionBackground := True;
  FSaveSelectionImgToFiles := False;
  FSaveSelectionStyles := True;
  FSaveStyles := True;
  FSaveStyleNames := True;
  FStyleLoadingMode := slmReplace;
  FUseStyleNames := False;
  FSaveDocParameters := True;
  FLoadDocParameters := True;
end;

destructor TRichViewXML.Destroy;
begin
  FMeta.Free;
  inherited;
end;

procedure TRichViewXML.InsertFromFile(FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  FBaseFilePath := ExtractFilePath(ExpandUNCFileName(FileName));
  try
    InsertFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

type
  TInsertPos = (ipBefore, ipAfter, ipInside);

procedure TRichViewXML.InsertFromStream(Stream: TStream);
var
  T: TXMLTree;
  T2: TXMLTag;
begin
  T := TXMLTree.Create(FEncoding);
  T.BaseFilePath := FBaseFilePath;
  //T.UnicodeTags.Add('utext');
  T.LoadFromStream(Stream);
  T2 := T.Items.FindTagOfName(TRVAnsiString(FRootElement));
  if T2 = nil then
  begin
    raise ERichViewXMLError.Create('No root element');
    Exit;
  end;
  InsertFromXML(T2);
  T.Free;
end;

procedure TRichViewXML.InsertFromXML(Node: TXMLTag);
var
  X: TCustomRichViewEdit;
  AOnLoadControl: TRVXMLLoadControlEvent;
  AOnAfterLoadControl: TRVXMLAfterLoadControlEvent;
  AOnUnknownTag: TRVXMLUnknownTagEvent;
  UI: TRVUndoInsertItemsInfo;
  T2: TXMLTag;
  Offs, DOffs, DIStartNo,DIEndNo: Integer;
  FullReformat, FullReformat2: Boolean;
  NonFirstItemsAdded, ItemsAdded, InsertPoint, LastInserted: Integer;
begin
  if not Assigned(FRichView) then Exit;

  if not (FRichView is TCustomRichViewEdit) then
  begin
    raise ERichViewXMLError.Create('Insertion is allowed only in RichViewEdit');
    Exit;
  end;

  {$IFNDEF RVDONOTUSEINPLACE}
  if (TCustomRichViewEdit(FRichView).InplaceEditor <> nil) and
    (TCustomRichViewEdit(FRichView).InplaceEditor is TCustomRichViewEdit) then
    begin
      X := TCustomRichViewEdit(FRichView);
      try
        FRichView := TCustomRichViewEdit(TCustomRichViewEdit(FRichView).InplaceEditor);
        InsertFromXML(Node);
      finally
        FRichView := X;
      end;
      Exit;
    end;
  {$ENDIF}

  T2 := Node.Items.FindTagOfName('meta');
  if T2 <> nil then
    FMeta := T2
  else begin
    FMeta.Free;
    FMeta := TXMLTag.Create('meta', nil, nil);
  end;

  //if Node.Root.UnicodeTags.IndexOf('utext') = -1 then
  //  Node.Root.UnicodeTags.Add('utext');

  AOnLoadControl := _OnLoadControl;
  AOnAfterLoadControl := _OnAfterLoadControl;
  AOnUnknownTag := _OnUnknownTag;
  if not Assigned(FOnLoadControl) then
    AOnLoadControl := nil;
  if not Assigned(FOnAfterLoadControl) then
    AOnAfterLoadControl := nil;
  if not Assigned(FOnUnknownTag) then
    AOnUnknownTag := nil;

  if not TCustomRichViewEdit(FRichView).BeforeInserting then
    exit;
  try
    FRichView.RVData.GetParaBoundsEx(TRVEditRVData(FRichView.RVData).CaretDrawItemNo,
                                   TRVEditRVData(FRichView.RVData).CaretDrawItemNo, DIStartNo,DIEndNo, True);

    InsertPoint := TCustomRichViewEdit(FRichView).CurItemNo;
    if TCustomRichViewEdit(FRichView).OffsetInCurItem>=FRichView.GetOffsAfterItem(InsertPoint) then
      inc(InsertPoint);

    ItemsAdded := FRichView.ItemCount;

    TRVEditRVData(FRichView.RVData).FRVFInserted := False;
    RVLoadFromXML(FRichView, nil, nil, FRVPrint, FInsLoadPrint, False, Node, InsertPoint,
      FInsLoadBackground, FInsStyleLoadingMode,
      AOnUnknownTag, AOnLoadControl, AOnAfterLoadControl, True, FullReformat, NonFirstItemsAdded, False,
      CanChangeUnits  {$IFNDEF RVDONOTUSESTYLETEMPLATES}, nil{$ENDIF});
    if not TRVEditRVData(FRichView.RVData).FRVFInserted then
      exit;

    ItemsAdded := FRichView.ItemCount-ItemsAdded;

    LastInserted := InsertPoint+NonFirstItemsAdded;
    Offs := FRichView.RVData.GetOffsAfterItem(LastInserted);

    ui := TRVEditRVData(FRichView.RVData).Do_InsertItems_1(InsertPoint+1,
      NonFirstItemsAdded);
    TRVEditRVData(FRichView.RVData).Do_InsertItems_2(InsertPoint+1,
      NonFirstItemsAdded, ui, FullReformat2);
    FullReformat := FullReformat or FullReformat2 or
      (FRichView.RVData.CalculateMinItemsWidthPlusEx(InsertPoint,LastInserted) >
       FRichView.RVData.DocumentWidth);
    TRVEditRVData(FRichView.RVData).ConcateAfterAdding(InsertPoint, LastInserted,
      ItemsAdded, Offs);
    if FullReformat then begin
      FRichView.RVData.Format_(False, True, False, 0,
        FRichView.RVData.GetCanvas, False, False, False, False, False);
      FRichView.Invalidate;
      end
    else
      FRichView.RVData.FormatParasExact(DIStartNo,DIEndNo,ItemsAdded,False);
    TRVEditRVData(FRichView.RVData).Item2DrawItem(LastInserted, Offs,
      TRVEditRVData(FRichView.RVData).CaretDrawItemNo, DOffs);
    TRVEditRVData(FRichView.RVData).OnChangeCaretLine(DOffs-2);
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    if FRichView.UseStyleTemplates and (FRichView.StyleTemplateInsertMode<>rvstimIgnoreSourceStyleTemplates) then
      FRichView.RVData.State := FRichView.RVData.State+[rvstForceStyleChangeEvent];
    {$ENDIF}
    TRVEditRVData(FRichView.RVData).ChangeCaret(False,True,False,True);
    TRVEditRVData(FRichView.RVData).DoAfterFormatAndCaretPositioning(False);
  finally
    TCustomRichViewEdit(FRichView).AfterInserting;
  end;
end;

procedure TRichViewXML.LoadFromFile(FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  FBaseFilePath := ExtractFilePath(ExpandUNCFileName(FileName));
  try
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TRichViewXML.AppendFromFile(FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  FBaseFilePath := ExtractFilePath(ExpandUNCFileName(FileName));
  try
    AppendFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TRichViewXML.DoLoadFromStream(Stream: TStream; ClearBefore: Boolean);
var
  T: TXMLTree;
  T2: TXMLTag;
begin
  T := TXMLTree.Create(FEncoding);
  T.BaseFilePath := FBaseFilePath;
  //T.UnicodeTags.Add('utext');
  T.LoadFromStream(Stream);
  T2 := T.Items.FindTagOfName(TRVAnsiString(FRootElement));
  if T2 = nil then
  begin
    raise ERichViewXMLError.Create('No root element');
    Exit;
  end;
  DoLoadFromXML(T2, ClearBefore);
  T.Free;
end;

procedure TRichViewXML.LoadFromStream(Stream: TStream);
begin
  DoLoadFromStream(Stream, True);
end;

procedure TRichViewXML.AppendFromStream(Stream: TStream);
begin
  DoLoadFromStream(Stream, False);
end;

procedure TRichViewXML.DoLoadFromXML(Node: TXMLTag; ClearBefore: Boolean);
var
  AOnLoadControl: TRVXMLLoadControlEvent;
  AOnAfterLoadControl: TRVXMLAfterLoadControlEvent;
  AOnUnknownTag: TRVXMLUnknownTagEvent;
  T2: TXMLTag;
  FullReformat: Boolean;
  Dummy, Dummy2: Integer;

begin
  if not Assigned(FRichView) then Exit;

  T2 := Node.Items.FindTagOfName('meta');
  if T2 <> nil then
    FMeta := T2
  else begin
    FMeta.Free;
    FMeta := TXMLTag.Create('meta', nil, nil);
  end;

  //if Node.Root.UnicodeTags.IndexOf('utext') = -1 then
  //  Node.Root.UnicodeTags.Add('utext');

  AOnLoadControl := _OnLoadControl;
  AOnAfterLoadControl := _OnAfterLoadControl;
  AOnUnknownTag := _OnUnknownTag;
  if not Assigned(FOnLoadControl) then
    AOnLoadControl := nil;
  if not Assigned(FOnAfterLoadControl) then
    AOnAfterLoadControl := nil;
  if not Assigned(FOnUnknownTag) then
    AOnUnknownTag := nil;

  if ClearBefore then
    FRichView.Clear;
  if RVHeader<>nil then
    RVHeader.Clear;
  if RVFooter<>nil then
    RVFooter.Clear;
  Dummy2 := FRichView.ItemCount;
  RVLoadFromXML(FRichView, FRVHeader, FRVFooter, FRVPrint,
    FLoadPrint, FLoadDocParameters, Node, Dummy2, FLoadBackground, FStyleLoadingMode,
    AOnUnknownTag, AOnLoadControl, AOnAfterLoadControl, False, FullReformat,
    Dummy, FLoadTagMode, CanChangeUnits {$IFNDEF RVDONOTUSESTYLETEMPLATES}, nil{$ENDIF});
end;

procedure TRichViewXML.LoadFromXML(Node: TXMLTag);
begin
  DoLoadFromXML(Node, True);
end;

procedure TRichViewXML.AppendFromXML(Node: TXMLTag);
begin
  DoLoadFromXML(Node, False);
end;

procedure TRichViewXML.SaveToFile(FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  FBaseFilePath := ExtractFilePath(ExpandUNCFileName(FileName));
  try
    SaveToStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TRichViewXML.SaveToStream(Stream: TStream);
var
  T: TXMLTree;
begin
  T := TXMLTree.Create(FEncoding);
  T.Items.AddTag(TRVAnsiString(FRootElement), T, T);
  T.BaseFilePath := FBaseFilePath;
  SaveToXML(T.Items[0]);
  T.SaveToStream(Stream, FWantTabs);
  T.Free;
end;

procedure TRichViewXML.SaveToXML(Node: TXMLTag);
var
  AOnSaveControl: TRVXMLSaveControlEvent;
  AOnUnknownItem: TRVXMLUnknownItemEvent;
begin
  if not Assigned(FRichView) then Exit;

  //if Node.Root.UnicodeTags.IndexOf('utext') = -1 then
  //  Node.Root.UnicodeTags.Add('utext');

  FMeta.Level := Node.Level + 1;
  if (FMeta.Items.Count > 0) or (FMeta.Value <> '') then
    Node.Items.Add(FMeta);

  AOnSaveControl := _OnSaveControl;
  AOnUnknownItem := _OnUnknownItem;
  if not Assigned(FOnSaveControl) then
    AOnSaveControl := nil;
  if not Assigned(FOnUnknownItem) then
    AOnUnknownItem := nil;

  FRichView.RVData.DoBeforeSaving;
  RVSaveToXML(FRichView, FRVHeader, FRVFooter, Node, FRVPrint,
    FSavePrint, FSaveDocParameters, FDefaultStyle, FSaveStyles,
    FSaveBackground, FSaveImgToFiles, 0, -1, FRichView.RVData.Items.Count-1, -1,
    AOnUnknownItem, AOnSaveControl, FUseStyleNames, FSaveStyleNames,
    FSaveLineBreaks);
end;

procedure TRichViewXML.SaveSelectionToFile(FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  FBaseFilePath := ExtractFilePath(ExpandUNCFileName(FileName));
  try
    SaveSelectionToStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TRichViewXML.SaveSelectionToStream(Stream: TStream);
var
  T: TXMLTree;
begin
  T := TXMLTree.Create(FEncoding);
  T.Items.AddTag(TRVAnsiString(FRootElement), T, T);
  T.BaseFilePath := FBaseFilePath;
  SaveSelectionToXML(T.Items[0]);
  T.SaveToStream(Stream, FWantTabs);
  T.Free;
end;


procedure TRichViewXML.SaveSelectionToXML(Node: TXMLTag);
var
  S, SO, E, EO: Integer;
  AOnSaveControl: TRVXMLSaveControlEvent;
  AOnUnknownItem: TRVXMLUnknownItemEvent;
begin
  if not Assigned(FRichView) then Exit;

  //if Node.Root.UnicodeTags.IndexOf('utext') = -1 then
  //  Node.Root.UnicodeTags.Add('utext');

  FMeta.Level := Node.Level + 1;
  if (FMeta.Items.Count > 0) or (FMeta.Value <> '') then
    Node.Items.Add(FMeta);

  AOnSaveControl := _OnSaveControl;
  AOnUnknownItem := _OnUnknownItem;
  if not Assigned(FOnSaveControl) then
    AOnSaveControl := nil;
  if not Assigned(FOnUnknownItem) then
    AOnUnknownItem := nil;

  FRichView.GetSelectionBounds(S, SO, E, EO, True);
  RVSaveToXML(FRichView, nil, nil, Node, FRVPrint, FSaveSelectionPrint, False, FDefaultStyle, FSaveSelectionStyles,
    FSaveSelectionBackground, FSaveSelectionImgToFiles, S, SO, E, EO,
    AOnUnknownItem, AOnSaveControl, FUseStyleNames, FSaveStyleNames, FSaveLineBreaks);
end;

procedure TRichViewXML.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation=opRemove) then begin
    if (AComponent=FRVPrint) then
      FRVPrint := nil
    else if (AComponent=FRichView) then
      FRichView := nil
    else if (AComponent=FRVHeader) then
      FRVHeader := nil
    else if (AComponent=FRVFooter) then
      FRVFooter := nil;
  end;
end;

{---------------------------------------------------------------------------}

procedure Register;
begin
  RegisterComponents('RichView', [TRichViewXML]);
end;


end.
