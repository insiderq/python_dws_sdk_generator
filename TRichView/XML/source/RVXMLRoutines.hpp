﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVXMLRoutines.pas' rev: 27.00 (Windows)

#ifndef RvxmlroutinesHPP
#define RvxmlroutinesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVXMLBase.hpp>	// Pascal unit
#include <RVXMLMisc.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVFloatingPos.hpp>	// Pascal unit
#include <RVFloatingBox.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvxmlroutines
{
//-- type declarations -------------------------------------------------------
typedef System::TMetaClass* TControlClass;

typedef void __fastcall (__closure *TRVXMLUnknownTagEvent)(Rvxmlbase::TXMLTag* Node, Crvdata::TCustomRVData* RVData, int ItemNo);

typedef Vcl::Controls::TControl* __fastcall (__closure *TRVXMLLoadControlEvent)(const System::UnicodeString ClassName, const System::UnicodeString ObjectData);

typedef void __fastcall (__closure *TRVXMLAfterLoadControlEvent)(Vcl::Controls::TControl* Control);

typedef void __fastcall (__closure *TRVXMLUnknownItemEvent)(Crvdata::TCustomRVData* RVData, int ItemNo, Rvitem::TCustomRVItemInfo* Item, Rvxmlbase::TXMLTag* Node);

typedef System::UnicodeString __fastcall (__closure *TRVXMLSaveControlEvent)(Vcl::Controls::TControl* Control);

enum DECLSPEC_DENUM TStyleLoadingMode : unsigned char { slmIgnore, slmReplace, slmMerge, slmMap };

struct DECLSPEC_DRECORD TRichViewXMLStyle
{
public:
	int StyleNo;
	int ParaNo;
	Rvstyle::TRVVAlign VAlign;
	System::Uitypes::TColor BreakColor;
	System::Byte BreakWidth;
};


typedef TRichViewXMLStyle *PRichViewXMLStyle;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRichViewXMLStyle DefaultXMLStyle;
extern DELPHI_PACKAGE bool __fastcall RVLoadFromXML(Richview::TCustomRichView* RichView, Richview::TCustomRichView* RVHeader, Richview::TCustomRichView* RVFooter, Ptblrv::TRVPrint* RVPrint, bool LoadPrint, bool LoadDocParameters, Rvxmlbase::TXMLTag* Node, int &InsertPosition, bool LoadBackground, TStyleLoadingMode StyleLoadingMode, TRVXMLUnknownTagEvent OnUnknownTag, TRVXMLLoadControlEvent OnLoadControl, TRVXMLAfterLoadControlEvent OnAfterLoadControl, bool EditMode, bool &FullReformat, int &NonFirstItemsAdded, bool LoadTagMode, bool CanChangeUnits, Rvstyle::TRVStyleTemplateCollection* ReadStyleTemplates);
extern DELPHI_PACKAGE void __fastcall RVSaveToXML(Richview::TCustomRichView* RichView, Richview::TCustomRichView* RVHeader, Richview::TCustomRichView* RVFooter, Rvxmlbase::TXMLTag* Node, Ptblrv::TRVPrint* RVPrint, bool SavePrint, bool SaveDocParameters, const TRichViewXMLStyle &Style, bool SaveStyles, bool SaveBackground, bool SaveImgToFiles, int SelStart, int SelOffset, int SelEnd, int SelEndOffset, TRVXMLUnknownItemEvent OnUnknownItem, TRVXMLSaveControlEvent OnSaveControl, bool UseStyleNames, bool SaveStyleNames, bool SaveLineBreaks);
}	/* namespace Rvxmlroutines */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVXMLROUTINES)
using namespace Rvxmlroutines;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvxmlroutinesHPP
