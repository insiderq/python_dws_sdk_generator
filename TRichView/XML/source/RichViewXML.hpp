﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RichViewXML.pas' rev: 27.00 (Windows)

#ifndef RichviewxmlHPP
#define RichviewxmlHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVXMLBase.hpp>	// Pascal unit
#include <RVXMLRoutines.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVERVData.hpp>	// Pascal unit
#include <RVUndo.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Richviewxml
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS ERichViewXMLError;
#pragma pack(push,4)
class PASCALIMPLEMENTATION ERichViewXMLError : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ERichViewXMLError(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ERichViewXMLError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ERichViewXMLError(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ERichViewXMLError(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ERichViewXMLError(NativeUInt Ident, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ERichViewXMLError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ERichViewXMLError(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ERichViewXMLError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERichViewXMLError(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERichViewXMLError(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERichViewXMLError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERichViewXMLError(NativeUInt Ident, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ERichViewXMLError(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRichViewXML;
typedef void __fastcall (__closure *TRichViewXMLUnknownTagEvent)(TRichViewXML* Sender, Rvxmlbase::TXMLTag* Node, Crvdata::TCustomRVData* RVData, int ItemNo);

typedef void __fastcall (__closure *TRichViewXMLLoadControlEvent)(TRichViewXML* Sender, const System::UnicodeString ClassName, const System::UnicodeString ObjectData, Vcl::Controls::TControl* &Control);

typedef void __fastcall (__closure *TRichViewXMLAfterLoadControlEvent)(TRichViewXML* Sender, Vcl::Controls::TControl* Control);

typedef void __fastcall (__closure *TRichViewXMLUnknownItemEvent)(TRichViewXML* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, Rvitem::TCustomRVItemInfo* Item, Rvxmlbase::TXMLTag* Node);

typedef void __fastcall (__closure *TRichViewXMLSaveControlEvent)(TRichViewXML* Sender, Vcl::Controls::TControl* Control, System::UnicodeString &S);

class PASCALIMPLEMENTATION TRichViewXML : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	System::UnicodeString FBaseFilePath;
	Rvxmlroutines::TRichViewXMLStyle FDefaultStyle;
	System::UnicodeString FEncoding;
	bool FInsLoadBackground;
	bool FInsLoadPrint;
	Rvxmlroutines::TStyleLoadingMode FInsStyleLoadingMode;
	bool FLoadBackground;
	bool FLoadPrint;
	bool FLoadTagMode;
	Rvxmlbase::TXMLTag* FMeta;
	TRichViewXMLLoadControlEvent FOnLoadControl;
	TRichViewXMLAfterLoadControlEvent FOnAfterLoadControl;
	TRichViewXMLSaveControlEvent FOnSaveControl;
	TRichViewXMLUnknownItemEvent FOnUnknownItem;
	TRichViewXMLUnknownTagEvent FOnUnknownTag;
	Richview::TCustomRichView* FRichView;
	Richview::TCustomRichView* FRVHeader;
	Richview::TCustomRichView* FRVFooter;
	System::UnicodeString FRootElement;
	Ptblrv::TRVPrint* FRVPrint;
	bool FSaveBackground;
	bool FSaveImgToFiles;
	bool FSavePrint;
	bool FSaveSelectionBackground;
	bool FSaveSelectionImgToFiles;
	bool FSaveSelectionPrint;
	bool FSaveSelectionStyles;
	bool FSaveStyleNames;
	bool FSaveStyles;
	Rvxmlroutines::TStyleLoadingMode FStyleLoadingMode;
	bool FUseStyleNames;
	bool FWantTabs;
	bool FSaveLineBreaks;
	bool FCanChangeUnits;
	bool FSaveDocParameters;
	bool FLoadDocParameters;
	Vcl::Controls::TControl* __fastcall _OnLoadControl(const System::UnicodeString ClassName, const System::UnicodeString ObjectData);
	void __fastcall _OnAfterLoadControl(Vcl::Controls::TControl* Control);
	System::UnicodeString __fastcall _OnSaveControl(Vcl::Controls::TControl* Control);
	void __fastcall _OnUnknownItem(Crvdata::TCustomRVData* RVData, int ItemNo, Rvitem::TCustomRVItemInfo* Item, Rvxmlbase::TXMLTag* Node);
	void __fastcall _OnUnknownTag(Rvxmlbase::TXMLTag* Node, Crvdata::TCustomRVData* RVData, int ItemNo);
	void __fastcall DoLoadFromStream(System::Classes::TStream* Stream, bool ClearBefore);
	void __fastcall DoLoadFromXML(Rvxmlbase::TXMLTag* Node, bool ClearBefore);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TRichViewXML(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRichViewXML(void);
	void __fastcall InsertFromFile(System::UnicodeString FileName);
	void __fastcall InsertFromStream(System::Classes::TStream* Stream);
	void __fastcall InsertFromXML(Rvxmlbase::TXMLTag* Node);
	void __fastcall LoadFromFile(System::UnicodeString FileName);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	void __fastcall LoadFromXML(Rvxmlbase::TXMLTag* Node);
	void __fastcall AppendFromFile(System::UnicodeString FileName);
	void __fastcall AppendFromStream(System::Classes::TStream* Stream);
	void __fastcall AppendFromXML(Rvxmlbase::TXMLTag* Node);
	void __fastcall SaveToFile(System::UnicodeString FileName);
	void __fastcall SaveToStream(System::Classes::TStream* Stream);
	void __fastcall SaveToXML(Rvxmlbase::TXMLTag* Node);
	void __fastcall SaveSelectionToFile(System::UnicodeString FileName);
	void __fastcall SaveSelectionToStream(System::Classes::TStream* Stream);
	void __fastcall SaveSelectionToXML(Rvxmlbase::TXMLTag* Node);
	__property Rvxmlbase::TXMLTag* Meta = {read=FMeta, write=FMeta};
	
__published:
	__property System::UnicodeString BaseFilePath = {read=FBaseFilePath, write=FBaseFilePath};
	__property System::Uitypes::TColor DefaultBreakColor = {read=FDefaultStyle.BreakColor, write=FDefaultStyle.BreakColor, nodefault};
	__property System::Byte DefaultBreakWidth = {read=FDefaultStyle.BreakWidth, write=FDefaultStyle.BreakWidth, nodefault};
	__property int DefaultParaNo = {read=FDefaultStyle.ParaNo, write=FDefaultStyle.ParaNo, nodefault};
	__property int DefaultStyleNo = {read=FDefaultStyle.StyleNo, write=FDefaultStyle.StyleNo, nodefault};
	__property Rvstyle::TRVVAlign DefaultVAlign = {read=FDefaultStyle.VAlign, write=FDefaultStyle.VAlign, nodefault};
	__property System::UnicodeString Encoding = {read=FEncoding, write=FEncoding};
	__property bool InsLoadBackground = {read=FInsLoadBackground, write=FInsLoadBackground, nodefault};
	__property bool InsLoadPrint = {read=FInsLoadPrint, write=FInsLoadPrint, nodefault};
	__property Rvxmlroutines::TStyleLoadingMode InsStyleLoadingMode = {read=FInsStyleLoadingMode, write=FInsStyleLoadingMode, nodefault};
	__property bool LoadBackground = {read=FLoadBackground, write=FLoadBackground, nodefault};
	__property bool LoadPrint = {read=FLoadPrint, write=FLoadPrint, nodefault};
	__property bool LoadTagMode = {read=FLoadTagMode, write=FLoadTagMode, nodefault};
	__property Richview::TCustomRichView* RichView = {read=FRichView, write=FRichView};
	__property Richview::TCustomRichView* RVHeader = {read=FRVHeader, write=FRVHeader};
	__property Richview::TCustomRichView* RVFooter = {read=FRVFooter, write=FRVFooter};
	__property System::UnicodeString RootElement = {read=FRootElement, write=FRootElement};
	__property Ptblrv::TRVPrint* RVPrint = {read=FRVPrint, write=FRVPrint};
	__property TRichViewXMLLoadControlEvent OnLoadControl = {read=FOnLoadControl, write=FOnLoadControl};
	__property TRichViewXMLAfterLoadControlEvent OnAfterLoadControl = {read=FOnAfterLoadControl, write=FOnAfterLoadControl};
	__property TRichViewXMLSaveControlEvent OnSaveControl = {read=FOnSaveControl, write=FOnSaveControl};
	__property TRichViewXMLUnknownItemEvent OnUnknownItem = {read=FOnUnknownItem, write=FOnUnknownItem};
	__property TRichViewXMLUnknownTagEvent OnUnknownTag = {read=FOnUnknownTag, write=FOnUnknownTag};
	__property bool SaveBackground = {read=FSaveBackground, write=FSaveBackground, nodefault};
	__property bool SaveImgToFiles = {read=FSaveImgToFiles, write=FSaveImgToFiles, nodefault};
	__property bool SavePrint = {read=FSavePrint, write=FSavePrint, nodefault};
	__property bool SaveSelectionBackground = {read=FSaveSelectionBackground, write=FSaveSelectionBackground, nodefault};
	__property bool SaveSelectionImgToFiles = {read=FSaveSelectionImgToFiles, write=FSaveSelectionImgToFiles, nodefault};
	__property bool SaveSelectionPrint = {read=FSaveSelectionPrint, write=FSaveSelectionPrint, nodefault};
	__property bool SaveSelectionStyles = {read=FSaveSelectionStyles, write=FSaveSelectionStyles, nodefault};
	__property bool SaveLineBreaks = {read=FSaveLineBreaks, write=FSaveLineBreaks, default=0};
	__property bool SaveStyleNames = {read=FSaveStyleNames, write=FSaveStyleNames, nodefault};
	__property bool SaveStyles = {read=FSaveStyles, write=FSaveStyles, nodefault};
	__property Rvxmlroutines::TStyleLoadingMode StyleLoadingMode = {read=FStyleLoadingMode, write=FStyleLoadingMode, nodefault};
	__property bool UseStyleNames = {read=FUseStyleNames, write=FUseStyleNames, nodefault};
	__property bool WantTabs = {read=FWantTabs, write=FWantTabs, nodefault};
	__property bool CanChangeUnits = {read=FCanChangeUnits, write=FCanChangeUnits, default=0};
	__property bool SaveDocParameters = {read=FSaveDocParameters, write=FSaveDocParameters, default=1};
	__property bool LoadDocParameters = {read=FLoadDocParameters, write=FLoadDocParameters, default=1};
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Richviewxml */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RICHVIEWXML)
using namespace Richviewxml;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RichviewxmlHPP
