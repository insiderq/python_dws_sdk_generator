unit RVXMLMisc;

interface

{$I RV_Defs.inc}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  SysUtils, Classes, RVItem, Controls, Graphics, CRVData, RVXMLBase, RVTable,
  RVStyle, RVFMisc, ImgList, RVClasses, RVTypes, RVUni;

type

  TBullet = record
    OldImgIndex, ImgIndex: Integer;
    ImgList: TCustomImageList;
  end;

  TBulletList = class (TList)
  private
    function Get(Index: Integer): TBullet;
    procedure Put(Index: Integer; const Value: TBullet);
  public
    procedure Add(ImgIndex, OldImgIndex: Integer; ImgList: TCustomImageList);
    function GetImgIndex(OldImgIndex: Integer; ImgList: TCustomImageList): Integer;
    property Items[Index: Integer]: TBullet read Get write Put;
  end;

  TImgListList = class (TList)
  private
    function Get(Index: Integer): TCustomImageList;
    procedure Put(Index: Integer; const Value: TCustomImageList);
  public
    function Add: TCustomImageList;
    procedure LoadFromXML(Node: TXMLTag);
    procedure SaveToXML(Node: TXMLTag; SaveToFiles: Boolean);
    property Items[Index: Integer]: TCustomImageList read Get write Put;
  end;

procedure ScanBullets(RVData: TCustomRVData; BulletList: TBulletList; ImgList: TImgListList;
  SelStart, SelEnd: Integer);

function GetImageFileName(const Path, Name, Ext: string): string;

implementation

{---------------------------------------------------------------------------}

procedure SetBulletInfoEx(RVData: TCustomRVData; ItemNo: Integer; Name:
  TRVAnsiString; Tag: TRVTag; ImgIndex: Integer; ImgList: TCustomImageList);
begin
  TRVBulletItemInfo(RVData.GetItem(ItemNo)).ImageList := ImgList;
  RVData.SetBulletInfo(ItemNo, Name, ImgIndex, nil, Tag);
end;

procedure ScanBullets(RVData: TCustomRVData; BulletList: TBulletList; ImgList: TImgListList;
  SelStart, SelEnd: Integer);
var
  I, J, ImgIndex, X, Y: Integer;
  Tag: TRVTag;
  Name: TRVAnsiString;
  Bmp: TBitmap;
  Img: TCustomImageList;
  Table: TRVTableItemInfo;
  TempImgList: TCustomImageList;
  BulletListCreated: Boolean;
begin
  if (SelStart >= RVData.Items.Count) or (SelStart < 0) then Exit;
  if SelEnd >= RVData.Items.Count then SelEnd := RVData.Items.Count - 1;
  if SelStart > SelEnd then Exit;
  BulletListCreated := BulletList = nil;
  if BulletListCreated then
    BulletList := TBulletList.Create;
  try
    with ImgList do
    begin
      for I := SelStart to SelEnd do
        if RVData.GetItemStyle(I) = rvsBullet then
        begin
          RVData.GetBulletInfo(I, Name, ImgIndex, TempImgList, Tag);
          Bmp := TBitmap.Create;
          TempImgList.GetBitmap(ImgIndex, Bmp);
          J := BulletList.GetImgIndex(ImgIndex, TempImgList);
          if J = -1 then
          begin
            Img := ImgList.Add;
            Img.Height := Bmp.Height;
            Img.Width := Bmp.Width;
            Img.AddMasked(Bmp, Bmp.Canvas.Pixels[0, 0]);
            BulletList.Add(ImgList.Count - 1, ImgIndex, TempImgList);
          end else
            Img := ImgList.Items[J];
          SetBulletInfoEx(RVData, I, Name, Tag, 0, Img);
        end else if RVData.GetItemStyle(I) = rvsTable then
        begin
          Table := TRVTableItemInfo(RVData.GetItem(I));
          for X := 0 to Table.Rows.Count - 1 do
            for Y := 0 to Table.Rows[0].Count - 1 do
              if Table.Cells[X, Y] <> nil then
                ScanBullets(Table.Cells[X, Y].GetRVData, BulletList, ImgList, 0,
                  Table.Cells[X, Y].GetRVData.Items.Count);
        end;
    end;
  finally
    if BulletListCreated then
      BulletList.Free;
  end;
end;

function GetImageFileName(const Path, Name, Ext: string): string;
var
  I: Integer;
begin
  I := 1;
  Result := Path + Name + '1.' + Ext;
  while FileExists(Result) do
  begin
    Inc(I);
    Result := Path + Name + IntToStr(I) + '.' + Ext;
  end;
end;

{---------------------------------------------------------------------------}

function TBulletList.Get(Index: Integer): TBullet;
begin
  Result := TBullet(inherited Get(Index)^);
end;

procedure TBulletList.Put(Index: Integer; const Value: TBullet);
begin
  inherited Put(Index, @Value);
end;

procedure TBulletList.Add(ImgIndex, OldImgIndex: Integer; ImgList: TCustomImageList);
var
  Bullet: ^TBullet;
begin
  New(Bullet);
  Bullet^.OldImgIndex := OldImgIndex;
  Bullet^.ImgIndex := ImgIndex;
  Bullet^.ImgList := ImgList;
  inherited Add(Bullet);
end;

function TBulletList.GetImgIndex(OldImgIndex: Integer; ImgList: TCustomImageList): Integer;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if (OldImgIndex = Items[I].OldImgIndex) and (ImgList = Items[I].ImgList) then
    begin
      Result := Items[I].ImgIndex;
      Exit;
    end;
  Result := -1;
end;

{---------------------------------------------------------------------------}

function TImgListList.Add: TCustomImageList;
begin
  Result := TCustomImageList.Create(nil);
  inherited Add(Result);
end;

function TImgListList.Get(Index: Integer): TCustomImageList;
begin
  Result := inherited Get(Index);
end;

procedure TImgListList.LoadFromXML(Node: TXMLTag);
var
  I: Integer;
  Bmp: TBitmap;
  Img: TCustomImageList;
  Stream: TRVMemoryStream;
begin
  for I := 0 to Node.Items.Count - 1 do
  begin

    if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name) = 'img' then
    begin
      Stream := TRVMemoryStream.Create;
      Bmp := TBitmap.Create;
      try
        RVFTextString2Stream(Node.Items[I].GetANSIValueString, Stream);
        Stream.Position := 0;
        Bmp.LoadFromStream(Stream);
      finally
        Stream.Free;
      end;
      Img := Self.Add;
      Img.Width := Bmp.Width;
      Img.Height := Bmp.Height;
      Img.AddMasked(Bmp, Bmp.Canvas.Pixels[0, 0]);
    end

    else if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name) = 'imgsource' then
    begin
      Bmp := TBitmap.Create;
      Bmp.LoadFromFile(Node.Root.BaseFilePath + Node.Items[I].GetValueString);
      Img := Self.Add;
      Img.Width := Bmp.Width;
      Img.Height := Bmp.Height;
      Img.AddMasked(Bmp, Bmp.Canvas.Pixels[0, 0]);
    end;

  end;
end;

procedure TImgListList.Put(Index:Integer; const Value: TCustomImageList);
begin
  inherited Put(Index, Value);
end;

procedure TImgListList.SaveToXML(Node: TXMLTag; SaveToFiles: Boolean);
var
  I: Integer;
  Bmp: TBitmap;
  Stream: TRVMemoryStream;
  Data: TRVAnsiString;
  FileName: string;
  T: TXMLTag;
begin
  for I := 0 to Self.Count - 1 do
  begin
    Stream := TRVMemoryStream.Create;
    Bmp := TBitmap.Create;
    Self.Items[I].GetBitmap(0, Bmp);

    if SaveToFiles then
    begin
      FileName := GetImageFileName(Node.Root.BaseFilePath, 'bullet', 'bmp');
      Bmp.SaveToFile(FileName);
      T := Node.Items.AddTag('imgsource', Node, Node.Root);
      {$IFDEF RVUNICODESTR}
      T.Value := RVU_GetRawUnicode(ExtractFileName(FileName));
      T.UnicodeMode := xmleRawUnicode;
      {$ELSE}
      T.Value := ExtractFileName(FileName);
      {$ENDIF}
    end else
    begin
      try
        Bmp.SaveToStream(Stream);
        Stream.Position := 0;
        Data := RVFStream2TextString(Stream);
        T := Node.Items.AddTag('img', Node, Node.Root);
        T.Value := Data;
      finally
        Stream.Free;
      end;
    end;

    Bmp.Free;
  end;
end;

end.
