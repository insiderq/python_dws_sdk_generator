﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVXMLDXE6.dpk' rev: 27.00 (Windows)

#ifndef Rvxmldxe6HPP
#define Rvxmldxe6HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <RVXMLRoutines.hpp>	// Pascal unit
#include <RichViewXML.hpp>	// Pascal unit
#include <RVXMLBase.hpp>	// Pascal unit
#include <RVXMLMisc.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Internal.ExcUtils.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.VarUtils.hpp>	// Pascal unit
#include <System.Variants.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <System.TimeSpan.hpp>	// Pascal unit
#include <System.SyncObjs.hpp>	// Pascal unit
#include <System.Generics.Defaults.hpp>	// Pascal unit
#include <System.Rtti.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.DateUtils.hpp>	// Pascal unit
#include <System.IOUtils.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Actions.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <System.HelpIntfs.hpp>	// Pascal unit
#include <Winapi.UxTheme.hpp>	// Pascal unit
#include <Vcl.GraphUtil.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <System.Win.ComObj.hpp>	// Pascal unit
#include <Winapi.FlatSB.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVAnimate.hpp>	// Pascal unit
#include <RVRTF.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVThumbMaker.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvxmldxe6
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvxmldxe6 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVXMLDXE6)
using namespace Rvxmldxe6;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Rvxmldxe6HPP
