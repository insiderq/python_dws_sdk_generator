﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVXMLBase.pas' rev: 27.00 (Windows)

#ifndef RvxmlbaseHPP
#define RvxmlbaseHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvxmlbase
{
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<Rvtypes::TRVAnsiString, 134217728> TRVRawStringArray;

typedef TRVRawStringArray *PRVRawStringArray;

class DELPHICLASS TRVXMLRawByteStringList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVXMLRawByteStringList : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FCount;
	int FCapacity;
	TRVRawStringArray *FItems;
	
public:
	void __fastcall Clear(void);
	void __fastcall Add(const Rvtypes::TRVRawByteString s);
	void __fastcall SaveToStream(System::Classes::TStream* Stream);
	__fastcall virtual ~TRVXMLRawByteStringList(void);
public:
	/* TObject.Create */ inline __fastcall TRVXMLRawByteStringList(void) : System::TObject() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TXMLValueType : unsigned char { vtText, vtCDATA };

typedef char * *PPChar;

class DELPHICLASS TXMLAttr;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TXMLAttr : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Rvtypes::TRVRawByteString FTempValue;
	
public:
	Rvtypes::TRVAnsiString Name;
	System::UnicodeString Value;
public:
	/* TObject.Create */ inline __fastcall TXMLAttr(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TXMLAttr(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TXMLAttrList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TXMLAttrList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	TXMLAttr* operator[](int Index) { return Items[Index]; }
	
protected:
	HIDESBASE TXMLAttr* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TXMLAttr* const Value);
	
public:
	TXMLAttr* __fastcall AddAttrTmp(const Rvtypes::TRVAnsiString Name, const Rvtypes::TRVRawByteString Value);
	TXMLAttr* __fastcall AddAttr(const Rvtypes::TRVAnsiString Name, const System::UnicodeString Value);
	virtual void __fastcall Clear(void);
	TXMLAttr* __fastcall FindAttrOfName(Rvtypes::TRVAnsiString Name);
	__property TXMLAttr* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TXMLAttrList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TXMLAttrList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TXMLTagValueEncoding : unsigned char { xmleANSI, xmleRawUnicode, xmleUTF8 };

class DELPHICLASS TXMLTag;
class DELPHICLASS TXMLTagList;
class DELPHICLASS TXMLTree;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TXMLTag : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Rvtypes::TRVAnsiString Name;
	Rvtypes::TRVRawByteString Value;
	int Level;
	TXMLValueType ValueType;
	TXMLTagList* Items;
	TXMLAttrList* Attr;
	TXMLTag* Parent;
	TXMLTree* Root;
	TXMLTagValueEncoding UnicodeMode;
	unsigned TagCodePage;
	unsigned ValueCodePage;
	void __fastcall Clear(void);
	__fastcall TXMLTag(Rvtypes::TRVRawByteString Str, TXMLTag* AParent, TXMLTree* ARoot);
	__fastcall virtual ~TXMLTag(void);
	void __fastcall GetDocument(TRVXMLRawByteStringList* S, bool Tabs, bool UTF8);
	TXMLTag* __fastcall NextSibling(void);
	System::UnicodeString __fastcall GetValueString(void);
	Rvtypes::TRVAnsiString __fastcall GetANSIValueString(void);
	Rvtypes::TRVRawByteString __fastcall GetRawUnicodeValueString(void);
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TXMLTagList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	TXMLTag* operator[](int Index) { return Items[Index]; }
	
protected:
	HIDESBASE TXMLTag* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TXMLTag* const Value);
	
public:
	TXMLTag* __fastcall AddTag(Rvtypes::TRVRawByteString Str, TXMLTag* Parent, TXMLTree* Root);
	TXMLTag* __fastcall InsertTag(int Index, Rvtypes::TRVRawByteString Str, TXMLTag* Parent, TXMLTree* Root);
	virtual void __fastcall Clear(void);
	TXMLTag* __fastcall FindTagOfName(Rvtypes::TRVAnsiString Name);
	__property TXMLTag* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TXMLTagList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TXMLTagList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TXMLTree : public TXMLTag
{
	typedef TXMLTag inherited;
	
public:
	System::UnicodeString Encoding;
	System::UnicodeString BaseFilePath;
	__fastcall TXMLTree(System::UnicodeString Enc);
	__fastcall virtual ~TXMLTree(void);
	void __fastcall ExtractBaseFilePath(System::UnicodeString FileName);
	HIDESBASE void __fastcall GetDocument(TRVXMLRawByteStringList* S, bool Tabs);
	void __fastcall LoadFromFile(System::UnicodeString FileName);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	void __fastcall SaveToFile(System::UnicodeString FileName, bool Tabs);
	void __fastcall SaveToStream(System::Classes::TStream* Stream, bool Tabs);
	void __fastcall Parse(Rvtypes::TRVRawByteString S, unsigned CodePage);
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define crlf L"\r\n"
static const System::WideChar tab = (System::WideChar)(0x9);
}	/* namespace Rvxmlbase */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVXMLBASE)
using namespace Rvxmlbase;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvxmlbaseHPP
