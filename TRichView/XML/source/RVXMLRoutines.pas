unit RVXMLRoutines;

interface

{$I RV_Defs.inc}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  SysUtils, Classes, Windows, Graphics, Controls, StdCtrls,
  {$IFDEF RICHVIEWDEFXE2}
  UITypes,
  {$ENDIF}
  RichView, RVStyle, CRVData,
  RVScroll, RVTable, RVFMisc,
  RVXMLBase, RVXMLMisc, RVEdit, RVItem, RVClasses,
  PtblRV, RVMarker, RVFuncs, CRVFData, RVUni, RVTypes,
  {$IFNDEF RVDONOTUSEDOCPARAMS}RVDocParams, Printers,{$ENDIF}
  {$IFNDEF RVDONOTUSESEQ}RVSeqItem, RVNote, RVSidenote,
  RVFloatingPos,RVFloatingBox,{$ENDIF}
  RVLabelItem, RVGrHandler, RVStr;

type

  TControlClass = class of TControl;

  TRVXMLUnknownTagEvent = procedure (Node: TXMLTag; RVData: TCustomRVData; ItemNo: Integer) of object;

  TRVXMLLoadControlEvent = function (const ClassName, ObjectData: string): TControl of object;

  TRVXMLAfterLoadControlEvent = procedure (Control: TControl) of object;

  TRVXMLUnknownItemEvent = procedure (RVData: TCustomRVData; ItemNo: Integer; Item: TCustomRVItemInfo;
    Node: TXMLTag) of object;

  TRVXMLSaveControlEvent = function (Control: TControl): string of object;

  TStyleLoadingMode = (slmIgnore, slmReplace, slmMerge, slmMap);

  TRichViewXMLStyle = record
    StyleNo, ParaNo: Integer;
    VAlign: TRVVAlign;
    BreakColor: TColor;
    BreakWidth: Byte;
  end;

  PRichViewXMLStyle = ^TRichViewXMLStyle;

const
  DefaultXMLStyle: TRichViewXMLStyle = (StyleNo: 0; ParaNo: 0; VAlign: rvvaBaseLine;
    BreakColor: clNone; BreakWidth: 1);

function RVLoadFromXML(RichView, RVHeader, RVFooter: TCustomRichView; RVPrint: TRVPrint;
  LoadPrint, LoadDocParameters: Boolean;
  Node: TXMLTag; var InsertPosition: Integer;
  LoadBackground: Boolean; StyleLoadingMode: TStyleLoadingMode;
  OnUnknownTag: TRVXMLUnknownTagEvent;
  OnLoadControl: TRVXMLLoadControlEvent;
  OnAfterLoadControl: TRVXMLAfterLoadControlEvent;
  EditMode: Boolean; var FullReformat: Boolean;
  var NonFirstItemsAdded: Integer;
  LoadTagMode, CanChangeUnits: Boolean
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; ReadStyleTemplates: TRVStyleTemplateCollection
  {$ENDIF}): Boolean;

procedure RVSaveToXML(RichView, RVHeader, RVFooter: TCustomRichView; Node: TXMLTag;
  RVPrint: TRVPrint; SavePrint, SaveDocParameters: Boolean;
  Style: TRichViewXMLStyle; SaveStyles: Boolean;
  SaveBackground: Boolean; SaveImgToFiles: Boolean;
  SelStart, SelOffset, SelEnd, SelEndOffset: Integer;
  OnUnknownItem: TRVXMLUnknownItemEvent;
  OnSaveControl: TRVXMLSaveControlEvent;
  UseStyleNames, SaveStyleNames, SaveLineBreaks: Boolean);

implementation

uses ImgList, DLines;

function GetLengthVal(const Val: String; RVStyle: TRVStyle;
  ValUnits: TRVStyleUnits): TRVStyleLength;
begin
  Result := RVStyle.DifferentUnitsToUnits(StrToInt(Val), ValUnits);
end;

function LoadBorderProperty(const AttrName: TRVAnsiString; Value: String;
  Border: TRVBorder; RVStyle: TRVStyle; XMLUnits: TRVStyleUnits): Boolean;
begin
  Result := True;
  if AttrName = 'borderleftoffset' then
    Border.BorderOffsets.Left := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'borderrightoffset' then
    Border.BorderOffsets.Right := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'bordertopoffset' then
    Border.BorderOffsets.Top := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'borderbottomoffset' then
    Border.BorderOffsets.Bottom := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'bordercolor' then
    Border.Color := StringToColor(Value)
  else if AttrName = 'borderintwidth' then
    Border.InternalWidth := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'borderstyle' then begin
    Value := LowerCase(Value);
    if Value = 'none' then
      Border.Style := rvbNone
    else if Value = 'single' then
      Border.Style := rvbSingle
    else if Value = 'double' then
      Border.Style := rvbDouble
    else if Value = 'triple' then
      Border.Style := rvbTriple
    else if Value = 'thickinside' then
      Border.Style := rvbThickInside
    else if Value = 'thickoutside' then
      Border.Style := rvbThickOutside;
    end
  else if AttrName = 'borderleftvisible' then begin
    if Value = '1' then
      Border.VisibleBorders.Left := True
    else if Value = '0' then
      Border.VisibleBorders.Left := False;
    end
  else if AttrName = 'borderrightvisible' then begin
    if Value = '1' then
      Border.VisibleBorders.Right := True
    else if Value = '0' then
      Border.VisibleBorders.Right := False;
    end
  else if AttrName = 'bordertopvisible' then begin
    if Value = '1' then
      Border.VisibleBorders.Top := True
    else if Value = '0' then
      Border.VisibleBorders.Top := False;
    end
  else if AttrName = 'borderbottomvisible' then begin
    if Value = '1' then
      Border.VisibleBorders.Bottom := True
    else if Value = '0' then
      Border.VisibleBorders.Bottom := False;
    end
  else if AttrName = 'borderwidth' then
    Border.Width := GetLengthVal(Value, RVStyle, XMLUnits)
  else
    Result := False;
end;

function LoadBackgroundRectProperty(const AttrName: TRVAnsiString; const Value: String;
  Background: TRVBackgroundRect; RVStyle: TRVStyle; XMLUnits: TRVStyleUnits): Boolean;
begin
  Result := True;
  if AttrName = 'backleftoffset' then
    Background.BorderOffsets.Left := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'backrightoffset' then
    Background.BorderOffsets.Right := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'backtopoffset' then
    Background.BorderOffsets.Top := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'backbottomoffset' then
    Background.BorderOffsets.Bottom := GetLengthVal(Value, RVStyle, XMLUnits)
  else if AttrName = 'backcolor' then
    Background.Color := StringToColor(Value)
  else
    Result := False;
end;

{-----------------------------LoadRVDataFromXML-----------------------------}

function LoadRVDataFromXML(RVData: TCustomRVData; Node: TXMLTag;
  Bullets: TImgListList; Default: PRichViewXMLStyle;
  var InsertPosition: Integer;
  PaletteMode: TRVPaletteAction; RVPalette: HPalette;
  RVLogPalette: PLogPalette; PrevCP: TRVCPInfo;
  StyleMap, ParaMap, ListMap: TRVIntegerList;
  OnUnknownTag: TRVXMLUnknownTagEvent;
  OnLoadControl: TRVXMLLoadControlEvent;
  OnAfterLoadControl: TRVXMLAfterLoadControlEvent;
  RVStyle: TRVStyle;
  EditMode: Boolean;
  var FirstTime, FullReformat: Boolean;
  var NonFirstItemsAdded: Integer;
  TagsArePChars: Boolean; XMLUnits: TRVStyleUnits): Boolean;

var Position, NewListNo: Integer;

  function FindNamedStyleNo(const Name: string): Integer;
  var
    I: Integer;
  begin
    for I := 0 to RVStyle.TextStyles.Count - 1 do
      if LowerCase(RVStyle.TextStyles[I].StyleName) = LowerCase(Name) then
      begin
        Result := I;
        Exit;
      end;

    Result := -1;
  end;

  function FindNamedParaNo(const Name: string): Integer;
  var
    I: Integer;
  begin
    for I := 0 to RVStyle.ParaStyles.Count - 1 do
      if LowerCase(RVStyle.ParaStyles[I].StyleName) = LowerCase(Name) then
      begin
        Result := I;
        Exit;
      end;

    Result := -1;
  end;

  function FindNamedListNo(const Name: string): Integer;
  var
    I: Integer;
  begin
    for I := 0 to RVStyle.ListStyles.Count - 1 do
      if LowerCase(RVStyle.ListStyles[I].StyleName) = LowerCase(Name) then
      begin
        Result := I;
        Exit;
      end;

    Result := -1;
  end;

  function InsertItem(Text: TRVRawByteString; var Item: TCustomRVItemInfo): Boolean;
  begin
    Item.BeforeLoading(rvlfOther);
    if (RVData.Items.Count = 0) or (Position = 0) then
      Item.SameAsPrev := False
    else if RVData.GetItem(Position - 1).GetBoolValue(rvbpFullWidth) then
      Item.SameAsPrev := False
    else if Item.SameAsPrev then
      Item.ParaNo := RVData.GetItem(Position - 1).ParaNo;

    if (Item.ParaNo > RVStyle.ParaStyles.Count) or (Item.ParaNo < 0) then
      Item.ParaNo := Default^.ParaNo;

    Item.BR := Item.BR and not Item.GetBoolValue(rvbpFullWidth);

    Item.UpdatePaletteInfo(PaletteMode, False, RVPalette, RVLogPalette);
    if FirstTime then
    begin
      Result := RVData.InsertFirstRVFItem(Position, Text, Item, EditMode, FullReformat, NewListNo);
      if not Result then Exit;
      if Item<>nil then begin
        Item.AfterLoading(rvlfOther);
        Inc(Position);
        InsertPosition := Position-1;
        FirstTime := False;
      end;
    end else begin
      Item.Inserting(RVData, Text, True);
      RVData.Items.InsertObject(Position, Text, Item);
      Item.Inserted(RVData, Position);
      if not (RVData is TRVTableCellData) then
        Item.AfterLoading(rvlfOther);
      {$IFNDEF RVDONOTUSELISTS}
      RVData.AddMarkerInList(Position);
      {$ENDIF}
      {$IFNDEF RVDONOTUSESEQ}
      RVData.AddSeqInList(Position);
      {$ENDIF}
      Inc(Position);
      Inc(NonFirstItemsAdded);
      Result := True;
    end;
  end;

  procedure ReadText(T: TXMLTag);
  var
    I: Integer;
    S: TRVAnsiString;
    Str: TRVRawByteString;
    SVal : String;
    Item: TRVTextItemInfo;
  begin
    Item := TRVTextItemInfo.Create(RVData);
    Item.StyleNo := Default^.StyleNo;
    Item.ParaNo := Default^.ParaNo;
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      SVal := T.Attr[I].Value;
      {$IFDEF RVOLDTAGS}
      if (S = 'tag') and TagsArePChars then
        Item.Tag := Integer(StrNew(PChar(SVal)))
      else if (S = 'tagnumber') and (not TagsArePChars) then
        Item.Tag := StrToIntDef(SVal, Item.Tag)
      {$ELSE}
      if (S = 'tag') then
        Item.Tag := SVal
      else if (S = 'tagnumber') then begin
        if SVal='0' then
          Item.Tag := ''
        else
          Item.Tag := SVal;
        end
      {$ENDIF}
      else if S = 'textstyleno' then
        Item.StyleNo := StrToIntDef(SVal, Item.StyleNo)
      else if S = 'parastyleno' then
        Item.ParaNo := StrToIntDef(SVal, Item.ParaNo)
      else if S = 'textstyle' then
        Item.StyleNo := FindNamedStyleNo(SVal)
      else if S = 'parastyle' then
        Item.ParaNo := FindNamedParaNo(SVal)
      else if S = 'clearleft' then
        Item.ClearLeft := SVal<>'0'
      else if S = 'clearright' then
        Item.ClearRight := SVal<>'0'
      else if S = 'br' then
        Item.SameAsPrev := SVal='0'
      else if S = 'newpara' then
        Item.BR := SVal='0'
    end;

    if Item.StyleNo = -1 then
      Item.StyleNo := Default^.StyleNo;
    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;

    Item.StyleNo := StyleMap[Item.StyleNo];
    Item.ParaNo := ParaMap[Item.ParaNo];

    if RVData.GetRVStyle.TextStyles[Item.StyleNo].Unicode then begin
      // Defaul value of Unicode property for Delphi 2009+ is True
      Include(Item.ItemOptions, rvioUnicode);
      Str := T.GetRawUnicodeValueString;
      end
    else begin
      T.ValueCodePage := RVData.GetStyleCodePage(Item.StyleNo);
      Str := T.GetANSIValueString;
    end;
    InsertItem(Str, TCustomRVItemInfo(Item));
  end;

  procedure ReadUnicodeText(T: TXMLTag);
  var
    I: Integer;
    Item: TRVTextItemInfo;
    S: TRVAnsiString;
    SVal: String;
    Str: TRVRawByteString;
  begin
    Item := TRVTextItemInfo.Create(RVData);
    Item.StyleNo := Default^.StyleNo;
    Item.ParaNo := Default^.ParaNo;
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      SVal := T.Attr[I].Value;
      {$IFDEF RVOLDTAGS}
      if (S = 'tag') and TagsArePChars then
        Item.Tag := Integer(StrNew(PChar(SVal)))
      else if (S = 'tagnumber') and (not TagsArePChars) then
        Item.Tag := StrToIntDef(SVal, Item.Tag)
      {$ELSE}
      if (S = 'tag') then
        Item.Tag := SVal
      else if (S = 'tagnumber') then begin
        if SVal='0' then
          Item.Tag := ''
        else
          Item.Tag := SVal;
        end
      {$ENDIF}
      else if S = 'textstyleno' then
        Item.StyleNo := StrToIntDef(SVal, Item.StyleNo)
      else if S = 'parastyleno' then
        Item.ParaNo := StrToIntDef(SVal, Item.ParaNo)
      else if S = 'textstyle' then
        Item.StyleNo := FindNamedStyleNo(SVal)
      else if S = 'parastyle' then
        Item.ParaNo := FindNamedParaNo(SVal)
      else if S = 'clearleft' then
        Item.ClearLeft := SVal<>'0'
      else if S = 'clearright' then
        Item.ClearRight := SVal<>'0'        
      else if S = 'br' then
        Item.SameAsPrev := SVal='0'
      else if S = 'newpara' then
        Item.BR := SVal='0';
    end;
    //SetLength(Str, Length(T.UniValue) * 2);
    //Move(PWideChar(T.UniValue)^, PChar(Str)^, Length(Str));

    Include(Item.ItemOptions, rvioUnicode);

    if Item.StyleNo = -1 then
      Item.StyleNo := Default^.StyleNo;
    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;

    Item.StyleNo := StyleMap[Item.StyleNo];
    Item.ParaNo := ParaMap[Item.ParaNo];

    T.ValueCodePage := RVData.GetStyleCodePage(Item.StyleNo);
    Str := T.GetRawUnicodeValueString;

    InsertItem(Str, TCustomRVItemInfo(Item));
  end;

  procedure ReadCheck(T: TXMLTag);
  var
    I: Integer;
    Tag: TRVTag;
    Vis: Boolean;
    S: TRVAnsiString;
  begin
    Tag := RVEMPTYTAG;
    Vis := True;
    for I := 0 to T.Attr.Count - 1 do
    begin

      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      {$IFDEF RVOLDTAGS}
      if (S = 'tag') and TagsArePChars then
        Tag := Integer(StrNew(PChar(String(T.Attr[I].Value))))
      else if (S = 'tagnumber') and (not TagsArePChars) then
        Tag := StrToIntDef(T.Attr[I].Value, Tag)
      {$ELSE}
      if (S = 'tag') then
        Tag := T.Attr[I].Value
      else if (S = 'tagnumber') then begin
        Tag := T.Attr[I].Value;
        if Tag='0' then
          Tag := '';
        end
      {$ENDIF}
      else if S = 'vis' then
      begin
        if T.Attr[I].Value = '0' then
          Vis := False
        else if T.Attr[I].Value = '1' then
          Vis := True;
      end;

    end;
    RVData.SetCheckpointInfo(Position - 1, Tag, T.GetValueString, Vis);
  end;

  function ReadStandardItemProperties(const AttrName: TRVAnsiString; Attr: TXMLAttr;
    Item: TCustomRVItemInfo): Boolean;
  begin
    Result := True;
    {$IFDEF RVOLDTAGS}
    if (AttrName = 'tag') and TagsArePChars then
      Item.Tag := Integer(StrNew(PChar(String(Attr.Value))))
    else if (AttrName = 'tagnumber') and (not TagsArePChars) then
      Item.Tag := StrToIntDef(Attr.Value, Item.Tag)
    {$ELSE}
    if AttrName = 'tag' then
        Item.Tag := Attr.Value
    else if (AttrName = 'tagnumber') then begin
      if Attr.Value='0' then
        Item.Tag := ''
      else
        Item.Tag := Attr.Value;
      end
    {$ENDIF}
    else if AttrName = 'parastyleno' then
      Item.ParaNo := StrToIntDef(Attr.Value, Item.ParaNo)
    else if AttrName = 'parastyle' then
      Item.ParaNo := FindNamedParaNo(String(Attr.Value))
    {$IFNDEF RVDONOTUSEITEMHINTS}
    else if AttrName = 'hint' then
      Item.Hint := Attr.Value
    {$ENDIF}
    else if AttrName = 'clearleft' then
      Item.ClearLeft := Attr.Value <> '0'
    else if AttrName = 'clearright' then
      Item.ClearRight := Attr.Value <> '0'
    else if AttrName = 'br' then
      Item.SameAsPrev := Attr.Value = '0'
    else if AttrName = 'newpara' then
      Item.BR := Attr.Value = '0'
    else
      Result := False;
  end;

  function ReadNonTextItemProperties(const AttrName: TRVAnsiString; Attr: TXMLAttr;
    Item: TRVNonTextItemInfo): Boolean;
  begin
    Result := ReadStandardItemProperties(AttrName, Attr, Item);
    if not Result then
      if AttrName = 'deleteprotect' then
        begin
          if Attr.Value = '1' then
            Item.DeleteProtect := True
          else if Attr.Value = '0' then
            Item.DeleteProtect := False;
         end
      else
        if AttrName = 'hidden' then
          begin
            if Attr.Value = '1' then
              Item.Hidden := True
            else if Attr.Value = '0' then
              Item.Hidden := False;
         end;
  end;

  function GetVAlign(const Value: String): TRVVAlign;
  begin
    if Value = 'middle' then
      Result := rvvaMiddle
    else if Value = 'abstop' then
      Result := rvvaAbsTop
    else if Value = 'absbottom' then
      Result := rvvaAbsBottom
    else if Value = 'absmiddle' then
      Result := rvvaAbsMiddle
    else if Value = 'left' then
      Result := rvvaLeft
    else if Value = 'right' then
      Result := rvvaRight
    else
      Result := rvvaBaseLine
  end;

  function GetCellVAlign(const Value: String): TRVCellVAlign;
  begin
    if Value = 'top' then
      Result := rvcTop
    else if Value = 'middle' then
      Result := rvcMiddle
    else if Value = 'bottom' then
      Result := rvcBottom
    else //if Value = 'default' then
      Result := rvcVDefault;
  end;

  function GetCellRotation(const Value: String): TRVDocRotation;
  begin
    if Value = '0' then
      Result := rvrotNone
    else if Value = '90' then
      Result := rvrot90
    else if Value = '180' then
      Result := rvrot180
    else //if Value = '270' then
      Result := rvrot270;
  end;


  function ReadRectItemProperties(const AttrName: TRVAnsiString; Attr: TXMLAttr;
    Item: TRVRectItemInfo): Boolean;
  begin
    Result := ReadNonTextItemProperties(AttrName, Attr, Item);
    if Result then
      exit;
    Result := True;
    if AttrName = 'valign' then
      Item.VAlign := GetVAlign(LowerCase(Attr.Value))
    else if AttrName = 'vshift' then
      Item.VShift := StrToIntDef(Attr.Value, Item.VShift)
    else if AttrName = 'vshiftabs' then
      begin
        if Attr.Value = '1' then
          Item.VShiftAbs := True
        else if Attr.Value = '0' then
          Item.VShiftAbs := False;
      end
    else if AttrName = 'spacing' then
      Item.Spacing := GetLengthVal(Attr.Value, RVStyle, XMLUnits)
    else if AttrName = 'outervspacing' then
      Item.OuterVSpacing := GetLengthVal(Attr.Value, RVStyle, XMLUnits)
    else if AttrName = 'outerhspacing' then
      Item.OuterHSpacing := GetLengthVal(Attr.Value, RVStyle, XMLUnits)
    else if AttrName = 'borderwidth' then
      Item.BorderWidth := GetLengthVal(Attr.Value, RVStyle, XMLUnits)
    else if AttrName = 'bordercolor' then
      Item.BorderColor := StringToColor(Attr.Value)
    else if AttrName = 'backgroundcolor' then
      Item.BackgroundColor := StringToColor(Attr.Value)
    else
      Result := False;
  end;

  procedure DoReadImage(T: TXMLTag; ItemClass: TRVGraphicItemInfoClass;
    StyleNo: Integer);
  var
    I: Integer;
    S: TRVAnsiString;
    Stream: TRVMemoryStream;
    Item: TRVGraphicItemInfo;
    Transparent: Boolean;
    TransparentMode: TTransparentMode;
    TransparentColor: TColor;
  begin
    Transparent := False;
    TransparentMode := tmAuto;
    TransparentColor := 0;

    Item := ItemClass.Create(RVData);
    Item.StyleNo := StyleNo;
    Item.ParaNo := Default^.ParaNo;
    Item.VAlign := Default^.VAlign;
    Item.Image := nil;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    Stream := nil;

    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if ReadRectItemProperties(S, T.Attr[I], Item) then
        continue;

      if S = 'class' then
        Item.Image := RVGraphicHandler.CreateGraphic(RVGraphicHandler.GetGraphicClass(T.Attr[I].Value))

      else if S = 'img' then
      begin
        Stream := TRVMemoryStream.Create;
        RVFTextString2Stream(TRVAnsiString(T.Attr[I].Value), Stream);
      end

      else if S = 'imgsource' then
        Item.Image := RVGraphicHandler.LoadFromFile(T.Root.BaseFilePath + T.Attr[I].Value)

      else if S = 'width' then
        Item.ImageWidth := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'height' then
        Item.ImageHeight := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'resizable' then
        Item.SetExtraIntProperty(rvepResizable, StrToInt(T.Attr[I].Value))

      else if S = 'interval' then
        Item.Interval := StrToInt(T.Attr[I].Value)

      else if S = 'transparent' then
      begin
        if T.Attr[I].Value = '1' then
          Transparent := True
        else if T.Attr[I].Value = '0' then
          Transparent := False;
      end

      else if S = 'transparentmode' then
      begin
        if LowerCase(T.Attr[I].Value) = 'auto' then
          TransparentMode := tmAuto
        else if LowerCase(T.Attr[I].Value) = 'fixed' then
          TransparentMode := tmFixed;
      end

      else if S = 'transparentcolor' then
        TransparentColor := StringToColor(T.Attr[I].Value)

      else if S = 'alt' then
        Item.Alt := T.Attr[I].Value

      else if S = 'imagefilename' then
        Item.ImageFileName := T.Attr[I].Value

      else if S = 'minheighonpage' then
        Item.MinHeightOnPage := StrToIntDef(T.Attr[I].Value,0)

      else if S = 'htmlimagesize' then
      begin
        if T.Attr[I].Value = '1' then
          Item.NoHTMLImageSize := False
        else if T.Attr[I].Value = '0' then
          Item.NoHTMLImageSize := True;
      end

    end;

    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;

    Item.ParaNo := ParaMap[Item.ParaNo];

    if Item.VShiftAbs then
      Item.VShift := RVStyle.DifferentUnitsToUnits(Item.VShift, XMLUnits);

    if (Stream <> nil) and (Item.Image <> nil) then
    begin
      Stream.Position := 0;
      try
        Item.Image.LoadFromStream(Stream);
      finally
        Stream.Free;
      end;
      if Item.Image is TBitmap then
      begin
        TBitmap(Item.Image).Transparent := Transparent;
        TBitmap(Item.Image).TransparentMode := TransparentMode;
        TBitmap(Item.Image).TransparentColor := TransparentColor;
      end;
      InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
    end else if Item.Image<>nil then
    begin
      if Item.Image is TBitmap then
      begin
        TBitmap(Item.Image).Transparent := Transparent;
        TBitmap(Item.Image).TransparentMode := TransparentMode;
        TBitmap(Item.Image).TransparentColor := TransparentColor;
      end;
      InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
    end;
  end;

  procedure ReadImage(T: TXMLTag);
  begin
    DoReadImage(T, TRVGraphicItemInfo, rvsPicture);
  end;

  procedure ReadHotImage(T: TXMLTag);
  begin
    DoReadImage(T, TRVHotGraphicItemInfo, rvsHotPicture);
  end;

  function ReadLabelProperties(const AttrName: TRVAnsiString; Attr: TXMLAttr;
    Item: TRVLabelItemInfo; AllowChangingText: Boolean): Boolean;
  begin
    Result := ReadNonTextItemProperties(AttrName, Attr, Item);
    if Result then
      exit;
    Result := True;
    if AttrName = 'textstyle' then
      item.TextStyleNo := FindNamedStyleNo(Attr.Value)
    else if AttrName = 'textstyleno' then
      item.TextStyleNo := StyleMap[StrToIntDef(Attr.Value, Item.TextStyleNo)]
    else if AttrName = 'minwidth' then
      item.MinWidth := GetLengthVal(Attr.Value, RVStyle, XMLUnits)
    else if AttrName = 'removeinternalleading' then
      item.RemoveInternalLeading := Attr.Value<>'0'
    else if AttrName =  'alignment' then begin
      if Attr.Value='left' then
        item.Alignment := taLeftJustify
      else if Attr.Value='right' then
        item.Alignment := taRightJustify
      else if Attr.Value='center' then
        item.Alignment := taCenter;
      end
    else if AttrName='protectstyle' then begin
      if Attr.Value='1' then
        Item.ProtectTextStyleNo := True
      else if Attr.Value='0' then
        Item.ProtectTextStyleNo := False;
      end
    else if AttrName='text' then begin
      if AllowChangingText then
        Item.Text := Attr.Value
      end
    else
      Result := False;
  end;

  procedure DoReadLabel(T: TXMLTag; Item: TRVLabelItemInfo;
    AllowChangingText: Boolean);
  var
    I: Integer;
    S: TRVAnsiString;
  begin
    for I := 0 to T.Attr.Count - 1 do begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      ReadLabelProperties(S, T.Attr[I], Item, AllowChangingText);
    end;
    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;
    Item.ParaNo := ParaMap[Item.ParaNo];
  end;

  procedure ReadLabel(T: TXMLTag);
  var Item: TRVLabelItemInfo;
      I: Integer;
      S: TRVAnsiString;
  begin
    Item := TRVLabelItemInfo.Create(RVData);
    Item.StyleNo := rvsLabel;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadLabel(T, Item, True);
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      if S='text' then
        Item.Text := T.Attr[I].Value;
    end;
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  {$IFNDEF RVDONOTUSESEQ}
  function ReadSeqProperties(const AttrName: TRVAnsiString; Attr: TXMLAttr;
    Item: TRVSeqItemInfo;
    AllowChangingText, AllowChangingSeqName, AllowChangingNumberType: Boolean): Boolean;
  begin
    Result := ReadLabelProperties(AttrName, Attr, Item, AllowChangingText);
    if Result then
      exit;
    Result := True;
    if AttrName='seqname' then begin
      if AllowChangingSeqName then
        Item.SeqName := Attr.Value
      end
    else if AttrName='numbertype' then begin
      if AllowChangingNumberType then begin
        if Attr.Value='decimal' then
          Item.NumberType := rvseqDecimal
        else if Attr.Value='loweralpha' then
          Item.NumberType := rvseqLowerAlpha
        else if Attr.Value='upperalpha' then
          Item.NumberType := rvseqUpperAlpha
        else if Attr.Value='lowerroman' then
          Item.NumberType := rvseqLowerRoman
        else if Attr.Value='upperroman' then
          Item.NumberType := rvseqUpperRoman;
      end;
      end
    else if AttrName='reset' then begin
      if Attr.Value='1' then
        Item.Reset := True
      else if Attr.Value='0' then
        Item.Reset := False;
      end
    else if AttrName='startfrom' then
      Item.StartFrom := StrToIntDef(Attr.Value, Item.StartFrom)
    else if AttrName='format' then
      Item.FormatString := Attr.Value
    else
      Result := False;
  end;

  procedure DoReadSeq(T: TXMLTag; Item: TRVSeqItemInfo;
    AllowChangingText, AllowChangingSeqName, AllowChangingNumberType: Boolean);
  var
    I: Integer;
    S: TRVAnsiString;
  begin
    for I := 0 to T.Attr.Count - 1 do begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      ReadSeqProperties(S, T.Attr[I], Item,
        AllowChangingText, AllowChangingSeqName, AllowChangingNumberType);
    end;
    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;
    Item.ParaNo := ParaMap[Item.ParaNo];
  end;

  procedure ReadSeq(T: TXMLTag);
  var Item: TRVSeqItemInfo;
  begin
    Item := TRVSeqItemInfo.Create(RVData);
    Item.StyleNo := rvsSequence;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadSeq(T, Item, True, True, True);
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadNoteDocument(T: TXMLTag; Item: TCustomRVNoteItemInfo);
  var InsertPosition, NonFirstItemsAdded: Integer;
      FirstItem, FullReformat: Boolean;
  begin
    InsertPosition := 0;
    FirstItem := False;
    FullReformat := False;
    LoadRVDataFromXML(Item.Document, T, Bullets, Default, InsertPosition, PaletteMode, RVPalette,
      RVLogPalette, PrevCP, StyleMap, ParaMap, ListMap, OnUnknownTag, OnloadControl, OnAfterLoadControl,
      RVStyle, False, FirstItem, FullReformat, NonFirstItemsAdded, TagsArePChars, XMLUnits);
  end;

  procedure DoReadNote(T: TXMLTag; Item: TCustomRVNoteItemInfo);
  var I: Integer;
  begin
    DoReadSeq(T, Item, False, False, False);
    for I := 0 to T.Items.Count - 1 do
      if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Items[I].Name) = 'document' then begin
        ReadNoteDocument(T.Items[I], Item);
        exit;
      end;
  end;

  procedure ReadFootnote(T: TXMLTag);
  var Item: TRVFootnoteItemInfo;
  begin
    Item := TRVFootnoteItemInfo.Create(RVData);
    Item.StyleNo := rvsFootnote;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadNote(T, Item);
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadEndnote(T: TXMLTag);
  var Item: TRVEndnoteItemInfo;
  begin
    Item := TRVEndnoteItemInfo.Create(RVData);
    Item.StyleNo := rvsEndnote;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadNote(T, Item);
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadNoteRef(T: TXMLTag);
  var Item: TRVNoteReferenceItemInfo;
  begin
    Item := TRVNoteReferenceItemInfo.Create(RVData);
    Item.StyleNo := rvsNoteReference;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadLabel(T, Item, False);
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  function GetBoxWidthType(const s: String; DefValue: TRVBoxWidthType): TRVBoxWidthType;
  begin
    if s='relpage' then
      Result := rvbwtRelPage
    else if s='relmaintextarea' then
      Result := rvbwtRelMainTextArea
    else if s='relleftmargin' then
      Result := rvbwtRelLeftMargin
    else if s='relrightmargin' then
      Result := rvbwtRelRightMargin
    else if s='relinnermargin' then
      Result := rvbwtRelInnerMargin
    else if s='reloutermargin' then
      Result := rvbwtRelOuterMargin
    else if s='absolute' then
      Result := rvbwtAbsolute
    else
      Result := DefValue;
  end;

  function GetBoxHeightType(const s: String; DefValue: TRVBoxHeightType): TRVBoxHeightType;
  begin
   if s='absolute' then
     Result := rvbhtAbsolute
   else if s='relpage' then
     Result := rvbhtRelPage
   else if s='relmaintextarea' then
     Result := rvbhtRelMainTextArea
   else if s='reltopmargin' then
     Result := rvbhtRelTopMargin
   else if s='bottommargin' then
     Result := rvbhtRelBottomMargin
   else if s='auto' then
     Result := rvbhtAuto
   else
     Result := DefValue;
  end;

  function GetBoxVAlign(const s: String; DefValue: TTextLayout): TTextLayout;
  begin
    if s='center' then
      Result := tlCenter
    else if s='bottom' then
      Result := tlBottom
    else if s='top' then
      Result := tlTop
    else
      Result := DefValue;
  end;

  procedure DoReadBoxProperties(T: TXMLTag; BoxProperties: TRVBoxProperties);
  var
    I: Integer;
    AttrName: TRVAnsiString;
    Value: String;
  begin
    for I := 0 to T.Attr.Count - 1 do begin
      AttrName := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      Value := T.Attr[i].Value;
      if LoadBorderProperty(AttrName, Value, BoxProperties.Border, RVStyle, XMLUnits) or
        LoadBackgroundRectProperty(AttrName, Value, BoxProperties.Background, RVStyle, XMLUnits) then
        continue;
      if AttrName='widthtype' then
        BoxProperties.WidthType := GetBoxWidthType(Value, BoxProperties.WidthType)
      else if AttrName='heighttype' then
        BoxProperties.HeightType := GetBoxHeightType(Value, BoxProperties.HeightType)
      else if AttrName='width' then
        BoxProperties.Width := StrToIntDef(Value, BoxProperties.Width)
      else if AttrName='height' then
        BoxProperties.Height := StrToIntDef(Value, BoxProperties.Height)
      else if AttrName='valign' then
        BoxProperties.VAlign := GetBoxVAlign(Value, BoxProperties.VAlign);
    end;
    if BoxProperties.WidthType=rvbwtAbsolute then
      BoxProperties.Width := RVStyle.DifferentUnitsToUnits(BoxProperties.Width, XMLUnits);
    if BoxProperties.HeightType=rvbhtAbsolute then
      BoxProperties.Height := RVStyle.DifferentUnitsToUnits(BoxProperties.Height, XMLUnits);
  end;

  function GetBoxPositionKind(const S: String; DefValue: TRVFloatPositionKind): TRVFloatPositionKind;
  begin
    if s='alignment' then
      Result := rvfpkAlignment
    else if s='absolute' then
      Result := rvfpkAbsPosition
    else if s='relative' then
      Result := rvfpkPercentPosition
    else
      Result := DefValue;
  end;

  function GetBoxHorizontalAnchor(const S: String; DefValue: TRVHorizontalAnchor): TRVHorizontalAnchor;
  begin
    if s='page' then
      Result := rvhanPage
    else if s='maintextarea' then
      Result := rvhanMainTextArea
    else if s='leftmargin' then
      Result := rvhanLeftMargin
    else if s='rightmargin' then
      Result := rvhanRightMargin
    else if s='innermargin' then
      Result := rvhanInnerMargin
    else if s='outermargin' then
      Result := rvhanOuterMargin
    else if s='character' then
      Result := rvhanCharacter
    else
      Result := DefValue;
  end;

  function GetBoxVerticalAnchor(const S: String; DefValue: TRVVerticalAnchor): TRVVerticalAnchor;
  begin
    if s='page' then
      Result := rvvanPage
    else if s='maintextarea' then
      Result := rvvanMainTextArea
    else if s='topmargin' then
      Result := rvvanTopMargin
    else if s='bottommargin' then
      Result := rvvanBottomMargin
    else if s='line' then
      Result := rvvanLine
    else if s='paragraph' then
      Result := rvvanParagraph
    else
      Result := DefValue;
  end;

  function GetBoxHorizontalAlignment(const S: String; DefValue: TRVFloatHorizontalAlignment): TRVFloatHorizontalAlignment;
  begin
    if s='left' then
      Result := rvfphalLeft
    else if s='center' then
      Result := rvfphalCenter
    else if s='right' then
      Result := rvfphalRight
    else
      Result := DefValue;
  end;

  function GetBoxVerticalAlignment(const S: String; DefValue: TRVFloatVerticalAlignment): TRVFloatVerticalAlignment;
  begin
    if s='top' then
      Result := rvfpvalTop
    else if s='center' then
      Result := rvfpvalCenter
    else if s='bottom' then
      Result := rvfpvalBottom
    else
      Result := DefValue;
  end;


  procedure DoReadBoxPosition(T: TXMLTag; BoxPosition: TRVBoxPosition);
  var
    I: Integer;
    AttrName: TRVAnsiString;
    Value: String;
  begin
    for I := 0 to T.Attr.Count - 1 do begin
      AttrName := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
      Value := T.Attr[i].Value;
      if AttrName='hanchor' then
        BoxPosition.HorizontalAnchor := GetBoxHorizontalAnchor(Value, BoxPosition.HorizontalAnchor)
      else if AttrName='vanchor' then
        BoxPosition.VerticalAnchor := GetBoxVerticalAnchor(Value, BoxPosition.VerticalAnchor)
      else if AttrName='hpositionkind' then
        BoxPosition.HorizontalPositionKind := GetBoxPositionKind(Value, BoxPosition.HorizontalPositionKind)
      else if AttrName='vpositionkind' then
        BoxPosition.VerticalPositionKind := GetBoxPositionKind(Value, BoxPosition.VerticalPositionKind)
      else if AttrName='hoffset' then
        BoxPosition.HorizontalOffset := StrToIntDef(Value, BoxPosition.HorizontalOffset)
      else if AttrName='voffset' then
        BoxPosition.VerticalOffset := StrToIntDef(Value, BoxPosition.VerticalOffset)
      else if AttrName='halignment' then
        BoxPosition.HorizontalAlignment := GetBoxHorizontalAlignment(Value, BoxPosition.HorizontalAlignment)
      else if AttrName='valignment' then
        BoxPosition.VerticalAlignment := GetBoxVerticalAlignment(Value, BoxPosition.VerticalAlignment)
      else if AttrName='relativetocell' then begin
        if Value='1' then
          BoxPosition.RelativeToCell := True
        else if Value='0' then
          BoxPosition.RelativeToCell := False;
        end
      else if AttrName='positionintext' then begin
        if Value='above' then
          BoxPosition.PositionInText := rvpitAboveText
        else if Value='below' then
          BoxPosition.PositionInText := rvpitBelowText;
      end;
    end;
    if BoxPosition.HorizontalPositionKind=rvfpkAbsPosition then
      BoxPosition.HorizontalOffset := RVStyle.DifferentUnitsToUnits(BoxPosition.HorizontalOffset, XMLUnits);
    if BoxPosition.VerticalPositionKind=rvfpkAbsPosition then
      BoxPosition.VerticalOffset := RVStyle.DifferentUnitsToUnits(BoxPosition.VerticalOffset, XMLUnits);
  end;

  procedure DoReadSidenote(T: TXMLTag; Item: TRVSidenoteItemInfo);
  var i: Integer;
      AttrName, TagName: TRVAnsiString;
  begin
    if Item.GetBoolValue(rvbpPlaceholder) then
      // read text box attributes
      for I := 0 to T.Attr.Count - 1 do begin
        AttrName := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);
        ReadNonTextItemProperties(AttrName, T.Attr[I], Item);
      end
    else
      // read sidenote attributes
      DoReadSeq(T, Item, False, False, False);
    for I := 0 to T.Items.Count - 1 do begin
      TagName := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Items[I].Name);
      if TagName = 'document' then
        ReadNoteDocument(T.Items[I], Item)
      else if TagName='boxposition' then
        DoReadBoxPosition(T.Items[I], Item.BoxPosition)
      else if TagName='boxproperties' then
        DoReadBoxProperties(T.Items[I], Item.BoxProperties)
    end;
  end;

  procedure ReadSidenote(T: TXMLTag);
  var Item: TRVSidenoteItemInfo;
  begin
    Item := TRVSidenoteItemInfo.Create(RVData);
    Item.StyleNo := rvsSidenote;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadSidenote(T, Item);
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadTextBoxItem(T: TXMLTag);
  var Item: TRVTextBoxItemInfo;
  begin
    Item := TRVTextBoxItemInfo.Create(RVData);
    Item.StyleNo := rvsTextBox;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    DoReadSidenote(T, Item);
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  {$ENDIF}

  procedure ReadBreak(T: TXMLTag);
  var
    I: Integer;
    S: TRVAnsiString;
    Item: TRVBreakItemInfo;
  begin
    Item := TRVBreakItemInfo.Create(RVData);
    Item.StyleNo := rvsBreak;
    Item.LineWidth := Default^.BreakWidth;
    Item.Color := Default^.BreakColor;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);    
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if ReadNonTextItemProperties(S, T.Attr[I], Item) then
        continue;

      if S = 'width' then
        Item.LineWidth := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)
      else if S = 'color' then
        Item.Color := StringToColor(T.Attr[I].Value)
      else if S = 'breakstyle' then begin
        if T.Attr[I].Value='line' then
          Item.Style := rvbsLine
        else if T.Attr[I].Value='rectangle' then
          Item.Style := rvbsRectangle
        else if T.Attr[I].Value='3d' then
          Item.Style := rvbs3D
        else if T.Attr[I].Value='dotted' then
          Item.Style := rvbsDotted
        else if T.Attr[I].Value='dashed' then
          Item.Style := rvbsDashed;
      end;
    end;
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadBullet(T: TXMLTag);
  var
    I: Integer;
    S: TRVAnsiString;
    Item: TRVBulletItemInfo;
  begin
    Item := TRVBulletItemInfo.Create(RVData);
    Item.StyleNo := rvsBullet;
    Item.ParaNo := Default^.ParaNo;
    Item.ImageIndex := 0;
    Item.VAlign := Default^.VAlign;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);    
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if ReadRectItemProperties(S, T.Attr[I], Item) then
        continue;

      if S = 'alt' then
          Item.Alt := T.Attr[I].Value

      else if S = 'img' then
        Item.ImageList := Bullets.Items[StrToInt(T.Attr[I].Value)];

    end;

    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;

    Item.ParaNo := ParaMap[Item.ParaNo];

    if Item.VShiftAbs then
      Item.VShift := RVStyle.DifferentUnitsToUnits(Item.VShift, XMLUnits);

    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadHotSpot(T: TXMLTag);
  var
    ImgList: TImageList;
    Img, HotImg: TBitmap;
    Stream: TRVMemoryStream;
    S: TRVAnsiString;
    I: Integer;
    Item: TRVHotSpotItemInfo;
  begin
    Item := TRVHotSpotItemInfo.Create(RVData);
    Item.StyleNo := rvsHotSpot;
    Item.ParaNo := Default^.ParaNo;
    Item.VAlign := Default^.VAlign;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);    
    Stream := TRVMemoryStream.Create;
    Img := nil;
    HotImg := nil;
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if ReadRectItemProperties(S, T.Attr[I], Item) then
        continue;

      if S = 'alt' then
          Item.Alt := T.Attr[I].Value

      else if S = 'img' then
      begin
        Stream.Clear;
        RVFTextString2Stream(TRVAnsiString(T.Attr[I].Value), Stream);
        Stream.Position := 0;
        Img := TBitmap.Create;
        Img.LoadFromStream(Stream);
      end

      else if S = 'hotimg' then
      begin
        Stream.Clear;
        RVFTextString2Stream(TRVAnsiString(T.Attr[I].Value), Stream);
        Stream.Position := 0;
        HotImg := TBitmap.Create;
        HotImg.LoadFromStream(Stream);
      end

      else if S = 'imgsource' then
      begin
        Img := TBitmap.Create;
        Img.LoadFromFile(T.Root.BaseFilePath + T.Attr[I].Value);
      end

      else if S = 'hotimgsource' then
      begin
        HotImg := TBitmap.Create;
        HotImg.LoadFromFile(T.Root.BaseFilePath + T.Attr[I].Value);
      end

    end;

    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;

    Item.ParaNo := ParaMap[Item.ParaNo];

    if Item.VShiftAbs then
      Item.VShift := RVStyle.DifferentUnitsToUnits(Item.VShift, XMLUnits);

    if Img <> nil then
    begin
      ImgList := TImageList.CreateSize(Img.Width, Img.Height);
      ImgList.AddMasked(Img, Img.Canvas.Pixels[0, 0]);
      Item.ImageList := ImgList;
      Item.ImageIndex := 0;
      Item.HotImageIndex := 0;
      if HotImg <> nil then
      begin
        ImgList.AddMasked(HotImg, HotImg.Canvas.Pixels[0, 0]);
        Item.HotImageIndex := 1;
      end;
      Img.Free;
    end;
    HotImg.Free;
    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
    Stream.Free;
  end;

  procedure ReadControl(T: TXMLTag);
  var
    S: TRVAnsiString;
    Stream, OStream: TStringStream;
    I: Integer;
    Item: TRVControlItemInfo;
    CtrlClass: TControlClass;
    CtrlClassName: String;
    Ctrl: TControl;

  begin
    Item := TRVControlItemInfo.Create(RVData);
    Item.StyleNo := rvsComponent;
    Item.ParaNo := Default^.ParaNo;
    Item.VAlign := Default^.VAlign;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);    
    CtrlClass := nil;
    OStream := nil;
    CtrlClassName := '';
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if ReadRectItemProperties(S, T.Attr[I], Item) then
        continue;

      if S = 'resizable' then
        Item.SetExtraIntProperty(rvepResizable, StrToInt(T.Attr[I].Value))

      else if S = 'visible' then
        Item.SetExtraIntProperty(rvepVisible, StrToInt(T.Attr[I].Value))

      else if S = 'data' then
      begin
        if Assigned(OnLoadControl) then
        begin
          Ctrl := OnLoadControl(CtrlClassName, T.Attr[I].Value);
          if Ctrl<>nil then
          begin
            Item.Control.Free;
            Item.Control := Ctrl;
          end
        end;
        if Item.Control=nil then
        begin
          Stream := TStringStream.Create(T.Attr[I].Value);
          OStream := TStringStream.Create('');
          try
            try
              ObjectTextToBinary(Stream, OStream);
              OStream.Position := 0;
            except
              OStream.Free;
              OStream := nil;
            end;
          finally
            Stream.Free;
          end;
        end;
      end

      else if S = 'class' then begin
        CtrlClassName := T.Attr[I].Value;
        CtrlClass := TControlClass(FindClass(CtrlClassName))
      end;

    end;

    if (OStream <> nil) and (Item.Control = nil) and (CtrlClass <> nil) then
    begin
      Item.Control := CtrlClass.Create(nil);
      Item.Control.Parent := RVData.GetParentControl;
      try
        OStream.ReadComponent(Item.Control);
        if Assigned(OnAfterLoadControl) then
          OnAfterLoadControl(Item.Control);
      finally
        OStream.Free;
      end;
    end;

    if Item.Control <> nil then
    begin

      if Item.ParaNo = -1 then
        Item.ParaNo := Default^.ParaNo;

      Item.ParaNo := ParaMap[Item.ParaNo];

      if Item.VShiftAbs then
        Item.VShift := RVStyle.DifferentUnitsToUnits(Item.VShift, XMLUnits);      

      InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
    end;
  end;

  procedure ReadTable(T: TXMLTag);
  var
    Item: TRVTableItemInfo;
    I, J, X, Y, RowSpan, ColSpan, Rows, Cols: Integer;
    S: TRVAnsiString;
    S2: string;
    Cell: TRVTableCellData;
    A: TXMLAttr;
    gr: TGraphic;
    pic: TPicture;
    Stream: TRVMemoryStream;

    CellFirstItem, CellFullReformat: Boolean;
    CellInsertPosition: Integer;
    CellNonFirstItemsAdded: Integer;

    procedure SetTableOption(Attr: TXMLAttr; Option: TRVTableOption);
    begin
      if Attr.Value = '1' then
        Item.Options := Item.Options + [Option]
      else if Attr.Value = '0' then
        Item.Options := Item.Options - [Option];
    end;
    procedure SetTablePrintOption(Attr: TXMLAttr; Option: TRVTablePrintOption);
    begin
      if Attr.Value = '1' then
        Item.PrintOptions := Item.PrintOptions + [Option]
      else if Attr.Value = '0' then
        Item.PrintOptions := Item.PrintOptions - [Option];
    end;

  begin


    A := T.Attr.FindAttrOfName('rows');
    if A = nil then Exit;
    Rows := StrToIntDef(A.Value, 0);
    if Rows = 0 then Exit;

    A := T.Attr.FindAttrOfName('cols');
    if A = nil then Exit;
    Cols := StrToIntDef(A.Value, 0);
    if Cols = 0 then Exit;

    gr := nil;

    Item := TRVTableItemInfo.CreateEx(Rows, Cols, RVData);
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if ReadNonTextItemProperties(S, T.Attr[I], Item) then
        continue;

      if S = 'width' then
      begin
        Item.BestWidth := StrToIntDef(T.Attr[I].Value, Item.BestWidth);
        if Item.BestWidth>0 then
          Item.BestWidth := RVStyle.DifferentUnitsToUnits(Item.BestWidth, XMLUnits);
      end

      else if S = 'bordercolor' then Item.BorderColor := StringToColor(T.Attr[I].Value)

      else if S = 'borderlightcolor' then Item.BorderLightColor := StringToColor(T.Attr[I].Value)

      else if S = 'borderstyle' then
      begin
        S2 := LowerCase(T.Attr[I].Value);
        if S2 = 'raised' then
          Item.BorderStyle := rvtbRaised
        else if S2 = 'lowered' then
          Item.BorderStyle := rvtbLowered
        else if S2 = 'color' then
          Item.BorderStyle := rvtbColor
        else if S2 = 'raisedcolor' then
          Item.BorderStyle := rvtbRaisedColor
        else if S2 = 'loweredcolor' then
          Item.BorderStyle := rvtbLoweredColor;
      end

      else if S = 'borderhspacing' then
        Item.BorderHSpacing := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'bordervspacing' then
        Item.BorderVSpacing := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'borderwidth' then
        Item.BorderWidth := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'cellbordercolor' then
        Item.CellBorderColor := StringToColor(T.Attr[I].Value)

      else if S = 'cellborderlightcolor' then
        Item.CellBorderLightColor := StringToColor(T.Attr[I].Value)

      else if S = 'cellborderstyle' then
      begin
        S2 := LowerCase(T.Attr[I].Value);
        if S2 = 'raised' then
          Item.CellBorderStyle := rvtbRaised
        else if S2 = 'lowered' then
          Item.CellBorderStyle := rvtbLowered
        else if S2 = 'color' then
          Item.CellBorderStyle := rvtbColor
        else if S2 = 'raisedcolor' then
          Item.CellBorderStyle := rvtbRaisedColor
        else if S2 = 'loweredcolor' then
          Item.CellBorderStyle := rvtbLoweredColor;
      end

      else if S = 'cellborderwidth' then
        Item.CellBorderWidth := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'cellhspacing' then
        Item.CellHSpacing := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'cellpadding' then
        Item.CellPadding := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)
      else if S = 'cellhpadding' then
        Item.CellHPadding := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)
      else if S = 'cellvpadding' then
        Item.CellVPadding := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'cellvspacing' then
        Item.CellVSpacing := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'color' then Item.Color := StringToColor(T.Attr[I].Value)

      else if S = 'houtermostrule' then
      begin
        if T.Attr[I].Value = '1' then
          Item.HOutermostRule := True
        else if T.Attr[I].Value = '0' then
          Item.HOutermostRule := False;
      end

      else if S = 'hrulecolor' then
        Item.HRuleColor := StringToColor(T.Attr[I].Value)

      else if S = 'hrulewidth' then
        Item.HRuleWidth := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'editing' then
        SetTableOption(T.Attr[I], rvtoEditing)

      else if S = 'rowsizing' then
        SetTableOption(T.Attr[I], rvtoRowSizing)

      else if S = 'colsizing' then
        SetTableOption(T.Attr[I], rvtoColSizing)

      else if S = 'rowselect' then
        SetTableOption(T.Attr[I], rvtoRowSelect)

      else if S = 'colselect' then
        SetTableOption(T.Attr[I], rvtoColSelect)

      else if S = 'cellselect' then
      begin
        if T.Attr[I].Value = '1' then
          Item.Options := Item.Options - [rvtoNoCellSelect]
        else if T.Attr[I].Value = '0' then
          Item.Options := Item.Options + [rvtoNoCellSelect];
      end

      else if S = 'rtfsavecellwidth' then
        SetTableOption(T.Attr[I], rvtoRTFSaveCellPixelBestWidth)

      else if S = 'rtfallowautofit' then
        SetTableOption(T.Attr[I], rvtoRTFAllowAutofit)

      else if S = 'hidegridlines' then
        SetTableOption(T.Attr[I], rvtoHideGridLines)

      else if S = 'cellbelowborder' then
        SetTableOption(T.Attr[I], rvtoCellBelowBorders)

      else if S = 'overlappingcorners' then
        SetTableOption(T.Attr[I], rvtoOverlappingCorners)

      else if S = 'ignorecontentwidth' then
        SetTableOption(T.Attr[I], rvtoIgnoreContentWidth)

      else if S = 'ignorecontentheight' then
        SetTableOption(T.Attr[I], rvtoIgnoreContentHeight)

      else if S = 'printhalftoneborders' then
        SetTablePrintOption(T.Attr[I], rvtoHalftoneBorders)

      else if S = 'printsplitrows' then
        SetTablePrintOption(T.Attr[I], rvtoRowsSplit)

      else if S = 'printwhiteback' then
        SetTablePrintOption(T.Attr[I], rvtoWhiteBackground)

      else if S = 'textsavecolsep' then
        Item.TextColSeparator := T.Attr[I].Value

      else if S = 'textsaverowsep' then
        Item.TextRowSeparator := T.Attr[I].Value

      else if S = 'voutermostrule' then
      begin
        if T.Attr[I].Value = '1' then
          Item.VOutermostRule := True
        else if T.Attr[I].Value = '0' then
          Item.VOutermostRule := False;
      end

      else if S = 'vrulecolor' then
        Item.VRuleColor := StringToColor(T.Attr[I].Value)

      else if S = 'vrulewidth' then
        Item.VRuleWidth := GetLengthVal(T.Attr[I].Value, RVStyle, XMLUnits)

      else if S = 'headerrowcount' then
        Item.HeadingRowCount := StrToIntDef(T.Attr[I].Value, Item.HeadingRowCount)

      else if S = 'leftbordervisible' then
      begin
        if T.Attr[I].Value = '1' then
          Item.VisibleBorders.Left := True
        else if T.Attr[I].Value = '0' then
          Item.VisibleBorders.Left := False;
      end

      else if S = 'rightbordervisible' then
      begin
        if T.Attr[I].Value = '1' then
          Item.VisibleBorders.Right := True
        else if T.Attr[I].Value = '0' then
          Item.VisibleBorders.Right := False;
      end

      else if S = 'topbordervisible' then
      begin
        if T.Attr[I].Value = '1' then
          Item.VisibleBorders.Top := True
        else if T.Attr[I].Value = '0' then
          Item.VisibleBorders.Top := False;
      end

      else if S = 'bottombordervisible' then
      begin
        if T.Attr[I].Value = '1' then
          Item.VisibleBorders.Bottom := True
        else if T.Attr[I].Value = '0' then
          Item.VisibleBorders.Bottom := False;
      end

      else if S = 'imagefilename' then
        Item.BackgroundImageFileName := T.Attr[I].Value

      else if s = 'backstyle' then
      begin
        if T.Attr[I].Value = 'color' then
          Item.BackgroundStyle := rvbsColor
        else if T.Attr[I].Value = 'stretch' then
          Item.BackgroundStyle := rvbsStretched
        else if T.Attr[I].Value = 'tile' then
          Item.BackgroundStyle := rvbsTiled
        else if T.Attr[I].Value = 'center' then
          Item.BackgroundStyle := rvbsCentered;
      end

      else if S = 'backimgclass' then
      begin
        gr.Free;
        gr := RVGraphicHandler.CreateGraphic(RVGraphicHandler.GetGraphicClass(T.Attr[I].Value));
      end

      else if S = 'backimg' then
      begin
        if gr<>nil then begin
          Stream := TRVMemoryStream.Create;
          try
            RVFTextString2Stream(TRVAnsiString(T.Attr[I].Value), Stream);
            Stream.Position := 0;
            gr.LoadFromStream(Stream);
            Item.BackgroundImage := gr;
          finally
            gr.Free;
            gr := nil;          
            Stream.Free;
          end;
        end;
      end

      else if S = 'backimgsource' then
      begin
        Pic := TPicture.Create;
        try
          Pic.LoadFromFile(T.Root.BaseFilePath + T.Attr[I].Value);
          Item.BackgroundImage := Pic.Graphic;
        finally
          Pic.Free;
        end;
      end

    end;

    gr.Free;
    gr := nil;

    for X := 0 to T.Items.Count - 1 do
    begin
      if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Items[X].Name) = 'row' then
      begin
        for J := 0 to T.Items[X].Attr.Count - 1 do
        begin
          A := T.Items[X].Attr[J];
          if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(A.Name) = 'valign' then
            Item.Rows[X].VAlign := GetCellVAlign(LowerCase(A.Value))
          else if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(A.Name) = 'pagebreakbefore' then
              Item.Rows[X].PageBreakBefore := A.Value <> '0'
        end;

        
        for Y := 0 to T.Items[X].Items.Count - 1 do
        begin
          if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Items[X].Items[Y].Name) = 'cell' then
          begin
            if Item.Rows[X].Items[Y] <> nil then
            begin
              Cell := Item.Rows[X].Items[Y];
              RowSpan := 1;
              ColSpan := 1;
              for I := 0 to T.Items[X].Items[Y].Attr.Count - 1 do
              begin
                A := T.Items[X].Items[Y].Attr[I];
                S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(A.Name);

                if S = 'height' then
                  Cell.BestHeight := GetLengthVal(A.Value, RVStyle, XMLUnits)

                else if S = 'width' then begin
                  Cell.BestWidth := StrToInt(A.Value);
                  if Cell.BestWidth>0 then
                    Cell.BestWidth := RVStyle.DifferentUnitsToUnits(Cell.BestWidth, XMLUnits);
                  end

                else if S = 'color' then
                  Cell.Color := StringToColor(A.Value)

                else if S = 'bordercolor' then
                  Cell.BorderColor := StringToColor(A.Value)

                else if S = 'borderlightcolor' then
                  Cell.BorderLightColor := StringToColor(A.Value)

                else if S = 'colspan' then
                  ColSpan := StrToIntDef(A.Value, ColSpan)

                else if S = 'rowspan' then
                  RowSpan := StrToIntDef(A.Value, RowSpan)

                {$IFNDEF RVDONOTUSEITEMHINTS}
                else if S = 'hint' then
                  Cell.Hint := A.Value
                {$ENDIF}

                else if S = 'valign' then
                  Cell.VAlign := GetCellVAlign(LowerCase(A.Value))

                else if S = 'rotation' then
                  Cell.Rotation := GetCellRotation(LowerCase(A.Value))

                else if S = 'imagefilename' then
                  Cell.BackgroundImageFileName := A.Value

                else if S = 'backimgclass' then
                begin
                  gr.Free;
                  gr := RVGraphicHandler.CreateGraphic(RVGraphicHandler.GetGraphicClass(A.Value));
                end

                else if s = 'backstyle' then
                begin
                  if A.Value = 'color' then
                    Cell.BackgroundStyle := rvbsColor
                  else if A.Value = 'stretch' then
                    Cell.BackgroundStyle := rvbsStretched
                  else if A.Value = 'tile' then
                    Cell.BackgroundStyle := rvbsTiled
                  else if A.Value = 'center' then
                    Cell.BackgroundStyle := rvbsCentered;
                end

                else if S = 'backimg' then
                begin
                  if gr<>nil then begin
                    Stream := TRVMemoryStream.Create;
                    try
                      RVFTextString2Stream(TRVAnsiString(A.Value), Stream);
                      Stream.Position := 0;
                      gr.LoadFromStream(Stream);
                      Cell.BackgroundImage := gr;
                    finally
                      gr.Free;
                      gr := nil;                    
                      Stream.Free;
                    end;
                  end;
                end

                else if S = 'backimgsource' then
                begin
                  Pic := TPicture.Create;
                  try
                    Pic.LoadFromFile(T.Root.BaseFilePath + A.Value);
                    Cell.BackgroundImage := Pic.Graphic;
                  finally
                    Pic.Free;
                  end;
                end

                else if S = 'leftbordervisible' then
                begin
                  if A.Value = '1' then
                    Cell.VisibleBorders.Left := True
                  else if A.Value = '0' then
                    Cell.VisibleBorders.Left := False;
                end

                else if S = 'rightbordervisible' then
                begin
                  if A.Value = '1' then
                    Cell.VisibleBorders.Right := True
                  else if A.Value = '0' then
                    Cell.VisibleBorders.Right := False;
                end

                else if S = 'topbordervisible' then
                begin
                  if A.Value = '1' then
                    Cell.VisibleBorders.Top := True
                  else if A.Value = '0' then
                    Cell.VisibleBorders.Top := False;
                end

                else if S = 'bottombordervisible' then
                begin
                  if A.Value = '1' then
                    Cell.VisibleBorders.Bottom := True
                  else if A.Value = '0' then
                    Cell.VisibleBorders.Bottom := False;
                end

              end;
              gr.Free;
              gr := nil;
              if (RowSpan > 1) or (ColSpan > 1) then
                Item.MergeCells(X, Y, ColSpan, RowSpan, True);
              Item.Cells[X, Y].GetRVData.Clear;
              CellFirstItem := False;
              CellFullReformat := False;
              CellInsertPosition := 0;
              Result := LoadRVDataFromXML(Cell.GetRVData, T.Items[X].Items[Y], Bullets, Default, CellInsertPosition, PaletteMode, RVPalette,
                RVLogPalette, PrevCP, StyleMap, ParaMap, ListMap, OnUnknownTag, OnloadControl, OnAfterLoadControl, RVStyle, False,
                CellFirstItem, CellFullReformat,CellNonFirstItemsAdded, TagsArePChars, XMLUnits);
//              if not Result then Exit;
            end;
          end;
        end;
      end;
    end;

    if Item.ParaNo = -1 then
      Item.ParaNo := Default^.ParaNo;

    Item.ParaNo := ParaMap[Item.ParaNo];

    InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadMarker(T: TXMLTag);
  var
    item: TRVMarkerItemInfo;
    I: Integer;
    S: TRVAnsiString;
  begin
    item := TRVMarkerItemInfo.CreateEx(RVData, -1, 0, 1, False);
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);    

    for I := 0 to T.Attr.Count - 1 do
      with T.Attr[I] do
      begin
        S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Name);

        if ReadNonTextItemProperties(S, T.Attr[I], Item) then
          continue;

        if S = 'htmlimagesize' then
        begin
          if T.Attr[I].Value = '1' then
            Item.NoHTMLImageSize := False
          else if T.Attr[I].Value = '0' then
            Item.NoHTMLImageSize := True;
        end

        else if S = 'style' then
          item.ListNo := FindNamedListNo(Value)

        else if S = 'styleno' then
          item.ListNo := StrToIntDef(Value, item.ListNo)

        else if S = 'level' then
          item.Level := StrToIntDef(Value, item.Level)

        else if S = 'start' then
          item.StartFrom := StrToIntDef(Value, item.StartFrom)

        else if S = 'usestart' then
        begin
          if Value = '1' then
            item.Reset := True
          else if Value = '0' then
            item.Reset := False;
        end;
      end;

      if Item.ParaNo = -1 then
        Item.ParaNo := Default^.ParaNo;

      Item.ParaNo := ParaMap[Item.ParaNo];
      if Item.ListNo>=0 then
        Item.ListNo := ListMap[Item.ListNo];

      InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;

  procedure ReadTab(T: TXMLTag);
  var
    item: TRVTabItemInfo;
    I: Integer;
    S: TRVAnsiString;
  begin
    item := TRVTabItemInfo.Create (RVData);
    item.StyleNo := rvsTab;
    if RVStyle.Units=rvstuTwips then
      Item.ConvertToDifferentUnits(RVStyle.Units, nil, False);    

    for I := 0 to T.Attr.Count - 1 do
      with T.Attr[I] do
      begin
        S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Name);

        if ReadNonTextItemProperties(S, T.Attr[I], Item) then
          continue;

        if S = 'textstyle' then
          item.TextStyleNo := FindNamedStyleNo(Value)

        else if S =  'textstyleno' then
          item.TextStyleNo := StyleMap[StrToIntDef(Value, item.TextStyleNo)];
      end;

      if Item.ParaNo = -1 then
        Item.ParaNo := Default^.ParaNo;

      Item.ParaNo := ParaMap[Item.ParaNo];

      InsertItem(T.GetANSIValueString, TCustomRVItemInfo(Item));
  end;


  procedure ReadDefault(T: TXMLTag);
  var
    I: Integer;
    S: TRVAnsiString;
  begin
    for I := 0 to T.Attr.Count - 1 do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[I].Name);

      if S = 'textstyleno' then
      begin
        Default^.StyleNo := StrToIntDef(T.Attr[I].Value, Default^.StyleNo);
      end

      else if S = 'parastyleno' then
      begin
        Default^.ParaNo := StrToIntDef(T.Attr[I].Value, Default^.ParaNo);
      end

      else if S = 'textstyle' then Default^.StyleNo := FindNamedStyleNo(T.Attr[I].Value)

      else if S = 'parastyle' then Default^.ParaNo := FindNamedParaNo(T.Attr[I].Value)

      else if S = 'valign' then Default^.VAlign := GetVAlign(LowerCase(T.Attr[I].Value))

      else if S = 'breakwidth' then Default^.BreakWidth := StrToIntDef(T.Attr[I].Value, Default^.BreakWidth)

      else if S = 'breakcolor' then Default^.BreakColor := StringToColor(T.Attr[I].Value);

    end;

    if Default^.StyleNo = -2 then
      Default^.StyleNo := 0;

    if Default^.ParaNo = -2 then
      Default^.ParaNo := 0;
  end;

var
  I: Integer;
  S: TRVAnsiString;
  T: TXMLTag;
  PageBreak: Boolean;
  DefaultCreated: Boolean;
  OldItemCount: Integer;
begin
  NonFirstItemsAdded := 0;
  Position := InsertPosition;
  Result := False;
  PrevCP := nil;
  DefaultCreated := Default = nil;
  if DefaultCreated then
  begin
    New(Default);
    Default^.StyleNo := 0;
    Default^.ParaNo := 0;
    Default^.VAlign := rvvaBaseLine;
    Default^.BreakColor := clNone;
    Default^.BreakWidth := 1;
    T := Node.Items.FindTagOfName('default');
    if T <> nil then
      ReadDefault(T);
  end;
  PageBreak := False;
  for I := 0 to Node.Items.Count - 1 do
  begin
    S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name);
    if S = 'text' then ReadText(Node.Items[I])
    else if S = 'utext' then ReadUnicodeText(Node.Items[I])
    else if S = 'check' then ReadCheck(Node.Items[I])
    else if S = 'image' then ReadImage(Node.Items[I])
    else if S = 'hotimage' then ReadHotImage(Node.Items[I])
    else if S = 'break' then ReadBreak(Node.Items[I])
    else if S = 'bullet' then ReadBullet(Node.Items[I])
    else if S = 'hotspot' then ReadHotSpot(Node.Items[I])
    else if S = 'control' then ReadControl(Node.Items[I])
    else if S = 'table' then ReadTable(Node.Items[I])
    else if S = 'marker' then ReadMarker(Node.Items[I])
    else if S = 'tab' then ReadTab(Node.Items[I])
    else if S = 'label' then ReadLabel(Node.Items[I])
    {$IFNDEF RVDONOTUSESEQ}
    else if S = 'seq' then ReadSeq(Node.Items[I])
    else if S = 'footnote' then ReadFootnote(Node.Items[I])
    else if S = 'endnote' then ReadEndnote(Node.Items[I])
    else if S = 'noteref' then ReadNoteRef(Node.Items[I])
    else if S = 'sidenote' then ReadSidenote(Node.Items[I])
    else if S = 'textbox' then ReadTextBoxItem(Node.Items[I])
    {$ENDIF}
    else if S = 'default' then ReadDefault(Node.Items[I])
    else if S = 'pagebreak' then begin
      PageBreak := True;
      Continue;
    end else if S = 'linebreak' then begin
      ;
    end else begin
      OldItemCount := RVData.ItemCount;
      if Assigned(OnUnknownTag) then
        OnUnknownTag(Node.Items[I], RVData, Position);
      if OldItemCount<>RVData.ItemCount then
        Inc(Position);
    end;
    if PageBreak then
      RVData.GetItem(Position-1).PageBreakBefore := True;
    PageBreak := False;
  end;
  if DefaultCreated then
    Dispose(Default);
end;

procedure SetTagValue(Tag: TXMLTag; const Value: String);
begin
  {$IFDEF RVUNICODESTR}
  Tag.Value := RVU_GetRawUnicode(Value);
  Tag.UnicodeMode := xmleRawUnicode;
  {$ELSE}
  Tag.Value := Value;
  {$ENDIF}
end;

procedure SaveRVRect(Node: TXMLTag; R: TRVRect; Prop: TRVParaInfoProperty;
  const FormatStr: TRVAnsiString; Props: TRVParaInfoProperties;
  DefH, DefV: Integer);
begin
  if (R.Left <> DefH) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['left']), IntToStr(R.Left));
  inc(Prop);
  if (R.Top <> DefV) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['top']), IntToStr(R.Top));
  inc(Prop);
  if (R.Right <> DefH) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['right']), IntToStr(R.Right));
  inc(Prop);
  if (R.Bottom <> DefV) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['bottom']), IntToStr(R.Bottom));
end;

procedure SaveRVBoolRect(Node: TXMLTag; R: TRVBooleanRect; Prop: TRVParaInfoProperty;
  const FormatStr: TRVAnsiString; Props: TRVParaInfoProperties);
begin
  if (not R.Left) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['left']), '0');
  inc(Prop);
  if (not R.Top) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['top']), '0');
  inc(Prop);
  if (not R.Right) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['right']), '0');
  inc(Prop);
  if (not R.Bottom) and (Prop in Props)  then
    Node.Attr.AddAttr({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      FormatStr, ['bottom']), '0');
end;

procedure SaveBorderToXML(Node: TXMLTag; Border: TRVBorder;
  Props: TRVParaInfoProperties; DefH, DefV: Integer; DefStyle: TRVBorderStyle);

  function GetBorderStyleStr: String;
  begin
    case Border.Style of
      rvbSingle: Result := 'single';
      rvbDouble: Result := 'double';
      rvbTriple: Result := 'triple';
      rvbThickInside: Result := 'thickinside';
      rvbThickOutside: Result := 'thickoutside';
      else Result := '';
    end;
  end;

begin
  SaveRVRect(Node, Border.BorderOffsets,rvpiBorder_BO_Left, 'border%soffset',
    Props, DefH, DefV);
  if (Border.Color <> clWindowText) and (rvpiBorder_Color in Props) then
    Node.Attr.AddAttr('bordercolor', ColorToString(Border.Color));
  if (Border.InternalWidth <> 1) and (rvpiBorder_InternalWidth in Props) then
    Node.Attr.AddAttr('borderintwidth', IntToStr(Border.InternalWidth));
  if (Border.Style <> DefStyle) and (rvpiBorder_Style in Props) then
    Node.Attr.AddAttr('borderstyle', GetBorderStyleStr);
  SaveRVBoolRect(Node, Border.VisibleBorders, rvpiBorder_Vis_Left, 'border%svisible',
    Props);
  if (Border.Width <> 1) and (rvpiBorder_Width in Props) then
    Node.Attr.AddAttr('borderwidth', IntToStr(Border.Width));
end;

procedure SaveBackgroundRectToXML(Node: TXMLTag; Background: TRVBackgroundRect;
  Props: TRVParaInfoProperties; DefH, DefV: Integer; DefColor: TColor);
begin
  SaveRVRect(Node, Background.BorderOffsets,rvpiBackground_BO_Left, 'back%soffset',
    Props, DefH, DefV);
  if (Background.Color <> DefColor) and (rvpiBackground_Color in Props) then
    Node.Attr.AddAttr('backcolor', ColorToString(Background.Color));
end;

{------------------------------SaveRVDataToXML------------------------------}

procedure SaveRVDataToXML(RVData: TCustomRVData; Node: TXMLTag;
  Default: TRichViewXMLStyle; SaveStyle: Boolean; RVStyle: TRVStyle;
  SaveImgToFiles: Boolean; Bullets: TImgListList;
  SelStart, SelOffset, SelEnd, SelEndOffset: Integer;
  TagsArePChars: Boolean; OnUnknownItem: TRVXMLUnknownItemEvent;
  OnSaveControl: TRVXMLSaveControlEvent;
  UseStyleNames, SaveLineBreaks: Boolean);

  function GetTagStr(const Tag: TRVTag): string;
  begin
    {$IFDEF RVOLDTAGS}
    if Tag = 0 then
      Result := ''
    else
      Result := PChar(Tag);
    {$ELSE}
    Result := Tag;
    {$ENDIF}
  end;

  procedure WriteCheck(T: TXMLTag; ItemNo: Integer);
  var
    T2: TXMLTag;
    Tag, Name: string;
    Cp: TCheckpointData;
    IntTag: TRVTag;
    Vis: Boolean;
  begin
    Cp := RVData.GetItemCheckpoint(ItemNo);
    if Cp <> nil then
    begin
      T2 := T.Items.AddTag('check', T, T.Root);
      T2.TagCodePage := RVData.GetRVStyle.DefCodePage;
      T2.ValueCodePage := T2.TagCodePage;
      RVData.GetCheckpointInfo(Cp, IntTag, Name, Vis);
      {$IFDEF RVOLDTAGS}
      if TagsArePChars then
      {$ENDIF}
      begin
        Tag := GetTagStr(IntTag);
        if Tag <> '' then
          T2.Attr.AddAttr('tag', Tag);
      end
      {$IFDEF RVOLDTAGS}
      else
      begin
        if IntTag <> 0 then
          T2.Attr.AddAttr('tagnumber', IntToStr(IntTag));
      end
      {$ENDIF};

      if Vis = False then
        T2.Attr.AddAttr('vis', '0');
      SetTagValue(T2, Name);
    end;
  end;

  procedure WriteItem(T: TXMLTag; Item: TCustomRVItemInfo; ItemNo: Integer);
  var StrValue: String;
  begin
    WriteCheck(T.Parent, ItemNo);
    {$IFDEF RVOLDTAGS}
    if TagsArePChars then
    {$ENDIF}
    begin
      if GetTagStr(Item.Tag) <> '' then
        T.Attr.AddAttr('tag', GetTagStr(Item.Tag));
    end
    {$IFDEF RVOLDTAGS}
    else
    begin
      if Item.Tag <> 0 then
        T.Attr.AddAttr('tagnumber', IntToStr(Item.Tag));
    end
    {$ENDIF};

    {$IFNDEF RVDONOTUSEITEMHINTS}
    if Item.GetExtraStrProperty(rvespHint, StrValue) and (StrValue<>'') then
      T.Attr.AddAttr('hint', StrValue);
    {$ENDIF}
    if Item.SameAsPrev then
    begin
      T.Attr.AddAttr('br', '0')
    end else if Item.ParaNo <> Default.ParaNo then
    begin
      if UseStyleNames then
        T.Attr.AddAttr('parastyle', RVStyle.ParaStyles[Item.ParaNo].StyleName)
      else
        T.Attr.AddAttr('parastyleno', IntToStr(Item.ParaNo));
    end;
    if Item.BR and (not Item.SameAsPrev) then
      T.Attr.AddAttr('newpara', '0');
    if Item.ClearLeft then
      T.Attr.AddAttr('clearleft', '1');
    if Item.ClearRight then
      T.Attr.AddAttr('clearright', '1');
  end;

  procedure WriteText(T: TXMLTag; Item: TRVTextItemInfo; ItemNo, StartOffs, EndOffs: Integer);
  var s: TRVRawByteString;
      DItemNo, DOffs, Offs1: Integer;
      FRVData: TCustomRVFormattedData;
      ItemFromNewLine: Boolean;
  begin
    if UseStyleNames then
    begin
      if Item.StyleNo <> Default.StyleNo then
        T.Attr.AddAttr('textstyle', RVStyle.TextStyles[Item.StyleNo].StyleName);
    end else begin
      if Item.StyleNo <> Default.StyleNo then
        T.Attr.AddAttr('textstyleno', IntToStr(Item.StyleNo));
    end;
    WriteItem(T, Item, ItemNo);

    s := RVData.Items[ItemNo];
    if (StartOffs>1) or (EndOffs<RVU_Length(s, Item.ItemOptions)+1) then
      s := RVU_Copy(s, StartOffs, EndOffs-StartOffs, Item.ItemOptions);
    //if rvioUnicode in Item.ItemOptions then
    //  T.UniValue := RVU_RawUnicodeToWideString(s);
    T.Value := s;

    if SaveLineBreaks then begin
      FRVData := TCustomRVFormattedData(RVData);
      FRVData.Item2DrawItem(ItemNo, StartOffs, DItemNo, DOffs);
      ItemFromNewLine := RVData.IsFromNewLine(ItemNo);
      while (DItemNo<FRVData.DrawItems.Count) and
            (FRVData.DrawItems[DItemNo].ItemNo=ItemNo) and
            (FRVData.DrawItems[DItemNo].Offs<EndOffs) do begin
        Offs1 := FRVData.DrawItems[DItemNo].Offs;
        if (Offs1>=StartOffs) and
           ((Offs1>1) or (not ItemFromNewLine and FRVData.DrawItems[DItemNo].FromNewLine)) then
          T.Parent.Items.InsertTag(
            T.Parent.Items.Count-1, 'linebreak', T.Parent, T.Parent.Root).Attr.AddAttr('position', IntToStr(Offs1));
        inc(DItemNo);
      end;
    end;

  end;

  procedure WriteNonTextItem(T: TXMLTag; Item: TCustomRVItemInfo; ItemNo: Integer);
  var IntValue: Integer;
  begin
    WriteItem(T, Item, ItemNo);
    if Item.GetExtraIntProperty(rvepDeleteProtect, IntValue) and (IntValue<>0) then
      T.Attr.AddAttr('deleteprotect', '1');
    if Item.GetExtraIntProperty(rvepHidden, IntValue) and (IntValue<>0) then
      T.Attr.AddAttr('hidden', '1');
  end;

  function GetVAlignStr(VAlign: TRVVAlign): String;
  begin
    case VAlign of
      rvvaBaseLine:
        Result := 'baseline';
      rvvaMiddle:
        Result := 'middle';
      rvvaAbsTop:
        Result := 'abstop';
      rvvaAbsBottom:
        Result := 'absbottom';
      rvvaAbsMiddle:
        Result := 'absmiddle';
      rvvaLeft:
        Result := 'left';
      rvvaRight:
        Result := 'right';
      else
        Result := '?';
    end;
  end;

  function GetCellVAlignStr(VAlign: TRVCellVAlign): String;
  begin
    case VAlign of
      rvcMiddle:
        Result := 'middle';
      rvcBottom:
        Result := 'bottom';
      rvcTop:
        Result := 'top';
      else
        Result := 'default';
    end;
  end;

  function GetCellRotationStr(Rotation: TRVDocRotation): String;
  begin
    case Rotation of
      rvrot90:
        Result := '90';
      rvrot180:
        Result := '180';
      rvrot270:
        Result := '270';
      else
        Result := '0';
    end;
  end;

  procedure WriteRectItem(T: TXMLTag; Item: TRVRectItemInfo; ItemNo: Integer);
  begin
    WriteNonTextItem(T, Item, ItemNo);

    if Item.VAlign <> Default.VAlign then
      T.Attr.AddAttr('valign', GetVAlignStr(Item.VAlign));

    if Item.VShift <> 0 then
      T.Attr.AddAttr('vshift', IntToStr(Item.VShift));

    if Item.VShiftAbs then
      T.Attr.AddAttr('vshiftabs', '1');

    T.Attr.AddAttr('spacing', IntToStr(Item.Spacing));

    if Item.OuterVSpacing>0 then
      T.Attr.AddAttr('outervspacing', IntToStr(Item.OuterVSpacing));
    if Item.OuterHSpacing>0 then
      T.Attr.AddAttr('outerhspacing', IntToStr(Item.OuterHSpacing));

    if Item.BorderWidth>0 then
      T.Attr.AddAttr('borderwidth', IntToStr(Item.BorderWidth));

    if Item.BorderColor<>clBlack then
      T.Attr.AddAttr('bordercolor', ColorToString(Item.BorderColor));

    if Item.BackgroundColor<>clNone then
      T.Attr.AddAttr('backgroundcolor', ColorToString(Item.BackgroundColor));
  end;

  procedure WriteLabelProps(T: TXMLTag; Item: TRVLabelItemInfo; ItemNo: Integer);
  var
    LValue: String;
  begin
    WriteRectItem(T, Item, ItemNo);
    with T.Attr do
    begin
      if UseStyleNames then
        AddAttr('textstyle', RVStyle.TextStyles[Item.TextStyleNo].StyleName)
      else
        AddAttr('textstyleno', IntToStr(Item.TextStyleNo));
      if item.MinWidth>0 then
        AddAttr('minwidth', IntToStr(Item.MinWidth));
      if item.RemoveInternalLeading then
        AddAttr('removeinternalleading', '1');
      case Item.Alignment of
        taRightJustify:
          LValue := 'right';
        taCenter:
          LValue := 'center';
        else
          LValue := '';
      end;
      if LValue<>'' then
        AddAttr('alignment', LValue);
      if Item.ProtectTextStyleNo then
        AddAttr('protectstyle', '1');
    end;
    T.Value := RVData.Items[ItemNo];    
  end;

  procedure WriteLabel(T: TXMLTag; Item: TRVLabelItemInfo; ItemNo: Integer);
  begin
    WriteLabelProps(T, Item, ItemNo);
    T.Attr.AddAttr('text', Item.Text);

  end;

  {$IFNDEF RVDONOTUSESEQ}
  procedure WriteSeq(T: TXMLTag; Item: TRVSeqItemInfo; ItemNo: Integer);
  var LValue: String;
  begin
    WriteLabelProps(T, Item, ItemNo);
    with T.Attr do
    begin
      AddAttr('seqname', Item.SeqName);
      case Item.NumberType of
        rvseqLowerAlpha:
          LValue := 'loweralpha';
        rvseqUpperAlpha:
          LValue := 'upperalpha';
        rvseqLowerRoman:
          LValue := 'lowerroman';
        rvseqUpperRoman:
          LValue := 'upperroman';
        else
          LValue := '';
      end;
      if LValue<>'' then
        AddAttr('numbertype', LValue);
      if Item.Reset then begin
        AddAttr('reset', '1');
        AddAttr('startfrom', IntToStr(Item.StartFrom));
      end;
      if Item.FormatString<>'' then
        AddAttr('format', Item.FormatString);
    end;
  end;

  procedure WriteNoteProps(T: TXMLTag; Item: TCustomRVNoteItemInfo; ItemNo: Integer);
  begin
    WriteLabelProps(T, Item, ItemNo);
    T.Items.AddTag( 'document', T, T.Root);
    SaveRVDataToXML(Item.Document, T.Items[0], Default, False, RVStyle,
      SaveImgToFiles, Bullets, 0, -1, Item.Document.ItemCount-1, -1,
      TagsArePChars, OnUnknownItem, OnSaveControl, UseStyleNames, SaveLineBreaks);
  end;

  procedure WriteFootnote(T: TXMLTag; Item: TRVFootnoteItemInfo; ItemNo: Integer);
  begin
    WriteNoteProps(T, Item, ItemNo);
  end;

  procedure WriteEndnote(T: TXMLTag; Item: TRVEndnoteItemInfo; ItemNo: Integer);
  begin
    WriteNoteProps(T, Item, ItemNo);
  end;

  procedure WriteNoteRef(T: TXMLTag; Item: TRVNoteReferenceItemInfo; ItemNo: Integer);
  begin
    WriteLabelProps(T, Item, ItemNo);
  end;

  procedure WriteBoxProperties(T: TXMLTag; Item: TRVSidenoteItemInfo);
  var T2: TXMLTag;
      LValue: String;

      function GetPositionKindStr(Kind: TRVFloatPositionKind): String;
      begin
        case Kind of
          rvfpkAlignment:   Result := 'alignment';
          rvfpkAbsPosition: Result := 'absolute';
          else {rvfpkPercentPosition:} Result := 'relative';
        end;
      end;

  begin
    // saving box position
    T2 := T.Items.AddTag('boxposition', T, T.Root);
    case Item.BoxPosition.HorizontalAnchor of
      rvhanPage:         LValue := 'page';
      rvhanMainTextArea: LValue := 'maintextarea';
      rvhanLeftMargin:   LValue := 'leftmargin';
      rvhanRightMargin:  LValue := 'rightmargin';
      rvhanInnerMargin:  LValue := 'innermargin';
      rvhanOuterMargin:  LValue := 'outermargin';
      else               LValue := 'character';
    end;
    T2.Attr.AddAttr('hanchor', LValue);
    case Item.BoxPosition.VerticalAnchor of
      rvvanParagraph:    LValue := 'paragraph';
      rvvanPage:         LValue := 'page';
      rvvanMainTextArea: LValue := 'maintextarea';
      rvvanTopMargin:    LValue := 'topmargin';
      rvvanBottomMargin: LValue := 'bottommargin';
      else               LValue := 'line';
    end;
    T2.Attr.AddAttr('vanchor', LValue);
    T2.Attr.AddAttr('hpositionkind', GetPositionKindStr(Item.BoxPosition.HorizontalPositionKind));
    T2.Attr.AddAttr('vpositionkind', GetPositionKindStr(Item.BoxPosition.VerticalPositionKind));
    T2.Attr.AddAttr('hoffset', IntToStr(Item.BoxPosition.HorizontalOffset));
    T2.Attr.AddAttr('voffset', IntToStr(Item.BoxPosition.VerticalOffset));
    case Item.BoxPosition.HorizontalAlignment of
      rvfphalLeft: LValue := 'left';
      rvfphalCenter: LValue := 'center';
      else {rvfphalRight:} LValue := 'right';
    end;
    T2.Attr.AddAttr('halignment', LValue);
    case Item.BoxPosition.VerticalAlignment of
      rvfpvalTop:    LValue := 'top';
      rvfpvalCenter: LValue := 'center';
      else {rvfpvalBottom:} LValue := 'bottom';
    end;
    T2.Attr.AddAttr('valignment', LValue);
    T2.Attr.AddAttr('relativetocell', IntToStr(ord(Item.BoxPosition.RelativeToCell)));
    case Item.BoxPosition.PositionInText of
      rvpitAboveText: LValue := 'above';
      else {rvpitBelowText:} LValue := 'below';
    end;
    T2.Attr.AddAttr('positionintext', LValue);
    // saving box properties
    T2 := T.Items.AddTag('boxproperties', T, T.Root);
    case Item.BoxProperties.WidthType of
      rvbwtRelPage:         LValue := 'relpage';
      rvbwtRelMainTextArea: LValue := 'relmaintextarea';
      rvbwtRelLeftMargin:   LValue := 'relleftmargin';
      rvbwtRelRightMargin:  LValue := 'relrightmargin';
      rvbwtRelInnerMargin:  LValue := 'relinnermargin';
      rvbwtRelOuterMargin:  LValue := 'reloutermargin';
      else {rvbwtAbsolute:} LValue := 'absolute';
    end;
    T2.Attr.AddAttr('widthtype', LValue);
    case Item.BoxProperties.HeightType of
      rvbhtAbsolute:        LValue := 'absolute';
      rvbhtRelPage:         LValue := 'relpage';
      rvbhtRelMainTextArea: LValue := 'relmaintextarea';
      rvbhtRelTopMargin:    LValue := 'reltopmargin';
      rvbhtRelBottomMargin: LValue := 'bottommargin';
      else {rvbhtAuto: ;}   LValue := 'auto';
    end;
    T2.Attr.AddAttr('heighttype', LValue);
    T2.Attr.AddAttr('width', IntToStr(Item.BoxProperties.Width));
    T2.Attr.AddAttr('height', IntToStr(Item.BoxProperties.Height));
    if Item.BoxProperties.VAlign<>tlTop then begin
      case Item.BoxProperties.VAlign of
        tlCenter: LValue := 'center';
        tlBottom: LValue := 'bottom';
        else  LValue := '';
      end;
      T2.Attr.AddAttr('valign', LValue);
    end;
    SaveBorderToXML(T2, Item.BoxProperties.Border, RVAllParaBorderProperties, 10, 5, rvbSingle);
    SaveBackgroundRectToXML(T2, Item.BoxProperties.Background, RVAllParaBackgroundProperties, 10, 5, clWindow);
  end;

  procedure WriteSidenote(T: TXMLTag; Item: TRVSidenoteItemInfo; ItemNo: Integer);
  begin
    WriteNoteProps(T, Item, ItemNo);
    WriteBoxProperties(T, Item);
  end;

  procedure WriteTextBox(T: TXMLTag; Item: TRVSidenoteItemInfo; ItemNo: Integer);
  begin
    WriteNonTextItem(T, Item, ItemNo);
    T.Items.AddTag( 'document', T, T.Root);
    SaveRVDataToXML(Item.Document, T.Items[0], Default, False, RVStyle,
      SaveImgToFiles, Bullets, 0, -1, Item.Document.ItemCount-1, -1,
      TagsArePChars, OnUnknownItem, OnSaveControl, UseStyleNames, SaveLineBreaks);
    WriteBoxProperties(T, Item);
  end;
  {$ENDIF}


  procedure WriteBreak(T: TXMLTag; Item: TRVBreakItemInfo; ItemNo: Integer);
  var BreakStyle: String;
  begin
    WriteNonTextItem(T, Item, ItemNo);

    if Item.LineWidth <> Default.BreakWidth then
      T.Attr.AddAttr('width', IntToStr(Item.LineWidth));

    if Item.Color <> Default.BreakColor then
      T.Attr.AddAttr('color', ColorToString(Item.Color));

    case Item.Style of
      rvbsRectangle:
        BreakStyle := 'rectangle';
      rvbs3d:
        BreakStyle := '3d';
      rvbsDotted:
        BreakStyle := 'dotted';
      rvbsDashed:
        BreakStyle := 'dashed';
      else
        BreakStyle := '';
    end;
    if BreakStyle<>'' then
      T.Attr.AddAttr('breakstyle', BreakStyle);
  end;

  procedure WriteImage(T: TXMLTag; Item: TRVGraphicItemInfo; ItemNo: Integer);
  var
    FileName: string;
    Stream: TRVMemoryStream;
    IntValue: Integer;
  begin
    WriteRectItem(T, Item, ItemNo);

    if Item.ImageWidth <> 0 then
      T.Attr.AddAttr('width', IntToStr(Item.ImageWidth));

    if Item.ImageHeight <> 0 then
      T.Attr.AddAttr('height', IntToStr(Item.ImageHeight));

    if Item.MinHeightOnPage <> 0 then
      T.Attr.AddAttr('minheighonpage', IntToStr(Item.MinHeightOnPage));

    if Item.NoHTMLImageSize then
      T.Attr.AddAttr('htmlimagesize', '0');

    if Item.Alt<>'' then
      T.Attr.AddAttr('alt', Item.Alt);

    if Item.ImageFileName<>'' then
      T.Attr.AddAttr('imagefilename', Item.ImageFileName);

    if Item.GetExtraIntProperty(rvepResizable, IntValue) and (IntValue<>1) then
      T.Attr.AddAttr('resizable', '0');

    if Item.Interval>0 then
      T.Attr.AddAttr('interval', IntToStr(Item.Interval));

    if Item.Image is TBitmap then
      with TBitmap(Item.Image) do
        if Transparent then
        begin
          T.Attr.AddAttr('transparent', '1');
          if TransparentMode = tmFixed then
          begin
            T.Attr.AddAttr('transparentmode', 'fixed');
            if TransparentColor <> 0 then
              T.Attr.AddAttr('transparentcolor', ColorToString(TransparentColor));
          end;
        end;

    if SaveImgToFiles then
    begin

      if RVData.Items[ItemNo] = '' then
        FileName := GetImageFileName(T.Root.BaseFilePath, 'Image',
          GraphicExtension(TGraphicClass(Item.Image.ClassType)))
      else
        FileName := GetImageFileName(T.Root.BaseFilePath, RVData.GetItemText(ItemNo),
          GraphicExtension(TGraphicClass(Item.Image.ClassType)));

      Item.Image.SaveToFile(FileName);
      T.Attr.AddAttr('imgsource', ExtractFileName(FileName));
    end else begin
      T.Attr.AddAttr('class', Item.Image.ClassName);

      Stream := TRVMemoryStream.Create;
      try
        Item.Image.SaveToStream(Stream);
        Stream.Position := 0;
        T.Attr.AddAttr('img', String(RVFStream2TextString(Stream)));
      finally
        Stream.Free;
      end;
    end;

    T.Value := RVData.Items[ItemNo];
  end;

  procedure WriteBullet(T: TXMLTag; Item: TRVBulletItemInfo; ItemNo: Integer);
  begin
    WriteRectItem(T, Item, ItemNo);

    if Item.NoHTMLImageSize then
      T.Attr.AddAttr('htmlimagesize', '0');

    if Item.Alt<>'' then
      T.Attr.AddAttr('alt', Item.Alt);

    T.Attr.AddAttr('img', IntToStr(Bullets.IndexOf(Item.ImageList)));

    T.Value := RVData.Items[ItemNo];
  end;


  procedure WriteHotSpot(T: TXMLTag; Item: TRVHotSpotItemInfo; ItemNo: Integer);
  var
    Stream: TRVMemoryStream;
    Bmp: TBitmap;
    FileName: string;
  begin
    WriteRectItem(T, Item, ItemNo);

    if Item.NoHTMLImageSize then
      T.Attr.AddAttr('htmlimagesize', '0');

    if Item.Alt<>'' then
      T.Attr.AddAttr('alt', Item.Alt);         

    Bmp := TBitmap.Create;
    Item.ImageList.GetBitmap(Item.ImageIndex, Bmp);
    if SaveImgToFiles then
    begin

      if RVData.Items[ItemNo] = '' then
        FileName := GetImageFileName(T.Root.BaseFilePath, 'HotSpot', 'bmp')
      else
        FileName := GetImageFileName(T.Root.BaseFilePath, RVData.GetItemText(ItemNo), 'bmp');

      Bmp.SaveToFile(FileName);
      T.Attr.AddAttr('imgsource', ExtractFileName(FileName));

    end else begin

      Stream := TRVMemoryStream.Create;
      try
        Bmp.SaveToStream(Stream);
        Stream.Position := 0;
        T.Attr.AddAttr('img', String(RVFStream2TextString(Stream)));
      finally
        Stream.Free;
      end;

    end;

    if Item.ImageIndex <> Item.HotImageIndex then
    begin
      Bmp := TBitmap.Create;
      Item.ImageList.GetBitmap(Item.HotImageIndex, Bmp);
      if SaveImgToFiles then
      begin

        if RVData.Items[ItemNo] = '' then
          FileName := GetImageFileName(T.Root.BaseFilePath, 'HotSpot', 'bmp')
        else
          FileName := GetImageFileName(T.Root.BaseFilePath, RVData.GetItemText(ItemNo), 'bmp');

        Bmp.SaveToFile(FileName);
        T.Attr.AddAttr('hotimgsource', ExtractFileName(FileName));

      end else begin

        Stream := TRVMemoryStream.Create;
        try
          Bmp.SaveToStream(Stream);
          Stream.Position := 0;
          T.Attr.AddAttr('hotimg', String(RVFStream2TextString(Stream)));
        finally
          Stream.Free;
        end;

      end;
    end;

    T.Value := RVData.Items[ItemNo];
  end;

  procedure WriteControl(T: TXMLTag; Item: TRVControlItemInfo; ItemNo: Integer);
  var
    Stream: TRVMemoryStream;
    OStream: TStringStream;
    IntValue: Integer;
  begin
    WriteRectItem(T, Item, ItemNo);

    if Item.GetExtraIntProperty(rvepResizable, IntValue) and (IntValue<>0) then
      T.Attr.AddAttr('resizable', '1');

    if Item.GetExtraIntProperty(rvepVisible, IntValue) and (IntValue<>1) then
      T.Attr.AddAttr('visible', '0');

    if Assigned(OnSaveControl) then
      T.Attr.AddAttr('data', OnSaveControl(Item.Control))
    else begin
      T.Attr.AddAttr('class', Item.Control.ClassName);
      Stream := TRVMemoryStream.Create;
      try
        Stream.WriteComponent(Item.Control);
        Stream.Position := 0;
        OStream := TStringStream.Create('');
        try
          ObjectBinaryToText(Stream, OStream);
          OStream.Position := 0;
//          T.Attr.AddAttr('data', StringReplace(OStream.DataString, crlf, ' ', [rfReplaceAll]));
          T.Attr.AddAttr('data', OStream.DataString);
        finally
          OStream.Free;
        end;
      finally
        Stream.Free;
      end;
    end;

    T.Value := RVData.Items[ItemNo];
  end;

  procedure WriteTable(T: TXMLTag; Item: TRVTableItemInfo; ItemNo: Integer);
  var
    X, Y: Integer;
    FileName: String;
    Stream: TRVMemoryStream;
    Cell: TRVTableCellData;
    CellTag: TXMLTag;
  begin
    WriteNonTextItem(T, Item, ItemNo);

    T.Attr.AddAttr('rows', IntToStr(Item.Rows.Count));
    T.Attr.AddAttr('cols', IntToStr(Item.Rows[0].Count));

    if Item.BestWidth <> 0 then
      T.Attr.AddAttr('width', IntToStr(Item.BestWidth));

    if Item.BorderColor <> clWindowText then
      T.Attr.AddAttr('bordercolor', ColorToString(Item.BorderColor));

    if Item.BorderLightColor <> clBtnHighlight then
      T.Attr.AddAttr('borderlightcolor', ColorToString(Item.BorderLightColor));

    case Item.BorderStyle of
      rvtbLowered:
        T.Attr.AddAttr('borderstyle', 'lowered');
      rvtbColor:
        T.Attr.AddAttr('borderstyle', 'color');
      rvtbRaisedColor:
        T.Attr.AddAttr('borderstyle', 'raisedcolor');
      rvtbLoweredColor:
        T.Attr.AddAttr('borderstyle', 'loweredcolor');
    end;

    if Item.BorderHSpacing <> 2 then
      T.Attr.AddAttr('borderhspacing', IntToStr(Item.BorderHSpacing));    

    if Item.BorderVSpacing <> 2 then
      T.Attr.AddAttr('bordervspacing', IntToStr(Item.BorderVSpacing));

    if Item.BorderWidth <> 0 then
      T.Attr.AddAttr('borderwidth', IntToStr(Item.BorderWidth));

    if Item.CellBorderColor <> clWindowText then
      T.Attr.AddAttr('cellbordercolor', ColorToString(Item.CellBorderColor));

    if Item.CellBorderLightColor <> clBtnHighlight then
      T.Attr.AddAttr('cellborderlightcolor', ColorToString(Item.CellBorderLightColor));

    case Item.CellBorderStyle of
      rvtbRaised:
        T.Attr.AddAttr('cellborderstyle', 'raised');
      rvtbColor:
          T.Attr.AddAttr('cellborderstyle', 'color');
      rvtbRaisedColor:
        T.Attr.AddAttr('cellborderstyle', 'raisedcolor');
      rvtbLoweredColor:
        T.Attr.AddAttr('cellborderstyle', 'loweredcolor');
    end;

    if Item.CellBorderWidth <> 0 then
      T.Attr.AddAttr('cellborderwidth', IntToStr(Item.CellBorderWidth));

    if Item.CellHSpacing <> 2 then
      T.Attr.AddAttr('cellhspacing', IntToStr(Item.CellHSpacing));

    if Item.CellHPadding <> 1 then
      T.Attr.AddAttr('cellhpadding', IntToStr(Item.CellHPadding));
    if Item.CellHPadding <> 1 then
      T.Attr.AddAttr('cellvpadding', IntToStr(Item.CellVPadding));

    if Item.CellVSpacing <> 2 then
      T.Attr.AddAttr('cellvspacing', IntToStr(Item.CellVSpacing));

    if Item.Color <> clWindow then
      T.Attr.AddAttr('color', ColorToString(Item.Color));

    if Item.HOutermostRule then
      T.Attr.AddAttr('houtermostrule', '1');

    if Item.HRuleColor <> clWindowText then
      T.Attr.AddAttr('hrulecolor', ColorToString(Item.HRuleColor));

    if Item.HRuleWidth <> 0 then
      T.Attr.AddAttr('hrulewidth', IntToStr(Item.HRuleWidth));

    if not (rvtoEditing in Item.Options) then
      T.Attr.AddAttr('editing', '0');

    if not (rvtoRowSizing in Item.Options) then
      T.Attr.AddAttr('rowsizing', '0');

    if not (rvtoColSizing in Item.Options) then
      T.Attr.AddAttr('colsizing', '0');

    if not (rvtoRowSelect in Item.Options) then
      T.Attr.AddAttr('rowselect', '0');

    if not (rvtoColSelect in Item.Options) then
      T.Attr.AddAttr('colselect', '0');

    if (rvtoNoCellSelect in Item.Options) then
      T.Attr.AddAttr('cellselect', '0');

    if rvtoRTFSaveCellPixelBestWidth in Item.Options then
      T.Attr.AddAttr('rtfsavecellwidth', '1');

    if rvtoRTFAllowAutoFit in Item.Options then
      T.Attr.AddAttr('rtfallowautofit', '1');

    if rvtoHideGridLines in Item.Options then
      T.Attr.AddAttr('hidegridlines', '1');

    if rvtoCellBelowBorders in Item.Options then
      T.Attr.AddAttr('cellbelowborder', '1');

    if rvtoOverlappingCorners in Item.Options then
      T.Attr.AddAttr('overlappingcorners', '1');

    if rvtoIgnoreContentWidth in Item.Options then
      T.Attr.AddAttr('ignorecontentwidth', '1');

    if rvtoIgnoreContentHeight in Item.Options then
      T.Attr.AddAttr('ignorecontentheight', '1');

    if not (rvtoHalftoneBorders in Item.PrintOptions) then
      T.Attr.AddAttr('printhalftoneborders', '0');

    if not (rvtoRowsSplit in Item.PrintOptions) then
      T.Attr.AddAttr('printsplitrows', '0');

    if rvtoWhiteBackground in Item.PrintOptions then
      T.Attr.AddAttr('printwhiteback', '1');

    if Item.TextColSeparator <> #13#10 then
      T.Attr.AddAttr('textsavecolsep', String(Item.TextColSeparator));

    if Item.TextRowSeparator <> #13#10 then
      T.Attr.AddAttr('textsaverowsep',String(Item.TextRowSeparator));

    if Item.VOutermostRule then
      T.Attr.AddAttr('voutermostrule', '1');

    if Item.VRuleColor <> clWindowText then
      T.Attr.AddAttr('vrulecolor', ColorToString(Item.VRuleColor));

    if Item.VRuleWidth <> 0 then
      T.Attr.AddAttr('vrulewidth', IntToStr(Item.VRuleWidth));

    if Item.HeadingRowCount <> 0 then
      T.Attr.AddAttr('headerrowcount', IntToStr(Item.HeadingRowCount));

    if Item.BackgroundImageFileName<>'' then
      T.Attr.AddAttr('imagefilename', String(Item.BackgroundImageFileName));

    case Item.BackgroundStyle of
      rvbsStretched:
        T.Attr.AddAttr('backstyle', 'stretch');
      rvbsTiled:
        T.Attr.AddAttr('backstyle', 'tile');
      rvbsCentered:
        T.Attr.AddAttr('backstyle', 'center');
    end;

    if (Item.BackgroundImage<>nil) and not Item.BackgroundImage.Empty then begin
      if SaveImgToFiles then
      begin

        if RVData.Items[ItemNo] = '' then
          FileName := GetImageFileName(T.Root.BaseFilePath, 'bkgnd',
            GraphicExtension(TGraphicClass(Item.BackgroundImage.ClassType)))
        else
          FileName := GetImageFileName(T.Root.BaseFilePath, RVData.GetItemText(ItemNo),
            GraphicExtension(TGraphicClass(Item.BackgroundImage.ClassType)));

        Item.BackgroundImage.SaveToFile(FileName);
        T.Attr.AddAttr('backimgsource', ExtractFileName(FileName));
      end else begin
        T.Attr.AddAttr('backimgclass', Item.BackgroundImage.ClassName);

        Stream := TRVMemoryStream.Create;
        try
          Item.BackgroundImage.SaveToStream(Stream);
          Stream.Position := 0;
          T.Attr.AddAttr('backimg', String(RVFStream2TextString(Stream)));
        finally
          Stream.Free;
        end;
      end;
    end;

    for X := 0 to Item.Rows.Count - 1 do
    begin
      T.Items.AddTag('row', T, T.Root);
      T.Items[X].Attr.AddAttr('valign', GetCellVAlignStr(Item.Rows[X].VAlign));
      if Item.Rows[X].PageBreakBefore then
        T.Items[X].Attr.AddAttr('pagebreakbefore', '1');

      for Y := 0 to Item.Rows[0].Count - 1 do
      begin
        T.Items[X].Items.AddTag('cell', T.Items[X], T.Items[X].Root);
        Cell := Item.Cells[X, Y];
        if Cell <> nil then
        begin
          CellTag := T.Items[X].Items[Y];
          if Cell.RowSpan > 1 then
            CellTag.Attr.AddAttr('rowspan', IntToStr(Cell.RowSpan));

          if Cell.ColSpan > 1 then
            CellTag.Attr.AddAttr('colspan', IntToStr(Cell.ColSpan));

          if Cell.BestHeight <> 0 then
            CellTag.Attr.AddAttr('height', IntToStr(Cell.BestHeight));

          if Cell.BestWidth <> 0 then
            CellTag.Attr.AddAttr('width', IntToStr(Cell.BestWidth));

          if Cell.Color <> clNone then
            CellTag.Attr.AddAttr('color', ColorToString(Cell.Color));

          if Cell.BorderColor <> clNone then
            CellTag.Attr.AddAttr('bordercolor', ColorToString(Cell.BorderColor));

          if Cell.BorderLightColor <> clNone then
            CellTag.Attr.AddAttr('borderlightcolor', ColorToString(Cell.BorderLightColor));

          {$IFNDEF RVDONOTUSEITEMHINTS}
          if Cell.Hint<>''then
            CellTag.Attr.AddAttr('hint', Cell.Hint);
          {$ENDIF}

          if Cell.VAlign<>rvcVDefault then
            CellTag.Attr.AddAttr('valign', GetCellVAlignStr(Cell.VAlign));

          if Cell.Rotation<>rvrotNone then
            CellTag.Attr.AddAttr('rotation', GetCellRotationStr(Cell.Rotation));

          case Cell.BackgroundStyle of
            rvbsStretched:
              CellTag.Attr.AddAttr('backstyle', 'stretch');
            rvbsTiled:
              CellTag.Attr.AddAttr('backstyle', 'tile');
            rvbsCentered:
              CellTag.Attr.AddAttr('backstyle', 'center');
          end;

          if Cell.BackgroundImageFileName<>'' then
            CellTag.Attr.AddAttr('imagefilename', String(Cell.BackgroundImageFileName));

          if (Cell.BackgroundImage<>nil) and not Cell.BackgroundImage.Empty then begin
            if SaveImgToFiles then
            begin
              FileName := GetImageFileName(T.Root.BaseFilePath, 'bkgnd',
                GraphicExtension(TGraphicClass(Cell.BackgroundImage.ClassType)));
              Cell.BackgroundImage.SaveToFile(FileName);
              CellTag.Attr.AddAttr('backimgsource', ExtractFileName(FileName));
            end else begin
              CellTag.Attr.AddAttr('backimgclass', Cell.BackgroundImage.ClassName);

              Stream := TRVMemoryStream.Create;
              try
                Cell.BackgroundImage.SaveToStream(Stream);
                Stream.Position := 0;
                CellTag.Attr.AddAttr('backimg', String(RVFStream2TextString(Stream)));
              finally
                Stream.Free;
              end;
            end;
          end;

          if not Cell.VisibleBorders.Left then
            CellTag.Attr.AddAttr('leftbordervisible', '0');

          if not Cell.VisibleBorders.Right then
            CellTag.Attr.AddAttr('rightbordervisible', '0');

          if not Cell.VisibleBorders.Top then
            CellTag.Attr.AddAttr('topbordervisible', '0');

          if not Cell.VisibleBorders.Bottom then
            CellTag.Attr.AddAttr('bottombordervisible', '0');

          SaveRVDataToXML(Cell.GetRVData, CellTag, Default, False, RVStyle,
            SaveImgToFiles, Bullets, 0, -1, Item.Cells[X, Y].GetRVData.Items.Count-1, -1,
            TagsArePChars, OnUnknownItem, OnSaveControl, UseStyleNames, SaveLineBreaks);
        end;
      end;
    end;
  end;

  procedure WriteMarker(T: TXMLTag; Item: TRVMarkerItemInfo; ItemNo: Integer);
  begin
    WriteNonTextItem(T, Item, ItemNo);
    with T.Attr do
    begin
      if UseStyleNames then
        AddAttr('style', RVStyle.ListStyles[Item.ListNo].StyleName)
      else
        AddAttr('styleno', IntToStr(Item.ListNo));

      if Item.Level <> 0 then
        AddAttr('level', IntToStr(Item.Level));

      if Item.NoHTMLImageSize then
        AddAttr('htmlimagesize', '0');

      if Item.StartFrom <> 0 then
        AddAttr('start', IntToStr(Item.StartFrom));

      if Item.Reset = True then
        AddAttr('usestart', '1');
    end;
  end;

  procedure WriteTab(T: TXMLTag; Item: TRVTabItemInfo; ItemNo: Integer);
  begin
    WriteNonTextItem(T, Item, ItemNo);
    with T.Attr do
    begin
      if UseStyleNames then
        AddAttr('textstyle', RVStyle.TextStyles[Item.TextStyleNo].StyleName)
      else
        AddAttr('textstyleno', IntToStr(Item.TextStyleNo));
    end;
  end;

  procedure AddNonTextItem(Node: TXMLTag; ItemNo: Integer);
  var Item: TCustomRVItemInfo;
      T: TXMLTag;
      DItemNo: Integer;
  begin
    if SaveLineBreaks and not RVData.IsFromNewLine(ItemNo) then begin
      TCustomRVFormattedData(RVData).Item2FirstDrawItem(ItemNo, DItemNo);
      if TCustomRVFormattedData(RVData).DrawItems[DItemNo].FromNewLine then
        Node.Items.AddTag('linebreak', Node, Node.Root);
    end;
    Item := RVData.GetItem(ItemNo);
    T := nil;
    case RVData.GetItemStyle(ItemNo) of
      rvsBreak: begin
        T := Node.Items.AddTag('break', Node, Node.Root);
        WriteBreak(T, TRVBreakItemInfo(Item), ItemNo); end;
      rvsPicture: begin
        T := Node.Items.AddTag('image', Node, Node.Root);
        WriteImage(T, TRVGraphicItemInfo(Item), ItemNo); end;
      rvsHotspot: begin
        T := Node.Items.AddTag('hotspot', Node, Node.Root);
        WriteHotSpot(T, TRVHotSpotItemInfo(Item), ItemNo); end;
      rvsComponent: begin
        T := Node.Items.AddTag('control', Node, Node.Root);
        WriteControl(T, TRVControlItemInfo(Item), ItemNo); end;
      rvsBullet: begin
        T := Node.Items.AddTag('bullet', Node, Node.Root);
        WriteBullet(T, TRVBulletItemInfo(Item), ItemNo); end;
      rvsHotPicture: begin
        T := Node.Items.AddTag('hotimage', Node, Node.Root);
        WriteImage(T, TRVGraphicItemInfo(Item), ItemNo); end;
      rvsTable: begin
        T := Node.Items.AddTag('table', Node, Node.Root);
        WriteTable(T, TRVTableItemInfo(Item), ItemNo); end;
      rvsListMarker: begin
        T := Node.Items.AddTag('marker', Node, Node.Root);
        WriteMarker(T, TRVMarkerItemInfo(Item), ItemNo); end;
      rvsTab: begin
        T := Node.Items.AddTag('tab', Node, Node.Root);
        WriteTab(T, TRVTabItemInfo(Item), ItemNo); end;
      rvsLabel: begin
        T := Node.Items.AddTag('label', Node, Node.Root);
        WriteLabel(T, TRVLabelItemInfo(Item), ItemNo); end;
      {$IFNDEF RVDONOTUSESEQ}
      rvsSequence: begin
        T := Node.Items.AddTag('seq', Node, Node.Root);
        WriteSeq(T, TRVSeqItemInfo(Item), ItemNo); end;
      rvsFootnote: begin
        T := Node.Items.AddTag('footnote', Node, Node.Root);
        WriteFootnote(T, TRVFootnoteItemInfo(Item), ItemNo);end;
      rvsEndnote: begin
        T := Node.Items.AddTag('endnote', Node, Node.Root);
        WriteEndnote(T, TRVEndnoteItemInfo(Item), ItemNo);end;
      rvsNoteReference: begin
        T := Node.Items.AddTag('noteref', Node, Node.Root);
        WriteNoteref(T, TRVNoteReferenceItemInfo(Item), ItemNo);end;
      rvsSidenote: begin
        T := Node.Items.AddTag('sidenote', Node, Node.Root);
        WriteSidenote(T, TRVSidenoteItemInfo(Item), ItemNo);
        end;
      rvsTextBox: begin
        T := Node.Items.AddTag('textbox', Node, Node.Root);
        WriteTextBox(T, TRVTextBoxItemInfo(Item), ItemNo);
        end;
      {$ENDIF}        
      else begin
        if Assigned(OnUnknownItem) then
          OnUnknownItem(RVData, ItemNo, Item,
            Node.Items.AddTag('', Node, Node.Root));
      end;
      if T<>nil then begin
        T.TagCodePage := RVData.GetRVStyle.DefCodePage;
        T.ValueCodePage := T.TagCodePage;
      end;
    end;
  end;

  procedure AddText(Node: TXMLTag; ItemNo, StartOffs, EndOffs: Integer);
  var T: TXMLTag;
      Item: TRVTextItemInfo;
  begin
    Item := TRVTextItemInfo(RVData.GetItem(ItemNo));
    if RVStyle.TextStyles[RVData.GetActualStyle(Item)].Unicode then begin
      T := Node.Items.AddTag('utext', Node, Node.Root);
      T.UnicodeMode := xmleRawUnicode;
      end
    else
      T := Node.Items.AddTag('text', Node, Node.Root);
    T.ValueCodePage := RVData.GetStyleCodePage(RVData.GetActualStyle(Item));
    T.TagCodePage := RVData.GetRVStyle.DefCodePage;
    WriteText(T, Item, ItemNo, StartOffs, EndOffs);
  end;

var
  I: Integer;
  T: TXMLTag;
begin
  if SaveStyle then
    if (Default.BreakColor <> DefaultXMLStyle.BreakColor) or
      (Default.BreakWidth <> DefaultXMLStyle.BreakWidth) or
      (Default.ParaNo <> DefaultXMLStyle.ParaNo) or
      (Default.StyleNo <> DefaultXMLStyle.StyleNo) or
      (Default.VAlign <> DefaultXMLStyle.VAlign) then
    begin
      T := Node.Items.AddTag('default', Node, Node.Root);
      T.TagCodePage := RVData.GetRVStyle.DefCodePage;
      T.ValueCodePage := T.TagCodePage;
      if Default.StyleNo <> 0 then
        T.Attr.AddAttr('textstyleno', IntToStr(Default.StyleNo));
      if Default.ParaNo <> 0 then
        T.Attr.AddAttr('parastyleno', IntToStr(Default.ParaNo));
      T.Attr.AddAttr('valign', GetVAlignStr(Default.VAlign));
      if Default.BreakColor <> clNone then
        T.Attr.AddAttr('breakcolor', ColorToString(Default.BreakColor));
      if Default.BreakWidth <> 1 then
        T.Attr.AddAttr('breakwidth', IntToStr(Default.BreakWidth));
      end;

  if (SelStart<0) or (SelEnd<0) then
    exit; // empty document

  if SelOffset<0 then
    SelOffset := RVData.GetOffsBeforeItem(SelStart);
  if SelEndOffset<0 then
    SelEndOffset := RVData.GetOffsAfterItem(SelEnd);

  if (SelOffset>=RVData.GetOffsAfterItem(SelStart)) and
     (SelOffset>RVData.GetOffsBeforeItem(SelStart)) then begin
    inc(SelStart);
    if SelStart=RVData.ItemCount then
      exit;
    SelOffset := RVData.GetOffsBeforeItem(SelStart);
  end;

  if (SelEndOffset<=RVData.GetOffsBeforeItem(SelEnd)) and
     (SelEndOffset<RVData.GetOffsBeforeItem(SelEnd)) then begin
    dec(SelEnd);
    if SelEnd<0 then
      exit;
    SelEndOffset := RVData.GetOffsAfterItem(SelEnd);
  end;

  if (SelStart = SelEnd) and (SelOffset = SelEndOffset) and (SelOffset>=0) then
    exit; // empty selection


  if (SelStart = SelEnd) then begin
    if (SelOffset<=RVData.GetOffsBeforeItem(SelStart)) and
       RVData.GetItem(SelStart).PageBreakBefore then
      Node.Items.AddTag('pagebreak', Node, Node.Root);
    if RVData.GetItem(SelStart) is TRVTextItemInfo then
      AddText(Node, SelStart, SelOffset, SelEndOffset)
    else
      AddNonTextItem(Node, SelStart);
    exit;
  end;

  if (SelOffset<=RVData.GetOffsBeforeItem(SelStart)) and
     RVData.GetItem(SelStart).PageBreakBefore then
    Node.Items.AddTag('pagebreak', Node, Node.Root);
  if RVData.GetItem(SelStart) is TRVTextItemInfo then
    AddText(Node, SelStart, SelOffset, RVData.GetOffsAfterItem(SelStart))
  else
    AddNonTextItem(Node, SelStart);


  for I := SelStart+1 to SelEnd-1 do begin
    if RVData.GetItem(I).PageBreakBefore then
      Node.Items.AddTag('pagebreak', Node, Node.Root);
    if RVData.GetItem(I) is TRVTextItemInfo then
      AddText(Node, I, RVData.GetOffsBeforeItem(I), RVData.GetOffsAfterItem(I))
    else
      AddNonTextItem(Node, I);
  end;

  if RVData.GetItem(SelEnd).PageBreakBefore then
      Node.Items.AddTag('pagebreak', Node, Node.Root);
  if RVData.GetItem(SelEnd) is TRVTextItemInfo then
    AddText(Node, SelEnd, RVData.GetOffsBeforeItem(SelEnd), SelEndOffset)
  else
    AddNonTextItem(Node, SelEnd);
end;


{$IFNDEF RVDONOTUSESTYLETEMPLATES}
{------------ Saving and loading sets of font and para properties -------------}

const FontInfoPropStr: array[TRVFontInfoProperty] of TRVAnsiString =
  (
    'fontname', 'size', 'charset', 'unicode',
    'bold', 'italic', 'underline', 'strikeout',
    'overline', 'allcaps', 'subsuperscripttype',
    'vshift', 'color', 'backcolor',
    'jump', 'hoverbackcolor', 'hovercolor', 'hoverunderline',
    'jumpcursor',
    'nextstyleno', 'protection', 'charscale', 'basestyleno',
    'bidimode', 'charspacing', 'htmlcode', 'rtfcode', 'docxcode', 'hidden',
    {$IFDEF RVLANGUAGEPROPERTY}
    'language',
    {$ENDIF}
    'underlinetype', 'underlinecolor', 'hoverunderlinecolor',
    '', '', '', '', ''
  );

const ParaInfoPropStr: array[TRVParaInfoProperty] of TRVAnsiString =
  (
    'firstindent', 'leftindent', 'rightindent',
    'spacebefore', 'spaceafter', 'alignment',
    'nextparano', 'defstyleno', 'linespacing', 'linespacingtype',
    'background_color',
    'background_bo_left', 'background_bo_top',
    'background_bo_right', 'background_bo_bottom',
    'border_color', 'border_style',
    'border_width', 'border_internalwidth',
    'border_bo_left', 'border_bo_top',
    'border_bo_right', 'border_bo_bottom',
    'border_vis_left', 'border_vis_top',
    'border_vis_right', 'border_vis_bottom',
    'nowrap', 'readonly', 'styleprotect', 'ignoreenter',
    'keeplinestogether', 'keepwithnext', 'tabs',
    'bidimode', 'outlinelevel',
    '', '', '', '', '');

procedure SaveFontProps(Node: TXMLTag; Props: TRVFontInfoProperties);
var Prop: TRVFontInfoProperty;
begin
  for Prop := Low(TRVFontInfoProperty) to High(TRVFontInfoProperty) do
    if (Prop in Props) and (FontInfoPropStr[Prop]<>'') then
      Node.Attr.AddAttr(FontInfoPropStr[Prop], '1');
end;

function GetFontProp(const S: TRVAnsiString; var Prop: TRVFontInfoProperty): Boolean;
begin
  Result := True;
  Prop := Low(TRVFontInfoProperty);
  repeat
    if FontInfoPropStr[Prop]=S then
      exit;
    inc(Prop);
  until Prop = High(TRVFontInfoProperty);
  Result := False;
end;

function GetFontProps(Node: TXMLTag): TRVFontInfoProperties;
var Prop : TRVFontInfoProperty;
    I: Integer;
begin
  Result := [];
  for i := 0 to  Node.Attr.Count-1 do
    if (Node.Attr[i].Value = '1') and GetFontProp(Node.Attr[i].Name, Prop) then
      Include(Result, Prop);
end;

procedure SaveParaProps(Node: TXMLTag; Props: TRVParaInfoProperties);
var Prop: TRVParaInfoProperty;
begin
  for Prop := Low(TRVParaInfoProperty) to High(TRVParaInfoProperty) do
    if (Prop in Props) and (ParaInfoPropStr[Prop]<>'') then
      Node.Attr.AddAttr(ParaInfoPropStr[Prop], '1');
end;

function GetParaProp(const S: TRVAnsiString; var Prop: TRVParaInfoProperty): Boolean;
begin
  Result := True;
  Prop := Low(TRVParaInfoProperty);
  repeat
    if ParaInfoPropStr[Prop]=S then
      exit;
    inc(Prop);
  until Prop = High(TRVParaInfoProperty);
  Result := False;
end;

function GetParaProps(Node: TXMLTag): TRVParaInfoProperties;
var Prop : TRVParaInfoProperty;
    I: Integer;
begin
  Result := [];
  for i := 0 to  Node.Attr.Count-1 do
    if (Node.Attr[i].Value = '1') and GetParaProp(Node.Attr[i].Name, Prop) then
      Include(Result, Prop);
end;
{$ENDIF}

{--------------------------------LoadStyles---------------------------------}

procedure DoLoadStyleBaseFromXML(Attr: TXMLAttr; Style: TCustomRVInfo);
var S: TRVAnsiString;
begin
  S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Attr.Name);
  if S = 'name' then
    Style.StyleName := Attr.Value
  else if S = 'standard' then
    Style.Standard := Attr.Value <> '0';
end;

procedure DoLoadFontOrParaStyleFromXML(Attr: TXMLAttr;
  Style: TCustomFontOrParaRVInfo
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ;UseStyleTemplates: Boolean; StyleTemplates: TRVStyleTemplateCollection
  {$ENDIF});
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
var S: TRVAnsiString;
    ST: TRVStyleTemplate;
{$ENDIF}
begin
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Attr.Name);
  if S='styletemplate' then begin
    if UseStyleTemplates and (StyleTemplates<>nil) then begin
      ST := StyleTemplates.FindItemByName(Attr.Value);
      if ST<>nil then
        Style.StyleTemplateId := ST.Id
    end;
    end
  else
  {$ENDIF}  
    DoLoadStyleBaseFromXML(Attr, Style);
end;

procedure DoLoadCustomFontFromXML(Attr: TXMLAttr; Font: TCustomRVFontInfo;
  RVStyle: TRVStyle; XMLUnits: TRVStyleUnits
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; UseStyleTemplates: Boolean;
  StyleTemplates: TRVStyleTemplateCollection
  {$ENDIF});
var
  S: TRVAnsiString;
  Val: String;
  Charset: Integer;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ST: TRVStyleTemplate;
  {$ENDIF}

  procedure LoadProtection(Attr: TXMLAttr; Option: TRVProtectOption);
  begin
    if Attr.Value = '1' then
      Font.Protection := Font.Protection + [Option]
    else if Attr.Value = '0' then
      Font.Protection := Font.Protection - [Option];
  end;

  procedure LoadFontStyle(Attr: TXMLAttr; FontStyle: TFontStyle);
  begin
    if Attr.Value = '1' then
      Font.Style := Font.Style + [FontStyle]
    else if Attr.Value = '0' then
      Font.Style := Font.Style - [FontStyle];
  end;

  procedure LoadFontStyleEx(Attr: TXMLAttr; FontStyle: TRVFontStyle);
  begin
    if Attr.Value = '1' then
      Font.StyleEx := Font.StyleEx + [FontStyle]
    else if Attr.Value = '0' then
      Font.StyleEx := Font.StyleEx - [FontStyle];
  end;

  procedure LoadOption(Attr: TXMLAttr; Option: TRVTextOption);
  begin
    if Attr.Value = '1' then
      Font.Options := Font.Options + [Option]
    else if Attr.Value = '0' then
      Font.Options := Font.Options - [Option];
  end;

  function GetUnderlineType(const s: String): TRVUnderlineType;
  begin
    if s='thick' then
      Result := rvutThick
    else if s='double' then
      Result := rvutDouble
    else if s='dotted' then
      Result := rvutDotted
    else if s='thickdotted' then
      Result := rvutThickDotted
    else if s='dashed' then
      Result := rvutDashed
    else if s='thickdashed' then
      Result := rvutThickDashed
    else if s='longdashed' then
      Result := rvutLongDashed
    else if s='thicklongdashed' then
      Result := rvutThickLongDashed
    else if s='dashdotted' then
      Result := rvutDashDotted
    else if s='thickdashdotted' then
      Result := rvutThickDashDotted
    else
      Result := rvutNormal;
  end;


begin
  S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Attr.Name);
  Val := Attr.Value;

  if S = 'backcolor' then
    Font.BackColor := StringToColor(Val)

  else if S = 'charscale' then
    Font.CharScale := StrToIntDef(Val, Font.CharScale)

  else if S = 'charspacing' then
    Font.CharSpacing := GetLengthVal(Val, RVStyle, XMLUnits)

  else if S = 'charset' then
  begin
    if IdentToCharset(Val, Charset) then
      Font.Charset := Charset;
  end

  else if S = 'color' then
    Font.Color := StringToColor(Val)

  else if S = 'fontname' then
    Font.FontName := Val

  else if S = 'hoverbackcolor' then
    Font.HoverBackColor := StringToColor(Val)

  else if S = 'underlinecolor' then
    Font.UnderlineColor := StringToColor(Val)

  else if S = 'hoverunderlinecolor' then
    Font.HoverUnderlineColor := StringToColor(Val)

  else if S = 'hovercolor' then
    Font.HoverColor := StringToColor(Val)

  else if S = 'hoverunderline' then
  begin
    if Val = '1' then
      Font.HoverEffects := Font.HoverEffects + [rvheUnderline]
    else if Val = '0' then
      Font.HoverEffects := Font.HoverEffects - [rvheUnderline]
  end

  else if S = 'underlinetype' then
    Font.UnderlineType := GetUnderlineType(Val)


  else if S = 'stylechangeprotect' then
    LoadProtection(Attr, rvprStyleProtect)

  else if S = 'texteditingprotect' then
    LoadProtection(Attr, rvprModifyProtect)

  else if S = 'textdeleteprotect' then
    LoadProtection(Attr, rvprDeleteProtect)

  else if S = 'textconcateprotect' then
    LoadProtection(Attr, rvprConcateProtect)

  else if S = 'styleautoswitchprotect' then
    LoadProtection(Attr, rvprDoNotAutoSwitch)

  else if S = 'parastartprotect' then
    LoadProtection(Attr, rvprParaStartProtect)

  else if S = 'stylesplitprotect' then
    LoadProtection(Attr, rvprStyleSplitProtect)

  else if S = 'sticking' then
    LoadProtection(Attr, rvprSticking)

  else if S = 'sticking2' then
    LoadProtection(Attr, rvprSticking2)

  else if S = 'sticking3' then
    LoadProtection(Attr, rvprSticking3)

  else if S = 'sticktotop' then
    LoadProtection(Attr, rvprStickToTop)

  else if S = 'sticktobottom' then
    LoadProtection(Attr, rvprStickToBottom)

  else if S = 'size' then
    Font.Size := StrToIntDef(Val, Font.Size)

  else if S = 'sizedouble' then
    Font.SizeDouble := StrToIntDef(Val, Font.SizeDouble)

  else if S = 'bold' then
    LoadFontStyle(Attr, fsBold)

  else if S = 'italic' then
    LoadFontStyle(Attr, fsItalic)

  else if S = 'underline' then
    LoadFontStyle(Attr, fsUnderline)

  else if S = 'strikeout' then
    LoadFontStyle(Attr, fsStrikeOut)

  else if S = 'overline' then
    LoadFontStyleEx(Attr, rvfsOverline)

  else if S = 'allcaps' then
    LoadFontStyleEx(Attr, rvfsAllCaps)

  else if S = 'vshift' then
    Font.VShift := StrToIntDef(Val, Font.VShift)

  else if S = 'subsuperscripttype' then
  begin
    if Val = 'normal' then
      Font.SubSuperScriptType := rvsssNormal
    else if Val = 'super' then
      Font.SubSuperScriptType := rvsssSuperScript
    else if Val = 'sub' then
      Font.SubSuperScriptType := rvsssSubscript;
  end

  else if S = 'bidimode' then
  begin
    if Val = 'none' then
      Font.BiDiMode := rvbdUnspecified
    else if Val = 'ltr' then
      Font.BiDiMode := rvbdLeftToRight
    else if Val = 'rtl' then
      Font.BiDiMode := rvbdRightToLeft;
  end

  else if S = 'rtfcode' then
    LoadOption(Attr, rvteoRTFCode)

  else if S = 'docxcode' then
    LoadOption(Attr, rvteoDocXCode)

  else if S = 'docxinruncode' then
    LoadOption(Attr, rvteoDocXInRunCode)

  else if S = 'htmlcode' then
    LoadOption(Attr, rvteoHTMLCode)

  else if S = 'hidden' then
    LoadOption(Attr, rvteoHidden)

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  else if S = 'parastyletemplate' then begin
    if UseStyleTemplates and (StyleTemplates<>nil) then begin
      ST := StyleTemplates.FindItemByName(Attr.Value);
      if ST<>nil then
        Font.ParaStyleTemplateId := ST.Id
    end;
    end
  {$ENDIF}
  
  else
    DoLoadFontOrParaStyleFromXML(Attr, Font
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates, StyleTemplates{$ENDIF});

end;

procedure DoLoadFontFromXML(Attr: TXMLAttr; Font: TFontInfo;
  RVStyle: TRVStyle; XMLUnits: TRVStyleUnits
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; UseStyleTemplates: Boolean;
  StyleTemplates: TRVStyleTemplateCollection
  {$ENDIF});
var S: TRVAnsiString;
  Val: String;
begin
  S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Attr.Name);
  Val := Attr.Value;

  if S = 'jump' then
    Font.Jump := Val <> '0'

  else if S = 'nextstyleno' then
    Font.NextStyleNo := StrToIntDef(Val, Font.NextStyleNo)

  else if S = 'unicode' then
    Font.Unicode := Val <> '0'

  else
    DoLoadCustomFontFromXML(Attr, Font, RVStyle, XMLUnits
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      , UseStyleTemplates, StyleTemplates
      {$ENDIF});
end;

procedure LoadFontFromXML(Node: TXMLTag; Font: TCustomRVFontInfo; RVStyle: TRVStyle;
  XMLUnits: TRVStyleUnits
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; UseStyleTemplates: Boolean;
  StyleTemplates: TRVStyleTemplateCollection
  {$ENDIF});
var
  I: Integer;
begin
  if Font is TFontInfo then
    for I := 0 to Node.Attr.Count - 1 do
      DoLoadFontFromXML(Node.Attr[I], TFontInfo(Font), RVStyle, XMLUnits
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates, StyleTemplates{$ENDIF})
  else
    for I := 0 to Node.Attr.Count - 1 do
      DoLoadCustomFontFromXML(Node.Attr[I], Font, RVStyle, XMLUnits
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates, StyleTemplates{$ENDIF});
end;


procedure DoLoadCustomParaFromXML(Attr: TXMLAttr; Para: TCustomRVParaInfo;
  RVStyle: TRVStyle; XMLUnits: TRVStyleUnits;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  UseStyleTemplates: Boolean;
  StyleTemplates: TRVStyleTemplateCollection;
  {$ENDIF}
  var LineSpacingChanged: Boolean);
var
  S: TRVAnsiString;
  SV: string;
begin
  S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Attr.Name);
  SV := Attr.Value;

  if LoadBorderProperty(S, SV, Para.Border, RVStyle, XMLUnits) or
     LoadBackgroundRectProperty(S, SV, Para.Background, RVStyle, XMLUnits) then
    exit;

  if S = 'align' then
  begin
    if SV = 'left' then
      Para.Alignment := rvaLeft
    else if SV = 'right' then
      Para.Alignment := rvaRight
    else if SV = 'center' then
      Para.Alignment := rvaCenter
    else if SV = 'justify' then
      Para.Alignment := rvaJustify;
  end

  else if S = 'bidimode' then
  begin
    if SV = 'none' then
      Para.BiDiMode := rvbdUnspecified
    else if SV = 'ltr' then
      Para.BiDiMode := rvbdLeftToRight
    else if SV = 'rtl' then
      Para.BiDiMode := rvbdRightToLeft;
  end

  else if S = 'linespacing' then
  begin
    Para.LineSpacing := StrToIntDef(SV, Para.LineSpacing);
    LineSpacingChanged := True;
  end

  else if S = 'linespacingtype' then
  begin
    if SV = '%' then
      Para.LineSpacingType := rvlsPercent
    else if SV = 'spacebetween' then
      Para.LineSpacingType := rvlsSpaceBetween
    else if SV = 'atleast' then
      Para.LineSpacingType := rvlsLineHeightAtLeast
    else if SV = 'exact' then
      Para.LineSpacingType := rvlsLineHeightExact;
  end

  else if S = 'firstindent' then
    Para.FirstIndent := GetLengthVal(SV, RVStyle, XMLUnits)

  else if S = 'leftindent' then
    Para.LeftIndent := GetLengthVal(SV, RVStyle, XMLUnits)

  else if S = 'wrap' then
  begin
    if SV = '1' then
      Para.Options := Para.Options - [rvpaoNoWrap]
    else if SV = '0' then
      Para.Options := Para.Options + [rvpaoNoWrap];
  end

  else if S = 'readonly' then
  begin
    if SV = '1' then
      Para.Options := Para.Options + [rvpaoReadOnly]
    else if SV = '0' then
      Para.Options := Para.Options - [rvpaoReadOnly];
  end

  else if S = 'styleprotect' then
  begin
    if SV = '1' then
      Para.Options := Para.Options + [rvpaoStyleProtect]
    else if SV = '0' then
      Para.Options := Para.Options - [rvpaoStyleProtect];
  end

  else if S = 'ignoreenter' then
  begin
    if SV = '1' then
      Para.Options := Para.Options + [rvpaoDoNotWantReturns]
    else if SV = '0' then
      Para.Options := Para.Options - [rvpaoDoNotWantReturns];
  end

  else if S = 'keeplinestogether' then
  begin
    if SV = '1' then
      Para.Options := Para.Options + [rvpaoKeepLinesTogether]
    else if SV = '0' then
      Para.Options := Para.Options - [rvpaoKeepLinesTogether];
  end

  else if S = 'keepwithnext' then
  begin
    if SV = '1' then
      Para.Options := Para.Options + [rvpaoKeepWithNext]
    else if SV = '0' then
      Para.Options := Para.Options - [rvpaoKeepWithNext];
  end

  else if S = 'rightindent' then
    Para.RightIndent := GetLengthVal(SV, RVStyle, XMLUnits)

  else if S = 'spaceafter' then
    Para.SpaceAfter := GetLengthVal(SV, RVStyle, XMLUnits)

  else if S = 'spacebefore' then
    Para.SpaceBefore := GetLengthVal(SV, RVStyle, XMLUnits)

  else if S = 'outlinelevel' then
    Para.OutlineLevel := StrToIntDef(SV, Para.OutlineLevel)

  else
    DoLoadFontOrParaStyleFromXML(Attr, Para
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates, StyleTemplates{$ENDIF});

end;


procedure DoLoadParaFromXML(Attr: TXMLAttr; Para: TParaInfo;
  RVStyle: TRVStyle; XMLUnits: TRVStyleUnits;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  UseStyleTemplates: Boolean;
  StyleTemplates: TRVStyleTemplateCollection;
  {$ENDIF}
  var LineSpacingChanged: Boolean);
var
  S: TRVAnsiString;
  SV: string;
begin
  S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Attr.Name);
  SV := Attr.Value;

  if S = 'nextparano' then
    Para.NextParaNo := StrToIntDef(SV, Para.NextParaNo)
  else
    DoLoadCustomParaFromXML(Attr, Para, RVStyle, XMLUnits,
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      UseStyleTemplates, StyleTemplates,
      {$ENDIF}
      LineSpacingChanged);
end;

procedure LoadParaFromXML(Node: TXMLTag; Para: TCustomRVParaInfo;
  RVStyle: TRVStyle; XMLUnits: TRVStyleUnits
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; UseStyleTemplates: Boolean;
  StyleTemplates: TRVStyleTemplateCollection
  {$ENDIF});
var
  I: Integer;
  LineSpacingChanged: Boolean;
  {$IFNDEF RVDONOTUSETABS}
  S: TRVAnsiString;
  SV: string;  
  Tab: TRVTabInfo;
  T: TXMLTag;
  J: Integer;
  {$ENDIF}
begin

 {$IFNDEF RVDONOTUSETABS}
  for I := 0 to Node.Items.Count - 1 do
    if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name) = 'tab' then
    begin
      T := Node.Items[I];
      Tab := Para.Tabs.Add;
      for J := 0 to T.Attr.Count - 1 do
      begin
        S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(T.Attr[J].Name);
        if S = 'position' then
          Tab.Position := GetLengthVal(T.Attr[J].Value, RVStyle, XMLUnits)
        else if S = 'align' then
        begin
          SV := LowerCase(T.Attr[J].Value);
          if SV = 'left' then
            Tab.Align := rvtaLeft
          else if SV = 'right' then
            Tab.Align := rvtaRight
          else if SV = 'center' then
            Tab.Align := rvtaCenter;
        end
        else if S = 'leader' then
          Tab.Leader := T.Attr[J].Value;
      end;
    end;
  {$ENDIF}

  LineSpacingChanged := False;

  if Para is TParaInfo then
    for I := 0 to Node.Attr.Count - 1 do
      DoLoadParaFromXML(Node.Attr[I], TParaInfo(Para), RVStyle, XMLUnits,
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates, StyleTemplates,{$ENDIF}
        LineSpacingChanged)
  else
    for I := 0 to Node.Attr.Count - 1 do
      DoLoadCustomParaFromXML(Node.Attr[I], Para, RVStyle, XMLUnits,
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates, StyleTemplates,{$ENDIF}
        LineSpacingChanged);

  if LineSpacingChanged and
    (Para.LineSpacingType in [rvlsSpaceBetween, rvlsLineHeightAtLeast, rvlsLineHeightExact]) then
    Para.LineSpacing := RVStyle.DifferentUnitsToUnits(Para.LineSpacing, XMLUnits);
end;

{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure LoadStyleTemplateFromXML(Node: TXMLTag; ST: TRVStyleTemplate;
  RVStyle: TRVStyle; XMLUnits: TRVStyleUnits; NextSL, ParentSL: TStrings);
var I: Integer;
  S: TRVAnsiString;
  SV: string;
  Index: Integer;
begin
  Index := NextSL.Count;
  NextSL.Add('');
  ParentSL.Add('');
  for I := 0 to Node.Attr.Count - 1 do begin
    S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Attr[I].Name);
    SV := Node.Attr[I].Value;

    if S='name' then
      ST.Name := SV

    else if S='quickaccess' then
      ST.QuickAccess := SV<>'0'

    else if S='next' then
      NextSL.Strings[Index] := SV

    else if S='parent' then
      ParentSL.Strings[Index] := SV

    else if S='kind' then begin
      if SV='paratext' then
        ST.Kind := rvstkParaText
      else if SV='para' then
        ST.Kind := rvstkPara
      else if SV='text' then
        ST.Kind := rvstkText;
    end;
  end;

  for I := 0 to Node.Items.Count - 1 do begin
    S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name);
    if S='font_props' then
      ST.ValidTextProperties := GetFontProps(Node.Items[I])

    else if S='para_props' then
      ST.ValidParaProperties := GetParaProps(Node.Items[I])

    else if S='font' then
      LoadFontFromXML(Node.Items[i], ST.TextStyle, RVStyle, XMLUnits, False, nil)

    else if S='para' then
      LoadParaFromXML(Node.Items[i], ST.ParaStyle, RVStyle, XMLUnits, False, nil);

  end;
end;
{$ENDIF}

procedure LoadMarkerFromXML(Node: TXMLTag; Item: TRVListLevel; RVStyle: TRVStyle;
  XMLUnits: TRVStyleUnits);
var
  S: TRVAnsiString;
  SV: String;
  WS: WideString;
  I, J: Integer;
  Stream: TRVMemoryStream;
  Cl: TClass;
  B: TBitmap;
  Gr: TGraphic;
begin
  Stream := nil;
  Cl := nil;
  for I := 0 to Node.Attr.Count - 1 do
    with Node.Attr[I] do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Name);

      if S = 'type' then
      begin
        SV := LowerCase(Value);
        if SV = 'bullet' then
          Item.ListType := rvlstBullet
        else if SV = 'unibullet' then
          Item.ListType := rvlstUnicodeBullet
        else if SV = 'picture' then
          Item.ListType := rvlstPicture
        else if SV = 'imglist' then
          Item.ListType :=  rvlstImageList
        else if SV = 'imglistcounter' then
          Item.ListType :=  rvlstImageListCounter
        else if SV = 'decimal' then
          Item.ListType := rvlstDecimal
        else if SV = 'loweralpha' then
          Item.ListType := rvlstLowerAlpha
        else if SV = 'upperalpha' then
          Item.ListType := rvlstUpperAlpha
        else if SV = 'lowerroman' then
          Item.ListType := rvlstLowerRoman
        else if SV = 'upperroman' then
          Item.ListType := rvlstUpperRoman;
      end

      else if S = 'fontcharset' then
      begin
        if IdentToCharset(Value, J) then
          Item.Font.Charset := J;
      end

      else if S = 'fontcolor' then
        Item.Font.Color := StringToColor(Value)

      else if S = 'fontname' then
        Item.Font.Name := Value

      else if S = 'fontsize' then
        Item.Font.Size := StrToIntDef(Value, Item.Font.Size)

      else if S = 'fontbold' then
      begin
        if Value = '0' then
          Item.Font.Style := Item.Font.Style - [fsBold]
        else if Value = '1' then
          Item.Font.Style := Item.Font.Style + [fsBold];
      end

      else if S = 'fontitalic' then
      begin
        if Value = '0' then
          Item.Font.Style := Item.Font.Style - [fsItalic]
        else if Value = '1' then
          Item.Font.Style := Item.Font.Style + [fsItalic];
      end

      else if S = 'fontunderline' then
      begin
        if Value = '0' then
          Item.Font.Style := Item.Font.Style - [fsUnderline]
        else if Value = '1' then
          Item.Font.Style := Item.Font.Style + [fsUnderline];
      end

      else if S = 'fontstrikeout' then
      begin
        if Value = '0' then
          Item.Font.Style := Item.Font.Style - [fsStrikeout]
        else if Value = '1' then
          Item.Font.Style := Item.Font.Style + [fsStrikeout];
      end

      else if S = 'formatstr' then
        Item.FormatString := Value

      else if S = 'formatstrw' then begin
          if Stream <> nil then
            Stream.Clear
          else
            Stream := TRVMemoryStream.Create;
          RVFTextString2Stream(TRVAnsiString(Value), Stream);
          Stream.Position := 0;
          SetLength(WS, Stream.Size div 2);
          Stream.ReadBuffer(Pointer(WS)^, Length(WS)*2);
          Item.FormatStringW := WS;
        end

      else if S = 'leftindent' then
        Item.LeftIndent := GetLengthVal(Value, RVStyle, XMLUnits)

      else if S = 'firstindent' then
        Item.FirstIndent := GetLengthVal(Value, RVStyle, XMLUnits)

      else if S = 'markerindent' then
        Item.MarkerIndent := GetLengthVal(Value, RVStyle, XMLUnits)

      else if S = 'markeralign' then
      begin
        SV := LowerCase(Value);
        if SV = 'left' then
          Item.MarkerAlignment := rvmaLeft
        else if SV = 'right' then
          Item.MarkerAlignment := rvmaRight
        else if SV = 'center' then
          Item.MarkerAlignment := rvmaCenter;
      end

      else if S = 'continuous' then
      begin
        if Value = '0' then
          Item.Options := Item.Options - [rvloContinuous]
        else if Value = '1' then
          Item.Options := Item.Options + [rvloContinuous];
      end

      else if S = 'levelreset' then
      begin
        if Value = '0' then
          Item.Options := Item.Options - [rvloLevelReset]
        else if Value = '1' then
          Item.Options := Item.Options + [rvloLevelReset];
      end

      else if S = 'legalstylenumbering' then
      begin
        if Value = '0' then
          Item.Options := Item.Options - [rvloLegalStyleNumbering]
        else if Value = '1' then
          Item.Options := Item.Options + [rvloLegalStyleNumbering];
      end

      else if S = 'imgsource' then
      begin
        if FileExists(Node.Root.BaseFilePath + Value) then
        begin
          Cl := nil;
          Item.Picture.LoadFromFile(Node.Root.BaseFilePath + Value);
        end;
      end

      else if S = 'img' then
      begin
        if Stream <> nil then
          Stream.Clear
        else
          Stream := TRVMemoryStream.Create;
        RVFTextString2Stream(TRVAnsiString(Value), Stream);
      end

      else if S = 'class' then
        Cl := FindClass(Value);

    end;

  if (Stream <> nil) and (Cl <> nil) then
  begin
    Stream.Position := 0;
    Gr := RVGraphicHandler.CreateGraphic(TGraphicClass(Cl));
    try
      Gr.LoadFromStream(Stream);
      Item.Picture.Graphic := Gr;
    finally
      Gr.Free;
    end;

  end;

  if Stream <> nil then
    Stream.Free;

  if Item.ListType in [rvlstImageList,rvlstImageListCounter] then
  begin
    Item.ImageIndex := 0;
    for I := 0 to Node.Items.Count - 1 do
      if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name) = 'img' then
      begin
        B := TBitmap.Create;
        if Node.Items[I].Value <> '' then
        begin
          Stream := TRVMemoryStream.Create;
          RVFTextString2Stream(Node.Items[I].GetANSIValueString, Stream);
          Stream.Position := 0;
          try
            B.LoadFromStream(Stream);
          finally
            Stream.Free;
          end;
        end else
        begin
          for J := 0 to Node.Items[I].Attr.Count - 1 do
            if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Attr[J].Name) = 'source' then
              B.LoadFromFile(Node.Root.BaseFilePath + Node.Items[I].Attr[J].Value);
        end;
        if Item.ImageList = nil then
          Item.ImageList := TImageList.CreateSize(B.Width, B.Height);
        Item.ImageList.Add(B, nil);
        B.Free;
      end;
  end;
end;

procedure LoadListFromXML(Node: TXMLTag; List: TRVListInfo; RVStyle: TRVStyle;
  XMLUnits: TRVStyleUnits);
var
  I: Integer;
  S: TRVAnsiString;
  L: TRVListLevel;
begin
  for I := 0 to Node.Attr.Count - 1 do
    with Node.Attr[I] do
    begin
      S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Name);

      if S = 'standard' then
      begin
        if Value = '0' then
          List.Standard := False
        else if Value = '1' then
          List.Standard := True;
      end

      else if S = 'name' then
        List.StyleName := Value;

    end;

  for I := 0 to Node.Items.Count - 1 do
    if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name) = 'marker' then
    begin
      L := List.Levels.Add;
      LoadMarkerFromXML(Node.Items[I], L, RVStyle, XMLUnits);
    end;
end;
{------------------------------------------------------------------------------}
procedure LoadRVStyle(Node: TXMLTag; RVStyle: TRVStyle;
  StyleLoadingMode: TStyleLoadingMode;
  StyleMap, ParaMap, ListMap: TRVIntegerList;
  CanChangeUnits: Boolean; var XMLUnits: TRVStyleUnits
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; UseStyleTemplates: Boolean;
  StyleTemplateInsertMode: TRVStyleTemplateInsertMode;
  StyleTemplates: TRVStyleTemplateCollection
  {$ENDIF});

var
  FontInfos: TFontInfos;
  ParaInfos: TParaInfos;
  ListInfos: TRVListInfos;

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  procedure GetParaStyleTemplates(Id: TRVStyleTemplateId; var STOld, STNew: TRVStyleTemplate);
  begin
    if (StyleLoadingMode = slmMerge) and (StyleTemplateInsertMode=rvstimIgnoreSourceStyleTemplates) then begin
      STOld := nil;
      STNew := RVStyle.StyleTemplates.NormalStyleTemplate;
      end
    else begin
      if StyleTemplates<>nil then
        STOld := StyleTemplates.FindItemById(Id)
      else
        STOld := nil;
      if STOld<>nil then
        STNew := RVStyle.StyleTemplates.FindItemByName(STOld.Name)
      else
        STNew := nil;
      if (STNew=nil) and RichViewAutoAssignNormalStyleTemplate then
        STNew := RVStyle.StyleTemplates.NormalStyleTemplate;
     end;
  end;
  {.......................................................}
  procedure GetTextStyleTemplates(Jump: Boolean; Id: TRVStyleTemplateId; var STOld, STNew: TRVStyleTemplate);
  begin
    STOld := nil;
    if (StyleLoadingMode = slmMerge) and (StyleTemplateInsertMode=rvstimIgnoreSourceStyleTemplates) then begin
      if Jump then
        STNew := RVStyle.StyleTemplates.FindItemByName(RVHYPERLINKSTYLETEMPLATENAME)
      else
        STNew := nil;
      end
    else begin
      if StyleTemplates<>nil then
        STOld := StyleTemplates.FindItemById(Id)
      else
        STOld := nil;
      if STOld<>nil then
        STNew := RVStyle.StyleTemplates.FindItemByName(STOld.Name)
      else
        STNew := nil;
     end;
  end;
  {.......................................................}
  procedure ApplyStyleTemplatesToParaStyles;
  var i: Integer;
      STOld, STNew: TRVStyleTemplate;
      DoIt: Boolean;
  begin
    if UseStyleTemplates and (StyleLoadingMode = slmMap) then
      exit;
    DoIt := UseStyleTemplates and (StyleLoadingMode in [slmMerge, slmReplace]);
    for i := 0 to ParaInfos.Count-1 do
      if DoIt then begin
        GetParaStyleTemplates(ParaInfos[i].StyleTemplateId, STOld, STNew);
        if STNew<>nil then begin
          ParaInfos[i].StyleTemplateId := STNew.Id;
          if (StyleLoadingMode = slmMerge) and (StyleTemplateInsertMode=rvstimUseTargetFormatting) then begin
            STOld.UpdateModifiedParaStyleProperties(ParaInfos[i]);
            STNew.ApplyToParaStyle(ParaInfos[i]);
          end;
          end
        else
          ParaInfos[i].StyleTemplateId := -1;
        end
      else
        ParaInfos[i].StyleTemplateId := -1;
  end;
  {.......................................................}
  procedure ApplyStyleTemplatesToTextStyles;
  var i: Integer;
      STOld, STNew, STPOld, STPNew: TRVStyleTemplate;
      DoIt: Boolean;
  begin
    if UseStyleTemplates and (StyleLoadingMode = slmMap) then
      exit;
    DoIt := UseStyleTemplates and (StyleLoadingMode in [slmMerge, slmReplace]);
    for i := 0 to FontInfos.Count-1 do
      if DoIt then begin
        GetParaStyleTemplates(FontInfos[i].ParaStyleTemplateId, STPOld, STPNew);
        GetTextStyleTemplates(FontInfos[i].Jump, FontInfos[i].StyleTemplateId, STOld, STNew);
        if STPNew=STNew then
          STNew := nil;
        if STNew<>nil then
          FontInfos[i].StyleTemplateId := STNew.Id
        else
          FontInfos[i].StyleTemplateId := -1;
        if STPNew<>nil then
          FontInfos[i].ParaStyleTemplateId := STPNew.Id
        else
          FontInfos[i].ParaStyleTemplateId := -1;
        if (StyleLoadingMode = slmMerge) and (StyleTemplateInsertMode=rvstimUseTargetFormatting) and
           ((STNew<>nil) or (STPNew<>nil)) then begin
            STOld.UpdateModifiedTextStyleProperties(FontInfos[i], STPOld);
            STNew.ApplyToTextStyle(FontInfos[i], STPNew);
        end;
        end
      else begin
        FontInfos[i].StyleTemplateId := -1;
        FontInfos[i].ParaStyleTemplateId := -1;
      end;
  end;
  {$ENDIF}

var
  I: Integer;
  Font: TFontInfo;
  Para: TParaInfo;
  List: TRVListInfo;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ST: TRVStyleTemplate;
  ParentSL, NextSL: TStringList;
  {$ENDIF}
  TagName, AttrName: TRVAnsiString;
  AttrValue: String;
begin
  StyleMap.Clear;
  ParaMap.Clear;
  ListMap.Clear;
  if (StyleLoadingMode = slmIgnore) or (Node = nil) then
  begin
    for I := 0 to RVStyle.TextStyles.Count - 1 do
      StyleMap.Add(I);
    for I := 0 to RVStyle.ParaStyles.Count - 1 do
      ParaMap.Add(I);
    for I := 0 to RVStyle.ListStyles.Count - 1 do
      ListMap.Add(I);

    Exit;
  end;
  XMLUnits := rvstuPixels;
  for I := 0 to Node.Attr.Count - 1 do
  begin
    AttrName := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Attr[I].Name);
    AttrValue := Node.Attr[I].Value;
    if AttrName = 'inactiveselcolor' then
      RVStyle.InactiveSelColor := StringToColor(AttrValue)
    else if AttrName = 'inactiveseltextcolor' then
      RVStyle.InactiveSelTextColor := StringToColor(AttrValue)
    else if AttrName = 'selcolor' then
      RVStyle.SelColor := StringToColor(AttrValue)
    else if AttrName = 'seltextcolor' then
      RVStyle.SelTextColor := StringToColor(AttrValue)
    else if AttrName = 'units' then begin
      if AttrValue='twips' then
        XMLUnits := rvstuTwips;
    end;
  end;
  if CanChangeUnits then
    RVStyle.ConvertToDifferentUnits(XMLUnits);

  FontInfos := TFontInfos.Create(TFontInfo, nil);
  ParaInfos := TParaInfos.Create(TParaInfo, nil);
  ListInfos := TRVListInfos.Create(TRVListInfo, nil);

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ParentSL := TStringList.Create;
  NextSL := TStringList.Create;
  {$ENDIF}
  try
    // 1. Loading
    for I := 0 to Node.Items.Count - 1 do
    begin
      TagName := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Items[I].Name);
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      if TagName = 'styletemplate'  then
      begin
         if UseStyleTemplates and (RVStyle.MainRVStyle=nil) and
           (StyleTemplateInsertMode<>rvstimIgnoreSourceStyleTemplates) then
           LoadStyleTemplateFromXML(Node.Items[I], StyleTemplates.Add, RVStyle,
             XMLUnits, NextSL, ParentSL);
      end
      else
     {$ENDIF}
      if TagName = 'font' then
      begin
        Font := FontInfos.Add;
        LoadFontFromXML(Node.Items[I], Font, RVStyle, XMLUnits
          {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates, StyleTemplates{$ENDIF});
      end else if TagName = 'para' then
      begin
        Para := ParaInfos.Add;
        LoadParaFromXML(Node.Items[I], Para, RVStyle, XMLUnits
          {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates,StyleTemplates{$ENDIF} );
      end else if TagName = 'list' then
      begin
        List := ListInfos.Add;
        LoadListFromXML(Node.Items[I], List, RVStyle, XMLUnits);
      end;
    end;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    // 2. Updating references in StyleTemplates
    if UseStyleTemplates  and (RVStyle.MainRVStyle=nil) and (StyleTemplates<>nil) then
      for I := 0 to StyleTemplates.Count-1 do
      begin
        if NextSL[I]<>'' then begin
        ST := StyleTemplates.FindItemByName(NextSL[I]);
        if ST<>nil then
          StyleTemplates[I].NextId := ST.Id;
      end;
      if ParentSL[I]<>'' then begin
        ST := StyleTemplates.FindItemByName(ParentSL[I]);
        if ST<>nil then
          StyleTemplates[I].ParentId := ST.Id;
      end;
    end;
    // 3.Merging StyleTemplates
    if UseStyleTemplates  and (RVStyle.MainRVStyle=nil) and (StyleTemplates<>nil) then
      case StyleLoadingMode of
        slmReplace:
          RVStyle.StyleTemplates.AssignStyleTemplates(StyleTemplates, True);
        slmMerge:
          RVStyle.StyleTemplates.MergeWith(StyleTemplates, nil);
        end;
    // 4. Updating FontInfos and ParaInfos according to StyleTemplates
    ApplyStyleTemplatesToParaStyles;
    ApplyStyleTemplatesToTextStyles;
    {$ENDIF}
    // 5. Merging styles
    case StyleLoadingMode of
      slmMerge:
        begin
          RVStyle.TextStyles.MergeWith(FontInfos, rvs_merge_SmartMerge, StyleMap, StyleMap);
          RVStyle.ParaStyles.MergeWith(ParaInfos, rvs_merge_SmartMerge, ParaMap, StyleMap);
          RVStyle.ListStyles.MergeWith(ListInfos, rvs_merge_SmartMerge, ListMap, StyleMap);
        end;
      slmMap:
        begin
          RVStyle.TextStyles.MergeWith(FontInfos, rvs_merge_Map, StyleMap, StyleMap);
          RVStyle.ParaStyles.MergeWith(ParaInfos, rvs_merge_Map, ParaMap, StyleMap);
          RVStyle.ListStyles.MergeWith(ListInfos, rvs_merge_Map, ListMap, StyleMap);
        end;
      slmReplace:
        begin
          RVStyle.TextStyles := FontInfos;
          RVStyle.ParaStyles := ParaInfos;
          RVStyle.ListStyles := ListInfos;
          for I := 0 to RVStyle.TextStyles.Count - 1 do
            StyleMap.Add(I);
          for I := 0 to RVStyle.ParaStyles.Count - 1 do
            ParaMap.Add(I);
          for I := 0 to RVStyle.ListStyles.Count - 1 do
            ListMap.Add(I);
        end;
    end;
  finally
    FontInfos.Free;
    ParaInfos.Free;
    ListInfos.Free;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    ParentSL.Free;
    NextSL.Free;
    {$ENDIF}
  end;
end;

{--------------------------------SaveStyles---------------------------------}


procedure SaveStyleBaseToXML(Node: TXMLTag; Style: TCustomRVInfo;
  SaveStyleName: Boolean; const DefaultStyleName: String);
begin
  if not Style.Standard then
    Node.Attr.AddAttr('standard', '0');
  if SaveStyleName then
    if Style.StyleName <> DefaultStyleName then
      Node.Attr.AddAttr('name', Style.StyleName);
end;

procedure SaveFontOrParaStyleToXML(Node: TXMLTag; Style: TCustomFontOrParaRVInfo;
  SaveStyleName {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates{$ENDIF}: Boolean;
  const DefaultStyleName: String;
  RVStyle: TRVStyle);
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
var StyleTemplate: TRVStyleTemplate;
{$ENDIF}
begin
  SaveStyleBaseToXML(Node, Style, SaveStyleName, DefaultStyleName);
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if UseStyleTemplates then begin
    StyleTemplate := RVStyle.StyleTemplates.FindItemById(Style.StyleTemplateId);
    if StyleTemplate<>nil then
      Node.Attr.AddAttr('styletemplate', StyleTemplate.Name);
  end;
  {$ENDIF}
end;

procedure SaveCustomFontStyleToXML(Node: TXMLTag; Font: TCustomRVFontInfo;
  SaveStyleName {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates{$ENDIF}: Boolean;
  RVStyle: TRVStyle; Props: TRVFontInfoProperties);

  function GetUnderlineTypeStr(ut: TRVUnderlineType): String;
  begin
    case ut of
      rvutThick:
        Result := 'thick';
      rvutDouble:
        Result := 'double';
      rvutDotted:
        Result := 'dotted';
      rvutThickDotted:
        Result := 'thickdotted';
      rvutDashed:
        Result := 'dashed';
      rvutThickDashed:
        Result := 'thickdashed';
      rvutLongDashed:
        Result := 'longdashed';
      rvutThickLongDashed:
        Result := 'thicklongdashed';
      rvutDashDotted:
        Result := 'dashdotted';
      rvutThickDashDotted:
        Result := 'thickdashdotted';
      else
        Result := 'normal';
    end;
  end;

var
  S: string;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ST: TRVStyleTemplate;
  {$ENDIF}
begin

  SaveFontOrParaStyleToXML(Node, Font, SaveStyleName,
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates,{$ENDIF}
    'Font Style', RVStyle);

  if (Font.BackColor <> clNone) and (rvfiBackColor in Props) then
    Node.Attr.AddAttr('backcolor', ColorToString(Font.BackColor));

  if (Font.CharScale <> 100) and (rvfiCharScale in Props) then
    Node.Attr.AddAttr('charscale', IntToStr(Font.CharScale));

  if (Font.CharSpacing <> 0) and (rvfiCharSpacing in Props) then
    Node.Attr.AddAttr('charspacing', ColorToString(Font.CharSpacing));

  if (Font.Charset <> DEFAULT_CHARSET) and (rvfiCharset in Props) then
    if CharsetToIdent(Font.Charset, S) then
      Node.Attr.AddAttr('charset', S);

  if (Font.Color <> clWindowText) and (rvfiColor in Props) then
    Node.Attr.AddAttr('color', ColorToString(Font.Color));

  if (Font.UnderlineColor <> clNone) and (rvfiUnderlineColor in Props) then
    Node.Attr.AddAttr('underlinecolor', ColorToString(Font.UnderlineColor));

  if (Font.FontName <> 'Arial') and (rvfiFontName in Props) then
    Node.Attr.AddAttr('fontname', Font.FontName);

  if (Font.UnderlineType<>rvutNormal) and (rvfiUnderlineType in Props) then
    Node.Attr.AddAttr('underlinetype', GetUnderlineTypeStr(Font.UnderlineType));

  if (Font.HoverBackColor <> clNone) and (rvfiHoverBackColor in Props) then
    Node.Attr.AddAttr('hoverbackcolor', ColorToString(Font.HoverBackColor));

  if (Font.HoverColor <> clNone) and (rvfiHoverColor in Props) then
    Node.Attr.AddAttr('hovercolor', ColorToString(Font.HoverColor));

  if (rvheUnderline in Font.HoverEffects) and (rvfiHoverUnderline in Props) then
    Node.Attr.AddAttr('hoverunderline', '1');

  if (Font.HoverUnderlineColor <> clNone) and (rvfiHoverUnderlineColor in Props) then
    Node.Attr.AddAttr('hoverunderlinecolor', ColorToString(Font.HoverUnderlineColor));

  if rvfiProtection in Props then begin
    if (rvprStyleProtect in Font.Protection) then
      Node.Attr.AddAttr('stylechangeprotect', '1');
    if rvprModifyProtect in Font.Protection then
      Node.Attr.AddAttr('texteditingprotect', '1');
    if rvprDeleteProtect in Font.Protection then
      Node.Attr.AddAttr('textdeleteprotect', '1');
    if rvprConcateProtect in Font.Protection then
      Node.Attr.AddAttr('textconcateprotect', '1');
    if rvprDoNotAutoSwitch in Font.Protection then
      Node.Attr.AddAttr('styleautoswitchprotect', '1');
    if rvprParaStartProtect in Font.Protection then
      Node.Attr.AddAttr('parastartprotect', '1');
    if rvprStyleSplitProtect in Font.Protection then
      Node.Attr.AddAttr('stylesplitprotect', '1');
    if rvprSticking in Font.Protection then
      Node.Attr.AddAttr('sticking', '1');
    if rvprSticking2 in Font.Protection then
      Node.Attr.AddAttr('sticking2', '1');
    if rvprSticking3 in Font.Protection then
      Node.Attr.AddAttr('sticking3', '1');
    if rvprStickToTop in Font.Protection then
      Node.Attr.AddAttr('sticktotop', '1');
    if rvprStickToBottom in Font.Protection then
      Node.Attr.AddAttr('sticktobottom', '1');
  end;

  if (Font.SizeDouble <> 20) and (rvfiSize in Props) then begin
    if Font.SizeDouble mod 2 = 0 then
      Node.Attr.AddAttr('size', IntToStr(Font.Size))
    else
      Node.Attr.AddAttr('sizedouble', IntToStr(Font.SizeDouble))
  end;

  if (fsBold in Font.Style) and (rvfiBold in Props) then
    Node.Attr.AddAttr('bold', '1');

  if (fsItalic in Font.Style) and (rvfiItalic in Props) then
    Node.Attr.AddAttr('italic', '1');

  if (fsUnderline in Font.Style) and (rvfiUnderline in Props) then
    Node.Attr.AddAttr('underline', '1');

  if (fsStrikeOut in Font.Style) and (rvfiStrikeout in Props) then
    Node.Attr.AddAttr('strikeout', '1');

  if (rvfsOverline in Font.StyleEx) and (rvfiOverline in Props) then
    Node.Attr.AddAttr('overline', '1');

  if (rvfsAllCaps in Font.StyleEx) and (rvfiAllCaps in Props) then
    Node.Attr.AddAttr('allcaps', '1');

  if (Font.VShift <> 0) and (rvfiVShift in Props) then
    Node.Attr.AddAttr('vshift', IntToStr(Font.VShift));

  if rvfiSubSuperScriptType in Props then
    case Font.SubSuperScriptType of
      rvsssSubscript:
        Node.Attr.AddAttr('subsuperscripttype', 'sub');
      rvsssSuperscript:
        Node.Attr.AddAttr('subsuperscripttype', 'super');
    end;

  if rvfiBiDiMode in Props then
    case Font.BiDiMode of
      rvbdLeftToRight:
        Node.Attr.AddAttr('bidimode', 'ltr');
      rvbdRightToLeft:
        Node.Attr.AddAttr('bidimode', 'rtl');
    end;

  if (rvteoRTFCode in Font.Options) and (rvfiRTFCode in Props) then
    Node.Attr.AddAttr('rtfcode', '1');

  if (rvteoDocXCode in Font.Options) and (rvfiDocXCode in Props) then
    Node.Attr.AddAttr('docxcode', '1');

  if (rvteoDocXInRunCode in Font.Options) and (rvfiDocXCode in Props) then
    Node.Attr.AddAttr('docxinruncode', '1');

  if (rvteoHTMLCode in Font.Options) and (rvfiHTMLCode in Props) then
    Node.Attr.AddAttr('htmlcode', '1');

  if (rvteoHidden in Font.Options) and (rvfiHidden in Props) then
    Node.Attr.AddAttr('hidden', '1');

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if UseStyleTemplates then begin
    ST := RVStyle.StyleTemplates.FindItemById(Font.ParaStyleTemplateId);
    if ST<>nil then
      Node.Attr.AddAttr('parastyletemplate', ST.Name);
  end;
  {$ENDIF}
end;

procedure SaveFontStyleToXML(Node: TXMLTag; Font: TFontInfo;
  SaveStyleName {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates{$ENDIF}: Boolean;
  RVStyle: TRVStyle;
  Props: TRVFontInfoProperties);
begin
  SaveCustomFontStyleToXML(Node, Font, SaveStyleName,
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates,{$ENDIF}
    RVStyle, Props);

  if Font.Jump then
    Node.Attr.AddAttr('jump', '1');

  if Font.Unicode then
    Node.Attr.AddAttr('unicode', '1')
  else
    Node.Attr.AddAttr('unicode', '0');

  if Font.NextStyleNo <> -1 then
    Node.Attr.AddAttr('nextstyleno', IntToStr(Font.NextStyleNo));
end;

procedure SaveCustomParaStyleToXML(Node: TXMLTag; Para: TCustomRVParaInfo;
  SaveStyleName {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates{$ENDIF}: Boolean; RVStyle: TRVStyle;
  Props: TRVParaInfoProperties);

  function GetAlignStr(Para: TCustomRVParaInfo): String;
  begin
    case Para.Alignment of
      rvaRight: Result := 'right';
      rvaCenter: Result := 'center';
      rvaJustify: Result := 'justify';
      else Result := '';
    end;
  end;


  function GetLineSpacingTypeStr(Para: TCustomRVParaInfo): String;
  begin
    case Para.LineSpacingType of
      rvlsSpaceBetween: Result := 'spacebetween';
      rvlsLineHeightAtLeast: Result := 'atleast';
      rvlsLineHeightExact: Result := 'exact';
      else Result := '';
    end;
  end;


    {$IFNDEF RVDONOTUSETABS}
var i: Integer;
    T: TXMLTag;
    {$ENDIF}
begin

  SaveFontOrParaStyleToXML(Node, Para, SaveStyleName,
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates,{$ENDIF}
    'Paragraph Style', RVStyle);

  if (Para.Alignment <> rvaLeft) and (rvpiAlignment in Props) then
      Node.Attr.AddAttr('align', GetAlignStr(Para));

  if (Para.BiDiMode <> rvbdUnspecified) and (rvpiBiDiMode in Props) then
  begin
    if Para.BiDiMode = rvbdLeftToRight then
      Node.Attr.AddAttr('bidimode', 'ltr')
    else if Para.BiDiMode = rvbdRightToLeft then
      Node.Attr.AddAttr('bidimode', 'rtl');
  end;

  if (Para.FirstIndent <> 0) and (rvpiFirstIndent in Props) then
    Node.Attr.AddAttr('firstindent', IntToStr(Para.FirstIndent));

  if (Para.LineSpacing <> 100) and (rvpiLineSpacing in Props) then
    Node.Attr.AddAttr('linespacing', IntToStr(Para.LineSpacing));

  if (Para.LineSpacingType<>rvlsPercent) and (rvpiLineSpacingType in Props) then
      Node.Attr.AddAttr('linespacingtype', GetLineSpacingTypeStr(Para));

  if (Para.LeftIndent <> 0) and (rvpiLeftIndent in Props) then
    Node.Attr.AddAttr('leftindent', IntToStr(Para.LeftIndent));

  if (rvpaoNoWrap in Para.Options) and (rvpiNoWrap in Props) then
    Node.Attr.AddAttr('wrap', '0');

  if (rvpaoReadOnly in Para.Options) and (rvpiReadOnly in Props) then
    Node.Attr.AddAttr('readonly', '1');

  if (rvpaoStyleProtect in Para.Options) and (rvpiStyleProtect in Props) then
    Node.Attr.AddAttr('styleprotect', '1');

  if (rvpaoDoNotWantReturns in Para.Options) and (rvpiDoNotWantReturns in Props) then
    Node.Attr.AddAttr('ignoreenter', '1');

  if (rvpaoKeepLinesTogether in Para.Options) and (rvpiKeepLinesTogether in Props) then
    Node.Attr.AddAttr('keeplinestogether', '1');

  if (rvpaoKeepWithNext in Para.Options) and (rvpiKeepWithNext in Props) then
    Node.Attr.AddAttr('keepwithnext', '1');

  if (Para.RightIndent <> 0) and (rvpiRightIndent in Props) then
    Node.Attr.AddAttr('rightindent', IntToStr(Para.RightIndent));

  if (Para.SpaceAfter <> 0) and (rvpiSpaceAfter in Props) then
    Node.Attr.AddAttr('spaceafter', IntToStr(Para.SpaceAfter));

  if (Para.SpaceBefore <> 0) and (rvpiSpaceBefore in Props) then
    Node.Attr.AddAttr('spacebefore', IntToStr(Para.SpaceBefore));

  if (Para.OutlineLevel <> 0) and (rvpiOutlineLevel in Props) then
    Node.Attr.AddAttr('outlinelevel', IntToStr(Para.OutlineLevel));

  SaveBorderToXML(Node, Para.Border, Props, 0, 0, rvbNone);
  SaveBackgroundRectToXML(Node, Para.Background, Props, 0, 0, clNone);

  {$IFNDEF RVDONOTUSETABS}
  if rvpiTabs in Props then
    for i := 0 to Para.Tabs.Count-1 do
    begin
      T := Node.Items.AddTag('tab', Node, Node.Root);
      T.TagCodePage := RVStyle.DefCodePage;
      T.ValueCodePage := T.TagCodePage;
      T.Attr.AddAttr('position', IntToStr(Para.Tabs[i].Position));
      case Para.Tabs[i].Align of
        rvtaRight:
          T.Attr.AddAttr('align', 'right');
        rvtaCenter:
          T.Attr.AddAttr('align', 'center');
      end;
      if Para.Tabs[i].Leader<>'' then
        T.Attr.AddAttr('leader', Para.Tabs[i].Leader);
    end;
  {$ENDIF}
end;

{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure SaveStyleTemplateToXML(Node: TXMLTag; ST: TRVStyleTemplate;
  RVStyle: TRVStyle);

  function GetKindStr(ST: TRVStyleTemplate): String;
  begin
    case ST.Kind of
      rvstkParaText: Result := 'paratext';
      rvstkPara: Result := 'para';
      else Result := 'text';
    end;
  end;

var T: TXMLTag;
begin
  T := Node.Items.AddTag('font_props', Node, Node.Root);
  T.TagCodePage := RVStyle.DefCodePage;
  T.ValueCodePage := T.TagCodePage;
  SaveFontProps(T, ST.ValidTextProperties);

  T := Node.Items.AddTag('font', Node, Node.Root);
  T.TagCodePage := RVStyle.DefCodePage;
  T.ValueCodePage := T.TagCodePage;
  SaveCustomFontStyleToXML(T, ST.TextStyle, False, False,
    RVStyle, ST.ValidTextProperties);

  if ST.Kind<>rvstkText then begin
    T := Node.Items.AddTag('para_props', Node, Node.Root);
    T.TagCodePage := RVStyle.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveParaProps(T, ST.ValidParaProperties);

    T := Node.Items.AddTag('para', Node, Node.Root);
    T.TagCodePage := RVStyle.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveCustomParaStyleToXML(T, ST.ParaStyle, False, False,
      RVStyle, ST.ValidParaProperties);

    if ST.Next<>nil then
      Node.Attr.AddAttr('next', ST.Next.Name);
  end;
  Node.Attr.AddAttr('name', ST.Name);
  if ST.Parent<>nil then
    Node.Attr.AddAttr('parent', ST.Parent.Name);

  Node.Attr.AddAttr('kind', GetKindStr(ST));
  if ST.QuickAccess then
    Node.Attr.AddAttr('quickaccess', '1');

end;
{$ENDIF}

procedure SaveParaStyleToXML(Node: TXMLTag; Para: TParaInfo;
  SaveStyleName {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates{$ENDIF}: Boolean;
  RVStyle: TRVStyle;
  Props: TRVParaInfoProperties);
begin
  SaveCustomParaStyleToXML(Node, Para, SaveStyleName,
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  UseStyleTemplates,
  {$ENDIF}
  RVStyle, Props);

  if Para.NextParaNo <> -1 then
    Node.Attr.AddAttr('nextparano', IntToStr(Para.NextParaNo));
end;

procedure SaveMarkerToXML(Node: TXMLTag; Item: TRVListLevel; RVStyle: TRVStyle;
  SaveImgToFiles: Boolean);

  procedure SaveMarkerFont;
  var
    S: string;
  begin
    with Node.Attr, Item.Font do
    begin

      if Charset <> DEFAULT_CHARSET then begin
        if CharsetToIdent(Charset, S) then
          AddAttr('fontcharset', S);
        Node.TagCodePage := RVU_Charset2CodePage(Charset);
      end;

      if Color <> clWindowText then
        AddAttr('fontcolor', ColorToString(Color));

      if Name <> 'Arial' then
        AddAttr('fontname', Name);

      if Size <> 8 then
        AddAttr('fontsize', IntToStr(Size));

      if fsBold in Style then
        AddAttr('fontbold', '1');

      if fsItalic in Style then
        AddAttr('fontitalic', '1');

      if fsUnderline in Style then
        AddAttr('fontunderline', '1');

      if fsStrikeout in Style then
        AddAttr('fontstrikeout', '1');

    end;
  end;

  procedure SaveDefProps;
  var Stream: TRVMemoryStream;
  begin
    with Node.Attr do
    begin
      if Item.FormatString <> '' then
        AddAttr('formatstr', Item.FormatString);

      if Item.FormatStringW <> '' then begin
        Stream := TRVMemoryStream.Create;
        Stream.WriteBuffer(Pointer(Item.FormatStringW)^, Length(Item.FormatStringW)*2);
        Stream.Position := 0;
        AddAttr('formatstrw', String(RVFStream2TextString(Stream)));
        Stream.Free;
      end;

      if Item.LeftIndent <> 0 then
        AddAttr('leftindent', IntToStr(Item.LeftIndent));

      if Item.FirstIndent <> 10 then
        AddAttr('firstindent', IntToStr(Item.FirstIndent));

      if Item.MarkerIndent <> 0 then
        AddAttr('markerindent', IntToStr(Item.MarkerIndent));

      if Item.MarkerAlignment <> rvmaLeft then
        if Item.MarkerAlignment = rvmaRight then
          AddAttr('markeralign', 'right')
        else
          AddAttr('markeralign', 'center');

      if not (rvloContinuous in Item.Options) then
        AddAttr('continuous', '0');

      if not (rvloLevelReset in Item.Options) then
        AddAttr('levelreset', '0');

      if rvloLegalStyleNumbering in Item.Options then
        AddAttr('legalstylenumbering', '1'); 
    end;
  end;

  procedure SavePicture(Gr: TGraphic);
  var
    Stream: TRVMemoryStream;
    FileName: string;
  begin
    if SaveImgToFiles then
    begin
      FileName := GetImageFileName(Node.Root.BaseFilePath, 'marker', GraphicExtension(TGraphicClass(Gr.ClassType)));
      Gr.SaveToFile(FileName);
      Node.Attr.AddAttr('imgsource', ExtractFileName(FileName));
    end else
    begin
      Stream := TRVMemoryStream.Create;
      try
        Gr.SaveToStream(Stream);
        Node.Attr.AddAttr('class', Gr.ClassName);
        Node.Attr.AddAttr('img', String(RVFStream2TextString(Stream)));
      finally
        Stream.Free;
      end;
    end;
  end;

  procedure SaveImgList;
  var
    B: TBitmap;
    N: TXMLTag;
    I: Integer;
    S: TRVMemoryStream;
  begin
    for I := Item.ImageIndex to Item.ImageList.Count - 1 do
    begin
      N := Node.Items.AddTag('img', Node, Node.Root);
      N.TagCodePage := RVStyle.DefCodePage;
      N.ValueCodePage := N.TagCodePage;
      B := TBitmap.Create;      
      try
        Item.ImageList.GetBitmap(I, B);
        if SaveImgToFiles then
          N.Attr.AddAttr('source', GetImageFileName(Node.Root.BaseFilePath, 'markeril',
            GraphicExtension(TBitmap)))
        else
        begin
          S := TRVMemoryStream.Create;
          B.SaveToStream(S);
          S.Position := 0;
          try
            N.Value := RVFStream2TextString(S);
          finally
            S.Free;
          end;
        end;
      finally
        B.Free;
      end;
    end;
  end;

begin
  with Node.Attr do
    case Item.ListType of

      rvlstBullet: begin
        AddAttr('type', 'bullet');
        SaveDefProps;
        SaveMarkerFont;
      end;

      rvlstUnicodeBullet: begin
        AddAttr('type', 'unibullet');
        SaveDefProps;
        SaveMarkerFont;
      end;

      rvlstPicture: begin
        AddAttr('type', 'picture');
        SaveDefProps;
        if Item.HasPicture then
          SavePicture(Item.Picture.Graphic);
      end;

      rvlstImageList: begin
        AddAttr('type', 'imglist');
        SaveDefProps;
        SaveImgList;
      end;

      rvlstImageListCounter: begin
        AddAttr('type', 'imglistcounter');
        SaveDefProps;
        SaveImgList;
      end;

      rvlstDecimal: begin
        AddAttr('type', 'decimal');
        SaveDefProps;
        SaveMarkerFont;
      end;

      rvlstLowerAlpha: begin
        AddAttr('type', 'loweralpha');
        SaveDefProps;
        SaveMarkerFont;
      end;

      rvlstUpperAlpha: begin
        AddAttr('type', 'upperalpha');
        SaveDefProps;
        SaveMarkerFont;
      end;

      rvlstLowerRoman: begin
        AddAttr('type', 'lowerroman');
        SaveDefProps;
        SaveMarkerFont;
      end;

      rvlstUpperRoman: begin
        AddAttr('type', 'upperroman');
        SaveDefProps;
        SaveMarkerFont;
      end;

    end;
end;

procedure SaveListToXML(Node: TXMLTag; Item: TRVListInfo; RVStyle: TRVStyle;
  SaveStyleNames: Boolean; SaveImgToFiles: Boolean);
var
  I: Integer;
  LNode: TXMLTag;
begin
  with Node.Attr do
  begin
    if not Item.Standard then
      AddAttr('standard', '0');

    if SaveStyleNames then
      if Item.StyleName <> 'List Style' then
        AddAttr('name', Item.StyleName);

    for I := 0 to Item.Levels.Count - 1 do begin
      LNode := Node.Items.AddTag('marker', Node, Node.Root);
      LNode.TagCodePage := RVStyle.DefCodePage;
      LNode.ValueCodePage := LNode.TagCodePage;
      SaveMarkerToXML(LNode, Item.Levels[I], RVStyle, SaveImgToFiles);
    end;
  end;
end;

procedure SaveRVStyle(Node: TXMLTag; RVStyle: TRVStyle; SaveStyleNames: Boolean;
  SaveImgToFiles {$IFNDEF RVDONOTUSESTYLETEMPLATES}, UseStyleTemplates{$ENDIF}: Boolean);
var
  I: Integer;
  T: TXMLTag;
begin
  Node.Attr.AddAttr('inactiveselcolor', ColorToString(RVStyle.InactiveSelColor));
  Node.Attr.AddAttr('inactiveseltextcolor', ColorToString(RVStyle.InactiveSelTextColor));
  Node.Attr.AddAttr('selcolor', ColorToString(RVStyle.SelColor));
  Node.Attr.AddAttr('seltextcolor', ColorToString(RVStyle.SelTextColor));
  if RVStyle.Units=rvstuTwips then
    Node.Attr.AddAttr('units', 'twips');

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // StyleTemplates (if saved) must be saved before TextStyles and ParaStyles
  if UseStyleTemplates and (RVStyle.MainRVStyle=nil) then
    for I := 0 to RVStyle.StyleTemplates.Count - 1 do
    begin
      T := Node.Items.AddTag('styletemplate', Node, Node.Root);
      T.TagCodePage := RVStyle.DefCodePage;
      T.ValueCodePage := T.TagCodePage;
      SaveStyleTemplateToXML(T, RVStyle.StyleTemplates[I], RVStyle);
   end;
   {$ENDIF}

  for I := 0 to RVStyle.TextStyles.Count - 1 do
  begin
    T := Node.Items.AddTag('font', Node, Node.Root);
    T.TagCodePage := RVStyle.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveFontStyleToXML(T, RVStyle.TextStyles[I], SaveStyleNames,
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates,{$ENDIF}
      RVStyle, RVAllFontInfoProperties);
  end;

  for I := 0 to RVStyle.ParaStyles.Count - 1 do
  begin
    T := Node.Items.AddTag('para', Node, Node.Root);
    T.TagCodePage := RVStyle.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveParaStyleToXML(T, RVStyle.ParaStyles[I], SaveStyleNames,
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates,{$ENDIF}
      RVStyle, RVAllParaInfoProperties);
  end;

  for I := 0 to RVStyle.ListStyles.Count - 1 do
  begin
    T := Node.Items.AddTag('list', Node, Node.Root);
    T.TagCodePage := RVStyle.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveListToXML(T, RVStyle.ListStyles[I], RVStyle, SaveStyleNames, SaveImgToFiles);
  end;

end;

{$IFNDEF RVDONOTUSEDOCPARAMS}
function GetFloatStr(Value: Extended): String;
var p: Integer;
begin
  Result := FloatToStr(Value);
  if {$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator<>'.' then begin
    p := Pos({$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator, Result);
    if p<>0 then
      Result[p] := '.';
  end;
end;


procedure SaveDocParametersProc(Node: TXMLTag; DocParams: TRVDocParameters);
var S: String;
begin
  case DocParams.Units of
    rvuCentimeters: S := 'cm';
    rvuMillimeters: S := 'mm';
    rvuPicas: S := 'picas';
    rvuPixels: S := 'pixels';
    rvuPoints: S := 'points';
    else S := '';
  end;
  if S<>'' then
    Node.Attr.AddAttr('units', S);
  if DocParams.Orientation<>poPortrait then
    Node.Attr.AddAttr('orientation', 'landscape');
  Node.Attr.AddAttr('pageheight', GetFloatStr(DocParams.PageHeight));
  Node.Attr.AddAttr('pagewidth', GetFloatStr(DocParams.PageWidth));
  Node.Attr.AddAttr('leftmargin', GetFloatStr(DocParams.LeftMargin));
  Node.Attr.AddAttr('topmargin', GetFloatStr(DocParams.TopMargin));
  Node.Attr.AddAttr('rightmargin', GetFloatStr(DocParams.RightMargin));
  Node.Attr.AddAttr('bottommargin', GetFloatStr(DocParams.BottomMargin));
  Node.Attr.AddAttr('headery', GetFloatStr(DocParams.HeaderY));
  Node.Attr.AddAttr('footery', GetFloatStr(DocParams.FooterY));
  Node.Attr.AddAttr('mirrormargins', IntToStr(ord(DocParams.MirrorMargins)));
  if DocParams.ZoomPercent<>100 then
    Node.Attr.AddAttr('zoompercent', IntToStr(DocParams.ZoomPercent));
  case DocParams.ZoomMode of
    rvzmFullPage: S := 'fullpage';
    rvzmPageWidth: S := 'pagewidth';
    else S := '';
  end;
  if S<>'' then
    Node.Attr.AddAttr('zoommode', S);
  if DocParams.Author<>'' then
    Node.Attr.AddAttr('author', DocParams.Author);
  if DocParams.Title<>'' then
    Node.Attr.AddAttr('title', DocParams.Title);
  if DocParams.Comments<>'' then
    Node.Attr.AddAttr('comments', DocParams.Comments);
end;

{$ENDIF}

procedure SaveDocProperties(Node: TXMLTag; Props: TStrings);
var I: Integer;
    T: TXMLTag;
begin
  for I := 0 to Props.Count-1 do
  begin
    T := Node.Items.AddTag('item', Node, Node.Root);
    T.TagCodePage := Node.TagCodePage;
    T.ValueCodePage := Node.ValueCodePage;
    {$IFDEF RVUNICODESTR}
    T.Value := RVU_GetRawUnicode(Props.Strings[I]);
    T.UnicodeMode := xmleRawUnicode;
    {$ELSE}
    T.Value := Props.Strings[I];
    {$ENDIF}
  end;
end;


{---------------------------Save/Load Background----------------------------}

procedure LoadBackgroundFromXML(Node: TXMLTag; RichView: TCustomRichView);
var
  Stream: TRVMemoryStream;
  I: Integer;
  S: TRVAnsiString;
  SV: String;
begin
  for I := 0 to Node.Attr.Count - 1 do
  begin
    S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Attr[I].Name);

    if S = 'backstyle' then
    begin
      SV := LowerCase(Node.Attr[I].Value);
      if SV = 'color' then
        RichView.BackgroundStyle := bsNoBitmap
      else if SV = 'stretch' then
        RichView.BackgroundStyle := bsStretched
      else if SV = 'tile' then
        RichView.BackgroundStyle := bsTiledAndScrolled
      else if SV = 'fixedtile' then
        RichView.BackgroundStyle := bsTiled
      else if SV = 'center' then
        RichView.BackgroundStyle := bsCentered;
    end

    else if S = 'backcolor' then
      RichView.Color := StringToColor(Node.Attr[I].Value)

    else if S = 'backimg' then
    begin
      Stream := TRVMemoryStream.Create;
      try
        RVFTextString2Stream(TRVAnsiString(Node.Attr[I].Value), Stream);
        Stream.Position := 0;
        RichView.BackgroundBitmap.LoadFromStream(Stream);
      finally
        Stream.Free;
      end;
    end

    else if S = 'backimgsource' then
      RichView.BackgroundBitmap.LoadFromFile(Node.Root.BaseFilePath + Node.Attr[I].Value)

    {
    else if S = 'hscrollbarvisible' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.HScrollVisible := True
      else if Node.Attr[I].Value = '0' then
        RichView.HScrollVisible := False;
    end

    else if S = 'vscrollbarvisible' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.VScrollVisible := True
      else if Node.Attr[I].Value = '0' then
        RichView.VScrollVisible := False;
    end
    }

    else if S = 'vsmallstep' then
      RichView.VSmallStep := StrToIntDef(Node.Attr[I].Value, RichView.VSmallStep)

    else if S = 'maxtextwidth' then
      RichView.MaxTextWidth := StrToIntDef(Node.Attr[I].Value, RichView.MaxTextWidth)

    else if S = 'mintextwidth' then
      RichView.MinTextWidth := StrToIntDef(Node.Attr[I].Value, RichView.MinTextWidth)

    else if S = 'leftmargin' then
      RichView.LeftMargin := StrToIntDef(Node.Attr[I].Value, RichView.LeftMargin)

    else if S = 'rightmargin' then
      RichView.RightMargin := StrToIntDef(Node.Attr[I].Value, RichView.RightMargin)

    else if S = 'topmargin' then
      RichView.TopMargin := StrToIntDef(Node.Attr[I].Value, RichView.TopMargin)

    else if S = 'bottommargin' then
      RichView.BottomMargin := StrToIntDef(Node.Attr[I].Value, RichView.BottomMargin)

    else if S = 'scrolltoend' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoScrollToEnd]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoScrollToEnd]
    end

    else if S = 'singleclick' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoSingleClick]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoSingleClick]
    end

    else if S = 'clienttextwidth' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoClientTextWidth]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoClientTextWidth]
    end

    else if S = 'showcheckpoints' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoShowCheckpoints]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoShowCheckpoints]
    end

    else if S = 'showpagebreaks' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoShowPageBreaks]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoShowPageBreaks]
    end

    else if S = 'autocopytext' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoAutoCopyText]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoAutoCopyText]
    end

    else if S = 'autocopyunitext' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoAutoCopyUnicodeText]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoAutoCopyUnicodeText]
    end

    else if S = 'autocopyrtf' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoAutoCopyRTF]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoAutoCopyRTF]
    end

    else if S = 'autocopyrvf' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoAutoCopyRVF]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoAutoCopyRVF]
    end

    else if S = 'autocopyimage' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoAutoCopyImage]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoAutoCopyImage]
    end

    else if S = 'formatinvalidate' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoFormatInvalidate]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoFormatInvalidate]
    end

    else if S = 'dblclickselectword' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoDblClickSelectsWord]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoDblClickSelectsWord]
    end

    else if S = 'rightclickdeselect' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.Options := RichView.Options + [rvoRClickDeselects]
      else if Node.Attr[I].Value = '0' then
        RichView.Options := RichView.Options - [rvoRClickDeselects]
    end

    else if S = 'checkevent' then
    begin
      SV := LowerCase(Node.Attr[I].Value);
      if SV = 'none' then
        RichView.CPEventKind := cpeNone
      else if SV = 'sectionstart' then
        RichView.CPEventKind := cpeAsSectionStart
      else if SV = 'visible' then
        RichView.CPEventKind := cpeWhenVisible;
    end

    else if S = 'bidimode' then
    begin
      SV := LowerCase(Node.Attr[I].Value);
      if SV = 'none' then
        RichView.BiDiMode := rvbdUnspecified
      else if SV = 'ltr' then
        RichView.BiDiMode := rvbdLeftToRight
      else if SV = 'rtl' then
        RichView.BiDiMode := rvbdRightToLeft;
    end

    else if S = 'rtfsavestylesheet' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RTFOptions := RichView.RTFOptions + [rvrtfSaveStyleSheet]
      else if Node.Attr[I].Value = '0' then
        RichView.RTFOptions := RichView.RTFOptions - [rvrtfSaveStyleSheet]
    end

    else if S = 'rtfunicodetoansi' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RTFOptions := RichView.RTFOptions + [rvrtfDuplicateUnicode]
      else if Node.Attr[I].Value = '0' then
        RichView.RTFOptions := RichView.RTFOptions - [rvrtfDuplicateUnicode]
    end

    else if S = 'rtfemftowmf' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RTFOptions := RichView.RTFOptions + [rvrtfSaveEMFAsWMF]
      else if Node.Attr[I].Value = '0' then
        RichView.RTFOptions := RichView.RTFOptions - [rvrtfSaveEMFAsWMF]
    end

    else if S = 'rtftransformjpeg' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RTFOptions := RichView.RTFOptions - [rvrtfSaveJpegAsJpeg]
      else if Node.Attr[I].Value = '0' then
        RichView.RTFOptions := RichView.RTFOptions + [rvrtfSaveJpegAsJpeg]
    end

    else if S = 'rtfdefaultimgformat' then
    begin
      SV := LowerCase(Node.Attr[I].Value);
      if SV = 'bmp' then
        RichView.RTFOptions := RichView.RTFOptions + [rvrtfSaveBitmapDefault]
      else if SV = 'wmf' then
        RichView.RTFOptions := RichView.RTFOptions - [rvrtfSaveBitmapDefault]
      else if SV = 'emf' then
      begin
        RichView.RTFOptions := RichView.RTFOptions - [rvrtfSaveBitmapDefault];
        RichView.RTFOptions := RichView.RTFOptions + [rvrtfSaveEMFDefault];
      end;
    end

    else if S = 'rvfautosaveimages' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSavePicturesBody]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSavePicturesBody]
    end

    else if S = 'rvfautosavecontrols' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSaveControlsBody]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSaveControlsBody]
    end

    else if S = 'rvfusestylenames' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoUseStyleNames]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoUseStyleNames]
    end

    else if S = 'rvfsavetextstyles' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSaveTextStyles]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSaveTextStyles]
    end

    else if S = 'rvfsaveparastyles' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSaveParaStyles]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSaveParaStyles]
    end

    else if S = 'rvfsavebinary' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSaveBinary]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSaveBinary]
    end

    else if S = 'rvfsavebackground' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSaveBack]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSaveBack]
    end

    else if S = 'rvfsavelayout' then
    begin
      if Node.Attr[I].Value = '1' then
        RichView.RVFOptions := RichView.RVFOptions + [rvfoSaveLayout]
      else if Node.Attr[I].Value = '0' then
        RichView.RVFOptions := RichView.RVFOptions - [rvfoSaveLayout];
    end;

    if not (RichView is TCustomRichViewEdit) then
    begin

      if S = 'tabnavigation' then
      begin
         SV := LowerCase(Node.Attr[I].Value);
        if SV = 'none' then
          RichView.TabNavigation := rvtnNone
        else if SV = 'tab' then
          RichView.TabNavigation := rvtnTab
        else if SV = 'ctrltab' then
          RichView.TabNavigation := rvtnCtrlTab;
      end

      else if S = 'allowselection' then
      begin
        if Node.Attr[I].Value = '1' then
          RichView.Options := RichView.Options + [rvoAllowSelection]
        else if Node.Attr[I].Value = '0' then
          RichView.Options := RichView.Options - [rvoAllowSelection]
      end;

    end;

    if RichView is TCustomRichViewEdit then
    begin

      if S = 'cleartagbystylechange' then
      begin
        if Node.Attr[I].Value = '1' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions +
            [rvoClearTagOnStyleApp]
        else if Node.Attr[I].Value = '0' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions -
            [rvoClearTagOnStyleApp]
      end

      else if S = 'ctrljump' then
      begin
        if Node.Attr[I].Value = '1' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions +
            [rvoCtrlJumps]
        else if Node.Attr[I].Value = '0' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions -
            [rvoCtrlJumps]
      end

      else if S = 'ignoreenter' then
      begin
        if Node.Attr[I].Value = '1' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions +
            [rvoDoNotWantReturns]
        else if Node.Attr[I].Value = '0' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions -
            [rvoDoNotWantReturns]
      end

      else if S = 'ignoretab' then
      begin
        if Node.Attr[I].Value = '1' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions -
            [rvoWantTabs]
        else if Node.Attr[I].Value = '0' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions +
            [rvoWantTabs]
      end

      else if S = 'langautoswitch' then
      begin
        if Node.Attr[I].Value = '1' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions +
            [rvoAutoSwitchLang]
        else if Node.Attr[I].Value = '0' then
          TCustomRichViewEdit(RichView).EditorOptions := TCustomRichViewEdit(RichView).EditorOptions -
            [rvoAutoSwitchLang]
      end;

    end;
  end;
end;

procedure SaveBackgroundToXML(Node: TXMLTag; RichView: TCustomRichView; SaveImgToFile: Boolean);
var
  Stream: TRVMemoryStream;
begin

  if RichView.BackgroundStyle = bsNoBitmap then
    Node.Attr.AddAttr('backstyle', 'color')
  else if RichView.BackgroundStyle = bsStretched then
    Node.Attr.AddAttr('backstyle', 'stretch')
  else if RichView.BackgroundStyle = bsTiledAndScrolled then
    Node.Attr.AddAttr('backstyle', 'tile')
  else if RichView.BackgroundStyle = bsTiled then
    Node.Attr.AddAttr('backstyle', 'fixedtile')
  else if RichView.BackgroundStyle = bsCentered then
    Node.Attr.AddAttr('backstyle', 'center');

  if ((RichView.BackgroundStyle = bsNoBitmap) or (RichView.BackgroundStyle = bsCentered) or
    RichView.BackgroundBitmap.Empty) then
      Node.Attr.AddAttr('backcolor', ColorToString(RichView.Color));

  if (RichView.BackgroundStyle <> bsNoBitmap) and (not RichView.BackgroundBitmap.Empty) then
  begin
  
    if SaveImgToFile then
    begin
      RichView.BackgroundBitmap.SaveToFile(Node.Root.BaseFilePath + 'backimg.bmp');
      Node.Attr.AddAttr('backimgsource', 'backimg.bmp');
    end else begin
      Stream := TRVMemoryStream.Create;
      try
        RichView.BackgroundBitmap.SaveToStream(Stream);
        Stream.Position := 0;
        Node.Attr.AddAttr('backimg', String(RVFStream2TextString(Stream)));
      finally
        Stream.Free;
      end;
    end;
    
  end;
  {
  if RichView.HScrollVisible then
    Node.Attr.AddAttr('hscrollbarvisible', '1')
  else
    Node.Attr.AddAttr('hscrollbarvisible', '0');

  if RichView.VScrollVisible then
    Node.Attr.AddAttr('vscrollbarvisible', '1')
  else
    Node.Attr.AddAttr('vscrollbarvisible', '0');
  }

  Node.Attr.AddAttr('vsmallstep', IntToStr(RichView.VSmallStep));

  Node.Attr.AddAttr('maxtextwidth', IntToStr(RichView.MaxTextWidth));

  Node.Attr.AddAttr('mintextwidth', IntToStr(RichView.MinTextWidth));

  Node.Attr.AddAttr('leftmargin', IntToStr(RichView.LeftMargin));

  Node.Attr.AddAttr('rightmargin', IntToStr(RichView.RightMargin));

  Node.Attr.AddAttr('topmargin', IntToStr(RichView.TopMargin));

  Node.Attr.AddAttr('bottommargin', IntToStr(RichView.BottomMargin));

  if rvoScrollToEnd in RichView.Options then
    Node.Attr.AddAttr('scrolltoend', '1')
  else
    Node.Attr.AddAttr('scrolltoend', '0');

  if rvoSingleClick in RichView.Options then
    Node.Attr.AddAttr('singleclick', '1')
  else
    Node.Attr.AddAttr('singleclick', '0');

  if rvoClientTextWidth in RichView.Options then
    Node.Attr.AddAttr('clienttextwidth', '1')
  else
    Node.Attr.AddAttr('clienttextwidth', '0');

  if rvoShowCheckpoints in RichView.Options then
    Node.Attr.AddAttr('showcheckpoints', '1')
  else
    Node.Attr.AddAttr('showcheckpoints', '0');

  if rvoShowPageBreaks in RichView.Options then
    Node.Attr.AddAttr('showpagebreaks', '1')
  else
    Node.Attr.AddAttr('showpagebreaks', '0');

  if rvoAutoCopyText in RichView.Options then
    Node.Attr.AddAttr('autocopytext', '1')
  else
    Node.Attr.AddAttr('autocopytext', '0');

  if rvoAutoCopyUnicodeText in RichView.Options then
    Node.Attr.AddAttr('autocopyunitext', '1')
  else
    Node.Attr.AddAttr('autocopyunitext', '0');

  if rvoAutoCopyRTF in RichView.Options then
    Node.Attr.AddAttr('autocopyrtf', '1')
  else
    Node.Attr.AddAttr('autocopyrtf', '0');

  if rvoAutoCopyRVF in RichView.Options then
    Node.Attr.AddAttr('autocopyrvf', '1')
  else
    Node.Attr.AddAttr('autocopyrvf', '0');

  if rvoAutoCopyImage in RichView.Options then
    Node.Attr.AddAttr('autocopyimage', '1')
  else
    Node.Attr.AddAttr('autocopyimage', '0');

  if rvoFormatInvalidate in RichView.Options then
    Node.Attr.AddAttr('formatinvalidate', '1')
  else
    Node.Attr.AddAttr('formatinvalidate', '0');

  if rvoDblClickSelectsWord in RichView.Options then
    Node.Attr.AddAttr('dblclickselectword', '1')
  else
    Node.Attr.AddAttr('dblclickselectword', '0');

  if rvoRClickDeselects in RichView.Options then
    Node.Attr.AddAttr('rightclickdeselect', '1')
  else
    Node.Attr.AddAttr('rightclickdeselect', '0');

  if RichView.CPEventKind = cpeNone then
    Node.Attr.AddAttr('checkevent', 'none')
  else if RichView.CPEventKind = cpeAsSectionStart then
    Node.Attr.AddAttr('checkevent', 'sectionstart')
  else if RichView.CPEventKind = cpeWhenVisible then
    Node.Attr.AddAttr('checkevent', 'visible');

  if RichView.BiDiMode = rvbdUnspecified then
    Node.Attr.AddAttr('bidimode', 'none')
  else if RichView.BiDiMode = rvbdLeftToRight then
    Node.Attr.AddAttr('bidimode', 'ltr')
  else if RichView.BiDiMode = rvbdRightToLeft then
    Node.Attr.AddAttr('bidimode', 'rtl');

  if rvrtfSaveStyleSheet in RichView.RTFOptions then
    Node.Attr.AddAttr('rtfsavestylesheet', '1')
  else
    Node.Attr.AddAttr('rtfsavestylesheet', '0');

  if rvrtfDuplicateUnicode in RichView.RTFOptions then
    Node.Attr.AddAttr('rtfunicodetoansi', '1')
  else
    Node.Attr.AddAttr('rtfunicodetoansi', '0');

  if rvrtfSaveEMFAsWMF in RichView.RTFOptions then
    Node.Attr.AddAttr('rtfemftowmf', '1')
  else
    Node.Attr.AddAttr('rtfemftowmf', '0');

  if rvrtfSaveJpegAsJpeg in RichView.RTFOptions then
    Node.Attr.AddAttr('rtftransformjpeg', '0')
  else
    Node.Attr.AddAttr('rtftransformjpeg', '1');

  if rvrtfSaveBitmapDefault in RichView.RTFOptions then
    Node.Attr.AddAttr('rtfdefaultimgformat', 'bmp')
  else if rvrtfSaveEMFDefault in RichView.RTFOptions then
    Node.Attr.AddAttr('rtfdefaultimgformat', 'emf')
  else
    Node.Attr.AddAttr('rtfdefaultimgformat', 'wmf');

  if rvfoSavePicturesBody in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfautosaveimages', '1')
  else
    Node.Attr.AddAttr('rvfautosaveimages', '0');

  if rvfoSaveControlsBody in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfautosavecontrols', '1')
  else
    Node.Attr.AddAttr('rvfautosavecontrols', '0');

  if rvfoUseStyleNames in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfusestylenames', '1')
  else
    Node.Attr.AddAttr('rvfusestylenames', '0');

  if rvfoSaveTextStyles in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfsavetextstyles', '1')
  else
    Node.Attr.AddAttr('rvfsavetextstyles', '0');

  if rvfoSaveParaStyles in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfsaveparastyles', '1')
  else
    Node.Attr.AddAttr('rvfsaveparastyles', '0');

  if rvfoSaveBinary in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfsavebinary', '1')
  else
    Node.Attr.AddAttr('rvfsavebinary', '0');

  if rvfoSaveBack in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfsavebackground', '1')
  else
    Node.Attr.AddAttr('rvfsavebackground', '0');

  if rvfoSaveLayout in RichView.RVFOptions then
    Node.Attr.AddAttr('rvfsavelayout', '1')
  else
    Node.Attr.AddAttr('rvfsavelayout', '0');

  if not (RichView is TCustomRichViewEdit) then
  begin

    if RichView.TabNavigation = rvtnNone then
      Node.Attr.AddAttr('tabnavigation', 'none')
    else if RichView.TabNavigation = rvtnTab then
      Node.Attr.AddAttr('tabnavigation', 'tab')
    else if RichView.TabNavigation = rvtnCtrlTab then
      Node.Attr.AddAttr('tabnavigation', 'ctrltab');

    if rvoAllowSelection in RichView.Options then
      Node.Attr.AddAttr('allowselection', '1')
    else
      Node.Attr.AddAttr('allowselection', '0');

  end;

  if RichView is TCustomRichViewEdit then
  begin

    if rvoClearTagOnStyleApp in TCustomRichViewEdit(RichView).EditorOptions then
      Node.Attr.AddAttr('cleartagbystylechange', '1')
    else
      Node.Attr.AddAttr('cleartagbystylechange', '0');

    if rvoCtrlJumps in TCustomRichViewEdit(RichView).EditorOptions then
      Node.Attr.AddAttr('ctrljump', '1')
    else
      Node.Attr.AddAttr('ctrljump', '0');

    if rvoDoNotWantReturns in TCustomRichViewEdit(RichView).EditorOptions then
      Node.Attr.AddAttr('ignoreenter', '1')
    else
      Node.Attr.AddAttr('ignoreenter', '0');

    if rvoWantTabs in TCustomRichViewEdit(RichView).EditorOptions then
      Node.Attr.AddAttr('ignoretab', '0')
    else
      Node.Attr.AddAttr('ignoretab', '1');

    if rvoAutoSwitchLang in TCustomRichViewEdit(RichView).EditorOptions then
      Node.Attr.AddAttr('langautoswitch', '1')
    else
      Node.Attr.AddAttr('langautoswitch', '0');

  end;

end;

{$IFNDEF RVDONOTUSEDOCPARAMS}
function GetRVUnits(const Value: String): TRVUnits;
begin
  if Value='cm' then
    Result := rvuCentimeters
  else if Value='mm' then
    Result := rvuMillimeters
  else if Value='picas' then
    Result := rvuPicas
  else if Value='pixels' then
    Result := rvuPixels
  else if Value='points' then
    Result := rvuPoints
  else
    Result := rvuInches;  
end;

function GetZoomMode(const Value: String): TRVZoomMode;
begin
  if Value='fullpage' then
    Result := rvzmFullPage
  else if Value='pagewidth' then
    Result := rvzmPageWidth
  else
    Result := rvzmCustom;
end;

function GetFloat(S: String): Extended;
var p: Integer;
begin
  if {$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator<>'.' then begin
    p := Pos('.', S);
    if p<>0 then
      S[p] := {$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator;
  end;
  Result := StrToFloat(S);
end;

procedure LoadDocParametersProc(Node: TXMLTag; DocParams: TRVDocParameters);
var
  I: Integer;
  S: TRVAnsiString;
  SV: String;
begin
  DocParams.Reset;
  if Node=nil then
    exit;
  for I := 0 to Node.Attr.Count - 1 do
  begin
    S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Attr[I].Name);
    SV := Node.Attr[I].Value;
    if S='units' then
      DocParams.Units := GetRVUnits(SV)
    else if S='orientation' then
      if SV='landscape' then
        DocParams.Orientation := poLandscape
      else
        DocParams.Orientation := poPortrait
    else if S='pageheight' then
      DocParams.PageHeight := GetFloat(SV)
    else if S='pagewidth' then
      DocParams.PageWidth := GetFloat(SV)
    else if S='leftmargin' then
      DocParams.LeftMargin := GetFloat(SV)
    else if S='topmargin' then
      DocParams.TopMargin := GetFloat(SV)
    else if S='rightmargin' then
      DocParams.RightMargin := GetFloat(SV)
    else if S='bottommargin' then
      DocParams.BottomMargin := GetFloat(SV)
    else if S='headery' then
      DocParams.HeaderY := GetFloat(SV)
    else if S='footery' then
      DocParams.FooterY := GetFloat(SV)
    else if S='mirrormargins' then
      DocParams.MirrorMargins := SV<>'0'
    else if S='zoompercent' then
      DocParams.ZoomPercent := StrToInt(SV)
    else if S='zoommode' then
      DocParams.ZoomMode := GetZoomMode(SV)
    else if S='author' then
      DocParams.Author := SV
    else if S='title' then
      DocParams.Title := SV
    else if S='comments' then
      DocParams.Comments := SV
  end;
end;
{$ENDIF}

procedure LoadDocProperties(Node: TXMLTag; Props: TStrings);
var I: Integer;
begin
  Props.BeginUpdate;
  try
    Props.Clear;
    if Node<>nil then
      for I := 0 to Node.Items.Count-1 do
        Props.Add(Node.Items[i].GetValueString);
  finally
    Props.EndUpdate;
  end;
end;
{------------------------------Save/Load RVPrint----------------------------}

procedure LoadRVPrint(Node: TXMLTag; RVPrint: TRVPrint);
var
  I: Integer;
  S: TRVAnsiString;
begin
  for I := 0 to Node.Attr.Count - 1 do
  begin
    S := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Node.Attr[I].Name);

    if S = 'bottommargin' then
      RVPrint.BottomMarginMM := StrToIntDef(Node.Attr[I].Value, RVPrint.BottomMarginMM)

    else if S = 'leftmargin' then
      RVPrint.LeftMarginMM := StrToIntDef(Node.Attr[I].Value, RVPrint.LeftMarginMM)

    else if S = 'rightmargin' then
      RVPrint.RightMarginMM := StrToIntDef(Node.Attr[I].Value, RVPrint.RightMarginMM)

    else if S = 'topmargin' then
      RVPrint.TopMarginMM := StrToIntDef(Node.Attr[I].Value, RVPrint.TopMarginMM)

    else if S = 'headermarginy' then
      RVPrint.HeaderYMM := StrToIntDef(Node.Attr[I].Value, RVPrint.HeaderYMM)

    else if S = 'footermarginy' then
      RVPrint.FooterYMM := StrToIntDef(Node.Attr[I].Value, RVPrint.FooterYMM)

    else if S = 'mirrormargins' then
    begin
      if Node.Attr[I].Value = '1' then
        RVPrint.MirrorMargins := True
      else if Node.Attr[I].Value = '0' then
        RVPrint.MirrorMargins := False;
    end

    else if S = 'previewcorrect' then
    begin
      if Node.Attr[I].Value = '1' then
        RVPrint.PreviewCorrection := True
      else if Node.Attr[I].Value = '0' then
        RVPrint.PreviewCorrection := False;
    end

    else if S = 'printback' then
    begin
      if Node.Attr[I].Value = '1' then
        RVPrint.TransparentBackground := False
      else if Node.Attr[I].Value = '0' then
        RVPrint.TransparentBackground := True;
    end;

  end;
end;

procedure SaveRVPrint(Node: TXMLTag; RVPrint: TRVPrint);
begin
  Node.Attr.AddAttr('bottommargin', IntToStr(RVPrint.BottomMarginMM));
  Node.Attr.AddAttr('leftmargin', IntToStr(RVPrint.LeftMarginMM));
  Node.Attr.AddAttr('rightmargin', IntToStr(RVPrint.RightMarginMM));
  Node.Attr.AddAttr('topmargin', IntToStr(RVPrint.TopMarginMM));
  Node.Attr.AddAttr('headermarginy', IntToStr(RVPrint.HeaderYMM));
  Node.Attr.AddAttr('footermarginy', IntToStr(RVPrint.FooterYMM));

  if RVPrint.MirrorMargins then
    Node.Attr.AddAttr('mirrormargins', '1')
  else
    Node.Attr.AddAttr('mirrormargins', '0');

  if RVPrint.PreviewCorrection then
    Node.Attr.AddAttr('previewcorrect', '1')
  else
    Node.Attr.AddAttr('previewcorrect', '0');

  if RVPrint.TransparentBackground then
    Node.Attr.AddAttr('printback', '0')
  else
    Node.Attr.AddAttr('printback', '1');
end;

{---------------------------------------------------------------------------}

function RVLoadFromXML(RichView, RVHeader, RVFooter: TCustomRichView; RVPrint: TRVPrint;
  LoadPrint, LoadDocParameters: Boolean;
  Node: TXMLTag; var InsertPosition: Integer;
  LoadBackground: Boolean; StyleLoadingMode: TStyleLoadingMode;
  OnUnknownTag: TRVXMLUnknownTagEvent;
  OnLoadControl: TRVXMLLoadControlEvent;
  OnAfterLoadControl: TRVXMLAfterLoadControlEvent;
  EditMode: Boolean; var FullReformat: Boolean;
  var NonFirstItemsAdded: Integer;
  LoadTagMode, CanChangeUnits: Boolean
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; ReadStyleTemplates: TRVStyleTemplateCollection
  {$ENDIF}): Boolean;
var
  Bullets: TImgListList;
  T, T1: TXMLTag;
  {$IFDEF RVOLDTAGS}
  A: TXMLAttr;
  {$ENDIF}
  StyleMap, ParaMap, ListMap: TRVIntegerList;
  FirstTime: Boolean;
  XMLUnits: TRVStyleUnits;
  v1, v2: Integer;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  StyleTemplates: TRVStyleTemplateCollection;
  {$ENDIF}
begin
  NonFirstItemsAdded := 0;
  Result := False;
  StyleMap := TRVIntegerList.Create;
  ParaMap := TRVIntegerList.Create;
  ListMap := TRVIntegerList.Create;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if ReadStyleTemplates<>nil then
    StyleTemplates := ReadStyleTemplates
  else if RichView.UseStyleTemplates and
    (RichView.StyleTemplateInsertMode<>rvstimIgnoreSourceStyleTemplates) and
    (RichView.Style.MainRVStyle=nil) then
    StyleTemplates := TRVStyleTemplateCollection.Create(nil)
  else
    StyleTemplates := nil;
  {$ENDIF}
  try
    T := Node.Items.FindTagOfName('style');
    XMLUnits := rvstuPixels;
    LoadRVStyle(T, RichView.Style, StyleLoadingMode, StyleMap, ParaMap, ListMap,
      CanChangeUnits and not EditMode and (StyleLoadingMode=slmReplace),
      XMLUnits
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      , RichView.UseStyleTemplates, RichView.StyleTemplateInsertMode, StyleTemplates
      {$ENDIF});

    T := Node.Items.FindTagOfName('bullets');
    Bullets := TImgListList.Create;
    try
      if T <> nil then
        Bullets.LoadFromXML(T);

      if LoadPrint and (RVPrint <> nil) then
      begin
        T := Node.Items.FindTagOfName('print');
        if T <> nil then
          LoadRVPrint(T, RVPrint);
      end;

      {$IFNDEF RVDONOTUSEDOCPARAMS}
      if LoadDocParameters and not EditMode then
      begin
        T := Node.Items.FindTagOfName('docparameters');
        LoadDocParametersProc(T, RichView.DocParameters);
      end;
      {$ENDIF}
      if LoadDocParameters and not EditMode then
      begin
        T := Node.Items.FindTagOfName('docproperties');
        LoadDocProperties(T, RichView.DocProperties);
      end;

      if not EditMode and ((RVHeader<>nil) or (RVFooter<>nil)) then begin
        T := Node.Items.FindTagOfName('subdocuments');
        if T<>nil then begin
          if (RVHeader<>nil) then begin
            T1 := T.Items.FindTagOfName('header');
            if T1<>nil then begin
              V1 := 0;
              V2 := 0;
              FullReformat := False;
              RVLoadFromXML(RVHeader, nil, nil, nil, False, False, T1, V1,
                False, StyleLoadingMode, OnUnknownTag,
                OnLoadControl, OnAfterLoadControl, False, FullReformat, V2,
                LoadTagMode, CanChangeUnits
                {$IFNDEF RVDONOTUSESTYLETEMPLATES}, StyleTemplates{$ENDIF});
            end;
          end;
          if (RVFooter<>nil) then begin
            T1 := T.Items.FindTagOfName('footer');
            if T1<>nil then begin
              V1 := 0;
              V2 := 0;
              FullReformat := False;
              RVLoadFromXML(RVFooter, nil, nil, nil, False, False, T1, V1,
                False, StyleLoadingMode, OnUnknownTag,
                OnLoadControl, OnAfterLoadControl, False, FullReformat, V2,
                LoadTagMode, CanChangeUnits
                {$IFNDEF RVDONOTUSESTYLETEMPLATES}, StyleTemplates{$ENDIF});
            end;
          end;
        end;
      end;

      T := Node.Items.FindTagOfName('document');
      if T = nil then begin
        Bullets.Free;
        Bullets := nil;
        Exit;
      end;
      {$IFDEF RVOLDTAGS}
      if LoadTagMode then
      begin
        A := T.Attr.FindAttrOfName('stringtags');
        if A <> nil then
        begin
          if A.Value = '1' then
            RichView.Options := RichView.Options + [rvoTagsArePChars]
          else if A.Value = '0' then
            RichView.Options := RichView.Options - [rvoTagsArePChars];
        end;
      end;
      {$ENDIF}
      if LoadBackground then
        LoadBackgroundFromXML(T, RichView);

      FirstTime := True;
      FullReformat := False;
      Result := LoadRVDataFromXML(RichView.RVData, T, Bullets, nil, InsertPosition, RichView.DoInPaletteMode, RichView.RVPalette,
        RichView.PRVLogPalette, nil, StyleMap, ParaMap, ListMap, OnUnknownTag,
        OnLoadControl, OnAfterLoadControl, RichView.Style, EditMode, FirstTime, FullReformat,
         NonFirstItemsAdded,
         {$IFDEF RVOLDTAGS}rvoTagsArePChars in RichView.Options{$ELSE}True{$ENDIF},
         XMLUnits);
    finally
      Bullets.Free;
    end;
  finally
    StyleMap.Free;
    ParaMap.Free;
    ListMap.Free;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    if StyleTemplates<>ReadStyleTemplates then begin
      if StyleTemplates<>nil then
        RichView.StyleTemplatesChange;
      StyleTemplates.Free;
    end;
    {$ENDIF}
  end;
end;

procedure RVSaveToXML(RichView, RVHeader, RVFooter: TCustomRichView; Node: TXMLTag;
  RVPrint: TRVPrint; SavePrint, SaveDocParameters: Boolean;
  Style: TRichViewXMLStyle; SaveStyles: Boolean;
  SaveBackground: Boolean; SaveImgToFiles: Boolean;
  SelStart, SelOffset, SelEnd, SelEndOffset: Integer;
  OnUnknownItem: TRVXMLUnknownItemEvent;
  OnSaveControl: TRVXMLSaveControlEvent;
  UseStyleNames: Boolean; SaveStyleNames, SaveLineBreaks: Boolean);
var
  Bullets: TImgListList;
  T,T1: TXMLTag;
begin
  if SaveStyles then
  begin
    T := Node.Items.AddTag('style', Node, Node.Root);
    T.TagCodePage := RichView.Style.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveRVStyle(T, RichView.Style, SaveStyleNames, SaveImgToFiles
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}, RichView.UseStyleTemplates{$ENDIF});
  end;

  {$IFNDEF RVDONOTUSEDOCPARAMS}
  if SaveDocParameters and RichView.DocParametersAssigned then
  begin
    T := Node.Items.AddTag('docparameters', Node, Node.Root);
    T.TagCodePage := RichView.Style.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveDocParametersProc(T, RichView.DocParameters);
  end;
  {$ENDIF}
  if SaveDocParameters and (RichView.DocProperties.Count>0) then
  begin
    T := Node.Items.AddTag('docproperties', Node, Node.Root);
    T.TagCodePage := RichView.Style.DefCodePage;
    T.ValueCodePage := T.TagCodePage;
    SaveDocProperties(T, RichView.DocProperties);
  end;


  Bullets := TImgListList.Create;
  try
    ScanBullets(RichView.RVData, nil, Bullets, SelStart, SelEnd);
    if Bullets.Count > 0 then
    begin
      T := Node.Items.AddTag('bullets', Node, Node.Root);
      T.TagCodePage := RichView.Style.DefCodePage;
      T.ValueCodePage := T.TagCodePage;
      Bullets.SaveToXML(T, SaveImgToFiles);
    end;

    if SavePrint and (RVPrint <> nil) then
    begin
      T := Node.Items.AddTag('print', Node, Node.Root);
      T.TagCodePage := RichView.Style.DefCodePage;
      T.ValueCodePage := T.TagCodePage;
      SaveRVPrint(T, RVPrint);
    end;

    if (RVHeader<>nil) or (RVFooter<>nil) then begin
      T := Node.Items.AddTag('subdocuments', Node, Node.Root);
      if RVHeader<>nil then begin
        T1 := T.Items.AddTag('header', T, T.Root);
        RVSaveToXML(RVHeader, nil, nil, T1, nil, False, False,
          Style, SaveStyles, False, SaveImgToFiles,
          0, -1, RVHeader.ItemCount-1, -1,
          OnUnknownItem, OnSaveControl, UseStyleNames,
          SaveStyleNames, SaveLineBreaks);
      end;
      if RVFooter<>nil then begin
        T1 := T.Items.AddTag('footer', T, T.Root);
        RVSaveToXML(RVFooter, nil, nil, T1, nil, False, False,
          Style, SaveStyles, False, SaveImgToFiles,
          0, -1, RVFooter.ItemCount-1, -1,
          OnUnknownItem, OnSaveControl, UseStyleNames,
          SaveStyleNames, SaveLineBreaks);
      end;
    end;      

    T := Node.Items.AddTag('document', Node, Node.Root);
    T.TagCodePage := RichView.Style.DefCodePage;
    T.ValueCodePage := T.TagCodePage;

    {$IFDEF RVOLDTAGS}
    if rvoTagsArePChars in RichView.Options then
      T.Attr.AddAttr('stringtags', '1')
    else
      T.Attr.AddAttr('stringtags', '0');
    {$ELSE}
    T.Attr.AddAttr('stringtags', '1');
    {$ENDIF}

    if SaveBackground then
      SaveBackgroundToXML(T, RichView, SaveImgToFiles);

    SaveRVDataToXML(RichView.RVData, T, Style, True, RichView.Style, SaveImgToFiles, Bullets,
      SelStart, SelOffset, SelEnd, SelEndOffset,
      {$IFDEF RVOLDTAGS}rvoTagsArePChars in RichView.Options{$ELSE}True{$ENDIF},
      OnUnknownItem,
      OnSaveControl, UseStyleNames, SaveLineBreaks)
  finally
    Bullets.Free;
  end;
end;

end.