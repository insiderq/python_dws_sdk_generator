unit RVXMLBase;

interface

{$I RV_Defs.inc}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  SysUtils, Classes, Graphics, RVTypes;

const
  crlf = #13#10;
  tab = #9;

type

 TRVRawStringArray = array[0..$7FFFFFF{MaxListSize}] of TRVAnsiString;
 PRVRawStringArray = ^TRVRawStringArray;

  TRVXMLRawByteStringList = class
  private
    FCount, FCapacity: Integer;
    FItems: PRVRawStringArray;
  public
    procedure Clear;
    procedure Add(const s: TRVRawByteString);
    procedure SaveToStream(Stream: TStream);
    destructor Destroy; override;
  end;

  TXMLValueType = (vtText, vtCDATA);
  PPChar = ^PRVAnsiChar;

  TXMLAttr = class (TObject)
  private
    FTempValue: TRVRawByteString;
  public
    Name: TRVAnsiString;
    Value: String;
  end;

  TXMLAttrList = class (TList)
  protected
    function Get(Index: Integer): TXMLAttr;
    procedure Put(Index: Integer; const Value: TXMLAttr);
  public
    function AddAttrTmp(const Name: TRVAnsiString; const Value: TRVRawByteString): TXMLAttr;
    function AddAttr(const Name: TRVAnsiString; const Value: String): TXMLAttr;
    procedure Clear; override;
    function FindAttrOfName(Name: TRVAnsiString): TXMLAttr;
    property Items[Index: Integer]: TXMLAttr read Get write Put; default;
  end;

  TXMLTagList = class;
  TXMLTree = class;

  TXMLTagValueEncoding = (xmleANSI, xmleRawUnicode, xmleUTF8);

  TXMLTag = class (TObject)
  public
    Name: TRVAnsiString;
    Value: TRVRawByteString;
    //UniValue: WideString;
    Level: Integer;
    ValueType: TXMLValueType;
    Items: TXMLTagList;
    Attr: TXMLAttrList;
    Parent: TXMLTag;
    Root: TXMLTree;
    UnicodeMode: TXMLTagValueEncoding;
    TagCodePage, ValueCodePage: Cardinal;
    procedure Clear;
    constructor Create(Str: TRVRawByteString; AParent: TXMLTag; ARoot: TXMLTree);
    destructor Destroy; override;
    procedure GetDocument(S: TRVXMLRawByteStringList; Tabs: Boolean; UTF8: Boolean);
    function NextSibling: TXMLTag;
    function GetValueString: String;
    function GetANSIValueString: TRVAnsiString;
    function GetRawUnicodeValueString: TRVRawByteString;
  end;

  TXMLTagList = class (TList)
  protected
    function Get(Index: Integer): TXMLTag;
    procedure Put(Index: Integer; const Value: TXMLTag);
  public
    function AddTag(Str: TRVRawByteString; Parent: TXMLTag; Root: TXMLTree): TXMLTag;
    function InsertTag(Index: Integer; Str: TRVRawByteString; Parent: TXMLTag;
      Root: TXMLTree): TXMLTag;
    procedure Clear; override;
    function FindTagOfName(Name: TRVAnsiString): TXMLTag;
    property Items[Index: Integer]: TXMLTag read Get write Put; default;
  end;

  TXMLTree = class (TXMLTag)
  public
    //UnicodeTags: TStringList;
    Encoding: String;
    BaseFilePath: string;
    constructor Create(Enc: String);
    destructor Destroy; override;
    procedure ExtractBaseFilePath(FileName: string);
    procedure GetDocument(S: TRVXMLRawByteStringList; Tabs: Boolean);
    procedure LoadFromFile(FileName: string);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToFile(FileName: string; Tabs: Boolean);
    procedure SaveToStream(Stream: TStream; Tabs: Boolean);
    procedure Parse(S: TRVRawByteString; CodePage: Cardinal);
  end;

implementation

uses RVStyle, RVUni;

{$IFNDEF RICHVIEWDEFXE2}
function AdvPos(const FindString, SourceString: TRVRawByteString; StartPos: Integer): Integer;
asm
        PUSH    ESI
        PUSH    EDI
        PUSH    EBX
        PUSH    EDX
        TEST    EAX,EAX
        JE      @@qt
        TEST    EDX,EDX
        JE      @@qt0
        MOV     ESI,EAX
        MOV     EDI,EDX
        MOV     EAX,[EAX-4]
        MOV     EDX,[EDX-4]
        DEC     EAX
        SUB     EDX,EAX
        DEC     ECX
        SUB     EDX,ECX
        JNG     @@qt0
        MOV     EBX,EAX
        XCHG    EAX,EDX
        NOP
        ADD     EDI,ECX
        MOV     ECX,EAX
        MOV     AL,BYTE PTR [ESI]
@@lp1:  CMP     AL,BYTE PTR [EDI]
        JE      @@uu
@@fr:   INC     EDI
        DEC     ECX
        JNZ     @@lp1
@@qt0:  XOR     EAX,EAX
        JMP     @@qt
@@ms:   MOV     AL,BYTE PTR [ESI]
        MOV     EBX,EDX
        JMP     @@fr
@@uu:   TEST    EDX,EDX
        JE      @@fd
@@lp2:  MOV     AL,BYTE PTR [ESI+EBX]
        XOR     AL,BYTE PTR [EDI+EBX]
        JNE     @@ms
        DEC     EBX
        JNE     @@lp2
@@fd:   LEA     EAX,[EDI+1]
        SUB     EAX,[ESP]
@@qt:   POP     ECX
        POP     EBX
        POP     EDI
        POP     ESI
end;
{$ELSE}
function AdvPos(const FindString, SourceString: TRVRawByteString; StartPos: Integer): Integer; inline;
begin
  Result := AnsiStrings.PosEx(FindString, SourceString, StartPos);
end;
{$ENDIF}

function ClosingBracketPos(const SourceString: TRVRawByteString; StartPos: Integer): Integer;
var p: Integer;
begin
  Result := 0;
  while True do begin
    p := AdvPos('>', SourceString, StartPos);
    if p=0 then
      exit;
    StartPos := AdvPos('=', SourceString, StartPos);
    if (StartPos=0) or (StartPos>p) then begin
      Result := p;
      exit;
    end;
    StartPos := AdvPos('"', SourceString, StartPos+1);
    if StartPos=0 then
      exit;
    StartPos := AdvPos('"', SourceString, StartPos+1);
    if StartPos=0 then
      exit;
  end;
end;

function StringReplace(const Str, OldStr, NewStr: TRVRawByteString;
  Flags: TReplaceFlags): TRVRawByteString;
var
  P: Cardinal;
begin
  P := 1;
  Result := Str;
  while True do
  begin
    P := AdvPos(OldStr, Result, P);
    if P = 0 then Exit;
    Delete(Result, P, Length(OldStr));
    Insert(NewStr, Result, P);
    Inc(P, Length(NewStr));
  end;
end;

function CleanUnicode(Str: TRVRawByteString): TRVUnicodeString;
var
  I, P, P2, L: Integer;
  S: TRVRawByteString;
begin
  Result := '';
  L := Length(Str);
  P := 1;
  P2 := 0;
  repeat
    P := AdvPos('&#', Str, P);
    if P = 0 then
    begin
      Result := Result + TRVUnicodeString(TRVAnsiString(Copy(Str, P2 + 1, L - P2)));
      Exit;
    end else
      Result := Result + TRVUnicodeString(TRVAnsiString(Copy(Str, P2 + 1, P - P2 - 1)));

    P2 := AdvPos(';', Str, P);
    if P2 = 0 then Exit;
    S := Copy(Str, P + 2, P2 - P - 2);
    I := RVStrToIntDef(S, 0);
    Result := Result + TRVUnicodeChar(I);
    P := P2;
    Inc(P);
  until P > L;
end;

function UnCleanUnicode(Str: TRVRawByteString): TRVRawByteString;
var
  W: Word;
  I: Integer;
  C: array[0..1] of TRVAnsiChar;
begin
  I := 1;
  Result := '';
  while I <= Length(Str) do
  begin
    C[0] := Str[I];
    C[1] := Str[I + 1];
    W := Word(C);
    if W > 255 then
      Result := Result + '&#' + RVIntToStr(W) + ';'
    else
      Result := Result + C[0];
    Inc(I, 2);
  end;
end;

function CleanUni(Str: TRVRawByteString): TRVRawByteString;
begin
  Result := StringReplace(Str, crlf, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, tab, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, '&lt;', '<', [rfReplaceAll]);
  Result := StringReplace(Result, '&gt;', '>', [rfReplaceAll]);
  Result := StringReplace(Result, '&apos;', '''', [rfReplaceAll]);
  Result := StringReplace(Result, '&amp;', '&', [rfReplaceAll]);
end;

function Clean(Str: TRVRawByteString): TRVRawByteString;
var
  P, P2, L, I, J: Integer;
  S: TRVRawByteString;
begin
  Result := StringReplace(Str, crlf, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, tab, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, '&lt;', '<', [rfReplaceAll]);
  Result := StringReplace(Result, '&gt;', '>', [rfReplaceAll]);
  Result := StringReplace(Result, '&apos;', '''', [rfReplaceAll]);
  L := Length(Result);
  P := 1;
  J := 0;
  repeat
    P := AdvPos('&#', Result, P - J);
    if P = 0 then Break;
    P2 := AdvPos(';', Result, P);
    if P2 = 0 then Break;
    J := P2 - P;
    S := Copy(Result, P + 2, P2 - P - 2);
    I := RVStrToIntDef(S, 0);
    Delete(Result, P, P2 - P + 1);
    Insert(TRVAnsiString(Char(I)), Result, P);
//    Result := Copy(Result, 1, P - 1) + Chr(I) + Copy(Result, P + 1, MaxInt);
    P := P2;
    Inc(P);
  until P > L; 
  Result := StringReplace(Result, '&amp;', '&', [rfReplaceAll]);
end;

function CleanSpec(Str: TRVRawByteString): TRVRawByteString;
begin
  Result := StringReplace(Str, '&quot;', '"', [rfReplaceAll]);
end;

function UnCleanSpec(Str: TRVRawByteString): TRVRawByteString;
var
  P: PRVAnsiChar;
  I, J: Integer;
begin
  P := PRVAnsiChar(Str);
  Result := '';
  I := 1;
  J := 1;
  while True do
  begin
    while Ord(P^) >= 32 do
    begin
      Inc(P);
      Inc(I);
    end;
    Result := Result + Copy(Str, J, I - J);
    if P^ = #0 then Exit;
{    if P^ = #0 then
    begin
      Result := Result + Copy(Str, J, I - J);
      Exit;
    end;
    Result := Result + Copy(Str, J - 1, I - J); }
    Result := Result + '&#' + RVIntToStr(Ord(P^)) + ';';
//    J := I + 1;
    Inc(I);
    J := I;
    Inc(P);
  end;
end;

function UnCleanUni(Str: TRVRawByteString): TRVRawByteString;
begin
  Result := StringReplace(Str, '<', '&lt;', [rfReplaceAll]);
  Result := StringReplace(Result, '>', '&gt;', [rfReplaceAll]);
end;

function UnClean(Str: TRVRawByteString): TRVRawByteString;
begin
  Result := StringReplace(Str, '"', '&quot;', [rfReplaceAll]);
  Result := UnCleanSpec(Result);
end;

function UnCleanText(Str: TRVRawByteString): TRVRawByteString;
begin
  Result := StringReplace(Str, '&', '&amp;', [rfReplaceAll]);
  Result := StringReplace(Result, '<', '&lt;', [rfReplaceAll]);
  Result := StringReplace(Result, '>', '&gt;', [rfReplaceAll]);
  Result := UnCleanSpec(Result);
end;

{TXMLAttrList}

function TXMLAttrList.AddAttrTmp(const Name: TRVAnsiString; const Value: TRVRawByteString): TXMLAttr;
begin
  Result := TXMLAttr.Create;
  Result.Name := Name;
  Result.FTempValue := Value;
  Result.Value := String(Value);
  inherited Add(Result);
end;

function TXMLAttrList.AddAttr(const Name: TRVAnsiString; const Value: String): TXMLAttr;
begin
  Result := TXMLAttr.Create;
  Result.Name := Name;
  Result.Value := Value;
  inherited Add(Result);
end;

procedure TXMLAttrList.Clear;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].Free;
  inherited;
end;

function TXMLAttrList.Get(Index: Integer): TXMLAttr;
begin
  Result := inherited Get(Index);
end;

function TXMLAttrList.FindAttrOfName(Name: TRVAnsiString): TXMLAttr;
var
  I: Integer;
begin
  Result := nil;
  Name := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Name);
  for I := 0 to Count - 1 do
    if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Items[I].Name) = Name then
    begin
      Result := Items[I];
      Exit;
    end;
end;

procedure TXMLAttrList.Put(Index: Integer; const Value: TXMLAttr);
begin
  inherited Put(Index, Value);
end;

{TXMLTag}

procedure TXMLTag.Clear;
begin
  Name := '';
  Value := '';
  ValueType := vtText;
  Attr.Clear;
  Items.Clear;
end;

constructor TXMLTag.Create(Str: TRVRawByteString; AParent: TXMLTag; ARoot: TXMLTree);
var
  P, P2, L: Integer;
  AName: TRVAnsiString;
  AValue: TRVRawByteString;
begin
  inherited Create;
  Items := TXMLTagList.Create;
  Attr := TXMLAttrList.Create;
  UnicodeMode := xmleANSI;
  ValueType := vtText;
  Value := '';
  Root := ARoot;
  Parent := AParent;
  if Parent <> nil then
    Level := Parent.Level + 1
  else
    Level := -1;
  Name := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Trim(Str);
  P := AdvPos(' ', Str, 1);
  if P = 0 then Exit;
  Name := Copy(Str, 1, P - 1);
  L := Length(Str);
  repeat
    P2 := AdvPos('=', Str, P);
    if P2 = 0 then Exit;
    AName := Trim(Copy(Str, P + 1, P2 - P - 1));
    P := AdvPos('"', Str, P2);
    if P = 0 then Exit;
    P2 := AdvPos('"', Str, P + 1);
    if P2 = 0 then Exit;
    AValue := CleanSpec(Clean(Copy(Str, P + 1, P2 - P - 1)));
    P := P2;
    Inc(P);
    Attr.AddAttrTmp(AName, AValue);
  until P > L; 
end;

destructor TXMLTag.Destroy;
begin
  Items.Free;
  Attr.Free;
  Items := nil;
  Attr  := nil;
  inherited Destroy;
end;

function StringToXMLString(const s: String; UTF8: Boolean; CodePage: TRVCodePage): TRVRawByteString;
begin
  {$IFDEF RVUNICODESTR}
  if UTF8 then
    Result := UTF8Encode(s)
  else
    Result := RVU_UnicodeToAnsi(CodePage, RVU_GetRawUnicode(s));
  {$ELSE}
  if UTF8 then
    Result := RVU_AnsiToUTF8(CodePage, s)
  else
    Result := s;
  {$ENDIF}
end;

procedure TXMLTag.GetDocument(S: TRVXMLRawByteStringList; Tabs, UTF8: Boolean);
var
  Spc, Str, Str2: TRVRawByteString;
  I: Integer;
begin
  if Tabs then
    Spc := StringOfChar(TRVAnsiChar(tab), Level);

  Str := Spc + '<' + Name;

  Str2 := '';
  for I := 0 to Attr.Count - 1 do
    Str2 := Str2 + ' ' + Attr[I].Name + '="' +
      UnClean(StringToXMLString(Attr[I].Value, UTF8, TagCodePage)) + '"';
  if Str2 <> '' then
    Str := Str + Str2;

  if (Value = '') and (Items.Count = 0) then
  begin
    Str := Str + ' />';
    S.Add(Str);
    Exit;
  end else
    Str := Str + '>';

  if UTF8 then begin
    if Value <> '' then
      case UnicodeMode of
        xmleRawUnicode:
          Str := Str + UnCleanText(RVU_GetHTMLUTF8EncodedUnicode(Value, True, False));
        xmleANSI:
          Str := Str + RVU_AnsiToUTF8(ValueCodePage, UnCleanText(Value));
        xmleUTF8:
          Str := Str + UnCleanText(Value);
      end;
    end
  else begin
    if Value <> '' then
      case UnicodeMode of
        xmleRawUnicode:
          Str := Str + UnCleanUni(UnCleanUnicode(Value));
        xmleANSI:
          Str := Str + UnCleanText(Value);
        xmleUTF8:
          Str := Str + UnCleanUni(UnCleanUnicode(RVU_GetRawUnicode(
            {$IFDEF RICHVIEWDEF2009}UTF8ToString(Value){$ELSE}UTF8Decode(Value){$ENDIF}
            )));
      end;
  end;

  if Items.Count = 0 then begin
    Str := Str + '</' + Name + '>';
    S.Add(Str);
    end
  else begin
    S.Add(Str);
    for I := 0 to Items.Count - 1 do
      Items[I].GetDocument(S, Tabs, UTF8);
    S.Add(Spc + '</' + Name + '>');
  end;
end;

function TXMLTag.NextSibling: TXMLTag;
var
  I: Integer;
begin
  Result := nil;
  if Parent = nil then Exit;
  I := Parent.Items.IndexOf(Self);
  if I = -1 then Exit;
  if I = Parent.Items.Count - 1 then Exit;
  Result := Parent.Items[I + 1];
end;

function TXMLTag.GetValueString: String;
begin
  {$IFDEF RVUNICODESTR}
  case UnicodeMode of
    xmleRawUnicode:
      Result := RVU_RawUnicodeToWideString(Value);
    xmleUTF8:
      Result := UTF8ToString(Value);
    else
      Result := RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(ValueCodePage, Value));
  end;
  {$ELSE}
  case UnicodeMode of
    xmleRawUnicode:
      Result := RVU_UnicodeToAnsi(ValueCodePage, Value);
    xmleUTF8:
      Result := RVU_UnicodeToAnsi(ValueCodePage, RVU_GetRawUnicode(UTF8Decode(Value)));
    else
      Result := Value;
  end;
  {$ENDIF}
end;

function TXMLTag.GetANSIValueString: TRVAnsiString;
begin
  case UnicodeMode of
    xmleRawUnicode:
      Result := RVU_UnicodeToAnsi(ValueCodePage, Value);
    xmleUTF8:
      {$IFDEF RVUNICODESTR}
      Result := RVU_UnicodeToAnsi(ValueCodePage, RVU_GetRawUnicode(UTF8ToString(Value)));
      {$ELSE}
      Result := RVU_UnicodeToAnsi(ValueCodePage, RVU_GetRawUnicode(UTF8Decode(Value)));
      {$ENDIF}
    else
      Result := Value;
  end;
end;

function TXMLTag.GetRawUnicodeValueString: TRVRawByteString;
begin
  case UnicodeMode of
    xmleRawUnicode:
      Result := Value;
    xmleUTF8:
      {$IFDEF RVUNICODESTR}
      Result := RVU_GetRawUnicode(UTF8ToString(Value));
      {$ELSE}
      Result := RVU_GetRawUnicode(UTF8Decode(Value));
      {$ENDIF}
    else
      Result := RVU_AnsiToUnicode(ValueCodePage, Value);
  end;
end;

{TXMLTagList}

function TXMLTagList.AddTag(Str: TRVRawByteString; Parent: TXMLTag;
  Root: TXMLTree): TXMLTag;
begin
  Result := TXMLTag.Create(Str, Parent, Root);
  inherited Add(Result);
end;

function TXMLTagList.InsertTag(Index: Integer; Str: TRVRawByteString;
  Parent: TXMLTag;
  Root: TXMLTree): TXMLTag;
begin
  Result := TXMLTag.Create(Str, Parent, Root);
  inherited Insert(Index, Result);
end;

procedure TXMLTagList.Clear;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].Free;
  inherited;
end;

function TXMLTagList.Get(Index: Integer): TXMLTag;
begin
  Result := inherited Get(Index);
end;

function TXMLTagList.FindTagOfName(Name: TRVAnsiString): TXMLTag;
var
  I: Integer;
begin
  Result := nil;
  Name := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Name);
  for I := 0 to Count - 1 do
    if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(Items[I].Name) = Name then
    begin
      Result := Items[I];
      Exit;
    end;
end;

procedure TXMLTagList.Put(Index: Integer; const Value: TXMLTag);
begin
  inherited Put(Index, Value);
end;

{TXMLTree}

constructor TXMLTree.Create(Enc: String);
begin
  inherited Create('', nil, nil);
  Level := -1;
  Encoding := Enc;
  //UnicodeTags := TStringList.Create;
end;

destructor TXMLTree.Destroy;
begin
  //UnicodeTags.Free;
  //UnicodeTags := nil;
  inherited Destroy;
end;

procedure TXMLTree.ExtractBaseFilePath(FileName: string);
begin
  BaseFilePath := ExtractFilePath(ExpandUNCFileName(FileName));
end;

procedure TXMLTree.GetDocument(S: TRVXMLRawByteStringList; Tabs: Boolean);
begin
  S.Clear;
  if Items.Count > 0 then
    Items[0].GetDocument(S, Tabs, UpperCase(Encoding)='UTF-8');
end;

procedure TXMLTree.LoadFromFile(FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TXMLTree.LoadFromStream(Stream: TStream);
var
  S: TRVRawByteString;
  P: PPChar;
begin
  SetLength(S, Stream.Size - Stream.Position);
  P := PPChar(@S);
  Stream.Read(P^^, Stream.Size - Stream.Position);
  Parse(S, 0);
end;

procedure TXMLTree.Parse(S: TRVRawByteString; CodePage: Cardinal);
var
  Tag, PITag: TXMLTag;
  P, P2, L: Integer;
  T: TRVRawByteString;
  PIAttr: TXMLAttr;
  UTF8: Boolean;

  procedure ConvertAttributes(Tag: TXMLTag);
  var PIAttr: TXMLAttr;
      Charset: LongInt;
      i: Integer;
  begin
    if not UTF8 then
      exit;
    Tag.TagCodePage := CodePage;
    if Tag.Name='marker' then begin
      PIAttr := Tag.Attr.FindAttrOfName('charset');
      if PIAttr<>nil then begin
        IdentToCharset(String(PIAttr.Value), Charset);
        Tag.TagCodePage := RVU_Charset2CodePage(Charset);
      end;
    end;
    for i := 0 to Tag.Attr.Count-1 do
      with Tag.Attr[i] do begin
        Name := RVU_UnicodeToAnsi(Tag.TagCodePage, RVU_GetRawUnicode(
          {$IFDEF RICHVIEWDEF2009}UTF8ToString(Name){$ELSE}UTF8Decode(Name){$ENDIF}));
        {$IFDEF RVUNICODESTR}
        Value := UTF8ToString(FTempValue);
        {$ELSE}
        Value := RVU_UnicodeToAnsi(Tag.TagCodePage, RVU_GetRawUnicode(
          {$IFDEF RICHVIEWDEF2009}UTF8ToString(FTempValue){$ELSE}UTF8Decode(FTempValue){$ENDIF}));
        {$ENDIF}
      end;
  end;

begin
  UTF8 := False;
  Items.Clear;
  Attr.Clear;
  Tag := Self;
  L := Length(S);
  p := 1;
  repeat
    P2 := AdvPos('<', S, P);
    if P2 <> 0 then
    begin
      T := Copy(S, P, P2 - P);
      if T <> '' then
      begin
        //if UnicodeTags.IndexOf(Tag.Name) = -1 then
          if UTF8 then
          begin
            Tag.UnicodeMode := xmleUTF8;
            Tag.Value := Clean(T);
            {$IFNDEF RVUNICODESTR}
            if Tag.Name='utext' then begin
            {$ENDIF}
              Tag.Value := RVU_GetRawUnicode(
                {$IFDEF RICHVIEWDEF2009}UTF8ToString(Tag.Value){$ELSE}UTF8Decode(Tag.Value){$ENDIF});
              Tag.UnicodeMode := xmleRawUnicode;
            {$IFNDEF RVUNICODESTR}
            end
            else begin
              Tag.Value := RVU_UnicodeToAnsi(CodePage, RVU_GetRawUnicode(
                {$IFDEF RICHVIEWDEF2009}UTF8ToString(Tag.Value){$ELSE}UTF8Decode(Tag.Value){$ENDIF}));
              Tag.UnicodeMode := xmleANSI;
            end;
            {$ENDIF}
          end
          else
          begin
            {$IFNDEF RVUNICODESTR}
            if Tag.Name='utext' then begin
            {$ENDIF}
               Tag.Value := RVU_GetRawUnicode(CleanUnicode(CleanUni(T)));
               Tag.UnicodeMode := xmleRawUnicode;
            {$IFNDEF RVUNICODESTR}
              end
            else begin
              Tag.Value := Clean(T);
              Tag.UnicodeMode := xmleANSI;
            end;
            {$ENDIF}
          end;
      end;
      P := P2;
      if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}UpperCase(Copy(S, P, 9)) = '<![CDATA[' then
      begin
        P2 := AdvPos(']]>', S, P);
        T := Copy(S, P + 9, P2 - P - 9);
        Tag.Value := T;
        Tag.ValueType := vtCDATA;
        P := P2 + 2;
      end else if Copy(S, P, 4) = '<!--' then
      begin
        P2 := AdvPos('-->', S, P);
        if P2 = 0 then Exit;
        P := P2 + 2;
      end else if Copy(S, P, 2) = '<?' then
      begin
        P2 := AdvPos('?>', S, P);
        if P2 = 0 then Exit;
        T := Copy(S, P + 2, P2 - P - 2);
        P := P2 + 1;
        if UTF8 then
          T := RVU_UnicodeToAnsi(CodePage, RVU_GetRawUnicode(
            {$IFDEF RICHVIEWDEF2009}UTF8ToString(T){$ELSE}UTF8Decode(T){$ENDIF}));
        PITag := TXMLTag.Create(T, nil, nil);
        try
          if {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}LowerCase(PITag.Name) = 'xml' then
          begin
            PIAttr := PITag.Attr.FindAttrOfName('encoding');
            if PIAttr <> nil then begin
              Encoding := PIAttr.Value;
              UTF8 := UpperCase(Encoding)='UTF-8';
            end;
          end;
        finally
          PITag.Free;
        end;
      end else
      begin
        P2 := ClosingBracketPos(S, P);
        if P2 = 0 then Exit;
        T := Trim(Copy(S, P + 1, P2 - P - 1));
        P := P2;
        if T[1] = '/' then
        begin
          Tag := Tag.Parent;
          if Tag = nil then Exit;
        end else if T[Length(T)] = '/' then
        begin
          Delete(T, Length(T), 1);
          ConvertAttributes(Tag.Items.AddTag(T, Tag, Self));
        end else begin
          Tag := Tag.Items.AddTag(T, Tag, Self);
          ConvertAttributes(Tag);
        end;
      end;
    end else
    begin
      T := Copy(S, P, Length(S) - P);
      if T <> '' then
        Tag.Value := Clean(T);
      Exit;
    end;
    Inc(P);
  until P > L;
end;

procedure TXMLTree.SaveToFile(FileName: string; Tabs: Boolean);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(Stream, Tabs);
  finally
    Stream.Free;
  end;
end;

procedure TXMLTree.SaveToStream(Stream: TStream; Tabs: Boolean);
var
  S: TRVRawByteString;
  StrList: TRVXMLRawByteStringList;
begin
  S := '<?xml version="1.0" encoding="' + TRVAnsiString(Encoding) + '"?>' + crlf;
  Stream.Write(PRVAnsiChar(S)^, Length(S));
  StrList := TRVXMLRawByteStringList.Create;
  GetDocument(StrList, Tabs);
  StrList.SaveToStream(Stream);
  StrList.Free;
end;

{ TRVXMLRawByteStringList }

procedure TRVXMLRawByteStringList.Add(const s: TRVRawByteString);
var NewCapacity: Integer;
begin
  if FCount=FCapacity then begin
    NewCapacity := FCapacity+8;
    ReallocMem(FItems, NewCapacity*sizeof(Pointer));
    FillChar((PRVAnsiChar(FItems)+FCapacity*sizeof(Pointer))^,
      (NewCapacity-FCapacity)*sizeof(Pointer), 0);
    FCapacity := NewCapacity;
  end;
  FItems^[FCount] := s;
  inc(FCount);
end;

procedure TRVXMLRawByteStringList.Clear;
begin
  if FCount <> 0 then
    Finalize(FItems^[0], FCount);
  if FCapacity<>0 then
    FreeMem(FItems);
  FItems := nil;
  FCapacity := 0;
  FCount := 0;
end;

destructor TRVXMLRawByteStringList.Destroy;
begin
  Clear;
  inherited;
end;

procedure TRVXMLRawByteStringList.SaveToStream(Stream: TStream);
var i: Integer;
    s: TRVAnsiString;
begin
  s := #13#10;
  for i := 0 to FCount-1 do begin
    if i<>0 then
      Stream.WriteBuffer(PRVAnsiChar(s)^, 2);
    Stream.WriteBuffer(PRVAnsiChar(FItems[i])^, Length(FItems[i]));
  end;
end;

end.
