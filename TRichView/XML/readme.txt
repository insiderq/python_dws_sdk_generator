TRichViewXML - free addon for RichView.

TRichViewXML, original version: Copyright(c) Jiri Banzet
TRichViewXML, improvements: Copyright(c) Sergey Tkachenko <svt@trichview.com>
TRichView: Copyright(c) Sergey Tkachenko <svt@trichview.com>

Directories:
SOURCE - source code of TRichViewXML.
  Contains source code and packages.
DOC - documentation, in TRichViewXML format.
  You can view it using viewer.exe.
  If viewer.exe is not included, compile project from
  VIEWER directory and copy compiled application into DOC.
VIEWER - source code for XML documentation viewer

Packages to install:
RVXMLD5.dpk - for Delphi 5;
RVXMLD6.dpk - for Delphi 6;
RVXMLD7.dpk - for Delphi 7;
RVXMLD9.bdsproj - for Developer Studio 2005 (Delphi for Win32);
RVXML2006.bdsproj - for Developer Studio 2006 (Delphi for Win32 and C++Builder);
RVXMLD2007.dproj - for Delphi 2007 (cannot be installed together with RVXMLCB2007.cbproj)
RVXMLD2009.dproj - for Delphi 2009 (cannot be installed together with RVXMLCB2009.cbproj)
RVXMLD2010.dproj - for Delphi 2010 (cannot be installed together with RVXMLCB2010.cbproj)
RVXMLDXE.dproj - for Delphi XE (cannot be installed together with RVXMLCBXE.cbproj)
RVXMLDXE2.dproj - for Delphi and C++Builder XE2
RVXMLDXE3.dproj - for Delphi and C++Builder XE3
RVXMLDXE4.dproj - for Delphi and C++Builder XE4
RVXMLDXE5.dproj - for Delphi and C++Builder XE5

RVXMLCB5.bpk - for C++Builder 5;
RVXMLCB6.bpk - for C++Builder 6.
RVXMLTCPP.bdsproj - for Turbo C++ 2006;
RVXMLCB2007.cbproj - for C++Builder 2007 (cannot be installed together with RVXMLD2007.dproj)
RVXMLCB2009.cbproj - for C++Builder 2009 (cannot be installed together with RVXMLD2009.dproj)
RVXMLCB2010.cbproj - for C++Builder 2010 (cannot be installed together with RVXMLD2010.dproj)
RVXMLCB2XE.cbproj - for C++Builder XE (cannot be installed together with RVXMLDXE.dproj)
RVXMLCB2XE4.cbproj - for C++Builder XE4 (if you do not have Delphi personality installed)
RVXMLCB2XE5.cbproj - for C++Builder XE5 (if you do not have Delphi personality installed)

Web:
http://www.trichview.com/resources/xml/