﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVSubData.pas' rev: 27.00 (Windows)

#ifndef RvsubdataHPP
#define RvsubdataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <RVUndo.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvsubdata
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVSubData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSubData : public Crvdata::TCustomRVData
{
	typedef Crvdata::TCustomRVData inherited;
	
private:
	Crvdata::TRVFlags FFlags;
	Rvitem::TCustomRVItemInfo* FOwner;
	
protected:
	Rvseqitem::TRVSeqList* FSeqList;
	Rvmarker::TRVMarkerList* FMarkers;
	DYNAMIC void __fastcall DestroyMarkers(void);
	DYNAMIC void __fastcall DestroySeqList(void);
	DYNAMIC System::UnicodeString __fastcall GetURL(int id);
	virtual Crvdata::TRVFlags __fastcall GetFlags(void);
	virtual void __fastcall SetFlags(const Crvdata::TRVFlags Value);
	
public:
	Crvdata::TCustomRVData* MainRVData;
	__fastcall TRVSubData(Rvitem::TCustomRVItemInfo* AOwner, Crvdata::TCustomRVData* AMainRVData);
	DYNAMIC void __fastcall ControlAction2(Crvdata::TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Vcl::Controls::TControl* &Control);
	virtual Crvdata::TCustomRVData* __fastcall GetParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetRootData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteRootData(void);
	void __fastcall MovingToUndoList(Rvundo::TRVUndoInfo* AContainerUndoItem);
	void __fastcall MovingFromUndoList(void);
	DYNAMIC Rvseqitem::TRVSeqList* __fastcall GetSeqList(bool AllowCreate);
	DYNAMIC Rvmarker::TRVMarkerList* __fastcall GetMarkers(bool AllowCreate);
	HIDESBASE bool __fastcall LoadRTFFromStream(System::Classes::TStream* Stream);
	HIDESBASE bool __fastcall LoadRTF(const System::UnicodeString FileName);
	HIDESBASE bool __fastcall SaveRTFToStream(System::Classes::TStream* Stream, const System::UnicodeString Path);
	HIDESBASE bool __fastcall SaveRTF(const System::UnicodeString FileName);
	HIDESBASE bool __fastcall LoadRVFFromStream(System::Classes::TStream* Stream);
	HIDESBASE bool __fastcall LoadRVF(const System::UnicodeString FileName);
	HIDESBASE bool __fastcall SaveRVFToStream(System::Classes::TStream* Stream, bool Root = true);
	HIDESBASE bool __fastcall SaveRVF(const System::UnicodeString FileName);
	HIDESBASE bool __fastcall AppendRVFFromStream(System::Classes::TStream* Stream, int ParaNo);
	DYNAMIC void __fastcall DoneStyleMappings(bool AsSubDoc);
	DYNAMIC bool __fastcall UseStyleTemplates(void);
	virtual System::Uitypes::TColor __fastcall GetColor(void);
	__property Rvitem::TCustomRVItemInfo* Owner = {read=FOwner};
public:
	/* TCustomRVData.Destroy */ inline __fastcall virtual ~TRVSubData(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvsubdata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSUBDATA)
using namespace Rvsubdata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvsubdataHPP
