﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDBPkgDXE6.dpk' rev: 27.00 (Windows)

#ifndef Rvdbpkgdxe6HPP
#define Rvdbpkgdxe6HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <DBRV.hpp>	// Pascal unit
#include <RVDBDsgn.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Internal.ExcUtils.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.VarUtils.hpp>	// Pascal unit
#include <System.Variants.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <System.TimeSpan.hpp>	// Pascal unit
#include <System.SyncObjs.hpp>	// Pascal unit
#include <System.Generics.Defaults.hpp>	// Pascal unit
#include <System.Rtti.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.DateUtils.hpp>	// Pascal unit
#include <System.IOUtils.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Actions.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <System.HelpIntfs.hpp>	// Pascal unit
#include <Winapi.UxTheme.hpp>	// Pascal unit
#include <Vcl.GraphUtil.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <System.Win.ComObj.hpp>	// Pascal unit
#include <Winapi.FlatSB.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVThumbMaker.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVAnimate.hpp>	// Pascal unit
#include <RVRTF.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <Data.SqlTimSt.hpp>	// Pascal unit
#include <Data.FmtBcd.hpp>	// Pascal unit
#include <Data.DB.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <Vcl.DBLogDlg.hpp>	// Pascal unit
#include <Vcl.DBPWDlg.hpp>	// Pascal unit
#include <Vcl.DBCtrls.hpp>	// Pascal unit
#include <IDEMessages.hpp>	// Pascal unit
#include <Vcl.CaptionedDockTree.hpp>	// Pascal unit
#include <Vcl.DockTabSet.hpp>	// Pascal unit
#include <PercentageDockTree.hpp>	// Pascal unit
#include <BrandingAPI.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <Winapi.Mapi.hpp>	// Pascal unit
#include <Vcl.ExtActns.hpp>	// Pascal unit
#include <Vcl.ActnMenus.hpp>	// Pascal unit
#include <Vcl.ActnMan.hpp>	// Pascal unit
#include <Vcl.PlatformDefaultStyleActnCtrls.hpp>	// Pascal unit
#include <BaseDock.hpp>	// Pascal unit
#include <DeskUtil.hpp>	// Pascal unit
#include <DeskForm.hpp>	// Pascal unit
#include <DockForm.hpp>	// Pascal unit
#include <Xml.Win.msxmldom.hpp>	// Pascal unit
#include <Xml.xmldom.hpp>	// Pascal unit
#include <ToolsAPI.hpp>	// Pascal unit
#include <Proxies.hpp>	// Pascal unit
#include <DesignEditors.hpp>	// Pascal unit
#include <Vcl.AxCtrls.hpp>	// Pascal unit
#include <Vcl.AppEvnts.hpp>	// Pascal unit
#include <TreeIntf.hpp>	// Pascal unit
#include <TopLevels.hpp>	// Pascal unit
#include <StFilSys.hpp>	// Pascal unit
#include <IDEHelp.hpp>	// Pascal unit
#include <ComponentDesigner.hpp>	// Pascal unit
#include <VCLEditors.hpp>	// Pascal unit
#include <ToolWnds.hpp>	// Pascal unit
#include <ColnEdit.hpp>	// Pascal unit
#include <RVDsgn.hpp>	// Pascal unit
#include <RVSEdit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdbpkgdxe6
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvdbpkgdxe6 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDBPKGDXE6)
using namespace Rvdbpkgdxe6;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Rvdbpkgdxe6HPP
