
{*******************************************************}
{                                                       }
{       RichView                                        }
{       Text strings for RichView.                      }
{       Non-localizable (except from, may be,           }
{       exception messages).                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVStr;

{$I RV_Defs.inc}

interface
uses Windows, RVTypes;

{================================ Error messages ==============================}
// These messages are the texts of exception.
// Should not occur in debugged application.
resourcestring
  errRVNil               = 'Parameter can''t be NIL';
  errRVNotFormatted      = 'RichView must be formatted for this operation';
  errRVNoMemory          = 'No memory';
  errRVTypesMismatch     = 'Can''t get or set this kind of information for this item';
  errRVUnicode           = 'Can''t perform Unicode operation';
  errRVCPByCP            = 'Calling AddCheckpoint after AddCheckpoint is not allowed in this version';
  errRVNoSuchCP          = 'Invalid checkpoint index - %d';
  errRVTagsTypesMismatch = 'Can''t assign RichView to RichView - tags types mismatch';
  errRVNoSuchCP2         = 'Invalid checkpoint data';
  errRVItemRangeError    = 'Item index is out of bounds';
  errRVCPExists          = 'Checkpoint already exists';
  errStyleIsNotAssigned  = 'Style of printable TRichView component is not assigned';
  errInvalidPageNo       = 'Invalid page number is specified for printing';
  errTRVItemFormattedDataNS =   'This method is not supported for TRVItemFormattedData';
  errRViewerOnly         = 'Not supported in editor';
  errRVUndo              = 'Undo error';
  errRVCP                = 'Checkpoint error';
  errRVItemReg1          = 'Can''t register RichView item type - system is not initialized yet';
  errRVItemReg2          = 'Can''t register RichView item type - this StyleNo is already registered';
  errRVUndoEmpty         = 'Can''t modify empty undo list';
  errRVUndoAdd           = 'Incorrect adding into undo list';
  errRVUndoEmptyBuffer   = 'Undo buffer is empty';
  errRVNegative          = 'Value must not be negative';
  errRVFDocProp          = 'Invalid RVF format';
  errRVCaretPosition     = 'Invalid caret position. Please contact the developer, if you can reproduce this problem';
  errRVPrint             = 'Internal printing error';
  errRVCompare           = 'Error when comparing positions in the document';
  errRVInvProp           = 'This property is not valid here';
  errRVError             = 'RichView Error';
  errWrongAssign         = 'Cannot assign value to this undefined property';
  errRVBadStyleTemplateParent = 'Circular references in StyleTemplates are not allowed';
  errRVBadStyleTemplateParent2 = 'Wrong kind of StyleTemplate parent';
  errRVFSubDoc           = 'Error in reading RVF subdocument';
  errRVReportHelperFormat = 'Do not call RVReportHelper.RichView.Format. Use RVReportHelper.Init';
  errRVInvalidPropertyValue = 'Invalid value of property %s';
  errRVRTFDoubleStyleSheet = 'RTF must have a single style sheet';
  errRVSTCannotAssign    = 'Cannot assign StyleTemplate property because MainRVStyle is assigned';
  errRVSTHFMismatch     = 'Headers/footers must use StyleTemplates of the main document';
  errRVSTHFPropMismatch = 'Headers/footers and main document have mismatched values of UseStyleTemplates and/or StyleTemplateInsertMode';
  {$IFNDEF RVDONOTUSEDOCX}
  errRVDocXImgSave         = 'Error while saving image to DocX';
  {$ENDIF}
{================================ Resource Names ==============================}
const
  RVRC_ZOOMIN_CURSOR  = 'RV_ZOOMIN_CURSOR';
  RVRC_ZOOMOUT_CURSOR = 'RV_ZOOMOUT_CURSOR';
  RVRC_JUMP_CURSOR    ='RV_JUMP_CURSOR';
  RVRC_FLIPARROW_CURSOR ='RV_FLIPARROW_CURSOR';
{=================================== INI ======================================}
const
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  RVNORMALSTYLETEMPLATENAME = 'Normal';
  RVHEADINGSTYLETEMPLATENAME = 'heading %d';
  RVHYPERLINKSTYLETEMPLATENAME = 'Hyperlink';
  RVDEFAULTPARAGRAPHFONTSTYLENAME = 'Default Paragraph Font';
  RVCAPTIONSTYLETEMPLATE = 'caption';

  STYLETEMPLATEINISECTION = 'Styles';

//    'No Spacing', 'Title',
//  'Default Paragraph Font'
  {$ENDIF}

  {$IFNDEF RVDONOTUSEINI}

  RVINIFILEYES = 'Yes';
  RVINIFILENO = 'No';
  RVINIFILEYESU = 'YES';
  RVINIFILENOU  = 'NO';
  RVINIUNKNOWN  = '?';

  RVINI_TEXTSTYLECOUNT   = 'FontsCount';
  RVINI_TEXTSTYLEPREFIX  = 'Font%s';
  RVINI_PARASTYLECOUNT   = 'ParasCount';
  RVINI_PARASTYLEPREFIX  = 'Para%s';
  RVINI_LISTSTYLECOUNT   = 'ListCount';
  RVINI_LISTSTYLEPREFIX  = 'List%s';

  RVINI_STANDARD         = 'Standard';

  RVINI_LEFT             = 'Left';
  RVINI_RIGHT            = 'Right';
  RVINI_TOP              = 'Top';
  RVINI_BOTTOM           = 'Bottom';

  RVINI_WIDTH            = 'Width';
  RVINI_STYLE            = 'Style';
  RVINI_INTERNALWIDTH    = 'InternalWidth';
  RVINI_BOFFSPREFIX      = 'Offsets%s';
  RVINI_VISBPREFIX       = 'Visible%s';

  RVINI_SPACEBEFORE      = 'SpaceBefore';
  RVINI_SPACEAFTER       = 'SpaceAfter';
  RVINI_LEFTINDENT       = 'LeftIndent';
  RVINI_RIGHTIDENT       = 'RightIndent';
  RVINI_FIRSTINDENT      = 'FirstIndent';
  RVINI_LINESPACING      = 'LineSpacing';
  RVINI_LINESPACINGTYPE  = 'LSType';  
  RVINI_NEXTPARANO       = 'NextParaNo';
  RVINI_DEFSTYLENO       = 'DefStyleNo';  
  RVINI_ALIGNMENT        = 'Alignment';
  RVINI_NOWRAP           = 'NoWrap';
  RVINI_READONLY         = 'ReadOnly';
  RVINI_STYLEPROTECT     = 'StyleProtect';
  RVINI_DONOTWANTRETURNS = 'DoNotWantReturns';
  RVINI_KEEPLINESTOGETHER = 'KeepLinesTogether';
  RVINI_KEEPWITHNEXT     = 'KeepWithNext';  

  RVINI_BORDERPREFIX     = 'Border%s';
  RVINI_BACKGROUNDPREFIX = 'Background%s';

  RVINI_STYLENAME        = 'StyleName';
  RVINI_FONTNAME         = 'Name';
  RVINI_JUMP             = 'Jump';
  {$IFDEF RVLANGUAGEPROPERTY}
  RVINI_LANGUAGE         = 'Language';
  {$ENDIF}
  RVINI_SPACESINTAB      = 'SpacesInTab';
  RVINI_DEFTABWIDTH      = 'DefTabWidth';  
  RVINI_JUMPCURSOR       = 'JumpCursor';
  RVINI_SIZE             = 'Size';
  RVINI_SIZEDOUBLE       = 'SizeDouble';
  RVINI_COLOR            = 'Color';
  RVINI_BACKCOLOR        = 'BackColor';
  RVINI_HOVERBACKCOLOR   = 'HoverBackColor';
  RVINI_HOVERCOLOR       = 'HoverColor';
  RVINI_HOVERUNDERLINE   = 'HoverUnderline';
  RVINI_CURRENTITEMCOLOR = 'CurItemColor';
  RVINI_CHARSET          = 'Charset';
  RVINI_CHARSCALE        = 'CharScale';
  RVINI_CHARSPACING      = 'CharSpacing';
  RVINI_OUTLINELEVEL     = 'OutlineLevel';  
  RVINI_BIDIMODE         = 'BiDiMode';  
  RVINI_BOLD             = 'Bold';
  RVINI_UNDERLINE        = 'Underline';
  RVINI_STRIKEOUT        = 'StrikeOut';
  RVINI_ITALIC           = 'Italic';
  RVINI_OVERLINE         = 'Overline';
  RVINI_ALLCAPS          = 'Caps';  
  RVINI_PROTECTION       = 'Protection';
  RVINI_RTFCODE          = 'RTFCode';
  RVINI_HTMLCODE         = 'HTMLCode';
  RVINI_DOCXCODE         = 'DocXCode';
  RVINI_DOCXINRUNCODE    = 'DocXInRunCode';
  RVINI_HIDDEN           = 'Hidden';  
  RVINI_VSHIFT           = 'VShift';
  RVINI_NEXTSTYLENO      = 'NextStyleNo';
  RVINI_BASESTYLENO      = 'BaseStyleNo';
  RVINI_UNICODE          = 'Unicode';
  RVINI_SCRIPT           = 'Script';
  RVINI_UNDERLINETYPE    = 'UnderlineType';
  RVINI_UNDERLINECOLOR    = 'UnderlineColor';
  RVINI_HOVERUNDERLINECOLOR = 'HoverUnderlineColor';

  RVINI_SELECTIONMODE    = 'SelectionMode';
  RVINI_SELECTIONSTYLE   = 'SelectionStyle';
  RVINI_SELCOLOR         = 'SelColor';
  RVINI_SELTEXTCOLOR     = 'SelTextColor';
  RVINI_ISELCOLOR        = 'ISelColor';
  RVINI_ISELTEXTCOLOR    = 'ISelTextColor';
  RVINI_CPCOLOR          = 'CheckpointColor';
  RVINI_CPEVCOLOR        = 'CheckpointEvColor';
  RVINI_PAGEBREAKCOLOR   = 'PageBreakColor';
  RVINI_SOFTPAGEBREAKCOLOR = 'SoftPageBreakColor';
  RVINI_LIVESPELLINGCOLOR = 'LiveSpellingColor';
  RVINI_FLOATLINECOLOR   = 'FloatLineColor';
  RVINI_SPECCHARCOLOR    = 'SpecCharColor';
  RVINI_USESOUND         = 'UseSound';
  RVINI_DEFUNICODESTYLE  = 'DefUnicodeStyle';
  RVINI_DEFCODEPAGE      = 'DefCodePage';
  RVINI_LINESELECTCURSOR = 'LineSelectCursor';
  RVINI_LINEWRAPMODE     = 'LineWrapMode';
  RVINI_UNITS            = 'Units';
  RVINI_FONTQUALITY      = 'FontQuality';
  {$IFDEF RICHVIEWDEF2010}
  RVINI_SELECTIONHANDLES = 'SelectionHandles';
  {$ENDIF}

  RVINI_FIELDHIGHLIGHTCOLOR = 'FieldHighlightColor';
  RVINI_FIELDHIGHLIGHTTYPE = 'FieldHighlightType';
  RVINI_DISABLEDFONTCOLOR = 'DisabledFontColor';  
  RVINI_FOOTNOTENUMBERING = 'FootnoteNumbering';
  RVINI_FOOTNOTEPAGERESET = 'FootnotePageReset';
  RVINI_ENDNOTENUMBERING  = 'EndnoteNumbering';
  RVINI_SIDENOTENUMBERING = 'SidenoteNumbering';


  RVINI_LISTTYPE         = 'ListType';
  RVINI_IMAGEINDEX       = 'ImageIndex';
  RVINI_FORMATSTRING     = 'FormatString';
  RVINI_MARKERINDENT     = 'MarkerIndent';
  RVINI_MARKERALIGNMENT  = 'MarkerAlignment';
  RVINI_FORMATSTRINGW    = 'FormatStringW';
  RVINI_PICTURE          = 'Picture';
  RVINI_GRAPHICCLASS     = 'GraphicClass';

  RVINI_TABALIGN         = 'Align';
  RVINI_TABPOSITION      = 'Pos';
  RVINI_TABLEADER        = 'Leader';
  RVINI_TABPREFIX        = 'Tab%s';
  RVINI_TABCOUNT         = 'TabCount';

  RVINI_FONT             = 'Font';
  RVINI_LOCONTINUOUS     = 'Continuous';
  RVINI_LOLEVELRESET     = 'LevelReset';
  RVINI_LEVELSCOUNT      = 'LevelsCount';
  RVINI_ONELEVELPREVIEW  = 'OneLevelPreview';
  RVINI_LISTID           = 'ListId';
  RVINI_LEVELPREFIX      = 'Lvl%s';


  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  RVINI_STYLETEMPLATEID = 'StyleTemplateId';
  RVINI_PARASTYLETEMPLATEID = 'ParaStyleTemplateId';  

  RVINI_STCOUNT   = 'STItemCount';
  RVINI_STPREFIX  = 'ST%s';

  RVINI_ST_ID     = 'Id';
  RVINI_ST_NAME   = 'Name';
  RVINI_ST_PARENTID = 'ParentId';
  RVINI_ST_NEXTID = 'NextId';
  RVINI_ST_KIND   = 'Kind';
  RVINI_ST_QUICKACCESS = 'QuickAccess';
  RVINI_ST_VALIDPARAPROPERTIES = 'ValidParaProperties';
  RVINI_ST_VALIDTEXTPROPERTIES = 'ValidTextProperties';
  {$ENDIF}

  RVSTYLE_REG = 'RVStyle';
  {$ENDIF}
  RVINI_SINGLESYMBOLS    = 'SingleSymbols';

  RVWCEDIT = 'E'#0'd'#0'i'#0't'#0#0#0;
{============================= DOCX =====================================}
{$IFNDEF RVDONOTUSEDOCX}
const
  RVDOCX_XMLSTART = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
  RVDOCX_XMLNS_O = 'urn:schemas-microsoft-com:office:office';
  RVDOCX_XMLNS_M = 'http://schemas.openxmlformats.org/officeDocument/2006/math';
  RVDOCX_XMLNS_R = 'http://schemas.openxmlformats.org/officeDocument/2006/relationships';
  RVDOCX_XMLNS_V = 'urn:schemas-microsoft-com:vml';
  RVDOCX_XMLNS_VE = 'http://schemas.openxmlformats.org/markup-compatibility/2006';
  RVDOCX_XMLNS_W = 'http://schemas.openxmlformats.org/wordprocessingml/2006/main';
  RVDOCX_XMLNS_WP = 'http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing';
  RVDOCX_XMLNS_WP14 = 'http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing';
  RVDOCX_XMLNS_WPS = 'http://schemas.microsoft.com/office/word/2010/wordprocessingShape';
  RVDOCX_XMLNS_W10 = 'urn:schemas-microsoft-com:office:word';
  RVDOCX_XMLNS_WNE = 'http://schemas.microsoft.com/office/word/2006/wordml';
  RVDOCX_XMLNS_CP  ='http://schemas.openxmlformats.org/package/2006/metadata/core-properties';
  RVDOCX_XMLNS_DC  ='http://purl.org/dc/elements/1.1/';
  RVDOCX_XMLNS_MC  = 'http://schemas.openxmlformats.org/markup-compatibility/2006';
{$ENDIF}
{============================= Characters =====================================}
const
  RVCHAR_MIDDLEDOT       = {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
  RVCHAR_DEGREE          = {$IFDEF RVUNICODESTR}#$00B0{$ELSE}#$B0{$ENDIF};
  RVCHAR_NBSP            = {$IFDEF RVUNICODESTR}#$00A0{$ELSE}#$A0{$ENDIF};
  RVCHAR_LQUOTE          = {$IFDEF RVUNICODESTR}#$2018{$ELSE}#$91{$ENDIF};
  RVCHAR_RQUOTE          = {$IFDEF RVUNICODESTR}#$2019{$ELSE}#$92{$ENDIF};
  RVCHAR_LDQUOTE         = {$IFDEF RVUNICODESTR}#$201C{$ELSE}#$93{$ENDIF};
  RVCHAR_RDQUOTE         = {$IFDEF RVUNICODESTR}#$201D{$ELSE}#$94{$ENDIF};
  RVCHAR_RDLOWQUOTE      = {$IFDEF RVUNICODESTR}#$201E{$ELSE}#$84{$ENDIF};
  RVCHAR_LDANGLEQUOTE    = {$IFDEF RVUNICODESTR}#$00AB{$ELSE}#$AB{$ENDIF};
  RVCHAR_RDANGLEQUOTE    = {$IFDEF RVUNICODESTR}#$00BB{$ELSE}#$BB{$ENDIF};
  RVCHAR_ELLIPSIS        = {$IFDEF RVUNICODESTR}#$2026{$ELSE}#$85{$ENDIF};
{================================== Misc ======================================}
const
  RVDEFAULTCHECKPOINTPREFIX = 'RichViewCheckpoint';
  RVDEFAULTCHARACTER        = '?';
  RVDEFAULTDELIMITERS       = ' .;,:(){}"/\<>!?[]'+RVCHAR_LQUOTE+RVCHAR_RQUOTE+
    RVCHAR_LDQUOTE+RVCHAR_RDQUOTE+'-+*='+RVCHAR_NBSP+RVCHAR_RDLOWQUOTE+
    RVCHAR_LDANGLEQUOTE+RVCHAR_RDANGLEQUOTE+RVCHAR_ELLIPSIS;
const RVAddress = 'http://www.trichview.com';
      RVVersion = 'v14.15';
      RVPalettePage = 'RichView';
      RVNOSTYLEMSG = 'Style is not defined'#13'Create a TRVStyle object and assign it to %s.Style';
      RVFTagEmptyStr = '0';
  { Names of Clipboard formats }
const
  RVFORMATNAME = 'RichView Format';
  RTFORMATNAME = 'Rich Text Format';
  URLFORMATNAME = 'UniformResourceLocator';
  HTMLFORMATNAME = 'HTML Format';
  { Substring in HTML Clipboard contents }
  HTMLClipboardSourceURL = TRVAnsiString('SourceURL:');
const
  { Default names of styles, default font names }
  RVDEFSTYLENAME0 = 'Normal text';
  RVDEFSTYLENAME1 = 'Heading';
  RVDEFSTYLENAME2 = 'Subheading';
  RVDEFSTYLENAME3 = 'Keywords';
  RVDEFSTYLENAME4 = 'Jump 1';
  RVDEFSTYLENAME5 = 'Jump 2';
  RVDEFPARASTYLENAME1 = 'Centered';
  RVDEFAULTDESIGNFONT = 'MS Sans Serif';
  RVDEFAULTSTYLEFONT  = 'Arial';
  RVDEFAULTTEXTSTYLENAME = 'Font Style';
  RVDEFAULTPARASTYLENAME = 'Paragraph Style';
  RVDEFAULTLISTSTYLENAME = 'List Style';
  RVDEFAULTSTYLETEMPLATENAME = 'Style %d';

  RVLISTLEVELDISPLAYNAME = '%s %d/%d/%d';

  RVFONT_SYMBOL = 'Symbol';
  RVFONT_WINGDINGS = 'Wingdings';
  RVFONT_ARIAL = 'Arial';
  RVFONT_TIMESNEWROMAN = 'Times New Roman';
  RVFONT_TAHOMA = 'Tahoma';

  RVListTypeStr: array [0..9] of PChar =
    ('bullet', 'pic', 'image-list', '1,2,3', 'a,b,c', 'A,B,C', 'i,ii,iii',
    'I,II,III', 'image-list counter', 'unicode bullet');
  RVAlignStr: array [0..2] of PChar =
    ('left', 'right', 'center');

  { Not strings but Clipboard formats }
var CFRV_RVF, CFRV_RTF, CFRV_HTML, CFRV_URL: Word;

implementation

initialization
  CFRV_RVF := RegisterClipboardFormat(RVFORMATNAME);
  CFRV_RTF := RegisterClipboardFormat(RTFORMATNAME);
  CFRV_URL := RegisterClipboardFormat(URLFORMATNAME);
  CFRV_HTML := RegisterClipboardFormat(HTMLFORMATNAME);  

end.