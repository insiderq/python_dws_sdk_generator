﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVAnimate.pas' rev: 27.00 (Windows)

#ifndef RvanimateHPP
#define RvanimateHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvanimate
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVAnimator;
class DELPHICLASS TRVAnimatorList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVAnimator : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Types::TRect FRVRVDataRect;
	System::Types::TRect FRVClientRect;
	System::Types::TRect FSRVClientRect;
	float FZoomPercent;
	TRVAnimatorList* List;
	
protected:
	int FrameIndex;
	Rvitem::TCustomRVItemInfo* Item;
	int Interval;
	bool Moved;
	virtual int __fastcall GetFrameCount(void) = 0 ;
	bool __fastcall IsVisible(void);
	void __fastcall CalcNextFrameIndex(void);
	void __fastcall DrawFrame(void);
	virtual void __fastcall ResetBackground(void);
	void __fastcall MakeBackground(Vcl::Graphics::TBitmap* &bmp, System::Uitypes::TColor &BackColor);
	
public:
	Crvfdata::TCustomRVFormattedData* RVData;
	__fastcall TRVAnimator(Crvfdata::TCustomRVFormattedData* ARVData, Rvitem::TCustomRVItemInfo* AItem);
	void __fastcall Update(Crvfdata::TCustomRVFormattedData* ARVData, Rvitem::TCustomRVItemInfo* AItem);
	__fastcall virtual ~TRVAnimator(void);
	virtual void __fastcall Reset(void) = 0 ;
	virtual void __fastcall ChangeFrame(void) = 0 ;
	virtual void __fastcall Draw(int X, int Y, Vcl::Graphics::TCanvas* Canvas, bool Animation) = 0 ;
	virtual System::Types::TSize __fastcall GetExportImageSize(void);
	virtual void __fastcall DrawForExport(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual bool __fastcall ExportIgnoresScale(void);
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVAnimatorList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	bool Active;
	int LastMinInterval;
	int MinInterval;
	__fastcall TRVAnimatorList(void);
	__fastcall virtual ~TRVAnimatorList(void);
	void __fastcall TimerEvent(void);
	virtual void __fastcall Clear(void);
	void __fastcall FreeAnimators(void);
	void __fastcall Reset(void);
	HIDESBASE void __fastcall Add(TRVAnimator* &Item);
	void __fastcall ResetBackground(void);
};

#pragma pack(pop)

class DELPHICLASS TRVBitmapAnimator;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBitmapAnimator : public TRVAnimator
{
	typedef TRVAnimator inherited;
	
protected:
	virtual int __fastcall GetFrameCount(void);
	
public:
	virtual void __fastcall Reset(void);
	virtual void __fastcall ChangeFrame(void);
	virtual void __fastcall Draw(int X, int Y, Vcl::Graphics::TCanvas* Canvas, bool Animation);
	virtual void __fastcall DrawForExport(Vcl::Graphics::TCanvas* Canvas);
	virtual bool __fastcall ExportIgnoresScale(void);
public:
	/* TRVAnimator.Create */ inline __fastcall TRVBitmapAnimator(Crvfdata::TCustomRVFormattedData* ARVData, Rvitem::TCustomRVItemInfo* AItem) : TRVAnimator(ARVData, AItem) { }
	/* TRVAnimator.Destroy */ inline __fastcall virtual ~TRVBitmapAnimator(void) { }
	
};

#pragma pack(pop)

typedef void __fastcall (*TRVMakeAnimatorProc)(Rvitem::TCustomRVItemInfo* item, Crvfdata::TCustomRVFormattedData* RVData, TRVAnimator* &anim);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVMakeAnimatorProc RV_MakeAnimator;
extern DELPHI_PACKAGE int RVMaxAnimations;
}	/* namespace Rvanimate */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVANIMATE)
using namespace Rvanimate;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvanimateHPP
