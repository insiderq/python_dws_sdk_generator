﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVFloatingBox.pas' rev: 27.00 (Windows)

#ifndef RvfloatingboxHPP
#define RvfloatingboxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvfloatingbox
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVBoxWidthType : unsigned char { rvbwtAbsolute, rvbwtRelPage, rvbwtRelMainTextArea, rvbwtRelLeftMargin, rvbwtRelRightMargin, rvbwtRelInnerMargin, rvbwtRelOuterMargin };

enum DECLSPEC_DENUM TRVBoxHeightType : unsigned char { rvbhtAbsolute, rvbhtRelPage, rvbhtRelMainTextArea, rvbhtRelTopMargin, rvbhtRelBottomMargin, rvbhtAuto };

typedef int TRVFloatSize;

class DELPHICLASS TRVBoxPaddingRect;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBoxPaddingRect : public Rvstyle::TRVRect
{
	typedef Rvstyle::TRVRect inherited;
	
public:
	__fastcall TRVBoxPaddingRect(System::Classes::TPersistent* AOwner);
	
__published:
	__property Left = {default=10};
	__property Right = {default=10};
	__property Top = {default=5};
	__property Bottom = {default=5};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVBoxPaddingRect(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVBoxPaddingRect(void) : Rvstyle::TRVRect() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBoxBorder;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBoxBorder : public Rvstyle::TRVBorder
{
	typedef Rvstyle::TRVBorder inherited;
	
protected:
	virtual Rvstyle::TRVRect* __fastcall CreateBorderOffsets(void);
	DYNAMIC bool __fastcall AllowNegativeOffsets(void);
	
public:
	__fastcall TRVBoxBorder(System::Classes::TPersistent* AOwner);
	
__published:
	__property Style = {default=1};
public:
	/* TRVBorder.Destroy */ inline __fastcall virtual ~TRVBoxBorder(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBoxBackgroundRect;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBoxBackgroundRect : public Rvstyle::TRVBackgroundRect
{
	typedef Rvstyle::TRVBackgroundRect inherited;
	
protected:
	virtual Rvstyle::TRVRect* __fastcall CreateBorderOffsets(void);
	DYNAMIC bool __fastcall AllowNegativeOffsets(void);
	
public:
	__fastcall TRVBoxBackgroundRect(System::Classes::TPersistent* AOwner);
	
__published:
	__property Color = {default=-16777211};
public:
	/* TRVBackgroundRect.Destroy */ inline __fastcall virtual ~TRVBoxBackgroundRect(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBoxProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBoxProperties : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TRVBoxBorder* FBorder;
	TRVBoxBackgroundRect* FBackground;
	TRVFloatSize FWidth;
	TRVFloatSize FHeight;
	TRVBoxWidthType FWidthType;
	TRVBoxHeightType FHeightType;
	Vcl::Stdctrls::TTextLayout FVAlign;
	void __fastcall SetBorder(TRVBoxBorder* Value);
	int __fastcall GetPropertyIdentifier(const Rvtypes::TRVAnsiString PropName);
	void __fastcall SetBackground(TRVBoxBackgroundRect* const Value);
	
public:
	__fastcall TRVBoxProperties(void);
	__fastcall virtual ~TRVBoxProperties(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle);
	bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	bool __fastcall GetExtraIntPropertyReformat(int Prop, Rvitem::TRVReformatType &ReformatType);
	int __fastcall GetChangedPropertyCount(void);
	void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	int __fastcall GetTotalLeftPadding(Rvstyle::TRVStyle* RVStyle);
	int __fastcall GetTotalRightPadding(Rvstyle::TRVStyle* RVStyle);
	int __fastcall GetTotalTopPadding(Rvstyle::TRVStyle* RVStyle);
	int __fastcall GetTotalBottomPadding(Rvstyle::TRVStyle* RVStyle);
	int __fastcall GetTotalLeftPaddingSaD(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice PSaD);
	int __fastcall GetTotalRightPaddingSaD(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice PSaD);
	int __fastcall GetTotalTopPaddingSaD(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice PSaD);
	int __fastcall GetTotalBottomPaddingSaD(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice PSaD);
	int __fastcall GetMinWidth(Rvstyle::TRVStyle* RVStyle);
	int __fastcall GetMinHeight(Rvstyle::TRVStyle* RVStyle);
	System::Types::TRect __fastcall GetInternalRect(const System::Types::TRect &ExternalRect, Rvstyle::TRVStyle* RVStyle);
	__property TRVBoxBorder* Border = {read=FBorder, write=SetBorder};
	__property TRVBoxBackgroundRect* Background = {read=FBackground, write=SetBackground};
	__property TRVFloatSize Width = {read=FWidth, write=FWidth, default=100};
	__property TRVFloatSize Height = {read=FHeight, write=FHeight, default=100};
	__property TRVBoxWidthType WidthType = {read=FWidthType, write=FWidthType, default=0};
	__property TRVBoxHeightType HeightType = {read=FHeightType, write=FHeightType, default=5};
	__property Vcl::Stdctrls::TTextLayout VAlign = {read=FVAlign, write=FVAlign, default=0};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const System::Word rveipcBoxWidth = System::Word(0x258);
static const System::Word rveipcBoxHeight = System::Word(0x259);
static const System::Word rveipcBoxWidthType = System::Word(0x25a);
static const System::Word rveipcBoxHeightType = System::Word(0x25b);
static const System::Word rveipcBoxBorderColor = System::Word(0x25c);
static const System::Word rveipcBoxBorderWidth = System::Word(0x25d);
static const System::Word rveipcBoxBorderInternalWidth = System::Word(0x25e);
static const System::Word rveipcBoxBorderStyle = System::Word(0x25f);
static const System::Word rveipcBoxBorderVisibleBorders_Left = System::Word(0x260);
static const System::Word rveipcBoxBorderVisibleBorders_Top = System::Word(0x261);
static const System::Word rveipcBoxBorderVisibleBorders_Right = System::Word(0x262);
static const System::Word rveipcBoxBorderVisibleBorders_Bottom = System::Word(0x263);
static const System::Word rveipcBoxBorderBorderOffsets_Left = System::Word(0x264);
static const System::Word rveipcBoxBorderBorderOffsets_Top = System::Word(0x265);
static const System::Word rveipcBoxBorderBorderOffsets_Right = System::Word(0x266);
static const System::Word rveipcBoxBorderBorderOffsets_Bottom = System::Word(0x267);
static const System::Word rveipcBoxColor = System::Word(0x268);
static const System::Word rveipcBoxPadding_Left = System::Word(0x269);
static const System::Word rveipcBoxPadding_Top = System::Word(0x26a);
static const System::Word rveipcBoxPadding_Right = System::Word(0x26b);
static const System::Word rveipcBoxPadding_Bottom = System::Word(0x26c);
static const System::Word rveipcBoxVAlign = System::Word(0x26d);
static const System::Int8 DEFAULT_WIDTH = System::Int8(0x64);
static const System::Int8 DEFAULT_HEIGHT = System::Int8(0x64);
static const System::Int8 DEFAULT_HPADDING = System::Int8(0xa);
static const System::Int8 DEFAULT_VPADDING = System::Int8(0x5);
}	/* namespace Rvfloatingbox */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVFLOATINGBOX)
using namespace Rvfloatingbox;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvfloatingboxHPP
