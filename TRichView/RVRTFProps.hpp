﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVRTFProps.pas' rev: 27.00 (Windows)

#ifndef RvrtfpropsHPP
#define RvrtfpropsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <RVRTF.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVMapWht.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvrtfprops
{
//-- type declarations -------------------------------------------------------
typedef void __fastcall (__closure *TRVAllowUseStyleEvent)(int StyleNo, bool TextStyle, bool &Allow);

typedef void __fastcall (__closure *TRVCustomImageItemEvent)(Crvdata::TCustomRVData* RVData, Vcl::Graphics::TGraphic* Graphic, bool Hypertext, Rvitem::TCustomRVItemInfo* &item, bool &FreeGraphic, Rvrtf::TRVRTFPicture* RTFPicture, const System::UnicodeString FileName, Rvtypes::TRVRawByteString &Name);

typedef System::TMetaClass* TRVRTFReaderClass;

class DELPHICLASS TRVRTFStyleComparer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFStyleComparer : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	virtual bool __fastcall AreMatched(Rvstyle::TFontInfo* TextStyle, Rvrtf::TRVRTFCharProperties* RTFCharProps) = 0 ;
	virtual int __fastcall SimilarityValue(Rvstyle::TCustomRVFontInfo* TextStyle, Rvrtf::TRVRTFCharProperties* RTFCharProps) = 0 ;
	virtual void __fastcall AssignFromRTF(Rvstyle::TCustomRVFontInfo* TextStyle, Rvrtf::TRVRTFCharProperties* RTFCharProps) = 0 ;
public:
	/* TObject.Create */ inline __fastcall TRVRTFStyleComparer(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFStyleComparer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFReaderProperties;
class PASCALIMPLEMENTATION TRVRTFReaderProperties : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	Rvrtf::TRVRTFHeaderFooterType HFType;
	Rvstyle::TRVReaderUnicode FUnicodeMode;
	Rvstyle::TRVReaderStyleMode FTextStyleMode;
	Rvstyle::TRVReaderStyleMode FParaStyleMode;
	bool FIgnorePictures;
	bool FIgnoreTables;
	bool FIgnoreBookmarks;
	bool FIgnoreNotes;
	bool FIgnoreSequences;
	bool FUseHypertextStyles;
	int FParaNo;
	int FStyleNo;
	bool FExplicitTableWidth;
	bool FSkipHiddenText;
	bool FHideTableGridLines;
	bool FLineBreaksAsParagraphs;
	Crvdata::TCustomRVData* FHeaderRVData;
	Crvdata::TCustomRVData* FFooterRVData;
	Crvdata::TCustomRVData* FFirstPageHeaderRVData;
	Crvdata::TCustomRVData* FFirstPageFooterRVData;
	Crvdata::TCustomRVData* FEvenPagesHeaderRVData;
	Crvdata::TCustomRVData* FEvenPagesFooterRVData;
	TRVRTFStyleComparer* FStyleComparer;
	System::UnicodeString FBookmarkName;
	int FEmptyPara;
	Crvdata::TCustomRVData* RVData;
	Crvdata::TCustomRVData* CurrentRVData;
	Rvstyle::TRVStyle* RVStyle;
	bool PageBreak;
	Crvdata::TCustomRVData* PageBreakRVData;
	double PixelsPerTwip;
	Rvrtf::TRVRTFReader* Reader;
	bool FirstTime;
	bool FUseCharsetForUnicode;
	System::Uitypes::TFontCharset FCharsetForUnicode;
	int InsertPoint;
	int CurrentRow;
	int CurrentCol;
	Rvclasses::TRVList* SubDataList;
	int FHeaderYMM;
	int FFooterYMM;
	Rvscroll::TRVRTFHighlight FConvertHighlight;
	System::UnicodeString FBasePath;
	bool FExtractMetafileBitmaps;
	bool FBasePathLinks;
	bool FUseSingleCellPadding;
	bool IgnoreLists;
	int LastMarkerIndex;
	bool IsLastMarkerWord97;
	Rvclasses::TRVIntegerList* LevelToListNo;
	Rvclasses::TRVIntegerList* *FListTableMap97;
	System::StaticArray<Rvclasses::TRVIntegerList*, 7> ListTableMaps97;
	int ListStylesCountBefore;
	int LastNoteRefIndex;
	int NoteCharTextStyleNo;
	bool FReadDocParameters;
	Rvclasses::TRVIntegerList* FStyleTemplateMap;
	Rvclasses::TRVIntegerList* FStyleTemplateToTemplateMap;
	Rvstyle::TRVStyleTemplateCollection* FRTFStyleTemplates;
	Rvstyle::TRVStyleTemplate* FRTFParaStyleTemplate;
	Rvstyle::TRVStyleTemplate* FParaStyleTemplate;
	bool FStoreImagesFileNames;
	void __fastcall InitReader(void);
	void __fastcall DoneReader(void);
	int __fastcall ReturnParaNo(Rvrtf::TRVRTFPosition Position);
	Rvitem::TRVTextItemInfo* __fastcall CreateTextItem(const Rvtypes::TRVAnsiString Text, const System::WideString WideText, int StyleNo, int ParaNo, bool UseUnicode, Rvtypes::TRVRawByteString &ResText);
	int __fastcall GetMarkerIndex(Rvrtf::TRVRTFMarkerProperties* RTFMarker);
	bool __fastcall InsertMarker(int ParaNo);
	bool __fastcall InsertMarker97(int ParaNo);
	void __fastcall ReaderUpdateMarker(System::TObject* Sender);
	void __fastcall CopyListLevel97(Rvstyle::TRVListLevel* RVLevel, Rvrtf::TRVRTFListLevel97* RTFLevel);
	void __fastcall MergeListTable97(void);
	bool __fastcall AreListStylesEqual97(Rvstyle::TRVListInfo* RVList, Rvrtf::TRVRTFList97* RTFList);
	int __fastcall FindListStyle97(Rvrtf::TRVRTFList97* RTFList, Rvclasses::TRVIntegerList* ForbiddenStyles);
	void __fastcall ExtractLastChar(System::WideChar &Ch, int &StyleNo);
	void __fastcall InsertItem(Rvtypes::TRVRawByteString &Text, Rvitem::TCustomRVItemInfo* item, Rvrtf::TRVRTFPosition Position);
	void __fastcall GetRTFBorder(Rvrtf::TRVRTFParaProperties* ParaProps, Rvstyle::TRVBorderStyle &RVBorderStyle, Rvstyle::TRVStyleLength &RVBorderWidth, Rvstyle::TRVStyleLength &RVBorderIntWidth, System::Uitypes::TColor &RVBorderColor, Rvstyle::TRVRect* &RVBorderOffs);
	int __fastcall FindParaNo(Rvstyle::TRVBorderStyle RVBorderStyle, int RVBorderWidth, int RVBorderIntWidth, System::Uitypes::TColor RVBorderColor, Rvstyle::TRVRect* RVBorderOffs);
	int __fastcall GetEmptyParaNo(Rvstyle::TRVAlignment Alignment, int LeftIndentTw);
	int __fastcall GetEmptyStyleNo(void);
	int __fastcall FindBestParaNo(void);
	int __fastcall FindStyleNo(bool AUnicode, bool AHypertext, bool SwitchProtect, int ParaNo);
	int __fastcall FindBestStyleNo(bool AUnicode, bool AHypertext, bool ASwitchProtect, int ParaNo);
	void __fastcall ConvertParaStyleFromRTF(Rvrtf::TRVRTFParaProperties* ParaProps, Rvstyle::TCustomRVParaInfo* ParaStyle, Rvstyle::TRVBorderStyle RVBorderStyle, Rvstyle::TRVStyleLength RVBorderWidth, Rvstyle::TRVStyleLength RVBorderIntWidth, System::Uitypes::TColor RVBorderColor, Rvstyle::TRVRect* RVBorderOffs);
	void __fastcall MakeParaStyle(Rvstyle::TParaInfo* ParaStyle, Rvstyle::TRVBorderStyle RVBorderStyle, Rvstyle::TRVStyleLength RVBorderWidth, Rvstyle::TRVStyleLength RVBorderIntWidth, System::Uitypes::TColor RVBorderColor, Rvstyle::TRVRect* RVBorderOffs);
	void __fastcall ConvertTextStyleFromRTF(Rvrtf::TRVRTFCharProperties* CharProps, Rvstyle::TCustomRVFontInfo* TextStyle, bool AUnicode, int ParaNo);
	void __fastcall MakeTextStyle(Rvstyle::TFontInfo* TextStyle, bool AUnicode, bool AHypertext, bool ASwitchProtect, int ParaNo);
	int __fastcall ReturnParaNo_(void);
	int __fastcall ReturnStyleNo(bool AUnicode, bool ASwitchProtect, int ParaNo);
	Rvrtf::TRVRTFReaderState* __fastcall FindHypertextState(void);
	bool __fastcall IsHypertext_(void);
	bool __fastcall IsHypertext(System::UnicodeString &Target, System::UnicodeString &Hint, System::UnicodeString &Extras);
	bool __fastcall AllowUseStyle(int StyleNo, bool TextStyle);
	void __fastcall SetParaNo(const int Value);
	void __fastcall SetStyleNo(const int Value);
	void __fastcall SetHideTableGridLines(const bool Value);
	void __fastcall SetIgnoreBookmarks(const bool Value);
	void __fastcall SetIgnoreNotes(const bool Value);
	void __fastcall SetIgnorePictures(const bool Value);
	void __fastcall SetIgnoreSequences(const bool Value);
	void __fastcall SetIgnoreTables(const bool Value);
	void __fastcall SetLineBreaksAsParagraphs(const bool Value);
	void __fastcall SetParaStyleMode(const Rvstyle::TRVReaderStyleMode Value);
	void __fastcall SetReadDocParameters(const bool Value);
	void __fastcall SetUseSingleCellPadding(const bool Value);
	void __fastcall SetExtractMetafileBitmaps(const bool Value);
	void __fastcall SetConvertHighlight(const Rvscroll::TRVRTFHighlight Value);
	void __fastcall SetStoreImagesFileNames(const bool Value);
	void __fastcall SetBasePathLinks(const bool Value);
	void __fastcall SetCharsetForUnicode(const System::Uitypes::TFontCharset Value);
	void __fastcall SetUseCharsetForUnicode(const bool Value);
	void __fastcall SetSkipHiddenText(const bool Value);
	void __fastcall SetTextStyleMode(const Rvstyle::TRVReaderStyleMode Value);
	void __fastcall SetUnicodeMode(const Rvstyle::TRVReaderUnicode Value);
	void __fastcall SetUseHypertextStyles(const bool Value);
	
protected:
	void __fastcall ReaderProgress(Rvrtf::TRVRTFReader* Sender, Rvrtf::TRVRTFProgressStage Stage, System::Byte PercentDone);
	void __fastcall NewReaderText(Rvrtf::TRVRTFReader* Sender, const Rvtypes::TRVAnsiString Text, Rvrtf::TRVRTFPosition Position);
	void __fastcall NewReaderSeq(Rvrtf::TRVRTFReader* Sender, Rvrtf::TRVRTFPosition Position, const System::UnicodeString SeqName, Rvrtf::TRVRTFSeqType NumberingType, bool Reset, int StartFrom);
	void __fastcall ReaderNote(Rvrtf::TRVRTFReader* Sender, Rvrtf::TRVRTFNoteEventType What, Rvrtf::TRVRTFPosition Position);
	void __fastcall ReaderEndParsing(System::TObject* Sender);
	void __fastcall NewReaderUnicodeText(Rvrtf::TRVRTFReader* Sender, const Rvtypes::TRVUnicodeString Text, Rvrtf::TRVRTFPosition Position);
	void __fastcall NewReaderPicture(Rvrtf::TRVRTFReader* Sender, Rvrtf::TRVRTFPicture* RTFPicture, Vcl::Graphics::TGraphic* Graphic, Rvrtf::TRVRTFPosition Position, const System::UnicodeString FileName, bool &Inserted);
	void __fastcall ReaderImportPicture(Rvrtf::TRVRTFReader* Sender, const System::UnicodeString Location, Vcl::Graphics::TGraphic* &Graphic, bool &Invalid);
	void __fastcall AssignRowProperties(void);
	void __fastcall ReaderTable(Rvrtf::TRVRTFReader* Sender, Rvrtf::TRVRTFTableEventKind WhatHappens, bool &Processed);
	void __fastcall ConvertStyleSheetToStyleTemplates(Rvstyle::TRVStyleTemplateCollection* StyleTemplates, Rvclasses::TRVIntegerList* Map);
	void __fastcall ReaderStyleSheet(System::TObject* Sender);
	void __fastcall ReaderPageBreak(System::TObject* Sender);
	void __fastcall ReaderHeaderFooter(Rvrtf::TRVRTFReader* Sender, Rvrtf::TRVRTFHeaderFooterType HFType, bool Starting, bool &Supported);
	void __fastcall ReaderBookmarkStart(Rvrtf::TRVRTFReader* Sender, const System::UnicodeString BookmarkName);
	DYNAMIC void __fastcall ReaderTranslateKeyword(Rvrtf::TRVRTFReader* Sender, const Rvtypes::TRVAnsiString Keyword, int Param, bool fParam, Rvtypes::TRVAnsiString &Text, bool &DoDefault);
	bool __fastcall IsInHeaderOrFooter(void);
	virtual void __fastcall Changed(void);
	
public:
	bool EditFlag;
	bool InsertFlag;
	Rvrtferr::TRVRTFErrorCode ErrorCode;
	bool FailedBecauseOfProtect;
	bool FullReformat;
	int NonFirstItemsAdded;
	bool AllowNewPara;
	TRVAllowUseStyleEvent OnAllowUseStyle;
	TRVCustomImageItemEvent OnCustomImageItem;
	int Index;
	__fastcall virtual TRVRTFReaderProperties(void);
	__fastcall virtual ~TRVRTFReaderProperties(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	Rvrtferr::TRVRTFErrorCode __fastcall ReadFromFile(const System::UnicodeString AFileName, Crvdata::TCustomRVData* ARVData, bool InsertMode = false);
	Rvrtferr::TRVRTFErrorCode __fastcall ReadFromStream(System::Classes::TStream* AStream, Crvdata::TCustomRVData* ARVData, bool InsertMode = false);
	Rvrtferr::TRVRTFErrorCode __fastcall InsertFromStreamEd(System::Classes::TStream* AStream, Crvdata::TCustomRVData* ARVData, int &AIndex);
	__property bool ExplicitTableWidth = {read=FExplicitTableWidth, write=FExplicitTableWidth, default=0};
	Crvdata::TCustomRVData* __fastcall GetHFRVData(Rvrtf::TRVRTFHeaderFooterType HFType);
	virtual TRVRTFReaderClass __fastcall GetRVRTFReaderClass(void);
	void __fastcall SetHeader(Crvdata::TCustomRVData* RVData, Rvstyle::TRVHFType HeaderType = (Rvstyle::TRVHFType)(0x0));
	void __fastcall SetFooter(Crvdata::TCustomRVData* RVData, Rvstyle::TRVHFType FooterType = (Rvstyle::TRVHFType)(0x0));
	__property int HeaderYMM = {read=FHeaderYMM, nodefault};
	__property int FooterYMM = {read=FFooterYMM, nodefault};
	__property System::UnicodeString BasePath = {read=FBasePath, write=FBasePath};
	__property Rvrtf::TRVRTFReader* RTFReader = {read=Reader};
	__property TRVRTFStyleComparer* StyleComparer = {read=FStyleComparer, write=FStyleComparer};
	__property Crvdata::TCustomRVData* HeaderRVData = {read=FHeaderRVData};
	__property Crvdata::TCustomRVData* FooterRVData = {read=FFooterRVData};
	__property Crvdata::TCustomRVData* FirstPageHeaderRVData = {read=FFirstPageHeaderRVData};
	__property Crvdata::TCustomRVData* FirstPageFooterRVData = {read=FFirstPageFooterRVData};
	__property Crvdata::TCustomRVData* EvenPagesHeaderRVData = {read=FEvenPagesHeaderRVData};
	__property Crvdata::TCustomRVData* EvenPagesFooterRVData = {read=FEvenPagesFooterRVData};
	
__published:
	__property Rvstyle::TRVReaderUnicode UnicodeMode = {read=FUnicodeMode, write=SetUnicodeMode, default=2};
	__property Rvstyle::TRVReaderStyleMode TextStyleMode = {read=FTextStyleMode, write=SetTextStyleMode, default=1};
	__property Rvstyle::TRVReaderStyleMode ParaStyleMode = {read=FParaStyleMode, write=SetParaStyleMode, default=1};
	__property bool IgnorePictures = {read=FIgnorePictures, write=SetIgnorePictures, default=0};
	__property bool IgnoreNotes = {read=FIgnoreNotes, write=SetIgnoreNotes, default=0};
	__property bool IgnoreSequences = {read=FIgnoreSequences, write=SetIgnoreSequences, default=0};
	__property bool IgnoreTables = {read=FIgnoreTables, write=SetIgnoreTables, default=0};
	__property bool IgnoreBookmarks = {read=FIgnoreBookmarks, write=SetIgnoreBookmarks, default=0};
	__property bool UseHypertextStyles = {read=FUseHypertextStyles, write=SetUseHypertextStyles, default=0};
	__property int TextStyleNo = {read=FStyleNo, write=SetStyleNo, default=0};
	__property int ParaStyleNo = {read=FParaNo, write=SetParaNo, default=0};
	__property bool SkipHiddenText = {read=FSkipHiddenText, write=SetSkipHiddenText, default=1};
	__property bool AutoHideTableGridLines = {read=FHideTableGridLines, write=SetHideTableGridLines, default=0};
	__property bool LineBreaksAsParagraphs = {read=FLineBreaksAsParagraphs, write=SetLineBreaksAsParagraphs, default=0};
	__property bool ReadDocParameters = {read=FReadDocParameters, write=SetReadDocParameters, default=0};
	__property bool UseSingleCellPadding = {read=FUseSingleCellPadding, write=SetUseSingleCellPadding, default=0};
	__property bool ExtractMetafileBitmaps = {read=FExtractMetafileBitmaps, write=SetExtractMetafileBitmaps, default=1};
	__property Rvscroll::TRVRTFHighlight ConvertHighlight = {read=FConvertHighlight, write=SetConvertHighlight, default=2};
	__property bool StoreImagesFileNames = {read=FStoreImagesFileNames, write=SetStoreImagesFileNames, default=0};
	__property bool BasePathLinks = {read=FBasePathLinks, write=SetBasePathLinks, default=1};
	__property bool UseCharsetForUnicode = {read=FUseCharsetForUnicode, write=SetUseCharsetForUnicode, default=0};
	__property System::Uitypes::TFontCharset CharsetForUnicode = {read=FCharsetForUnicode, write=SetCharsetForUnicode, default=1};
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE int RV_TableGridEps;
}	/* namespace Rvrtfprops */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVRTFPROPS)
using namespace Rvrtfprops;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvrtfpropsHPP
