﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CtrlImg.pas' rev: 27.00 (Windows)

#ifndef CtrlimgHPP
#define CtrlimgHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Ctrlimg
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* __fastcall DrawButton(Vcl::Stdctrls::TButton* ctrl, System::Uitypes::TColor Color = (System::Uitypes::TColor)(0x1fffffff));
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* __fastcall DrawEdit(Vcl::Stdctrls::TEdit* ctrl, System::Uitypes::TColor Color = (System::Uitypes::TColor)(0x1fffffff));
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* __fastcall DrawMemo(Vcl::Stdctrls::TMemo* ctrl, System::Uitypes::TColor Color = (System::Uitypes::TColor)(0x1fffffff));
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* __fastcall DrawGraphicControl(Vcl::Controls::TGraphicControl* ctrl, System::Uitypes::TColor Color = (System::Uitypes::TColor)(0x1fffffff));
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* __fastcall DrawPanel(Vcl::Extctrls::TPanel* ctrl, System::Uitypes::TColor Color = (System::Uitypes::TColor)(0x1fffffff));
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* __fastcall DrawControl(Vcl::Controls::TControl* ctrl, System::Uitypes::TColor Color = (System::Uitypes::TColor)(0x1fffffff));
}	/* namespace Ctrlimg */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CTRLIMG)
using namespace Ctrlimg;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CtrlimgHPP
