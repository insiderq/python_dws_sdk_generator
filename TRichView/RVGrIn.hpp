﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVGrIn.pas' rev: 27.00 (Windows)

#ifndef RvgrinHPP
#define RvgrinHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvgrin
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVGraphicInterface;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVGraphicInterface : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	virtual void __fastcall TextOut_A(Vcl::Graphics::TCanvas* Canvas, int X, int Y, char * Str, int Length) = 0 ;
	virtual void __fastcall TextOut_W(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::WideChar * Str, int Length) = 0 ;
	virtual void __fastcall TextOut_(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::WideChar * Str, int Length) = 0 ;
	virtual void __fastcall ExtTextOut_A(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int Options, System::Types::PRect Rect, char * Str, int Length, System::PInteger Dx) = 0 ;
	virtual void __fastcall ExtTextOut_W(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int Options, System::Types::PRect Rect, System::WideChar * Str, int Length, System::PInteger Dx) = 0 ;
	virtual void __fastcall ExtTextOut_(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int Options, System::Types::PRect Rect, System::WideChar * Str, int Length, System::PInteger Dx) = 0 ;
	virtual void __fastcall DrawText(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, System::Types::TRect &R, System::Classes::TAlignment Alignment) = 0 ;
	virtual void __fastcall GetTextExtentPoint32_A(Vcl::Graphics::TCanvas* Canvas, char * Str, int Length, System::Types::TSize &Size) = 0 ;
	virtual void __fastcall GetTextExtentPoint32_W(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, System::Types::TSize &Size) = 0 ;
	virtual void __fastcall GetTextExtentPoint32_(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, System::Types::TSize &Size) = 0 ;
	int __fastcall TextWidth(Vcl::Graphics::TCanvas* Canvas, const System::UnicodeString S);
	int __fastcall TextHeight(Vcl::Graphics::TCanvas* Canvas, const System::UnicodeString S);
	virtual void __fastcall GetTextExtentExPoint_A(Vcl::Graphics::TCanvas* Canvas, char * Str, int Length, int MaxExtent, System::PInteger PFit, System::PInteger PDX, System::Types::TSize &Size) = 0 ;
	virtual void __fastcall GetTextExtentExPoint_W(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, int MaxExtent, System::PInteger PFit, System::PInteger PDX, System::Types::TSize &Size) = 0 ;
	virtual unsigned __fastcall GetCharacterPlacement_A(Vcl::Graphics::TCanvas* Canvas, char * Str, int Length, tagGCP_RESULTSA &Results, bool Ligate) = 0 ;
	virtual unsigned __fastcall GetCharacterPlacement_W(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, tagGCP_RESULTSA &Results, bool Ligate) = 0 ;
	virtual unsigned __fastcall GetOutlineTextMetrics(Vcl::Graphics::TCanvas* Canvas, unsigned Size, Winapi::Windows::POutlineTextmetricW POTM) = 0 ;
	virtual bool __fastcall GetTextMetrics(Vcl::Graphics::TCanvas* Canvas, tagTEXTMETRICW &TM) = 0 ;
	virtual bool __fastcall FontHasLigationGlyphs(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual void __fastcall SetTextAlign(Vcl::Graphics::TCanvas* Canvas, unsigned Flags) = 0 ;
	virtual unsigned __fastcall GetTextAlign(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual int __fastcall GetTextCharacterExtra(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual void __fastcall SetTextCharacterExtra(Vcl::Graphics::TCanvas* Canvas, int CharExtra) = 0 ;
	virtual void __fastcall SetLogFont(Vcl::Graphics::TCanvas* Canvas, const tagLOGFONTW &LogFont) = 0 ;
	virtual NativeUInt __fastcall GetFontHandle(Vcl::Graphics::TCanvas* Canvas);
	virtual NativeUInt __fastcall SetFontHandle(Vcl::Graphics::TCanvas* Canvas, const NativeUInt FontHandle) = 0 ;
	virtual void __fastcall SetDiagonalBrush(Vcl::Graphics::TCanvas* Canvas, System::Uitypes::TColor BackColor, System::Uitypes::TColor ForeColor, int XOrg, int YOrg) = 0 ;
	virtual Vcl::Graphics::TCanvas* __fastcall CreateScreenCanvas(void) = 0 ;
	virtual void __fastcall DestroyScreenCanvas(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual Vcl::Graphics::TCanvas* __fastcall CreateCompatibleCanvas(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual void __fastcall DestroyCompatibleCanvas(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual Vcl::Graphics::TCanvas* __fastcall CreatePrinterCanvas(void) = 0 ;
	virtual void __fastcall GetViewportExtEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TSize &Size) = 0 ;
	virtual void __fastcall GetViewportOrgEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TPoint &Point) = 0 ;
	virtual void __fastcall GetWindowExtEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TSize &Size) = 0 ;
	virtual void __fastcall GetWindowOrgEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TPoint &Point) = 0 ;
	virtual void __fastcall SetViewportExtEx(Vcl::Graphics::TCanvas* Canvas, int XExt, int YExt, System::Types::PSize Size) = 0 ;
	virtual void __fastcall SetViewportOrgEx(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::Types::PPoint Point) = 0 ;
	virtual void __fastcall SetWindowExtEx(Vcl::Graphics::TCanvas* Canvas, int XExt, int YExt, System::Types::PSize Size) = 0 ;
	virtual void __fastcall SetWindowOrgEx(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::Types::PPoint Point) = 0 ;
	void __fastcall MoveWindowOrgAdvanced(Vcl::Graphics::TCanvas* Canvas, int DX, int DY);
	virtual int __fastcall GetMapMode(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual int __fastcall SetMapMode(Vcl::Graphics::TCanvas* Canvas, int Mode) = 0 ;
	virtual bool __fastcall SetGraphicsMode(Vcl::Graphics::TCanvas* Canvas, bool Advanced) = 0 ;
	virtual bool __fastcall IsAdvancedGraphicsMode(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual bool __fastcall GetWorldTransform(Vcl::Graphics::TCanvas* Canvas, tagXFORM &XForm) = 0 ;
	virtual bool __fastcall SetWorldTransform(Vcl::Graphics::TCanvas* Canvas, const tagXFORM &XForm) = 0 ;
	virtual bool __fastcall ModifyWorldTransform(Vcl::Graphics::TCanvas* Canvas, const tagXFORM &XForm) = 0 ;
	virtual void __fastcall InvalidateRect(Vcl::Controls::TWinControl* Control, System::Types::PRect lpRect) = 0 ;
	virtual void __fastcall IntersectClipRect(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2) = 0 ;
	virtual void __fastcall IntersectClipRectEx(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2, NativeUInt &Rgn, bool &RgnValid) = 0 ;
	virtual void __fastcall RemoveClipRgn(Vcl::Graphics::TCanvas* Canvas, NativeUInt &Rgn, bool &RgnValid) = 0 ;
	virtual void __fastcall RestoreClipRgn(Vcl::Graphics::TCanvas* Canvas, NativeUInt Rgn, bool RgnValid) = 0 ;
	virtual int __fastcall SaveCanvasState(Vcl::Graphics::TCanvas* Canvas) = 0 ;
	virtual void __fastcall RestoreCanvasState(Vcl::Graphics::TCanvas* Canvas, int State) = 0 ;
	virtual void __fastcall FillRect(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	virtual void __fastcall FillColorRect(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, System::Uitypes::TColor Color) = 0 ;
	virtual void __fastcall Rectangle(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2);
	virtual void __fastcall Polyline(Vcl::Graphics::TCanvas* Canvas, void *Points, int Count) = 0 ;
	virtual void __fastcall Polygon(Vcl::Graphics::TCanvas* Canvas, System::Types::TPoint const *Points, const int Points_High);
	void __fastcall Ellipse(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2);
	virtual void __fastcall DrawFocusRect(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect);
	virtual void __fastcall MoveTo(Vcl::Graphics::TCanvas* Canvas, int X, int Y);
	virtual void __fastcall LineTo(Vcl::Graphics::TCanvas* Canvas, int X, int Y);
	void __fastcall Line(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2);
	void __fastcall LineDragDropCaret(Vcl::Graphics::TCanvas* Canvas, int X, int Y1, int Y2);
	void __fastcall DrawTableResizeVLine(Vcl::Graphics::TCanvas* Canvas, int X, int Y1, int Y2);
	void __fastcall DrawTableResizeHLine(Vcl::Graphics::TCanvas* Canvas, int X1, int X2, int Y);
	virtual void __fastcall DrawBitmap(Vcl::Graphics::TCanvas* Canvas, int X, int Y, Vcl::Graphics::TBitmap* bmp) = 0 ;
	virtual void __fastcall StretchDrawBitmap(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, Vcl::Graphics::TBitmap* bmp) = 0 ;
	virtual void __fastcall DrawGraphic(Vcl::Graphics::TCanvas* Canvas, int X, int Y, Vcl::Graphics::TGraphic* Graphic);
	virtual void __fastcall StretchDrawGraphic(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, Vcl::Graphics::TGraphic* Graphic);
	virtual void __fastcall PrintBitmap(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, Vcl::Graphics::TBitmap* bmp);
	virtual void __fastcall CopyRect(Vcl::Graphics::TCanvas* DestCanvas, const System::Types::TRect &Dest, Vcl::Graphics::TCanvas* SourceCanvas, const System::Types::TRect &Source);
	virtual void __fastcall DrawImageList(Vcl::Graphics::TCanvas* Canvas, Vcl::Imglist::TCustomImageList* ImageList, int Index, int X, int Y, System::Uitypes::TColor BlendColor) = 0 ;
	virtual int __fastcall GetDeviceCaps(Vcl::Graphics::TCanvas* Canvas, int Index) = 0 ;
public:
	/* TObject.Create */ inline __fastcall TRVGraphicInterface(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVGraphicInterface(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvgrin */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVGRIN)
using namespace Rvgrin;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvgrinHPP
