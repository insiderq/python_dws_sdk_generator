﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVTypes.pas' rev: 27.00 (Windows)

#ifndef RvtypesHPP
#define RvtypesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.SysConst.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvtypes
{
//-- type declarations -------------------------------------------------------
typedef System::AnsiString TRVAnsiString;

typedef System::UnicodeString TRVUnicodeString;

typedef char TRVAnsiChar;

typedef System::WideChar TRVUnicodeChar;

typedef char * PRVAnsiChar;

typedef System::WideChar * PRVUnicodeChar;

typedef System::RawByteString TRVRawByteString;

typedef NativeUInt TRVHandle;

typedef System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)> TRVSetOfAnsiChar;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVAnsiString __fastcall RVFloatToStr(System::Extended Value);
extern DELPHI_PACKAGE System::Extended __fastcall RVStrToFloat(const TRVAnsiString S);
extern DELPHI_PACKAGE TRVAnsiString __fastcall RVIntToHex(int Value, int Digits);
extern DELPHI_PACKAGE TRVAnsiString __fastcall RVIntToStr(int Value);
extern DELPHI_PACKAGE int __fastcall RVStrToInt(TRVAnsiString Value);
extern DELPHI_PACKAGE int __fastcall RVStrToIntDef(TRVAnsiString Value, int Default);
extern DELPHI_PACKAGE int __fastcall RVPos(const TRVAnsiString substr, const TRVAnsiString str);
}	/* namespace Rvtypes */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVTYPES)
using namespace Rvtypes;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvtypesHPP
