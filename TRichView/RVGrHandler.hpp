﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVGrHandler.pas' rev: 27.00 (Windows)

#ifndef RvgrhandlerHPP
#define RvgrhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvgrhandler
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVGraphicHandler;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVGraphicHandler : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Classes::TList* FHTMLGraphicFormats;
	
protected:
	Vcl::Graphics::TGraphicClass FPngGraphicClass;
	Vcl::Graphics::TGraphicClass FJpegGraphicClass;
	
public:
	__fastcall TRVGraphicHandler(void);
	__fastcall virtual ~TRVGraphicHandler(void);
	DYNAMIC System::UnicodeString __fastcall GetGraphicExt(Vcl::Graphics::TGraphic* Graphic);
	virtual Rvfuncs::TRVGraphicType __fastcall GetGraphicType(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC Vcl::Graphics::TGraphicClass __fastcall GetGraphicClassByType(Rvfuncs::TRVGraphicType GraphicType);
	DYNAMIC bool __fastcall IsTransparent(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC bool __fastcall IsVectorGraphic(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC bool __fastcall IsResizeable(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC bool __fastcall IsHTMLGraphic(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC Vcl::Graphics::TGraphicClass __fastcall GetGraphicClass(const System::UnicodeString ClassName);
	Vcl::Graphics::TGraphic* __fastcall CreateGraphicByType(Rvfuncs::TRVGraphicType GraphicType);
	Vcl::Graphics::TGraphic* __fastcall CreateGraphic(Vcl::Graphics::TGraphicClass GraphicClass);
	DYNAMIC Vcl::Graphics::TGraphic* __fastcall LoadFromFile(const System::UnicodeString FileName);
	void __fastcall RegisterHTMLGraphicFormat(Vcl::Graphics::TGraphicClass GraphicClass);
	DYNAMIC void __fastcall PrepareGraphic(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC void __fastcall AfterImportGraphic(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC void __fastcall SetPalette(Vcl::Graphics::TGraphic* Graphic, Winapi::Windows::PLogPalette PLogPal);
	virtual void __fastcall RegisterPngGraphic(Vcl::Graphics::TGraphicClass GraphicClass);
	bool __fastcall IsPngClassAssigned(void);
	DYNAMIC HICON __fastcall GetIconHandle(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC NativeUInt __fastcall GetMetafileHandle(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC void __fastcall AfterLoadMetafile(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC void __fastcall GetMetafileMMSize(Vcl::Graphics::TGraphic* Graphic, System::Types::TSize &Size);
	DYNAMIC bool __fastcall IsEnhancedMetafile(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC void __fastcall SetMetafileEnhanced(Vcl::Graphics::TGraphic* Graphic, bool Enhanced);
	DYNAMIC void __fastcall SetMetafileHandle(Vcl::Graphics::TGraphic* Graphic, NativeUInt Handle);
	DYNAMIC void __fastcall SetBitmapHandle(Vcl::Graphics::TGraphic* Graphic, HBITMAP Handle);
	DYNAMIC int __fastcall GetBitmapScanLineWidth(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC Vcl::Graphics::TCanvas* __fastcall GetBitmapCanvas(Vcl::Graphics::TGraphic* Graphic);
	DYNAMIC bool __fastcall IsJpegClassAssigned(void);
	virtual void __fastcall RegisterJpegGraphic(Vcl::Graphics::TGraphicClass GraphicClass);
	DYNAMIC bool __fastcall CanConvertIconToPng(void);
	DYNAMIC Vcl::Graphics::TGraphic* __fastcall IconToPng(Vcl::Graphics::TGraphic* Graphic);
	Vcl::Graphics::TGraphic* __fastcall ExtractBitmapFromMetafile(Vcl::Graphics::TGraphic* Graphic);
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVGraphicHandler* RVGraphicHandler;
}	/* namespace Rvgrhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVGRHANDLER)
using namespace Rvgrhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvgrhandlerHPP
