{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       TRVGraphicHandler - class for managing          }
{       graphics.                                       }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVGrHandler;

interface

{$I RV_Defs.inc}
uses Windows, Graphics, Classes, SysUtils,
     {$IFNDEF RVDONOTUSEJPEGIMAGE}
     Jpeg,
     {$ENDIF}
     {$IFNDEF RVDONOTUSEPNGIMAGE}
     PngImage,
     {$ENDIF}
     {$IFDEF RVUSEDXPNGIMAGE}
     dxGDIPlusClasses, dxCore, dxCoreGraphics,
     {$ENDIF}
     RVFuncs;

type

  TRVGraphicHandler = class
    private
      FHTMLGraphicFormats: TList;
    protected
      FPngGraphicClass: TGraphicClass;
      FJpegGraphicClass: TGraphicClass;
    public
      constructor Create;
      destructor Destroy; override;
      // info about class
      function GetGraphicExt(Graphic: TGraphic): String; dynamic;
      function GetGraphicType(Graphic: TGraphic): TRVGraphicType; virtual;
      function GetGraphicClassByType(GraphicType: TRVGraphicType): TGraphicClass; dynamic;
      function IsTransparent(Graphic: TGraphic): Boolean; dynamic;
      function IsVectorGraphic(Graphic: TGraphic): Boolean; dynamic;
      function IsResizeable(Graphic: TGraphic): Boolean; dynamic;
      function IsHTMLGraphic(Graphic: TGraphic): Boolean; dynamic;
      // creation
      function GetGraphicClass(const ClassName: String): TGraphicClass; dynamic;
      function CreateGraphicByType(GraphicType: TRVGraphicType): TGraphic;
      function CreateGraphic(GraphicClass: TGraphicClass): TGraphic;
      function LoadFromFile(const FileName: String): TGraphic; dynamic;
      // registration
      procedure RegisterHTMLGraphicFormat(GraphicClass: TGraphicClass);
      // misc
      procedure PrepareGraphic(Graphic: TGraphic); dynamic;
      procedure AfterImportGraphic(Graphic: TGraphic); dynamic;
      procedure SetPalette(Graphic: TGraphic; PLogPal: PLogPalette); dynamic;
      // png
      procedure RegisterPngGraphic(GraphicClass: TGraphicClass); virtual;
      function IsPngClassAssigned: Boolean;
      // icon
      function GetIconHandle(Graphic: TGraphic): HICON; dynamic;
      // metafile
      function GetMetafileHandle(Graphic: TGraphic): HENHMETAFILE; dynamic;
      procedure AfterLoadMetafile(Graphic: TGraphic); dynamic;
      procedure GetMetafileMMSize(Graphic: TGraphic; var Size: TSize); dynamic;
      function IsEnhancedMetafile(Graphic: TGraphic): Boolean; dynamic;
      procedure SetMetafileEnhanced(Graphic: TGraphic; Enhanced: Boolean); dynamic;
      procedure SetMetafileHandle(Graphic: TGraphic; Handle: HENHMETAFILE); dynamic;
      // bitmap
      procedure SetBitmapHandle(Graphic: TGraphic; Handle: HBITMAP); dynamic;
      function GetBitmapScanLineWidth(Graphic: TGraphic): Integer; dynamic;
      function GetBitmapCanvas(Graphic: TGraphic): TCanvas; dynamic;
      // jpeg
      function IsJpegClassAssigned: Boolean; dynamic;
      procedure RegisterJpegGraphic(GraphicClass: TGraphicClass); virtual;
      // conversion
      function CanConvertIconToPng: Boolean; dynamic;
      function IconToPng(Graphic: TGraphic): TGraphic; dynamic;
      function ExtractBitmapFromMetafile(Graphic: TGraphic): TGraphic;
  end;

var RVGraphicHandler: TRVGraphicHandler;

implementation
uses RVTypes;

{================================ TRVGraphicHandler ===========================}
constructor TRVGraphicHandler.Create;
begin
  inherited Create;
  {$IFNDEF RVDONOTUSEPNGIMAGE}
  RegisterPngGraphic(TPngImage);
  {$ENDIF}
  {$IFDEF RVUSEDXPNGIMAGE}
  RegisterPngGraphic(TdxPNGImage);
  {$ENDIF}
  {$IFNDEF RVDONOTUSEJPEGIMAGE}
  RegisterJpegGraphic(TJPEGImage);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
destructor TRVGraphicHandler.Destroy;
begin
  FHTMLGraphicFormats.Free;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.IsHTMLGraphic(Graphic: TGraphic): Boolean;
begin
  Result := (GetGraphicType(Graphic) in [rvgtJPEG, rvgtPNG]) or
   ((FHTMLGraphicFormats<>nil) and
    (FHTMLGraphicFormats.IndexOf(Graphic.ClassType)>=0));
end;
{------------------------------------------------------------------------------}
procedure TRVGraphicHandler.RegisterHTMLGraphicFormat(
  GraphicClass: TGraphicClass);
begin
  if FHTMLGraphicFormats=nil then
    FHTMLGraphicFormats := TList.Create;
  if FHTMLGraphicFormats.IndexOf(GraphicClass)<0 then
    FHTMLGraphicFormats.Add(GraphicClass);
end;
{------------------------------------------------------------------------------}
{ Returns a file extension for Graphic }
function TRVGraphicHandler.GetGraphicExt(Graphic: TGraphic): String;
begin
  {$IFDEF RICHVIEWDEF2010}
  if Graphic is TWicImage then
    case TWicImage(Graphic).ImageFormat of
      wifBmp:  Result := 'bmp';
      wifPng:  Result := 'png';
      wifJpeg: Result := 'jpg';
      wifGif:  Result := 'gif';
      wifTiff: Result := 'tif';
      else Result := GraphicExtension(TGraphicClass(Graphic.ClassType))
    end
  else
  {$ENDIF}
  {$IFDEF RVUSEDXPNGIMAGE}
  if Graphic is TdxSmartImage then
    case TdxSmartImage(Graphic).ImageDataFormat of
      dxImageBitmap: Result := 'bmp';
      dxImageJpeg:   Result := 'jpg';
      dxImagePng:    Result := 'png';
      dxImageTiff:   Result := 'tif';
      dxImageGif:    Result := 'gif';
      else Result := GraphicExtension(TGraphicClass(Graphic.ClassType))
    end
  else
  {$ENDIF}
    Result := GraphicExtension(TGraphicClass(Graphic.ClassType))
end;
{------------------------------------------------------------------------------}
{ Returns a graphic type for Graphic. This method is used to create an instance
  of a graphic object

  Special notes:
  - metafile class must support Clipboard (CF_ENHMETAFILE):
    * metafile.Assign(Clipboard)
    * metafile.SaveToClipboardFormat(...)
  - bitmap class must support Clipboard (CF_BITMAP):
    * bitmap.Assign(Clipboard)
    * bitmap.LoadFromClipboardFormat(...)
    * bitmap.SaveToClipboardFormat(...)
}
function TRVGraphicHandler.GetGraphicType(
  Graphic: TGraphic): TRVGraphicType;
begin
  {$IFDEF RICHVIEWDEF2010}
  if Graphic is TWicImage then
    case TWicImage(Graphic).ImageFormat of
      wifBmp: Result := rvgtBitmap;
      wifPng: Result := rvgtPNG;
      wifJpeg: Result := rvgtJPEG;
      {wifGif: Result := rvgtGIF;}
      else Result := rvgtOther;
    end
  else
  {$ENDIF}
  if Graphic is TBitmap then
    Result := rvgtBitmap
  else if Graphic is TIcon then
    Result := rvgtIcon
  else if Graphic is TMetafile then
    Result := rvgtMetafile
  {$IFNDEF RVDONOTUSEJPEGIMAGE}
  else if Graphic is TJPEGImage then
    Result := rvgtJPEG
  {$ENDIF}
  {$IFNDEF RVDONOTUSEPNGIMAGE}
  else if Graphic is TPngImage then
    Result := rvgtPng
  {$ENDIF}
  {$IFDEF RVUSEDXPNGIMAGE}
  else if Graphic is TdxPngImage then
    Result := rvgtPng
  else if Graphic is TdxSmartImage then
    case TdxSmartImage(Graphic).ImageDataFormat of
      dxImageBitmap: Result := rvgtBitmap;
      dxImageJpeg:   Result := rvgtJPEG;
      dxImagePng:    Result := rvgtPNG;
      {dxImageGif:    Result := rvgtGIF;}
      else   Result := rvgtOther;
    end
  {$ENDIF}
  else if (FPngGraphicClass<>nil) and (Graphic is FPngGraphicClass) then
    Result := rvgtPNG
  else
    Result := rvgtOther;
end;
{------------------------------------------------------------------------------}
{ Specifies the default PNG class }
procedure TRVGraphicHandler.RegisterPngGraphic(GraphicClass: TGraphicClass);
begin
  FPngGraphicClass := GraphicClass;
end;
{------------------------------------------------------------------------------}
{ Is a default PNG class assigned? }
function TRVGraphicHandler.IsPngClassAssigned: Boolean;
begin
  Result := FPngGraphicClass<>nil;
end;
{------------------------------------------------------------------------------}
{ Vector graphic is printed without drawing on a temporal bitmap }
function TRVGraphicHandler.IsVectorGraphic(Graphic: TGraphic): Boolean;
begin
  Result := GetGraphicType(Graphic)=rvgtMetafile;
end;
{------------------------------------------------------------------------------}
{ Is it possible to resize this image? }
function TRVGraphicHandler.IsResizeable(Graphic: TGraphic): Boolean;
begin
  Result := not (Graphic is TIcon);
end;
{------------------------------------------------------------------------------}
procedure TRVGraphicHandler.AfterLoadMetafile(Graphic: TGraphic);
begin
  {$IFNDEF RVDONOTCORRECTWMFSCALE}
  if (Graphic is TMetafile) and not TMetafile(Graphic).Enhanced and
    (TMetafile(Graphic).Inch=0) then
    TMetafile(Graphic).Inch := 1440;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.GetMetafileHandle(Graphic: TGraphic): HENHMETAFILE;
begin
  if Graphic is TMetafile then
    Result := TMetafile(Graphic).Handle
  else
    Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TRVGraphicHandler.GetMetafileMMSize(Graphic: TGraphic; var Size: TSize);
begin
  if Graphic is TMetafile then begin
    Size.cx := TMetafile(Graphic).MMWidth;
    Size.cy := TMetafile(Graphic).MMHeight;
    end
  else begin
    Size.cx := 0;
    Size.cy := 0;
  end;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.IsEnhancedMetafile(Graphic: TGraphic): Boolean;
begin
  Result := (Graphic is TMetafile) and TMetafile(Graphic).Enhanced;
end;
{------------------------------------------------------------------------------}
procedure TRVGraphicHandler.SetMetafileEnhanced(Graphic: TGraphic; Enhanced: Boolean);
begin
  if Graphic is TMetafile then
    TMetafile(Graphic).Enhanced := Enhanced;
end;
{------------------------------------------------------------------------------}
procedure TRVGraphicHandler.SetMetafileHandle(Graphic: TGraphic; Handle: HENHMETAFILE);
begin
  if Graphic is TMetafile then
    TMetafile(Graphic).Handle := Handle;
end;
{------------------------------------------------------------------------------}
procedure TRVGraphicHandler.SetBitmapHandle(Graphic: TGraphic; Handle: HBITMAP);
begin
  if Graphic is TBitmap then
    TBitmap(Graphic).Handle := Handle;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.GetBitmapScanLineWidth(Graphic: TGraphic): Integer;
begin
  {$IFDEF RICHVIEWCBDEF3}
  if Graphic is TBitmap then
    if Graphic.Height>1 then
      Result := abs(PRVAnsiChar(TBitmap(Graphic).ScanLine[1])-PRVAnsiChar(TBitmap(Graphic).ScanLine[0]))
    else
      Result := Graphic.Width
  else
  {$ENDIF}
    Result := 0;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.GetBitmapCanvas(Graphic: TGraphic): TCanvas;
begin
  if Graphic is TBitmap then
    Result := TBitmap(Graphic).Canvas
  else
    Result := nil;
end;
{------------------------------------------------------------------------------}
{ Assigns the palette (PLogPal) to Graphic.
  Does something only on D3+, CB3+                                             }
procedure TRVGraphicHandler.SetPalette(Graphic: TGraphic;
  PLogPal: PLogPalette);
{$IFDEF RICHVIEWCBDEF3}
var Palette: HPALETTE;
{$ENDIF}
begin
  if PLogPal<>nil then begin
    {$IFNDEF RVDONOTUSEJPEGIMAGE}
    if Graphic is TJpegImage then
      TJpegImage(Graphic).PixelFormat := jf8Bit;
    {$ENDIF}
    {$IFDEF RICHVIEWCBDEF3}
    Palette := CreatePalette(PLogPal^);
    Graphic.Palette := Palette;
    if Graphic.Palette<>Palette then
      DeleteObject(Palette);
    {$ENDIF}
  end;
end;
{------------------------------------------------------------------------------}
{ Returns true, if Graphic can contain transparent areas           }
function TRVGraphicHandler.IsTransparent(Graphic: TGraphic): Boolean;
begin
  {$IFDEF RICHVIEWCBDEF3}
  if Graphic is TBitmap then
    Result := Graphic.Transparent
  {$IFNDEF RVDONOTUSEJPEGIMAGE}
  else if Graphic is TJpegImage then
    Result := False
  {$ENDIF}
  {$IFDEF RICHVIEWDEF2010}
  else if Graphic is TWicImage then
    Result := not (TWicImage(Graphic).ImageFormat in [wifJPEG, wifTiff])
  {$ENDIF}
  {$IFDEF RVUSEDXPNGIMAGE}
  else if Graphic is TdxSmartImage then
    Result := not (TdxSmartImage(Graphic).ImageDataFormat in [dxImageJpeg, dxImageTiff])
  {$ENDIF}
  else
    Result := True;
  {$ELSE}
  Result := True;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{ Returns a graphic class for the specified GraphicType, or nil if not available }
function TRVGraphicHandler.GetGraphicClassByType(GraphicType: TRVGraphicType): TGraphicClass;
begin
  case GraphicType of
    rvgtBitmap: Result := TBitmap;
    rvgtIcon:   Result := TIcon;
    rvgtMetafile: Result := TMetafile;
    rvgtJPEG:   Result := FJpegGraphicClass;
    rvgtPNG:    Result := FPngGraphicClass;
    else        Result := nil;
  end;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.GetGraphicClass(const ClassName: String): TGraphicClass;
begin
  {$IFNDEF RVDONOTUSEPNGIMAGE}
  if CompareText(ClassName, 'TPNGObject')=0 then
    Result := TPngImage
  else
  {$ENDIF}
    Result := TGraphicClass(GetClass(ClassName));
end;
{------------------------------------------------------------------------------}
{ Creates and returns an object of the specified GraphicType, if possible }
function TRVGraphicHandler.CreateGraphicByType(GraphicType: TRVGraphicType): TGraphic;
var GraphicClass: TGraphicClass;
begin
  GraphicClass := GetGraphicClassByType(GraphicType);
  if GraphicClass<>nil then
    Result := CreateGraphic(GraphicClass)
  else
    Result := nil;
end;
{------------------------------------------------------------------------------}
{ This procedure creates a graphic object by its class.
  This is a workaround for D2-D5 bug (private constructor of TGraphic). }
type
  TGraphicAccess=class(TGraphic);
function TRVGraphicHandler.CreateGraphic(GraphicClass: TGraphicClass): TGraphic;
begin
  Result := TGraphic(GraphicClass.NewInstance);
  TGraphicAccess(Result).Create;
end;
{------------------------------------------------------------------------------}
{ Prepares Graphic for using }
procedure TRVGraphicHandler.PrepareGraphic(Graphic: TGraphic);
begin
  if Graphic is TIcon then
    TIcon(Graphic).Handle;
end;
{------------------------------------------------------------------------------}
{ This is a default procedure called after importing external files by links
  in documents (RTF). }
procedure TRVGraphicHandler.AfterImportGraphic(Graphic: TGraphic);
begin

end;
{------------------------------------------------------------------------------}
{ Returns icon handle }
function TRVGraphicHandler.GetIconHandle(Graphic: TGraphic): HICON;
begin
  if Graphic is TIcon then
    Result := TIcon(Graphic).Handle
  else
    Result := 0;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.LoadFromFile(const FileName: String): TGraphic;
var pic: TPicture;
begin
  Result := nil;
  try
    pic := TPicture.Create;
    try
      pic.LoadFromFile(FileName);
      Result := CreateGraphic(TGraphicClass(pic.Graphic.ClassType));
      Result.Assign(pic.Graphic);
      if RVGraphicHandler.GetGraphicType(Result)=rvgtMetafile then
        AfterLoadMetafile(Result);
    finally
      pic.Free;
    end;
  except
    RVFreeAndNil(Result);
  end;
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF2010}
function RVIconToWicPng(Icon: TGraphic): TWicImage;
var Stream: TMemoryStream;
begin
  Result := TWicImage.Create;
  if Result.ImagingFactory=nil then begin
    RVFreeAndNil(Result);
    exit;
  end;
  try
    Stream := TMemoryStream.Create;
    try
      Icon.SaveToStream(Stream);
      Stream.Position := 0;
      Result.LoadFromStream(Stream);
      Result.ImageFormat := wifPng;
    finally
      Stream.Free;
    end;
  except
    RVFreeAndNil(Result);
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEPNGIMAGE}
// Copying alpha channel (if exists) from Bitmap to Png)
// (Bitmap.PixelFormat must be pf32Bit)
function CopyAlpha(Bitmap: TBitmap; Png: TPngImage): Boolean;
var Line1, Line2, Line, P: PRVAnsiChar;
    PngAlphaLine: pByteArray;
    i, j, Delta: Integer;
begin
  // 1. Does Bitmap have alpha channel?
  Result := False;
  if (Bitmap.Width=0) or (Bitmap.Height=0) then
    exit;
  Line1 := Bitmap.ScanLine[0];
  if Bitmap.Height>1 then begin
    Line2 := Bitmap.ScanLine[1];
    Delta := Line2-Line1;
    end
  else
    Delta := 0;
  Line := Line1;
  for i := 0 to Bitmap.Height-1 do begin
    P := Line;
    for j := 0 to Bitmap.Width-1 do begin
      if PRGBQuad(P)^.rgbReserved<>0 then begin
        Result := True;
        break;
      end;
      if Result then
        break;
      inc(P, sizeof(TRGBQuad));
    end;
    inc(Line, Delta);
  end;
  if not Result then
    exit;
  // 2. If yes, copying it to PNG
  Png.CreateAlpha;
  Line := Line1;
  for i := 0 to Bitmap.Height-1 do begin
    P := Line;
    PngAlphaLine := Png.AlphaScanline[i];
    for j := 0 to Bitmap.Width-1 do begin
      PngAlphaLine[j] := PRGBQuad(P)^.rgbReserved;
      inc(P, sizeof(TRGBQuad));
    end;
    inc(Line, Delta);
  end;
end;
{------------------------------------------------------------------------------}
// Assigning Png's alpha channel from Bitmap, where Bitmap is a mask
// (Bitmap.PixelFormat must be pf24Bit)
procedure MakeAlphaFromMask(Bitmap: TBitmap; Png: TPngImage);
var Line1, Line2, Line, P: PRVAnsiChar;
    PngAlphaLine: pByteArray;
    i, j, Delta: Integer;
begin
  if (Bitmap.Width=0) or (Bitmap.Height=0) then
    exit;
  Line1 := Bitmap.ScanLine[0];
  if Bitmap.Height>1 then begin
    Line2 := Bitmap.ScanLine[1];
    Delta := Line2-Line1;
    end
  else
    Delta := 0;
  Line := Line1;
  Png.CreateAlpha;
  for i := 0 to Bitmap.Height-1 do begin
    P := Line;
    PngAlphaLine := Png.AlphaScanline[i];
    for j := 0 to Bitmap.Width-1 do begin
      PngAlphaLine[j] := not PRGBTriple(P)^.rgbtBlue;
      inc(P, sizeof(TRGBTriple));
    end;
    inc(Line, Delta);
  end;
end;
{$ENDIF}
{$IFDEF RVUSEDXPNGIMAGE}
{------------------------------------------------------------------------------}
// Filling Bits from Bitmap and Mask
// (Bitmap.PixelFormat must be pf32Bit; if Mask is used, it is converted to pf24Bit)
procedure FillBitmapBits(Bitmap, Mask: TBitmap; Bits: PRGBQuad);
var Line1, Line2, Line, P: PRVAnsiChar;
    Line1M, Line2M, LineM, PM: PRVAnsiChar;
    PBits: PRVAnsiChar;
    i, j, Delta, DeltaM, DeltaBits: Integer;
    HasAlpha: Boolean;
    Alpha: Byte;
begin
  if (Bitmap.Width=0) or (Bitmap.Height=0) then
    exit;
  // Copying bitmap to bits. Checking is it has an alpha channel
  HasAlpha := False;
  Line1 := Bitmap.ScanLine[0];
  if Bitmap.Height>1 then begin
    Line2 := Bitmap.ScanLine[1];
    Delta := Line2-Line1;
    end
  else
    Delta := 0;
  DeltaBits := sizeof(TRGBQuad)*Bitmap.Width;
  Line := Line1;
  PBits := PRVAnsiChar(Bits);
  for i := 0 to Bitmap.Height-1 do begin
    P := Line;
    Move(P^, PBits^, sizeof(TRGBQuad)*Bitmap.Width);
    for j := 0 to Bitmap.Width-1 do begin
      if PRGBQuad(P)^.rgbReserved<>0 then
        HasAlpha := True;
      inc(P, sizeof(TRGBQuad));
    end;
    inc(Line, Delta);
    inc(PBits, DeltaBits);
  end;
  if HasAlpha then begin
    // If alpha, multiplying RGB
    PBits := PRVAnsiChar(Bits);
    for i := 0 to Bitmap.Height*Bitmap.Width-1 do begin
      Alpha := PRGBQuad(PBits)^.rgbReserved;
      PRGBQuad(PBits)^.rgbRed := MulDiv(PRGBQuad(PBits)^.rgbRed, Alpha, 255);
      PRGBQuad(PBits)^.rgbGreen := MulDiv(PRGBQuad(PBits)^.rgbGreen, Alpha, 255);
      PRGBQuad(PBits)^.rgbBlue := MulDiv(PRGBQuad(PBits)^.rgbBlue, Alpha, 255);
      inc(PBits, sizeof(TRGBQuad));
    end;
    end
  else begin
    // If no alpha, copying Mask to Bit's alpha channel
    if (Bitmap.Width<>Mask.Width) or (Bitmap.Height<>Mask.Height) then
      exit;
    Mask.PixelFormat := pf24Bit;
    PBits := PRVAnsiChar(Bits);
    Line1M := Mask.ScanLine[0];
    if Mask.Height>1 then begin
      Line2M := Mask.ScanLine[1];
      DeltaM := Line2M-Line1M;
      end
    else
      DeltaM := 0;
    LineM := Line1M;
    for i := 0 to Mask.Height-1 do begin
      PM := LineM;
      for j := 0 to Mask.Width-1 do begin
        PRGBQuad(PBits)^.rgbReserved := not PRGBTriple(PM)^.rgbtBlue;
        inc(PBits, sizeof(TRGBQuad));
        inc(PM, sizeof(TRGBTriple));
      end;
      inc(LineM, DeltaM);
    end;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVGraphicHandler.CanConvertIconToPng: Boolean;
begin
  {$IFDEF RVPNGKNOWN}
  Result := True;
  {$ELSE}
  Result := False;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{ Converting TIcon to png. 3 different methods are possible:
  - using TdxPngImage
  - using TPngImage
  - using TWicImage }
function TRVGraphicHandler.IconToPng(Graphic: TGraphic): TGraphic;
{$IFNDEF RVDONOTUSEPNGIMAGE}
var
  PIconInfo: TIconInfo;
  Bitmap, Mask: TBitmap;
  Handle: HICON;
{$ELSE}
{$IFDEF RVUSEDXPNGIMAGE}
var
  PIconInfo: TIconInfo;
  Bitmap, Mask: TBitmap;
  Bits: TRGBColors;
  Handle: HICON;
{$ENDIF}
{$ENDIF}
begin
  {$IFDEF RVUSEDXPNGIMAGE}
  Result := nil;
  Handle := GetIconHandle(Graphic);
  if Handle=0 then
    exit;
  Result := nil;
  try
    Bitmap := TBitmap.Create;
    Mask   := TBitmap.Create;
    try
      GetIconInfo(Handle, PIconInfo);
      Bitmap.Handle := PIconInfo.hbmColor;
      Mask.Handle := PIconInfo.hbmMask;
      Bitmap.PixelFormat := pf32Bit;
      SetLength(Bits, Bitmap.Width*Bitmap.Height);
      FillBitmapBits(Bitmap, Mask, PRGBQuad(Bits));
      Result := TdxPNGImage.CreateFromBits(Bitmap.Width, Bitmap.Height, Bits, True);
    finally
      Bitmap.Free;
      Mask.Free
    end;
  except
    RVFreeAndNil(Result);
  end;
  {$ELSE}
  {$IFNDEF RVDONOTUSEPNGIMAGE}
  Result := nil;
  Handle := GetIconHandle(Graphic);
  if Handle=0 then
    exit;
  Result := TPngImage.Create;
  try
    Bitmap := TBitmap.Create;
    Mask   := TBitmap.Create;
    try
      GetIconInfo(Handle, PIconInfo);
      Bitmap.Handle := PIconInfo.hbmColor;
      Mask.Handle := PIconInfo.hbmMask;
      Bitmap.PixelFormat := pf32Bit;
      Bitmap.Transparent := False;
      Result.Assign(Bitmap);
      if not CopyAlpha(Bitmap, TPngImage(Result)) then begin
        Mask.PixelFormat := pf24Bit;
        MakeAlphaFromMask(Mask, TPngImage(Result));
      end;
    finally
      Bitmap.Free;
      Mask.Free
    end;
  except
    RVFreeAndNil(Result);
  end;
  {$ELSE}
  {$IFDEF RICHVIEWDEF2010}
  Result := RVIconToWicPng(Graphic);
  {$ELSE}
  Result := nil;
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
type
  PGraphic = ^TGraphic;
function RVEnhMetaFileProc(DC: THandle; PHTable: PHandleTable;
  PEMFR: PENHMetaRecord; Obj: Integer; Data: Pointer): Integer; export; stdcall;
var PEMRSDIB: PEMRStretchDIBits;
    PEMRSDIBTD: PEMRSetDIBitsToDevice;
    //PEMRBB: PEMRBitBlt;
  bi: PBitmapInfo;
begin
  case PEMFR.iType of
    1, 9..11, 14, 17, 21, 24, 25, 33, 34, 37, 48, 70, 75, 98:
      Result := 1;
    EMR_SETDIBITSTODEVICE:
      begin
        if Assigned(PGraphic(Data)^) then begin
          RVFreeAndNil(PGraphic(Data)^);
          Result := 0;
          exit;
        end;
        PGraphic(Data)^ := RVGraphicHandler.CreateGraphicByType(rvgtBitmap);
        PEMRSDIBTD := PEMRSetDIBitsToDevice(PEMFR);
        bi := PBitmapInfo(PRVAnsiChar(PEMRSDIBTD)+PEMRSDIBTD.offBmiSrc);
        RVGraphicHandler.SetBitmapHandle(PGraphic(Data)^,
          CreateDIBitmap(DC, bi.bmiHeader, CBM_INIT,
            PRVAnsiChar(PEMRSDIBTD)+ PEMRSDIBTD.offBitsSrc, bi^, PEMRSDIBTD.iUsageSrc));
        Result := 1;
      end;
    EMR_STRETCHDIBITS:
      begin
        if Assigned(PGraphic(Data)^) then begin
          RVFreeAndNil(PGraphic(Data)^);
          Result := 0;
          exit;
        end;
        PGraphic(Data)^ := RVGraphicHandler.CreateGraphicByType(rvgtBitmap);
        PEMRSDIB := PEMRStretchDIBits(PEMFR);
        bi := PBitmapInfo(PRVAnsiChar(PEMRSDIB)+PEMRSDIB.offBmiSrc);
        RVGraphicHandler.SetBitmapHandle(PGraphic(Data)^,
          CreateDIBitmap(DC, bi.bmiHeader, CBM_INIT,
            PRVAnsiChar(PEMRSDIB)+ PEMRSDIB.offBitsSrc, bi^, PEMRSDIB.iUsageSrc));
        Result := 1;
      end;
    EMR_BITBLT:
      begin
      {
        if Assigned(PGraphic(Data)^) then begin
          (PGraphic(Data)^).Free;
          PGraphic(Data)^ := nil;
          Result := 0;
          exit;
        end;
        PGraphic(Data)^ := RVGraphicHandler.CreateGraphicByType(rvgtBitmap);
        PEMRBB := PEMRBitBlt(PEMFR);
        bi := PBitmapInfo(PRVAnsiChar(PEMRBB)+PEMRBB.offBmiSrc);
        RVGraphicHandler.SetBitmapHandle(PGraphic(Data)^,
          CreateDIBitmap(DC, bi.bmiHeader, CBM_INIT,
            PRVAnsiChar(PEMRBB)+ PEMRBB.offBitsSrc, bi^, PEMRBB.iUsageSrc));
        }
        Result := 1;
      end;
    else begin
      RVFreeAndNil(PGraphic(Data)^);
      Result := 0;
    end;
  end;
end;
{------------------------------------------------------------------------------}
function TRVGraphicHandler.ExtractBitmapFromMetafile(Graphic: TGraphic): TGraphic;
var bmp: TGraphic;
    DC: THandle;
begin
  bmp := nil;
  DC := GetDC(0);
  try
    EnumEnhMetaFile(DC, GetMetafileHandle(Graphic), @RVEnhMetaFileProc, @bmp,
      Bounds(0, 0, Graphic.Width, Graphic.Height));
  finally
    ReleaseDC(0, DC);
  end;
  Result := bmp;
end;
{------------------------------------------------------------------------------}
{ Is a default JPEG class assigned? }
function TRVGraphicHandler.IsJpegClassAssigned: Boolean;
begin
  Result := FJpegGraphicClass<>nil;
end;
{------------------------------------------------------------------------------}
{ Specifies the default JPEG class }
procedure TRVGraphicHandler.RegisterJpegGraphic(GraphicClass: TGraphicClass);
begin
  FJpegGraphicClass := GraphicClass;
end;

initialization

  RVGraphicHandler := TRVGraphicHandler.Create;

  {$IFNDEF RVDONOTUSEPNGIMAGE}
  RVGraphicHandler.RegisterHTMLGraphicFormat(TPngImage);
  {$ENDIF}

  {$IFNDEF RVDONOTUSERVF}
  RegisterClasses([TBitmap, TIcon, TMetafile]);
  {$IFDEF RICHVIEWDEF2010}
  RegisterClass(TWicImage);
  {$ENDIF}
  {$IFNDEF RVDONOTUSEJPEGIMAGE}
  RegisterClasses([TJpegImage]);
  {$ENDIF}
  {$IFNDEF RVDONOTUSEPNGIMAGE}
  RegisterClass(TPngImage);
  {$ENDIF}
  {$ENDIF}

finalization

  RVFreeAndNil(RVGraphicHandler);

end.
