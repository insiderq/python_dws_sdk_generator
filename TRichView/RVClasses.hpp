﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVClasses.pas' rev: 27.00 (Windows)

#ifndef RvclassesHPP
#define RvclassesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.RTLConsts.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvclasses
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	virtual void __fastcall Clear(void);
	HIDESBASE void __fastcall Delete(int Index);
	void __fastcall DeleteAsPointer(int Index);
	void __fastcall SortPointers(void);
	int __fastcall Find(System::TObject* FindThis, System::Classes::TListSortCompare CompareProc);
	__fastcall virtual ~TRVList(void);
public:
	/* TObject.Create */ inline __fastcall TRVList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVListIndexerItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVListIndexerItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::TObject* Item;
	int Index;
public:
	/* TObject.Create */ inline __fastcall TRVListIndexerItem(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVListIndexerItem(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVListIndexer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVListIndexer : public TRVList
{
	typedef TRVList inherited;
	
public:
	HIDESBASE void __fastcall Sort(void);
	int __fastcall GetItemIndex(System::TObject* Item);
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVListIndexer(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVListIndexer(void) : TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVIndexedList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVIndexedList : public TRVList
{
	typedef TRVList inherited;
	
private:
	TRVListIndexer* FIndexer;
	
public:
	__fastcall TRVIndexedList(void);
	__fastcall virtual ~TRVIndexedList(void);
	void __fastcall CreateIndexer(void);
	HIDESBASE int __fastcall Find(System::TObject* Item);
};

#pragma pack(pop)

class DELPHICLASS TRVIntegerList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVIntegerList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	int operator[](int Index) { return Items[Index]; }
	
protected:
	HIDESBASE int __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, const int Value);
	
public:
	__fastcall TRVIntegerList(int Count, int Value);
	__fastcall TRVIntegerList(TRVIntegerList* Source);
	void __fastcall SaveToStream(System::Classes::TStream* Stream);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	HIDESBASE void __fastcall Sort(void);
	void __fastcall InitWith(int Value, int Count);
	void __fastcall Fill(int Value);
	HIDESBASE void __fastcall Add(int Value);
	bool __fastcall AreAllEqualTo(int Value);
	int __fastcall AddUnique(int Value);
	HIDESBASE void __fastcall Insert(int Index, int Value);
	HIDESBASE void __fastcall Assign(TRVIntegerList* Source);
	bool __fastcall IsIdentityMap(void);
	void __fastcall FillReverseMap(TRVIntegerList* Map);
	__property int Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVIntegerList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVIntegerList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVColorList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVColorList : public TRVIntegerList
{
	typedef TRVIntegerList inherited;
	
public:
	HIDESBASE void __fastcall AddUnique(int Value);
public:
	/* TRVIntegerList.CreateEx */ inline __fastcall TRVColorList(int Count, int Value) : TRVIntegerList(Count, Value) { }
	/* TRVIntegerList.CreateCopy */ inline __fastcall TRVColorList(TRVIntegerList* Source) : TRVIntegerList(Source) { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVColorList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVColorList(void) : TRVIntegerList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVMemoryStream;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMemoryStream : public System::Classes::TCustomMemoryStream
{
	typedef System::Classes::TCustomMemoryStream inherited;
	
private:
	int FCapacity;
	void *FMemory;
	NativeInt FSize;
	NativeInt FPosition;
	void __fastcall SetCapacity(int NewCapacity);
	
protected:
	virtual void * __fastcall Realloc(int &NewCapacity);
	HIDESBASE void __fastcall SetPointer(void * Ptr, NativeInt Size);
	__property int Capacity = {read=FCapacity, write=SetCapacity, nodefault};
	
public:
	__fastcall virtual ~TRVMemoryStream(void);
	void __fastcall Clear(void);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	void __fastcall LoadFromFile(const System::UnicodeString FileName);
	virtual void __fastcall SetSize(int NewSize)/* overload */;
	virtual int __fastcall Write(const void *Buffer, int Count)/* overload */;
	virtual int __fastcall Read(void *Buffer, int Count)/* overload */;
	virtual int __fastcall Read(System::DynamicArray<System::Byte> Buffer, int Offset, int Count)/* overload */;
	virtual __int64 __fastcall Seek(const __int64 Offset, System::Classes::TSeekOrigin Origin)/* overload */;
	virtual void __fastcall SaveToStream(System::Classes::TStream* Stream);
	__property void * Memory = {read=FMemory};
public:
	/* TObject.Create */ inline __fastcall TRVMemoryStream(void) : System::Classes::TCustomMemoryStream() { }
	
	/* Hoisted overloads: */
	
protected:
	inline void __fastcall  SetSize(const __int64 NewSize){ System::Classes::TStream::SetSize(NewSize); }
	
public:
	inline int __fastcall  Write(const System::DynamicArray<System::Byte> Buffer, int Offset, int Count){ return System::Classes::TStream::Write(Buffer, Offset, Count); }
	inline int __fastcall  Write(const System::DynamicArray<System::Byte> Buffer, int Count){ return System::Classes::TStream::Write(Buffer, Count); }
	inline int __fastcall  Read(System::DynamicArray<System::Byte> &Buffer, int Count){ return System::Classes::TStream::Read(Buffer, Count); }
	inline int __fastcall  Seek(int Offset, System::Word Origin){ return System::Classes::TStream::Seek(Offset, Origin); }
	inline __int64 __fastcall  Seek _DEPRECATED_ATTRIBUTE0 (const __int64 Offset, System::Word Origin){ return System::Classes::TStream::Seek(Offset, Origin); }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const int RVMemoryDelta = int(0x40000);
}	/* namespace Rvclasses */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCLASSES)
using namespace Rvclasses;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvclassesHPP
