﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVZip.pas' rev: 27.00 (Windows)

#ifndef RvzipHPP
#define RvzipHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.ZLib.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvzip
{
//-- type declarations -------------------------------------------------------
#pragma pack(push,1)
struct DECLSPEC_DRECORD TRVZipCommonFileHeader
{
public:
	System::Word VersionNeededToExtract;
	System::Word GeneralPurposeBitFlag;
	System::Word CompressionMethod;
	int LastModFileTimeDate;
	unsigned Crc32;
	unsigned CompressedSize;
	unsigned UncompressedSize;
	System::Word FileNameLength;
	System::Word ExtraFieldLength;
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD TRVZipLocalFileHeader
{
public:
	unsigned LocalFileHeaderSignature;
	TRVZipCommonFileHeader CommonFileHeader;
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD TRVZipFileHeader
{
public:
	unsigned CentralFileHeaderSignature;
	System::Word VersionMadeBy;
	TRVZipCommonFileHeader CommonFileHeader;
	System::Word FileCommentLength;
	System::Word DiskNumberStart;
	System::Word InternalFileAttributes;
	unsigned ExternalFileAttributes;
	unsigned RelativeOffsetOfLocalHeader;
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD TRVZipEndOfCentralDir
{
public:
	unsigned EndOfCentralDirSignature;
	System::Word NumberOfThisDisk;
	System::Word NumberOfTheDiskWTSoTCD;
	System::Word TotalNumberOfEntriesOnThisDisk;
	System::Word TotalNumberOfEntries;
	unsigned SizeOfTheCentralDirectory;
	unsigned OffsetOfStartOfCentralDirectory;
	System::Word ZipFileCommentLength;
};
#pragma pack(pop)


class DELPHICLASS TRVZipPacker;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVZipPacker : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	typedef System::DynamicArray<TRVZipFileHeader> _TRVZipPacker__1;
	
	typedef System::DynamicArray<Rvtypes::TRVAnsiString> _TRVZipPacker__2;
	
	
private:
	System::Classes::TStream* FZipStream;
	bool Failed;
	_TRVZipPacker__1 FCentralDirectory;
	_TRVZipPacker__2 FFileNames;
	System::UnicodeString FZipFileName;
	
public:
	bool __fastcall CreateZipFile(const System::UnicodeString FileName);
	void __fastcall CreateZipInStream(System::Classes::TStream* Stream);
	bool __fastcall Close(void);
	bool __fastcall Add(const Rvtypes::TRVAnsiString FileName, char * Content, unsigned Size);
public:
	/* TObject.Create */ inline __fastcall TRVZipPacker(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVZipPacker(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvzip */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVZIP)
using namespace Rvzip;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvzipHPP
