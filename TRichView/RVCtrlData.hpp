﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVCtrlData.pas' rev: 27.00 (Windows)

#ifndef RvctrldataHPP
#define RvctrldataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvctrldata
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVControlData;
class PASCALIMPLEMENTATION TRVControlData : public Crvfdata::TCustomRVFormattedData
{
	typedef Crvfdata::TCustomRVFormattedData inherited;
	
public:
	int TopLevelFocusedItemNo;
	Crvfdata::TCustomRVFormattedData* TopLevelFocusedRVData;
	Rvscroll::TRVTabNavigationType TabNavigation;
	virtual void __fastcall ClearTemporal(void);
	void __fastcall DoTabNavigation(bool Shift, Vcl::Controls::TWinControl* PrevCtrl);
	virtual void __fastcall PaintBuffered(void);
	void __fastcall DrawFocusedRect(Vcl::Graphics::TCanvas* Canvas);
	DYNAMIC void __fastcall Deselect(Rvitem::TCustomRVItemInfo* NewPartiallySelected, bool MakeEvent);
	DYNAMIC void __fastcall DeleteParas(int FirstItemNo, int LastItemNo);
	void __fastcall ExecuteFocused(void);
	DYNAMIC void __fastcall AdjustFocus(int NewFocusedItemNo, System::Classes::TPersistent* TopLevelRVData, int TopLevelItemNo);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	__fastcall TRVControlData(void);
public:
	/* TCustomRVFormattedData.Destroy */ inline __fastcall virtual ~TRVControlData(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvctrldata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCTRLDATA)
using namespace Rvctrldata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvctrldataHPP
