﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVResize.pas' rev: 27.00 (Windows)

#ifndef RvresizeHPP
#define RvresizeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvresize
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVResizeHandleIndex : unsigned char { rvrhLeftTop, rvrhRightTop, rvrhRightBottom, rvrhLeftBottom, rvrhTop, rvrhRight, rvrhBottom, rvrhLeft };

enum DECLSPEC_DENUM TRVResizeHandlesPosition : unsigned char { rvhpInside, rvhpOutside };

class DELPHICLASS TRVItemResizer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVItemResizer : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FWidth;
	int FHeight;
	bool FRotated;
	Dlines::TRVDrawLineInfo* FDrawItem;
	int FBorderWidth;
	int FBorderHeight;
	int FDrawItemNo;
	int FItemNo;
	bool FDragging;
	bool FDragCancelled;
	int FDx;
	int FDy;
	System::Types::TRect FDragRect;
	TRVResizeHandleIndex FDraggedResizeHandle;
	Rvitem::TCustomRVItemInfo* FItem;
	TRVResizeHandlesPosition FPosition;
	bool __fastcall IsCornerResizeHandle(TRVResizeHandleIndex Index);
	void __fastcall GetResizeHandleShift(TRVResizeHandleIndex Index, int &DX, int &DY);
	
public:
	__fastcall TRVItemResizer(Dlines::TRVDrawLineInfo* ADrawItem, Rvitem::TCustomRVItemInfo* AItem, int ADrawItemNo, int ABorderWidth, int ABorderHeight, bool ARotated);
	void __fastcall Draw(Vcl::Graphics::TCanvas* Canvas, int HOffs, int VOffs, Rvgrin::TRVGraphicInterface* GraphicInterface);
	void __fastcall GetResizeHandleCoords(TRVResizeHandleIndex Index, bool Shifted, int &X, int &Y);
	bool __fastcall GetResizeHandleAt(int X, int Y, TRVResizeHandleIndex &Index);
	bool __fastcall MouseDown(int X, int Y);
	void __fastcall MouseUp(int X, int Y);
	void __fastcall DragTo(System::Classes::TShiftState Shift, int X, int Y);
	System::Uitypes::TCursor __fastcall GetResizeHandleCursor(TRVResizeHandleIndex Index);
	void __fastcall XorDrawing(Vcl::Graphics::TCanvas* Canvas, int HOffs, int VOffs);
	void __fastcall CancelDrag(void);
	void __fastcall UpdateItem(Dlines::TRVDrawLineInfo* ADrawItem, Rvitem::TCustomRVItemInfo* AItem, int ADrawItemNo, int ABorderWidth, int ABorderHeight, bool ARotated);
	__property bool Dragging = {read=FDragging, nodefault};
	__property bool DragCancelled = {read=FDragCancelled, nodefault};
	__property int ItemNo = {read=FItemNo, nodefault};
	__property int DrawItemNo = {read=FDrawItemNo, nodefault};
	__property Dlines::TRVDrawLineInfo* DrawItem = {read=FDrawItem};
	__property int Width = {read=FWidth, nodefault};
	__property int Height = {read=FHeight, nodefault};
	__property TRVResizeHandlesPosition Position = {read=FPosition, write=FPosition, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVItemResizer(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 RichViewResizeHandleSize = System::Int8(0x6);
}	/* namespace Rvresize */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVRESIZE)
using namespace Rvresize;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvresizeHPP
