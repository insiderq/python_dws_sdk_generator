﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVGifAnimate2007.pas' rev: 27.00 (Windows)

#ifndef Rvgifanimate2007HPP
#define Rvgifanimate2007HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVAnimate.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <Vcl.Imaging.GIFImg.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvgifanimate2007
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVGifImageAnimator;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVGifImageAnimator : public Rvanimate::TRVAnimator
{
	typedef Rvanimate::TRVAnimator inherited;
	
private:
	Vcl::Graphics::TBitmap* bmp;
	Vcl::Graphics::TBitmap* bmpsrc;
	System::Uitypes::TColor FBackColor;
	int FX;
	int FY;
	int FParaNo;
	int FLastDrawnFrameIndex;
	void __fastcall CalcInterval(void);
	
protected:
	virtual int __fastcall GetFrameCount(void);
	virtual void __fastcall ResetBackground(void);
	
public:
	__fastcall virtual ~TRVGifImageAnimator(void);
	virtual void __fastcall Reset(void);
	virtual void __fastcall ChangeFrame(void);
	virtual void __fastcall Draw(int X, int Y, Vcl::Graphics::TCanvas* Canvas, bool Animation);
	virtual System::Types::TSize __fastcall GetExportImageSize(void);
	virtual void __fastcall DrawForExport(Vcl::Graphics::TCanvas* Canvas);
public:
	/* TRVAnimator.Create */ inline __fastcall TRVGifImageAnimator(Crvfdata::TCustomRVFormattedData* ARVData, Rvitem::TCustomRVItemInfo* AItem) : Rvanimate::TRVAnimator(ARVData, AItem) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvgifanimate2007 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVGIFANIMATE2007)
using namespace Rvgifanimate2007;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Rvgifanimate2007HPP
