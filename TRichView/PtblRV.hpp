﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'PtblRV.pas' rev: 27.00 (Windows)

#ifndef PtblrvHPP
#define PtblrvHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Winapi.CommDlg.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <PtRVData.hpp>	// Pascal unit
#include <Winapi.WinSpool.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Ptblrv
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TCustomRVPrint;
typedef void __fastcall (__closure *TRVPrintComponentEvent)(TCustomRVPrint* Sender, Vcl::Controls::TControl* PrintMe, Vcl::Graphics::TBitmap* &ComponentImage);

typedef void __fastcall (__closure *TRVPrintingEvent)(Richview::TCustomRichView* Sender, int PageCompleted, Richview::TRVPrintingStep Step);

class DELPHICLASS TRVPrint;
typedef void __fastcall (__closure *TRVPagePrepaintEvent)(TRVPrint* Sender, int PageNo, Vcl::Graphics::TCanvas* Canvas, bool Preview, const System::Types::TRect &PageRect, const System::Types::TRect &PrintAreaRect);

enum DECLSPEC_DENUM TRVFixMarginsMode : unsigned char { rvfmmAutoCorrect, rvfmmIgnore };

class DELPHICLASS TRVEndnotePage;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEndnotePage : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int Index;
	int Page;
public:
	/* TObject.Create */ inline __fastcall TRVEndnotePage(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVEndnotePage(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVEndnotePageList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEndnotePageList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVEndnotePage* operator[](int Index) { return Items[Index]; }
	
private:
	TRVEndnotePage* __fastcall GetItems(int Index);
	void __fastcall SetItems(int Index, TRVEndnotePage* const Value);
	
public:
	__property TRVEndnotePage* Items[int Index] = {read=GetItems, write=SetItems/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVEndnotePageList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVEndnotePageList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVEndnoteList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEndnoteList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	Ptrvdata::TRVEndnotePtblRVData* operator[](int Index) { return Items[Index]; }
	
private:
	TCustomRVPrint* FOwner;
	TRVEndnotePageList* Pages;
	Ptrvdata::TRVEndnotePtblRVData* __fastcall GetItems(int Index);
	void __fastcall SetItems(int Index, Ptrvdata::TRVEndnotePtblRVData* const Value);
	
public:
	__fastcall TRVEndnoteList(TCustomRVPrint* AOwner);
	__fastcall virtual ~TRVEndnoteList(void);
	void __fastcall DrawPage(int GlobalPageNo, int PageNo, Vcl::Graphics::TCanvas* Canvas, bool Preview, bool Correction);
	__property Ptrvdata::TRVEndnotePtblRVData* Items[int Index] = {read=GetItems, write=SetItems/*, default*/};
};

#pragma pack(pop)

class DELPHICLASS TRVFootnoteList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFootnoteList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	Ptrvdata::TRVFootnotePtblRVData* operator[](int Index) { return Items[Index]; }
	
private:
	TCustomRVPrint* FOwner;
	Ptrvdata::TRVFootnotePtblRVData* __fastcall GetItems(int Index);
	void __fastcall SetItems(int Index, Ptrvdata::TRVFootnotePtblRVData* const Value);
	
public:
	__fastcall TRVFootnoteList(TCustomRVPrint* AOwner);
	void __fastcall SortByFootnotes(void);
	Ptrvdata::TRVFootnotePtblRVData* __fastcall FindByFootnote(Rvnote::TRVFootnoteItemInfo* Footnote);
	__property Ptrvdata::TRVFootnotePtblRVData* Items[int Index] = {read=GetItems, write=SetItems/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVFootnoteList(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFootnoteDrawItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFootnoteDrawItem : public Dlines::TRVComplexLineHeightDrawLineInfo
{
	typedef Dlines::TRVComplexLineHeightDrawLineInfo inherited;
	
public:
	Ptrvdata::TRVFootnotePtblRVData* DocumentRVData;
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVFootnoteDrawItem(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : Dlines::TRVComplexLineHeightDrawLineInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVFootnoteDrawItem(void) : Dlines::TRVComplexLineHeightDrawLineInfo() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVFootnoteDrawItem(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TCustomPrintableRV;
class PASCALIMPLEMENTATION TCustomPrintableRV : public Richview::TCustomRichView
{
	typedef Richview::TCustomRichView inherited;
	
private:
	TCustomRVPrint* FRVPrint;
	
protected:
	int FNoteSeparatorHeight;
	int FNoteLineWidth;
	TRVEndnoteList* FEndnotes;
	TRVFootnoteList* FFootnotes;
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	
public:
	__fastcall virtual TCustomPrintableRV(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomPrintableRV(void);
	Ptrvdata::TRVFootnotePtblRVData* __fastcall GetFootnoteRVData(Rvnote::TRVFootnoteItemInfo* Footnote);
	void __fastcall DrawNoteSeparatorAbove(int PageNo, int Y, Vcl::Graphics::TCanvas* Canvas, bool FullSize);
	void __fastcall CalcFootnotesCoords(System::Classes::TList* References, int PageNo);
	void __fastcall FreeNotesLists(void);
	DYNAMIC void __fastcall ApplyLayoutInfo(Crvdata::TRVLayoutInfo* Layout);
	virtual bool __fastcall CanUseCustomPPI(void);
	void __fastcall InitFormatPages(void);
	int __fastcall FormatPages(void);
	void __fastcall FinalizeFormatPages(void);
	void __fastcall DrawPage(int PageNo, Vcl::Graphics::TCanvas* Canvas, bool Preview, bool Correction);
	virtual void __fastcall Paint(void);
	__property TCustomRVPrint* RVPrint = {read=FRVPrint, write=FRVPrint};
	__property int NoteSeparatorHeight = {read=FNoteSeparatorHeight, nodefault};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomPrintableRV(HWND ParentWindow) : Richview::TCustomRichView(ParentWindow) { }
	
};


class DELPHICLASS TPrintableRV;
class PASCALIMPLEMENTATION TPrintableRV : public TCustomPrintableRV
{
	typedef TCustomPrintableRV inherited;
	
private:
	void __fastcall DoOnPrinting(int PageCompleted, Richview::TRVPrintingStep Step);
	
protected:
	virtual Rvrvdata::TRichViewRVDataClass __fastcall GetDataClass(void);
	
public:
	bool FMirrorMargins;
	bool FFacingPages;
	bool FTitlePage;
	Rvstyle::TRVUnitsRect* FMargins;
	Rvstyle::TRVUnits FUnits;
	__fastcall virtual TPrintableRV(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TPrintableRV(void);
	void __fastcall PrintPages(int firstPgNo, int lastPgNo, const System::UnicodeString Title, int Copies, bool Collate);
	void __fastcall Print(const System::UnicodeString Title, int Copies, bool Collate);
	void __fastcall ContinuousPrint(void);
public:
	/* TWinControl.CreateParented */ inline __fastcall TPrintableRV(HWND ParentWindow) : TCustomPrintableRV(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TCustomRVPrint : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	bool FPreviewCorrection;
	TRVPrintComponentEvent FOnPrintComponent;
	int FMinPrintedItemNo;
	int FMaxPrintedItemNo;
	bool FIgnorePageBreaks;
	bool __fastcall GetTransparentBackground(void);
	void __fastcall SetTransparentBackground(const bool Value);
	int __fastcall GetPreview100PercentHeight(void);
	int __fastcall GetPreview100PercentWidth(void);
	Rvstyle::TRVColorMode __fastcall GetColorMode(void);
	void __fastcall SetColorMode(const Rvstyle::TRVColorMode Value);
	bool __fastcall GetIsDestinationReady(void);
	int __fastcall GetFirstPageNo(void);
	int __fastcall GetLastPageNo(void);
	
protected:
	virtual void __fastcall Loaded(void);
	DYNAMIC TCustomPrintableRV* __fastcall CreateRichView(void);
	int __fastcall GetPagesCount(void);
	int __fastcall FormatEndnotes(void);
	void __fastcall PreformatFootnotes(void);
	void __fastcall PostformatFootnotes(void);
	DYNAMIC bool __fastcall IgnoreEndnotes(void);
	
public:
	TCustomPrintableRV* rv;
	bool Ready;
	int StartAt;
	int EndAt;
	int FormattingID;
	__fastcall virtual TCustomRVPrint(System::Classes::TComponent* AOwner);
	void __fastcall Clear(void);
	void __fastcall UpdatePaletteInfo(void);
	void __fastcall GetFirstItemOnPage(int PageNo, int &ItemNo, int &OffsetInItem);
	void __fastcall GetFirstItemOnPageEx(int PageNo, int &ItemNo, int &OffsetInItem, int &ExtraData);
	int __fastcall GetPageNo(Crvdata::TCustomRVData* RVData, int ItemNo, int OffsetInItem);
	bool __fastcall IsComplexSoftPageBreak(int PageNo);
	void __fastcall AssignComplexSoftPageBreakToItem(int PageNo, Crvfdata::TCustomRVFormattedData* RVData);
	void __fastcall DrawPreview(int pgNo, Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &PageRect);
	void __fastcall DrawMarginsRect(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &PageRect, int PageNo);
	bool __fastcall SavePageAsRVF(System::Classes::TStream* Stream, int PageNo);
	bool __fastcall SaveFromPageAsRVF(System::Classes::TStream* Stream, int FirstPageNo);
	void __fastcall GetMinimalMargins(System::Types::TRect &MarginsRect, bool ScreenResolution);
	DYNAMIC Richview::TCustomRichView* __fastcall GetSourceRichView(void);
	__property int PagesCount = {read=GetPagesCount, nodefault};
	__property int Preview100PercentWidth = {read=GetPreview100PercentWidth, nodefault};
	__property int Preview100PercentHeight = {read=GetPreview100PercentHeight, nodefault};
	__property bool IsDestinationReady = {read=GetIsDestinationReady, nodefault};
	__property int FirstPageNo = {read=GetFirstPageNo, nodefault};
	__property int LastPageNo = {read=GetLastPageNo, nodefault};
	
__published:
	__property bool PreviewCorrection = {read=FPreviewCorrection, write=FPreviewCorrection, nodefault};
	__property TRVPrintComponentEvent OnPrintComponent = {read=FOnPrintComponent, write=FOnPrintComponent};
	__property bool TransparentBackground = {read=GetTransparentBackground, write=SetTransparentBackground, default=0};
	__property Rvstyle::TRVColorMode ColorMode = {read=GetColorMode, write=SetColorMode, default=1};
	__property int MinPrintedItemNo = {read=FMinPrintedItemNo, write=FMinPrintedItemNo, default=0};
	__property int MaxPrintedItemNo = {read=FMaxPrintedItemNo, write=FMaxPrintedItemNo, default=-1};
	__property bool IgnorePageBreaks = {read=FIgnorePageBreaks, write=FIgnorePageBreaks, default=0};
public:
	/* TComponent.Destroy */ inline __fastcall virtual ~TCustomRVPrint(void) { }
	
};


class PASCALIMPLEMENTATION TRVPrint : public TCustomRVPrint
{
	typedef TCustomRVPrint inherited;
	
private:
	TRVPrintingEvent FOnFormatting;
	TRVPrintingEvent FOnPrinting;
	TRVPagePrepaintEvent FOnPagePrepaint;
	TRVPagePrepaintEvent FOnPagePostPaint;
	bool FClipMargins;
	Richview::TCustomRichView* FPrintMe;
	TRVFixMarginsMode FFixMarginsMode;
	int __fastcall GetLM(void);
	int __fastcall GetRM(void);
	int __fastcall GetTM(void);
	int __fastcall GetBM(void);
	void __fastcall SetLM(int mm);
	void __fastcall SetRM(int mm);
	void __fastcall SetTM(int mm);
	void __fastcall SetBM(int mm);
	bool __fastcall GetMirrorMargins(void);
	void __fastcall SetMirrorMargins(const bool Value);
	int __fastcall GetFooterYMM(void);
	int __fastcall GetHeaderYMM(void);
	void __fastcall SetFooterYMM(const int Value);
	void __fastcall SetHeaderYMM(const int Value);
	Rvstyle::TRVLength __fastcall GetFooterY(void);
	Rvstyle::TRVLength __fastcall GetHeaderY(void);
	void __fastcall SetFooterY(const Rvstyle::TRVLength Value);
	void __fastcall SetHeaderY(const Rvstyle::TRVLength Value);
	Rvstyle::TRVUnitsRect* __fastcall GetMargins(void);
	void __fastcall SetMargins(Rvstyle::TRVUnitsRect* const Value);
	Rvstyle::TRVUnits __fastcall GetUnits(void);
	void __fastcall SetUnits(const Rvstyle::TRVUnits Value);
	void __fastcall LMReader(System::Classes::TReader* reader);
	void __fastcall RMReader(System::Classes::TReader* reader);
	void __fastcall TMReader(System::Classes::TReader* reader);
	void __fastcall BMReader(System::Classes::TReader* reader);
	void __fastcall HYReader(System::Classes::TReader* reader);
	void __fastcall FYReader(System::Classes::TReader* reader);
	bool __fastcall GetFacingPages(void);
	bool __fastcall GetTitlePage(void);
	void __fastcall SetFacingPages(const bool Value);
	void __fastcall SetTitlePage(const bool Value);
	void __fastcall ClearHeaderFooter(void);
	
protected:
	DYNAMIC TCustomPrintableRV* __fastcall CreateRichView(void);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	
public:
	__fastcall virtual TRVPrint(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVPrint(void);
	DYNAMIC Richview::TCustomRichView* __fastcall GetSourceRichView(void);
	void __fastcall AssignSource(Richview::TCustomRichView* PrintMe);
	void __fastcall AssignDocParameters(Rvdocparams::TRVDocParameters* DocParameters);
	void __fastcall SetHeader(Crvfdata::TCustomRVFormattedData* RVData, Rvstyle::TRVHFType HeaderType = (Rvstyle::TRVHFType)(0x0));
	void __fastcall SetFooter(Crvfdata::TCustomRVFormattedData* RVData, Rvstyle::TRVHFType FooterType = (Rvstyle::TRVHFType)(0x0));
	int __fastcall FormatPages(Rvscroll::TRVDisplayOptions PrintOptions);
	void __fastcall PrintPages(int firstPgNo, int lastPgNo, System::UnicodeString Title, int Copies, bool Collate);
	void __fastcall Print(System::UnicodeString Title, int Copies, bool Collate);
	void __fastcall ContinuousPrint(void);
	void __fastcall MakePreview(int pgNo, Vcl::Graphics::TBitmap* bmp);
	void __fastcall MakeScaledPreview(int pgNo, Vcl::Graphics::TBitmap* bmp);
	System::Types::TRect __fastcall GetHeaderRect(int PageNo);
	System::Types::TRect __fastcall GetFooterRect(int PageNo);
	HIDESBASE void __fastcall Clear(void);
	void __fastcall ConvertToUnits(Rvstyle::TRVUnits AUnits);
	__property int LeftMarginMM = {read=GetLM, write=SetLM, nodefault};
	__property int RightMarginMM = {read=GetRM, write=SetRM, nodefault};
	__property int TopMarginMM = {read=GetTM, write=SetTM, nodefault};
	__property int BottomMarginMM = {read=GetBM, write=SetBM, nodefault};
	__property int FooterYMM = {read=GetFooterYMM, write=SetFooterYMM, nodefault};
	__property int HeaderYMM = {read=GetHeaderYMM, write=SetHeaderYMM, nodefault};
	
__published:
	__property Rvstyle::TRVUnitsRect* Margins = {read=GetMargins, write=SetMargins};
	__property Rvstyle::TRVLength FooterY = {read=GetFooterY, write=SetFooterY};
	__property Rvstyle::TRVLength HeaderY = {read=GetHeaderY, write=SetHeaderY};
	__property Rvstyle::TRVUnits Units = {read=GetUnits, write=SetUnits, default=2};
	__property bool ClipMargins = {read=FClipMargins, write=FClipMargins, default=0};
	__property bool MirrorMargins = {read=GetMirrorMargins, write=SetMirrorMargins, default=0};
	__property bool FacingPages = {read=GetFacingPages, write=SetFacingPages, default=0};
	__property bool TitlePage = {read=GetTitlePage, write=SetTitlePage, default=0};
	__property TRVPrintingEvent OnFormatting = {read=FOnFormatting, write=FOnFormatting};
	__property TRVPrintingEvent OnSendingToPrinter = {read=FOnPrinting, write=FOnPrinting};
	__property TRVPagePrepaintEvent OnPagePrepaint = {read=FOnPagePrepaint, write=FOnPagePrepaint};
	__property TRVPagePrepaintEvent OnPagePostpaint = {read=FOnPagePostPaint, write=FOnPagePostPaint};
	__property TRVFixMarginsMode FixMarginsMode = {read=FFixMarginsMode, write=FFixMarginsMode, default=0};
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE HDC __fastcall RV_GetPrinterDC(void);
}	/* namespace Ptblrv */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PTBLRV)
using namespace Ptblrv;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PtblrvHPP
