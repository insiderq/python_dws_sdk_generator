﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVUndo.pas' rev: 27.00 (Windows)

#ifndef RvundoHPP
#define RvundoHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvundo
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVUndoAction : unsigned char { rvuMisc, rvuDeleteItem, rvuDeleteItems, rvuDeleteSubstring, rvuInsertItem, rvuInsertItems, rvuInsertSubstring, rvuNewLine, rvuBR, rvuPara, rvuPageBreak, rvuTyping, rvuTag, rvuStyleNo, rvuCheckpoint, rvuModifyItem, rvuChangeText, rvuChangeVAlign, rvuChangeListMarker, rvuProperty };

class DELPHICLASS TRVUndoInfo;
class DELPHICLASS TRVUndoList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	TRVUndoAction Action;
	TRVUndoList* FUndoList;
	__fastcall virtual TRVUndoInfo(void);
	DYNAMIC bool __fastcall RequiresFullReformat1(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
	DYNAMIC bool __fastcall RequiresSuperFormat(void);
	DYNAMIC bool __fastcall RequiresRepagination(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormattedDocBefore(void);
	DYNAMIC bool __fastcall ChangesStyles(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall SetItemsRange(int &StartItem, int &EndItem, Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC int __fastcall ItemsAdded(void);
	Crvfdata::TCustomRVFormattedData* __fastcall GetUndoListOwnerRVData(void);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoInfo(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TRVUndoInfoClass;

class DELPHICLASS TRVUndoItemNoInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoItemNoInfo : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	int ItemNo;
	int LastAffectedItemNo;
	__fastcall virtual TRVUndoItemNoInfo(void);
	DYNAMIC void __fastcall SetItemsRange(int &StartItem, int &EndItem, Rvrvdata::TRichViewRVData* RVData);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoItemNoInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoChangeSubDoc;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoChangeSubDoc : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Crvdata::TCustomRVData* SubDoc;
	Rvclasses::TRVMemoryStream* Stream;
	DYNAMIC bool __fastcall RequiresFormat(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresRepagination(Rvrvdata::TRichViewRVData* RVData);
	__fastcall virtual ~TRVUndoChangeSubDoc(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoChangeSubDoc(void) : TRVUndoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoReformateRange;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoReformateRange : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	bool SuperReformat;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresSuperFormat(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoReformateRange(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoReformateRange(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoChangeVAlignInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoChangeVAlignInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	Rvstyle::TRVVAlign VAlign;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoChangeVAlignInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoChangeVAlignInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoResizeInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoResizeInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	int OldWidth;
	
public:
	int Width;
	int Height;
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoResizeInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoResizeInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoRawStringInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoRawStringInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	Rvtypes::TRVRawByteString s;
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoRawStringInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoRawStringInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoChangeTextInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoChangeTextInfo : public TRVUndoRawStringInfo
{
	typedef TRVUndoRawStringInfo inherited;
	
private:
	int OldWidth;
	
public:
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoChangeTextInfo(void) : TRVUndoRawStringInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoChangeTextInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoSubStringInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoSubStringInfo : public TRVUndoRawStringInfo
{
	typedef TRVUndoRawStringInfo inherited;
	
public:
	int Index;
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoSubStringInfo(void) : TRVUndoRawStringInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoSubStringInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoItemInfo : public TRVUndoRawStringInfo
{
	typedef TRVUndoRawStringInfo inherited;
	
public:
	Rvitem::TCustomRVItemInfo* Item;
	__fastcall virtual ~TRVUndoItemInfo(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoItemInfo(void) : TRVUndoRawStringInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoReplaceItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoReplaceItemInfo : public TRVUndoItemInfo
{
	typedef TRVUndoItemInfo inherited;
	
public:
	int ItemNo;
	DYNAMIC bool __fastcall RequiresFormat(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemInfo.Destroy */ inline __fastcall virtual ~TRVUndoReplaceItemInfo(void) { }
	
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoReplaceItemInfo(void) : TRVUndoItemInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoItemListInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoItemListInfo : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Rvitem::TRVItemList* List;
	__fastcall virtual TRVUndoItemListInfo(void);
	__fastcall virtual ~TRVUndoItemListInfo(void);
};

#pragma pack(pop)

class DELPHICLASS TRVUndoItemRangeInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoItemRangeInfo : public TRVUndoItemListInfo
{
	typedef TRVUndoItemListInfo inherited;
	
public:
	int StartItemNo;
	int LastAffectedItemNo;
	__fastcall virtual TRVUndoItemRangeInfo(void);
	DYNAMIC void __fastcall SetItemsRange(int &StartItem, int &EndItem, Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemListInfo.Destroy */ inline __fastcall virtual ~TRVUndoItemRangeInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoListInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoListInfo : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Rvclasses::TRVIntegerList* List;
	__fastcall virtual TRVUndoListInfo(void);
	__fastcall virtual ~TRVUndoListInfo(void);
};

#pragma pack(pop)

class DELPHICLASS TRVUndoParaListInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoParaListInfo : public TRVUndoListInfo
{
	typedef TRVUndoListInfo inherited;
	
private:
	bool FR;
	
public:
	int StartItemNo;
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall SetItemsRange(int &StartItem, int &EndItem, Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoListInfo.Create */ inline __fastcall virtual TRVUndoParaListInfo(void) : TRVUndoListInfo() { }
	/* TRVUndoListInfo.Destroy */ inline __fastcall virtual ~TRVUndoParaListInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoParaInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoParaInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	bool FR;
	
public:
	int ParaNo;
	int Count;
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall SetItemsRange(int &StartItem, int &EndItem, Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoParaInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoParaInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoStyleNoInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoStyleNoInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	int OldWidth;
	
public:
	int WasStyleNo;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoStyleNoInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoStyleNoInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoAssociatedTextStyleNoInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoAssociatedTextStyleNoInfo : public TRVUndoStyleNoInfo
{
	typedef TRVUndoStyleNoInfo inherited;
	
public:
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoAssociatedTextStyleNoInfo(void) : TRVUndoStyleNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoAssociatedTextStyleNoInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoDeleteItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoDeleteItemInfo : public TRVUndoItemInfo
{
	typedef TRVUndoItemInfo inherited;
	
private:
	bool FR;
	
public:
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC int __fastcall ItemsAdded(void);
public:
	/* TRVUndoItemInfo.Destroy */ inline __fastcall virtual ~TRVUndoDeleteItemInfo(void) { }
	
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoDeleteItemInfo(void) : TRVUndoItemInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoModifyItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoModifyItemInfo : public TRVUndoItemInfo
{
	typedef TRVUndoItemInfo inherited;
	
public:
	DYNAMIC bool __fastcall RequiresFullReformat1(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemInfo.Destroy */ inline __fastcall virtual ~TRVUndoModifyItemInfo(void) { }
	
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoModifyItemInfo(void) : TRVUndoItemInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoNewLineInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoNewLineInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	bool FR;
	
public:
	bool WasSameAsPrev;
	int WasParaNo;
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoNewLineInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoNewLineInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoBRInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoBRInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	bool WasBR;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoBRInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoBRInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoPageBreakInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoPageBreakInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	bool WasPageBreak;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoPageBreakInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoPageBreakInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoClearTextFlowInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoClearTextFlowInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	bool WasClearLeft;
	bool WasClearRight;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoClearTextFlowInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoClearTextFlowInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoExtraIntProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoExtraIntProperty : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	int OldWidth;
	
public:
	int OldValue;
	int Prop;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoExtraIntProperty(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoExtraIntProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoExtraStrProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoExtraStrProperty : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	System::UnicodeString OldValue;
	int Prop;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoExtraStrProperty(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoExtraStrProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoDeleteItemsInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoDeleteItemsInfo : public TRVUndoItemRangeInfo
{
	typedef TRVUndoItemRangeInfo inherited;
	
private:
	int EndItemNo;
	bool FR;
	
public:
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC int __fastcall ItemsAdded(void);
public:
	/* TRVUndoItemRangeInfo.Create */ inline __fastcall virtual TRVUndoDeleteItemsInfo(void) : TRVUndoItemRangeInfo() { }
	
public:
	/* TRVUndoItemListInfo.Destroy */ inline __fastcall virtual ~TRVUndoDeleteItemsInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoDeleteSubstringInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoDeleteSubstringInfo : public TRVUndoSubStringInfo
{
	typedef TRVUndoSubStringInfo inherited;
	
private:
	int OldWidth;
	
public:
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoDeleteSubstringInfo(void) : TRVUndoSubStringInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoDeleteSubstringInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoInsertSubstringInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoInsertSubstringInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	int OldWidth;
	
public:
	int Index;
	int Length;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoInsertSubstringInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoInsertSubstringInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRedoTypingInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRedoTypingInfo : public TRVUndoSubStringInfo
{
	typedef TRVUndoSubStringInfo inherited;
	
private:
	int OldWidth;
	
public:
	bool Unicode;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVRedoTypingInfo(void) : TRVUndoSubStringInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRedoTypingInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoTypingInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoTypingInfo : public TRVUndoInsertSubstringInfo
{
	typedef TRVUndoInsertSubstringInfo inherited;
	
private:
	int OldWidth;
	
public:
	bool Unicode;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoTypingInfo(void) : TRVUndoInsertSubstringInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoTypingInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoInsertItemsInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoInsertItemsInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	bool FR;
	
public:
	int Count;
	DYNAMIC bool __fastcall RequiresFullReformat1(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC int __fastcall ItemsAdded(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoInsertItemsInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoInsertItemsInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoInsertItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoInsertItemInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	bool FR;
	
public:
	DYNAMIC bool __fastcall RequiresFullReformat1(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC int __fastcall ItemsAdded(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoInsertItemInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoInsertItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoTagInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoTagInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	Rvstyle::TRVTag WasTag;
	bool TagsArePChars;
	DYNAMIC bool __fastcall RequiresFormat(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	__fastcall virtual ~TRVUndoTagInfo(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoTagInfo(void) : TRVUndoItemNoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoAddCPInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoAddCPInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoAddCPInfo(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoAddCPInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoDeleteCPInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoDeleteCPInfo : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
public:
	Rvitem::TRVCPInfo* Checkpoint;
	bool TagsArePChars;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	__fastcall virtual ~TRVUndoDeleteCPInfo(void);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoDeleteCPInfo(void) : TRVUndoItemNoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoModifyItemProps;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoModifyItemProps : public TRVUndoItemNoInfo
{
	typedef TRVUndoItemNoInfo inherited;
	
private:
	int OldW;
	
public:
	bool AffectWidth;
	bool AffectSize;
	System::TObject* SubObject;
	virtual TRVUndoInfoClass __fastcall GetOppositeClass(void);
	DYNAMIC bool __fastcall RequiresFormat(void);
	DYNAMIC bool __fastcall RequiresFullReformat1(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall SetOppositeUndoInfoProps(TRVUndoModifyItemProps* UndoInfo);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoModifyItemProps(void) : TRVUndoItemNoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoModifyItemProps(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoModifyItemTerminator;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoModifyItemTerminator : public TRVUndoModifyItemProps
{
	typedef TRVUndoModifyItemProps inherited;
	
public:
	bool Opening;
	__fastcall virtual TRVUndoModifyItemTerminator(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoModifyItemTerminator(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoModifyItemIntProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoModifyItemIntProperty : public TRVUndoModifyItemProps
{
	typedef TRVUndoModifyItemProps inherited;
	
public:
	System::UnicodeString PropertyName;
	int Value;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoModifyItemIntProperty(void) : TRVUndoModifyItemProps() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoModifyItemIntProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoModifyItemStrProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoModifyItemStrProperty : public TRVUndoModifyItemProps
{
	typedef TRVUndoModifyItemProps inherited;
	
public:
	System::UnicodeString PropertyName;
	System::UnicodeString Value;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoItemNoInfo.Create */ inline __fastcall virtual TRVUndoModifyItemStrProperty(void) : TRVUndoModifyItemProps() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoModifyItemStrProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoModifyItemIntProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoModifyItemIntProperties : public TRVUndoModifyItemProps
{
	typedef TRVUndoModifyItemProps inherited;
	
public:
	System::Classes::TStringList* PropList;
	__fastcall virtual TRVUndoModifyItemIntProperties(void);
	__fastcall virtual ~TRVUndoModifyItemIntProperties(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
};

#pragma pack(pop)

class DELPHICLASS TRVCompositeUndo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVCompositeUndo : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	int ItemNo;
	bool IsRedo;
	TRVUndoList* UndoList;
	__fastcall virtual ~TRVCompositeUndo(void);
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC void __fastcall SetItemsRange(int &StartItem, int &EndItem, Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVCompositeUndo(void) : TRVUndoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoStyleTemplate;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoStyleTemplate : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Rvstyle::TRVStyleTemplateCollection* OldStyleTemplates;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
	__fastcall virtual ~TRVUndoStyleTemplate(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoStyleTemplate(void) : TRVUndoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoTextParaStyles;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoTextParaStyles : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Crvdata::TCustomRVData* SubDoc;
	Rvstyle::TFontInfos* OldTextStyles;
	Rvstyle::TParaInfos* OldParaStyles;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall ChangesStyles(void);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresRepagination(Rvrvdata::TRichViewRVData* RVData);
	__fastcall virtual ~TRVUndoTextParaStyles(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoTextParaStyles(void) : TRVUndoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoChangeAllInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoChangeAllInfos : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Crvdata::TCustomRVData* SubDoc;
	Rvclasses::TRVIntegerList* Indices;
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall ChangesStyles(void);
	__fastcall virtual ~TRVUndoChangeAllInfos(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoChangeAllInfos(void) : TRVUndoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoChangeAllParasInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoChangeAllParasInfo : public TRVUndoChangeAllInfos
{
	typedef TRVUndoChangeAllInfos inherited;
	
public:
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoChangeAllInfos.Destroy */ inline __fastcall virtual ~TRVUndoChangeAllParasInfo(void) { }
	
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoChangeAllParasInfo(void) : TRVUndoChangeAllInfos() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoChangeAllTextStyleInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoChangeAllTextStyleInfo : public TRVUndoChangeAllInfos
{
	typedef TRVUndoChangeAllInfos inherited;
	
public:
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoChangeAllInfos.Destroy */ inline __fastcall virtual ~TRVUndoChangeAllTextStyleInfo(void) { }
	
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoChangeAllTextStyleInfo(void) : TRVUndoChangeAllInfos() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoEditorIntProp;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoEditorIntProp : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Richview::TRVIntProperty Prop;
	int OldValue;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
	DYNAMIC bool __fastcall RequiresFullReformat2(Rvrvdata::TRichViewRVData* RVData);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoEditorIntProp(void) : TRVUndoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoEditorIntProp(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoEditorStrProp;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoEditorStrProp : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Richview::TRVStrProperty Prop;
	System::UnicodeString OldValue;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoEditorStrProp(void) : TRVUndoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoEditorStrProp(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoEditorFloatProp;
class PASCALIMPLEMENTATION TRVUndoEditorFloatProp : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Richview::TRVFloatProperty Prop;
	Rvstyle::TRVLength OldValue;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoEditorFloatProp(void) : TRVUndoInfo() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVUndoEditorFloatProp(void) { }
	
};


class DELPHICLASS TRVUndoBackgroundImage;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoBackgroundImage : public TRVUndoInfo
{
	typedef TRVUndoInfo inherited;
	
public:
	Vcl::Graphics::TGraphic* OldGraphic;
	DYNAMIC void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	DYNAMIC bool __fastcall RequiresFormat(void);
	__fastcall virtual ~TRVUndoBackgroundImage(void);
public:
	/* TRVUndoInfo.Create */ inline __fastcall virtual TRVUndoBackgroundImage(void) : TRVUndoInfo() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVUndoInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoInfos : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
private:
	void __fastcall PerformUndo(Rvrvdata::TRichViewRVData* RVData, bool Reformat);
	
public:
	System::UnicodeString Caption;
	Rvedit::TRVUndoType UndoType;
	int CaretItemNo;
	int CaretOffs;
	void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData, bool Reformat);
	void __fastcall Redo(Rvrvdata::TRichViewRVData* RVData, bool Reformat);
	bool __fastcall CanDelete(void);
	void __fastcall ChangeUndoList(TRVUndoList* UndoList);
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVUndoInfos(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVUndoInfos(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVUndoList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
private:
	int FReformatLock;
	void __fastcall Pop(void);
	
public:
	Crvfdata::TCustomRVFormattedData* FRVData;
	int Limit;
	int GroupModeCount;
	__fastcall TRVUndoList(Crvfdata::TCustomRVFormattedData* ARVData);
	void __fastcall PopIfEmpty(void);
	bool __fastcall BeginItem(Rvedit::TRVUndoType UndoType, const System::UnicodeString Caption, int CaretItemNo, int CaretOffs, Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall EndItem(Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall AddInfo(TRVUndoInfo* Info, Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall AddInfoIfPossible(TRVUndoInfo* Info, Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall AddInfos(TRVUndoInfos* Infos, Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall AddTyping(int CaretItemNo, int CaretOffs, bool Unicode, Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall AddUntyping(const Rvtypes::TRVRawByteString c, int CaretItemNo, int CaretOffs, Rvedit::TCustomRichViewEdit* Editor);
	void __fastcall Undo(Rvrvdata::TRichViewRVData* RVData);
	void __fastcall Redo(Rvrvdata::TRichViewRVData* RVData);
	Rvedit::TRVUndoType __fastcall CurrentUndoType(void);
	System::UnicodeString __fastcall CurrentUndoCaption(void);
	void __fastcall LockRFR(void);
	void __fastcall UnlockRFR(void);
	void __fastcall SetUndoGroupMode(bool GroupUndo, Rvedit::TCustomRichViewEdit* Editor);
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVUndoList(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRedoList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRedoList : public TRVUndoList
{
	typedef TRVUndoList inherited;
	
public:
	/* TRVUndoList.Create */ inline __fastcall TRVRedoList(Crvfdata::TCustomRVFormattedData* ARVData) : TRVUndoList(ARVData) { }
	
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRedoList(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvundo */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVUNDO)
using namespace Rvundo;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvundoHPP
