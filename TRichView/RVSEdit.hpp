﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVSEdit.pas' rev: 27.00 (Windows)

#ifndef RvseditHPP
#define RvseditHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <DesignIntf.hpp>	// Pascal unit
#include <DesignEditors.hpp>	// Pascal unit
#include <ColnEdit.hpp>	// Pascal unit
#include <VCLEditors.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVCodePages.hpp>	// Pascal unit
#include <RVReport.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvsedit
{
//-- type declarations -------------------------------------------------------
typedef Designeditors::TDefaultEditor TRVComponentEditor;

class DELPHICLASS TRVSEditor;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSEditor : public Designeditors::TDefaultEditor
{
	typedef Designeditors::TDefaultEditor inherited;
	
private:
	void __fastcall LoadStylesFromINIFile(void);
	void __fastcall SaveStylesToINIFile(void);
	void __fastcall LoadRVST(void);
	void __fastcall SaveRVST(void);
	
protected:
	int VerbIndex;
	virtual void __fastcall EditProperty(const Designintf::_di_IProperty PropertyEditor, bool &Continue);
	
public:
	virtual void __fastcall ExecuteVerb(int Index);
	virtual System::UnicodeString __fastcall GetVerb(int Index);
	virtual int __fastcall GetVerbCount(void);
public:
	/* TComponentEditor.Create */ inline __fastcall virtual TRVSEditor(System::Classes::TComponent* AComponent, Designintf::_di_IDesigner ADesigner) : Designeditors::TDefaultEditor(AComponent, ADesigner) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVSEditor(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVEEditor;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEEditor : public Designeditors::TDefaultEditor
{
	typedef Designeditors::TDefaultEditor inherited;
	
public:
	virtual void __fastcall ExecuteVerb(int Index);
	virtual System::UnicodeString __fastcall GetVerb(int Index);
	virtual int __fastcall GetVerbCount(void);
public:
	/* TComponentEditor.Create */ inline __fastcall virtual TRVEEditor(System::Classes::TComponent* AComponent, Designintf::_di_IDesigner ADesigner) : Designeditors::TDefaultEditor(AComponent, ADesigner) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVEEditor(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVCodePageProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVCodePageProperty : public Designeditors::TIntegerProperty
{
	typedef Designeditors::TIntegerProperty inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual System::UnicodeString __fastcall GetValue(void);
	virtual void __fastcall GetValues(System::Classes::TGetStrProc Proc);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVCodePageProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TIntegerProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVCodePageProperty(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};

#pragma pack(pop)

typedef System::UnicodeString __fastcall (*TRVGetUnitNameFunc)(System::Classes::TPersistent* Obj, const System::UnicodeString PropName);

class DELPHICLASS TRVStyleLengthProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStyleLengthProperty : public Designeditors::TIntegerProperty
{
	typedef Designeditors::TIntegerProperty inherited;
	
protected:
	System::UnicodeString __fastcall GetUnits(void);
	DYNAMIC System::UnicodeString __fastcall GetUnitName(System::Classes::TPersistent* Obj, const System::UnicodeString PropName);
	
public:
	void __fastcall PropDrawName(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool ASelected);
	System::Types::TRect __fastcall PropDrawNameRect(const System::Types::TRect &ARect);
	System::Types::TRect __fastcall PropDrawValueRect(const System::Types::TRect &ARect);
	void __fastcall PropDrawValue(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool ASelected);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVStyleLengthProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TIntegerProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVStyleLengthProperty(void) { }
	
private:
	void *__ICustomPropertyDrawing80;	// Vcleditors::ICustomPropertyDrawing80 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {73100176-DF0B-4900-AA52-4E67D7D04895}
	operator Vcleditors::_di_ICustomPropertyDrawing80()
	{
		Vcleditors::_di_ICustomPropertyDrawing80 intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator Vcleditors::ICustomPropertyDrawing80*(void) { return (Vcleditors::ICustomPropertyDrawing80*)&__ICustomPropertyDrawing80; }
	#endif
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {E1A50419-1288-4B26-9EFA-6608A35F0824}
	operator Vcleditors::_di_ICustomPropertyDrawing()
	{
		Vcleditors::_di_ICustomPropertyDrawing intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator Vcleditors::ICustomPropertyDrawing*(void) { return (Vcleditors::ICustomPropertyDrawing*)&__ICustomPropertyDrawing80; }
	#endif
	
};

#pragma pack(pop)

class DELPHICLASS TRVLineSpacingValueProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVLineSpacingValueProperty : public TRVStyleLengthProperty
{
	typedef TRVStyleLengthProperty inherited;
	
protected:
	DYNAMIC System::UnicodeString __fastcall GetUnitName(System::Classes::TPersistent* Obj, const System::UnicodeString PropName);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVLineSpacingValueProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : TRVStyleLengthProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVLineSpacingValueProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVLengthProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVLengthProperty : public Designeditors::TFloatProperty
{
	typedef Designeditors::TFloatProperty inherited;
	
protected:
	System::UnicodeString __fastcall GetUnits(void);
	DYNAMIC System::UnicodeString __fastcall GetUnitName(System::Classes::TPersistent* Obj, const System::UnicodeString PropName);
	
public:
	void __fastcall PropDrawName(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool ASelected);
	System::Types::TRect __fastcall PropDrawNameRect(const System::Types::TRect &ARect);
	System::Types::TRect __fastcall PropDrawValueRect(const System::Types::TRect &ARect);
	void __fastcall PropDrawValue(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool ASelected);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVLengthProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TFloatProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVLengthProperty(void) { }
	
private:
	void *__ICustomPropertyDrawing80;	// Vcleditors::ICustomPropertyDrawing80 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {73100176-DF0B-4900-AA52-4E67D7D04895}
	operator Vcleditors::_di_ICustomPropertyDrawing80()
	{
		Vcleditors::_di_ICustomPropertyDrawing80 intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator Vcleditors::ICustomPropertyDrawing80*(void) { return (Vcleditors::ICustomPropertyDrawing80*)&__ICustomPropertyDrawing80; }
	#endif
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {E1A50419-1288-4B26-9EFA-6608A35F0824}
	operator Vcleditors::_di_ICustomPropertyDrawing()
	{
		Vcleditors::_di_ICustomPropertyDrawing intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator Vcleditors::ICustomPropertyDrawing*(void) { return (Vcleditors::ICustomPropertyDrawing*)&__ICustomPropertyDrawing80; }
	#endif
	
};

#pragma pack(pop)

class DELPHICLASS TRVStyleTemplateIdProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStyleTemplateIdProperty : public Designeditors::TIntegerProperty
{
	typedef Designeditors::TIntegerProperty inherited;
	
private:
	Rvstyle::TRVStyleTemplateCollection* __fastcall GetCollection(void);
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual System::UnicodeString __fastcall GetValue(void);
	virtual void __fastcall GetValues(System::Classes::TGetStrProc Proc);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVStyleTemplateIdProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TIntegerProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVStyleTemplateIdProperty(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};

#pragma pack(pop)

class DELPHICLASS TRVStyleTemplateNameProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStyleTemplateNameProperty : public Designeditors::TStringProperty
{
	typedef Designeditors::TStringProperty inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual void __fastcall GetValues(System::Classes::TGetStrProc Proc);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVStyleTemplateNameProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TStringProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVStyleTemplateNameProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVStyleTemplateCollectionProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStyleTemplateCollectionProperty : public Colnedit::TCollectionProperty
{
	typedef Colnedit::TCollectionProperty inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVStyleTemplateCollectionProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Colnedit::TCollectionProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVStyleTemplateCollectionProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVLargeSetEditProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVLargeSetEditProperty : public Designeditors::TPropertyEditor
{
	typedef Designeditors::TPropertyEditor inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVLargeSetEditProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TPropertyEditor(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVLargeSetEditProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVValidParaPropertiesEditProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVValidParaPropertiesEditProperty : public TRVLargeSetEditProperty
{
	typedef TRVLargeSetEditProperty inherited;
	
public:
	virtual void __fastcall GetProperties(Designintf::TGetPropProc Proc);
	virtual System::UnicodeString __fastcall GetValue(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVValidParaPropertiesEditProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : TRVLargeSetEditProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVValidParaPropertiesEditProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVValidTextPropertiesEditProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVValidTextPropertiesEditProperty : public TRVLargeSetEditProperty
{
	typedef TRVLargeSetEditProperty inherited;
	
public:
	virtual void __fastcall GetProperties(Designintf::TGetPropProc Proc);
	virtual System::UnicodeString __fastcall GetValue(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVValidTextPropertiesEditProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : TRVLargeSetEditProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVValidTextPropertiesEditProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVLargeSetElementProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVLargeSetElementProperty : public Designeditors::TNestedProperty
{
	typedef Designeditors::TNestedProperty inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual void __fastcall GetValues(System::Classes::TGetStrProc Proc);
public:
	/* TNestedProperty.Create */ inline __fastcall TRVLargeSetElementProperty(Designeditors::TPropertyEditor* Parent) : Designeditors::TNestedProperty(Parent) { }
	/* TNestedProperty.Destroy */ inline __fastcall virtual ~TRVLargeSetElementProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVValidParaPropertiesElementProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVValidParaPropertiesElementProperty : public TRVLargeSetElementProperty
{
	typedef TRVLargeSetElementProperty inherited;
	
private:
	Rvstyle::TRVParaInfoProperty FElement;
	
protected:
	__fastcall TRVValidParaPropertiesElementProperty(Designeditors::TPropertyEditor* Parent, Rvstyle::TRVParaInfoProperty AElement);
	__property Rvstyle::TRVParaInfoProperty Element = {read=FElement, nodefault};
	virtual bool __fastcall GetIsDefault(void);
	
public:
	virtual bool __fastcall AllEqual(void);
	virtual System::UnicodeString __fastcall GetName(void);
	virtual System::UnicodeString __fastcall GetValue(void);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
	Rvstyle::TRVParaInfoProperties __fastcall GetRealValueAt(int Index);
	void __fastcall SetRealValue(const Rvstyle::TRVParaInfoProperties &Value);
public:
	/* TNestedProperty.Destroy */ inline __fastcall virtual ~TRVValidParaPropertiesElementProperty(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};

#pragma pack(pop)

class DELPHICLASS TRVValidTextPropertiesElementProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVValidTextPropertiesElementProperty : public TRVLargeSetElementProperty
{
	typedef TRVLargeSetElementProperty inherited;
	
private:
	Rvstyle::TRVFontInfoProperty FElement;
	
protected:
	__fastcall TRVValidTextPropertiesElementProperty(Designeditors::TPropertyEditor* Parent, Rvstyle::TRVFontInfoProperty AElement);
	__property Rvstyle::TRVFontInfoProperty Element = {read=FElement, nodefault};
	virtual bool __fastcall GetIsDefault(void);
	
public:
	virtual bool __fastcall AllEqual(void);
	virtual System::UnicodeString __fastcall GetName(void);
	virtual System::UnicodeString __fastcall GetValue(void);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
	Rvstyle::TRVFontInfoProperties __fastcall GetRealValueAt(int Index);
	void __fastcall SetRealValue(const Rvstyle::TRVFontInfoProperties &Value);
public:
	/* TNestedProperty.Destroy */ inline __fastcall virtual ~TRVValidTextPropertiesElementProperty(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVGetUnitNameFunc RVGetUnitNameFunc;
extern DELPHI_PACKAGE TRVGetUnitNameFunc RVGetRVUnitNameFunc;
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetRVUnitsName(Rvstyle::TRVUnits Units);
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Rvsedit */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSEDIT)
using namespace Rvsedit;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvseditHPP
