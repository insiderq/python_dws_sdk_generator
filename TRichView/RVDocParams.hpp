﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDocParams.pas' rev: 27.00 (Windows)

#ifndef RvdocparamsHPP
#define RvdocparamsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdocparams
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVDocParameters;
#pragma pack(push,2)
class PASCALIMPLEMENTATION TRVDocParameters : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	Rvstyle::TRVLength FPageWidth;
	Rvstyle::TRVLength FPageHeight;
	Rvstyle::TRVLength FLeftMargin;
	Rvstyle::TRVLength FRightMargin;
	Rvstyle::TRVLength FTopMargin;
	Rvstyle::TRVLength FBottomMargin;
	Rvstyle::TRVLength FHeaderY;
	Rvstyle::TRVLength FFooterY;
	Rvstyle::TRVUnits FUnits;
	System::Uitypes::TPrinterOrientation FOrientation;
	int FZoomPercent;
	Rvscroll::TRVZoomMode FZoomMode;
	bool FMirrorMargins;
	bool FFacingPages;
	bool FTitlePage;
	System::UnicodeString FAuthor;
	System::UnicodeString FTitle;
	System::UnicodeString FComments;
	bool __fastcall StorePageHeight(void);
	bool __fastcall StorePageWidth(void);
	bool __fastcall StoreBottomMargin(void);
	bool __fastcall StoreLeftMargin(void);
	bool __fastcall StoreRightMargin(void);
	bool __fastcall StoreTopMargin(void);
	bool __fastcall StoreHeaderY(void);
	bool __fastcall StoreFooterY(void);
	bool __fastcall StoreAuthor(void);
	bool __fastcall StoreComments(void);
	bool __fastcall StoreTitle(void);
	
public:
	__fastcall TRVDocParameters(void);
	void __fastcall Reset(void);
	void __fastcall ResetLayout(void);
	void __fastcall ConvertToUnits(Rvstyle::TRVUnits AUnits);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	bool __fastcall AreAllValuesDefault(void);
	int __fastcall GetRVFLineCount(void);
	void __fastcall SaveToRVF(System::Classes::TStream* Stream);
	void __fastcall ReadProperyFromString(const Rvtypes::TRVRawByteString s, bool UTF8);
	void __fastcall SaveToRTF(System::Classes::TStream* Stream, bool SaveAnsi);
	void __fastcall SaveOOXMLSectionProperties(System::Classes::TStream* Stream);
	void __fastcall SaveOOXMLDocumentSettings(System::Classes::TStream* Stream);
	bool __fastcall HasOOXMLCoreProperties(void);
	void __fastcall SaveOOXMLCoreProperties(System::Classes::TStream* Stream);
	int __fastcall ToTwips(Rvstyle::TRVLength Value);
	Rvstyle::TRVLength __fastcall FromTwips(int Value);
	Rvstyle::TRVLength __fastcall UnitsPerInch(Rvstyle::TRVUnits Units);
	
__published:
	__property Rvstyle::TRVLength PageWidth = {read=FPageWidth, write=FPageWidth, stored=StorePageWidth};
	__property Rvstyle::TRVLength PageHeight = {read=FPageHeight, write=FPageHeight, stored=StorePageHeight};
	__property Rvstyle::TRVUnits Units = {read=FUnits, write=FUnits, default=0};
	__property System::Uitypes::TPrinterOrientation Orientation = {read=FOrientation, write=FOrientation, default=0};
	__property Rvstyle::TRVLength LeftMargin = {read=FLeftMargin, write=FLeftMargin, stored=StoreLeftMargin};
	__property Rvstyle::TRVLength RightMargin = {read=FRightMargin, write=FRightMargin, stored=StoreRightMargin};
	__property Rvstyle::TRVLength TopMargin = {read=FTopMargin, write=FTopMargin, stored=StoreTopMargin};
	__property Rvstyle::TRVLength BottomMargin = {read=FBottomMargin, write=FBottomMargin, stored=StoreBottomMargin};
	__property Rvstyle::TRVLength HeaderY = {read=FHeaderY, write=FHeaderY, stored=StoreHeaderY};
	__property Rvstyle::TRVLength FooterY = {read=FFooterY, write=FFooterY, stored=StoreFooterY};
	__property int ZoomPercent = {read=FZoomPercent, write=FZoomPercent, default=100};
	__property Rvscroll::TRVZoomMode ZoomMode = {read=FZoomMode, write=FZoomMode, default=2};
	__property bool MirrorMargins = {read=FMirrorMargins, write=FMirrorMargins, default=0};
	__property System::UnicodeString Author = {read=FAuthor, write=FAuthor, stored=StoreAuthor};
	__property System::UnicodeString Title = {read=FTitle, write=FTitle, stored=StoreTitle};
	__property System::UnicodeString Comments = {read=FComments, write=FComments, stored=StoreComments};
	__property bool FacingPages = {read=FFacingPages, write=FFacingPages, default=0};
	__property bool TitlePage = {read=FTitlePage, write=FTitlePage, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVDocParameters(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvdocparams */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDOCPARAMS)
using namespace Rvdocparams;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdocparamsHPP
