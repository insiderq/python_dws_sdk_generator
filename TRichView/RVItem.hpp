﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVItem.pas' rev: 27.00 (Windows)

#ifndef RvitemHPP
#define RvitemHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVWordPaint.hpp>	// Pascal unit
#include <RVZip.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvitem
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS ERichViewError;
#pragma pack(push,4)
class PASCALIMPLEMENTATION ERichViewError : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ERichViewError(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ERichViewError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ERichViewError(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ERichViewError(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ERichViewError(NativeUInt Ident, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ERichViewError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ERichViewError(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ERichViewError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERichViewError(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERichViewError(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERichViewError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERichViewError(NativeUInt Ident, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ERichViewError(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVItemOption : unsigned char { rvioSameAsPrev, rvioPageBreakBefore, rvioBR, rvioUnicode, rvioClearLeft, rvioClearRight, rvioGroupWithNext };

typedef System::Set<TRVItemOption, TRVItemOption::rvioSameAsPrev, TRVItemOption::rvioGroupWithNext> TRVItemOptions;

enum DECLSPEC_DENUM TRVItemState : unsigned char { rvisSpellChecked };

typedef System::Set<TRVItemState, TRVItemState::rvisSpellChecked, TRVItemState::rvisSpellChecked> TRVItemStates;

enum DECLSPEC_DENUM TRVItemDrawState : unsigned char { rvidsSelected, rvidsCurrent, rvidsHover, rvidsPrinting, rvidsPreview, rvidsPreviewCorrection, rvidsControlFocused, rvidsControlDisabled, rvidsCanUseCustomPPI, rvidsRTL, rvidsShowSpecialCharacters, rvidsDrawInplaceOnMainCanvas, rvidsUseWordPainters, rvidsPrintSimulation };

typedef System::Set<TRVItemDrawState, TRVItemDrawState::rvidsSelected, TRVItemDrawState::rvidsPrintSimulation> TRVItemDrawStates;

enum DECLSPEC_DENUM TRVFReadMode : unsigned char { rmText, rmBeforeBinary, rmBinary, rmBeforeUnicode, rmUnicode, rmAfterUnicode };

enum DECLSPEC_DENUM TRVFReadState : unsigned char { rstHeader, rstData, rstSkip };

enum DECLSPEC_DENUM TRVItemBoolProperty : unsigned char { rvbpIgnorePara, rvbpFullWidth, rvbpValid, rvbpRequiresRVFLines, rvbpDrawingChangesFont, rvbpCanSaveUnicode, rvbpAlwaysInText, rvbpImmediateControlOwner, rvbpResizable, rvbpResizeHandlesOutside, rvbpHasSubRVData, rvbpClickSelect, rvbpNoHTML_P, rvbpSwitchToAssStyleNo, rvbpFloating, rvbpKeepWithPrior, rvbpDocXInRun, rvbpDocXOnlyBeforeAfter, rvbpPlaceholder };

enum DECLSPEC_DENUM TRVItemBoolPropertyEx : unsigned char { rvbpDisplayActiveState, rvbpPrintToBMP, rvbpPrintToBMPIfNoMetafiles, rvbpJump, rvbpAllowsFocus, rvbpHotColdJump, rvbpXORFocus, rvbpActualPrintSize };

enum DECLSPEC_DENUM TRVEnterDirection : unsigned char { rvedLeft, rvedRight, rvedTop, rvedBottom };

enum DECLSPEC_DENUM TRVExtraItemProperty : unsigned char { rvepUnknown, rvepVShift, rvepVShiftAbs, rvepImageWidth, rvepImageHeight, rvepTransparent, rvepTransparentMode, rvepTransparentColor, rvepMinHeightOnPage, rvepSpacing, rvepResizable, rvepDeleteProtect, rvepNoHTMLImageSize, rvepAnimationInterval, rvepVisible, rvepHidden, rvepShared, rvepBorderWidth, rvepColor, rvepBorderColor, rvepOuterHSpacing, rvepOuterVSpacing, rvepVAlign };

enum DECLSPEC_DENUM TRVExtraItemStrProperty : unsigned char { rvespUnknown, rvespHint, rvespAlt, rvespImageFileName, rvespTag };

enum DECLSPEC_DENUM TRVEStyleConversionType : unsigned char { rvscParaStyle, rvscTextStyle, rvscParaStyleConversion, rvscTextStyleConversion, rvscParaStyleTemplateReset, rvscParaStyleTemplateNoReset, rvscTextStyleTemplateReset, rvscTextStyleTemplateNoReset };

enum DECLSPEC_DENUM TRVReformatType : unsigned char { rvrfNone, rvrfNormal, rvrfFull, rvrfSRVRepaint, rvrfSRVReformat };

enum DECLSPEC_DENUM TRVRTFDocType : unsigned char { rvrtfdtMain, rvrtfdtHeaderFooter, rvrtfdtNote, rvrtfdtSidenote, rvrtfdtTextBox };

struct DECLSPEC_DRECORD TRVRTFSavingData
{
public:
	System::UnicodeString Path;
	Rvfuncs::TCustomRVRTFTables* RTFTables;
	Rvclasses::TRVIntegerList* StyleToFont;
	Rvclasses::TRVIntegerList* ListOverrideOffsetsList1;
	Rvclasses::TRVIntegerList* ListOverrideOffsetsList2;
};


class DELPHICLASS TRVMultiDrawItemPart;
class DELPHICLASS TCustomRVItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMultiDrawItemPart : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Dlines::TRVDrawLineInfo* FOwner;
	
public:
	int Height;
	DYNAMIC int __fastcall GetSoftPageBreakInfo(void);
	DYNAMIC bool __fastcall IsComplexSoftPageBreak(Dlines::TRVDrawLineInfo* DrawItem);
	DYNAMIC void __fastcall AssignSoftPageBreaksToItem(Dlines::TRVDrawLineInfo* DrawItem, TCustomRVItemInfo* Item);
	virtual int __fastcall GetImageHeight(void);
	__fastcall TRVMultiDrawItemPart(Dlines::TRVDrawLineInfo* AOwner);
	__property Dlines::TRVDrawLineInfo* Owner = {read=FOwner};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVMultiDrawItemPart(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVDeleteUnusedStylesData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDeleteUnusedStylesData : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	bool FInitialized;
	bool FConvertedToShifts;
	Rvclasses::TRVIntegerList* FUsedTextStyles;
	Rvclasses::TRVIntegerList* FUsedParaStyles;
	Rvclasses::TRVIntegerList* FUsedListStyles;
	bool FTextStyles;
	bool FParaStyles;
	bool FListStyles;
	
public:
	__fastcall TRVDeleteUnusedStylesData(bool ATextStyles, bool AParaStyles, bool AListStyles);
	__fastcall virtual ~TRVDeleteUnusedStylesData(void);
	void __fastcall Init(Rvstyle::TRVStyle* RVStyle);
	void __fastcall ConvertFlagsToShifts(Rvstyle::TRVStyle* RVStyle);
	__property Rvclasses::TRVIntegerList* UsedTextStyles = {read=FUsedTextStyles};
	__property Rvclasses::TRVIntegerList* UsedParaStyles = {read=FUsedParaStyles};
	__property Rvclasses::TRVIntegerList* UsedListStyles = {read=FUsedListStyles};
	__property bool TextStyles = {read=FTextStyles, nodefault};
	__property bool ParaStyles = {read=FParaStyles, nodefault};
	__property bool ListStyles = {read=FListStyles, nodefault};
	__property bool ConvertedToShifts = {read=FConvertedToShifts, nodefault};
};

#pragma pack(pop)

class DELPHICLASS TRVCPInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVCPInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString Name;
	TRVCPInfo* Next;
	TRVCPInfo* Prev;
	bool RaiseEvent;
	bool Persistent;
	TCustomRVItemInfo* ItemInfo;
	int ItemNo;
	Rvstyle::TRVTag Tag;
	void __fastcall Assign(TRVCPInfo* Source, bool TagsArePChars);
	TRVCPInfo* __fastcall CreateCopy(bool TagsArePChars);
public:
	/* TObject.Create */ inline __fastcall TRVCPInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVCPInfo(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVSubRVDataPos : unsigned char { rvdFirst, rvdLast, rvdChosenUp, rvdChosenDown, rvdNext, rvdPrev };

class DELPHICLASS TRVStoreSubRVData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStoreSubRVData : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	DYNAMIC TRVStoreSubRVData* __fastcall Duplicate(void);
	DYNAMIC int __fastcall Compare(TRVStoreSubRVData* StoreSub);
public:
	/* TObject.Create */ inline __fastcall TRVStoreSubRVData(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVStoreSubRVData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVPaintItemPart;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVPaintItemPart : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	/* TObject.Create */ inline __fastcall TRVPaintItemPart(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVPaintItemPart(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVItemInfo : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	bool __fastcall GetSameAsPrev(void);
	void __fastcall SetSameAsPrev(const bool Value);
	bool __fastcall GetBR(void);
	void __fastcall SetBR(bool Value);
	bool __fastcall GetPageBreakBefore(void);
	void __fastcall SetPageBreakBefore(const bool Value);
	bool __fastcall GetClearLeft(void);
	bool __fastcall GetClearRight(void);
	void __fastcall SetClearLeft(const bool Value);
	void __fastcall SetClearRight(const bool Value);
	
protected:
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	void __fastcall SetExtraPropertyFromRVFStr(const Rvtypes::TRVRawByteString Str, bool UTF8Strings);
	virtual int __fastcall GetAssociatedTextStyleNo(void);
	virtual void __fastcall SetAssociatedTextStyleNo(int Value);
	
public:
	Rvtypes::TRVRawByteString ItemText;
	int StyleNo;
	int ParaNo;
	TRVItemOptions ItemOptions;
	TRVItemStates ItemState;
	Rvwordpaint::TRVWordPainterList* WordPaintList;
	TRVCPInfo* Checkpoint;
	int JumpID;
	Rvstyle::TRVTag Tag;
	int DrawItemNo;
	System::UnicodeString Hint;
	__fastcall virtual TCustomRVItemInfo(System::Classes::TPersistent* RVData);
	__fastcall virtual ~TCustomRVItemInfo(void);
	void __fastcall ClearLiveSpellingResult(void);
	void __fastcall ClearWordPainters(int Index);
	bool __fastcall AdjustWordPaintersOnInsert(int Index, const System::UnicodeString Text, System::WideChar ch, System::Classes::TPersistent* RVData);
	bool __fastcall AdjustWordPaintersOnDelete(int Index, int Count);
	bool __fastcall GetMisspelling(int Offs, int &MisOffs, int &MisLength);
	void __fastcall AddMisspelling(int StartOffs, int Length);
	bool __fastcall IsMisspelled(int Index);
	bool __fastcall ValidateMisspelledWord(const System::UnicodeString AItemText, const System::UnicodeString AWord);
	HIDESBASEDYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	void __fastcall AssignParaProperties(TCustomRVItemInfo* Source);
	DYNAMIC void __fastcall TransferProperties(TCustomRVItemInfo* Source, System::Classes::TPersistent* RVData);
	DYNAMIC System::Classes::TPersistent* __fastcall GetSubRVDataAt(int X, int Y, bool ReturnClosest);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	DYNAMIC bool __fastcall OwnsControl(Vcl::Controls::TControl* AControl);
	DYNAMIC bool __fastcall OwnsInplaceEditor(Vcl::Controls::TControl* AEditor);
	bool __fastcall CanBeBorderStart(void);
	bool __fastcall ParaStart(bool CountBR);
	__property bool SameAsPrev = {read=GetSameAsPrev, write=SetSameAsPrev, nodefault};
	virtual Vcl::Graphics::TGraphic* __fastcall AsImage(void);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC void __fastcall UpdatePaletteInfo(Rvscroll::TRVPaletteAction PaletteAction, bool ForceRecreateCopy, HPALETTE Palette, Winapi::Windows::PLogPalette LogPalette);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, TRVFReadMode &ReadMode, TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall SaveRVFSelection(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo);
	DYNAMIC void __fastcall SaveTextSelection(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int LineWidth, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, TRVRTFSavingData &SavingData, TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall FillRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList, System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLBeforeRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLAfterRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	virtual void __fastcall PaintFullWidth(int Left, int Right, int Top, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Rvstyle::TRVStyle* Style, const System::Types::TRect &ClipRect, Dlines::TRVDrawLineInfo* dli, int ExtraX, int ExtraY, TRVPaintItemPart* PaintPart);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual bool __fastcall PrintToBitmap(Vcl::Graphics::TBitmap* Bkgnd, bool Preview, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	virtual int __fastcall GetImageWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageWidthEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual int __fastcall GetImageHeightEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual Rvstyle::TRVStyleLength __fastcall GetBorderWidth(void);
	virtual Rvstyle::TRVStyleLength __fastcall GetBorderHeight(void);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall Hiding(void);
	DYNAMIC void __fastcall Unhiding(System::TObject* RVData);
	DYNAMIC void __fastcall FinalizeUndoGroup(void);
	DYNAMIC bool __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	DYNAMIC bool __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	DYNAMIC bool __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall BeforeLoading(Rvstyle::TRVLoadFormat FileFormat);
	DYNAMIC void __fastcall AfterLoading(Rvstyle::TRVLoadFormat FileFormat);
	DYNAMIC void __fastcall DeselectPartial(void);
	DYNAMIC bool __fastcall PartiallySelected(void);
	DYNAMIC bool __fastcall CanDeletePartiallySelected(void);
	DYNAMIC void __fastcall DeletePartiallySelected(void);
	DYNAMIC void __fastcall ApplyStyleConversionToSubRVDatas(int UserData, bool SelectedOnly, TRVEStyleConversionType ConvType);
	DYNAMIC void __fastcall ApplyStyleConversion(System::Classes::TPersistent* RVData, int ItemNo, int UserData, TRVEStyleConversionType ConvType, bool Recursive);
	virtual Dlines::TRVDrawLineInfo* __fastcall CreatePrintingDrawItem(System::TObject* RVData, System::TObject* ParaStyle, const Rvstyle::TRVScreenAndDevice &sad);
	DYNAMIC void __fastcall StartExport(void);
	DYNAMIC void __fastcall EndExport(void);
	virtual void __fastcall Inserting(System::TObject* RVData, Rvtypes::TRVRawByteString &Text, bool Safe);
	virtual void __fastcall Inserted(System::TObject* RVData, int ItemNo);
	DYNAMIC void __fastcall BeforeUndoChangeProperty(void);
	DYNAMIC void __fastcall AfterUndoChangeProperty(void);
	DYNAMIC bool __fastcall EnterItem(TRVEnterDirection From, int Coord);
	DYNAMIC System::Uitypes::TCursor __fastcall GetHypertextCursor(Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall BuildJumps(int &StartJumpNo);
	DYNAMIC void __fastcall Focusing(void);
	DYNAMIC bool __fastcall MoveFocus(bool GoForward, System::Classes::TPersistent* &TopLevelRVData, int &TopLevelItemNo);
	DYNAMIC void __fastcall ClearFocus(void);
	DYNAMIC void __fastcall Execute(System::Classes::TPersistent* RVData);
	DYNAMIC bool __fastcall AdjustFocusToControl(Vcl::Controls::TControl* Control, System::Classes::TPersistent* &TopLevelRVData, int &TopLevelItemNo);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	DYNAMIC void __fastcall MarkStylesInUse(TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(TRVDeleteUnusedStylesData* Data);
	DYNAMIC System::Classes::TPersistent* __fastcall GetSubRVData(TRVStoreSubRVData* &StoreState, TRVSubRVDataPos Position);
	DYNAMIC void __fastcall ChooseSubRVData(TRVStoreSubRVData* StoreState);
	DYNAMIC void __fastcall CleanUpChosen(void);
	DYNAMIC void __fastcall ResetSubCoords(void);
	DYNAMIC bool __fastcall SetExtraIntProperty(TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(TRVExtraItemProperty Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraStrProperty(TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrProperty(TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	DYNAMIC bool __fastcall SetExtraStrPropertyEx(int Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrPropertyEx(int Prop, System::UnicodeString &Value);
	DYNAMIC TRVReformatType __fastcall GetExtraIntPropertyReformat(int Prop);
	DYNAMIC TRVReformatType __fastcall GetExtraStrPropertyReformat(int Prop);
	DYNAMIC bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	DYNAMIC int __fastcall GetSoftPageBreakDY(int Data);
	int __fastcall GetActualTextStyleNo(System::Classes::TPersistent* Obj);
	virtual void __fastcall DrawBackgroundForPrinting(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect, const System::Types::TRect &FullRect, Rvstyle::TRVColorMode ColorMode, int ItemBackgroundLayer, Rvgrin::TRVGraphicInterface* GraphicIterface, bool AllowBitmaps);
	DYNAMIC void __fastcall ClearSoftPageBreaks(void);
	DYNAMIC void __fastcall UpdateAnimator(System::TObject* RVData);
	DYNAMIC void __fastcall RemoveLinkToAnimator(void);
	DYNAMIC void __fastcall KeyDown(System::Word Key, bool Shift);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	DYNAMIC void __fastcall ChangeParagraphStyles(Rvclasses::TRVIntegerList* ParaMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall ChangeTextStyles(Rvclasses::TRVIntegerList* TextMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall MouseLeave(void);
	DYNAMIC System::Uitypes::TColor __fastcall GetSubDocColor(void);
	DYNAMIC void __fastcall SaveFormattingToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFormattingFromStream(System::Classes::TStream* Stream);
	__property bool BR = {read=GetBR, write=SetBR, nodefault};
	__property bool PageBreakBefore = {read=GetPageBreakBefore, write=SetPageBreakBefore, nodefault};
	__property bool ClearLeft = {read=GetClearLeft, write=SetClearLeft, nodefault};
	__property bool ClearRight = {read=GetClearRight, write=SetClearRight, nodefault};
	__property int AssociatedTextStyleNo = {read=GetAssociatedTextStyleNo, write=SetAssociatedTextStyleNo, nodefault};
};

#pragma pack(pop)

typedef System::TMetaClass* TCustomRVItemInfoClass;

class DELPHICLASS TRVItemList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVItemList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	Rvtypes::TRVRawByteString operator[](int Index) { return Items[Index]; }
	
private:
	Rvtypes::TRVRawByteString __fastcall GetItem(int Index);
	TCustomRVItemInfo* __fastcall GetObject(int Index);
	void __fastcall SetItem(int Index, const Rvtypes::TRVRawByteString Value);
	void __fastcall SetObject(int Index, TCustomRVItemInfo* const Value);
	
public:
	void __fastcall AddObject(const Rvtypes::TRVRawByteString ItemText, TCustomRVItemInfo* Item);
	void __fastcall InsertObject(int Index, const Rvtypes::TRVRawByteString ItemText, TCustomRVItemInfo* Item);
	int __fastcall IndexOfObject(TCustomRVItemInfo* Item);
	__property Rvtypes::TRVRawByteString Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
	__property TCustomRVItemInfo* Objects[int Index] = {read=GetObject, write=SetObject};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVItemList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVItemList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVTextItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTextItemInfo : public TCustomRVItemInfo
{
	typedef TCustomRVItemInfo inherited;
	
public:
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall Execute(System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall MarkStylesInUse(TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(TRVDeleteUnusedStylesData* Data);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
public:
	/* TCustomRVItemInfo.Create */ inline __fastcall virtual TRVTextItemInfo(System::Classes::TPersistent* RVData) : TCustomRVItemInfo(RVData) { }
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVTextItemInfo(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TRVTextItemInfoClass;

class DELPHICLASS TRVNonTextItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVNonTextItemInfo : public TCustomRVItemInfo
{
	typedef TCustomRVItemInfo inherited;
	
protected:
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	
public:
	bool DeleteProtect;
	bool Hidden;
	virtual int __fastcall GetLeftOverhang(void);
	virtual void __fastcall AdjustInserted(int x, int y, bool adjusty, System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	DYNAMIC bool __fastcall SetExtraIntProperty(TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(TRVExtraItemProperty Prop, int &Value);
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetHeightEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual int __fastcall GetWidthEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	DYNAMIC void __fastcall FreeThumbnail(void);
	virtual bool __fastcall HasThumbnail(void);
public:
	/* TCustomRVItemInfo.Create */ inline __fastcall virtual TRVNonTextItemInfo(System::Classes::TPersistent* RVData) : TCustomRVItemInfo(RVData) { }
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVNonTextItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFullLineItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFullLineItemInfo : public TRVNonTextItemInfo
{
	typedef TRVNonTextItemInfo inherited;
	
public:
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
public:
	/* TCustomRVItemInfo.Create */ inline __fastcall virtual TRVFullLineItemInfo(System::Classes::TPersistent* RVData) : TRVNonTextItemInfo(RVData) { }
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVFullLineItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSimpleRectItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSimpleRectItemInfo : public TRVNonTextItemInfo
{
	typedef TRVNonTextItemInfo inherited;
	
public:
	/* TCustomRVItemInfo.Create */ inline __fastcall virtual TRVSimpleRectItemInfo(System::Classes::TPersistent* RVData) : TRVNonTextItemInfo(RVData) { }
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVSimpleRectItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRectItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRectItemInfo : public TRVSimpleRectItemInfo
{
	typedef TRVSimpleRectItemInfo inherited;
	
protected:
	int FMinHeightOnPage;
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	Rvtypes::TRVAnsiString __fastcall GetVShiftCSS(Rvstyle::TRVStyle* RVStyle);
	Rvtypes::TRVAnsiString __fastcall GetVAlignCSS(void);
	Rvtypes::TRVAnsiString __fastcall GetCSS(Rvstyle::TRVStyle* RVStyle);
	void __fastcall PaintDef(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	
public:
	Rvstyle::TRVVAlign VAlign;
	int VShift;
	bool VShiftAbs;
	Rvstyle::TRVStyleLength Spacing;
	Rvstyle::TRVStyleLength BorderWidth;
	Rvstyle::TRVStyleLength OuterHSpacing;
	Rvstyle::TRVStyleLength OuterVSpacing;
	System::Uitypes::TColor BackgroundColor;
	System::Uitypes::TColor BorderColor;
	__fastcall virtual TRVRectItemInfo(System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	DYNAMIC bool __fastcall SetExtraIntProperty(TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(TRVExtraItemProperty Prop, int &Value);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	virtual Rvstyle::TRVStyleLength __fastcall GetBorderWidth(void);
	virtual Rvstyle::TRVStyleLength __fastcall GetBorderHeight(void);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVRectItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVTabItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTabItemInfo : public TRVSimpleRectItemInfo
{
	typedef TRVSimpleRectItemInfo inherited;
	
private:
	int FTextStyleNo;
	void __fastcall DrawTab(Vcl::Graphics::TCanvas* Canvas, int x, int y, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData, Rvstyle::TRVTextDrawStates TextDrawState, bool CanUseCustomPPI, bool RTL, bool SpecialChars, bool Printing, Rvstyle::TRVColorMode ColorMode);
	
protected:
	virtual int __fastcall GetAssociatedTextStyleNo(void);
	virtual void __fastcall SetAssociatedTextStyleNo(int Value);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	
public:
	System::UnicodeString Leader;
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	DYNAMIC void __fastcall MarkStylesInUse(TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(TRVDeleteUnusedStylesData* Data);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	DYNAMIC void __fastcall ApplyStyleConversion(System::Classes::TPersistent* RVData, int ItemNo, int UserData, TRVEStyleConversionType ConvType, bool Recursive);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, TRVRTFSavingData &SavingData, TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	__property int TextStyleNo = {read=FTextStyleNo, write=FTextStyleNo, nodefault};
public:
	/* TCustomRVItemInfo.Create */ inline __fastcall virtual TRVTabItemInfo(System::Classes::TPersistent* RVData) : TRVSimpleRectItemInfo(RVData) { }
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVTabItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVControlItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVControlItemInfo : public TRVRectItemInfo
{
	typedef TRVRectItemInfo inherited;
	
protected:
	bool FResizable;
	bool FVisible;
	bool FShared;
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	
public:
	Vcl::Controls::TControl* Control;
	int PercentWidth;
	__fastcall TRVControlItemInfo(System::Classes::TPersistent* RVData, Vcl::Controls::TControl* AControl, Rvstyle::TRVVAlign AVAlign);
	__fastcall virtual TRVControlItemInfo(System::Classes::TPersistent* RVData);
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetHeightEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual int __fastcall GetWidthEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	__fastcall virtual ~TRVControlItemInfo(void);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	virtual void __fastcall AdjustInserted(int x, int y, bool adjusty, System::Classes::TPersistent* RVData);
	virtual bool __fastcall PrintToBitmap(Vcl::Graphics::TBitmap* Bkgnd, bool Preview, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode);
	virtual Dlines::TRVDrawLineInfo* __fastcall CreatePrintingDrawItem(System::TObject* RVData, System::TObject* ParaStyle, const Rvstyle::TRVScreenAndDevice &sad);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	DYNAMIC bool __fastcall OwnsControl(Vcl::Controls::TControl* AControl);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, TRVFReadMode &ReadMode, TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC bool __fastcall SetExtraIntProperty(TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(TRVExtraItemProperty Prop, int &Value);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, TRVRTFSavingData &SavingData, TRVRTFDocType DocType, bool HiddenParent);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageWidth(Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall Hiding(void);
	DYNAMIC void __fastcall Unhiding(System::TObject* RVData);
	virtual void __fastcall Inserting(System::TObject* RVData, Rvtypes::TRVRawByteString &Text, bool Safe);
	DYNAMIC void __fastcall Focusing(void);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	__property int MinHeightOnPage = {read=FMinHeightOnPage, write=FMinHeightOnPage, nodefault};
};

#pragma pack(pop)

class DELPHICLASS TRVGraphicItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVGraphicItemInfo : public TRVRectItemInfo
{
	typedef TRVRectItemInfo inherited;
	
protected:
	System::TObject* FAnimator;
	bool FResizable;
	bool FShared;
	bool FHasThumbnail;
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	
public:
	Vcl::Graphics::TGraphic* Image;
	Vcl::Graphics::TGraphic* ImageCopy;
	Rvstyle::TRVStyleLength ImageWidth;
	Rvstyle::TRVStyleLength ImageHeight;
	int Interval;
	bool NoHTMLImageSize;
	System::UnicodeString Alt;
	System::UnicodeString ImageFileName;
	__fastcall virtual TRVGraphicItemInfo(System::Classes::TPersistent* RVData, Vcl::Graphics::TGraphic* AImage, Rvstyle::TRVVAlign AVAlign);
	__fastcall virtual TRVGraphicItemInfo(System::Classes::TPersistent* RVData);
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetHeightEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual int __fastcall GetWidthEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	DYNAMIC void __fastcall TransferProperties(TCustomRVItemInfo* Source, System::Classes::TPersistent* RVData);
	DYNAMIC bool __fastcall SetExtraIntProperty(TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(TRVExtraItemProperty Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraStrProperty(TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrProperty(TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	DYNAMIC void __fastcall UpdatePaletteInfo(Rvscroll::TRVPaletteAction PaletteAction, bool ForceRecreateCopy, HPALETTE Palette, Winapi::Windows::PLogPalette LogPalette);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	__fastcall virtual ~TRVGraphicItemInfo(void);
	virtual Vcl::Graphics::TGraphic* __fastcall AsImage(void);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC System::UnicodeString __fastcall GetMoreImgAttributes(System::Classes::TPersistent* RVData, int ItemNo, System::UnicodeString Path, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, TRVFReadMode &ReadMode, TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, TRVRTFSavingData &SavingData, TRVRTFDocType DocType, bool HiddenParent);
	virtual bool __fastcall PrintToBitmap(Vcl::Graphics::TBitmap* Bkgnd, bool Preview, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	virtual int __fastcall GetImageHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageHeightEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual int __fastcall GetImageWidthEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	virtual Dlines::TRVDrawLineInfo* __fastcall CreatePrintingDrawItem(System::TObject* RVData, System::TObject* ParaStyle, const Rvstyle::TRVScreenAndDevice &sad);
	DYNAMIC void __fastcall UpdateAnimator(System::TObject* RVData);
	DYNAMIC void __fastcall RemoveLinkToAnimator(void);
	DYNAMIC void __fastcall FreeThumbnail(void);
	virtual bool __fastcall HasThumbnail(void);
	void __fastcall SetImage(Vcl::Graphics::TGraphic* AImage, System::Classes::TPersistent* RVData);
	__property int MinHeightOnPage = {read=FMinHeightOnPage, write=FMinHeightOnPage, nodefault};
	__property bool Shared = {read=FShared, write=FShared, nodefault};
};

#pragma pack(pop)

typedef System::TMetaClass* TRVGraphicItemInfoClass;

class DELPHICLASS TRVHotGraphicItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVHotGraphicItemInfo : public TRVGraphicItemInfo
{
	typedef TRVGraphicItemInfo inherited;
	
public:
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	__fastcall virtual TRVHotGraphicItemInfo(System::Classes::TPersistent* RVData, Vcl::Graphics::TGraphic* AImage, Rvstyle::TRVVAlign AVAlign);
	DYNAMIC void __fastcall Execute(System::Classes::TPersistent* RVData);
public:
	/* TRVGraphicItemInfo.Create */ inline __fastcall virtual TRVHotGraphicItemInfo(System::Classes::TPersistent* RVData) : TRVGraphicItemInfo(RVData) { }
	/* TRVGraphicItemInfo.Destroy */ inline __fastcall virtual ~TRVHotGraphicItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBulletItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBulletItemInfo : public TRVRectItemInfo
{
	typedef TRVRectItemInfo inherited;
	
protected:
	virtual int __fastcall GetImageIndex(bool Hot);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	
public:
	Vcl::Imglist::TCustomImageList* ImageList;
	int ImageIndex;
	bool NoHTMLImageSize;
	System::UnicodeString Alt;
	__fastcall TRVBulletItemInfo(System::Classes::TPersistent* RVData, int AImageIndex, Vcl::Imglist::TCustomImageList* AImageList, Rvstyle::TRVVAlign AVAlign);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetHeightEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	virtual int __fastcall GetWidthEx(Rvstyle::TRVStyle* RVStyle, Rvstyle::PRVScreenAndDevice sad);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual bool __fastcall PrintToBitmap(Vcl::Graphics::TBitmap* Bkgnd, bool Preview, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, TRVFReadMode &ReadMode, TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, TRVRTFSavingData &SavingData, TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC bool __fastcall SetExtraIntProperty(TRVExtraItemProperty Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntProperty(TRVExtraItemProperty Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraStrProperty(TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrProperty(TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	virtual int __fastcall GetImageHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetImageWidth(Rvstyle::TRVStyle* RVStyle);
public:
	/* TRVRectItemInfo.Create */ inline __fastcall virtual TRVBulletItemInfo(System::Classes::TPersistent* RVData) : TRVRectItemInfo(RVData) { }
	
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVBulletItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVHotspotItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVHotspotItemInfo : public TRVBulletItemInfo
{
	typedef TRVBulletItemInfo inherited;
	
protected:
	virtual int __fastcall GetImageIndex(bool Hot);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	
public:
	int HotImageIndex;
	__fastcall TRVHotspotItemInfo(System::Classes::TPersistent* RVData, int AImageIndex, int AHotImageIndex, Vcl::Imglist::TCustomImageList* AImageList, Rvstyle::TRVVAlign AVAlign);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	DYNAMIC void __fastcall Execute(System::Classes::TPersistent* RVData);
public:
	/* TRVRectItemInfo.Create */ inline __fastcall virtual TRVHotspotItemInfo(System::Classes::TPersistent* RVData) : TRVBulletItemInfo(RVData) { }
	
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVHotspotItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBreakItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBreakItemInfo : public TRVFullLineItemInfo
{
	typedef TRVFullLineItemInfo inherited;
	
protected:
	DYNAMIC Rvtypes::TRVRawByteString __fastcall SaveRVFHeaderTail(System::Classes::TPersistent* RVData);
	
public:
	Rvstyle::TRVStyleLength LineWidth;
	Rvstyle::TRVBreakStyle Style;
	System::Uitypes::TColor Color;
	__fastcall TRVBreakItemInfo(System::Classes::TPersistent* RVData, System::Byte ALineWidth, Rvstyle::TRVBreakStyle AStyle, System::Uitypes::TColor AColor);
	DYNAMIC void __fastcall Assign(TCustomRVItemInfo* Source);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	virtual void __fastcall PaintFullWidth(int Left, int Right, int Top, Vcl::Graphics::TCanvas* Canvas, TRVItemDrawStates State, Rvstyle::TRVStyle* Style, const System::Types::TRect &ClipRect, Dlines::TRVDrawLineInfo* dli, int ExtraX, int ExtraY, TRVPaintItemPart* PaintPart);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC bool __fastcall ReadRVFHeaderTail(char * &P, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, TRVRTFSavingData &SavingData, TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall FillRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList, System::Classes::TPersistent* RVData);
	virtual bool __fastcall GetBoolValue(TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
public:
	/* TCustomRVItemInfo.Create */ inline __fastcall virtual TRVBreakItemInfo(System::Classes::TPersistent* RVData) : TRVFullLineItemInfo(RVData) { }
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVBreakItemInfo(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVTextItemInfoClass RichViewTextItemClass;
extern DELPHI_PACKAGE int RichViewFloatingPlaceHolderHeight;
static const System::Int8 RVItemOptionsMask = System::Int8(0x3f);
static const System::Int8 RVF_SAVETYPE_TEXT = System::Int8(0x0);
static const System::Int8 RVF_SAVETYPE_ONREQUEST = System::Int8(0x1);
static const System::Int8 RVF_SAVETYPE_BINARY = System::Int8(0x2);
static const System::Int8 RVF_SAVETYPE_HEXUNICODE = System::Int8(0x3);
static const System::Int8 RVF_SAVETYPE_TEXT_IMGONREQUEST = System::Int8(0x4);
static const System::Int8 RVF_SAVETYPE_BINARY_IMGONREQUEST = System::Int8(0x5);
static const System::Byte rveipcImageIndex = System::Byte(0xc8);
static const System::Byte rveipcHotImageIndex = System::Byte(0xc9);
static const System::Byte rveipcBreakWidth = System::Byte(0xc8);
static const System::Byte rveipcBreakStyle = System::Byte(0xc9);
static const System::Byte rveipcBreakColor = System::Byte(0xca);
extern DELPHI_PACKAGE TRVItemOptions __fastcall RVFGetItemOptions(TRVItemOptions ItemOptions, bool ForceSameAsPrev);
extern DELPHI_PACKAGE void __fastcall WriteRVFExtraIntPropertyStr(System::Classes::TStream* Stream, TRVExtraItemProperty Prop, int Value);
extern DELPHI_PACKAGE void __fastcall WriteRVFExtraStrPropertyStr(System::Classes::TStream* Stream, TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
extern DELPHI_PACKAGE void __fastcall WriteRVFExtraCustomPropertyStr(System::Classes::TStream* Stream, const Rvtypes::TRVAnsiString Name, const System::UnicodeString Value);
extern DELPHI_PACKAGE void __fastcall WriteRVFExtraCustomPropertyInt(System::Classes::TStream* Stream, const Rvtypes::TRVAnsiString Name, int Value);
extern DELPHI_PACKAGE void __fastcall RegisterRichViewItemClass(int StyleNo, TCustomRVItemInfoClass ItemClass);
extern DELPHI_PACKAGE TCustomRVItemInfo* __fastcall CreateRichViewItem(int StyleNo, System::Classes::TPersistent* RVData);
extern DELPHI_PACKAGE TCustomRVItemInfo* __fastcall RV_DuplicateItem(TCustomRVItemInfo* Source, System::Classes::TPersistent* RVData, bool DuplicateCheckpoint);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetExtraIMGStr(Rvstyle::TRVSaveOptions SaveOptions, int Width, int Height, bool NoHTMLImageSize);
extern DELPHI_PACKAGE void __fastcall RVSaveImageToRTF(System::Classes::TStream* Stream, Rvstyle::TRVStyle* RVStyle, Vcl::Graphics::TGraphic* Image, Rvstyle::TRVStyleLength ImageWidth, Rvstyle::TRVStyleLength ImageHeight, Rvstyle::TRVRTFOptions Options, System::TObject* Animator);
extern DELPHI_PACKAGE void __fastcall RVSaveImageListImageToRTF(System::Classes::TStream* Stream, Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex, Rvstyle::TRVRTFOptions RTFOptions);
extern DELPHI_PACKAGE void __fastcall RVSaveImageSharedImageInHTML(Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex, Vcl::Graphics::TGraphic* Graphic, System::UnicodeString &Location, System::Classes::TPersistent* RVData, int ItemNo, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, Rvclasses::TRVList* Bullets);
extern DELPHI_PACKAGE void __fastcall RVDrawEdge(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &r, System::Uitypes::TColor TopLeftColor, System::Uitypes::TColor BottomRightColor, int LineWidth, Rvgrin::TRVGraphicInterface* GraphicInterface);
}	/* namespace Rvitem */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVITEM)
using namespace Rvitem;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvitemHPP
