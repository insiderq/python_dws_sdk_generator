﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVRTFErr.pas' rev: 27.00 (Windows)

#ifndef RvrtferrHPP
#define RvrtferrHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvrtferr
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVRTFErrorCode : unsigned char { rtf_ec_OK, rtf_ec_StackUnderflow, rtf_ec_StackOverflow, rtf_ec_UnmatchedBrace, rtf_ec_InvalidHex, rtf_ec_BadTable, rtf_ec_Assertion, rtf_ec_EndOfFile, rtf_ec_FileOpenError, rtf_ec_Exception, rtf_ec_InvalidPicture, rtf_ec_Aborted };

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvrtferr */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVRTFERR)
using namespace Rvrtferr;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvrtferrHPP
