{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       Classes for DocX export                         }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVDocX;

interface
{$I RV_Defs.inc}
{$IFNDEF RVDONOTUSEDOCX}
uses Classes, Graphics, RVClasses,
     {$IFDEF RICHVIEWDEF4}
     ImgList,
     {$ENDIF}
     RVZip, RVTypes;

type
  TRVDocXPart = (rvdocxpMain, rvdocxpNumbering, rvdocxpFootnotes, rvdocxpEndnotes,
    rvdocxpHeader, rvdocxpFooter,
    rvdocxpHeader1, rvdocxpFooter1,
    rvdocxpHeaderEven, rvdocxpFooterEven);
  TRVDocXParts = set of TRVDocXPart;

  TRVDocXPartsList = class (TRVIntegerList)
  private
    function GetItems(Index: Integer): TRVDocXParts;
    procedure SetItems(Index: Integer; const Value: TRVDocXParts);
  public
    procedure IncludePart(Index: Integer; Part: TRVDocXPart);
    property Items[Index: Integer]: TRVDocXParts read GetItems write SetItems; default;
  end;

  TRVDocXImageListPicType = (rvdxilptBitmap, rvdxilptIcon, rvdxilptPng);

  TRVDocXImageListItem = class
  public
    ImageList: TCustomImageList;
    PicsType: TRVDocXImageListPicType;
    SavedIndices: TRVDocXPartsList;
    constructor Create(AImageList: TCustomImageList);
    destructor Destroy; override;
  end;

  TRVDocXRelationCatalog = class
  private
    FHyperlinks, FImages: TStringList;
    function GetHyperlinks: TStringList;
    function GetImages: TStringList;
  public
    property Hyperlinks: TStringList read GetHyperlinks;
    property Images: TStringList read GetImages;
    function IsEmpty: Boolean;
    destructor Destroy; override;
  end;

  TRVDocXSavingData = class
  private
    function GetCatalogs(Index: TRVDocXPart): TRVDocXRelationCatalog;
    function GetCurCatalog: TRVDocXRelationCatalog;
    procedure GetImageListImageInfo(ImageList: TCustomImageList; ImageIndex: Integer;
      PicType: TRVDocXImageListPicType;
      var ImageListIndex: Integer; var IsNew: Boolean);
  protected
    FZip: TRVZipPacker;
    FCatalogs: array [TRVDocXPart] of TRVDocXRelationCatalog;
    FImageLists: TRVList; // list of TRVDocXImageListItem
  public
    CurPart: TRVDocXPart;
    HasNumPics, HasBitmaps, HasIcons, HasPng, CurInTextBox: Boolean;
    BookmarkIndex, ImageIndex, TotalImageIndex, BreakIndex, ShapeIndex,
    NoteBookmarkIndex: Integer;
    ListOverrideOffsetsList1, ListOverrideOffsetsList2: TRVIntegerList;
    {$IFNDEF RVDONOTUSESEQ}
    FootnoteIndex, EndnoteIndex: Integer;
    FootnotesStream, EndnotesStream: TRVMemoryStream;
    {$ENDIF}
    SectProps: TRVAnsiString;
    CallProgress: Boolean;
    procedure AddGraphic(Gr: TGraphic; FileName: TRVAnsiString);
    procedure AddImageListImage(ImageList: TCustomImageList; ImageIndex: Integer;
      var FileName, RelId: TRVAnsiString);
    function HasImageListImagesFor(Part: TRVDocXPart): Boolean;
    constructor Create(Zip: TRVZipPacker);
    destructor Destroy; override;
    property Catalogs[Index:TRVDocXPart]: TRVDocXRelationCatalog read GetCatalogs;
    property CurCatalog: TRVDocXRelationCatalog read GetCurCatalog;
    property ImageLists: TRVList read FImageLists;
  end;
{$ENDIF}

implementation

{$IFNDEF RVDONOTUSEDOCX}
uses RVFuncs, RVItem, RVStr, RVGrHandler;
{================================ TRVDocXPartsList ============================}
function TRVDocXPartsList.GetItems(Index: Integer): TRVDocXParts;
begin
  Result := TRVDocXParts(Word(Get(Index)));
end;
{------------------------------------------------------------------------------}
procedure TRVDocXPartsList.SetItems(Index: Integer; const Value: TRVDocXParts);
begin
  Put(Index, Word(Value));
end;
{------------------------------------------------------------------------------}
procedure TRVDocXPartsList.IncludePart(Index: Integer; Part: TRVDocXPart);
begin
  Items[Index] := Items[Index] + [Part];
end;
{================================ TRVDocXImageListItem ========================}
constructor TRVDocXImageListItem.Create(AImageList: TCustomImageList);
begin
  inherited Create;
  ImageList := AImageList;
  SavedIndices := TRVDocXPartsList.CreateEx(ImageList.Count, 0);
end;
{------------------------------------------------------------------------------}
destructor TRVDocXImageListItem.Destroy;
begin
  SavedIndices.Free;
  inherited;
end;
{============================= TRVDocXRelationCatalog =========================}
destructor TRVDocXRelationCatalog.Destroy;
begin
  FHyperlinks.Free;
  FImages.Free;
  inherited;
end;
{------------------------------------------------------------------------------}
function TRVDocXRelationCatalog.GetHyperlinks: TStringList;
begin
  if FHyperlinks=nil then
    FHyperlinks := TStringList.Create;
  Result := FHyperlinks;
end;
{------------------------------------------------------------------------------}
function TRVDocXRelationCatalog.GetImages: TStringList;
begin
  if FImages=nil then
    FImages := TStringList.Create;
  Result := FImages;
end;
{------------------------------------------------------------------------------}
function TRVDocXRelationCatalog.IsEmpty: Boolean;
begin
  Result := (FImages=nil) and (FHyperlinks=nil);
end;
{================================= TRVDocXSavingData ==========================}
constructor TRVDocXSavingData.Create(Zip: TRVZipPacker);
begin
  inherited Create;
  FZip := Zip;
  ListOverrideOffsetsList1 := TRVIntegerList.Create;
  ListOverrideOffsetsList2 := TRVIntegerList.Create;
end;
{------------------------------------------------------------------------------}
destructor TRVDocXSavingData.Destroy;
var i: TRVDocXPart;
begin
  for i := Low(TRVDocXPart) to High(TRVDocXPart) do
    FCatalogs[i].Free;
  ListOverrideOffsetsList1.Free;
  ListOverrideOffsetsList2.Free;
  FImageLists.Free;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TRVDocXSavingData.GetImageListImageInfo(ImageList: TCustomImageList;
  ImageIndex: Integer; PicType: TRVDocXImageListPicType;
  var ImageListIndex: Integer; var IsNew: Boolean);
var i: Integer;
    Item: TRVDocXImageListItem;
begin
  ImageListIndex := -1;
  if FImageLists=nil then
    FImageLists := TRVList.Create;
  for i := 0 to FImageLists.Count-1 do begin
    Item := TRVDocXImageListItem(FImageLists.Items[i]);
    if (Item.ImageList=ImageList) and (Item.PicsType=PicType) then begin
      ImageListIndex := i;
      IsNew := Item.SavedIndices[ImageIndex]=[];
      Item.SavedIndices.IncludePart(ImageIndex, CurPart);
      break;
    end;
  end;
  if ImageListIndex<0 then begin
    Item := TRVDocXImageListItem.Create(ImageList);
    Item.PicsType := PicType;
    Item.SavedIndices.IncludePart(ImageIndex, CurPart);
    FImageLists.Add(Item);
    ImageListIndex := FImageLists.Count-1;
    IsNew := True;
  end;
end;
{------------------------------------------------------------------------------}
function TRVDocXSavingData.GetCatalogs(
  Index: TRVDocXPart): TRVDocXRelationCatalog;
begin
  if FCatalogs[Index]=nil then
    FCatalogs[Index] := TRVDocXRelationCatalog.Create;
  Result := FCatalogs[Index];
end;
{------------------------------------------------------------------------------}
function TRVDocXSavingData.GetCurCatalog: TRVDocXRelationCatalog;
begin
  Result := Catalogs[CurPart];
end;
{------------------------------------------------------------------------------}
procedure TRVDocXSavingData.AddGraphic(Gr: TGraphic; FileName: TRVAnsiString);
var Stream: TRVMemoryStream;
begin
  Stream := TRVMemoryStream.Create;
  try
    Gr.SaveToStream(Stream);
    if not FZip.Add( 'word/media/'+FileName, PRVAnsiChar(Stream.Memory), Stream.Size) then
      raise ERichViewError.Create(errRVDocXImgSave);
  finally
    Stream.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVDocXSavingData.AddImageListImage(ImageList: TCustomImageList;
  ImageIndex: Integer; var FileName, RelId: TRVAnsiString);
var Icon: TIcon;
    Bitmap: TBitmap;
    ImageListIndex: Integer;
    IsNew: Boolean;
    Gr: TGraphic;
    PicType: TRVDocXImageListPicType;
begin
  RelId := '';
  FileName := '';
  if (ImageList<>nil) and (ImageIndex<0) or (ImageIndex>=ImageList.Count) then
    exit;
  Gr := nil;
  try
    if RVGraphicHandler.CanConvertIconToPng then begin
      Icon := TIcon.Create;
      ImageList.GetIcon(ImageIndex, Icon);
      Gr := RVGraphicHandler.IconToPng(Icon);
      end
    else begin
      Icon := nil;
      Gr := nil;
    end;
    if Gr<>nil then begin
      Icon.Free;
      HasPng := True;
      PicType := rvdxilptPng;
      end
    else if (CurPart<>rvdocxpNumbering) and (Icon<>nil) then begin
      Gr := Icon;
      HasIcons := True;
      PicType := rvdxilptIcon;
      end
    else begin
      Icon.Free;
      Bitmap := TBitmap.Create;
      Bitmap.Width := ImageList.Width;
      Bitmap.Height := ImageList.Height;
      ImageList.Draw(Bitmap.Canvas, 0, 0, ImageIndex);
      if RVGraphicHandler.IsPngClassAssigned then begin
        try
          Gr := RVGraphicHandler.CreateGraphicByType(rvgtPNG);
          Gr.Assign(Bitmap);
        finally
          Bitmap.Free;
        end;
        HasPng := True;
        PicType := rvdxilptPng;
        end
      else begin
        Gr := Bitmap;
        HasBitmaps := True;
        PicType := rvdxilptBitmap;
      end;
    end;
    GetImageListImageInfo(ImageList, ImageIndex, PicType, ImageListIndex, IsNew);
    FileName := 'bullet'+RVIntToStr(ImageListIndex)+'_'+RVIntToStr(ImageIndex)+'.'+
      TRVAnsiString(RVGraphicHandler.GetGraphicExt(Gr));
    if IsNew then
      AddGraphic(Gr, FileName);
  finally
    Gr.Free;
  end;
  RelId := 'prId'+RVIntToStr(ImageListIndex)+'_'+RVIntToStr(ImageIndex);
end;
{------------------------------------------------------------------------------}
function TRVDocXSavingData.HasImageListImagesFor(Part: TRVDocXPart): Boolean;
var i, j: Integer;
    Item: TRVDocXImageListItem;
begin
  Result := False;
  if FImageLists=nil then
    exit;
  for i := 0 to ImageLists.Count-1 do begin
    Item := TRVDocXImageListItem(FImageLists.Items[i]);
    for j := 0 to Item.SavedIndices.Count-1 do
      if Part in Item.SavedIndices[j] then begin
        Result := True;
        exit;
      end;
  end;
end;
{$ENDIF}

end.
