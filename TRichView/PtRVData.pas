{*******************************************************}
{                                                       }
{       RichView                                        }
{       A set of classes representing documents and     }
{       subdocuments in RichView stored in TRVPrint     }
{       and TRVReportHelper.                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit PtRVData;

interface
{$I RV_Defs.inc}

uses SysUtils, Classes, Windows, Graphics, Printers, StdCtrls,
     DLines, RVFuncs, RVItem, RVBack,
     CRVData, CRVFData, RVRVData, RVTypes,
     {$IFNDEF RVDONOTUSELISTS}
     RVMarker,
     {$ENDIF}
     {$IFNDEF RVDONOTUSESEQ}
     RVNote, RVSideNote, RVFloatingPos, RVFloatingBox,
     {$ENDIF}
     RVStyle, RVScroll, RichView, RVUni, RVClasses, RVGrIn, RVFontCache;
type
  { ---------------------------------------------------------------------------}

  {$IFNDEF RVDONOTUSESEQ}
  TRVFootnotePtblRVData = class;
  TRVSidenotePtblRVData = class;

  { List of TRVFootnotePtblRVData }
  TRVFootnoteRefList = class (TList)
    private
      function Get(Index: Integer): TRVFootnotePtblRVData;
      procedure Put(Index: Integer; const Value: TRVFootnotePtblRVData);
    public
      procedure DeleteByFootnote(Footnote: TRVFootnoteItemInfo);
      procedure Sort;
      function GetFootnoteIndex(Footnote: TRVFootnoteItemInfo): Integer;
      property Items[Index: Integer]: TRVFootnotePtblRVData read Get write Put; default;
  end;

  TRVSidenoteList = class (TRVList)
    private
      function GetItems(Index: Integer): TRVSidenotePtblRVData;
      procedure SetItems(Index: Integer; const Value: TRVSidenotePtblRVData);
    public
      property Items[Index: Integer]: TRVSidenotePtblRVData read GetItems write SetItems; default;
  end;
  {$ELSE}
  TRVFootnoteRefList = TList;
  {$ENDIF}

  { ----------------------------------------------------------------------------
    TRVMultiDrawItemPartsList: list of TRVMultiDrawItemPart.
    Used as TRVMultiDrawItemInfo.PartsList.
  }
  TRVMultiDrawItemPartsList = class (TRVList)
    private
      function Get(Index: Integer): TRVMultiDrawItemPart;
      procedure Put(Index: Integer; const Value: TRVMultiDrawItemPart);
    public
      property Items[Index: Integer]: TRVMultiDrawItemPart read Get write Put; default;
  end;

  { ----------------------------------------------------------------------------
    TRVMultiDrawItemInfo: ancestor class of drawing item containing multiple
    parts on several tables. Inherited classes: TRVTablePrintPart,
    TRVImagePrintPart.
    Note: this class is inherited from TRVComplexLineHeightDrawLineInfo,
    not TRVDrawLineInfo, though it is not necessary used in paragraphs with
    complex line spacing. But it may be there...
  }
  TCustomPrintableRVData = class;

  TRVMultiDrawItemInfo = class (TRVComplexLineHeightDrawLineInfo)
    private
      FPartsList: TRVMultiDrawItemPartsList;
    public
      constructor Create;
      destructor Destroy; override;
      procedure ResetPages(var FootnoteRVDataList: TRVFootnoteRefList;
        var ReleasedHeightAfterFootnotes: Integer;
        FootnotesChangeHeight: Boolean); dynamic;
      procedure UnformatLastPage(var FootnoteRVDataList: TRVFootnoteRefList;
        var ReleasedHeightAfterFootnotes: Integer;
        FootnotesChangeHeight: Boolean); dynamic;
      procedure AddAllFootnotes(var FootnoteRVDataList: TRVFootnoteRefList;
        var Height: Integer; FootnotesChangeHeight: Boolean); dynamic;
      procedure DecHeightByFootnotes(var Height: Integer;
        var ThisPageHasFootnotes: Boolean); dynamic;
      procedure RemoveAllFootnotes(var FootnoteRVDataList: TRVFootnoteRefList;
        var Height: Integer; FootnotesChangeHeight: Boolean); dynamic;
      function GetPartNo(RVData: TCustomRVData; ItemNo, OffsInItem: Integer;
        var PartNo, LocalPageNo, LocalPageCount: Integer; var PtRVData: TCustomPrintableRVData): Boolean; dynamic;
      function GetSubDocCoords(Location: TRVStoreSubRVData; Part: Integer;
        var InnerCoords, OuterCoords: TRect; var Origin: TPoint): Boolean; dynamic;
      function GetLocalPageNoFor(Location: TRVStoreSubRVData; Part: Integer): Integer; dynamic;
      function GetPrintableRVDataFor(Location: TRVStoreSubRVData): TCustomPrintableRVData; dynamic;
      property PartsList: TRVMultiDrawItemPartsList read FPartsList;
  end;
  { ----------------------------------------------------------------------------
    TRVPageInfo: information about one page.
  }
  TRVPageInfo = class (TCollectionItem)
    public
      StartY, StartDrawItemNo, StartPart, StartY2, DocumentHeight: Integer;
      {$IFNDEF RVDONOTUSESEQ}
      FootnoteRVDataList: TRVFootnoteRefList;
      Sidenotes: TRVSidenoteList;
      destructor Destroy; override;
      {$ENDIF}
      procedure Assign(Source: TPersistent); override;
  end;
  { ----------------------------------------------------------------------------
    TRVPageCollection: collection of TRVPageInfo.
    Class of TCustomMainPtblRVData.Pages.
  }
  TRVPageCollection = class (TCollection)
    private
      function GetItem(Index: Integer): TRVPageInfo;
      procedure SetItem(Index: Integer; const Value: TRVPageInfo);
    public
      constructor Create;
      function Add: TRVPageInfo;
      property Items[Index: Integer]: TRVPageInfo
       read GetItem write SetItem; default;
  end;

  TCustomPrintableRVData = class(TRichViewRVData)
    private
      FBackgroundBmp: TBitmap;    // valid only in DrawPage
      FDrawItem: TRVDrawLineInfo; // valid only in DrawPage
      FItemTop: Integer;          // valid only in DrawPage
    protected
      function GetBack: TRVBackground; virtual;
      function GetTopCoord(PageNo, GlobalPageNo: Integer): Integer; virtual;
      function GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer; virtual;
      function GetPrintableAreaTop(PageNo: Integer): Integer; virtual;
      function GetExtraOffsForBackground: Integer; virtual;

      procedure DoPagePrepaint(Canvas: TCanvas; GlobalPageNo: Integer;
        Preview, Correction, InRange: Boolean); virtual;
      procedure DoPagePostpaint(Canvas: TCanvas; GlobalPageNo: Integer;
        Preview, InRange: Boolean); virtual;
      procedure CheckPageNo(PageNo: Integer); virtual;
      function ShareItems: Boolean; override;
      function InitPrinterCanvas: TCanvas; dynamic;
      procedure DonePrinterCanvas(Canvas: TCanvas); dynamic;
      function GetColorMode: TRVColorMode; virtual;
      procedure DoOnHyperlink(RVData: TCustomRVData; ItemNo: Integer;
        const R: TRect); virtual;
      procedure DoOnCheckpoint(RVData: TCustomRVData;
        ItemNo, X, Y: Integer); virtual;
      function PageExists(PageNo: Integer): Boolean; virtual;
      {$IFNDEF RVDONOTUSESEQ}
      function IgnoreFootnotes: Boolean; dynamic;
      procedure CalcFootnoteCoords(References: TRVFootnoteRefList;
        PageNo: Integer);
      {$ENDIF}
      function NoMetafiles: Boolean; dynamic;
    public
      ParentDrawsBack: Boolean;
      function GetSourceRVDataForPrinting: TCustomRVData; dynamic;
      function GetSourceRVDataForPrintingEx: TCustomRVData; dynamic;
      procedure GetDrawItemsRange(PageNo: Integer;
        var StartNo, EndNo, Part: Integer);  virtual;
      function GetPrintableAreaLeft(PageNo: Integer): Integer; virtual;
      procedure DrawPage(PageNo, GlobalPageNo: Integer; Canvas: TCanvas;
        Preview, Correction: Boolean); virtual;
      procedure DrawBackToBitmap(Left,Top: Integer; bmp: TBitmap;
        const sad: TRVScreenAndDevice;
        ItemBackgroundLayer: Integer; // 0 - do not draw; -1 - draw completely; others - item specific
        RelativeToItem, AllowBitmaps, ParentAllowBitmaps: Boolean;
        PageNo: Integer); virtual;
      procedure GetPrintableItemsRange(var MinItemNo, MaxItemNo: Integer); dynamic;
      function GetContentHeightForPage(PageNo: Integer): Integer; virtual;
  end;

  TCustomMultiPagePtblRVData = class(TCustomPrintableRVData)
    protected
      StreamSavePage: Integer;
      procedure DoFormatting(PageCompleted: Integer; Step:TRVPrintingStep); dynamic;
      function GetInitialStartAt: Integer; dynamic;
      function GetFurtherStartAt: Integer; dynamic;
      procedure SetEndAt(Value: Integer); dynamic;
      procedure IncEndAtByStartAt(PageNo: Integer); dynamic;
      function GetTopCoord(PageNo, GlobalPageNo: Integer): Integer; override;
      function GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer; override;
      procedure RVFGetLimits(SaveScope: TRVFSaveScope;
        var StartItem, EndItem, StartOffs, EndOffs: Integer;
        var StartPart, EndPart: TRVMultiDrawItemPart;
        var SelectedItem: TCustomRVItemInfo); override;
      function PageExists(PageNo: Integer): Boolean; override;
      function AllowEmptyFirstPage: Boolean; dynamic;
      {$IFNDEF RVDONOTUSESEQ}
      function GetFootnoteRVData(
        Footnote: TRVFootnoteItemInfo): TRVFootnotePtblRVData;
      procedure DrawSidenotes(Canvas: TCanvas; PageNo:Integer; Preview: Boolean;
        PositionInText: TRVFloatPositionInText);
      function GetSidenoteList(PageNo: Integer): TRVSidenoteList; dynamic;
      {$ENDIF}
    public
      Pages: TRVPageCollection;
      destructor Destroy; override;
      procedure GetDrawItemsRange(PageNo: Integer;
        var StartNo, EndNo, Part: Integer);  override;
      procedure FormatNextPage(var i, StartAt, StartY, Y: Integer;
        var Splitting: Boolean; var MaxHeight: Integer; 
        var FootnoteRVDataList: TRVFootnoteRefList;
        FootnotesChangeHeight: Boolean);
      function CanPlaceFirstPageHere(var Height: Integer;
        ParentIsFirstItemOnPage: Boolean;
        const sad: TRVScreenAndDevice;
        ThisPageHasFootnotes, FootnotesChangeHeight: Boolean): Boolean;
      {$IFNDEF RVDONOTUSERVF}
      function SavePageAsRVF(Stream: TStream; PageNo: Integer; Color: TColor;
        Background: TRVBackground; Layout: TRVLayoutInfo): Boolean;
      function SaveFromPageAsRVF(Stream: TStream; FirstPageNo: Integer; Color: TColor;
        Background: TRVBackground; Layout: TRVLayoutInfo): Boolean;
      {$ENDIF}
      {$IFNDEF RVDONOTUSESEQ}
      function GetNoteSeparatorHeight: Integer;
      {$ENDIF}
      procedure GetFirstItemOnPageEx(PageNo: Integer; var ItemNo, OffsetInItem,
        ExtraData: Integer);
      function IsComplexSoftPageBreak(PageNo: Integer): Boolean;
      procedure AssignComplexSoftPageBreakToItem(PageNo: Integer;
        RVData: TCustomRVFormattedData);
      function GetPageNo(RVData: TCustomRVData;
        ItemNo, OffsetInItem: Integer; PrintSourceRVData: TCustomRVData;
        var PageNo, LocalPageNo, LocalPageCount: Integer; var PtRVData: TCustomPrintableRVData): Boolean;
      {$IFNDEF RVDONOTUSESEQ}
      function GetAnchorObjectCoords(PtRVData: TCustomPrintableRVData;
        ItemNo, CurPageNo, PageNoInPtRVData: Integer;
        HorObject: TRVHorizontalAnchor; VertObject: TRVVerticalAnchor;
        RelativeToCell: Boolean; var RHorz, RVert: TRect; var FoundHere: Boolean): Boolean;
      {$ENDIF}
  end;

  TCustomMainPtblRVData = class(TCustomMultiPagePtblRVData)
    private
      {$IFNDEF RVDONOTUSELISTS}
      FPrevMarkers: TRVMarkerList;
      {$ENDIF}
    protected
      procedure DoFormatting(PageCompleted: Integer; Step:TRVPrintingStep); override;
      function GetBack: TRVBackground; override;
      function GetTopCoord(PageNo, GlobalPageNo: Integer): Integer; override;
      function GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer; override;
      procedure GetSADForFormatting(Canvas: TCanvas; var sad: TRVScreenAndDevice); override;
      function GetPrintableAreaTop(PageNo: Integer): Integer; override;
      procedure CheckPageNo(PageNo: Integer); override;
      procedure Prepare; dynamic;
      function GetColorMode: TRVColorMode; override;
      function GetFirstItemMarker(var ListNo, Level: Integer): Boolean; override;
      function GetInitialStartAt: Integer; override;
      procedure SetEndAt(Value: Integer); override;
      procedure IncEndAtByStartAt(PageNo: Integer); override;
      {$IFNDEF RVDONOTUSESEQ}
      procedure DoPagePrepaint(Canvas: TCanvas; GlobalPageNo: Integer;
        Preview, Correction, InRange: Boolean); override;
      procedure DoPagePostpaint(Canvas: TCanvas; PageNo:Integer;
        Preview, InRange: Boolean); override;
      {$ENDIF}
    public
      PrinterCanvas : TCanvas;
      TmpTMPix, TmpBMPix: Integer;
      PrnSad: TRVScreenAndDevice;
      FTopMarginPix, FBottomMarginPix: Integer;
      Transparent: Boolean;
      TmpMNormal, TmpMFirstPage, TmpMEvenPages, PhysM: TRect;
      ContentHeightNormal, ContentHeightFirstPage, ContentHeightEvenPages: Integer;
      ColorMode: TRVColorMode;
      FIsDestinationReady: Boolean;
      function GetSourceRVDataForPrinting: TCustomRVData; override;
      procedure Clear; override;
      function GetPageWidth: Integer; dynamic;
      function GetPageHeight: Integer; dynamic;
      function GetPrintableAreaLeft(PageNo: Integer): Integer; override;
      procedure InitFormatPages;
      function FormatPages: Integer;
      procedure FinalizeFormatPages;
      function GetCanvas: TCanvas; override;
      procedure DrawPage(PageNo, GlobalPageNo: Integer; Canvas: TCanvas;
        Preview, Correction: Boolean); override;
      function GetColor: TColor; override;
      {$IFNDEF RVDONOTUSELISTS}
      function GetPrevMarkers: TRVMarkerList; override;
      {$ENDIF}
      constructor Create(RichView: TRVScroller); override;
      procedure GetPrintableItemsRange(var MinItemNo, MaxItemNo: Integer); override;
      function IgnorePageBreaks: Boolean; override;
      function GetTmpM(HFType: TRVHFType): PRect;
      function GetTmpMForPage(PageNo: Integer): PRect;
      function GetContentHeightForPage(PageNo: Integer): Integer; override;
  end;

  TRVHeaderFooterRVData = class;

  TPrintableRVData = class(TCustomMainPtblRVData)
    private
      Rgn: TRVHandle;
      RgnValid: Boolean;
    protected
      procedure DoFormatting(PageCompleted: Integer; Step:TRVPrintingStep); override;
      function InitPrinterCanvas: TCanvas; override;
      procedure DonePrinterCanvas(Canvas: TCanvas); override;
      procedure DoPagePrepaint(Canvas: TCanvas; GlobalPageNo: Integer;
        Preview, Correction, InRange: Boolean); override;
      procedure DoPagePostpaint(Canvas: TCanvas; GlobalPageNo: Integer;
        Preview, InRange: Boolean); override;
      procedure Prepare; override;
    public
      TmpLMMir: Integer;
      Header, Footer, FirstPageHeader, FirstPageFooter,
      EvenPagesHeader, EvenPagesFooter: TRVHeaderFooterRVData;
      HeaderY, FooterY: TRVLength;
      function GetHeaderOrFooter(HFType: TRVHFType;
        IsHeader: Boolean): TRVHeaderFooterRVData;
      function GetPrintableAreaLeft(PageNo: Integer): Integer; override;
      constructor Create(RichView: TRVScroller); override;
      destructor Destroy; override;
      function GetSourceRVDataForPrintingEx: TCustomRVData; override;
      function GetPageWidth: Integer; override;
      function GetPageHeight: Integer; override;
      function GetHeaderForPage(PageNo: Integer): TRVHeaderFooterRVData;
      function GetFooterForPage(PageNo: Integer): TRVHeaderFooterRVData;
  end;

  TRectPtblRVData = class(TCustomMultiPagePtblRVData)
    protected
      procedure GetSADForFormatting(Canvas: TCanvas;
        var sad: TRVScreenAndDevice); override;
      function GetPrintableAreaTop(PageNo: Integer): Integer; override;
      function GetExtraOffsForBackground: Integer; override;
      function GetTopCoord(PageNo, GlobalPageNo: Integer): Integer; override;
      function GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer; override;
      function InitPrinterCanvas: TCanvas; override;
      function GetColorMode: TRVColorMode; override;
      procedure DoOnHyperlink(RVData: TCustomRVData; ItemNo: Integer;
        const R: TRect); override;
      procedure DoOnCheckpoint(RVData: TCustomRVData; ItemNo, X, Y: Integer); override;
      {$IFNDEF RVDONOTUSESEQ}
      function IgnoreFootnotes: Boolean; override;
      {$ENDIF}
      function NoMetafiles: Boolean; override;
    public
      FSourceDataForPrinting: TCustomRVData;
      FParentPrintData: TCustomPrintableRVData;
      Left,Top,DX,DY,Width,Height,VOffsInRotatedCell: Integer;
      Transparent: Boolean;
      FColor: TColor;
      function GetSourceRVDataForPrinting: TCustomRVData; override;
      procedure CreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas); override;
      procedure DestroyFontInfoCache(var Cache: TRVFontInfoCache); override;
      function GetFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
        RVData: TCustomRVFormattedData): TRVFontInfoCache; override;
      function GetMaxTextWidth: Integer; override;
      function GetMinTextWidth: Integer; override;
      function GetPrintableAreaLeft(PageNo: Integer): Integer; override;
      function GetParentData: TCustomRVData; override;
      function GetRootData: TCustomRVData; override;
      function GetAbsoluteParentData: TCustomRVData; override;
      function GetAbsoluteRootData: TCustomRVData; override;
      constructor Create(RichView: TRVScroller;
        SourceDataForPrinting: TCustomRVData;
        ParentPrintData: TCustomPrintableRVData); {$IFDEF RICHVIEWDEF4} reintroduce;{$ENDIF}
      procedure DrawBackToBitmap(Left,Top: Integer; bmp: TBitmap;
        const sad: TRVScreenAndDevice; ItemBackgroundLayer: Integer;
        RelativeToItem, AllowBitmaps, ParentAllowBitmaps: Boolean; PageNo: Integer); override;
      function GetWidth: Integer; override;
      function GetHeight: Integer; override;
      function GetLeftMargin: Integer; override;
      function GetRightMargin: Integer; override;
      function GetTopMargin: Integer; override;
      function GetBottomMargin: Integer; override;
      function GetCanvas: TCanvas; override;
      function GetColor: TColor; override;
      function GetRotation: TRVDocRotation; override;
  end;

  TRVHeaderFooterRVData = class (TRectPtblRVData)
    protected
      {$IFNDEF RVDONOTUSESEQ}
      function IgnoreFootnotes: Boolean; override;
      {$ENDIF}
    public
      {$IFNDEF RVDONOTUSESEQ}
      Sidenotes: TRVSidenoteList;
      {$ENDIF}
      FLeftMargin, FRightMargin, FTopMargin, FBottomMargin: Integer;
      constructor Create(RichView: TRVScroller; SourceDataForPrinting: TCustomRVData;
        ParentPrintData: TCustomPrintableRVData);
      {$IFNDEF RVDONOTUSESEQ}        
      function GetSidenoteList(PageNo: Integer): TRVSidenoteList; override;
      destructor Destroy; override;
      {$ENDIF}
      function GetRVStyle: TRVStyle; override;
      procedure CreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas); override;
      function GetFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
        RVData: TCustomRVFormattedData): TRVFontInfoCache; override;
      function GetLeftMargin: Integer; override;
      function GetRightMargin: Integer; override;
      function GetTopMargin: Integer; override;
      function GetBottomMargin: Integer; override;
  end;

  {$IFNDEF RVDONOTUSESEQ}
  TRVEndnotePtblRVData = class(TCustomMultiPagePtblRVData)
    protected
      function GetInitialStartAt: Integer; override;
      function GetFurtherStartAt: Integer; override;
      procedure SetEndAt(Value: Integer); override;
      procedure IncEndAtByStartAt(PageNo: Integer); override;
      procedure GetSADForFormatting(Canvas: TCanvas;
        var sad: TRVScreenAndDevice); override;
      function GetTopCoord(PageNo, GlobalPageNo: Integer): Integer; override;
      function GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer; override;
      function AllowEmptyFirstPage: Boolean; override;
      function IgnoreFootnotes: Boolean; override;
    public
      Endnote: TRVEndnoteItemInfo;
      StartAt, NextStartAt, EndAt: Integer;
      FromNewPage: Boolean;
      constructor Create(RichView: TRVScroller); override;
      function GetSourceRVDataForPrinting: TCustomRVData; override;
      function GetCanvas: TCanvas; override;
      function GetDocProperties: TStringList; override;
      function GetPrintableAreaLeft(PageNo: Integer): Integer; override;
      function GetPrintableAreaTop(PageNo: Integer): Integer; override;
      (*
      procedure CreateFontInfoCache(ACanvas: TCanvas); override;
      procedure DestroyFontInfoCache(var Cache: TRVFontInfoCache); override;
      function GetFontInfoCache(ACanvas: TCanvas;
        RVData: TCustomRVFormattedData): TRVFontInfoCache; override;
      *)
      function GetParentData: TCustomRVData; override;
      function GetRootData: TCustomRVData; override;
      function GetAbsoluteParentData: TCustomRVData; override;
      function GetAbsoluteRootData: TCustomRVData; override;
  end;

  TRVFootnotePtblRVData = class (TRectPtblRVData)
    protected
      function IgnoreFootnotes: Boolean; override;
    public
      IndexOnPage: Integer;
      Footnote: TRVFootnoteItemInfo;
      FootnoteItemRVData: TCustomRVFormattedData;
      FootnoteDItemNo: Integer;
      procedure AdjustFootnoteRefWidths;
  end;

  TRVSidenotePtblRVData = class (TRectPtblRVData)
    protected
      function IgnoreFootnotes: Boolean; override;
    public
      FullRect: TRect;
      Sidenote: TRVSidenoteItemInfo;
      SidenoteItemRVData: TCustomRVFormattedData;
      SidenoteItemNo: Integer;
  end;
  procedure RVPrintFormatSidenotes(OwnerRVData: TCustomMultiPagePtblRVData;
   MainRVData: TPrintableRVData; HFType: TRVHFType);

  {$ENDIF}


implementation
uses PtblRV,
  {$IFDEF RICHVIEWDEF10}Types,{$ENDIF}
  {$IFNDEF RVDONOTUSETABLES}RVTable,{$ENDIF}
  {$IFNDEF RVDONOTUSESEQ}RVSeqItem,{$ENDIF}
  RVStr, Math;


{$IFNDEF RVDONOTUSESEQ}
procedure RVPrintFormatSidenotes(OwnerRVData: TCustomMultiPagePtblRVData;
  MainRVData: TPrintableRVData; HFType: TRVHFType);

  {....................................................................}
  function GetLeftMargin(PageNo: Integer): Integer;
  begin
    Result := MainRVData.PhysM.Left + MainRVData.GetPrintableAreaLeft(PageNo);
  end;
  {....................................................................}
  function GetRightMargin(PageNo: Integer): Integer;
  begin
    Result := (MainRVData.PhysM.Left + MainRVData.GetPageWidth + MainRVData.PhysM.Right)-
      (MainRVData.PhysM.Left + MainRVData.GetPrintableAreaLeft(PageNo) + MainRVData.RichView.Width);
  end;
  {....................................................................}
  function GetSidenoteWidth(PageNo: Integer; Sidenote: TRVSidenoteItemInfo): Integer;
  begin
    case Sidenote.BoxProperties.WidthType of
      rvbwtAbsolute:
        begin
          Result := OwnerRVData.GetRVStyle.GetAsDevicePixelsX(Sidenote.BoxProperties.Width, @(MainRVData.PrnSad));
          exit;
        end;
      rvbwtRelPage:
        Result := MainRVData.PhysM.Left + MainRVData.GetPageWidth + MainRVData.PhysM.Right;
      rvbwtRelMainTextArea:
        Result := MainRVData.RichView.Width;
      rvbwtRelLeftMargin:
        Result := GetLeftMargin(PageNo);
      rvbwtRelRightMargin:
        Result := GetRightMargin(PageNo);
      rvbwtRelInnerMargin:
        if PageNo mod 2 = 1 then
          Result := GetLeftMargin(PageNo)
        else
          Result := GetRightMargin(PageNo)
      else // rvbwtRelOuterMargin:
        if PageNo mod 2 = 0 then
          Result := GetLeftMargin(PageNo)
        else
          Result := GetRightMargin(PageNo)
    end;
    Result := Round(Result*Sidenote.BoxProperties.Width/1000/100);
  end;
  {....................................................................}
  function GetSidenoteHeight(PageNo: Integer; Sidenote: TRVSidenoteItemInfo): Integer;
  begin
    case Sidenote.BoxProperties.HeightType of
      rvbhtAbsolute:
        begin
          Result := OwnerRVData.GetRVStyle.GetAsDevicePixelsY(Sidenote.BoxProperties.Height, @(MainRVData.PrnSad));
          exit;
        end;
      rvbhtRelPage:
        Result := MainRVData.PhysM.Top + MainRVData.GetPageHeight + MainRVData.PhysM.Bottom;
      rvbhtRelMainTextArea:
        Result := MainRVData.GetContentHeightForPage(PageNo);
      rvbhtRelTopMargin:
        Result := MainRVData.PhysM.Top + MainRVData.GetTmpMForPage(PageNo).Top;
      else // rvbhtRelBottomMargin:
        Result := MainRVData.PhysM.Bottom + MainRVData.GetTmpMForPage(PageNo).Bottom;
    end;
    Result := Round(Result*Sidenote.BoxProperties.Height/1000/100);
  end;
  {....................................................................}
  function GetInDocCoords(OwnerRVData: TCustomMultiPagePtblRVData;
    SideNoteRVData: TRVSidenotePtblRVData; LocalPageNo, GlobalPageNo: Integer;
    var RHorz, RVert: TRect): Boolean;
  var FoundHere: Boolean;
  begin
    FoundHere := False;
    Result := OwnerRVData.GetAnchorObjectCoords(
      SideNoteRVData.SidenoteItemRVData as TCustomPrintableRVData,
      SideNoteRVData.SidenoteItemNo, GlobalPageNo, LocalPageNo,
      SideNoteRVData.Sidenote.BoxPosition.HorizontalAnchor,
      SideNoteRVData.Sidenote.BoxPosition.VerticalAnchor,
      SideNoteRVData.Sidenote.BoxPosition.RelativeToCell, RHorz, RVert, FoundHere);
  end;
  {....................................................................}
  function GetLeftMarginOffs(PageNo: Integer): Integer;
  begin
    Result := MainRVData.GetPrintableAreaLeft(PageNo);
  end;
  {....................................................................}
  function GetRightMarginOffs(PageNo: Integer): Integer;
  begin
    Result := MainRVData.GetPrintableAreaLeft(PageNo)+MainRVData.RichView.Width;
  end;
  {....................................................................}
  function GetSidenoteLeft(PageNo, BoxWidth: Integer; Sidenote: TRVSidenoteItemInfo;
    const Padding, AnchorInDoc: TRect; InDocPos: Boolean): Integer;
  var AnchorLeft, AnchorRight, DX: Integer;
  begin
    inc(BoxWidth, Padding.Left+Padding.Right);
    AnchorLeft := 0;
    if InDocPos then
      AnchorLeft := AnchorInDoc.Left
    else
      case Sidenote.BoxPosition.HorizontalAnchor of
        rvhanPage, rvhanLeftMargin:
          AnchorLeft := -MainRVData.PhysM.Left;
        rvhanMainTextArea:
          AnchorLeft := GetLeftMarginOffs(PageNo);
        rvhanRightMargin:
          AnchorLeft := GetRightMarginOffs(PageNo);
        rvhanInnerMargin:
          if PageNo mod 2 = 1 then
            AnchorLeft := -MainRVData.PhysM.Left
          else
            AnchorLeft := GetRightMarginOffs(PageNo);
        rvhanOuterMargin:
          if PageNo mod 2 = 0 then
            AnchorLeft := -MainRVData.PhysM.Left
          else
            AnchorLeft := GetRightMarginOffs(PageNo);
      end;
    Result := AnchorLeft;
    case Sidenote.BoxPosition.HorizontalPositionKind of
      rvfpkAlignment, rvfpkPercentPosition:
        begin
          AnchorRight := AnchorLeft;
          if InDocPos then
            AnchorRight := AnchorInDoc.Right
          else
            case Sidenote.BoxPosition.HorizontalAnchor of
              rvhanLeftMargin:
                AnchorRight := GetLeftMarginOffs(PageNo);
              rvhanMainTextArea:
                AnchorRight := GetRightMarginOffs(PageNo);
              rvhanPage, rvhanRightMargin:
                AnchorRight := MainRVData.GetPageWidth + MainRVData.PhysM.Right;
              rvhanInnerMargin:
                if PageNo mod 2 = 1 then
                  AnchorRight := GetLeftMarginOffs(PageNo)
                else
                  AnchorRight := MainRVData.GetPageWidth + MainRVData.PhysM.Right;
              rvhanOuterMargin:
                if PageNo mod 2 = 0 then
                  AnchorRight := GetLeftMarginOffs(PageNo)
                else
                  AnchorRight := MainRVData.GetPageWidth + MainRVData.PhysM.Right;
            end;
          DX := 0;
          case Sidenote.BoxPosition.HorizontalPositionKind of
            rvfpkAlignment:
              case Sidenote.BoxPosition.HorizontalAlignment of
                rvfphalRight:  DX := AnchorRight-AnchorLeft-BoxWidth;
                rvfphalCenter: DX := (AnchorRight-AnchorLeft-BoxWidth) div 2;
              end;
            rvfpkPercentPosition:
              DX := Round((AnchorRight-AnchorLeft)*Sidenote.BoxPosition.HorizontalOffset/1000/100);
          end;
          inc(Result, DX);
        end;
      rvfpkAbsPosition:
        inc(Result, RV_XToDevice(Sidenote.BoxPosition.HorizontalOffset, MainRVData.PrnSad));
    end;
    inc(Result, Padding.Left);
  end;
  {....................................................................}
  function GetSidenoteTop(PageNo, BoxHeight: Integer; Sidenote: TRVSidenoteItemInfo;
    const Padding, AnchorInDoc: TRect; InDocPos: Boolean): Integer;
  var AnchorTop, AnchorBottom, DY: Integer;
  begin
    inc(BoxHeight, Padding.Top+Padding.Bottom);
    AnchorTop := 0;
    if InDocPos then
      AnchorTop := AnchorInDoc.Top
    else
      case Sidenote.BoxPosition.VerticalAnchor of
        rvvanPage, rvvanTopMargin:
          AnchorTop := -MainRVData.PhysM.Top;
        rvvanMainTextArea:
          AnchorTop := MainRVData.GetTmpMForPage(PageNo).Top;
        rvvanBottomMargin:
          AnchorTop := MainRVData.GetTmpMForPage(PageNo).Top+MainRVData.GetContentHeightForPage(PageNo);
      end;
    Result := AnchorTop;
    case Sidenote.BoxPosition.VerticalPositionKind of
      rvfpkAlignment, rvfpkPercentPosition:
        begin
          AnchorBottom := AnchorTop;
          if InDocPos then
            AnchorBottom := AnchorInDoc.Bottom
          else
            case Sidenote.BoxPosition.VerticalAnchor of
              rvvanTopMargin:
                AnchorBottom := MainRVData.GetTmpMForPage(PageNo).Top;
              rvvanMainTextArea:
                AnchorBottom := MainRVData.GetTmpMForPage(PageNo).Top+MainRVData.GetContentHeightForPage(PageNo);
              rvvanPage, rvvanBottomMargin:
                AnchorBottom := MainRVData.GetPageHeight+MainRVData.PhysM.Bottom;
            end;
          DY := 0;
          case Sidenote.BoxPosition.VerticalPositionKind of
            rvfpkAlignment:
              case Sidenote.BoxPosition.VerticalAlignment of
                rvfpvalBottom:  DY := AnchorBottom-AnchorTop-BoxHeight;
                rvfpvalCenter: DY := (AnchorBottom-AnchorTop-BoxHeight) div 2;
              end;
            rvfpkPercentPosition:
              DY := Round((AnchorBottom-AnchorTop)*Sidenote.BoxPosition.VerticalOffset/1000/100);
          end;
          inc(Result, DY);
        end;
      rvfpkAbsPosition:
        inc(Result, RV_YToDevice(Sidenote.BoxPosition.VerticalOffset, MainRVData.PrnSad));
    end;
    inc(Result, Padding.Top);
  end;
  {....................................................................}
  procedure FillPadding(Sidenote: TRVSidenoteItemInfo; var Padding: TRect);
  var RVStyle: TRVStyle;
      PSaD: PRVScreenAndDevice;
  begin
    RVStyle := Sidenote.Document.GetRVStyle;
    PSaD :=  @(MainRVData.PrnSad);
    Padding.Left   := Sidenote.BoxProperties.GetTotalLeftPaddingSaD(RVStyle, PSaD);
    Padding.Top    := Sidenote.BoxProperties.GetTotalTopPaddingSaD(RVStyle, PSaD);
    Padding.Right  := Sidenote.BoxProperties.GetTotalRightPaddingSaD(RVStyle, PSaD);
    Padding.Bottom := Sidenote.BoxProperties.GetTotalBottomPaddingSaD(RVStyle, PSaD);
  end;
  {....................................................................}
  function GetSidenoteList(OwnerRVData: TCustomMultiPagePtblRVData; PageNo: Integer): TRVSidenoteList;
  var PageInfo: TRVPageInfo;
  begin
    if OwnerRVData is TRVHeaderFooterRVData then begin
      if TRVHeaderFooterRVData(OwnerRVData).Sidenotes=nil then
        TRVHeaderFooterRVData(OwnerRVData).Sidenotes := TRVSidenoteList.Create;
        Result := TRVHeaderFooterRVData(OwnerRVData).Sidenotes;
        end
      else begin
        PageInfo := OwnerRVData.Pages[PageNo-1];
        if PageInfo.Sidenotes=nil then
          PageInfo.Sidenotes := TRVSidenoteList.Create;
        Result := PageInfo.Sidenotes;
      end;
  end;
  {....................................................................}
  function NeedsInDocPositionHorz(Sidenote: TRVSidenoteItemInfo): Boolean;
  begin
    Result := SideNote.BoxPosition.HorizontalAnchor=rvhanCharacter;
    {$IFNDEF RVDONOTUSETABLES}
    if Result then
      exit;
    Result := SideNote.BoxPosition.RelativeToCell and
      (TCustomRVData(Sidenote.ParentRVData).GetSourceRVData is TRVTableCellData);
    {$ENDIF}
  end;
  {....................................................................}
  function NeedsInDocPositionVert(Sidenote: TRVSidenoteItemInfo): Boolean;
  begin
    Result := SideNote.BoxPosition.VerticalAnchor in [rvvanLine,rvvanParagraph];
    {$IFNDEF RVDONOTUSETABLES}
    if Result then
      exit;
    Result := SideNote.BoxPosition.RelativeToCell and
      (TCustomRVData(Sidenote.ParentRVData).GetSourceRVData is TRVTableCellData);
    {$ENDIF}
  end;
  {....................................................................}
var Sidenote: TRVSidenoteItemInfo;
  RVData: TRVSidenotePtblRVData;
  SidenoteItemRVData: TCustomPrintableRVData;
  ItemNo, LocalPageNo: Integer;
  FullHeight, DY: Integer;
  Padding, AnchorInDocHorz, AnchorInDocVert: TRect;
  InDocPosHorz, InDocPosVert: Boolean;
  {....................................................................}
  procedure InsertSidenoteRVData(GlobalPageNo: Integer);
  begin
    FillPadding(Sidenote, Padding);
    RVData := TRVSidenotePtblRVData.Create(MainRVData.RichView, Sidenote.Document,
      OwnerRVData);
    RVData.ParentDrawsBack := True;
    RVData.Transparent := Sidenote.BoxProperties.Background.Color=clNone;
    RVData.Sidenote := Sidenote;
    RVData.FColor := Sidenote.BoxProperties.Background.Color;
    RVData.SidenoteItemRVData := SidenoteItemRVData;
    RVData.SidenoteItemNo := ItemNo;
    RVData.Width := Max(0, GetSidenoteWidth(GlobalPageNo, Sidenote)-(Padding.Left+Padding.Right));
    RVData.Format(True, False);
    RVData.Height := RVData.DocumentHeight;
    if Sidenote.BoxProperties.HeightType=rvbhtAuto then begin
      FullHeight := RVData.DocumentHeight;
      DY := 0;
      end
    else begin
      FullHeight := Max(0, GetSidenoteHeight(GlobalPageNo, Sidenote)-(Padding.Top+Padding.Bottom));
      case Sidenote.BoxProperties.VAlign of
        tlTop:    DY := 0;
        tlCenter: DY := (FullHeight-RVData.DocumentHeight) div 2;
        else      DY := FullHeight-RVData.DocumentHeight;
      end;
    end;
    if DY<0 then
      DY := 0;
    AnchorInDocHorz := Rect(0,0,0,0);
    AnchorInDocVert := Rect(0,0,0,0);
    InDocPosHorz := NeedsInDocPositionHorz(SideNote);
    InDocPosVert := NeedsInDocPositionVert(SideNote);
    if InDocPosHorz or InDocPosVert then
      GetInDocCoords(OwnerRVData, RVData, LocalPageNo, GlobalPageNo,
        AnchorInDocHorz, AnchorInDocVert);
    RVData.FullRect := Bounds(
      GetSidenoteLeft(GlobalPageNo, RVData.Width, SideNote, Padding, AnchorInDocHorz, InDocPosHorz),
      GetSidenoteTop(GlobalPageNo, FullHeight, SideNote, Padding, AnchorInDocVert, InDocPosVert),
      RVData.Width, FullHeight);
    RVData.Left := RVData.FullRect.Left;
    RVData.Top := RVData.FullRect.Top +DY;
    GetSidenoteList(OwnerRVData, GlobalPageNo).Add(RVData);
  end;
  {....................................................................}
  { For headers and footers, making sure that margins are calculated for the correct page }
  procedure CalculateGlobalPageNo(var GlobalPageNo: Integer);
  begin
    if OwnerRVData is TRVHeaderFooterRVData then
      case HFType of
        rvhftNormal:
          GlobalPageNo := 3;
        rvhftFirstPage:
          GlobalPageNo := 1;
        rvhftEvenPages:
          GlobalPageNo := 2;
      end;
  end;
  {....................................................................}
var
  Found: Boolean;
  i, PageCount, GlobalPageNo: Integer;
begin
  Sidenote := RVGetFirstSidenoteInRootRVData(OwnerRVData.GetSourceRVDataForPrintingEx, False);
  if Sidenote=nil then
    exit;
  repeat
    if SideNote.ParentRVData is TCustomRVFormattedData then begin  // otherwise, it is inserted in another note
      ItemNo := TCustomRVData(SideNote.ParentRVData).GetItemNo(Sidenote);
      Found := OwnerRVData.GetPageNo(TCustomRVData(SideNote.ParentRVData),
        ItemNo, 1, OwnerRVData.GetSourceRVDataForPrintingEx,
        GlobalPageNo, LocalPageNo, PageCount, SidenoteItemRVData);
      if Found then begin
        CalculateGlobalPageNo(GlobalPageNo);
        for i := 0 to PageCount-1 do
          InsertSidenoteRVData(GlobalPageNo+i);
      end;
    end;
    Sidenote := RVGetNextSidenoteInRootRVData(OwnerRVData.GetSourceRVDataForPrintingEx, Sidenote, False);
  until Sidenote=nil;
end;
{$ENDIF}
{============================ TRVMultiDrawItemInfo ============================}
constructor TRVMultiDrawItemInfo.Create;
begin
  inherited Create;
  FPartsList := TRVMultiDrawItemPartsList.Create;
end;
{------------------------------------------------------------------------------}
destructor TRVMultiDrawItemInfo.Destroy;
begin
  FPartsList.Free;
  inherited  Destroy;
end;
{------------------------------------------------------------------------------}
procedure TRVMultiDrawItemInfo.ResetPages(var FootnoteRVDataList: TRVFootnoteRefList;
  var ReleasedHeightAfterFootnotes: Integer; FootnotesChangeHeight: Boolean);
begin
  FPartsList.Clear;
end;
{------------------------------------------------------------------------------}
procedure TRVMultiDrawItemInfo.UnformatLastPage(var FootnoteRVDataList: TRVFootnoteRefList;
  var ReleasedHeightAfterFootnotes: Integer; FootnotesChangeHeight: Boolean);
begin

end;
{------------------------------------------------------------------------------}
{ This function is called for drawing items that cannot be split }
procedure TRVMultiDrawItemInfo.AddAllFootnotes(
  var FootnoteRVDataList: TRVFootnoteRefList; var Height: Integer;
  FootnotesChangeHeight: Boolean);
begin

end;
{------------------------------------------------------------------------------}
{ This function is called for drawing items that cannot be split }
procedure TRVMultiDrawItemInfo.DecHeightByFootnotes(var Height: Integer;
  var ThisPageHasFootnotes: Boolean);
begin

end;
{------------------------------------------------------------------------------}
{ This function is called for drawing items that cannot be split }
procedure TRVMultiDrawItemInfo.RemoveAllFootnotes(
  var FootnoteRVDataList: TRVFootnoteRefList; var Height: Integer;
  FootnotesChangeHeight: Boolean);
begin

end;
{------------------------------------------------------------------------------}
{ Returns the index of part containing the given position, or -1 if not found.
  Also returns printable RVData containing this position }
function TRVMultiDrawItemInfo.GetPartNo(RVData: TCustomRVData; ItemNo, OffsInItem: Integer;
  var PartNo, LocalPageNo, LocalPageCount: Integer; var PtRVData: TCustomPrintableRVData): Boolean;
begin
  PartNo := -1;
  PtRVData := nil;
  LocalPageCount := 0;
  Result := False;
end;
{------------------------------------------------------------------------------}
function TRVMultiDrawItemInfo.GetSubDocCoords(Location: TRVStoreSubRVData; Part: Integer;
  var InnerCoords, OuterCoords: TRect; var Origin: TPoint): Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
function TRVMultiDrawItemInfo.GetLocalPageNoFor(Location: TRVStoreSubRVData; Part: Integer): Integer;
begin
  Result := 1;
end;
{------------------------------------------------------------------------------}
function TRVMultiDrawItemInfo.GetPrintableRVDataFor(Location: TRVStoreSubRVData): TCustomPrintableRVData;
begin
  Result := nil;
end;
{=============================== TRVPageInfo ==================================}
{$IFNDEF RVDONOTUSESEQ}
destructor TRVPageInfo.Destroy;
begin
  FootnoteRVDataList.Free;
  Sidenotes.Free;
  inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVPageInfo.Assign(Source: TPersistent);
{$IFNDEF RVDONOTUSESEQ}
var i: Integer;
{$ENDIF}
begin
  if Source is TRVPageInfo then begin
    StartY := TRVPageInfo(Source).StartY;
    StartDrawItemNo := TRVPageInfo(Source).StartDrawItemNo;
    StartPart := TRVPageInfo(Source).StartPart;
    StartY2 := TRVPageInfo(Source).StartY2;
    DocumentHeight := TRVPageInfo(Source).DocumentHeight;
    {$IFNDEF RVDONOTUSESEQ}
    FootnoteRVDataList.Free;
    if TRVPageInfo(Source).FootnoteRVDataList=nil then
      FootnoteRVDataList := nil
    else begin
      FootnoteRVDataList := TRVFootnoteRefList.Create;
      for i := 0 to TRVPageInfo(Source).FootnoteRVDataList.Count-1 do
        FootnoteRVDataList.Add(TRVPageInfo(Source).FootnoteRVDataList[i]);
    end;
    {$ENDIF}
    end
  else
    inherited Assign(Source);
end;
{============================= TRVPageCollection ==============================}
function TRVPageCollection.Add: TRVPageInfo;
begin
  Result := TRVPageInfo(inherited Add);
end;
{------------------------------------------------------------------------------}
constructor TRVPageCollection.Create;
begin
  inherited Create(TRVPageInfo);
end;
{------------------------------------------------------------------------------}
function TRVPageCollection.GetItem(Index: Integer): TRVPageInfo;
begin
  Result := TRVPageInfo(inherited GetItem(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVPageCollection.SetItem(Index: Integer;
  const Value: TRVPageInfo);
begin
  inherited SetItem(Index, Value);
end;
{======================== TRVMultiDrawItemPartsList ===========================}
function TRVMultiDrawItemPartsList.Get(
  Index: Integer): TRVMultiDrawItemPart;
begin
  Result := TRVMultiDrawItemPart(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVMultiDrawItemPartsList.Put(Index: Integer;
  const Value: TRVMultiDrawItemPart);
begin
  inherited Put(Index, Value);
end;
{============================== TCustomPrintableRVData ========================}
procedure TCustomPrintableRVData.DonePrinterCanvas(Canvas: TCanvas);
begin

end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetSourceRVDataForPrinting: TCustomRVData;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetSourceRVDataForPrintingEx: TCustomRVData;
begin
  Result := GetSourceRVDataForPrinting;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetColorMode: TRVColorMode;
begin
  Result := rvcmPrinterColor;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.ShareItems: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.InitPrinterCanvas: TCanvas;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetBack: TRVBackground;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetTopCoord(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetPrintableAreaLeft(PageNo: Integer): Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetPrintableAreaTop(PageNo: Integer): Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetExtraOffsForBackground: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.DoPagePrepaint(Canvas: TCanvas;
  GlobalPageNo:Integer; Preview, Correction, InRange: Boolean);
begin

end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.DoPagePostpaint(Canvas: TCanvas;
  GlobalPageNo: Integer; Preview, InRange: Boolean);
begin

end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.GetDrawItemsRange(PageNo: Integer; var StartNo, EndNo, Part: Integer);
begin
  StartNo := 0;
  EndNo   := DrawItems.Count-1;
  Part    := -1;
  //FirstOffs := 0;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.NoMetafiles: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.CheckPageNo(PageNo: Integer);
begin

end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.DrawBackToBitmap(Left, Top: Integer;
  bmp: TBitmap; const sad: TRVScreenAndDevice; ItemBackgroundLayer: Integer;
  RelativeToItem, AllowBitmaps, ParentAllowBitmaps: Boolean; PageNo: Integer);
var pi: TParaInfo;
    item: TCustomRVItemInfo;
    Clr: TColor;
    r,r2: TRect;
begin
  if RelativeToItem then begin
    inc(Left, RV_XToScreen(FDrawItem.Left, sad));
    inc(Top,  RV_YToScreen(FItemTop-GetPrintableAreaTop(PageNo), sad));
  end;
  item := GetItem(FDrawItem.ItemNo);
  pi := GetRVStyle.ParaStyles[item.ParaNo];
  r := Rect(0,0, bmp.Width, bmp.Height);
  if AllowBitmaps and ParentAllowBitmaps and (pi.Background.Color=clNone) and (FBackgroundBmp<>nil) then
    GetRVStyle.GraphicInterface.CopyRect(
      bmp.Canvas, Rect(0,0, bmp.Width, bmp.Height),
      FBackgroundBmp.Canvas, Bounds(Left, Top, bmp.Width, bmp.Height))
  else begin
    Clr := pi.Background.Color;
    if Clr = clNone then
      Clr := GetColor;
    if Clr = clNone then
      Clr := clWhite;
    bmp.Canvas.Pen.Color := Clr;
    bmp.Canvas.Brush.Color := Clr;
    GetRVStyle.GraphicInterface.FillRect(bmp.Canvas, r);
  end;
  if ItemBackgroundLayer<>0 then begin
    r2 := Bounds(RV_XToScreen(FDrawItem.Left, sad)-Left,
      RV_YToScreen(FItemTop-GetPrintableAreaTop(PageNo)-GetExtraOffsForBackground, sad)-Top,
      RV_XToScreen(FDrawItem.Width,sad),
      RV_YToScreen(FDrawItem.Height,sad));
    item.DrawBackgroundForPrinting(bmp.Canvas, r, r2, GetColorMode,
      ItemBackgroundLayer, GetRVStyle.GraphicInterface, AllowBitmaps);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.GetPrintableItemsRange(var MinItemNo, MaxItemNo: Integer);
begin
  MinItemNo := 0;
  MaxItemNo := ItemCount-1;
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.GetContentHeightForPage(PageNo: Integer): Integer;
begin
  Result := FRichView.ClientHeight;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.DoOnHyperlink(RVData: TCustomRVData;
  ItemNo: Integer; const R: TRect);
begin
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.DoOnCheckpoint(RVData: TCustomRVData;
  ItemNo, X, Y: Integer);
begin
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRVData.PageExists(PageNo: Integer): Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TCustomPrintableRVData.IgnoreFootnotes: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.CalcFootnoteCoords(References: TRVFootnoteRefList;
  PageNo: Integer);
begin
  if Self is TCustomMainPtblRVData then
    TCustomPrintableRV((GetAbsoluteRootData as TCustomMainPtblRVData).
      FRichView).CalcFootnotesCoords(References, PageNo);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomPrintableRVData.DrawPage(PageNo, GlobalPageNo: Integer;
  Canvas: TCanvas; Preview, Correction: Boolean);
var i,no: Integer;
    dli:TRVDrawLineInfo;
    item: TCustomRVItemInfo;
    zerocoord: Integer;
    first, last, part: Integer;
    tmpbmp : TBitmap;
    LeftOffs: Integer;
    RVStyle: TRVStyle;
    sad: TRVScreenAndDevice;
    MinItemNo, MaxItemNo: Integer;
    CanPrintToBmp: Boolean;
    //FloatingDrawItems: TRVFloatingDrawItems;
    {.......................................}
    function GetDevX(ScreenX: Integer):Integer;
    begin
      Result := MulDiv(ScreenX, sad.ppixDevice, sad.ppixScreen);
    end;
    {.......................................}
    function GetDevY(ScreenY: Integer):Integer;
    begin
      Result := MulDiv(ScreenY, sad.ppiyDevice, sad.ppiyScreen);
    end;
    {.......................................}
    procedure DrawBackground; // in-out: backgroundbmp
                              // in: Canvas
    var BackWidth, BackHeight: Integer;
        Color: TColor;
    begin
      BackWidth  := RV_XToScreen(GetWidth,  sad);
      BackHeight := RV_YToScreen(GetContentHeightForPage(GlobalPageNo), sad);
      if (GetBack<>nil) and
         (GetBack.Style <> bsNoBitmap) and
         not GetBack.Bitmap.Empty then begin
        if GetBack.Style=bsTiledAndScrolled then
          GetBack.Style:=bsTiled;
        FBackgroundBmp := TBitmap.Create;
        FBackgroundBmp.Width := BackWidth;
        FBackgroundBmp.Height := BackHeight;
        if Preview and (GetRVLogPalette<>nil) then
          FBackgroundBmp.Palette := CreatePalette(GetRVLogPalette^);
        GetBack.Draw(FBackgroundBmp.Canvas, Rect(0,0, BackWidth, BackHeight),
          0,0, 0, 0, BackWidth, BackHeight, GetColor, False, False,
          RVStyle.GraphicInterface, nil, nil, False);
        RV_PictureToDevice(Canvas, GetPrintableAreaLeft(GlobalPageNo),
          GetPrintableAreaTop(GlobalPageNo), -1, -1, @sad, FBackgroundBmp, Preview,
          RVStyle.GraphicInterface);
        end
      else begin
        FBackgroundBmp := nil;
        if not ParentDrawsBack then
          with Canvas do
            if GetColor<>clNone then begin
              case GetColorMode of
                rvcmColor:
                  Color := GetColor;
                rvcmPrinterColor:
                  Color := RV_GetPrnColor(GetColor);
                rvcmGrayScale:
                  Color := RV_GetGray(RV_GetPrnColor(GetColor));
                else
                  Color := clWhite;
              end;
              Pen.Color := Color;
              Brush.Color := Color;
              Brush.Style := bsSolid;
              Pen.Style := psClear;
              FillRect(Bounds(GetPrintableAreaLeft(GlobalPageNo), GetPrintableAreaTop(GlobalPageNo),
                GetWidth, GetContentHeightForPage(GlobalPageNo)));
              Pen.Style := psSolid;
            end;
      end;
    end;
    {.......................................}
    procedure DrawBackTotmpbmp(Top: Integer); // in: backgroundbmp,tmpbmp,dli,item
    begin
      DrawBackToBitmap(
        RV_XToScreen(FDrawItem.Left, sad)+RVStyle.GetAsPixels(item.GetBorderWidth),
        RV_YToScreen(FItemTop-GetPrintableAreaTop(GlobalPageNo), sad)+
        RVStyle.GetAsPixels(item.GetBorderHeight),
        tmpbmp, sad, -1, False, True, True, GlobalPageNo);
    end;
    {.......................................}
    procedure DrawParagraph(i: Integer);
    var R, R1: TRect;
        dli: TRVDrawLineInfo;
        item: TCustomRVItemInfo;
        pi: TParaInfo;
        j: Integer;
    begin
      dli := DrawItems[i];
      if (dli.ItemNo<MinItemNo) or (dli.ItemNo>MaxItemNo) then
        exit;
      item := GetItem(dli.ItemNo);
      pi := RVStyle.ParaStyles[item.ParaNo];
      if (pi.Border.Style<>rvbNone) or (pi.Background.Color<>clNone) or
        Assigned(RVStyle.OnDrawParaBack) then begin
        R.Left := LeftOffs+GetDevX(GetLeftMargin)+RVStyle.GetAsDevicePixelsX(pi.LeftIndent, @sad);
        if pi.FirstIndent<0 then
          inc(R.Left, RVStyle.GetAsDevicePixelsX(pi.FirstIndent, @sad));
        R.Right:= LeftOffs+GetDevX(GetLeftMargin)-RVStyle.GetAsDevicePixelsX(pi.RightIndent, @sad)+TextWidth;
        if (i=first) and (part>=0) then begin
          R.Top := dli.Top;
          R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[part].Height;
          end
        else begin
          R.Top := dli.Top-dli.ExtraSpaceAbove;
          if (i=last) and (dli is TRVMultiDrawItemInfo) and
            (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
            R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[0].Height
          else
            R.Bottom := dli.Top+dli.Height+dli.ExtraSpaceBelow;
        end;
        for j := i+1 to last do begin
          dli := DrawItems[j];
          if (dli.ItemNo<>DrawItems[j-1].ItemNo) and
             GetItem(dli.ItemNo).CanBeBorderStart then break;
          if dli.Top-dli.ExtraSpaceAbove<R.Top then
            R.Top := dli.Top-dli.ExtraSpaceAbove;
          if (j=last) and (dli is TRVMultiDrawItemInfo) and
            (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
            R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[0].Height
          else
            if dli.Top+dli.Height+dli.ExtraSpaceBelow>R.Bottom then
              R.Bottom := dli.Top+dli.Height+dli.ExtraSpaceBelow;
        end;
        {
        if (FloatingDrawItems<>nil) and (FloatingDrawItems.Count>0) then begin
          dec(R.Right, R.Left-1);
          FloatingDrawItems.AdjustLineSize(R.Left, 0, R.Left+R.Right,
            R.Top, R.Left, R.Right, True);
            inc(R.Right, R.Left-1);
        end;
        }
        OffsetRect(R,0,-zerocoord);
        dec(R.Right);
        dec(R.Bottom);
        R1 := R;
        pi.Background.PrepareDrawSaD(R1, RVStyle, sad);
        RVStyle.DrawParaBack(Canvas, item.ParaNo, R1, True, GetColorMode);
        pi.Border.DrawSaD(R, Canvas, RVStyle, sad, GetColorMode, RVStyle.GraphicInterface);
      end;
      {
      for j := i to last do begin
        dli := DrawItems[j];
        if (j>i) and (dli.ItemNo<>DrawItems[j-1].ItemNo) and
           GetItem(dli.ItemNo).CanBeBorderStart then
          break;
        if dli is TRVFloatingDrawLineInfo then begin
          if FloatingDrawItems=nil then
            FloatingDrawItems := TRVFloatingDrawItems.Create;
            FloatingDrawItems.Add(dli);
        end;
      end;
      }
    end;
    {.......................................}
    procedure DrawTextBacks(i: Integer);
    var R: TRect;
        dli: TRVDrawLineInfo;
        item: TCustomRVItemInfo;
        j: Integer;
        LineTopY, LineBottomY, ItemNo, StyleNo: Integer;
        {.......................................}
        procedure CalculateLineVerticalBounds(i, Top, Bottom: Integer);
        var j: Integer;
            dli: TRVDrawLineInfo;
        begin
          LineTopY := Top;
          LineBottomY := Bottom;
          for j := i+1 to last do begin
            dli := DrawItems[j];
            if dli.FromNewLine then
              break;
            if dli.Top-dli.ExtraSpaceAbove<LineTopY then
              LineTopY := dli.Top-dli.ExtraSpaceAbove;
            if (j=last) and (dli is TRVMultiDrawItemInfo) and
              (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
              LineBottomY := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[0].Height
            else
              if dli.Top+dli.Height+dli.ExtraSpaceBelow>LineBottomY then
                LineBottomY := dli.Top+dli.Height+dli.ExtraSpaceBelow;
          end;
        end;
        {.......................................}
        procedure DrawTextBack(ItemNo: Integer);
        begin
          if RVStyle.TextBackgroundKind=rvtbkLines then begin
            R.Top := LineTopY;
            R.Bottom := LineBottomY;
          end;
          OffsetRect(R, LeftOffs, -zerocoord);
          RVStyle.DrawTextBack(Canvas, ItemNo, StyleNo,
            Self, R.Left, R.Top, R.Right-R.Left, R.Bottom-R.Top,
            [], True, GetColorMode);
        end;
        {.......................................}
    begin
      dli := DrawItems[i];
      if (dli.ItemNo<MinItemNo) or (dli.ItemNo>MaxItemNo) then
        exit;
      item := GetItem(dli.ItemNo);
      if (RVStyle.TextBackgroundKind<>rvtbkSimple) or Assigned(RVStyle.OnDrawTextBack) then begin
        if (i=first) and (part>=0) then begin
          R.Top := dli.Top;
          R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[part].Height;
          end
        else begin
          R.Top := dli.Top-dli.ExtraSpaceAbove;
          if (i=last) and (dli is TRVMultiDrawItemInfo) and
            (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
            R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[0].Height
          else
            R.Bottom := dli.Top+dli.Height+dli.ExtraSpaceBelow;
        end;
        R.Left := dli.Left;
        R.Right := dli.Left+dli.Width;
        StyleNo := GetActualStyle(item);
        ItemNo  := dli.ItemNo;
        if RVStyle.TextBackgroundKind=rvtbkLines then
          CalculateLineVerticalBounds(i, R.Top, R.Bottom);
        for j := i+1 to last do begin
          dli := DrawItems[j];
          item := GetItem(dli.ItemNo);
          if (dli.ItemNo<>DrawItems[j-1].ItemNo) and item.CanBeBorderStart then
            break;
          if (StyleNo>=0) and (StyleNo=GetActualStyle(item)) and not dli.FromNewLine then begin
            if dli.Top-dli.ExtraSpaceAbove<R.Top then
              R.Top := dli.Top-dli.ExtraSpaceAbove;
            if dli.Left<R.Left then
              R.Left := dli.Left;
            if dli.Left+dli.Width>R.Right then
              R.Right := dli.Left+dli.Width;
            if (j=last) and (dli is TRVMultiDrawItemInfo) and
              (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
              R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[0].Height
            else
              if dli.Top+dli.Height+dli.ExtraSpaceBelow>R.Bottom then
                R.Bottom := dli.Top+dli.Height+dli.ExtraSpaceBelow;
            end
          else begin
            if StyleNo>=0 then
              DrawTextBack(ItemNo);
            R.Top := dli.Top-dli.ExtraSpaceAbove;
            R.Left := dli.Left;
            R.Right := dli.Left+dli.Width;
            if (j=last) and (dli is TRVMultiDrawItemInfo) and
              (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
              R.Bottom := dli.Top+TRVMultiDrawItemInfo(dli).PartsList[0].Height
            else
              R.Bottom := dli.Top+dli.Height+dli.ExtraSpaceBelow;
            if dli.FromNewLine and (RVStyle.TextBackgroundKind=rvtbkLines) then
              CalculateLineVerticalBounds(j, R.Top, R.Bottom);
            StyleNo := GetActualStyle(item);
            ItemNo := dli.ItemNo;
          end;
        end;
        if StyleNo>=0 then
          DrawTextBack(ItemNo);
      end;
    end;
    {.......................................}
    procedure DrawItemBorder;
    var LItem: TRVRectItemInfo;
        R: TRect;
    begin
      if not (item is TRVRectItemInfo) then
        exit;
      LItem := TRVRectItemInfo(Item);
      R := Bounds(dli.ObjectLeft+LeftOffs, FItemTop, dli.ObjectWidth, dli.ObjectHeight);
      if LItem.BackgroundColor<>clNone then begin
        Canvas.Brush.Style := bsSolid;
        Canvas.Brush.Color := LItem.BackgroundColor;
        RVStyle.GraphicInterface.FillRect(Canvas, R);
      end;
      if (LItem.BorderWidth>0) and (LItem.BorderColor<>clNone) then begin
        // to-do: different widths if different hor/ver resolution
        Canvas.Pen.Width := RVStyle.GetAsDevicePixelsY(LItem.BorderWidth, @sad);
        Canvas.Pen.Color := LItem.BorderColor;
        Canvas.Brush.Color := clNone;
        Canvas.Brush.Style := bsClear;
        RVStyle.GraphicInterface.Rectangle(Canvas, R.Left, R.Top, R.Right, R.Bottom);
      end;

    end;
    {.......................................}
var w, h, part2: Integer;
    BiDiMode, BiDiMode2: TRVBiDiMode;
    TextStyle: TFontInfo;
begin
  RVStyle := GetRVStyle;
  if RVStyle=nil then
    raise ERichViewError.Create(errStyleIsNotAssigned);
  GetSADForFormatting(Canvas, sad);
  Canvas.Brush.Style := bsClear;
  FBackgroundBmp := nil;
  GetDrawItemsRange(PageNo, first, last, part);
  GetPrintableItemsRange(MinItemNo, MaxItemNo);
  if (first<0) or (last<0) or ((DrawItems[first].ItemNo>=MinItemNo) and (DrawItems[first].ItemNo<=MaxItemNo)) then
    DrawBackground;
  DoPagePrepaint(Canvas, GlobalPageNo, Preview, Correction,
    (first<0) or (last<0) or ((DrawItems[first].ItemNo>=MinItemNo) and (DrawItems[first].ItemNo<=MaxItemNo)));
  if not PageExists(PageNo) then begin
    FBackgroundBmp.Free;
    DoPagePostpaint(Canvas, GlobalPageNo, Preview,
      (last<0) or (last<0) or ((DrawItems[last].ItemNo>=MinItemNo) and (DrawItems[last].ItemNo<=MaxItemNo)));
    exit;
  end;
//  GetDrawItemsRange(pgNo, first, last, part);
  zerocoord := GetTopCoord(PageNo, GlobalPageNo);
  LeftOffs  := GetPrintableAreaLeft(GlobalPageNo);
  tmpbmp := TBitmap.Create;
  {$IFDEF RVDEBUGTABLE}
  if Self is TRectPtblRVData then begin
    Canvas.Pen.Color := clRed;
    Canvas.Pen.Width := 0;
    RVStyle.GraphicInterface.Rectangle(Canvas,
      GetPrintableAreaLeft, GetPrintableAreaTop,
      GetPrintableAreaLeft+TRectPtblRVData(Self).Width,
      GetPrintableAreaTop +TRectPtblRVData(Self).Height);
    end
  else begin
  { // Debug: drawing lines showing RVPrint.StartAt and EndAt
    if PgNo=1 then
      with Self as TCustomMainPtblRVData do  begin
        Canvas.Pen.Color := clRed;
        Canvas.Pen.Width := 0;
        Canvas.Pen.Style  := psDot;
        Canvas.MoveTo(LeftOffs-300, TmpTM+TmpTMPix+TPrintableRV(FRichView).FRVPrint.StartAt);
        Canvas.LineTo(300+GetWidth+LeftOffs-GetDevX(GetRightMargin), TmpTM+TmpTMPix+TPrintableRV(FRichView).FRVPrint.StartAt);
        Canvas.Pen.Style  := psSolid;
      end;
    if PgNo=(Self as TCustomMainPtblRVData).PagesColl.Count then
      with Self as TCustomMainPtblRVData do  begin
        Canvas.Pen.Color := clRed;
        Canvas.Pen.Width := 0;
        Canvas.Pen.Style  := psDot;
        Canvas.MoveTo(LeftOffs-300, TmpTM+TmpTMPix+TPrintableRV(FRichView).FRVPrint.EndAt);
        Canvas.LineTo(300+GetWidth+LeftOffs-GetDevX(GetRightMargin), TmpTM+TmpTMPix+TPrintableRV(FRichView).FRVPrint.EndAt);
        Canvas.Pen.Style  := psSolid;
      end
    }
  end;
  {$ENDIF}
  if Preview and (GetRVLogPalette<>nil) then
    tmpbmp.Palette := CreatePalette(GetRVLogPalette^);
  try
    part2 := part;
    //FloatingDrawItems := nil;
    //try
      for i:=first to last do begin
        dli := DrawItems[i];
        item := GetItem(dli.ItemNo);
        if (i=last) and (i<>first) and (dli is TRVMultiDrawItemInfo) and
            (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
          part := 0;
        if ((i=first) or
            ((dli.ItemNo<>DrawItems[i-1].ItemNo) and item.CanBeBorderStart)) and
           (item.StyleNo<>rvsBreak) then begin
          DrawParagraph(i);
          DrawTextBacks(i);
        end;
        if part<>-1 then begin
          if i=first then
            zerocoord := GetTopCoord2(PageNo, GlobalPageNo);
          part := -1;
        end;
      end;
    //finally
    //  FloatingDrawItems.Free;
    //end;
    zerocoord := GetTopCoord(PageNo, GlobalPageNo);
    part := part2;
    for i:=first to last do begin
      dli := DrawItems[i];
      FDrawItem := dli;
      if dli.ItemNo<MinItemNo then
        continue;
      if dli.ItemNo>MaxItemNo then
        break;
      item := GetItem(dli.ItemNo);
      BiDiMode := GetParaBiDiMode(item.ParaNo);
      if (i=last) and (i<>first) and (dli is TRVMultiDrawItemInfo) and
          (TRVMultiDrawItemInfo(dli).PartsList.Count>0) then
        part := 0;
      if item.GetBoolValueEx(rvbpJump, RVStyle) then
        DoOnHyperlink(GetSourceRVDataForPrinting, dli.ItemNo,
          Bounds(dli.Left+LeftOffs, dli.Top-zerocoord, dli.Width, dli.Height));
      if (item.Checkpoint<>nil) and IsDrawItemItemStart(i) and (Part<=0) then begin
        //if (Part>0) or ((Part=0)and(i=first)) then {!}
        //  FItemTop := -zerocoord
        //else
          FItemTop := dli.Top-zerocoord;
        DoOnCheckpoint(GetSourceRVDataForPrinting, dli.ItemNo,
          dli.Left+LeftOffs, FItemTop);
        if (rvoShowCheckpoints in Options) then
          RVStyle.DrawCheckpoint(Canvas, dli.Left+LeftOffs, FItemTop,
            LeftOffs, GetWidth, Self, dli.ItemNo,
            0, item.Checkpoint.RaiseEvent, nil);
      end;
      no := GetActualStyle(item);
      if no>=0 then begin{ text }
        if RVStyle.TextBackgroundKind<>rvtbkSimple then begin
          RVStyle.ApplyStyleTextColor(Canvas, no, [], True, GetColorMode);;
          Canvas.Brush.Style := bsClear;
          end
        else
          RVStyle.ApplyStyleColor(Canvas, no, [], True, GetColorMode);
        RVStyle.ApplyStyle(Canvas, no, BiDiMode,
          TCustomPrintableRV(RichView).CanUseCustomPPI,
          TCustomPrintableRV(RichView).CanUseCustomPPI,
          nil, False, False);
        TextStyle := RVStyle.TextStyles[no];
        if TextStyle.BiDiMode=rvbdUnspecified then
          BidiMode2 := BiDiMode
        else
          BidiMode2 := TextStyle.BiDiMode;
        if BidiMode2=rvbdUnspecified then begin
          if TextStyle.CharSpacing<>0 then
            RVStyle.GraphicInterface.SetTextCharacterExtra(Canvas,
              RVStyle.GetAsDevicePixelsX(TextStyle.CharSpacing, @sad));
        end;
        RVStyle.DrawStyleText(
          RV_ReturnProcessedString(DrawItems.GetString(i,Items), RVStyle.TextStyles[no],
            IsDrawItemLastOnWrappedLine(i), False, True),
          Canvas, dli.ItemNo, dli.Offs,  no, Self,
          dli.SpaceAtLeft,
          dli.Left+LeftOffs, dli.Top-zerocoord,
          dli.Width, dli.Height,
          {$IFDEF RVUSEBASELINE}
          dli.BaseLine-zerocoord,
          {$ELSE}
          {$IFDEF RVHIGHFONTS}
          dli.DrawOffsY,
          {$ENDIF}
          {$ENDIF}
          [], True, Preview and Correction,
          TCustomPrintableRV(RichView).CanUseCustomPPI, GetColorMode,
          BiDiMode, nil);
        end
      else begin // nontext
        if (not NoMetafiles and item.GetBoolValueEx(rvbpPrintToBMP, RVStyle)) or
           (NoMetafiles and item.GetBoolValueEx(rvbpPrintToBMPIfNoMetafiles, RVStyle)) then begin
          //if (Part>0) or ((Part=0) and (i=first)) then {!}
          //  FItemTop := -zerocoord
          //else
            FItemTop := dli.ObjectTop-zerocoord;
          if (Part>=0) then
            h := TRVMultiDrawItemInfo(dli).PartsList[Part].GetImageHeight
          else
            h := item.GetImageHeight(RVStyle);
          if item.GetBoolValue(rvbpFullWidth) then
            w := RV_XToScreen(GetWidth-GetDevX(GetRightMargin)-dli.Left, sad)
          else
            w  := item.GetImageWidth(RVStyle);
          tmpbmp.PixelFormat := pf32bit;
          tmpbmp.Width := w;
          tmpbmp.Height := h;
          DrawItemBorder;
          DrawBackToTmpBmp(FItemTop);
          Canvas.Lock;
          try
            CanPrintToBmp := item.PrintToBitmap(tmpbmp, Preview, FRichView, dli,
              Part, GetColorMode);
          finally
            Canvas.Unlock;
          end;
          if CanPrintToBmp then
            RV_PictureToDevice(Canvas,
              dli.ObjectLeft+LeftOffs+RVStyle.GetAsDevicePixelsX(item.GetBorderWidth, @sad),
              FItemTop + RVStyle.GetAsDevicePixelsY(item.GetBorderHeight, @sad),
              w, h, @sad, tmpbmp, Preview, RVStyle.GraphicInterface);
          end
        else begin
          //if (Part>0) or ((Part=0) and (i=first)) then
          //  FItemTop := -zerocoord
          //else
            FItemTop := dli.ObjectTop-zerocoord;
          if item.GetBoolValue(rvbpFullWidth) then
            item.Print(Canvas, dli.ObjectLeft+LeftOffs, FItemTop,
              LeftOffs+GetDevX(GetLeftMargin)+TextWidth, //GetWidth+LeftOffs-GetDevX(GetRightMargin),
              Preview, Correction, sad, nil, dli, part, GetColorMode, Self, GlobalPageNo)
          else begin
            DrawItemBorder;
            item.Print(Canvas, dli.ObjectLeft+LeftOffs, FItemTop,0,
              Preview, Correction, sad, nil, dli, part, GetColorMode, Self, GlobalPageNo);
          end;
        end;
        if part<>-1 then begin
          if i=first then
            zerocoord := GetTopCoord2(PageNo, GlobalPageNo);
          part := -1;
        end;
      end;
    end;
    {
     // Debug: drawing red line showing bottom of document
    if Self is TCustomMainPtblRVData then begin
      Canvas.Pen.Color := clRed;
      Canvas.MoveTo(LeftOffs, TCustomMultiPagePtblRVData(Self).Pages[pgNo-1].DocumentHeight+
        TCustomMainPtblRVData(Self).TmpTMPix+TCustomMainPtblRVData(Self).TmpM.Top);
      Canvas.LineTo(LeftOffs+100, TCustomMultiPagePtblRVData(Self).Pages[pgNo-1].DocumentHeight+
        TCustomMainPtblRVData(Self).TmpTMPix+TCustomMainPtblRVData(Self).TmpM.Top);
    end;
    }
    DoPagePostpaint(Canvas, GlobalPageNo, Preview,
      (last<0) or ((DrawItems[last].ItemNo>=MinItemNo) and (DrawItems[last].ItemNo<=MaxItemNo)));
    {$IFDEF RVWATERMARK}
    if rvflRoot in Flags then begin
      Canvas.Brush.Style := bsClear;
      Canvas.Font.Name := 'Arial';
      Canvas.Font.Size := 8;
      Canvas.Font.Style := [];
      Canvas.Font.Color := clRed;
      Canvas.TextOut(GetPrintableAreaLeft(PgNo), GetPrintableAreaTop, 'unregistered');
    end;
    {$ENDIF}
  finally
    RVFreeAndNil(FBackgroundBmp);
    FDrawItem := nil;
    tmpbmp.Free;
  end;
end;
{========================= TCustomMultiPagePtblRVData =========================}
destructor TCustomMultiPagePtblRVData.Destroy;
begin
  Pages.Free;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.DoFormatting(PageCompleted: Integer;
  Step: TRVPrintingStep);
begin

end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.GetInitialStartAt: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.GetFurtherStartAt: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.SetEndAt(Value: Integer);
begin
  // nothing to do here
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.IncEndAtByStartAt(PageNo: Integer);
begin
  // nothing to do here
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.GetDrawItemsRange(PageNo: Integer;
  var StartNo, EndNo, Part: Integer);
begin
  if Pages=nil then begin
    inherited GetDrawItemsRange(PageNo, StartNo, EndNo, Part);
    exit;
  end;
  if PageNo>Pages.Count then begin
    StartNo := -1;
    EndNo := -1;
    exit;
  end;
  StartNo := Pages[PageNo-1].StartDrawItemNo;
  Part := Pages[PageNo-1].StartPart-1;
  //FirstOffs := TRVPageInfo(PagesColl.Items[PageNo-1]).FirstItemOff;
  if PageNo=Pages.Count then
    EndNo := DrawItems.Count-1
  else begin
    EndNo := Pages[PageNo].StartDrawItemNo-1;
    if Pages[PageNo].StartPart>1 then
      inc(EndNo);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.GetTopCoord(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := Pages[PageNo-1].StartY;
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := Pages[PageNo-1].StartY2;
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.FormatNextPage(var i, StartAt, StartY, Y: Integer;
  var Splitting: Boolean; var MaxHeight: Integer;
  var FootnoteRVDataList: TRVFootnoteRefList; FootnotesChangeHeight: Boolean);
var dli :TRVDrawLineInfo;
    rvpi       : TRVPageInfo;
    nPages, EndAt: Integer;
    MustBreak: Boolean;
    pi: TParaInfo;
    SaD: TRVScreenAndDevice;
    RVStyle: TRVStyle;
    PageAdded: Boolean;
    {$IFNDEF RVDONOTUSESEQ}
    FootnoteRVData: TRVFootnotePtblRVData;
    {$ENDIF}
    IgnorePB: Boolean;
    {................................................}
    { Returns Y coordinate of bottom of the line ended on the LastDItemNo-th
      drawing item.
      The result is incremented by SpaceAfter of paragraph style.
      Floating items are taken into account. }
    function GetLineBottomCoord(LastDItemNo: Integer): Integer;
    var
      dli :TRVDrawLineInfo;
      pi: TParaInfo;
      i: Integer;
    begin
      if LastDItemNo<0 then begin
        Result := 0;
        exit;
      end;
      dli := DrawItems[LastDItemNo];
      pi := GetRVStyle.ParaStyles[GetItemPara(dli.ItemNo)];
      Result := dli.Top+dli.Height+dli.ExtraSpaceBelow;
      for i := LastDItemNo  downto 0 do begin
        dli := DrawItems[i];
        if dli.Top+dli.Height+dli.ExtraSpaceBelow>Result then
          Result := dli.Top+dli.Height+dli.ExtraSpaceBelow;
        if (dli is TRVFloatingDrawLineInfo) and
          (TRVFloatingDrawLineInfo(dli).FloatBottom>Result) then
          Result := TRVFloatingDrawLineInfo(dli).FloatBottom;
        if dli.FromNewLine and not dli.FloatingAboveUsed then
          break;
      end;
      inc(Result, RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD));
    end;
    {................................................}
    { Returns Y coordinate of top of the line started from FirstDItemNo-th
      drawing item.
      The result is decremented by SpaceBefore of paragraph style. }
    function GetLineTopCoord(FirstDItemNo: Integer): Integer;
    var
      dli :TRVDrawLineInfo;
      pi: TParaInfo;
      i: Integer;
    begin
      dli := DrawItems[FirstDItemNo];
      pi := GetRVStyle.ParaStyles[GetItemPara(dli.ItemNo)];
      Result := dli.Top-dli.ExtraSpaceAbove;
      for i := FirstDItemNo+1 to DrawItems.Count-1 do begin
        dli := DrawItems[i];
        if dli.FromNewLine then
          break;
        if dli.Top-dli.ExtraSpaceAbove<Result then
          Result := dli.Top-dli.ExtraSpaceAbove;
      end;
      dec(Result, RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD));
    end;
    {................................................}
    procedure ProcessEndAt;
    var rvpi: TRVPageInfo;
    begin
      rvpi := Pages[nPages-1];
      if rvpi.StartPart=0 then
        dec(EndAt,rvpi.StartY)
      else
        dec(EndAt,rvpi.StartY2);
      rvpi.DocumentHeight := EndAt{+TmpTMPix+TmpBMPix};
    end;
    {................................................}
    {
      function FindFirstItemOnPage.
      The main implicit hidden parameter of this function is i - index
      of the first drawitem that does not fit the page.
      This function goes from i downto 0 to find the first item that will
      start a new page (taking keep-with-next, keep-lines-together and
      floating pictures into account).

      If MultiItem, the i-th item is a table. For this table, a splitting
      may be initialized.
      For such tables, two options are possible.
       If this table cannot be splitted, OnlyIfExceedPage=False.
        Such tables are treated like other items, with the only exception:
        if i is not changed, this function does not add a page.
       If this table can be splitted, OnlyIfExceedPage=True.
        For such tables, we are interested only in floating pictures from
        the lines above, if these pictures exceed the page. If such pictures
        exist, the table is processed like if OnlyIfExceedPage=False.
      If i changed, a splitting unitialized for this table.

      Among other actions, this function undoes changes made for footnotes
      (for drawitems in range between old and new values of i, inclisive).
      If IgnoreLastFootnote=True, these changes were not made for the footnote
      corresponding to the i-th drawing item (old value of i),
      so they must not be undone for it.

      This function can be called for i = DrawItems.Count to check the document
      end. In this case, OnlyIfExceedPage=True.

      A new page is added if:
      - (MultiItem=False or i is changed) and (new value of i <> DrawItems.Count) 
    }
    function FindFirstItemOnPage(MultiItem, IgnoreLastFootnote, OnlyIfExceedPage: Boolean;
      pi: TParaInfo): Boolean;
    var nextnewline, oldnexntewline, nextnewline2, j, k, tmp, oldi, oldi2,
        LastDItemOnTheFirstLineOfPage: Integer;
        LineUp: Boolean;
        {$IFNDEF RVDONOTUSESEQ}
        FootnoteRefIndex: Integer;
        {$ENDIF}
        KeepAllTogether: Boolean;
    begin
      KeepAllTogether := GetSourceRVDataForPrinting.KeepTogether;
      oldi := i;
      if oldi<DrawItems.Count then
        oldi2 := oldi
      else
        oldi2 := oldi-1;
      LastDItemOnTheFirstLineOfPage := Pages.Items[nPages-1].StartDrawItemNo;
      for j := LastDItemOnTheFirstLineOfPage+1 to DrawItems.Count-1 do
        if DrawItems[j].FromNewLine then begin
          LastDItemOnTheFirstLineOfPage := j-1;
          break;
        end;
      { testing the previous line for floating pictures }
      if not OnlyIfExceedPage then begin
        LineUp := False;
        for j := i-1 downto j do begin
          if (DrawItems[j] is TRVFloatingDrawLineInfo) and
            (TRVFloatingDrawLineInfo(DrawItems[j]).FloatBottom > StartY+MaxHeight-StartAt) then
            LineUp := True;
          if DrawItems[j].FromNewLine then begin
            if LineUp and (j>Pages.Items[nPages-1].StartDrawItemNo) then
              i := j;
            break;
          end;
        end;
      end;
      { searching for the item which will be the first on the new page }
      nextnewline := i;
      while True do begin
        { checking keep-with-next and keep-lines-together }
        if not OnlyIfExceedPage then begin
          oldnexntewline := -1;
          while True do begin
            nextnewline2 := -1;
            for j:=nextnewline downto 0 do
              if DrawItems[j].FromNewLine then begin
                if nextnewline2<0 then
                  nextnewline2 := j;
                if not (KeepAllTogether or
                        (rvpaoKeepLinesTogether in pi.Options) or
                        (rvpaoKeepWithNext in pi.Options))
                   or IsDrawItemParaStart(j) then begin
                  nextnewline := j;
                  break;
                end;
              end;
            if (StartAt<=0) and (nextnewline2>=0) and
              (nextnewline <= Pages.Items[nPages-1].StartDrawItemNo) then begin
              if oldnexntewline>=0 then begin
                nextnewline := oldnexntewline;
                break;
              end;
              nextnewline := nextnewline2;
            end;
            if (StartAt<=0) and
               ((nextnewline <= Pages.Items[nPages-1].StartDrawItemNo) or (nextnewline<=0)) then
              break;
            if nextnewline=0 then
              break;
            pi := GetRVStyle.ParaStyles[GetItemPara(DrawItems[nextnewline-1].ItemNo)];
            if not IsDrawItemParaStart(nextnewline) or
               not (KeepAllTogether or (rvpaoKeepWithNext in pi.Options)) or
               (not IgnorePB and PageBreaksBeforeItems[DrawItems[nextnewline].ItemNo]) then
              break;
            if oldnexntewline<0 then
              oldnexntewline := nextnewline;
            dec(nextnewline);
          end;
        end;
        { checking intersections with floating items }
        if nextnewline < DrawItems.Count then begin
          tmp := DrawItems[nextnewline].Top;
          k := nextnewline;
          for j := nextnewline+1 to DrawItems.Count-1 do begin
            if DrawItems[j].FromNewLine then
              break;
            if DrawItems[j].Top<tmp then begin
              tmp := DrawItems[j].Top;
              k := j;
            end;
          end;
          end
        else
          k := nextnewline;
        if OnlyIfExceedPage then
          j := StartY+MaxHeight-StartAt+1
        else
          j := 0;
        k := DrawItems.FindFirstFloatingAboveDrawItem(k,
          LastDItemOnTheFirstLineOfPage+1, j, OnlyIfExceedPage);
        if (k<0) or (k>=nextnewline) then
          break;
        OnlyIfExceedPage := False;
        nextnewline := k;
        if nextnewline=0 then
          break;
        pi := GetRVStyle.ParaStyles[GetItemPara(DrawItems[nextnewline-1].ItemNo)];
      end;
      { page must contain one line at least}
      if not OnlyIfExceedPage and ((StartAt<=0) or not AllowEmptyFirstPage) and
        (nextnewline <= Pages.Items[nPages-1].StartDrawItemNo) then begin
        tmp := nextnewline;
        nextnewline := DrawItems.Count;
        for j := tmp+1 to DrawItems.Count-1 do
          if DrawItems[j].FromNewLine then begin
            nextnewline := j;
            break;
          end;
        if (nextnewline<i) and not IsDrawItemParaStart(nextnewline) then
          for j := nextnewline+1 to i do
            if IsDrawItemParaStart(j) then begin
              nextnewline := j-1;
              break;
            end;
      end;
      {$IFNDEF RVDONOTUSESEQ}
      if (FootnoteRVDataList<>nil) and not IgnoreFootnotes then begin
        for j := oldi2 downto nextnewline do
          if (not IgnoreLastFootnote or (j<oldi)) then begin
            if (GetItem(DrawItems[j].ItemNo) is TRVFootnoteItemInfo) then begin
              FootnoteRefIndex := FootnoteRVDataList.GetFootnoteIndex(TRVFootnoteItemInfo(GetItem(DrawItems[j].ItemNo)));
              if FootnoteRefIndex>=0 then begin
                FootnoteRVDataList.Delete(FootnoteRefIndex);
                if FootnoteRVDataList.Count=0 then begin
                  RVFreeAndNil(FootnoteRVDataList);
                end;
              end;
            end;
            if DrawItems[j] is TRVMultiDrawItemInfo then
              if (oldi<>nextnewline) then begin
                TRVMultiDrawItemInfo(DrawItems[j]).ResetPages(FootnoteRVDataList,
                  MaxHeight, FootnotesChangeHeight);
              end;
              {
              else
                TRVMultiDrawItemInfo(DrawItems[j]).RemoveAllFootnotes(FootnoteRVDataList,
                  MaxHeight, FootnotesChangeHeight)};
            if FootnotesChangeHeight then
              MaxHeight := DrawItems[j].Top+(1)+StartAt-StartY;
          end;
        end
      else
      {$ENDIF}
      begin
        if (oldi<>nextnewline) then
          for j := oldi2 downto nextnewline do
            if DrawItems[j] is TRVMultiDrawItemInfo then
              TRVMultiDrawItemInfo(DrawItems[j]).ResetPages(FootnoteRVDataList,
                  MaxHeight, FootnotesChangeHeight);
      end;
      if (nextnewline<>DrawItems.Count) and ((nextnewline<>oldi) or not MultiItem) then begin
        { searching min y of first line in new page }
        StartY := GetLineTopCoord(nextnewline);
        rvpi             := Pages.Add;
        rvpi.StartDrawItemNo := nextnewline;
        rvpi.StartY      := StartY;
        PageAdded := True;
        {$IFNDEF RVDONOTUSESEQ}
        if not IgnoreFootnotes and (Self is TCustomMainPtblRVData) then begin
          Pages[Pages.Count-2].FootnoteRVDataList := FootnoteRVDataList;
          CalcFootnoteCoords(FootnoteRVDataList, nPages);
        end;
        {$ENDIF}
        StartAt := GetFurtherStartAt;
        EndAt := GetLineBottomCoord(nextnewline-1);
        Splitting := False;
        ProcessEndAt;
        if MultiItem then begin
          (DrawItems[oldi] as TRVMultiDrawItemInfo).PartsList.Clear;
          DoFormatting(nPages,rvpsProceeding);
        end;
      end;
      i := nextnewline;
      Result := i<>oldi;
    end;
    {................................................}
begin
  if ItemCount=0 then
    exit;
  RVStyle := GetRVStyle;
  IgnorePB := IgnorePageBreaks;
  GetSADForFormatting(nil, SaD);
  if Pages.Count=0 then begin
    rvpi := Pages.Add;
    rvpi.StartY := 0;
    rvpi.StartDrawItemNo := 0;
    StartY := 0;
    i := 0;
    StartAt := GetInitialStartAt;
    Splitting := False;
  end;
  nPages := Pages.Count;
  PageAdded := False;
  if Splitting then begin
    inc(Y, MaxHeight);
    dli := DrawItems[i];
    pi := GetRVStyle.ParaStyles[GetItemPara(dli.ItemNo)];
    if dli.SplitAt(y, SaD, Pages[nPages-1].StartDrawItemNo=i,
      TList(FootnoteRVDataList), MaxHeight, FootnotesChangeHeight, IgnorePageBreaks) then begin
      rvpi             := Pages.Add;
      rvpi.StartDrawItemNo := i;
      dli              := DrawItems[i];
      rvpi.StartY      := dli.Top{!} -RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD);
      rvpi.StartPart   := TRVMultiDrawItemInfo(dli).PartsList.Count;
      PageAdded := True;      
      {$IFNDEF RVDONOTUSESEQ}
      if not IgnoreFootnotes and (Self is TCustomMainPtblRVData) then begin
        Pages[nPages-1].FootnoteRVDataList := FootnoteRVDataList;
        CalcFootnoteCoords(FootnoteRVDataList, nPages);
      end;
      {$ENDIF}
      if Pages[nPages-1].StartDrawItemNo=i then begin
        Pages[nPages-1].StartY2 :=
          dli.Top+dli.Height-
           TRVMultiDrawItemInfo(dli).PartsList[TRVMultiDrawItemInfo(dli).PartsList.Count-2].Height-
           RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD);
        Pages[nPages-1].DocumentHeight :=
          TRVMultiDrawItemInfo(dli).PartsList[TRVMultiDrawItemInfo(dli).PartsList.Count-2].Height+
          RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD)+
          RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD)
        end
      else begin
        EndAt := dli.Top+
          TRVMultiDrawItemInfo(dli).PartsList[TRVMultiDrawItemInfo(dli).PartsList.Count-2].Height+
          RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD)+
          RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD);
        ProcessEndAt;
      end;
      DoFormatting(nPages, rvpsProceeding);
      StartAt := GetFurtherStartAt;
      StartY := 0;
      y := StartY{+MaxHeight}-StartAt+rvpi.StartY-dli.Top{!};
      dec(y, RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD));
      exit;
    end;
    if Pages[nPages-1].StartDrawItemNo=i then begin
      StartY := dli.Top+dli.Height-
        TRVMultiDrawItemInfo(dli).PartsList[TRVMultiDrawItemInfo(dli).PartsList.Count-1].Height-
        RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD);
      Pages[nPages-1].StartY2 := StartY;
    end;
    inc(i);
    Splitting := False;
  end;
  while i<DrawItems.Count do begin
    dli := DrawItems[i];
    pi := GetRVStyle.ParaStyles[GetItemPara(dli.ItemNo)];
    MustBreak := ((i>0) and (dli.ItemNo<>DrawItems[i-1].ItemNo) and
      (not IgnorePB and GetItem(dli.ItemNo).PageBreakBefore) and
      (Pages[nPages-1].StartDrawItemNo<>i));
    if (dli is TRVMultiDrawItemInfo) and dli.InitSplit(SaD, IgnorePB) then begin
      if Pages[nPages-1].StartDrawItemNo=i then begin
        Pages[nPages-1].StartPart := 1;
        Pages[nPages-1].StartY := dli.Top{!}-RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD);
      end;
      y := StartY+MaxHeight-StartAt-dli.Top;
      dec(y, RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD));
      if MustBreak or //(y<=0) or
         ((Pages[nPages-1].StartDrawItemNo<>i) and
         not dli.CanSplitFirst(y, SaD, Pages[nPages-1].StartDrawItemNo=i,
           FootnoteRVDataList<>nil, FootnotesChangeHeight, IgnorePB)) then begin
        { multipage item will not be placed on this page, it will start the next page }
        if not MustBreak and FindFirstItemOnPage(True, False, False, pi) then
          exit;
        rvpi := Pages.Add;
        rvpi.StartDrawItemNo := i;
        rvpi.StartY       := dli.Top{!}-RVStyle.GetAsDevicePixelsY(pi.SpaceBefore, @SaD);
        rvpi.StartPart    := 1;
        EndAt := GetLineBottomCoord(i-1);
        ProcessEndAt;
        DoFormatting(nPages, rvpsProceeding);
        StartY := 0;
        StartAt := GetFurtherStartAt;
        y := StartY{+MaxHeight}-StartAt+rvpi.StartY-dli.Top{!};
        dec(y, RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD));
        Splitting := True;
        PageAdded := True;        
        {$IFNDEF RVDONOTUSESEQ}
        if not IgnoreFootnotes and (Self is TCustomMainPtblRVData) then begin
          Pages[Pages.Count-2].FootnoteRVDataList := FootnoteRVDataList;
          CalcFootnoteCoords(FootnoteRVDataList, nPages);
        end;
        {$ENDIF}        
        exit;
      end;
      if FindFirstItemOnPage(True, False, True, pi) then
        exit;
      Splitting := True;
      dec(Y, MaxHeight);
      FormatNextPage(i, StartAt, StartY, Y, Splitting, MaxHeight,
        FootnoteRVDataList, FootnotesChangeHeight);
      exit;
    end;
    y := dli.Top+dli.Height+dli.ExtraSpaceBelow;
    if not MustBreak then begin
      inc(y, RVStyle.GetAsDevicePixelsY(pi.SpaceAfter, @SaD));
      {if (dli is TRVFloatingDrawLineInfo) then begin
        if (TRVFloatingDrawLineInfo(dli).FloatBottom>y) then
          y := TRVFloatingDrawLineInfo(dli).FloatBottom;
        //MustBreak := DrawItems.DoesFloatingExceedY(i, StartY+MaxHeight-StartAt, Items,
        //  GetRVStyle, @SaD);
      end;}
    end;
    if (y>StartY+MaxHeight-StartAt) or MustBreak then begin // i-th draw item does not fit in page, or mandatory break
      FindFirstItemOnPage(False, True, False, pi);
      break;
    end;
    {$IFNDEF RVDONOTUSESEQ}
    if not IgnoreFootnotes then begin
      if (GetItem(dli.ItemNo) is TRVFootnoteItemInfo) then begin
        FootnoteRVData := GetFootnoteRVData(TRVFootnoteItemInfo(GetItem(dli.ItemNo)));
        if (FootnoteRVDataList=nil) or
          (FootnoteRVDataList.GetFootnoteIndex(TRVFootnoteItemInfo(GetItem(dli.ItemNo)))<0) then begin
          if dli is TRVFootnoteDrawItem then begin
            TRVFootnoteDrawItem(dli).DocumentRVData := FootnoteRVData;
            FootnoteRVData.FootnoteItemRVData := Self;
            FootnoteRVData.FootnoteDItemNo := i;
          end;
          if FootnotesChangeHeight then
            dec(MaxHeight, FootnoteRVData.DocumentHeight);
          if FootnoteRVDataList=nil then begin
            FootnoteRVDataList := TRVFootnoteRefList.Create;
            if FootnotesChangeHeight then
              dec(MaxHeight, GetNoteSeparatorHeight);
          end;
          FootnoteRVDataList.Add(FootnoteRVData);
        end;
      end;
      if dli is TRVMultiDrawItemInfo then
        TRVMultiDrawItemInfo(dli).AddAllFootnotes(FootnoteRVDataList, MaxHeight,
          FootnotesChangeHeight);
      if y>StartY+MaxHeight-StartAt then begin // i-th draw item does not fit in page
        FindFirstItemOnPage(False, False, False, pi);
        break;
      end;
    end;
    {$ENDIF}
    inc(i);
  end;
  if not PageAdded then begin
    FindFirstItemOnPage(False, False, True, nil);  
    {$IFNDEF RVDONOTUSESEQ}
    if not IgnoreFootnotes and (Self is TCustomMainPtblRVData) then begin
      Pages[Pages.Count-1].FootnoteRVDataList := FootnoteRVDataList;
      CalcFootnoteCoords(FootnoteRVDataList, nPages);
    end;
    {$ENDIF}
    EndAt := GetLineBottomCoord(i-1);
    ProcessEndAt;
    SetEndAt(EndAt);
    IncEndAtByStartAt(nPages);
    DoFormatting(nPages,rvpsProceeding);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.CanPlaceFirstPageHere(var Height: Integer;
  ParentIsFirstItemOnPage: Boolean; const sad: TRVScreenAndDevice;
  ThisPageHasFootnotes, FootnotesChangeHeight: Boolean): Boolean;
var i, y, SpaceAfter: Integer;
    ParaStyle: TParaInfo;
    DrawItem: TRVDrawLineInfo;
    KeepAllTogether: Boolean;
    {........................................................}
    {$IFNDEF RVDONOTUSESEQ}
    procedure CheckFootnote(DItemNo: Integer);
    var FootnoteRVData: TRVFootnotePtblRVData;
    begin
      if IgnoreFootnotes then
        exit;
      if GetItem(DrawItems[DItemNo].ItemNo) is TRVFootnoteItemInfo then begin
        FootnoteRVData := GetFootnoteRVData(TRVFootnoteItemInfo(
          GetItem(DrawItems[DItemNo].ItemNo)));
        dec(Height, FootnoteRVData.DocumentHeight);
        if not ThisPageHasFootnotes then begin
          dec(Height, GetNoteSeparatorHeight);
          ThisPageHasFootnotes := True;
        end;
      end;
      if DrawItems[DItemNo] is TRVMultiDrawItemInfo then
        if TRVMultiDrawItemInfo(DrawItems[DItemNo]).InitSplit(sad, IgnorePageBreaks) then
          TRVMultiDrawItemInfo(DrawItem).PartsList.Clear
        else
          TRVMultiDrawItemInfo(DrawItems[DItemNo]).DecHeightByFootnotes(Height,
            ThisPageHasFootnotes);
    end;
    {$ENDIF}
    {........................................................}
    function CanSplit(DItemNo: Integer): Boolean;
    begin
      DrawItem := DrawItems[DItemNo];
      if DrawItem is TRVMultiDrawItemInfo and
         TRVMultiDrawItemInfo(DrawItem).InitSplit(sad, IgnorePageBreaks) then begin
        Result := TRVMultiDrawItemInfo(DrawItem).CanSplitFirst(
          Height-(DrawItem.Top+DrawItem.ExtraSpaceBelow+SpaceAfter), sad, False,
          ThisPageHasFootnotes, FootnotesChangeHeight, IgnorePageBreaks);
        TRVMultiDrawItemInfo(DrawItem).PartsList.Clear;
        end
      else
        Result := False;
    end;
    {........................................................}
begin
  Result := True;

  // if no items, we can always place this RVData
  if DrawItems.Count=0 then
    exit;

  if ParentIsFirstItemOnPage then begin
    // If parent table is at the beginning of the page, we return True if
    // this RVData has more than 1 line
    { Actually, this function is never called with this parameter = True,
      because the printing procedure always attempts to split multipart items
      starting page }
    for i := 1 to DrawItems.Count-1 do
      if DrawItems[i].FromNewLine then
        exit;
    Result := False;
    exit;
  end;

  // Returning True if there is enough space for the first line ...
  // ... or for the first paragraph, if "KeepLinesTogether" is set;
  // ... or for several paragraphs, if "KeepWithNext" is set.
  Result := False;

  KeepAllTogether := GetSourceRVDataForPrinting.KeepTogether;

  ParaStyle := GetRVStyle.ParaStyles[GetItemPara(0)];
  SpaceAfter := GetRVStyle.GetAsDevicePixelsY(ParaStyle.SpaceAfter, @sad);
  DrawItem := DrawItems[0];

  if CanSplit(0) then begin
    Result := True;
    exit;
  end;
  y := DrawItem.Top+DrawItem.Height+DrawItem.ExtraSpaceBelow+SpaceAfter;
  {$IFNDEF RVDONOTUSESEQ}
  if y>=Height then
    exit;
  if FootnotesChangeHeight then
    CheckFootnote(0);
  {$ENDIF}
  if y>=Height then
    exit;
  if (DrawItem is TRVFloatingDrawLineInfo) and
    (TRVFloatingDrawLineInfo(DrawItem).FloatBottom>=Height) then
    exit;

  for i := 1 to DrawItems.Count-1 do begin
    DrawItem := DrawItems[i];
    if DrawItem.FromNewLine then begin
      if IsDrawItemParaStart(i) then begin
        if not (KeepAllTogether or (rvpaoKeepWithNext in ParaStyle.Options)) or
           (not IgnorePageBreaks and PageBreaksBeforeItems[DrawItem.ItemNo]) then
          break;
        ParaStyle := GetRVStyle.ParaStyles[GetItemPara(DrawItem.ItemNo)];
        SpaceAfter := GetRVStyle.GetAsDevicePixelsY(ParaStyle.SpaceAfter, @sad);
        end
      else
        if not (KeepAllTogether or
                (rvpaoKeepLinesTogether in ParaStyle.Options) or
                (rvpaoKeepWithNext in ParaStyle.Options)) then
          break;
    end;
    if CanSplit(i) then
      break;
    y := DrawItem.Top+DrawItem.Height+DrawItem.ExtraSpaceBelow+SpaceAfter;
    {$IFNDEF RVDONOTUSESEQ}
    if y>=Height then
      exit;    
    if FootnotesChangeHeight then
      CheckFootnote(i);
    {$ENDIF}
    if y>=Height then
      exit;
    if (DrawItem is TRVFloatingDrawLineInfo) and
      (TRVFloatingDrawLineInfo(DrawItem).FloatBottom>=Height) then
      exit;
  end;
  Result := True;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
function TCustomMultiPagePtblRVData.SavePageAsRVF(Stream: TStream; PageNo: Integer;
  Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo): Boolean;
var SaD: TRVScreenAndDevice;
begin
  StreamSavePage := PageNo;
  try
    State := State + [rvstSavingPage];
    GetSADForFormatting(nil, SaD);
    Result := SaveRVFToStreamEx(Stream, rvfss_Page, Color, Background, Layout, True, @SaD)
  finally
    State := State - [rvstSavingPage];
  end;
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.SaveFromPageAsRVF(Stream: TStream;
  FirstPageNo: Integer; Color: TColor;
  Background: TRVBackground; Layout: TRVLayoutInfo): Boolean;
var SaD: TRVScreenAndDevice;
begin
  StreamSavePage := FirstPageNo;
  try
    State := State + [rvstSavingPage];
    GetSADForFormatting(nil, SaD);
    Result := SaveRVFToStreamEx(Stream, rvfss_FromPage, Color, Background, Layout, True, @SaD);
  finally
    State := State - [rvstSavingPage];
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.GetFirstItemOnPageEx(PageNo: Integer;
  var ItemNo, OffsetInItem, ExtraData: Integer);
var DItemNo, DItemOffs, Part: Integer;
begin
  DItemNo := Pages[PageNo-1].StartDrawItemNo;
  Part := Pages[PageNo-1].StartPart;
  if Part>0 then
    ExtraData := (DrawItems[DItemNo] as TRVMultiDrawItemInfo).PartsList[Part-1].
      GetSoftPageBreakInfo
  else
    ExtraData := -1;

  if GetItemStyle(DrawItems[DItemNo].ItemNo)<0 then
    DItemOffs := 0
  else
    DItemOffs := 1;

  DrawItem2Item(DItemNo, DItemOffs, ItemNo, OffsetInItem);
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.IsComplexSoftPageBreak(
  PageNo: Integer): Boolean;
var DItemNo, Part: Integer;
begin
  DItemNo := Pages[PageNo-1].StartDrawItemNo;
  Part := Pages[PageNo-1].StartPart;
  if Part>0 then
    Result := (DrawItems[DItemNo] as TRVMultiDrawItemInfo).PartsList[Part-1].
      IsComplexSoftPageBreak(TRVMultiDrawItemInfo(DrawItems[DItemNo]))
  else
    Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.AssignComplexSoftPageBreakToItem(
  PageNo: Integer; RVData: TCustomRVFormattedData);
var DItemNo, Part: Integer;
begin
  DItemNo := Pages[PageNo-1].StartDrawItemNo;
  Part := Pages[PageNo-1].StartPart;
  if Part>0 then
    (DrawItems[DItemNo] as TRVMultiDrawItemInfo).
      PartsList[Part-1].AssignSoftPageBreaksToItem(
        TRVMultiDrawItemInfo(DrawItems[DItemNo]),
        RVData.GetRVData.GetItem(DrawItems[DItemNo].ItemNo))
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.GetPageNo(RVData: TCustomRVData;
  ItemNo, OffsetInItem: Integer; PrintSourceRVData: TCustomRVData;
  var PageNo, LocalPageNo, LocalPageCount: Integer; var PtRVData: TCustomPrintableRVData): Boolean;
var i, PageIndex, StartItemNo, StartOffs, Part: Integer;
begin
  Result := False;
  // if RVData = main document
  if (PrintSourceRVData.GetRVData=RVData) or
     (PrintSourceRVData.GetSourceRVData=RVData) then begin
    if Pages=nil then begin
      PageNo := 1;
      LocalPageNo := PageNo;
      LocalPageCount := 1;
      PtRVData := Self;
      Result := True;
      exit;
    end;
    for PageIndex := Pages.Count-1 downto 0 do begin
      StartItemNo := DrawItems[Pages[PageIndex].StartDrawItemNo].ItemNo;
      if StartItemNo<ItemNo then begin
        PageNo := PageIndex+1; // this page
        LocalPageNo := PageNo;
        LocalPageCount := 1;
        PtRVData := Self;
        Result := True;
        exit;
      end;
      if StartItemNo=ItemNo then begin
        StartOffs := DrawItems[Pages[PageIndex].StartDrawItemNo].Offs;
        if StartOffs<=OffsetInItem then begin
          PageNo := PageIndex+1; // this page
          LocalPageNo := PageNo;
          LocalPageCount := 1;
          PtRVData := Self;
          Result := True;
          exit;
        end;
      end;
    end;
    exit; // <- actually, we should not be here
  end;
  // may be RVData is contained in subitems?
  for i := 0 to DrawItems.Count-1 do
    if DrawItems[i] is TRVMultiDrawItemInfo then begin
      Result := TRVMultiDrawItemInfo(DrawItems[i]).GetPartNo(RVData,
        ItemNo, OffsetInItem, Part, LocalPageNo, LocalPageCount, PtRVData);
      if Result then begin
        if Part<0 then
          Part := 0;
        if Pages<>nil then begin
          for PageIndex := 0 to Pages.Count-1 do begin
            if Pages[PageIndex].StartDrawItemNo=i then begin
              if TRVMultiDrawItemInfo(DrawItems[i]).PartsList.Count>0 then
                PageNo := PageIndex+Part+1-(Pages[PageIndex].StartPart-1) // prior page, or this page, or may be next, next next,...
              else
                PageNo := PageIndex+1; // this page
              exit;
            end;
            if Pages[PageIndex].StartDrawItemNo>i then begin
              PageNo := PageIndex; // prior page
              exit;
            end;
          end;
          PageNo := Pages.Count; // last page
          end
        else
          PageNo := 1;
        exit;
      end;
    end;
  Result := False;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TCustomMultiPagePtblRVData.GetAnchorObjectCoords(PtRVData: TCustomPrintableRVData;
  ItemNo, CurPageNo, PageNoInPtRVData: Integer;
  HorObject: TRVHorizontalAnchor; VertObject: TRVVerticalAnchor;
  RelativeToCell: Boolean; var RHorz, RVert: TRect; var FoundHere: Boolean): Boolean;

  {................................................................}
  // We cannot use Item2FirstDrawItem for printatble RVData
  function GetDrawItemNo(RVData: TCustomRVFormattedData; ItemNo: Integer): Integer;
  var i: Integer;
  begin
    for i := 0 to RVData.DrawItems.Count-1 do
      if RVData.DrawItems[i].ItemNo=ItemNo then begin
        Result := i;
        exit;
      end;
    Result := -1;
  end;
  {................................................................}
  procedure GetLineBounds(RVData: TCustomRVFormattedData; DrawItemNo: Integer;
    var DItemNo1, DItemNo2: Integer);
  begin
    DItemNo1 := DrawItemNo;
    while not RVData.DrawItems[DItemNo1].FromNewLine do
      dec(DItemNo1);
    DItemNo2 := DrawItemNo;
    while (DItemNo2+1<RVData.DrawItems.Count) and not RVData.DrawItems[DItemNo2+1].FromNewLine do
      inc(DItemNo2);
  end;
  {................................................................}
  procedure GetParaOnPageBounds(RVData: TCustomPrintableRVData; DrawItemNo, PageNo: Integer;
    var DItemNo1, DItemNo2: Integer);
  var MinDrawItemNo, MaxDrawItemNo, Part: Integer;
  begin
    RVData.GetDrawItemsRange(PageNo, MinDrawItemNo, MaxDrawItemNo, Part);

    DItemNo1 := DrawItemNo;
    while not RVData.IsDrawItemParaStart(DItemNo1) do
      dec(DItemNo1);
    DItemNo2 := DrawItemNo;
    while not RVData.IsDrawItemParaEnd(DItemNo2) do
      inc(DItemNo2);
    if DItemNo1<MinDrawItemNo then
      DItemNo1 := MinDrawItemNo;
    if DItemNo2>MaxDrawItemNo then
      DItemNo2 := MaxDrawItemNo;
  end;
  {................................................................}
  procedure GetTopBottom(RVData: TCustomRVFormattedData; DItemNo1, DItemNo2: Integer;
    var Top, Bottom: Integer);
  var i: Integer;
      DrawItem: TRVDrawLineInfo;
  begin
     DrawItem := RVData.DrawItems[DItemNo1];
     Top := DrawItem.Top-DrawItem.ExtraSpaceAbove;
     Bottom := DrawItem.Top+DrawItem.Height+DrawItem.ExtraSpaceBelow;
     for i := DItemNo1+1 to DItemNo2 do begin
       DrawItem := RVData.DrawItems[i];
       Top := Min(Top, DrawItem.Top-DrawItem.ExtraSpaceAbove);
       Bottom := Max(Bottom, DrawItem.Top+DrawItem.Height+DrawItem.ExtraSpaceBelow);
     end;
  end;
  {................................................................}

var i, DrawItemNo, DItemNo1, DItemNo2, Part, FirstPart, LocalPage: Integer;
    ParaStyle: TParaInfo;
    RVData: TCustomRVData;
    CurPtRVData: TCustomPrintableRVData;
    SaD: TRVScreenAndDevice;
    StoreSub : TRVStoreSubRVData;
    Item: TCustomRVItemInfo;
    InnerCoords, OuterCoords: TRect;
    OriginVert, OriginHorz: TPoint;
    OriginOffs: TSize;
begin
  Result := False;

  if Self=PtRVData then begin
    if (Pages=nil) and (PageNoInPtRVData<>1) then
      exit;
    if (Pages<>nil) and (PageNoInPtRVData>Pages.Count) then
      exit;
    DrawItemNo := GetDrawItemNo(PtRVData, ItemNo);
    if DrawItemNo<0 then
      exit;
    RVert.Left := PtRVData.DrawItems[DrawItemNo].Left;
    RVert.Right := RVert.Left + PtRVData.DrawItems[DrawItemNo].Width;
    case VertObject of
      rvvanLine:
        GetLineBounds(PtRVData, DrawItemNo, DItemNo1, DItemNo2);
      rvvanParagraph:
        GetParaOnPageBounds(PtRVData, DrawItemNo, PageNoInPtRVData, DItemNo1, DItemNo2);
      else
        begin
          DItemNo1 := DrawItemNo;
          DItemNo2 := DrawItemNo;
        end;
    end;
    GetTopBottom(PtRVData, DItemNo1, DItemNo2, RVert.Top, RVert.Bottom);
    if VertObject=rvvanParagraph then begin
      ParaStyle := PtRVData.GetRVStyle.ParaStyles[
        PtRVData.GetItemParaEx(PtRVData.DrawItems[DrawItemNo].ItemNo)];
      PtRVData.GetSADForFormatting(nil, SaD);
      dec(RVert.Top, PtRVData.GetRVStyle.GetAsDevicePixelsY(ParaStyle.SpaceBefore, @SaD));
      inc(RVert.Bottom, PtRVData.GetRVStyle.GetAsDevicePixelsY(ParaStyle.SpaceAfter, @SaD));
    end;
    if (Self is TCustomMainPtblRVData) or (Self is TRVHeaderFooterRVData) then begin
      // for main data, CurPageNo must be = PageNoInPtRVData and must be = global page no
      GetDrawItemsRange(CurPageNo, DItemNo1, DItemNo2, Part);
      OriginVert.X := GetPrintableAreaLeft(CurPageNo);
      if (Part<0) or (DrawItemNo=DItemNo1) then
        OriginVert.Y  := -GetTopCoord(CurPageNo, CurPageNo)
      else
        OriginVert.Y  := -GetTopCoord2(CurPageNo, CurPageNo);
      OffsetRect(RVert, OriginVert.X, OriginVert.Y);
      end
    else if (Self is TCustomMultiPagePtblRVData) and
      (TCustomMultiPagePtblRVData(Self).Pages<>nil) then begin
      GetDrawItemsRange(CurPageNo, DItemNo1, DItemNo2, Part);
      if (Part<0) or (DrawItemNo=DItemNo1) then
        OriginVert.Y  := -Pages[CurPageNo-1].StartY
      else
        OriginVert.Y  := -Pages[CurPageNo-1].StartY2;
      OffsetRect(RVert, 0, OriginVert.Y);
    end;
    if GetSourceRVDataForPrinting.GetRotation<>rvrotNone then
      RVert := Rect(0,0,0,0);
    RHorz := RVert;
    FoundHere := True;
    Result := True;
    exit
  end;
  // may be RVData is contained in subitems?
  GetDrawItemsRange(CurPageNo, DItemNo1, DItemNo2, Part); // the only place where we actually need CurPageNo parameter
  if DItemNo1<0 then
    exit;
  FirstPart := Part;
  for i := DItemNo1 to DItemNo2 do begin
    if DrawItems[i] is TRVMultiDrawItemInfo then begin
      if (i=DItemNo2) and (i<>DItemNo1) and (TRVMultiDrawItemInfo(DrawItems[i]).PartsList.Count>0) then
        Part := 0;
      StoreSub := nil;
      Item := GetItem(DrawItems[i].ItemNo);
      RVData := TCustomRVData(Item.GetSubRVData(StoreSub, rvdFirst));
      while RVData<>nil do begin
        CurPtRVData := TRVMultiDrawItemInfo(DrawItems[i]).GetPrintableRVDataFor(StoreSub);
        if (CurPtRVData<>nil) and (CurPtRVData is TCustomMultiPagePtblRVData) then begin
          LocalPage := TRVMultiDrawItemInfo(DrawItems[i]).GetLocalPageNoFor(StoreSub, Part);
          if LocalPage>=1 then
            Result := TCustomMultiPagePtblRVData(CurPtRVData).GetAnchorObjectCoords(
              PtRVData, ItemNo, LocalPage, PageNoInPtRVData,
              HorObject, VertObject, RelativeToCell, RHorz, RVert, FoundHere);
        end;
        if Result then begin
          TRVMultiDrawItemInfo(DrawItems[i]).GetSubDocCoords(StoreSub, Part,
            InnerCoords, OuterCoords, OriginHorz);
          OriginVert := OriginHorz;
          if FoundHere and RelativeToCell then begin
            if HorObject in [rvhanPage, rvhanMainTextArea, rvhanLeftMargin, rvhanRightMargin] then begin
              case HorObject of
                rvhanPage:
                  RHorz := OuterCoords;
                rvhanMainTextArea:
                  RHorz := InnerCoords;
                rvhanLeftMargin:
                  begin
                    RHorz := OuterCoords;
                    RHorz.Right := InnerCoords.Left;
                  end;
                rvhanRightMargin:
                  begin
                    RHorz := OuterCoords;
                    RHorz.Left := InnerCoords.Right
                  end;
              end;
              OriginHorz := Point(0,0);
            end;
            if VertObject in [rvvanPage, rvvanMainTextArea, rvvanTopMargin, rvvanBottomMargin] then begin
              case VertObject of
                rvvanPage:
                  RVert := OuterCoords;
                rvvanMainTextArea:
                  RVert := InnerCoords;
                rvvanTopMargin:
                  begin
                    RVert := OuterCoords;
                    RVert.Bottom := InnerCoords.Top;
                  end;
                rvvanBottomMargin:
                  begin
                    RVert := OuterCoords;
                    RVert.Top := InnerCoords.Bottom;
                  end;
              end;
              OriginVert := Point(0,0);
            end;
          end;
          OriginOffs.cx := DrawItems[i].ObjectLeft;
          OriginOffs.cy := DrawItems[i].ObjectTop;
          if (Self is TCustomMainPtblRVData) or (Self is TRVHeaderFooterRVData)  then begin
            // for main data, CurPageNo must be = PageNoInPtRVData and must be = global page no
            inc(OriginOffs.cx, GetPrintableAreaLeft(CurPageNo));
            if (FirstPart<0) or (i=DItemNo1) then
              inc(OriginOffs.cy, -GetTopCoord(CurPageNo, CurPageNo))
            else
              inc(OriginOffs.cy, -GetTopCoord2(CurPageNo, CurPageNo))
            end
          else if (Self is TCustomMultiPagePtblRVData) and
            (TCustomMultiPagePtblRVData(Self).Pages<>nil) then begin
            if (FirstPart<0) or (i=DItemNo1) then
              inc(OriginOffs.cy, -Pages[CurPageNo-1].StartY)
            else
              inc(OriginOffs.cy, -Pages[CurPageNo-1].StartY2);
          end;
          OffsetRect(RHorz, OriginHorz.X+OriginOffs.cx, OriginHorz.Y+OriginOffs.cy);
          OffsetRect(RVert, OriginVert.X+OriginOffs.cx, OriginVert.Y+OriginOffs.cy);
          FoundHere := False;
          break;
        end;
        RVData := TCustomRVData(Item.GetSubRVData(StoreSub, rvdNext));
      end;
      StoreSub.Free;
      if Result then
        exit;
    end;
    Part := -1;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.RVFGetLimits(SaveScope: TRVFSaveScope;
  var StartItem, EndItem, StartOffs, EndOffs: Integer;
  var StartPart, EndPart: TRVMultiDrawItemPart;
  var SelectedItem: TCustomRVItemInfo);
var StartPartIndex, EndPartIndex: Integer;
begin
  if SaveScope in [rvfss_Page, rvfss_FromPage] then begin
    SelectedItem := nil;
    StartPartIndex := -1;
    EndPartIndex   := -1;
    GetDrawItemsRange(StreamSavePage, StartItem, EndItem, StartPartIndex);
    StartOffs := GetOffsBeforeDrawItem(StartItem);
    if StartPartIndex<0 then
      StartPart := nil
    else
      StartPart := (DrawItems[StartItem] as TRVMultiDrawItemInfo).PartsList[StartPartIndex];
    if SaveScope=rvfss_FromPage then begin
      EndItem := DrawItems.Count-1;
      EndOffs   := GetOffsAfterDrawItem(EndItem);
      EndPart := nil;
      end
    else begin
      EndOffs   := GetOffsAfterDrawItem(EndItem);
      if (EndItem>StartItem) and
        (StreamSavePage<Pages.Count) and
        (Pages[StreamSavePage].StartPart>1) then
        EndPartIndex := 0;
      if EndPartIndex<0 then
        EndPart := nil
      else
        EndPart := (DrawItems[EndItem] as TRVMultiDrawItemInfo).PartsList[EndPartIndex];
    end;
    DrawItem2Item(StartItem, StartOffs, StartItem, StartOffs);
    DrawItem2Item(EndItem, EndOffs, EndItem, EndOffs);
    end
  else
    inherited RVFGetLimits(SaveScope, StartItem, EndItem, StartOffs, EndOffs,
      StartPart, EndPart, SelectedItem);
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.PageExists(PageNo: Integer): Boolean;
begin
  if Pages=nil then
    Result := PageNo<=1
  else
    Result := PageNo<=Pages.Count;
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.AllowEmptyFirstPage: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TCustomMultiPagePtblRVData.GetFootnoteRVData(
  Footnote: TRVFootnoteItemInfo): TRVFootnotePtblRVData;
begin
  Result := TCustomPrintableRV((GetAbsoluteRootData as TCustomMainPtblRVData).
    FRichView).GetFootnoteRVData(Footnote);
end;
{------------------------------------------------------------------------------}
function TCustomMultiPagePtblRVData.GetNoteSeparatorHeight: Integer;
begin
  Result := TCustomPrintableRV((GetAbsoluteRootData as TCustomMainPtblRVData).
    FRichView).NoteSeparatorHeight;
end;
{------------------------------------------------------------------------------}
procedure TCustomMultiPagePtblRVData.DrawSidenotes(Canvas: TCanvas; PageNo:Integer;
  Preview: Boolean; PositionInText: TRVFloatPositionInText);
    {.................................................}
    function CanPrintSidenote(SD: TRVSidenotePtblRVData;
      MinItemNo, MaxItemNo: Integer): Boolean;
    var ItemNo: Integer;
    begin
      ItemNo := (SD.SidenoteItemRVData as TCustomPrintableRVData).GetSourceRVDataForPrinting.
        GetAbsoluteRootItemNo(SD.SidenoteItemNo);
      Result := (ItemNo>=MinItemNo) and (ItemNo<=MaxItemNo);
    end;

var i, MinItemNo, MaxItemNo: Integer;
   SD: TRVSidenotePtblRVData;
   BackRect, BorderRect: TRect;
   sad: TRVScreenAndDevice;
   SidenoteList: TRVSidenoteList;
   Rgn: TRVHandle;
   RgnValid: Boolean;
begin
  SidenoteList := GetSidenoteList(PageNo);
  if SidenoteList=nil then
    exit;
  GetPrintableItemsRange(MinItemNo, MaxItemNo);
  GetSADForFormatting(Canvas, sad);
  for i := 0 to SidenoteList.Count-1 do begin
    SD := SidenoteList[i];
    if (SD.Sidenote.BoxPosition.PositionInText=PositionInText) and
      CanPrintSidenote(SD, MinItemNo, MaxItemNo) then begin
      BackRect := SD.FullRect;
      SD.Sidenote.BoxProperties.Background.PrepareDrawSaD(BackRect, SD.GetRVStyle, sad);
      SD.Sidenote.BoxProperties.Background.Draw(BackRect, Canvas, True, GetColorMode,
        SD.GetRVStyle.GraphicInterface);
      SD.GetRVStyle.GraphicInterface.IntersectClipRectEx(Canvas,
        BackRect.Left, BackRect.Top, BackRect.Right, BackRect.Bottom, Rgn, RgnValid);
      try
        SD.DrawPage(1, PageNo, Canvas, Preview,
          TRVPrint(TPrintableRV(FRichView).RVPrint).PreviewCorrection);
      finally
        SD.GetRVStyle.GraphicInterface.RestoreClipRgn(Canvas, Rgn, RgnValid);
      end;
      BorderRect := SD.FullRect;
      SD.Sidenote.BoxProperties.Border.DrawSaD(BorderRect, Canvas, SD.GetRVStyle, sad,
        GetColorMode, SD.GetRVStyle.GraphicInterface);
    end;
  end;
end;

function TCustomMultiPagePtblRVData.GetSidenoteList(PageNo: Integer): TRVSidenoteList;
begin
  if Pages<>nil then
    Result := Pages[PageNo-1].Sidenotes
  else
    Result := nil;
end;
{$ENDIF}
{============================ TCustomMainPtblRVData ===========================}
constructor TCustomMainPtblRVData.Create(RichView: TRVScroller);
begin
  inherited;
  Pages := TRVPageCollection.Create;
  ColorMode := rvcmPrinterColor;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetSourceRVDataForPrinting: TCustomRVData;
begin
  Result := TCustomRichView(RichView).RVData;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetInitialStartAt: Integer;
begin
  Result := TPrintableRV(FRichView).RVPrint.StartAt;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.SetEndAt(Value: Integer);
begin
  TPrintableRV(FRichView).RVPrint.EndAt := Value;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.GetPrintableItemsRange(var MinItemNo, MaxItemNo: Integer);
begin
  MinItemNo := TPrintableRV(FRichView).RVPrint.MinPrintedItemNo;
  MaxItemNo := TPrintableRV(FRichView).RVPrint.MaxPrintedItemNo;
  if MaxItemNo<0 then
    MaxItemNo := ItemCount-1;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.IgnorePageBreaks: Boolean;
begin
  Result := TPrintableRV(FRichView).RVPrint.IgnorePageBreaks;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
procedure TCustomMainPtblRVData.DoPagePrepaint(Canvas: TCanvas; GlobalPageNo: Integer;
  Preview, Correction, InRange: Boolean);
begin
  if GlobalPageNo<=Pages.Count then
    DrawSidenotes(Canvas, GlobalPageNo, Preview, rvpitBelowText);
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.DoPagePostpaint(Canvas: TCanvas; PageNo:Integer;
  Preview, InRange: Boolean);

    {.................................................}
    function CanPrintFootnote(FD: TRVFootnotePtblRVData;
      MinItemNo, MaxItemNo: Integer): Boolean;
    var ItemNo: Integer;
    begin
      ItemNo := (FD.FootnoteItemRVData as TCustomPrintableRVData).GetSourceRVDataForPrinting.
        GetAbsoluteRootItemNo(FD.FootnoteItemRVData.DrawItems[FD.FootnoteDItemNo].ItemNo);
      Result := (ItemNo>=MinItemNo) and (ItemNo<=MaxItemNo);
    end;
    {.................................................}
    procedure PrintFootnotes;
    var i, MinItemNo, MaxItemNo: Integer;
      FD: TRVFootnotePtblRVData;
    begin
      if Pages[PageNo-1].FootnoteRVDataList=nil then
        exit;
      GetPrintableItemsRange(MinItemNo, MaxItemNo);
      for i := 0 to Pages[PageNo-1].FootnoteRVDataList.Count-1 do begin
        FD := Pages[PageNo-1].FootnoteRVDataList[i];
        if CanPrintFootnote(FD, MinItemNo, MaxItemNo) then
          FD.DrawPage(1, PageNo, Canvas, Preview,
            TRVPrint(TPrintableRV(FRichView).RVPrint).PreviewCorrection);
      end;
      FD := Pages[PageNo-1].FootnoteRVDataList[0];
      if CanPrintFootnote(FD, MinItemNo, MaxItemNo) then
        TPrintableRV(FRichView).DrawNoteSeparatorAbove(PageNo, FD.Top, Canvas, False);
    end;
    {.................................................}

begin
  if PageNo>Pages.Count then
    exit;
  PrintFootnotes;
  DrawSidenotes(Canvas, PageNo, Preview, rvpitAboveText);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.IncEndAtByStartAt(PageNo: Integer);
begin
  if PageNo=1 then
    inc(TPrintableRV(FRichView).RVPrint.EndAt,
      TPrintableRV(FRichView).RVPrint.StartAt);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSELISTS}
function TCustomMainPtblRVData.GetPrevMarkers: TRVMarkerList;
begin
  if FPrevMarkers=nil then
    FPrevMarkers := TRVMarkerList.Create;
  Result := FPrevMarkers;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.Clear;
{$IFNDEF RVDONOTUSELISTS}
var i: Integer;
{$ENDIF}
begin
  inherited;
  {$IFNDEF RVDONOTUSELISTS}
  if FPrevMarkers<>nil then begin
    for i := 0 to FPrevMarkers.Count-1 do
      TRVMarkerItemInfo(FPrevMarkers.Items[i]).Free;
    RVFreeAndNil(FPrevMarkers);
  end;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetFirstItemMarker(var ListNo,
  Level: Integer): Boolean;
begin
  if (rvstFirstParaAborted in State) then begin
    ListNo := FFirstParaListNo;
    Level  := FFirstParaLevel;
    Result := ListNo>=0;
    end
  else
    Result := inherited GetFirstItemMarker(ListNo, Level);
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetColor: TColor;
begin
  if Transparent then
    Result := clNone
  else
    Result := inherited GetColor;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetPageHeight: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetPageWidth: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.DrawPage(PageNo, GlobalPageNo: Integer; Canvas: TCanvas;
  Preview, Correction: Boolean);
begin
  if PageNo>0 then
    inherited
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.InitFormatPages;
begin
   DoFormatting(0, rvpsStarting);
   PrinterCanvas := InitPrinterCanvas;
   FIsDestinationReady := PrinterCanvas<>nil;
   if FIsDestinationReady then begin
     Prepare;
     TCustomRichView(FRichView).MaxTextWidth :=
       RV_XToScreen(FRichView.Width, PrnSaD)-GetLeftMargin-GetRightMargin;
     Format_(False, True, False, 0, PrinterCanvas, False, False, False, False, False);
     end
   else
     ClearTemporal;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.FormatPages: Integer;
var i, StartAt, StartY, y, PageNo: Integer;
    Splitting: Boolean;
    MaxHeight: Integer;
    FootnoteRVDataList: TRVFootnoteRefList;
begin
   Pages.Clear;
   Result := 0;
   if DrawItems.Count = 0 then exit;
   DoFormatting(0, rvpsProceeding);
   i := 0;
   while i<DrawItems.Count do begin
     FootnoteRVDataList := nil;
     PageNo := Pages.Count;
     if PageNo<1 then
       PageNo := 1;
     MaxHeight := GetContentHeightForPage(PageNo) - (TmpTMPix+TmpBMPix);
     FormatNextPage(i, StartAt, StartY, Y, Splitting, MaxHeight,
       FootnoteRVDataList, True);
   end;
   Result := Pages.Count;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.FinalizeFormatPages;
begin
  DonePrinterCanvas(PrinterCanvas);
  PrinterCanvas := nil;
  DoFormatting(Pages.Count, rvpsFinished);
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetCanvas: TCanvas;
begin
  Result := PrinterCanvas;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.DoFormatting(PageCompleted: Integer; Step: TRVPrintingStep);
begin
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetPrintableAreaLeft(PageNo: Integer): Integer;
begin
  Result := GetTmpMForPage(PageNo).Left;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetPrintableAreaTop(PageNo: Integer): Integer;
begin
  Result := GetTmpMForPage(PageNo).Top;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetBack: TRVBackground;
begin
  Result := TCustomRichView(FRichView).Background;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetTmpM(HFType: TRVHFType): PRect;
begin
  case HFType of
    rvhftNormal:    Result := @TmpMNormal;
    rvhftFirstPage: Result := @TmpMFirstPage;
    rvhftEvenPages: Result := @TmpMEvenPages;
    else            Result := nil;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetTmpMForPage(PageNo: Integer): PRect;
begin
  Result := @TmpMNormal;
  if TPrintableRV(FRichView).FFacingPages and (PageNo mod 2 = 0) then
    Result := @TmpMEvenPages;
  if TPrintableRV(FRichView).FTitlePage and (PageNo=1) then
    Result := @TmpMFirstPage;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetContentHeightForPage(PageNo: Integer): Integer;
begin
  Result := ContentHeightNormal;
  if TPrintableRV(FRichView).FFacingPages and (PageNo mod 2 = 0) then
    Result := ContentHeightEvenPages;
  if TPrintableRV(FRichView).FTitlePage and (PageNo=1) then
    Result := ContentHeightFirstPage;
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetTopCoord(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := Pages[PageNo-1].StartY-GetTmpMForPage(GlobalPageNo).Top-TmpTMPix;
  if PageNo=1 then
    dec(Result,TPrintableRV(FRichView).RVPrint.StartAt);
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := Pages[PageNo-1].StartY2-GetTmpMForPage(GlobalPageNo).Top-TmpTMPix;
  if PageNo=1 then
    dec(Result,TPrintableRV(FRichView).RVPrint.StartAt);
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.GetSADForFormatting(Canvas: TCanvas;
  var sad: TRVScreenAndDevice);
begin
  sad := PrnSaD;
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.CheckPageNo(PageNo: Integer);
begin
  if (PageNo<1) or (PageNo>Pages.Count) then
    raise ERichViewError.Create(errInvalidPageNo);
end;
{------------------------------------------------------------------------------}
procedure TCustomMainPtblRVData.Prepare;
begin
  RV_InfoAboutSaD(PrnSaD, PrinterCanvas, GetRVStyle.GraphicInterface);
  if RichViewPixelsPerInch<>0 then begin
    PrnSaD.ppixScreen := RichViewPixelsPerInch;
    PrnSaD.ppiyScreen := RichViewPixelsPerInch;
  end;
  GetSADForFormatting(PrinterCanvas, PrnSad);
  TmpTMPix := RV_YToDevice(FTopMarginPix, PrnSaD);
  TmpBMPix := RV_YToDevice(FBottomMarginPix, PrnSaD);
end;
{------------------------------------------------------------------------------}
function TCustomMainPtblRVData.GetColorMode: TRVColorMode;
begin
  Result := ColorMode;
end;
{============================== TPrintableRVData ==============================}
constructor TPrintableRVData.Create(RichView: TRVScroller);
begin
  inherited Create(RichView);
  HeaderY := 10.0;
  FooterY := 10.0;
end;
{------------------------------------------------------------------------------}
destructor TPrintableRVData.Destroy;
begin
  Footer.Free;
  Header.Free;
  FirstPageHeader.Free;
  FirstPageFooter.Free;
  EvenPagesHeader.Free;
  EvenPagesFooter.Free;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetSourceRVDataForPrintingEx: TCustomRVData;
begin
  Result := TPrintableRV(RichView).RVPrint.GetSourceRichView.RVData;
end;
{------------------------------------------------------------------------------}
procedure TPrintableRVData.DoPagePrepaint(Canvas: TCanvas; GlobalPageNo:Integer;
  Preview, Correction, InRange: Boolean);
var r: TRect;
    PTmpM: PRect;
    HF: TRVHeaderFooterRVData;
begin
  PTmpM := GetTmpMForPage(GlobalPageNo);
  r := Bounds(GetPrintableAreaLeft(GlobalPageNo), GetPrintableAreaTop(GlobalPageNo),
    GetWidth,  GetContentHeightForPage(GlobalPageNo));
  if Assigned(TRVPrint(TPrintableRV(FRichView).RVPrint).OnPagePrepaint) then
    TRVPrint(TPrintableRV(FRichView).RVPrint).OnPagePrepaint(
      TRVPrint(TPrintableRV(FRichView).RVPrint), GlobalPageNo, Canvas, Preview,
        Rect(0, 0, PTmpM.Left+PTmpM.Right+GetWidth,
          PTmpM.Top+PTmpM.Bottom+GetContentHeightForPage(GlobalPageNo)),
        r
      );

  if InRange then begin
    HF := GetHeaderForPage(GlobalPageNo);
    if HF<>nil then begin
      HF.Left := GetPrintableAreaLeft(GlobalPageNo);
      {$IFNDEF RVDONOTUSESEQ}
      HF.DrawSidenotes(Canvas, 1, Preview, rvpitBelowText);
      {$ENDIF}
      HF.DrawPage(1, GlobalPageNo, Canvas, Preview, Correction);
      {$IFNDEF RVDONOTUSESEQ}
      HF.DrawSidenotes(Canvas, 1, Preview, rvpitAboveText);
      {$ENDIF}
    end;
    HF := GetFooterForPage(GlobalPageNo);
    if HF<>nil then begin
      HF.Left := GetPrintableAreaLeft(GlobalPageNo);
      {$IFNDEF RVDONOTUSESEQ}
      HF.DrawSidenotes(Canvas, 1, Preview, rvpitBelowText);
      {$ENDIF}
      HF.DrawPage(1, GlobalPageNo, Canvas, Preview, Correction);
      {$IFNDEF RVDONOTUSESEQ}
      HF.DrawSidenotes(Canvas, 1, Preview, rvpitAboveText);
      {$ENDIF}
    end;
  end;

  inherited;
  if TRVPrint(TPrintableRV(FRichView).RVPrint).ClipMargins then
    TPrintableRV(FRichView).Style.GraphicInterface.IntersectClipRectEx(Canvas,
      r.Left, r.Top, r.Right, r.Bottom, Rgn, RgnValid);
end;
{------------------------------------------------------------------------------}
procedure TPrintableRVData.DoPagePostpaint(Canvas: TCanvas; GlobalPageNo:Integer;
  Preview, InRange: Boolean);
var r: TRect;
    PTmpM: PRect;
begin
  if TRVPrint(TPrintableRV(FRichView).RVPrint).ClipMargins then
    TPrintableRV(FRichView).Style.GraphicInterface.RestoreClipRgn(Canvas, Rgn, RgnValid);
  try
    inherited;
  finally
    if InRange then begin
      PTmpM := GetTmpMForPage(GlobalPageNo);
      r := Bounds(GetPrintableAreaLeft(GlobalPageNo), GetPrintableAreaTop(GlobalPageNo),
        GetWidth, GetContentHeightForPage(GlobalPageNo));
      if Assigned(TRVPrint(TPrintableRV(FRichView).RVPrint).OnPagePostpaint) then
        TRVPrint(TPrintableRV(FRichView).RVPrint).OnPagePostpaint(
          TRVPrint(TPrintableRV(FRichView).RVPrint), GlobalPageNo, Canvas, Preview,
            Rect(0, 0, PTmpM.Left+PTmpM.Right+GetWidth,
              PTmpM.Top+PTmpM.Bottom+GetContentHeightForPage(GlobalPageNo)),
            r
          );
    end;
  end;
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.InitPrinterCanvas: TCanvas;
begin
  if GetRVStyle=nil then
    Result := nil
  else
    Result := GetRVStyle.GraphicInterface.CreatePrinterCanvas;
end;
{------------------------------------------------------------------------------}
procedure TPrintableRVData.DonePrinterCanvas(Canvas: TCanvas);
begin
  if GetRVStyle<>nil then
    GetRVStyle.GraphicInterface.DestroyCompatibleCanvas(Canvas);
end;
{------------------------------------------------------------------------------}
procedure TPrintableRVData.DoFormatting(PageCompleted: Integer;
  Step: TRVPrintingStep);
begin
  if Assigned(TRVPrint(TPrintableRV(FRichView).RVPrint).OnFormatting) then
    TRVPrint(TPrintableRV(FRichView).RVPrint).OnFormatting(TPrintableRV(FRichView), PageCompleted, Step);
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetPrintableAreaLeft(PageNo: Integer): Integer;
begin
  if not TPrintableRV(FRichView).FMirrorMargins or ((PageNo mod 2)=1) then
    Result := GetTmpMForPage(PageNo).Left
  else
    Result := TmpLMMir;
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetFooterForPage(
  PageNo: Integer): TRVHeaderFooterRVData;
begin
  Result := Footer;
  if TPrintableRV(FRichView).FFacingPages and (PageNo mod 2 = 0) then
    Result := EvenPagesFooter;
  if TPrintableRV(FRichView).FTitlePage and (PageNo=1) then
    Result := FirstPageFooter;
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetHeaderForPage(
  PageNo: Integer): TRVHeaderFooterRVData;
begin
  Result := Header;
  if TPrintableRV(FRichView).FFacingPages and (PageNo mod 2 = 0) then
    Result := EvenPagesHeader;
  if TPrintableRV(FRichView).FTitlePage and (PageNo=1) then
    Result := FirstPageHeader;
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetHeaderOrFooter(HFType: TRVHFType; IsHeader: Boolean): TRVHeaderFooterRVData;
begin
  Result := nil;
  if IsHeader then
    case HFType of
      rvhftNormal:    Result := Header;
      rvhftFirstPage: Result := FirstPageHeader;
      rvhftEvenPages: Result := EvenPagesHeader;
    end
  else
    case HFType of
      rvhftNormal:    Result := Footer;
      rvhftFirstPage: Result := FirstPageFooter;
      rvhftEvenPages: Result := EvenPagesFooter;
    end
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetPageHeight: Integer;
begin
  Result := Printer.PageHeight;
end;
{------------------------------------------------------------------------------}
function TPrintableRVData.GetPageWidth: Integer;
begin
  Result := Printer.PageWidth;
end;
{------------------------------------------------------------------------------}
procedure TPrintableRVData.Prepare;
var lpy, lpx, footeroffs: Integer;
    GraphicInterface: TRVGraphicInterface;
    HFType: TRVHFType;
    HF: TRVHeaderFooterRVData;
    PTmpM: PRect;
begin
  inherited Prepare;


  PrinterCanvas.Font.PixelsPerInch := PrnSaD.ppiyDevice;

  GraphicInterface := GetRVStyle.GraphicInterface;

  lpy := GraphicInterface.GetDeviceCaps(PrinterCanvas, LOGPIXELSY);
  lpx := GraphicInterface.GetDeviceCaps(PrinterCanvas, LOGPIXELSX);

  PhysM.Left := GraphicInterface.GetDeviceCaps(PrinterCanvas, PHYSICALOFFSETX);
  PhysM.Top  := GraphicInterface.GetDeviceCaps(PrinterCanvas, PHYSICALOFFSETY);
  PhysM.Right  := GraphicInterface.GetDeviceCaps(PrinterCanvas, PHYSICALWIDTH) -(PhysM.Left+GetPageWidth);
  PhysM.Bottom := GraphicInterface.GetDeviceCaps(PrinterCanvas, PHYSICALHEIGHT)-(PhysM.Top+GetPageHeight);

  {
  if phW>phoX+GetPageWidth then
    phW := phoX+GetPageWidth;
  if phH>phoY+GetPageHeight then
    phH := phoY+GetPageHeight;
  }

  with TPrintableRV(FRichView) do begin
    TmpMNormal.Left   := RV_UnitsToPixels(FMargins.Left,   FUnits, lpx) - PhysM.Left;
    TmpMNormal.Top    := RV_UnitsToPixels(FMargins.Top,    FUnits, lpy) - PhysM.Top;
    TmpMNormal.Right  := RV_UnitsToPixels(FMargins.Right,  FUnits, lpx) - PhysM.Right;
    TmpMNormal.Bottom := RV_UnitsToPixels(FMargins.Bottom, FUnits, lpy) - PhysM.Bottom;
    TmpLMMir    := RV_UnitsToPixels(FMargins.Right,  FUnits, lpx) - PhysM.Left;
  end;

  if (TPrintableRV(FRichView).RVPrint as TRVPrint).FixMarginsMode=rvfmmAutoCorrect then begin
    if TmpMNormal.Left<0 then begin
      inc(TmpMNormal.Right, TmpMNormal.Left);
      TmpMNormal.Left := 0;
    end;
    if TmpMNormal.Top<0 then begin
      inc(TmpMNormal.Bottom, TmpMNormal.Top);
      TmpMNormal.Top := 0;
    end;
    if TmpMNormal.Right<0 then
      TmpMNormal.Right := 0;
    if TmpMNormal.Bottom<0 then
      TmpMNormal.Bottom := 0;
    if TmpLMMir<0 then
      TmpLMMir := 0;
  end;
  TmpMFirstPage := TmpMNormal;
  TmpMEvenPages := TmpMNormal;

  for HFType := Low(TRVHFType) to High(TRVHFType) do begin
    HF := GetHeaderOrFooter(HFType, True);
    PTmpM := GetTmpM(HFType);
    if HF<>nil then begin
      HF.Left := PTmpM.Left;
      HF.Width := GetPageWidth - (PTmpM.Left+PTmpM.Right);
      HF.Top := RV_UnitsToPixels(HeaderY, TPrintableRV(FRichView).FUnits, lpy)- PhysM.Top;
      if HF.Top<0 then
        HF.Top := 0;
      HF.Height := 1;
      HF.Format(True, False);
      if PTmpM.Top<HF.Top+HF.DocumentHeight then
        PTmpM.Top := HF.Top+HF.DocumentHeight;
    end;
  end;

  for HFType := Low(TRVHFType) to High(TRVHFType) do begin
    HF := GetHeaderOrFooter(HFType, False);
    PTmpM := GetTmpM(HFType);
    if HF<>nil then begin
      HF.Left := PTmpM.Left;
      HF.Width := GetPageWidth - (PTmpM.Left+PTmpM.Right);
      HF.Top := 0;
      HF.Height := 1;
      HF.Format(True, False);
      footeroffs := RV_UnitsToPixels(FooterY, TPrintableRV(FRichView).FUnits, lpy)-PhysM.Bottom+HF.DocumentHeight;
      HF.Top := GetPageHeight-footeroffs;
      if PTmpM.Bottom<footeroffs then
        PTmpM.Bottom := footeroffs;
    end;
  end;

  ContentHeightNormal := GetPageHeight - (TmpMNormal.Top+TmpMNormal.Bottom);
  ContentHeightFirstPage := GetPageHeight - (TmpMFirstPage.Top+TmpMFirstPage.Bottom);
  ContentHeightEvenPages := GetPageHeight - (TmpMEvenPages.Top+TmpMEvenPages.Bottom);

  State := State+[rvstSkipformatting];
  try

    FRichView.Width := GetPageWidth - (TmpMNormal.Left+TmpMNormal.Right);
    FRichView.Height:= ContentHeightNormal;
    TCustomRichView(FRichView).VScrollVisible := False;
    TCustomRichView(FRichView).HScrollVisible := False;
  finally
    State := State-[rvstSkipformatting];
  end;
  {$IFNDEF RVDONOTUSESEQ}
  for HFType := Low(TRVHFType) to High(TRVHFType) do begin
    HF := GetHeaderOrFooter(HFType, True);
    if HF<>nil then
      RVPrintFormatSidenotes(HF, Self, HFType);
    HF := GetHeaderOrFooter(HFType, False);
    if HF<>nil then
      RVPrintFormatSidenotes(HF, Self, HFType);
  end;
  {$ENDIF}
end;
{================================ TRectPtblRVData =============================}
constructor TRectPtblRVData.Create(RichView: TRVScroller;
  SourceDataForPrinting: TCustomRVData;
  ParentPrintData: TCustomPrintableRVData);
begin
  inherited Create(RichView);
  FSourceDataForPrinting := SourceDataForPrinting;
  FParentPrintData := ParentPrintData;
  ShareItemsFrom(SourceDataForPrinting);
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetPrintableAreaLeft(PageNo: Integer): Integer;
begin
  Result := Left+DX;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetPrintableAreaTop(PageNo: Integer): Integer;
begin
  Result := Top+DY;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetExtraOffsForBackground: Integer;
begin
  Result := VOffsInRotatedCell;
end;
{------------------------------------------------------------------------------}
procedure TRectPtblRVData.GetSADForFormatting(Canvas: TCanvas; var sad: TRVScreenAndDevice);
begin
  sad := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).PrnSaD;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetTopCoord(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := -(Top+DY);
  if Pages<>nil then
    inc(Result, inherited GetTopCoord(PageNo, GlobalPageNo));
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := -(Top+DY);
  if Pages<>nil then
    inc(Result, inherited GetTopCoord2(PageNo, GlobalPageNo));
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetWidth: Integer;
begin
  Result := Width;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetHeight: Integer;
begin
  Result := Height;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.InitPrinterCanvas: TCanvas;
begin
  Result := GetCanvas;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetColorMode: TRVColorMode;
begin
  Result := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).ColorMode;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetCanvas: TCanvas;
begin
  Result := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).PrinterCanvas;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetColor: TColor;
begin
  Result := FColor;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetRotation: TRVDocRotation;
begin
  Result := FSourceDataForPrinting.GetRotation;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetBottomMargin: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetLeftMargin: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetRightMargin: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetTopMargin: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TRectPtblRVData.DoOnHyperlink(RVData: TCustomRVData;
  ItemNo: Integer; const R: TRect);
begin
  FParentPrintData.DoOnHyperlink(RVData, ItemNo, R);
end;
{------------------------------------------------------------------------------}
procedure TRectPtblRVData.DoOnCheckpoint(RVData: TCustomRVData;
  ItemNo, X, Y: Integer);
begin
  FParentPrintData.DoOnCheckpoint(RVData, ItemNo, X, Y);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TRectPtblRVData.IgnoreFootnotes: Boolean;
begin
  Result := FParentPrintData.IgnoreFootnotes;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRectPtblRVData.NoMetafiles: Boolean;
begin
  if GetAbsoluteRootData is TCustomPrintableRVData then
    Result := TCustomPrintableRVData(GetAbsoluteRootData).NoMetafiles
  else
    Result := inherited NoMetafiles;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetSourceRVDataForPrinting: TCustomRVData;
begin
  Result := FSourceDataForPrinting;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetParentData: TCustomRVData;
begin
  Result := FParentPrintData; // TCustomRichView(FRichView).RVData;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetRootData: TCustomRVData;
begin
  Result := GetParentData.GetRootData;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetAbsoluteParentData: TCustomRVData;
begin
  Result := FParentPrintData;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetAbsoluteRootData: TCustomRVData;
begin
  Result := GetAbsoluteParentData.GetRootData;
end;
{------------------------------------------------------------------------------}
procedure TRectPtblRVData.DrawBackToBitmap(Left, Top: Integer;
  bmp: TBitmap; const sad: TRVScreenAndDevice; ItemBackgroundLayer: Integer;
  RelativeToItem, AllowBitmaps, ParentAllowBitmaps: Boolean; PageNo: Integer);
var r, r2: TRect;
    l,t: Integer;
    pi: TParaInfo;
    item: TCustomRVItemInfo;
begin
  if RelativeToItem then begin
    inc(Left, RV_XToScreen(FDrawItem.Left, sad));
    inc(Top, RV_YToScreen(FItemTop-GetPrintableAreaTop(PageNo), sad));
  end;
  item := GetItem(FDrawItem.ItemNo);
  pi := GetRVStyle.ParaStyles[item.ParaNo];
  r := Rect(0,0, bmp.Width, bmp.Height);
  if pi.Background.Color<>clNone then begin
    bmp.Canvas.Brush.Color := pi.Background.Color;
    GetRVStyle.GraphicInterface.FillRect(bmp.Canvas, r);
    end
  else if Transparent then begin
    if not ParentAllowBitmaps then
      AllowBitmaps := False;
    l := Left+
      RV_XToScreen(GetPrintableAreaLeft(PageNo)-
        FParentPrintData.GetPrintableAreaLeft({FParentPrintData.F}PageNo), sad);
    t := Top+
      RV_YToScreen(GetPrintableAreaTop(PageNo)+VOffsInRotatedCell-FParentPrintData.GetPrintableAreaTop(PageNo), sad);
    FParentPrintData.DrawBackToBitmap(l, t, bmp, sad, -1, False,
      AllowBitmaps, GetRotation=rvrotNone, PageNo);
    end
  else
    inherited;
  if ItemBackgroundLayer<>0 then begin
    r2 := Bounds(RV_XToScreen(FDrawItem.Left, sad)-Left,
      RV_YToScreen(FItemTop-GetPrintableAreaTop(PageNo), sad)-Top,
      RV_XToScreen(FDrawItem.Width,sad),
      RV_YToScreen(FDrawItem.Height,sad));
    GetItem(FDrawItem.ItemNo).DrawBackgroundForPrinting(bmp.Canvas, r, r2,
      GetColorMode, ItemBackgroundLayer, GetRVStyle.GraphicInterface, AllowBitmaps);
  end;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetMaxTextWidth: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetMinTextWidth: Integer;
begin
  Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TRectPtblRVData.CreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas);
begin

end;
{------------------------------------------------------------------------------}
procedure TRectPtblRVData.DestroyFontInfoCache(var Cache: TRVFontInfoCache);
begin
  if (Cache<>nil) and (Cache.Owner=Self) then begin
    if Cache=FFontInfoCache then
      FFontInfoCache := nil;
    RVFreeAndNil(Cache);
  end;
end;
{------------------------------------------------------------------------------}
function TRectPtblRVData.GetFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
      RVData: TCustomRVFormattedData): TRVFontInfoCache;
begin
  Result := FParentPrintData.GetFontInfoCache(ACanvas, AFormatCanvas, RVData);
end;
{========================= TRVHeaderFooterRVData ==============================}
constructor TRVHeaderFooterRVData.Create(RichView: TRVScroller;
  SourceDataForPrinting: TCustomRVData;
  ParentPrintData: TCustomPrintableRVData);
begin
  inherited;
  FColor := clNone;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
destructor TRVHeaderFooterRVData.Destroy;
begin
  RVFreeAndNil(Sidenotes);
  inherited Destroy;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVHeaderFooterRVData.CreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas);
begin
  FFontInfoCache.Free;
  FFontInfoCache := DoCreateFontInfoCache(ACanvas, AFormatCanvas, Self);
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
  RVData: TCustomRVFormattedData): TRVFontInfoCache;
begin
  Result := FFontInfoCache;
  if Result = nil then
    Result := DoCreateFontInfoCache(ACanvas, AFormatCanvas, RVData);
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetBottomMargin: Integer;
begin
  Result := FBottomMargin;
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetLeftMargin: Integer;
begin
  Result := FLeftMargin;
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetRightMargin: Integer;
begin
  Result := FRightMargin;
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetTopMargin: Integer;
begin
  Result := FTopMargin;
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetRVStyle: TRVStyle;
begin
  Result := FSourceDataForPrinting.GetRVStyle;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TRVHeaderFooterRVData.IgnoreFootnotes: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
function TRVHeaderFooterRVData.GetSidenoteList(PageNo: Integer): TRVSidenoteList;
begin
  if PageNo=1 then
    Result := Sidenotes
  else
    Result := nil;
end;
{$ENDIF}
{========================== TRVEndnotePtblRVData ==============================}
{$IFNDEF RVDONOTUSESEQ}
constructor TRVEndnotePtblRVData.Create(RichView: TRVScroller);
begin
  inherited Create(RichView);
  Pages := TRVPageCollection.Create;
  ParentDrawsBack := True;
end;
(*
procedure TRVEndnotePtblRVData.CreateFontInfoCache(ACanvas: TCanvas);
begin
  inherited;

end;
{------------------------------------------------------------------------------}
procedure TRVEndnotePtblRVData.DestroyFontInfoCache(
  var Cache: TRVFontInfoCache);
begin
  inherited;

end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetFontInfoCache(ACanvas: TCanvas;
  RVData: TCustomRVFormattedData): TRVFontInfoCache;
begin

end;
*)
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetParentData: TCustomRVData;
begin
  Result := TPrintableRV(FRichView).RVData;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetRootData: TCustomRVData;
begin
  Result := TPrintableRV(FRichView).RVData;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetAbsoluteParentData: TCustomRVData;
begin
  Result := TPrintableRV(FRichView).RVData;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetAbsoluteRootData: TCustomRVData;
begin
  Result := TPrintableRV(FRichView).RVData;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetSourceRVDataForPrinting: TCustomRVData;
begin
  Result := Endnote.Document;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetInitialStartAt: Integer;
begin
  Result := StartAt;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetFurtherStartAt: Integer;
begin
  Result := NextStartAt;
end;
{------------------------------------------------------------------------------}
procedure TRVEndnotePtblRVData.IncEndAtByStartAt(PageNo: Integer);
begin
  if PageNo=1 then
    inc(EndAt, StartAt)
  else
    inc(EndAt, NextStartAt)  
end;
{------------------------------------------------------------------------------}
procedure TRVEndnotePtblRVData.SetEndAt(Value: Integer);
begin
  EndAt := Value;
end;
{------------------------------------------------------------------------------}
procedure TRVEndnotePtblRVData.GetSADForFormatting(Canvas: TCanvas;
  var sad: TRVScreenAndDevice);
begin
  sad := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).PrnSaD;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetCanvas: TCanvas;
begin
  Result := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).PrinterCanvas;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetDocProperties: TStringList;
begin
  Result := nil;
end;
function TRVEndnotePtblRVData.GetTopCoord(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := Pages[PageNo-1].StartY-
    TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).GetTmpMForPage(GlobalPageNo).Top-
    TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).TmpTMPix;
  if PageNo=1 then
    dec(Result,StartAt)
  else
    dec(Result,NextStartAt)
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetTopCoord2(PageNo, GlobalPageNo: Integer): Integer;
begin
  Result := Pages[PageNo-1].StartY2-
    TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).GetTmpMForPage(GlobalPageNo).Top-
    TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).TmpTMPix;
  if PageNo=1 then
    dec(Result,StartAt)
  else
    dec(Result,NextStartAt)
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.AllowEmptyFirstPage: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetPrintableAreaLeft(
  PageNo: Integer): Integer;
begin
  Result := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).
    GetPrintableAreaLeft(PageNo);
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.GetPrintableAreaTop(PageNo: Integer): Integer;
begin
  Result := TCustomMainPtblRVData(TPrintableRV(FRichView).RVData).
    GetPrintableAreaTop(PageNo);
end;
{------------------------------------------------------------------------------}
function TRVEndnotePtblRVData.IgnoreFootnotes: Boolean;
begin
  Result := True;
end;
{======================= TRVFootnotePtblRVData ================================}
function TRVFootnotePtblRVData.IgnoreFootnotes: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TRVFootnotePtblRVData.AdjustFootnoteRefWidths;
var i: Integer;
begin
  // !!! not implemented: adjusting widths in tables
  for i := 0 to DrawItems.Count-1 do begin
    if GetItem(DrawItems[i].ItemNo) is TRVNoteReferenceItemInfo then
      ChangeDItemWidth(i, TRVNoteReferenceItemInfo(GetItem(DrawItems[i].ItemNo)).
        GetFinalPrintingWidth(GetCanvas, DrawItems[i], Self))
  end;
end;
{============================ TRVSidenotePtblRVData ===========================}
function TRVSidenotePtblRVData.IgnoreFootnotes: Boolean;
begin
  Result := True;
end;
{=========================== TRVFootnoteRefList ===============================}
procedure TRVFootnoteRefList.DeleteByFootnote(
  Footnote: TRVFootnoteItemInfo);
var i: Integer;
begin
  for i := Count-1 downto 0 do
    if Items[i].Footnote=Footnote then begin
      Delete(i);
      exit;
    end;
end;
{------------------------------------------------------------------------------}
function CompareFootnoteRVData(Item1, Item2: Pointer): Integer;
begin
  Result := TRVFootnotePtblRVData(Item1).Footnote.Counter-
    TRVFootnotePtblRVData(Item2).Footnote.Counter;
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteRefList.Sort;
begin
  inherited Sort(CompareFootnoteRVData);
end;
{------------------------------------------------------------------------------}
function TRVFootnoteRefList.GetFootnoteIndex(Footnote: TRVFootnoteItemInfo): Integer;
var i: Integer;
begin
  for i := Count-1 downto 0 do
    if Items[i].Footnote=Footnote then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;
{------------------------------------------------------------------------------}
function TRVFootnoteRefList.Get(Index: Integer): TRVFootnotePtblRVData;
begin
  Result := TRVFootnotePtblRVData(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteRefList.Put(Index: Integer;
  const Value: TRVFootnotePtblRVData);
begin
  inherited Put(Index, Value);
end;
{============================= TRVSidenoteList ================================}
function TRVSidenoteList.GetItems(Index: Integer): TRVSidenotePtblRVData;
begin
  Result := TRVSidenotePtblRVData(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteList.SetItems(Index: Integer;
  const Value: TRVSidenotePtblRVData);
begin
  inherited Put(Index, Value);
end;

{$ENDIF}


end.
