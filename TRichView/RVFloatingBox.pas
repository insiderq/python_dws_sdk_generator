{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       TRVBoxProperties - class defining size, border  }
{       and a background properties of                  }
{       a floating box (used in TRVSidenoteItemInfo)    }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVFloatingBox;

{$I RV_Defs.inc}

interface

{$IFNDEF RVDONOTUSESEQ}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF} Windows,
  Classes, SysUtils, Graphics, StdCtrls, RVTypes, RVStyle, CRVData, RVItem,
  RVNote, RVFuncs, Math;


const
  rveipcBoxWidth                    = 600;
  rveipcBoxHeight                   = 601;
  rveipcBoxWidthType                = 602;
  rveipcBoxHeightType               = 603;

  rveipcBoxBorderColor              = 604;
  rveipcBoxBorderWidth              = 605;
  rveipcBoxBorderInternalWidth      = 606;
  rveipcBoxBorderStyle              = 607;
  rveipcBoxBorderVisibleBorders_Left   = 608;
  rveipcBoxBorderVisibleBorders_Top    = 609;
  rveipcBoxBorderVisibleBorders_Right  = 610;
  rveipcBoxBorderVisibleBorders_Bottom = 611;
  rveipcBoxBorderBorderOffsets_Left     = 612;
  rveipcBoxBorderBorderOffsets_Top    = 613;
  rveipcBoxBorderBorderOffsets_Right  = 614;
  rveipcBoxBorderBorderOffsets_Bottom = 615;

  rveipcBoxColor            = 616;
  rveipcBoxPadding_Left     = 617;
  rveipcBoxPadding_Top      = 618;
  rveipcBoxPadding_Right    = 619;
  rveipcBoxPadding_Bottom   = 620;

  rveipcBoxVAlign               = 621;

const
  DEFAULT_WIDTH = 100;
  DEFAULT_HEIGHT = 100;
  DEFAULT_HPADDING = 10;
  DEFAULT_VPADDING = 5;

type
  TRVBoxWidthType = (rvbwtAbsolute, rvbwtRelPage, rvbwtRelMainTextArea,
    rvbwtRelLeftMargin, rvbwtRelRightMargin,
    rvbwtRelInnerMargin, rvbwtRelOuterMargin);
  TRVBoxHeightType = (rvbhtAbsolute, rvbhtRelPage, rvbhtRelMainTextArea,
    rvbhtRelTopMargin, rvbhtRelBottomMargin, rvbhtAuto);

  {
  Meaning of TRVFloatSize values depend on the corresponding position kind:
  rvbwtAbsolute or rvbhtAbsolute:
    value is like TRVStyleLength; it depends in RVStyle.Units - either pixels or twips
  *Rel* values:
    value is in 1/1000 of percent; for example, 50000 = 50%
  rvbhtAuto:
    not used
  }
  TRVFloatSize = type Integer;

  TRVBoxPaddingRect = class (TRVRect)
    public
      constructor CreateOwned(AOwner: TPersistent);
    published
      property Left default DEFAULT_HPADDING;
      property Right default DEFAULT_HPADDING;
      property Top default DEFAULT_VPADDING;
      property Bottom default DEFAULT_VPADDING;
  end;

  TRVBoxBorder = class (TRVBorder)
    protected
      function CreateBorderOffsets: TRVRect; override;
      function AllowNegativeOffsets: Boolean; override;
    public
      constructor Create(AOwner: TPersistent);
    published
      property Style default rvbSingle;
  end;

  TRVBoxBackgroundRect = class (TRVBackgroundRect)
    protected
      function CreateBorderOffsets: TRVRect; override;
      function AllowNegativeOffsets: Boolean; override;
    public
      constructor Create(AOwner: TPersistent);
    published
      property Color default clWindow;
  end;

  TRVBoxProperties = class (TPersistent)
    private
      FBorder: TRVBoxBorder;
      FBackground: TRVBoxBackgroundRect;
      FWidth, FHeight: TRVFloatSize;
      FWidthType: TRVBoxWidthType;
      FHeightType: TRVBoxHeightType;
      FVAlign: TTextLayout;
      procedure SetBorder(Value: TRVBoxBorder);
      function GetPropertyIdentifier(const PropName: TRVAnsiString): Integer;
      procedure SetBackground(const Value: TRVBoxBackgroundRect);
    public
      constructor Create;
      destructor Destroy; override;
      procedure Assign(Source: TPersistent); override;
      procedure ConvertToDifferentUnits(NewUnits: TRVStyleUnits; RVStyle: TRVStyle);
      function SetExtraIntPropertyEx(Prop: Integer; Value: Integer): Boolean;
      function GetExtraIntPropertyEx(Prop: Integer; var Value: Integer): Boolean;
      function SetExtraCustomProperty(const PropName: TRVAnsiString; const Value: String): Boolean;
      function GetExtraIntPropertyReformat(Prop: Integer; var ReformatType: TRVReformatType): Boolean;
      function GetChangedPropertyCount: Integer;
      procedure SaveRVFExtraProperties(Stream: TStream);
      function GetTotalLeftPadding(RVStyle: TRVStyle): Integer;
      function GetTotalRightPadding(RVStyle: TRVStyle): Integer;
      function GetTotalTopPadding(RVStyle: TRVStyle): Integer;
      function GetTotalBottomPadding(RVStyle: TRVStyle): Integer;
      function GetTotalLeftPaddingSaD(RVStyle: TRVStyle; PSaD: PRVScreenAndDevice): Integer;
      function GetTotalRightPaddingSaD(RVStyle: TRVStyle; PSaD: PRVScreenAndDevice): Integer;
      function GetTotalTopPaddingSaD(RVStyle: TRVStyle; PSaD: PRVScreenAndDevice): Integer;
      function GetTotalBottomPaddingSaD(RVStyle: TRVStyle; PSaD: PRVScreenAndDevice): Integer;
      function GetMinWidth(RVStyle: TRVStyle): Integer;
      function GetMinHeight(RVStyle: TRVStyle): Integer;
      function GetInternalRect(const ExternalRect: TRect; RVStyle: TRVStyle): TRect;
      property Border: TRVBoxBorder read FBorder write SetBorder;
      property Background: TRVBoxBackgroundRect read FBackground write SetBackground;
      property Width: TRVFloatSize read FWidth write FWidth default DEFAULT_WIDTH;
      property Height: TRVFloatSize read FHeight write FHeight default DEFAULT_HEIGHT;
      property WidthType: TRVBoxWidthType read FWidthType write FWidthType default rvbwtAbsolute;
      property HeightType: TRVBoxHeightType read FHeightType write FHeightType default rvbhtAuto;
      property VAlign: TTextLayout read FVAlign write FVAlign default tlTop;
  end;

{$ENDIF}


implementation

{$IFNDEF RVDONOTUSESEQ}

const
  rveipcBox_First = rveipcBoxWidth;
  rveipcBox_Last = rveipcBoxVAlign;


const PropertyNames: array[rveipcBox_First..rveipcBox_Last] of PRVAnsiChar =
  ('width', 'height', 'widthtype', 'heighttype',
   'bordercolor', 'borderwidth', 'borderiwidth', 'borderstyle',
   'vb_left', 'vb_top', 'vb_right', 'vb_bottom',
   'bo_left', 'bo_top', 'bo_right', 'bo_bottom',
    'color',
   'padding_left', 'padding_top', 'padding_right', 'padding_bottom',
   'contentvalign');
const PropertyPrefix = 'box';

function GetBoxPropName(Prop: Integer): TRVAnsiString;
begin
  Result := TRVAnsiString(PropertyPrefix)+PropertyNames[Prop];
end;
{============================ TRVBoxPaddingRect ===============================}
constructor TRVBoxPaddingRect.CreateOwned(AOwner: TPersistent);
begin
  inherited CreateOwned(AOwner);
  Left := DEFAULT_HPADDING;
  Right := DEFAULT_HPADDING;
  Top := DEFAULT_VPADDING;
  Bottom := DEFAULT_VPADDING;
end;
{=============================== TRVBoxBorder ================================}
constructor TRVBoxBorder.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  Style := rvbSingle;
end;
{------------------------------------------------------------------------------}
function TRVBoxBorder.CreateBorderOffsets: TRVRect;
begin
  Result := TRVBoxPaddingRect.CreateOwned(Self);
end;
{------------------------------------------------------------------------------}
function TRVBoxBorder.AllowNegativeOffsets: Boolean;
begin
  Result := False;
end;
{============================= TRVBoxBackgroundRect ===========================}
constructor TRVBoxBackgroundRect.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  Color := clWindow;
end;
{------------------------------------------------------------------------------}
function TRVBoxBackgroundRect.CreateBorderOffsets: TRVRect;
begin
  Result := TRVBoxPaddingRect.CreateOwned(Self);
end;
{------------------------------------------------------------------------------}
function TRVBoxBackgroundRect.AllowNegativeOffsets: Boolean;
begin
  Result := False;
end;
{============================ TRVFloatingBoxProperties =======================}
constructor TRVBoxProperties.Create;
begin
  inherited Create;
  FBorder := TRVBoxBorder.Create(Self);
  FBackground := TRVBoxBackgroundRect.Create(Self);
  Width := DEFAULT_WIDTH;
  Height := DEFAULT_HEIGHT;
  HeightType := rvbhtAuto;
end;
{------------------------------------------------------------------------------}
destructor TRVBoxProperties.Destroy;
begin
  RVFreeAndNil(FBorder);
  RVFreeAndNil(FBackground);
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TRVBoxProperties.Assign(Source: TPersistent);
begin
  if Source is TRVBoxProperties then begin
    Border := TRVBoxProperties(Source).Border;
    Background := TRVBoxProperties(Source).Background;
    Width := TRVBoxProperties(Source).Width;
    Height := TRVBoxProperties(Source).Height;
    WidthType := TRVBoxProperties(Source).WidthType;
    HeightType  := TRVBoxProperties(Source).HeightType;
    end
  else
    inherited Assign(Source);
end;
{------------------------------------------------------------------------------}
procedure TRVBoxProperties.SetBackground(const Value: TRVBoxBackgroundRect);
begin
  FBackground.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TRVBoxProperties.SetBorder(Value: TRVBoxBorder);
begin
  FBorder.Assign(Value);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetExtraIntPropertyEx(Prop: Integer;
  var Value: Integer): Boolean;
begin
  Result := (Prop>=rveipcBox_First) and (Prop<=rveipcBox_Last);
  if not Result then
    exit;
  case Prop of
    rveipcBoxWidth:
      Value := Width;
    rveipcBoxHeight:
      Value := Height;
    rveipcBoxVAlign:
      Value := ord(VAlign);
    rveipcBoxWidthType:
      Value := ord(WidthType);
    rveipcBoxHeightType:
      Value := ord(HeightType);
    rveipcBoxBorderColor:
      Value := ord(Border.Color);
    rveipcBoxBorderWidth:
      Value := Border.Width;
    rveipcBoxBorderInternalWidth:
      Value := Border.InternalWidth;
    rveipcBoxBorderStyle:
      Value := ord(Border.Style);
    rveipcBoxBorderVisibleBorders_Left:
      Value := ord(Border.VisibleBorders.Left);
    rveipcBoxBorderVisibleBorders_Top:
      Value := ord(Border.VisibleBorders.Top);
    rveipcBoxBorderVisibleBorders_Right:
      Value := ord(Border.VisibleBorders.Right);
    rveipcBoxBorderVisibleBorders_Bottom:
      Value := ord(Border.VisibleBorders.Bottom);
    rveipcBoxBorderBorderOffsets_Left:
      Value := ord(Border.BorderOffsets.Left);
    rveipcBoxBorderBorderOffsets_Top:
      Value := ord(Border.BorderOffsets.Top);
    rveipcBoxBorderBorderOffsets_Right:
      Value := ord(Border.BorderOffsets.Right);
    rveipcBoxBorderBorderOffsets_Bottom:
      Value := ord(Border.BorderOffsets.Bottom);
    rveipcBoxColor:
      Value := ord(Background.Color);
    rveipcBoxPadding_Left:
      Value := Background.BorderOffsets.Left;
    rveipcBoxPadding_Top:
      Value := Background.BorderOffsets.Top;
    rveipcBoxPadding_Right:
      Value := Background.BorderOffsets.Right;
    rveipcBoxPadding_Bottom:
      Value := Background.BorderOffsets.Bottom;
  end;
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.SetExtraIntPropertyEx(Prop, Value: Integer): Boolean;
begin
  Result := (Prop>=rveipcBox_First) and (Prop<=rveipcBox_Last);
  if not Result then
    exit;
  case Prop of
    rveipcBoxWidth:
      Width := Value;
    rveipcBoxHeight:
      Height := Value;
    rveipcBoxVAlign:
      VAlign := TTextLayout(Value);
    rveipcBoxWidthType:
      WidthType := TRVBoxWidthType(Value);
    rveipcBoxHeightType:
      HeightType := TRVBoxHeightType(Value);
    rveipcBoxBorderColor:
      Border.Color := TColor(Value);
    rveipcBoxBorderWidth:
      Border.Width := Value;
    rveipcBoxBorderInternalWidth:
      Border.InternalWidth := Value;
    rveipcBoxBorderStyle:
      Border.Style := TRVBorderStyle(Value);
    rveipcBoxBorderVisibleBorders_Left:
      Border.VisibleBorders.Left := Value<>0;
    rveipcBoxBorderVisibleBorders_Top:
      Border.VisibleBorders.Top := Value<>0;
    rveipcBoxBorderVisibleBorders_Right:
      Border.VisibleBorders.Right := Value<>0;
    rveipcBoxBorderVisibleBorders_Bottom:
      Border.VisibleBorders.Bottom := Value<>0;
    rveipcBoxBorderBorderOffsets_Left:
      Border.BorderOffsets.Left := Value;
    rveipcBoxBorderBorderOffsets_Top:
      Border.BorderOffsets.Top := Value;
    rveipcBoxBorderBorderOffsets_Right:
      Border.BorderOffsets.Right := Value;
    rveipcBoxBorderBorderOffsets_Bottom:
      Border.BorderOffsets.Bottom := Value;
    rveipcBoxColor:
      Background.Color := TColor(Value);
    rveipcBoxPadding_Left:
      Background.BorderOffsets.Left := Value;
    rveipcBoxPadding_Top:
      Background.BorderOffsets.Top := Value;
    rveipcBoxPadding_Right:
      Background.BorderOffsets.Right := Value;
    rveipcBoxPadding_Bottom:
      Background.BorderOffsets.Bottom := Value;
  end;
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetPropertyIdentifier(
  const PropName: TRVAnsiString): Integer;
var i: Integer;
begin
  for i := rveipcBox_First to rveipcBox_Last do
    if PropName=GetBoxPropName(i) then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalLeftPadding(RVStyle: TRVStyle): Integer;
begin
  Result := GetTotalLeftPaddingSaD(RVStyle, nil);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalRightPadding(RVStyle: TRVStyle): Integer;
begin
  Result := GetTotalRightPaddingSaD(RVStyle, nil);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalTopPadding(RVStyle: TRVStyle): Integer;
begin
  Result := GetTotalTopPaddingSaD(RVStyle, nil);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalBottomPadding(RVStyle: TRVStyle): Integer;
begin
  Result := GetTotalBottomPaddingSaD(RVStyle, nil);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalLeftPaddingSaD(RVStyle: TRVStyle;
  PSaD: PRVScreenAndDevice): Integer;
begin
  Result := Max(
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsX(Background.BorderOffsets.Left, PSaD)),
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsX(Border.BorderOffsets.Left, PSaD))+
    RVStyle.GetAsDevicePixelsX(Border.GetTotalWidth, PSaD));
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalRightPaddingSaD(RVStyle: TRVStyle;
  PSaD: PRVScreenAndDevice): Integer;
begin
  Result := Max(
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsX(Background.BorderOffsets.Right, PSaD)),
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsX(Border.BorderOffsets.Right, PSaD))+
    RVStyle.GetAsDevicePixelsX(Border.GetTotalWidth, PSaD));
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalTopPaddingSaD(RVStyle: TRVStyle;
  PSaD: PRVScreenAndDevice): Integer;
begin
  Result := Max(
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsY(Background.BorderOffsets.Top, PSaD)),
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsY(Border.BorderOffsets.Top, PSaD))+
    RVStyle.GetAsDevicePixelsY(Border.GetTotalWidth, PSaD));
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetTotalBottomPaddingSaD(RVStyle: TRVStyle;
  PSaD: PRVScreenAndDevice): Integer;
begin
  Result := Max(
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsY(Background.BorderOffsets.Bottom, PSaD)),
    RVGetPositiveValue(RVStyle.GetAsDevicePixelsY(Border.BorderOffsets.Bottom, PSaD))+
    RVStyle.GetAsDevicePixelsY(Border.GetTotalWidth, PSaD));
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetMinWidth(RVStyle: TRVStyle): Integer;
begin
  Result := GetTotalLeftPadding(RVStyle)+GetTotalRightPadding(RVStyle);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetMinHeight(RVStyle: TRVStyle): Integer;
begin
  Result := GetTotalTopPadding(RVStyle)+GetTotalBottomPadding(RVStyle);
end;
{------------------------------------------------------------------------------}
function  TRVBoxProperties.GetInternalRect(const ExternalRect: TRect;
  RVStyle: TRVStyle): TRect;
begin
  Result := Rect(
    ExternalRect.Left + GetTotalLeftPadding(RVStyle),
    ExternalRect.Top + GetTotalTopPadding(RVStyle),
   ExternalRect.Right - GetTotalRightPadding(RVStyle),
    ExternalRect.Bottom - GetTotalBottomPadding(RVStyle));
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.SetExtraCustomProperty(const PropName: TRVAnsiString; const Value: String): Boolean;
var Prop: Integer;
begin
  Prop := GetPropertyIdentifier(PropName);
  Result := Prop>=0;
  if Result then
    SetExtraIntPropertyEx(Prop, StrToInt(Value));
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetExtraIntPropertyReformat(Prop: Integer; var ReformatType: TRVReformatType): Boolean;
begin
  case Prop of
    rveipcBoxWidth, rveipcBoxHeight, rveipcBoxWidthType, rveipcBoxHeightType,
    rveipcBoxBorderWidth, rveipcBoxBorderInternalWidth, rveipcBoxBorderStyle,
    rveipcBoxBorderBorderOffsets_Left, rveipcBoxBorderBorderOffsets_Top,
    rveipcBoxBorderBorderOffsets_Right, rveipcBoxBorderBorderOffsets_Bottom,
    rveipcBoxPadding_Left, rveipcBoxPadding_Top,
    rveipcBoxPadding_Right, rveipcBoxPadding_Bottom,
    rveipcBoxVAlign:
      begin
        ReformatType := rvrfSRVReformat;
        Result := True;
      end;
    rveipcBoxBorderColor, rveipcBoxColor,
    rveipcBoxBorderVisibleBorders_Left, rveipcBoxBorderVisibleBorders_Top,
    rveipcBoxBorderVisibleBorders_Right, rveipcBoxBorderVisibleBorders_Bottom:
      begin
        ReformatType := rvrfSRVRepaint;
        Result := True;
      end;
    else
      Result := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVBoxProperties.ConvertToDifferentUnits(
  NewUnits: TRVStyleUnits; RVStyle: TRVStyle);
begin
  if WidthType=rvbwtAbsolute then
    Width := RVStyle.GetAsDifferentUnits(Width, NewUnits);
  if HeightType=rvbhtAbsolute then
    Height := RVStyle.GetAsDifferentUnits(Height, NewUnits);
  Border.ConvertToDifferentUnits(RVStyle, NewUnits);
  Background.BorderOffsets.ConvertToDifferentUnits(RVStyle, NewUnits);
end;
{------------------------------------------------------------------------------}
function TRVBoxProperties.GetChangedPropertyCount: Integer;
begin
  Result :=
    ord(Width<>DEFAULT_WIDTH)+
    ord(Height<>DEFAULT_HEIGHT)+
    ord(VAlign<>tlTop)+
    ord(WidthType<>rvbwtAbsolute)+
    ord(HeightType<>rvbhtAuto)+
    ord(Border.Color<>clWindowText)+
    ord(Border.Width<>1)+
    ord(Border.InternalWidth<>1)+
    ord(Border.Style<>rvbSingle)+
    ord(not Border.VisibleBorders.Left)+
    ord(not Border.VisibleBorders.Top)+
    ord(not Border.VisibleBorders.Right)+
    ord(not Border.VisibleBorders.Bottom)+
    ord(Border.BorderOffsets.Left<>DEFAULT_HPADDING)+
    ord(Border.BorderOffsets.Top<>DEFAULT_VPADDING)+
    ord(Border.BorderOffsets.Right<>DEFAULT_HPADDING)+
    ord(Border.BorderOffsets.Bottom<>DEFAULT_VPADDING)+
    ord(Background.Color<>clWindow)+
    ord(Background.BorderOffsets.Left<>DEFAULT_HPADDING)+
    ord(Background.BorderOffsets.Top<>DEFAULT_VPADDING)+
    ord(Background.BorderOffsets.Right<>DEFAULT_HPADDING)+
    ord(Background.BorderOffsets.Bottom<>DEFAULT_VPADDING);
end;
{------------------------------------------------------------------------------}
procedure TRVBoxProperties.SaveRVFExtraProperties(Stream: TStream);

  procedure SaveProp(Prop: Integer; Value, DefValue: Integer);
  begin
    if Value<>DefValue then
      WriteRVFExtraCustomPropertyInt(Stream, GetBoxPropName(Prop), Value);
  end;

begin
  SaveProp(rveipcBoxWidth, Width, DEFAULT_WIDTH);
  SaveProp(rveipcBoxHeight, Height, DEFAULT_HEIGHT);
  SaveProp(rveipcBoxVAlign, ord(VAlign), ord(tlTop));
  SaveProp(rveipcBoxWidthType, ord(WidthType), ord(rvbwtAbsolute));
  SaveProp(rveipcBoxHeightType, ord(HeightType), ord(rvbhtAuto));
  SaveProp(rveipcBoxBorderColor, Border.Color, ord(clWindowText));
  SaveProp(rveipcBoxBorderWidth, Border.Width, 1);
  SaveProp(rveipcBoxBorderInternalWidth, Border.InternalWidth, 1);
  SaveProp(rveipcBoxBorderStyle, ord(Border.Style), ord(rvbSingle));
  SaveProp(rveipcBoxBorderVisibleBorders_Left, ord(Border.VisibleBorders.Left), 1);
  SaveProp(rveipcBoxBorderVisibleBorders_Top, ord(Border.VisibleBorders.Top), 1);
  SaveProp(rveipcBoxBorderVisibleBorders_Right, ord(Border.VisibleBorders.Right), 1);
  SaveProp(rveipcBoxBorderVisibleBorders_Bottom, ord(Border.VisibleBorders.Bottom), 1);
  SaveProp(rveipcBoxBorderBorderOffsets_Left, ord(Border.BorderOffsets.Left), DEFAULT_HPADDING);
  SaveProp(rveipcBoxBorderBorderOffsets_Top, ord(Border.BorderOffsets.Top), DEFAULT_VPADDING);
  SaveProp(rveipcBoxBorderBorderOffsets_Right, ord(Border.BorderOffsets.Right), DEFAULT_HPADDING);
  SaveProp(rveipcBoxBorderBorderOffsets_Bottom, ord(Border.BorderOffsets.Bottom), DEFAULT_VPADDING);
  SaveProp(rveipcBoxColor, ord(Background.Color), ord(clWindow));
  SaveProp(rveipcBoxPadding_Left, ord(Background.BorderOffsets.Left), DEFAULT_HPADDING);
  SaveProp(rveipcBoxPadding_Top, ord(Background.BorderOffsets.Top), DEFAULT_VPADDING);
  SaveProp(rveipcBoxPadding_Right, ord(Background.BorderOffsets.Right), DEFAULT_HPADDING);
  SaveProp(rveipcBoxPadding_Bottom, ord(Background.BorderOffsets.Bottom), DEFAULT_VPADDING);
end;

{$ENDIF}


end.
