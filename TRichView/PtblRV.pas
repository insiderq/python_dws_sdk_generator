
{*******************************************************}
{                                                       }
{       RichView                                        }
{       TCustomRVPrint: ancestor of TRVPrint and        }
{       TRVReportHelper.                                }
{       TRVPrint: component for printing                }
{       RichView.                                       }
{       (registered on "RichView" page of               }
{       the Component Palette)                          }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit PtblRV;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RVScroll, RichView, RVItem, RVStyle, Printers, CommDlg, DLines, RVFuncs,
  CRVData, CRVFData, RVRVData, PtRVData, WinSpool,
  {$IFNDEF RVDONOTUSESEQ}
  RVNote, RVSidenote,
  {$ENDIF}
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  RVDocParams,
  {$ENDIF}
  RVClasses, RVUni, RVTypes;

{$I RV_Defs.inc}

type
  TCustomRVPrint = class;
  TRVPrint = class;
  {------------------------------------------------------------}
  TRVPrintComponentEvent = procedure (Sender: TCustomRVPrint; PrintMe: TControl;
    var ComponentImage: TBitmap) of object;

  TRVPrintingEvent = procedure (Sender: TCustomRichView; PageCompleted: Integer;
    Step:TRVPrintingStep) of object;

  TRVPagePrepaintEvent = procedure (Sender: TRVPrint; PageNo: Integer;
    Canvas: TCanvas; Preview: Boolean; PageRect, PrintAreaRect: TRect) of object;
  {------------------------------------------------------------}
  TRVFixMarginsMode = (rvfmmAutoCorrect, rvfmmIgnore);
  {------------------------------------------------------------}
  {$IFNDEF RVDONOTUSESEQ}
  TRVEndnotePage = class
    public
      Index, Page: Integer;
  end;

  TRVEndnotePageList = class(TRVList)
  private
    function GetItems(Index: Integer): TRVEndnotePage;
    procedure SetItems(Index: Integer; const Value: TRVEndnotePage);
    public
      property Items[Index: Integer]: TRVEndnotePage read GetItems write SetItems; default;
  end;

  TRVEndnoteList = class (TRVList)
    private
      FOwner: TCustomRVPrint;
      Pages: TRVEndnotePageList;
      function GetItems(Index: Integer): TRVEndnotePtblRVData;
      procedure SetItems(Index: Integer; const Value: TRVEndnotePtblRVData);
    public
      constructor Create(AOwner: TCustomRVPrint);
      destructor Destroy; override;
      procedure DrawPage(GlobalPageNo, PageNo: Integer; Canvas: TCanvas;
        Preview, Correction: Boolean);
      property Items[Index: Integer]: TRVEndnotePtblRVData read GetItems write SetItems; default;
  end;

  TRVFootnoteList = class (TRVList)
    private
      FOwner: TCustomRVPrint;
      function GetItems(Index: Integer): TRVFootnotePtblRVData;
      procedure SetItems(Index: Integer; const Value: TRVFootnotePtblRVData);
    public
      constructor Create(AOwner: TCustomRVPrint);
      procedure SortByFootnotes;
      function FindByFootnote(Footnote: TRVFootnoteItemInfo): TRVFootnotePtblRVData;
      property Items[Index: Integer]: TRVFootnotePtblRVData read GetItems write SetItems; default;
  end;


  {
    Note: this class is inherited from TRVComplexLineHeightDrawLineInfo,
    not TRVDrawLineInfo, though it is not necessary used in paragraphs with
    complex line spacing. But it may be there...
  }
  TRVFootnoteDrawItem = class(TRVComplexLineHeightDrawLineInfo)
    public
      DocumentRVData: TRVFootnotePtblRVData;
  end;
  {$ENDIF}
  {------------------------------------------------------------}
  TCustomPrintableRV = class(TCustomRichView)
    private
      FRVPrint: TCustomRVPrint;
    protected
      {$IFNDEF RVDONOTUSESEQ}
      FNoteSeparatorHeight, FNoteLineWidth: Integer;
      FEndnotes: TRVEndnoteList;
      FFootnotes: TRVFootnoteList;
      {$ENDIF}
      procedure CreateParams(var Params: TCreateParams); override;
    public
      constructor Create(AOwner: TComponent); override;
      {$IFNDEF RVDONOTUSESEQ}
      destructor Destroy; override;
      function GetFootnoteRVData(
        Footnote: TRVFootnoteItemInfo): TRVFootnotePtblRVData;
      procedure DrawNoteSeparatorAbove(PageNo, Y: Integer; Canvas: TCanvas;
        FullSize: Boolean);
      procedure CalcFootnotesCoords(References: TList; PageNo: Integer);
      procedure FreeNotesLists;
      {$ENDIF}
      {$IFNDEF RVDONOTUSERVF}
      procedure ApplyLayoutInfo (Layout: TRVLayoutInfo); override;
      {$ENDIF}
      function CanUseCustomPPI: Boolean; virtual;
      procedure InitFormatPages;
      function FormatPages: Integer;
      procedure FinalizeFormatPages;
      procedure DrawPage(PageNo: Integer; Canvas: TCanvas; Preview,
        Correction: Boolean);
      procedure Paint; override;
      property RVPrint: TCustomRVPrint read FRVPrint write FRVPrint;
      {$IFNDEF RVDONOTUSESEQ}
      property NoteSeparatorHeight: Integer read FNoteSeparatorHeight;
      {$ENDIF}
  end;
  {------------------------------------------------------------}
  TPrintableRV = class(TCustomPrintableRV)
    private
      procedure DoOnPrinting(PageCompleted: Integer; Step:TRVPrintingStep);
    protected
      function GetDataClass: TRichViewRVDataClass; override;
    public
      FMirrorMargins, FFacingPages, FTitlePage: Boolean;
      FMargins: TRVUnitsRect;
      FUnits: TRVUnits;
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure PrintPages(firstPgNo, lastPgNo: Integer;
        const Title: String; Copies: Integer; Collate: Boolean);
      procedure Print(const Title: String; Copies: Integer; Collate: Boolean);
      procedure ContinuousPrint;
  end;
  {------------------------------------------------------------}
  TCustomRVPrint = class(TComponent)
    private
      { Private declarations }
      FPreviewCorrection: Boolean;
      FOnPrintComponent: TRVPrintComponentEvent;
      FMinPrintedItemNo, FMaxPrintedItemNo: Integer;
      FIgnorePageBreaks: Boolean;
      function GetTransparentBackground: Boolean;
      procedure SetTransparentBackground(const Value: Boolean);
      function GetPreview100PercentHeight: Integer;
      function GetPreview100PercentWidth: Integer;
      function GetColorMode: TRVColorMode;
      procedure SetColorMode(const Value: TRVColorMode);
      function GetIsDestinationReady: Boolean;
      function GetFirstPageNo: Integer;
      function GetLastPageNo: Integer;
    protected
      { Protected declarations }
      procedure Loaded; override;
      function CreateRichView: TCustomPrintableRV; dynamic;
      function GetPagesCount: Integer;
      {$IFNDEF RVDONOTUSESEQ}
      function FormatEndnotes: Integer;
      procedure PreformatFootnotes;
      procedure PostformatFootnotes;
      function IgnoreEndnotes: Boolean; dynamic;
      {$ENDIF}
    public
      { Public declarations }
      rv: TCustomPrintableRV;
      Ready: Boolean;
      StartAt,EndAt: Integer;
      FormattingID: Integer;
      constructor Create(AOwner: TComponent); override;
      procedure Clear;
      procedure UpdatePaletteInfo;
      procedure GetFirstItemOnPage(PageNo: Integer;
        var ItemNo, OffsetInItem: Integer);
      procedure GetFirstItemOnPageEx(PageNo: Integer;
        var ItemNo, OffsetInItem, ExtraData: Integer);
      function GetPageNo(RVData: TCustomRVData;
        ItemNo, OffsetInItem: Integer): Integer;
      function IsComplexSoftPageBreak(PageNo: Integer): Boolean;
      procedure AssignComplexSoftPageBreakToItem(PageNo: Integer;
        RVData: TCustomRVFormattedData);
      procedure DrawPreview(pgNo: Integer; Canvas:  TCanvas;
        const PageRect: TRect);
      procedure DrawMarginsRect(Canvas:  TCanvas; const PageRect: TRect;
        PageNo: Integer);
      {$IFNDEF RVDONOTUSERVF}
      function SavePageAsRVF(Stream: TStream; PageNo: Integer): Boolean;
      function SaveFromPageAsRVF(Stream: TStream; FirstPageNo: Integer): Boolean;
      {$ENDIF}
      procedure GetMinimalMargins(var MarginsRect: TRect;
        ScreenResolution: Boolean);
      function GetSourceRichView: TCustomRichView; dynamic;
      property PagesCount: Integer read GetPagesCount;
      property Preview100PercentWidth: Integer read GetPreview100PercentWidth;
      property Preview100PercentHeight: Integer read GetPreview100PercentHeight;
      property IsDestinationReady: Boolean read GetIsDestinationReady;
      property FirstPageNo: Integer read GetFirstPageNo;
      property LastPageNo: Integer read GetLastPageNo;
    published
      { Published declarations }
      property PreviewCorrection: Boolean
        read FPreviewCorrection  write FPreviewCorrection;
      property OnPrintComponent: TRVPrintComponentEvent
        read FOnPrintComponent write FOnPrintComponent;
      property TransparentBackground: Boolean
        read GetTransparentBackground write SetTransparentBackground default False;
      property ColorMode: TRVColorMode
        read GetColorMode write SetColorMode default rvcmPrinterColor;
      property MinPrintedItemNo: Integer read FMinPrintedItemNo write FMinPrintedItemNo default 0;
      property MaxPrintedItemNo: Integer read FMaxPrintedItemNo write FMaxPrintedItemNo default -1;
      property IgnorePageBreaks: Boolean read FIgnorePageBreaks write FIgnorePageBreaks default False;
  end;
  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVPrint = class(TCustomRVPrint)
    private
      { Private declarations }
      FOnFormatting, FOnPrinting: TRVPrintingEvent;
      FOnPagePrepaint, FOnPagePostPaint: TRVPagePrepaintEvent;
      FClipMargins: Boolean;
      FPrintMe: TCustomRichView;
      FFixMarginsMode: TRVFixMarginsMode;
      function GetLM: Integer;
      function GetRM: Integer;
      function GetTM: Integer;
      function GetBM: Integer;
      procedure SetLM(mm: Integer);
      procedure SetRM(mm: Integer);
      procedure SetTM(mm: Integer);
      procedure SetBM(mm: Integer);
      function GetMirrorMargins: Boolean;
      procedure SetMirrorMargins(const Value: Boolean);
      function GetFooterYMM: Integer;
      function GetHeaderYMM: Integer;
      procedure SetFooterYMM(const Value: Integer);
      procedure SetHeaderYMM(const Value: Integer);
      function GetFooterY: TRVLength;
      function GetHeaderY: TRVLength;
      procedure SetFooterY(const Value: TRVLength);
      procedure SetHeaderY(const Value: TRVLength);
      function GetMargins: TRVUnitsRect;
      procedure SetMargins(const Value: TRVUnitsRect);
      function GetUnits: TRVUnits;
      procedure SetUnits(const Value: TRVUnits);
      procedure LMReader(reader: TReader);
      procedure RMReader(reader: TReader);
      procedure TMReader(reader: TReader);
      procedure BMReader(reader: TReader);
      procedure HYReader(reader: TReader);
      procedure FYReader(reader: TReader);
      function GetFacingPages: Boolean;
      function GetTitlePage: Boolean;
      procedure SetFacingPages(const Value: Boolean);
      procedure SetTitlePage(const Value: Boolean);
      procedure ClearHeaderFooter;
    protected
      { Protected declarations }
      function CreateRichView: TCustomPrintableRV; override;
      procedure DefineProperties(Filer: TFiler);override;
    public
      { Public declarations }
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      function GetSourceRichView: TCustomRichView; override;
      procedure AssignSource(PrintMe: TCustomRichView);
      {$IFNDEF RVDONOTUSEDOCPARAMS}
      procedure AssignDocParameters(DocParameters: TRVDocParameters);
      {$ENDIF}
      procedure SetHeader(RVData: TCustomRVFormattedData;
        HeaderType: TRVHFType{$IFDEF RICHVIEWDEF4}=rvhftNormal{$ENDIF});
      procedure SetFooter(RVData: TCustomRVFormattedData;
        FooterType: TRVHFType{$IFDEF RICHVIEWDEF4}=rvhftNormal{$ENDIF});
      function FormatPages(PrintOptions:TRVDisplayOptions): Integer;
      procedure PrintPages(firstPgNo, lastPgNo: Integer; Title: String;
        Copies: Integer; Collate: Boolean);
      procedure Print(Title: String; Copies: Integer; Collate: Boolean);
      procedure ContinuousPrint;
      procedure MakePreview(pgNo: Integer; bmp: TBitmap);
      procedure MakeScaledPreview(pgNo: Integer; bmp: TBitmap);
      function GetHeaderRect(PageNo: Integer): TRect;
      function GetFooterRect(PageNo: Integer): TRect;
      procedure Clear;
      procedure ConvertToUnits(AUnits: TRVUnits);
      // deprecated properties
      property LeftMarginMM:  Integer read GetLM write SetLM; // deprecated - use Margins
      property RightMarginMM: Integer read GetRM write SetRM; // deprecated - use Margins
      property TopMarginMM:   Integer read GetTM write SetTM; // deprecated - use Margins
      property BottomMarginMM:Integer read GetBM write SetBM; // deprecated - use Margins
      property FooterYMM:     Integer read GetFooterYMM write SetFooterYMM; // deprecated - use FooterY
      property HeaderYMM:     Integer read GetHeaderYMM write SetHeaderYMM; // deprecated - use HeaderY
    published
      { Published declarations }
      property Margins: TRVUnitsRect read GetMargins write SetMargins;
      property FooterY: TRVLength read GetFooterY write SetFooterY;
      property HeaderY: TRVLength read GetHeaderY write SetHeaderY;
      property Units: TRVUnits read GetUnits write SetUnits default rvuMillimeters;
      property ClipMargins:   Boolean read FClipMargins write FClipMargins default False;
      property MirrorMargins: Boolean read GetMirrorMargins write SetMirrorMargins default False;
      property FacingPages: Boolean read GetFacingPages write SetFacingPages default False;
      property TitlePage: Boolean read GetTitlePage write SetTitlePage default False;
      property OnFormatting: TRVPrintingEvent read FOnFormatting write FOnFormatting;
      property OnSendingToPrinter: TRVPrintingEvent read FOnPrinting write FOnPrinting;
      property OnPagePrepaint: TRVPagePrepaintEvent read FOnPagePrepaint write FOnPagePrepaint;
      property OnPagePostpaint: TRVPagePrepaintEvent read FOnPagePostpaint write FOnPagePostpaint;
      property FixMarginsMode: TRVFixMarginsMode
        read FFixMarginsMode write FFixMarginsMode default rvfmmAutoCorrect;
  end;

function RV_GetPrinterDC: HDC;
implementation
uses Math;
{==============================================================================}
type
  TPrinterDevice = class
  public
    Driver, Device, Port: String;
  end;

function RV_GetPrinterDC: HDC;
var ADevice, ADriver, APort: array[0..79] of Char;
    ADeviceMode: THandle;
    DevMode: PDeviceMode;
begin
  Printer.GetPrinter(ADevice,ADriver,APort,ADeviceMode);
  if ADeviceMode<>0 then
    DevMode := PDeviceMode(GlobalLock(ADeviceMode))
  else
    DevMode := nil;
  Result := CreateDC(ADriver, ADevice, APort, DevMode);
  if ADeviceMode<>0 then
    GlobalUnlock(ADeviceMode);
end;
{=============================== TCustomRVPrint ===============================}
constructor TCustomRVPrint.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMinPrintedItemNo := 0;
  FMaxPrintedItemNo := -1;
  rv := CreateRichView;
  rv.FRVPrint := Self;
  //if not (csDesigning in ComponentState) and (Self.Owner is TWinControl) then
  //  rv.Parent := TWinControl(Self.Owner);
  PreviewCorrection := True;
  ColorMode := rvcmPrinterColor;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.CreateRichView: TCustomPrintableRV;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetSourceRichView: TCustomRichView;
begin
  Result := rv;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.Clear;
begin
  rv.Clear;
  {$IFNDEF RVDONOTUSESEQ}
  RVFreeAndNil(rv.FEndnotes);
  RVFreeAndNil(rv.FFootnotes);
  {$ENDIF}  
  Ready := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.GetFirstItemOnPageEx(PageNo: Integer;
  var ItemNo, OffsetInItem, ExtraData: Integer);
begin
  if PageNo <= TPrintableRVData(rv.RVData).Pages.Count then
    TPrintableRVData(rv.RVData).GetFirstItemOnPageEx(PageNo, ItemNo,
      OffsetInItem, ExtraData)
  else begin
    ItemNo := -1;
    OffsetInItem := -1;
    ExtraData := -1;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetPageNo(RVData: TCustomRVData;
  ItemNo, OffsetInItem: Integer): Integer;
var LocalPageNo, PageCount: Integer;
    PtblRVData: TCustomPrintableRVData;
begin
  if not TPrintableRVData(rv.RVData).GetPageNo(RVData, ItemNo, OffsetInItem,
    GetSourceRichView.RVData, Result, LocalPageNo, PageCount, PtblRVData) then
    Result := -1;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.IsComplexSoftPageBreak(PageNo: Integer): Boolean;
begin
  if PageNo <= TPrintableRVData(rv.RVData).Pages.Count then
    Result := TPrintableRVData(rv.RVData).IsComplexSoftPageBreak(PageNo)
  else
    Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.AssignComplexSoftPageBreakToItem(PageNo: Integer;
  RVData: TCustomRVFormattedData);
begin
  if PageNo <= TPrintableRVData(rv.RVData).Pages.Count then
    TPrintableRVData(rv.RVData).AssignComplexSoftPageBreakToItem(PageNo, RVData);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.GetFirstItemOnPage(PageNo: Integer; var ItemNo,
  OffsetInItem: Integer);
var ExtraData: Integer;
begin
  GetFirstItemOnPageEx(PageNo, ItemNo, OffsetInItem, ExtraData);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
function TCustomRVPrint.SavePageAsRVF(Stream: TStream; PageNo: Integer): Boolean;
var Layout: TRVLayoutInfo;
begin
  Layout := rv.CreateLayoutInfo;
  try
    Result := TCustomMainPtblRVData(rv.RVData).SavePageAsRVF(Stream, PageNo,
      rv.Color, rv.Background, Layout);
  finally
    Layout.Free;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.SaveFromPageAsRVF(Stream: TStream; FirstPageNo: Integer): Boolean;
var Layout: TRVLayoutInfo;
begin
  Layout := rv.CreateLayoutInfo;
  try
    Result := TCustomMainPtblRVData(rv.RVData).SaveFromPageAsRVF(Stream,
      FirstPageNo, rv.Color, rv.Background, Layout);
  finally
    Layout.Free;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetPagesCount: Integer;
begin
  Result := TPrintableRVData(rv.RVData).Pages.Count;
  {$IFNDEF RVDONOTUSESEQ}
  if rv.FEndnotes<>nil then
    inc(Result, rv.FEndnotes.Pages.Count-1);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TCustomRVPrint.IgnoreEndnotes: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.FormatEndnotes: Integer;
var Endnote: TRVEndnoteItemInfo;
  RVData: TRVEndnotePtblRVData;
  PageInfo: TRVEndnotePage;
  LastPage: TRVPageInfo;
  DefMaxHeight, MaxHeight, LStartAt, LStartAt2, StartY, y, i, EndnotePage,
  CurHeight: Integer;
  Splitting: Boolean;
  FootnoteRVDataList: TRVFootnoteRefList;
  PageNo: Integer;
  PtblRVData: TCustomMainPtblRVData;
  TmpM: PRect;
begin
  Result := 0;
  PtblRVData := TCustomMainPtblRVData(rv.RVData);
  if (PtblRVData.Pages=nil) or (PtblRVData.Pages.Count=0) then
    exit;
  PageNo := PtblRVData.Pages.Count;
  LastPage := PtblRVData.Pages[PageNo-1];
  TmpM := PtblRVData.GetTmpMForPage(PageNo);
  DefMaxHeight := PtblRVData.GetPageHeight-
    (PtblRVData.TmpTMPix+PtblRVData.TmpBMPix+TmpM.Top+TmpM.Bottom);
  if LastPage.FootnoteRVDataList<>nil then
    MaxHeight := LastPage.FootnoteRVDataList[0].Top-rv.NoteSeparatorHeight-
      TmpM.Top-(PtblRVData.TmpTMPix+PtblRVData.TmpBMPix)
  else
    MaxHeight := DefMaxHeight;

  if IgnoreEndnotes then
    Endnote := nil
  else
    Endnote := RVGetFirstEndnote(GetSourceRichView, False);

  if Endnote=nil then begin
    if LastPage.FootnoteRVDataList<>nil then
      EndAt := DefMaxHeight;
    exit;
  end;

  if Endnote<>nil then begin
    rv.Style.TextStyles[0].Apply(TCustomMainPtblRVData(rv.RVData).PrinterCanvas,
      rvbdUnspecified, False, False, nil, True, False, rv.Style);
    LStartAt := EndAt+rv.FNoteSeparatorHeight;
    rv.FEndnotes := TRVEndnoteList.Create(Self);
    PageInfo := TRVEndnotePage.Create;
    PageInfo.Index := 0;
    PageInfo.Page  := 1;
    repeat
      RVData := TRVEndnotePtblRVData.Create(rv);
      RVData.ShareItemsFrom(Endnote.Document);
      RVData.Endnote := Endnote;
      RVData.Format_(False, True, False, 0, RVData.GetCanvas, False, False, False, False, False);
      CurHeight := MaxHeight-LStartAt;
      if not RVData.CanPlaceFirstPageHere(CurHeight, False,
          TCustomMainPtblRVData(rv.RVData).PrnSad, False, False) then begin
        inc(PageNo);
        TmpM := PtblRVData.GetTmpMForPage(PageNo);
        DefMaxHeight := PtblRVData.GetPageHeight-
          (PtblRVData.TmpTMPix+PtblRVData.TmpBMPix+TmpM.Top+TmpM.Bottom);
        MaxHeight := DefMaxHeight;
        LStartAt := rv.FNoteSeparatorHeight;
        RVData.FromNewPage := True;
        if PageInfo<>nil then begin
          PageInfo.Index := -1;
          PageInfo.Page  := -1;
          rv.FEndnotes.Pages.Add(PageInfo);
        end;
        PageInfo := TRVEndnotePage.Create;
        PageInfo.Index := rv.FEndnotes.Count;
        PageInfo.Page  := 1;
      end;
      if PageInfo<>nil then begin
        rv.FEndnotes.Pages.Add(PageInfo);
        PageInfo := nil;
      end;
      RVData.StartAt := LStartAt;
      RVData.NextStartAt := rv.FNoteSeparatorHeight;
      i := 0;
      EndnotePage := 1;
      while i<RVData.DrawItems.Count do begin
        if EndnotePage>1 then begin
          inc(PageNo);
          TmpM := PtblRVData.GetTmpMForPage(PageNo);
          DefMaxHeight := PtblRVData.GetPageHeight-
          (PtblRVData.TmpTMPix+PtblRVData.TmpBMPix+TmpM.Top+TmpM.Bottom);
          MaxHeight := DefMaxHeight;
          PageInfo := TRVEndnotePage.Create;
          PageInfo.Page  := EndnotePage;
          PageInfo.Index := rv.FEndnotes.Count;
          rv.FEndnotes.Pages.Add(PageInfo);
          PageInfo := nil;
        end;
        FootnoteRVDataList := nil;
        RVData.FormatNextPage(i, LStartAt2, StartY, y,
          Splitting, MaxHeight, FootnoteRVDataList, False);
        inc(EndnotePage);
      end;
      LStartAt := RVData.EndAt;
      rv.FEndnotes.Add(RVData);
      Endnote := RVGetNextEndnote(GetSourceRichView, Endnote, False);
    until Endnote=nil;
    Result := rv.FEndnotes.Pages.Count-1;
    if (rv.FEndnotes.Pages.Count=0) and (LastPage.FootnoteRVDataList<>nil) then
      EndAt := DefMaxHeight
    else
      EndAt := LStartAt;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.PreformatFootnotes;
var Footnote: TRVFootnoteItemInfo;
  RVData: TRVFootnotePtblRVData;
  TmpM: TRect;
begin
  rv.FNoteSeparatorHeight := rv.Style.GraphicInterface.TextHeight(
    TCustomMainPtblRVData(rv.RVData).PrinterCanvas, '-');
  rv.FNoteLineWidth := RV_YToDevice(1, TCustomMainPtblRVData(rv.RVData).PrnSad);

  Footnote := RVGetFirstFootnote(GetSourceRichView, True);
  if Footnote=nil then
    exit;
  TmpM := TCustomMainPtblRVData(rv.RVData).TmpMNormal;
  rv.FFootnotes := TRVFootnoteList.Create(Self);
  repeat
    RVData := TRVFootnotePtblRVData.Create(rv, Footnote.Document,
      TCustomPrintableRVData(rv.RVData));
    RVData.Footnote := Footnote;
    RVData.ParentDrawsBack := True;
    RVData.Transparent := True;
    RVData.FColor := clNone;
    RVData.Left := TmpM.Left;
    RVData.Width := TCustomMainPtblRVData(rv.RVData).GetPageWidth - (TmpM.Left+TmpM.Right);
    RVData.Top := 0;
    RVData.Height := 1;
    RVData.Format(True, False);
    rv.FFootnotes.Add(RVData);
    Footnote := RVGetNextFootnote(GetSourceRichView, Footnote, True);
  until Footnote=nil;
  rv.FFootnotes.SortByFootnotes;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.PostformatFootnotes;
var PageIndex, i, Delta, TopOffs: Integer;
    PageInfo: TRVPageInfo;
begin

  for PageIndex := 0 to TCustomMainPtblRVData(rv.RVData).Pages.Count-1 do begin
    TopOffs := TCustomMainPtblRVData(rv.RVData).GetTmpMForPage(PageIndex+1).Top+
      TCustomMainPtblRVData(rv.RVData).TmpTMPix;
    PageInfo := TCustomMainPtblRVData(rv.RVData).Pages[PageIndex];
    if (PageInfo.FootnoteRVDataList<>nil) and
       (PageInfo.FootnoteRVDataList[0].Top<TopOffs+PageInfo.DocumentHeight+rv.NoteSeparatorHeight) then
    begin
      Delta := TopOffs+PageInfo.DocumentHeight+rv.NoteSeparatorHeight-PageInfo.FootnoteRVDataList[0].Top;
      for i := 0 to PageInfo.FootnoteRVDataList.Count-1 do
        inc(PageInfo.FootnoteRVDataList[i].Top, Delta);
    end;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetTransparentBackground: Boolean;
begin
  Result := TPrintableRVData(rv.RVData).Transparent;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.SetTransparentBackground(const Value: Boolean);
begin
  TPrintableRVData(rv.RVData).Transparent := Value;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.Loaded;
begin
  inherited Loaded;
  UpdatePaletteInfo;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.UpdatePaletteInfo;
begin
  rv.UpdatePaletteInfo;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetIsDestinationReady: Boolean;
begin
  Result := TCustomMainPtblRVData(rv.RVData).FIsDestinationReady;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetPreview100PercentHeight: Integer;
begin
  with TCustomMainPtblRVData(rv.RVData) do
    Result := RV_YToScreen(rv.ClientHeight+TmpMNormal.Top+TmpMNormal.Bottom, PrnSaD);
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetPreview100PercentWidth: Integer;
begin
  with TCustomMainPtblRVData(rv.RVData) do
    Result := RV_XToScreen(rv.ClientWidth+TmpMNormal.Left+TmpMNormal.Right, PrnSaD);
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetColorMode: TRVColorMode;
begin
  Result := TCustomMainPtblRVData(rv.RVData).ColorMode;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.SetColorMode(const Value: TRVColorMode);
begin
  TCustomMainPtblRVData(rv.RVData).ColorMode := Value;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.DrawPreview(pgNo: Integer; Canvas: TCanvas;
  const PageRect: TRect);
var OldMapMode, OldPPI: Integer;
    OldWindowExt, OldViewportExt: TSize;
    OldViewportOrg: TPoint;
begin
  if rv.Style=nil then
    exit;
  OldMapMode := rv.Style.GraphicInterface.GetMapMode(Canvas);
  rv.Style.GraphicInterface.SetMapMode(Canvas, MM_TEXT);
  Canvas.Brush.Color := clWhite;
  Canvas.Pen.Color := clWhite;
  rv.Style.GraphicInterface.FillRect(Canvas, PageRect);
  rv.Style.GraphicInterface.SetMapMode(Canvas,MM_ANISOTROPIC);
  with TCustomMainPtblRVData(rv.RVData) do
   rv.Style.GraphicInterface.SetWindowExtEx(Canvas,
     rv.ClientWidth +TmpMNormal.Left+TmpMNormal.Right,
     rv.ClientHeight+TmpMNormal.Top+TmpMNormal.Bottom, @OldWindowExt);
  with PageRect do begin
    rv.Style.GraphicInterface.SetViewportExtEx(Canvas,
      Right-Left, Bottom-Top, @OldViewportExt);
    rv.Style.GraphicInterface.SetViewportOrgEx(Canvas,
      Left,Top, @OldViewportOrg);
  end;
  OldPPI := Canvas.Font.PixelsPerInch;
  Canvas.Font.PixelsPerInch := TCustomMainPtblRVData(rv.RVData).PrnSaD.ppiyDevice;
  try
    if (pgNo>=1) and (pgNo<=PagesCount) then
      rv.DrawPage(pgNo, Canvas, True, PreviewCorrection);
  finally
    Canvas.Font.PixelsPerInch := OldPPI;
    rv.Style.GraphicInterface.SetViewportOrgEx(Canvas, OldViewportOrg.X, OldViewportOrg.Y, nil);
    rv.Style.GraphicInterface.SetViewportExtEx(Canvas, OldViewportExt.cx, OldViewportExt.cy, nil);
    rv.Style.GraphicInterface.SetWindowExtEx(Canvas, OldWindowExt.cx, OldWindowExt.cy, nil);
    rv.Style.GraphicInterface.SetMapMode(Canvas, OldMapMode);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.DrawMarginsRect(Canvas: TCanvas; const PageRect: TRect;
  PageNo: Integer);
var FullWidth, FullHeight: Integer;
    RectWidth, RectHeight, LM: Integer;
begin
  with TCustomMainPtblRVData(rv.RVData) do begin
     FullWidth  := rv.ClientWidth+GetTmpMForPage(PageNo).Left+GetTmpMForPage(PageNo).Right;
     FullHeight := GetContentHeightForPage(PageNo)+GetTmpMForPage(PageNo).Top+GetTmpMForPage(PageNo).Bottom;
     RectWidth  := PageRect.Right-PageRect.Left;
     RectHeight := PageRect.Bottom-PageRect.Top;
     LM := TCustomPrintableRVData(rv.RVData).GetPrintableAreaLeft(PageNo);
     rv.Style.GraphicInterface.Rectangle(Canvas,
       PageRect.Left+MulDiv(LM,RectWidth,FullWidth),
       PageRect.Top+MulDiv(GetTmpMForPage(PageNo).Top,RectHeight,FullHeight),
       PageRect.Left+MulDiv(LM+rv.ClientWidth,RectWidth,FullWidth),
       PageRect.Top+MulDiv(GetTmpMForPage(PageNo).Top+GetContentHeightForPage(PageNo),RectHeight,FullHeight)
      );
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVPrint.GetMinimalMargins(var MarginsRect: TRect;
  ScreenResolution: Boolean);
begin
  MarginsRect := TCustomMainPtblRVData(rv.RVData).PhysM;
  if ScreenResolution then
    with TCustomMainPtblRVData(rv.RVData) do begin
      MarginsRect.Left   := RV_XToScreen(MarginsRect.Left,   PrnSaD);
      MarginsRect.Top    := RV_YToScreen(MarginsRect.Top,    PrnSaD);
      MarginsRect.Right  := RV_XToScreen(MarginsRect.Right,  PrnSaD);
      MarginsRect.Bottom := RV_YToScreen(MarginsRect.Bottom, PrnSaD);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetFirstPageNo: Integer;
begin
  if (FMinPrintedItemNo<=0) then
    Result := 1
  else begin
    if FMinPrintedItemNo>=GetSourceRichView.RVData.ItemCount then
      Result := PagesCount+1
    else
      Result := GetPageNo(GetSourceRichView.RVData,
        FMinPrintedItemNo, GetSourceRichView.RVData.GetOffsBeforeItem(FMinPrintedItemNo));
    if Result<=0 then
      Result := 1;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVPrint.GetLastPageNo: Integer;
begin
  if (FMaxPrintedItemNo<0) or (FMaxPrintedItemNo>=GetSourceRichView.RVData.ItemCount-1) then
    Result := PagesCount
  else begin
    Result := GetPageNo(GetSourceRichView.RVData,
      FMaxPrintedItemNo, GetSourceRichView.RVData.GetOffsAfterItem(FMaxPrintedItemNo));
    if Result<=0 then
      Result := PagesCount;
  end;
end;
{=============================== TRVPrint =====================================}
constructor TRVPrint.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Units := rvuMillimeters;
  Margins.SetAll(20.0);
end;
{------------------------------------------------------------------------------}
destructor TRVPrint.Destroy;
begin
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.DefineProperties(Filer: TFiler);
begin
  inherited;
  // readers for deprecated properties (previosly published)
  Filer.DefineProperty('LeftMarginMM',   LMReader, nil, False);
  Filer.DefineProperty('RightMarginMM',  RMReader, nil, False);
  Filer.DefineProperty('TopMarginMM',    TMReader, nil, False);
  Filer.DefineProperty('BottomMarginMM', BMReader, nil, False);
  Filer.DefineProperty('HeaderYMM',      HYReader, nil, False);
  Filer.DefineProperty('FooterYMM',      FYReader, nil, False);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.Clear;
begin
  inherited Clear;
  ClearHeaderFooter;
  FPrintMe := nil;
end;
{------------------------------------------------------------------------------}
function TRVPrint.CreateRichView: TCustomPrintableRV;
begin
  Result := TPrintableRV.Create(Self);
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetSourceRichView: TCustomRichView;
begin
  Result := FPrintMe;
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetMargins: TRVUnitsRect;
begin
  Result := TPrintableRV(rv).FMargins;
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetMirrorMargins: Boolean;
begin
  Result := TPrintableRV(rv).FMirrorMargins;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetMirrorMargins(const Value: Boolean);
begin
  TPrintableRV(rv).FMirrorMargins := Value;
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetTitlePage: Boolean;
begin
  Result := TPrintableRV(rv).FTitlePage;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetTitlePage(const Value: Boolean);
begin
  TPrintableRV(rv).FTitlePage := Value;
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetFacingPages: Boolean;
begin
  Result := TPrintableRV(rv).FFacingPages;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetFacingPages(const Value: Boolean);
begin
  TPrintableRV(rv).FFacingPages := Value;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetMargins(const Value: TRVUnitsRect);
begin
  TPrintableRV(rv).FMargins.Assign(Value);
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetUnits: TRVUnits;
begin
   Result := TPrintableRV(rv).FUnits;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetUnits(const Value: TRVUnits);
begin
   TPrintableRV(rv).FUnits := Value;
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetLM: Integer;
begin
  Result := Round(RV_UnitsToUnits(Margins.Left, Units, rvuMillimeters));
end;
{------------------------------------------------------------------------------}
function  TRVPrint.GetRM: Integer;
begin
  Result := Round(RV_UnitsToUnits(Margins.Right, Units, rvuMillimeters));
end;
{------------------------------------------------------------------------------}
function  TRVPrint.GetTM: Integer;
begin
  Result := Round(RV_UnitsToUnits(Margins.Top, Units, rvuMillimeters));
end;
{------------------------------------------------------------------------------}
function  TRVPrint.GetBM: Integer;
begin
  Result := Round(RV_UnitsToUnits(Margins.Bottom, Units, rvuMillimeters));
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetLM(mm: Integer);
begin
  Margins.Left := RV_UnitsToUnits(mm, rvuMillimeters, Units);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetRM(mm: Integer);
begin
  Margins.Right := RV_UnitsToUnits(mm, rvuMillimeters, Units);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetTM(mm: Integer);
begin
  Margins.Top := RV_UnitsToUnits(mm, rvuMillimeters, Units);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetBM(mm: Integer);
begin
  Margins.Bottom := RV_UnitsToUnits(mm, rvuMillimeters, Units);
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetFooterYMM: Integer;
begin
  Result := Round(RV_UnitsToUnits(TPrintableRVData(rv.RVData).FooterY, Units, rvuMillimeters));
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetHeaderYMM: Integer;
begin
  Result := Round(RV_UnitsToUnits(TPrintableRVData(rv.RVData).HeaderY, Units, rvuMillimeters));
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetFooterYMM(const Value: Integer);
begin
  TPrintableRVData(rv.RVData).FooterY := RV_UnitsToUnits(Value, rvuMillimeters, Units);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetHeaderYMM(const Value: Integer);
begin
  TPrintableRVData(rv.RVData).HeaderY := RV_UnitsToUnits(Value, rvuMillimeters, Units);
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetFooterY: TRVLength;
begin
  Result := TPrintableRVData(rv.RVData).FooterY;
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetHeaderY: TRVLength;
begin
  Result := TPrintableRVData(rv.RVData).HeaderY;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetFooterY(const Value: TRVLength);
begin
  TPrintableRVData(rv.RVData).FooterY := Value;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetHeaderY(const Value: TRVLength);
begin
  TPrintableRVData(rv.RVData).HeaderY := Value;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.ClearHeaderFooter;
{$IFNDEF RVDONOTUSESEQ}
var HFType: TRVHFType;
    IsHeader: Boolean;
    HF: TRVHeaderFooterRVData;
{$ENDIF}
begin
  {$IFNDEF RVDONOTUSESEQ}
  for IsHeader := False to True do
    for HFType := Low(TRVHFType) to High(TRVHFType) do begin
      HF := TPrintableRVData(rv.RVData).GetHeaderOrFooter(HFType, IsHeader);
      if HF<>nil then
        RVFreeAndNil(HF.Sidenotes);
    end;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TRVPrint.FormatPages(PrintOptions:TRVDisplayOptions): Integer;
begin
  if FPrintMe<>nil then
    FPrintMe.RVData.DoBeforeSaving;
  Result := 0;
  inc(FormattingID);
  if FormattingID=10000 then
    FormattingID := 0;
  ClearHeaderFooter;
  rv.InitFormatPages;
  if IsDestinationReady then begin
    {$IFNDEF RVDONOTUSESEQ}
    PreformatFootnotes;
    {$ENDIF}
    Result := rv.FormatPages;
    {$IFNDEF RVDONOTUSESEQ}
    PostformatFootnotes;
    inc(Result, FormatEndnotes);
    RVPrintFormatSidenotes(rv.RVData as TCustomMultiPagePtblRVData,
      rv.RVData as TPrintableRVData, rvhftNormal{ignored});
    {$ENDIF}
    rv.FinalizeFormatPages;
    Ready := True;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.Print(Title: String; Copies: Integer; Collate: Boolean);
begin
  TPrintableRV(rv).Print(Title, Copies, Collate);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.PrintPages(firstPgNo, lastPgNo: Integer; Title: String;
                              Copies: Integer; Collate: Boolean);
begin
  TPrintableRV(rv).PrintPages(firstPgNo, lastPgNo, Title, Copies, Collate);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.ContinuousPrint;
begin
  TPrintableRV(rv).ContinuousPrint;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.AssignSource(PrintMe: TCustomRichView);
begin
  FPrintMe := PrintMe;
  rv.RVData.ShareItemsFrom(PrintMe.RVData);
  rv.RVData.State := rv.RVData.State+[rvstSkipFormatting];
  try
    rv.LeftMargin   := PrintMe.LeftMargin;
    rv.RightMargin  := PrintMe.RightMargin;
    rv.TopMargin    := 0;
    rv.BottomMargin := 0;
    TPrintableRVData(rv.RVData).FTopMarginPix    := PrintMe.TopMargin;
    TPrintableRVData(rv.RVData).FBottomMarginPix := PrintMe.BottomMargin;
    rv.Style := PrintMe.Style;
    rv.DoInPaletteMode := PrintMe.DoInPaletteMode;
    rv.BackgroundBitmap := PrintMe.BackgroundBitmap;
    rv.BackgroundStyle := PrintMe.BackgroundStyle;
    rv.Color := PrintMe.Color;
    rv.BiDiMode := PrintMe.BiDiMode;
  finally
    rv.RVData.State := rv.RVData.State-[rvstSkipFormatting];
  end;
end;
{------------------------------------------------------------------------------}
// Readers for deprecated properties (previously published)
procedure TRVPrint.LMReader(reader: TReader);
begin
  LeftMarginMM := Reader.ReadInteger;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.TMReader(reader: TReader);
begin
  TopMarginMM := Reader.ReadInteger;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.RMReader(reader: TReader);
begin
  RightMarginMM := Reader.ReadInteger;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.BMReader(reader: TReader);
begin
  BottomMarginMM := Reader.ReadInteger;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.HYReader(reader: TReader);
begin
  HeaderYMM := Reader.ReadInteger;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.FYReader(reader: TReader);
begin
  FooterYMM := Reader.ReadInteger;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCPARAMS}
procedure TRVPrint.AssignDocParameters(DocParameters: TRVDocParameters);
begin
  Margins.Left   := RV_UnitsToUnits(DocParameters.LeftMargin,   DocParameters.Units, Units);
  Margins.Right  := RV_UnitsToUnits(DocParameters.RightMargin,  DocParameters.Units, Units);
  Margins.Top    := RV_UnitsToUnits(DocParameters.TopMargin,    DocParameters.Units, Units);
  Margins.Bottom := RV_UnitsToUnits(DocParameters.BottomMargin, DocParameters.Units, Units);
  HeaderY        := RV_UnitsToUnits(DocParameters.HeaderY,      DocParameters.Units, Units);
  FooterY        := RV_UnitsToUnits(DocParameters.FooterY,      DocParameters.Units, Units);
  MirrorMargins  := DocParameters.MirrorMargins;
  FacingPages    := DocParameters.FacingPages;
  TitlePage      := DocParameters.TitlePage;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVPrint.SetHeader(RVData: TCustomRVFormattedData; HeaderType: TRVHFType);
var PHeader: ^TRVHeaderFooterRVData;
begin
  case HeaderType of
    rvhftNormal:
      PHeader := @(TPrintableRVData(rv.RVData).Header);
    rvhftFirstPage:
      PHeader := @(TPrintableRVData(rv.RVData).FirstPageHeader);
    rvhftEvenPages:
      PHeader := @(TPrintableRVData(rv.RVData).EvenPagesHeader);
    else
      exit;
  end;
  RVFreeAndNil(PHeader^);
  if RVData<>nil then begin
    PHeader^ :=
      TRVHeaderFooterRVData.Create(rv, RVData, rv.RVData as TCustomPrintableRVData);
    PHeader^.FLeftMargin := RVData.GetLeftMargin;
    PHeader^.FRightMargin := RVData.GetRightMargin;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.SetFooter(RVData: TCustomRVFormattedData; FooterType: TRVHFType);
var PFooter: ^TRVHeaderFooterRVData;
begin
  case FooterType of
    rvhftNormal:
      PFooter := @(TPrintableRVData(rv.RVData).Footer);
    rvhftFirstPage:
      PFooter := @(TPrintableRVData(rv.RVData).FirstPageFooter);
    rvhftEvenPages:
      PFooter := @(TPrintableRVData(rv.RVData).EvenPagesFooter);
    else
      exit;
  end;
  RVFreeAndNil(PFooter^);
  if RVData<>nil then begin
    PFooter^ :=
      TRVHeaderFooterRVData.Create(rv, RVData, rv.RVData as TCustomPrintableRVData);
    PFooter^.FLeftMargin := RVData.GetLeftMargin;
    PFooter^.FRightMargin := RVData.GetRightMargin;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.MakeScaledPreview(pgNo: Integer; bmp: TBitmap);
begin
  DrawPreview(pgNo, bmp.Canvas, Rect(0,0,bmp.Width, bmp.Height));
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.MakePreview(pgNo: Integer; bmp: TBitmap);
var w,h: Integer;
begin
   w := Preview100PercentWidth;
   h := Preview100PercentHeight;

   if bmp.Width <> w then bmp.Width := w;
   if bmp.Height <> h then bmp.Height := h;
   MakeScaledPreview(pgNo,bmp);
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetFooterRect(PageNo: Integer): TRect;
begin
  if TPrintableRVData(rv.RVData).Footer=nil then
    Result := Rect(0,0,0,0)
  else
    with TPrintableRVData(rv.RVData).GetFooterForPage(PageNo) do
      Result := Bounds(Left+DX,Top+DY,Width,DocumentHeight);
end;
{------------------------------------------------------------------------------}
function TRVPrint.GetHeaderRect(PageNo: Integer): TRect;
begin
  if TPrintableRVData(rv.RVData).Header=nil then
    Result := Rect(0,0,0,0)
  else
    with TPrintableRVData(rv.RVData).GetHeaderForPage(PageNo) do
      Result := Bounds(Left+DX,Top+DY,Width,DocumentHeight);
end;
{------------------------------------------------------------------------------}
procedure TRVPrint.ConvertToUnits(AUnits: TRVUnits);
begin
  if AUnits<>Units then begin
    Margins.ConvertToUnits(Units, AUnits);
    HeaderY   := RV_UnitsToUnits(HeaderY, Units, AUnits);
    FooterY   := RV_UnitsToUnits(FooterY, Units, AUnits);
    Units := AUnits;
  end;
end;
{======================== TCustomPrintableRV ==================================}
constructor TCustomPrintableRV.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Visible := False;
  Flags := Flags - [rvflUseJumps, rvflCanUseCustomPPI, rvflCanProcessGetText] +
    [rvflPrinting,rvflShareContents,rvflAllowCustomDrawItems];
  TopMargin    := 0;     // do not change!
  BottomMargin := 0;  // do not change!
  BorderStyle  := bsNone;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
destructor TCustomPrintableRV.Destroy;
begin
  FreeNotesLists;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.FreeNotesLists;
begin
  RVFreeAndNil(FEndNotes);
  RVFreeAndNil(FFootNotes);
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRV.GetFootnoteRVData(
  Footnote: TRVFootnoteItemInfo): TRVFootnotePtblRVData;
begin
  Result := FFootnotes.FindByFootnote(Footnote);
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.CalcFootnotesCoords(References: TList; PageNo: Integer);
var i, y: Integer;
    FootnoteData: TRVFootnotePtblRVData;
    DrawItem: TRVDrawLineInfo;
begin
  if References=nil then
    exit;
  // Calculating coordinates of footnotes, assigning numbers (if restarted on page)
  TRVFootnoteRefList(References).Sort;
  y := TCustomMainPtblRVData(RVData).GetPageHeight-TCustomMainPtblRVData(RVData).GetTmpMForPage(PageNo).Bottom;
  for i := References.Count-1 downto 0 do begin
    FootnoteData := TRVFootnotePtblRVData(References[i]);
    dec(y, FootnoteData.DocumentHeight);
    FootnoteData.Top := y;
    FootnoteData.Left :=
      TCustomMainPtblRVData(RVData).GetPrintableAreaLeft(PageNo)+
      RV_XToDevice(LeftMargin, TCustomMainPtblRVData(RVData).PrnSad);
    FootnoteData.IndexOnPage := i+1;
  end;
  if not Style.FootnotePageReset then
    exit;
  // Adjusting widths of footnote characters and references to them
  for i := 0 to References.Count-1 do begin
    FootnoteData := TRVFootnotePtblRVData(References[i]);
    DrawItem := FootnoteData.FootnoteItemRVData.DrawItems[FootnoteData.FootnoteDItemNo];
    FootnoteData.FootnoteItemRVData.ChangeDItemWidth(FootnoteData.FootnoteDItemNo,
      FootnoteData.Footnote.GetFinalPrintingWidth(
        TCustomMainPtblRVData(RVData).PrinterCanvas, DrawItem, FootnoteData.FootnoteItemRVData));
    FootnoteData.AdjustFootnoteRefWidths;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.DrawNoteSeparatorAbove(PageNo, Y: Integer;
  Canvas: TCanvas; FullSize: Boolean);
var X, Width: Integer;
begin
  X := TCustomMainPtblRVData(RVData).GetPrintableAreaLeft(PageNo)+
    RV_XToDevice(LeftMargin, TCustomMainPtblRVData(RVData).PrnSad);
  Canvas.Pen.Width := FNoteLineWidth;
  Canvas.Pen.Style := psInsideFrame;
  Canvas.Pen.Color := clGray;
  //Canvas.Rectangle(X,Y,X+100,Y-FNoteSeparatorHeight);
  //exit;
  dec(Y, FNoteSeparatorHeight div 2);
  Width := Self.Width - RV_XToDevice(RightMargin+LeftMargin,
    TCustomMainPtblRVData(RVData).PrnSad);
  if not FullSize then begin
    if BiDiMode=rvbdRightToLeft then
      inc(X, Width - (Width div 3));
    Width := Width div 3;
  end;
  Style.GraphicInterface.MoveTo(Canvas, X+(FNoteSeparatorHeight div 2), Y);  
  inc(X, Width-FNoteSeparatorHeight);
  Style.GraphicInterface.LineTo(Canvas, X, Y);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomPrintableRV.CanUseCustomPPI: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.InitFormatPages;
begin
  RVData.State := RVData.State+[rvstSkipformatting];
  try
    VScrollVisible := False;
    HScrollVisible := False;
  finally
    RVData.State := RVData.State-[rvstSkipformatting];
  end;
  {$IFNDEF RVDONOTUSESEQ}
  FreeNotesLists;
  {$ENDIF}
  TPrintableRVData(RVData).InitFormatPages;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.Paint;
begin
  // drawing nothing
end;
{------------------------------------------------------------------------------}
function TCustomPrintableRV.FormatPages: Integer;
begin
  Result := TPrintableRVData(RVData).FormatPages;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.FinalizeFormatPages;
begin
  TPrintableRVData(RVData).FinalizeFormatPages;
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.DrawPage(PageNo: Integer; Canvas: TCanvas;
  Preview, Correction: Boolean);
{$IFNDEF RVDONOTUSESEQ}
var LastItemNo: Integer;
{$ENDIF}
begin
  if (PageNo<RVPrint.FirstPageNo) or (PageNo>RVPrint.LastPageNo) then
    exit;
  TPrintableRVData(RVData).DrawPage(PageNo, PageNo, Canvas, Preview, Correction);
  {$IFNDEF RVDONOTUSESEQ}
  LastItemNo := RVData.ItemCount-1;
  if (FEndnotes<>nil) and
     (PageNo>=TPrintableRVData(RVData).Pages.Count) and
     (LastItemNo>=RVPrint.MinPrintedItemNo) and
     ((RVPrint.MaxPrintedItemNo<0) or (LastItemNo<=RVPrint.MaxPrintedItemNo)) then
    FEndnotes.DrawPage(PageNo,
      PageNo-TPrintableRVData(RVData).Pages.Count, Canvas, Preview, Correction);
  {$ENDIF}  
end;
{------------------------------------------------------------------------------}
procedure TCustomPrintableRV.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.Style := (Params.Style and not WS_CHILD) or WS_POPUP;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
procedure TCustomPrintableRV.ApplyLayoutInfo(Layout: TRVLayoutInfo);
begin
  inherited;
  if Layout.FirstParaAborted<>0 then begin
    Include(RVData.State, rvstFirstParaAborted);
    TCustomMainPtblRVData(RVData).FFirstParaListNo := Layout.FirstMarkerListNo;
    TCustomMainPtblRVData(RVData).FFirstParaLevel := Layout.FirstMarkerLevel;
    end
  else begin
    Exclude(RVData.State, rvstFirstParaAborted);
    TCustomMainPtblRVData(RVData).FFirstParaListNo := -1;
    TCustomMainPtblRVData(RVData).FFirstParaLevel := -1;
  end;
  if Layout.LastParaAborted<>0 then
    Include(RVData.State, rvstLastParaAborted)
  else
    Exclude(RVData.State, rvstLastParaAborted);
end;
{$ENDIF}
{============================= TPrintableRV ===================================}
constructor TPrintableRV.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMargins := TRVUnitsRect.Create(Self);
end;
{------------------------------------------------------------------------------}
destructor TPrintableRV.Destroy;
begin
  RVFreeAndNil(FMargins);
  inherited;
end;
{------------------------------------------------------------------------------}
function TPrintableRV.GetDataClass: TRichViewRVDataClass;
begin
  Result := TPrintableRVData;
end;
{------------------------------------------------------------------------------}
procedure TPrintableRV.ContinuousPrint;
var i: Integer;
begin
  for i := 1 to TPrintableRVData(RVData).Pages.Count do begin
    if i<>1 then
      Printer.NewPage;
    DrawPage(i, Printer.Canvas,False,False);
  end;
end;
{------------------------------------------------------------------------------}
procedure TPrintableRV.DoOnPrinting(PageCompleted: Integer; Step:TRVPrintingStep);
begin
  if Assigned(TRVPrint(RVPrint).FOnPrinting) then
    TRVPrint(RVPrint).FOnPrinting(Self, PageCompleted, Step);
end;
{------------------------------------------------------------------------------}
procedure TPrintableRV.PrintPages(firstPgNo, lastPgNo: Integer;
  const Title: String; Copies: Integer; Collate: Boolean);
var i,copyno: Integer;
    PrinterCopies: Integer;
begin
   if firstPgNo<RVPrint.FirstPageNo then
     firstPgNo := RVPrint.FirstPageNo;
   if lastPgNo>RVPrint.LastPageNo then
     lastPgNo := RVPrint.LastPageNo;
   if lastPgNo<firstPgNo then
     exit;
   FRVPrint.StartAt := 0;
   DoOnPrinting(0, rvpsStarting);
   Printer.Title := Title;
   PrinterCopies := Printer.Copies; { storing }
   {
   // unfortunately, Printer.Capabilities are not updated correctly
   // when the printer is changed in a dialog, so we cannot rely on it
   if (pcCopies in Printer.Capabilities) and not Collate then
     begin
       Printer.Copies := Copies;
                                 // Printer can make copies if needed
       Copies := 1;              // TRichView does not need to support copies itself
     end
   else
   }
     Printer.Copies := 1;        // TRichView will provide copies and collation itself
   Printer.BeginDoc;
   if Collate then
     for copyno:= 1 to Copies do
       for i := firstPgNo to lastPgNo do begin
         DrawPage(i, Printer.Canvas,False,False);
         DoOnPrinting(i, rvpsProceeding);
         if not ((i=lastPgNo) and (copyno=Copies)) then
           Printer.NewPage;
       end
   else
     for i := firstPgNo to lastPgNo do
       for copyno:= 1 to Copies do begin
         DrawPage(i, Printer.Canvas,False,False);
         DoOnPrinting(i, rvpsProceeding);
         if not ((i=lastPgNo) and (copyno=Copies)) then
           Printer.NewPage;
       end;
   Printer.EndDoc;
   Printer.Copies := PrinterCopies; { restoring }
   DoOnPrinting(0, rvpsFinished);
end;
{------------------------------------------------------------------------------}
procedure TPrintableRV.Print(const Title: String; Copies: Integer; Collate: Boolean);
begin
   PrintPages(RVPrint.GetFirstPageNo, RVPrint.GetLastPageNo, Title, Copies, Collate);
end;
{$IFNDEF RVDONOTUSESEQ}
{=========================== TRVEndnotePageList ===============================}
function TRVEndnotePageList.GetItems(Index: Integer): TRVEndnotePage;
begin
  Result := TRVEndnotePage(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVEndnotePageList.SetItems(Index: Integer;
  const Value: TRVEndnotePage);
begin
  inherited Put(Index, Value);
end;
{=============================== TRVEndnoteList ===============================}
constructor TRVEndnoteList.Create(AOwner: TCustomRVPrint);
begin
  inherited Create;
  FOwner := AOwner;
  Pages := TRVEndnotePageList.Create;
end;
{------------------------------------------------------------------------------}
destructor TRVEndnoteList.Destroy;
begin
  Pages.Free;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TRVEndnoteList.DrawPage(GlobalPageNo, PageNo: Integer; Canvas: TCanvas;
  Preview, Correction: Boolean);
var i, StartAt: Integer;
begin
  i := Pages[PageNo].Index;
  if i<0 then
    exit;
  if (PageNo=0) or (Pages[PageNo].Page>1) or
     Items[i].FromNewPage then begin
    if Pages[PageNo].Page<=1 then
      StartAt := Items[i].StartAt
    else
      StartAt := Items[i].NextStartAt;
    FOwner.rv.DrawNoteSeparatorAbove(GlobalPageNo,
      TCustomMainPtblRVData(FOwner.rv.RVData).GetTmpMForPage(GlobalPageNo).Top+
      TCustomMainPtblRVData(FOwner.rv.RVData).TmpTMPix+
      StartAt, Canvas, (Pages[PageNo].Page>1) or Items[i].FromNewPage);
  end;
  Items[i].DrawPage(Pages[PageNo].Page, GlobalPageNo, Canvas, Preview,
    Correction);
  if Items[i].Pages.Count>Pages[PageNo].Page then
    exit;
  for i := Pages[PageNo].Index+1 to Count-1 do begin
    if Items[i].FromNewPage then
      break;
    Items[i].DrawPage(1, GlobalPageNo, Canvas, Preview, Correction);
    if Items[i].Pages.Count>1 then
      break;
  end;
end;
{------------------------------------------------------------------------------}
function TRVEndnoteList.GetItems(Index: Integer): TRVEndnotePtblRVData;
begin
  Result := TRVEndnotePtblRVData(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVEndnoteList.SetItems(Index: Integer;
  const Value: TRVEndnotePtblRVData);
begin
  inherited Put(Index, Value);
end;
{================================ TRVFootnoteList =============================}
constructor TRVFootnoteList.Create(AOwner: TCustomRVPrint);
begin
  inherited Create;
  FOwner := AOwner;
end;
{------------------------------------------------------------------------------}
function TRVFootnoteList.GetItems(Index: Integer): TRVFootnotePtblRVData;
begin
  Result := TRVFootnotePtblRVData(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteList.SetItems(Index: Integer;
  const Value: TRVFootnotePtblRVData);
begin
  inherited Put(Index, Value);
end;
{------------------------------------------------------------------------------}
function CompareFootnoteRVData(Item1, Item2: Pointer): Integer;
begin
  Result := Integer(TRVFootnotePtblRVData(Item1).Footnote)-Integer(TRVFootnotePtblRVData(Item2).Footnote);
end;
{------------------------------------------------------------------------------}
function CompareFootnoteRVDataAndFootnote(Item1, Item2: Pointer): Integer;
begin
  Result := Integer(TRVFootnotePtblRVData(Item1).Footnote)-Integer(Item2);
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteList.SortByFootnotes;
begin
  Sort(CompareFootnoteRVData);
end;
{------------------------------------------------------------------------------}
function TRVFootnoteList.FindByFootnote(
  Footnote: TRVFootnoteItemInfo): TRVFootnotePtblRVData;
var Index: Integer;
begin
  Index := Find(Footnote, CompareFootnoteRVDataAndFootnote);
  if Index<0 then
    Result := nil
  else
    Result := Items[Index];
end;

{$ENDIF}

end.

