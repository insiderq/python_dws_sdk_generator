﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDataList.pas' rev: 27.00 (Windows)

#ifndef RvdatalistHPP
#define RvdatalistHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdatalist
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVItemFormattedData;
class DELPHICLASS TRVDataList;
class PASCALIMPLEMENTATION TRVItemFormattedData : public Crvfdata::TCustomRVFormattedData
{
	typedef Crvfdata::TCustomRVFormattedData inherited;
	
protected:
	TRVDataList* FList;
	virtual Crvdata::TRVFlags __fastcall GetFlags(void);
	virtual void __fastcall SetFlags(const Crvdata::TRVFlags Value);
	
public:
	__fastcall TRVItemFormattedData(TRVDataList* AList);
	DYNAMIC void __fastcall ShowRectangle(int Left, int Top, int Width, int Height);
	virtual void __fastcall ScrollTo(int Y, bool Redraw);
	virtual void __fastcall HScrollTo(int X);
	virtual int __fastcall GetVSmallStep(void);
	virtual Crvdata::TCustomRVData* __fastcall GetParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetRootData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteRootData(void);
	DYNAMIC System::UnicodeString __fastcall GetURL(int id);
	virtual int __fastcall GetAreaWidth(void);
	virtual int __fastcall GetAreaHeight(void);
	virtual int __fastcall GetMinTextWidth(void);
	virtual int __fastcall GetMaxTextWidth(void);
	virtual int __fastcall GetLeftMargin(void);
	virtual int __fastcall GetRightMargin(void);
	virtual int __fastcall GetTopMargin(void);
	virtual int __fastcall GetBottomMargin(void);
	virtual void __fastcall AdjustVScrollUnits(void);
	virtual void __fastcall SetDocumentAreaSize(int Width, int Height, bool UpdateH);
	virtual Rvback::TRVBackground* __fastcall GetBackground(void);
	DYNAMIC bool __fastcall IsAssignedRVMouseDown(void);
	DYNAMIC bool __fastcall IsAssignedRVMouseUp(void);
	DYNAMIC bool __fastcall IsAssignedJump(void);
	DYNAMIC bool __fastcall IsAssignedCheckpointVisible(void);
	DYNAMIC int __fastcall GetFirstItemVisible(void);
	DYNAMIC int __fastcall GetLastItemVisible(void);
	DYNAMIC void __fastcall DoRVMouseMove(int id);
	DYNAMIC void __fastcall DoRVMouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);
	DYNAMIC void __fastcall DoRVMouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);
	DYNAMIC void __fastcall DoCheckpointVisible(Rvstyle::TCheckpointData CheckpointData);
	DYNAMIC void __fastcall ControlAction2(Crvdata::TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Vcl::Controls::TControl* &Control);
	DYNAMIC void __fastcall DoJump(int id);
	DYNAMIC System::Uitypes::TCursor __fastcall GetNormalCursor(void);
public:
	/* TCustomRVFormattedData.Destroy */ inline __fastcall virtual ~TRVItemFormattedData(void) { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDataList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
protected:
	Crvdata::TRVFlags Flags;
	virtual Crvdata::TCustomRVData* __fastcall GetParentRVData(void);
	
public:
	__fastcall TRVDataList(Crvdata::TCustomRVData* AParentRVData);
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVDataList(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvdatalist */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDATALIST)
using namespace Rvdatalist;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdatalistHPP
