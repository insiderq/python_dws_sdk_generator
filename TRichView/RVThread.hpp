﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVThread.pas' rev: 27.00 (Windows)

#ifndef RvthreadHPP
#define RvthreadHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvthread
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVWordEnumThread;
class PASCALIMPLEMENTATION TRVWordEnumThread : public System::Classes::TThread
{
	typedef System::Classes::TThread inherited;
	
private:
	int NextItemNo;
	int NextOffs;
	int StopWorking;
	System::UnicodeString CurText;
	System::WideChar *CurTextPtr;
	System::WideChar *CurTextStartPtr;
	Rvscroll::TRVScroller* FRichView;
	int CurItemNo;
	int WordOffs;
	int WordLen;
	bool CurItemCheckStarted;
	Crvdata::TCustomRVData* NextRVData;
	Crvdata::TCustomRVData* CurRVData;
	bool FForceSetBack;
	bool Delaying;
	bool EditedWhileDelayed;
	void __fastcall AddMisspelling(void);
	void __fastcall SyncProc(void);
	System::UnicodeString __fastcall GetNextWord(void);
	void __fastcall GetMisspellingDrawItems(int &DItemNo1, int &DItemNo2);
	void __fastcall SetBack(void);
	
protected:
	virtual void __fastcall Execute(void);
	virtual void __fastcall DoTerminate(void);
	
public:
	bool HasModifiedWord;
	bool CheckUnchecked;
	__fastcall TRVWordEnumThread(void);
	void __fastcall Reset(Rvscroll::TRVScroller* RichView);
	void __fastcall LaterSetBackTo(Crvdata::TCustomRVData* RVData, int ItemNo, int Offs);
	void __fastcall SetBackToCurItem(Crvdata::TCustomRVData* RVData, int ItemNo);
	bool __fastcall IsChecked(Crvdata::TCustomRVData* RVData, int ItemNo);
	void __fastcall RemoveRVData(Crvdata::TCustomRVData* RVData);
	__fastcall virtual ~TRVWordEnumThread(void);
	void __fastcall Stop(bool ResetNexts, bool IgnoreDelayed);
	void __fastcall ContinueCheck(void);
	void __fastcall DoContinueCheck(void);
	void __fastcall ContinueCheckWithoutResuming(void);
	void __fastcall Finish(void);
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool RichViewApostropheInWord;
}	/* namespace Rvthread */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVTHREAD)
using namespace Rvthread;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvthreadHPP
