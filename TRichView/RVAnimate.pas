{*******************************************************}
{                                                       }
{       RichView                                        }
{       Displaying animations in TRichView.             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

{$I RV_Defs.inc}

unit RVAnimate;

interface

{$IFNDEF RVDONOTUSEANIMATION}

uses Classes, Windows, Graphics, Controls, CRVData,
     DLines, RVItem, CRVFData, RVStyle, RVGrIn;

type
  TRVAnimatorList = class;

  { ---------------------------------------------------------------------------
    TRVAnimator: abstract base class for all animators in TRichView
  }
  TRVAnimator = class
    private
      FRVRVDataRect, FRVClientRect, FSRVClientRect: TRect;
      FZoomPercent: Single;
      List: TRVAnimatorList;
    protected
      FrameIndex: Integer;
      Item: TCustomRVItemInfo;
      Interval: Integer;
      Moved: Boolean;
      function GetFrameCount: Integer; virtual; abstract;
      function IsVisible: Boolean;
      procedure CalcNextFrameIndex;
      procedure DrawFrame;
      procedure ResetBackground; virtual;
      procedure MakeBackground(var bmp: TBitmap; var BackColor: TColor);
    public
      RVData: TCustomRVFormattedData;
      constructor Create(ARVData: TCustomRVFormattedData; AItem: TCustomRVItemInfo);
      procedure Update(ARVData: TCustomRVFormattedData; AItem: TCustomRVItemInfo);
      destructor Destroy; override;
      procedure Reset; virtual; abstract;
      procedure ChangeFrame; virtual; abstract;
      procedure Draw(X, Y: Integer; Canvas: TCanvas; Animation: Boolean); virtual; abstract;
      function GetExportImageSize: TSize; virtual;
      procedure DrawForExport(Canvas: TCanvas); virtual; abstract;
      function ExportIgnoresScale: Boolean; virtual;
  end;
  { ---------------------------------------------------------------------------
    TRVAnimatorList: list of animators (classes inherited from TRVAnimator).
    An object of this class is contained in TRichViewRVData (FAnimatorList) for
    root richviews.
  }
  TRVAnimatorList = class (TList)
    public
      Active: Boolean;
      LastMinInterval,
      MinInterval: Integer;
      constructor Create;
      destructor Destroy; override;
      procedure TimerEvent;
      procedure Clear; {$IFDEF RICHVIEWDEF4}override;{$ENDIF}
      procedure FreeAnimators;
      procedure Reset;
      procedure Add(var Item: TRVAnimator);
      procedure ResetBackground;
  end;
  { ---------------------------------------------------------------------------
    TRVBitmapAnimator: displaying animations for bitmaps.
    The source bitmap is sliced into frames (ImageWidth x ImageHeight).
    This animator is created for graphic items having nonzero Interval property
    (interval is a time to display one frame, in 1/100 sec.)
  }
  TRVBitmapAnimator = class (TRVAnimator)
    protected
      function GetFrameCount: Integer; override;
    public
      procedure Reset; override;
      procedure ChangeFrame; override;
      procedure Draw(X, Y: Integer; Canvas: TCanvas; Animation: Boolean); override;
      procedure DrawForExport(Canvas: TCanvas); override;
      function ExportIgnoresScale: Boolean; override;
  end;

  { Type of procedure to create animators }
  TRVMakeAnimatorProc = procedure (item: TCustomRVItemInfo; RVData: TCustomRVFormattedData;
      var anim: TRVAnimator);
  { Variable pointing to such procedure }
  var RV_MakeAnimator: TRVMakeAnimatorProc;

  { Maximal possible number of animations in one richview }
const RVMaxAnimations: Integer = 100000;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSEANIMATION}
uses
  {$IFDEF RICHVIEWDEF10}
  Types,
  {$ENDIF}
  RichView, RVFuncs;
{================================== TRVAnimator ===============================}
{ Constructor }
constructor TRVAnimator.Create(ARVData: TCustomRVFormattedData; AItem: TCustomRVItemInfo);
begin
  inherited Create;
  Update(ARVData, AItem);
end;
{------------------------------------------------------------------------------}
{ Destructor. Removes itself from the list. }
destructor TRVAnimator.Destroy;
begin
  if List<>nil then begin
    List.Remove(Self);
    if List.Count=0 then
      List.Active := False;
  end;
  if Item<>nil then
    Item.RemoveLinkToAnimator;
  inherited;
end;
{------------------------------------------------------------------------------}
{ Advances FrameIndex }
procedure TRVAnimator.CalcNextFrameIndex;
begin
  inc(FrameIndex);
  if FrameIndex>=GetFrameCount then
    FrameIndex := 0;
end;
{------------------------------------------------------------------------------}
{ Returns image size used when exporting to RTF }
function TRVAnimator.GetExportImageSize: TSize;
begin
  Result.cx := Item.GetImageWidth(RVData.GetRVStyle);
  Result.cy := Item.GetImageHeight(RVData.GetRVStyle);
end;
{------------------------------------------------------------------------------}
{ Returns True if RTF export must ignore ImageWidth and ImageHeight item
  properties }
function TRVAnimator.ExportIgnoresScale: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
{ Sets new values of RVData and Item.
  If ARVData parameter = nil, it's not changed. }
procedure TRVAnimator.Update(ARVData: TCustomRVFormattedData; AItem: TCustomRVItemInfo);
begin
  if ARVData<>nil then
    RVData := ARVData;
  Item  := AItem;
end;
{------------------------------------------------------------------------------}
procedure TRVAnimator.MakeBackground(var bmp: TBitmap; var BackColor: TColor);
var r: TRect;
    UseBmp: Boolean;
    Rotation: TRVDocRotation;
    XForm: TXForm;
    bmp2: TBitmap;
    GraphicInterface: TRVGraphicInterface;
    X, Y: Integer;
    Width, Height: Integer;
    VAlign: TRVVerticalAlign;    
begin
  if (Item is TRVRectItemInfo) and (TRVRectItemInfo(Item).BackgroundColor<>clNone) then begin
    BackColor := TRVRectItemInfo(Item).BackgroundColor;
    RVFreeAndNil(bmp);
    exit;
  end;
  RVData.GetItemBackground(RVData.DrawItems[item.DrawItemNo].ItemNo, r, True,
    BackColor, bmp, UseBmp, RVData.GetRVStyle.GraphicInterface, True, True);
  if not UseBmp then begin
    RVFreeAndNil(bmp);
    exit;
  end;
  Rotation := RVData.GetRotation;
  if Rotation=rvrotNone then
    exit;
  if Rotation=rvrot270 then
    Rotation := rvrot90
  else if Rotation=rvrot90 then
    Rotation := rvrot270;

  bmp2 := TBitmap.Create;
  if Rotation = rvrot180 then begin
    bmp2.Width := bmp.Width-2;
    bmp2.Height := bmp.Height-2;
    end
  else begin
    bmp2.Width := bmp.Height-2;
    bmp2.Height := bmp.Width-2;
  end;    

   if Rotation = rvrot90 then
     Height := bmp2.Width
   else
     Height := bmp2.Height;
   Width := bmp.Width;


  case Rotation of
    rvrot180, rvrot90:
      VAlign := rvveraBottom
    else
      VAlign := rvveraTop;
  end;
  RVFillRotationMatrix(XForm, Rotation, 0, 0, Width, Height, Height, VAlign);
  GraphicInterface := RVData.GetRVStyle.GraphicInterface;
  GraphicInterface.SetGraphicsMode(bmp2.Canvas, True);
  GraphicInterface.SetWorldTransform(bmp2.Canvas, XForm);

  case Rotation of
    rvrot180:
      begin
        X := 1;
        Y := -1;
      end;
    rvrot90:
      begin
        X := -1;
        Y := -1;
      end;
    else // rvrot270
      begin
        X := -1;
        Y := -1;
      end;
  end;

  bmp2.Canvas.Draw(X, Y, bmp);
  GraphicInterface.SetGraphicsMode(bmp2.Canvas, False);
  bmp.Free;
  bmp := bmp2;
end;
{------------------------------------------------------------------------------}
type
  TCustomControlHack = class(TCustomControl)
  end;
{------------------------------------------------------------------------------}
{ Draws the current frame of animation }
procedure TRVAnimator.DrawFrame;
var x, y: Integer;
    ditem: TRVDrawLineInfo;
    Ctrl: TControl;
    OldState: TRVStoreDocTransform;
    Canvas: TCanvas;
    Ifc: IRVScaleRichViewInterface;
    GraphicInterface: TRVGraphicInterface;
    VAlign: TRVVerticalAlign;
    Rotation: TRVDocRotation;
    Height: Integer;
    State: Integer;
    ClientRect: TRect;
begin
  if (Item.DrawItemNo<0) or (Item.DrawItemNo>=TCustomRVFormattedData(RVData.GetRVData).DrawItems.Count) then
    exit;
  TCustomRVFormattedData(RVData.GetSourceRVData).ResetSubCoords;
  Ifc := RVData.GetScaleRichViewInterface;
  GraphicInterface := RVData.GetRVStyle.GraphicInterface;
  if Ifc<>nil then begin
    // Drawing in TSRichViewEdit
    Canvas := TCustomControlHack(Ifc.GetSRichViewEdit).Canvas;
    State := GraphicInterface.SaveCanvasState(Canvas);
    ClientRect := Ifc.GetClientRect;
    with ClientRect do
      GraphicInterface.IntersectClipRect(Canvas, Left, Top, Right, Bottom);
    GraphicInterface.SetMapMode(Canvas, MM_ANISOTROPIC);
    GraphicInterface.SetWindowOrgEx(Canvas, 0, 0, nil);
    GraphicInterface.SetViewPortOrgEx(Canvas, FSRVClientRect.Left, FSRVClientRect.Top, nil);
    GraphicInterface.SetWindowExtEx(Canvas, 100, 100, nil);
    GraphicInterface.SetViewPortExtEx(Canvas, Round(FZoomPercent), Round(FZoomPercent), nil);
    Rotation := RVData.GetRealRotation(True);
    if Rotation<>rvrotNone then begin
      if Rotation in [rvrot90, rvrot270] then
        Height := FRVRVDataRect.Right-FRVRVDataRect.Left
      else
        Height := FRVRVDataRect.Bottom-FRVRVDataRect.Top;
        case Rotation of
          rvrot180, rvrot90:
            VAlign := rvveraBottom
          else
            VAlign := rvveraTop;
        end;
      RVData.DoPrepareTransform(Canvas, OldState, 0, 0, 0, 0,
        FRVRVDataRect.Right-FRVRVDataRect.Left,
        Height,
        FRVRVDataRect.Bottom-FRVRVDataRect.Top,
        VAlign, rvtrbAbsRoot);
    end;
    Draw(
      RVData.GetRVStyle.GetAsPixels(item.GetBorderWidth),
      RVData.GetRVStyle.GetAsPixels(item.GetBorderHeight),
      Canvas, True);
    GraphicInterface.RestoreCanvasState(Canvas, State);
    exit;
  end;
  // Drawing in TRichView
  Ctrl := RVData.GetParentControl;
  Canvas := TCustomRichView(Ctrl).Canvas;
  RVData.ResetSubCoords;
  RVData.GetOrigin(x,y);
  RVData.PrepareTransform(Canvas, OldState, 0, 0, rvtrbRecursive, -1);
  try
    ditem := TCustomRVFormattedData(RVData.GetRVData).DrawItems[Item.DrawItemNo];
    inc(x, ditem.ObjectLeft);
    inc(y, ditem.ObjectTop);
    if Ctrl<>nil then begin
      dec(x, TCustomRichView(Ctrl).HScrollPos);
      dec(y, TCustomRichView(Ctrl).VScrollPos*TCustomRichView(Ctrl).VSmallStep);
    end;
    Draw(
      x+RVData.GetRVStyle.GetAsPixels(item.GetBorderWidth),
      y+RVData.GetRVStyle.GetAsPixels(item.GetBorderHeight),
      Canvas, True);
  finally
    RVData.RestoreTransform(Canvas, OldState);
  end;
end;
{------------------------------------------------------------------------------}
{ A place to invalidate stored background images. }
procedure TRVAnimator.ResetBackground;
begin

end;
{------------------------------------------------------------------------------}
{ Is this animation visible? }
function TRVAnimator.IsVisible: Boolean;
var Ctrl: TCustomRichView;
    DrawItem: TRVDrawLineInfo;
    Ifc: IRVScaleRichViewInterface;
    R: TRect;
begin
  if (Item.DrawItemNo<0) or
     (Item.DrawItemNo>=TCustomRVFormattedData(RVData.GetRVData).DrawItems.Count) then begin
    Result := False;
    exit;
  end;
  DrawItem := TCustomRVFormattedData(RVData.GetRVData).DrawItems[item.DrawItemNo];
  FRVRVDataRect := Bounds(DrawItem.ObjectLeft, DrawItem.Top, DrawItem.ObjectWidth, DrawItem.ObjectHeight);
  R := FRVRVDataRect;
  RVData.RotateRectFromDocToScreen(R, rvtrbAbsRoot);
  Ctrl := TCustomRichView(RVData.GetAbsoluteRootData.GetParentControl);
  R.TopLeft := Ctrl.DocumentToClient(R.TopLeft);
  R.BottomRight := Ctrl.DocumentToClient(R.BottomRight);
  Moved := (R.Left<>FRVClientRect.Left) or (R.Top<>FRVClientRect.Top) or
    (R.Right<>FRVClientRect.Right) or (R.Bottom<>FRVClientRect.Bottom);
  FRVClientRect := R;
  Ifc := RVData.GetScaleRichViewInterface;
  if Ifc<>nil then begin
    FSRVClientRect := FRVClientRect;
    Result := Ifc.IsAniRectVisible(Ctrl, FSRVClientRect, FZoomPercent)
    end
  else
    Result := (FRVClientRect.Bottom>0) and (FRVClientRect.Top<Ctrl.ClientHeight);
end;
{================================ TRVAnimatorList =============================}
{ Constructor }
constructor TRVAnimatorList.Create;
begin
  inherited Create;
  LastMinInterval := MaxInt;
  MinInterval     := MaxInt;
end;
{ Destructor }
destructor TRVAnimatorList.Destroy;
begin
  Clear;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
{ Adds a new item.
  If the number of items exceeds the maximum, frees the Item instead. }
procedure TRVAnimatorList.Add(var Item: TRVAnimator);
begin
  if Count=RVMaxAnimations then begin
    RVFreeAndNil(Item);
    exit;
  end;
  Item.Reset;
  if MinInterval>Item.Interval then
    MinInterval := Item.Interval;
  inherited Add(Item);
  Item.List := Self;
end;
{------------------------------------------------------------------------------}
{ Clears the list. Important: animators are not destroyed, just unlinked from
  the list. }
procedure TRVAnimatorList.Clear;
var i: Integer;
begin
  for i := 0 to Count-1 do
    TRVAnimator(Items[i]).List := nil;
  inherited Clear;
  Active := False;
end;
{------------------------------------------------------------------------------}
{ Clears the list, destroying all animators. }
procedure TRVAnimatorList.FreeAnimators;
var i: Integer;
begin
  for i := 0 to Count-1 do begin
    TRVAnimator(Items[i]).List := nil;
    TRVAnimator(Items[i]).Free;
  end;
  inherited Clear;
  Active := False;
end;
{------------------------------------------------------------------------------}
{ Calls ResetBackground of all items. }
procedure TRVAnimatorList.ResetBackground;
var i: Integer;
begin
  for i := 0 to Count-1 do
    TRVAnimator(Items[i]).ResetBackground;
end;
{------------------------------------------------------------------------------}
{ A procedure to call regularly on timer.
  It's called when MinInterval is elapsed.
  Decreases Interval of all animators by MinIterval.
  If it becomes 0, calls ChangeFrame and redraws visible animators.
  Calculates new MinInteval (min of Interval of all animators) }
procedure TRVAnimatorList.TimerEvent;
var i, Elapsed: Integer;
    Animator: TRVAnimator;
begin
  Elapsed := MinInterval;
  MinInterval := MaxInt;
  for i := 0 to Count-1 do begin
    Animator := TRVAnimator(Items[i]);
    dec(Animator.Interval, Elapsed);
    if Animator.Interval<=0 then begin
      Animator.ChangeFrame;
      if Animator.IsVisible then
        Animator.DrawFrame
      else
        Animator.ResetBackground;
    end;
    if MinInterval>Animator.Interval then
      MinInterval := Animator.Interval;
  end;
end;
{------------------------------------------------------------------------------}
{ Calls Reset for all animators. Calculates new MinInterval. }
procedure TRVAnimatorList.Reset;
var i: Integer;
    Animator: TRVAnimator;
begin
  MinInterval := MaxInt;
  for i := 0 to Count-1 do begin
    Animator := TRVAnimator(Items[i]);
    Animator.Reset;
    if MinInterval>Animator.Interval then
      MinInterval := Animator.Interval;
  end;
end;
{============================== TRVBitmapAnimator =============================}
{ Draws the current frame at (X,Y) on Canvas.
  Animation=True, if this is a drawing on timer.
  Animation=False, if this is a drawing from item.Paint. }  
procedure TRVBitmapAnimator.Draw(X, Y: Integer; Canvas: TCanvas; Animation: Boolean);
var bmp: TBitmap;
    w,h,nCols: Integer;
    RVStyle: TRVStyle;
begin
  bmp := TBitmap(TRVGraphicItemInfo(Item).ImageCopy);
  if bmp=nil then
    bmp := TBitmap(TRVGraphicItemInfo(Item).Image);
  RVStyle := RVData.GetRVStyle;
  w := TRVGraphicItemInfo(Item).GetImageWidth(RVStyle);
  h := TRVGraphicItemInfo(Item).GetImageHeight(RVStyle);
  nCols := bmp.Width div w;
  RVStyle.GraphicInterface.CopyRect(Canvas,
    Bounds(X,Y,w,h), bmp.Canvas,
    Bounds((FrameIndex mod nCols)*w, (FrameIndex div nCols)*h,w,h));
end;
{------------------------------------------------------------------------------}
{ Draws for RTF export }
procedure TRVBitmapAnimator.DrawForExport(Canvas: TCanvas);
var fi: Integer;
begin
  fi := FrameIndex;
  FrameIndex := 0;
  try
    Draw(0, 0, Canvas, False);
  finally
    FrameIndex := fi;
  end;
end;
{------------------------------------------------------------------------------}
{ RTF export must ignore image scaling }
function TRVBitmapAnimator.ExportIgnoresScale: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
{ Rewinds to the first frame. Updates Interval. }
procedure TRVBitmapAnimator.Reset;
begin
  Interval := TRVGraphicItemInfo(Item).Interval*10;
  FrameIndex := 0;
end;
{------------------------------------------------------------------------------}
{ Change frame to the next one. Updates Interval. }
procedure TRVBitmapAnimator.ChangeFrame;
begin
  Interval := TRVGraphicItemInfo(Item).Interval*10;
  CalcNextFrameIndex;
end;
{------------------------------------------------------------------------------}
{ Returns a number of frames in TRVBitmapAnimator for item } 
function GetBitmapFrameCount(item: TRVGraphicItemInfo; RVStyle: TRVStyle): Integer;
var w,h: Integer;
begin
  w := Item.GetImageWidth(RVStyle);
  h := Item.GetImageHeight(RVStyle);
  if (w=0) or (h=0) then
    Result := 0
  else
    Result := (Item.Image.Width div w)*(Item.Image.Height div h);
end;
{------------------------------------------------------------------------------}
{ Returns the count of frames }
function TRVBitmapAnimator.GetFrameCount: Integer;
begin
  Result := GetBitmapFrameCount(TRVGraphicItemInfo(Item), RVData.GetRVStyle);
end;
{==============================================================================}
{ This procedure creates an animator (anim) for the item, if it's necessary.
  Otherwise it frees-and-nils anim.
  This procedure can create only TRVBitmapAnimator.
  In other units (for example, in RVGifAnimate), there may be other procedures
  creating different animators.
  They must be assigned to RV_MakeAnimator. They must do their work and call
  the previous RV_MakeAnimator, thus making a procedure chain. }
procedure RV_MakeAnimatorDef(item: TCustomRVItemInfo; RVData: TCustomRVFormattedData;
  var anim: TRVAnimator);
begin
  if item is TRVGraphicItemInfo then begin
    if (TRVGraphicItemInfo(item).Interval>0) and
       (TRVGraphicItemInfo(item).Image is TBitmap) and
       (GetBitmapFrameCount(TRVGraphicItemInfo(item), RVData.GetRVStyle)>1) then begin
        if (anim<>nil) and not (anim is TRVBitmapAnimator) then
          RVFreeAndNil(anim);
        if anim=nil then begin
          anim := TRVBitmapAnimator.Create(RVData, Item);
          RVData.InsertAnimator(TObject(anim));
          end
        else if anim<>nil then begin
          anim.Update(RVData, Item);
          anim.Reset;
        end;
        exit;
    end;
  end;
  RVFreeAndNil(anim);
end;

initialization
  RV_MakeAnimator := RV_MakeAnimatorDef;

{$ENDIF}

end.
