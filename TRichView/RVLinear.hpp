﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVLinear.pas' rev: 27.00 (Windows)

#ifndef RvlinearHPP
#define RvlinearHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvlinear
{
//-- type declarations -------------------------------------------------------
struct DECLSPEC_DRECORD TRVSelection
{
public:
	int SelStart;
	int SelLength;
	bool MultiCell;
	int StartRow;
	int StartCol;
	int RowOffs;
	int ColOffs;
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE int RVCharsPerLineBreak;
extern DELPHI_PACKAGE System::WideChar RVNonTextCharacter;
extern DELPHI_PACKAGE bool __fastcall RichViewToLinear(Richview::TCustomRichView* rv, Crvdata::TCustomRVData* CurRVData, Crvdata::TCustomRVData* RVData, int ItemNo, int ItemOffs, int &LinearPos);
extern DELPHI_PACKAGE bool __fastcall LinearToRichView(Richview::TCustomRichView* rv, Crvdata::TCustomRVData* CurRVData, int &LinearPos, Crvdata::TCustomRVData* &RVData, int &ItemNo, int &ItemOffs);
extern DELPHI_PACKAGE int __fastcall RVGetLinearCaretPos(Rvedit::TCustomRichViewEdit* rve);
extern DELPHI_PACKAGE void __fastcall RVSetLinearCaretPos(Rvedit::TCustomRichViewEdit* rve, int LinearPos);
extern DELPHI_PACKAGE void __fastcall RVGetSelection(Richview::TCustomRichView* rv, int &SelStart, int &SelLength);
extern DELPHI_PACKAGE void __fastcall RVSetSelection(Richview::TCustomRichView* rv, int SelStart, int SelLength);
extern DELPHI_PACKAGE void __fastcall RVGetSelectionEx(Richview::TCustomRichView* rv, TRVSelection &Selection);
extern DELPHI_PACKAGE void __fastcall RVSetSelectionEx(Richview::TCustomRichView* rv, const TRVSelection &Selection);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVGetTextRange(Richview::TCustomRichView* rv, int RangeStart, int RangeLength);
extern DELPHI_PACKAGE int __fastcall RVGetTextLength(Richview::TCustomRichView* rv);
}	/* namespace Rvlinear */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVLINEAR)
using namespace Rvlinear;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvlinearHPP
