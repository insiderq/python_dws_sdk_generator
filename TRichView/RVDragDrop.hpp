﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDragDrop.pas' rev: 27.00 (Windows)

#ifndef RvdragdropHPP
#define RvdragdropHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <Winapi.ShlObj.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdragdrop
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVDropTarget;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDropTarget : public Rvscroll::TRVScrollerInternalIfcObject
{
	typedef Rvscroll::TRVScrollerInternalIfcObject inherited;
	
private:
	bool FAccepted;
	bool __fastcall HasAcceptableFormats(const _di_IDataObject DataObj);
	bool __fastcall AllowMoving(int KeyState, int Effect);
	int __fastcall GetEffect(int KeyState, int Effect);
	void __fastcall FillFormatEtc(tagFORMATETC &FmtEtc, System::Word Format);
	bool __fastcall GetFiles(const NativeUInt Handle, System::Classes::TStrings* Files);
	
protected:
	DYNAMIC System::Types::TPoint __fastcall ScreenToRichView(const System::Types::TPoint &pt);
	DYNAMIC void __fastcall DoDragDropCaretChanged(void);
	DYNAMIC bool __fastcall OwnerDragOver2(int X, int Y);
	
public:
	virtual HRESULT __stdcall DragEnter(const _di_IDataObject DataObj, int KeyState, const System::Types::TPoint pt, int &Effect);
	HRESULT __stdcall DragOver(int KeyState, const System::Types::TPoint pt, int &Effect);
	HRESULT __stdcall DragLeave(void);
	HRESULT __stdcall Drop(const _di_IDataObject DataObj, int KeyState, const System::Types::TPoint pt, int &Effect);
	bool __fastcall GetMedium(const _di_IDataObject DataObj, System::Word Format, tagSTGMEDIUM &StgMedium);
	Rvclasses::TRVMemoryStream* __fastcall GetAsStream(const _di_IDataObject DataObj, System::Word Format);
	bool __fastcall GetAsTextA(const _di_IDataObject DataObj, System::Word Format, Rvtypes::TRVAnsiString &s);
	bool __fastcall GetAsTextW(const _di_IDataObject DataObj, System::Word Format, Rvtypes::TRVRawByteString &s);
	Vcl::Graphics::TGraphic* __fastcall GetAsBitmap(const _di_IDataObject DataObj, bool TryDIBFirst);
	Vcl::Graphics::TGraphic* __fastcall GetAsMetafile(const _di_IDataObject DataObj);
	System::Classes::TStringList* __fastcall GetAsFiles(const _di_IDataObject DataObj);
	bool __fastcall HasFormat(const _di_IDataObject DataObj, System::Word Format);
	__fastcall virtual ~TRVDropTarget(void);
	DYNAMIC bool __fastcall RegisterDragDropWindow(void);
	DYNAMIC void __fastcall UnRegisterDragDropWindow(void);
public:
	/* TRVScrollerInternalIfcObject.Create */ inline __fastcall virtual TRVDropTarget(Rvscroll::TRVScroller* AOwner) : Rvscroll::TRVScrollerInternalIfcObject(AOwner) { }
	
private:
	void *__IDropTarget;	// IDropTarget 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {00000122-0000-0000-C000-000000000046}
	operator _di_IDropTarget()
	{
		_di_IDropTarget intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator IDropTarget*(void) { return (IDropTarget*)&__IDropTarget; }
	#endif
	
};

#pragma pack(pop)

class DELPHICLASS TRVEnumFormatEtc;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEnumFormatEtc : public System::TInterfacedObject
{
	typedef System::TInterfacedObject inherited;
	
private:
	Vcl::Graphics::TGraphic* FGraphic;
	int FIndex;
	System::Word __fastcall GetCurFormat(void);
	
public:
	__fastcall TRVEnumFormatEtc(Vcl::Graphics::TGraphic* Graphic, int Index);
	HRESULT __stdcall Next(int Celt, /* out */ void *Elt, System::PLongInt PCeltFetched);
	HRESULT __stdcall Skip(int Celt);
	HRESULT __stdcall Reset(void);
	HRESULT __stdcall Clone(/* out */ _di_IEnumFORMATETC &Enum);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVEnumFormatEtc(void) { }
	
private:
	void *__IEnumFORMATETC;	// IEnumFORMATETC 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {00000103-0000-0000-C000-000000000046}
	operator _di_IEnumFORMATETC()
	{
		_di_IEnumFORMATETC intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator IEnumFORMATETC*(void) { return (IEnumFORMATETC*)&__IEnumFORMATETC; }
	#endif
	
};

#pragma pack(pop)

class DELPHICLASS TRVDropSource;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDropSource : public Rvscroll::TRVScrollerInternalIfcObject
{
	typedef Rvscroll::TRVScrollerInternalIfcObject inherited;
	
protected:
	tagSTGMEDIUM FMedium;
	bool FUseMedium;
	DYNAMIC bool __fastcall IsAvailableFormat(System::Word Format);
	DYNAMIC System::Classes::TMemoryStream* __fastcall GetAsStream(System::Word Format);
	DYNAMIC HRESULT __fastcall GetAsHandle(System::Word Format, NativeUInt &Handle);
	DYNAMIC HRESULT __fastcall SaveToHandle(System::Word Format, NativeUInt Handle);
	
public:
	HRESULT __stdcall QueryContinueDrag(BOOL FEscapePressed, int KeyState);
	HRESULT __stdcall GiveFeedback(int Effect);
	virtual HRESULT __stdcall GetData(const tagFORMATETC &FormatEtcIn, /* out */ tagSTGMEDIUM &Medium);
	virtual HRESULT __stdcall GetDataHere(const tagFORMATETC &FormatEtcIn, /* out */ tagSTGMEDIUM &Medium);
	virtual HRESULT __stdcall QueryGetData(const tagFORMATETC &FormatEtc);
	virtual HRESULT __stdcall GetCanonicalFormatEtc(const tagFORMATETC &FormatEtc, /* out */ tagFORMATETC &FormatEtcOut);
	virtual HRESULT __stdcall SetData(const tagFORMATETC &FormatEtc, tagSTGMEDIUM &Medium, BOOL FRelease);
	virtual HRESULT __stdcall EnumFormatEtc(int Direction, /* out */ _di_IEnumFORMATETC &EnumFormatEtc);
	virtual HRESULT __stdcall DAdvise(const tagFORMATETC &FormatEtc, int advf, const _di_IAdviseSink advsink, /* out */ int &Connection);
	virtual HRESULT __stdcall DUnadvise(int Connection);
	virtual HRESULT __stdcall EnumDAdvise(/* out */ _di_IEnumSTATDATA &EnumAdvise);
	__fastcall virtual ~TRVDropSource(void);
	bool __fastcall StoreData(System::Word Format);
public:
	/* TRVScrollerInternalIfcObject.Create */ inline __fastcall virtual TRVDropSource(Rvscroll::TRVScroller* AOwner) : Rvscroll::TRVScrollerInternalIfcObject(AOwner) { }
	
private:
	void *__IDataObject;	// IDataObject 
	void *__IDropSource;	// IDropSource 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {0000010E-0000-0000-C000-000000000046}
	operator _di_IDataObject()
	{
		_di_IDataObject intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator IDataObject*(void) { return (IDataObject*)&__IDataObject; }
	#endif
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {00000121-0000-0000-C000-000000000046}
	operator _di_IDropSource()
	{
		_di_IDropSource intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator IDropSource*(void) { return (IDropSource*)&__IDropSource; }
	#endif
	
};

#pragma pack(pop)

typedef System::TMetaClass* TRVDropSourceClass;

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvdragdrop */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDRAGDROP)
using namespace Rvdragdrop;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdragdropHPP
