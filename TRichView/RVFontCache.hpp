﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVFontCache.pas' rev: 27.00 (Windows)

#ifndef RvfontcacheHPP
#define RvfontcacheHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvfontcache
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVFontInfoCacheItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFontInfoCacheItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Rvscroll::TRVBiDiMode LastBiDiMode;
	Vcl::Graphics::TCanvas* Canvas;
	tagTEXTMETRICW TextMetric;
	Rvstyle::TRVExtraFontInfo ExtraFontInfo;
	Rvstyle::TFontInfo* FontInfo;
	int VerticalOffset;
	int EmptyLineHeight;
	int HyphenWidth;
	Rvstyle::TRVDocRotation Rotation;
public:
	/* TObject.Create */ inline __fastcall TRVFontInfoCacheItem(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVFontInfoCacheItem(void) { }
	
};

#pragma pack(pop)

typedef TRVFontInfoCacheItem* *PRVFontInfoCacheItem;

class DELPHICLASS TRVFontInfoCache;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFontInfoCache : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVFontInfoCacheItem* operator[](int Index) { return Items[Index]; }
	
protected:
	Vcl::Graphics::TCanvas* FCanvas;
	Vcl::Graphics::TCanvas* FDrawCanvas;
	Rvstyle::TRVStyle* FRVStyle;
	bool FCanUseCustomPPI;
	System::TObject* FOwner;
	TRVFontInfoCacheItem* FInvalidItem;
	virtual TRVFontInfoCacheItem* __fastcall GetItems(int Index) = 0 ;
	
public:
	Rvscroll::TRVBiDiMode CurParaBiDiMode;
	bool IgnoreParaBiDiMode;
	int LastTextStyle;
	Rvstyle::TRVDocRotation Rotation;
	__fastcall virtual TRVFontInfoCache(System::TObject* const AData, Rvstyle::TRVStyle* const ARVStyle, Vcl::Graphics::TCanvas* const ACanvas, Vcl::Graphics::TCanvas* const ADrawCanvas, bool ACanUseCustomPPI);
	virtual void __fastcall Clear(void);
	__property TRVFontInfoCacheItem* Items[int Index] = {read=GetItems/*, default*/};
	__property System::TObject* Owner = {read=FOwner};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVFontInfoCache(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFontInfoCacheFast;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFontInfoCacheFast : public TRVFontInfoCache
{
	typedef TRVFontInfoCache inherited;
	
protected:
	virtual TRVFontInfoCacheItem* __fastcall GetItems(int Index);
	
public:
	virtual void __fastcall Clear(void);
public:
	/* TRVFontInfoCache.Create */ inline __fastcall virtual TRVFontInfoCacheFast(System::TObject* const AData, Rvstyle::TRVStyle* const ARVStyle, Vcl::Graphics::TCanvas* const ACanvas, Vcl::Graphics::TCanvas* const ADrawCanvas, bool ACanUseCustomPPI) : TRVFontInfoCache(AData, ARVStyle, ACanvas, ADrawCanvas, ACanUseCustomPPI) { }
	
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVFontInfoCacheFast(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFontInfoCacheLowResource;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFontInfoCacheLowResource : public TRVFontInfoCache
{
	typedef TRVFontInfoCache inherited;
	
protected:
	virtual TRVFontInfoCacheItem* __fastcall GetItems(int Index);
public:
	/* TRVFontInfoCache.Create */ inline __fastcall virtual TRVFontInfoCacheLowResource(System::TObject* const AData, Rvstyle::TRVStyle* const ARVStyle, Vcl::Graphics::TCanvas* const ACanvas, Vcl::Graphics::TCanvas* const ADrawCanvas, bool ACanUseCustomPPI) : TRVFontInfoCache(AData, ARVStyle, ACanvas, ADrawCanvas, ACanUseCustomPPI) { }
	
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVFontInfoCacheLowResource(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvfontcache */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVFONTCACHE)
using namespace Rvfontcache;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvfontcacheHPP
