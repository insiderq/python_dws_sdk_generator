﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVFloatingPos.pas' rev: 27.00 (Windows)

#ifndef RvfloatingposHPP
#define RvfloatingposHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvfloatingpos
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVHorizontalAnchor : unsigned char { rvhanCharacter, rvhanPage, rvhanMainTextArea, rvhanLeftMargin, rvhanRightMargin, rvhanInnerMargin, rvhanOuterMargin };

enum DECLSPEC_DENUM TRVVerticalAnchor : unsigned char { rvvanLine, rvvanParagraph, rvvanPage, rvvanMainTextArea, rvvanTopMargin, rvvanBottomMargin };

enum DECLSPEC_DENUM TRVFloatPositionKind : unsigned char { rvfpkAlignment, rvfpkAbsPosition, rvfpkPercentPosition };

enum DECLSPEC_DENUM TRVFloatPositionInText : unsigned char { rvpitAboveText, rvpitBelowText };

typedef int TRVFloatPosition;

enum DECLSPEC_DENUM TRVFloatHorizontalAlignment : unsigned char { rvfphalLeft, rvfphalCenter, rvfphalRight };

enum DECLSPEC_DENUM TRVFloatVerticalAlignment : unsigned char { rvfpvalTop, rvfpvalCenter, rvfpvalBottom };

class DELPHICLASS TRVBoxPosition;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBoxPosition : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TRVHorizontalAnchor FHorizontalAnchor;
	TRVVerticalAnchor FVerticalAnchor;
	TRVFloatPositionKind FHorizontalPositionKind;
	TRVFloatPositionKind FVerticalPositionKind;
	TRVFloatPosition FHorizontalOffset;
	TRVFloatPosition FVerticalOffset;
	TRVFloatHorizontalAlignment FHorizontalAlignment;
	TRVFloatVerticalAlignment FVerticalAlignment;
	TRVFloatPositionInText FPositionInText;
	bool FRelativeToCell;
	int __fastcall GetPropertyIdentifier(const Rvtypes::TRVAnsiString PropName);
	
public:
	__fastcall TRVBoxPosition(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle);
	bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	bool __fastcall GetExtraIntPropertyReformat(int Prop, Rvitem::TRVReformatType &ReformatType);
	int __fastcall GetChangedPropertyCount(void);
	void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	__property TRVHorizontalAnchor HorizontalAnchor = {read=FHorizontalAnchor, write=FHorizontalAnchor, default=0};
	__property TRVVerticalAnchor VerticalAnchor = {read=FVerticalAnchor, write=FVerticalAnchor, default=0};
	__property TRVFloatPositionKind HorizontalPositionKind = {read=FHorizontalPositionKind, write=FHorizontalPositionKind, default=1};
	__property TRVFloatPositionKind VerticalPositionKind = {read=FVerticalPositionKind, write=FVerticalPositionKind, default=1};
	__property TRVFloatPosition HorizontalOffset = {read=FHorizontalOffset, write=FHorizontalOffset, default=0};
	__property TRVFloatPosition VerticalOffset = {read=FVerticalOffset, write=FVerticalOffset, default=0};
	__property TRVFloatHorizontalAlignment HorizontalAlignment = {read=FHorizontalAlignment, write=FHorizontalAlignment, default=0};
	__property TRVFloatVerticalAlignment VerticalAlignment = {read=FVerticalAlignment, write=FVerticalAlignment, default=0};
	__property TRVFloatPositionInText PositionInText = {read=FPositionInText, write=FPositionInText, default=0};
	__property bool RelativeToCell = {read=FRelativeToCell, write=FRelativeToCell, default=1};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVBoxPosition(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const System::Word rveipcFloatHorizontalAnchor = System::Word(0x1f4);
static const System::Word rveipcFloatHorizontalPositionKind = System::Word(0x1f5);
static const System::Word rveipcFloatHorizontalOffset = System::Word(0x1f6);
static const System::Word rveipcFloatHorizontalAlign = System::Word(0x1f7);
static const System::Word rveipcFloatVerticalAnchor = System::Word(0x1f8);
static const System::Word rveipcFloatVerticalPositionKind = System::Word(0x1f9);
static const System::Word rveipcFloatVerticalOffset = System::Word(0x1fa);
static const System::Word rveipcFloatVerticalAlign = System::Word(0x1fb);
static const System::Word rveipcFloatRelativeToCell = System::Word(0x1fc);
static const System::Word rveipcFloatPositionInText = System::Word(0x1fd);
}	/* namespace Rvfloatingpos */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVFLOATINGPOS)
using namespace Rvfloatingpos;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvfloatingposHPP
