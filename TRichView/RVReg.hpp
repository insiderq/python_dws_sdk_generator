﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVReg.pas' rev: 27.00 (Windows)

#ifndef RvregHPP
#define RvregHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RVPP.hpp>	// Pascal unit
#include <CtrlImg.hpp>	// Pascal unit
#include <RVMisc.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVOfficeCnv.hpp>	// Pascal unit
#include <RVReport.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvreg
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Rvreg */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVREG)
using namespace Rvreg;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvregHPP
