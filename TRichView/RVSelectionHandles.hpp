﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVSelectionHandles.pas' rev: 27.00 (Windows)

#ifndef RvselectionhandlesHPP
#define RvselectionhandlesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvselectionhandles
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVSelectionHandles;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSelectionHandles : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Types::TPoint FStartPoint;
	bool FDragging;
	bool FDraggingUpperBound;
	void __fastcall DoDrawHandle(Vcl::Graphics::TCanvas* Canvas, int X, int Y, const System::Types::TSize &Offset, bool UpperBound, Rvstyle::TRVStyle* RVStyle);
	void __fastcall DrawHandle(Vcl::Graphics::TCanvas* Canvas, int X, int Top, int Bottom, const System::Types::TSize &Offset, bool UpperBound, Rvstyle::TRVStyle* RVStyle);
	System::Types::TPoint __fastcall GetHandleHitTestCenter(bool UpperBound);
	int __fastcall GetDistanceToHandleCenter(const System::Types::TPoint &Point, bool UpperBound);
	float __fastcall GetSizeMultiplier(void);
	
public:
	int X1;
	int X2;
	int Top1;
	int Top2;
	int Bottom1;
	int Bottom2;
	System::Types::TSize Offset1;
	System::Types::TSize Offset2;
	Crvdata::TCustomRVData* RVData;
	NativeUInt Rgn1;
	NativeUInt Rgn2;
	bool RgnValid1;
	bool RgnValid2;
	__fastcall virtual ~TRVSelectionHandles(void);
	void __fastcall CalculatePositions(void);
	void __fastcall Draw(Vcl::Graphics::TCanvas* Canvas, int XOffs, int YOffs, Rvstyle::TRVStyle* RVStyle, bool Draw1, bool Draw2);
	bool __fastcall GetHandleAt(int X, int Y, bool &UpperBound);
	void __fastcall StartDrag(int X, int Y, bool &UpperBound);
	bool __fastcall MoveTo(int X, int Y);
	void __fastcall EndDrag(void);
	void __fastcall ExcludeRegions(Vcl::Graphics::TCanvas* Canvas);
	__property bool Dragging = {read=FDragging, nodefault};
public:
	/* TObject.Create */ inline __fastcall TRVSelectionHandles(void) : System::TObject() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 RV_SELECTIONHANDLE_HEIGHT = System::Int8(0x13);
static const System::Int8 RV_SELECTIONHANDLE_LINEWIDTH = System::Int8(0x2);
#define RV_SELECTIONHANDLE_LUMINANCERATIO  (6.500000E-01)
static const System::Int8 RV_SELECTIONHANDLE_TRANSPARENCY = System::Int8(0x78);
static const System::Int8 RV_SELECTIONHANDLE_HITTESTRADIUS = System::Int8(0x13);
}	/* namespace Rvselectionhandles */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSELECTIONHANDLES)
using namespace Rvselectionhandles;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvselectionhandlesHPP
