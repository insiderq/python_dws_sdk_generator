{*******************************************************}
{                                                       }
{       TRichView                                       }
{       TRVSelectionHandles represents handles          }
{       for selecting on touch-screens                  }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVSelectionHandles;

interface

{$I RV_Defs.inc}

{$IFDEF RICHVIEWDEF2010}

uses Windows, Graphics, Classes,
     RVStyle, RVGrIn, CRVData, RVTypes;

type

  TRVSelectionHandles = class
    private
      FStartPoint: TPoint;
      FDragging, FDraggingUpperBound: Boolean;
      procedure DoDrawHandle(Canvas: TCanvas; X, Y: Integer;
        const Offset: TSize;
        UpperBound: Boolean; RVStyle: TRVStyle);
      procedure DrawHandle(Canvas: TCanvas; X, Top, Bottom: Integer;
        const Offset: TSize; UpperBound: Boolean; RVStyle: TRVStyle);
      function GetHandleHitTestCenter(UpperBound: Boolean): TPoint;
      function GetDistanceToHandleCenter(const Point: TPoint; UpperBound: Boolean): Integer;
      function GetSizeMultiplier: Single;
    public
      X1, X2, Top1, Top2, Bottom1, Bottom2: Integer;
      Offset1, Offset2: TSize;
      RVData: TCustomRVData;
      Rgn1, Rgn2: TRVHandle;
      RgnValid1, RgnValid2: Boolean;
      destructor Destroy; override;
      procedure CalculatePositions;
      procedure Draw(Canvas: TCanvas; XOffs, YOffs: Integer; RVStyle: TRVStyle;
        Draw1, Draw2: Boolean);
      function GetHandleAt(X, Y: Integer; var UpperBound: Boolean): Boolean;
      procedure StartDrag(X, Y: Integer;  var UpperBound: Boolean);
      function MoveTo(X, Y: Integer): Boolean;
      procedure EndDrag;
      procedure ExcludeRegions(Canvas: TCanvas);
      property Dragging: Boolean read FDragging;
  end;

const RV_SELECTIONHANDLE_HEIGHT = 19; // size = (Height/2, Height)
const RV_SELECTIONHANDLE_LINEWIDTH = 2; // border width
const RV_SELECTIONHANDLE_LUMINANCERATIO = 0.65; // applied to selection color
const RV_SELECTIONHANDLE_TRANSPARENCY = 120; // background transparency, range: 0..255
const RV_SELECTIONHANDLE_HITTESTRADIUS = RV_SELECTIONHANDLE_HEIGHT;
{$ENDIF}

implementation

{$IFDEF RICHVIEWDEF2010}
uses RVFuncs,
  {$IFDEF RICHVIEWDEF10}
  Types,
  {$ENDIF}
  Math, CRVFData;
{======================== TRVSelectionHandles =================================}
destructor TRVSelectionHandles.Destroy;
begin
  if RgnValid1 then
    DeleteObject(Rgn1);
  if RgnValid2 then
    DeleteObject(Rgn2);
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
function TRVSelectionHandles.GetSizeMultiplier: Single;
var Ifc: IRVScaleRichViewInterface;
begin
  Ifc := RVData.GetScaleRichViewInterface;
  if Ifc=nil then
    Result := 1.0
  else
    Result := 100/Ifc.GetCurrentPageZoomPercent;
end;
{------------------------------------------------------------------------------}
function TRVSelectionHandles.GetHandleHitTestCenter(UpperBound: Boolean): TPoint;
var HalfHeight: Integer;
begin
  HalfHeight := Round(GetSizeMultiplier*RV_SELECTIONHANDLE_HEIGHT/2);
  if UpperBound then
    Result := Point(X1+Offset1.cx-1, Bottom1+HalfHeight)
  else
    Result := Point(X2+Offset2.cx+1, Bottom2+HalfHeight)
end;
{------------------------------------------------------------------------------}
function TRVSelectionHandles.GetDistanceToHandleCenter(const Point: TPoint;
  UpperBound: Boolean): Integer;
var Center: TPoint;
begin
  Center := GetHandleHitTestCenter(UpperBound);
  Result := Round(Sqrt(Sqr(Point.X-Center.X)+Sqr(Point.Y-Center.Y)));
end;
{------------------------------------------------------------------------------}
function TRVSelectionHandles.GetHandleAt(X, Y: Integer; var UpperBound: Boolean): Boolean;
var Distance1, Distance2: Integer;
    HitTestRadius: Integer;
begin
  Result := False;
  Distance1 := GetDistanceToHandleCenter(Point(X,Y), True);
  Distance2 := GetDistanceToHandleCenter(Point(X,Y), False);
  HitTestRadius := Round(RV_SELECTIONHANDLE_HITTESTRADIUS*GetSizeMultiplier);
  if Distance1<=HitTestRadius then begin
    Result := True;
    UpperBound := Distance1<Distance2;
    end
  else if Distance2<=HitTestRadius then begin
    Result := True;
    UpperBound := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVSelectionHandles.DrawHandle(Canvas: TCanvas;
  X, Top, Bottom: Integer; const Offset: TSize; UpperBound: Boolean;
  RVStyle: TRVStyle);
begin
  DoDrawHandle(Canvas, X, Bottom, Offset, UpperBound, RVStyle)
end;
{------------------------------------------------------------------------------}
procedure TRVSelectionHandles.Draw(Canvas: TCanvas; XOffs, YOffs: Integer;
  RVStyle: TRVStyle; Draw1, Draw2: Boolean);
var Rgn: TRVHandle;
    RgnValid: Boolean;
begin
  if not Draw1 and not Draw2 then
    exit;
  RVStyle.GraphicInterface.RemoveClipRgn(Canvas, Rgn, RgnValid);
  try
    SelectClipRgn(Canvas.Handle, 0);
    if Draw1 then
      DrawHandle(Canvas, X1-XOffs, Top1-YOffs, Bottom1-YOffs, Offset1, True, RVStyle);
    if Draw2 then
      DrawHandle(Canvas, X2-XOffs, Top2-YOffs, Bottom2-YOffs, Offset2, False, RVStyle);
  finally
    RVStyle.GraphicInterface.RestoreClipRgn(Canvas, Rgn, RgnValid);
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVSelectionHandles.CalculatePositions;
var MaxHeight, MaxWidth, HandleHeight, HandleWidth, DX: Integer;
    Kind: TRVSelectionHandleKind;
begin

  Offset1.cx := 0;
  Offset1.cy := 0;
  Offset2.cx := 0;
  Offset2.cy := 0;
  Kind := RVData.GetRVStyle.SelectionHandleKind;

  HandleHeight := Round((RV_SELECTIONHANDLE_HEIGHT+RV_SELECTIONHANDLE_LINEWIDTH)*GetSizeMultiplier);
  if Kind=rvshkWedge then
    HandleWidth :=Round((RV_SELECTIONHANDLE_HEIGHT/2+RV_SELECTIONHANDLE_LINEWIDTH)*GetSizeMultiplier)
  else
    HandleWidth := HandleHeight;

  if (Kind=rvshkCircle) and (Abs(Bottom1-Bottom2)<=3) and (Abs(X1-X2)<HandleWidth+3) then begin
    DX := Round((HandleWidth-Abs(X1-X2)+3)/2);
    if X1>X2 then
      DX := -DX;
     Offset1.cx := -DX;
     Offset2.cx := DX;
  end;

  if RVData.GetScaleRichViewInterface<>nil then
    exit;

  MaxHeight := Max(TCustomRVFormattedData(RVData).GetFullDocumentHeight,
    TCustomRVFormattedData(RVData).GetHeight);
  MaxWidth := Max(TCustomRVFormattedData(RVData).GetAreaWidth,
    TCustomRVFormattedData(RVData).GetWidth);


  if Bottom1+HandleHeight>MaxHeight then
    Offset1.cy := MaxHeight-(Bottom1+HandleHeight);
  if Bottom2+HandleHeight>MaxHeight then
    Offset2.cy := MaxHeight-(Bottom2+HandleHeight);
  if Bottom1+Offset1.cy<0 then
    Offset1.cy := -Bottom1;
  if Bottom2+Offset2.cy<0 then
    Offset2.cy := -Bottom2;

  case Kind of
  rvshkWedge:
    begin
      if X1+Offset1.cx-HandleWidth<0 then
        Offset1.cx := -(X1-HandleWidth);
      if X2+Offset2.cx+HandleWidth>MaxWidth then
        Offset2.cx := MaxWidth-(X2+HandleWidth);
    end;
  rvshkCircle:
    begin
      if X1+Offset1.cx-HandleWidth div 2 < 0 then
        Offset1.cx := -(X1-HandleWidth div 2);
      if X2+Offset2.cx+HandleWidth div 2 > MaxWidth then
        Offset2.cx := MaxWidth-(X2+HandleWidth div 2);
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVSelectionHandles.StartDrag(X, Y: Integer;  var UpperBound: Boolean);
begin
  FDragging := True;
  FStartPoint := Point(X, Y);
  FDraggingUpperBound := UpperBound;
end;
{------------------------------------------------------------------------------}
function TRVSelectionHandles.MoveTo(X, Y: Integer): Boolean;
var Pt, OldPos: TPoint;
    DrawItemNo, DrawItemOffs, ItemNo, Offs, StartNo, StartOffs, EndNo, EndOffs: Integer;
begin
  if FDraggingUpperBound then begin
    Pt := Point(X1-FStartPoint.X+X, Bottom1-FStartPoint.Y+Y-2);
    OldPos := Point(X1, Bottom1);
    end
  else begin
    Pt := Point(X2-FStartPoint.X+X, Bottom2-FStartPoint.Y+Y-2);
    OldPos := Point(X2, Bottom2);
  end;
  TCustomRVFormattedData(RVData).FindDrawItemForSel(Pt.X, Pt.Y, DrawItemNo, DrawItemOffs,
    False, True, True, False);
  TCustomRVFormattedData(RVData).GetSelectionBounds(StartNo, StartOffs, EndNo, EndOffs, True);
  TCustomRVFormattedData(RVData).DrawItem2Item(DrawItemNo, DrawItemOffs, ItemNo, Offs);
  if FDraggingUpperBound then begin
    Result := (ItemNo<>StartNo) or (Offs<>StartOffs);
    StartNo := ItemNo;
    StartOffs := Offs;
    FDraggingUpperBound := (ItemNo<EndNo) or ((ItemNo=EndNo) and (Offs<=EndOffs));
    end
  else begin
    Result := (ItemNo<>EndNo) or (Offs<>EndOffs);
    EndNo := ItemNo;
    EndOffs := Offs;
    FDraggingUpperBound := (ItemNo<StartNo) or ((ItemNo=StartNo) and (Offs<StartOffs));
  end;
  if Result then begin
    TCustomRVFormattedData(RVData).SetSelectionBounds(StartNo, StartOffs, EndNo, EndOffs);
    if FDraggingUpperBound then begin
      inc(FStartPoint.X, X1-OldPos.X);
      inc(FStartPoint.Y, Bottom1-OldPos.Y);
      end
    else begin
      inc(FStartPoint.X, X2-OldPos.X);
      inc(FStartPoint.Y, Bottom2-OldPos.Y);
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVSelectionHandles.EndDrag;
begin
  FDragging := False;
end;
{------------------------------------------------------------------------------}
procedure TRVSelectionHandles.ExcludeRegions(Canvas: TCanvas);
begin
  if RgnValid1 then
    ExtSelectClipRgn(Canvas.Handle, Rgn1, RGN_DIFF);
  if RgnValid2 then
    ExtSelectClipRgn(Canvas.Handle, Rgn2, RGN_DIFF);
end;
{------------------------------------------------------------------------------}
{$R-}
procedure TRVSelectionHandles.DoDrawHandle(Canvas: TCanvas; X, Y: Integer;
  const Offset: TSize; UpperBound: Boolean; RVStyle: TRVStyle);
var Points: array [0..3] of TPoint;
    Kind: TRVSelectionHandleKind;
    MarkerRect: TRect;
  {...............................................................}
  procedure MakeShape;
  var Height: Single;
  begin
    FillChar(Points, (High(Points)+1)*sizeof(TPoint), 0);
    Height := RV_SELECTIONHANDLE_HEIGHT*GetSizeMultiplier;
    case Kind of
    rvshkWedge:
      begin
        Points[0].X := X;
        Points[0].Y := Y;
        Points[1].X := X;
        Points[1].Y := Y+Round(Height);
        if UpperBound then
          Points[2].X := X-Round(Height/2)
        else
          Points[2].X := X+Round(Height/2);
        Points[2].Y := Points[1].Y;
        Points[3].X := Points[2].X;
        Points[3].Y := Y+Round(Height/2);
        MarkerRect := Bounds(Min(Points[0].X, Points[2].X), Points[0].Y,
          Round(Height/2), Round(Height));

      end;
    rvshkCircle:
      begin
        Points[0].X := X-Round(Height/2);
        Points[0].Y := Y;
        Points[1].X := Points[0].X+Round(Height);
        Points[1].Y := Points[0].Y+Round(Height);
        MarkerRect := Bounds(Points[0].X, Points[0].Y, Round(Height), Round(Height));
      end;
  end;
  end;
  {...............................................................}
  procedure MakeRegion(var Rgn: TRVHandle);
  var DPoints: array [0..3] of TPoint;
  begin
    Move(Points[0], DPoints[0], (High(Points)+1)*sizeof(TPoint));
    LPtoDP(Canvas.Handle, DPoints, High(Points)+1);
    case Kind of
    rvshkWedge:
      Rgn := CreatePolygonRgn(DPoints, High(DPoints)+1, WINDING);
    rvshkCircle:
      Rgn := CreateEllipticRgn(DPoints[0].X, DPoints[0].Y, DPoints[1].X, DPoints[1].Y);
    end;
  end;
  {...............................................................}
  function CanUseTransparency: Boolean;
  var ScreenDC: HDC;
  begin
    ScreenDC := GetDC(0);
    Result := GetDeviceCaps(ScreenDC,BITSPIXEL)>=16;
    ReleaseDC(0, ScreenDC);
  end;
  {...............................................................}
  function GetTranslucentBitmapEx: TBitmap;
  type
     RGBAARR = array [0..0] of TRGBQuad;
     PRGBAARR = ^RGBAARR;
  var i,j: Integer;
      Opacity: Byte;
      rgb : PRGBAArr;
  begin
    Result := TBitmap.Create;
    Result.Width := MarkerRect.Right-MarkerRect.Left;
    Result.Height := MarkerRect.Bottom-MarkerRect.Top;
    Result.PixelFormat := pf32bit;
    Result.AlphaFormat := afPremultiplied;
    Opacity := (255-RV_SELECTIONHANDLE_TRANSPARENCY);
    for i := 0 to Result.Height-1 do begin
      rgb := Result.ScanLine[i];
      for j := 0 to Result.Width-1 do
        with rgb[j] do begin
           rgbRed := Opacity;
           rgbGreen := Opacity;
           rgbBlue := Opacity;
           rgbReserved := Opacity;
         end;
    end;
  end;
  {...............................................................}
  {
  function GetTranslucentBitmap: TBitmap;
  type
     RGBARR = array [0..0] of TRGBTriple;
     PRGBARR = ^RGBARR;
  var i,j: Integer;
      WhiteOpacity: Word;
      rgb : PRGBARR;
  begin
    Result := TBitmap.Create;
    Result.Width := MarkerRect.Right-MarkerRect.Left;
    Result.Height := MarkerRect.Bottom-MarkerRect.Top;
    Result.Canvas.CopyRect(Rect(0, 0, Result.Width, Result.Height), Canvas,
      MarkerRect);
    Result.PixelFormat := pf24bit;
    WhiteOpacity := (255-RV_SELECTIONHANDLE_TRANSPARENCY)*255; // opacity*white color
    for i := 0 to Result.Height-1 do begin
      rgb := Result.ScanLine[i];
      for j := 0 to Result.Width-1 do
        with rgb[j] do begin
           rgbtBlue  := (rgbtBlue *RV_SELECTIONHANDLE_TRANSPARENCY + WhiteOpacity) div 255;
           rgbtGreen := (rgbtGreen*RV_SELECTIONHANDLE_TRANSPARENCY + WhiteOpacity) div 255;
           rgbtRed   := (rgbtRed  *RV_SELECTIONHANDLE_TRANSPARENCY + WhiteOpacity) div 255;
         end;
    end;
  end;
  }
  {...............................................................}
  procedure InitPenAndBrush(UseTransparency: Boolean);
  var Color: TColor;
  begin
    if UseTransparency then begin
      Color := RVStyle.SelColor;
      if Color=clNone then
        Color := clHighlight;
      Canvas.Pen.Color := RV_ChangeColorLuminance(ColorToRGB(Color),
        RV_SELECTIONHANDLE_LUMINANCERATIO);
      {if RVData.GetScaleRichViewInterface=nil then begin}
        Canvas.Brush.Color := clNone;
        Canvas.Brush.Style := bsClear;
        {end
      else begin
        Canvas.Brush.Color := clWhite;
        Canvas.Brush.Style := bsSolid;
      end; }
      {
      if FDragging and (FDraggingUpperBound=UpperBound) then begin
        Canvas.Brush.Color := clRed;
        Canvas.Brush.Style := bsSolid
      end;
      }
      end
    else begin
      Canvas.Pen.Color := clBlack;
      Canvas.Brush.Color := clWhite;
      Canvas.Brush.Style := bsSolid;
    end;
    Canvas.Pen.Width := Round(RV_SELECTIONHANDLE_LINEWIDTH*GetSizeMultiplier);
    Canvas.Pen.Style := psInsideFrame;
  end;
  {...............................................................}
  procedure DrawBorder;
  begin
    case Kind of
    rvshkWedge:
      Canvas.Polygon(Points);
    rvshkCircle:
      Canvas.Ellipse(Points[0].X, Points[0].Y, Points[1].X, Points[1].Y);
    end;
  end;
  {...............................................................}
var Background: TBitmap;
    OldRgn, Rgn: TRVHandle;
    RgnValid: Boolean;
    UseTransparency: Boolean;
begin
  Kind := RVData.GetRVStyle.SelectionHandleKind;
  UseTransparency := CanUseTransparency;
  if (Offset.cx<>0) or (Offset.cy<>0) then begin
    if Kind=rvshkWedge then begin
      Canvas.Pen.Width := 2;
      Canvas.Pen.Style := psSolid;
      Canvas.Pen.Color := clRed;
      RVStyle.GraphicInterface.Line(Canvas, X, Y, X+Offset.cx, Y+Offset.cy);
    end;
    inc(X, Offset.cx);
    inc(Y, Offset.cy);
  end;
  InitPenAndBrush(UseTransparency);
  MakeShape;
  if {(RVData.GetScaleRichViewInterface=nil) and} UseTransparency then begin
    Background := GetTranslucentBitmapEx;
    try
      OldRgn := CreateRectRgn(0,0,1,1);
      RgnValid := GetClipRgn(Canvas.Handle, OldRgn)=1;
      MakeRegion(Rgn);
      try
        ExtSelectClipRgn(Canvas.Handle, Rgn, RGN_COPY);
        Canvas.Draw(MarkerRect.Left, MarkerRect.Top, Background);
      finally
        if RgnValid then
          SelectClipRgn(Canvas.Handle, OldRgn)
        else
          SelectClipRgn(Canvas.Handle, 0);
        DeleteObject(OldRgn);
        DeleteObject(Rgn);
      end;
    finally
      Background.Free;
    end;
  end;
  DrawBorder;
  if (RVData.GetScaleRichViewInterface<>nil) then
    if UpperBound then begin
      if RgnValid1 then
        DeleteObject(Rgn1);
      MakeRegion(Rgn1);
      RgnValid1 := True;
      end
    else begin
      if RgnValid2 then
        DeleteObject(Rgn2);
      MakeRegion(Rgn2);
      RgnValid2 := True;
    end;
end;
{$ENDIF}

end.
