﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVCodePages.pas' rev: 27.00 (Windows)

#ifndef RvcodepagesHPP
#define RvcodepagesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvcodepages
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall GetCodePageValues(System::Classes::TGetStrProc Proc);
extern DELPHI_PACKAGE System::UnicodeString __fastcall CodePageToIdent(Rvstyle::TRVCodePage CodePage);
extern DELPHI_PACKAGE bool __fastcall IdentToCodePage(const System::UnicodeString Ident, Rvstyle::TRVCodePage &CodePage);
}	/* namespace Rvcodepages */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCODEPAGES)
using namespace Rvcodepages;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvcodepagesHPP
