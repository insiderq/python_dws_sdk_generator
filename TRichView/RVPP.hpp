﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVPP.pas' rev: 27.00 (Windows)

#ifndef RvppHPP
#define RvppHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <CRVPP.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvpp
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVPrintPreview;
class PASCALIMPLEMENTATION TRVPrintPreview : public Crvpp::TCustomRVPrintPreview
{
	typedef Crvpp::TCustomRVPrintPreview inherited;
	
private:
	Ptblrv::TCustomRVPrint* FRVPrint;
	int FStoredPageNo;
	System::Types::TRect FStoredRect;
	int FStoredDocID;
	Vcl::Graphics::TMetafile* FMetafile;
	bool FCachePageImage;
	void __fastcall SetRVPrint(Ptblrv::TCustomRVPrint* const Value);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC bool __fastcall CanDrawContents(void);
	DYNAMIC void __fastcall DrawContents(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	virtual void __fastcall DrawMargins(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, int PageNo);
	DYNAMIC int __fastcall GetPreview100PercentWidth(void);
	DYNAMIC int __fastcall GetPreview100PercentHeight(void);
	DYNAMIC System::Types::TRect __fastcall GetPhysMargins(void);
	DYNAMIC int __fastcall GetPageCount(void);
	virtual void __fastcall SetPageNo(const int Value);
	virtual void __fastcall Loaded(void);
	void __fastcall UpdateImage(int PageNo, const System::Types::TRect &R);
	
public:
	__fastcall virtual TRVPrintPreview(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVPrintPreview(void);
	
__published:
	__property bool CachePageImage = {read=FCachePageImage, write=FCachePageImage, default=0};
	__property Ptblrv::TCustomRVPrint* RVPrint = {read=FRVPrint, write=SetRVPrint};
	__property ZoomInCursor = {default=102};
	__property ZoomOutCursor = {default=103};
	__property OnZoomChanged;
	__property MarginsPen;
	__property PrintableAreaPen;
	__property ClickMode = {default=1};
	__property PageBorderColor = {default=-16777203};
	__property PageBorderWidth = {default=2};
	__property ShadowColor = {default=-16777195};
	__property ShadowWidth = {default=4};
	__property BackgroundMargin = {default=20};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Constraints;
	__property Ctl3D;
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property HelpContext = {default=0};
	__property ParentCtl3D = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Touch;
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGesture;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseDown;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnStartDrag;
	__property BorderStyle;
	__property HScrollVisible = {default=1};
	__property Tracking = {default=1};
	__property UseXPThemes = {default=1};
	__property VScrollVisible = {default=1};
	__property WheelStep = {default=2};
	__property OnHScrolled;
	__property OnVScrolled;
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVPrintPreview(HWND ParentWindow) : Crvpp::TCustomRVPrintPreview(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvpp */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVPP)
using namespace Rvpp;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvppHPP
