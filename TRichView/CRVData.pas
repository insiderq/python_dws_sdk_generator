{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       TCustomRVData is a basic class representing     }
{       RichView document.                              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit CRVData;

interface
{$I RV_Defs.inc}
uses
  {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  {$IFNDEF RVDONOTUSEJPEGIMAGE}
  Jpeg,
  {$ENDIF}
  {$IFNDEF RVDONOTUSEPNGIMAGE}
  PngImage,
  {$ENDIF}
  {$IFDEF RVUSEDXPNGIMAGE}
  dxGDIPlusClasses,
  {$ENDIF}
  {$IFNDEF RVDONOTUSESEQ}
  RVSeqItem,
  {$ENDIF}
  {$IFNDEF RVDONOTUSELISTS}
  RVMarker,
  {$ENDIF}
  {$IFDEF RICHVIEWDEF4}
  ImgList,
  {$ENDIF}
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  RVDocParams,
  {$ENDIF}
  {$IFNDEF RVDONOTUSEDOCX}
  RVDocX,
  {$ENDIF}
  RVStyle, RVBack, RVFuncs, RVItem, RVScroll, RVUni, RVClasses,
  RVRTFErr, RVTypes;

type

  TCustomRVData = class;

  { For ScaleRichView }

  TRVRepaintOperation = (rvrrInvalidate, rvrrInvalidateLiveSpell, rvrrUpdate);

  IRVScaleRichViewInterface = interface
    function GetFormatCanvas: TCanvas;
    procedure DrawControl(Left, Top: Integer; Canvas: TCanvas;
      RVData: TCustomRVData; DrawItemNo: Integer);
    procedure GetMouseClientCoords(RichView: TWinControl; var X, Y: Integer);
    procedure DoOnBiDiModeChange(RichView: TWinControl);
    {$IFNDEF RVDONOTUSESEQ}
    procedure DoAfterReadNote(RVData: TCustomRVData; ItemNo: Integer);
    function CreateNoteSRVData(StreamingId: Integer): TObject;
    {$ENDIF}
    procedure FloatingBoxChanged(RVData: TCustomRVData; ItemNo: Integer; OnlyRedrawing: Boolean);
    function GetSRichViewEdit: TCustomControl;
    procedure DoBeforeSaving(RichView: TWinControl);
    procedure DoOnSetHint(RichView: TWinControl);
    procedure DoOnRepaint(RichView: TWinControl; Operation: TRVRepaintOperation;
      Rect: PRect);
    procedure DoOnFormat(RVData: TCustomRVData);
    procedure DoOnClear(RVData: TCustomRVData);
    function IsAniRectVisible(RichView: TWinControl; var ClientR: TRect; var ZoomPercent: Single): Boolean;
    function GetClientRect: TRect;
    {$IFNDEF RVDONOTUSESMARTPOPUP}
    function GetSmartPopupProperties: TObject;
    function GetCurrentPageZoomPercent: Single;
    procedure ConvertRVToSRV(var ClientPt: TPoint);
    {$ENDIF}
    function GetDrawnPageZoomPercent: Single;
    function IsOnImportPictureAssigned: Boolean;
    function IsOnReadHyperlinkAssigned: Boolean;
    function IsOnWriteHyperlinkAssigned: Boolean;
    function GetRTFReadPropertiesClass: TClass;
    function GetActiveEditor(MainEditor: Boolean): TWinControl;
    procedure Format;
    procedure Paginate;
    procedure StoreSubDocChanges;
    procedure DoOnAddUndo(RichView: TWinControl);
    procedure DoOnDeleteLastUndo(RichView: TWinControl);
    procedure DoOnChange(RichView: TWinControl; ClearRedo: Boolean);
  end;

  { State of RVData }
  TRVState = (
    rvstMakingSelection,     // Mouse selection is in process
    rvstLineSelection,       // Line selection (started from the left margin)
                             //   is in process
    rvstDrawHover,           // There is a highlighted hyperlink
    rvstSkipFormatting,      // Reformatting is not allowed
    rvstFormatting,          // The document is being formatted
    rvstFormattingPart,      // Part of document is being formatted
    //rvstIgnoreNextMouseDown, // Next mouse-down event must be ignored
    rvstChangingBkPalette,   // Palette of background image is being updated
                             //   (avoiding recursive call)
    rvstCompletelySelected,  // RVData is completely selected (table cell)
    rvstClearing,            // Document is being cleared
                             //   (avoiding recursive call)
    rvstDoNotMoveChildren,   // Call of AdjustChildrenCoords is not allowed
    rvstForceStyleChangeEvent, // Editor must call events for changing styles
                             //   even if next value assigned to current style
                             //   is equal to the current value
    rvstIgnoreNextChar,      // Next call of WMChar or KeyPress must be ignored
                             //  (the character is already processed in WMKeyDown)
    rvstDoNotTab,            // Next call of DoTabNavigation will be ignored
    rvstDeselecting,         // Deselection is in process
                             //   (avoiding recursive call)
    rvstUnAssigningChosen,   // Unassigning chosen data is in process
                             //   (avoiding recursive call)
    rvstNoScroll,            // Scrolling is not allowed
    rvstFinalizingUndo,      // Avoiding recursive calls of FinalizeUndoGroup
    rvstRTFSkipPar,          // Saving to RTF: do not save paragraph mark
    rvstLoadingAsPartOfItem, // This RVData is loaded from RVF as a part of item
                             //   (cell is loaded with table)
                             //   (not calling AfterLoading(rvlfRVF) for items
                             //   of this RVData: will be called for the container item
    rvstLoadingAsSubDoc,     // The same, but also set while loading documents
                             // in footnotes and endnotes
    rvstNoKillFocusEvents,   // Disabling processing of WMKillFocus
    rvstEditorUnformatted,   // TRichViewEdit was not formatted before the call of Format
    rvstNameSet,             // TRichView.Name was assigned. Used to detect placing
                             // the component on the form from the Component Palette
    rvstFirstParaAborted,    // This is a page saved and reloaded in ReportHelper,
                             //   and the first paragraph is not completely on this page
    rvstLastParaAborted,     // The same for the last paragraph
    rvstInvalidSelection,    // Selection is not valid: do not create item resizer
    rvstDoNotClearCurTag,    // TRVEditRVData.ChangeEx must not clear "current tag"
                             //   ("current tag" is used in methods for inserting text)
    rvstStartingDragDrop,    // Dragging from this RichView is about to start
                             //   (WM_RVDRAGDROP was posted). Only in absolute
                             //    root RVData
    rvstCanDragDropDeleteSelection, // After dragging from this editor, selection
                             //  must be deleted (set when moving data to
                             //  another control)
    rvstKeyPress,            // do some special processing related to d&d in keypress
    rvstDoNotSaveContent,   // used to copy unselected cells to the Clipboard
    rvstPreserveSoftPageBreaks, // if set, Clear does not clear soft page breaks
    rvstNoDBExitUpdate,     // if set, DBRichViewEdit does not update data when losing focus
    rvstSavingPage,         // set in SavePageAsRVF for root RVData
    rvstCreatingInplace,    // set in root RVData when creating inplace
    rvstNoSpecialCharacters, // this RVData never displays special characters
    rvstJumpsJustBuilt,   // a list of hypertext links was just created by TRVEditRVData.BuildJumpsInfo
    rvstNoStyleTemplateAdustment, // disables adjustment of StyleNo basing on StyleTemplates
    rvstTouchMouseDown);  // the last mouse message was a touch screen tap

  TRVStates = set of TRVState;

  { Flags for RVData }
  TRVFlag = (
     rvflUseJumps,            // List of hyperlink coords must be maintained
    rvflIgnoreMouseLeave,    // Needs ignoring mouse leaving message
    rvflTrim,                // Formatting routine may not show spaces in line
                             //   wrap places (always set)
    rvflShareContents,       // This RVData uses smb. else's items
    rvflUseExternalLeading,  // Formatting routine uses font external leading
                             // (never set)
    rvflMouseXYAlwaysCorrect,// Mouse processing procedures may not preprocess
                             //   coordinates (used in table cells)
    rvflAllowCustomDrawItems,// Formatting routine may create drawing items
                             // of custom types (not only defined in DLines.pas) -
                             // used in RVPrint and RVReportHelper
    rvflPrinting,            // This is RVData with formatting for printing (or
                             //   reports)
    rvflRootEditor,          // This is TRVEditRVData (not RVData of inplace)
    rvflRoot,                // This is TRichViewRVData or TRVEditRVData
                             //  (not RVData of inplace)
    rvflDBRichViewEdit,      // This is TDBRichViewEdit.RVData
    rvflCanUseCustomPPI,     // Allows using RVStyle.TextStyles.PixelsPerInch
    rvflCanProcessGetText,    // Allows processing WM_GETTEXT, WM_SETTEXT, WM_GETTEXTLENGTH
    rvflProcessedWidthHeight);  // If not set, Width and Height for rotated RVData
                              // have original values (in coord space of DrawItems)
                              // If set, they are rotated (in cell editors)
  TRVFlags = set of TRVFlag;

  { Which part to save in RVF? }
  TRVFSaveScope = (
     rvfss_Full,              // the whole document
     rvfss_Selection,         // selection
     rvfss_Page,              // page (for TRVPrint or TRVReportHelper)
     rvfss_FromPage,          // pages from the specified (for TRVPrint or TRVReportHelper)
     rvfss_FullInPage);        // the whole document, as a part of page

  PRVIntegerList = ^TRVIntegerList;
  TRVRTFFontTable = class;

  TRVEnumItemsProc = procedure (RVData: TCustomRVData; ItemNo: Integer;
    var UserData1: Integer; const UserData2: String;
    var ContinueEnum: Boolean) of object;

  { ----------------------------------------------------------------------------
    TRVLayoutInfo: information about document layout for saving and loading in
    RVF.
    Main properties:
    - margins,
    - min- and maxtextwidth,
    - bidimode.
    For saving RVReportHelper page, additional properties:
    - LastParaAborted: <>0 if the last page paragraph is not completely on the page;
    - FirstParaAborted: the same for the first page paragraph;
    - FirstMarkerListNo, FirstMarkerLevel - information about marker of the
      first page paragraph (marker is before this page)
  }
  TRVLayoutInfo = class
    public
      Loaded: Boolean;
      LeftMargin, RightMargin, TopMargin, BottomMargin: Integer;
      MinTextWidth, MaxTextWidth: Integer;
      BiDiMode: TRVBiDiMode;
      LastParaAborted, FirstParaAborted: Integer;
      FirstMarkerListNo, FirstMarkerLevel: Integer;
      constructor Create;
      procedure SaveToStream(Stream: TStream; IncludeSize, OnlyPageInfo: Boolean);
      procedure LoadFromStream(Stream: TStream; IncludeSize: Boolean);
      procedure SaveTextToStream(Stream: TStream; OnlyPageInfo: Boolean);
      procedure LoadText(const s: TRVAnsiString);
      procedure LoadBinary(const s: TRVRawByteString);
  end;
  {$IFNDEF RVDONOTUSEHTML}
  { ----------------------------------------------------------------------------
    TRVHTMLBulletInfo: information for saving shared images in HTML
      (several items can use the same image file).
    Used by: "bullets", "hotspots", list markers with pictures and image lists.
  }
  TRVHTMLBulletInfo = class
    public
      FileName: String;
      ImageList: TCustomImageList;
      ImageIndex: Integer;
      BackColor: TColor;
      Graphic: TGraphic;
  end;
  {$ENDIF}
  { ----------------------------------------------------------------------------
    TRVRTFFontTableItem: item of RTF font table (TRVRTFFontTable)
  }
  TRVRTFFontTableItem = class
    public
      FontName: String;
      {$IFDEF RICHVIEWCBDEF3}
      Charset: TFontCharset;
      {$ENDIF}
  end;
  { ----------------------------------------------------------------------------
    TRVRTFFontTable: RTF font table. Created for saving to RTF, contains all
    fonts used in the document (both by styles and by items)
  }
  TRVRTFFontTable = class (TRVList)
    private
      function Get(Index: Integer): TRVRTFFontTableItem;
      procedure Put(Index: Integer; const Value: TRVRTFFontTableItem);
    public
      function Find(const FontName: String
        {$IFDEF RICHVIEWCBDEF3}; Charset: TFontCharset{$ENDIF}): Integer;
      function AddUnique(const FontName: String
        {$IFDEF RICHVIEWCBDEF3}; Charset: TFontCharset{$ENDIF}): Integer;
      property Items[Index: Integer]: TRVRTFFontTableItem read Get write Put; default;
  end;
  { ----------------------------------------------------------------------------
    TRVFRTFTables
  }
  TRVRTFTables = class (TCustomRVRTFTables)
  private
    FFontTable: TRVRTFFontTable;
    FColorList: TRVColorList;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    FStyleSheetMap: TRVIntegerList;
    {$ENDIF}
  public
    constructor Create;
    destructor Destroy; override;
    procedure ClearColors; override;
    procedure ClearFonts; override;
    procedure AddColor(Color: TColor); override;
    function GetColorIndex(Color: TColor): Integer; override;
    function AddFontFromStyle(TextStyle: TCustomRVFontInfo): Integer; override;
    function AddFontFromStyleEx(TextStyle: TCustomRVFontInfo;
      ValidProperties: TRVFontInfoProperties; Root: Boolean): Integer; override;
    function AddFont(Font: TFont): Integer; override;
    function GetFontStyleIndex(TextStyle: TCustomRVFontInfo): Integer; override;
    function GetFontIndex(Font: TFont): Integer; override;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    procedure ClearStyleSheetMap; override;
    procedure AddStyleSheetMap(StyleTemplateNo: Integer); override;
    function GetStyleSheetIndex(StyleTemplateNo: Integer; CharStyle: Boolean): Integer; override;
    {$ENDIF}
  end;


  TRVThumbnailCache = class (TList)
  public
    MaxCount: Integer;
    constructor Create;
    procedure AddThumbnail(ImageOwner: TObject);
    procedure RemoveThumbnail(ImageOwner: TObject);
  end;

  { ----------------------------------------------------------------------------
    TCustomRVData: RichView document. This class is not used directly.
    Direct descendant: TCustomRVFormattedData.
  }
  TCustomRVData = class(TPersistent)
  private
    { Private declarations }
    FFirstJumpNo: Integer;
    FItems: TRVItemList;
    procedure CheckItemIndex(i: Integer);
    { Property values }
    function GetPageBreaksBeforeItems(Index: Integer): Boolean;
    procedure SetPageBreaksBeforeItems(Index: Integer;  Value: Boolean);
    function GetClearLeft(Index: Integer): Boolean;
    function GetClearRight(Index: Integer): Boolean;
    procedure SetClearLeft(Index: Integer; const Value: Boolean);
    procedure SetClearRight(Index: Integer; const Value: Boolean);
    function GetItemCount: Integer;
    { HTML & RTF }
    function ShouldSaveTextToFormat(StyleNo: Integer; AllowedCode: TRVTextOption): Boolean;
    {$IFNDEF RVDONOTUSEHTML}
    function ShouldSaveTextToHTML(StyleNo: Integer): Boolean;
    function GetHTMLATag(ItemNo: Integer; CSS: TRVRawByteString; Options: TRVSaveOptions): TRVAnsiString;
    {$ENDIF}
    {$IFNDEF RVDONOTUSERTF}
    function ShouldSaveTextToRTF(StyleNo: Integer): Boolean;
    {$ENDIF}
    {$IFNDEF RVDONOTUSEDOCX}
    function ShouldSaveTextToDocX(StyleNo: Integer): Boolean;
    {$ENDIF}
    { Others }
    procedure AddNLRTag_(const s: TRVRawByteString; StyleNo, ParaNo: Integer;
      const Tag: TRVTag);
    {$IFNDEF RVDONOTUSEUNICODE}
    procedure AddNLATag_(const s: TRVAnsiString; StyleNo, ParaNo: Integer;
      const Tag: TRVTag);
    {$ENDIF}
    function AddTextUniversal(const text: TRVRawByteString;
      StyleNo, FirstParaNo, OtherParaNo: Integer;
      AsSingleParagraph, CheckUnicode: Boolean; CodePage: TRVCodePage;
      const Tag: TRVTag): Boolean;
  protected
    { Protected declarations }
    FAllowNewPara: Boolean;
    FirstCP, LastCP, NotAddedCP: TRVCPInfo;
    function IsWordWrapAllowed: Boolean; virtual;
    function NextCharStr(const str: TRVRawByteString; ItemNo, Index: Integer): Integer;
    function PrevCharStr(const str: TRVRawByteString; ItemNo, Index: Integer): Integer;
    function NextChar(ItemNo: Integer; Index: Integer): Integer;
    function PrevChar(ItemNo: Integer; Index: Integer): Integer;
    procedure CheckItemClass(ItemNo: Integer;
      RequiredClass: TCustomRVItemInfoClass);
    function ShareItems: Boolean; dynamic;
    function CanLoadLayout: Boolean; dynamic;
    function GetURL(id: Integer): String; dynamic; abstract;
    function GetOptions: TRVOptions; virtual;
    procedure SetOptions(const Value: TRVOptions); virtual;
    function GetRVFOptions: TRVFOptions; virtual;
    procedure SetRVFOptions(const Value: TRVFOptions); virtual;
    function GetRTFOptions: TRVRTFOptions; virtual;
    procedure SetRTFOptions(const Value: TRVRTFOptions); virtual;
    function GetRVFWarnings: TRVFWarnings; virtual;
    procedure SetRVFWarnings(const Value: TRVFWarnings); virtual;
    function GetDelimiters: String; dynamic;
    function GetRVFTextStylesReadMode: TRVFReaderStyleMode; virtual;
    function GetRVFParaStylesReadMode: TRVFReaderStyleMode; virtual;
    procedure RVFGetLimits(SaveScope: TRVFSaveScope;
      var StartItem, EndItem, StartOffs, EndOffs: Integer;
      var StartPart, EndPart: TRVMultiDrawItemPart;
      var SelectedItem: TCustomRVItemInfo); dynamic;
    function GetRTFProperties:TPersistent  {TRVRTFReaderProperties}; dynamic;
    {$IFNDEF RVDONOTUSERVF}
    procedure DoOnStyleReaderError(Reader: TReader; const Message: string;
      var Handled: Boolean);
    function InsertRVFFromStream_(Stream: TStream; var Index: Integer;
      AParaNo: Integer; AllowReplaceStyles, AppendMode, EditFlag: Boolean;
      var Color: TColor; Background: TRVBackground;
      Layout: TRVLayoutInfo; var NonFirstItemsAdded: Integer;
      var Protect, FullReformat: Boolean; LoadAsSubDoc: Boolean
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      ; ReadStyleTemplates: TRVStyleTemplateCollection
      {$ENDIF}):Boolean;
    procedure DataWriter(Stream: TStream); dynamic;
    procedure DataReader(Stream: TStream);
    procedure ApplyLayoutInfo (Layout: TRVLayoutInfo); 
    {$ENDIF}
    procedure NormalizeParas(StartItemNo: Integer);
    procedure InsertCheckpoint(ItemNo: Integer; const Tag: TRVTag;
      const Name: String; RaiseEvent: Boolean);
    procedure UnlinkCheckpoint(cp: TRVCPInfo; DecCPCount: Boolean);
    function FindCPBeforeItem(ItemNo: Integer): TRVCPInfo;
    procedure UpdateCPItemNo;
    procedure InternalFreeItem(item: TCustomRVItemInfo; Clearing: Boolean); virtual;
    function IsDelimiter(const s: TRVRawByteString; Index: Integer;
      ItemOptions: TRVItemOptions; CodePage: TRVCodePage): Boolean;
    procedure Replace0(var s: TRVRawByteString);
    function RV_CanConcateItems(FirstItemNo: Integer; item1, item2: TCustomRVItemInfo;
      IgnorePara, AllowConcateNonText: Boolean): Boolean;
    procedure SimpleConcate(FirstItemNo: Integer; item1, item2: TCustomRVItemInfo);
    procedure MassSimpleConcate(FirstItemNo, LastItemNo: Integer);
    procedure SimpleConcateSubitems(ItemNo: Integer);
    function SupportsPageBreaks: Boolean; dynamic;
    {$IFNDEF RVDONOTUSEHTML}
    procedure SaveHTMLCheckpoint(Stream: TStream; Checkpoint: TRVCPInfo;
      var cpno: Integer; const Prefix: String; FromNewLine: Boolean;
      Options: TRVSaveOptions);
    function GetTextForHTML(const Path: String; ItemNo: Integer; CSSVersion: Boolean;
      SaveOptions: TRVSaveOptions): TRVRawByteString;
    {$IFNDEF RVDONOTUSESEQ}
    procedure SaveHTMLNotes(const Path, ImagesPrefix, CPPrefix: String;
      Stream: TStream; CSSVersion: Boolean; Options: TRVSaveOptions;
      Color: TColor; var imgSaveNo: Integer; Bullets: TRVList;
      {$IFNDEF RVDONOTUSELISTS}ListBullets: TRVHTMLListCSSList;{$ENDIF}
      NoteClass: TCustomRVItemInfoClass; StyleNo: Integer);
     {$ENDIF}
    {$ENDIF}
    function GetFirstParaItem(ItemNo: Integer): Integer;
    function GetFirstParaSectionItem(ItemNo: Integer): Integer;
    {$IFDEF RVUSELISTORSEQ}
    function FindPreviousItem(ItemNo: Integer;
      ItemClass: TCustomRVItemInfoClass): TCustomRVItemInfo;
    function FindItemLocalLocationFrom(StartItemNo: Integer;
      ItemToFind: TCustomRVItemInfo): Integer;
    {$ENDIF}
    {$IFNDEF RVDONOTUSESEQ}
    function FindPreviousSeq(ItemNo: Integer): TRVSeqItemInfo;
    procedure DestroySeqList; dynamic;
    function FindLastSeqIndex(StartAfterMeIndex: Integer;
      SeqNames: TStringList): Integer;
    {$ENDIF}
    {$IFNDEF RVDONOTUSELISTS}
    procedure DestroyMarkers; dynamic;
    function FindPreviousMarker(ItemNo: Integer): TRVMarkerItemInfo;
    function FindLastMarkerIndex(StartAfterMeIndex: Integer;
      ListStyles: TRVIntegerList): Integer;
    {$ENDIF}
    function GetFlags: TRVFlags; virtual;                   abstract;
    procedure SetFlags(const Value: TRVFlags); virtual;     abstract;
    procedure AddStringFromFile(const s: TRVAnsiString; StyleNo,ParaNo: Integer;
      FromNewLine, AsSingleParagraph: Boolean; var FirstTime, PageBreak: Boolean);
    procedure AfterDeleteStyles(Data: TRVDeleteUnusedStylesData); dynamic;
    function GetMaxLength: Integer; virtual;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    procedure ModifyStyleNoByTemplate(Item: TCustomRVItemInfo;
       NewStyleNo: Integer); dynamic;
    {$ENDIF}
    procedure AdjustItemParaNoBeforeAdding(Item: TCustomRVItemInfo);
  public
    State: TRVStates;
    FFirstParaListNo, FFirstParaLevel: Integer;
    CPCount: Integer;
    { Constructors - destructors }
    constructor Create;
    destructor Destroy; override;
    { Document/control & styles properties }
    function GetRVData: TCustomRVData; virtual;
    function GetSourceRVData: TCustomRVData; virtual;
    function GetStyleCodePage(StyleNo: Integer): TRVCodePage;
    function GetItemCodePage(ItemNo: Integer): TRVCodePage;
    function IsSymbolCharset(ItemNo: Integer): Boolean;
    function GetItemCodePage2(Item: TCustomRVItemInfo): TRVCodePage;
    function GetStyleLocale(StyleNo: Integer): Cardinal;
    function GetDefaultCodePage: TRVCodePage;
    function GetRVStyle: TRVStyle; virtual;
    function GetParentControl: TWinControl; dynamic;
    procedure GetParentInfo(var ParentItemNo: Integer;
      var Location: TRVStoreSubRVData); dynamic;
    function GetAbsoluteRootItemNo(ItemNo: Integer): Integer;
    function GetChosenRVData: TCustomRVData; dynamic;
    function GetTopLevelChosenRVData: TCustomRVData;
    function GetChosenItem: TCustomRVItemInfo; dynamic;
    function GetParentData: TCustomRVData; virtual;
    function GetRootData: TCustomRVData; virtual;
    function GetAbsoluteParentData: TCustomRVData; virtual;
    function GetAbsoluteRootData: TCustomRVData; virtual;
    {$IFNDEF RVDONOTUSESEQ}
    function GetNoteText: String; dynamic;
    {$ENDIF}
    { Palette and thumbnails }
    function GetRVPalette: HPALETTE; virtual;
    function GetRVLogPalette: PLogPalette; virtual;
    function GetDoInPaletteMode: TRVPaletteAction; virtual;
    procedure UpdateItemsPaletteInfo;
    function GetThumbnailCache: TRVThumbnailCache; virtual;
    procedure FreeBackgroundThumbnail; dynamic;
    { Item properties }
    function GetItemOptions(ItemNo: Integer): TRVItemOptions; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetItemNo(Item: TCustomRVItemInfo): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetItem(ItemNo: Integer): TCustomRVItemInfo; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function SetItemExtraIntProperty(ItemNo: Integer;
      Prop: TRVExtraItemProperty; Value: Integer): Boolean;
    function GetItemExtraIntProperty(ItemNo: Integer;
      Prop: TRVExtraItemProperty; var Value: Integer): Boolean;
    function SetItemExtraIntPropertyEx(ItemNo: Integer;
      Prop: Integer; Value: Integer): Boolean;
    function GetItemExtraIntPropertyEx(ItemNo: Integer;
      Prop: Integer; var Value: Integer): Boolean;
    function SetItemExtraStrProperty(ItemNo: Integer;
      Prop: TRVExtraItemStrProperty; const Value: String): Boolean;
    function GetItemExtraStrProperty(ItemNo: Integer;
      Prop: TRVExtraItemStrProperty; var Value: String): Boolean;
    function SetItemExtraStrPropertyEx(ItemNo, Prop: Integer;
      const Value: String): Boolean;
    function GetItemExtraStrPropertyEx(ItemNo, Prop: Integer;
      var Value: String): Boolean;
    function GetItemTag(ItemNo: Integer): TRVTag;
    function GetItemVAlign(ItemNo: Integer): TRVVAlign; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function IsParaStart(ItemNo: Integer): Boolean; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetItemPara(ItemNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function IsFromNewLine(ItemNo: Integer): Boolean; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetOffsAfterItem(ItemNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetOffsBeforeItem(ItemNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function ItemLength(ItemNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    procedure SetItemTag(ItemNo: Integer; const ATag: TRVTag);
    procedure SetItemVAlign(ItemNo: Integer; AVAlign: TRVVAlign);
    function GetItemStyle(ItemNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetActualStyle(Item: TCustomRVItemInfo): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetActualTextStyle(Item: TCustomRVItemInfo): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetActualStyleEx(StyleNo, ParaNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
     {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    function GetStyleFromStyleTemplate(StyleNo, ParaNo: Integer): Integer; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function UseStyleTemplates: Boolean; dynamic;
    function StyleTemplateInsertMode: TRVStyleTemplateInsertMode; dynamic;
    {$ENDIF}
    function GetItemTextR(ItemNo: Integer): TRVRawByteString;
    procedure SetItemTextR(ItemNo: Integer; const s: TRVRawByteString);
    procedure SetItemTextA(ItemNo: Integer; const s: TRVAnsiString);
    procedure SetItemText(ItemNo: Integer; const s: String);
    function GetItemTextA(ItemNo: Integer): TRVAnsiString;
    function GetItemTextExA(ItemNo: Integer; CodePage: TRVCodePage): TRVAnsiString;
    function GetItemText(ItemNo: Integer): String;
    function GetItemTextEx(ItemNo: Integer; CodePage: TRVCodePage): String;
    {$IFNDEF RVDONOTUSEUNICODE}
    {$IFDEF RICHVIEWCBDEF3}
    function GetTextInItemFormatW(ItemNo: Integer; const s: TRVUnicodeString): TRVRawByteString;
    function GetItemTextW(ItemNo: Integer): TRVUnicodeString;
    function GetItemTextExW(ItemNo: Integer; const SourceStr: TRVRawByteString): TRVUnicodeString;
    procedure SetItemTextW(ItemNo: Integer; const s: TRVUnicodeString);
    {$ENDIF}
    function GetTextInItemFormatA(ItemNo: Integer; const s: TRVAnsiString): TRVRawByteString;
    {$ENDIF}
    function FindControlItemNo(actrl: TControl): Integer;
    function IsHiddenItem(ItemNo: Integer): Boolean;
    function IsHiddenParaEnd(FirstItemNo: Integer): Boolean;
    { BiDi }
    function GetItemBiDiMode(ItemNo: Integer): TRVBiDiMode; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetParaBiDiMode(ParaNo: Integer): TRVBiDiMode; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
    function GetBiDiMode: TRVBiDiMode; virtual;
    { Operations on items - internal }
    procedure FreeItem(ItemNo: Integer; Clearing: Boolean);
    { Operations on items - public }
    procedure Clear; dynamic;
    procedure DeleteItems(FirstItemNo, Count: Integer); dynamic;
    procedure DeleteSection(const CpName: String);
    procedure MassSimpleConcateAll;
    { Related to events }
    function IsAssignedOnProgress: Boolean; dynamic;
    procedure DoProgress(Operation: TRVLongOperation; Stage: TRVProgressStage;
      PercentDone: Byte); dynamic;
    function GetExtraRTFCode(Area: TRVRTFSaveArea; Obj: TObject;
      Index1, Index2: Integer; InStyleSheet: Boolean): TRVAnsiString; dynamic;
    function GetExtraDocXCode(Area: TRVDocXSaveArea; Obj: TObject;
      Index1, Index2: Integer): TRVAnsiString; dynamic;
    function GetExtraHTMLCode(Area: TRVHTMLSaveArea;
      CSSVersion: Boolean): String; dynamic;
    function GetParaHTMLCode(RVData: TCustomRVData; ItemNo: Integer;
      ParaStart, CSSVersion: Boolean): String; dynamic;
    function GetParaHTMLCode2(RVData: TCustomRVData; ItemNo: Integer;
      ParaStart, CSSVersion: Boolean; Options: TRVSaveOptions;
      RVStyle: TRVStyle): TRVRawByteString;
    procedure ReadHyperlink(const Target, Extras: String; DocFormat: TRVLoadFormat;
      var StyleNo: Integer; var ItemTag: TRVTag;
      var ItemName: TRVRawByteString); dynamic;
    procedure WriteHyperlink(id: Integer; RVData: TCustomRVData; ItemNo: Integer;
       SaveFormat: TRVSaveFormat; var Target, Extras: String); dynamic;
    function SaveItemToFile(const Path: String; RVData: TCustomRVData;
      ItemNo: Integer; SaveFormat: TRVSaveFormat; Unicode: Boolean;
      var Text: TRVRawByteString): Boolean; virtual;
    function ImportPicture(const Location: String;
      Width, Height: Integer; var Invalid: Boolean): TGraphic; dynamic;
    function GetItemHint(RVData: TCustomRVData; ItemNo: Integer;
      const UpperRVDataHint: String): String; dynamic;
    function DoSavePicture(DocumentSaveFormat: TRVSaveFormat;
      RVData: TCustomRVData; ItemNo: Integer;
      const imgSavePrefix, Path: String; var imgSaveNo: Integer;
      OverrideFiles: Boolean; CurrentFileColor: TColor;
      gr: TGraphic): String; virtual;
    function SavePicture(DocumentSaveFormat: TRVSaveFormat;
      const imgSavePrefix, Path: String; var imgSaveNo: Integer;
      OverrideFiles: Boolean; CurrentFileColor: TColor;
      gr: TGraphic): String;
    function RVFPictureNeeded(const ItemName: String; Item: TRVNonTextItemInfo;
      Index1, Index2: Integer; PictureInsideRVF: Boolean): TGraphic; dynamic;
    procedure ControlAction(RVData: TCustomRVData; ControlAction: TRVControlAction;
      ItemNo: Integer; Item: TCustomRVItemInfo); dynamic;
    procedure ItemAction(ItemAction: TRVItemAction; Item: TCustomRVItemInfo;
      var Text: TRVRawByteString; RVData: TCustomRVData); virtual;
    function GetScaleRichViewInterface: IRVScaleRichViewInterface; dynamic;
    {$IFNDEF RVDONOTUSESEQ}
    procedure AfterReadNote(RVData: TCustomRVData; ItemNo: Integer);
    {$ENDIF}
    procedure DoBeforeSaving; dynamic;
    procedure ControlAction2(RVData: TCustomRVData; ControlAction: TRVControlAction;
      ItemNo: Integer; var Control:  TControl); dynamic; abstract;
    function RVFControlNeeded(const ItemName: String;
      const ItemTag: TRVTag): TControl; dynamic;
    function RVFImageListNeeded(ImageListTag: Integer): TCustomImageList; dynamic;
    procedure HTMLSaveImage(RVData: TCustomRVData; ItemNo: Integer;
      const Path: String; BackgroundColor: TColor; var Location: String;
      var DoDefault: Boolean); dynamic;
    procedure SaveImage2(RVData: TCustomRVData; ItemNo: Integer;
      Graphic: TGraphic; SaveFormat: TRVSaveFormat;
      const Path, ImagePrefix: String; var ImageSaveNo: Integer;
      var Location: String; var DoDefault: Boolean); dynamic;
    function SaveComponentToFile(const Path: String; SaveMe: TComponent;
      SaveFormat: TRVSaveFormat): String; virtual;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    procedure DoStyleTemplatesChange; dynamic;
    {$ENDIF}
    { Text save and load }
    {$IFNDEF RVDONOTUSEUNICODE}
    function LoadTextFromStreamW(Stream: TStream; StyleNo, ParaNo: Integer;
      DefAsSingleParagraph: Boolean):Boolean;
    function LoadTextW(const FileName: String; StyleNo, ParaNo: Integer;
      DefAsSingleParagraph: Boolean):Boolean;
    {$ENDIF}
    function SaveTextToStream(const Path: String; Stream: TStream;
      LineWidth: Integer; SelectionOnly, TextOnly, Unicode,
      UnicodeWriteSignature: Boolean; CodePage: TRVCodePage;
      Root: Boolean):Boolean;
    function SaveText(const FileName: String; LineWidth: Integer;
      Unicode: Boolean; CodePage: TRVCodePage):Boolean;
    function LoadText(const FileName: String; StyleNo, ParaNo: Integer;
      AsSingleParagraph: Boolean; CodePage: TRVCodePage):Boolean;
    function LoadTextFromStream(Stream: TStream; StyleNo, ParaNo: Integer;
      AsSingleParagraph: Boolean; CodePage: TRVCodePage):Boolean;
    { HTML save }
    {$IFNDEF RVDONOTUSEHTML}
    function NextItemFromSpace(ItemNo: Integer): Boolean;
    function SaveBackgroundToHTML(bmp: TBitmap; Color: TColor;
      const Path, ImagesPrefix: String; var imgSaveNo: Integer;
      SaveOptions: TRVSaveOptions): String;
    function SaveHTMLToStreamEx(Stream: TStream;
      const Path, Title, ImagesPrefix, ExtraStyles, ExternalCSS, CPPrefix: String;
      Options: TRVSaveOptions; Color: TColor; var CurrentFileColor: TColor;
      var imgSaveNo: Integer; LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
      Background: TRVBackground; Bullets: TRVList
      {$IFNDEF RVDONOTUSELISTS};ListBullets: TRVHTMLListCSSList{$ENDIF}): Boolean; dynamic;
    function SaveHTMLToStream(Stream: TStream;
      const Path, Title, ImagesPrefix: String;
      Options: TRVSaveOptions; Color: TColor; var imgSaveNo: Integer;
      LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
      Background: TRVBackground; Bullets: TRVList): Boolean; dynamic;
    function SaveHTMLEx(const FileName, Title, ImagesPrefix,
      ExtraStyles, ExternalCSS, CPPrefix: String;
      Options: TRVSaveOptions; Color: TColor; var CurrentFileColor: TColor;
      var imgSaveNo: Integer; LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
      Background: TRVBackground): Boolean;
    function SaveHTML(const FileName, Title, ImagesPrefix: String;
      Options: TRVSaveOptions; Color: TColor; var imgSaveNo: Integer;
      LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
      Background: TRVBackground): Boolean;
    {$ENDIF}
    function GetNextFileName(const ImagesPrefix, Path, Ext: String;
      var imgSaveNo: Integer; OverrideFiles: Boolean): String; dynamic;
    { RVF save and load }
    {$IFNDEF RVDONOTUSERVF}
    procedure DoOnCtrlReaderError(Reader: TReader; const Message: string;
      var Handled: Boolean);    
    function LoadRVFFromStream(Stream: TStream; var Color: TColor;
      Background: TRVBackground; Layout: TRVLayoutInfo
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      ; ReadStyleTemplates: TRVStyleTemplateCollection
      {$ENDIF}):Boolean;
    function InsertRVFFromStream(Stream: TStream; Index: Integer;
      var Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo;
      AllowReplaceStyles: Boolean
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      ; ReadStyleTemplates: TRVStyleTemplateCollection
      {$ENDIF}):Boolean;
    function AppendRVFFromStream(Stream: TStream; ParaNo: Integer;
      var Color: TColor; Background: TRVBackground):Boolean;
    function LoadRVF(const FileName: String;
      var Color: TColor; Background: TRVBackground;
      Layout: TRVLayoutInfo):Boolean;
    //SelectionOnly=True - reserved here
    function SaveRVFToStream(Stream: TStream; SelectionOnly: Boolean;
      Color: TColor; Background: TRVBackground;
      Layout: TRVLayoutInfo; Root: Boolean{$IFDEF RICHVIEWDEF4}=True{$ENDIF}):Boolean;
    function SaveRVFToStreamEx(Stream: TStream; SaveScope: TRVFSaveScope;
      Color: TColor; Background: TRVBackground;
      Layout: TRVLayoutInfo; Root: Boolean; sad: PRVScreenAndDevice):Boolean;
    //SelectionOnly=True - reserved here
    function SaveRVF(const FileName: String; SelectionOnly: Boolean;
      Color: TColor; Background: TRVBackground;
      Layout: TRVLayoutInfo; Root: Boolean):Boolean;
    {$ENDIF}
    procedure InitStyleMappings(var PTextStylesMapping, PParaStylesMapping,
      PListStylesMapping: PRVIntegerList; var PUnits: PRVStyleUnits); dynamic;
    procedure DoneStyleMappings(AsSubDoc: Boolean); dynamic;
    function InsertFirstRVFItem(var Index: Integer; var s: TRVRawByteString;
      var item: TCustomRVItemInfo; EditFlag: Boolean; var FullReformat: Boolean;
      var NewListNo: Integer): Boolean; dynamic;
    function GetRVFSaveScope(SelectionOnly: Boolean):TRVFSaveScope;
    function SaveProgressInit(var Progress: Integer; Max: Integer;
      Operation: TRVLongOperation): Boolean;
    procedure SaveProgressRun(var Progress: Integer; Position, Max: Integer;
      Operation: TRVLongOperation);
    procedure SaveProgressDone(Operation: TRVLongOperation);
    { RTF save and load }
    procedure MakeFontTable(RTFTables: TCustomRVRTFTables;
      StyleToFont: TRVIntegerList
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}; var StyleTemplateToFont: TRVIntegerList{$ENDIF});
    {$IFNDEF RVDONOTUSERTF}
    {$IFNDEF RVDONOTUSELISTS}
    procedure SaveRTFListTable97(Stream: TStream; RTFTables: TCustomRVRTFTables;
      ListOverrideOffsetsList: TRVIntegerList; Header, Footer,
      FirstPageHeader, FirstPageFooter,
      EvenPagesHeader, EvenPagesFooter: TCustomRVData);
    {$ENDIF}
    function SaveRTFToStream(Stream: TStream; SelectionOnly: Boolean;
      Level: Integer; Color: TColor; Background: TRVBackground;
      var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
      CompleteDocument, HiddenParent: Boolean;
      Header, Footer, FirstPageHeader, FirstPageFooter,
      EvenPagesHeader, EvenPagesFooter: TCustomRVData):Boolean; dynamic;
    function SaveRTF(const FileName: String; SelectionOnly: Boolean;
      Color: TColor; Background: TRVBackground):Boolean;
    {$ENDIF}
    {$IFNDEF RVDONOTUSEDOCX}
    procedure SaveOOXMLFontTableToStream(Stream: TStream);
    procedure SaveOOXMLStylesToStream(Stream: TStream);
    function GetOOXMLCheckpointStr(CP: TRVCPInfo; SavingData: TRVDocXSavingData): TRVAnsiString;
    function SaveOOXMLDocumentSettingsToStream(Stream: TStream): Boolean;
    function SaveOOXMLCorePropertiesToStream(Stream: TStream): Boolean;
    procedure SaveOOXMLDocumentToStream(Stream: TStream; const Path: String; SelectionOnly: Boolean;
      RootDocument, InCell, HiddenParent: Boolean; Color: TColor; Background: TRVBackground;
      SavingData: TRVDocXSavingData); dynamic;
    {$IFNDEF RVDONOTUSELISTS}
    procedure SaveOOXMLListTable(Stream: TStream;
      SavingData: TRVDocXSavingData; Header, Footer,
      FirstPageHeader, FirstPageFooter,
      EvenPagesHeader, EvenPagesFooter: TCustomRVData);
    {$ENDIF}
    {$ENDIF}

    {$IFNDEF RVDONOTUSERTFIMPORT}
    function LoadRTFFromStream(Stream: TStream):TRVRTFErrorCode;
    function LoadRTF(const FileName: String):TRVRTFErrorCode;
    {$IFDEF RVUSEWORDDOC}
    function LoadWordDoc(const FileName: String):TRVRTFErrorCode;
    {$ENDIF}
    {$ENDIF}
    procedure MakeRTFTables(RTFTables: TCustomRVRTFTables;
      ListOverrideCountList: TRVIntegerList; AddDefColors: Boolean);
    procedure FillRTFTables(RTFTables: TCustomRVRTFTables;
      ListOverrideCountList: TRVIntegerList);
    { Adding items - general }
    procedure AddItemR(const Text: TRVRawByteString; Item: TCustomRVItemInfo;
      AdjustPara: Boolean);
    procedure AddItem(const Text: String; Item: TCustomRVItemInfo);
    //procedure AddItem(const Text: String; Item: TCustomRVItemInfo);
    procedure AddItemAsIsR(const Text: TRVRawByteString; Item: TCustomRVItemInfo);
    { Adding items - text }
    procedure AddFmt(const FormatStr: String; const Args: array of const;
      StyleNo, ParaNo: Integer);
    procedure AddNLR(const s: TRVRawByteString; StyleNo, ParaNo: Integer);
    procedure AddNL(const s: String; StyleNo, ParaNo: Integer);
    procedure AddNLRTag(const s: TRVRawByteString; StyleNo, ParaNo: Integer;
      const Tag: TRVTag);
    procedure AddNLTag(const s: String; StyleNo, ParaNo: Integer; const Tag: TRVTag);
    procedure AddTextNLR(const s: TRVRawByteString; StyleNo, FirstParaNo, OtherParaNo: Integer
      {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
    procedure AddTextNL(const s: String; StyleNo, FirstParaNo, OtherParaNo: Integer
      {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
    procedure AddTextNLA(const s: TRVAnsiString; StyleNo, FirstParaNo, OtherParaNo: Integer
      {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
    procedure AddTextBlockNLA(const s: TRVAnsiString; StyleNo, ParaNo: Integer
      {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
    procedure AddNLATag(const s: TRVAnsiString; StyleNo, ParaNo: Integer;
      const Tag: TRVTag);
    {$IFNDEF RVDONOTUSEUNICODE}
    {$IFDEF RICHVIEWCBDEF3}
    procedure AddNLWTag(const s: TRVUnicodeString; StyleNo, ParaNo: Integer;
      const Tag: TRVTag);
    {$ENDIF}
    procedure AddNLWTagRaw(const s: TRVRawByteString; StyleNo, ParaNo: Integer;
      const Tag: TRVTag);
    procedure AddTextNLWRaw(const s: TRVRawByteString; StyleNo, FirstParaNo,
      OtherParaNo: Integer; DefAsSingleParagraph: Boolean);
    procedure AddTextNLW(const s: TRVUnicodeString; StyleNo, FirstParaNo,
      OtherParaNo: Integer; DefAsSingleParagraph: Boolean);
    {$ENDIF}
    { Adding items - others }
    {$IFNDEF RVDONOTUSETABS}
    procedure AddTab(TextStyleNo, ParaNo: Integer);
    {$ENDIF}
    procedure AddBreakExTag(Width: TRVStyleLength; Style: TRVBreakStyle;
      Color: TColor; const Tag: TRVTag);
    procedure AddBreak;
    procedure AddBreakEx(Width: TRVStyleLength; Style: TRVBreakStyle; Color: TColor);
    procedure AddBreakTag(const Tag: TRVTag);
    procedure AddBulletEx(const Name: TRVAnsiString; ImageIndex: Integer;
      ImageList: TCustomImageList; ParaNo: Integer);
    procedure AddBulletExTag(const Name: TRVAnsiString; ImageIndex: Integer;
      ImageList: TCustomImageList; ParaNo: Integer; const Tag: TRVTag);
    procedure AddHotspotEx(const Name: TRVAnsiString; ImageIndex,
      HotImageIndex: Integer; ImageList: TCustomImageList; ParaNo: Integer);
    procedure AddHotspotExTag(const Name: TRVAnsiString; ImageIndex,
      HotImageIndex: Integer; ImageList: TCustomImageList; ParaNo: Integer;
      const Tag: TRVTag);
    procedure AddPictureExTag(const Name: TRVAnsiString; gr: TGraphic; ParaNo: Integer;
      VAlign: TRVVAlign; const Tag: TRVTag);
    procedure AddControlExTag(const Name: TRVAnsiString; ctrl: TControl;
      ParaNo: Integer; VAlign: TRVVAlign; const Tag: TRVTag);
    procedure AddPictureEx(const Name: TRVAnsiString; gr: TGraphic; ParaNo: Integer;
      VAlign: TRVVAlign);
    procedure AddControlEx(const Name: TRVAnsiString; ctrl: TControl;
      ParaNo: Integer; VAlign: TRVVAlign);
    procedure AddHotPicture(const Name: TRVAnsiString; gr: TGraphic; ParaNo: Integer;
      VAlign: TRVVAlign);
    procedure AddHotPictureTag(const Name: TRVAnsiString; gr: TGraphic; ParaNo: Integer;
      VAlign: TRVVAlign; const Tag: TRVTag);
    { Checkpoints - internal }
    procedure FreeCheckpoint(var cp: TRVCPInfo; AdjustLinks, DecCPCount: Boolean);
    procedure SetCP(Item: TCustomRVItemInfo; var PrevCP, CP: TRVCPInfo);
    procedure UpdateCPPos(cp: TRVCPInfo; ItemNo: Integer);
    { Checkpoints - public }
    function AddNamedCheckpointExTag(const CpName: String; RaiseEvent: Boolean;
      const Tag: TRVTag): Integer;
    procedure SetCheckpointInfo(ItemNo: Integer; const ATag: TRVTag;
      const AName: String; ARaiseEvent: Boolean);
    function RemoveCheckpoint(ItemNo: Integer): Boolean;
    function GetFirstCheckpoint: TCheckpointData;
    function GetNextCheckpoint(CheckpointData: TCheckpointData): TCheckpointData;
    function GetLastCheckpoint: TCheckpointData;
    function GetPrevCheckpoint(CheckpointData: TCheckpointData): TCheckpointData;
    function GetItemCheckpoint(ItemNo: Integer):TCheckpointData;
    function FindCheckpointByName(const Name: String): TCheckpointData;
    function FindCheckpointByTag(const Tag: TRVTag): TCheckpointData;
    function GetCheckpointByNo(No: Integer): TCheckpointData;
    function GetCheckpointItemNo(CheckpointData: TCheckpointData): Integer;
    function GetCheckpointNo(CheckpointData: TCheckpointData): Integer;
    procedure GetCheckpointInfo(CheckpointData: TCheckpointData;
      var Tag: TRVTag; var Name: String; var RaiseEvent: Boolean);
    { Get info for specific item types }
    procedure GetBreakInfo(ItemNo: Integer; var AWidth: TRVStyleLength;
      var AStyle: TRVBreakStyle; var AColor: TColor; var ATag: TRVTag);
    procedure GetBulletInfo(ItemNo: Integer; var AName: TRVAnsiString;
      var AImageIndex: Integer; var AImageList: TCustomImageList;
      var ATag: TRVTag);
    procedure GetHotspotInfo(ItemNo: Integer; var AName: TRVAnsiString;
      var AImageIndex, AHotImageIndex: Integer; var AImageList: TCustomImageList;
      var ATag: TRVTag);
    procedure GetPictureInfo(ItemNo: Integer; var AName: TRVAnsiString;
      var Agr: TGraphic; var AVAlign: TRVVAlign; var ATag: TRVTag);
    procedure GetControlInfo(ItemNo: Integer; var AName: TRVAnsiString;
      var Actrl: TControl; var AVAlign: TRVVAlign; var ATag: TRVTag);
    procedure GetTextInfo(ItemNo: Integer; var AText: String;
      var ATag: TRVTag);
    { Set info for specific item types }
    procedure SetGrouped(ItemNo: Integer; Grouped: Boolean);
    procedure SetBreakInfo(ItemNo: Integer; AWidth: TRVStyleLength;
      AStyle: TRVBreakStyle; AColor: TColor; const ATag: TRVTag);
    procedure SetBulletInfo(ItemNo: Integer; const AName: TRVAnsiString;
      AImageIndex: Integer; AImageList: TCustomImageList; const ATag: TRVTag);
    procedure SetHotspotInfo(ItemNo: Integer; const AName: TRVAnsiString;
      AImageIndex, AHotImageIndex: Integer; AImageList: TCustomImageList;
      const ATag: TRVTag);
    function SetPictureInfo(ItemNo: Integer; const  AName: TRVAnsiString;
      Agr: TGraphic; AVAlign: TRVVAlign; const ATag: TRVTag): Boolean;
    function SetControlInfo(ItemNo: Integer; const AName: TRVAnsiString;
      AVAlign: TRVVAlign; const ATag: TRVTag): Boolean;
    { Styles }
    procedure DoMarkStylesInUse(Data: TRVDeleteUnusedStylesData);
    procedure DoUpdateStyles(Data: TRVDeleteUnusedStylesData);
    procedure MarkStylesInUse(Data: TRVDeleteUnusedStylesData); dynamic;
    procedure DeleteMarkedStyles(Data: TRVDeleteUnusedStylesData);
    procedure DeleteUnusedStyles(TextStyles, ParaStyles, ListStyles: Boolean);
    procedure AfterAddStyle(StyleInfo: TCustomRVInfo); dynamic;
    function GetOrAddTextStyle(TextStyle: TFontInfo): Integer;
    function GetOrAddParaStyle(ParaStyle: TParaInfo): Integer;
    { Numbered sequences }
    {$IFNDEF RVDONOTUSESEQ}
    function GetSeqList(AllowCreate: Boolean): TRVSeqList; dynamic;
    procedure AddSeqInList(ItemNo: Integer);
    procedure DeleteSeqFromList(Item: TCustomRVItemInfo; Clearing: Boolean);
    {$ENDIF}
    { Paragraph list markers}
    {$IFNDEF RVDONOTUSELISTS}
    function GetMarkers(AllowCreate: Boolean): TRVMarkerList; dynamic;
    function GetPrevMarkers: TRVMarkerList; dynamic;
    function SetListMarkerInfo(AItemNo, AListNo, AListLevel, AStartFrom,
      AParaNo: Integer; AUseStartFrom: Boolean): Integer;
    procedure RecalcMarker(AItemNo: Integer; AllowCreateList: Boolean);
    procedure RemoveListMarker(ItemNo: Integer);
    function GetListMarkerInfo(AItemNo: Integer; var AListNo, AListLevel,
      AStartFrom: Integer; var AUseStartFrom: Boolean): Integer;
    procedure AddMarkerInList(ItemNo: Integer);
    procedure DeleteMarkerFromList(Item: TCustomRVItemInfo; Clearing: Boolean);
    {$ENDIF}
    { Others }
    function IsDelimiterA(ch: TRVAnsiChar; CodePage: TRVCodePage): Boolean;
    function IsDelimiterW(ch: TRVUnicodeChar): Boolean;
    function EnumItems(Proc: TRVEnumItemsProc; var UserData1: Integer;
      const UserData2: String): Boolean;
    procedure ShareItemsFrom(Source: TCustomRVData);
    procedure AssignItemsFrom(Source: TCustomRVData);
    procedure DrainFrom(Victim: TCustomRVData);
    procedure SetParagraphStyleToAll(ParaNo: Integer);
    procedure SetAddParagraphMode(AllowNewPara: Boolean);
    procedure AppendFrom(Source: TCustomRVData);
    procedure Inserting(RVData: TCustomRVData; Safe: Boolean);
    function Edit: TCustomRVData; dynamic;
    procedure Beep;
    procedure ExpandToParaSection(ItemNo1,ItemNo2: Integer;
      var FirstItemNo, LastItemNo: Integer);
    procedure ExpandToPara(ItemNo1,ItemNo2: Integer;
      var FirstItemNo, LastItemNo: Integer);
    function ReplaceTabs(const s: TRVRawByteString; StyleNo: Integer;
      UnicodeDef: Boolean): TRVRawByteString;
    procedure AdjustInItemsRange(var ItemNo: Integer);
    function GetColor: TColor; virtual;
    {$IFNDEF RVDONOTUSEDOCPARAMS}
    function GetDocParameters(AllowCreate: Boolean): TRVDocParameters; dynamic;
    {$ENDIF}
    procedure ConvertToDifferentUnits(NewUnits: TRVStyleUnits;
      RVStyle: TRVStyle; ConvertItems: Boolean); dynamic;
    function KeepTogether: Boolean; dynamic;
    function GetCurParaStyleNo: Integer; dynamic;
    { Rotation }
    function GetRotation: TRVDocRotation; virtual;
    function GetRealRotation(FromAbsRoot: Boolean): TRVDocRotation;
    function IsVerticalText: Boolean;
    function IsReallyVerticalText: Boolean;
    function IsRotationUsed: Boolean;
    { Used by style templates }
    procedure ChangeParagraphStyles(ParaMapList, NewIndices, OldIndices: TRVIntegerList;
      var Index: Integer); dynamic;
    procedure ChangeTextStyles(TextMapList, NewIndices, OldIndices: TRVIntegerList;
      var Index: Integer); dynamic;
    { Properties }
    function GetDocProperties: TStringList; dynamic;
    function GetExtraDocuments: TStrings; dynamic;
    property Flags: TRVFlags read GetFlags write SetFlags;
    property Items: TRVItemList read FItems;
    property ItemCount: Integer read GetItemCount;
    property Options: TRVOptions read GetOptions write SetOptions;
    property RVFOptions: TRVFOptions read GetRVFOptions write SetRVFOptions;
    property RTFOptions: TRVRTFOptions read GetRTFOptions write SetRTFOptions;
    property RVFWarnings: TRVFWarnings read GetRVFWarnings write SetRVFWarnings;
    property FirstJumpNo: Integer read FFirstJumpNo write FFirstJumpNo;
    property PageBreaksBeforeItems[Index: Integer]: Boolean
      read GetPageBreaksBeforeItems write SetPageBreaksBeforeItems;
    property ClearLeft[Index: Integer]: Boolean
      read GetClearLeft write SetClearLeft;
    property ClearRight[Index: Integer]: Boolean
      read GetClearRight write SetClearRight;
  end;

  TRVItemListHack = class(TRVItemList)

  end;


  procedure RVCheckUni(Length: Integer);
  function RVCompareLocations(RVData1: TCustomRVData; ItemNo1: Integer;
    RVData2: TCustomRVData; ItemNo2: Integer): Integer;

{$IFNDEF RVDONOTUSERTF}
procedure RVSaveFontToRTF(Stream: TStream; Font: TFont;
  RTFTables: TCustomRVRTFTables; RVStyle: TRVStyle);
{$ENDIF}

const
  RichViewSavePInHTML:     Boolean = False;
  RichViewSaveDivInHTMLEx: Boolean = False;
  RichViewSavePageBreaksInText: Boolean = False;
  RichViewDoNotCheckRVFStyleRefs: Boolean = False;
  RichViewAllowCopyTableCells: Boolean = True;
  RichViewMaxThumbnailCount: Integer = 20;

  cssBKAttStrFixed = 'fixed';
  cssBKAttStrScroll = 'scroll';
  cssBKRepStrRepeat = 'repeat';
  cssBKRepStrNoRepeat = 'no-repeat';

  rv_cssBkAttachment : array[TBackgroundStyle] of PRVAnsiChar
      = ('', cssBKAttStrFixed, cssBKAttStrFixed, cssBKAttStrScroll, cssBKAttStrFixed,
        cssBKAttStrFixed, cssBKAttStrFixed, cssBKAttStrFixed, cssBKAttStrFixed);
  rv_cssBkRepeat     : array[TBackgroundStyle] of PRVAnsiChar =
        ('', cssBKRepStrNoRepeat, cssBKRepStrRepeat, cssBKRepStrRepeat,
        cssBKRepStrNoRepeat, cssBKRepStrNoRepeat, cssBKRepStrNoRepeat,
        cssBKRepStrNoRepeat, cssBKRepStrNoRepeat);

procedure RV_RegisterHTMLGraphicFormat(ClassType: TGraphicClass);
  {$IFDEF RICHVIEWDEF6}deprecated {$IFDEF RICHVIEWDEF2009}'Use RVGraphicHandler.RegisterHTMLGraphicFormat'{$ENDIF};{$ENDIF}
procedure RV_RegisterPngGraphic(ClassType: TGraphicClass);
  {$IFDEF RICHVIEWDEF6}deprecated {$IFDEF RICHVIEWDEF2009}'Use RVGraphicHandler.RegisterPngGraphic'{$ENDIF};{$ENDIF}
function RV_IsHTMLGraphicFormat(gr: TGraphic): Boolean;
  {$IFDEF RICHVIEWDEF6}deprecated {$IFDEF RICHVIEWDEF2009}'Use RVGraphicHandler.IsHTMLGraphic'{$ENDIF};{$ENDIF}
function StringToHTMLString(const s: String; Options: TRVSaveOptions;
  RVStyle: TRVStyle): TRVRawByteString;
function StringToHTMLString2(const s: String; Options: TRVSaveOptions;
  CodePage: TRVCodePage): TRVRawByteString;
function StringToHTMLString3(const s: String; UTF8: Boolean;
  CodePage: TRVCodePage): TRVRawByteString;

implementation
uses RVFMisc, RVStr,
  {$IFDEF RICHVIEWDEF10}
  Types,
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  UITypes,
  {$ENDIF}
  {$IFNDEF RVDONOTUSESEQ}
  RVNote, RVSidenote,
  {$ENDIF}
  {$IFNDEF RVDONOTUSETABLES}
  RVTable,
  {$ENDIF}
  RVRTFProps, RVGrHandler;

const RVF_DOCPROP_TEXTSTYLES  = 1;
      RVF_DOCPROP_PARASTYLES  = 2;
      RVF_DOCPROP_LISTSTYLES  = 4;
      RVF_DOCPROP_LAYOUT      = 3;
      RVF_DOCPROP_DOCPROPLIST = 5;
      RVF_DOCPROP_PREVMARKERS = 6;
      RVF_DOCPROP_DOCPARAMETERS = 7;
      RVF_DOCPROP_EXTRADOCUMENT = 8;
      RVF_DOCPROP_UNITS       = 9;
      RVF_DOCPROP_STYLETEMPLATES = 10;

const RVFVersion = 1;
      RVFSubVersion = 3;
      RVFSubSubVersion: TRVAnsiString =
      {$IFDEF RVUNICODESTR}
        ' 1';
      {$ELSE}
        '';
      {$ENDIF}

// Constants related to OnProgress event
const QUICKTEXTSIZE = 1000000;
      QUICKRVFSIZE = 1000000;
      QUICKITEMCOUNT = 1000;
      PROGRESSMAXCOUNTER = 500;

const
  crlf = #13#10;
{==============================================================================}
{ Raises an exception - error in processing Unicode text.                      }
procedure RVRaiseUni;
begin
  raise ERichViewError.Create(errRVUnicode);
end;
{------------------------------------------------------------------------------}
{ Raises an exception is Length is odd value. It's used to check lengths of
  "raw Unicode" string.                                                        }
procedure RVCheckUni(Length: Integer);
begin
  if Length mod 2 <> 0 then
    RVRaiseUni;
end;
{========================== HTML Graphic Classes ==============================}
{ Registers the graphic class ClassType as an HTML graphic class.              }
procedure RV_RegisterHTMLGraphicFormat(ClassType: TGraphicClass);
begin
  RVGraphicHandler.RegisterHTMLGraphicFormat(ClassType);
end;
{------------------------------------------------------------------------------}
{ Is this a picture of HTML graphic class?                                     }
function RV_IsHTMLGraphicFormat(gr: TGraphic): Boolean;
begin
  Result := RVGraphicHandler.IsHTMLGraphic(gr);
end;
{================================ Png =========================================}
procedure RV_RegisterPngGraphic(ClassType: TGraphicClass);
begin
  RVGraphicHandler.RegisterPngGraphic(ClassType);
end;
{================================= HTML =======================================}
{ Converts s to ANSI or UTF-8, depending on Options.
  The conversion uses RVStyle.DefCodePage }
function StringToHTMLString(const s: String; Options: TRVSaveOptions;
  RVStyle: TRVStyle): TRVRawByteString;
begin
  if rvsoUTF8 in Options then
    {$IFDEF RVUNICODESTR}
    Result := Utf8Encode(s)
    {$ELSE}
    Result := RVU_AnsiToUTF8(RVStyle.DefCodePage, s)
    {$ENDIF}
  else
    {$IFDEF RVUNICODESTR}
    Result := RVU_UnicodeToAnsi(RVStyle.DefCodePage, RVU_GetRawUnicode(s));
    {$ELSE}
    Result := s;
    {$ENDIF}
end;
{------------------------------------------------------------------------------}
{ Converts s to ANSI or UTF-8, depending on Options.
  The conversion uses CodePage }
function StringToHTMLString2(const s: String; Options: TRVSaveOptions;
  CodePage: TRVCodePage): TRVRawByteString;
begin
  if rvsoUTF8 in Options then
    {$IFDEF RVUNICODESTR}
    Result := Utf8Encode(s)
    {$ELSE}
    Result := RVU_AnsiToUTF8(CodePage, s)
    {$ENDIF}
  else
    {$IFDEF RVUNICODESTR}
    Result := RVU_UnicodeToAnsi(CodePage, RVU_GetRawUnicode(s));
    {$ELSE}
    Result := s;
    {$ENDIF}
end;
{ The same, but boolean instead of Options }
function StringToHTMLString3(const s: String; UTF8: Boolean;
  CodePage: TRVCodePage): TRVRawByteString;
begin
  if UTF8 then
    {$IFDEF RVUNICODESTR}
    Result := Utf8Encode(s)
    {$ELSE}
    Result := RVU_AnsiToUTF8(CodePage, s)
    {$ENDIF}
  else
    {$IFDEF RVUNICODESTR}
    Result := RVU_UnicodeToAnsi(CodePage, RVU_GetRawUnicode(s));
    {$ELSE}
    Result := s;
    {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure CalcLocalURL(var Url, LocalUrl: String);
var p: Integer;
    Prefix: String;
begin
  if (Url<>'') and (Url[1]='#') then begin
    LocalUrl := Copy(Url, 2, Length(Url));
    Url := '';
    exit;
  end;
  Prefix := '';
  while True do begin
    p := AnsiPos('#', Url);
    if p>0 then begin
      LocalUrl := Copy(Url, p+1, Length(Url));
      Url := Copy(Url, 1, p-1);
      p := AnsiPos('.', Url);
      if p<>0 then
        break
      else begin
        Prefix := Url+'#'; // # is in a file name; continuing
        Url := LocalUrl;
        LocalUrl := '';
      end;
      end
    else begin
      LocalUrl := '';
      break;
    end;
  end;
  Url := Prefix + Url;
end;
{============================== TRVThumbnailCache =============================}
constructor TRVThumbnailCache.Create;
begin
  inherited Create;
  MaxCount := RichViewMaxThumbnailCount;
end;
{------------------------------------------------------------------------------}
procedure TRVThumbnailCache.AddThumbnail(ImageOwner: TObject);
var Index: Integer;
    DelImageOwner: TObject;
begin
  if MaxCount<0 then
    exit;
  Index := IndexOf(ImageOwner);
  if Index>=0 then
    Move(Index, Count-1)
  else begin
    if (MaxCount>0) and (Count=MaxCount) then begin
      DelImageOwner := Items[Count-1];
      if DelImageOwner is TRVNonTextItemInfo then
        TRVNonTextItemInfo(DelImageOwner).FreeThumbnail
      else if DelImageOwner is TCustomRVData then
        TCustomRVData(DelImageOwner).FreeBackgroundThumbnail;
      Delete(Count-1);
    end;
    if MaxCount>0 then
      Add(ImageOwner);
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVThumbnailCache.RemoveThumbnail(ImageOwner: TObject);
var Index: Integer;
begin
  Index := IndexOf(ImageOwner);
  if Index>=0 then
    Delete(Index);
end;
{================================ TRTFFontTable ===============================}
{ Returns an index of (FontName, Charset) item, or -1 if not found.
  Charset is not supported in D2/CB1 version.
  FontName is case insensitive.                                                }
function TRVRTFFontTable.Find(const FontName: String
  {$IFDEF RICHVIEWCBDEF3}; Charset: TFontCharset{$ENDIF}): Integer;
var i: Integer;
begin
  for i := 0 to Count-1 do
    if (AnsiCompareText(Items[i].FontName,FontName)=0)
       {$IFDEF RICHVIEWCBDEF3}
       and (Items[i].Charset = Charset)
       {$ENDIF}
       then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;
{------------------------------------------------------------------------------}
{ Adds (FontName, Charset) item if it does not present.
  In any case, returns an index of (FontName, Charset) item.
  Charset is not supported in D2/CB1 version.
  FontName is case insensitive.                                                }  
function TRVRTFFontTable.AddUnique(const FontName: String
 {$IFDEF RICHVIEWCBDEF3}; Charset: TFontCharset{$ENDIF}): Integer;
var item: TRVRTFFontTableItem;
begin
  Result := Find(FontName{$IFDEF RICHVIEWCBDEF3}, Charset{$ENDIF});
  if Result<0 then begin
    item := TRVRTFFontTableItem.Create;
    item.FontName := FontName;
    {$IFDEF RICHVIEWCBDEF3}
    item.Charset := Charset;
    {$ENDIF}
    Add(item);
    Result := Count-1;
  end;
end;
{------------------------------------------------------------------------------}
{ Reads Items[Index]                                                            }
function TRVRTFFontTable.Get(Index: Integer): TRVRTFFontTableItem;
begin
  Result := TRVRTFFontTableItem(inherited Get(Index));
end;
{------------------------------------------------------------------------------}
{ Writes Items[Index]                                                          }
procedure TRVRTFFontTable.Put(Index: Integer; const Value: TRVRTFFontTableItem);
begin
  inherited Put(Index, Value);
end;
{================================== TRVFRTFTables =============================}
constructor TRVRTFTables.Create;
begin
  inherited Create;
  FFontTable := TRVRTFFontTable.Create;
  FColorList := TRVColorList.Create;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  FStyleSheetMap := TRVIntegerList.Create; 
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
destructor TRVRTFTables.Destroy;
begin
  FFontTable.Free;
  FColorList.Free;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  FStyleSheetMap.Free;
  {$ENDIF}
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TRVRTFTables.ClearColors;
begin
  FColorList.Clear;
end;
{------------------------------------------------------------------------------}
procedure TRVRTFTables.ClearFonts;
begin
  FFontTable.Clear;
end;
{------------------------------------------------------------------------------}
procedure TRVRTFTables.AddColor(Color: TColor);
begin
  FColorList.AddUnique(Color);
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.GetColorIndex(Color: TColor): Integer;
begin
  Result := FColorList.IndexOf(Pointer(Color));
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.GetFontStyleIndex(TextStyle: TCustomRVFontInfo): Integer;
begin
  Result := FFontTable.Find(TextStyle.FontName
    {$IFDEF RICHVIEWCBDEF3},TextStyle.Charset{$ENDIF});
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.GetFontIndex(Font: TFont): Integer;
begin
  Result := FFontTable.Find(Font.Name
    {$IFDEF RICHVIEWCBDEF3},Font.Charset{$ENDIF});
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.AddFontFromStyle(TextStyle: TCustomRVFontInfo): Integer;
begin
  Result := FFontTable.AddUnique(TextStyle.FontName
    {$IFDEF RICHVIEWCBDEF3},TextStyle.Charset{$ENDIF});
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.AddFontFromStyleEx(TextStyle: TCustomRVFontInfo;
  ValidProperties: TRVFontInfoProperties; Root: Boolean): Integer;
{$IFDEF RICHVIEWCBDEF3}var Charset: TFontCharset;{$ENDIF}
var FontName: String;
    HasFont: Boolean;
begin
  HasFont := Root;
  if rvfiFontName in ValidProperties then begin
    FontName := TextStyle.FontName;
    HasFont := True;
    end
  else
    FontName := RVFONT_ARIAL;
  {$IFDEF RICHVIEWCBDEF3}
  if rvfiCharset in ValidProperties then begin
    Charset := TextStyle.Charset;
    HasFont := True;
    end
  else
    Charset := DEFAULT_CHARSET;
  {$ENDIF}
  if HasFont then
    Result := FFontTable.AddUnique(FontName
      {$IFDEF RICHVIEWCBDEF3},Charset{$ENDIF})
  else
    Result := -1;
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.AddFont(Font: TFont): Integer;
begin
  Result := FFontTable.AddUnique(Font.Name
    {$IFDEF RICHVIEWCBDEF3},Font.Charset{$ENDIF});
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure TRVRTFTables.ClearStyleSheetMap;
begin
  FStyleSheetMap.Clear;
end;
{------------------------------------------------------------------------------}
procedure TRVRTFTables.AddStyleSheetMap(StyleTemplateNo: Integer);
begin
  FStyleSheetMap.Add(StyleTemplateNo);
end;
{------------------------------------------------------------------------------}
function TRVRTFTables.GetStyleSheetIndex(StyleTemplateNo: Integer;
  CharStyle: Boolean): Integer;
var i: Integer;
begin
  for i := 0 to FStyleSheetMap.Count-1 do
    if FStyleSheetMap[i]=StyleTemplateNo then begin
      Result := i;
      if CharStyle and (i+1<FStyleSheetMap.Count) and
        (FStyleSheetMap[i+1]=StyleTemplateNo) then
        inc(Result);
      exit;
    end;
  Result := -1;
end;
{$ENDIF}
{============================ TRVLayoutInfo ===================================}
{ Constructor.                                                                 }
constructor TRVLayoutInfo.Create;
begin
  inherited Create;
  FirstMarkerListNo := -1;
end;
{------------------------------------------------------------------------------}
{ Loads iteslf from the Stream.
  If IncludeSize=True, first reads size (4 bytes) of the rest of data; reports
  error if the size is too small; reads at least size bytes (for compatibility
  with possible future extensions).                                            }
procedure TRVLayoutInfo.LoadFromStream(Stream: TStream; IncludeSize: Boolean);
var v, version: Integer;
const defsize1 = sizeof(Integer)*(4+2)+sizeof(TRVBiDiMode);
begin
   if IncludeSize then
     Stream.ReadBuffer(v, sizeof(Integer)); // ignoring
   Stream.ReadBuffer(version,      sizeof(Integer));
   Stream.ReadBuffer(v,            sizeof(Integer));
   if (version=0) and (v<defsize1) then
     raise ERichViewError.Create(errRVFDocProp);
   if version=0 then begin
     Stream.ReadBuffer(LeftMargin,   sizeof(Integer));
     Stream.ReadBuffer(RightMargin,  sizeof(Integer));
     Stream.ReadBuffer(TopMargin,    sizeof(Integer));
     Stream.ReadBuffer(BottomMargin, sizeof(Integer));
     Stream.ReadBuffer(MinTextWidth, sizeof(Integer));
     Stream.ReadBuffer(MaxTextWidth, sizeof(Integer));
     Stream.ReadBuffer(BiDiMode,     sizeof(TRVBiDiMode));
     dec(v, defsize1);
   end;
   if v>=sizeof(Integer)*4 then begin
     Stream.ReadBuffer(FirstParaAborted, sizeof(Integer));
     Stream.ReadBuffer(LastParaAborted, sizeof(Integer));
     Stream.ReadBuffer(FirstMarkerListNo, sizeof(Integer));
     Stream.ReadBuffer(FirstMarkerLevel, sizeof(Integer));
     dec(v, sizeof(Integer)*4);
   end;
   if v>0 then
     Stream.Seek(v,soFromCurrent);
   Loaded := True;
end;
{------------------------------------------------------------------------------}
{ Saves itself to the stream.
  If IncluseSize=True, first saves its size (4 bytes).
  Size is usually processed by RVF loading procedures.                         }
procedure TRVLayoutInfo.SaveToStream(Stream: TStream;
  IncludeSize, OnlyPageInfo: Boolean);
var v,size,version: Integer;
const defsize1 = sizeof(Integer)*(4+2)+sizeof(TRVBiDiMode);
begin
   if OnlyPageInfo then begin
     size := 0;
     version := 1;
     end
   else begin
     size := defsize1;
     version := 0;
   end;
   if (FirstParaAborted<>0) or (LastParaAborted<>0) then
     inc(size, sizeof(Integer)*4);
   if IncludeSize then begin
     v := size+sizeof(Integer)*2;
     Stream.WriteBuffer(v, sizeof(Integer));
   end;
   Stream.WriteBuffer(version,      sizeof(Integer));
   v := size;
   Stream.WriteBuffer(v,            sizeof(Integer));
   if not OnlyPageInfo then begin
     Stream.WriteBuffer(LeftMargin,   sizeof(Integer));
     Stream.WriteBuffer(RightMargin,  sizeof(Integer));
     Stream.WriteBuffer(TopMargin,    sizeof(Integer));
     Stream.WriteBuffer(BottomMargin, sizeof(Integer));
     Stream.WriteBuffer(MinTextWidth, sizeof(Integer));
     Stream.WriteBuffer(MaxTextWidth, sizeof(Integer));
     Stream.WriteBuffer(BiDiMode,     sizeof(TRVBiDiMode));
   end;
   if (FirstParaAborted<>0) or (LastParaAborted<>0) then begin
     Stream.WriteBuffer(FirstParaAborted, sizeof(Integer));
     Stream.WriteBuffer(LastParaAborted, sizeof(Integer));
     Stream.WriteBuffer(FirstMarkerListNo, sizeof(Integer));
     Stream.WriteBuffer(FirstMarkerLevel, sizeof(Integer));
   end;
end;
{------------------------------------------------------------------------------}
{ Loads itself from the hexadecimal string: extracts the string to a temporal
  memory stream, and calls LoadFromStream(..., False).                         }
procedure TRVLayoutInfo.LoadText(const s: TRVAnsiString);
var TmpStream: TRVMemoryStream;
begin
   TmpStream := TRVMemoryStream.Create;
   try
     RVFTextString2Stream(s, TmpStream);
     TmpStream.Position := 0;
     LoadFromStream(TmpStream, False);
   finally
     TmpStream.Free;
   end;
end;
{------------------------------------------------------------------------------}
{ Loads itself from the binary string: copies the string to a temporal memory
  stream, and calls LoadFromStream(..., False).                                }
procedure TRVLayoutInfo.LoadBinary(const s: TRVRawByteString);
var TmpStream: TMemoryStream;
begin
   TmpStream := TMemoryStream.Create;
   try
     TmpStream.WriteBuffer(PRVAnsiChar(s)^, Length(s));
     TmpStream.Position := 0;
     LoadFromStream(TmpStream, False);
   finally
     TmpStream.Free;
   end;
end;
{------------------------------------------------------------------------------}
{ Saves itself to stream as a hexadecimal string that can be loaded by
  LoadText.                                                                    }
procedure TRVLayoutInfo.SaveTextToStream(Stream: TStream; OnlyPageInfo: Boolean);
var TmpStream: TRVMemoryStream;
    s: TRVAnsiString;
begin
   TmpStream := TRVMemoryStream.Create;
   try
     SaveToStream(TmpStream, False, OnlyPageInfo);
     TmpStream.Position := 0;
     s := RVFStream2TextString(TmpStream);
     RVFWriteLine(Stream, s);
   finally
     TmpStream.Free;
   end;
end;
{$I+}
{================================ TCustomRVData ===============================}
constructor TCustomRVData.Create;
begin
  inherited Create;
  if not ShareItems then
    FItems := TRVItemList.Create;
  FAllowNewPara  := True;
  CPCount        := 0;
  State          := [];
end;
{------------------------------------------------------------------------------}
destructor TCustomRVData.Destroy;
begin
  Clear;
  if not ShareItems then
    RVFreeAndNil(FItems);
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SavePicture(DocumentSaveFormat: TRVSaveFormat;
  const imgSavePrefix, Path: String; var imgSaveNo: Integer;
  OverrideFiles: Boolean; CurrentFileColor: TColor;
  gr: TGraphic): String;
var fn: String;
    bmp: TBitmap;
    ext: String;
    jpg: TGraphic;
begin
   if (DocumentSaveFormat=rvsfHTML) and RVGraphicHandler.IsJpegClassAssigned then begin
     ext := '.jpg';
     if RVGraphicHandler.IsHTMLGraphic(gr) then
       ext := '.'+ RVGraphicHandler.GetGraphicExt(gr);
     end
   else
     ext := '.bmp';
   fn := GetNextFileName(imgSavePrefix, Path, Ext, imgSaveNo, OverrideFiles);
   Result := ExtractFilePath(imgSavePrefix);
   if (Length(Result)>0) and (Result[Length(Result)]<>'\') then
     Result := Result+'\';
   Result := Result+ExtractFileName(fn);
   if (DocumentSaveFormat=rvsfHTML) and RVGraphicHandler.IsHTMLGraphic(gr) then begin
     gr.SaveToFile(fn);
     exit;
   end;
   bmp := TBitmap.Create;
   try
     {$IFDEF RICHVIEWCBDEF3}
     bmp.PixelFormat := pf32bit;
     {$ENDIF}
     bmp.Height := gr.Height;
     bmp.Width := gr.Width;
     if CurrentFileColor=clNone then
       CurrentFileColor := clWhite;
     bmp.Canvas.Brush.Color := CurrentFileColor;
     bmp.Canvas.Pen.Color := CurrentFileColor;
     GetRVStyle.GraphicInterface.FillRect(bmp.Canvas, Rect(0,0,gr.Width,gr.Height));
     GetRVStyle.GraphicInterface.DrawGraphic(bmp.Canvas, 0, 0, gr);
     if DocumentSaveFormat=rvsfHTML then begin
       jpg := RVGraphicHandler.CreateGraphicByType(rvgtJPEG);
       try
         jpg.Assign(bmp);
         jpg.SaveToFile(fn);
       finally
         jpg.Free;
       end;
       end
     else
       bmp.SaveToFile(fn);
   finally
     bmp.Free;
   end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.DoSavePicture(DocumentSaveFormat: TRVSaveFormat;
  RVData: TCustomRVData; ItemNo: Integer;
  const imgSavePrefix, Path: String; var imgSaveNo: Integer;
  OverrideFiles: Boolean; CurrentFileColor: TColor;
  gr: TGraphic): String;
var DoDefault: Boolean;
begin
   Result := '';
   SaveImage2(RVData, ItemNo, gr, DocumentSaveFormat, Path, imgSavePrefix,
     imgSaveNo, Result, DoDefault);
   if not DoDefault then
     exit;
  Result := SavePicture(DocumentSaveFormat, imgSavePrefix, Path, imgSaveNo,
    OverrideFiles, CurrentFileColor, gr);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.ItemLength(ItemNo: Integer): Integer;
begin
  with GetItem(ItemNo) do
    if StyleNo<0 then
      Result := 1
    else
      Result := RVU_Length(Items[ItemNo], ItemOptions);
end;
{------------------------------------------------------------------------------}
{ Returns the file name in the directory Path. File name is built as
  ImagesPrefix + <number> + Ext.
  If OverrideFiles=True, <number> is imgSaveNo+1.
  If not, <number> is increased until file name does not belong to an existing
  file.
  On exit, imgSaveNo = <number>.
  Notes:
  - ImagesPrefix can contain path. It may be the full path (contains ':')
    or relative path. In the last case the file is assumed to be in
    Path + ExtractFilePath(ImagesPrefix).
  - It's assumed that the directory exists. }
function TCustomRVData.GetNextFileName(const ImagesPrefix, Path, Ext: String;
  var imgSaveNo: Integer; OverrideFiles: Boolean): String;
var FullPath: String;
begin
  if {$IFDEF RICHVIEWCBDEF3}AnsiPos{$ELSE}Pos{$ENDIF}(':',ImagesPrefix)>0 then
    FullPath := ImagesPrefix
  else
    FullPath := Path+ImagesPrefix;
  while True do begin
    inc(imgSaveNo);
    Result := FullPath+IntToStr(imgSaveNo)+Ext;
    if not FileExists(Result) then
      exit;
    {$WARNINGS OFF}
    if OverrideFiles and ((FileGetAttr(Result) and faReadOnly)=0) then
      exit;
    {$WARNINGS ON}
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AdjustItemParaNoBeforeAdding(Item: TCustomRVItemInfo);
begin
  if (Item.ParaNo=-1) and (Items.Count<>0) and
     not GetItem(Items.Count-1).GetBoolValue(rvbpFullWidth) then begin
    Item.SameAsPrev := True;
    Item.ParaNo := GetItemPara(Items.Count-1);
    end
  else begin
    {$IFNDEF RVDONOTUSELISTS}
    if (Items.Count<>0) and (GetItemStyle(Items.Count-1)=rvsListMarker) then
      AddNLR('',0,-1);
    {$ENDIF}
    Item.SameAsPrev := False;
    Item.BR := (Item.BR  or not FAllowNewPara) and not Item.GetBoolValue(rvbpFullWidth) and
      (Items.Count<>0);
    if Item.ParaNo=-1 then
      Item.ParaNo := 0;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddItemR(const Text: TRVRawByteString;
  Item: TCustomRVItemInfo; AdjustPara: Boolean);
var s: TRVRawByteString;
    OldCP: TRVCPInfo;
begin
  if AdjustPara then
    AdjustItemParaNoBeforeAdding(Item);
  if Item.Checkpoint<>nil then begin
    OldCP := Item.Checkpoint;
    with Item.Checkpoint do
      AddNamedCheckpointExTag(Name, RaiseEvent, Tag);
    OldCP.Free;
  end;
  SetCP(Item, LastCP, NotAddedCP);
  Item.UpdatePaletteInfo(GetDoInPaletteMode, False, GetRVPalette, GetRVLogPalette);
  s := Text;
  Item.Inserting(Self, s, not (rvstClearing in State));
  Items.AddObject(s, Item);
  Item.Inserted(Self, Items.Count-1);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddItem(const Text: String; Item: TCustomRVItemInfo);
begin
  AdjustItemParaNoBeforeAdding(Item);
  AddItemR(
    RVU_StringToRawByteString(Text, rvioUnicode in Item.ItemOptions,
      GetItemCodePage2(Item)),
    Item, False);
  {$IFNDEF RVDONOTUSESEQ}
  AddSeqInList(ItemCount-1);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddItemAsIsR(const Text: TRVRawByteString; Item: TCustomRVItemInfo);
var s: TRVRawByteString;
begin
  if Item.Checkpoint<>nil then
    with Item.Checkpoint do
      AddNamedCheckpointExTag(Name, RaiseEvent, Tag);
  SetCP(Item, LastCP, NotAddedCP);
  Item.UpdatePaletteInfo(GetDoInPaletteMode, False, GetRVPalette, GetRVLogPalette);
  s := Text;
  Item.Inserting(Self, s, not (rvstClearing in State));
  Items.AddObject(s, Item);
  Item.Inserted(Self, Items.Count-1);
end;
{------------------------------------------------------------------------------}
{ Does not replace tabs }
procedure TCustomRVData.AddNLRTag_(const s: TRVRawByteString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
var Item: TCustomRVItemInfo;
begin
  Item := RichViewTextItemClass.Create(Self);
  if StyleNo<0 then
    Item.StyleNo := rvsDefStyle
  else
    Item.StyleNo := StyleNo;
  Item.ParaNo  := ParaNo;
  AdjustItemParaNoBeforeAdding(Item);
  Item.Tag     := Tag;
  {$IFNDEF RVDONOTUSEUNICODE}
  if (GetRVStyle<>nil) and (GetRVStyle.TextStyles[GetActualStyle(Item)].Unicode) then
    Include(Item.ItemOptions, rvioUnicode);
  {$ENDIF}
  AddItemR(s, Item, False);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEUNICODE}
procedure TCustomRVData.AddNLATag_(const s: TRVAnsiString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
var ress: TRVRawByteString;
    LParaNo: Integer;
begin
  LParaNo := ParaNo;
  if (StyleNo<0) or (StyleNo=rvsDefStyle) then
    StyleNo := rvsDefStyle;
  if LParaNo=-1 then begin
   if Items.Count<>0 then
     LParaNo := GetItemPara(Items.Count-1)
   else
     LParaNo := 0;
  end;
  if (GetRVStyle<>nil) and
     (GetRVStyle.TextStyles[GetActualStyleEx(StyleNo, LParaNo)].Unicode) then
    ress := RVU_AnsiToUnicode(GetStyleCodePage(GetActualStyleEx(StyleNo, LParaNo)), s)
  else
    ress := s;
  AddNLRTag_(ress, StyleNo, ParaNo, Tag);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNLRTag(const s: TRVRawByteString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
var Item: TCustomRVItemInfo;
begin
  Item := RichViewTextItemClass.Create(Self);
  if StyleNo<0 then
    Item.StyleNo := rvsDefStyle
  else
    Item.StyleNo := StyleNo;
  Item.ParaNo  := ParaNo;
  Item.Tag     := Tag;
  AdjustItemParaNoBeforeAdding(Item);
  {$IFNDEF RVDONOTUSEUNICODE}
  if (GetRVStyle<>nil) and (GetRVStyle.TextStyles[GetActualStyle(Item)].Unicode) then
    Include(Item.ItemOptions, rvioUnicode);
  {$ENDIF}
  AddItemR(ReplaceTabs(s, GetActualStyle(Item), False), Item, False);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNLTag(const s: String; StyleNo, ParaNo: Integer;
  const Tag: TRVTag);
begin
  {$IFDEF RVUNICODESTR}
  AddNLWTag(s, StyleNo, ParaNo, Tag);
  {$ELSE}
  AddNLATag(s, StyleNo, ParaNo, Tag);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEUNICODE}
function TCustomRVData.GetTextInItemFormatA(ItemNo: Integer;
  const s: TRVAnsiString): TRVRawByteString;
begin
  if (GetItemStyle(ItemNo)>=0) and (rvioUnicode in GetItemOptions(ItemNo)) then
    Result := RVU_AnsiToUnicode(GetItemCodePage(ItemNo), s)
  else
    Result := s;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNLWTagRaw(const s: TRVRawByteString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
var ansis: TRVRawByteString;
begin
  ansis := s;
  if StyleNo<0 then
    StyleNo := rvsDefStyle;
  if (GetRVStyle<>nil) and
     not GetRVStyle.TextStyles[GetActualStyleEx(StyleNo, ParaNo)].Unicode then
    ansis := RVU_UnicodeToAnsi(GetStyleCodePage(GetActualStyleEx(StyleNo, ParaNo)), ansis);
  AddNLRTag(ansis, StyleNo, ParaNo, Tag);
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWCBDEF3}
function TCustomRVData.GetTextInItemFormatW(ItemNo: Integer;
  const s: TRVUnicodeString): TRVRawByteString;
begin
  Result := RVU_GetRawUnicode(s);
  if (GetItemStyle(ItemNo)<0) or not (rvioUnicode in GetItemOptions(ItemNo)) then
    Result := RVU_UnicodeToAnsi(GetItemCodePage(ItemNo), Result);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNLWTag(const s: TRVUnicodeString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
begin
  AddNLWTagRaw(RVU_GetRawUnicode(s), StyleNo, ParaNo, Tag);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTextExW(ItemNo: Integer;
  const SourceStr: TRVRawByteString): TRVUnicodeString;
var s: TRVRawByteString;
begin
  s := SourceStr;
  if (GetItemStyle(ItemNo)<0) then
    s := RVU_AnsiToUnicode(GetItemCodePage(ItemNo), s)
  else if not (rvioUnicode in GetItemOptions(ItemNo)) then
    if not IsSymbolCharset(ItemNo) then
      s := RVU_AnsiToUnicode(GetItemCodePage(ItemNo), s)
    else
      s := RVU_SymbolCharsetToUnicode(s);
  Result := RVU_RawUnicodeToWideString(s);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTextW(ItemNo: Integer): TRVUnicodeString;
begin
  Result := GetItemTextExW(ItemNo, Items[ItemNo]);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetItemTextW(ItemNo: Integer; const s: TRVUnicodeString);
begin
  Items[ItemNo] := GetTextInItemFormatW(ItemNo, s);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNLATag(const s: TRVAnsiString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
var ress: TRVRawByteString;
    LParaNo: Integer;
begin
  LParaNo := ParaNo;
  if StyleNo<0 then
    StyleNo := rvsDefStyle;
  if (GetRVStyle<>nil) and
     (GetRVStyle.TextStyles[GetActualStyleEx(StyleNo, LParaNo)].Unicode) then
    ress := RVU_AnsiToUnicode(GetStyleCodePage(GetActualStyleEx(StyleNo, LParaNo)), s)
  else
    ress := s;
  AddNLRTag(ress, StyleNo, ParaNo, Tag);
end;
{$ELSE}
procedure TCustomRVData.AddNLATag(const s: TRVAnsiString; StyleNo, ParaNo: Integer;
  const Tag: TRVTag);
begin
  AddNLRTag(s, StyleNo, ParaNo, Tag);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetItemTextA(ItemNo: Integer; const s: TRVAnsiString);
begin
  Items[ItemNo] :=
    {$IFNDEF RVDONOTUSEUNICODE}
    GetTextInItemFormatA(ItemNo, s);
    {$ELSE}
    s;
    {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetItemText(ItemNo: Integer; const s: String);
begin
  {$IFDEF RVUNICODESTR}
  SetItemTextW(ItemNo, s);
  {$ELSE}
  SetItemTextA(ItemNo, s);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTextExA(ItemNo: Integer; CodePage: TRVCodePage): TRVAnsiString;
{$IFNDEF RVDONOTUSEUNICODE}
var s: TRVRawByteString;
{$ENDIF}
begin
  Result := Items[ItemNo];
  {$IFNDEF RVDONOTUSEUNICODE}
  if (GetItemStyle(ItemNo)>=0) and (rvioUnicode in GetItemOptions(ItemNo)) then
    if not IsSymbolCharset(ItemNo) then
      Result := RVU_UnicodeToAnsi(CodePage, Result)
    else begin
      Result := RV_SymbolCharsetToAnsiCharset(Result);
      if CodePage<>CP_ACP then begin
        s := RVU_AnsiToUnicode(CP_ACP, Result);
        Result := RVU_UnicodeToAnsi(CodePage, s);
      end;
    end;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTextA(ItemNo: Integer): TRVAnsiString;
begin
  Result := GetItemTextExA(ItemNo, GetItemCodePage(ItemNo));
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemText(ItemNo: Integer): String;
begin
  {$IFDEF RVUNICODESTR}
  Result := GetItemTextW(ItemNo);
  {$ELSE}
  Result := GetItemTextA(ItemNo);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTextEx(ItemNo: Integer; CodePage: TRVCodePage): String;
begin
  {$IFDEF RVUNICODESTR}
  Result := GetItemTextW(ItemNo);
  {$ELSE}
  Result := GetItemTextExA(ItemNo, CodePage);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNLR(const s: TRVRawByteString; StyleNo, ParaNo: Integer);
begin
  AddNLRTag(s, StyleNo, ParaNo, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddNL(const s: String; StyleNo, ParaNo: Integer);
begin
  {$IFDEF RVUNICODESTR}
  AddNLWTag(s, StyleNo, ParaNo, RVEMPTYTAG);
  {$ELSE}
  AddNLATag(s, StyleNo, ParaNo, RVEMPTYTAG);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddFmt(const FormatStr: String; const Args: array of const;
  StyleNo, ParaNo: Integer);
begin
  AddNLTag(Format(FormatStr,Args), StyleNo, ParaNo, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddTextNLR(const s: TRVRawByteString; StyleNo,
  FirstParaNo, OtherParaNo : Integer
  {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
begin
  AddTextUniversal(s, StyleNo, FirstParaNo, OtherParaNo, False, False, CP_ACP,
    {$IFDEF RICHVIEWDEF4}Tag{$ELSE}RVEMPTYTAG{$ENDIF});
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddTextNL(const s: String; StyleNo,
  FirstParaNo, OtherParaNo : Integer
  {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
begin
  {$IFDEF RVUNICODESTR}
  AddTextNLW(s, StyleNo, FirstParaNo, OtherParaNo, False);
  {$ELSE}
  AddTextNLA(s, StyleNo, FirstParaNo, OtherParaNo
    {$IFDEF RICHVIEWDEF4}, Tag{$ENDIF});
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddTextNLA(const s: TRVAnsiString; StyleNo,
  FirstParaNo, OtherParaNo : Integer
  {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
begin
  AddTextUniversal(s, StyleNo, FirstParaNo, OtherParaNo, False, True, CP_ACP,
    {$IFDEF RICHVIEWDEF4}Tag{$ELSE}RVEMPTYTAG{$ENDIF});
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddTextBlockNLA(const s: TRVAnsiString; StyleNo, ParaNo: Integer
  {$IFDEF RICHVIEWDEF4};const Tag: TRVTag=RVEMPTYTAG{$ENDIF});
begin
  AddTextUniversal(s, StyleNo, ParaNo, ParaNo, True, True, CP_ACP,
    {$IFDEF RICHVIEWDEF4}Tag{$ELSE}RVEMPTYTAG{$ENDIF});
end;
{------------------------------------------------------------------------------}
function TCustomRVData.AddNamedCheckpointExTag(const CpName: String;
  RaiseEvent: Boolean; const Tag: TRVTag): Integer;
begin
  {$IFDEF RVALLOWCPBYCP}
  if NotAddedCP<>nil then begin
    Result := CPCount-1;
    exit;
  end;
  {$ELSE}
  if NotAddedCP<>nil then
    raise ERichViewError.Create(errCPByCP);
  {$ENDIF}
  NotAddedCP := TRVCPInfo.Create;
  NotAddedCP.Name := CPName;
  NotAddedCP.Tag := Tag;
  NotAddedCP.Next := nil;
  NotAddedCP.Prev := nil;
  //NotAddedCP.ItemNo := -1;
  NotAddedCP.RaiseEvent := RaiseEvent;
  Result := CPCount;
  inc(CPCount);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSETABS}
procedure TCustomRVData.AddTab(TextStyleNo, ParaNo: Integer);
var Item: TRVTabItemInfo;
begin
  Item := TRVTabItemInfo.Create(Self);
  Item.StyleNo := rvsTab;
  Item.AssociatedTextStyleNo := TextStyleNo;
  Item.ParaNo  := ParaNo;
  Item.SameAsPrev := ParaNo=-1;
  AddItemR('', Item, True);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddBreakExTag(Width: TRVStyleLength; Style: TRVBreakStyle;
  Color: TColor; const Tag: TRVTag);
var Item: TRVBreakItemInfo;
begin
  Item := TRVBreakItemInfo.CreateEx(Self, Width, Style, Color);
  Item.SameAsPrev := False;
  Item.ParaNo     := 0;
  Item.Tag        := Tag;
  AddItemR('',Item, True);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddBreakEx(Width: TRVStyleLength; Style: TRVBreakStyle;
  Color: TColor);
begin
  AddBreakExTag(Width, Style, Color, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddBreakTag(const Tag: TRVTag);
begin
  AddBreakExTag(1, rvbsLine, clNone, Tag);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddBreak;
begin
  AddBreakTag(RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddHotspotExTag(const Name: TRVAnsiString;
  ImageIndex, HotImageIndex: Integer; ImageList: TCustomImageList;
  ParaNo: Integer; const Tag: TRVTag);
var Item: TRVHotspotItemInfo;
begin
  Item := TRVHotspotItemInfo.CreateEx(Self, ImageIndex, HotImageIndex,
    ImageList, rvvaBaseLine);
  Item.ParaNo := ParaNo;
  Item.Tag := Tag;
  AddItemR(Name, Item, True);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddHotspotEx(const Name: TRVAnsiString;
  ImageIndex, HotImageIndex: Integer; ImageList: TCustomImageList;
  ParaNo: Integer);
begin
  AddHotspotExTag(Name, ImageIndex, HotImageIndex, ImageList, ParaNo, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddBulletExTag(const Name: TRVAnsiString;
  ImageIndex: Integer; ImageList: TCustomImageList;
  ParaNo: Integer; const Tag: TRVTag);
var Item: TRVBulletItemInfo;
begin
  Item            := TRVBulletItemInfo.CreateEx(Self, ImageIndex, ImageList, rvvaBaseline);
  Item.ParaNo     := ParaNo;
  Item.Tag        := Tag;
  AddItemR(Name, Item, True);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddBulletEx(const Name: TRVAnsiString; ImageIndex: Integer;
  ImageList: TCustomImageList; ParaNo: Integer);
begin
  AddBulletExTag(Name, ImageIndex, ImageList, ParaNo, RVEMPTYTAG)
end;
{------------------------------------------------------------------------------}
{ "gr" does not copied, do not free it!                                        }
procedure TCustomRVData.AddPictureExTag(const Name: TRVAnsiString; gr: TGraphic;
  ParaNo: Integer; VAlign: TRVVAlign; const Tag: TRVTag);
var Item: TRVGraphicItemInfo;
begin
  Item := TRVGraphicItemInfo.CreateEx(Self, gr, VAlign);
  Item.ParaNo  := ParaNo;
  Item.Tag     := Tag;
  AddItemR(Name, Item, True);
end;
{------------------------------------------------------------------------------}
{ gr does not copied, do not free it!                                          }
procedure TCustomRVData.AddPictureEx(const Name: TRVAnsiString; gr: TGraphic;
  ParaNo: Integer; VAlign: TRVVAlign);
begin
  AddPictureExTag(Name, gr, ParaNo, VAlign, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddHotPicture(const Name: TRVAnsiString; gr: TGraphic;
  ParaNo: Integer; VAlign: TRVVAlign);
begin
  AddHotPictureTag(Name, gr, ParaNo, VAlign, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddHotPictureTag(const Name: TRVAnsiString; gr: TGraphic;
  ParaNo: Integer; VAlign: TRVVAlign; const Tag: TRVTag);
var Item: TRVHotGraphicItemInfo;
begin
  Item := TRVHotGraphicItemInfo.CreateEx(Self, gr, VAlign);
  Item.ParaNo  := ParaNo;
  Item.Tag     := Tag;
  AddItemR(Name, Item, True);
end;
{------------------------------------------------------------------------------}
{ do not free ctrl yourself!                                                   }
procedure TCustomRVData.AddControlExTag(const Name: TRVAnsiString; ctrl: TControl;
  ParaNo: Integer; VAlign: TRVVAlign; const Tag: TRVTag);
var Item: TRVControlItemInfo;
begin
  Item         := TRVControlItemInfo.CreateEx(Self, ctrl, VAlign);
  Item.StyleNo := rvsComponent;
  Item.ParaNo  := ParaNo;
  Item.Tag     := Tag;
  AddItemR(Name, Item, True);
  ctrl.Parent := GetParentControl;
end;
{------------------------------------------------------------------------------}
{ do not free ctrl yourself!                                                   }
procedure TCustomRVData.AddControlEx(const Name: TRVAnsiString; ctrl: TControl;
  ParaNo: Integer; VAlign: TRVVAlign);
begin
  AddControlExTag(Name, ctrl, ParaNo, VAlign, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetAddParagraphMode(AllowNewPara: Boolean);
begin
 FAllowNewPara := AllowNewPara;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetCP(Item: TCustomRVItemInfo; var PrevCP, CP: TRVCPInfo);
begin
  if CP=nil then
    exit;
  CP.Prev := PrevCP;
  CP.ItemInfo := Item;
  if (PrevCP=nil) then begin // inserting before first, making first
    if FirstCP<>nil then
      FirstCP.Prev := CP;
    CP.Next := FirstCP;
    FirstCP := CP;
    end
  else
    CP.Next := PrevCP.Next;
  if PrevCP<>nil then
    PrevCP.Next := CP;
  if CP.Next<>nil then
    CP.Next.Prev := CP;
  if PrevCP=LastCP then
    LastCP := CP;
  Item.Checkpoint := CP;
  CP := nil;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.UnlinkCheckpoint(cp: TRVCPInfo; DecCPCount: Boolean);
begin
  if cp<>nil then begin
    cp.ItemInfo := nil;
    if FirstCP = cp then FirstCP := cp.Next;
    if LastCP = cp  then LastCP  := cp.Prev;
    if cp.Prev<>nil then cp.Prev.Next := cp.Next;
    if cp.Next<>nil then cp.Next.Prev := cp.Prev;
    if DecCPCount then
      dec(CPCount);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.FreeCheckpoint(var cp: TRVCPInfo; AdjustLinks,DecCPCount: Boolean);
begin
  if cp<>nil then begin
    if AdjustLinks then
      UnlinkCheckpoint(cp,False);
    {$IFDEF RVOLDTAGS}
    if rvoTagsArePChars in Options then
      StrDispose(PChar(cp.Tag));
    {$ENDIF}
    RVFreeAndNil(cp);
    if DecCPCount then
      dec(CPCount);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.ShareItems: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.CanLoadLayout: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DeleteItems(FirstItemNo, Count: Integer);
var i: Integer;
begin
  if ShareItems then exit;
  if FirstItemNo>=Items.Count then exit;
  if FirstItemNo+Count>Items.Count then
    Count := Items.Count-FirstItemNo;
  for i := FirstItemNo to FirstItemNo+Count-1 do
    FreeItem(i,False);
  for i :=1 to Count do
    Items.Delete(FirstItemNo);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DeleteSection(const CpName: String);
var startno, endno: Integer;
    cp: TRVCPInfo;
begin
  if ShareItems then exit;
   cp := FirstCP;
   startno := -1;
   endno := -1;
   while cp<>nil do begin
     if cp.Name=CpName then begin
       startno := Items.IndexOfObject(cp.ItemInfo);
       endno := Items.Count-1;
       break;
     end;
     cp := cp.Next;
   end;
   if startno=-1 then exit;
   cp := cp.Next;
   while cp<>nil do begin
     if cp.Name<>'' then begin
       endno := Items.IndexOfObject(cp.ItemInfo)-1;
       break;
     end;
     cp := cp.Next;
   end;
   DeleteItems(startno, endno-startno+1);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.InternalFreeItem(item: TCustomRVItemInfo; Clearing: Boolean);
begin
  if Item=nil then
    exit;
  {$IFNDEF RVDONOTUSESEQ}
  DeleteSeqFromList(item, Clearing);
  {$ENDIF}
  {$IFNDEF RVDONOTUSELISTS}
  DeleteMarkerFromList(item, Clearing);
  {$ENDIF}
  FreeCheckpoint(Item.Checkpoint, True, True);
  {$IFDEF RVOLDTAGS}
  if rvoTagsArePChars in Options then
    StrDispose(PChar(Item.Tag));
  {$ENDIF}
  if (Item is TRVNonTextItemInfo) and TRVNonTextItemInfo(Item).HasThumbnail then
    GetThumbnailCache.RemoveThumbnail(TRVNonTextItemInfo(Item));
  Item.Free;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.FreeItem(ItemNo: Integer; Clearing: Boolean);
var item: TCustomRVItemInfo;
    s: TRVRawByteString;
begin
  item := TCustomRVItemInfo(Items.Objects[ItemNo]);
  if item=nil then
    exit;
  s := Items[ItemNo];
  ItemAction(rviaDestroying, item, s, Self);
  ControlAction(Self, rvcaDestroy, ItemNo, item);
  InternalFreeItem(item, Clearing);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.Clear;
var i: Integer;
    Clearing: Boolean;
begin
  Clearing := rvstClearing in State;
  Include(State, rvstClearing);
  try
    Exclude(State, rvstFirstParaAborted);
    Exclude(State, rvstLastParaAborted);
    FFirstParaListNo := -1;
    FFirstParaLevel  := -1;
    if not ShareItems then begin
      for i:=0 to Items.Count-1 do
        FreeItem(i,True);
      Items.Clear;
    end;
    FreeCheckpoint(NotAddedCP, False, True);
    FirstCP := nil;
    LastCP  := nil;
    if GetDocProperties<>nil then
      GetDocProperties.Clear;
  finally
    if not Clearing then
      Exclude(State, rvstClearing);  
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetOffsBeforeItem(ItemNo: Integer): Integer;
begin
  if GetItemStyle(ItemNo)<0 then
    Result := 0
  else
    Result := 1;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetOffsAfterItem(ItemNo: Integer): Integer;
begin
  if GetItemStyle(ItemNo)<0 then
    Result := 1
  else
    Result := RVU_Length(Items[ItemNo], GetItemOptions(ItemNo))+1;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.ReplaceTabs(const s: TRVRawByteString; StyleNo: Integer;
  UnicodeDef: Boolean): TRVRawByteString;
begin
  if GetRVStyle = nil then begin
    {$IFNDEF RVDONOTUSEUNICODE}
    if UnicodeDef then
      Result := RV_ReplaceTabsW(s,8)
    else
    {$ENDIF}
      Result := RV_ReplaceTabsA(s,8)
    end
  else
    {$IFNDEF RVDONOTUSEUNICODE}
    if GetRVStyle.TextStyles[StyleNo].Unicode then
      Result := RV_ReplaceTabsW(s, GetRVStyle.SpacesInTab)
    else
    {$ENDIF}
      Result := RV_ReplaceTabsA(s, GetRVStyle.SpacesInTab);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddStringFromFile(const s: TRVAnsiString;
  StyleNo,ParaNo: Integer; FromNewLine, AsSingleParagraph: Boolean;
  var FirstTime, PageBreak: Boolean);
begin
  if not FromNewLine then
    ParaNo := -1;
  {$IFNDEF RVDONOTUSEUNICODE}
  AddNLATag(s,StyleNo,ParaNo,RVEMPTYTAG);
  {$ELSE}
  AddNLRTag(s,StyleNo,ParaNo,RVEMPTYTAG);
  {$ENDIF}
  if AsSingleParagraph and FirstTime then begin
    SetAddParagraphMode(False);
    FirstTime := False;
  end;
  if PageBreak then begin
    PageBreaksBeforeItems[Items.Count-1] := True;
    PageBreak := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadText(const FileName: String; StyleNo, ParaNo: Integer;
  AsSingleParagraph: Boolean; CodePage: TRVCodePage): Boolean;
var Stream: TFileStream;
begin
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      Result := LoadTextFromStream(Stream, StyleNo, ParaNo, AsSingleParagraph,
        CodePage)
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.AddTextUniversal(const text: TRVRawByteString;
  StyleNo, FirstParaNo, OtherParaNo: Integer;
  AsSingleParagraph, CheckUnicode: Boolean;
  CodePage: TRVCodePage; const Tag: TRVTag): Boolean;
var ANP: Boolean;
    {$IFNDEF RVDONOTUSEUNICODE}UnicodeStyle,{$ENDIF}
    {$IFDEF RVOLDTAGS}CopyTags,{$ENDIF}
    FromNewLine, FirstTime, ProcessPageBreaks, PageBreak, ProcessTabs: Boolean;
    ParaNo : Integer;
    fulltextstartptr, startptr, ptr, endptr: PRVAnsiChar;
    SkipIfEqual: TRVAnsiChar;
    TabItem: TRVTabItemInfo;
    CallProgress: Boolean;
    Progress: Integer;
    {........................................................}
    procedure AddTextItem;
    var AParaNo: Integer;
        ATag: TRVTag;
        s: TRVRawByteString;
        NewProgress: Integer;
    begin
      s := System.Copy(text, startptr-fulltextstartptr+1, ptr-startptr);
      if (s='') and not FromNewLine then
        exit;
      if FromNewLine or PageBreak then
        AParaNo := ParaNo
      else
        AParaNo := -1;
      {$IFDEF RVOLDTAGS}
      if CopyTags then
        ATag := RV_CopyTag(Tag, True)
      else
      {$ENDIF}
        ATag := Tag;
      {$IFNDEF RVDONOTUSEUNICODE}
      if not CheckUnicode then
      {$ENDIF}
        if ProcessTabs then
          AddNLRTag_(s, StyleNo, AParaNo, ATag)
        else
          AddNLRTag(s, StyleNo, AParaNo, ATag)
      {$IFNDEF RVDONOTUSEUNICODE}
      else if (CodePage<>CP_ACP) and UnicodeStyle then begin
        s := RVU_AnsiToUnicode(CodePage, s);
        if ProcessTabs then
          AddNLRTag_(s, StyleNo, AParaNo, ATag)
        else
          AddNLRTag(s, StyleNo, AParaNo, ATag)
        end
      else
        if ProcessTabs then
          AddNLATag_(s, StyleNo, AParaNo, ATag)
        else
          AddNLATag(s, StyleNo, AParaNo, ATag)
      {$ENDIF};
      FromNewLine := False;
      if PageBreak then begin
        PageBreaksBeforeItems[Items.Count-1] := True;
        PageBreak := False;
      end;
      if AsSingleParagraph and FirstTime then begin
        SetAddParagraphMode(False);
        FirstTime := False;
      end;
      if CallProgress then begin
        NewProgress := MulDiv(ptr-fulltextstartptr, 100, Length(Text));
        if NewProgress<>Progress then begin
          Progress := NewProgress;
          DoProgress(rvloTextRead, rvpstgRunning, Progress);
        end;
      end;
    end;
    {........................................................}
begin
  CallProgress := IsAssignedOnProgress and (Length(Text)>QUICKTEXTSIZE);
  if CallProgress then begin
    DoProgress(rvloTextRead, rvpstgStarting, 0);
    Progress := 0;
  end;
  ANP := FAllowNewPara;
  FirstTime := True;
  Result := True;
  ProcessPageBreaks := SupportsPageBreaks;
  PageBreak         := False;
  ProcessTabs       := (GetRVStyle<>nil) and (GetRVStyle.SpacesInTab<=0);
  {$IFNDEF RVDONOTUSEUNICODE}
  UnicodeStyle      := (GetRVStyle<>nil) and (StyleNo<>rvsDefStyle) and
    GetRVStyle.TextStyles[StyleNo].Unicode;
  {$ENDIF}
  {$IFDEF RVOLDTAGS}
  CopyTags          := (Tag<>0) and (rvoTagsArePChars in Options);
  {$ENDIF}
  ParaNo            := FirstParaNo;
  FromNewLine       := ParaNo>=0;
  try
    fulltextstartptr := PRVAnsiChar(text);
    startptr := fulltextstartptr;
    ptr      := startptr;
    endptr   := PRVAnsiChar(text)+Length(text);
    SkipIfEqual := #0;
    while ptr<endptr do begin
      if SkipIfEqual<>#0 then begin
        if (ptr^=SkipIfEqual) then begin
          inc(startptr);
          inc(ptr);
          SkipIfEqual := #0;
          continue;
        end;
        SkipIfEqual := #0;
      end;
      if ((ptr^) in [#10, #12, #13]) or (ProcessTabs and ((ptr^)=#9)) then begin
        AddTextItem;
        startptr := ptr+1;
      end;
      case ptr^ of
       #9: // tab
         begin
           if ProcessTabs then begin
             TabItem := TRVTabItemInfo.Create(Self);
             TabItem.StyleNo := rvsTab;
             TabItem.AssociatedTextStyleNo := StyleNo;
             if FromNewLine then
               TabItem.ParaNo := ParaNo
             else
               TabItem.ParaNo := -1;
             AddItemR('', TabItem, True);
             FromNewLine := False;
           end;
         end;
       #12: // page break
         begin
           PageBreak := ProcessPageBreaks;
           FromNewLine := True;
           ParaNo := OtherParaNo;
         end;
       #13:
         begin
           FromNewLine := True;
           SkipIfEqual := #10;
           ParaNo := OtherParaNo;
         end;
       #10:
         begin
           FromNewLine := True;
           SkipIfEqual := #13;
           ParaNo := OtherParaNo;
         end;
      end;
      inc(ptr);
    end;
    AddTextItem;
  except
    Result := False;
  end;
  SetAddParagraphMode(ANP);
  {$IFDEF RVOLDTAGS}
  if CopyTags then
    StrDispose(PChar(Tag));
  {$ENDIF}
  if CallProgress then
    DoProgress(rvloTextRead, rvpstgEnding, 0);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadTextFromStream(Stream: TStream; StyleNo,
  ParaNo: Integer; AsSingleParagraph: Boolean; CodePage: TRVCodePage):Boolean;
var FullText: TRVRawByteString;
begin
  if Stream.Size=Stream.Position then begin
    Result := True;
    exit;
  end;
  SetLength(FullText, Stream.Size-Stream.Position);
  Stream.ReadBuffer(PRVAnsiChar(FullText)^, Length(FullText));
  Replace0(FullText);
  Result := AddTextUniversal(FullText, StyleNo, ParaNo, ParaNo,
    AsSingleParagraph, True, CodePage, RVEMPTYTAG);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveTextToStream(const Path: String; Stream: TStream;
  LineWidth: Integer;
  SelectionOnly, TextOnly, Unicode, UnicodeWriteSignature: Boolean;
  CodePage: TRVCodePage; Root: Boolean):Boolean;
var i, StartItemNo,EndItemNo,StartOffs,EndOffs: Integer;
    SelectedItem: TCustomRVItemInfo;
    {$IFNDEF RVDONOTUSELISTS}
    MarkerItemNo: Integer;
    {$ENDIF}
    Item: TCustomRVItemInfo;
    s: TRVRawByteString;
    NotUsedPart: TRVMultiDrawItemPart;
    CustomSave: Boolean;
    {$IFNDEF RVDONOTUSEUNICODE}
    UniSign: Word;
    {$ENDIF}
    CallProgress: Boolean;
    Progress: Integer;
    RVStyle: TRVStyle;
    {..................................................}
    function GetStr(Item:TCustomRVItemInfo; const s: TRVRawByteString;
      CustomSave: Boolean) : TRVRawByteString;
    {$IFNDEF RVDONOTUSEUNICODE}
    var StyleNo: Integer;
    {$ENDIF}
    begin
      {$IFNDEF RVDONOTUSEUNICODE}
      if not CustomSave then begin
        if (Item=nil) or (Item.StyleNo<0) then begin
          if Unicode and ((Item=nil) or not Item.GetBoolValue(rvbpCanSaveUnicode)) then
            Result := RVU_AnsiToUnicode(GetDefaultCodePage, s)
          else
            Result := s
          end
        else if (rvioUnicode in Item.ItemOptions) and not Unicode then
          Result := RVU_UnicodeToAnsi(CodePage, s)
        else if not (rvioUnicode in Item.ItemOptions) and Unicode then begin
          StyleNo := GetActualStyle(Item);
          if CompareText(RVStyle.TextStyles[StyleNo].FontName, RVFONT_SYMBOL)=0 then
            Result := RV_MakeUniSymbolStrA(s)
          else if CompareText(RVStyle.TextStyles[StyleNo].FontName, RVFONT_WINGDINGS)=0 then
            Result := RV_MakeUniWingdingsStrA(s)
          else
            Result := RVU_AnsiToUnicode(GetStyleCodePage(StyleNo), s)
          end
        else if (rvioUnicode in Item.ItemOptions) and Unicode then begin
          StyleNo := GetActualStyle(Item);
          if CompareText(RVStyle.TextStyles[StyleNo].FontName, RVFONT_SYMBOL)=0 then
            Result := RV_MakeUniSymbolStrRaw(s)
          else if CompareText(RVStyle.TextStyles[StyleNo].FontName, RVFONT_WINGDINGS)=0 then
            Result := RV_MakeUniWingdingsStrRaw(s)
          else
            Result := s;
          end
        else
          Result := s;
        end
      else
      {$ENDIF}
        Result := s;
    end;
    {..................................................}
    function GetTextStr(ItemNo, StartOffs, EndOffs: Integer;
      var CustomSave: Boolean): TRVRawByteString;
    begin
      if TextOnly and
        (rvteoHidden in RVStyle.TextStyles[GetActualStyle(GetItem(ItemNo))].Options) then begin
        Result := '';
        exit;
      end;
      if StartOffs<0 then
        Result := Items[ItemNo]
      else
        Result := RVU_Copy(Items[ItemNo], StartOffs, EndOffs-StartOffs,
          GetItem(ItemNo).ItemOptions);
      CustomSave := SaveItemToFile(Path, Self, ItemNo, rvsfText, Unicode, Result);
    end;
    {..................................................}
    function GetNonTextStr(ItemNo, StartOffs, EndOffs: Integer;
      var CustomSave: Boolean): TRVRawByteString;
    var SaveUnicode: Boolean;
        Item: TCustomRVItemInfo;
    begin
      CustomSave := False;
      Item := GetItem(ItemNo);
      Result := '';
      if TextOnly and TRVNonTextItemInfo(Item).Hidden then
        exit;
      if (not TextOnly or Item.GetBoolValue(rvbpAlwaysInText)) and
         (StartOffs<EndOffs) then begin
        CustomSave := SaveItemToFile(Path, Self, ItemNo, rvsfText, Unicode, Result);
        if not CustomSave then begin
          {$IFNDEF RVDONOTUSEUNICODE}
          SaveUnicode := Unicode and Item.GetBoolValue(rvbpCanSaveUnicode);
          {$ELSE}
          SaveUnicode := False;
          {$ENDIF}
          Result := GetItem(ItemNo).AsText(LineWidth, Self, Items[ItemNo], Path,
            TextOnly, SaveUnicode, CodePage);
        end;
      end;
    end;
    {..................................................}
    procedure WriteCRLF;
    begin
      {$IFNDEF RVDONOTUSEUNICODE}
      if Unicode then
        RVFWrite(Stream, #13#0#10#0)
      else
      {$ENDIF}
        RVFWrite(Stream, crlf);
    end;
    {..................................................}
    {$IFNDEF RVDONOTUSESEQ}
    procedure WriteEndNotes;
    var EndNote: TRVEndnoteItemInfo;
        s: TRVRawByteString;
    begin
      if SelectionOnly or TextOnly or not (rvflRoot in Flags) then
        exit;
      EndNote := RVGetFirstEndnoteInRootRVData(Self, True);
      if EndNote=nil then
        exit;
      WriteCRLF;
      if LineWidth>=3 then begin
        SetLength(s, LineWidth div 3);
        FillChar(PRVAnsiChar(s)^, LineWidth div 3, ord('-'));
        {$IFNDEF RVDONOTUSEUNICODE}
        if Unicode then
          s := RVU_AnsiToUnicode(GetDefaultCodePage, s);
        {$ENDIF}
        RVFWrite(Stream, s);
      end;
      WriteCRLF;
      repeat
        EndNote.Document.SaveTextToStream(Path, Stream, 0, False, TextOnly,
          Unicode, False, CodePage, False);
        EndNote :=
          TRVEndnoteItemInfo(GetNextNote(Self, EndNote, TRVEndnoteItemInfo, True));
        if EndNote<>nil then
          WriteCRLF;
      until EndNote=nil;
    end;
    {$ENDIF}
    {..................................................}
begin
  RVStyle := GetRVStyle;
  CallProgress := False;
  try
    Result := True;
    if CodePage = CP_ACP then
      CodePage := GetDefaultCodePage;
    if not SelectionOnly then
      DoBeforeSaving;
    RVFGetLimits(GetRVFSaveScope(SelectionOnly), StartItemNo, EndItemNo,
      StartOffs, EndOffs, NotUsedPart, NotUsedPart, SelectedItem);
    if SelectedItem<>nil then begin
      SelectedItem.SaveTextSelection(Stream, Self, LineWidth, Path, TextOnly,
        Unicode, CodePage);
      exit;
    end;
    if (StartItemNo=-1) or (StartItemNo>EndItemNo) then
      exit;
    CallProgress := Root and SaveProgressInit(Progress, EndItemNo-StartItemNo,
      rvloTextWrite);
    {$IFNDEF RVDONOTUSEUNICODE}
    if Unicode and UnicodeWriteSignature then begin
      UniSign := UNI_LSB_FIRST;
      Stream.WriteBuffer(UniSign, 2);
    end;
    {$ENDIF}
    {$IFNDEF RVDONOTUSELISTS}
    if SelectionOnly then begin
      MarkerItemNo := GetFirstParaSectionItem(StartItemNo);
      if (GetItemStyle(MarkerItemNo)=rvsListMarker) then begin
        s := GetNonTextStr(MarkerItemNo, 0, 1, CustomSave);
        RVFWrite(Stream, GetStr(GetItem(MarkerItemNo), s, CustomSave));
      end;
    end;
    {$ENDIF}
    Item := GetItem(StartItemNo);
    if StartItemNo = EndItemNo then begin
      if Item.StyleNo<0 then
        s := GetNonTextStr(StartItemNo, StartOffs, EndOffs, CustomSave)
      else
        s := GetTextStr(StartItemNo, StartOffs, EndOffs, CustomSave);
      RVFWrite(Stream, GetStr(Item, s, CustomSave));
      end
    else begin
      if Item.StyleNo < 0 then
        s := GetNonTextStr(StartItemNo, StartOffs, 1, CustomSave)
      else
        s := GetTextStr(StartItemNo, StartOffs, RVU_Length(Items[StartItemNo],
          Item.ItemOptions)+1, CustomSave);
      RVFWrite(Stream, GetStr(Item, s, CustomSave));
      for i := StartItemNo+1 to EndItemNo-1 do begin
        Item := GetItem(i);
        if not (TextOnly and IsHiddenItem(i)) then
          if Item.PageBreakBefore and RichViewSavePageBreaksInText then
            RVFWrite(Stream, GetStr(nil, #$0C, False))
          else if not Item.SameAsPrev then
            WriteCRLF;
        if Item.StyleNo < 0 then
          s := GetNonTextStr(i, 0, 1, CustomSave)
        else
          s := GetTextStr(i, -1, -1, CustomSave);
        RVFWrite(Stream, GetStr(Item, s, CustomSave));
        if CallProgress then
          SaveProgressRun(Progress, i-StartItemNo+1, EndItemNo-StartItemNo,
            rvloTextWrite);
      end;
      Item := GetItem(EndItemNo);
      if not (TextOnly and IsHiddenItem(EndItemNo)) then
        if Item.PageBreakBefore and RichViewSavePageBreaksInText then
          RVFWrite(Stream, GetStr(nil, #$0C, False))
        else if not Item.SameAsPrev then
          WriteCRLF;
       if Item.StyleNo < 0 then
        s := GetNonTextStr(EndItemNo, 0, EndOffs, CustomSave)
      else
        s := GetTextStr(EndItemNo, 1, EndOffs, CustomSave);
      RVFWrite(Stream, GetStr(Item, s, CustomSave));
    end;
    {$IFNDEF RVDONOTUSESEQ}
    WriteEndNotes;
    {$ENDIF}
  except
    Result := False;
  end;
  if CallProgress then
    SaveProgressDone(rvloTextWrite);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveText(const FileName: String; LineWidth: Integer;
  Unicode: Boolean; CodePage: TRVCodePage): Boolean;
var Stream: TFileStream;
begin
  try
    Stream := TFileStream.Create(FileName, fmCreate);
    try
      Result := SaveTextToStream(ExtractFilePath(FileName), Stream, LineWidth,
        False, False, Unicode, Unicode, CodePage, True);
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEUNICODE}
procedure TCustomRVData.AddTextNLWRaw(const s: TRVRawByteString; StyleNo, FirstParaNo,
  OtherParaNo : Integer; DefAsSingleParagraph: Boolean);
var
    ParaNo: Integer;
    startptr,ptr,endptr: PWord;
    fulltextstartptr: PRVAnsiChar;
    SkipIfEqual: Word;
    ANP: Boolean;
    ProcessTabs, ProcessPageBreaks, PageBreak, FromNewLine: Boolean;
    TabItem: TRVTabItemInfo;
    CallProgress: Boolean;
    Progress, ProgressCounter, NewProgress: Integer;
    {.................................}
    procedure AddTextItem(AllowAddingEmpty: Boolean);
    var str: TRVRawByteString;
        AParaNo: Integer;
    begin
      if (startptr=ptr) and (not FromNewLine or not AllowAddingEmpty) then
        exit;
      str := Copy(s, PRVAnsiChar(startptr)-PRVAnsiChar(s)+1, PRVAnsiChar(ptr)-PRVAnsiChar(startptr));
      if FromNewLine or PageBreak then
        AParaNo := ParaNo
      else
        AParaNo := -1;
      AddNLWTagRaw(str, StyleNo, AParaNo, RVEMPTYTAG);
      FromNewLine := False;
      if PageBreak then begin
        PageBreaksBeforeItems[Items.Count-1] := True;
        PageBreak := False;
      end;
    end;
    {.................................}
begin
   ANP := FAllowNewPara;
   RVCheckUni(Length(s));
   CallProgress := IsAssignedOnProgress and (Length(s)>QUICKTEXTSIZE);
   if CallProgress then
     DoProgress(rvloTextRead, rvpstgStarting, 0);
   Progress := 0;
   ProgressCounter := PROGRESSMAXCOUNTER;
   fulltextstartptr := PRVAnsiChar(s);
   startptr := PWord(fulltextstartptr);
   endptr   := PWord(PRVAnsiChar(s)+Length(s));
   RVU_ProcessByteOrderMark(startptr, Length(s) div 2);
   ptr      := startptr;
   if ptr=endptr then begin
     if FirstParaNo<>-1 then
       AddNLR(s, StyleNo, FirstParaNo);
     exit;
   end;
   ParaNo := FirstParaNo;
   FromNewLine := ParaNo>=0;
   SkipIfEqual := 0;
   ProcessPageBreaks := SupportsPageBreaks;
   PageBreak         := False;
   ProcessTabs       := (GetRVStyle<>nil) and (GetRVStyle.SpacesInTab<=0);
   //SetAddParagraphMode(not DefAsSingleParagraph);
   while PRVAnsiChar(ptr)<PRVAnsiChar(endptr) do begin
     if SkipIfEqual<>0 then begin
       if (ptr^=SkipIfEqual) then begin
         inc(PRVAnsiChar(startptr),2);
         inc(PRVAnsiChar(ptr), 2);
         SkipIfEqual := 0;
         continue;
       end;
       SkipIfEqual := 0;
     end;
     case ptr^ of
       UNI_LineSeparator, UNI_VerticalTab:
         begin
           AddTextItem(True);
           SetAddParagraphMode(False);
           ParaNo := OtherParaNo;
           FromNewLine := True;
           startptr := PWord(PRVAnsiChar(ptr)+2);
         end;
       UNI_ParagraphSeparator:
         begin
           AddTextItem(True);
           SetAddParagraphMode(True);
           ParaNo := OtherParaNo;
           FromNewLine := True;
           startptr := PWord(PRVAnsiChar(ptr)+2);
         end;
       UNI_FF:
         begin
           AddTextItem(True);
           PageBreak := ProcessPageBreaks;
           ParaNo := OtherParaNo;
           FromNewLine := True;
           startptr := PWord(PRVAnsiChar(ptr)+2);
         end;
       UNI_CR:
         begin
           AddTextItem(True);
           SetAddParagraphMode(not DefAsSingleParagraph);
           SkipIfEqual := UNI_LF;
           ParaNo := OtherParaNo;
           FromNewLine := True;
           startptr := PWord(PRVAnsiChar(ptr)+2);
         end;
       UNI_LF:
         begin
           AddTextItem(True);
           SetAddParagraphMode(not DefAsSingleParagraph);
           SkipIfEqual := UNI_CR;
           ParaNo := OtherParaNo;
           FromNewLine := True;
           startptr := PWord(PRVAnsiChar(ptr)+2);
         end;
       UNI_Tab:
         begin
           if ProcessTabs then begin
             AddTextItem(False);
             TabItem := TRVTabItemInfo.Create(Self);
             TabItem.StyleNo := rvsTab;
             TabItem.AssociatedTextStyleNo := StyleNo;
             if FromNewLine then
               TabItem.ParaNo := ParaNo
             else
               TabItem.ParaNo := -1;
             AddItemR('', TabItem, True);
             FromNewLine := False;
             SetAddParagraphMode(not DefAsSingleParagraph);
             startptr := PWord(PRVAnsiChar(ptr)+2);
           end;
         end;
     end;
     inc(PRVAnsiChar(ptr), 2);
     if CallProgress then begin
       dec(ProgressCounter);
       if ProgressCounter=0 then begin
         ProgressCounter := PROGRESSMAXCOUNTER;
         NewProgress := MulDiv(PRVAnsiChar(ptr)-fulltextstartptr, 100, Length(s));
         if NewProgress<>Progress then begin
           Progress := NewProgress;
           DoProgress(rvloTextRead, rvpstgRunning, Progress);
         end;
       end;
     end;
   end;
   AddTextItem(True);
   SetAddParagraphMode(ANP);
   if CallProgress then
     DoProgress(rvloTextRead, rvpstgEnding, 0);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddTextNLW(const s: TRVUnicodeString; StyleNo, FirstParaNo,
  OtherParaNo: Integer; DefAsSingleParagraph: Boolean);
begin
  AddTextNLWRaw(RVU_GetRawUnicode(s), StyleNo, FirstParaNo, OtherParaNo,
    DefAsSingleParagraph);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadTextFromStreamW(Stream: TStream; StyleNo, ParaNo: Integer;
      DefAsSingleParagraph: Boolean):Boolean;
var s: TRVRawByteString;
begin
  Result := True;
  try
    RVCheckUni(Stream.Size-Stream.Position);
    SetLength(s, Stream.Size-Stream.Position);
    Stream.ReadBuffer(PRVAnsiChar(s)^,Stream.Size-Stream.Position);
    AddTextNLWRaw(s, StyleNo, ParaNo, ParaNo, DefAsSingleParagraph);
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadTextW(const FileName: String; StyleNo, ParaNo: Integer;
  DefAsSingleParagraph: Boolean): Boolean;
var Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    Result := LoadTextFromStreamW(Stream, StyleNo, ParaNo, DefAsSingleParagraph);
  finally
    Stream.Free;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.ShouldSaveTextToFormat(StyleNo: Integer;
  AllowedCode: TRVTextOption): Boolean;
var CodeOptions: TRVTextOptions;
begin
  CodeOptions := GetRVStyle.TextStyles[StyleNo].Options *
    [rvteoHTMLCode, rvteoRTFCode, rvteoDocXCode, rvteoDocXInRunCode];
  Result := (CodeOptions=[]) or (CodeOptions=[AllowedCode]);
end;
{$IFNDEF RVDONOTUSEHTML}
{------------------------------------------------------------------------------}
function TCustomRVData.SaveHTML(const FileName, Title, ImagesPrefix: String;
  Options: TRVSaveOptions; Color: TColor;
  var imgSaveNo: Integer;
  LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
  Background: TRVBackground): Boolean;
var Stream: TFileStream;
begin
  try
    Stream := TFileStream.Create(FileName, fmCreate);
    try
      Result := SaveHTMLToStream(Stream, ExtractFilePath(FileName),
        Title, ImagesPrefix, Options, Color, imgSaveNo,
        LeftMargin, TopMargin, RightMargin, BottomMargin,
        Background, nil);
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveHTMLEx(const FileName, Title, ImagesPrefix,
  ExtraStyles, ExternalCSS, CPPrefix: String; Options: TRVSaveOptions;
  Color: TColor; var CurrentFileColor: TColor;
  var imgSaveNo: Integer;
  LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
  Background: TRVBackground):Boolean;
var Stream: TFileStream;
begin
  try
    Stream := TFileStream.Create(FileName, fmCreate);
    try
      Result := SaveHTMLToStreamEx(Stream, ExtractFilePath(FileName),
        Title, ImagesPrefix, ExtraStyles, ExternalCSS, CPPrefix, Options,
        Color, CurrentFileColor, imgSaveNo, LeftMargin, TopMargin,
        RightMargin, BottomMargin, Background, nil{$IFNDEF RVDONOTUSELISTS},nil{$ENDIF});
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.ShouldSaveTextToHTML(StyleNo: Integer): Boolean;
begin
  Result := ShouldSaveTextToFormat(StyleNo, rvteoHTMLCode);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetHTMLATag(ItemNo: Integer; CSS: TRVRawByteString;
  Options: TRVSaveOptions): TRVAnsiString;
var Target, Extras: String;
begin
  WriteHyperlink(GetItem(ItemNo).JumpID+FirstJumpNo, Self, ItemNo, rvsfHTML,
    Target, Extras);
  if (Target<>'') or (Extras<>'') then begin
    if Extras<>'' then
      Extras := ' '+Extras;
    if CSS<>'' then
      CSS := ' '+CSS;
    if Target = '' then
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}
        Format('<a%s%s>',[CSS, StringToHTMLString(Extras, Options, GetRVStyle)])
    else begin
      Target := RV_MakeHTMLURLStr(Target);
      if not (rvoPercentEncodedURL in Self.Options) then
        Target := RV_EncodeURL(Target);
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}
        Format('<a%s href="%s"%s>',[CSS,
          StringToHTMLString(Target, Options, GetRVStyle),
          StringToHTMLString(Extras, Options, GetRVStyle)]);
    end;
    end
  else
    Result := '';
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveHTMLCheckpoint(Stream: TStream;
  Checkpoint: TRVCPInfo; var cpno: Integer; const Prefix: String;
  FromNewLine: Boolean; Options: TRVSaveOptions);
var CPId: TRVRawByteString;
begin
  if Checkpoint<>nil then begin
    if FromNewLine then
      RVWriteLn(Stream,'');
    if (rvsoUseCheckpointsNames in Options) and (Checkpoint.Name<>'') then
      CPId := StringToHTMLString(Checkpoint.Name, Options, GetRVStyle)
    else
      CPId := StringToHTMLString(Prefix+IntToStr(cpno), Options, GetRVStyle);
    RVWriteLn(Stream,'<a name="'+CPId+'"></a>');
    inc(cpno);
  end;
end;
{------------------------------------------------------------------------------}
// Returns true if the next item is in the same paragraph, is a text item,
// and its first character is a space character
function TCustomRVData.NextItemFromSpace(ItemNo: Integer): Boolean;
var s: TRVRawByteString;
begin
  inc(ItemNo);
  Result := False;
  if (ItemNo=ItemCount) or IsFromNewLine(ItemNo) or (GetItemStyle(ItemNo)<0) then
    exit;
  s := GetItemTextR(ItemNo);
  if s='' then
    exit;
  {$IFNDEF RVDONOTUSEUNICODE}
  if rvioUnicode in GetItemOptions(ItemNo) then
    Result := (s[1]=' ') and (s[2]=#0)
  else
  {$ENDIF}
    Result := s[1]=' ';

end;
{------------------------------------------------------------------------------}
{ Returns text string for saving to HTML. Path - path for saving HTML (pictures).
  ItemNo - index of text item to save.
  CSSVersion is True is called from SaveHTMLEx.
  Calls OnSaveItemToHTML, if assigned.
  If CSSVersion, special processing for "Symbol" font                          }
function TCustomRVData.GetTextForHTML(const Path: String; ItemNo: Integer;
  CSSVersion: Boolean; SaveOptions: TRVSaveOptions): TRVRawByteString;
var Item: TCustomRVItemInfo;
    FontInfo: TFontInfo;
    StyleNo: Integer;
begin
  Result := Items[ItemNo];
  if not SaveItemToFile(Path, Self, ItemNo, rvsfHTML,
    rvsoUTF8 in SaveOptions, Result) then begin
    if (Result='') and IsFromNewLine(ItemNo) and
      ((ItemNo+1=ItemCount) or IsParaStart(ItemNo+1)) then begin
        Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<br%s>', [RV_HTMLGetEndingSlash(SaveOptions)]);
      exit;
    end;
    Item := GetItem(ItemNo);
    StyleNo := GetActualStyle(Item);
    FontInfo := GetRVStyle.TextStyles[StyleNo];
    {$IFNDEF RVDONOTUSEUNICODE}
    if rvioUnicode in Item.ItemOptions then
      if CSSVersion and (AnsiCompareText(FontInfo.FontName, RVFONT_SYMBOL)=0) then
        Result := RV_MakeHTMLSymbolStrRaw(Result, rvsoUTF8 in SaveOptions, NextItemFromSpace(ItemNo))
      else if CSSVersion and (AnsiCompareText(FontInfo.FontName, RVFONT_WINGDINGS)=0) then
        Result := RV_MakeHTMLWingdingsStrRaw(Result, rvsoUTF8 in SaveOptions, NextItemFromSpace(ItemNo))
      else if rvsoUTF8 in SaveOptions then
        Result := RVU_GetHTMLUTF8EncodedUnicode(Result, rvteoHTMLCode in FontInfo.Options,
          NextItemFromSpace(ItemNo))
      else
        Result := RVU_GetHTMLEncodedUnicode(Result, rvteoHTMLCode in FontInfo.Options,
          NextItemFromSpace(ItemNo))
    else
    {$ENDIF}
      if CSSVersion and (AnsiCompareText(FontInfo.FontName, RVFONT_SYMBOL)=0) then
        Result := RV_MakeHTMLSymbolStrA(Result, rvsoUTF8 in SaveOptions, NextItemFromSpace(ItemNo))
      else if CSSVersion and (AnsiCompareText(FontInfo.FontName, RVFONT_WINGDINGS)=0) then
        Result := RV_MakeHTMLWingdingsStrA(Result, rvsoUTF8 in SaveOptions, NextItemFromSpace(ItemNo))
      else begin
        Result := RV_MakeHTMLStr(Result, rvteoHTMLCode in FontInfo.Options,
          NextItemFromSpace(ItemNo));
        if rvsoUTF8 in SaveOptions then
          Result := RVU_AnsiToUTF8(GetStyleCodePage(StyleNo), Result);
      end;
    end
  {$IFNDEF RVDONOTUSEUNICODE}
  else if rvsoUTF8 in SaveOptions then
    Result := UTF8Encode(RVU_RawUnicodeToWideString(Result));
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
procedure TCustomRVData.SaveHTMLNotes(const Path, ImagesPrefix, CPPrefix: String;
  Stream: TStream; CSSVersion: Boolean;
  Options: TRVSaveOptions; Color: TColor; var imgSaveNo: Integer; Bullets: TRVList;
  {$IFNDEF RVDONOTUSELISTS}ListBullets: TRVHTMLListCSSList;{$ENDIF}
  NoteClass: TCustomRVItemInfoClass; StyleNo: Integer);
var Note: TCustomRVNoteItemInfo;
    ColorStr: TRVAnsiString;
    SeqList: TRVSeqList;
const BreakColor = $C0C0C0;

   function DoGetNextNote(Note: TCustomRVNoteItemInfo): TCustomRVNoteItemInfo;
   begin
     Result := Note;
     repeat
       Result := GetNextNote(GetAbsoluteRootData, Result,
         TCustomRVNoteItemInfoClass(NoteClass), True);
     until (Result=nil) or (StyleNo=0) or (Result.StyleNo=StyleNo);
   end;

begin
  if (Self is TRVNoteData) or not (rvflRoot in Flags) then
    exit;
  SeqList := GetSeqList(False);
  Include(Options, rvsoMiddleOnly);
  Exclude(Options, rvsoFirstOnly);
  Exclude(Options, rvsoLastOnly);
  Note := DoGetNextNote(nil);
  if Note<>nil then begin
    if CSSVersion or (rvsoForceNonTextCSS in Options) then
      ColorStr := 'style= "color: '+RV_GetHTMLRGBStr(BreakColor, False)+'; border-width: 0px"'
    else
      ColorStr := 'color='+ RV_GetHTMLRGBStr(BreakColor, True);
    RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<hr %s %s width="30%%" align="left" size="2" %s>',
      [RV_HTMLGetNoValueAttribute('noshade', Options), ColorStr,
       RV_HTMLGetEndingSlash(Options)]));
  end;
  while Note<>nil do begin
    Note.GetIndexInList(SeqList);
    RVFWrite(Stream, '<a name="'+Note.GetHTMLAnchorName+'"></a>');
    if CSSVersion then
      Note.Document.SaveHTMLToStreamEx(Stream, Path, '', ImagesPrefix, '', '',
        CPPrefix, Options, Color, Color,  imgSaveNo,
        0, 0, 0, 0, nil, Bullets{$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF})
    else
      Note.Document.SaveHTMLToStream(Stream, Path, '', ImagesPrefix, Options, Color, imgSaveNo,
        0, 0, 0, 0, nil, Bullets);
    Note := DoGetNextNote(Note);
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.SaveHTMLToStream(Stream: TStream;
  const Path, Title, ImagesPrefix: String;
  Options: TRVSaveOptions; Color: TColor;
  var imgSaveNo: Integer; LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
  Background: TRVBackground; Bullets: TRVList): Boolean;
    {......................................................}
    procedure WriteExtraHTMLCode(Area: TRVHTMLSaveArea; AddSpace: Boolean;
      RVStyle: TRVStyle);
    var s: String;
        s2: TRVRawByteString;
    begin
      s := GetExtraHTMLCode(Area, False);
      if s<>'' then begin
        s2 := StringToHTMLString(s, Options, RVStyle);
        if AddSpace then
          RVWrite(Stream,' '+s2)
        else
          RVWrite(Stream, s2);
      end;
    end;
    {...........................................................}
    procedure SaveFirst(Stream: TStream; const Path: String; Title: String);
    var s: TRVRawByteString;
        s2: String;
    begin
      if rvsoXHTML in Options then begin
        RVWrite(Stream, '<?xml version="1.0"');
        {$IFDEF RICHVIEWCBDEF3}
        if rvsoUTF8 in Options then
          s := 'UTF-8'
        else
          s := RV_CharSet2HTMLLang(GetRVStyle.TextStyles[0].CharSet);
        if s<>'' then
          RVWrite(Stream,{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(' encoding="%s"',[s]));
        {$ENDIF}
        RVWriteLn(Stream, '?>');
      end;
      s := StringToHTMLString(Title, Options, GetRVStyle);
      RVWriteLn(Stream,'<html><head><title>'+s+'</title>');
      {$IFDEF RICHVIEWCBDEF3}
      if rvsoUTF8 in Options then
        s := 'UTF-8'
      else
        s := RV_CharSet2HTMLLang(GetRVStyle.TextStyles[0].CharSet);
      if s<>'' then
        RVWriteLn(Stream,{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<meta http-equiv="Content-Type" content="text/html; charset=%s"%s>',
          [s, RV_HTMLGetEndingSlash(Options)]));
      {$ENDIF}
      WriteExtraHTMLCode(rv_thms_Head, False, GetRVStyle);
      RVWriteLn(Stream,'</head>');
      RVWrite(Stream,'<body');
      if Color<>clNone then
         RVWrite(Stream,' bgcolor='+RV_GetHTMLRGBStr(Color, True));
      if (Background.Style<>bsNoBitmap) and
         (not Background.Bitmap.Empty) then begin
         s2 := SaveBackgroundToHTML(Background.Bitmap, Color, Path, ImagesPrefix,
           imgSaveNo, Options);
         if s2<>'' then begin
           s := StringToHTMLString(s2, Options, GetRVStyle);
           RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(' background="%s"', [s]));
           if (Background.Style<>bsTiledAndScrolled) then
             RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(' bgproperties=%s', [RV_HTMLGetStrAttrVal('fixed', Options)]));
         end;
      end;
      WriteExtraHTMLCode(rv_thms_BodyAttribute, True, GetRVStyle);
      RVWriteLn(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(' leftmargin=%s topmargin=%s rightmargin=%s bottommargin=%s>',
        [RV_HTMLGetIntAttrVal(LeftMargin,   Options),
         RV_HTMLGetIntAttrVal(TopMargin,    Options),
         RV_HTMLGetIntAttrVal(RightMargin,  Options),
         RV_HTMLGetIntAttrVal(BottomMargin, Options)]));
      WriteExtraHTMLCode(rv_thms_Body, False, GetRVStyle);
    end;
    {...........................................................}
    procedure SaveLast(Stream: TStream);
    begin
      WriteExtraHTMLCode(rv_thms_End, False, GetRVStyle);
      RVWriteLn(Stream,'</body></html>');
    end;
    {......................................................}
    function GetPageBreakCSS(item: TCustomRVItemInfo): TRVAnsiString;
    begin
      if (rvsoForceNonTextCSS in Options) and item.PageBreakBefore then 
        Result := ' style="page-break-before: always;"'
      else
        Result := '';
    end;
    {...........................................................}
    function GetBiDiModeAttr(BiDiMode: TRVBiDiMode): TRVAnsiString;
    begin
      case BiDiMode of
        rvbdLeftToRight:
          Result := 'LTR';
        rvbdRightToLeft:
          Result := 'RTL';
        else
          Result := '';
      end;
      if Result<>'' then
        Result := ' dir='+RV_HTMLGetStrAttrVal(Result,  Options);
    end;
    {...........................................................}
    function GetAlignAttr(Alignment: TRVAlignment): TRVAnsiString;
    begin
      case Alignment of
        rvaCenter:
          Result := 'center';
        rvaRight:
          Result := 'right';
        rvaJustify:
          Result := 'justify';
        else
          Result := '';
      end;
      if Result<>'' then
        Result := ' align='+RV_HTMLGetStrAttrVal(Result,  Options);
    end;
    {...........................................................}
    function GetBRClear(item: TCustomRVItemInfo): TRVAnsiString;
    begin
      if item.ClearLeft then
        if item.ClearRight then
          Result := 'all'
        else
          Result := 'left'
      else
        if item.ClearRight then
          Result := 'right'
        else begin
          Result := '';
          exit;
        end;
      Result := ' clear='+RV_HTMLGetStrAttrVal(Result,  Options);
    end;    
    {...........................................................}
    function GetOpenDIVTag(ParaStyle: TParaInfo; item: TCustomRVItemInfo;
      const OpenFont: TRVAnsiString): TRVAnsiString;
    var s: TRVAnsiString;
    begin
      Result := GetAlignAttr(ParaStyle.Alignment)+
        GetBiDiModeAttr(ParaStyle.BiDiMode)+
        GetPageBreakCSS(item);
      if ParaStyle.IsHeading(6) then
        s := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('h%d', [ParaStyle.OutlineLevel])
      else if RichViewSavePInHTML then
        s := 'p'
      else
        s := 'div';
      Result := '<'+s+Result+'>';
      if item.ClearLeft or item.ClearRight then
        Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<font size=1><br%s%s></font>%s',
          [GetBRClear(item), RV_HTMLGetEndingSlash(Options), Result]);
      if ParaStyle.IsHeading(6) then
        Result := Result+OpenFont;
    end;
    {...........................................................}
    {$IFNDEF RVDONOTUSELISTS}
    function GetOpenMarkerDIVTag(ParaStyle: TParaInfo; item: TCustomRVItemInfo): TRVAnsiString;
    begin
      Result := GetAlignAttr(ParaStyle.Alignment)+
        GetBiDiModeAttr(ParaStyle.BiDiMode)+
        GetPageBreakCSS(item);
      if Result='' then
        exit;
      Result := '<div'+Result+'>';
    end;
    {$ENDIF}
    {...........................................................}
    function GetCloseDIVTag(ParaStyle: TParaInfo;
      const CloseFont: TRVAnsiString): TRVAnsiString;
    begin
      if ParaStyle.IsHeading(6) then
        Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%s</h%d>',
          [CloseFont, ParaStyle.OutlineLevel])
      else if RichViewSavePInHTML then
        Result := '</p>'
      else
        Result := '</div>';
    end;
    {...........................................................}
    procedure SaveMiddle(Stream: TStream; const Path: String);
    var
      i: Integer;
      item: TCustomRVItemInfo;
      CloseDIV: Boolean;
      s2, s3, OpenFont, CloseFont: TRVRawByteString;
      HintTag2: TRVRawByteString;
      ATag: TRVAnsiString;
      HintTag: String;
      cpno: Integer;
      CreateBulletList: Boolean;
      RVStyle: TRVStyle;
      TextStyleNo: Integer;
      {$IFNDEF RVDONOTUSELISTS}
      marker: TRVMarkerItemInfo;
      CloseMarkerDiv: Boolean;
      {$ENDIF}
      CallProgress: Boolean;
      Progress: Integer;
    begin
      DoBeforeSaving;
      cpno := 0;
      {$IFNDEF RVDONOTUSELISTS}
      marker := nil;
      CloseMarkerDiv := False;
      {$ENDIF}
      RVStyle := GetRVStyle;
      CreateBulletList := Bullets=nil;
      if CreateBulletList then
        Bullets := TRVList.Create;
      CallProgress := CreateBulletList and SaveProgressInit(Progress, ItemCount,
        rvloHTMLWrite);
      try
        if not (rvsoDefault0Style in Options) then begin
          OpenFont := StringToHTMLString2(
            RV_HTMLOpenFontTag(RVStyle.TextStyles[0],
              RVStyle.TextStyles[0], False, Options),
            Options, CP_ACP);
          CloseFont := RV_HTMLCloseFontTag(RVStyle.TextStyles[0],
            RVStyle.TextStyles[0], False);
          RVWriteLn(Stream, OpenFont);
          end
        else begin
          OpenFont := '';
          CloseFont := '';
        end;
        CloseDIV := False;
        for i:=0 to Items.Count-1 do begin
          item := GetItem(i);
          if not item.SameAsPrev then begin
            if item.BR then
              RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<br%s%s>',
                [GetBRClear(item), RV_HTMLGetEndingSlash(Options)]))
            else begin
              {$IFNDEF RVDONOTUSELISTS}
              if marker<>nil then begin
                if CloseMarkerDiv then
                  RVWriteLn(Stream,'</li></div>')
                else
                  RVWriteLn(Stream,'</li>');
                RVWrite(Stream, GetParaHTMLCode2(Self, i, False, False, Options, RVStyle));
              end;
              {$ENDIF}
              if CloseDIV then begin
                RVWriteLn(Stream,GetCloseDIVTag(RVStyle.ParaStyles[GetItemPara(i-1)], CloseFont));
                RVWrite(Stream, GetParaHTMLCode2(Self, i, False, False, Options, RVStyle));
                CloseDIV := False;
              end;
            end;
            if not item.BR then
              case item.StyleNo of
                rvsBreak: ;
                {$IFNDEF RVDONOTUSELISTS}
                rvsListMarker:
                  begin
                    if (rvsoMarkersAsText in Options) or
                       (TRVMarkerItemInfo(item).GetLevelInfo(RVStyle)=nil) then begin
                      RVWrite(Stream, GetParaHTMLCode2(Self, i, True, False, Options, RVStyle));
                      RVWrite(Stream, GetOpenDIVTag(RVStyle.ParaStyles[item.ParaNo], item, OpenFont));
                      CloseDIV := True;
                      end
                    else begin
                      TRVMarkerItemInfo(item).SaveHTMLSpecial(Stream, marker,
                        RVStyle, False, Options{$IFNDEF RVDONOTUSELISTS},nil{$ENDIF});
                      RVWrite(Stream, GetParaHTMLCode2(Self, i, True, False, Options, RVStyle));
                      marker := TRVMarkerItemInfo(item);
                      s3 := GetOpenMarkerDIVTag(RVStyle.ParaStyles[marker.ParaNo], marker);
                      CloseMarkerDiv := s3<>'';
                      if CloseMarkerDiv then
                        RVWrite(Stream, s3);
                      if marker.GetLevelInfo(RVStyle).HasNumbering then
                        RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<li value=%s%s>',
                          [RV_HTMLGetIntAttrVal(marker.Counter, Options), GetPageBreakCSS(marker)]))
                      else
                        RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<li%s>',[GetPageBreakCSS(marker)]));
                    end;
                  end;
                {$ENDIF}
                else
                  begin
                    {$IFNDEF RVDONOTUSELISTS}
                    if marker<>nil then begin
                      marker.HTMLOpenOrCloseTags(Stream, marker.Level, -1, RVStyle,
                        Options, False{$IFNDEF RVDONOTUSELISTS},nil{$ENDIF});
                      marker := nil;
                    end;
                    {$ENDIF}
                    RVWrite(Stream, GetParaHTMLCode2(Self, i, True, False, Options, RVStyle));
                    RVWrite(Stream, GetOpenDIVTag(RVStyle.ParaStyles[item.ParaNo], item, OpenFont));
                    CloseDIV := True;
                  end;
              end;
          end;
          SaveHTMLCheckpoint(Stream, item.Checkpoint, cpno, RVDEFAULTCHECKPOINTPREFIX, True, Options);
          ATag := '';
          HintTag := '';
          HintTag2 := '';
          if item.GetBoolValueEx(rvbpJump, RVStyle) then
            ATag := GetHTMLATag(i, '', Options);
          if ATag<>'' then
            RVWrite(Stream, ATag)
          else begin
            item.GetExtraStrProperty(rvespHint, HintTag);
            if HintTag<>'' then begin
              HintTag := RV_GetHintStr(rvsfHTML, HintTag);
              HintTag2 := StringToHTMLString(HintTag, Options, RVStyle)
            end;
          end;
          if (item.StyleNo<0) and (item.AssociatedTextStyleNo<0) then begin // non-text
            s2 := '';
            if SaveItemToFile(Path, Self, i, rvsfHTML, rvsoUTF8 in Options, s2) then begin
              {$IFNDEF RVDONOTUSEUNICODE}
              if rvsoUTF8 in Options then
                s2 := UTF8Encode(RVU_RawUnicodeToWideString(s2));
              {$ENDIF}
              RVWrite(Stream, s2)
              end
            else begin
              item.SaveToHTML(Stream, Self, i, Items[i], Path, ImagesPrefix,
                imgSaveNo, Color, Options, False, Bullets
                {$IFNDEF RVDONOTUSELISTS},nil{$ENDIF});
              if item.StyleNo=rvsBreak then
                RVWriteLn(Stream,'');
            end;
            end
          else begin
            TextStyleNo := GetActualTextStyle(item);
            if ShouldSaveTextToHTML(TextStyleNo) then begin // text or tab
              if HintTag2<>'' then
                RVWrite(Stream, '<span '+HintTag2+'>');
              if TextStyleNo<>0 then begin
                s3 := StringToHTMLString2(
                  RV_HTMLOpenFontTag(RVStyle.TextStyles[TextStyleNo],
                    RVStyle.TextStyles[0], True, Options),
                  Options, CP_ACP);
                RVWrite(Stream, s3);
              end;
              if item.StyleNo>=0 then begin
                s2 := GetTextForHTML(Path, i, False, Options);
                RVWrite(Stream, s2);
                end
              else begin
                s2 := '';
                if SaveItemToFile(Path, Self, i, rvsfHTML, rvsoUTF8 in Options, s2) then begin
                  {$IFNDEF RVDONOTUSEUNICODE}
                  if rvsoUTF8 in Options then
                    s2 := UTF8Encode(RVU_RawUnicodeToWideString(s2));
                  {$ENDIF}
                  RVWrite(Stream, s2)
                  end
                 else
                   item.SaveToHTML(Stream, Self, i, Items[i], Path, ImagesPrefix,
                     imgSaveNo, Color, Options, False, Bullets{$IFNDEF RVDONOTUSELISTS},nil{$ENDIF});
              end;
              if TextStyleNo<>0 then
                RVWrite(Stream,
                  RV_HTMLCloseFontTag(RVStyle.TextStyles[TextStyleNo],
                    RVStyle.TextStyles[0], True));
              if HintTag<>'' then
                RVWrite(Stream, '</span>');
            end;
          end;
          if ATag<>'' then
            RVWrite(Stream,'</a>');
          if CallProgress then
            SaveProgressRun(Progress, i+1, ItemCount, rvloHTMLWrite);
        end;
        {$IFNDEF RVDONOTUSELISTS}
        if marker<>nil then begin
          if CloseMarkerDiv then
            RVWriteLn(Stream,'</li></div>')
          else
            RVWriteLn(Stream,'</li>');
          RVWrite(Stream, GetParaHTMLCode2(Self, ItemCount, False, False, Options, RVStyle));
          marker.HTMLOpenOrCloseTags(Stream, marker.Level, -1, RVStyle, Options,
            False{$IFNDEF RVDONOTUSELISTS},nil{$ENDIF});
        end;
        {$ENDIF}
        if CloseDIV then begin
          RVWriteLn(Stream,GetCloseDIVTag(RVStyle.ParaStyles[GetItemPara(ItemCount-1)], CloseFont));
          RVWrite(Stream, GetParaHTMLCode2(Self, ItemCount, False, False, Options, RVStyle));
        end;
        if not (rvsoDefault0Style in Options) then
          RVWriteLn(Stream, CloseFont);
        SaveHTMLCheckpoint(Stream, NotAddedCP, cpno, RVDEFAULTCHECKPOINTPREFIX, False, Options);
        {$IFNDEF RVDONOTUSESEQ}
        SaveHTMLNotes(Path, ImagesPrefix, '', Stream, False, Options, Color,
          imgSaveNo, Bullets, {$IFNDEF RVDONOTUSELISTS}nil,{$ENDIF}TRVFootnoteItemInfo, 0);
        SaveHTMLNotes(Path, ImagesPrefix, '', Stream, False, Options, Color,
          imgSaveNo, Bullets, {$IFNDEF RVDONOTUSELISTS}nil,{$ENDIF}TRVEndnoteItemInfo, 0);
        SaveHTMLNotes(Path, ImagesPrefix, '', Stream, False, Options, Color,
          imgSaveNo, Bullets, {$IFNDEF RVDONOTUSELISTS}nil,{$ENDIF}TRVSidenoteItemInfo, rvsSidenote);
        {$ENDIF}
      finally
        if CreateBulletList then begin
          Bullets.Free;
          if CallProgress then
            SaveProgressDone(rvloHTMLWrite);
        end;
      end;
    end;
    {...........................................................}
begin
  Result := False;
  if GetRVStyle = nil then exit;
  Result := True;
  try
    if not (rvsoMiddleOnly in Options) and
       not (rvsoLastOnly in Options) then
       SaveFirst(Stream, Path, Title);
    if not (rvsoFirstOnly in Options) and
       not (rvsoLastOnly in Options) then
       SaveMiddle(Stream, Path);
    if not (rvsoFirstOnly in Options) and
       not (rvsoMiddleOnly in Options) then
       SaveLast(Stream);
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveBackgroundToHTML(bmp: TBitmap; Color: TColor;
  const Path, ImagesPrefix: String; var imgSaveNo: Integer;
  SaveOptions: TRVSaveOptions): String;
var DoDefault: Boolean;
begin
  Result := '';
  HTMLSaveImage(Self, -1, Path, Color, Result, DoDefault);
  if DoDefault then
    Result := RV_GetHTMLPath(DoSavePicture(rvsfHTML, Self, -1, ImagesPrefix, Path,
      imgSaveNo, rvsoOverrideImages in SaveOptions, Color, bmp));
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveHTMLToStreamEx(Stream: TStream;
  const Path, Title, ImagesPrefix, ExtraStyles, ExternalCSS, CPPrefix: String;
  Options: TRVSaveOptions; Color: TColor; var CurrentFileColor: TColor;
  var imgSaveNo: Integer; LeftMargin, TopMargin, RightMargin, BottomMargin: Integer;
  Background: TRVBackground; Bullets: TRVList
  {$IFNDEF RVDONOTUSELISTS};ListBullets: TRVHTMLListCSSList{$ENDIF}): Boolean;
{$IFNDEF RVDONOTUSELISTS}
var CreateListBulletList: Boolean;
{$ENDIF}
    {......................................................}
    procedure WriteExtraHTMLCode(Area: TRVHTMLSaveArea; AddSpace: Boolean;
      RVStyle: TRVStyle);
    var s: String;
        s2: TRVRawByteString;
    begin
      s := GetExtraHTMLCode(Area, True);
      if s<>'' then begin
        s2 := StringToHTMLString(s, Options, RVStyle);
        if AddSpace then
          RVWrite(Stream,' '+s2)
        else
          RVWrite(Stream, s2);
      end;
    end;
    {......................................................}
    function GetBackgroundVPos(BStyle: TBackgroundStyle): TRVAnsiString;
    begin
      case BStyle of
        bsTopLeft, bsTopRight:
          Result := 'top';
        bsBottomLeft, bsBottomRight:
          Result := 'bottom';
        else
          Result := 'center';
      end;
    end;
    {......................................................}
    function GetBackgroundHPos(BStyle: TBackgroundStyle): TRVAnsiString;
    begin
      case BStyle of
        bsTopLeft, bsBottomLeft:
          Result := 'left';
        bsTopRight, bsBottomRight:
          Result := 'right';
        else
          Result := 'center';
      end;
    end;
    {......................................................}
    function GetBodyCSS(const Delim, Quote: TRVAnsiString): TRVRawByteString;
    var s: TRVRawByteString;
        s2: String;
    begin
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        'margin: %dpx %dpx %dpx %dpx;',
        [TopMargin, RightMargin, BottomMargin, LeftMargin]);
      s := RV_GetHTMLRGBStr(Color, False);
      if s<>'' then
        Result := Result+Delim+'background-color: '+s+';';
      if (Background.Style<>bsNoBitmap) and
         (not Background.Bitmap.Empty) then begin
         s2 := SaveBackgroundToHTML(Background.Bitmap, Color, Path, ImagesPrefix,
           imgSaveNo, Options);
         if s2<>'' then begin
           s := StringToHTMLString(s2, Options, GetRVStyle);
           Result := Result + Delim+
             {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
               'background-image: url(%s%s%s);', [Quote,s,Quote])+
             Delim + 'background-repeat: ' + TRVAnsiString(rv_cssBkRepeat[Background.Style])+';'+
             Delim + 'background-attachment: ' + TRVAnsiString(rv_cssBkAttachment[Background.Style])+';';
           case Background.Style of
             bsStretched:
               Result := Result + Delim + 'background-size: 100% 100%;';
             bsCentered, bsTopLeft, bsTopRight, bsBottomLeft, bsBottomRight:
               Result := Result + Delim + 'background-position: ' +
                 GetBackgroundVPos(Background.Style)+
                 ' '+GetBackgroundHPos(Background.Style)+';';
           end;
         end;
      end;
    end;
    {......................................................}

    procedure SaveFirst(Stream: TStream; const Path, Title, ExtraStyles,
      ExternalCSS: String);
    var s: TRVRawByteString;
        CSSOptions: TRVSaveCSSOptions;
    begin
      if rvsoXHTML in Options then begin
        RVWrite(Stream, '<?xml version="1.0"');
        {$IFDEF RICHVIEWCBDEF3}
        if rvsoUTF8 in Options then
          s := 'UTF-8'
        else
          s := RV_CharSet2HTMLLang(GetRVStyle.TextStyles[0].CharSet);
        if s<>'' then
          RVWrite(Stream,{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(' encoding="%s"',[s]));
        {$ENDIF}
        RVWriteLn(Stream, '?>');
        RVWriteLn(Stream, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">');
        RVWriteLn(Stream, '<html xmlns="http://www.w3.org/1999/xhtml">');
        end
      else begin
        if rvsoMarkersAsText in Options then
          RVWriteLn(Stream,'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">')
        else
          RVWriteLn(Stream,'<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">');
        RVWrite(Stream, '<html>');
      end;
      s := StringToHTMLString(Title, Options, GetRVStyle);
      RVWriteLn(Stream,'<head><title>'+s+'</title>');
      {$IFDEF RICHVIEWCBDEF3}
      if rvsoUTF8 in Options then
        s := 'UTF-8'
      else
        s := RV_CharSet2HTMLLang(GetRVStyle.TextStyles[0].CharSet);
      if s<>'' then
        RVWriteLn(Stream,
          {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<meta http-equiv="Content-Type" content="text/html; charset=%s"%s>',
            [s, RV_HTMLGetEndingSlash(Options)]));
      {$ENDIF}
      RVWriteLn(Stream,
        {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<meta http-equiv="Content-Style-Type" content="text/css"%s>',
          [RV_HTMLGetEndingSlash(Options)]));
      if not (rvsoInlineCSS in Options) or (ExtraStyles<>'') then begin
        RVWrite(Stream, '<style type="text/css">');
        if rvsoXHTML in Options then
          RVWriteLn(Stream,'')
        else
          RVWriteLn(Stream,'<!--');
        if not (rvsoInlineCSS in Options) then begin
          RVWriteLn(Stream, 'body {');
          RVWriteLn(Stream, '  '+GetBodyCSS(#13#10'  ', '"'));
          RVWriteLn(Stream, '}');
          if (ExternalCSS='') then begin
            CSSOptions := [];
            if (rvsoNoDefCSSStyle in Options) then
              Include(CSSOptions, rvcssNoDefCSSStyle);
            if (rvsoUTF8 in Options) then
              Include(CSSOptions, rvcssUTF8);
            if (rvsoDefault0Style in Options) then
              Include(CSSOptions, rvcssDefault0Style);
            if (rvsoMarkersAsText in Options) then
              Include(CSSOptions, rvcssMarkersAsText);
            {$IFNDEF RVDONOTUSELISTS}
            CreateListBulletList := (ListBullets=nil);
            if CreateListBulletList then begin
              ListBullets := TRVHTMLListCSSList.Create;
              GetRVStyle.ListStyles.SaveCSSToStream(nil, ListBullets, CSSOptions, '');
            end;
            {$ENDIF}
            GetRVStyle.SaveCSSToStream(Stream, CSSOptions);
          end;
        end;
        if ExtraStyles<>'' then
          RVWriteLn(Stream, StringToHTMLString(ExtraStyles, Options, GetRVStyle));
        if not (rvsoXHTML in Options) then
          RVWrite(Stream,'-->');
        RVWriteLn(Stream,'</style>');
      end;
      if (ExternalCSS<>'') and not (rvsoInlineCSS in Options) then
        RVWriteLn(Stream, '<link type="text/css" href="'+
          StringToHTMLString(ExternalCSS, Options, GetRVStyle)+
          '" rel="stylesheet"'+RV_HTMLGetEndingSlash(Options)+'>');
      WriteExtraHTMLCode(rv_thms_Head, False, GetRVStyle);
      RVWriteLn(Stream,'</head>');
      RVWrite(Stream,'<body');
      if rvsoInlineCSS in Options then
        RVWrite(Stream,' style="'+GetBodyCSS(' ', '''')+'"');
      WriteExtraHTMLCode(rv_thms_BodyAttribute, True, GetRVStyle);
      RVWriteLn(Stream,'>');
      WriteExtraHTMLCode(rv_thms_Body, False, GetRVStyle);
    end;
    {......................................................}
    procedure SaveLast(Stream: TStream);
    begin
      WriteExtraHTMLCode(rv_thms_End, False, GetRVStyle);
      RVWriteLn(Stream,'</body></html>');
    end;
    {......................................................}
    function GetTextCSS(TextStyleNo: Integer; RVStyle: TRVStyle): TRVRawByteString;
    var MemoryStream: TStream;
    begin
      if (rvsoInlineCSS in Options) then begin
        MemoryStream := TMemoryStream.Create;
        try
          RVStyle.TextStyles[TextStyleNo].SaveCSSToStream(MemoryStream,
            nil, False, rvsoUTF8 in Options, RVStyle.TextStyles[TextStyleNo].Jump);
          SetLength(Result, MemoryStream.Size);
          MemoryStream.Position := 0;
          MemoryStream.ReadBuffer(PRVAnsiChar(Result)^, Length(Result));
        finally
          MemoryStream.Free;
        end;
        Result := 'style="'+Result+'"';
        end
      else
        Result := 'class='+RV_HTMLGetStrAttrVal('rvts'+RVIntToStr(TextStyleNo), Options);
    end;
    {......................................................}
    function GetClearFloatingCSSValue(item: TCustomRVItemInfo;
      OnlyValue, SpaceBefore: Boolean): TRVAnsiString;
    begin
      if item.ClearLeft then
        if item.ClearRight then
          Result := 'clear: both;'
        else
          Result := 'clear: left;'
      else
        if item.ClearRight then
          Result := 'clear: right;'
        else
          Result := '';
      if (Result<>'') and not OnlyValue then
          Result := 'style="'+Result+'"';
      if (Result<>'') and SpaceBefore then
        Result := ' '+Result;
    end;
    {......................................................}
    function GetPageBreakAndClearFloatingCSS(item: TCustomRVItemInfo;
      OnlyValue, SpaceBefore: Boolean): TRVAnsiString;
    begin
      if item.PageBreakBefore then
        Result := 'page-break-before: always;'
      else
        Result := '';
      if item.ClearLeft then
        RV_AddStrA(Result, GetClearFloatingCSSValue(item, True, False));
      if (Result<>'') and not OnlyValue then
          Result := 'style="'+Result+'"';
      if (Result<>'') and SpaceBefore then
        Result := ' '+Result;
    end;
    {......................................................}
    function GetParaCSSValue(item: TCustomRVItemInfo; RVStyle: TRVStyle;
      IgnoreLeftIndents: Boolean): TRVAnsiString;
    var MemoryStream: TStream;
    begin
      if (rvsoInlineCSS in Options) then begin
        MemoryStream := TMemoryStream.Create;
        try
          RVStyle.ParaStyles[item.ParaNo].SaveCSSToStream(MemoryStream, nil,
            False, False, IgnoreLeftIndents);
          SetLength(Result, MemoryStream.Size);
          MemoryStream.Position := 0;
          MemoryStream.ReadBuffer(PRVAnsiChar(Result)^, Length(Result));
        finally
          MemoryStream.Free;
        end;
        end
      else
        Result := '';
      Result := Result+GetPageBreakAndClearFloatingCSS(item, True, Result<>'')
    end;
    {......................................................}
    function GetParaCSS(item: TCustomRVItemInfo; RVStyle: TRVStyle;
      IgnoreLeftIndents: Boolean): TRVAnsiString;
    begin
      if (rvsoInlineCSS in Options) then
        Result := 'style="'+GetParaCSSValue(item, RVStyle, IgnoreLeftIndents)+'"'
      else if (Item.ParaNo>0) or (rvsoNoDefCSSStyle in Options) then
        Result := 'class='+RV_HTMLGetStrAttrVal('rvps'+RVIntToStr(item.ParaNo), Options)+
         GetPageBreakAndClearFloatingCSS(item, False, True)
      else
        Result := GetPageBreakAndClearFloatingCSS(item, False, False);
    end;
    {......................................................}
    function GetItemTextStyleNo(ItemNo: Integer): Integer;
    begin
      Result := GetActualTextStyle(GetItem(ItemNo));
      if Result<0 then
        Result := GetItem(ItemNo).StyleNo;
    end;
    {......................................................}
    function GetParagraphTag(ParaNo: Integer): TRVAnsiString;
    begin
      if GetRVStyle.ParaStyles[ParaNo].IsHeading(6) then
        Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('h%d',
          [GetRVStyle.ParaStyles[ParaNo].OutlineLevel])
      else if RichViewSaveDivInHTMLEx then
        Result := 'div'
      else
        Result := 'p';
    end;
    {......................................................}
    procedure SaveMiddle(Stream: TStream; const Path: String; CPPrefix: String);
    var i: Integer;
        item: TCustomRVItemInfo;
        s: TRVRawByteString;
        HintAttr: String;
        ATag: TRVAnsiString;
        cpno, OpenedPara, TextStyleNo: Integer;
        CreateBulletList, Use0StyleAsDef, DIVOpened: Boolean;
        RVStyle: TRVStyle;
        {$IFNDEF RVDONOTUSELISTS}
        s2: String;
        marker: TRVMarkerItemInfo;
        {$ENDIF}
        CallProgress: Boolean;
        Progress: Integer;
    begin
      DoBeforeSaving;
      if CPPrefix='' then
        CPPrefix := RVDEFAULTCHECKPOINTPREFIX;
      RVStyle := GetRVStyle;
      cpno    := 0;
      OpenedPara := -1;
      DIVOpened := False;
      {$IFNDEF RVDONOTUSELISTS}
      marker := nil;
      {$ENDIF}
      CreateBulletList := Bullets=nil;
      if CreateBulletList then
        Bullets := TRVList.Create;
      CallProgress := CreateBulletList and SaveProgressInit(Progress, ItemCount,
        rvloHTMLWrite);
      try
        Use0StyleAsDef := (RVStyle.TextStyles.Count>=1) and
          not RVStyle.TextStyles[0].Jump and
          (RVStyle.TextStyles[0].BackColor=clNone) and
          not (rvsoNoDefCSSStyle in Options);
        for i:=0 to Items.Count-1 do begin
          item := GetItem(i);
          if (not item.SameAsPrev) then begin
            if (i>0) and ((OpenedPara<0) {$IFNDEF RVDONOTUSELISTS}and (marker=nil){$ENDIF}) {or
               item.BR} then
              RVWriteLn(Stream,'');
            if item.BR then
              RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<br%s%s>',
                [GetClearFloatingCSSValue(item, False, True), RV_HTMLGetEndingSlash(Options)]))
            else begin
              {$IFNDEF RVDONOTUSELISTS}
              if marker<>nil then begin
                RVWriteLn(Stream,'</li>');
                RVWrite(Stream, GetParaHTMLCode2(Self, i, False, True, Options, RVStyle));
                end
              else if (OpenedPara>=0) then
              {$ENDIF}
              begin
                if DIVOpened then
                  RVWriteLn(Stream,'</div>')
                else
                  RVWriteLn(Stream,
                    {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('</%s>',
                     [GetParagraphTag(OpenedPara)]));
                DIVOpened := False;
                RVWrite(Stream, GetParaHTMLCode2(Self, i, False, True, Options, RVStyle));
              end;
              CurrentFileColor := RVStyle.ParaStyles[item.ParaNo].Background.Color;
              if CurrentFileColor=clNone then
                CurrentFileColor := Color;
              case item.StyleNo of
                rvsBreak:
                  OpenedPara := -1;
                {$IFNDEF RVDONOTUSELISTS}
                rvsListMarker:
                  begin
                    if TRVMarkerItemInfo(item).GetLevelInfo(RVStyle)<>nil then begin
                      if rvsoMarkersAsText in Options then begin
                        RVWrite(Stream, GetParaHTMLCode2(Self, i, True, True, Options, RVStyle));
                        if ((item.ParaNo=0) and not (rvsoNoDefCSSStyle in Options)) or
                           (rvsoInlineCSS in Options) then
                          RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<%s style="%s %s">',
                            [GetParagraphTag(item.ParaNo), GetParaCSSValue(item, RVStyle, True),
                             TRVMarkerItemInfo(item).GetLevelInfo(RVStyle).GetIndentCSSForTextVersion(RVStyle)]))
                        else
                          RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<%s %s style="%s">',
                            [GetParagraphTag(item.ParaNo), GetParaCSS(item, RVStyle, True),
                             TRVMarkerItemInfo(item).GetLevelInfo(RVStyle).GetIndentCSSForTextVersion(RVStyle)]));
                        OpenedPara := item.ParaNo;
                        end
                      else begin
                        TRVMarkerItemInfo(item).SaveHTMLSpecial(Stream, marker,
                          RVStyle, True, Options{$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF});
                        RVWrite(Stream, GetParaHTMLCode2(Self, i, True, True, Options, RVStyle));
                        marker := TRVMarkerItemInfo(item);
                        if (marker.GetLevelInfo(RVStyle).HasNumbering) and
                          not (rvsoXHTML in Options) then
                          RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}
                            Format('<li value=%s',[RV_HTMLGetIntAttrVal(marker.Counter, Options)]))
                        else
                          RVWrite(Stream,'<li');
                        s := GetParaCSS(item, RVStyle, True);
                        if s<>'' then
                          RVWrite(Stream, ' '+s);
                        s2 := marker.GetLICSS(Self, i, Path, ImagesPrefix,
                          imgSaveNo, CurrentFileColor, Options, Bullets);
                        RVWrite(Stream, StringToHTMLString(s2, Options, RVStyle));
                        RVWrite(Stream,'>');
                        OpenedPara := -1;
                      end
                      end
                    else begin
                      {$IFNDEF RVDONOTUSELISTS}
                      if marker<>nil then begin
                        marker.HTMLOpenOrCloseTags(Stream, marker.Level, -1, RVStyle,
                          Options, True{$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF});
                        marker := nil;
                      end;
                      {$ENDIF}
                      RVWrite(Stream, GetParaHTMLCode2(Self, i, True, True, Options, RVStyle));
                      s := GetParaCSS(item, RVStyle, False);
                      if s<>'' then
                        s := ' '+s;
                      RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<%s%s>',
                        [GetParagraphTag(item.ParaNo), s]));
                      OpenedPara := item.ParaNo;
                    end;
                  end;
                {$ENDIF}
                else
                  begin
                    {$IFNDEF RVDONOTUSELISTS}
                    if marker<>nil then begin
                      marker.HTMLOpenOrCloseTags(Stream, marker.Level, -1, RVStyle,
                        Options, True{$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF});
                      marker := nil;
                    end;
                    {$ENDIF}
                    RVWrite(Stream, GetParaHTMLCode2(Self, i, True, True, Options, RVStyle));
                    s := GetParaCSS(item, RVStyle, False);
                    if s<>'' then
                      s := ' '+s;
                    if item.GetBoolValue(rvbpNoHTML_P) then begin
                      RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<div%s>',[s]));
                      DIVOpened := True;
                      end
                    else
                      RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<%s%s>',
                        [GetParagraphTag(item.ParaNo), s]));
                    OpenedPara := item.ParaNo;
                  end;
              end
            end;
          end;
          SaveHTMLCheckpoint(Stream, item.Checkpoint, cpno, CPPrefix, False, Options);
          if (item.StyleNo<0) and (item.AssociatedTextStyleNo<0) then begin
            ATag := '';
            if item.GetBoolValueEx(rvbpJump, RVStyle) then begin
              ATag := GetHTMLATag(i, '', Options);
              if ATag<>'' then
                RVWrite(Stream, ATag);
            end;
            s := '';
            if SaveItemToFile(Path, Self, i, rvsfHTML, rvsoUTF8 in Options, s) then begin
              {$IFNDEF RVDONOTUSEUNICODE}
              if rvsoUTF8 in Options then
                s := UTF8Encode(RVU_RawUnicodeToWideString(s));
              {$ENDIF}
              RVWrite(Stream, s);
              end
            else
              item.SaveToHTML(Stream, Self, i, Items[i], Path, ImagesPrefix,
                imgSaveNo, CurrentFileColor, Options, True, Bullets
                {$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF});
            if item.GetBoolValueEx(rvbpJump, RVStyle) then begin
              if ATag<>'' then
                RVWrite(Stream, '</a>');
            end;
            end
          else begin
            if RVStyle.ParaStyles[item.ParaNo].IsHeading(6) then
              RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<span %s>',
                [GetTextCSS(0, RVStyle)]));
            TextStyleNo := GetItemTextStyleNo(i);
            if ShouldSaveTextToHTML(TextStyleNo) then begin
              ATag := '';
              if item.GetBoolValueEx(rvbpJump, RVStyle) then
                ATag := GetHTMLATag(i, GetTextCSS(TextStyleNo, RVStyle), Options);
              if ATag<>'' then
                RVWrite(Stream, ATag)
              else begin
                if (TextStyleNo<>0) or not Use0StyleAsDef or (rvsoInlineCSS in Options) then
                  RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<span %s>',
                    [GetTextCSS(TextStyleNo, RVStyle)]));
              end;
              HintAttr := '';
              if ATag='' then begin
                item.GetExtraStrProperty(rvespHint, HintAttr);
                HintAttr := RV_GetHintStr(rvsfHTML, HintAttr);
                if HintAttr<>'' then
                  RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<span %s>',
                    [StringToHTMLString(HintAttr, Options, RVStyle)]));
              end;
              if item.StyleNo>=0 then begin
                s := GetTextForHTML(Path, i, True, Options);
                RVWrite(Stream, s);
                end
              else begin
                s := '';
                if SaveItemToFile(Path, Self, i, rvsfHTML, rvsoUTF8 in Options, s) then begin
                  {$IFNDEF RVDONOTUSEUNICODE}
                  if rvsoUTF8 in Options then
                    s := UTF8Encode(RVU_RawUnicodeToWideString(s));
                  {$ENDIF}
                  RVWrite(Stream, s);
                  end
                else
                  item.SaveToHTML(Stream, Self, i, Items[i], Path, ImagesPrefix,
                    imgSaveNo, CurrentFileColor, Options, True, Bullets
                    {$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF});
              end;
              if HintAttr<>'' then
                RVWrite(Stream,'</span>');
              if ATag<>'' then
                RVWrite(Stream,'</a>')
              else if (TextStyleNo<>0) or not Use0StyleAsDef or (rvsoInlineCSS in Options) then
                RVWrite(Stream, '</span>');
              if RVStyle.ParaStyles[item.ParaNo].IsHeading(6) then
                RVWrite(Stream, '</span>');
            end;
          end;
          if CallProgress then
            SaveProgressRun(Progress, i+1, ItemCount, rvloHTMLWrite);
        end;
        {$IFNDEF RVDONOTUSELISTS}
        if marker<>nil then begin
          RVWriteLn(Stream,'</li>');
          RVWrite(Stream, GetParaHTMLCode2(Self, ItemCount, False, True, Options, RVStyle));
          marker.HTMLOpenOrCloseTags(Stream, marker.Level, -1, RVStyle, Options,
            True{$IFNDEF RVDONOTUSELISTS},ListBullets{$ENDIF});
        end;
        {$ENDIF}
        if (OpenedPara<>-1) then begin
          if DIVOpened then
            RVWriteLn(Stream,'</div>')
          else
            RVWriteLn(Stream,
              {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('</%s>',
                [GetParagraphTag(OpenedPara)]));
          RVWrite(Stream, GetParaHTMLCode2(Self, ItemCount, False, True, Options, RVStyle));
        end;
        SaveHTMLCheckpoint(Stream, NotAddedCP, cpno, CPPrefix, False, Options);
        {$IFNDEF RVDONOTUSESEQ}
        SaveHTMLNotes(Path, ImagesPrefix, '', Stream, True, Options, Color,
          imgSaveNo, Bullets, {$IFNDEF RVDONOTUSELISTS}ListBullets,{$ENDIF}TRVFootnoteItemInfo, 0);
        SaveHTMLNotes(Path, ImagesPrefix, '', Stream, True, Options, Color,
          imgSaveNo, Bullets, {$IFNDEF RVDONOTUSELISTS}ListBullets,{$ENDIF}TRVEndNoteItemInfo, 0);
        SaveHTMLNotes(Path, ImagesPrefix, '', Stream, True, Options, Color,
          imgSaveNo, Bullets, {$IFNDEF RVDONOTUSELISTS}ListBullets,{$ENDIF}TRVSidenoteItemInfo, rvsSidenote);
        {$ENDIF}
      finally
        {$IFNDEF RVDONOTUSELISTS}
        if CreateListBulletList then
          ListBullets.Free;
        {$ENDIF}
        if CreateBulletList then begin
          Bullets.Free;
          if CallProgress then
            SaveProgressDone(rvloHTMLWrite);
        end;
      end;
    end;
    {......................................................}
begin
  Result := False;
  if GetRVStyle = nil then exit;
  Result := True;
  CurrentFileColor := Color;
  {$IFNDEF RVDONOTUSELISTS}
  CreateListBulletList := False;
  {$ENDIF}
  try
    if not (rvsoMiddleOnly in Options) and
       not (rvsoLastOnly in Options) then
       SaveFirst(Stream, Path, Title, ExtraStyles, ExternalCSS);
    if not (rvsoFirstOnly in Options) and
       not (rvsoLastOnly in Options) then
       SaveMiddle(Stream, Path, CPPrefix);
    if not (rvsoFirstOnly in Options) and
       not (rvsoMiddleOnly in Options) then
       SaveLast(Stream);
  except
    Result := False;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.CheckItemClass(ItemNo: Integer; RequiredClass: TCustomRVItemInfoClass);
begin
  if not (GetItem(ItemNo) is RequiredClass) then
    raise ERichViewError.Create(errRVTypesMismatch);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.FindControlItemNo(actrl: TControl): Integer;
var i: Integer;
begin
  for i := 0 to Items.Count-1 do
    if GetItem(i).OwnsControl(actrl) then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.RemoveCheckpoint(ItemNo: Integer): Boolean;
var OldCP: TRVCPInfo;
begin
  with GetItem(ItemNo) do begin
    OldCP := Checkpoint;
    FreeCheckpoint(Checkpoint, True, True);
  end;
  Result := OldCP<>nil;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetGrouped(ItemNo: Integer; Grouped: Boolean);
begin
  with GetItem(ItemNo) do
  if Grouped then
    Include(ItemOptions, rvioGroupWithNext)
  else
    Exclude(ItemOptions, rvioGroupWithNext)
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetBreakInfo(ItemNo: Integer; AWidth: TRVStyleLength;
  AStyle: TRVBreakStyle; AColor: TColor; const ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVBreakItemInfo);
  with TRVBreakItemInfo(GetItem(ItemNo)) do begin
    Color     := AColor;
    LineWidth := AWidth;
    Style     := AStyle;
  end;
  SetItemTag(ItemNo, ATag);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetBulletInfo(ItemNo: Integer; const AName: TRVAnsiString;
  AImageIndex: Integer; AImageList: TCustomImageList; const ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVBulletItemInfo);
  with TRVBulletItemInfo(GetItem(ItemNo)) do
    ImageIndex := AImageIndex;
  SetItemTag(ItemNo, ATag);
  Items[ItemNo] := AName;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SetControlInfo(ItemNo: Integer; const AName: TRVAnsiString;
  AVAlign: TRVVAlign; const ATag: TRVTag): Boolean;
begin
  CheckItemClass(ItemNo, TRVControlItemInfo);
  with TRVControlItemInfo(GetItem(ItemNo))  do begin
    Result := (VAlign<>AVAlign);
    VAlign := AVAlign;
  end;
  SetItemTag(ItemNo, ATag);
  Items[ItemNo] := AName;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetHotspotInfo(ItemNo: Integer;
  const AName: TRVAnsiString; AImageIndex, AHotImageIndex: Integer;
  AImageList: TCustomImageList; const ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVHotspotItemInfo);
  with TRVHotspotItemInfo(GetItem(ItemNo)) do begin
    ImageIndex    := AImageIndex;
    HotImageIndex := AHotImageIndex;
  end;
  SetItemTag(ItemNo, ATag);
  Items[ItemNo] := AName;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetCheckpointInfo(ItemNo: Integer; const ATag: TRVTag;
  const AName: String; ARaiseEvent: Boolean);
begin
  with GetItem(ItemNo) do begin
    if Checkpoint=nil then
      InsertCheckpoint(ItemNo, ATag, AName, ARaiseEvent)
    else begin
      {$IFDEF RVOLDTAGS}
      if rvoTagsArePChars in Options then
        StrDispose(PChar(Checkpoint.Tag));
      {$ENDIF}
      Checkpoint.Tag        := ATag;
      Checkpoint.Name       := AName;
      Checkpoint.RaiseEvent := ARaiseEvent;
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetItemTag(ItemNo: Integer; const ATag: TRVTag);
begin
  with GetItem(ItemNo) do begin
    if Tag=ATag then exit;
    {$IFDEF RVOLDTAGS}
    if rvoTagsArePChars in Options then
      StrDispose(PChar(Tag));
    {$ENDIF}
    Tag := ATag;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetItemVAlign(ItemNo: Integer; AVAlign: TRVVAlign);
var Item: TCustomRVItemInfo;
begin
  Item := GetItem(ItemNo);
  if not (item is TRVRectItemInfo) then
    raise ERichViewError.Create(errRVTypesMismatch);
  TRVRectItemInfo(Item).VAlign := AVAlign
end;
{------------------------------------------------------------------------------}
{ Sets item's property of integer type.
  ItemNo - index of item. Prop identifies the property. Value - new property
  value.
  Returns true is this item type has this property                             }
function TCustomRVData.SetItemExtraIntProperty(ItemNo: Integer;
  Prop: TRVExtraItemProperty; Value: Integer): Boolean;
begin
  Result := SetItemExtraIntPropertyEx(ItemNo, ord(Prop), Value);
end;
{------------------------------------------------------------------------------}
{ Gets item's property of integer type.
  ItemNo - index of item. Prop identifies the property. Value receives a
  property value.
  Returns true is this item type has this property                             }
function TCustomRVData.GetItemExtraIntProperty(ItemNo: Integer;
  Prop: TRVExtraItemProperty; var Value: Integer): Boolean;
begin
  Result := GetItem(ItemNo).GetExtraIntProperty(Prop, Value);
end;
{------------------------------------------------------------------------------}
{ Similar to SetItemExtraIntProperty, but the property is identified by an
  integer variable instead of TRVExtraItemProperty variable.
  If Prop is in range of TRVExtraItemProperty, this method works like
  SetItemExtraIntProperty.
  Otherwise, it supports additional item-specific properties,
  see rveipc*** constants.
}
function TCustomRVData.SetItemExtraIntPropertyEx(ItemNo: Integer;
  Prop: Integer; Value: Integer): Boolean;
{$IFNDEF RVDONOTUSESEQ}
var SeqList: TRVSeqList;
    SeqItem: TRVSeqItemInfo;
{$ENDIF}
begin
  Result := GetItem(ItemNo).SetExtraIntPropertyEx(Prop, Value);
  {$IFNDEF RVDONOTUSEANIMATION}
  if Result and
    (Prop=ord(rvepAnimationInterval)) or
    (Prop=ord(rvepImageWidth)) or
    (Prop=ord(rvepImageHeight)) then
    GetItem(ItemNo).UpdateAnimator(Self);
  {$ENDIF}
  {$IFNDEF RVDONOTUSESEQ}
  if (GetItem(ItemNo) is TRVSeqItemInfo) then begin
    SeqItem := TRVSeqItemInfo(GetItem(ItemNo));
    SeqList := GetSeqList(False);
    case Prop of
       rveipcSeqNumberType:
         SeqItem.CalcDisplayString(SeqList);
       rveipcSeqStartFrom, rveipcSeqReset:
         SeqList.RecalcCounters(SeqItem.GetIndexInList(SeqList), GetRVStyle, True);
    end;
  end;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{ Similar to GetItemExtraIntProperty, but the property is identified by an
  integer variable instead of TRVExtraItemProperty variable.
  If Prop is in range of TRVExtraItemProperty, this method works like
  GetItemExtraIntProperty.
  Otherwise, it supports additional item-specific properties,
  see rveipc*** constants.
}
function TCustomRVData.GetItemExtraIntPropertyEx(ItemNo: Integer;
  Prop: Integer; var Value: Integer): Boolean;
begin
  Result := GetItem(ItemNo).GetExtraIntPropertyEx(Prop, Value);
end;
{------------------------------------------------------------------------------}
{ Sets item's property of string type.
  ItemNo - index of item. Prop identifies the property. Value - new property
  value.
  Returns true is this item type has this property                             }
function TCustomRVData.SetItemExtraStrProperty(ItemNo: Integer;
  Prop: TRVExtraItemStrProperty; const Value: String): Boolean;
begin
  Result := GetItem(ItemNo).SetExtraStrProperty(Prop, Value);
end;
{------------------------------------------------------------------------------}
{ Gets item's property of string type.
  ItemNo - index of item. Prop identifies the property. Value receives a
  property value.
  Returns true is this item type has this property                             }
function TCustomRVData.GetItemExtraStrProperty(ItemNo: Integer;
  Prop: TRVExtraItemStrProperty; var Value: String): Boolean;
begin
  Result := GetItem(ItemNo).GetExtraStrProperty(Prop, Value);
end;
{------------------------------------------------------------------------------}
{ Similar to SetItemExtraStrProperty, but the property is identified by an
  integer variable instead of TRVExtraItemStrProperty variable.
  If Prop is in range of TRVExtraItemStrProperty, this method works like
  SetItemExtraStrProperty.
  Otherwise, it supports additional item-specific properties,
  see rvespc*** constants.
}
function TCustomRVData.SetItemExtraStrPropertyEx(ItemNo, Prop: Integer;
  const Value: String): Boolean;
{$IFNDEF RVDONOTUSESEQ}
var SeqList: TRVSeqList;
    SeqItem: TRVSeqItemInfo;
{$ENDIF}
begin
  Result := GetItem(ItemNo).SetExtraStrPropertyEx(Prop, Value);
  {$IFNDEF RVDONOTUSESEQ}
  if (GetItem(ItemNo) is TRVSeqItemInfo) then begin
    SeqItem := TRVSeqItemInfo(GetItem(ItemNo));
    SeqList := GetSeqList(False);
    case Prop of
       rvespcSeqFormatString:
         SeqItem.CalcDisplayString(SeqList);
       rvespcSeqName:
         SeqList.RecalcCounters(SeqItem.GetIndexInList(SeqList), GetRVStyle, True);
    end;
  end;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{ Similar to GetItemExtraStrProperty, but the property is identified by an
  integer variable instead of TRVExtraItemStrProperty variable.
  If Prop is in range of TRVExtraItemStrProperty, this method works like
  GetItemExtraStrProperty.
  Otherwise, it supports additional item-specific properties,
  see rvespc*** constants.
}
function TCustomRVData.GetItemExtraStrPropertyEx(ItemNo, Prop: Integer;
  var Value: String): Boolean;
begin
  Result := GetItem(ItemNo).GetExtraStrPropertyEx(Prop, Value);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SetPictureInfo(ItemNo: Integer; const AName: TRVAnsiString;
  Agr: TGraphic; AVAlign: TRVVAlign; const ATag: TRVTag): Boolean;
begin
  CheckItemClass(ItemNo, TRVGraphicItemInfo);
  with TRVGraphicItemInfo(GetItem(ItemNo)) do begin
    Result := (Agr.Width<>Image.Width) or
              (Agr.Height<>Image.Height) or
              (VAlign<>AVAlign);
    SetImage(Agr, Self);
    VAlign := AVAlign;
  end;
  SetItemTag(ItemNo, ATag);
  Items[ItemNo] := AName;
  GetItem(ItemNo).UpdatePaletteInfo(GetDoInPaletteMode, True, GetRVPalette,
    GetRVLogPalette);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemStyle(ItemNo: Integer): Integer;
begin
  Result := GetActualStyle(GetItem(ItemNo));
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetActualStyle(Item: TCustomRVItemInfo): Integer;
begin
  Result := Item.StyleNo;
  if Result=rvsDefStyle then begin
    if GetRVStyle.ParaStyles[Item.ParaNo].DefStyleNo>=0 then
      Result := GetRVStyle.ParaStyles[Item.ParaNo].DefStyleNo
    else
      Result := 0;
  end;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if (Result>=0) then begin
    Result := GetStyleFromStyleTemplate(Result, Item.ParaNo);
    if (Result<>Item.StyleNo) and (Item.StyleNo<>rvsDefStyle) then
      ModifyStyleNoByTemplate(Item, Result);
  end;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetActualTextStyle(Item: TCustomRVItemInfo): Integer;
begin
  if Item.StyleNo>=0 then
    Result := GetActualStyle(Item)
  else begin
    Result := Item.AssociatedTextStyleNo;
    if Result=rvsDefStyle then begin
      if GetRVStyle.ParaStyles[Item.ParaNo].DefStyleNo>=0 then
        Result := GetRVStyle.ParaStyles[Item.ParaNo].DefStyleNo
      else
        Result := 0;
    end;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    if (Result>=0) then begin
      Result := GetStyleFromStyleTemplate(Result, Item.ParaNo);
      if (Result<>Item.AssociatedTextStyleNo) and (Item.AssociatedTextStyleNo<>rvsDefStyle) then
        ModifyStyleNoByTemplate(Item, Result)
    end;
    {$ENDIF}
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetActualStyleEx(StyleNo, ParaNo: Integer): Integer;
begin
  Result := StyleNo;
  if Result=rvsDefStyle then begin
    if GetRVStyle.ParaStyles[ParaNo].DefStyleNo>=0 then
      Result := GetRVStyle.ParaStyles[ParaNo].DefStyleNo
    else
      Result := 0;
  end;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if (Result>=0) then
    Result := GetStyleFromStyleTemplate(Result, ParaNo);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure TCustomRVData.ModifyStyleNoByTemplate(Item: TCustomRVItemInfo;
   NewStyleNo: Integer);
begin
  if Item.StyleNo>=0 then
    Item.StyleNo := NewStyleNo
  else
    Item.AssociatedTextStyleNo := NewStyleNo;
end;
{------------------------------------------------------------------------------}
{ This method returns the index of text style basing on the given StyleNo and
  ParaNo, taking StyleTemplates into account.
  Normally, it just returns StyleNo. However, it is possible that
  ParaStyleTemplateId of this style does not correspond to paragraph's
  StyleTemplateId. In this case, a style with the proper ParaStyleTemplateId
  (and adjusted properties) is returned. If it doesn't exist, it is added.
  Another possible case to correct: if ParaStyleTemplateId of this style is
  equal to StyleTemplateId. In this case, the method returns the style with
  StyleTemplateId=-1.
}
function TCustomRVData.GetStyleFromStyleTemplate(StyleNo, ParaNo: Integer): Integer;
var RVStyle: TRVStyle;
    TextStyle, NewTextStyle: TFontInfo;
    ParaStyle: TParaInfo;
    StyleTemplate, ParaStyleTemplate: TRVStyleTemplate;
begin
  Result := StyleNo;
  if not UseStyleTemplates or
    (rvstNoStyleTemplateAdustment in GetAbsoluteRootData.State) or
    (ParaNo<0) then
    exit;
  RVStyle := GetRVStyle;
  if RVStyle=nil then
    exit;
  TextStyle := RVStyle.TextStyles[StyleNo];
  ParaStyle := RVStyle.ParaStyles[ParaNo];
  if (TextStyle.ParaStyleTemplateId<>ParaStyle.StyleTemplateId) or
     ((TextStyle.StyleTemplateId>0) and (TextStyle.StyleTemplateId=TextStyle.ParaStyleTemplateId)) then begin
    NewTextStyle := DefRVFontInfoClass.Create(nil);
    try
      NewTextStyle.Assign(TextStyle);
      StyleTemplate := RVStyle.StyleTemplates.FindItemById(NewTextStyle.StyleTemplateId);
      ParaStyleTemplate := RVStyle.StyleTemplates.FindItemById(NewTextStyle.ParaStyleTemplateId);
      StyleTemplate.UpdateModifiedTextStyleProperties(NewTextStyle, ParaStyleTemplate{, False});
      NewTextStyle.ParaStyleTemplateId := ParaStyle.StyleTemplateId;
      if NewTextStyle.StyleTemplateId=NewTextStyle.ParaStyleTemplateId then
        NewTextStyle.StyleTemplateId := -1;
      StyleTemplate := RVStyle.StyleTemplates.FindItemById(NewTextStyle.StyleTemplateId);
      ParaStyleTemplate := RVStyle.StyleTemplates.FindItemById(NewTextStyle.ParaStyleTemplateId);
      StyleTemplate.ApplyToTextStyle(NewTextStyle, ParaStyleTemplate);
      Result := GetOrAddTextStyle(NewTextStyle);
    finally
      NewTextStyle.Free;
    end;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetBreakInfo(ItemNo: Integer; var AWidth: TRVStyleLength;
  var AStyle: TRVBreakStyle; var AColor: TColor; var ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVBreakItemInfo);
  with TRVBreakItemInfo(GetItem(ItemNo)) do begin
    AWidth := LineWidth;
    AStyle := Style;
    AColor := Color;
    ATag := Tag;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetBulletInfo(ItemNo: Integer; var AName: TRVAnsiString;
  var AImageIndex: Integer; var AImageList: TCustomImageList; var ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVBulletItemInfo);
  with GetItem(ItemNo) as TRVBulletItemInfo do begin
    AImageIndex := ImageIndex;
    AImageList := ImageList;
    ATag := Tag;
  end;
  AName := Items[ItemNo];
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetControlInfo(ItemNo: Integer; var AName: TRVAnsiString;
  var Actrl: TControl; var AVAlign: TRVVAlign; var ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVControlItemInfo);
  with TRVControlItemInfo(GetItem(ItemNo)) do begin
    Actrl := Control;
    ATag := Tag;
    AVAlign := VAlign;
  end;
  AName := Items[ItemNo];
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetHotspotInfo(ItemNo: Integer; var AName: TRVAnsiString;
  var AImageIndex, AHotImageIndex: Integer; var AImageList: TCustomImageList;
  var ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVHotspotItemInfo);
  with TRVHotspotItemInfo(GetItem(ItemNo)) do begin
    AImageIndex := ImageIndex;
    AHotImageIndex := HotImageIndex;
    AImageList := ImageList;
    ATag := Tag;
  end;
  AName := Items[ItemNo];
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTag(ItemNo: Integer): TRVTag;
begin
  Result := GetItem(ItemNo).Tag;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemVAlign(ItemNo: Integer): TRVVAlign;
var Item: TCustomRVItemInfo;
begin
  Item := GetItem(ItemNo);
  if not (item is TRVRectItemInfo) then
    raise ERichViewError.Create(errRVTypesMismatch);
  Result := TRVRectItemInfo(Item).VAlign;  
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetPictureInfo(ItemNo: Integer; var AName: TRVAnsiString;
  var Agr: TGraphic; var AVAlign: TRVVAlign; var ATag: TRVTag);
begin
  CheckItemClass(ItemNo, TRVGraphicItemInfo);
  with GetItem(ItemNo) as TRVGraphicItemInfo do begin
    Agr := Image;
    ATag := Tag;
    AVAlign := VAlign;
  end;
  AName := Items[ItemNo];
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetTextInfo(ItemNo: Integer; var AText: String;
  var ATag: TRVTag);
begin
  if (GetItemStyle(ItemNo)<0) then
    raise ERichViewError.Create(errRVTypesMismatch);
  ATag  := GetItem(ItemNo).Tag;
  AText := GetItemText(ItemNo);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsFromNewLine(ItemNo: Integer): Boolean;
begin
  Result := not GetItem(ItemNo).SameAsPrev;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemPara(ItemNo: Integer): Integer;
begin
  Result := GetItem(ItemNo).ParaNo;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsParaStart(ItemNo: Integer): Boolean;
begin
  Result := not GetItem(ItemNo).SameAsPrev and
            not GetItem(ItemNo).BR;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.CheckItemIndex(i: Integer);
begin
  if (i<0) or (i>=Items.Count) then
    raise ERichViewError.Create(errRVItemRangeError);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetPageBreaksBeforeItems(Index: Integer): Boolean;
begin
  CheckItemIndex(Index);
  Result := GetItem(Index).PageBreakBefore;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetPageBreaksBeforeItems(Index: Integer;
  Value: Boolean);
begin
  CheckItemIndex(Index);
  {$IFNDEF RVDONOTUSELISTS}
  if (Index>0) and (GetItemStyle(Index-1)=rvsListMarker) then
    dec(Index);
  {$ENDIF}
  GetItem(Index).PageBreakBefore := Value;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetClearLeft(Index: Integer): Boolean;
begin
  CheckItemIndex(Index);
  Result := GetItem(Index).ClearLeft;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetClearRight(Index: Integer): Boolean;
begin
  CheckItemIndex(Index);
  Result := GetItem(Index).ClearRight;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetClearLeft(Index: Integer; const Value: Boolean);
begin
  CheckItemIndex(Index);
  GetItem(Index).ClearLeft := Value;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetClearRight(Index: Integer;
  const Value: Boolean);
begin
  CheckItemIndex(Index);
  GetItem(Index).ClearRight := Value;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.FindCheckpointByName(
  const Name: String): TCheckpointData;
var cp: TRVCPInfo;
begin
  Result := nil;
  cp := FirstCP;
  while cp<>nil do begin
    if cp.Name=Name then begin
      Result := cp;
      exit;
    end;
    cp := cp.Next;
  end;
  if (NotAddedCP<>nil) and (NotAddedCP.Name=Name) then
    Result := NotAddedCP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.FindCheckpointByTag(const Tag: TRVTag): TCheckpointData;
var cp: TRVCPInfo;
begin
  Result := nil;
  cp := FirstCP;
  while cp<>nil do begin
    if RV_CompareTags(cp.Tag,Tag, rvoTagsArePChars in Options) then begin
      Result := cp;
      exit;
    end;
    cp := cp.Next;
  end;
  if (NotAddedCP<>nil) and RV_CompareTags(NotAddedCP.Tag,Tag,rvoTagsArePChars in Options) then
    Result := NotAddedCP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetCheckpointByNo(No: Integer): TCheckpointData;
var i: Integer;
    cp: TRVCPInfo;
begin
  if (no<0) or (no>=CPCount) then begin
    raise ERichViewError.Create(SysUtils.Format(errRVNoSuchCP,[no]));
    exit;
  end;
  if (no=CPCount-1) and (NotAddedCP<>nil) then
    Result := NotAddedCP
  else begin
    cp := FirstCP;
    for i := 1 to no do begin
      if cp = nil then break;
      cp := cp.Next;
    end;
    //Assert(cp<>nil, 'Can''t find checkpoint');
    Result := cp;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetFirstCheckpoint: TCheckpointData;
begin
  Result := FirstCP;
  if Result = nil then
    Result := NotAddedCP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetLastCheckpoint: TCheckpointData;
begin
  Result := NotAddedCP;
  if Result = nil then
    Result := LastCP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetNextCheckpoint(CheckpointData: TCheckpointData): TCheckpointData;
begin
  Result := nil;
  if CheckpointData=nil then
    raise ERichViewError.Create(errRVNil);
  if CheckpointData=NotAddedCP then exit;
  Result := TRVCPInfo(CheckpointData).Next;
  if Result = nil then
    Result := NotAddedCP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetPrevCheckpoint(CheckpointData: TCheckpointData): TCheckpointData;
begin
  if CheckpointData=nil then
    raise ERichViewError.Create(errRVNil);
  if CheckpointData=NotAddedCP then begin
    Result := LastCP;
    exit;
  end;
  Result := TRVCPInfo(CheckpointData).Prev;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemCheckpoint(ItemNo: Integer): TCheckpointData;
begin
  Result := GetItem(ItemNo).Checkpoint;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetCheckpointItemNo(CheckpointData: TCheckpointData): Integer;
begin
  if CheckpointData = nil then
    raise ERichViewError.Create(errRVNil);

  if CheckpointData=NotAddedCP then
    Result := -1
  else begin
    Result := Items.IndexOfObject(TRVCPInfo(CheckpointData).ItemInfo);
    if Result=-1 then
      raise ERichViewError.Create(errRVNoSuchCP2);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetCheckpointNo(CheckpointData: TCheckpointData): Integer;
var cp: TRVCPInfo;
begin
  if CheckpointData = nil then
    raise ERichViewError.Create(errRVNil);

  cp := FirstCP;
  Result := 0;

  while cp<>nil do begin
    if cp=CheckpointData then exit;
    cp := cp.Next;
    inc(Result);
  end;

  if CheckpointData=NotAddedCP then exit;

  if CheckpointData = nil then
    raise ERichViewError.Create(errRVNoSuchCP2);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetCheckpointInfo(CheckpointData: TCheckpointData;
  var Tag: TRVTag; var Name: String; var RaiseEvent: Boolean);
begin
  if CheckpointData = nil then
    raise ERichViewError.Create(errRVNil);
  Name       := TRVCPInfo(CheckpointData).Name;
  Tag        := TRVCPInfo(CheckpointData).Tag;
  RaiseEvent := TRVCPInfo(CheckpointData).RaiseEvent;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.NormalizeParas(StartItemNo: Integer);
var i,ParaNo: Integer;
begin
  if Items.Count=0 then exit;
  i := StartItemNo;
  if i>=Items.Count then
    i := Items.Count-1;
  while (i>0) and not GetItem(i).CanBeBorderStart do
    dec(i);
  ParaNo := GetItemPara(i);
  inc(i);
  while (i<Items.Count) and not GetItem(i).CanBeBorderStart do begin
    GetItem(i).ParaNo := ParaNo;
    inc(i);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.FindCPBeforeItem(ItemNo: Integer): TRVCPInfo;
begin
  UpdateCPItemNo;
  if (FirstCP=nil) or
     (FirstCP.ItemNo>=ItemNo) then begin
    Result := nil; // no CP before
    exit;
  end;
  Result := FirstCP;
  while Result.Next<>nil do begin
    if Result.Next.ItemNo>=ItemNo then exit;
    Result := Result.Next;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetExtraDocuments: TStrings;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.RVFGetLimits(SaveScope: TRVFSaveScope; var StartItem,
  EndItem, StartOffs, EndOffs: Integer; var StartPart, EndPart: TRVMultiDrawItemPart;
  var SelectedItem: TCustomRVItemInfo);
begin
  StartItem := 0;
  EndItem   := Items.Count-1;
  if StartItem<Items.Count then begin
    StartOffs := GetOffsBeforeItem(StartItem);
    if EndItem>=0 then
      EndOffs   := GetOffsAfterItem(EndItem);
  end;
  StartPart := nil;
  EndPart   := nil;
  SelectedItem := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVFOptions: TRVFOptions;
begin
  Result := GetRootData.GetRVFOptions;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetExtraRTFCode(Area: TRVRTFSaveArea; Obj: TObject;
  Index1, Index2: Integer; InStyleSheet: Boolean): TRVAnsiString;
begin
  Result := GetAbsoluteRootData.GetExtraRTFCode(Area, Obj, Index1, Index2, InStyleSheet);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetExtraDocXCode(Area: TRVDocXSaveArea; Obj: TObject;
  Index1, Index2: Integer): TRVAnsiString;
begin
  Result := GetAbsoluteRootData.GetExtraDocXCode(Area, Obj, Index1, Index2);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetExtraHTMLCode(Area: TRVHTMLSaveArea; CSSVersion: Boolean): String;
begin
  Result := GetAbsoluteRootData.GetExtraHTMLCode(Area, CSSVersion);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsAssignedOnProgress: Boolean;
begin
 Result := GetAbsoluteRootData.IsAssignedOnProgress;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoProgress(Operation: TRVLongOperation;
  Stage: TRVProgressStage; PercentDone: Byte);
begin
  GetAbsoluteRootData.DoProgress(Operation, Stage, PercentDone);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveProgressInit(var Progress: Integer;
  Max: Integer; Operation: TRVLongOperation): Boolean;
begin
  Result := (Max>QUICKITEMCOUNT) and IsAssignedOnProgress;
  Progress := 0;
  if Result then
    DoProgress(Operation, rvpstgStarting, 0);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveProgressRun(var Progress: Integer;
  Position, Max: Integer; Operation: TRVLongOperation);
var NewProgress: Integer;
begin
  NewProgress := MulDiv(Position, 100, Max);
  if NewProgress<>Progress then begin
    Progress := NewProgress;
    DoProgress(Operation, rvpstgRunning, Progress);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveProgressDone(Operation: TRVLongOperation);
begin
  DoProgress(Operation, rvpstgEnding, 0);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetParaHTMLCode(RVData: TCustomRVData; ItemNo: Integer;
  ParaStart, CSSVersion: Boolean): String;
begin
  Result := GetAbsoluteRootData.GetParaHTMLCode(RVData, ItemNo, ParaStart,
    CSSVersion);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetParaHTMLCode2(RVData: TCustomRVData; ItemNo: Integer;
  ParaStart, CSSVersion: Boolean; Options: TRVSaveOptions;
  RVStyle: TRVStyle): TRVRawByteString;
begin
  Result := StringToHTMLString(
    GetParaHTMLCode(RVData, ItemNo, ParaStart, CSSVersion),
    Options, RVStyle);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetRVFOptions(const Value: TRVFOptions);
begin
  GetRootData.SetRVFOptions(Value);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVFWarnings: TRVFWarnings;
begin
  Result := GetRootData.GetRVFWarnings;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetRVFWarnings(const Value: TRVFWarnings);
begin
  GetRootData.SetRVFWarnings(Value);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVFSaveScope(SelectionOnly: Boolean):TRVFSaveScope;
begin
  if SelectionOnly then
    Result := rvfss_Selection
  else if rvstSavingPage in GetAbsoluteRootData.State then
    Result := rvfss_FullInPage
  else
    Result := rvfss_Full;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
{------------------------------------------------------------------------------}
type
  TRVFHeader = record
    StyleNo,ParaNo, ReadType, ExtraValue: Integer;
    DataCount, DataRead: Integer;
    Item: TCustomRVItemInfo;
    ClassName: String;
    Name: TRVRawByteString;
    Version, SubVersion: Integer;
    UTF8Strings: Boolean;
    RaiseEvent: Integer;
    PersistentCheckpoint: Integer;
    CheckPointTag: TRVTag;
    AssociatedTextStyleNameUsed: Boolean;
    PUnits: PRVStyleUnits;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    StyleTemplates: TRVStyleTemplateCollection;
    {$ENDIF}
  end;
  {------------------------------------------------------------------------------}
procedure TCustomRVData.DataReader(Stream: TStream);
var Size: Integer;
    MemStream: TRVMemoryStream;
    Color: TColor;
    Back: TRVBackground;
    Layout: TRVLayoutInfo;
    R: Boolean;
begin
  MemStream := TRVMemoryStream.Create;
  Layout := TRVLayoutInfo.Create;
  Include(State, rvstLoadingAsPartOfItem);
  Include(State, rvstLoadingAsSubDoc);
  try
    Stream.ReadBuffer(Size, SizeOf(Size));
    MemStream.SetSize(Size);
    Stream.ReadBuffer(MemStream.Memory^, Size);
    Back := nil;
    R := LoadRVFFromStream(MemStream, Color, Back, Layout
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}, nil{$ENDIF});
    if GetParentData<>nil then
      GetParentData.RVFWarnings := GetParentData.RVFWarnings+RVFWarnings;
    if not R then
      raise EReadError.Create(errRVFSubDoc);
    if Layout.Loaded then
      ApplyLayoutInfo(Layout);
  finally
    MemStream.Free;
    Layout.Free;
    Exclude(State, rvstLoadingAsSubDoc);    
    Exclude(State, rvstLoadingAsPartOfItem);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ApplyLayoutInfo(Layout: TRVLayoutInfo);
begin
 if Layout.FirstParaAborted<>0 then begin
    Include(State, rvstFirstParaAborted);
    FFirstParaListNo := Layout.FirstMarkerListNo;
    FFirstParaLevel := Layout.FirstMarkerLevel;
    end
  else begin
    Exclude(State, rvstFirstParaAborted);
    FFirstParaListNo := -1;
    FFirstParaLevel := -1;
  end;
  if Layout.LastParaAborted<>0 then
    Include(State, rvstLastParaAborted)
  else
    Exclude(State, rvstLastParaAborted); 
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DataWriter(Stream: TStream);
var StartPos,Size: Integer;
    sb: Boolean;
begin
  Size := 0;
  StartPos := Stream.Position;
  Stream.WriteBuffer(Size, SizeOf(Size));
  sb := rvfoSaveBack in RVFOptions;
  RVFOptions := RVFOptions - [rvfoSaveBack];
  GetRVData.SaveRVFToStream(Stream, False, clNone, nil, nil, False);
  if sb then
    RVFOptions := RVFOptions + [rvfoSaveBack];
  Size := Stream.Position-SizeOf(Size)-StartPos;
  Stream.Position := StartPos;
  Stream.WriteBuffer(Size, SizeOf(Size));
  Stream.Position := StartPos+SizeOf(Size)+Size;
end;
{------------------------------------------------------------------------------}
function InsertRVFHeaderData(RVData: TCustomRVData; const Caption: TRVRawByteString;
  var Header: TRVFHeader; var PrevCP, CurCP: TRVCPInfo;
  var Index, InsertPoint: Integer; var FirstTime: Boolean; AParaNo: Integer;
  AppendMode, EditFlag: Boolean; var NonFirstItemsAdded: Integer;
  var FullReformat: Boolean; TextStylesMapping,
  ListStylesMapping: TRVIntegerList): Boolean;
var item: TCustomRVItemInfo;
    CP: TRVCPInfo;
    Caption2: TRVRawByteString;
    NewListNo: Integer;
    {$IFNDEF RVDONOTUSELISTS}
    OldListNo: Integer;
    {$ENDIF}
    {$IFNDEF RVDONOTUSEUNICODE}
    TextStyle: TFontInfo;
    {$ENDIF}
    NoSTAdj: Boolean;
    ActStyleNo: Integer;
begin
  Result := True;
  {$IFNDEF RVDONOTUSELISTS}
  OldListNo := -1;
  {$ENDIF}
  if (Header.StyleNo=rvsBack) or
     (Header.StyleNo=rvsVersionInfo) then exit;
  if Header.Item<>nil then begin
    NoSTAdj := rvstNoStyleTemplateAdustment in RVData.GetAbsoluteRootData.State;
    try
      RVData.GetAbsoluteRootData.State := RVData.GetAbsoluteRootData.State + [rvstNoStyleTemplateAdustment];
      ActStyleNo := RVData.GetActualStyle(Header.Item);
    finally
      if not NoSTAdj then
        RVData.GetAbsoluteRootData.State := RVData.GetAbsoluteRootData.State - [rvstNoStyleTemplateAdustment];
    end;
    if Header.StyleNo>=0 then begin
      if EditFlag and
         (rvprRVFInsertProtect in RVData.GetRVStyle.TextStyles[ActStyleNo].Protection) then
        exit;
      item := RichViewTextItemClass.Create(RVData);
      item.BeforeLoading(rvlfRVF);
      item.Assign(Header.Item);
      item.Tag := RV_CopyTag(Header.Item.Tag, rvoTagsArePChars in RVData.Options);
      end
    else begin
      item := Header.Item;
      if Header.PUnits^<>RVData.GetRVStyle.Units then begin
        item.ConvertToDifferentUnits(RVData.GetRVStyle.Units, nil, False);
        RVData.RVFWarnings := RVData.RVFWarnings+[rvfwConvUnits];
      end;
      Header.Item := nil;
      if (item.AssociatedTextStyleNo>=0) and (item.AssociatedTextStyleNo<>rvsDefStyle) and
        not RichViewDoNotCheckRVFStyleRefs then begin
        if (TextStylesMapping<>nil) and not Header.AssociatedTextStyleNameUsed then begin
          if item.AssociatedTextStyleNo>=TextStylesMapping.Count then begin
            RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
            if rvfoConvUnknownStylesToZero in RVData.RVFOptions then
              item.AssociatedTextStyleNo := 0
            else begin
              item.Free;
              exit;
            end;
          end;
          item.AssociatedTextStyleNo := TextStylesMapping[item.AssociatedTextStyleNo];
        end;
        if item.AssociatedTextStyleNo>=RVData.GetRVStyle.TextStyles.Count then begin
          RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
          if rvfoConvUnknownStylesToZero in RVData.RVFOptions then
            item.AssociatedTextStyleNo := 0
          else begin
            item.Free;
            exit;
          end;
        end;
      end;
      if not item.GetBoolValue(rvbpValid) then begin
        item.Free;
        exit;
      end;
    end;
    if (Header.ParaNo<0) and
       ((FirstTime and EditFlag) or
        ((InsertPoint>0) and (RVData.Items.Count<>0) and
          not RVData.GetItem(InsertPoint-1).GetBoolValue(rvbpFullWidth))) then begin
      item.SameAsPrev := True;
      if FirstTime and EditFlag then
        item.ParaNo := RVData.GetCurParaStyleNo
      else
        item.ParaNo := RVData.GetItem(InsertPoint-1).ParaNo;
      end
    else begin
      item.SameAsPrev := False;
      if (Header.ParaNo<>-1) then
        item.ParaNo := Header.ParaNo
      else
        item.ParaNo := 0;
    end;
    item.UpdatePaletteInfo(RVData.GetDoInPaletteMode, False,
      RVData.GetRVPalette, RVData.GetRVLogPalette);
    if CurCP<> nil then begin
      if CurCP=RVData.NotAddedCP then begin
        dec(RVData.CPCount);
        RVData.NotAddedCP := nil;
      end;
      CP := CurCP;
      if not EditFlag then begin
        inc(RVData.CPCount);
        RVData.SetCP(item, PrevCP, CurCP)
        end
      else begin
        item.Checkpoint := CurCP;
        CurCP.ItemInfo := item;
        CurCP := nil;
      end;
      PrevCP := CP;
    end;
    Caption2 := Caption;
    {$IFNDEF RVDONOTUSEUNICODE}
    if (rvioUnicode in item.ItemOptions) and (item.StyleNo>=0) and
       (Header.ReadType=RVF_SAVETYPE_HEXUNICODE) then
      Caption2 := RVDecodeString(Caption);
    if (rvioUnicode in item.ItemOptions) and
       (item.StyleNo>=0) and
       (RVData.GetRVStyle<>nil) and
       not RVData.GetRVStyle.TextStyles[ActStyleNo].Unicode then begin
      Caption2 := RVU_UnicodeToAnsi(RVData.GetStyleCodePage(ActStyleNo), Caption);
      Exclude(item.ItemOptions, rvioUnicode);
      RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvFromUnicode];
    end;
    if not (rvioUnicode in item.ItemOptions) and
       (item.StyleNo>=0) and
       (RVData.GetRVStyle<>nil) and
       RVData.GetRVStyle.TextStyles[ActStyleNo].Unicode then begin
      TextStyle := RVData.GetRVStyle.TextStyles[ActStyleNo];
      if (TextStyle.Charset=SYMBOL_CHARSET) or
         (CompareText(TextStyle.FontName, RVFONT_SYMBOL)=0) or
         (CompareText(TextStyle.FontName, RVFONT_WINGDINGS)=0) then
        Caption2 := RVU_SymbolCharsetToUnicode(Caption)
      else
        Caption2 := RVU_AnsiToUnicode(RVData.GetStyleCodePage(ActStyleNo), Caption);
      Include(item.ItemOptions, rvioUnicode);
      RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvToUnicode];
    end;
    {$ENDIF}
    {$IFNDEF RVDONOTUSELISTS}
    if (item.StyleNo=rvsListMarker) and not RichViewDoNotCheckRVFStyleRefs then begin
      if (ListStylesMapping<>nil) and (TRVMarkerItemInfo(item).ListNo>=0) and
        not Header.AssociatedTextStyleNameUsed then begin
        if TRVMarkerItemInfo(item).ListNo>=ListStylesMapping.Count then begin
          RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
          TRVMarkerItemInfo(item).ListNo := 0;
          end
        else begin
          OldListNo := TRVMarkerItemInfo(item).ListNo;
          TRVMarkerItemInfo(item).ListNo := ListStylesMapping[OldListNo];
        end;
      end;
      if TRVMarkerItemInfo(item).ListNo>=RVData.GetRVStyle.ListStyles.Count then begin
        RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
        TRVMarkerItemInfo(item).ListNo := 0;
        if TRVMarkerItemInfo(item).ListNo>=RVData.GetRVStyle.ListStyles.Count then
          TRVMarkerItemInfo(item).ListNo := -1;
      end;
    end;
    {$ENDIF}
    if FirstTime then begin
      if AppendMode then begin
        if AParaNo=-1 then begin
          item.SameAsPrev := (InsertPoint>0) and
            not RVData.GetItem(InsertPoint-1).GetBoolValue(rvbpFullWidth);
          if item.SameAsPrev then
            item.ParaNo := RVData.GetItem(InsertPoint-1).ParaNo
          else
            item.ParaNo := 0;
          end
        else begin
          item.SameAsPrev := False;
          item.ParaNo := AParaNo;
        end;
      end;
      if not RVData.InsertFirstRVFItem(InsertPoint, Caption2, item, EditFlag,
        FullReformat, NewListNo) then begin
        Result := False;
        exit;
      end;
      if item<>nil then begin
        {$IFNDEF RVDONOTUSESEQ}
        if item is TCustomRVNoteItemInfo then
          RVData.AfterReadNote(RVData, InsertPoint);
        {$ENDIF}      
        inc(InsertPoint);
        Index := InsertPoint-1;
        FirstTime := False;
      end;
      {$IFNDEF RVDONOTUSELISTS}
      if (OldListNo>=0) and (NewListNo>=0) then
        ListStylesMapping[OldListNo] := NewListNo;
      {$ENDIF}
      end
    else begin
      item.Inserting(RVData, Caption2, True);
      RVData.Items.InsertObject(InsertPoint, Caption2, item);
      item.Inserted(RVData, InsertPoint);
      {$IFNDEF RVDONOTUSESEQ}
      RVData.AddSeqInList(InsertPoint);
      if item is TCustomRVNoteItemInfo then
        RVData.AfterReadNote(RVData, InsertPoint);
      {$ENDIF}      
      {$IFNDEF RVDONOTUSELISTS}
      RVData.AddMarkerInList(InsertPoint);
      {$ENDIF}
      inc(InsertPoint);
      inc(NonFirstItemsAdded);
    end;
    if item<>nil then begin
      RVData.ControlAction(RVData, rvcaAfterRVFLoad, InsertPoint-1, item);
      if not (rvstLoadingAsPartOfItem in RVData.State) then
        item.AfterLoading(rvlfRVF);
      end
    else
      RVData.FreeCheckpoint(CurCP, False, False);
    end
  else begin
    // unknown item type
    if CurCP<> nil then begin
       if not EditFlag then begin
         inc(RVData.CPCount);
         item := RichViewTextItemClass.Create(RVData);
         RVData.SetCP(item, PrevCP, CurCP);
         RVData.InternalFreeItem(item,False);
         end
       else begin
         RVData.FreeCheckpoint(CurCP, False, False);
       end;
    end;
  end;
end;
{------------------------------------------------------------------------------}
function RVFReadHeader(RVData: TCustomRVData; const CurrentLine: TRVRawByteString;
  var Header: TRVFHeader; AParaNo: Integer; var Color: TColor;
  Background: TRVBackground;
  TextStylesMapping, ParaStylesMapping: TRVIntegerList;
  AllowReplaceStyles: Boolean): Boolean;
var P: PRVAnsiChar;
    ItemOptions: Integer;
    ABackgroundStyle, AColor, Tmp: Integer;
    Tag: TRVTag;
    TextStyleNameUsed, ParaStyleNameUsed: Boolean;
begin
  P := PRVAnsiChar(CurrentLine);
  Result := False;
  Header.DataRead := 0;
  Header.AssociatedTextStyleNameUsed := False;
  if not RVFReadTextStyle(RVData.GetRVStyle,P,Header.StyleNo, Header.UTF8Strings,
    TextStyleNameUsed) then
    exit; {error}
  if Header.StyleNo = rvsVersionInfo then begin
    Header.DataCount := 0;
    Result := (RVFReadInteger(P,Header.Version) and
               RVFReadInteger(P,Header.SubVersion));
    if RVFReadInteger(P, Tmp) and (Tmp>=1) then
      Header.UTF8Strings := True;
    exit;
  end;
  if not (RVFReadInteger(P,Header.DataCount) and
    RVFReadParaStyle(RVData.GetRVStyle,P,Header.ParaNo, Header.UTF8Strings,
    ParaStyleNameUsed)) then
    exit; {error}
  if (Header.StyleNo<>rvsBack) and (Header.StyleNo<>rvsCheckpoint) and
     (Header.StyleNo<>rvsDocProperty) then
    Header.Item := CreateRichViewItem(Header.StyleNo, RVData);
  if Header.Item<>nil then
    Header.Item.BeforeLoading(rvlfRVF);
  if (Header.Version>=1)and(Header.SubVersion>=2) then begin
    if not RVFReadInteger(P,ItemOptions) then
      exit; {error}
    if Header.Item<>nil then
      Header.Item.ItemOptions := TRVItemOptions(Byte(ItemOptions));
  end;
  if not (RVFReadInteger(P,Header.ReadType) and
     RVFReadTag(P, rvoTagsArePChars in RVData.Options,
     (Header.Version>1) or (Header.SubVersion>2), Tag, Header.UTF8Strings)) then
    exit; {error}
  if Header.StyleNo = rvsDocProperty then begin
    if not RVFReadInteger(P,Header.ExtraValue) then
      exit; {error}
    {$IFNDEF RVDONOTUSEDOCPARAMS}
    if (Header.ExtraValue=RVF_DOCPROP_DOCPARAMETERS) and
       (RVData.GetDocParameters(False)<>nil) and
       AllowReplaceStyles and (rvfoLoadDocProperties in RVData.RVFOptions) then
      RVData.GetDocParameters(False).Reset;
    {$ENDIF}
  end;
  if Header.StyleNo=rvsCheckpoint then
    Header.CheckpointTag := Tag
  else if Header.Item<>nil then
    Header.Item.Tag := Tag;
  if (Header.Item<>nil) and (Header.StyleNo>=0) and
     not RichViewDoNotCheckRVFStyleRefs then begin
    if (TextStylesMapping<>nil) and (Header.StyleNo<>rvsDefStyle) and
      not TextStyleNameUsed then begin
      if Header.StyleNo>=TextStylesMapping.Count then begin
        RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
        if rvfoConvUnknownStylesToZero in RVData.RVFOptions then
          Header.StyleNo := 0
        else
          exit;
        end
      else
        Header.StyleNo := TextStylesMapping[Header.StyleNo];
    end;
    Header.Item.StyleNo := Header.StyleNo;
    if (Header.StyleNo<>rvsDefStyle) and
       (Header.StyleNo>=RVData.GetRVStyle.TextStyles.Count) then begin
      RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
      if rvfoConvUnknownStylesToZero in RVData.RVFOptions then
        Header.Item.StyleNo := 0
      else
        exit;
    end;
  end;
  if (Header.Item<>nil) and (Header.ParaNo>=0) and
      not RichViewDoNotCheckRVFStyleRefs then begin
    if (ParaStylesMapping<>nil) and not ParaStyleNameUsed then begin
      if Header.ParaNo>=ParaStylesMapping.Count then begin
        RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
        exit;
      end;
      Header.ParaNo := ParaStylesMapping[Header.ParaNo];
    end;
    if Header.ParaNo>=RVData.GetRVStyle.ParaStyles.Count then begin
      RVData.RVFWarnings := RVData.RVFWarnings + [rvfwConvUnknownStyles];
      if rvfoConvUnknownStylesToZero in RVData.RVFOptions then
        Header.ParaNo := 0
      else
        exit;
    end;
  end;
  case Header.StyleNo of
  {*}rvsCheckpoint:
    begin
      if not (P^ in [#0, #10, #13]) then begin
        if not RVFReadInteger(P,Header.RaiseEvent) then
          exit;
        end
      else
        Header.RaiseEvent := 0;
      if not (P^ in [#0, #10, #13]) then begin
        if not RVFReadInteger(P,Header.PersistentCheckpoint) then
          exit;
        end
      else
        Header.PersistentCheckpoint := 0;
    end;
  {*}rvsBack:
    begin
      if not (RVFReadInteger(P, ABackgroundStyle) and
         RVFReadInteger(P, AColor)) then
        exit;
      if (rvfoLoadBack in RVData.RVFOptions) and AllowReplaceStyles then begin
        Color := AColor;
        if Background<>nil then begin
          Background.Style := TBackgroundStyle(ABackgroundStyle);
          Background.Bitmap.Handle := 0;
        end;
      end;
    end;
  {*}else
    begin
      if Header.Item = nil then begin
        Result := True;
        exit;
      end;
      if not Header.Item.ReadRVFHeaderTail(P, RVData, Header.UTF8Strings,
        Header.AssociatedTextStyleNameUsed) then
        exit;
    end;
  end;
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoOnStyleReaderError(Reader: TReader;
  const Message: string; var Handled: Boolean);
begin
  RVFWarnings := RVFWarnings + [rvfwUnknownStyleProperties];
  Handled := True;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoOnCtrlReaderError(Reader: TReader; const Message: string;
  var Handled: Boolean);
begin
  RVFWarnings := RVFWarnings + [rvfwUnknownCtrlProperties];
  Handled := True;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.InsertRVFFromStream_(Stream: TStream; var Index: Integer;
  AParaNo: Integer; AllowReplaceStyles, AppendMode, EditFlag: Boolean;
  var Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo;
  var NonFirstItemsAdded: Integer; var Protect, FullReformat: Boolean;
  LoadAsSubDoc: Boolean
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; ReadStyleTemplates: TRVStyleTemplateCollection
  {$ENDIF}):Boolean;
var BufferString, CurrentLine: TRVRawByteString;
    Size: Integer;
    P, EndP, StartP: PRVAnsiChar;
    ReadState:TRVFReadState;
    ReadMode: TRVFReadMode;
    Header: TRVFHeader;
    FirstInsert: Boolean;
    PrevCP, CurCP: TRVCPInfo;
    PTextStylesMapping: PRVIntegerList;
    PParaStylesMapping: PRVIntegerList;
    PListStylesMapping: PRVIntegerList;
    InsertPoint: Integer;
    CallProgress: Boolean;
    Progress: Integer;
    RVStyle: TRVStyle;
  {.......................................................}
   procedure FreeCheckpointTag; // in-out: Header
   begin
     {$IFDEF RVOLDTAGS}
     if rvoTagsArePChars in Options then
       StrDispose(PChar(Header.CheckpointTag));
     {$ENDIF}
     Header.CheckpointTag := RVEMPTYTAG;
   end;
  {.......................................................}
  {$IFDEF RICHVIEWCBDEF3}
  procedure ReadStyles(Styles: TCustomRVInfos; StylesReadMode: TRVFReaderStyleMode);
  var Reader: TReader;
      TmpStream: TRVMemoryStream;
      Val: TValueType;
      {$IFDEF RICHVIEWDEF2009}
      DUS: Boolean;
      {$ENDIF}
  begin
    TmpStream := TRVMemoryStream.Create;
    try
      case Header.ReadType of
        RVF_SAVETYPE_TEXT:
          RVFTextString2Stream(CurrentLine, TmpStream);
        RVF_SAVETYPE_BINARY: 
          TmpStream.WriteBuffer(PRVAnsiChar(CurrentLine)^, Length(CurrentLine));
      end;
      if (TmpStream.Size>0) and (StylesReadMode<>rvf_sIgnore) then begin
        TmpStream.Position := 0;
        TmpStream.ReadBuffer(Val, sizeof(Val));
        if Val<>vaCollection then
          abort;
        Reader := TReader.Create(TmpStream, 4096);
        try
          Reader.OnError := DoOnStyleReaderError;
          {$IFDEF RICHVIEWDEF2009}
          DUS := RVStyle.DefaultUnicodeStyles;
          //RVStyle.DefaultUnicodeStyles := False;
          {$ENDIF}
          {$IFNDEF RVDONOTUSELISTS}
          if Styles is TRVListInfos then
            TRVListInfos(Styles).FRVData := Self;
          {$ENDIF}
          try
            Reader.ReadCollection(Styles);
          finally
            {$IFNDEF RVDONOTUSELISTS}
            if Styles is TRVListInfos then
              TRVListInfos(Styles).FRVData := nil;
            {$ENDIF}
            {$IFDEF RICHVIEWDEF2009}
            RVStyle.DefaultUnicodeStyles := DUS;
            {$ENDIF}
          end;
        finally
          Reader.Free;
        end;
      end;
    finally
      TmpStream.Free;
    end;
    if Header.PUnits^<>RVStyle.Units then
      Styles.ConvertToDifferentUnits(RVStyle.Units);
  end;
  {.......................................................}
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  procedure ReadAndMergeStyleTemplates;
  var Reader: TReader;
      TmpStream: TRVMemoryStream;
      Val: TValueType;
  begin
    if not (UseStyleTemplates and (GetRVFTextStylesReadMode=rvf_sInsertMerge) and
      (GetRVFParaStylesReadMode=rvf_sInsertMerge)) or
      (StyleTemplateInsertMode=rvstimIgnoreSourceStyleTemplates) then
      exit;
    if Header.StyleTemplates<>nil then
      abort;
    Header.StyleTemplates := TRVStyleTemplateCollection.Create(nil);
    if AllowReplaceStyles then
      RVStyle.StyleTemplates.Clear;
    TmpStream := TRVMemoryStream.Create;
    try
      case Header.ReadType of
        RVF_SAVETYPE_TEXT:
          RVFTextString2Stream(CurrentLine, TmpStream);
        RVF_SAVETYPE_BINARY:
          TmpStream.WriteBuffer(PRVAnsiChar(CurrentLine)^, Length(CurrentLine));
      end;
      if (TmpStream.Size>0) then begin
        TmpStream.Position := 0;
        TmpStream.ReadBuffer(Val, sizeof(Val));
        if Val<>vaCollection then
          abort;
        Reader := TReader.Create(TmpStream, 4096);
        try
          Reader.OnError := DoOnStyleReaderError;
          Reader.ReadCollection(Header.StyleTemplates);
        finally
          Reader.Free;
        end;
        if Header.PUnits^<>RVStyle.Units then
          Header.StyleTemplates.ConvertToDifferentUnits(RVStyle.Units);
        Header.StyleTemplates.UpdateReferences;
        if AllowReplaceStyles then
          RVStyle.StyleTemplates.AssignStyleTemplates(Header.StyleTemplates, True)
        else
          RVStyle.StyleTemplates.MergeWith(Header.StyleTemplates, nil);
      end;
    finally
      TmpStream.Free;
    end;

  end;
  {.......................................................}
  procedure GetParaStyleTemplates(Id: TRVStyleTemplateId; var STOld, STNew: TRVStyleTemplate);
  begin
    if not AllowReplaceStyles and (StyleTemplateInsertMode=rvstimIgnoreSourceStyleTemplates) then begin
      STOld := nil;
      STNew := RVStyle.StyleTemplates.NormalStyleTemplate;
      end
    else begin
      if Header.StyleTemplates<>nil then
        STOld := Header.StyleTemplates.FindItemById(Id)
      else
        STOld := nil;
      if STOld<>nil then
        STNew := RVStyle.StyleTemplates.FindItemByName(STOld.Name)
      else
        STNew := nil;
      if (STNew=nil) and RichViewAutoAssignNormalStyleTemplate then
        STNew := RVStyle.StyleTemplates.NormalStyleTemplate;
     end;
  end;
  {.......................................................}
  procedure GetTextStyleTemplates(Jump: Boolean; Id: TRVStyleTemplateId; var STOld, STNew: TRVStyleTemplate);
  begin
    STOld := nil;
    if (StyleTemplateInsertMode=rvstimIgnoreSourceStyleTemplates) then begin
      if Jump then
        STNew := RVStyle.StyleTemplates.FindItemByName(RVHYPERLINKSTYLETEMPLATENAME)
      else
        STNew := nil;
      end
    else begin
      if Header.StyleTemplates<>nil then
        STOld := Header.StyleTemplates.FindItemById(Id)
      else
        STOld := nil;
      if STOld<>nil then
        STNew := RVStyle.StyleTemplates.FindItemByName(STOld.Name)
      else
        STNew := nil;
     end;
  end;
  {.......................................................}
  procedure ApplyStyleTemplatesToParaStyles(ParaStyles: TParaInfos);
  var i: Integer;
      STOld, STNew: TRVStyleTemplate;
      DoIt: Boolean;
  begin
    DoIt := UseStyleTemplates and (GetRVFTextStylesReadMode=rvf_sInsertMerge) and
      (GetRVFParaStylesReadMode=rvf_sInsertMerge);
    if AllowReplaceStyles and not DoIt then
      exit;
    for i := 0 to ParaStyles.Count-1 do
      if DoIt then begin
        GetParaStyleTemplates(ParaStyles[i].StyleTemplateId, STOld, STNew);
        if STNew<>nil then begin
          ParaStyles[i].StyleTemplateId := STNew.Id;
          if not AllowReplaceStyles and (StyleTemplateInsertMode=rvstimUseTargetFormatting) then begin
            STOld.UpdateModifiedParaStyleProperties(ParaStyles[i]);
            STNew.ApplyToParaStyle(ParaStyles[i]);
          end;
          end
        else
          ParaStyles[i].StyleTemplateId := -1;
        end
      else
        ParaStyles[i].StyleTemplateId := -1;
  end;
  {.......................................................}
  procedure ApplyStyleTemplatesToTextStyles(TextStyles: TFontInfos);
  var i: Integer;
      STOld, STNew, STPOld, STPNew: TRVStyleTemplate;
      DoIt: Boolean;
  begin
    DoIt := UseStyleTemplates and (GetRVFTextStylesReadMode=rvf_sInsertMerge) and
      (GetRVFParaStylesReadMode=rvf_sInsertMerge);
    if AllowReplaceStyles and not DoIt then
      exit;
    for i := 0 to TextStyles.Count-1 do
      if DoIt then begin
        GetParaStyleTemplates(TextStyles[i].ParaStyleTemplateId, STPOld, STPNew);
        GetTextStyleTemplates(TextStyles[i].Jump, TextStyles[i].StyleTemplateId, STOld, STNew);
        if STPNew=STNew then
          STNew := nil;
        if STNew<>nil then
          TextStyles[i].StyleTemplateId := STNew.Id
        else
          TextStyles[i].StyleTemplateId := -1;
        if STPNew<>nil then
          TextStyles[i].ParaStyleTemplateId := STPNew.Id
        else
          TextStyles[i].ParaStyleTemplateId := -1;
        if not AllowReplaceStyles and ((STNew<>nil) or (STPNew<>nil)) and
          (StyleTemplateInsertMode=rvstimUseTargetFormatting) then begin
            STOld.UpdateModifiedTextStyleProperties(TextStyles[i], STPOld{, False});
            STNew.ApplyToTextStyle(TextStyles[i], STPNew);
        end;
        end
      else begin
        TextStyles[i].StyleTemplateId := -1;
        TextStyles[i].ParaStyleTemplateId := -1;
      end;
  end;
  {$ENDIF}
  {.......................................................}
  procedure MergeStyles(Base, Loaded: TCustomRVInfos;
                        var Mapping: TRVIntegerList;
                        StylesReadMode: TRVFReaderStyleMode);
  begin
    if (Loaded.Count=0) or (StylesReadMode = rvf_sIgnore) or (Mapping<>nil) then
      exit;
    if AllowReplaceStyles then begin
      {$IFNDEF RVDONOTUSELISTS}
      if Base is TRVListInfos then
        TRVListInfos(Base).FRichViewAllowAssignListID := True;
      {$ENDIF}
      try
        Base.Assign(Loaded);
      finally
        {$IFNDEF RVDONOTUSELISTS}
        if Base is TRVListInfos then
          TRVListInfos(Base).FRichViewAllowAssignListID := False;
        {$ENDIF}
      end;
      exit;
    end;
    Mapping := TRVIntegerList.Create;
    case StylesReadMode of
      rvf_sInsertMap:
        Base.MergeWith(Loaded, rvs_merge_Map, Mapping, PTextStylesMapping^, Self);
      rvf_sInsertMerge:
        Base.MergeWith(Loaded, rvs_merge_SmartMerge, Mapping, PTextStylesMapping^, Self);
    end;
  end;
  {.......................................................}
  procedure ClearExtraDocuments;
  var ExtraDocuments: TStrings;
      i: Integer;
  begin
    ExtraDocuments := GetExtraDocuments;
    if ExtraDocuments=nil then
      exit;
    for i := 0 to ExtraDocuments.Count-1 do
      (ExtraDocuments.Objects[i] as TCustomRVData).Clear;
  end;
  {.......................................................}
   procedure ReadDocProperty;
   var Styles, BaseStyles: TCustomRVInfos;
       StylesReadMode: TRVFReaderStyleMode;
       PMapping: PRVIntegerList;
       {$IFNDEF RVDONOTUSELISTS}
       i, ListNo: Integer;
       {$ENDIF}
       ExtraDocuments: TStrings;
       Index: Integer;
       TmpStream: TRVMemoryStream;
       TmpColor: TColor;
   begin
     case Header.DataRead of
       0:
         begin
           if Header.ExtraValue=RVF_DOCPROP_UNITS then
             Header.PUnits^ := TRVStyleUnits(RVStrToInt(CurrentLine))
           else if AllowReplaceStyles and (rvfoLoadDocProperties in RVFOptions) then
             case Header.ExtraValue of
               RVF_DOCPROP_DOCPROPLIST:
                 if GetDocProperties<>nil then
                   GetDocProperties.Add(RVFStringToString(CurrentLine, Header.UTF8Strings));
               {$IFNDEF RVDONOTUSEDOCPARAMS}
               RVF_DOCPROP_DOCPARAMETERS:
                 GetDocParameters(True).ReadProperyFromString(CurrentLine, Header.UTF8Strings);
               {$ENDIF}
               RVF_DOCPROP_EXTRADOCUMENT:
                 Header.ClassName := RVFStringToString(CurrentLine, Header.UTF8Strings);
             end;
           // if Header.ExtraValue<>values above
           // then ignoring this line (should be name of TRVStyle)
           if Header.ReadType=RVF_SAVETYPE_BINARY then
             ReadMode := rmBeforeBinary;
         end;
       1:
         begin
           case Header.ExtraValue of
             {$IFNDEF RVDONOTUSESTYLETEMPLATES}
             RVF_DOCPROP_STYLETEMPLATES:
               begin
                 ReadAndMergeStyleTemplates;
                 exit;
               end;
             {$ENDIF}
             RVF_DOCPROP_TEXTSTYLES:
               begin
                 if AllowReplaceStyles and (rvfoCanChangeUnits in RVFOptions) and
                    (Header.PUnits^<>RVStyle.Units) then
                   RVStyle.ConvertToDifferentUnits(Header.PUnits^);
                 if Header.PUnits^<>RVStyle.Units then
                   RVFWarnings := RVFWarnings+[rvfwConvUnits];
                 Styles := TFontInfos.Create(RVStyle.GetTextStyleClass, nil);
                 StylesReadMode := GetRVFTextStylesReadMode;
                 BaseStyles := RVStyle.TextStyles;
                 PMapping := PTextStylesMapping;
               end;
             RVF_DOCPROP_PARASTYLES:
               begin
                 if AllowReplaceStyles and (rvfoCanChangeUnits in RVFOptions) and
                    (Header.PUnits^<>RVStyle.Units) then
                   RVStyle.ConvertToDifferentUnits(Header.PUnits^);
                 if Header.PUnits^<>RVStyle.Units then
                   RVFWarnings := RVFWarnings+[rvfwConvUnits];
                 Styles := TParaInfos.Create(RVStyle.GetParaStyleClass, nil);
                 StylesReadMode := GetRVFParaStylesReadMode;
                 BaseStyles := RVStyle.ParaStyles;
                 PMapping := PParaStylesMapping;
               end;
             {$IFNDEF RVDONOTUSELISTS}
             RVF_DOCPROP_LISTSTYLES:
               begin
                 Styles := TRVListInfos.Create(RVStyle.GetListStyleClass, nil);
                 StylesReadMode := GetRVFParaStylesReadMode;
                 BaseStyles := RVStyle.ListStyles;
                 PMapping := PListStylesMapping;
               end;
             {$ENDIF}
             RVF_DOCPROP_LAYOUT:
               begin
                 if (Layout=nil) or (not AllowReplaceStyles) or
                    not (rvfoLoadLayout in RVFOptions) then
                   exit;
                 case Header.ReadType of
                   RVF_SAVETYPE_TEXT:
                      Layout.LoadText(CurrentLine);
                   RVF_SAVETYPE_BINARY:
                      Layout.LoadBinary(CurrentLine);
                 end;
                 if (Layout.FirstParaAborted<>0) and (Layout.FirstMarkerListNo>=0) and
                    (PListStylesMapping^<>nil) then
                   Layout.FirstMarkerListNo := PListStylesMapping^[Layout.FirstMarkerListNo];
                 exit;
               end;
             RVF_DOCPROP_PREVMARKERS:
               begin
                 {$IFNDEF RVDONOTUSELISTS}
                 if (GetPrevMarkers=nil) or (not AllowReplaceStyles) or
                   not (rvfoLoadLayout in RVFOptions) then
                   exit;
                 case Header.ReadType of
                   RVF_SAVETYPE_TEXT:
                      GetPrevMarkers.LoadText(CurrentLine, Self);
                   RVF_SAVETYPE_BINARY:
                      GetPrevMarkers.LoadBinary(CurrentLine, Self);
                 end;
                 if (PListStylesMapping^<>nil) then
                   for i := 0 to GetPrevMarkers.Count-1 do begin
                     ListNo := TRVMarkerItemInfo(GetPrevMarkers.Items[i]).ListNo;
                     if ListNo>=0 then
                     TRVMarkerItemInfo(GetPrevMarkers.Items[i]).ListNo :=
                       PListStylesMapping^[ListNo];
                   end;
                 {$ENDIF}
                 exit;
               end;
             RVF_DOCPROP_DOCPROPLIST:
               begin
                 if AllowReplaceStyles and (rvfoLoadDocProperties in RVFOptions) and
                   (GetDocProperties<>nil) then
                   GetDocProperties.Add(RVFStringToString(CurrentLine, Header.UTF8Strings));
                 exit;
               end;
             {$IFNDEF RVDONOTUSEDOCPARAMS}
             RVF_DOCPROP_DOCPARAMETERS:
               begin
                 if AllowReplaceStyles and (rvfoLoadDocProperties in RVFOptions) then
                   GetDocParameters(True).ReadProperyFromString(CurrentLine, Header.UTF8Strings);
                 exit;
               end;
             {$ENDIF}
             RVF_DOCPROP_EXTRADOCUMENT:
               begin
                 if not AllowReplaceStyles then
                   exit;
                 ExtraDocuments := GetExtraDocuments;
                 if ExtraDocuments=nil then
                   exit;
                 Index := ExtraDocuments.IndexOf(Header.ClassName);
                 if Index<0 then
                   exit;
                 case Header.ReadType of
                   RVF_SAVETYPE_TEXT:
                     begin
                       TmpStream := TRVMemoryStream.Create;
                       RVFTextString2Stream(CurrentLine, TmpStream);
                     end;
                   RVF_SAVETYPE_BINARY:
                     TmpStream := RVFStringToStream(CurrentLine);
                   else
                     TmpStream := nil;
                 end;
                 try
                   (ExtraDocuments.Objects[Index] as TCustomRVData).
                     LoadRVFFromStream(TmpStream, TmpColor, nil, nil
                     {$IFNDEF RVDONOTUSESTYLETEMPLATES},Header.StyleTemplates{$ENDIF});
                 finally
                   TmpStream.Free;
                 end;
                 exit;
               end;
             else
               exit;
           end;
           try
             ReadStyles(Styles, StylesReadMode);
             {$IFNDEF RVDONOTUSESTYLETEMPLATES}
             if not AllowReplaceStyles or
               (AllowReplaceStyles and RichViewAutoAssignNormalStyleTemplate and
               (Header.StyleTemplates=nil)) then begin
               if Styles is TParaInfos then
                 ApplyStyleTemplatesToParaStyles(TParaInfos(Styles))
               else if Styles is TFontInfos then
                 ApplyStyleTemplatesToTextStyles(TFontInfos(Styles))
             end;
             {$ENDIF}
             MergeStyles(BaseStyles, Styles, PMapping^, StylesReadMode);
           finally
             Styles.Free;
           end;
         end;
       else
         if AllowReplaceStyles and (rvfoLoadDocProperties in RVFOptions) then
           case Header.ExtraValue of
             RVF_DOCPROP_DOCPROPLIST:
               if GetDocProperties<>nil then
                 GetDocProperties.Add(RVFStringToString(CurrentLine, Header.UTF8Strings));
             {$IFNDEF RVDONOTUSEDOCPARAMS}
             RVF_DOCPROP_DOCPARAMETERS:
               GetDocParameters(True).ReadProperyFromString(CurrentLine, Header.UTF8Strings);
             {$ENDIF}
           end;
     end;
   end;
  {$ENDIF}
  {.......................................................}
                             // in    : CurrentLine
   procedure ReadBackground; // in-out: Header
                             // out   : ReadMode, ReadState
   var bmp : TBitmap;
   begin
     case Header.DataRead of
       0:
         begin
           // ignoring this line (should be TBitmap)
           if Header.ReadType=RVF_SAVETYPE_BINARY then
             ReadMode := rmBeforeBinary;
         end;
       1:
         begin
           if rvfoLoadBack in RVFOptions then begin
             if Background<>nil then begin
               if Header.ReadType=RVF_SAVETYPE_BINARY then
                 RVFLoadPictureBinary(CurrentLine, Background.Bitmap)
               else
                 if not RVFLoadPicture(CurrentLine, Background.Bitmap) then abort; {error}
               end
             else begin
               bmp := TBitmap.Create;
               if Header.ReadType=RVF_SAVETYPE_BINARY then
                 RVFLoadPictureBinary(CurrentLine, bmp)
               else
                 if not RVFLoadPicture(CurrentLine, bmp) then abort; {error}
               bmp.Free;
             end;
           end;
           ReadState := rstSkip;
         end;
     end;
   end;
  {.......................................................}
                               // in: ReadMode
                               // in-out: P
    procedure ReadCurrentLine; // out: CurrentLine
    var Start: PRVAnsiChar;
        Size, NewProgress: Integer;
    begin
      Start := P;
      case ReadMode of
        rmBinary:
          begin
            Move(P^,Size, SizeOf(Size));
            inc(Start, SizeOf(Size));
            inc(P,     SizeOf(Size)+Size);
          end;
        rmUnicode:
          begin
            while (PWord(P)^<>UNI_ParagraphSeparator) and
                  (PWord(P)^<>0) and
                  (P<EndP) do Inc(P,2);
          end;
        else
          begin
            while not (P^ in [#0, #10, #13]) do Inc(P);
          end;
      end;
      SetString(CurrentLine, Start, P - Start);
      if CallProgress then begin
        NewProgress := MulDiv(P-StartP, 100, EndP-StartP);
        if NewProgress<>Progress then begin
          Progress := NewProgress;
          DoProgress(rvloRVFRead, rvpstgRunning, Progress);
        end;
      end;

    end;
  { // test: allowing zero characters in Unicode
    procedure ReadCurrentLine; // out: CurrentLine
    var Start: PRVAnsiChar;
        Size: Integer;
        Ptr: PWordArray;
        i: Integer;
    begin
      Start := P;
      case ReadMode of
        rmBinary:
          begin
            Move(P^,Size, SizeOf(Size));
            inc(Start, SizeOf(Size));
            inc(P,     SizeOf(Size)+Size);
          end;
        rmUnicode:
          begin
            while (PWord(P)^<>UNI_ParagraphSeparator) and
                  (P<EndP) do Inc(P,2);
          end;
        else
          begin
            while not (P^ in [#0, #10, #13]) do Inc(P);
          end;
      end;
      SetString(CurrentLine, Start, P - Start);
      if ReadMode = rmUnicode then begin
        Ptr := Pointer(CurrentLine);
        for i := 0 to Length(CurrentLine) div 2 - 1 do
          if Ptr[i]=0 then
            Ptr[i] := ord('?');
      end;
    end; }
  {.......................................................}
   procedure SkipCurrentLineTail; // in-out: P, ReadMode
   begin
     case ReadMode of
       rmText:
         begin
           if P^ = #13 then Inc(P);
           if P^ = #10 then Inc(P);
         end;
       rmBeforeUnicode:
         begin
           if P^ = #13 then Inc(P) else abort; {error}
           if P^ = #10 then Inc(P) else abort; {error}
           ReadMode := rmUnicode;
         end;
       rmUnicode:
         begin
           if PWord(P)^=UNI_ParagraphSeparator then
             Inc(P, 2);
         end;
       rmAfterUnicode:
         begin
           if PWord(P)^=UNI_ParagraphSeparator then
             Inc(P, 2);
           ReadMode := rmText;
         end;
       rmBeforeBinary:
         begin
           if P^ = #13 then Inc(P) else abort; {error}
           if P^ = #10 then Inc(P) else abort; {error}
           ReadMode := rmBinary;
         end;
       rmBinary:
         begin
           ReadMode := rmText;
         end;
     end;
   end;
  {.......................................................}
  var StartIndex: Integer;
begin
  RVStyle := GetRVStyle;
  NonFirstItemsAdded := 0;
  Protect          := True;
  FirstInsert      := True;
  StartIndex       := Index;
  InsertPoint      := Index;
  if Index>Items.Count then
    Index := Items.Count;
  if Index=Items.Count then begin
    PrevCP := LastCP;
    if EditFlag then
      CurCP := nil
    else
      CurCP  := NotAddedCP;
    end
  else begin
    PrevCP := FindCPBeforeItem(Index);
    CurCP := nil;
  end;
  RVFWarnings := [];
  if AllowReplaceStyles and (GetDocProperties<>nil) then
    GetDocProperties.Clear;
  if AllowReplaceStyles then
    ClearExtraDocuments;
  FillChar(Header,sizeof(Header),0);
  Header.Version := 1;
  Header.SubVersion := 0;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  Header.StyleTemplates := ReadStyleTemplates;
  {$ENDIF} 
  InitStyleMappings(PTextStylesMapping, PParaStylesMapping, PListStylesMapping,
    Header.PUnits);
  try
    Size := Stream.Size - Stream.Position;
    CallProgress := IsAssignedOnProgress and (Size>QUICKRVFSIZE) and
      not (rvstLoadingAsSubDoc in State);
    if CallProgress then begin
      DoProgress(rvloRVFRead, rvpstgStarting, 0);
      Progress := 0;
    end;
    SetString(BufferString, nil, Size);
    Stream.Read(Pointer(BufferString)^, Size);
    StartP := Pointer(BufferString);
    P := StartP;
    EndP := StartP+Size;
    ReadState := rstHeader;
    ReadMode := rmText;
    if P <> nil then
      while P < EndP do begin
        ReadCurrentLine;
        case ReadState of
          rstHeader:
            begin
              if not RVFReadHeader(Self, CurrentLine, Header, AParaNo,
                Color, Background, PTextStylesMapping^, PParaStylesMapping^,
                AllowReplaceStyles) then
                abort; {error}
              if (Header.DataCount=0) then begin
                if not InsertRVFHeaderData(Self, '', Header, PrevCP, CurCP,
                  Index, InsertPoint, FirstInsert, AParaNo, AppendMode, EditFlag,
                  NonFirstItemsAdded, FullReformat, PTextStylesMapping^,
                  PListStylesMapping^) then
                  abort; {error}
                ReadState := rstHeader;
                end
              else
                if ((Header.Item=nil) and (Header.StyleNo<>rvsCheckpoint) and
                    (Header.StyleNo<>rvsBack)
                    {$IFDEF RICHVIEWCBDEF3}
                    and (Header.StyleNo<>rvsDocProperty)
                    {$ENDIF}
                    ) or
                   ((Header.Item<>nil) and
                    not Header.Item.GetBoolValue(rvbpRequiresRVFLines)) then
                  ReadState := rstSkip
                else begin
                  ReadState := rstData;
                  {$IFNDEF RVDONOTUSEUNICODE}
                  if (Header.Item<>nil) and
                     (rvioUnicode in Header.Item.ItemOptions) and
                     (Header.ReadType<>RVF_SAVETYPE_HEXUNICODE) then
                    ReadMode := rmBeforeUnicode;
                  {$ENDIF}
                end;
            end;
          rstData:
            begin
              if Header.StyleNo<0 then begin
                case Header.StyleNo of
                {*} rvsBack:
                   ReadBackground;
                {$IFDEF RICHVIEWCBDEF3}
                {*} rvsDocProperty:
                   ReadDocProperty;
                {$ENDIF}
                {*} rvsCheckpoint:
                  begin
                    if CurCP = nil then begin
                      CurCP := TRVCPInfo.Create;
                      CurCP.Name := RVFStringToString(CurrentLine, Header.UTF8Strings);
                      CurCP.Tag  := Header.CheckpointTag;
                      CurCP.RaiseEvent := Boolean(Header.RaiseEvent);
                      CurCP.Persistent := Boolean(Header.PersistentCheckpoint);
                      Header.CheckpointTag := RVEMPTYTAG;
                    end;
                  end;
                {*} else
                  begin
                    if Header.Item<>nil then
                      if not Header.Item.ReadRVFLine(CurrentLine, Self,
                         Header.ReadType, Header.DataRead, Header.DataCount,
                         Header.Name, ReadMode, ReadState, Header.UTF8Strings,
                         Header.AssociatedTextStyleNameUsed) then
                        abort;
                    if Header.DataRead=Header.DataCount-1 then
                      if not InsertRVFHeaderData(Self, Header.Name, Header,
                         PrevCP, CurCP, Index, InsertPoint, FirstInsert, AParaNo,
                         AppendMode, EditFlag, NonFirstItemsAdded, FullReformat,
                         PTextStylesMapping^, PListStylesMapping^) then
                        abort; {error}
                  end
                end
                end
              else begin
                if not InsertRVFHeaderData(Self, CurrentLine, Header, PrevCP, CurCP,
                   Index, InsertPoint, FirstInsert, AParaNo, AppendMode, EditFlag,
                   NonFirstItemsAdded, FullReformat, PTextStylesMapping^,
                   PListStylesMapping^) then
                  abort; {error}
                if Header.DataRead=Header.DataCount-1 then begin
                  {$IFDEF RVOLDTAGS}
                  if rvoTagsArePChars in Options then
                    StrDispose(PChar(Header.Item.Tag));
                  {$ENDIF}
                  RVFreeAndNil(Header.Item);
                end;
              end;
              inc(Header.DataRead);
              if Header.DataRead=Header.DataCount then begin
                ReadState := rstHeader;
                if ReadMode=rmUnicode then
                  ReadMode := rmAfterUnicode;
              end;
            end;
          rstSkip:
            begin
              inc(Header.DataRead);
              if (Header.DataRead=Header.DataCount-1) and
                (Header.ReadType=RVF_SAVETYPE_BINARY) then
                ReadMode := rmBeforeBinary
              else if Header.DataRead=Header.DataCount then begin
                if not InsertRVFHeaderData(Self, Header.Name, Header, PrevCP, CurCP,
                   Index, InsertPoint, FirstInsert, AParaNo, AppendMode, EditFlag,
                   NonFirstItemsAdded, FullReformat, PTextStylesMapping^,
                   PListStylesMapping^) then
                  abort; {error}
                ReadState := rstHeader;
              end;
            end;
        end;
        SkipCurrentLineTail;
      end; // of while
    Result := (ReadState = rstHeader);
    {$IFNDEF RVDONOTUSELISTS}
    if (InsertPoint-1>=0) and (InsertPoint-1<ItemCount) and
       (GetItemStyle(InsertPoint-1)=rvsListMarker) and
       ((InsertPoint=ItemCount) or IsParaStart(InsertPoint)) then begin
      Header.StyleNo := 0;
      Header.ParaNo := -1;
      Header.Item := RichViewTextItemClass.Create(Self);
      InsertRVFHeaderData(Self, '', Header, PrevCP, CurCP, Index, InsertPoint,
        FirstInsert, AParaNo, AppendMode, EditFlag,
        NonFirstItemsAdded, FullReformat, PTextStylesMapping^, PListStylesMapping^);
      RVFreeAndNil(Header.Item);
    end;
    {$ENDIF}
    if not EditFlag then
      NormalizeParas(StartIndex);
    Protect := False;
  except
    Result := False;
  end;

  DoneStyleMappings(LoadAsSubDoc);
  FreeCheckpointTag;
  if Result and (InsertPoint=Items.Count) and (NotAddedCP=nil) then begin
    if CurCP<> nil then inc(CPCount);
    NotAddedCP := CurCP
    end
  else
    if NotAddedCP<>CurCP then
      FreeCheckpoint(CurCP, False, False); // ignore cp from stream
  RVFreeAndNil(Header.Item);
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if UseStyleTemplates and (Header.StyleTemplates<>nil) and
    (StyleTemplateInsertMode<>rvstimIgnoreSourceStyleTemplates) and
     not (rvstLoadingAsSubDoc in State) then
    DoStyleTemplatesChange;
  if Header.StyleTemplates<>ReadStyleTemplates then
    Header.StyleTemplates.Free;
  {$ENDIF}
  if CallProgress then
    DoProgress(rvloRVFRead, rvpstgEnding, 0);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.AppendRVFFromStream(Stream: TStream; ParaNo: Integer;
                             var Color: TColor;
                             Background: TRVBackground):Boolean;
var Dummy: Integer;
    Dummy2, Dummy3: Boolean;
    Index: Integer;
begin
  Index := Items.Count;
  Result := InsertRVFFromStream_(Stream, Index, ParaNo, False, True, False,
    Color, Background, nil, Dummy, Dummy2, Dummy3, False
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}, nil{$ENDIF});
end;
{------------------------------------------------------------------------------}
function TCustomRVData.InsertRVFFromStream(Stream: TStream; Index: Integer;
  var Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo;
  AllowReplaceStyles: Boolean
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; ReadStyleTemplates: TRVStyleTemplateCollection
  {$ENDIF}):Boolean;
var Dummy: Integer;
    Dummy2,Dummy3: Boolean;
begin
  // AParaNo is used only if AppendMode=True
  Result := InsertRVFFromStream_(Stream, Index, -1, AllowReplaceStyles, False,
    False, Color, Background, Layout, Dummy, Dummy2, Dummy3, False
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}, ReadStyleTemplates{$ENDIF});
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadRVF(const FileName: String; var Color: TColor;
  Background: TRVBackground; Layout: TRVLayoutInfo):Boolean;
var Stream: TFileStream;
begin
  try
    Stream := TFileStream.Create(FileName,fmOpenRead or fmShareDenyWrite);
    try
      Result := LoadRVFFromStream(Stream, Color, Background, Layout
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}, nil{$ENDIF});
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadRVFFromStream(Stream: TStream; var Color: TColor;
  Background: TRVBackground; Layout: TRVLayoutInfo
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  ; ReadStyleTemplates: TRVStyleTemplateCollection
  {$ENDIF}):Boolean;
begin
  Clear;
  Result := InsertRVFFromStream(Stream,0, Color, Background, Layout, True
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}, ReadStyleTemplates{$ENDIF});
end;
{------------------------------------------------------------------------------}
procedure RVFWriteCheckpoint(Stream: TStream; TagsArePChars: Boolean;
  cp: TRVCPInfo);
begin
  if cp=nil then
    exit;
  RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %s %d %d',
    [rvsCheckpoint, 1, 0, 0, 0, RVFSaveTag(TagsArePChars,cp.Tag),
     Integer(cp.RaiseEvent), Integer(cp.Persistent)]));
  RVFWriteLine(Stream, StringToRVFString(cp.Name));
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveRVF(const FileName: String; SelectionOnly: Boolean;
  Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo;
  Root: Boolean):Boolean;
var Stream: TFileStream;
begin
  try
    Stream := TFileStream.Create(FileName,fmCreate);
    try
      Result := SaveRVFToStream(Stream, SelectionOnly, Color, Background,
        Layout, Root);
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveRVFToStream(Stream: TStream; SelectionOnly: Boolean;
  Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo;
  Root: Boolean):Boolean;
begin
  Result := SaveRVFToStreamEx(Stream, GetRVFSaveScope(SelectionOnly), Color,
    Background, Layout, Root, nil);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveRVFToStreamEx(Stream: TStream; SaveScope: TRVFSaveScope;
  Color: TColor; Background: TRVBackground; Layout: TRVLayoutInfo; Root: Boolean;
  sad: PRVScreenAndDevice):Boolean;
var i: Integer;
    Header: TRVFHeader;
    SectionBackOffs: Integer;
    StartItem, EndItem, StartOffs, EndOffs: Integer;
    StartPart, EndPart: TRVMultiDrawItemPart;
    MarkerItemNo: Integer;
    SelectedItem: TCustomRVItemInfo;
    CallProgress: Boolean;
    Progress: Integer;
   {.......................................................}
     procedure RVFSaveVersionInfo;
     begin
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d%s',
         [rvsVersionInfo, RVFVersion, RVFSubVersion, RVFSubSubVersion]));
     end;
   {.......................................................}
   {$IFDEF RICHVIEWCBDEF3}

     {...................................................................}
     procedure RVFSaveStyles(Id: Integer; Styles: TCollection);
     var SaveType: Integer;
         Writer: TWriter;
         TmpStream: TRVMemoryStream;
         Pos,Pos2: Integer;
         {$IFNDEF RVDONOTUSESTYLETEMPLATES}
         IgnoreStyleTemplates: Boolean;
         {$ENDIF}
     begin
       if rvfoSaveBinary in RVFOptions then
         SaveType := RVF_SAVETYPE_BINARY
       else
         SaveType := RVF_SAVETYPE_TEXT;
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
          [rvsDocProperty, 2, 0, 0, SaveType, 0, Id]));
       RVFWriteLine(Stream, TRVAnsiString(GetRVStyle.Name));
       {$IFNDEF RVDONOTUSESTYLETEMPLATES}
       IgnoreStyleTemplates := GetRVStyle.IgnoreStyleTemplates;
       GetRVStyle.IgnoreStyleTemplates := not (rvfoSaveTextStyles in RVFOptions) or
         not (rvfoSaveParaStyles in RVFOptions)
         {$IFNDEF RVFALWAYSSTORESTYLETEMPLATES}
         or not UseStyleTemplates
         {$ENDIF}
         ;
       {$ENDIF}
       try
         if rvfoSaveBinary in RVFOptions then begin
           Pos := Stream.Position;
           Stream.WriteBuffer(Pos, sizeof(Pos));
           Writer := TWriter.Create(Stream, 4096);
           {$IFNDEF RVDONOTUSELISTS}
           if Styles is TRVListInfos then
             TRVListInfos(Styles).FRVData := Self;
           {$ENDIF}
           try
             Writer.WriteCollection(Styles)
           finally
             Writer.Free;
             {$IFNDEF RVDONOTUSELISTS}
             if Styles is TRVListInfos then
               TRVListInfos(Styles).FRVData := nil;
             {$ENDIF}
           end;
           Pos2 := Stream.Position;
           Stream.Position := Pos;
           Pos := Pos2-Pos-sizeof(Pos);
           Stream.WriteBuffer(Pos, sizeof(Pos));
           Stream.Position := Pos2;
           end
         else begin
           TmpStream := TRVMemoryStream.Create;
           try
             Writer := TWriter.Create(TmpStream, 4096);
             {$IFNDEF RVDONOTUSELISTS}
             if Styles is TRVListInfos then
               TRVListInfos(Styles).FRVData := Self;
             {$ENDIF}
             try
               Writer.WriteCollection(Styles);
             finally
               Writer.Free;
               {$IFNDEF RVDONOTUSELISTS}
               if Styles is TRVListInfos then
                 TRVListInfos(Styles).FRVData := nil;
               {$ENDIF}
             end;
             TmpStream.Position := 0;
             RVFWriteLine(Stream, RVFStream2TextString(TmpStream));
          finally
            TmpStream.Free;
           end;
         end;
       finally
         {$IFNDEF RVDONOTUSESTYLETEMPLATES}
         GetRVStyle.IgnoreStyleTemplates := IgnoreStyleTemplates;
         {$ENDIF}
       end;
     end;
   {$ENDIF}
   {.......................................................}
   {
   function MakeTemporalLayout: TRVLayoutInfo;
   begin
     Result := nil;
     if not (rvstFirstParaAborted in State) and
        not (rvstLastParaAborted in State) then
       exit;
     Result := TRVLayoutInfo.Create;
     Result.FirstParaAborted  := ord(rvstFirstParaAborted in State);
     Result.LastParaAborted   := ord(rvstLastParaAborted in State);
     Result.FirstMarkerListNo := FFirstParaListNo;
     Result.FirstMarkerLevel  := FFirstParaLevel;
   end;
   }
   {.......................................................}
     procedure RVFSaveLayout;
     var SaveType : Integer;
     begin
       if rvfoSaveBinary in RVFOptions then
         SaveType := RVF_SAVETYPE_BINARY
       else
         SaveType := RVF_SAVETYPE_TEXT;
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
          [rvsDocProperty, 2, 0, 0, SaveType, 0, RVF_DOCPROP_LAYOUT]));
       RVFWriteLine(Stream, '');
       case SaveType of
         2:
           Layout.SaveToStream(Stream,True, Self<>GetRootData);
         0:
           Layout.SaveTextToStream(Stream, Self<>GetRootData);
       end;
     end;
   {.......................................................}
     procedure RVFSaveUnits;
     begin
       if GetRVStyle.Units<>rvstuPixels then begin
         RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
            [rvsDocProperty, 1, 0, 0, RVF_SAVETYPE_TEXT, 0, RVF_DOCPROP_UNITS]));
         RVFWriteLine(Stream, '1'); // ord(rvstuTwips)
       end;
     end;
   {.......................................................}
     procedure RVFSaveSubDocuments;
     var SaveType, i, v: Integer;
         DocStream: TRVMemoryStream;
         ExtraDocuments: TStrings;
     begin
       ExtraDocuments := GetExtraDocuments;
       if ExtraDocuments=nil then
         exit;
       if rvfoSaveBinary in RVFOptions then
         SaveType := RVF_SAVETYPE_BINARY
       else
         SaveType := RVF_SAVETYPE_TEXT;
       for i := 0 to ExtraDocuments.Count-1 do begin
         RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
            [rvsDocProperty, 2, 0, 0, SaveType, 0, RVF_DOCPROP_EXTRADOCUMENT]));
         RVFWriteLine(Stream, StringToRVFString(ExtraDocuments[i]));
         DocStream := TRVMemoryStream.Create;
         try
           (ExtraDocuments.Objects[i] as TCustomRVData).SaveRVFToStream(DocStream,
             False, clNone, nil, nil, True);
           case SaveType of
           2:
             begin
               v := DocStream.Size;
               Stream.WriteBuffer(v, sizeof(v));
               Stream.WriteBuffer(DocStream.Memory^, v);
             end;
           0:
             RVFWriteLine(Stream, RVFStream2TextString(DocStream));
           end;
         finally
           DocStream.Free;
         end;
       end;
     end;
   {.......................................................}
   {$IFNDEF RVDONOTUSEDOCPARAMS}
     procedure RVFSaveDocParameters;
     var DocParameters: TRVDocParameters;
         Count: Integer;
     begin
       DocParameters := GetDocParameters(False);
       if DocParameters=nil then
         Count := 0
       else
         Count := DocParameters.GetRVFLineCount;
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
          [rvsDocProperty, Count, 0, 0, RVF_SAVETYPE_TEXT, 0, RVF_DOCPROP_DOCPARAMETERS]));
       if DocParameters<>nil then
         DocParameters.SaveToRVF(Stream);
     end;
   {$ENDIF}
   {.......................................................}
   {$IFNDEF RVDONOTUSELISTS}
     procedure RVFSavePrevMarkers(StartItemNo: Integer);
     var SaveType, MarkerIndex : Integer;
         Marker: TRVMarkerItemInfo;
     begin
       if StartItemNo=0 then
         exit;
       Marker := FindPreviousMarker(StartItemNo-1);
       if Marker=nil then
         exit;
       MarkerIndex := Marker.GetIndexInList(GetMarkers(False));
       if MarkerIndex<0 then
         exit;
       if rvfoSaveBinary in RVFOptions then
         SaveType := RVF_SAVETYPE_BINARY
       else
         SaveType := RVF_SAVETYPE_TEXT;
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
          [rvsDocProperty, 2, 0, 0, SaveType, 0, RVF_DOCPROP_PREVMARKERS]));
       RVFWriteLine(Stream, '');
       case SaveType of
         2:
           GetMarkers(False).SaveToStream(Stream, MarkerIndex+1, True);
         0:
           GetMarkers(False).SaveTextToStream(Stream, MarkerIndex+1);
       end;
     end;
   {$ENDIF}
   {.......................................................}
     procedure RVFSaveDocPropertiesStringList;
     var i: Integer;
         dp: TStringList;
     begin
       dp := GetAbsoluteRootData.GetDocProperties;
       if (dp=nil) or (dp.Count=0) then
         exit;
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d',
          [rvsDocProperty, dp.Count, 0, 0, RVF_SAVETYPE_TEXT, 0, RVF_DOCPROP_DOCPROPLIST]));
       for i := 0 to dp.Count-1 do
         RVFWriteLine(Stream, StringToRVFString(dp.Strings[i]));
     end;
   {.......................................................}
     procedure RVFSaveBackground;
     var SaveType, LineCount: Integer;
     begin
       if Background=nil then exit;
       if Background.Bitmap.Empty or (Background.Style=bsNoBitmap) then
         RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d %d',
                            [rvsBack, 0, -1, 0, 0, 0, ord(Background.Style), Color]))
       else begin
         LineCount := 2;
         if rvfoSaveBinary in RVFOptions then
           SaveType := RVF_SAVETYPE_BINARY
         else
           SaveType := RVF_SAVETYPE_TEXT;
         RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %d %d %d %d %d %d',
            [rvsBack, LineCount, 0, 0, SaveType, 0, ord(Background.Style), Color]));
         if SaveType<>1 then begin
           RVFWriteLine(Stream, TRVAnsiString(Background.Bitmap.ClassName));
           if rvfoSaveBinary in RVFOptions then
             RVFSavePictureBinary(Stream, Background.Bitmap)
           else
             RVFWriteLine(Stream, RVFSavePicture(Background.Bitmap));
         end;
       end;
     end;
   {.......................................................}
   { Saves text of the ItemNo-th item in range from StartOffs to EndOffs characters.
     If the ItemNo-th item is not a text item, saves an empty text line of style
     GetItem(ItemNo).AssociatedTextStyleNo (it's assumed that it's >=0).
     If StartOffs=GetOffsetBeforeItem(ItemNo) and the item has a checkpoint,
     it's saved too.
     If ForceSavingPara, item's paragraph index is saved, even if it does not
     start a new paragraph. }
    procedure WritePartialTextLine(ItemNo, StartOffs, EndOffs: Integer;
      ForceSavingPara: Boolean); // in: Stream
    var AFromStart: Boolean;
        AParaNo, TextStyleNo: Integer;
        SaveMode: Integer;
        Tail: TRVRawByteString;
        Text: TRVRawByteString;
        item: TCustomRVItemInfo;
        {$IFNDEF RVDONOTUSEUNICODE}
        Unicode: Boolean;
        {$ENDIF}
        ItemOptions: TRVItemOptions;
    begin
      AFromStart := (StartOffs <= GetOffsBeforeItem(ItemNo));
      item := GetItem(ItemNo);
      TextStyleNo := item.StyleNo;
      if TextStyleNo<0 then
        TextStyleNo := item.AssociatedTextStyleNo;
      {$IFNDEF RVDONOTUSEUNICODE}
      Unicode := GetRVStyle.TextStyles[TextStyleNo].Unicode;
      {$ENDIF}
      {$IFDEF RICHVIEWCBDEF3}
      {$IFNDEF RVDONOTUSEUNICODE}
      if Unicode and not (rvfoSaveBinary in RVFOptions) then
        SaveMode := RVF_SAVETYPE_HEXUNICODE
      else
      {$ENDIF}
        SaveMode := RVF_SAVETYPE_TEXT;
     {$ELSE}
     SaveMode := RVF_SAVETYPE_TEXT;
     {$ENDIF}
     if (AFromStart and not item.SameAsPrev) or ForceSavingPara then
       AParaNo := item.ParaNo
     else
       AParaNo   := -1;
     if AFromStart then
       RVFWriteCheckpoint(Stream, rvoTagsArePChars in Options, item.Checkpoint);
     Tail := '';
     {$IFNDEF RVDONOTUSEITEMHINTS}
     {$IFDEF RICHVIEWCBDEF3}
     if item.Hint<>'' then
       Tail := ' '+StringToRVFString(AnsiQuotedStr(item.Hint, '"'));
     {$ENDIF}
     {$ENDIF}
     ItemOptions := GetItemOptions(ItemNo);
     {$IFNDEF RVDONOTUSEUNICODE}
     if Unicode then
       Include(ItemOptions, rvioUnicode);
     {$ENDIF}
     RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%s %d %s %d %d %s%s',
       [RVFSaveText(GetRVStyle, rvfoUseStyleNames in RVFOptions, TextStyleNo), 1,
       RVFSavePara(GetRVStyle, rvfoUseStyleNames in RVFOptions, AParaNo),
       Byte(ItemOptions) and RVItemOptionsMask,
       SaveMode, RVFSaveTag(rvoTagsArePChars in Options, item.Tag),
       Tail]));
     if item.StyleNo>=0 then
       Text := RVU_Copy(Items[ItemNo], StartOffs, EndOffs-StartOffs, GetItemOptions(ItemNo))
     else
       Text := '';
     RVFWriteLineX(Stream, Text,
       {$IFNDEF RVDONOTUSEUNICODE}Unicode{$ELSE}False{$ENDIF}, SaveMode=3);
     MarkerItemNo := -1;
    end;
   {.......................................................}
    function IsTheSameStyleText: Boolean; // in: i, Header
    begin
      with GetItem(i) do
        Result := (not SameAsPrev) and (StyleNo>=0) and (StyleNo=Header.StyleNo) and
          (ParaNo=Header.ParaNo) and
          ((Byte(ItemOptions) and RVItemOptionsMask) = (Byte(Header.Item.ItemOptions)and RVItemOptionsMask)) and
          RV_CompareTags(Tag, Header.Item.Tag, rvoTagsArePChars in Options) and
          {$IFNDEF RVDONOTUSEITEMHINTS}
          (Hint=Header.Item.Hint) and
          {$ENDIF}
          (Checkpoint=nil);
    end;
  {.......................................................}
   procedure RVFWritePrevStrings(i: Integer); // in: Header, SectionBackOffs
   var j: Integer;
       ItemOptions: TRVItemOptions;
       SaveMode: Integer;
       Tail: TRVRawByteString;
   begin
     {$IFDEF RICHVIEWCBDEF3}
      if (rvfoSaveBinary in RVFOptions) or
         not (rvioUnicode in GetItemOptions(i-SectionBackOffs)) then
        SaveMode := RVF_SAVETYPE_TEXT
      else
        SaveMode := RVF_SAVETYPE_HEXUNICODE;
     {$ELSE}
     SaveMode := RVF_SAVETYPE_TEXT;
     {$ENDIF}
     RVFWriteCheckpoint(Stream, rvoTagsArePChars in Options, Header.Item.Checkpoint);
     if MarkerItemNo>=0 then begin
       ItemOptions := RVFGetItemOptions(Header.Item.ItemOptions, MarkerItemNo>=0);
       RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%s %d %s %d %d %s',
         [RVFSaveText(GetRVStyle, rvfoUseStyleNames in RVFOptions, Header.StyleNo),
         1,
         RVFSavePara(GetRVStyle, rvfoUseStyleNames in RVFOptions, -1),
         Byte(ItemOptions) and RVItemOptionsMask,
         SaveMode, RVFSaveTag(rvoTagsArePChars in Options, Header.Item.Tag)]));
       RVFWriteLineX(Stream, Items[i-SectionBackOffs],
         rvioUnicode in GetItemOptions(i-SectionBackOffs), SaveMode=3);
       dec(SectionBackOffs);
       MarkerItemNo := -1;
     end;
     if SectionBackOffs=0 then
       exit;
     Tail := '';
     {$IFNDEF RVDONOTUSEITEMHINTS}
     {$IFDEF RICHVIEWCBDEF3}
     if GetItem(i-SectionBackOffs).Hint<>'' then
       Tail := ' '+StringToRVFString(AnsiQuotedStr(GetItem(i-SectionBackOffs).Hint, '"'));
     {$ENDIF}
     {$ENDIF}
     RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%s %d %s %d %d %s%s',
       [RVFSaveText(GetRVStyle, rvfoUseStyleNames in RVFOPtions, Header.StyleNo),
       SectionBackOffs,
       RVFSavePara(GetRVStyle, rvfoUseStyleNames in RVFOPtions, Header.ParaNo),
       Byte(Header.Item.ItemOptions) and RVItemOptionsMask,
       SaveMode, RVFSaveTag(rvoTagsArePChars in Options, Header.Item.Tag),
       Tail]));
     for j := i-SectionBackOffs to i-1 do
       RVFWriteLineX(Stream, Items[j], rvioUnicode in GetItemOptions(j), SaveMode=3);
     SectionBackOffs := 0;
   end;
  {.......................................................}
   procedure RVFSetHeaderHeader(i: Integer); // in: Header
   begin
     with GetItem(i) do begin
       Header.Item.Checkpoint := Checkpoint;
       Header.Item.ItemOptions := ItemOptions;
       Header.StyleNo      := StyleNo;
       //if StyleNo>=0 then
         Header.Item.StyleNo := StyleNo;
       {$IFNDEF RVDONOTUSEITEMHINTS}
       Header.Item.Hint := Hint;
       {$ENDIF}
       if SameAsPrev and not ((i=StartItem) and (SaveScope in [rvfss_Page, rvfss_FromPage])) then
         Header.ParaNo   := -1
       else
         Header.ParaNo   := ParaNo;
       Header.Item.Tag     := Tag;
     end;
   end;
  {.......................................................}
   procedure RVFWriteNonText(i: Integer; Part: TRVMultiDrawItemPart); // in: Header
   {$IFNDEF RVDONOTUSELISTS}
   var StartFrom: Integer;
       Reset: Boolean;
       marker: TRVMarkerItemInfo;
   {$ENDIF}
   begin
     with GetItem(i) do begin
       RVFWriteCheckpoint(Stream, rvoTagsArePChars in Options, Checkpoint);
       {$IFNDEF RVDONOTUSELISTS}
       if StyleNo=rvsListMarker then begin
         marker := TRVMarkerItemInfo(GetItem(i));
         StartFrom := marker.StartFrom;
         Reset     := marker.Reset;
         if SaveScope in [rvfss_Page, rvfss_FromPage, rvfss_FullInPage] then begin
           marker.StartFrom := marker.Counter;
           marker.Reset     := True;
         end;
         end
       else begin
         StartFrom := 0;    // avoiding warnings
         Reset     := False;
         marker    := nil;
       end;
       {$ENDIF}
       SaveRVF(Stream, Self, i, Header.ParaNo, Items[i], Part, MarkerItemNo>=0, SaD);
       {$IFNDEF RVDONOTUSELISTS}
       if StyleNo=rvsListMarker then begin
         marker.StartFrom := StartFrom;
         marker.Reset     := Reset;
       end;
       {$ENDIF}
       MarkerItemNo := -1;
     end;
   end;
  {.......................................................}
  { Should the first selected item be saved as an empty text line? }
  function ShouldSaveEndOfNonTextItemAsEmptyText: Boolean;
  begin
    Result := (SaveScope=rvfss_Selection) and
      (GetItem(StartItem).AssociatedTextStyleNo>=0) and
      (StartOffs>=GetOffsAfterItem(StartItem)) and
      GetItem(StartItem).GetBoolValue(rvbpSwitchToAssStyleNo) and
      ((StartItem+1=ItemCount) or IsFromNewLine(StartItem+1));
  end;
  {.......................................................}
  { Should the last selected item be saved as an empty text line? }  
  function ShouldSaveBeginningOfNonTextItemAsEmptyText: Boolean;
  begin
    Result := (SaveScope=rvfss_Selection) and
      (GetItem(EndItem).AssociatedTextStyleNo>=0) and
      (EndOffs<=GetOffsBeforeItem(EndItem)) and
      GetItem(EndItem).GetBoolValue(rvbpSwitchToAssStyleNo) and
      IsFromNewLine(EndItem);
  end;
  {.......................................................}
begin
  Result := True;
  if (Items.Count=0) {or (SelectionOnly and not SelectionExists)} then
    exit;
  if SaveScope<>rvfss_Selection then
    DoBeforeSaving;
  FillChar(Header, sizeof(Header), 0);
  Header.Item := RichViewTextItemClass.Create(Self);
  CallProgress := False;
  try
    RVFSaveVersionInfo;
    if Root then
      RVFSaveUnits;    
    if (SaveScope<>rvfss_Selection) and (rvfoSaveBack in RVFOptions) then
      RVFSaveBackground;
    if Root then begin
      {$IFDEF RICHVIEWCBDEF3}
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      if (rvfoSaveTextStyles in RVFOptions) and (rvfoSaveParaStyles in RVFOptions) and
        UseStyleTemplates and
        ((GetRVStyle.MainRVStyle=nil) or GetRVStyle.SaveStyleTemplatesForNonMain) then begin
        RVFSaveStyles(RVF_DOCPROP_STYLETEMPLATES, GetRVStyle.StyleTemplates);
        GetRVStyle.UpdateModifiedProperties;
      end;
      {$ENDIF}
      if (rvfoSaveTextStyles in RVFOptions) then
        RVFSaveStyles(RVF_DOCPROP_TEXTSTYLES, GetRVStyle.TextStyles);
      if (rvfoSaveParaStyles in RVFOptions) and (Self=GetRootData) then begin
        RVFSaveStyles(RVF_DOCPROP_PARASTYLES, GetRVStyle.ParaStyles);
        {$IFNDEF RVDONOTUSELISTS}
        RVFSaveStyles(RVF_DOCPROP_LISTSTYLES, GetRVStyle.ListStyles);
        {$ENDIF}
      end;
      {$ENDIF}
      if (rvfoSaveDocProperties in RVFOptions) and
         not (SaveScope in [rvfss_Page, rvfss_FromPage]) then begin
        RVFSaveDocPropertiesStringList;
        {$IFNDEF RVDONOTUSEDOCPARAMS}
        RVFSaveDocParameters;
        {$ENDIF}
      end;
    end;
    if (SaveScope in [rvfss_Page, rvfss_FromPage]) and (Layout<>nil) then begin
      RVFGetLimits(SaveScope,StartItem,EndItem,StartOffs,EndOffs,StartPart,EndPart, SelectedItem);
      if (StartItem>=0) and (StartItem<=EndItem) then begin
        if (StartOffs>GetOffsBeforeItem(StartItem)) or
           ((StartOffs<=GetOffsBeforeItem(StartItem)) and not IsParaStart(StartItem)) then begin
          Layout.FirstParaAborted := 1;
          {$IFNDEF RVDONOTUSELISTS}
          MarkerItemNo := GetFirstParaItem(StartItem);
          if (MarkerItemNo<>StartItem) and (GetItemStyle(MarkerItemNo)=rvsListMarker) then begin
            Layout.FirstMarkerListNo := TRVMarkerItemInfo(GetItem(MarkerItemNo)).ListNo;
            Layout.FirstMarkerLevel := TRVMarkerItemInfo(GetItem(MarkerItemNo)).Level;
          end;
          {$ENDIF}
        end;
        if (EndOffs<GetOffsAfterItem(EndItem)) or
           ((EndOffs>=GetOffsAfterItem(EndItem)) and not ((EndItem+1=ItemCount) or (IsParaStart(EndItem+1)))) then
          Layout.LastParaAborted := 1;
        {$IFNDEF RVDONOTUSELISTS}
        if (Self=GetRootData) then
          RVFSavePrevMarkers(StartItem);
        {$ENDIF}
      end;
    end;
    if (SaveScope<>rvfss_Selection) then
      RVFSaveSubDocuments;
    if (SaveScope<>rvfss_Selection) and (rvfoSaveLayout in RVFOptions) then begin
      if Layout<>nil then
        RVFSaveLayout
      {
      else begin
        Layout := MakeTemporalLayout;
        if Layout<>nil then
          RVFSaveLayout;
        Layout.Free;
        Layout := nil;
      end;
      }
    end;

    {$IFNDEF RVDONOTUSEINPLACE}
    if (SaveScope=rvfss_Selection) and (GetChosenRVData<>nil) then begin
      Result := GetChosenRVData.SaveRVFToStreamEx(Stream, SaveScope,
        clNone, nil, nil, False, sad);
      Header.Item.Free;
      exit;
    end;
    {$ENDIF}
    RVFGetLimits(SaveScope,StartItem, EndItem, StartOffs, EndOffs,
      StartPart, EndPart, SelectedItem);
    if SelectedItem<>nil then begin
      if RichViewAllowCopyTableCells then
        SelectedItem.SaveRVFSelection(Stream, Self, -1, SelectedItem.ParaNo);
      Header.Item.Free;
      exit;
    end;
    if (StartItem=-1) or (StartItem>EndItem) then begin
      Header.Item.Free;
      exit;
    end;
    if (StartItem=EndItem) and
       ((StartOffs>GetOffsBeforeItem(StartItem)) or
        (EndOffs  <GetOffsAfterItem(EndItem))) then begin
      // only part of text line is selected
      WritePartialTextLine(StartItem, StartOffs, EndOffs,
        SaveScope in [rvfss_Page, rvfss_FromPage]);
      Header.Item.Free;
      exit;
    end;
    CallProgress := Root and SaveProgressInit(Progress, EndItem-StartItem,
      rvloRVFWrite);
    SectionBackOffs := 0;
    if (StartPart<>nil) then begin
      Header.ParaNo := GetItem(StartItem).ParaNo;
      RVFWriteNonText(StartItem, StartPart);
      inc(StartItem);
      if StartItem<ItemCount then
        StartOffs := GetOffsBeforeItem(StartItem);
    end;
    MarkerItemNo := -1;
    {$IFNDEF RVDONOTUSELISTS}
    if (SaveScope=rvfss_Selection) and (StartPart=nil) then begin
      MarkerItemNo := GetFirstParaItem(StartItem);
      if (MarkerItemNo<>StartItem) and (GetItemStyle(MarkerItemNo)=rvsListMarker) then begin
        Header.ParaNo := GetItem(MarkerItemNo).ParaNo;
        RVFWriteNonText(MarkerItemNo, nil);
        MarkerItemNo := GetFirstParaItem(StartItem);
        end
      else
        MarkerItemNo := -1;
    end;
    {$ENDIF}
    if (EndPart<>nil) then begin
      dec(EndItem);
      if EndItem>=0 then
        EndOffs := GetOffsAfterItem(EndItem);
    end;
    for i := StartItem to EndItem do begin
      if (i=StartItem) then begin
        if ((GetItemStyle(i)>=0) or ShouldSaveEndOfNonTextItemAsEmptyText()) and
         ((StartOffs>GetOffsBeforeItem(i)) or (SaveScope in [rvfss_Page, rvfss_FromPage])) then begin
          WritePartialTextLine(StartItem, StartOffs, GetOffsAfterItem(StartItem),
            SaveScope in [rvfss_Page, rvfss_FromPage]);
          continue;
        end;
        if (StartOffs>GetOffsBeforeItem(i)) then
          continue;
      end;
      if (i>StartItem) and IsTheSameStyleText then
        inc(SectionBackOffs)
      else begin
        if SectionBackOffs>0 then
          RVFWritePrevStrings(i);
        RVFSetHeaderHeader(i);
        if Header.StyleNo<0 then begin
          if (i<EndItem) or (EndOffs=1) then
            RVFWriteNonText(i, nil)
          end
        else
          SectionBackOffs := 1;
      end;
      if CallProgress then
        SaveProgressRun(Progress, i-StartItem, EndItem-StartItem, rvloRVFWrite);
    end;
    if (Header.StyleNo<0) and ShouldSaveBeginningOfNonTextItemAsEmptyText() then
      WritePartialTextLine(EndItem, GetOffsBeforeItem(EndItem), EndOffs, False)
    else if (Header.StyleNo>=0) and (EndOffs<GetOffsAfterItem(EndItem)) then begin
      dec(SectionBackOffs);
      if SectionBackOffs>0 then
        RVFWritePrevStrings(EndItem);
      WritePartialTextLine(EndItem, GetOffsBeforeItem(EndItem), EndOffs, False);
      end
    else begin
      if SectionBackOffs<>0 then
        RVFWritePrevStrings(EndItem+1);
      if (EndItem=Items.Count-1) and (EndOffs=1) then
        RVFWriteCheckpoint(Stream, rvoTagsArePChars in Options, NotAddedCP);
    end;
    if (EndPart<>nil) then begin
      RVFSetHeaderHeader(EndItem+1);
      RVFWriteNonText(EndItem+1, EndPart);
    end;
  except;
    Result := False;
  end;
  Header.Item.Free;
  if CallProgress then
    SaveProgressDone(rvloRVFWrite);
end;
{$ENDIF}{RVDONOTUSERVF}
{------------------------------------------------------------------------------}
function TCustomRVData.InsertFirstRVFItem(var Index: Integer;
  var s: TRVRawByteString; var item: TCustomRVItemInfo; EditFlag: Boolean;
  var FullReformat: Boolean;
  var NewListNo: Integer): Boolean;
begin
  FullReformat := False;
  NewListNo := -1;
  item.Inserting(Self, s, True);
  Items.InsertObject(Index, s, item);
  item.Inserted(Self, Index);
  {$IFNDEF RVDONOTUSESEQ}
  AddSeqInList(Index);
  {$ENDIF}  
  {$IFNDEF RVDONOTUSELISTS}
  AddMarkerInList(Index);
  {$ENDIF}
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.UpdateItemsPaletteInfo;
var i: Integer;
begin
  if not ShareItems then
    for i := 0 to Items.Count-1 do
      GetItem(i).UpdatePaletteInfo(GetDoInPaletteMode, False,
        GetRVPalette, GetRVLogPalette);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetThumbnailCache: TRVThumbnailCache;
begin
  Result := GetAbsoluteRootData.GetThumbnailCache;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.FreeBackgroundThumbnail;
begin

end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.InsertCheckpoint(ItemNo: Integer; const Tag: TRVTag;
                             const Name: String; RaiseEvent: Boolean);
var
  cp: TRVCPInfo;
begin
  if GetItem(ItemNo).Checkpoint<>nil then
    raise ERichViewError.Create(errRVCPExists);
  cp            := TRVCPInfo.Create;
  cp.Tag        := Tag;
  cp.Name       := Name;
  cp.RaiseEvent := RaiseEvent;
  cp.ItemInfo   := GetItem(ItemNo);
  cp.Next := nil;
  cp.Prev := nil;
  GetItem(ItemNo).Checkpoint := cp;
  inc(CPCount);
  UpdateCPPos(cp, ItemNo);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.UpdateCPPos(cp: TRVCPInfo; ItemNo: Integer);
var cpi: TRVCPInfo;
begin
  if ItemNo=-1 then exit;
  UpdateCPItemNo;
  cp.Prev := nil;
  cp.Next := nil;
  if FirstCP = nil then begin
    FirstCP := cp;
    LastCP  := cp;
    end
  else if FirstCP.ItemNo>cp.ItemNo then begin
    cp.Next := FirstCP;
    FirstCP.Prev := cp;
    FirstCP      := cp;
    end
  else if LastCP.ItemNo<=cp.ItemNo then begin
    LastCP.Next := cp;
    cp.Prev := LastCP;
    LastCP := cp
    end
  else begin
    cpi := FirstCP;
    while cpi.Next<>nil do begin
      if cpi.Next.ItemNo>cp.ItemNo then break;
      cpi := cpi.Next;
    end;
    if cpi.Next<>nil then cpi.Next.Prev := cp;
    cp.Next := cpi.Next;
    cpi.Next := cp;
    cp.Prev := cpi;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ShareItemsFrom(Source: TCustomRVData);
begin
  if ShareItems then begin
    Clear;
    FItems := Source.Items;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AssignItemsFrom(Source: TCustomRVData);
begin
  FItems := Source.Items;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AppendFrom(Source: TCustomRVData);
var i: Integer;
    item,itemcopy: TCustomRVItemInfo;
begin
  {$IFDEF RVOLDTAGS}
  if (rvoTagsArePChars in Options) <> (rvoTagsArePChars in Source.Options) then
    raise ERichViewError.Create(errRVTagsTypesMismatch);
  {$ENDIF}
  for i:=0 to Source.Items.Count-1 do begin
    item := Source.GetItem(i);
    itemcopy := RV_DuplicateItem(item, Self, True);
    if itemcopy.GetBoolValue(rvbpValid) then begin
      if itemcopy.SameAsPrev then
        itemcopy.ParaNo := -1;
      AddItemR(Source.Items[i],itemcopy, True);
      {$IFNDEF RVDONOTUSESEQ}
      AddSeqInList(ItemCount-1);
      {$ENDIF}
      {$IFNDEF RVDONOTUSELISTS}
      AddMarkerInList(ItemCount-1);
      {$ENDIF}
      end
    else
      InternalFreeItem(itemcopy,False);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.UpdateCPItemNo;
var i,cnt: Integer;
begin
  cnt := 0;
  if cnt=CPCount then exit;
  for i := 0 to Items.Count-1 do
    if GetItem(i).Checkpoint<>nil then begin
      GetItem(i).Checkpoint.ItemNo := i;
      inc(cnt);
      if cnt=CPCount then
        exit;
    end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsDelimiterA(ch: TRVAnsiChar; CodePage: TRVCodePage): Boolean;
var Del: String;
begin
  if ch=#9 then begin
    Result := True;
    exit;
  end;
  Del := GetDelimiters;
  {$IFDEF RVUNICODESTR}
  Result := Pos(RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(CodePage, ch)), Del)<>0;
  {$ELSE}
  Result := RV_CharPos(PRVAnsiChar(Del), ch, Length(Del))<>0;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsDelimiterW(ch: TRVUnicodeChar): Boolean;
var Del: String;
begin
  if ch=#9 then begin
    Result := True;
    exit;
  end;
  Del := GetDelimiters;
  {$IFDEF RVUNICODESTR}
  Result := Pos(ch, Del)<>0;
  {$ELSE}
  Result := (ord(ch)<256) and (Pos(Char(ch), Del)<>0);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsDelimiter(const s: TRVRawByteString; Index: Integer;
  ItemOptions: TRVItemOptions; CodePage: TRVCodePage): Boolean;
begin
  {$IFNDEF RVDONOTUSEUNICODE}
  if rvioUnicode in ItemOptions then
    Result := IsDelimiterW(TRVUnicodeChar(PWord(PRVAnsiChar(s)+(Index-1)*2)^))
  else
  {$ENDIF}
    Result := IsDelimiterA(s[Index], CodePage);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemOptions(ItemNo: Integer): TRVItemOptions;
begin
  Result := GetItem(ItemNo).ItemOptions;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetStyleCodePage(StyleNo: Integer): TRVCodePage;
begin
  {$IFDEF RICHVIEWCBDEF3}
  if (GetRVStyle<>nil) then
    if (StyleNo>=0) and (GetRVStyle.TextStyles[StyleNo].Charset<>DEFAULT_CHARSET) then
      Result := RVU_Charset2CodePage(GetRVStyle.TextStyles[StyleNo].Charset)
    else
      Result := GetRVStyle.DefCodePage
  else
  {$ENDIF}
    Result := CP_ACP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemCodePage(ItemNo: Integer): TRVCodePage;
begin
  Result := GetStyleCodePage(GetActualStyle(GetItem(ItemNo)))
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsSymbolCharset(ItemNo: Integer): Boolean;
var StyleNo: Integer;
begin
  StyleNo := GetActualStyle(GetItem(ItemNo));
  if StyleNo<0 then
    Result := False
  else
    Result := GetRVStyle.TextStyles[StyleNo].IsSymbolCharset;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemCodePage2(Item: TCustomRVItemInfo): TRVCodePage;
begin
  Result := GetStyleCodePage(GetActualStyle(Item));
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetStyleLocale(StyleNo: Integer): Cardinal;
begin
  {$IFDEF RICHVIEWCBDEF3}
  if (GetRVStyle<>nil) and (StyleNo>=0) then
    Result := RVMAKELCID(RVU_Charset2Language(GetRVStyle.TextStyles[StyleNo].Charset))
  else
  {$ENDIF}
    Result := RVMAKELCID(LANG_NEUTRAL);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetDefaultCodePage: TRVCodePage;
begin
  {$IFNDEF RVDONOTUSEUNICODE}
  if (GetRVStyle<>nil) then
    Result := GetRVStyle.DefCodePage
  else
  {$ENDIF}
    Result := CP_ACP;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetDoInPaletteMode: TRVPaletteAction;
begin
  Result := GetRootData.GetDoInPaletteMode;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetOptions: TRVOptions;
begin
  Result := GetRootData.GetOptions;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetOptions(const Value: TRVOptions);
begin
  GetRootData.SetOptions(Value);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetDocProperties: TStringList;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVLogPalette: PLogPalette;
begin
  Result := GetRootData.GetRVLogPalette;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVPalette: HPALETTE;
begin
  Result := GetRootData.GetRVPalette;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetDelimiters: String;
begin
  Result := GetRootData.GetDelimiters;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVFParaStylesReadMode: TRVFReaderStyleMode;
begin
  Result := GetRootData.GetRVFParaStylesReadMode;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVFTextStylesReadMode: TRVFReaderStyleMode;
begin
  Result := GetRootData.GetRVFTextStylesReadMode;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.RVFPictureNeeded(const ItemName: String;
  Item: TRVNonTextItemInfo; Index1, Index2: Integer;
  PictureInsideRVF: Boolean): TGraphic;
begin
  Result := GetRootData.RVFPictureNeeded(ItemName, Item, Index1, Index2,
    PictureInsideRVF);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveComponentToFile(const Path: String;
  SaveMe: TComponent; SaveFormat: TRVSaveFormat): String;
begin
  Result := GetAbsoluteRootData.SaveComponentToFile(Path, SaveMe, SaveFormat);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure TCustomRVData.DoStyleTemplatesChange;
begin
  GetAbsoluteRootData.DoStyleTemplatesChange;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.SaveItemToFile(const Path: String;
  RVData: TCustomRVData; ItemNo: Integer; SaveFormat: TRVSaveFormat;
  Unicode: Boolean; var Text: TRVRawByteString): Boolean;
begin
  Result := GetAbsoluteRootData.SaveItemToFile(Path, RVData, ItemNo, SaveFormat,
    Unicode, Text);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.ImportPicture(const Location: String; Width,
  Height: Integer; var Invalid: Boolean): TGraphic;
begin
  Result := GetAbsoluteRootData.ImportPicture(Location, Width, Height, Invalid);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemHint(RVData: TCustomRVData; ItemNo: Integer;
  const UpperRVDataHint: String): String;
begin
  Result := GetAbsoluteParentData.GetItemHint(RVData, ItemNo, UpperRVDataHint);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.RVFControlNeeded(const ItemName: String;
  const ItemTag: TRVTag): TControl;
begin
  Result := GetRootData.RVFControlNeeded(ItemName, ItemTag);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.RVFImageListNeeded(ImageListTag: Integer): TCustomImageList;
begin
  Result := GetRootData.RVFImageListNeeded(ImageListTag);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.HTMLSaveImage(RVData: TCustomRVData;
  ItemNo: Integer; const Path: String; BackgroundColor: TColor;
  var Location: String; var DoDefault: Boolean);
begin
  GetAbsoluteRootData.HTMLSaveImage(RVData, ItemNo, Path, BackgroundColor,
    Location, DoDefault);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveImage2(RVData: TCustomRVData; ItemNo: Integer;
  Graphic: TGraphic; SaveFormat: TRVSaveFormat; const Path, ImagePrefix: String;
  var ImageSaveNo: Integer; var Location: String; var DoDefault: Boolean);
begin
  GetAbsoluteRootData.SaveImage2(RVData, ItemNo, Graphic, SaveFormat, Path,
    ImagePrefix, ImageSaveNo, Location, DoDefault);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVStyle: TRVStyle;
begin
  Result := GetParentData.GetRVStyle;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetParentControl: TWinControl;
begin
  Result := GetRootData.GetParentControl;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ReadHyperlink(const Target, Extras: String;
  DocFormat: TRVLoadFormat; var StyleNo: Integer; var ItemTag: TRVTag;
  var ItemName: TRVRawByteString);
begin
  GetRootData.ReadHyperlink(Target, Extras, DocFormat, StyleNo, ItemTag, ItemName);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.WriteHyperlink(id: Integer; RVData: TCustomRVData;
  ItemNo: Integer; SaveFormat: TRVSaveFormat; var Target, Extras: String);
begin
  GetAbsoluteRootData.WriteHyperlink(id, RVData, ItemNo, SaveFormat,
    Target, Extras);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ControlAction(RVData: TCustomRVData;
  ControlAction: TRVControlAction; ItemNo: Integer; Item: TCustomRVItemInfo);
begin
  if (item is TRVControlItemInfo) then
    GetAbsoluteRootData.ControlAction2(RVData, ControlAction, ItemNo,
      TRVControlItemInfo(item).Control);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ItemAction(ItemAction: TRVItemAction;
  Item: TCustomRVItemInfo; var Text: TRVRawByteString; RVData: TCustomRVData);
begin

end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetScaleRichViewInterface: IRVScaleRichViewInterface;
begin
  Result := GetAbsoluteRootData.GetScaleRichViewInterface;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
procedure TCustomRVData.AfterReadNote(RVData: TCustomRVData; ItemNo: Integer);
var Ifc: IRVScaleRichViewInterface;
begin
  Ifc := GetScaleRichViewInterface;
  if Ifc<>nil then
    Ifc.DoAfterReadNote(RVData, ItemNo);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoBeforeSaving;
var Ifc: IRVScaleRichViewInterface;
begin
  if (rvflRoot in Flags) then begin
    Ifc := GetScaleRichViewInterface;
    if Ifc<>nil then
      Ifc.DoBeforeSaving(GetParentControl);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.Replace0(var s: TRVRawByteString);
var p: Integer;
begin
  while True do begin
    p := RVPos(#0,s);
    if p=0 then break;
    s[p] := RVDEFAULTCHARACTER;
  end;
end;
{------------------------------- RTF ------------------------------------------}
procedure TCustomRVData.SetRTFOptions(const Value: TRVRTFOptions);
begin
  GetRootData.SetRTFOptions(Value);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRTFOptions: TRVRTFOptions;
begin
  Result := GetRootData.GetRTFOptions;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.MakeFontTable(RTFTables: TCustomRVRTFTables;
  StyleToFont: TRVIntegerList
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}; var StyleTemplateToFont: TRVIntegerList{$ENDIF});
var i {$IFNDEF RVDONOTUSELISTS},j{$ENDIF}: Integer;
   Index: Integer;
   RVStyle: TRVStyle;
begin
 RVStyle := GetRVStyle;
 RTFTables.ClearFonts;
 if StyleToFont<>nil then
   StyleToFont.Clear;
 for i := 0 to RVStyle.TextStyles.Count-1 do begin
   Index := RTFTables.AddFontFromStyle(RVStyle.TextStyles[i]);
   if StyleToFont<>nil then
     StyleToFont.Add(Index);
 end;
 {$IFNDEF RVDONOTUSESTYLETEMPLATES}
 if UseStyleTemplates then begin
   StyleTemplateToFont := TRVIntegerList.Create;
   for i := 0 to RVStyle.StyleTemplates.Count-1 do begin
     Index := RTFTables.AddFontFromStyleEx(RVStyle.StyleTemplates[i].TextStyle,
       RVStyle.StyleTemplates[i].ValidTextProperties,
       (RVStyle.StyleTemplates[i].Parent=nil) and
       (RVStyle.StyleTemplates[i].Kind<>rvstkText));
     StyleTemplateToFont.Add(Index);
   end;
   end
 else
   StyleTemplateToFont := nil;
 {$ENDIF}
 {$IFNDEF RVDONOTUSELISTS}
 for i := 0 to RVStyle.ListStyles.Count-1 do
   for j := 0 to RVStyle.ListStyles[i].Levels.Count-1 do
     if RVStyle.ListStyles[i].Levels[j].UsesFont then
       RTFTables.AddFont(RVStyle.ListStyles[i].Levels[j].Font);
 {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.MakeRTFTables(RTFTables: TCustomRVRTFTables;
  ListOverrideCountList: TRVIntegerList; AddDefColors: Boolean);
{$IFDEF RVRTFADDSTDCOLORS}
   const ArrDefColorTable: array [0..16] of TColor =
         (
         clWindowText, clBlack, clBlue, clAqua,
         clLime, clFuchsia, clRed, clYellow,
         clWhite, clNavy, clTeal, clGreen,
         clPurple, clMaroon, clOlive,
         clGray, clSilver
         );
{$ELSE}
   const ArrDefColorTable: array [0..0] of TColor =
         (
         clWindowText
         );
{$ENDIF}
   {..................................................}
   procedure AddTextStyleColors(RTFTables: TCustomRVRTFTables;
     TextStyle: TCustomRVFontInfo);
   begin
     RTFTables.AddColor(TextStyle.Color);
     RTFTables.AddColor(TextStyle.BackColor);
     RTFTables.AddColor(TextStyle.HoverColor);
     RTFTables.AddColor(TextStyle.HoverBackColor);
     RTFTables.AddColor(TextStyle.UnderlineColor);
     RTFTables.AddColor(TextStyle.HoverUnderlineColor);
   end;
   {..................................................}
   procedure AddParaStyleColors(RTFTables: TCustomRVRTFTables;
     ParaStyle: TCustomRVParaInfo);
   begin
     if (ParaStyle.Border.Style<>rvbNone) then
       RTFTables.AddColor(ParaStyle.Border.Color);
     RTFTables.AddColor(ParaStyle.Background.Color);
   end;
   {..................................................}
   {$IFNDEF RVDONOTUSESTYLETEMPLATES}
   procedure AddTextStyleColorsEx(RTFTables: TCustomRVRTFTables;
     TextStyle: TCustomRVFontInfo; ValidProperties: TRVFontInfoProperties);
   begin
     if rvfiColor in ValidProperties then
       RTFTables.AddColor(TextStyle.Color);
     if rvfiBackColor in ValidProperties then
       RTFTables.AddColor(TextStyle.BackColor);
     if rvfiHoverColor in ValidProperties then
       RTFTables.AddColor(TextStyle.HoverColor);
     if rvfiHoverBackColor in ValidProperties then
       RTFTables.AddColor(TextStyle.HoverBackColor);
     if rvfiUnderlineColor in ValidProperties then
       RTFTables.AddColor(TextStyle.UnderlineColor);
     if rvfiHoverUnderlineColor in ValidProperties then
       RTFTables.AddColor(TextStyle.HoverUnderlineColor);
   end;
   {..................................................}
   procedure AddParaStyleColorsEx(RTFTables: TCustomRVRTFTables;
     ParaStyle: TCustomRVParaInfo; ValidProperties: TRVParaInfoProperties);
   begin
     if rvpiBorder_Color in ValidProperties  then
       RTFTables.AddColor(ParaStyle.Border.Color);
     if rvpiBackground_Color in ValidProperties then
       RTFTables.AddColor(ParaStyle.Background.Color);
   end;
   {$ENDIF}
   {..................................................}
   var i{$IFNDEF RVDONOTUSELISTS},j{$ENDIF}: Integer;
       RVStyle: TRVStyle;
begin
   RVStyle := GetRVStyle;
   if ListOverrideCountList<>nil then
     ListOverrideCountList.Clear;
   if RTFTables<>nil then begin
     RTFTables.ClearColors;
     if AddDefColors then
       for i := Low(ArrDefColorTable) to High(ArrDefColorTable) do
         RTFTables.AddColor(ArrDefColorTable[i]);
     for i := 0 to RVStyle.TextStyles.Count-1 do
       AddTextStyleColors(RTFTables, RVStyle.TextStyles[i]);
     for i := 0 to RVStyle.ParaStyles.Count-1 do
       AddParaStyleColors(RTFTables, RVStyle.ParaStyles[i]);
     {$IFNDEF RVDONOTUSESTYLETEMPLATES}
     if UseStyleTemplates then
       for i := 0 to RVStyle.StyleTemplates.Count-1 do
         with RVStyle.StyleTemplates[i] do begin
           AddTextStyleColorsEx(RTFTables, TextStyle, ValidTextProperties);
           AddParaStyleColorsEx(RTFTables, ParaStyle, ValidParaProperties);
         end;
     {$ENDIF}
   end;
   {$IFNDEF RVDONOTUSELISTS}
   for i := 0 to RVStyle.ListStyles.Count-1 do begin
     if ListOverrideCountList<>nil then
       ListOverrideCountList.Add(1);
     if RTFTables<>nil then
       for j := 0 to RVStyle.ListStyles[i].Levels.Count-1 do
         with RVStyle.ListStyles[i].Levels[j] do
           if UsesFont then
             RTFTables.AddColor(Font.Color);
   end;
   {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.FillRTFTables(RTFTables: TCustomRVRTFTables;
  ListOverrideCountList: TRVIntegerList);
var i: Integer;
begin
   for i := 0 to Items.Count-1 do
     with GetItem(i) do
       if StyleNo<0 then
         FillRTFTables(RTFTables, ListOverrideCountList, Self);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTF}
function TCustomRVData.SaveRTF(const FileName: String; SelectionOnly: Boolean;
  Color: TColor; Background: TRVBackground):Boolean;
var Stream: TFileStream;
    SavingData: TRVRTFSavingData;
begin
  try
    Stream := TFileStream.Create(FileName,fmCreate);
    try
      FillChar(SavingData, sizeof(TRVRTFSavingData), 0);
      SavingData.Path := ExtractFilePath(FileName);
      Result := SaveRTFToStream(Stream, SelectionOnly,
        0, Color, Background, SavingData, rvrtfdtMain, True, False,
        nil, nil, nil, nil, nil, nil);
    finally
      Stream.Free;
    end;
  except
    Result := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure RVSaveFontToRTF(Stream: TStream; Font: TFont;
  RTFTables: TCustomRVRTFTables; RVStyle: TRVStyle);
var idx: Integer;
    {$IFDEF RICHVIEWCBDEF3}
    Language: Cardinal;
    {$ENDIF}
begin
  idx := RTFTables.GetFontIndex(Font);
  if idx>=0 then
    RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\f%d', [idx]));
  if fsBold in Font.Style then
    RVFWrite(Stream, '\b');
  if fsItalic in Font.Style then
    RVFWrite(Stream, '\i');
  if fsUnderline in Font.Style then
    RVFWrite(Stream, '\ul');
  if fsStrikeOut in Font.Style then
    RVFWrite(Stream, '\strike');
  RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\fs%d', [Font.Size*2]));
  {$IFDEF RICHVIEWCBDEF3}
  if (Font.Charset<>DEFAULT_CHARSET) and (Font.Charset<>RVStyle.TextStyles[0].Charset) then begin
    Language := RVU_Charset2Language(Font.Charset);
    if Language<>0 then
      RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\lang%d', [Language]));
  end;
  {$ENDIF}
  if Font.Color<>clWindowText then
    RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\cf%d', [RTFTables.GetColorIndex(Font.Color)]));
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSELISTS}
procedure TCustomRVData.SaveRTFListTable97(Stream: TStream; RTFTables: TCustomRVRTFTables;
  ListOverrideOffsetsList: TRVIntegerList; Header, Footer,
  FirstPageHeader, FirstPageFooter, EvenPagesHeader, EvenPagesFooter: TCustomRVData);
var IDList, TemplateIDList: TRVIntegerList;
    i,j, id, levelcount, idx: Integer;
    s1,s2: TRVAnsiString;
    RVStyle: TRVStyle;
    LevelInfo: TRVListLevel;
    {...................................................}
    function GetLevelNfc(LevelInfo: TRVListLevel): Integer;
    begin
      case LevelInfo.ListType of
        rvlstBullet,
        {$IFNDEF RVDONOTUSEUNICODE}
        rvlstUnicodeBullet,
        {$ENDIF}
        rvlstPicture, rvlstImageList:
          Result := 23;
        rvlstDecimal,rvlstImageListCounter:
          Result := 0;
        rvlstLowerAlpha:
          Result := 4;
        rvlstUpperAlpha:
          Result := 3;
        rvlstLowerRoman:
          Result := 2;
        rvlstUpperRoman:
          Result := 1;
        else
          Result := 255;
      end;
    end;
    {...................................................}
    function GetLevelJc(LevelInfo: TRVListLevel): Integer;
    begin
      case LevelInfo.MarkerAlignment of
        rvmaLeft:
          Result := 0;
        rvmaCenter:
          Result := 1;
        rvmaRight:
          Result := 2;
        else
          Result := -1;
      end;
    end;
    {...................................................}
    procedure GetLevelText(LevelInfo: TRVListLevel;
      var LevelText, LevelNumbers: TRVAnsiString);
    var  s: String;
        i: Integer;
    begin
      case LevelInfo.ListType of
        rvlstBullet:
          begin
            {$IFDEF RVUNICODESTR}
            LevelText := RVMakeRTFStrW(LevelInfo.FormatString, RVStyle.DefCodePage,
              rvrtfDuplicateUnicode in RTFOptions, False, False, False);
            {$ELSE}
            LevelText := RVMakeRTFStr(LevelInfo.FormatString, False, False, False);
            {$ENDIF}
            LevelText := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\''%.2x%s',[Length(LevelInfo.FormatString),LevelText]);
            LevelNumbers := '';
          end;
        {$IFNDEF RVDONOTUSEUNICODE}
        {$IFDEF RICHVIEWCBDEF3}
        rvlstUnicodeBullet:
          begin
            LevelText := RVMakeRTFStrW(LevelInfo.FormatStringW, RVStyle.DefCodePage,
              rvrtfDuplicateUnicode in RTFOptions, False, False, False);
            LevelText := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\''%.2x%s',[Length(LevelInfo.FormatStringW),LevelText]);
            LevelNumbers := '';
          end;
        {$ENDIF}
        {$ENDIF}
        rvlstDecimal,rvlstImageListCounter,
        rvlstLowerAlpha,rvlstUpperAlpha,
        rvlstLowerRoman,rvlstUpperRoman:
          begin
            {$IFDEF RVUNICODESTR}
            LevelText := RVMakeRTFStrW(LevelInfo.FormatString, RVStyle.DefCodePage,
              rvrtfDuplicateUnicode in RTFOptions, False, False, True);
            {$ELSE}
            LevelText := RVMakeRTFStr(LevelInfo.FormatString, False, False, True);
            {$ENDIF}
            LevelText := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(LevelText, [
              PRVAnsiChar('\''00'), PRVAnsiChar('\''01'), PRVAnsiChar('\''02'),
              PRVAnsiChar('\''03'), PRVAnsiChar('\''04'), PRVAnsiChar('\''05'),
              PRVAnsiChar('\''06'), PRVAnsiChar('\''07'), PRVAnsiChar('\''08')]);
            s := Format(LevelInfo.FormatString, [#1, #2, #3, #4, #5, #6, #7, #8, #9]);
            LevelNumbers := '';
            for i := 1 to Length(s) do
              if s[i]<=#9 then
                LevelNumbers := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%s\''%.2x',[LevelNumbers,i]);
            LevelText := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\''%.2x%s',[Length(s),LevelText]);
          end;
        else
          begin
            LevelText := '\''00';
            LevelNumbers := '';
          end;
      end;
    end;
    {...................................................}
    // Returns RVData in the same order as their bodies are saved in RTF
    function GetRVDataByIndex(RVDataIndex: Integer; var RVData: TCustomRVData): Boolean;
    begin
      Result := True;
      case RVDataIndex of
        0: RVData := Header;
        1: RVData := Footer;
        2: RVData := FirstPageHeader;
        3: RVData := FirstPageFooter;
        4: RVData := EvenPagesHeader;
        5: RVData := EvenPagesFooter;
        6: RVData := Self;
        else Result := False;
      end;
    end;
    {...................................................}
    procedure SaveListOverrideTable;
    var i,j,k, Index, RVDataIndex: Integer;
        Markers: TRVMarkerList;
        Marker: TRVMarkerItemInfo;
        RVData: TCustomRVData;
    begin
      index := 1;
      for i := 0 to IDList.Count-1 do begin
        RVFWriteLine(Stream,
          {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\listoverride\listid%d\listoverridecount0\ls%d}', [IDList[i],index]));
        inc(index);
        RVDataIndex := 0;
        while GetRVDataByIndex(RVDataIndex, RVData) do begin
          if RVData<>nil then begin
            Markers := RVData.GetMarkers(False);
            if (Markers<>nil) and (ListOverrideOffsetsList[i]>1) then begin
              for j := 0 to Markers.Count-1 do begin
                Marker := Markers[j];
                if (Marker.ListNo=i) and (Marker.Level>=0) and Marker.Reset then begin
                  RVFWrite(Stream,
                    {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\listoverride\listid%d\listoverridecount%d',
                    [IDList[i],Marker.Level+1]));
                  for k := 0 to Marker.Level-1 do
                    RVFWrite(Stream, '{\lfolevel}');
                  RVFWrite(Stream,
                    {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\lfolevel\listoverridestartat\levelstartat%d}', [Marker.StartFrom]));
                  RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\ls%d}', [index]));
                  inc(index);
                end;
              end;
            end;
          end;
          inc(RVDataIndex);
        end;
      end;
    end;
    {...................................................}
    // transforming a list of counts to a list of offsets
    procedure FinalizeListOverrideTable;
    var i, prevcount,curcount: Integer;
    begin
      if RVStyle.ListStyles.Count>0 then begin
        prevcount := ListOverrideOffsetsList[0];
        ListOverrideOffsetsList[0] := 1; // starting from 1
        for i := 1 to RVStyle.ListStyles.Count-1 do begin
          curcount := ListOverrideOffsetsList[i];
          ListOverrideOffsetsList[i] := ListOverrideOffsetsList[i-1]+prevcount;
          prevcount := curcount;
        end;
      end;
    end;
    {...................................................}
begin
  RVStyle := GetRVStyle;
  if (RVStyle.ListStyles.Count=0) then begin
    RVFWriteLine(Stream, '');
    exit;
  end;

  IDList := TRVIntegerList.Create;
  TemplateIDList := TRVIntegerList.Create;
  try
    // writing list table
    RVFWrite(Stream, '{\*\listtable');
    for i := 0 to RVStyle.ListStyles.Count-1 do begin
      repeat
        id := Random(MaxInt);
      until IDList.IndexOf(Pointer(id))<0;
      TemplateIDList.Add(id);
      RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\list\listtemplateid%d',[id]));
      if RVStyle.ListStyles[i].Levels.Count=1 then
        RVFWrite(Stream, '\listsimple1');
      RVFWrite(Stream, GetExtraRTFCode(rv_rtfs_ListStyle, RVStyle.ListStyles[i], i , -1, False));
      RVFWriteLine(Stream, '');
      levelcount := RVStyle.ListStyles[i].Levels.Count;
      if levelcount>1 then
        levelcount := 9;
      for j := 0 to levelcount-1 do begin
        // writing list level
        if j<RVStyle.ListStyles[i].Levels.Count then
          idx := j
        else
          idx := RVStyle.ListStyles[i].Levels.Count-1;
        LevelInfo := RVStyle.ListStyles[i].Levels[idx];
        RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\listlevel\levelnfc%d\leveljc%d\li%d\fi%d\jclisttab\tx%d',
          [GetLevelNfc(LevelInfo), GetLevelJc(LevelInfo),
           RVStyle.GetAsTwips(LevelInfo.LeftIndent),
           RVStyle.GetAsTwips(LevelInfo.MarkerIndent-LevelInfo.LeftIndent),
           RVStyle.GetAsTwips(LevelInfo.FirstIndent+LevelInfo.LeftIndent)]));
        if GetLevelNfc(LevelInfo)<>23 then
          RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\levelstartat%d', [LevelInfo.StartFrom]));
        if rvloLegalStyleNumbering in LevelInfo.Options then
          RVFWrite(Stream, '\levellegal1');
        if not (rvloLevelReset in LevelInfo.Options) then
          RVFWrite(Stream, '\levelnorestart1');
        GetLevelText(LevelInfo, s1, s2);
        RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\leveltext%s;}{\levelnumbers%s;}', [s1,s2]));
        if LevelInfo.UsesFont then
          RVSaveFontToRTF(Stream, LevelInfo.Font, RTFTables, RVStyle);
        RVFWrite(Stream, GetExtraRTFCode(rv_rtfs_ListStyle, RVStyle.ListStyles[i], i , j, False)); 
        RVFWriteLine(Stream, '}');
      end;
      repeat
        id := Random(MaxInt);
      until TemplateIDList.IndexOf(Pointer(id))<0;
      IDList.Add(id);
      RVFWriteLine(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\listid%d}',[id]));
    end;
    RVFWriteLine(Stream, '}');
    // writing list override table (in the order as subdocuments are saved in RTF)
    RVFWriteLine(Stream, '{\*\listoverridetable');
    SaveListOverrideTable;
    RVFWriteLine(Stream, '}');
    FinalizeListOverrideTable
  finally
    IDList.Free;
    TemplateIDList.Free;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.ShouldSaveTextToRTF(StyleNo: Integer): Boolean;
begin
  Result := ShouldSaveTextToFormat(StyleNo, rvteoRTFCode);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveRTFToStream(Stream: TStream;
  SelectionOnly: Boolean; Level: Integer; Color: TColor;
  Background: TRVBackground; var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
  CompleteDocument, HiddenParent: Boolean; Header, Footer,
  FirstPageHeader, FirstPageFooter, EvenPagesHeader, EvenPagesFooter: TCustomRVData): Boolean;
var RVStyle: TRVStyle;
    {$IFNDEF RVDONOTUSELISTS}
    LastListLevel: TRVListLevel;
    {$ENDIF}
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    StyleTemplateToFont: TRVIntegerList;
    LastParaStyleSheetNo, LastTextStyleSheetNo: Integer;
    LastParaStyleTemplateId, LastTextStyleTemplateId: TRVStyleTemplateId;
    {$ENDIF}
    LastParaNo, LastTextStyleNo, CurTextStyleNo, LastDefBiDiParaNo: Integer;
    CallProgress: Boolean;
    Progress: Integer;

    function GetTwipsPerPixel: Double;
    var DC: HDC;
    begin
      DC := CreateCompatibleDC(0);
      if RichViewPixelsPerInch>0 then
        Result := (72*20) / RichViewPixelsPerInch
      else
        Result := (72*20) / GetDeviceCaps(DC, LOGPIXELSY);
      DeleteDC(DC);
    end;
   {.................................................}
   procedure SaveFontTable(RTFTables: TCustomRVRTFTables);
   var i: Integer;
       Charset: Integer;
       FontName: TRVAnsiString;
       FontTable: TRVRTFFontTable;
   begin
     if RTFTables is TRVRTFTables then
       FontTable := TRVRTFTables(RTFTables).FFontTable
     else
       exit;
     RVFWrite(Stream, '{\fonttbl');
     for i := 0 to FontTable.Count-1 do begin
       {$IFDEF RICHVIEWCBDEF3}
       Charset := FontTable[i].Charset;
       {$ELSE}
       Charset := 1;
       {$ENDIF}
       RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\f%d\fnil',[i]));
       if Charset<>1 then
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\fcharset%d',[Charset]));
       {$IFDEF RVRTFNOUNICODEFONTNAMES}
         {$IFDEF RVUNICODESTR}
         FontName := RVU_UnicodeToAnsi(CP_ACP, RVU_GetRawUnicode(FontTable[i].FontName));
         FontName := RVMakeRTFStr(FontName, False, False, False);
         {$ELSE}
         FontName := MakeRTFIdentifierStr(FontTable[i].FontName, CP_ACP, rvrtfDuplicateUnicode in RTFOptions);
         {$ENDIF}
       {$ELSE}
       FontName := MakeRTFIdentifierStr(FontTable[i].FontName, CP_ACP, rvrtfDuplicateUnicode in RTFOptions);
       {$ENDIF}
       RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(' %s;}',[FontName]));
     end;
     RVFWrite(Stream, '}');
   end;
   {.................................................}
   procedure SaveColorTable(RTFTables: TCustomRVRTFTables);
   var i: Integer;
       Color: Integer;
       ColorList: TRVColorList;
   begin
     if RTFTables is TRVRTFTables then
       ColorList := TRVRTFTables(RTFTables).FColorList
     else
       exit;
     RVFWrite(Stream, '{\colortbl;');
     for i := 1 to ColorList.Count-1 do begin
       Color := ColorToRGB(Integer(ColorList.Items[i]));
       RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\red%d\green%d\blue%d;',
                [
                  Color and $0000FF,
                  (Color and $00FF00) shr 8,
                  (Color and $FF0000) shr 16
                ]));
     end;
     RVFWrite(Stream, '}');
   end;
   {.................................................}
   procedure SaveTextStyle(StyleNo: Integer; StyleToFont: TRVIntegerList;
     RTFTables: TCustomRVRTFTables; ToStyleSheet: Boolean; TextStyle: TFontInfo;
     InheritedStyle: Boolean);
   begin
     if TextStyle=nil then
       TextStyle := RVStyle.TextStyles[StyleNo];
     if not ToStyleSheet then begin
       {$IFNDEF RVDONOTUSESTYLETEMPLATES}
       if UseStyleTemplates then begin
         if LastTextStyleTemplateId<>TextStyle.StyleTemplateId then begin
           LastTextStyleTemplateId := TextStyle.StyleTemplateId;
           if LastTextStyleTemplateId<=0 then
             LastTextStyleSheetNo := -1
           else
             LastTextStyleSheetNo := RVStyle.StyleTemplates.FindById(LastTextStyleTemplateId);
           if LastTextStyleSheetNo>=0 then
             LastTextStyleSheetNo := RTFTables.GetStyleSheetIndex(LastTextStyleSheetNo, True);
         end;
         if LastTextStyleSheetNo>=0 then
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\cs%d', [LastTextStyleSheetNo]))
         end
       else
       {$ENDIF}
         if (rvrtfSaveStyleSheet in RTFOptions) and TextStyle.Standard then
           RVFWrite(Stream,{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\cs%d', [CurTextStyleNo+RVStyle.ParaStyles.Count]));
       LastTextStyleNo := StyleNo;
     end;
     if StyleNo>=RVStyle.TextStyles.Count then
       StyleNo := 0;
     if StyleNo>=0 then
       RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}
         Format('\f%d', [StyleToFont.Items[StyleNo]]));
     TextStyle.SaveRTFToStream(Stream, StyleToFont, RTFTables, RVStyle,
       InheritedStyle);
     if not (rvteoHidden in RVStyle.TextStyles[StyleNo].Options) and HiddenParent then
       RVFWrite(Stream, '\v ');
     with RVStyle.TextStyles[StyleNo] do begin
       RVFWrite(Stream, GetExtraRTFCode(rv_rtfs_TextStyle,
         RVStyle.TextStyles[StyleNo], StyleNo, -1, ToStyleSheet));
       RVFWrite(Stream, ' ');
     end;
   end;
   {.................................................}
   {$IFNDEF RVDONOTUSELISTS}
   function IsListLevelNew(item: TCustomRVItemInfo): Boolean;
   begin
     if item.StyleNo<>rvsListMarker then begin
       Result := LastListLevel<>nil;
       exit;
     end;
     Result := TRVMarkerItemInfo(item).GetLevelInfo(RVStyle)<>LastListLevel;
   end;
   {$ENDIF}
   {.................................................}
   procedure SaveParaStyle(ParaNo: Integer; RTFTables: TCustomRVRTFTables;
     ToStyleSheet: Boolean; item: TCustomRVItemInfo; ParaStyle: TParaInfo);
   var s: TRVAnsiString;
       MinAllowedTabPos: Integer;
   begin
     if ParaStyle=nil then
       ParaStyle := RVStyle.ParaStyles[ParaNo];
     if ParaNo>=RVStyle.ParaStyles.Count then
       ParaNo := 0;
     if not ToStyleSheet then begin
       {$IFNDEF RVDONOTUSESTYLETEMPLATES}
       if UseStyleTemplates then begin
         if LastParaStyleTemplateId<>ParaStyle.StyleTemplateId then begin
           LastParaStyleTemplateId := ParaStyle.StyleTemplateId;
           if LastParaStyleTemplateId<=0 then
             LastParaStyleSheetNo := -1
           else
             LastParaStyleSheetNo := RVStyle.StyleTemplates.FindById(LastParaStyleTemplateId);
           if LastParaStyleSheetNo>=0 then
             LastParaStyleSheetNo := RTFTables.GetStyleSheetIndex(LastParaStyleSheetNo, False);
         end;
         if LastParaStyleSheetNo>=0 then
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\s%d', [LastParaStyleSheetNo]))
         end
       else
       {$ENDIF}
         if (rvrtfSaveStyleSheet in RTFOptions) and ParaStyle.Standard then
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\s%d', [ParaNo]));
       LastParaNo := ParaNo;
     end;
     MinAllowedTabPos := 0;
     with ParaStyle do begin
       {$IFNDEF RVDONOTUSELISTS}
       LastListLevel := nil;
       if (item<>nil) and (item.StyleNo = rvsListMarker) and
          (TRVMarkerItemInfo(item).GetLevelInfo(RVStyle)<>nil) then begin
         LastListLevel := TRVMarkerItemInfo(item).GetLevelInfo(RVStyle);
         with LastListLevel do begin
           {$IFNDEF RVDONOTUSETABS}
           MinAllowedTabPos := FirstIndent+LeftIndent; // MinAllowedTabPos must be in RVStyle.Units
           {$ENDIF}
           s := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\li%d\fi%d\jclisttab\tx%d',
            [RVStyle.GetAsTwips(LeftIndent),
             RVStyle.GetAsTwips(MarkerIndent-LeftIndent),
             RVStyle.GetAsTwips(FirstIndent+LeftIndent)]);
         end;
         end
       else
       {$ENDIF}
         s := '';
       SaveRTFToStream(Stream, RTFTables, MinAllowedTabPos, s, RVStyle);
       if not ToStyleSheet then begin
         if Level<>1 then
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('\itap%d',[Level]))
         else 
           RVFWrite(Stream, '\intbl');
       end;
       if ParaNo>=0 then
         RVFWrite(Stream, GetExtraRTFCode(rv_rtfs_ParaStyle, ParaStyle, ParaNo, -1, ToStyleSheet));
       RVFWrite(Stream, ' ');
     end;
   end;
   {.................................................}
   // Legacy code: saving RTF stylesheet basing in TextStyles and ParaStyles having
   // Standard = True
   procedure SaveOldStyleSheet;
   var i: Integer;
   begin
     RVFWrite(Stream, '{\stylesheet');
     for i := 0 to RVStyle.ParaStyles.Count-1 do begin
       if RVStyle.ParaStyles[i].Standard then begin
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\s%d',[i]));
         SaveParaStyle(i, SavingData.RTFTables, True, nil, nil);
         RVFWrite(Stream, MakeRTFIdentifierStr(RVStyle.ParaStyles[i].StyleName,
           RVStyle.DefCodePage, rvrtfDuplicateUnicode in RTFOptions));
         RVFWrite(Stream, ';}');
       end;
     end;
     for i := 0 to RVStyle.TextStyles.Count-1 do begin
       if RVStyle.TextStyles[i].Standard then begin
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\*\cs%d',[i+RVStyle.ParaStyles.Count]));
         SaveTextStyle(i, SavingData.StyleToFont, SavingData.RTFTables, True, nil, False);
         RVFWrite(Stream, MakeRTFIdentifierStr(RVStyle.TextStyles[i].StyleName,
           RVStyle.DefCodePage, rvrtfDuplicateUnicode in RTFOptions)+';}');
       end;
     end;
     RVFWrite(Stream, '}');
   end;
   {.................................................}
   {$IFNDEF RVDONOTUSESTYLETEMPLATES}
   // Building an array of RTF stylesheet entries.
   // array[i] = index of the style template saved as the i-th RTF stylesheet entry.
   // This array must be created before saving RTF stylesheet, because an entry
   // can refer to entry with a greater index
   procedure BuildNewStyleSheetMap(RTFTables: TCustomRVRTFTables);
   var i: Integer;
       DefaultSaved: Boolean;
   begin
     DefaultSaved := False;
     RTFTables.ClearStyleSheetMap;
     for i := 0 to RVStyle.StyleTemplates.Count-1 do begin
       if (RVStyle.StyleTemplates[i].Kind<>rvstkPara) and not DefaultSaved then begin
         RTFTables.AddStyleSheetMap(-1); // Default Paragraph Font
         DefaultSaved := True;
       end;
       RTFTables.AddStyleSheetMap(i);
       if RVStyle.StyleTemplates[i].Kind=rvstkParaText then
         RTFTables.AddStyleSheetMap(i); // for text+para styles, saving char style entry next
     end;
   end;
   {.................................................}
   // Saving RTF stylesheet basing on StyleTemplates
   procedure SaveNewStyleSheet(StyleTemplateToFont: TRVIntegerList;
     RTFTables: TCustomRVRTFTables);
   var i: Integer;
       ParaStyle: TParaInfo;
       TextStyle: TFontInfo;
       DefaultIndex: Integer;
       StyleSheetIndex, Index: Integer;
   begin
     DefaultIndex := -1;
     BuildNewStyleSheetMap(RTFTables);
     StyleSheetIndex := 0;
     RVFWrite(Stream, '{\stylesheet');
     for i := 0 to RVStyle.StyleTemplates.Count-1 do begin
       if (RVStyle.StyleTemplates[i].Kind<>rvstkPara) and (DefaultIndex<0) then begin
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
           '{\*\cs%d \additive \ssemihidden \sunhideused Default Paragraph Font;}',
           [StyleSheetIndex]));
         DefaultIndex := StyleSheetIndex;
         inc(StyleSheetIndex);
       end;
       if RVStyle.StyleTemplates[i].Kind<>rvstkText then begin
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
           '{\s%d',[StyleSheetIndex]));
         ParaStyle := TParaInfo.Create(nil);
         try
           RVStyle.StyleTemplates[i].ApplyToParaStyle(ParaStyle);
           SaveParaStyle(-1, RTFTables, True, nil, ParaStyle);
         finally
           ParaStyle.Free;
         end;
       end
       else begin
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
           '{\*\cs%d \additive',[StyleSheetIndex]));
       end;
       if RVStyle.StyleTemplates[i].Parent<>nil then begin
         Index := RVStyle.StyleTemplates.FindById(RVStyle.StyleTemplates[i].ParentId);
         if Index>=0 then
           Index := RTFTables.GetStyleSheetIndex(Index, RVStyle.StyleTemplates[i].Kind=rvstkText);
         if Index>=0 then
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
             '\sbasedon%d',[Index]));
       end;
       if (RVStyle.StyleTemplates[i].Next<>nil) and
          (RVStyle.StyleTemplates[i].Next<>RVStyle.StyleTemplates[i]) then begin
         Index := RVStyle.StyleTemplates.FindById(RVStyle.StyleTemplates[i].NextId);
         if Index>=0 then
           Index := RTFTables.GetStyleSheetIndex(Index, RVStyle.StyleTemplates[i].Kind=rvstkText);
         if Index>=0 then
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
             '\snext%d',[Index]));
       end;
       if RVStyle.StyleTemplates[i].Kind=rvstkParaText then
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
           '\slink%d',[StyleSheetIndex+1]));
       if StyleTemplateToFont[i]>=0 then
         RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}
           Format('\f%d', [StyleTemplateToFont[i]]));
       TextStyle := TFontInfo.Create(nil);
       try
         RVStyle.StyleTemplates[i].ApplyToTextStyle(TextStyle, nil);
         SaveTextStyle(-1, nil, RTFTables, True, TextStyle,
           (RVStyle.StyleTemplates[i].Kind=rvstkText) or (RVStyle.StyleTemplates[i].Parent<>nil));
         RVFWrite(Stream, GetExtraRTFCode(rv_rtfs_StyleTemplate, RVStyle.StyleTemplates[i], i, -1, True));
         RVFWrite(Stream, MakeRTFIdentifierStr(RVStyle.StyleTemplates[i].Name,
           RVStyle.DefCodePage, rvrtfDuplicateUnicode in RTFOptions)+';}');
         inc(StyleSheetIndex);
         if RVStyle.StyleTemplates[i].Kind=rvstkParaText then begin
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
             '{\*\cs%d \additive',[StyleSheetIndex]));
           RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
             '\slink%d',[StyleSheetIndex-1]));
           if RVStyle.StyleTemplates[i].Parent<>nil then begin
             if RVStyle.StyleTemplates[i].ParentId = RVStyle.StyleTemplates.NormalStyleTemplate.Id then
               Index := DefaultIndex
             else begin
               Index := RVStyle.StyleTemplates.FindById(RVStyle.StyleTemplates[i].ParentId);
               if Index>=0 then
                 Index := RTFTables.GetStyleSheetIndex(Index, True);
             end;
             if Index>=0 then
               RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
                 '\sbasedon%d',[Index]));
           end;
           if StyleTemplateToFont[i]>=0 then
             RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}
               Format('\f%d', [StyleTemplateToFont[i]]));
           SaveTextStyle(-1, nil, RTFTables, True, TextStyle, True);
           RVFWrite(Stream, GetExtraRTFCode(rv_rtfs_StyleTemplateText, RVStyle.StyleTemplates[i], i, -1, True));
           inc(StyleSheetIndex);
           RVFWrite(Stream, MakeRTFIdentifierStr(
             RVStyle.StyleTemplates.MakeNameUnique(RVStyle.StyleTemplates[i].Name+' Text'),
             RVStyle.DefCodePage, rvrtfDuplicateUnicode in RTFOptions)+';}');
         end;
       finally
         TextStyle.Free;
       end;
     end;
     RVFWrite(Stream, '}');
   end;
   {$ENDIF}
   {.................................................}
   procedure SaveHF(HFRVData: TCustomRVData; const Keyword: TRVAnsiString);
   begin
     if HFRVData<>nil then begin
       RVFWrite(Stream, '{\'+Keyword+' ');
       HFRVData.SaveRTFToStream(Stream, False, Level, clNone, nil, SavingData,
         rvrtfdtHeaderFooter, False, False, nil, nil, nil, nil, nil, nil);
       RVFWrite(Stream, '\par}');
     end;
   end;
   {.................................................}
   procedure SaveHeader;
   var CodePage: TRVCodePage;
       Language: Cardinal;
       UC: Integer;
   begin
     {$IFNDEF RVDONOTUSEUNICODE}
     {$IFDEF RICHVIEWCBDEF3}
     CodePage := GetRVStyle.DefCodePage;
     {$IFDEF RVLANGUAGEPROPERTY}
     Language := GetRVStyle.TextStyles[0].Language;
     if Language=0 then
       Language := $0400;
     {$ELSE}
     Language := RVU_Charset2Language(GetRVStyle.TextStyles[0].CharSet);
     {$ENDIF}
     {$ELSE}
     CodePage := 1252;
     Language := $0400;
     {$ENDIF}
     {$ELSE}
     CodePage := 1252;
     Language := $0400;
     {$ENDIF}
     if rvrtfDuplicateUnicode in RTFOptions then
       UC := 1
     else
       UC := 0;
     RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\rtf1\fbidis\ansi\ansicpg%d\uc%d\deff0\deflang%d\deflangfe%d',
       [CodePage, UC, Language, Language]));
     case GetBiDiMode of
       rvbdLeftToRight:
         RVFWrite(Stream, '\ltrdoc');
       rvbdRightToLeft:
         RVFWrite(Stream, '\rtldoc');
     end;
     {$IFNDEF RVDONOTUSESEQ}
     if RVGetFirstEndnoteInRootRVData(Self, True)<>nil then
       RVFWrite(Stream,
         '\fet2{\*\aftnsep\pard\plain\chftnsep }{\*\aftnsepc\pard\plain\chftnsepc}');
     if RVGetFirstFootnoteInRootRVData(Self, True)<>nil then
       RVFWrite(Stream,
         '\fet2{\*\ftnsep\pard\plain\chftnsep }{\*\ftnsepc\pard\plain\chftnsepc}');
     {$ENDIF}
     {$IFNDEF RVDONOTUSEDOCPARAMS}
     if (rvrtfSaveDocParameters in RTFOptions) then begin
       if GetDocParameters(False)<>nil then
         GetDocParameters(False).SaveToRTF(Stream, rvrtfDuplicateUnicode in RTFOptions)
       else
         RVFWriteLine(Stream, '\paperw11906\paperh16838\margl1800\margr1800\margt1440\margb1440');
     end;
     {$ENDIF}
     SaveFontTable(SavingData.RTFTables);
     SaveColorTable(SavingData.RTFTables);
     {$IFNDEF RVDONOTUSESTYLETEMPLATES}
     if UseStyleTemplates then begin
       SaveNewStyleSheet(StyleTemplateToFont, SavingData.RTFTables);
       StyleTemplateToFont.Free;
       end
     else
     {$ENDIF}
       if (rvrtfSaveStyleSheet in RTFOptions) then
         SaveOldStyleSheet;
     {$IFNDEF RVDONOTUSELISTS}
     SaveRTFListTable97(Stream, SavingData.RTFTables,  SavingData.ListOverrideOffsetsList1,
       Header, Footer, FirstPageHeader, FirstPageFooter,
       EvenPagesHeader, EvenPagesFooter);
     SavingData.ListOverrideOffsetsList2.Assign(SavingData.ListOverrideOffsetsList1);
     {$ELSE}
     RVFWriteLine(Stream, '');
     {$ENDIF}
     SaveHF(Header, 'header');
     SaveHF(Footer, 'footer');
     SaveHF(FirstPageHeader, 'headerf');
     SaveHF(FirstPageFooter, 'footerf');
     SaveHF(EvenPagesHeader, 'headerl');
     SaveHF(EvenPagesFooter, 'footerl');
     RVFWriteLine(Stream, GetExtraRTFCode(rv_rtfs_Doc, nil, -1, -1, False));     
   end;
   {.................................................}
var i, CPIndex: Integer;
    item: TCustomRVItemInfo;
    s: TRVRawByteString;
    StartItem,EndItem,StartOffs,EndOffs
    {$IFNDEF RVDONOTUSELISTS}
    ,MarkerItemNo
    {$ENDIF}
    : Integer;
    UrlExtras, UrlTarget, UrlLocalTarget: String;
    UrlExtrasA, UrlTargetA, UrlLocalTargetA: TRVAnsiString;
    NotUsedPart: TRVMultiDrawItemPart;
    SelectedItem: TCustomRVItemInfo;
begin
  if not SelectionOnly then
    DoBeforeSaving;
  {$IFNDEF RVDONOTUSEINPLACE}
  if SelectionOnly and (GetChosenRVData<>nil) then begin
    Result := GetChosenRVData.SaveRTFToStream(Stream, SelectionOnly, Level,
      Color, Background, SavingData, DocType, True, HiddenParent,
      nil, nil, nil, nil, nil, nil);
    exit;
  end;
  {$ENDIF}
  Result := True;
  RVFGetLimits(GetRVFSaveScope(SelectionOnly),StartItem,EndItem,StartOffs,EndOffs,NotUsedPart,NotUsedPart, SelectedItem);
  if (SelectedItem<>nil) or (StartItem=-1) or (StartItem>EndItem) then
    exit;
  LastParaNo := -1;
  LastDefBiDiParaNo := -1;
  LastTextStyleNo := -1;
  {$IFNDEF RVDONOTUSELISTS}
  LastListLevel := nil;
  {$ENDIF}
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  LastParaStyleSheetNo := -1;
  LastTextStyleSheetNo := -1;
  LastParaStyleTemplateId := -1;
  LastTextStyleTemplateId := -1;
  {$ENDIF}

  if (Level=0) and CompleteDocument then begin
    SavingData.RTFTables := TRVRTFTables.Create;
    SavingData.StyleToFont := TRVIntegerList.Create;
    SavingData.ListOverrideOffsetsList1 := TRVIntegerList.Create;
    SavingData.ListOverrideOffsetsList2 := TRVIntegerList.Create;
  end;
  RVStyle := GetRVStyle;
  CPIndex := 0;
  CallProgress := (Level=0) and SaveProgressInit(Progress,  EndItem-StartItem, rvloRTFWrite);
  Progress := 0;
  try
    Include(State, rvstRTFSkipPar);
    if (Level=0) and CompleteDocument then begin
      MakeFontTable(SavingData.RTFTables, SavingData.StyleToFont
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}, StyleTemplateToFont{$ENDIF});
      MakeRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1, True);
      if Header<>nil then
        Header.FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      if Footer<>nil then
        Footer.FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      if FirstPageHeader<>nil then
        FirstPageHeader.FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      if FirstPageFooter<>nil then
        FirstPageFooter.FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      if EvenPagesHeader<>nil then
        EvenPagesHeader.FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      if EvenPagesFooter<>nil then
        EvenPagesFooter.FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      FillRTFTables(SavingData.RTFTables, SavingData.ListOverrideOffsetsList1);
      if (Color<>clWindow) then
        SavingData.RTFTables.AddColor(Color);
      SaveHeader;
      {$IFNDEF RVDONOTUSELISTS}
      if SelectionOnly then begin
        MarkerItemNo := GetFirstParaItem(StartItem);
        if (MarkerItemNo<>StartItem) and (GetItemStyle(MarkerItemNo)=rvsListMarker) then begin
          SaveParaStyle(GetItem(MarkerItemNo).ParaNo, SavingData.RTFTables, False, GetItem(MarkerItemNo), nil);
          GetItem(MarkerItemNo).SaveRTF(Stream, Self, MarkerItemNo,
            Level, SavingData, DocType, HiddenParent);
          Exclude(State, rvstRTFSkipPar);
        end;
      end;
      {$ENDIF}
    end;
    for i := StartItem to EndItem do begin
      if (i=StartItem) and (GetItemStyle(i)<0) and (StartOffs>=GetOffsAfterItem(i)) then
        continue;
      if (i=EndItem) and (GetItemStyle(i)<0) and (EndOffs<=GetOffsBeforeItem(i)) then begin
        if SelectionOnly and IsParaStart(i) then
          RVFWrite(Stream,'\par');
        continue;
      end;
      if not ((StartItem=EndItem) and (GetItemStyle(StartItem)>=0)) then begin
        if (i=StartItem) and (StartOffs>=GetOffsAfterItem(i)) and (Items[i]<>'') then
          continue
        else if (i=EndItem) and (EndOffs<=GetOffsBeforeItem(i)) and (Items[i]<>'') then begin
          if SelectionOnly and IsParaStart(i) then
            RVFWrite(Stream,'\par');
          continue;
        end;
      end;
      item := GetItem(i);
      if not item.SameAsPrev then begin
        RVFWriteLine(Stream,'');
        if item.GetBoolValue(rvbpFullWidth) and PageBreaksBeforeItems[i] then begin
          RVFWrite(Stream,'\page ');
          {$IFNDEF RVDONOTUSETABLES}
          if (i>StartItem) and (item is TRVTableItemInfo) and (GetItem(i-1) is TRVTableItemInfo) then
            RVFWrite(Stream,'\par ');
          {$ENDIF}
        end;
        if item.BR then
          RVFWrite(Stream,'\line ')
        else begin
          if not (rvstRTFSkipPar in State) then begin
            RVFWrite(Stream, '\par ');
            {$IFNDEF RVDONOTUSELISTS}
            if (i>0) and (GetItemStyle(GetFirstParaItem(i-1))=rvsListMarker) then begin
              RVFWrite(Stream, '\plain');
              LastTextStyleNo := -1;
            end;
            {$ENDIF}
          end;
          if not item.GetBoolValue(rvbpFullWidth) and PageBreaksBeforeItems[i]  then
            RVFWrite(Stream,'\page ');
          if (item.ParaNo<>LastParaNo)
             {$IFNDEF RVDONOTUSELISTS}or IsListLevelNew(item){$ENDIF} then begin
            RVFWrite(Stream, '\pard');
            SaveParaStyle(item.ParaNo, SavingData.RTFTables, False, item, nil);
          end;
        end;
      end;
      Exclude(State, rvstRTFSkipPar);
      if (item.Checkpoint<>nil) and (item.Checkpoint.Name<>'') then begin
        // I decided to use names of checkpoints here (if assigned).
        // If several checkpoints have the same name, only one of them
        // will be used as a bookmark in MS Word.
        s := MakeRTFIdentifierStr(MakeRTFBookmarkNameStr(item.Checkpoint.Name),
          RVStyle.DefCodePage, rvrtfDuplicateUnicode in RTFOptions);
        //if s='' then
        //  s := 'RichViewCheckpoint'+RVIntToStr(CPIndex);
        RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\*\bkmkstart %s}{\*\bkmkend %s}',[s,s]));
        inc(CPIndex);
      end;
      if item.GetBoolValueEx(rvbpJump,RVStyle) then begin
        WriteHyperlink(item.JumpID+FirstJumpNo, Self, i, rvsfRTF,
          UrlTarget, UrlExtras);
        if not (rvoPercentEncodedURL in Self.Options) then
          UrlTarget := RV_EncodeURL(UrlTarget);
        CalcLocalURL(UrlTarget, UrlLocalTarget);
        UrlTargetA := RVMakeRTFFileNameStr(UrlTarget, RVStyle.DefCodePage,
          rvrtfDuplicateUnicode in RTFOptions);
        UrlLocalTargetA := RVMakeRTFFileNameStr(UrlLocalTarget, RVStyle.DefCodePage,
          rvrtfDuplicateUnicode in RTFOptions);
        {$IFDEF RVUNICODESTR}
        UrlExtrasA := RVMakeRTFStrW(UrlExtras,
          RVStyle.DefCodePage, rvrtfDuplicateUnicode in RTFOptions, False, False, False);
        {$ELSE}
        UrlExtrasA := RVMakeRTFStr(UrlExtras, False, False, False);
        {$ENDIF}
        if UrlTargetA<>'' then
          UrlTargetA := '"'+UrlTargetA+'"';
        if UrlLocalTargetA<>'' then
          RV_AddStrA(UrlTargetA, ' \\l "'+UrlLocalTargetA+'"');
        if UrlLocalTargetA = '' then
          UrlExtrasA := ''
        else
          RV_AddStrA(UrlTargetA, UrlExtrasA);
        if UrlTargetA<>'' then
          RVFWrite(Stream,
            {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\field{\*\fldinst HYPERLINK %s}{\fldrslt ',
              [UrlTargetA]));
          LastTextStyleNo := -1;
        end
      else
        UrlTargetA := '';
      if ((item.StyleNo>=0) or (item.AssociatedTextStyleNo>=0)) and
         ShouldSaveTextToRTF(GetActualTextStyle(item)) then begin
        if item.StyleNo>=0 then begin
          if (i=StartItem) then
            if (i=EndItem) then
              s := RVU_Copy(Items[i], StartOffs, EndOffs-StartOffs, item.ItemOptions)
            else
              s := RVU_Copy(Items[i], StartOffs,
                RVU_Length(Items[i],item.ItemOptions)-StartOffs+1, item.ItemOptions)
          else
            if i=EndItem then
              s := RVU_Copy(Items[i], 1, EndOffs-1, item.ItemOptions)
            else
              s := Items[i];
          CurTextStyleNo := GetActualStyle(item);
          end
        else begin
          s := '';
          CurTextStyleNo := GetActualTextStyle(item);
        end;
        if LastTextStyleNo<>CurTextStyleNo then begin
          RVFWrite(Stream, '\plain ');
          SaveTextStyle(CurTextStyleNo, SavingData.StyleToFont, SavingData.RTFTables, False, nil, False);
          LastDefBiDiParaNo := -1;
          if (RVStyle.TextStyles[CurTextStyleNo].BiDiMode=rvbdUnspecified) then
             case GetParaBiDiMode(item.ParaNo) of
               rvbdLeftToRight:
                 begin
                   RVFWrite(Stream, '\ltrch ');
                   LastDefBiDiParaNo := item.ParaNo;
                 end;
               rvbdRightToLeft:
                 begin
                   RVFWrite(Stream, '\rtlch ');
                   LastDefBiDiParaNo := item.ParaNo;
                 end;
             end;
          end
        else if (LastDefBiDiParaNo<>Item.ParaNo) and
          ((LastDefBiDiParaNo<0) or (GetParaBiDiMode(LastDefBiDiParaNo)<>GetParaBiDiMode(Item.ParaNo))) and
          (RVStyle.TextStyles[CurTextStyleNo].BiDiMode=rvbdUnspecified) then begin
             case GetParaBiDiMode(item.ParaNo) of
               rvbdLeftToRight, rvbdUnspecified:
                 RVFWrite(Stream, '\ltrch ');
               rvbdRightToLeft:
                 RVFWrite(Stream, '\rtlch ');
             end;
          LastDefBiDiParaNo := Item.ParaNo;
        end;

        if SaveItemToFile(SavingData.Path, Self, i, rvsfRTF, False, s) then
          RVFWrite(Stream, s)
        else begin
          if item.StyleNo>=0 then begin
            {$IFNDEF RVDONOTUSEUNICODE}
            if rvioUnicode in item.ItemOptions then
              RVWriteUnicodeRTFStr(Stream, s, GetStyleCodePage(GetActualStyle(item)),
                rvrtfDuplicateUnicode in RTFOptions, False,
                rvteoRTFCode in RVStyle.TextStyles[GetActualStyle(item)].Options,
                False, False)
            else
            {$ENDIF}
              RVFWrite(Stream, RVMakeRTFStr(s,rvteoRTFCode in
                RVStyle.TextStyles[GetActualStyle(item)].Options, True, False));
            end
          else
            item.SaveRTF(Stream, Self, i, Level, SavingData, DocType, HiddenParent);
        end;
        end
      else begin
        if (LastParaNo<>-1) and (RVStyle.ParaStyles[LastParaNo].BiDiMode=rvbdRightToLeft) then
          RVFWrite(Stream, '\ltrpar');
        s := '';
        if SaveItemToFile(SavingData.Path, Self, i, rvsfRTF, False, s) then
          RVFWrite(Stream, s)
        else
          item.SaveRTF(Stream, Self, i, Level, SavingData, DocType, HiddenParent);
        LastTextStyleNo := -1;
        if (LastParaNo<>-1) and (RVStyle.ParaStyles[LastParaNo].BiDiMode=rvbdRightToLeft) then
          RVFWrite(Stream, '\rtlpar');
      end;
      if UrlTargetA<>'' then begin
        RVFWrite(Stream, '}}');
        LastTextStyleNo := -1;
      end;
      if CallProgress then
        SaveProgressRun(Progress, i-StartItem, EndItem-StartItem, rvloRTFWrite);
    end;
    if NotAddedCP<>nil then begin
      // I decided to use names of checkpoints here (if assigned).
      // If several checkpoints have the same name, only one of them
      // will be used as a bookmark in MS Word.
      s := MakeRTFIdentifierStr(MakeRTFBookmarkNameStr(NotAddedCP.Name), RVStyle.DefCodePage,
        rvrtfDuplicateUnicode in RTFOptions);
      if s='' then
        s := 'RichViewCheckpoint'+RVIntToStr(CPIndex);
      RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('{\*\bkmkstart %s}{\*\bkmkend %s}',[s,s]));
    end;
    if (Level=0) and CompleteDocument and
      {$IFDEF RVRTFNOPARAFTERLASTTABLE}not ((EndItem>=0) and GetItem(EndItem).GetBoolValue(rvbpFullWidth)) and{$ENDIF}
      (not SelectionOnly or ((EndItem=ItemCount-1) and (EndOffs>=GetOffsAfterItem(EndItem)))) then
        RVFWrite(Stream, '\par');
    if (Level=0) and CompleteDocument then
      RVFWrite(Stream, '}');
  except
    Result := False;
  end;
  if (Level=0) and CompleteDocument then begin
    SavingData.RTFTables.Free;
    SavingData.StyleToFont.Free;
    SavingData.ListOverrideOffsetsList1.Free;
    SavingData.ListOverrideOffsetsList2.Free;
    if CallProgress then
      SaveProgressDone(rvloRTFWrite);
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCX}
procedure WriteXMLString(Stream: TStream; const Element, Text: TRVAnsiString);
var Pr: TRVAnsiString;
begin
  if (Text<>'') and ((Text[1]=' ') or (Text[Length(Text)]=' ')) then
    Pr := ' xml:space="preserve"'
  else
    Pr := '';
  RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('<%s%s>%s</%s>',
    [Element, Pr, Text, Element]));
end;
{------------------------------------------------------------------------------}
function RVGetOOXMLFontStr(Font: TFont; AllProperties: Boolean): TRVAnsiString;
var FN: TRVAnsiString;
begin
  if AllProperties then begin
    FN := RV_MakeOOXMLStr(Font.Name);
    Result := '<w:rFonts w:ascii="'+FN+'" w:hAnsi="'+FN+'"/>'+
      RV_GetXMLBoolPropElement('b', fsBold in Font.Style)+
      RV_GetXMLBoolPropElement('i', fsItalic in Font.Style)+
      RV_GetXMLBoolPropElement('strike', fsStrikeout in Font.Style)+
      RV_GetXMLIntPropElement('sz', Font.Size*2)+
      RV_GetXMLColorPropElement('color', Font.Color);
      if fsUnderline in Font.Style then
        Result := Result+RV_GetXMLStrPropElement('u', 'single')
      else
        Result := Result+RV_GetXMLStrPropElement('u', 'none');
    end
  else begin
    Result := '';
    if AnsiCompareText(Font.Name, RVFONT_ARIAL)<>0 then begin
      FN := RV_MakeOOXMLStr(Font.Name);
      Result := Result+'<w:rFonts w:ascii="'+FN+'" w:hAnsi="'+FN+'"/>';
    end;
    if fsBold in Font.Style then
      Result := Result+RV_GetXMLBoolPropElement('b', True);
    if fsItalic in Font.Style then
      Result := Result+RV_GetXMLBoolPropElement('i', True);
    if fsStrikeout in Font.Style then
      Result := Result+RV_GetXMLBoolPropElement('strike', True);
    if fsUnderline in Font.Style then
      Result := Result+RV_GetXMLStrPropElement('u', 'single');
    if Font.Size<>10 then
      Result := Result + RV_GetXMLIntPropElement('sz', Font.Size*2);
    if Font.Color<>clWindowText then
      Result := Result + RV_GetXMLColorPropElement('color', Font.Color);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.ShouldSaveTextToDocX(StyleNo: Integer): Boolean;
begin
  Result := ShouldSaveTextToFormat(StyleNo, rvteoDocXCode) or
    ShouldSaveTextToFormat(StyleNo, rvteoDocXInRunCode);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSELISTS}
procedure TCustomRVData.SaveOOXMLListTable(Stream: TStream;
  SavingData: TRVDocXSavingData; Header, Footer,
  FirstPageHeader, FirstPageFooter, EvenPagesHeader, EvenPagesFooter: TCustomRVData);
var
    RVStyle: TRVStyle;

    {...................................................}
    procedure SaveNumPics;
    var i, j, Index: Integer;
        FileName: TRVAnsiString;
        LevelInfo: TRVListLevel;
        Gr: TGraphic;
        RelId: TRVAnsiString;
        Bmp: TGraphic;
    const FmtString =
      '<w:numPicBullet w:numPicBulletId="%d"><w:pict>'+
      '<v:shape id="numpic%d" '+
      'style="width:%s;height:%s" '+
      'o:bullet="t">'+
      '<v:imagedata r:id="%s" o:title="numpic%d"/></v:shape></w:pict></w:numPicBullet>';
    begin
      Index := 0;
      for i := 0 to RVStyle.ListStyles.Count-1 do
        for j := 0 to RVStyle.ListStyles[i].Levels.Count-1 do begin
          LevelInfo := RVStyle.ListStyles[i].Levels[j];
          if (LevelInfo.ListType = rvlstPicture) and (LevelInfo.Picture.Graphic<>nil) then begin
            SavingData.HasNumPics := True;
            Gr := LevelInfo.Picture.Graphic;
            case RVGraphicHandler.GetGraphicType(Gr) of
              rvgtIcon:
                begin
                  Gr := RVGraphicHandler.IconToPng(Gr);
                  if Gr=nil then begin
                    Gr := LevelInfo.Picture.Graphic;
                    Bmp := TBitmap.Create;
                    Bmp.Width := Gr.Width;
                    Bmp.Height := Gr.Height;
                    TBitmap(Bmp).Canvas.Draw(0, 0, Gr);
                    Gr := RVGraphicHandler.CreateGraphicByType(rvgtPNG);
                    if Gr<>nil then begin
                      Gr.Assign(Bmp);
                      Bmp.Free;
                      end
                    else
                      Gr := Bmp;
                  end
                end;
              rvgtBitmap:
                begin
                  Bmp := Gr;
                  Gr := RVGraphicHandler.CreateGraphicByType(rvgtPNG);
                  if Gr<>nil then
                    Gr.Assign(Bmp)
                  else
                    Gr := Bmp;
                end;
            end;
            try
              inc(SavingData.ImageIndex);
              FileName := 'img'+RVIntToStr(SavingData.ImageIndex)+'.'+
                TRVAnsiString(RVGraphicHandler.GetGraphicExt(Gr));
              SavingData.Catalogs[rvdocxpNumbering].Images.Add(String(FileName));
              SavingData.AddGraphic(Gr, FileName);
              RelId := 'prId'+RVIntToStr(SavingData.Catalogs[rvdocxpNumbering].Images.Count);
              RVFWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
                FmtString,
                [Index, Index,
                RVStyle.GetCSSSize(RVStyle.PixelsToUnits(Gr.Width)),
                RVStyle.GetCSSSize(RVStyle.PixelsToUnits(Gr.Height)),
                RelId, Index]));
              inc(Index);
            finally
              if Gr<>LevelInfo.Picture.Graphic then
                Gr.Free;
            end;
            end
          else if LevelInfo.ListType = rvlstImageList then begin
            SavingData.AddImageListImage(LevelInfo.ImageList, LevelInfo.ImageIndex,
              FileName, RelId);
            if RelId<>'' then begin
              SavingData.HasNumPics := True;
              RVFWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
                FmtString,
                [Index, Index,
                 RVStyle.GetCSSSize(RVStyle.PixelsToUnits(LevelInfo.ImageList.Width)),
                 RVStyle.GetCSSSize(RVStyle.PixelsToUnits(LevelInfo.ImageList.Height)),
                 RelId, Index]));
              inc(Index);
            end;
          end;
        end;
    end;
    {...................................................}
    function GetLevelNumFormatStr(LevelInfo: TRVListLevel): TRVAnsiString;
    begin
      case LevelInfo.ListType of
        rvlstBullet,
        {$IFNDEF RVDONOTUSEUNICODE}
        rvlstUnicodeBullet,
        {$ENDIF}
        rvlstPicture, rvlstImageList:
          Result := 'bullet';
        rvlstDecimal,rvlstImageListCounter:
          Result := 'decimal';
        rvlstLowerAlpha:
          Result := 'lowerLetter';
        rvlstUpperAlpha:
          Result := 'upperLetter';
        rvlstLowerRoman:
          Result := 'lowerRoman';
        rvlstUpperRoman:
          Result := 'upperRoman';
        else
          Result := 'none';
      end;
    end;
    {...................................................}
    function GetLevelAlignStr(LevelInfo: TRVListLevel): TRVAnsiString;
    begin
      case LevelInfo.MarkerAlignment of
        rvmaCenter:
          Result := 'center';
        rvmaRight:
          Result := 'right';
        else
          Result := 'left';
      end;
    end;
    {...................................................}
    function GetLevelText(LevelInfo: TRVListLevel): TRVAnsiString;
    begin
      case LevelInfo.ListType of
        rvlstBullet:
          Result := RV_MakeOOXMLStr(LevelInfo.FormatString);
        {$IFNDEF RVDONOTUSEUNICODE}
        {$IFDEF RICHVIEWCBDEF3}
        rvlstUnicodeBullet:
          Result := RV_MakeOOXMLStrW(LevelInfo.FormatStringW);
        {$ENDIF}
        {$ENDIF}
        rvlstDecimal,rvlstImageListCounter,
        rvlstLowerAlpha,rvlstUpperAlpha,
        rvlstLowerRoman,rvlstUpperRoman:
          Result := RV_MakeOOXMLStr(Format(LevelInfo.FormatString, ['%1', '%2', '%3', '%4', '%5',
          '%6', '%7', '%8', '%9']));
        else
          Result := '';
      end;
    end;
    {...................................................}
    // Returns RVData in the same order as their bodies are saved in RTF
    function GetRVDataByIndex(RVDataIndex: Integer; var RVData: TCustomRVData): Boolean;
    begin
      Result := True;
      case RVDataIndex of
        0: RVData := Header;
        1: RVData := Footer;
        2: RVData := FirstPageHeader;
        3: RVData := FirstPageFooter;
        4: RVData := EvenPagesHeader;
        5: RVData := EvenPagesFooter;
        6: RVData := Self;
        else Result := False;
      end;
    end;
    {...................................................}
    function GetParaProperties(LevelInfo: TRVListLevel): TRVAnsiString;
    var LeftIndent, FirstLineIndent, TabIndent: Integer;
        FirstLineName: TRVAnsiString;
    begin
      LeftIndent := RVStyle.GetAsTwips(LevelInfo.LeftIndent);
      FirstLineIndent := RVStyle.GetAsTwips(LevelInfo.MarkerIndent-LevelInfo.LeftIndent);
      if FirstLineIndent>=0 then
        FirstLineName := 'firstLine'
      else begin
        FirstLineIndent := -FirstLineIndent;
        FirstLineName := 'hanging';
      end;
      TabIndent := RVStyle.GetAsTwips(LevelInfo.FirstIndent+LevelInfo.LeftIndent);
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '<w:pPr><w:ind w:left="%d" w:%s="%d"/><w:tab w:val="num" w:pos="%d"/></w:pPr>',
        [LeftIndent, FirstLineName, FirstLineIndent, TabIndent]);
    end;
    {...................................................}
    function GetListLevelStr(LevelInfo: TRVListLevel;
      ListNo, LevelNo: Integer; var NumPicIndex: Integer; CanIncrementNumPic: Boolean): TRVAnsiString;
    var s: TRVAnsiString;
        Idx: Integer;
    begin
      if ((LevelInfo.ListType=rvlstPicture) and (LevelInfo.Picture.Graphic<>nil)) or
         ((LevelInfo.ListType=rvlstImageList) and (LevelInfo.ImageList<>nil) and
          (LevelInfo.ImageIndex>=0) and (LevelInfo.ImageIndex<LevelInfo.ImageList.Count)) then begin
        if CanIncrementNumPic then
          Idx := NumPicIndex
        else
          Idx := NumPicIndex-1;
        Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '<w:lvl w:ilvl="%d"><w:numFmt w:val="bullet"/><w:lvlPicBulletId w:val="%d"/>'+
        '<w:lvlText w:val="*"/><w:lvlJc w:val="%s"/>',
        [LevelNo, Idx, GetLevelAlignStr(LevelInfo)]);
        if CanIncrementNumPic then
          inc(NumPicIndex);
        end
      else begin
        Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
          '<w:lvl w:ilvl="%d"><w:numFmt w:val="%s"/><w:lvlText w:val="%s"/><w:lvlJc w:val="%s"/>',
          [LevelNo, GetLevelNumFormatStr(LevelInfo), GetLevelText(LevelInfo),
          GetLevelAlignStr(LevelInfo)]);
        if LevelInfo.HasNumbering then
          Result := Result + RV_GetXMLIntPropElement('start', LevelInfo.StartFrom);
        if rvloLegalStyleNumbering in LevelInfo.Options then
          Result := Result + RV_GetXMLBoolPropElement('isLgl', True);
        if not (rvloLevelReset in LevelInfo.Options) then
          Result := Result + RV_GetXMLIntPropElement('lvlRestart', 0);
      end;
      Result := Result + GetExtraDocXCode(rv_docxs_ListStyle, RVStyle.ListStyles[ListNo],
        ListNo, LevelNo);
      Result := Result + GetParaProperties(LevelInfo);
      if LevelInfo.UsesFont then begin
        s := RVGetOOXMLFontStr(LevelInfo.Font,
          {$IFNDEF RVDONOTUSESTYLETEMPLATES}UseStyleTemplates{$ELSE}False{$ENDIF});
        if s<>'' then
          Result := Result + '<w:rPr>'+s+'</w:rPr>';
      end;
      Result := Result + '</w:lvl>';
    end;
    {...................................................}
    function GetListTypeStr(ListInfo: TRVListInfo): TRVAnsiString;
    var i: Integer;
        AllNumbering: Boolean;
        AllBullets: Boolean;
    begin
      Result := 'hybridMultilevel';
      if ListInfo.Levels.Count=0 then
        exit;
      if ListInfo.Levels.Count=1 then begin
        Result := 'singleLevel';
        exit;
      end;
      AllNumbering := True;
      AllBullets := True;
      for i := 0 to ListInfo.Levels.Count-1 do
        if ListInfo.Levels[i].HasNumbering then
          AllBullets := False
        else
          AllNumbering := False;
      if AllNumbering or AllBullets then
        Result := 'multilevel';
    end;
    {...................................................}
    procedure SaveListTable;
    var i, j, idx, levelcount, NumPicIndex: Integer;
        LevelInfo: TRVListLevel;
    begin
      SaveNumPics;
      NumPicIndex := 0;
      for i := 0 to RVStyle.ListStyles.Count-1 do begin
        RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
          '<w:abstractNum w:abstractNumId="%d">',[i]));
        RVFWrite(Stream,
          RV_GetXMLStrPropElement('multiLevelType', GetListTypeStr(RVStyle.ListStyles[i])));
        RVFWrite(Stream, GetExtraDocXCode(rv_docxs_ListStyle, RVStyle.ListStyles[i], i, -1));
        levelcount := RVStyle.ListStyles[i].Levels.Count;
        if levelcount>1 then
          levelcount := 9;
        for j := 0 to levelcount-1 do begin
          // writing list level
          if j<RVStyle.ListStyles[i].Levels.Count then
            idx := j
          else
            idx := RVStyle.ListStyles[i].Levels.Count-1;
          LevelInfo := RVStyle.ListStyles[i].Levels[idx];
          RVFWrite(Stream, GetListLevelStr(LevelInfo, i, j, NumPicIndex,
            idx=j));
        end;
        RVFWrite(Stream, '</w:abstractNum>');
      end;
    end;
    {...................................................}
    // writing list override table (in the order as subdocuments are saved in RTF)
    procedure SaveListOverrideTable;
    var i,j,Index, RVDataIndex: Integer;
        Markers: TRVMarkerList;
        Marker: TRVMarkerItemInfo;
        RVData: TCustomRVData;
    begin
      index := 1;
      for i := 0 to RVStyle.ListStyles.Count-1 do begin
        RVFWrite(Stream,
          {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
            '<w:num w:numId="%d"><w:abstractNumId w:val="%d"/></w:num>', [index, i]));
        inc(index);
        RVDataIndex := 0;
        while GetRVDataByIndex(RVDataIndex, RVData) do begin
          if RVData<>nil then begin
            Markers := RVData.GetMarkers(False);
            if (Markers<>nil) and (SavingData.ListOverrideOffsetsList1[i]>1) then begin
              for j := 0 to Markers.Count-1 do begin
                Marker := Markers[j];
                if (Marker.ListNo=i) and (Marker.Level>=0) and Marker.Reset then begin
                  RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
                    '<w:num w:numId="%d"><w:abstractNumId w:val="%d"/>'+
                    '<w:lvlOverride w:ilvl="%d"><w:startOverride w:val="%d"/></w:lvlOverride></w:num>',
                    [index, i, Marker.Level, Marker.StartFrom]));
                  inc(index);
                end;
              end;
            end;
          end;
          inc(RVDataIndex);
        end;
      end;
    end;
    {...................................................}
    // transforming a list of counts to a list of offsets
    procedure FinalizeListOverrideTable;
    var i, prevcount,curcount: Integer;
    begin
      if RVStyle.ListStyles.Count>0 then begin
        prevcount := SavingData.ListOverrideOffsetsList1[0];
        SavingData.ListOverrideOffsetsList1[0] := 1; // starting from 1
        for i := 1 to RVStyle.ListStyles.Count-1 do begin
          curcount := SavingData.ListOverrideOffsetsList1[i];
          SavingData.ListOverrideOffsetsList1[i] := SavingData.ListOverrideOffsetsList1[i-1]+prevcount;
          prevcount := curcount;
        end;
      end;
      SavingData.ListOverrideOffsetsList2.Assign(SavingData.ListOverrideOffsetsList1);
    end;
    {...................................................}
var Part: TRVDocXPart;
begin
  Part := SavingData.CurPart;
  try
    SavingData.CurPart := rvdocxpNumbering;
    RVStyle := GetRVStyle;
    RVFWriteLine(Stream, RVDOCX_XMLSTART);
    RVFWrite(Stream, '<w:numbering '+ RV_MakeXMLNSList(['ve', 'o', 'r', 'm', 'v', 'wp', 'w10', 'w', 'wne'])+'>');
    SaveListTable;
    SaveListOverrideTable;
    RVFWrite(Stream, '</w:numbering>');
    FinalizeListOverrideTable;
  finally
    SavingData.CurPart := Part;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveOOXMLFontTableToStream(Stream: TStream);
var i: Integer;
  RTFTables: TCustomRVRTFTables;
  FontTable: TRVRTFFontTable;
  StyleToFont: TRVIntegerList;
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  StyleTemplateToFont: TRVIntegerList;
  {$ENDIF}
begin
  RTFTables := TRVRTFTables.Create;
  try
    StyleToFont := nil;
    MakeFontTable(RTFTables, StyleToFont
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}, StyleTemplateToFont{$ENDIF});
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    StyleTemplateToFont.Free;
    {$ENDIF}
    FontTable := TRVRTFTables(RTFTables).FFontTable;
    RVFWriteLine(Stream, RVDOCX_XMLSTART);
    RVFWrite(Stream, '<w:fonts '+ RV_MakeXMLNSList(['r', 'w'])+'>');
    for i := 0 to FontTable.Count-1 do
      RVFWrite(Stream,
        {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
          '<w:font w:name="%s"><w:charset w:val="%s"/></w:font>',
        [StringToHTMLString3(FontTable[i].FontName, True, CP_ACP),
         RVIntToHex(FontTable[i].Charset, 2)]));
    RVFWrite(Stream, '</w:fonts>');
  finally
    RTFTables.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveOOXMLStylesToStream(Stream: TStream);
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
var i: Integer;
    RVStyle: TRVStyle;
    DefaultSaved: Boolean;
    s: TRVAnsiString;
    {$IFNDEF RVDONOTUSETABS}
    Tabs: TRVTabInfos;
    {$ENDIF}
    StyleTemplate: TRVStyleTemplate;
{$ENDIF}
  {.........................................................}
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  procedure WriteSTProperties(StyleTemplate: TRVStyleTemplate; LinkedCS: Boolean;
    StyleTemplateIndex: Integer);
  var s: String;
      Str: TRVAnsiString;
  begin
    s := StyleTemplate.Name;
    if LinkedCS then
      s := RVStyle.StyleTemplates.MakeNameUnique(s+' Text');
    RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      '<w:name w:val="%s"/>', [RV_MakeOOXMLStr(s)]));
    if StyleTemplate.QuickAccess then
      RVFWrite(Stream, '<w:qFormat/>');
    Str := '';
    case StyleTemplate.Kind of
      rvstkText:
        if StyleTemplate.Parent=nil then
          Str := 'def'
        else
          Str := 'c'+RVIntToStr(StyleTemplate.Parent.Index);
      rvstkPara:
        if StyleTemplate.Parent<>nil then
          Str := RVIntToStr(StyleTemplate.Parent.Index);
      rvstkParaText:
        if LinkedCS then
          if (StyleTemplate.Parent=nil) or (StyleTemplate.Parent=RVStyle.StyleTemplates.NormalStyleTemplate) then
            Str := 'def'
          else
            Str := 'c'+RVIntToStr(StyleTemplate.Parent.Index)
        else if StyleTemplate.Parent<>nil then
          Str := RVIntToStr(StyleTemplate.Parent.Index);
    end;
    if Str<>'' then
      RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '<w:basedOn w:val="%s"/>', [Str]));
    if ((StyleTemplate.Kind=rvstkPara) or
      ((StyleTemplate.Kind=rvstkParaText) and not LinkedCS)) and (StyleTemplate.Next<>nil) then
      RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '<w:next w:val="%s"/>', [RVIntToStr(StyleTemplate.Next.Index)]));
    if StyleTemplate.Kind=rvstkParaText then begin
      Str := RVIntToStr(StyleTemplate.Index);
      if not LinkedCS then
        Str := 'c'+Str;
      RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '<w:link w:val="%s"/>', [Str]));
    end;
    RVFWrite(Stream, GetExtraDocXCode(rv_docxs_StyleTemplate, StyleTemplate,
      StyleTemplateIndex, -1));
  end;
  {$ENDIF}
  {.........................................................}

begin
  RVFWriteLine(Stream, RVDOCX_XMLSTART);
  RVFWrite(Stream, '<w:styles '+ RV_MakeXMLNSList(['r', 'w'])+'>');
  RVFWrite(Stream, '<w:docDefaults>'+
    '<w:rPrDefault><w:pPr><w:spacing w:after="0" w:before="0" w:line="240" w:lineRule="auto"/>'+
    '</w:pPr></w:rPrDefault>'+
    '<w:rPrDefault><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial"/>'+
    '<w:sz w:val="20"/></w:rPr></w:rPrDefault></w:docDefaults>');
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if UseStyleTemplates then begin
    RVStyle := GetRVStyle;
    DefaultSaved := False;
    for i := 0 to RVStyle.StyleTemplates.Count-1 do begin
      StyleTemplate := RVStyle.StyleTemplates[i];
      if (StyleTemplate.Kind<>rvstkPara) and not DefaultSaved then begin
        RVFWrite(Stream,
          '<w:style w:type="character" w:default="1" w:styleId="def">'+
          '<w:name w:val="Default Paragraph Font"/><w:uiPriority w:val="1"/>'+
          '<w:semiHidden/><w:unhideWhenUsed/></w:style>');
        DefaultSaved := True;
      end;
      if StyleTemplate.Kind<>rvstkText then begin
        if StyleTemplate=RVStyle.StyleTemplates.NormalStyleTemplate then
          s := ' w:default="1"'
        else
          s := '';
        RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
           '<w:style w:type="paragraph"%s w:styleId="%d">',[s, i]));
        WriteSTProperties(StyleTemplate, False, i);
        {$IFNDEF RVDONOTUSETABS}
        if StyleTemplate.Parent<>nil then
          Tabs := StyleTemplate.Parent.GetTabs
        else
          Tabs := nil;
        {$ENDIF}
        s := StyleTemplate.ParaStyle.GetOOXMLStr(RVStyle,
          {$IFNDEF RVDONOTUSETABS}0, Tabs,{$ENDIF}
          StyleTemplate.ValidParaProperties)+
          GetExtraDocXCode(rv_docxs_StyleTemplatePara, StyleTemplate, i, -1);
        if s<>'' then
          RVFWrite(Stream, '<w:pPr>'+s+'</w:pPr>');
        s := StyleTemplate.TextStyle.GetOOXMLStr(RVStyle, StyleTemplate.ValidTextProperties)+
          GetExtraDocXCode(rv_docxs_StyleTemplateText, StyleTemplate, i, -1);
        if s<>'' then
          RVFWrite(Stream, '<w:rPr>'+s+'</w:rPr>');
        RVFWrite(Stream, '</w:style>');
      end;
      if StyleTemplate.Kind<>rvstkPara then begin
        RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
             '<w:style w:type="character" w:styleId="c%d">',[i]));
        WriteSTProperties(StyleTemplate, StyleTemplate.Kind=rvstkParaText, i);
        s := StyleTemplate.TextStyle.GetOOXMLStr(RVStyle, StyleTemplate.ValidTextProperties)+
          GetExtraDocXCode(rv_docxs_StyleTemplateText, StyleTemplate, i, -1);
        if s<>'' then
          RVFWrite(Stream, '<w:rPr>'+s+'</w:rPr>');
        RVFWrite(Stream, '</w:style>');
      end;
    end;
    end
  else
  {$ENDIF}
    RVFWrite(Stream, '<w:style w:type="paragraph" w:default="1" w:styleId="0"><w:name w:val="Normal"/><w:qFormat/></w:style>');
  RVFWrite(Stream, '</w:styles>');
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetOOXMLCheckpointStr(CP: TRVCPInfo;
  SavingData: TRVDocXSavingData): TRVAnsiString;
var BookmarkName: TRVAnsiString;
begin
  if CP.Name='' then
    exit;
  BookmarkName := RV_MakeOOXMLStr(MakeRTFBookmarkNameStr(CP.Name));
  Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:bookmarkStart w:id="%d" w:name="%s"/><w:bookmarkEnd w:id="%d"/>',
    [SavingData.BookmarkIndex, BookmarkName, SavingData.BookmarkIndex]);
  inc(SavingData.BookmarkIndex);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveOOXMLDocumentSettingsToStream(Stream: TStream): Boolean;
var s: TRVAnsiString;
    {$IFNDEF RVDONOTUSEDOCPARAMS}
    SaveDocParams: Boolean;
    {$ENDIF}
begin
  Result := True;
  s := GetExtraDocXCode(rv_docxs_Settings, nil, -1, -1);
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  SaveDocParams := (rvrtfSaveDocParameters in RTFOptions) and (GetDocParameters(False)<>nil);
  {$ENDIF}
  RVFWriteLine(Stream, RVDOCX_XMLSTART);
  RVFWrite(Stream, '<w:settings '+ RV_MakeXMLNSList(['o', 'r', 'm', 'v', 'w10', 'w'])+'>');
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  if SaveDocParams then
    GetDocParameters(False).SaveOOXMLDocumentSettings(Stream);
  {$ENDIF}
  RVFWrite(Stream, s+
    '<w:compat><w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word" w:val="15"/>'+
    '</w:compat></w:settings>');
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SaveOOXMLCorePropertiesToStream(Stream: TStream): Boolean;
var s: TRVAnsiString;
    {$IFNDEF RVDONOTUSEDOCPARAMS}
    SaveDocParams: Boolean;
    {$ENDIF}
begin
  s := GetExtraDocXCode(rv_docxs_CoreProperties, nil, -1, -1);
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  SaveDocParams := (rvrtfSaveDocParameters in RTFOptions) and (GetDocParameters(False)<>nil)
    and GetDocParameters(False).HasOOXMLCoreProperties;
  Result := SaveDocParams or (s<>'');
  {$ELSE}
  Result := s<>'';
  {$ENDIF}
  if not Result then
    exit;
  RVFWriteLine(Stream, RVDOCX_XMLSTART);
  RVFWrite(Stream, '<cp:coreProperties '+ RV_MakeXMLNSList(['cp', 'dc'])+'>');
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  if SaveDocParams then
    GetDocParameters(False).SaveOOXMLCoreProperties(Stream);
  {$ENDIF}
  RVFWrite(Stream, s+'</cp:coreProperties>');
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SaveOOXMLDocumentToStream(Stream: TStream; const Path: String;
  SelectionOnly, RootDocument, InCell, HiddenParent: Boolean; Color: TColor; Background: TRVBackground;
  SavingData: TRVDocXSavingData);
var RVStyle: TRVStyle;
    LastParaNo: Integer;
    Progress: Integer;
    CallProgress: Boolean;
var i: Integer;
    item: TCustomRVItemInfo;
    StartItem,EndItem,StartOffs,EndOffs, FirstParaFirstItemNo: Integer;
    UrlTargetCur, UrlLocalTargetCur, UrlExtrasCur: String;
    NotUsedPart: TRVMultiDrawItemPart;
    SelectedItem: TCustomRVItemInfo;
    TextOptions: TRVTextOptions;
    {.................................................................}
    function GetRootEl: TRVAnsiString;
    begin
      case SavingData.CurPart of
        rvdocxpHeader, rvdocxpHeader1, rvdocxpHeaderEven: Result := 'hdr';
        rvdocxpFooter, rvdocxpFooter1, rvdocxpFooterEven: Result := 'ftr';
        else Result := 'document';
      end;
    end;
    {.................................................................}
    procedure WriteParaProps(ParaNo: Integer; Item: TCustomRVItemInfo;
      ItemNo: Integer);
    var s: TRVAnsiString;
        Para: TParaInfo;
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}
        ST: TRVStyleTemplate;
        {$ENDIF}
        {$IFNDEF RVDONOTUSETABS}
        ParentTabs: TRVTabInfos;
        MinAllowedTabPos: TRVStyleLength;
        {$ENDIF}
        {$IFNDEF RVDONOTUSELISTS}
        Marker: TRVMarkerItemInfo;
        LevelInfo: TRVListLevel;
        ListOverrideNo: Integer;
        FirstParaItem: TCustomRVItemInfo;
        {$ENDIF}
        {$IFDEF RVDONOTUSESTYLETEMPLATES}
        ModifiedProperties: TRVParaInfoProperties;
        {$ENDIF}
    begin
      {$IFNDEF RVDONOTUSELISTS}
      if FirstParaFirstItemNo>=0 then
        FirstParaItem := GetItem(FirstParaFirstItemNo)
      else
        FirstParaItem := Item;
      {$ENDIF}
      s := '';
      Para := RVStyle.ParaStyles[ParaNo];
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      if UseStyleTemplates then begin
        ST := RVStyle.StyleTemplates.FindItemById(Para.StyleTemplateId);
        if (ST<>nil) and (ST<>RVStyle.StyleTemplates.NormalStyleTemplate) then
          s := s + RV_GetXMLIntPropElement('pStyle', ST.Index);
        ST.UpdateModifiedParaStyleProperties(Para);
        {$IFNDEF RVDONOTUSETABS}
        ParentTabs := ST.GetTabs;
        {$ENDIF}
        end
      else
      {$ENDIF}
      begin
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}Para.{$ENDIF}ModifiedProperties :=
          Para.CalculateModifiedProperties2(RVAllParaInfoProperties);
        {$IFNDEF RVDONOTUSETABS}
        ParentTabs := nil;
        {$ENDIF}
      end;
      {$IFNDEF RVDONOTUSELISTS}
      if (FirstParaItem.StyleNo=rvsListMarker) and
         (TRVMarkerItemInfo(FirstParaItem).GetLevelInfo(RVStyle)<>nil) then begin
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}Para.{$ENDIF}ModifiedProperties :=
          {$IFNDEF RVDONOTUSESTYLETEMPLATES}Para.{$ENDIF}ModifiedProperties-
          [rvpiFirstIndent, rvpiLeftIndent];
        {$IFNDEF RVDONOTUSETABS}
        with TRVMarkerItemInfo(FirstParaItem).GetLevelInfo(RVStyle) do
          MinAllowedTabPos := FirstIndent+LeftIndent;
        {$ENDIF}
        end
      {$IFNDEF RVDONOTUSETABS}
      else
        MinAllowedTabPos := 0
      {$ENDIF};
      {$ELSE}
      MinAllowedTabPos := 0;
      {$ENDIF}
      s := s+Para.GetOOXMLStr(RVStyle, {$IFNDEF RVDONOTUSETABS}MinAllowedTabPos, ParentTabs,{$ENDIF}
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}Para.{$ENDIF}ModifiedProperties);
      {$IFNDEF RVDONOTUSELISTS}
      if FirstParaItem.StyleNo=rvsListMarker then begin
        Marker := TRVMarkerItemInfo(FirstParaItem);
        LevelInfo := Marker.GetLevelInfo(RVStyle);
        if LevelInfo<>nil then begin
          if Marker.Reset and LevelInfo.HasNumbering then begin
            SavingData.ListOverrideOffsetsList1[Marker.ListNo] :=
              SavingData.ListOverrideOffsetsList1[Marker.ListNo]+1;
            ListOverrideNo := SavingData.ListOverrideOffsetsList1[Marker.ListNo];
            end
          else
            ListOverrideNo := SavingData.ListOverrideOffsetsList2[Marker.ListNo];
          s := s + {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
            '<w:numPr><w:ilvl w:val="%d"/><w:numId w:val="%d"/></w:numPr>',
            [Marker.Level, ListOverrideNo]);
        end;
      end;
      {$ENDIF}
      s := s + GetExtraDocXCode(rv_docxs_ParaStyle, Para, ParaNo,
          -1);
      if HiddenParent or IsHiddenParaEnd(ItemNo) then
        s := s + '<w:rPr><w:vanish/></w:rPr>';
      if s<>'' then
         RVFWrite(Stream, '<w:pPr>'+s+'</w:pPr>');
      FirstParaFirstItemNo := -1;
    end;
    {.................................................................}
    procedure WriteRunProps(StyleNo, ItemNo: Integer);
    var s: TRVAnsiString;
        TextStyle: TFontInfo;
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}
        ParaST, ST: TRVStyleTemplate;
        {$ENDIF}
        {$IFDEF RVDONOTUSESTYLETEMPLATES}
        ModifiedProperties: TRVFontInfoProperties;
        {$ENDIF}
    begin
      s := '';
      TextStyle := RVStyle.TextStyles[StyleNo];
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      if UseStyleTemplates then begin
        ST := RVStyle.StyleTemplates.FindItemById(TextStyle.StyleTemplateId);
        ParaST := RVStyle.StyleTemplates.FindItemById(TextStyle.ParaStyleTemplateId);
        if ST<>nil then
          s := s + RV_GetXMLStrPropElement('rStyle', 'c'+RVIntToStr(ST.Index));
        ST.UpdateModifiedTextStyleProperties(TextStyle, ParaST{, False});
        end
      else
      {$ENDIF}
      begin
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}TextStyle.{$ENDIF}ModifiedProperties :=
          TextStyle.CalculateModifiedProperties2(RVAllFontInfoProperties{, False});
      end;
      s := s+TextStyle.GetOOXMLStr(RVStyle,
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}TextStyle.{$ENDIF}ModifiedProperties)+
        GetExtraDocXCode(rv_docxs_TextStyle, TextStyle, StyleNo, -1);
      if HiddenParent or IsHiddenItem(ItemNo) then
        s := s + '<w:vanish/>';
      if s<>'' then
         RVFWrite(Stream, '<w:rPr>'+s+'</w:rPr>');
    end;
    {.................................................................}
    procedure CloseHyperlink;
    begin
      if (UrlTargetCur='') and (UrlLocalTargetCur='') then
        exit;
      RVFWrite(Stream, '</w:hyperlink>');
      UrlTargetCur := '';
      UrlLocalTargetCur := '';
      UrlExtrasCur := '';
    end;
    {.................................................................}
    procedure TryOpenHyperlink(Item: TCustomRVItemInfo);
    var
      UrlTarget, UrlLocalTarget, UrlExtras: String;
      s: TRVAnsiString;
    begin
      if Item.GetBoolValueEx(rvbpJump, RVStyle) then begin
        WriteHyperlink(Item.JumpID+FirstJumpNo, Self, i, rvsfDocX,
          UrlTarget, UrlExtras);
        CalcLocalURL(UrlTarget, UrlLocalTarget);
        if (Length(UrlTarget)>=2) and (UrlTarget[2]=':') then
          UrlTarget := 'file:///'+UrlTarget;
        if (UrlTargetCur=UrlTarget) and (UrlLocalTargetCur=UrlLocalTarget) and
          (UrlExtrasCur=UrlExtras) then
          exit;
        CloseHyperlink;
        UrlTargetCur := UrlTarget;
        UrlLocalTargetCur := UrlLocalTarget;
        UrlExtrasCur := UrlExtras;
        if (UrlTarget='') and (UrlLocalTarget='') then
          exit;
        if UrlExtras<>'' then
          UrlExtras := ' '+UrlExtras;
        s := '<w:hyperlink';
        if UrlTarget<>'' then begin
          SavingData.CurCatalog.Hyperlinks.Add(UrlTarget);
          RV_AddStrA(s, 'r:id="hrId'+RVIntToStr(SavingData.CurCatalog.Hyperlinks.Count)+'"');
        end;
        if UrlExtras<>'' then
          {$IFDEF RVUNICODESTR}
          RV_AddStrA(s, Utf8Encode(UrlExtras));
          {$ELSE}
          RV_AddStrA(s, RVU_AnsiToUTF8(CP_ACP, UrlExtras));
          {$ENDIF}
        if UrlLocalTarget<>'' then
          RV_AddStrA(s, 'w:anchor="'+RV_MakeOOXMLStr(UrlLocalTarget)+'"');
        s := s+'>';
        RVFWrite(Stream, s);
        end
      else
        CloseHyperlink;
    end;
    {.................................................................}
    procedure SavePageProperties;
    var s: TRVAnsiString;
      {$IFNDEF RVDONOTUSEDOCPARAMS}
      SaveDocParameters: Boolean;
      {$ENDIF}
    begin
      if SavingData.CurPart<>rvdocxpMain then
        exit;
      s := GetExtraDocXCode(rv_docxs_SectProps, nil, -1, -1);
      {$IFNDEF RVDONOTUSEDOCPARAMS}
      SaveDocParameters := (rvrtfSaveDocParameters in RTFOptions) and (GetDocParameters(False)<>nil);
      {$ENDIF}
      if {$IFNDEF RVDONOTUSEDOCPARAMS}SaveDocParameters or {$ENDIF}
        (SavingData.SectProps<>'') or (s<>'') then begin
        RVFWrite(Stream, '<w:sectPr>');
        RVFWrite(Stream, SavingData.SectProps);
        {$IFNDEF RVDONOTUSEDOCPARAMS}
        if SaveDocParameters then
          GetDocParameters(False).SaveOOXMLSectionProperties(Stream);
        {$ENDIF}
        RVFWrite(Stream, s);
        RVFWrite(Stream, '</w:sectPr>');
      end;
    end;
    {.................................................................}
    function GetTextItemStr(ItemNo: Integer; Item: TCustomRVItemInfo;
      SpecialCode: Boolean): TRVAnsiString;
    var s: TRVRawByteString;
    begin
      s := Items[ItemNo];
      if (ItemNo=StartItem) then
        if (ItemNo=EndItem) then
          s := RVU_Copy(s, StartOffs, EndOffs-StartOffs, Item.ItemOptions)
        else
          s := RVU_Copy(s, StartOffs,
            RVU_Length(s, Item.ItemOptions)-StartOffs+1, Item.ItemOptions)
      else
        if ItemNo=EndItem then
          s := RVU_Copy(s, 1, EndOffs-1, Item.ItemOptions);
      if SaveItemToFile(Path, Self, ItemNo, rvsfDocX, True, s) then
        Result := UTF8Encode(RVU_RawUnicodeToWideString(s))
      else begin
      {$IFNDEF RVDONOTUSEUNICODE}
        if not SpecialCode then
          Result := RV_MakeOOXMLStrW(GetItemTextExW(i, s))
        else
          Result := UTF8Encode(GetItemTextExW(i, s));
      {$ELSE}
        if not SpecialCode then
          Result := RV_MakeOOXMLStrEx(s, GetItemCodePage(i))
        else
          Result := RVU_AnsiToUTF8(GetItemCodePage(i), s);
      {$ENDIF}
      end;
    end;
    {.................................................................}
    procedure SaveNonTextItem(ItemNo: Integer);
    var s: TRVRawByteString;
    begin
      s := '';
      if SaveItemToFile(Path, Self, ItemNo, rvsfDocX, True, s) then
        RVFWrite(Stream, UTF8Encode(RVU_RawUnicodeToWideString(s)))
      else
        GetItem(ItemNo).SaveOOXML(Stream, Self, ItemNo, SavingData, HiddenParent);
    end;
    {.................................................................}
    procedure CloseBody;
    begin
      if RootDocument then begin
        SavePageProperties;
        RVFWrite(Stream, '</w:body></w:'+GetRootEl+'>');
      end;
    end;
    {.................................................................}
begin
  RVStyle := GetRVStyle;
  UrlTargetCur := '';
  UrlLocalTargetCur := '';
  UrlExtrasCur := '';
  Progress := 0;
  if RootDocument then begin
    RVFWriteLine(Stream, RVDOCX_XMLSTART);
    RVFWrite(Stream, '<w:'+GetRootEl+' '+
      RV_MakeXMLNSList(['o', 'r', 'm', 'v', 'wp', 'w10', 'w', 'wp14', 'wne', 'wps', 'mc'])+
      ' mc:Ignorable="wp14">');
    if (Color<>clNone) and (Color<>clWindow) and (SavingData.CurPart=rvdocxpMain) then
      RVFWrite(Stream, '<w:background w:color="'+RV_GetColorHex(Color, True)+'"/>');
    RVFWrite(Stream, '<w:body>');
  end;
  RVFGetLimits(GetRVFSaveScope(SelectionOnly),StartItem,EndItem,StartOffs,EndOffs,NotUsedPart,NotUsedPart, SelectedItem);
  if (SelectedItem<>nil) or (StartItem=-1) or (StartItem>EndItem) then begin
    CloseBody;
    exit;
  end;
  CallProgress := RootDocument and (SavingData.CurPart=rvdocxpMain) and
      SaveProgressInit(Progress,  EndItem-StartItem, rvloDocXWrite);
  if RootDocument and (SavingData.CurPart=rvdocxpMain) then
    SavingData.CallProgress := CallProgress;
  LastParaNo := -1;
  FirstParaFirstItemNo := GetFirstParaItem(StartItem);
  for i := StartItem to EndItem do begin
    if (i=StartItem) and (GetItemStyle(i)<0) and (StartOffs>=GetOffsAfterItem(i)) then
      continue;
    if (i=EndItem) and (GetItemStyle(i)<0) and (EndOffs<=GetOffsBeforeItem(i)) then
      continue;
    if not ((StartItem=EndItem) and (GetItemStyle(StartItem)>=0)) then begin
      if (i=StartItem) and (StartOffs>=GetOffsAfterItem(i)) and (Items[i]<>'') then
        continue
      else if (i=EndItem) and (EndOffs<=GetOffsBeforeItem(i)) and (Items[i]<>'') then
        continue;
    end;
    Item := GetItem(i);
    if Item.PageBreakBefore then begin
      if LastParaNo>=0 then begin
        CloseHyperlink;
        RVFWrite(Stream, '</w:p>');
        LastParaNo := -1;
      end;
      RVFWrite(Stream, '<w:p><w:r><w:br w:type="page"/></w:r></w:p>');
    end;
    if not Item.GetBoolValue(rvbpDocXInRun) then begin
      if LastParaNo>=0 then begin
        CloseHyperlink;
        RVFWrite(Stream, '</w:p>');
      end;
      Item.SaveOOXML(Stream, Self, i, SavingData, HiddenParent);
      LastParaNo := -1;
      end
    else begin
      if ((i=StartItem) and (StartOffs<=GetOffsAfterItem(i))) or IsParaStart(i) then begin
        if LastParaNo>=0 then begin
          CloseHyperlink;
          RVFWrite(Stream, '</w:p>');
        end;
        RVFWrite(Stream, '<w:p>');
        LastParaNo := Item.ParaNo;
        WriteParaProps(LastParaNo, Item, i);
      end;
      TryOpenHyperlink(Item);
      if Item.BR then
        RVFWrite(Stream, '<w:r><w:br/></w:r>');
      if Item.Checkpoint<>nil then
        RVFWrite(Stream, GetOOXMLCheckpointStr(Item.Checkpoint, SavingData));
      if (Item.StyleNo>=0) and ShouldSaveTextToDocX(GetActualStyle(Item)) then begin
        TextOptions := RVStyle.TextStyles[GetActualStyle(Item)].Options;
        if rvteoDocXCode in TextOptions then
          RVFWrite(Stream, GetTextItemStr(i, Item, True))
        else begin
          RVFWrite(Stream, '<w:r>');
          WriteRunProps(GetActualStyle(Item), i);
          if rvteoDocXInRunCode in TextOptions then
            RVFWrite(Stream, GetTextItemStr(i, Item, True))
          else
            WriteXMLString(Stream, 'w:t', GetTextItemStr(i, Item, False));
          RVFWrite(Stream, '</w:r>');
        end;
        end
      else if {$IFNDEF RVDONOTUSELISTS}(Item.StyleNo<>rvsListMarker) and{$ENDIF}
        (Item.AssociatedTextStyleNo<0) or ShouldSaveTextToDocX(GetActualTextStyle(Item)) then begin
        Item.SaveOOXMLBeforeRun(Stream, Self, i, SavingData, HiddenParent);
        if not Item.GetBoolValue(rvbpDocXOnlyBeforeAfter) then begin
          RVFWrite(Stream, '<w:r>');
          if Item.AssociatedTextStyleNo>=0 then
            WriteRunProps(GetActualTextStyle(Item), i);
          SaveNonTextItem(i);
          RVFWrite(Stream, '</w:r>');
        end;
        Item.SaveOOXMLAfterRun(Stream, Self, i, SavingData, HiddenParent);
      end;
    end;
    if CallProgress then
      SaveProgressRun(Progress, i-StartItem, EndItem-StartItem, rvloDocXWrite);
  end;
  if LastParaNo>=0 then begin
    CloseHyperlink;
    RVFWrite(Stream, '</w:p>');
  end;
  if InCell and not GetItem(ItemCount-1).GetBoolValue(rvbpDocXInRun) then
    if HiddenParent then
      RVFWrite(Stream, '<w:p><w:pPr><w:rPr><w:vanish/></w:rPr></w:pPr></w:p>')
    else
      RVFWrite(Stream, '<w:p/>');
  CloseBody;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.GetParentData: TCustomRVData;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRootData: TCustomRVData;
begin
  Result := Self;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetAbsoluteParentData: TCustomRVData;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetAbsoluteRootData: TCustomRVData;
begin
  Result := Self;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TCustomRVData.GetNoteText: String;
begin
  Result := GetAbsoluteRootData.GetNoteText;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.DrainFrom(Victim: TCustomRVData);
var i: Integer;
    item: TCustomRVItemInfo;
begin
  if Victim=nil then exit;
  {$IFDEF RVOLDTAGS}
  if (rvoTagsArePChars in Options) <> (rvoTagsArePChars in Victim.Options) then
    raise ERichViewError.Create(errRVTagsTypesMismatch);
  {$ENDIF}
  for i := 0 to Victim.Items.Count-1 do begin
    item := Victim.GetItem(i);
    if item.SameAsPrev then
      item.ParaNo := -1;
    AddItemR(Victim.Items[i], item, True);
  end;
  if NotAddedCP=nil then
    NotAddedCP := Victim.NotAddedCP
  else
    Victim.NotAddedCP.Free;
  Victim.Items.Clear;
  Victim.FirstCP := nil;
  Victim.LastCP := nil;
  Victim.NotAddedCP := nil;
  Victim.CPCount := 0;
  Include(Victim.State, rvstPreserveSoftPageBreaks);
  try
    Victim.Clear;
  finally
    Exclude(Victim.State, rvstPreserveSoftPageBreaks);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemNo(Item: TCustomRVItemInfo): Integer;
begin
  Result := Items.IndexOfObject(Item);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.Inserting(RVData: TCustomRVData; Safe: Boolean);
var i: Integer;
    s: TRVRawByteString;
begin
  for i := 0 to Items.Count-1 do begin
    s := Items[i];
    GetItem(i).Inserting(RVData, s, Safe);
    Items[i] := s;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.Beep;
begin
  if (GetRVStyle<>nil) and (GetRVStyle.UseSound) then
    MessageBeep(MB_OK);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetParagraphStyleToAll(ParaNo: Integer);
var i: Integer;
begin
  for i := 0 to Items.Count-1 do
    if GetItemStyle(i)<>rvsBreak then
      GetItem(i).ParaNo := ParaNo;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRVData: TCustomRVData;
begin
  Result := Self;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetSourceRVData: TCustomRVData;
begin
  Result := Self;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItem(ItemNo: Integer): TCustomRVItemInfo;
begin
  Result := TCustomRVItemInfo(TRVItemListHack(Items).Get(ItemNo));
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetRTFProperties: TPersistent;
begin
  Result := GetRootData.GetRTFProperties;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
function TCustomRVData.UseStyleTemplates: Boolean;
begin
  Result := GetAbsoluteRootData.UseStyleTemplates;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.StyleTemplateInsertMode: TRVStyleTemplateInsertMode;
begin
  Result := GetAbsoluteRootData.StyleTemplateInsertMode;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCPARAMS}
function TCustomRVData.GetDocParameters(AllowCreate: Boolean): TRVDocParameters;
begin
  Result := GetRootData.GetDocParameters(AllowCreate);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.RV_CanConcateItems(FirstItemNo: Integer;
  item1, item2: TCustomRVItemInfo; IgnorePara: Boolean;
  AllowConcateNonText: Boolean): Boolean;
var RVStyle: TRVStyle;
begin
  RVStyle := GetRVStyle;
  if (item1=nil) or (item2=nil) then begin
    Result := False;
    exit;
  end;
  if not AllowConcateNonText and
    ((item1.StyleNo<0) or (item2.StyleNo<0)) then begin
    Result := False;
    exit;
  end;
  if (((item1.StyleNo>=0) and (GetItemTextR(FirstItemNo)='')) or
     ((item2.StyleNo>=0) and (GetItemTextR(FirstItemNo+1)=''))
      {$IFNDEF RVDONOTUSELISTS}and (item1.StyleNo<>rvsListMarker){$ENDIF}) and
     (IgnorePara or item2.SameAsPrev) then begin
    Result := True;
    exit;
  end;
  if (item1.StyleNo<0) or (item2.StyleNo<0) then begin
    Result := False;
    exit;
  end;
  Result := (item1.StyleNo=item2.StyleNo) and
            (IgnorePara or item2.SameAsPrev) and
            {$IFNDEF RVDONOTUSEUNICODE}
            (RVStyle.TextStyles[GetActualStyle(item1)].Unicode=
              RVStyle.TextStyles[GetActualStyle(item2)].Unicode) and
            {$ENDIF}
            {$IFNDEF RVDONOTUSEITEMHINTS}
            (item1.Hint=item2.Hint) and
            {$ENDIF}
            RV_CompareTags(item1.Tag,item2.Tag, rvoTagsArePChars in Options) and
            (item2.Checkpoint=nil) and
            (
            (Length(Items[FirstItemNo])=0) or
            (Length(Items[FirstItemNo+1])=0) or
            ([rvprConcateProtect,rvprModifyProtect]*
              RVStyle.TextStyles[GetActualStyle(item1)].Protection=[])
            )
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SimpleConcate(FirstItemNo: Integer; item1,
  item2: TCustomRVItemInfo);
begin
  if (item2.StyleNo>=0) and (GetItemTextR(FirstItemNo+1)='') then begin
    // merging because the second item is empty: deleting the second item
    InternalFreeItem(item2,False);
    Items.Delete(FirstItemNo+1);
    exit;
  end;
  if (item1.StyleNo>=0) and (GetItemTextR(FirstItemNo)='') then begin
    // merging because the first item is empty: transferring parastart properties
    // from item1 to item2 and deleting the first item
    item2.AssignParaProperties(item1);
    if (item2.Checkpoint=nil) and (item1.Checkpoint<>nil) then begin
      item2.Checkpoint := item1.Checkpoint;
      item2.Checkpoint.ItemInfo := item2;
      item1.Checkpoint := nil;
    end;
    InternalFreeItem(item1,False);
    Items.Delete(FirstItemNo);
    exit;
  end;
  // merging because of two items of the same style
  if (item1.StyleNo>=0) and (item2.StyleNo>=0)
    {$IFNDEF RVDONOTUSEUNICODE}
    and
    (GetRVStyle.TextStyles[GetActualStyle(item1)].Unicode=
     GetRVStyle.TextStyles[GetActualStyle(item2)].Unicode)
     {$ENDIF} then begin
    if (GetItemTextR(FirstItemNo)='') and (GetItemTextR(FirstItemNo+1)<>'') then begin
      item1.StyleNo := item2.StyleNo;
      if item1.Tag=RVEMPTYTAG then begin
        item1.Tag := Item2.Tag;
        Item2.Tag := RVEMPTYTAG;
      end;
    end;
    Items[FirstItemNo] := Items[FirstItemNo]+Items[FirstItemNo+1];
  end;
  InternalFreeItem(item2,False);
  Items.Delete(FirstItemNo+1);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.MassSimpleConcate(FirstItemNo,
  LastItemNo: Integer);
var i: Integer;
    item1,
    item2: TCustomRVItemInfo;
begin
  if FirstItemNo<0 then
    FirstItemNo := 0;
  if LastItemNo>=Items.Count then
    LastItemNo := Items.Count-1;
  for i := LastItemNo downto FirstItemNo+1 do begin
    SimpleConcateSubitems(i);
    item1 := GetItem(i-1);
    item2 := GetItem(i);
    if RV_CanConcateItems(i-1, item1, item2, False, True) then
      SimpleConcate(i-1, item1, item2);
  end;
  if (FirstItemNo>=0) and (FirstItemNo<=LastItemNo) then
    SimpleConcateSubitems(FirstItemNo);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.MassSimpleConcateAll;
begin
  MassSimpleConcate(0, ItemCount-1);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SimpleConcateSubitems(ItemNo: Integer);
var StoreSub: TRVStoreSubRVData;
    SubRVData: TCustomRVData;
    item: TCustomRVItemInfo;
    i: Integer;
begin
  item := GetItem(ItemNo);
  SubRVData := TCustomRVData(item.GetSubRVData(StoreSub,rvdFirst));
  while SubRVData<>nil do begin
    SubRVData.MassSimpleConcate(0, SubRVData.ItemCount-1);
    for i := 0 to SubRVData.ItemCount-1 do
      SubRVData.SimpleConcateSubitems(i);
    SubRVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdNext));
  end;
  StoreSub.Free;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTFIMPORT}
function TCustomRVData.LoadRTF(const FileName: String): TRVRTFErrorCode;
var rp: TRVRTFReaderProperties;
    ItemNo: Integer;
begin
  rp := TRVRTFReaderProperties(GetRTFProperties);
  if rp<>nil then begin
    ItemNo := Items.Count-1;
    rp.BasePath := ExtractFilePath(FileName);
    try
      Result := rp.ReadFromFile(FileName, Self, False);
    finally
      rp.BasePath := '';
    end;
    MassSimpleConcate(ItemNo, Items.Count-1);
    end
  else
    Result := rtf_ec_Assertion;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.LoadRTFFromStream(Stream: TStream): TRVRTFErrorCode;
var rp: TRVRTFReaderProperties;
    ItemNo: Integer;
begin
  rp := TRVRTFReaderProperties(GetRTFProperties);
  if rp<>nil then begin
    ItemNo := Items.Count-1;
    Result := rp.ReadFromStream(Stream, Self, False);
    MassSimpleConcate(ItemNo, ItemCount-1);
    end
  else
    Result := rtf_ec_Assertion;
end;
{------------------------------------------------------------------------------}
{$IFDEF RVUSEWORDDOC}
function TCustomRVData.LoadWordDoc(const FileName: String):TRVRTFErrorCode;
var rp: TRVRTFReaderProperties;
    ItemNo: Integer;
begin
  rp := TRVRTFReaderProperties(GetRTFProperties);
  if rp<>nil then begin
    ItemNo := Items.Count-1;
    Result := rp.ReadFromWordDocFile(FileName, Self);
    MassSimpleConcate(ItemNo, Items.Count-1);
    end
  else
    Result := rtf_ec_Assertion;
end;
{$ENDIF}
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVData.AfterAddStyle(StyleInfo: TCustomRVInfo);
begin
  GetAbsoluteRootData.AfterAddStyle(StyleInfo);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetOrAddTextStyle(TextStyle: TFontInfo): Integer;
var Count: Integer;
begin
  Count := GetRVStyle.TextStyles.Count;
  Result := GetRVStyle.FindTextStyle(TextStyle);
  If Result=Count then
    AfterAddStyle(GetRVStyle.TextStyles[Result]);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetOrAddParaStyle(ParaStyle: TParaInfo): Integer;
var Count: Integer;
begin
  Count := GetRVStyle.ParaStyles.Count;
  Result := GetRVStyle.FindParaStyle(ParaStyle);
  If Result=Count then
    AfterAddStyle(GetRVStyle.ParaStyles[Result]);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoMarkStylesInUse(Data: TRVDeleteUnusedStylesData);
var i: Integer;
begin
  for i := 0 to Items.Count-1 do
    GetItem(i).MarkStylesInUse(Data);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoUpdateStyles(Data: TRVDeleteUnusedStylesData);
var i: Integer;
begin
  for i := 0 to Items.Count-1 do
    GetItem(i).UpdateStyles(Data);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.MarkStylesInUse(Data: TRVDeleteUnusedStylesData);
    {............................................}
    procedure ExpandStyle(Index, FirstIndex: Integer; Styles: TCustomRVInfos;
      Used, Expanded: TRVIntegerList);
    var Style: TCustomRVInfo;
    begin
      if Expanded[Index]<>0 then
        exit;
      Used[Index] := 1;
      Expanded[Index] := 1;
      Style := TCustomRVInfo(Styles.Items[Index]);
      if Style.BaseStyleNo>=0 then begin
        if Style.BaseStyleNo >= Styles.Count then
          Style.BaseStyleNo := -1
        else
          ExpandStyle(Style.BaseStyleNo, FirstIndex, Styles, Used, Expanded);
      end;
      if (Styles is TFontInfos) and (TFontInfo(Style).NextStyleNo>=0) then begin
        if TFontInfo(Style).NextStyleNo>= Styles.Count then
          TFontInfo(Style).NextStyleNo := -1
        else
          ExpandStyle(TFontInfo(Style).NextStyleNo, FirstIndex, Styles, Used, Expanded)
        end
      else if (Styles is TParaInfos) and (TParaInfo(Style).NextParaNo>=0) then begin
        if TParaInfo(Style).NextParaNo >= Styles.Count then
          TParaInfo(Style).NextParaNo := -1
        else
          ExpandStyle(TParaInfo(Style).NextParaNo, FirstIndex, Styles, Used, Expanded);
      end;
    end;
    {............................................}
    procedure ExpandStyles(Styles: TCustomRVInfos; Used: TRVIntegerList);
    var i: Integer;
        Expanded: TRVIntegerList;
    begin
      Expanded := TRVIntegerList.CreateEx(Used.Count, 0);
      for i := 0 to Used.Count-1 do
        if (Used[i]<>0) then
          ExpandStyle(i, i, Styles, Used, Expanded);
      Expanded.Free;
    end;
    {............................................}
    procedure MarkDefStyles;
    var i: Integer;
        RVStyle: TRVStyle;
    begin
      RVStyle := GetRVStyle;
      for i := 0 to RVStyle.ParaStyles.Count-1 do
        if (Data.UsedParaStyles[i]<>0) and
           (RVStyle.ParaStyles[i].DefStyleNo>=0) then
          Data.UsedTextStyles[RVStyle.ParaStyles[i].DefStyleNo] := 1;
    end;
    {............................................}
begin
  Data.Init(GetRVStyle);
  DoMarkStylesInUse(Data);
  if Data.ParaStyles then
    ExpandStyles(GetRVStyle.ParaStyles, Data.UsedParaStyles);
  if Data.TextStyles then begin
    MarkDefStyles;
    {$IFNDEF RVDONOTUSEUNICODE}
    if (GetRVStyle.DefUnicodeStyle>=0) then
      if GetRVStyle.DefUnicodeStyle>=Data.UsedTextStyles.Count then
        GetRVStyle.DefUnicodeStyle := -1
      else
        Data.UsedTextStyles[GetRVStyle.DefUnicodeStyle] := 1;
    {$ENDIF}
    ExpandStyles(GetRVStyle.TextStyles, Data.UsedTextStyles);
  end;
  {$IFNDEF RVDONOTUSELISTS}
  if Data.ListStyles then
    ExpandStyles(GetRVStyle.ListStyles, Data.UsedListStyles);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DeleteMarkedStyles(Data: TRVDeleteUnusedStylesData);
begin
  Data.ConvertFlagsToShifts(Self.GetRVStyle);
  DoUpdateStyles(Data);
  AfterDeleteStyles(Data);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DeleteUnusedStyles(TextStyles, ParaStyles, ListStyles: Boolean);
var Data: TRVDeleteUnusedStylesData;
begin
  Data := TRVDeleteUnusedStylesData.Create(TextStyles, ParaStyles, ListStyles);
  try
    MarkStylesInUse(Data);
    DeleteMarkedStyles(Data);
    {$IFNDEF RVDONOTUSEUNICODE}
    if TextStyles and (GetRVStyle.DefUnicodeStyle>=0) then
      if GetRVStyle.DefUnicodeStyle>=Data.UsedTextStyles.Count then
        GetRVStyle.DefUnicodeStyle := -1
      else
        GetRVStyle.DefUnicodeStyle :=
          GetRVStyle.DefUnicodeStyle-Data.UsedTextStyles[GetRVStyle.DefUnicodeStyle]+1;
    {$ENDIF}
  finally
    Data.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AfterDeleteStyles(Data: TRVDeleteUnusedStylesData);
begin
  // nothing to do here
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.InitStyleMappings(var PTextStylesMapping,
  PParaStylesMapping, PListStylesMapping: PRVIntegerList;
  var PUnits: PRVStyleUnits);
begin
  GetRootData.InitStyleMappings(
    PTextStylesMapping, PParaStylesMapping, PListStylesMapping, PUnits);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DoneStyleMappings(AsSubDoc: Boolean);
begin
  // nothing to do here
end;
{------------------------------------------------------------------------------}
function TCustomRVData.SupportsPageBreaks: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AdjustInItemsRange(var ItemNo: Integer);
begin
  if ItemNo>=Items.Count then
    ItemNo := Items.Count-1;
  if ItemNo<0 then
    ItemNo := 0;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.NextChar(ItemNo, Index: Integer): Integer;
begin
  Result := NextCharStr(Items[ItemNo], ItemNo, Index);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.PrevChar(ItemNo, Index: Integer): Integer;
begin
  Result := PrevCharStr(Items[ItemNo], ItemNo, Index);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.IsWordWrapAllowed: Boolean;
begin
  Result := GetAbsoluteRootData.IsWordWrapAllowed;
end;
{------------------------------------------------------------------------------}
// Returns True if "hidden" option is set for this item
function TCustomRVData.IsHiddenItem(ItemNo: Integer): Boolean;
{$IFNDEF RVDONOTUSELISTS}
var i: Integer;
{$ENDIF}
begin
  if GetItemStyle(ItemNo)<0 then begin
    Result := TRVNonTextItemInfo(GetItem(ItemNo)).Hidden;
    {$IFNDEF RVDONOTUSELISTS}
    // hiding paragraph marker if the whole paragraph section is hidden
    if not Result and (GetItemStyle(ItemNo)=rvsListMarker) then begin
      for i := ItemNo+1 to ItemCount-1 do begin
        if IsFromNewLine(i) then
          break;
        if not IsHiddenItem(i) then
          exit;
      end;
      Result := True;
    end;
    {$ENDIF}
    end
  else
    Result := rvteoHidden in GetRVStyle.TextStyles[GetItemStyle(ItemNo)].Options;
end;
{------------------------------------------------------------------------------}
// Returns True if last item(s) in this paragraph is hidden
function TCustomRVData.IsHiddenParaEnd(FirstItemNo: Integer): Boolean;
var i: Integer;
begin
  for i := FirstItemNo+1 to ItemCount-1 do
    if IsParaStart(i) then begin
      Result := IsHiddenItem(i-1);
      exit;
    end;
  Result := IsHiddenItem(ItemCount-1);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.NextCharStr(const str: TRVRawByteString;
  ItemNo, Index: Integer): Integer;
{$IFNDEF RVDONOTUSEUNICODE}
var s: TRVRawByteString;
    p1,p2: Pointer;
{$ENDIF}
begin
  {$IFNDEF RVDONOTUSEUNICODE}
  if RVNT and (rvioUnicode in GetItemOptions(ItemNo)) then begin
    s := str;
    SetLength(s, Length(s)+1);
    s[Length(s)]:=#0;
    p1 := Pointer(s);
    p2 := CharNextW(Pointer(PRVAnsiChar(p1)+(Index-1)*2));
    Result := (PRVAnsiChar(p2)-PRVAnsiChar(p1)) div 2+1;
    end
  else
  {$ENDIF}
    Result := Index+1;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.PrevCharStr(const str: TRVRawByteString;
  ItemNo, Index: Integer): Integer;
{$IFNDEF RVDONOTUSEUNICODE}
var s: TRVRawByteString;
    p1,p2: Pointer;
{$ENDIF}
begin
  {$IFNDEF RVDONOTUSEUNICODE}
  if RVNT and (rvioUnicode in GetItemOptions(ItemNo)) then begin
    s := str;
    SetLength(s, Length(s)+1);
    s[Length(s)]:=#0;
    p1 := Pointer(s);
    p2 := CharPrevW(p1, Pointer(PRVAnsiChar(p1)+(Index-1)*2));
    if p2=PRVAnsiChar(p1)+(Index-1)*2 then
      p2 := p1;
    Result := (PRVAnsiChar(p2)-PRVAnsiChar(p1)) div 2+1;
    end
  else
  {$ENDIF}
    Result := Index-1;
end;
{------------------------------------------------------------------------------}
// Returns the first item in the paragraph, where the ItemNo-th item belongs.
function TCustomRVData.GetFirstParaItem(ItemNo: Integer): Integer;
begin
  Result := ItemNo;
  while (Result>0) and not IsParaStart(Result) do
    dec(Result);
end;
{------------------------------------------------------------------------------}
// Returns the first item in the paragraph section , where the ItemNo-th item belongs.
function TCustomRVData.GetFirstParaSectionItem(ItemNo: Integer): Integer;
begin
  Result := ItemNo;
  while (Result>0) and not IsFromNewLine(Result) do
    dec(Result);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.GetParentInfo(var ParentItemNo: Integer;
  var Location: TRVStoreSubRVData);
begin
  ParentItemNo := -1;
  Location   := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetAbsoluteRootItemNo(ItemNo: Integer): Integer;
var RVData: TCustomRVData;
    StoreSub: TRVStoreSubRVData;
begin
  Result := ItemNo;
  RVData := Self;
  while RVData.GetAbsoluteParentData<>nil do begin
    RVData.GetParentInfo(Result, StoreSub);
    StoreSub.Free;
    RVData := RVData.GetAbsoluteParentData;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetBiDiMode: TRVBiDiMode;
begin
  Result := GetRootData.GetBiDiMode;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemBiDiMode(ItemNo: Integer): TRVBiDiMode;
var item: TCustomRVItemInfo;
begin
  item := GetItem(ItemNo);
  if item.StyleNo>=0 then
    Result := GetRVStyle.TextStyles[GetActualStyle(item)].BiDiMode
  else
    Result := rvbdUnspecified;
  if Result=rvbdUnspecified then
    Result := GetParaBiDiMode(item.ParaNo);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetParaBiDiMode(
  ParaNo: Integer): TRVBiDiMode;
begin
  Result := GetRVStyle.ParaStyles[ParaNo].BiDiMode;
  if Result=rvbdUnspecified then
    Result := GetBiDiMode;
end;
{------------------------------------------------------------------------------}
{$IFDEF RVUSELISTORSEQ}
function TCustomRVData.FindPreviousItem(ItemNo: Integer;
  ItemClass: TCustomRVItemInfoClass): TCustomRVItemInfo;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; LastItemNo: Integer): TCustomRVItemInfo; forward;
   {...................................................}
   function FindItemInItem(Item: TCustomRVItemInfo; StoreSub: TRVStoreSubRVData): TCustomRVItemInfo;
   var RVData: TCustomRVData;
   begin
     Result := nil;
     if StoreSub=nil then
       RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdLast))
     else
       RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdPrev));
     if RVData<>nil then begin
       repeat
         Result := FindItemInRVData(RVData, RVData.Items.Count-1);
         if Result<>nil then
           break;
         RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdPrev));
       until RVData=nil;
     end;
     StoreSub.Free;
   end;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; LastItemNo: Integer): TCustomRVItemInfo;
   var i: Integer;
   begin
     for i := LastItemNo downto 0 do begin
       if RVData.GetItem(i) is ItemClass then
         Result := RVData.GetItem(i)
       else
         Result := FindItemInItem(RVData.GetItem(i), nil);
       if Result<>nil then
         exit;
     end;
     Result := nil;
   end;
   {...................................................}
var RVData: TCustomRVData;
    StoreSub: TRVStoreSubRVData;
begin
  Result := nil;
  RVData := Self;
  while RVData<>nil do begin
    Result := FindItemInRVData(RVData, ItemNo);
    if Result<>nil then
      break;
    RVData.GetParentInfo(ItemNo, StoreSub);
    if ItemNo<0 then begin
      StoreSub.Free;
      break;
    end;
    RVData := RVData.GetAbsoluteParentData;
    Result := FindItemInItem(RVData.GetItem(ItemNo), StoreSub);
    if Result<>nil then
      break;
    dec(ItemNo);
  end;
end;
{------------------------------------------------------------------------------}
{ Returns index of item in the given RVData containing ItemToFind (or index of
  this item itself. The search is started from StartItemNo }
function TCustomRVData.FindItemLocalLocationFrom(StartItemNo: Integer;
  ItemToFind: TCustomRVItemInfo): Integer;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData;
     FirstItemNo: Integer): Integer; forward;
   {...................................................}
   function FindItemInItem(Item: TCustomRVItemInfo): Boolean;
   var RVData: TCustomRVData;
       StoreSub: TRVStoreSubRVData;
   begin
     Result := False;
     RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdFirst));
     if RVData<>nil then begin
       repeat
         Result := FindItemInRVData(RVData.GetRVData, 0)>=0;
         if Result then
           break;
         RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdNext));
       until RVData=nil;
     end;
     StoreSub.Free;
   end;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; FirstItemNo: Integer): Integer;
   var i: Integer;
   begin
     for i := FirstItemNo to RVData.ItemCount-1 do
       if (RVData.GetItem(i)=ItemToFind) or
           FindItemInItem(RVData.GetItem(i)) then begin
         Result := i;
         exit;
       end;
     Result := -1;
   end;
   {...................................................}
begin;
  Result := FindItemInRVData(Self, StartItemNo);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
procedure TCustomRVData.AddSeqInList(ItemNo: Integer);
var List: TRVSeqList;
    PrevSeq: TRVSeqItemInfo;
    Index: Integer;
begin
  if not (GetItem(ItemNo) is TRVSeqItemInfo) then
    exit;
  List := GetSeqList(True);
  if List=nil then
    exit;
  if TRVSeqItemInfo(GetItem(ItemNo)).GetIndexInList(List)>=0 then
    exit;
  PrevSeq := FindPreviousSeq(ItemNo-1);
  Index := List.InsertAfter(TRVSeqItemInfo(GetItem(ItemNo)), PrevSeq);
  GetSeqList(False).RecalcCounters(Index, GetRVStyle, False);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DeleteSeqFromList(Item: TCustomRVItemInfo;
  Clearing: Boolean);
var List: TRVSeqList;
    Index: Integer;
begin
  if Item is TRVSeqItemInfo then begin
    List := GetSeqList(False);
    if List=nil then
      exit;
    Index := TRVSeqItemInfo(Item).GetIndexInList(List);
    List.Delete(Index);
    if List.Count=0 then
      DestroySeqList
    else if not Clearing then
      List.RecalcCounters(Index, GetRVStyle, False);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetSeqList(AllowCreate: Boolean): TRVSeqList;
begin
  Result := GetAbsoluteParentData.GetSeqList(AllowCreate);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DestroySeqList;
begin
  GetAbsoluteRootData.DestroySeqList;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.FindPreviousSeq(ItemNo: Integer): TRVSeqItemInfo;
begin
   Result := TRVSeqItemInfo(FindPreviousItem(ItemNo, TRVSeqItemInfo));
end;
{------------------------------------------------------------------------------}
{ Returns index in GetSeqList of the last seq item having SeqName listed in SeqNames }
{ Note: SeqNames must be sorted! }
function TCustomRVData.FindLastSeqIndex(StartAfterMeIndex: Integer;
  SeqNames: TStringList): Integer;
var i, j: Integer;
    SeqList: TRVSeqList;
begin
  Result := -1;
  SeqList := GetSeqList(False);
  if SeqList=nil then
    exit;
  for i := SeqList.Count-1 downto StartAfterMeIndex+1 do
    if SeqNames.Find(TRVSeqItemInfo(SeqList[i]).SeqName, j) then begin
      Result := i;
      exit;
    end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSELISTS}
function TCustomRVData.SetListMarkerInfo(AItemNo, AListNo, AListLevel,
  AStartFrom, AParaNo: Integer; AUseStartFrom: Boolean): Integer;
var Marker: TRVMarkerItemInfo;
    s: TRVRawByteString;
    Markers: TRVMarkerList;
begin
  if (AItemNo>=Items.Count) or (AItemNo<0) then
    Result := Items.Count
  else begin
    Result := GetFirstParaItem(AItemNo);
    if GetItem(Result).GetBoolValue(rvbpFullWidth) then begin
      Result := -1;
      exit;
    end;
  end;
  if (Result<Items.Count) and (GetItemStyle(Result)=rvsListMarker) then begin
    Marker := TRVMarkerItemInfo(GetItem(Result));
    Marker.ListNo    := AListNo;
    Marker.Level     := AListLevel;
    Marker.StartFrom := AStartFrom;
    Marker.Reset     := AUseStartFrom;
    Markers := GetMarkers(False);
    if Markers<>nil then
      Markers.RecalcCounters(Marker.GetIndexInList(Markers), GetRVStyle, True);
    end
  else begin
    Marker := TRVMarkerItemInfo.CreateEx(Self, AListNo, AListLevel, AStartFrom, AUseStartFrom);
    s := '';
    Marker.Inserting(Self,s,True);
    if Result<Items.Count then begin
      GetItem(Result).SameAsPrev := True;
      Marker.ParaNo := GetItemPara(Result);
      end
    else begin
      Marker.ParaNo := AParaNo;
      if AParaNo<0 then
        if Items.Count=0 then
          Marker.ParaNo := 0
        else
          Marker.ParaNo := GetItemPara(Items.Count-1);
    end;
    Items.InsertObject(Result, s, Marker);
    Marker.Inserted(Self, Result);
    AddMarkerInList(Result);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.RecalcMarker(AItemNo: Integer; AllowCreateList: Boolean);
var Markers: TRVMarkerList;
begin
  if GetItemStyle(AItemNo)<>rvsListMarker then
    exit;
  Markers := GetMarkers(AllowCreateList);
  if Markers=nil then
    exit;
  Markers.RecalcCounters(TRVMarkerItemInfo(GetItem(AItemNo)).GetIndexInList(Markers), GetRVStyle, True);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.RemoveListMarker(ItemNo: Integer);
begin
  ItemNo := GetFirstParaItem(ItemNo);
  if GetItemStyle(ItemNo)=rvsListMarker then begin
    DeleteItems(ItemNo,1);
    if ItemNo<Items.Count then
      GetItem(ItemNo).SameAsPrev := False;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetListMarkerInfo(AItemNo: Integer;
                                 var AListNo, AListLevel, AStartFrom: Integer;
                                 var AUseStartFrom: Boolean): Integer;
begin
  Result := GetFirstParaItem(AItemNo);
  if GetItemStyle(Result)<>rvsListMarker then begin
    Result := -1;
    exit;
  end;
  with TRVMarkerItemInfo(GetItem(Result)) do begin
    AListNo := ListNo;
    AListLevel := Level;
    AStartFrom := StartFrom;
    AUseStartFrom := Reset;
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetMarkers(AllowCreate: Boolean): TRVMarkerList;
begin
  Result := GetAbsoluteParentData.GetMarkers(AllowCreate);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetPrevMarkers: TRVMarkerList;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DestroyMarkers;
begin
  GetAbsoluteRootData.DestroyMarkers;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.AddMarkerInList(ItemNo: Integer);
var List: TRVMarkerList;
    PrevMarker: TRVMarkerItemInfo;
    Index: Integer;
begin
  if GetItemStyle(ItemNo)<>rvsListMarker then
    exit;
  List := GetMarkers(True);
  if List=nil then
    exit;
  if TRVMarkerItemInfo(GetItem(ItemNo)).GetIndexInList(List)>=0 then
    exit;
  PrevMarker := FindPreviousMarker(ItemNo-1);
  Index := List.InsertAfter(TRVMarkerItemInfo(GetItem(ItemNo)), PrevMarker);
  GetMarkers(False).RecalcCounters(Index, GetRVStyle, False);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.DeleteMarkerFromList(Item: TCustomRVItemInfo; Clearing: Boolean);
var List: TRVMarkerList;
    Index: Integer;
begin
  if Item.StyleNo=rvsListMarker then begin
    List := GetMarkers(False);
    if List=nil then
      exit;
    Index := TRVMarkerItemInfo(Item).GetIndexInList(List);
    List.Delete(Index);
    if List.Count=0 then
      DestroyMarkers
    else if not Clearing then
      List.RecalcCounters(Index, GetRVStyle, False);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.FindPreviousMarker(ItemNo: Integer): TRVMarkerItemInfo;
begin
   Result := TRVMarkerItemInfo(FindPreviousItem(ItemNo, TRVMarkerItemInfo));
end;
{------------------------------------------------------------------------------}
{ Returns index in GetMarkers of the last marker of style listed in ListStyles }
function TCustomRVData.FindLastMarkerIndex(StartAfterMeIndex: Integer;
  ListStyles: TRVIntegerList): Integer;
var i, j, ListNo: Integer;
    ok: Boolean;
    Markers: TRVMarkerList;
begin
  Result := -1;
  Markers := GetMarkers(False);
  if Markers=nil then
    exit;
  for i := Markers.Count-1 downto StartAfterMeIndex+1 do begin
    ok := False;
    ListNo := TRVMarkerItemInfo(Markers[i]).ListNo;
    for j := 0 to ListStyles.Count-1 do
      if ListStyles[j] = ListNo then begin
        ok := True;
        break;
      end;
    if ok then begin
      Result := i;
      exit;
    end;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVData.GetChosenItem: TCustomRVItemInfo;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetChosenRVData: TCustomRVData;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetTopLevelChosenRVData: TCustomRVData;
begin
  Result := Self;
  while Result.GetChosenRVData<>nil do
    Result := Result.GetChosenRVData;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetItemTextR(ItemNo: Integer): TRVRawByteString;
begin
  Result := Items[ItemNo];
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.SetItemTextR(ItemNo: Integer; const s: TRVRawByteString);
begin
  if rvioUnicode in GetItemOptions(ItemNo) then
    RVCheckUni(Length(s));
  Items[ItemNo] := s;
end;
{------------------------------------------------------------------------------}
{ Returns the first and the last item of paragraph section containing
  the given range of items }
procedure TCustomRVData.ExpandToParaSection(ItemNo1,ItemNo2: Integer;
  var FirstItemNo, LastItemNo: Integer);
begin
  FirstItemNo := ItemNo1;
  while (FirstItemNo>0) and not IsFromNewLine(FirstItemNo) do
    dec(FirstItemNo);
  LastItemNo := ItemNo2+1;
  while (LastItemNo<Items.Count) and not IsFromNewLine(LastItemNo) do
    inc(LastItemNo);
  dec(LastItemNo);
end;
{------------------------------------------------------------------------------}
{ Returns the first and the last item of paragraph containing
  the given range of items }
procedure TCustomRVData.ExpandToPara(ItemNo1,ItemNo2: Integer;
  var FirstItemNo, LastItemNo: Integer);
begin
  FirstItemNo := ItemNo1;
  while (FirstItemNo>0) and not IsParaStart(FirstItemNo) do
    dec(FirstItemNo);
  LastItemNo := ItemNo2+1;
  while (LastItemNo<Items.Count) and not IsParaStart(LastItemNo) do
    inc(LastItemNo);
  dec(LastItemNo);
end;
{------------------------------------------------------------------------------}
{ READ method for ItemCount property }
function TCustomRVData.GetItemCount: Integer;
begin
  Result := Items.Count;
end;
{------------------------------------------------------------------------------}
{ Inits editing mode and returns RVData of inplace editor. For most RVDatas,
  this method does nothing and returns themselves.
  Overriden in TRVTableCellData. }
function TCustomRVData.Edit: TCustomRVData;
begin
  Result := Self;
end;
{------------------------------------------------------------------------------}
{ Enumerates all items from the first to the last one: calls Proc for each item.
  Items in sub-documents (cells) are included. If they are edited, RVData of
  inplace editor is used as a parameter. Value of UserData is passed as a
  last parameter of Proc. }
function TCustomRVData.EnumItems(Proc: TRVEnumItemsProc; var UserData1: Integer;
      const UserData2: String): Boolean;
var i: Integer;
    RVData: TCustomRVData;
    StoreSub: TRVStoreSubRVData;
    item: TCustomRVItemInfo;
begin
  Result := True;
  for i := 0 to ItemCount-1 do begin
    Proc(Self, i, UserData1, UserData2, Result);
    if not Result then
      exit;
    item := GetItem(i);
    RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdFirst));
    if RVData<>nil then begin
       repeat
         Result := RVData.GetRVData.EnumItems(Proc, UserData1, UserData2);
         if not Result then
           break;
         RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdNext));
       until RVData=nil;
     end;
     StoreSub.Free;
   end;
end;
{------------------------------------------------------------------------------}
{ Compares two locations in the same document:
  (RVData1, ItemNo1) and (RVData2, ItemNo2).
  Return value: 0 if equal, <0 if (1) before (2), >0 if (1) after (2).
  Table is assumed before its cells. }
function RVCompareLocations(RVData1: TCustomRVData; ItemNo1: Integer;
  RVData2: TCustomRVData; ItemNo2: Integer): Integer;
var CurItemNo2: Integer;
    CurRVData2: TCustomRVData;
    StoreSub1,StoreSub2: TRVStoreSubRVData;
begin
  RVData1 := RVData1.GetSourceRVData;
  RVData2 := RVData2.GetSourceRVData;
  CurRVData2 := RVData2;
  CurItemNo2 := ItemNo2;
  StoreSub1 := nil;
  StoreSub2 := nil;
  while True do begin
    while True do begin
      if RVData1=CurRVData2 then begin
        Result := ItemNo1-CurItemNo2; // different items?
        if Result=0 then
          if StoreSub1<>nil then
            if StoreSub2<>nil then
              Result := StoreSub1.Compare(StoreSub2) // cells in the same table?
            else
              Result := +1 // (1) is from table cell, (2) is a table itself
          else
            if StoreSub2<>nil then
              Result := -1  // (2) is from table cell, (1) is a table itself
            else
              Result := 0; // the same item;
        StoreSub1.Free;
        StoreSub2.Free;
        exit;
      end;
      StoreSub2.Free;
      CurRVData2.GetParentInfo(CurItemNo2, StoreSub2);
      if CurItemNo2<0 then
        break;
      CurRVData2 := CurRVData2.GetAbsoluteParentData.GetSourceRVData;
    end;
    StoreSub1.Free;
    RVData1.GetParentInfo(ItemNo1, StoreSub1);
    if ItemNo1<0 then
      raise ERichViewError.Create(errRVCompare);
    RVData1 := RVData1.GetAbsoluteParentData.GetSourceRVData;
    CurRVData2 := RVData2;
  end;
end;
{------------------------------------------------------------------------------}
{ Access to TCustomRichView.MaxLength property }
function TCustomRVData.GetMaxLength: Integer;
begin
  Result := GetAbsoluteRootData.GetMaxLength;
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetColor: TColor;
begin
  Result := clNone;
end;
{------------------------------------------------------------------------------}
// Convert all units from GetRVStyle.Units to NewUnits
procedure TCustomRVData.ConvertToDifferentUnits(NewUnits: TRVStyleUnits;
   RVStyle: TRVStyle; ConvertItems: Boolean);
var i: Integer;
    RVData: TCustomRVData;
begin
  if not ConvertItems then
    exit;
  RVData := GetRVData;
  for i := 0 to RVData.ItemCount-1 do
    RVData.GetItem(i).ConvertToDifferentUnits(NewUnits, RVStyle, True);
end;
{------------------------------------------------------------------------------}
{ Returns the rotation relative to the parent RVData. Overriden in table cells. }
function TCustomRVData.GetRotation: TRVDocRotation;
begin
  Result := rvrotNone;
end;
{------------------------------------------------------------------------------}
{ Returns the degree of rotation, taking rotations of the parent documents
  into account.
  FromAbsRoot = True: relative to the absolute root.
  FromAbsRoot = False:
    - for inplace editor: the same as FromAbsRoot.
    - for cells: local
   }
function TCustomRVData.GetRealRotation(FromAbsRoot: Boolean): TRVDocRotation;
var RVData: TCustomRVData;
    Degree: Integer;
begin
  if not FromAbsRoot and (GetParentData<>nil) then begin
    Result := GetRotation;
    exit;
  end;
  Degree := 0;
  RVData := Self;
  repeat
    case RVData.GetRotation of
      rvrot90:   inc(Degree, 90);
      rvrot180:  inc(Degree, 180);
      rvrot270:  inc(Degree, 270);
    end;
    if Degree>360 then
      dec(Degree, 360);
    RVData := RVData.GetAbsoluteParentData
  until RVData=nil;
  case Degree of
    90: Result := rvrot90;
    180: Result := rvrot180;
    270: Result := rvrot270;
    else Result := rvrotNone;
  end;
end;
{------------------------------------------------------------------------------}
{ Is text vertical relative to the parent RVData? }
function TCustomRVData.IsVerticalText: Boolean;
begin
  Result := GetRotation in [rvrot90, rvrot270];
end;
{------------------------------------------------------------------------------}
{ Is text really vertical? }
function TCustomRVData.IsReallyVerticalText: Boolean;
begin
  Result := GetRealRotation(True) in [rvrot90, rvrot270];
end;
{------------------------------------------------------------------------------}
{ This RVData or at least one of its parent was rotated }
function TCustomRVData.IsRotationUsed: Boolean;
var RVData: TCustomRVData;
begin
  Result := True;
  RVData := Self;
  repeat
    if RVData.GetRotation<>rvrotNone then
      exit;
    RVData := RVData.GetAbsoluteParentData;
  until RVData=nil;
  Result := False;
end;
{------------------------------------------------------------------------------}
// Instructs to keep the content on the same page when printing, if possible.
// (may be true for table cells)
function TCustomRVData.KeepTogether: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ChangeParagraphStyles(ParaMapList, NewIndices,
  OldIndices: TRVIntegerList; var Index: Integer);
var i: Integer;
begin
  for i :=0 to ItemCount-1 do
    GetItem(i).ChangeParagraphStyles(ParaMapList, NewIndices, OldIndices, Index);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVData.ChangeTextStyles(TextMapList, NewIndices,
  OldIndices: TRVIntegerList; var Index: Integer);
var i: Integer;
begin
  for i :=0 to ItemCount-1 do
    GetItem(i).ChangeTextStyles(TextMapList, NewIndices, OldIndices, Index);
end;
{------------------------------------------------------------------------------}
function TCustomRVData.GetCurParaStyleNo: Integer;
begin
  Result := 0;
end;


end.