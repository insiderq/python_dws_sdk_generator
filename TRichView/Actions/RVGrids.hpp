﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVGrids.pas' rev: 27.00 (Windows)

#ifndef RvgridsHPP
#define RvgridsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvgrids
{
//-- type declarations -------------------------------------------------------
typedef void __fastcall (__closure *TRVGridDrawEvent)(System::TObject* Sender, int ACol, int ARow, const System::Types::TRect &ARect, bool Selected);

enum DECLSPEC_DENUM TRVGridBoundsType : unsigned char { bbtByCellBounds, bbtByGridBounds };

class DELPHICLASS TRVCustomGrid;
class PASCALIMPLEMENTATION TRVCustomGrid : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
private:
	int FRow;
	int FCol;
	int FFirstRow;
	int FRows;
	int FCols;
	int FRowsVisible;
	int FGridLineWidth;
	int FColWidth;
	int FRowHeight;
	System::Uitypes::TColor FGridLineColor;
	Vcl::Forms::TFormBorderStyle FBorderStyle;
	TRVGridBoundsType FBoundsType;
	int FWCol;
	int FHRow;
	TRVGridDrawEvent FOnDrawCell;
	System::Classes::TNotifyEvent FOnSelectCell;
	System::Classes::TNotifyEvent FOnTopLeftChanged;
	void __fastcall SetRows(const int Value);
	void __fastcall SetCols(const int Value);
	void __fastcall SetGridLineColor(const System::Uitypes::TColor Value);
	void __fastcall SetRowsVisible(const int Value);
	void __fastcall SetGridLineWidth(const int Value);
	void __fastcall SetBorderStyle(const Vcl::Forms::TBorderStyle Value);
	void __fastcall SetBoundsType(const TRVGridBoundsType Value);
	void __fastcall SetColWidth(const int Value);
	void __fastcall SetRowHeight(const int Value);
	MESSAGE void __fastcall WMGetDlgCode(Winapi::Messages::TWMNoParams &Msg);
	HIDESBASE MESSAGE void __fastcall WMSize(Winapi::Messages::TWMSize &Msg);
	HIDESBASE MESSAGE void __fastcall WMVScroll(Winapi::Messages::TWMScroll &Msg);
	HIDESBASE MESSAGE void __fastcall CMShowingChanged(Winapi::Messages::TMessage &Message);
	
protected:
	virtual void __fastcall Initalize(void);
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	virtual void __fastcall Loaded(void);
	DYNAMIC void __fastcall Resize(void);
	virtual void __fastcall Paint(void);
	virtual void __fastcall DrawGrid(void);
	virtual void __fastcall DrawCell(int ACol, int ARow, const System::Types::TRect &ARect, bool ASelected);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC bool __fastcall DoMouseWheelDown(System::Classes::TShiftState Shift, const System::Types::TPoint &MousePos);
	DYNAMIC bool __fastcall DoMouseWheelUp(System::Classes::TShiftState Shift, const System::Types::TPoint &MousePos);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	void __fastcall UpdateScrollPos(void);
	void __fastcall UpdateScrollRange(void);
	virtual void __fastcall DoSelectCell(void);
	virtual void __fastcall DoTopLeftChanged(void);
	__property int Row = {read=FRow, nodefault};
	__property int Col = {read=FCol, nodefault};
	__property int RowCount = {read=FRows, write=SetRows, nodefault};
	__property int RowsVisible = {read=FRowsVisible, write=SetRowsVisible, nodefault};
	__property int ColCount = {read=FCols, write=SetCols, nodefault};
	__property int DefaultColWidth = {read=FColWidth, write=SetColWidth, nodefault};
	__property int DefaultRowHeight = {read=FRowHeight, write=SetRowHeight, nodefault};
	__property System::Uitypes::TColor GridLineColor = {read=FGridLineColor, write=SetGridLineColor, nodefault};
	__property int GridLineWidth = {read=FGridLineWidth, write=SetGridLineWidth, nodefault};
	__property Vcl::Forms::TBorderStyle BorderStyle = {read=FBorderStyle, write=SetBorderStyle, default=1};
	__property TRVGridBoundsType BoundsType = {read=FBoundsType, write=SetBoundsType, default=0};
	__property TRVGridDrawEvent OnDrawCell = {read=FOnDrawCell, write=FOnDrawCell};
	__property System::Classes::TNotifyEvent OnSelectCell = {read=FOnSelectCell, write=FOnSelectCell};
	__property System::Classes::TNotifyEvent OnTopLeftChanged = {read=FOnTopLeftChanged, write=FOnTopLeftChanged};
	
public:
	__fastcall virtual TRVCustomGrid(System::Classes::TComponent* AOwner);
	int __fastcall GetColBegin(int ACol);
	virtual int __fastcall GetRowBegin(int ARow);
	System::Types::TRect __fastcall GetCellRect(int ACol, int ARow);
	int __fastcall GetColAt(int X);
	int __fastcall GetRowAt(int Y);
	virtual bool __fastcall SelectCell(int ACol, int ARow);
	__property Canvas;
public:
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TRVCustomGrid(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVCustomGrid(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


class DELPHICLASS TRVGrid;
class PASCALIMPLEMENTATION TRVGrid : public TRVCustomGrid
{
	typedef TRVCustomGrid inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property Constraints;
	__property Ctl3D;
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property Visible = {default=1};
	__property Color = {default=-16777211};
	__property OnClick;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnStartDock;
	__property OnStartDrag;
	__property BorderStyle = {default=1};
	__property BoundsType = {default=0};
	__property RowCount;
	__property RowsVisible;
	__property ColCount;
	__property GridLineColor;
	__property GridLineWidth;
	__property DefaultColWidth;
	__property DefaultRowHeight;
	__property Row;
	__property Col;
	__property OnDrawCell;
	__property OnSelectCell;
	__property OnTopLeftChanged;
public:
	/* TRVCustomGrid.Create */ inline __fastcall virtual TRVGrid(System::Classes::TComponent* AOwner) : TRVCustomGrid(AOwner) { }
	
public:
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TRVGrid(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVGrid(HWND ParentWindow) : TRVCustomGrid(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvgrids */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVGRIDS)
using namespace Rvgrids;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvgridsHPP
