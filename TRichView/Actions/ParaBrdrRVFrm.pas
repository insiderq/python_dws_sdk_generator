
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for paragraph borders and background     }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}



unit ParaBrdrRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Math,
  Dialogs, BorderSidesRVFrm, RVStyle, StdCtrls, RVColorCombo, RVScroll,
  RichView, Buttons, RVSpinEdit, RVOfficeRadioBtn, ImgList, RVTable,
  ComCtrls, RVColorGrid, RVALocalize, ColorRVFrm, RVGrids, RichViewActions;

type
  TfrmRVParaBrdr = class(TfrmRVBorderSides)
    ImageList1: TImageList;
    lblIW: TLabel;
    seIW: TRVSpinEdit;
    btnOffsets: TButton;
    rgBorderType: TRVOfficeRadioGroup;
    gbFillColor: TGroupBox;
    lblBackColor: TLabel;
    rvcg: TRVColorGrid;
    btnColor: TButton;
    btnPadding: TButton;
    ColorDialog1: TColorDialog;
    lblIWMU: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure rgBorderTypeClick(Sender: TObject);
    procedure cmbColorColorChange(Sender: TObject);
    procedure btnClick(Sender: TObject);
    procedure seIWChange(Sender: TObject);
    procedure btnOffsetsClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure btnPaddingClick(Sender: TObject);
    procedure rvcgColorChange(Sender: TObject);
    procedure btnColorClick(Sender: TObject);
    procedure cmbWidthClick(Sender: TObject);
  private
    { Private declarations }
    _gbSample, _PageControl1, _lblBackColor: TControl;
    FBackColor: TColor;
    LblBackColorRight: Integer;
    table: TRVTableItemInfo;
    BOLeft, BOTop, BORight, BOBottom: TRVStyleLength;
    UBOLeft, UBOTop, UBORight, UBOBottom: Boolean;
    PaddingLeft, PaddingTop, PaddingRight, PaddingBottom: TRVStyleLength;
    FPaddingLeft, FPaddingTop, FPaddingRight, FPaddingBottom: Boolean;
    Updating: Boolean;
    function GetOff(Offs: TRVStyleLength; UseIt: Boolean): TRVStyleLength;
    procedure SetBackColor(Value: TColor);
    function GetCD: TColorDialog;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    ColorDialog: TColorDialog;
    BCIndeterminate, OnlyPositiveOffsets: Boolean;
    procedure UpdateSample;
    procedure SetBO(Offsets: TRVRect; UseOffsets: TRVBooleanRect);
    procedure GetBO(Offsets: TRVRect; UseOffsets: TRVBooleanRect);
    procedure SetPadding(Padding: TRVRect; UsePadding: TRVBooleanRect);
    procedure GetPadding(Padding: TRVRect; UsePadding: TRVBooleanRect);
    procedure Localize; override;
    procedure SetToForm(RVStyle: TRVStyle; Border: TRVBorder;
      Background: TRVBackgroundRect; ValidProperties: TRVParaInfoBorderProperties);
    procedure GetFromForm(Border: TRVBorder;
      Background: TRVBackgroundRect;
      var ValidProperties: TRVParaInfoBorderProperties);
    property BackColor: TColor read FBackColor write SetBackColor;
  end;


implementation
uses FourSidesRVFrm, BaseRVFrm;

{$R *.dfm}

procedure TfrmRVParaBrdr.FormCreate(Sender: TObject);
var PreviewText: TRVALocString;
begin
  _gbSample := gbSample;
  _PageControl1 := PageControl1;
  _lblBackColor := lblBackColor;
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiHTML:
      begin
        btnOffsets.Visible := False;
        rgBorderType.Items[3].Free;
        rgBorderType.Items[3].Free;
        rgBorderType.Items[3].Free;
      end;
    rvauiRTF:
      begin
        btnPadding.Visible := False;
        lblIW.Visible := False;
        lblIWMU.Visible := False;
        seIW.Visible := False;
      end;
  end;
  UpdateLengthSpinEdit(seIW, True, True);
  inherited;
  SetControlCaption(_lblBackColor, '');
  BCIndeterminate := True;
  FBackColor := clNone;
  {$IFDEF USERVTNT}
  rvs.TextStyles[0].Unicode := True;
  {$ENDIF}
  table := TRVTableItemInfo.CreateEx(1,1, rv.RVData);
  table.Cells[0,0].VAlign := rvcMiddle;
  table.Cells[0,0].Clear;
  {$IFDEF USELOREMIPSUM}
  PreviewText := LoremIpsum1;
  {$ELSE}
  PreviewText := RVA_GetS(rvam_pbb_PreviewText, ControlPanel);
  {$ENDIF}
  table.Cells[0,0].{$IFDEF USERVTNT}AddNLWTag{$ELSE}AddNLTag{$ENDIF}(PreviewText,0,0, RVEMPTYTAG);
  table.Cells[0,0].SetAddParagraphMode(False);
  table.Cells[0,0].{$IFDEF USERVTNT}AddNLWTag{$ELSE}AddNLTag{$ENDIF}(PreviewText,0,0, RVEMPTYTAG);
  {$IFnDEF USELOREMIPSUM}
  table.Cells[0,0].{$IFDEF USERVTNT}AddNLWTag{$ELSE}AddNLTag{$ENDIF}(PreviewText,0,0, RVEMPTYTAG);
  {$ENDIF}
  table.Cells[0,0].SetAddParagraphMode(True);
//  table.Cells[0,0].BestHeight := rv.Height;
  table.CellPadding := 0;
  table.BorderVSpacing := 0;
  table.BorderHSpacing := 0;
  table.CellHSpacing := 0;
  table.CellVSpacing := 0;
  table.ParaNo := 1;
  rv.AddItem('', table);
  rv.Format;
  PrepareImageList(ImageList1);
end;

procedure TfrmRVParaBrdr.FormActivate(Sender: TObject);
begin
  inherited;
  cmbColor.ColorDialog := GetCD;
  UpdateSample;
end;

procedure TfrmRVParaBrdr.UpdateSample;
var ParaInfo: TParaInfo;
    BW: Integer;
begin
  if not Visible then
    exit;
  ParaInfo := rvs.ParaStyles[0];
  if rgBorderType.ItemIndex>=0 then
    ParaInfo.Border.Style := TRVBorderStyle(rgBorderType.ItemIndex)
  else
    ParaInfo.Border.Style := rvbNone;
  ParaInfo.Border.Color := cmbColor.ChosenColor;
  if GetXBoxItemIndex(_cmbWidth)>=0 then
    ParaInfo.Border.Width := RVA_GetComboBorderWidth2(GetXBoxItemIndex(_cmbWidth),
      False, rvs, TRVAControlPanel(ControlPanel))
  else
    ParaInfo.Border.Width := 1;
  ParaInfo.Border.InternalWidth := rvs.RVUnitsToUnits(seIW.Value,
    RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)));

  with ParaInfo.Border.VisibleBorders do begin
    Left   := GetButtonDown(_btnLeft);
    Top    := GetButtonDown(_btnTop);
    Right  := GetButtonDown(_btnRight);
    Bottom := GetButtonDown(_btnBottom);
  end;

  with ParaInfo.Border.BorderOffsets do begin
    Left   := GetOff(BOLeft, UBOLeft);
    Top    := GetOff(BOTop, UBOTop);
    Right  := GetOff(BORight, UBORight);
    Bottom := GetOff(BOBottom, UBOBottom);
  end;

  with ParaInfo.Background.BorderOffsets do begin
    Left   := GetOff(PaddingLeft, FPaddingLeft);
    Top    := GetOff(PaddingTop, FPaddingTop);
    Right  := GetOff(PaddingRight, FPaddingRight);
    Bottom := GetOff(PaddingBottom, FPaddingBottom);
  end;

  BW := ParaInfo.Border.GetTotalWidth;
  ParaInfo.LeftIndent :=  Max(GetOff(BOLeft, UBOLeft)+BW, GetOff(PaddingLeft, FPaddingLeft));
  ParaInfo.RightIndent := Max(GetOff(BORight, UBORight)+BW, GetOff(PaddingTop, FPaddingTop));
  ParaInfo.SpaceBefore := Max(GetOff(BOTop, UBOTop)+BW, GetOff(PaddingRight, FPaddingRight));
  ParaInfo.SpaceAfter :=  Max(GetOff(BOBottom, UBOBottom)+BW, GetOff(PaddingBottom, FPaddingBottom));

  ParaInfo.Background.Color := BackColor;

  table.Changed;
  rv.Format;

  seIW.Enabled      := (rgBorderType.ItemIndex>1);
end;

procedure TfrmRVParaBrdr.rgBorderTypeClick(Sender: TObject);
begin
  if not Visible then
    exit;
  if (rgBorderType.ItemIndex=0) and (TRVAControlPanel(ControlPanel).UserInterface=rvauiRTF) then begin
    UBOLeft := True;
    UBOTop := True;
    UBORight := True;
    UBOBottom := True;
    FPaddingLeft := True;
    FPaddingTop := True;
    FPaddingRight := True;
    FPaddingBottom := True;
    BOLeft := 0;
    BOTop := 0;
    BORight := 0;
    BOBottom := 0;
    PaddingLeft := 0;
    PaddingTop := 0;
    PaddingRight := 0;
    PaddingBottom := 0;
  end;
  if rgBorderType.ItemIndex>0 then begin
    SetButtonDown(_btnLeft, True);
    SetButtonDown(_btnTop, True);
    SetButtonDown(_btnRight, True);
    SetButtonDown(_btnBottom, True);
    SetButtonFlat(_btnLeft, False);
    SetButtonFlat(_btnTop, False);
    SetButtonFlat(_btnRight, False);
    SetButtonFlat(_btnBottom, False);
  end;
  UpdateSample;
end;

function TfrmRVParaBrdr.GetCD: TColorDialog;
begin
  if ColorDialog<>nil then
    Result := ColorDialog
  else
    Result := ColorDialog1;
end;

procedure TfrmRVParaBrdr.cmbColorColorChange(Sender: TObject);
begin
  inherited;
  if not Visible then
    exit;
  if not cmbColor.Indeterminate and (rgBorderType.ItemIndex=0) then
    rgBorderType.ItemIndex := 1;
  UpdateSample;
end;

procedure TfrmRVParaBrdr.btnClick(Sender: TObject);
begin
  inherited;
  if (Sender is TSpeedButton) and TSpeedButton(Sender).Down and
    (rgBorderType.ItemIndex=0) then
    rgBorderType.ItemIndex := 1;
  if TRVAControlPanel(ControlPanel).UserInterface=rvauiRTF then begin
    if not GetButtonDown(_btnLeft) then begin
      UBOLeft := True;
      FPaddingLeft := True;
      BOLeft := 0;
      PaddingLeft := 0;
    end;
    if not GetButtonDown(_btnRight) then begin
      UBORight := True;
      FPaddingRight := True;
      BORight := 0;
      PaddingRight := 0;
    end;
    if not GetButtonDown(_btnTop) then begin
      UBOTop := True;
      FPaddingTop := True;
      BOTop := 0;
      PaddingTop := 0;
    end;
    if not GetButtonDown(_btnBottom) then begin
      UBOBottom := True;
      FPaddingBottom := True;
      BOBottom := 0;
      PaddingBottom := 0;
    end;
  end;
  UpdateSample;
end;

procedure TfrmRVParaBrdr.seIWChange(Sender: TObject);
begin
  UpdateSample;
end;

procedure TfrmRVParaBrdr.GetBO(Offsets: TRVRect;
  UseOffsets: TRVBooleanRect);
begin
  Offsets.Left   := BOLeft;
  Offsets.Top    := BOTop;
  Offsets.Right  := BORight;
  Offsets.Bottom := BOBottom;

  UseOffsets.Left   := UBOLeft;
  UseOffsets.Top    := UBOTop;
  UseOffsets.Right  := UBORight;
  UseOffsets.Bottom := UBOBottom;
end;

procedure TfrmRVParaBrdr.SetBO(Offsets: TRVRect;
  UseOffsets: TRVBooleanRect);
begin
  BOLeft   := Offsets.Left;
  BOTop    := Offsets.Top;
  BORight  := Offsets.Right;
  BOBottom := Offsets.Bottom;

  UBOLeft   := UseOffsets.Left;
  UBOTop    := UseOffsets.Top;
  UBORight  := UseOffsets.Right;
  UBOBottom := UseOffsets.Bottom;
end;

procedure TfrmRVParaBrdr.GetPadding(Padding: TRVRect;
  UsePadding: TRVBooleanRect);
begin
  with Padding do begin
    Left   := PaddingLeft;
    Top    := PaddingTop;
    Right  := PaddingRight;
    Bottom := PaddingBottom;
  end;

  with UsePadding do begin
    Left   := FPaddingLeft;
    Top    := FPaddingTop;
    Right  := FPaddingRight;
    Bottom := FPaddingBottom;
  end;
end;

procedure TfrmRVParaBrdr.SetPadding(Padding: TRVRect;
  UsePadding: TRVBooleanRect);
begin
  with Padding do begin
    PaddingLeft   := Left;
    PaddingTop    := Top;
    PaddingRight  := Right;
    PaddingBottom := Bottom;
  end;

  with UsePadding do begin
    FPaddingLeft   := Left;
    FPaddingTop    := Top;
    FPaddingRight  := Right;
    FPaddingBottom := Bottom;
  end;
end;

function TfrmRVParaBrdr.GetOff(Offs: TRVStyleLength; UseIt: Boolean): TRVStyleLength;
begin
  if UseIt then
    Result := Offs
  else
    Result := 0;
end;

procedure TfrmRVParaBrdr.btnOffsetsClick(Sender: TObject);
var frm: TfrmRVFourSides;
begin
  frm := TfrmRVFourSides.Create(Application, ControlPanel);
  try
    frm.SetFormCaption(RVA_GetS(rvam_pbb_OffsetsTitle, ControlPanel));
    frm.SetControlCaption(frm._gb, RVA_GetSHUnits(rvam_pbb_OffsetsGB,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), ControlPanel));
    frm.SetValues(BOLeft, BOTop, BORight, BOBottom,
       UBOLeft, UBOTop, UBORight, UBOBottom, rvs,
       OnlyPositiveOffsets or (TRVAControlPanel(ControlPanel).UserInterface<>rvauiFull),
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
    frm.HelpContext := 90301;
    if frm.ShowModal=mrOk then begin
      frm.GetValues(BOLeft, BOTop, BORight, BOBottom,
       UBOLeft, UBOTop, UBORight, UBOBottom, rvs,
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
      if TRVAControlPanel(ControlPanel).UserInterface=rvauiRTF then begin
        PaddingLeft := BOLeft;
        PaddingTop  := BOTop;
        PaddingRight := BORight;
        PaddingBottom := BOBottom;
        FPaddingLeft := UBOLeft;
        FPaddingTop := UBOTop;
        FPaddingRight := UBORight;
        FPaddingBottom := UBOBottom;
        if UBOLeft and (BOLeft>0) then
          SetButtonDown(_btnLeft, True);
        if UBORight and (BORight>0) then
          SetButtonDown(_btnRight, True);
        if UBOTop and (BOTop>0) then
          SetButtonDown(_btnTop, True);
        if UBOBottom and (BOBottom>0) then
          SetButtonDown(_btnBottom, True);
      end;
      if (rgBorderType.ItemIndex=0) then
        rgBorderType.ItemIndex := 1;
      UpdateSample;
    end;
  finally
    frm.Free;
  end;
end;

procedure TfrmRVParaBrdr.PageControl1Change(Sender: TObject);
begin
  _gbSample.Parent := GetPageControlActivePage(_PageControl1);
  _btnLeft.Visible := GetPageControlActivePageIndex(_PageControl1)=0;
  _btnRight.Visible := GetPageControlActivePageIndex(_PageControl1)=0;
  _btnTop.Visible := GetPageControlActivePageIndex(_PageControl1)=0;
  _btnBottom.Visible := GetPageControlActivePageIndex(_PageControl1)=0;
end;

type
  TControlHack = class (TControl)
  end;

procedure TfrmRVParaBrdr.SetBackColor(Value: TColor);
var rgb: Integer;
begin
  if Value<>clNone then
    Value := ColorToRGB(Value);
  if BCIndeterminate then begin
    SetControlCaption(_lblBackColor, '');
    rvcg.Indeterminate := True;
    end
  else begin
    FBackColor := Value;
    Updating := True;
    rvcg.ChosenColor := Value;
    Updating := False;
    if rvcg.Indeterminate then begin
      TControlHack(_lblBackColor).ParentBiDiMode := False;
      _lblBackColor.BiDiMode := bdLeftToRight;
      rgb := ColorToRGB(FBackColor);
      SetControlCaption(_lblBackColor, 'RGB('+
        IntToStr(rgb and $0000FF)+','+
        IntToStr((rgb and $00FF00) shr 8)+','+
        IntToStr((rgb and $FF0000) shr 16)+')');
      end
    else begin
      TControlHack(_lblBackColor).ParentBiDiMode := True;
      SetControlCaption(_lblBackColor, rvcg.GetColorName);
    end;
    if BiDiMode=bdRightToLeft then
      lblBackColor.Left := LblBackColorRight-lblBackColor.Width;    
  end;
  UpdateSample;
end;


procedure TfrmRVParaBrdr.btnPaddingClick(Sender: TObject);
var frm: TfrmRVFourSides;
begin
  frm := TfrmRVFourSides.Create(Application, ControlPanel);
  try
    frm.SetControlCaption(frm._gb, RVA_GetSHUnits(rvam_4s_DefTitle,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), ControlPanel));
    frm.SetValues(PaddingLeft, PaddingTop, PaddingRight, PaddingBottom,
       FPaddingLeft, FPaddingTop, FPaddingRight, FPaddingBottom, rvs,
       OnlyPositiveOffsets or (TRVAControlPanel(ControlPanel).UserInterface<>rvauiFull),
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
    if frm.ShowModal=mrOk then begin
      frm.GetValues(PaddingLeft, PaddingTop, PaddingRight, PaddingBottom,
       FPaddingLeft, FPaddingTop, FPaddingRight, FPaddingBottom, rvs,
       RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
      if TRVAControlPanel(ControlPanel).UserInterface=rvauiHTML then begin
        BOLeft    := PaddingLeft;
        BOTop     := PaddingTop;
        BORight   := PaddingRight;
        BOBottom  := PaddingBottom;
        UBOLeft   := FPaddingLeft;
        UBOTop    := FPaddingTop;
        UBORight  := FPaddingRight;
        UBOBottom := FPaddingBottom;
      end;
      UpdateSample;
    end;
  finally
    frm.Free;
  end;
end;


procedure TfrmRVParaBrdr.rvcgColorChange(Sender: TObject);
begin
  if not Visible or Updating then
    exit;
  BCIndeterminate := rvcg.Indeterminate;
  BackColor := rvcg.ChosenColor;
end;

procedure TfrmRVParaBrdr.btnColorClick(Sender: TObject);
begin
  if BackColor=clNone then
    GetCD.Color := clYellow
  else
    GetCD.Color := ColorToRGB(BackColor);
  if GetCD.Execute then begin
    BCIndeterminate := False;
    BackColor := GetCD.Color;
  end;
end;


procedure TfrmRVParaBrdr.cmbWidthClick(Sender: TObject);
var w: TRVLength;
begin
  inherited;
  if not Visible then
    exit;
  if (GetXBoxItemIndex(_cmbWidth)>=0) and (rgBorderType.ItemIndex=0) then
    rgBorderType.ItemIndex := 1;
  if (rgBorderType.ItemIndex>0) and (TRVAControlPanel(ControlPanel).UserInterface=rvauiRTF) then begin
    if GetXBoxItemIndex(_cmbWidth)>=0 then
      w := rvs.GetAsRVUnits(
        RVA_GetComboBorderWidth2(
          GetXBoxItemIndex(_cmbWidth), False, rvs, TRVAControlPanel(ControlPanel)),
          RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)))
    else
      w := 1;
    if (rgBorderType.ItemIndex>=4) then
      w := w*2;
    seIW.Value := w;
  end;
  UpdateSample;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVParaBrdr.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_pbb_Title, ControlPanel);
  ts1.Caption := RVA_GetSHAsIs(rvam_pbb_BorderTab, ControlPanel);
  ts2.Caption := RVA_GetSHAsIs(rvam_pbb_BackgroundTab, ControlPanel);
  gbSettings.Caption := RVA_GetSHAsIs(rvam_pbb_Settings, ControlPanel);
  lblColor.Caption := RVA_GetSAsIs(rvam_pbb_Color, ControlPanel);
  lblWidth.Caption := RVA_GetSAsIs(rvam_pbb_Width, ControlPanel);
  lblIW.Caption := RVA_GetSAsIs(rvam_pbb_InternalWidth, ControlPanel);
  lblIWMU.Caption := RVA_GetUnitsNameAsIs(
    RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)), True, ControlPanel);
  btnOffsets.Caption := RVA_GetSAsIs(rvam_pbb_Offsets, ControlPanel);
  rgBorderType.Caption := RVA_GetSH(rvam_pbb_BorderType, ControlPanel);
  rgBorderType.Items[0].Caption := RVA_GetS(rvam_pbb_BTNone, ControlPanel);
  rgBorderType.Items[1].Caption := RVA_GetS(rvam_pbb_BTSingle, ControlPanel);
  rgBorderType.Items[2].Caption := RVA_GetS(rvam_pbb_BTDouble, ControlPanel);
  if rgBorderType.Items.Count>3 then begin
    rgBorderType.Items[3].Caption := RVA_GetS(rvam_pbb_BTTriple, ControlPanel);
    rgBorderType.Items[4].Caption := RVA_GetS(rvam_pbb_BTThickInside, ControlPanel);
    rgBorderType.Items[5].Caption := RVA_GetS(rvam_pbb_BTThickOutside, ControlPanel);
  end;
  btnTop.Hint    := RVA_GetSAsIs(rvam_TopSide, ControlPanel)    + ' (Ctrl+T)|';
  btnLeft.Hint   := RVA_GetSAsIs(rvam_LeftSide, ControlPanel)   + ' (Ctrl+L)|';
  btnBottom.Hint := RVA_GetSAsIs(rvam_BottomSide, ControlPanel) + ' (Ctrl+B)|';
  btnRight.Hint  := RVA_GetSAsIs(rvam_RightSide, ControlPanel)  + ' (Ctrl+R)|';
  gbFillColor.Caption := RVA_GetSHAsIs(rvam_pbb_FillColor, ControlPanel);
  btnColor.Caption := RVA_GetSAsIs(rvam_pbb_MoreColors, ControlPanel);
  btnPadding.Caption := RVA_GetSAsIs(rvam_pbb_Padding, ControlPanel);
  rvcg.FirstCaption :=  RVA_GetS(rvam_cl_Transparent, ControlPanel);
  rvs.TextStyles[0].Charset := RVA_GetCharset(ControlPanel);
  gbSample.Caption := RVA_GetSHAsIs(rvam_pbb_Sample, ControlPanel);
  LblBackColorRight := lblBackColor.Left+lblBackColor.Width;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVParaBrdr.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _gbSample then
    _gbSample := NewControl
  else if OldControl = _PageControl1 then
    _PageControl1 := NewControl
  else if OldControl = _lblBackColor then
    _lblBackColor := NewControl
  else
    inherited;
end;
{$ENDIF}

procedure TfrmRVParaBrdr.SetToForm(RVStyle: TRVStyle; Border: TRVBorder;
      Background: TRVBackgroundRect;
      ValidProperties: TRVParaInfoBorderProperties);
var idx: Integer;
    br: TRVBooleanRect;
begin
  RVA_FillWidthComboBox(TRVAComboBox(_cmbWidth), Canvas, False, TRVAControlPanel(ControlPanel));
  rvs.Units := RVStyle.Units;
  ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;

  if rvpibBorder_Width in ValidProperties then begin
    idx := RVA_GetComboBorderItemIndex(Border.Width, False, RVStyle,
      GetXBoxItemCount(_cmbWidth), TRVAControlPanel(ControlPanel));
    SetXBoxItemIndex(_cmbWidth, idx);
  end;
  if rvpibBorder_Color in ValidProperties then
    cmbColor.ChosenColor := Border.Color;
  if rvpibBackground_Color in ValidProperties then begin
    BCIndeterminate := False;
    BackColor := Background.Color;
  end;

  if rvpibBorder_Style in ValidProperties then
    if ord(Border.Style)<= rgBorderType.Items.Count then
      rgBorderType.ItemIndex := ord(Border.Style)
    else
      rgBorderType.ItemIndex := 2; // double
  if rvpibBorder_InternalWidth in ValidProperties then
    seIW.Value := RVStyle.GetAsRVUnits(Border.InternalWidth,
      RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)));
  br := TRVBooleanRect.Create(False);
  br.Left   := rvpibBorder_BO_Left   in ValidProperties;
  br.Top    := rvpibBorder_BO_Top    in ValidProperties;
  br.Right  := rvpibBorder_BO_Right  in ValidProperties;
  br.Bottom := rvpibBorder_BO_Bottom in ValidProperties;
  SetBO(Border.BorderOffsets, br);
  br.Left   := rvpibBackground_BO_Left   in ValidProperties;
  br.Top    := rvpibBackground_BO_Top    in ValidProperties;
  br.Right  := rvpibBackground_BO_Right  in ValidProperties;
  br.Bottom := rvpibBackground_BO_Bottom in ValidProperties;
  SetPadding(Background.BorderOffsets, br);
  br.Free;
  if rvpibBorder_Vis_Left in ValidProperties then begin
    SetButtonFlat(_btnLeft, False);
    SetButtonDown(_btnLeft, Border.VisibleBorders.Left);
  end;
  if rvpibBorder_Vis_Top in ValidProperties then begin
    SetButtonFlat(_btnTop, False);
    SetButtonDown(_btnTop, Border.VisibleBorders.Top);
  end;
  if rvpibBorder_Vis_Right in ValidProperties then begin
    SetButtonFlat(_btnRight, False);
    SetButtonDown(_btnRight, Border.VisibleBorders.Right);
  end;
  if rvpibBorder_Vis_Bottom in ValidProperties then begin
    SetButtonFlat(_btnBottom, False);
    SetButtonDown(_btnBottom, Border.VisibleBorders.Bottom);
  end;
end;


procedure TfrmRVParaBrdr.GetFromForm(Border: TRVBorder;
  Background: TRVBackgroundRect;
  var ValidProperties: TRVParaInfoBorderProperties);
var br: TRVBooleanRect;
begin
  ValidProperties := [];
  if GetXBoxItemIndex(_cmbWidth)>=0 then begin
    Include(ValidProperties,rvpibBorder_Width);
    Border.Width := RVA_GetComboBorderWidth2(GetXBoxItemIndex(_cmbWidth),
      False, rvs, TRVAControlPanel(ControlPanel));
  end;
  if not cmbColor.Indeterminate then begin
    Include(ValidProperties,rvpibBorder_Color);
    Border.Color := cmbColor.ChosenColor;
  end;
  if not BCIndeterminate then begin
    Include(ValidProperties,rvpibBackground_Color);
    Background.Color := BackColor;
  end;
  if rgBorderType.ItemIndex>=0 then begin
    Include(ValidProperties,rvpibBorder_Style);
    Border.Style := TRVBorderStyle(rgBorderType.ItemIndex);
  end;
  if not seIW.Indeterminate then begin
    Include(ValidProperties,rvpibBorder_InternalWidth);
    Border.InternalWidth := rvs.RVUnitsToUnits(seIW.Value,
      RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)));
  end;
  br := TRVBooleanRect.Create(False);
  GetBO(Border.BorderOffsets, br);
  if br.Left then
    Include(ValidProperties, rvpibBorder_BO_Left);
  if br.Top then
    Include(ValidProperties, rvpibBorder_BO_Top);
  if br.Right then
    Include(ValidProperties, rvpibBorder_BO_Right);
  if br.Bottom then
    Include(ValidProperties, rvpibBorder_BO_Bottom);
  GetPadding(Background.BorderOffsets, br);
  if br.Left then
    Include(ValidProperties, rvpibBackground_BO_Left);
  if br.Top then
    Include(ValidProperties, rvpibBackground_BO_Top);
  if br.Right then
    Include(ValidProperties, rvpibBackground_BO_Right);
  if br.Bottom then
    Include(ValidProperties, rvpibBackground_BO_Bottom);
  br.Free;

  if not GetButtonFlat(_btnLeft) then begin
    Include(ValidProperties, rvpibBorder_Vis_Left);
    Border.VisibleBorders.Left := GetButtonDown(_btnLeft);
  end;
  if not GetButtonFlat(_btnTop) then begin
    Include(ValidProperties, rvpibBorder_Vis_Top);
    Border.VisibleBorders.Top := GetButtonDown(_btnTop);
  end;
  if not GetButtonFlat(_btnRight) then begin
    Include(ValidProperties, rvpibBorder_Vis_Right);
    Border.VisibleBorders.Right := GetButtonDown(_btnRight);
  end;
  if not GetButtonFlat(_btnBottom) then begin
    Include(ValidProperties, rvpibBorder_Vis_Bottom);
    Border.VisibleBorders.Bottom := GetButtonDown(_btnBottom);
  end;
end;

end.
