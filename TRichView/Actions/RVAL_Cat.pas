{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Catala (SP) translation                         }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Josep Mas i Dedeu, 2008-03-13          }
{                bep@xci.cat                            }
{ Updated, 2014-02-11                                   }
{*******************************************************}

unit RVAL_Cat;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'polzades', 'cm', 'mm', 'piques', 'p�xels', 'punts',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fitxer', '&Edici�', 'F&ormat', 'Fo&nt', '&Par�graf', '&Insereix', '&Taula', 'Fine&stra', '&Ajuda',
  // exit
  '&Surt',
  // top-level menus: View, Tools,
  '&Veure', 'Eines',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Mida', 'Estil', 'Selecciona', 'Ali&neaci� Contingut Cel�la', 'Contorns C&el�la',
  // menus: Table cell rotation
  '&Rotaci� de cel�les',
  // menus: Text flow, Footnotes/endnotes
  '&Flux de &text', '&Notes al peu',
  // ribbon tabs: tab1, tab2, view, table
  '&Inici', '&Avan�at', '&Vista', '&Taula',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'porta-retalls', 'Font', 'Par�graf', 'Llista', 'Edici�',
  // ribbon groups: insert, background, page setup,
  'Insertar', 'Fons', 'Configurar p�gina',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Enlla�os', 'Notes al peu', 'Vistes de document', 'Mostrar/Ocultar', 'Zoom', 'Opcions',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Insertar', 'Eliminar', 'Operacions', 'Contorns',
  // ribbon groups: styles 
  'Estils',
  // ribbon screen tip footer,
  'Prem F1 per a m�s ajuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documents recents', 'Guardar una c�pia del document', 'Vista pr�via i impresi� del document',
  // ribbon label: units combo
  'Unitats:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nou', 'Nou|Crea un nou document en blanc',
  // TrvActionOpen
  '&Obrir...', 'Obrir|Obra un document de disc',
  // TrvActionSave
  '&Guardar', 'Guardar|Guarda el document a disc',
  // TrvActionSaveAs
  'Guardar &Com...', 'Guardar Com...|Guarda el document a disc amb un nom nou',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta el document a un altre format',
  // TrvActionPrintPreview
  '&Vista Preliminar', 'Visualitza la impressi�|Mostra com s''imprimir� el document',
  // TrvActionPrint
  'Im&primeix...', 'Imprimeix|Estableix els par�metres d''impressi� i imprimeix el document',
  // TrvActionQuickPrint
  '&Imprimir', '&Impressi� r�pida', 'Imprimir|Imprimeix el document',
  // TrvActionPageSetup
  'Configu&raci� de la p�gina...', 'Configuraci� de la p�gina|Definir marges, Mida del paper, Orientaci�, Font, cap�aleres i peus de p�gina',
  // TrvActionCut
  'Re&talla', 'Retalla|Retalla la selecci� i posa-la al porta-retalls',
  // TrvActionCopy
  '&Copia', 'Copia|Copia la selecci� i posa-la al porta-retalls',
  // TrvActionPaste
  '&Enganxa', 'Enganxa|Enganxa els continguts del porta-retalls',
  // TrvActionPasteAsText
  'Enganxa com a &text', 'Enganxa com a text|Inserta el text del porta-retalls',
  // TrvActionPasteSpecial
  'Enganxament &Especial...', 'Enganxament especial|Enganxa els continguts del porta-retalls en el format especificat',
  // TrvActionSelectAll
  'Seleccionar &Tot', 'Seleccionar tot|Seleccionar tot el document',
  // TrvActionUndo
  '&Desf�s', 'Desf�s|Torna a la darrera acci�',
  // TrvActionRedo
  '&Ref�s', 'Ref�s|Ref�s la darrera acci� desfeta',
  // TrvActionFind
  '&Cerca...', 'Cerca|Cerca el text en el document',
  // TrvActionFindNext
  'Cerca el &seg�e&nt', 'Cerca el &seg�ent|Continua l''�ltim cerca',
  // TrvActionReplace
  '&Reempla�a...', 'Reempla�a|Cerca i ree&mpla�a el text al document',
  // TrvActionInsertFile
  '&Fitxer...', 'Insereix un fitxer|Insereix el contingut del fitxer al document',
  // TrvActionInsertPicture
  '&Imatge...', 'Insereix una imatge|Insereix una imatge del disc',
  // TRVActionInsertHLine
  '&L�nia horitzontal', 'Insereix una l�nia horitzontal|Insereix una l�nia horitzontal',
  // TRVActionInsertHyperlink
  'Hiperen&lla�...', 'Insereix un Hiperenlla�|Insereix un enlla� d''hypertext',
  // TRVActionRemoveHyperlinks
  '&Eliminar hiperenlla�', 'Eliminar hiperenlla�|Elimina tots els hiperenlla�os al text seleccionat',
  // TrvActionInsertSymbol
  '&S�mbol...', 'Insereix un s�mbol|Insereix un s�mbol',
  // TrvActionInsertNumber
  '&N�mero...', 'Insereix un N�mero|Insereix un n�mero',
  // TrvActionInsertCaption
  'Inser� T�tol...', 'Insereix un T�tol|Insereix un t�tol per a l''objecte seleccionat',
  // TrvActionInsertTextBox
  'Quadre &Text', 'Insereix un Quadre de Text|Insereix un quadre de text',
  // TrvActionInsertSidenote
  'Nota dins el marc', 'Insereix Nota dins el marc|Insereix una nota que apareix en un marc de text',
  // TrvActionInsertPageNumber
  'N�mero &P�gina', 'Insereix el N�mero de P�gina|Insereix el n�mero de p�gina',
  // TrvActionParaList
  '&Pics i numeraci�...', 'Pics i numeraci�|Aplica o edita els pics o numeraci� al par�graf seleccionat',
  // TrvActionParaBullets
  '&Vinyetes', 'Vinyetes|Afegeix o elimina vinyetes del par�graf',
  // TrvActionParaNumbering
  '&Numeraci�', 'Numeraci�|Afegeix o elimina numeraci� del par�graf',
  // TrvActionColor
  '&Color de Fons...', 'Fons|Canvia el color de fons del document',
  // TrvActionFillColor
  'Colo&r per emplenar...', 'Color per emplenar|Canvia el color de fons de  text, par�grafs, cel�la, taula o document',
  // TrvActionInsertPageBreak
  '&Insereix salt de p�gina', 'Insereix salt de p�gina|Insereix un salt de p�gina',
  // TrvActionRemovePageBreak
  '&Elimina salt de p�gina', 'Elimina salt de p�gina|Elimina el salt de p�gina',
  // TrvActionClearLeft
  'Neteja el flux de text a l''esquerra', 'Neteja el flux de text a l''esquerra|Posiciona aquest par�graf sota de qualsevol imatge alineada a l''esquerra',
  // TrvActionClearRight
  'Neteja el flux de text a la dreta', 'Neteja el flux de text a la  dreta|Posiciona aquest par�graf sota de qualsevol imatge alineada a la dreta',
  // TrvActionClearBoth
  'Neteja el flux de text a ambd�s costats', 'Neteja el flux de text ambd�s costats|Posiciona aquest par�graf sota de qualsevol imatge alineada a la dreta o a l''esquerra',
  // TrvActionClearNone
  'Flux de text &normal', 'Flux de text normal|Permet insertar text al voltant de les imatges alineades a la dreta i esquerra',
  // TrvActionVAlign
  'Posici� de l''&Objecte...', 'Posici� de l''Objecte|Canvia la posici� de l''objecte seleccionat',
  // TrvActionItemProperties
  '&Propietats Objecte...', 'Propietats Objecte|Defineix les propietats de l''objecte actiu',
  // TrvActionBackground
  '&Fons...', 'Fons|Escollir color i imatge de fons',
  // TrvActionParagraph
  '&Par�graf...', 'Par�graf|Canvia els atributs del par�graf',
  // TrvActionIndentInc
  '&Augmenta el sagnat', 'Augmenta el sagnat|Incrementa el sagnat esquerra del par�graf seleccionat',
  // TrvActionIndentDec
  '&Redueix el sagnat', 'Redueix el sagnat|Redueix el sagnat esquerra del par�graf seleccionat',
  // TrvActionWordWrap
  '&Ajust de paraula', 'Ajust de paraula|Commuta l''ajustament de paraula pel par�graf seleccionat',
  // TrvActionAlignLeft
  'A&lineaci� esquerra', 'Alineaci� esquerra|Alineaci� del text selecionat a l''esquerra',
  // TrvActionAlignRight
  'Alineaci� D&reta', 'Alineaci� dreta|Alineaci� del text selecionat a la dreta',
  // TrvActionAlignCenter
  'Alineaci� &Centrada', 'Alineaci� centrada|Centra el text seleccionat',
  // TrvActionAlignJustify
  '&Justifica', 'Justifica|Alinea el text seleccionat als dos costats',
  // TrvActionParaColor
  'Color &fons del par�graf...', 'Color fons del par�graf|Defineix el color de fons del par�graf',
  // TrvActionLineSpacing100
  'E&spai Interlineat', 'Espai Interlineat|Definexi l''espai Interlineat',
  // TrvActionLineSpacing150
  '1.5 I&nterlineat', '1.5 Interlineat|Definexi l''espai Interlineat igual a 1.5 l�nies',
  // TrvActionLineSpacing200
  'Espai Interlineat &Doble', 'Espai Interlineat Doble|Defineix l''espai interlineat al doble',
  // TrvActionParaBorder
  'Par�graf Contorns i Fons...', 'Par�graf Contorns i Fons|Defineix contorns i fons pel text seleccionat',
  // TrvActionInsertTable
  '&Insereix Taula...', '&Taula', 'Insereix taula|Insereix una taula nova',
  // TrvActionTableInsertRowsAbove
  'Insereix Fila &a Sobre', 'Insereix fila a sobre|Insereix una fila nova a sobre de la cel�la seleccionada',
  // TrvActionTableInsertRowsBelow
  'Insereix Fila a Sota', 'Insereix fila a sota|Insereix una fila nova a sota de la cel�la seleccionada',
  // TrvActionTableInsertColLeft
  'Insereix Columna a l''Esquerra', 'Insereix columna a l''esquerra|Insereix una columna nova a l''esquerra de la cel�la seleccionada',
  // TrvActionTableInsertColRight
  'Insereix Columna a la Dreta', 'Insereix columna a la dreta|Insereix una columna nova a la dreta de la cel�la seleccionada',
  // TrvActionTableDeleteRows
  'Suprimeix Files', 'Suprimeix files|Suprimeix les files de les cel�les seleccionades',
  // TrvActionTableDeleteCols
  'Suprimeix &Columnes', 'Suprimeix Columnes|Suprimeix les columnes de les cel�les seleccionades',
  // TrvActionTableDeleteTable
  'Suprimeix Taula', 'Suprimeix taula|Suprimeix la taula',
  // TrvActionTableMergeCells
  'Fusiona Cel�les', 'Fusiona ce&l�les|Fusiona les ce&l�les seleccionades',
  // TrvActionTableSplitCells
  'Divideix Cel�les...', 'Divideix cel�les|Divideix les cel�les seleccionades',
  // TrvActionTableSelectTable
  'Selecciona &Taula', 'Selecciona taula|Selecciona la taula',
  // TrvActionTableSelectRows
  'Selecciona Files', 'Selecciona files|Selecciona files',
  // TrvActionTableSelectCols
  'Selecciona Col&umnes', 'Selecciona Columnes|Selecciona columnes',
  // TrvActionTableSelectCell
  'Selecciona C&el�la', 'Selecciona Cel�la|Selecciona cel�les',
  // TrvActionTableCellVAlignTop
  'Alinea Cel�la a Part Superior', 'Alinea cel�la a la part superior|Alinea el contingut de la cel�la a la part superior',
  // TrvActionTableCellVAlignMiddle
  'Alinea Cel�la al &Mig', 'Alinea cel�la al mig|Alinea el contingut de la cel�la al mig',
  // TrvActionTableCellVAlignBottom
  'Alinea Cel�la a Part Inferior', 'Alinea cel�la a la part inferior|Alinea el contingut de la cel�la a la part inferior',
  // TrvActionTableCellVAlignDefault
  'Cel�la amb Alineament Vertical per &Defecte', 'Cel�la amb alineament vertical per defecte|Assignat alineament vertical per defecte a les cel�les seleccionades',
  // TrvActionTableCellRotationNone
  '&Sense rotaci� de cel�la', 'Sense rotaci� de cel�la|Girar el contingut de la cel�la 0�',
  // TrvActionTableCellRotation90
  'Girar cel�la &90�', 'Girar cel�la 90�|Girar el contingut de la cel�la 90�',
  // TrvActionTableCellRotation180
  'Girar cel�la &180�', 'Girar cel�la 180�|Girar el contingut de la cel�la 180�',
  // TrvActionTableCellRotation270
  'Girar cel�la &270�', 'Girar cel�la 270�|Girar el contingut de la cel�la 270�',
  // TrvActionTableProperties
  '&Propietats Taula...', 'Propietats taula|Canvia les propietats de la taula seleccionada',
  // TrvActionTableGrid
  'Mostra L�nies &Graella', 'Mostra l�nies graella|Mostra o oculta l�nies de la graella',
  // TRVActionTableSplit
  '&Dividir taula', 'Dividir taula|Divideix la taula en dos comen�ant des de la fila seleccionada',
  // TRVActionTableToText
  '&Converteix en text...', 'Convertir en text|Converteix la taula a text',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena les files de la taula',
  // TrvActionTableCellLeftBorder
  'Vora Esquerra', 'Vora Esquerra|Mostra o oculta la vora esquerra de la cel�la',
  // TrvActionTableCellRightBorder
  'Vora Dreta', 'Vora Dreta|Mostra o oculta la vora dreta de la cel�la',
  // TrvActionTableCellTopBorder
  'Vora Superior', 'Vora Superior|Mostra o oculta la vora superior de la cel�la',
  // TrvActionTableCellBottomBorder
  'Vora Inferior', 'Vora Inferior|Mostra o oculta la vora inferior de la cel�la',
  // TrvActionTableCellAllBorders
  'Totes les Vores', 'Totes les vores|Mostra o oculta totes les vores de la cel�la',
  // TrvActionTableCellNoBorders
  'Sense Vores', 'Sense Vores|Oculta totes les vores de la cel�la',
  // TrvActionFonts & TrvActionFontEx
  'Tipus Lletra...', 'Tipus de lletra|Canvia el tipus de lletra del text seleccionat',
  // TrvActionFontBold
  'Negreta', 'Negreta|Canvia l''estil del text seleccionat a negreta',
  // TrvActionFontItalic
  'Cursiva', 'Cursiva|Canvia l''estil del text seleccionat a cursiva',
  // TrvActionFontUnderline
  'S&ubratllat', 'Subratllat|Canvia l''estil del text seleccionat a subratllat',
  // TrvActionFontStrikeout
  'Ratllat', 'Ratllat|Ratlla el text seleccionat',
  // TrvActionFontGrow
  'Augmenta Tipus Lletra', 'Augmenta Tipus Lletra|Augmenta el 10% l''al�ada del text seleccionat',
  // TrvActionFontShrink
  'Redueix Tipus Lletra', 'Redueix Tipus Lletra|Redueix el 10% l''al�ada del text seleccionat',
  // TrvActionFontGrowOnePoint
  'Augmenta Tipus Lletra un Punt', 'Augmenta Tipus Lletra un Punt|Augmenta un punt l''al�ada del text seleccionat',
  // TrvActionFontShrinkOnePoint
  'Redueix Tipus Lletra un Punt', 'Redueix Tipus Lletra un Punt|Redueix un punt l''al�ada del text seleccionat',
  // TrvActionFontAllCaps
  'Tot Maj�scules', 'Tot Maj�scules|Canvia tots els car�cters del text seleccionat a maj�scules',
  // TrvActionFontOverline
  'Sobreratllat', 'Sobreratllat|Posar sobreratllat el text seleccionat',
  // TrvActionFontColor
  '&Color text...', 'Color del text|Canvia el color del text seleccionat',
  // TrvActionFontBackColor
  'Color Fons del Text...', 'Color Fons del Text|Canvia el color del fons del text seleccionat',
  // TrvActionAddictSpell3
  'Verificaci� Ortografia', 'Verificaci� Ortografia|Verifica l''ortografia',
  // TrvActionAddictThesaurus3
  '&Tesaurus', 'Tesaurus|Proporciona sin�nims de la paraula seleccionada',
  // TrvActionParaLTR
  'D''Esquerra a Dreta', 'D''esquerra a dreta|Estableix la direcci� del text d''esquerra a dreta pel par�graf seleccionat',
  // TrvActionParaRTL
  'De Dreta a Esquerra', 'De dreta a esquerra|Estableix la direcci� del text de dreta a esquerra pel par�graf seleccionat',
  // TrvActionLTR
  'Text d''Esquerra a Dreta', 'Text d''Esquerra a Dreta|Estableix la direcci� d''esquerra a dreta pel text seleccionat',
  // TrvActionRTL
  'Text de Dreta a Esquerra', 'Text de Dreta a Esquerra|Estableix la direcci� de dreta a esquerra pel text seleccionat',
  // TrvActionCharCase
  'Maj�scules/min�scules', 'Maj�scules/min�scules|Canvia de maj�scules/min�scules el text seleccionat',
  // TrvActionShowSpecialCharacters
  'Visualitza car�cters &no imprimibles', 'Visualitza els car�cters no imprimibles|Mostra o oculta els car�cters no imprimibles, com marques de par�graf, tabuladors i espais',
  // TrvActionSubscript
  'Su&bratllat','Subratllat|Converteix el text seleccionat a subratllat',
  // TrvActionSuperscript
  'Sobreratllat','Sobreratllat|Converteix el text seleccionat a sobreratllat',
  // TrvActionInsertFootnote
  '&Nota al peu', 'Nota al peu|Inserta una nota al peu',
  // TrvActionInsertEndnote
  '&Nota final', 'Nota final|Inserta una nota final',
  // TrvActionEditNote
  'E&ditar nota', 'Editar nota|Permet editar les notes al peu o notes finals',
  // TrvActionHide
  '&Ocultar', 'Ocultar|Oculta o mostra el fragment seleccionat',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Estils...', 'Estils|Obrir el di�leg de gesti� d''estils',
  // TrvActionAddStyleTemplate
  '&Afegir Estil...', 'Afegir estil|Crear un nou estil de text o par�graf',
  // TrvActionClearFormat,
  'Treure Format', 'Treure Format|Treure tots els formats dels text o par�grafs del fragment seleccionat',
  // TrvActionClearTextFormat,
  'Treure Format &Text', 'Treure Format Text|Treure tots els formats del text seleccionat',
  // TrvActionStyleInspector
  '&Inspector Estils', 'Inspector Estils|Mostra o oculta l''Inspector d''Estils',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------------
   'D''acord', 'Cancel�la', 'Tanca', 'Insereix', '&Obre...', '&Desa...', '&Elimina', 'Ajuda', 'Eliminar',
  // Others  -------------------------------------------------------------------
  // percents
  'per cent',
  // left, top, right, bottom sides
  'Costat Esquerre', 'Costat Superior', 'Costat Dret', 'Costat Inferior',
  // save changes? confirm title
  'Voleu desar els canvis a %s?', 'Confirma',
  // warning: losing formatting
  '%s podria contenir caracter�stiques que no s�n compatibles amb el format seleccionat.'#13+
  'Voleu desar el document en aquest format?',
  // RVF format name
  'Format RichView',
  // Error messages ------------------------------------------------------------
  'Error',
  'Error en carregar el fitxer.'#13#13'Possibles causes:'#13'- el format d''aquest fitxer no �s suportat per aquesta aplicaci�;'#13+
  '- el fitxer �s corrupte;'#13'- el fitxer est� sent obert i bloquejat per una altra aplicaci�.',
  'Error carregant fitxer d''imatge.'#13#13'Possibles causes:'#13'- el format d''aquesta imatge no �s suportat per aquesta aplicaci�;'#13+
  '- el fitxer no cont� una imatge;'#13+
  '- el fitxer �s corrupte;'#13'- el fitxer est� sent obert i bloquejat per una altra aplicaci�.',
  'Error en desar el fitxer.'#13#13'Possibles causes:'#13'- Sense espai en disc;'#13+
  '- el disc est� protegit contra escriptura;'#13'- dispositiu extra�ble no introdu�t;'#13+
  '- el fitxer est� sent obert i bloquejat per una altra aplicaci�;'#13'- el disk est� corromput.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Fitxers RichView (*.rvf)|*.rvf',  'Fitxers RTF (*.rtf)|*.rtf' , 'Fitxers XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Fitxers Text (*.txt)|*.txt', 'Fitxers Text - Unicode (*.txt)|*.txt', 'Fitxers Text - Autodetecci� (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Fitxers HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - simplificat (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documents de Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'S''ha completat la cerca', 'La cadena a cercar ''%s'' no s''ha trobat.',
  // 1 string replaced; Several strings replaced
  '1 cadena reempla�ada.', '%d cadenes reempla�ades.',
  // continue search from the beginning/end?
  'S''ha arribat al final del document, continuar al comen�ament?',
  'S''ha arribat al comen�ament del document, continuar al final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Negre', 'Marr�', 'Verd Oliva', 'Verd Fosc', 'Verd Marbrenc', 'Blau Mar�', 'Anyil', 'Gris-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Vermell Fosc', 'Taronja', 'Groc Fosc', 'Verd', 'Verd Blav�s', 'Blau', 'Blau-Gris', 'Gris-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vermell', 'Taronja Clar', 'Llimona', 'Verd Mar', 'Blau Cel', 'Blau Clar', 'Violeta', 'Gris-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Or', 'Groc', 'Verd Viu', 'Turquesa', 'Blau Cel', 'Pruna', 'Gris-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa Clar', 'Canyella', 'Groc Clar', 'Verd Clar', 'Turquesa Clar', 'Blau P�l�lid', 'Lavanda', 'Blanc',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparent', '&Auto', '&M�s Colors...', 'Per &Defecte',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fons', 'C&olor:', 'Posici�', 'Fons', 'Texts de Mostra.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  'Cap', 'Ampliat', 'F&itxes Fixes', 'Fitxes', 'C&entrat',
  // Padding button
  '&Encoixinament...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Color per emplenar', '&Aplica a:', '&M�s Colors...', '&Encoixinament...',
  'Si us plau selecciona un color',
  // [apply to:] text, paragraph, table, cell
  'text', 'par�graf' , 'taula', 'cel�la',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Tipus de lletra', 'Tipus de lletra', 'Disposici�',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Tipus de lletra:', 'Mida', 'Estil', 'Negreta', 'Curs&iva',
  // Script, Color, Back color labels, Default charset
  'Seq��ncia:', '&Color:',  'Fons:', '(Per Defecte)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efectes', 'S&ubratllat', 'S&obreratllat', 'Barra&t', '&Tot maj�scules',
  // Sample, Sample text
  'Mostra', 'Text de mostra',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espaiat entre car�cters', 'E&spaiat:', '&Expandit', '&Condensat',
  // Offset group-box, Offset label, Down, Up,
  'Despla�ament vertical', 'Despla�ament:', 'Baixa', 'P&uja',
  // Scaling group-box, Scaling
  'Escalat horitzontal', 'Esc&alat:',
  // Sub/super script group box, Normal, Sub, Super
  'Subratllats i sobreratllats','&Normal','Su&bratllat','&Sobreratllat',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Encoixinament', 'Superior:', 'Esquerra:', 'Inferior:', 'Dreta:', 'Valors iguals',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insereix hiperenlla�', 'Hiperenlla�', 'T&ext:', 'Objec&tiu', '<<selecci�>>',
  // cannot open URL
  'No es pot navegar a "%s"',
  // hyperlink properties button, hyperlink style
  'Personalitza...', 'E&stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Atributs Hiperenlla�', 'Colors normals', 'Colors actius',
  // Text [color], Background [color]
  '&Text:', '&Fons:',
  // Underline [color]
  'Subratllat:',
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'F&ons:',
  // Underline [color], Attributes group-box,
  'Subratllat:', 'Atributs',
  // Underline type: always, never, active (below the mouse)
  'S&ubratllat:', 'sempre', 'mai', 'actiu',
  // Same as normal check-box
  '&Normal',
  // underline active links check-box
  'S&ubratllar enlla�os actius',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insereix un s�mbol', 'Tipus de lletra:', 'Joc de &car�cters:', 'Unicode',
  // Unicode block
  '&Bloca:',
  // Character Code, Character Unicode Code, No Character
  'Car�cter: %d', 'Car�cter: Unicode %d', '(sense car�cter)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Insereix una Taula', 'Mida de la taula', 'Nombre de &columnes:', 'Nombre de files:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Format de la taula', 'Mida &autom�tica', 'Ajusta la finestra', 'Mida personalitzada',
  // Remember check-box
  'Recorda &dimensions per a noves taules',
  // VAlign Form ---------------------------------------------------------------
  'Posici�',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size,, Line, Table, Rows, Cells tabs
  'Propietats', 'Imatge', 'Posici� i Mida', 'L�nia', 'Taula', 'Files', 'Cel�les',
  // Image Appearance, Image Misc, Number tabs
  'Aparen�a', 'Miscel�lania', 'N�mero',
  // Box position, Box size, Box appearance tabs
  'Posici�', 'Mida', 'Aparen�a',
  // Preview label, Transparency group-box, checkbox, label
  'Previsualitzaci�:', 'Transpar�ncia', '&Transparent', '&Color transparent:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Canvia...', 'De&sar...', 'Autom�tic',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alineament vertical', '&Alineaci�:',
  'inferior, a la l�nia base del text', 'centre, a la l�nia de base del text',
  'superior, a la part superior de la l�nia', 'inferior, a la part inferior de la l�nia', 'central, al centre de la l�nia',
  // align to left side, align to right side
  'a l''esquerra', 'a la dreta',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Canviar per:', 'Expandit', '&Amplada:', '&Al�ada:', 'Mida per defecte: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Escalar &proporcionalment', '&Reiniciar',
  // Inside the border, border, outside the border groupboxes
  'Dins el contorn', 'Contorn', 'Fora del contorn',
  // Fill color, Padding (i.e. margins inside border) labels
  'Color per emplenar:', 'Se&paraci�:',
  // Border width, Border color labels
  'Amplada contorn:', '&Color contorn:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espaiat &horitzontal:', 'Espaiat &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Miscel�lani', 'Indicador de funci�:',
  // spacing group-box, spacing, web group-box, alt text
  'Web', '&Text alternatiu:',
  // Horz line group-box, color, width, style
  'L�nia horitzontal', '&Color:', 'Amplada:', '&Estil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Taula', 'Amplada:', 'Color per emplenar:', 'Espaiat de &Cel�la...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Separaci� &horitzontal de cel�les:', 'Separaci� &vertical de cel�les:', 'Contorn i fons',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Visible c&ostats contorns...', 'Autom�tic', 'autom�tic',
  // Keep row content together checkbox
  'Mantenir contingut junt',
  // Rotation groupbox, rotacions: 0, 90, 180, 270
  'Rotaci�', 'Cap', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Contorn:', 'Visible c&ostats contorns...',
  // Border, CellBorders buttons
  'Contorns de la &taula...', 'Contorns de la &cel�la...',
  // Table border, Cell borders dialog titles
  'Contorns de la taula', 'Contorns de la &cel�la per defecte',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Impressi�', 'No permetre tallar les columnes en diferents p�gines', 'N�mero d''encap�alament de files:',
  'Repeteix l''encap�alament de les files de la taula a cada p�gina',
  // top, center, bottom, default
  'Superior', '&Centre', 'Inferior', 'Per &Defecte',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Par�metres', 'Amplada preferida:', 'Al�ada m�nima:', 'Color per emplenar:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Contorn', 'Costats visibles:',  'C&olor ombra:', 'Color clar', 'C&olor:',
  // Background image
  '&Imatge...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Posici� horitzontal', 'Posici� vertical', 'Posici� en el text',
  // Position types: align, absolute, relative
  'Alinear', 'Posici� absoluta', 'Posici� relativa',
  // [Align] left side, center, right side
  'costat esquerra del quadre al costat esquerra de',
  'centre del quadre al centre de',
  'costat dret del quadre al costat dret de',
  // [Align] top side, center, bottom side
  'costat superior del quadre al costat superior de',
  'centre del quadre al centre de',
  'costat inferior del quadre al costat inferior de',
  // [Align] relative to
  'relatiu a',
  // [Position] to the right of the left side of
  'a la dreta del costat esquerra',
  // [Position] below the top side of
  'per sota del costat superior',
  // Anchors: page, main text area (x2)
  '&P�gina', '�rea de text principal', 'P�gin&a', '�rea principal de te&xt',
  // Anchors: left margin, right margin
  'Marge esquerra', 'Marge d&ret',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Marge superior', 'Marge inferior', 'Marge &interior', 'Marge exteri&or',
  // Anchors: character, line, paragraph
  '&Car�cter', 'L&�nia', 'Par�&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Marg&es sim�trics', 'Format de la cel�la',
  // Above text, below text
  'Text superior', 'Text inferior',
  // Width, Height groupboxes, Width, Height labels
  'Amplada', 'Al�ada', 'Amplada:', 'Al�ada:',
  // Auto height label, Auto height combobox item
  'Autom�tic', 'autom�tic',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alineament vertical', 'Superior', '&Centre', 'Inferior',
  // Border and background button and title
  'C&ontorn i fons...', 'Contorn i fons quadre',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Enganxament especial', 'Enganxa com:', 'Format de text enriquit', 'format HTML',
  'Text', 'Text Unicode', 'Imatge Bitmap', 'Imatge Metafitxer',
  'Arxius d''imatges', 'URL',
  // Options group-box, styles
  'Opcions', '&Estils:',
  // style options: apply target styles, use source styles, ignore styles
 'Aplica estils al document dest�', 'mantenir estils i aparen�a',
 'mantenir aparen�a, ignorar estils',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Pics i Numeracions', 'Amb pics', 'Numerada', 'Llistes de pics', 'Llistes numerades',
  // Customize, Reset, None
  'Personalitza...', '&Restaura', 'Cap',
  // Numbering: continue, reset to, create new
  'Segueix la numeraci�', 'restaura la numeraci� a', 'crea una nova llista, iniciant des de',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Ar�bic gran', 'Rom� gran', 'Decimal', 'Ar�bia petit', 'Rom� gran',
  // Bullet type
  'Cercle', 'Disc', 'Quadrat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nivell:', '&Iniciar des de:', '&Continuar', 'Numeraci�',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personalitzar Llistes', 'Nivells', '&Comptador:', 'Propietats de la llista', 'Tipus de &llista:',
  // List types: bullet, image
  'vinyeta', 'imatge', 'Insereix un N�mero|',
  // Number format, Number, Start level from, Font button
  'Format &num�ric:', 'N�mero', 'Inicia el nivell de numeraci� des de:', 'Tipus de lletra...',
  // Image button, bullet character, Bullet button,
  '&Imatge...', 'Car�cter de Vinyeta', 'Vinyeta...',
  // Position of list text, bullet, number, image, text
  'Posici� text de llista', 'Posici� vinyeta', 'Posici� n�mero', 'Posici� Imatge', 'Posici� text',
  // at, left indent, first line indent, from left indent
  'de:', 'Sagnat &esquerra:', 'Sagnat de la primera l�nia:', 'des de sagnat esquerra',
  // [only] one level preview, preview
  'Previsualitzaci� d''un nivell', 'Previsualitzaci�',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Alineaci� esquerra', 'Alineaci� dreta', 'centre',
  // level #, this level (for combo-box)
  'Nivell %d', 'Aquest nivell',
  // Marker character dialog title
  'Edita el car�cter de vinyeta',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Contorn i Fons del par�graf', 'Contorn', 'Fons',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Par�metres', '&Color:', 'Amplada:', 'Amplada int&erna:', '&Marges de Text...',
  // Sample, Border type group-boxes;
  'Mostra', 'Tipus contorn',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Cap', '&Senzill', '&Doble', '&Triple', 'Gruix &interior', 'Gruix exteri&or',
  // Fill color group-box; More colors, padding buttons
  'Color per emplenar', '&M�s Colors...', 'Encoixinament...',
  // preview text
  'Text text text text. Text text text text. Text text text text.',
  // title and group-box in Offsets dialog
  'Marges del Text', 'Marges del Contorn',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Par�graf', 'Alineaci�', 'Esquerra', 'Dreta', '&Centre', '&Justifica',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espaiat', 'Anterior:', 'Posterior:', 'E&spaiat de l�nia:', 'de:',
  // Indents group-box; indents: left, right, first line
  'Sagnat', '&Esquerra:', 'Dreta:', 'Primera l�nia:',
  // indented, hanging
  'Sagnats', 'Suspesa', 'Mostra',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Senzill', '1.5 l�nies', 'Doble', 'Al m�nim','Exacte', 'M�ltiple',
  // preview text
  'Text text text text. Text text text text. Text text text text. Text text text text. Text text text text. Text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Sagnat i Espaiat', 'Tabulacions', 'Flux del text',
  // tab stop position label; buttons: set, delete, delete all
  'Posici� de la &tabulaci�:', 'Aplica', 'Suprimeix', 'Suprimeix-ho Tot',
  // tab align group; left, right, center aligns,
  'Alineaci�', '&Esquerra', 'Dreta', '&Centre',
  // leader radio-group; no leader
  'L�der', '(Cap)',
  // tab stops to be deleted, delete none, delete all labels
  'Les tabulacions seran suprimides:', '(Cap)', 'Totes.',
  // Pagination group-box, keep with next, keep lines together
  'Paginaci�', 'Conserva amb el seg�ent', 'Mantingues les l�nies juntes',
  // Outline level, Body text, Level #
  '&Nivell d''esquema:', 'Text de cos', 'Nivel %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Previsualitza la impressi�', 'Amplada de la p�gina', 'P�gina completa', 'P�gines:',
  // Invalid Scale, [page #] of #
  'Si us plau introdueix un n�mero del 10 al 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primera P�gina', 'P�gina Anterior', 'P�gina Seg�ent', '�ltima P�gina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espaiat de Cel�la', 'Espaiat', 'Entre &cel�les', 'Entre contorn de taula i cel�les',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horitzontal:', 'Ve&rtical:', 'H&oritzontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Contorns', 'Par�metres', '&Color:', 'Color de la &Llum:', 'Color de l''ombra:',
  // Width; Border type group-box;
  'Amplada:', 'Tipus contorn',
  // Border types: none, sunken, raised, flat
  '&Cap', 'Enfonsat', '&Elevat', 'Pla',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Divideix', 'Divideix a',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'E&specifica n�mero de files i columnes', 'Cel�la &Original',
  // number of columns, rows, merge before
  'N�mero de &columnes:', 'N�mero de files:', 'Combina abans de dividir',
  // to original cols, rows check-boxes
  'Divideix a les columnes originals', 'Divideix a les files originals',
  // Add Rows form -------------------------------------------------------------
  'Afegeix Files', 'Nombre de Files:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Configuraci� de la p�gina', 'P�gina', 'Cap�alera i peu de p�gina',
  // margins group-box, left, right, top, bottom
  'Marges (mil�l�metres)', 'Marges (polzades)', '&Esquerra:', 'Superior:', 'Dreta:', 'Inferior:',
  // mirror margins check-box
  'R�plica &Marges',
  // orientation group-box, portrait, landscape
  'Orientaci�', 'Vertical', 'Horitzontal',
  // paper group-box, paper size, default paper source
  'Paper', 'Mida:', 'Font:',
  // header group-box, print on the first page, font button
  'Cap�alera', '&Text:', '&Cap�alera a la primera p�gina', 'Tipus de lletra...',
  // header alignments: left, center, right
  '&Esquerra', '&Centre', 'Dreta',
  // the same for footer (different hotkeys)
  'Peu', 'Te&xt:', 'Peu a la primera &p�gina', 'Tipus de lletra...',
  '&Esquerra', 'Ce&ntre', 'Dreta',
  // page numbers group-box, start from
  'N�meros de p�gina', 'Comen�a per:',
  // hint about codes
  'Combinacions de car�cters especials:'#13'&&p - n�mero de p�gina; &&P - nombre de p�gines; &&d - data actual; &&t - hora actual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'P�ina de c�dis de l''arxiu de text', '&Selecciona la codificaci� d''arxius:',
  // thai, japanese, chinese (simpl), korean
  'Tailandesa', 'Japonesa', 'Xina (Simplificada)', 'Coreana',
  // chinese (trad), central european, cyrillic, west european
  'Xina (Tradicional)', 'D''Europa Central i Oriental', 'cir�l�lica', 'D''Europa Occidental',
  // greek, turkish, hebrew, arabic
  'Grega', 'turca', 'Hebrea', 'Ar�bic',
  // baltic, vietnamese, utf-8, utf-16
  'B�ltica', 'Vietnamita', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estil visual', '&Selecciona estil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertir en text', 'Selecciona un &delimitador:',
  // line break, tab, ';', ','
  'salt de l�nia', 'tabulaci�', 'punt i coma', 'coma',
  // Table sort form -----------------------------------------------------------
  // error message
  'No �s possible ordenar una taula amb files combinades',
  // title, main options groupbox
  'Criteri d''ordenaci� de taula', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar per la columna:', 'Distingir &maj�scules de min�scules',
  // heading row, range or rows
  '&Excloure fila de cap�alera', 'Files a ordenar: des de %d fins a %d',
  // order, ascending, descending
  'Ordre', '&Ascendent', '&Descendent',
  // data type, text, number
  'Tipus', '&Text', '&N�mero',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserir N�mero', 'Propietats', 'Nom &Comptador:', 'Tipus &Numeraci�:',
  // numbering groupbox, continue, start from
  'Numeraci�', 'C&ontinuar', 'Iniciar de&s de:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'T�tol', 'Etiqueta:', '&Excloure etiqueta de t�tol',
  // position radiogroup
  'Posici�', 'Objecte seleccion&at superior', 'O&bjecte seleccionat inferior',
  // caption text
  'T�tol &text:',  
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numeraci�', 'Figura', 'Taula',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Visible els costats del contorn', 'Contorn',
  // Left, Top, Right, Bottom checkboxes
  'Costat esquerra', 'Cos&tat superior', 'Costat d&ret', 'Costat inferior',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Canviar la mida de la columna', 'Canviar la mida de la fila',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Sagnat de la primera l�nia', 'Sagnat Esquerra', 'Sagnat Negatiu', 'Sagnat Dreta',
  // Hints on lists: up one level (left), down one level (right)
  'Un nivell cap amunt', 'Un nivell cap avall',
  // Hints for margins: bottom, left, right and top
  'Marge Inferior', 'Marge Esquerra', 'Marge Dreta', 'Marge Superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulaci� Alineat Esquerra', 'Tabulaci� Alineat Dreta', 'Tabulaci� Alineat Centre', 'Tabulaci� Alineat Decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Sagnat Normal', 'Sense espai', 'Encap�alament %d', 'Llista Par�grafs',
  // Hyperlink, Title, Subtitle
  'Hiperenlla�', 'T�tol', 'Subt�tol',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '�mfasi', 'Subt�tol �mfasi', '�mfasi Intens', 'Fort',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cita', 'Cita Intensa', 'Subt�tol Refer�ncia', 'Refer�ncia Intensa',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Bloc Text', 'Variable HTML', 'Codi HTML', 'Sigles HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definici� HTML', 'Teclat HTML', 'Exemple HTML', 'M�quina d''escriure HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Preformatejat HTML', 'Citar HTML', 'Encap�alament', 'Peu', 'N�mero P�gina',
  // Caption
  'T�tol',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Refer�ncia Nota al Final', 'Refer�ncia Nota al Peu', 'Text Nota al Final', 'Text Nota al Peu',
  // Sidenote Reference, Sidenote Text
  'Refer�ncia de nota al marge', 'Text de nota al marge',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'color', 'color fons', 'transparent', 'predeterminat', 'color subratllat',
  // default background color, default text color, [color] same as text
  'color predeterminat de fons', 'color predeterminat de text', 'mateix text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'senzill', 'gruix', 'doble', 'puntejat', 'puntejat gruixut', 'discontinua',
  // underline types: thick dashed, long dashed, thick long dashed,
  'discontinua gruixuda', 'tra�os llargs', 'tra�os llargs gruixuts',
  // underline types: dash dotted, thick dash dotted,
  'punts discontinus', 'punts discontinus gruixuts',
  // underline types: dash dot dotted, thick dash dot dotted
  'punt i ratlla discontinuu', 'punt i ratlla discontinuu gruixut',
  // sub/superscript: not, subsript, superscript
  'no sub/super�ndex', 'sub�ndex', 'super�ndex',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'mode bi-di:', 'heretat', 'd''Esquerra a dreta', 'de dreta a esquerra',
  // bold, not bold
  'Negreta', 'sense Negreta',
  // italic, not italic
  'Cursiva', 'sense Cursiva',
  // underlined, not underlined, default underline
  'subratllat', 'sense subratllar', 'subratllat per defecte',
  // struck out, not struck out
  'tatxat', 'sense tatxar',
  // overlined, not overlined
  'sobreratllat', 'sense sobreratllar',
  // all capitals: yes, all capitals: no
  'Tot maj�scules', 'cap maj�scula',
  // vertical shift: none, by x% up, by x% down
  'sense despla�ament vertical', 'despla�ada %d%% amunt', 'despla�ada %d%% avall',
  // characters width [: x%]
  'amplada car�cters',
  // character spacing: none, expanded by x, condensed by x
  'espaiat car�cters normal', 'espaiat ampliada per %s', 'espaiat condensat per %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'font per defecte', 'par�graf per defecte', 'heretat',
  // [hyperlink] highlight, default hyperlink attributes
  'real�ar', 'per defecte',
  // paragraph aligmnment: left, right, center, justify
  'alineat a l''esquerra', 'alineat a la dreta', 'centrat', 'justificat',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altura l�nia: %d%%', 'espaiat entre l�nies: %s', 'altura l�nia: almenys %s',
  'altura l�nia: exactament %s',
  // no wrap, wrap
  'ajust de l�nia desactivada', 'ajust de l�nia',
  // keep lines together: yes, no
  'ajuntar l�nies', 'no ajuntar les l�nies',
  // keep with next: yes, no
  'mantenir amb el par�graf seg�ent', 'no mantenir amb el par�graf seg�ent',
  // read only: yes, no
  'nom�s lectura', 'editable',
  // body text, heading level x
  'nivell d''esquema: text de cos', 'nivell d''esquema: %d',
  // indents: first line, left, right
  'sagnia primera l�nia', 'sagnia esquerra', 'sagnia dreta',
  // space before, after
  'espai abans', 'espai despr�s',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulacions', 'cap', 'alinear', 'esquerra', 'dreta', 'centre',
  // tab leader (filling character)
  'l�der',
  // no, yes
  'no', 's�',
  // [padding/spacing/side:] left, top, right, bottom
  'esquerra', 'superior', 'dreta', 'inferior',
  // border: none, single, double, triple, thick inside, thick outside
  'cap', 'simple', 'doble', 'triple', 'gruix interior', 'gruix exterior',
  // background, border, padding, spacing [between text and border]
  'fons', 'contorn', 'separaci�', 'espaiat',
  // border: style, width, internal width, visible sides
  'estil', 'amplada', 'amplada interna', 'costats visibles',
  // style inspector -----------------------------------------------------------
  // title
  'Inspector estils',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estil', '(Cap)', 'Par�graf', 'Font', 'Atributs',
  // border and background, hyperlink, standard [style]
  'Contorn i fons', 'Hiperenlla�', '(est�ndard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estils', 'Estil',
  // name, applicable to,
  '&Nom:', 'Aplicable &a:',
  // based on, based on (no &),
  '&Basat en:', 'Basat en:',
  //  next style, next style (no &)
  'Estil del par�gra&f seg�ent:', 'Estil del par�graf seg�ent:',
  // quick access check-box, description and preview
  '&Acc�s r�pid', 'Descripci� i &previsualitzaci�:',
  // [applicable to:] paragraph and text, paragraph, text
  'par�graf i text', 'par�graf', 'text',
  // text and paragraph styles, default font
  'Estil de Text i Par�graf', 'Font per Defecte',
  // links: edit, reset, buttons: add, delete
  'Edici�', 'Reiniciar', '&Afegir', '&Esborrar',
  // add standard style, add custom style, default style name
  'Afegir Estil E&st�ndard...', 'Afegir Estil Personalitzat', 'Estil %d',
  // choose style
  'Triar e&stil:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Estils RichView (*.rvst)|*.rvst', 'Error al carregar fitxer', 'Aquest fitxer no cont� estils',
  // Title, group-box, import styles
  'Importar Estils des del Fitxer', 'Importar', 'I&mportar estils:',
  // existing styles radio-group: Title, override, auto-rename
  'Estils existents', 's&obreescriure', '&afegir amb un altre nom',
  // Select, Unselect, Invert,
  '&Seleccionar', 'Desseleccionar', '&Invertir',
  // select/unselect: all styles, new styles, existing styles
  'Tots els Estils', 'Estils &Nous', 'Estils &Existents',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Esborrar Format', 'Tots els Estils',
  // dialog title, prompt
  'Estils', 'Triar estil per apli&car:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sin�nims', '&Ignora''ls tots', '&Afegeix al Diccionari',
  // Progress messages ---------------------------------------------------------
  'Descarregar %s', 'Preparant per imprimir...', 'Imprimint la p�gina %d',
  'Conversi� des de RTF...',  'Conversi� a RTF...',
  'Llegit %s...', 'Escrit %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Cap Nota', 'Nota al peu', 'Nota al final', 'Nota dins el marc', 'Quadre de Text'
  );


initialization
  RVA_RegisterLanguage(
    'Catalan', // english language name, do not translate
    'Catal�', // native language name
    ANSI_CHARSET, @Messages);

end.

