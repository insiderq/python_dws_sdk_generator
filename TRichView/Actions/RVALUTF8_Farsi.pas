﻿// This file is a copy of RVAL_Farsi.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Farsi (Persian) translation                     }
{                                                       }
{       Copyright (c) Muhammad B Mamouri                }
{       mamouri@rasasoftware.com                        }
{       http://www.rasasoftware.com                     }
{                                                       }
{*******************************************************}
{ Updated: Mehrdad Esmaeili 2011-Oct-6                  }
{ Updated: Mehrdad Esmaeili 2012-Oct-17                 }
{ Updated: Mehrdad Esmaeili 2014-May-01                 }
{*******************************************************}

unit RVALUTF8_Farsi;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'اينچ', 'سانتي‌متر', 'ميلي‌متر', 'پيكاس', 'پيكسل', 'پوينت',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&پرونده', '&ويرايش', '&قالب‌بندى', '&فونت', '&پاراگراف', '&درج', '&جدول', '&پنجره', '&راهنما',
  // exit
  '&خروج',
  // top-level menus: View, Tools,
  '&نمايش', 'ا&بزار',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'اندازه', 'شيوه', 'ا&نتخاب', 'چينش محتويات خانه', 'ح&واشى خانه',
  // menus: Table cell rotation
  'چرخاندن سلول',
  // menus: Text flow, Footnotes/endnotes
  'متن شناور', 'يادداشت پاياني',
  // ribbon tabs: tab1, tab2, view, table
  'خانه', 'پيشرفته', 'مشاهده', 'جدول',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'كليپ‌برد', 'فونت', 'پاراگراف', 'ليست', 'ويرايش',
  // ribbon groups: insert, background, page setup,
  'درج', 'پس‌زمينه', 'تنظيمات صفحه',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'پيوند‌ها', 'يادداشت پاياني', 'نماي سند', 'آشكار / پنهان', 'بزرگنمايي', 'گزينه‌ها',
  // ribbon groups on table tab: insert, delete, operations, borders
  'درج', 'حذف', 'عمليات', 'لبه‌ها',
  // ribbon groups: styles
  'سبک ها',
  // ribbon screen tip footer,
  'براي راهنمايي بيشتر كليد F1 را فشار دهيد',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'اسناد اخير', 'ذخيره نمودن يك كپي از سند', 'پيش‌نمايش و چاپ سند',
  // ribbon label: units combo
  'واحد‌ها:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&جديد', 'جديد|ايجاد يك سند خالي جديد',
  // TrvActionOpen
  '&فراخوانى ...', 'فراخوانى|فراخوانى يك سند از روى ديسك',
  // TrvActionSave
  '&ضبط', 'ضبط|ضبط سند بر روى ديسك',
  // TrvActionSaveAs
  'ضبط &با نام ...', 'ضبط نا نام|ضبط سند بر روي ديسك با نام جديد',
  // TrvActionExport
  '&صدور ...', 'صدور|صدور سند به يك فرمت جديد',
  // TrvActionPrintPreview
  'پ&يش‌نمايش چاپ', 'پيش‌نمايش چاپ|نمايش سند در قالبى كه چاپ مى‌شود',
  // TrvActionPrint
  '&چاپ ...', 'چاپ|تغيير تنظيمات چاپ و چاپ سند',
  // TrvActionQuickPrint
  'چاپ سريع', 'چاپ سريع', 'چاپ سريع|چاپ سريع سند' ,
  // TrvActionPageSetup
  'ت&نظيم صفحه', 'تنظيم صفحه|تنظيم حواشى، اندازه كاغذ، جهت كاغذ، منبع، سرصفحه و پاصفحه',
  // TrvActionCut
  '&برش', 'برش|برش متن انتخاب شده و قرار دادن آن در حافظه',
  // TrvActionCopy
  '&رونوشت', 'رونوشت|رونوشت گرفتن از متن انتخاب شده و قرار دادن آن در حافظه',
  // TrvActionPaste
  '&الصاق', 'الصاق|درج محتويات موجود در حافظه',
  // TrvActionPasteAsText
  'الصاق به صورت متني', 'الصاق به صورت متني|الصاق از كليپ برد به صورت متني',
  // TrvActionPasteSpecial
  'الصاق &ويژه', 'الصاق ويژه|درج محتويات حافظه به صورت فرمت مشخص شده',
  // TrvActionSelectAll
  'انتخاب &كل', 'انتخاب كل|انتخاب كل سند',
  // TrvActionUndo
  '&خنثى كردن آخرين عمل', 'خنثى كردن آخرين عمل|از بين بردن اثر آخرين عمل',
  // TrvActionRedo
  '&دوباره انجام دادن', 'دوباره انجام دادن|انجام دوباره آخرين مرحله',
  // TrvActionFind
  '&جستجو ...', 'جستجو|جستجوى يك متن خاص در سند',
  // TrvActionFindNext
  'جستجوى &بعدى', 'جستجوى بعدى|ادامه آخرين جستجو',
  // TrvActionReplace
  '&جايگزينى ...', 'جايگزينى|جستجو و جايگزينى يك متن خاص در سند',
  // TrvActionInsertFile
  '&فايل ...', 'درج فايل|درج محتويات فايل در سند',
  // TrvActionInsertPicture
  '&تصوير ...', 'درج تصوير|درج تصوير از روى ديسك',
  // TRVActionInsertHLine
  '&خط افقى', 'درج خط افقى|درج يك خط افقى',
  // TRVActionInsertHyperlink
  '&لينك', 'درج لينك|درج لينك',
  // TRVActionRemoveHyperlinks
  'حذف فراپيند‌ها', 'حذف فراپيوند‌ها|حذف تمامي فراپيوند‌ها در متن انتخابي',
  // TrvActionInsertSymbol
  '&علامت ويژه', 'درج علامت ويژه|درج علامت ويژه',
  // TrvActionInsertNumber
   'درج شماره', 'درج شماره|درج نمودن يک شماره',
  // TrvActionInsertCaption
   'درج عنوان...', 'درج عنوان|درج يک عنوان براي شيء انتخاب شده',
  // TrvActionInsertTextBox
   'جعبه متن', 'درج جعبه متن|درج يک جعبه متن',
  // TrvActionInsertSidenote
   'يادداشت جانبي', 'درج يادداشت جانبي|درج يادداشتي که در يک جعبه متن نمايش داده خواهد شد',
  // TrvActionInsertPageNumber
   'شماره صفحه', 'درج شماره صفحه|درج نمودن شماره صفحه',
  // TrvActionParaList
  '&نشانه‌گذار گرد و شمارشى...', 'نشانه‌گذار گرد و شمارشي|ساخت يا ويرايش نشانه‌گذار گرد يا شمارشي براي پاراگراف انتخابى',
  // TrvActionParaBullets
  '&نشانه‌گذار گرد', 'نشانه‌گذار گرد|حذف و اضافه نشانه‌گذار گرد براى پاراگراف',
  // TrvActionParaNumbering
  '&شماره انداز', 'شماره انداز|اضافه يا حذف شماره انداز از سند',
  // TrvActionColor
  '&رنگ پس زمينه ...', 'پس زمينه|تغيير پس زمينه سند',
  // TrvActionFillColor
  '&رنگ كردن', 'رنگ كردن|تغيير رنگ پس‌زمينه متن، پاراگراف، خانه، جدول يا سند',
  // TrvActionInsertPageBreak
  '&درج قطع‌كننده صفحه', 'درج قطع‌كننده صفحه|يك قطع‌كننده صفحه درج كن',
  // TrvActionRemovePageBreak
  '&حذف قطع‌كننده صفحه', 'حذف قطع‌كننده صفحه|حذف قطع كننده صفحه',
  // TrvActionClearLeft
  'پاك‌كردن متن شناور سمت چپ', 'پاك‌كردن متن شناور سمت چپ|قرار دادن پاراگراف انتخابي در پايين هر تصويري كه از سمت چپ تراز شده باشد',
  // TrvActionClearRight
  'پاك‌كردن متن شناور سمت راست', 'پاك‌كردن متن شناور سمت راست|قرار دادن پاراگراف انتخابي در پايين هر تصويري كه از سمت راست تراز شده باشد',
  // TrvActionClearBoth
  'پاك‌كردن متن شناور سمت چپ و راست', 'پاك‌كردن متن شناور سمت چپ و راست|قرار دادن پاراگراف انتخابي در پايين هر تصويري كه از سمت چپ يا راست تراز شده باشد',
  // TrvActionClearNone
  'متن شناور معمولي', 'متن شناور معمولي|متن مي‌تواند در اطراف تصاويري كه از سمت چپ يا راست تراز شده‌اند شناور گردد',
  // TrvActionVAlign
  'موقعيت شيء...', 'موقعيت شيء|تغيير موقعيت شيء انتخابي',
  // TrvActionItemProperties
  '&مشخصات شى', 'مشخصات شى|تعريف مشخصات شى فعال',
  // TrvActionBackground
  '&پس زمينه ...', 'پس‌زمينه|انتخاب رنگ و تصوير پس‌زمينه',
  // TrvActionParagraph
  '&پاراگراف ...', 'پاراگراف|تغيير مشخصات پاراگراف',
  // TrvActionIndentInc
  '&افزايش تورفتگى', 'افزايش تورفتگى|افزايش تورفتگى چپ پاراگراف انتخابى',
  // TrvActionIndentDec
  '&كاهش تورفتگي', 'كاهش تورفتگى|كاهش تورفتگي چپ پاراگراف انتخابى',
  // TrvActionWordWrap
  'چينش &لغات', 'چينش لغات|تغيير وضعيت چينش لغات براى پاراگراف فعلى',
  // TrvActionAlignLeft
  'تراز &چپ', 'تراز چپ|تراز كردن متن انتخابى به چپ',
  // TrvActionAlignRight
  'تراز &راست', 'تراز راست|تراز كردن متن انتخابى به راست',
  // TrvActionAlignCenter
  'تراز &وسط', 'تراز وسط|وسط چين كردن متن انتخابى',
  // TrvActionAlignJustify
  '&تراز كامل', 'تراز كامل|تراز متن انتخابى هم به راست هم به چپ',
  // TrvActionParaColor
  'رنگ &پس‌زمينه پاراگراف ...', 'رنگ پس‌زمينه پاراگراف|مقداردهى رنگ پس‌زمينه پاراگراف',
  // TrvActionLineSpacing100
  '&فاصله‌گذارى عادى خطوط', 'فاصله گذارى عادى خطوط|مقدار فاصله گذارى خطوط را عادى كن',
  // TrvActionLineSpacing150
  'فاصله‌گذارى 1.5 برابر خطوط', 'فاصله‌گذارى 1.5 برابر خطوط|فاصله خطوط را برابر اندازه 1.5 خط قرار بده',
  // TrvActionLineSpacing200
  'فاصله‌گذارى 2 برابر خطوط', 'فاصله‌گذارى 2 برابر خطوط|فاصله‌گذارى خطوط را 2 برابر قرار بده',
  // TrvActionParaBorder
  'پس‌زمينه و &حاشيه پاراگراف ...', 'پس‌زمينه و حاشيه پاراگراف|اعمال حاشيه و پس زمينه براى پاراگراف انتخابى',
  // TrvActionInsertTable
  'درج جدول', 'درج جدول', 'درج جدول|درج يك جدول جديد',
  // TrvActionTableInsertRowsAbove
  'درج ستون در &بالا', 'در ستون در بالا|درج يك ستون جديد بالاى خانه‌ى انتخابى',
  // TrvActionTableInsertRowsBelow
  'درج ستون در &پايين', 'درج ستون در پايين|درج يك ستون جديد پايين خانه‌ى انتخابى',
  // TrvActionTableInsertColLeft
  'درج ستون در &چپ', 'درج ستون در چپ|درج يك ستون جديد در سمت چپ خانه‌ى انتخابى',
  // TrvActionTableInsertColRight
  'درج ستون در &راست', 'درج ستون در راست|درج يك ستون جديد در سمت راست خانه‌ى انتخابى',
  // TrvActionTableDeleteRows
  '(حذف &رديف(ها', '(حذف رديف(ها|رديفهاى خانه‌هاى انتخاب شده را حذف كن',
  // TrvActionTableDeleteCols
  '(حذف ستون(ها', '(حذف ستون(ها|ستون‌هاى رديف‌هاى انتخاب شده را حذف كن',
  // TrvActionTableDeleteTable
  '&حذف جدول', 'حذف جدول|حذف جدول',
  // TrvActionTableMergeCells
  '&ادغام خانه‌ها ...', 'ادغام خانه‌ها|ادغام خانه‌هاى انتخابى',
  // TrvActionTableSplitCells
  '&جداكردن خانه‌ها ...', 'جداكردن خانه‌ها|جدا كردن خانه‌هاى انتخابى',
  // TrvActionTableSelectTable
  'انتخاب &جدول', 'انتخاب جدول|انتخاب كردن جدول',
  // TrvActionTableSelectRows
  '&انتخاب رديف‌ها', 'انتخاب رديف‌ها|انتخاب كردن رديف‌ها',
  // TrvActionTableSelectCols
  'انتخاب &ستون‌ها', 'انتخاب ستون‌ها|انتخاب كردن ستون‌ها',
  // TrvActionTableSelectCell
  'انتخاب &خانه', 'انتخاب خانه|انتخاب كردن خانه',
  // TrvActionTableCellVAlignTop
  'تراز خانه به &بالا', 'تراز خانه به بالا|تراز كردن محتويات خانه به بالا',
  // TrvActionTableCellVAlignMiddle
  'تراز خانه به &وسط', 'تراز خانه به وسط|تراز كردن ممتويات خانه به وسط',
  // TrvActionTableCellVAlignBottom
  'تراز خانه به &پايين', 'تراز خانه به پايين|تراز كردن محتويات خانه به پايين',
  // TrvActionTableCellVAlignDefault
  'تراز پيش فرض &عمودى خانه', 'تراز پيش فرض عمودي خانه|انتخاب تراز پيش فرض عمودى براي خانه انتخابى',
  // TrvActionTableCellRotationNone
  'لغو چرخاندن سلول', 'لغو چرخاندن سلول|لغو چرخش محتويات سلول',
  // TrvActionTableCellRotation90
  'چرخاندن سلول به اندازه 90 درجه', 'چرخاندن سلول به اندازه 90 درجه|چرخاندن محتويات سلول به اندازه 90 درجه',
  // TrvActionTableCellRotation180
  'چرخاندن سلول به اندازه 180 درجه', 'چرخاندن سلول به اندازه 180 درجه|چرخاندن محتويات سلول به اندازه 180 درجه',
  // TrvActionTableCellRotation270
  'چرخاندن سلول به اندازه 270 درجه', 'چرخاندن سلول به اندازه 270 درجه|چرخاندن محتويات سلول به اندازه 270 درجه',
  // TrvActionTableProperties
  '&مشخصات جدول ...', 'مشخصات جدول|تغيير مشخصات جدول انتخابى',
  // TrvActionTableGrid
  'نمايش صفحه شطرنجى', 'نمايش صفحه شطرنجى|آشكار يا مخفى كردن صفحه شطرنجى در جدول',
  // TRVActionTableSplit
  'تقسيم جدول', 'تقسيم جدول|تقسيم جدول به دو جدول',
  // TRVActionTableToText
  'تبديل به متن...', 'تبديل به متن|تبديل جدول به متن',
  // TRVActionTableSort
  'مرتب‌سازي...', 'مرتب‌سازي|مرتب‌سازي رديف‌هاي جدول',
  // TrvActionTableCellLeftBorder
  'حاشيه &چپ', 'حاشيه چپ|آشكار يا مخفى كردن حاشيه چپ خانه',
  // TrvActionTableCellRightBorder
  'حاشيه &راست', 'حاشيه راست|آشكار يا مخفي كردن حاشيه راست خانه',
  // TrvActionTableCellTopBorder
  'حاشيه &بالا', 'حاشيه بالا|آشكار يا مخفي كردن حاشيه بالاي خانه',
  // TrvActionTableCellBottomBorder
  'حاشيه &پايين', 'حاشيه پايين|آشكار يا مخفي كردن حاشيه پايين خانه',
  // TrvActionTableCellAllBorders
  'همه &حاشيه‌ها', 'همه حاشيه‌ها|آشكار يا مخفي كردن همه حاشيه‌هاى خانه‌ها',
  // TrvActionTableCellNoBorders
  '&بدون حاشيه', 'بدون حاشيه|مخفى كردن همه حاشيه‌هاى خانه',
  // TrvActionFonts & TrvActionFontEx
  '&قلم ...', 'قلم|تغيير قلم متن انتخابى',
  // TrvActionFontBold
  '&سياه', 'سياه|تغيير سبك متن انتخاب شده به سياه',
  // TrvActionFontItalic
  '&ايتاليك', 'ايتاليك|تغيير سبك متن انتخاب شده به ايتاليك',
  // TrvActionFontUnderline
  '&زير خط دار', 'زير خط دار|تغيير سبك متن انتخاب شده به زير خط دار',
  // TrvActionFontStrikeout
  '&خط ميانه', 'خط ميانه|قرار دادن خط ميانه روى متن انتخابى',
  // TrvActionFontGrow
  '&افزايش اندازه قلم', 'افزايش اندازه قلم|افزايش 10%ى اندازه قلم متن انتخابى',
  // TrvActionFontShrink
  '&جمع شدن قلم', 'جمع شدن قلم|جمع كردن 10%ى متن انتخابى',
  // TrvActionFontGrowOnePoint
  '&افزايش يك درجه اندازه قلم', 'افزايش يك درجه اندازه قلم|افزايش اندازه قلم انتخاب شده تا يك اندازه',
  // TrvActionFontShrinkOnePoint
  'ج&مع شدن يك درجه قلم', 'جمع شدن يك درجه قلم|افزايش يك درجه جمعى قلم',
  // TrvActionFontAllCaps
  '&حروف بزرگ', 'حروف بزرگ|تغيير همه كاراكترهاى متن انتخابى به حروف بزرگ',
  // TrvActionFontOverline
  '&خط رو', 'خط رو|اضافه كردن خط روي متن انتخابى',
  // TrvActionFontColor
  '&رنگ متن ...', 'رنگ متن|تغيير رنگ متن انتخابى',
  // TrvActionFontBackColor
  'تغيير رنگ &پس‌زمينه', 'رنگ پس‌زمينه متن|تغيير رنگ پس‌زمينه متن انتخابى',
  // TrvActionAddictSpell3
  '&خطاياب', 'خطا ياب|خطايابى متن',
  // TrvActionAddictThesaurus3
  '&مجموعه لغات', 'مجموعه لغات|ارائه واژه مترادف براى كلمه انتخابى',
  // TrvActionParaLTR
  'چپ به راست', 'چپ به راست|جهت پاراگراف انتخابى را چپ به راست قرار بده',
  // TrvActionParaRTL
  'راست به چپ', 'راست به چپ|جهت پاراگراف انتخابى را راست به چپ قرار بده',
  // TrvActionLTR
  'متن چپ به راست', 'چپ به راست|جهت متن انتخابى را چپ به راست قرار بده',
  // TrvActionRTL
  'متن راست به چپ', 'متن راست به چپ|جهت متن انتخابى را راست به چپ قرار بده',
  // TrvActionCharCase
  'وضعيت كاراكتر', 'وضعيت كاراكتر|تغيير وضعيت كاراكتر انتخابى',
  // TrvActionShowSpecialCharacters
  'كاراكترهاي &غيرچاپى', 'كاراكترهاى غيرچاپى|نمايش يا مخفي كردن كاراكترهاي غيرچاپي، همچون علامتهاي پاراگراف، تب‌ها و فاصله‌ها',
  // TrvActionSubscript
  'زيرنويس (Subscript)', 'زيرنويس|تبديل متن انتخابي به زيرنويس (Subscript)',
  // TrvActionSuperscript
  'بالا‌نويس (Superscript)', 'بالا‌نويس|تبديل متن انتخابي به بالا‌نويس (Superscript)',
  // TrvActionInsertFootnote
  'پاورقي (Footnote)', 'پاورقي|درج يك پاورقي',
  // TrvActionInsertEndnote
  'يادداشت پاياني (Endnote)', 'يادداشت پاياني|درج يادداشت پاياني',
  // TrvActionEditNote
  'ويرايش يادداشت', 'ويرايش يادداشت|ويرايش پاورقي يا يادداشت پاياني',
  'مخفي', 'مخفي|آشكار يا مخفي كردن قطعه انتخابي',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  'سبک ها...', 'سبک ها|بازنمودن کادرمحاوره اي مديريت سبک',
  // TrvActionAddStyleTemplate
  'افزوردن سبک...', 'افزودن سبک|ايجاد سبک جديد پاراگراف يا متن',
  // TrvActionClearFormat,
  'پاک کردن فرمت', 'پاک کردن فرمت|پاک کردن فرمت از متن يا پاراگراف انتخاب شده',
  // TrvActionClearTextFormat,
  'پاک کردن فرمت متن', 'پاک کردن فرمت متن|پاک کردن تمامي فرمت هاي متن انتخاب شده',
  // TrvActionStyleInspector
  'بازبين سبک', 'بازبين سبک|نمايش يا پنهان نمودن بازبين سبک',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'تاييد', 'انصراف', 'بستن', 'درج', '&فراخوانى ...', '&ضبط ...', '&حذف', 'راهنما',  'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'درصد',
  // left, top, right, bottom sides
  'سمت چپ', 'سمت بالا', 'سمت راست', 'سمت پايين',
  // save changes? confirm title
  'آيا تغييرات در %s ضبط گردد؟', 'پرسش',
  // warning: losing formatting
  '%s حاوي خصوصياتى است كه با فرمت انتخابى شما هماهنگ نيست.'#13+
  'آيا هنوز مايل هستيد كه متن خود را با اين فرمت ذخيره كنيد؟',
  // RVF format name
  'فرمت RichView',
  // Error messages ------------------------------------------------------------
  'خطا',
  'خطا در فراخوانى فايل.' + #13#13 + 'دلايل احتمالى:' + #13 + '- فرمت اين فايل به وسيله اين برنامه پشتيبانى نمى شود;'+#13+
  '- فايل خراب شده است;' + #13 + '- فايل به وسيله برنامه ديگرى باز و قفل شده است.',
  'خطا در فراخوانى تصوير.' + #13#13 + 'دلايل احتمالى:' + #13 + '- فرمت فايل از نوعى است كه توسط اين برنامه پشتيبانى نمى‌شود.' + #13+
  '- فايل حاوى تصوير نيست;' + #13+
  '- فايل خراب شده است;' + #13 + '- فايل به وسيله برنامه ديگرى باز و قفل شده است.',
  'خطا در ضبط فايل.' + #13#13 + 'دلايل احتمالى:' + #13 + '- فضاى كافى وجود ندارد;' + #13+
  '- ديسك در برابر نوشتن محافظت شده است;' + #13 + '- رسانه قابل حمل وارد دستگاه نشده است.' + #13+
  '- فايل به وسيله برنامه ديگرى باز و قفل شده است' + #13 + '- رسانه ديسك خراب شده است.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Files (*.rvf)|*.rvf',  'RTF Files (*.rtf)|*.rtf' , 'XML Files (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Text Files (*.txt)|*.txt', 'Text Files - Unicode (*.txt)|*.txt', 'Text Files - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Files (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplified (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Documents (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'جستجو پايان يافت', 'هيچ موردى در جستجو به دنبال ''%s'' يافت نشد',
  // 1 string replaced; Several strings replaced
  '1 رشته جايگزين شد.', '%d رشته جايگزين شد.',
  // continue search from the beginning/end?
  'جستجو به انتهاى متن رسيد، جستجو از ابتدا انجام شود؟',
  'جستجو به ابتداى متن رسيد، جستجو از انتها انجام شود؟',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'بدون پس زمينه', 'خودكار',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'سياه', 'قهوه‌اى', 'سبز زيتونى', 'سبز سير', 'خاكسترى مايل به آبى تيره', 'آبى تيره', 'نيلى', 'خاكسترى-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'قرمز تيره', 'نارنجى', 'زرد تيره', 'سبز', 'خاكسترى مايل به آبى', 'آبى', 'آبى-خاكسترى', 'خاكسترى-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'قرمز', 'نارنجى روشن', 'ليمويى', 'سبز مايل به آبى', 'لاجوردى', 'آبى روشن', 'بنفش', 'خاكسترى-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'صورتى', 'طلائى', 'زرد', 'سبز روشن', 'فيروزه‌اى', 'آبى آسمانى', 'قرمز مايل به خاكسترى', 'خاكسترى-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'سرخ رنگ', 'قهوه‌اى مايل به زرد', 'زرد روشن', 'سبز روشن', 'فيروزه‌اى روشن', 'آبى رنگ پوريده', 'بنفش كمرنگ', 'سفيد',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&لدون پس‌زمينه', '&خودكار', '&رنگ‌هاى بيشتر ...', '&پيش فرض',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'پس‌زمينه', '&رنگ:', 'موقعيت', 'پس زمينه', 'متن نمونه.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&هيچ كدام', 'پنجره &كامل', 'كاشى &ثابت', 'آ&جر كاشى', '&وسط',
  // Padding button
  '&حاشيه‌ها ...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'پر كردن رنگ', 'ا&عمال به:', 'ر&نگ‌هاى بيشتر ...', '&حاشيه‌ها',
  'لطفا رنگى انتخاب كنيد.',
  // [apply to:] text, paragraph, table, cell
  'متن', 'پاراگراف' , 'جدول', 'خانه',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'قلم', 'قلم', 'طرح‌بندى',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&قلم:', 'ا&ندازه', 'سبك', 'س&ياه', 'ايتا&ليك',
  // Script, Color, Back color labels, Default charset
  '&متن:', '&رنگ:', 'پس زمينه:', '(پيش فرض)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'تاثير', 'زير خط دار', '&خط بالا دار', '&خط رو دار', 'با حروف &بزرگ',
  // Sample, Sample text
  'نمونه', 'متن نمونه',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'فاصله گذارى كاراكترها', '&فاصله:', '&بسط يافته', '&فشرده',
  // Offset group-box, Offset label, Down, Up,
  'بالانس', '&بالانس:', '&پايين', '&بالا',
  // Scaling group-box, Scaling
  'مقياس‌گذارى افقى', '&مقياس گذارى:',
  // Sub/super script group box, Normal, Sub, Super
  'زيرنويس و بالانويس (Subscript/Superscript) :', 'عادي', 'زيرنويس', 'بالا‌نويس',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'تنظيم فواصل پس‌زمينه', '&بالا:', '&چپ:', '&پايين:', '&راست:', '&مقادير برابر',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'درج لينك', 'لينك', '&متن:', '&نشانه:', '<<انتخاب>>',
  // cannot open URL
  'امكان هدايت به "%s" وجود ندارد',
  // hyperlink properties button, hyperlink style
  '&دلبخواه ....', 'سبک:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ٌolors
  'مشخصات لينك', 'رنگهاى معمول', 'رنگهاى فعال',
  // Text [color], Background [color]
  '&متن:', '&پس زمينه:',
  // Underline [color]
  'رنگ زيرخط:',
  // Text [color], Background [color] (different hotkeys)
  'م&تن:', 'پس زمينه:',
  // Underline [color], Attributes group-box,
  'رنگ زيرخط:', 'خصوصيات',
  // Underline type: always, never, active (below the mouse)
  'نوع زيرخط:', 'هميشگي', 'هيچ وقت', 'فعال',
  // Same as normal check-box
  'به شكل &عادى',
  // underline active links check-box
  'زيرخط دار نمودن لينک هاي فعال',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'درج علامت ويژه', '&قلم:', '&كاراكتر ست:', 'يونيكد',
  // Unicode block
  'قالب يونيكد :',  
  // Character Code, Character Unicode Code, No Character
  'كد كاراكتر: %d', 'كد كاراكتر: يونيكد %d', '(هيچ كاراكترى)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'درج جدول', 'اندازه جدول', 'تعداد &ستون‌ها:', 'تعداد &رديف‌ها',
  // Table layout group-box, Autosize, Fit, Manual size,
  'طرح‌بندي جدول', 'اندازه پيش‌فرض', 'فيت كردن جدول در پنجره', 'تغيير اندازه دستي جدول',
  // Remember check-box
  'ابعاد را براي جدول جديد به خاطر بياور',
  // VAlign Form ---------------------------------------------------------------
  'موقعيت',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'مشخصات', 'تصوير', 'موقعيت و اندازه', 'خط', 'جدول', 'رديف‌ها', 'سلول‌ها',
  // Image Appearance, Image Misc, Number tabs
   'ظاهر', 'متفرقه', 'شماره',
  // Box position, Box size, Box appearance tabs
   'موقعيت', 'اندازه', 'ظاهر',
  // Preview label, Transparency group-box, checkbox, label
  'پيش نمايش:', 'شفافيت', 'شفاف', 'رنگ تشخيص نقاط شفاف :',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&تغيير ...', 'ذخيره...', 'رنگ پيش‌فرض',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'تراز عمودي', 'نحوه تراز :',
  'تراز نمودن پايين تصوير با خط پايه متن',   'تراز نمودن وسط تصوير با خط پايه متن',
  'تراز نمودن بالاي تصوير با بالاي خط',  'تراز نمودن پايين تصوير با پايين خط',  'تراز نمودن وسط تصوير با وسط خط',
  // align to left side, align to right side
  'تراز به سمت چپ',  'تراز به سمت راست',
  // Shift By label, Stretch group box, Width, Height, Default size
  'جا‌به‌جايي :', 'كشيدگي', 'پهنا :', 'ارتفاع :', 'اندازه پيش فرض:',
  // Scale proportionally checkbox, Reset ("no stretch") button
   'تغيير مقياس متناسب', 'بدون کشيدگي',
  // Inside the border, border, outside the border groupboxes
   'درون لبه', 'لبه', 'بيرون لبه',
  // Fill color, Padding (i.e. margins inside border) labels
   'اعمال رنگ:', 'حاشيه:',
  // Border width, Border color labels
   'ضخامت لبه:', 'رنگ لبه:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
   'فاصله گذاري افقي:', 'فاصله گذاري عمودي:',
  // Miscellaneous groupbox, Tooltip label
   'متفرقه', 'توضيح شناور (Tooltip) :',
  // web group-box, alt text
  'وب (Web)', 'متن جايگزين :',
  // Horz line group-box, color, width, style
  'خط افقي', 'رنگ :', 'ضخامت :', 'نوع :',
  // Table group-box; Table Width, Color, CellSpacing,
  'جدول', 'عرض جدول :', 'رنگ پس‌زمينه جدول :', 'فاصله‌گذاري سلول‌ها :',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
   'حاشيه افقي سلول:', 'حاشيه عمودي سلول:', 'لبه و پس زمينه',
  // Visible table border sides button, auto width (in combobox), auto width label
   'لبه هاي نمايان:', 'پيش فرض', 'پيش فرض',
  // Keep row content together checkbox
   'حفظ محتوا در کنار يکديگر',
  // Rotation groupbox, rotations: 0, 90, 180, 270
   'چرخش', 'بدون چرخش', '90 درجه', '180 درجه', '270 درجه',
  // Cell border label, Visible cell border sides button,
   'لبه:', 'لبه هاي جانبي نمايان:',
  // Border, CellBorders buttons
  'نوع لبه جدول ...', 'نوع لبه سلول‌ها ...  ',
  // Table border, Cell borders dialog titles
  'نوع لبه جدول', 'نوع لبه پيش‌فرض سلول‌ها',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'چاپ كردن', '&جدول را در يك صفحه تنها قرار بده', 'تعداد ستون‌هاى عنوان',
  'تعداد رديف‌هاي عنوان',  'رديف‌هاي عنوان در هر صفحه از جدول تكرار مي‌شوند',
  // top, center, bottom, default
  'بالا', 'وسط', 'پايين', 'پيش فرض',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'تنظيمات', 'پهناي مطلوب :', 'حداقل ارتفاع :', 'اعمال رنگ :',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'نوع لبه سلول‌ها', 'لبه‌هاي نمايان :',  'رنگ سايه :', 'رنگ روشن :', 'رنگ خطوط :',
  // Background image
  'تصوير پس‌زمينه ...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
   'موقعيت افقي', 'موقعيت عمودي', 'موقعيت در متن',
  // Position types: align, absolute, relative
   'موقعيت تطبيقي', 'موقعيت مطلق', 'موقعيت نسبي',
  // [Align] left side, center, right side
   'لبه چپ جعبه با لبه چپ',
   'مرکز جعبه با مرکز',
   'لبه راست جعبه با لبه راست',
  // [Align] top side, center, bottom side
   'لبه بالايي جعبه با لبه بالايي',
   'مرکز جعبه با مرکز',
   'لبه پاييني جعبه با لبه پاييني',
  // [Align] relative to
   'تراز نسبت به',
  // [Position] to the right of the left side of
   'موقعيت راست لبه چپي',
  // [Position] below the top side of
   'موقعيت پايين لبه بالايي',
  // Anchors: page, main text area (x2)
   'صفحه', 'محدوده متن اصلي', 'صفحه', 'محدوده متن اصلي',
  // Anchors: left margin, right margin
   'حاشيه چپ', 'حاشيه راست',
  // Anchors: top margin, bottom margin, inner margin, outer margin
   'حاشيه بالا', 'حاشيه پايين', 'حاشيه دروني', 'حاشيه بيروني',
  // Anchors: character, line, paragraph
   'کاراکتر', 'خط', 'پاراگراف',
  // Mirrored margins checkbox, layout in table cell checkbox
   'حواشي معکوس', 'جانمايي در سلول جدول',
  // Above text, below text
   'مافوق متن', 'مادون متن',
  // Width, Height groupboxes, Width, Height labels
   'پهنا', 'ارتفاع', 'پهنا:', 'ارتفاع:',
  // Auto height label, Auto height combobox item
   'خودکار', 'خودکار',
  // Vertical alignment groupbox; alignments: top, center, bottom
   'تراز عمودي', 'بالا', 'مرکز', 'پايين',
  // Border and background button and title
   'لبه و پس زمينه...', 'لبه و پس زمينه جعبه',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'الصاق ويژه...',  'الصاق به صورت :',  'فرمت Rich Text',
  'فرمت HTML',  'متن',  'متن Unicode',  'تصوير Bitmap',
  'تصوير Metafile',  'فايل گرافيكي',
  // Options group-box, styles
  'گزينه ها', 'سبک ها:',
  // style options: apply target styles, use source styles, ignore styles
  'اعمال سبک هاي سند هدف', 'حفظ سبک ها و ظاهر',
  'حفظ ظاهر و ناديده گرفتن سبک ها',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'نشانه‌گذار گرد و شمارشى', 'نشانه‌گذارى شده به صورت گرد', 'نشانه‌گذارى شده به صورت شمارشى', 'ليست‌هاى گرد شده', 'ليست‌هاى شمارشى',
  // Customize, Reset, None
  '&انتخابى شدن ...', '&باز نشاندن', 'هيچ كدام',
  // Numbering: continue, reset to, create new
  'ادامه شمارنده', 'باز نشاندن نمره‌گذارى', 'ايجاد يك ليست جديد، شروع از',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'حروف بزرگ انگليسي', 'حروف بزرگ رومي', 'اعداد دهدهي', 'حروف كوچك انگليسي', 'حروف كوچك رومي',
  // Bullet type
  'دايره', 'ديسك', 'مربع',
  // Level, Start from, Continue numbering, Numbering group-box
  'سطح (Level) :', 'شروع از :', 'ادامه', 'شمارشي',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'ليست انتخابى‌شده', 'مراحل', '&مجموع"', 'مشخصات ليست', 'نوع &ليست:',
  // List types: bullet, image
  'نشانه‌گذار گرد', 'تصوير', 'درج عدد|',
  // Number format, Number, Start level from, Font button
  'فرمت &عدد:', 'عدد', 'عدد شماره‌گذارى را شروع كن از:', '&قلم ...',
  // Image button, bullet character, Bullet button,
  '&تصوير ...', '&كاراكتر نشانه‌گذار', '&نشانه‌گذار ...',
  // Position of list text, bullet, number, image, text
  'موقعيت متن ليست', 'موقعيت نشانه‌گذار', 'موقعيت عدد', 'موقعيت تصوير', 'موقعيت متن',
  // at, left indent, first line indent, from left indent
  '&در:', 'تو&رفتگى چپ', '&تورفتگى اولين خط:', 'از تورفتگى چپ',
  // [only] one level preview, preview
  'پيش‌نمايش &يك مرحله‌اى', 'پيش‌نمايش',
  // Preview text
  'متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'تراز چپ', 'تراز راست', 'وسط',
  // level #, this level (for combo-box)
  'Level %d', 'اين مرحله',
  // Marker character dialog title
  'ويرايش كاراكتر نشانه‌گذار',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'حاشيه و پس‌زمينه پاراگراف', 'حاشيه', 'پس‌زمينه',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'تنظيمات', '&رنگ:', '&عرض:', 'عرض &داخلى:', '&بالانس ...',
  // Sample, Border type group-boxes;
  'نمونه', 'نوع حاشيه',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&هيچ كدام', '&تنها', '&دو برابر', '&سه برابر', 'ضخيم از &داخل', 'ضخيم از &خارج',
  // Fill color group-box; More colors, padding buttons
  'پر كردن رنگ', '&رنگ‌هاى بيشتر ...', '&حاشيه‌ها ...',
  // preview text
  'متن متن متن متن متن. متن متن متن متن متن . متن متن متن متن متن .',
  // title and group-box in Offsets dialog
  'بالانس', 'بالانس حاشيه',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'پاراگراف', 'تراز', '&چپ', '&راست', '&وسط', 'هم تراز شدن',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'فاصله‌گذارى', '&قبل:', '&بعد:', '&فاصله خطوط:', 'مقياس:',
  // Indents group-box; indents: left, right, first line
  'تورفتگى‌ها', 'چ&پ:', 'را&ست:', 'اولين &خط:',
  // indented, hanging
  '&تو رفته', '&معلق شده', 'نمونه',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'تنها', '1.5 خط', 'دو برابر', 'حداقل', 'دقيق', 'چندگانه',
  // preview text
  'متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. متن متن متن متن متن. ',
  // tabs: Indents and spacing, Tabs, Text flow
  'تورفتگى و فاصله‌گذارى', 'تب‌ها', 'جهت متن',
  // tab stop position label; buttons: set, delete, delete all
  'محل &تب:', '&تنظيم', '&حذف', 'حذف &همه',
  // tab align group; left, right, center aligns,
  'چينش', '&چپ', '&راست', '&وسط',
  // leader radio-group; no leader
  'سردسته', '(هيچ)',
  // tab stops to be deleted, delete none, delete all labels
  'تب‌هايى كه بايد حذف شوند:', '(هيچ)', 'همه.',
  // Pagination group-box, keep with next, keep lines together
  'صفحه‌گذارى', 'همراه &با بعدى قرار بده', '&خطوط را كنار هم قرار بده',
  // Outline level, Body text, Level #
  'سطح نماي كلي:', 'متن بدنه سند', 'سطح %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'پيش‌نمايش چاپ', 'عرض صفحه', 'تمام صفحه', 'صفحات:',
  // Invalid Scale, [page #] of #
  'لطفا عددى بين 10 تا 500 وارد كنيد', 'از %d',
  // Hints on buttons: first, prior, next, last pages
  'اولين صفحه', 'صفحه قبلى', 'صفحه بعدى', 'آخرين صفحه',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'فاصله‌گذارى خانه', 'فاصله‌گذارى', 'بين خانه‌ها', 'از حاشيه جدول تا خانه',
  // vertical, horizontal (x2)
  'عمودي :', 'افقي :', 'عمودي :', 'افقي :',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'حاشيه‌ها', 'تنظيمات', '&رنگ:', '&رنگ روشن:', 'رنگ &سايه:',
  // Width; Border type group-box;
  '&عرص:', 'نوع حاشيه',
  // Border types: none, sunken, raised, flat
  '&هيچ كدام', '&فرو رفته', '&برجسته', '&مسطح',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'جدا كردن', 'جدا كردن به',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&تعداد مشخص از رديف‌ها و ستون‌ها', 'خانه‌هاى ا&صلى',
  // number of columns, rows, merge before
  'تعداد &ستون‌ها:', 'تعداد رديف‌ها:', '&ادغام قبل از جدا كردن',
  // to original cols, rows check-boxes
  'جدا كردن به &ستون‌هاى اصلى', 'جدا كردن به &رديف‌هاى اصلى',
  // Add Rows form -------------------------------------------------------------
  'اضافه كردن رديف‌ها', '&مجموع رديف‌ها:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'تنظيم صفحه', 'صفحه', 'سرصفحه و پاصفحه',
  // margins group-box, left, right, top, bottom
  'حواشى (ميليمتر)', 'حواشى (اينچ)', '&چپ:', '&بالا:', '&راست:', '&پايين:',
  // mirror margins check-box
  '&حاشيه بازتاب',
  // orientation group-box, portrait, landscape
  'جهت', '&طولى', '&عرضى',
  // paper group-box, paper size, default paper source
  'كاغذ', 'ا&ندازه:', '&منبع:',
  // header group-box, print on the first page, font button
  'سرصفحه', '&متن:', '&سرصفحه در صفحه اول', '&قلم ...',
  // header alignments: left, center, right
  '&چپ', '&وسط', '&راست',
  // the same for footer (different hotkeys)
  'پاورقى', '&متن:', '&پاصفحه در صفحه اول', 'ق&لم ...',
  'چ&پ', 'وس&ط', 'را&ست',
  // page numbers group-box, start from
  'شماره صفحه', '&شروع از:',
  // hint about codes
  'تركيب خاص كاراكتر:' + #13 + '&&p - شماره صفحه; &&P - مجموع صفحات; &&d - تاريخ فعلى; &&t - زمان فعلى.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'كدپيج فايل متني', 'انتخاب نوع رمز‌گذاري فايل:',
  // thai, japanese, chinese (simpl), korean
  'تايلندي', 'ژاپني', 'چيني ساده‌شده', 'كره‌اي',
  // chinese (trad), central european, cyrillic, west european
  'چيني سنتي', 'اروپايي مركزي و شرقي', 'سيريليك', 'اروپاي غربي',
  // greek, turkish, hebrew, arabic
  'يوناني', 'تركي', 'عبري', 'عربي',
  // baltic, vietnamese, utf-8, utf-16
  'بالتيك', 'ويتنامي', 'يونيكد (UTF-8)', 'يونيكد (UTF-16)',
  // Style form ----------------------------------------------------------------
  'حالت بصري', 'انتخاب حالت:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'تبديل به متن', 'انتخاب جدا كننده:',
  // line break, tab, ';', ','
  'شكستن خط', 'تب', 'سميكالن', 'كاما',
  // Table sort form -----------------------------------------------------------
  // error message
  'انجام مرتب‌سازي روي جدولي كه داراي رديف‌هاي ادغام شده باشد امكان‌ پذير نمي‌باشد.',
  // title, main options groupbox
  'مرتب‌سازي جدول', 'مرتب‌سازي',
  // sort by column, case sensitive
  'مرتب سازي بر اساس ستون:', 'حساسيت به حروف بزرگ و كوچك',
  // heading row, range or rows
  'مستثني كردن رديف سرتيتر', 'مرتب‌سازي رديف‌ها از %d تا %d',
  // order, ascending, descending
  'ترتيب', 'صعودي', 'نزولي',
  // data type, text, number
  'نوع', 'متن', 'عدد',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
   'درج شماره', 'خصوصيات', 'نام شمارنده:', 'نوع شماره گذاري:',
  // numbering groupbox, continue, start from
   'شماره گذاري', 'ادامه', 'شروع از:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
   'عنوان', 'متن عنوان:', 'فقط نمايش شرح عنوان',
  // position radiogroup
   'موقعيت', 'در بالاي شيء انتخابي', 'در پايين شيء انتخابي',
  // caption text
   'شرح عنوان:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
   'شماره گذاري', 'شکل شماره: ', 'جدول شماره: ',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
   'لبه هاي جانبي نمايان', 'لبه',
  // Left, Top, Right, Bottom checkboxes
   'لبه چپ', 'لبه بالا', 'لبه راست', 'لبه پايين',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'تغيير اندازه ستون جدول', 'تغيير اندازه رديف جدول',
  // Hints on indents: first line, left (together with first), left (without first), right
  'تورفتگي اولين خط پاراگراف انتخابي', 'تورفتگي سمت چپ تمام سطر‌هاي پاراگراف انتخابي', 'تورفتگي تمام سطر‌هاي پاراگراف انتخابي به غير از خط اول', 'تورفتگي سمت راست تمام سطر‌هاي پاراگراف انتخابي',
  // Hints on lists: up one level (left), down one level (right)
  'يك سطح بالاتر در ليست', 'يك سطح پايين‌تر در ليست',
  // Hints for margins: bottom, left, right and top
  'حاشيه پايين سند', 'حاشيه چپ سند', 'حاشيه راست سند', 'حاشيه بالاي سند',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'تراز تب از چپ', 'تراز تب از راست', 'تراز تب از وسط', 'تراز اعشاري تب',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'معمولي', 'تورفتگي معمولي', 'بي فاصله', 'عنوان %d', 'ليست پاراگراف',
  // Hyperlink, Title, Subtitle
  'فراپيوند', 'عنوان', 'عنوان فرعي',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'تاکيد', 'تاکيد دقيق', 'تاکيد قوي', 'سخت',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'نقل قول', 'نقل قول قوي', 'مرجع دقيق', 'مرجع قوي',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'متن بلوک', 'متغير HTML', 'کد HTML', 'مخفف HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'تعريف HTML', 'صفحه کليد HTML', 'نمونه HTML', 'ماشين تحرير HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'از پيش قالب بندي شده HTML', 'استشهاد HTML', 'سرصفحه', 'پاصفحه', 'شماره صفحه',
  // Caption
   'عنوان',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'مرجع يادداشت پاياني', 'مرجع پاورقي', 'متن يادداشت پاياني', 'متن پاورقي',
  // Sidenote Reference, Sidenote Text
   'ارجاع يادداشت جانبي', 'متن يادداشت جانبي',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'رنگ', 'رنگ زمينه', 'شفاف', 'پيش فرض', 'رنگ زيرخط',
  // default background color, default text color, [color] same as text
  'رنگ زمينه پيش فرض', 'رنگ متن پيش فرض', 'شبيه متن',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'تکي', 'ضخيم', 'دوبرابر', 'خط نقطه چين', 'خط نقطه چين ضخيم', 'خط تيره دار',
  // underline types: thick dashed, long dashed, thick long dashed,
  'خط تيره دار ضخيم', 'خط تيره دار بلند', 'خط تيره دار بلند ضخيم',
  // underline types: dash dotted, thick dash dotted,
  'خط تيره و نقطه چين', 'خط تيره و نقطه چين ضخيم',
  // underline types: dash dot dotted, thick dash dot dotted
  'خط تيره و نقطه چين ترکيبي', 'خط تيره و نقطه چين ترکيبي ضخيم',
  // sub/superscript: not, subsript, superscript
  'غير زير/بالا نويس', 'زيرنويس', 'بالانويس',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'جهت متن:', 'موروثي', 'چپ به راست', 'راست به چپ',
  // bold, not bold
  'ضخيم', 'غير ضخيم',
  // italic, not italic
  'ايتاليک', 'غير ايتاليک',
  // underlined, not underlined, default underline
  'زيرخط دار', 'غير زيرخط دار', 'زيرخط پيش فرض',
  // struck out, not struck out
  'بيرون زده', 'غير بيرون زده',
  // overlined, not overlined
  'داراي خط رو', 'بدون خط رو',
  // all capitals: yes, all capitals: no
  'همه حروف بزرگ', 'همه حروف بزرگ خاموش',
  // vertical shift: none, by x% up, by x% down
  'بدون انتقال عمودي', 'انتقال %d%% بالا', 'انتقال %d%% پايين',
  // characters width [: x%]
  'پهناي کاراکترها',
  // character spacing: none, expanded by x, condensed by x
  'فاصله معمولي بين کاراکترها', 'فاصله گذاري بسط يافته %s', 'فاصله گذاري فشرده شده %s',
  // charset
  'اسکريپت',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'قلم پيش فرض', 'پاراگراف پيش فرض', 'موروثي',
  // [hyperlink] highlight, default hyperlink attributes
  'هاي لايت', 'پيش فرض',
  // paragraph aligmnment: left, right, center, justify
  'تراز از چپ', 'تراز از راست', 'وسط چين', 'هم تراز',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'ارتفاع خط: %d%%', 'فاصله بين خطوط: %s', 'ارتفاع خط : حداقل %s',
  'ارتفاع خط : دقيقا %s',
  // no wrap, wrap
  'چينش خطوط غير فعال', 'چينش خطوط',
  // keep lines together: yes, no
  'خطوط را کنار هم قرار بده', 'خطوط را کنار هم قرار نده',
  // keep with next: yes, no
  'کنار پاراگراف بعدي قرار بده', 'کنار پاراگراف بعدي قرار نده',
  // read only: yes, no
  'فقط خواندني', 'قايل ويرايش',
  // body text, heading level x
  'سطح نماي کلي : متن بدنه سند', 'سطح نماي کلي : %d',
  // indents: first line, left, right
  'تورفتگي خط اول', 'تورفتگي چپ', 'تورفتگي راست',
  // space before, after
  'فاصله ماقبل', 'فاصله مابعد',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'تب ها', 'هيچ', 'تراز', 'چپ', 'راست', 'وسط',
  // tab leader (filling character)
  'کاراکتر پرکننده',
  // no, yes
  'خير', 'بلي',
  // [padding/spacing/side:] left, top, right, bottom
  'چپ', 'بالا', 'راست', 'پايين',
  // border: none, single, double, triple, thick inside, thick outside
  'هيچ', 'تکي', 'دوبرابر', 'سه برابر', 'ضخيم از داخل', 'ضخيم از خارج',
  // background, border, padding, spacing [between text and border]
  'پس زمينه', 'حاشيه', 'حاشيه گذاري', 'فاصله گذاري',
  // border: style, width, internal width, visible sides
  'سبک', 'پهنا', 'پهناي داخلي', 'لبه هاي نمايان',
  // style inspector -----------------------------------------------------------
  // title
  'بازبين سبک',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'سبک', '(هيچ)', 'پاراگراف', 'قلم', 'خواص',
  // border and background, hyperlink, standard [style]
  'حاشيه و پس زمينه', 'فراپيوند', '(استاندارد)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'سبک ها', 'سبک',
  // name, applicable to,
  'نام :', 'قابل اعمال روي :',
  // based on, based on (no &),
  'بر اساس :', 'بر اساس :',
  //  next style, next style (no &)
  'سبک براي پاراگراف بعدي :', 'سبک براي پاراگراف بعدي :',
  // quick access check-box, description and preview
  'دسترسي سريع', 'شرح و پيش نمايش :',
  // [applicable to:] paragraph and text, paragraph, text
  'پاراگراف و متن', 'پاراگراف', 'متن',
  // text and paragraph styles, default font
  'سبک هاي متن و پاراگراف', 'قلم پيش فرض',
  // links: edit, reset, buttons: add, delete
  'ويرايش', 'باز نشاندن', 'افزودن', 'حذف',
  // add standard style, add custom style, default style name
  'افزودن سبک استاندارد...', 'افزودن سبک سفارشي', 'سبک %d',
  // choose style
  'انتخاب سبک :',
  // import, export,
  'وارد کردن...', 'صادر کردن...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'سبک هاي RichView (*.rvst)|*.rvst', 'خطا در فراخواني فايل', 'اين فايل شامل هيچ سبکي نيست.',
  // Title, group-box, import styles
  'وارد کردن سبک ها از درون فايل', 'وارد کردن', 'وارد کردن سبک ها :',
  // existing styles radio-group: Title, override, auto-rename
  'سبک هاي موجود', 'پايمال کردن', 'افزودن با نام جديد',
  // Select, Unselect, Invert,
  'انتخاب', 'عدم انتخاب', 'معکوس',
  // select/unselect: all styles, new styles, existing styles
  'همه سبک ها', 'سبک هاي جديد', 'سبک هاي موجود',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(پاک کردن فرمت)', '(همه سبک ها)',
  // dialog title, prompt
  'سبک ها', 'انتخاب سبک براي اعمال :',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&مترادف', 'همه را &ناديده بگير', '&اضافه به فرهنگ لغت',
  // Progress messages ---------------------------------------------------------
  'در حال دانلود %s', 'آماده سازي براي چاپ...', 'در حال چاپ صفحه %d',
  'تبديل فرمت RTF...',  'تبديل به فرمت RTF...',
  'خواندن %s...', 'نوشتن %s...', 'متن',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
   'بدون يادداشت', 'پاورقي', 'يادداشت پاياني', 'يادداشت جانبي', 'جعبه متن'
  );


initialization
  RVA_RegisterLanguage(
    'Farsi', // english language name, do not translate
     'فارسي', // native language name
    ARABIC_CHARSET, @Messages);

end.
