�
 TFRMRVITEMPROP 0�W  TPF0�TfrmRVItemPropfrmRVItemPropLeft�Top� BorderStylebsDialogCaption
PropertiesClientHeight�ClientWidth�
KeyPreview	OldCreateOrder	PositionpoScreenCenter
OnActivateFormActivate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TButtonbtnOkLeft� Top�WidthdHeightAnchorsakRightakBottom CaptionOKDefault	ModalResultTabOrder  TButton	btnCancelLeftATop�WidthdHeightAnchorsakRightakBottom Cancel	CaptionCancelModalResultTabOrder  TPageControlpcLeftTopWidth�Height�
ActivePagetsBoxPositionTabOrder  	TTabSheettsImageCaption Image  TLabel
lblPreviewLeftTopWidth)HeightCaptionPreview:  TPanelPanel1LeftTopWidth�Height
BevelOuterbvNoneBorderStylebsSingleColorclBtnShadowTabOrder  TImageimgLeft Top Width�HeightAlignalClientCenter	Stretch	Transparent	
OnDblClickbtnChangeImageClick   TButtonbtnChangeImageLeft/Top*WidthdHeightCaption
C&hange...TabOrderOnClickbtnChangeImageClick  	TGroupBoxgbTranspLeftTopWidth!HeightQCaptionTransparencyTabOrder TLabel
lblTrColorLeftxTopWidthVHeightCaptionTransparent &color:FocusControlcmbColor  TRVColorCombocmbColorLeftxTop'Width� HeightIndeterminate	OnColorChangecmbColorColorChangeTabOrderCaptioncmbColorParentColor  	TCheckBoxcbTranspLeftTop%WidthaHeightCaption&TransparentTabOrder    TButtonbtnSaveImageLeft.TopJWidthdHeightCaption&Save...TabOrderOnClickbtnSaveImageClick   	TTabSheettsImgLayoutCaption Size and Position
ImageIndex 	TGroupBoxgbVAlignLeftTopWidth�Height\Caption Vertical alignment TabOrder  TLabel
lblAlignToLeftTopWidthHeightCaption&Align:FocusControl
cmbAlignTo  TLabel	lblVShiftLeftTop<Width&HeightCaption
&Shift by:FocusControlseVShift  	TComboBox
cmbAlignToLeft� TopWidth� HeightStylecsDropDownList
ItemHeightTabOrder OnChangecmbAlignToChangeItems.Stringsbottom to base line of textmiddle to base line of texttop to line topbottom to line bottommiddle to line middle	left side
right side   TRVSpinEditseVShiftLeft� Top7Width\Height	Increment       ��?MaxValue       �@MinValue       ��Indeterminate	TabOrder  	TComboBoxcmbVShiftTypeLeft� Top8Width\HeightStylecsDropDownList
ItemHeightTabOrderOnClickcmbVShiftTypeClickItems.StringsPercentPixels    	TGroupBoxgbSizeLeftTopgWidth�Height� Caption	 Stretch TabOrder TLabellblWidthLeftTopWidthHeightCaption&WidthFocusControlseWidth  TLabel	lblHeightLeftTop=WidthHeightCaption&HeightFocusControlseHeight  TLabel
lblDefSizeLeft9TopxWidth/HeightCaption
lblDefSize  TLabellblDefSize2Left9Top� Width5HeightBiDiModebdLeftToRightCaptionlblDefSize2ParentBiDiModeVisible  TLabelLabel13LeftYTopWidthHeightCaption%FocusControl
cmbAlignTo  TLabelLabel14LeftYTop=WidthHeightCaption%FocusControlseVShift  TRVSpinEditseWidthLeft� TopWidth\Height	Increment       ��?MaxValue      @�@MinValue       ��?Value       ��?Indeterminate	TabOrder OnChangeseWidthChange  TRVSpinEditseHeightLeft� Top8Width\Height	Increment       ��?MaxValue      @�@MinValue       ��?Value       ��?Indeterminate	TabOrderOnChangeseHeightChange  TRVSpinEdit
seWidthPrcLeft� TopWidth\Height	Increment       ��?MaxValue      @�@MinValue       ��?Value       ��?Indeterminate	TabOrderOnChangeseWidthPrcChange  TRVSpinEditseHeightPrcLeft� Top8Width\Height	Increment       ��?MaxValue      @�@MinValue       ��?Value       ��?Indeterminate	TabOrderOnChangeseHeightPrcChange  	TComboBoxcmbImgSizeUnitsLeft� TopXWidth\HeightStylecsDropDownList
ItemHeightTabOrderOnClickcmbImgSizeUnitsClick  	TCheckBoxcbImgScaleProportionallyLeftTop� Width� HeightCaptionScale &proportionallyTabOrderOnClickcbImgScaleProportionallyClick  TButtonbtnImgSizeResetLeft� Top� WidthdHeightCaptionResetTabOrderOnClickbtnImgSizeResetClick    	TTabSheettsImgAppearanceCaption Appearance
ImageIndex	 	TGroupBoxgbImgInsideBorderLeftTopWidth�HeightkCaptionInside the borderTabOrder  TLabellblImgColorLeftTopWidth)HeightCaption&Fill color:FocusControlcmbImgFillColor  TLabellblImgSpacingLeftTopEWidth*HeightCaption	&Padding:FocusControlseImgSpacing  TLabellblImgSpacingMULeftJTopEWidthHeightCaptionpixels  TRVColorCombocmbImgFillColorLeft� TopWidth� HeightIndeterminate	TabOrder CaptioncmbColorParentColor  TRVSpinEditseImgSpacingLeft� Top@Width\Height	Increment       ��?MaxValue       �@Indeterminate	TabOrder   	TGroupBoxgbImgBorderLeftTopvWidth�HeightkCaptionBorderTabOrder TLabellblImgBorderColorLeftTopWidth<HeightCaptionBorder &color:FocusControlcmbImgBorderColor  TLabellblImgBorderWidthLeftTopEWidth>HeightCaptionBorder &width:FocusControlcmbImgBorderWidth  TRVColorCombocmbImgBorderColorLeft� TopWidth� Height	AutoColorclWindowTextIndeterminate	OnColorChangecmbImgBorderColorColorChangeTabOrder CaptioncmbColorParentColor  	TComboBoxcmbImgBorderWidthLeft� Top@Width� HeightStylecsOwnerDrawFixedDropDownCount
ItemHeightTabOrder
OnDrawItemcmbImgBorderWidthDrawItemItems.Strings123456    	TGroupBoxgbImgOutsideBorderLeftTop� Width�HeightcCaptionOutside the borderTabOrder TLabellblImgOuterVSpacingLeftTop=WidthNHeightCaption&Vertical spacing:FocusControlseImgOuterVSpacing  TLabellblImgOuterVSpacingMULeftJTop=WidthHeightCaptionpixels  TLabellblImgOuterHSpacingLeftTopWidthZHeightCaption&Horizontal spacing:FocusControlseImgOuterHSpacing  TLabellblImgOuterHSpacingMULeftJTopWidthHeightCaptionpixels  TRVSpinEditseImgOuterVSpacingLeft� Top8Width\Height	Increment       ��?MaxValue       �@Indeterminate	TabOrder  TRVSpinEditseImgOuterHSpacingLeft� TopWidth\Height	Increment       ��?MaxValue       �@Indeterminate	TabOrder     	TTabSheet	tsImgMiscCaptionMiscellaneous
ImageIndex
 	TGroupBoxgbWebLeftTopWidth�HeightSCaption Web TabOrder  TLabellblAltLeftTopWidthAHeightCaptionAlternate &text:FocusControltxtAlt  TEdittxtAltLeftTop*WidthmHeightTabOrder    	TGroupBox	gbImgMiscLeftTop^Width�HeightSCaptionMiscellaneousTabOrder TLabel
lblImgHintLeftTopWidth#HeightCaption	T&ooltip:FocusControl
txtImgHint  TEdit
txtImgHintLeftTop*WidthmHeightTabOrder     	TTabSheettsBreakCaption Line 
ImageIndex 	TGroupBoxgbBreakLeftTopWidth�Height� Caption Horizontal line TabOrder  TLabellblBreakColorLeftATop!WidthHeightCaption&Color:FocusControlcmbBreakColor  TLabellblBreakWidthLeftATopFWidthHeightCaption&Width:FocusControlcmbWidth  TLabellblBreakStyleLeftATopmWidthHeightCaption&Style:FocusControlcmbBreakStyle  TRVColorCombocmbBreakColorLeft� TopWidth� Height	AutoColorclWindowTextIndeterminate	OnColorChangecmbBreakColorColorChangeTabOrder CaptioncmbColorParentColor  	TComboBoxcmbWidthLeft� TopAWidth� HeightStylecsOwnerDrawFixedDropDownCount
ItemHeightTabOrderOnClickcmbWidthClick
OnDrawItemcmbWidthDrawItemItems.Strings123456   	TComboBoxcmbBreakStyleLeft� TopeWidth� HeightStylecsOwnerDrawFixedDropDownCount
ItemHeightTabOrder
OnDrawItemcmbBreakStyleDrawItemItems.Stringslinerect3ddotdash     	TTabSheettsTableCaption Table 
ImageIndex 	TGroupBoxgbTableSizeLeftTopWidth�Height� Caption Table TabOrder  TLabellblCellHPaddingLeftTop7WidthhHeightCaption&Horizontal cell pading:FocusControlseCellHPadding  TLabellblCellVPaddingLeftTopUWidthbHeightCaption&Vertical cell padding:FocusControlseCellVPaddingLayouttlCenter  TLabellblTableWidthLeftTopWidthHeightCaption&Width:FocusControlseTableWidth  TLabellblCellHPaddingMULeft Top7WidthHeightCaptionpixels  TLabellblCellVPaddingMULeft TopUWidthHeightCaptionpixels  TLabellblTableDefWidthLeft� TopWidth<HeightCaptiondefault width  TRVSpinEditseTableWidthLeft� TopWidth\Height	Increment       ��?MaxValue      @�@MinValue       ��?Value       ��?Indeterminate	TabOrder OnChangeseTableWidthChange  	TComboBox	cmbTWTypeLeftTopWidthdHeightStylecsDropDownList
ItemHeightTabOrderOnClickcmbTWTypeClickItems.StringsDefaultPercentPixels   TRVSpinEditseCellHPaddingLeft� Top2Width\Height	Increment       ��?MaxValue       �@Indeterminate	TabOrder  TRVSpinEditseCellVPaddingLeft� TopPWidth\Height	Increment       ��?MaxValue       �@MinValue       ���Indeterminate	TabOrder  TButton
btnSpacingLeftTopoWidth� HeightCaptionCell &Spacing...TabOrderOnClickbtnSpacingClick   	TGroupBoxgbTablePrintLeftTop(Width�HeightFCaption
 Printing TabOrder TLabellblHeadingRowsLeftTopWidthmHeightCaptionCount of &heading rows:FocusControlseHRC  	TCheckBox	cbNoSplitLeftTop,Width^HeightCaption&Keep table on single pageTabOrder  TRVSpinEditseHRCLeft� TopWidth\Height	Increment       ��?MaxValue       �@Indeterminate	TabOrder    	TGroupBoxgbTableBackAndBorderLeftTop� Width�Height}CaptionBorder and backgroundTabOrder TLabellblTableColorLeftTopWidth*HeightCaption&Fill Color:FocusControlcmbTableColor  TRVColorCombocmbTableColorLeft� TopWidth� HeightIndeterminate	OnColorChangecmbBreakColorColorChangeTabOrder CaptioncmbColorParentColor  TButtonbtnTableBackLeftTopXWidth� HeightCaption	&Image...TabOrderOnClickbtnTableBackClick  TButtonbtnTableBorderLeftTop8Width� HeightCaption&Table Border...TabOrderOnClickbtnTableBorderClick  TButtonbtnCellBorderLeft� Top8Width� HeightCaption&Cell Borders...TabOrderOnClickbtnCellBordersClick  TButtonbtnTableVisibleBordersLeft� TopXWidth� HeightCaption&Visible Border Sides...TabOrderOnClickbtnTableVisibleBordersClick    	TTabSheettsRowsCaption Rows 
ImageIndex TRVOfficeRadioGrouprgRowVAlignLeftTopWidth�HeightWColumnsItems
ImageIndexCaption&Top 
ImageIndexCaption&Center 
ImageIndexCaption&Bottom  MarginImagesilTableTextAreaHeightSquare	Caption Vertical alignment Ctl3D	ParentCtl3DTabOrder   	TGroupBox
gbRowPrintLeftTop`Width�Height1Caption
 Printing TabOrder 	TCheckBoxcbRowKeepTogetherLeftTopWidth^HeightCaption&Keep content togetherStatecbGrayedTabOrder     	TTabSheettsCellsCaption Cells 
ImageIndex 	TGroupBoxgbCellsLeftTopWidth�HeightSCaption Settings  TabOrder  TLabellblCellBestHeightMULeftTop3WidthHeightCaptionPixels  TLabellblCellHeightLeftTop3WidthGHeightCaption&Height at least:FocusControlseCellBestHeight  TLabellblCellDefWidthLeft� TopWidth HeightCaptiondefault  TLabellblCellWidthLeftTopWidthJHeightCaption&Preferred width:FocusControlseCellBestWidth  TRVSpinEditseCellBestWidthLeft� TopWidth\Height	Increment       ��?MaxValue       �	@MinValue       ��?Value       ��?Indeterminate	TabOrder OnChangeseCellBestWidthChange  	TComboBoxcmbCellBWTypeLeftTopWidthdHeightStylecsDropDownList
ItemHeightTabOrderOnClickcmbCellBWTypeClickItems.StringsdefaultPercentPixels   TRVSpinEditseCellBestHeightLeft� Top.Width\Height	Increment       ��?MaxValue       �	@MinValue       ��?Value       ��?Indeterminate	TabOrder   TRVOfficeRadioGrouprgCellVAlignLeftTopYWidth� Height� ColumnsItems
ImageIndexCaption&Top 
ImageIndexCaption&Center 
ImageIndexCaption&Bottom 
ImageIndexCaption&Default  MarginImagesilTableTextAreaHeightSquare	Caption Vertical alignment Ctl3D	ParentCtl3DTabOrder  TRVOfficeRadioGrouprgCellRotationLeft� TopYWidth� Height� ColumnsItems
ImageIndexCaption&None 
ImageIndex	Caption&90 
ImageIndex
Caption&180 
ImageIndexCaption&270  MarginImagesilTableTextAreaHeightSquare	CaptionRotationCtl3D	ParentCtl3DTabOrder  	TGroupBoxgbCellBorderLeftTop� Width�Height� Caption Border and backgroundTabOrder TLabellblCellColorLeftTopWidth)HeightCaption&Fill color:FocusControlcmbCellColor  TLabellblCellBorderColorLeft� Top%WidthDHeightCaption&Shadow color:FocusControlcmbCellBorderColor  TLabellblCellBorderLightColorLeft� TopRWidth4HeightCaption&Light color:FocusControlcmbCellBorderLightColor  TLabellblCellBorderLeft� TopWidth"HeightCaptionBorder:  TButtonbtnCellBackLeftTopCWidth� HeightCaption	&Image...TabOrderOnClickbtnCellBackClick  TRVColorCombocmbCellColorLeftTop$Width� HeightIndeterminate	OnColorChangecmbBreakColorColorChangeTabOrder CaptioncmbColorParentColor  TRVColorCombocmbCellBorderColorLeft� Top5Width� Height	AutoColorclWindowTextIndeterminate	OnColorChangecmbCellBorderColorColorChangeTabOrderCaptioncmbColorParentColor  TRVColorCombocmbCellBorderLightColorLeft� TopbWidth� Height	AutoColor	clBtnFaceIndeterminate	OnColorChange"cmbCellBorderLightColorColorChangeTabOrderCaptioncmbColorParentColor  TButtonbtnCellVisibleBordersLeftTopcWidth� HeightCaption&Visible Border SidesTabOrderOnClickbtnCellVisibleBordersClick    	TTabSheettsBoxPositionCaptionPosition
ImageIndex 	TGroupBox	gbBoxHPosLeft� TopWidthHeightWCaptionHorizontal PositionTabOrder  TLabel
lblBoxHTxtLeftTopNWidth1HeightCaptionrelative to:  TLabel	lblBoxHMULeftpTop8WidthHeightCaption%  	TComboBoxcmbBoxHPositionLeftTopWidth� HeightStylecsDropDownList
ItemHeightTabOrder OnClickcmbBoxHPositionClickItems.StringsAlignAbsolute positionRelative position   TRVOfficeRadioButton
rbBoxHPageLeftTop`WidthnHeightPCaptionPageTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVSpinEdit
seBoxHOffsLeftTop3Width\Height	Increment       ��?MaxValue       �@TabOrderOnChangeseBoxHOffsChange  TRVOfficeRadioButtonrbBoxHLeftMarginLeftTop� WidthnHeightPCaptionLeft marginTabOrderImagesilBoxAnchor
ImageIndex Square	  TRVOfficeRadioButtonrbBoxHMainTextAreaLeft� Top`WidthnHeightPCaptionMain text areaTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxHRightMarginLeft� Top� WidthnHeightPCaptionRight marginTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxHCharacterLeftTopWidthnHeightPCaption	CharacterTabOrderImagesilBoxAnchor
ImageIndexSquare	  	TComboBoxcmbBoxHAlignLeftTop3Width� HeightStylecsDropDownList
ItemHeightTabOrderItems.Strings left side of box to left side ofcenter of box to center of"right side of box to right side of   	TCheckBoxcbBoxPosMirrorLeftxTop8Width� HeightCaptionMirrored marginsTabOrderOnClickcbBoxPosMirrorClick   TListBoxlstBoxPositionLeftTopWidthyHeighti
ItemHeightItems.StringsHorizontal PositionVertical PositionPosition in Text TabOrderOnClicklstBoxPositionClick  	TGroupBox	gbBoxVPosLeft� TopWidthHeightWCaptionVertical PositionTabOrderVisible TLabel
lblBoxVTxtLeftTopNWidth1HeightCaptionrelative to:  TLabel	lblBoxVMULeftpTop8WidthHeightCaption%  	TComboBoxcmbBoxVPositionLeftTopWidth� HeightStylecsDropDownList
ItemHeightTabOrder OnClickcmbBoxVPositionClickItems.StringsAlignAbsolute positionRelative position   TRVOfficeRadioButton
rbBoxVPageLeftTop`WidthnHeightPCaptionPageTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVSpinEdit
seBoxVOffsLeftTop3Width\Height	Increment       ��?MaxValue       �@TabOrderOnChangeseBoxVOffsChange  TRVOfficeRadioButtonrbBoxVTopMarginLeftTop� WidthnHeightPCaption
Top marginTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxVMainTextAreaLeft� Top`WidthnHeightPCaptionMain text areaTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxVBottomMarginLeft� Top� WidthnHeightPCaptionBottom marginTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButton
rbBoxVLineLeftTopWidthnHeightPCaptionLineTabOrderImagesilBoxAnchor
ImageIndexSquare	  	TComboBoxcmbBoxVAlignLeftTop3Width� HeightStylecsDropDownList
ItemHeightTabOrderItems.Stringstop side of box to top side ofcenter of box to center of$bottom side of box to bottom side of   TRVOfficeRadioButton
rbBoxVParaLeft� TopWidthnHeightPCaption	ParagraphTabOrderImagesilBoxAnchor
ImageIndexSquare	   TRVOfficeRadioGroup	rgBoxZPosLeft� TopWidthHeight� Items
ImageIndex Caption
Above text 
ImageIndexCaption
Below text  Images	ilBoxZPosSquare	CaptionPosition in textTabOrderVisible  	TCheckBoxcbRelativeToCellLeft� Top`WidthHeightCaptionLayout in table cellTabOrder   	TTabSheet	tsBoxSizeCaptionSize
ImageIndex 	TGroupBoxgbBoxHeightLeftTopWidth�Height� CaptionHeightTabOrder  TLabellblBoxHeightTxtLeftTop8Width1HeightCaptionrelative to:  TLabellblBoxHeightLeftTop"Width"HeightCaption&Height:FocusControlcmbBoxHeight  TLabellblBoxAutoHeightLefttTop"WidthQHeightCaptionlblBoxAutoHeightVisible  TRVOfficeRadioButtonrbBoxHeightPageLeftTopPWidthZHeightPCaptionPageTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxHeightTopMarginLeft� TopPWidthZHeightPCaption
Top marginTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxHeightMainTextAreaLeftfTopPWidthZHeightPCaptionMain text areaChecked	TabOrderTabStop	ImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxHeightBottomMarginLeft'TopPWidthZHeightPCaptionBottom marginTabOrderImagesilBoxAnchor
ImageIndexSquare	  	TComboBoxcmbBoxHeightLeft� TopWidthxHeightStylecsDropDownList
ItemHeightTabOrderOnClickcmbBoxHeightClickItems.Stringspxpercentauto   TRVSpinEditseBoxHeightLefttTopWidth\Height	Increment       ��?MaxValue       �@TabOrder OnChangeseBoxHeightChange   	TGroupBox
gbBoxWidthLeftTop� Width�Height� CaptionWidthTabOrder TLabellblBoxWidthTxtLeftTop8Width1HeightCaptionrelative to:  TLabellblBoxWidthLeftTop"WidthHeightCaption&Width:FocusControlcmbBoxWidth  TRVOfficeRadioButtonrbBoxWidthPageLeftTopPWidthZHeightPCaptionPageTabOrderImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxWidthLeftMarginLeft� TopPWidthZHeightPCaptionLeft marginTabOrderImagesilBoxAnchor
ImageIndex Square	  TRVOfficeRadioButtonrbBoxWidthMainTextAreaLeftfTopPWidthZHeightPCaptionMain text areaChecked	TabOrderTabStop	ImagesilBoxAnchor
ImageIndexSquare	  TRVOfficeRadioButtonrbBoxWidthRightMarginLeft'TopPWidthZHeightPCaptionRight marginTabOrderImagesilBoxAnchor
ImageIndexSquare	  	TComboBoxcmbBoxWidthLeft� TopWidthxHeightStylecsDropDownList
ItemHeightTabOrderOnClickcmbBoxWidthClickItems.Stringspxpercent   TRVSpinEdit
seBoxWidthLefttTopWidth\Height	Increment       ��?MaxValue       �@TabOrder OnChangeseBoxWidthChange  	TCheckBoxcbBoxWidthMirrorLeftTop� Width� HeightCaptionMirrored marginsTabOrderOnClickcbBoxWidthMirrorClick    	TTabSheettsBoxContentCaption
Appearance
ImageIndex TRVOfficeRadioGrouprgBoxVAlignLeftTopWidth�HeightWColumnsItems
ImageIndexCaption&Top 
ImageIndexCaption&Center 
ImageIndexCaption&Bottom  MarginImagesilTableTextAreaHeightSquare	Caption Vertical alignment Ctl3D	ParentCtl3DTabOrder   TButtonbtnBoxBorderLeftTophWidth� HeightCaptionBorder and Background...TabOrderOnClickbtnBoxBorderClick   	TTabSheettsSeqCaptionNumber
ImageIndex 	TGroupBoxgbSeqLeftTopWidth�Height� Caption
PropertiesTabOrder  TLabel
lblSeqNameLeft`TopWidthEHeightCaptionCounter name:FocusControl
cmbSeqName  TLabel
lblSeqTypeLeft`TopFWidthMHeightCaption&Numbering type:FocusControl
cmbSeqType  	TComboBox
cmbSeqNameLeft`Top(Width� HeightDropDownCount
ItemHeight TabOrder OnChangecmbSeqNameChangeOnClickcmbSeqNameClick  	TComboBox
cmbSeqTypeLeft`TopXWidth� HeightStylecsDropDownListDropDownCount
ItemHeightTabOrderItems.Strings1, 2, 3, ...a, b, c, ...A, B, C, ...i, ii, iii, ...I, II, III, ...    	TGroupBoxgbSeqNumberingLeftTop� Width�HeightnCaption	NumberingTabOrder TRadioButtonrbSeqContinueLeft`TopWidthqHeightCaption	&ContinueChecked	TabOrder TabStop	  TRadioButton
rbSeqResetLeft`Top0WidthqHeightCaption&Start from:TabOrder  TRVSpinEditseSeqStartFromLeft{TopGWidth9Height	Increment       ��?MaxValue      @�@MinValue      @��Value       ��?TabOrderOnChangeseSeqStartFromChange     
TImageListilTableHeightWidthLeft� Top�  
TImageListilBoxAnchorHeight$Width!LeftZTop�  
TImageList	ilBoxZPosHeight9Width1Left(Top�  TSavePictureDialogspdOptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing Left� Top�   