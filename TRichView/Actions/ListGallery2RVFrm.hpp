﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ListGallery2RVFrm.pas' rev: 27.00 (Windows)

#ifndef Listgallery2rvfrmHPP
#define Listgallery2rvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Listgallery2rvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVListGallery2;
class PASCALIMPLEMENTATION TfrmRVListGallery2 : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Controls::TImageList* il1;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* tsBullets;
	Vcl::Comctrls::TTabSheet* tsNumbering;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rg1;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rg2;
	Vcl::Stdctrls::TLabel* lblLevel;
	Rvspinedit::TRVSpinEdit* seLevel;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TGroupBox* gbNumbering;
	Rvspinedit::TRVSpinEdit* seStartFrom;
	Vcl::Stdctrls::TRadioButton* rbContinue;
	Vcl::Stdctrls::TRadioButton* rbStartFrom;
	void __fastcall seStartFromChange(System::TObject* Sender);
	void __fastcall rg2DblClickItem(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	Rvedit::TCustomRichViewEdit* frve;
	int BeforeListNo;
	int AfterListNo;
	Rvstyle::TRVStyleLength FIndentStep;
	
public:
	Vcl::Controls::TControl* _rbStartFrom;
	Vcl::Controls::TControl* _pc;
	Vcl::Controls::TControl* _tsBullets;
	Vcl::Controls::TControl* _tsNumbering;
	void __fastcall Init(Rvedit::TCustomRichViewEdit* rve, Rvstyle::TRVStyleLength IndentStep);
	void __fastcall GetListStyle(Rvstyle::TRVListInfo* &ListStyle, int &ListNo, int &Level, int &StartFrom, bool &UseStartFrom);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVListGallery2(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVListGallery2(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVListGallery2(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVListGallery2(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Listgallery2rvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_LISTGALLERY2RVFRM)
using namespace Listgallery2rvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Listgallery2rvfrmHPP
