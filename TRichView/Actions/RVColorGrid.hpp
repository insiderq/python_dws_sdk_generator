﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVColorGrid.pas' rev: 27.00 (Windows)

#ifndef RvcolorgridHPP
#define RvcolorgridHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVGrids.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvcolorgrid
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVColorGrid;
class PASCALIMPLEMENTATION TRVColorGrid : public Rvgrids::TRVCustomGrid
{
	typedef Rvgrids::TRVCustomGrid inherited;
	
private:
	System::UnicodeString FFirstCaption;
	System::Uitypes::TColor FFirstColor;
	bool FFlat;
	bool FIndeterminate;
	int FFirstRowHeight;
	System::Classes::TNotifyEvent FOnColorChange;
	System::Classes::TComponent* FControlPanel;
	void __fastcall SetControlPanel(System::Classes::TComponent* Value);
	System::Uitypes::TColor __fastcall GetChosenColor(void);
	void __fastcall SetChosenColor(const System::Uitypes::TColor Value);
	void __fastcall SetFirstCaption(const System::UnicodeString Value);
	void __fastcall SetFirstColor(const System::Uitypes::TColor Value);
	void __fastcall SetFlat(const bool Value);
	void __fastcall SetIndeterminate(const bool Value);
	bool __fastcall GetFlat(void);
	MESSAGE void __fastcall WMColorChanged(Winapi::Messages::TMessage &Msg);
	
protected:
	virtual void __fastcall Initalize(void);
	DYNAMIC void __fastcall Resize(void);
	virtual void __fastcall DrawCell(int ACol, int ARow, const System::Types::TRect &ARect, bool ASelected);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	System::Classes::TComponent* __fastcall GetControlPanel(void);
	
public:
	__fastcall virtual TRVColorGrid(System::Classes::TComponent* AOwner);
	__property System::Uitypes::TColor ChosenColor = {read=GetChosenColor, write=SetChosenColor, nodefault};
	System::UnicodeString __fastcall GetColorName(void);
	virtual bool __fastcall SelectCell(int ACol, int ARow);
	virtual int __fastcall GetRowBegin(int ARow);
	
__published:
	__property System::UnicodeString FirstCaption = {read=FFirstCaption, write=SetFirstCaption};
	__property System::Uitypes::TColor FirstColor = {read=FFirstColor, write=SetFirstColor, default=536870911};
	__property bool Flat = {read=GetFlat, write=SetFlat, default=0};
	__property bool Indeterminate = {read=FIndeterminate, write=SetIndeterminate, nodefault};
	__property System::Classes::TNotifyEvent OnColorChange = {read=FOnColorChange, write=FOnColorChange};
	__property System::Classes::TComponent* ControlPanel = {read=FControlPanel, write=SetControlPanel};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property Constraints;
	__property Ctl3D;
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property Visible = {default=1};
	__property Color = {default=-16777211};
	__property OnClick;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnStartDock;
	__property OnStartDrag;
	__property BorderStyle = {default=1};
public:
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TRVColorGrid(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVColorGrid(HWND ParentWindow) : Rvgrids::TRVCustomGrid(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word WM_RVGRIDCOLORCHANGED = System::Word(0x410);
}	/* namespace Rvcolorgrid */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCOLORGRID)
using namespace Rvcolorgrid;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvcolorgridHPP
