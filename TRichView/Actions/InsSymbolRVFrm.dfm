�
 TFRMRVINSERTSYMBOL 0N  TPF0�TfrmRVInsertSymbolfrmRVInsertSymbolLeft� Top
HelpContext�a ActiveControldgCaptionInsert SymbolClientHeightcClientWidth�Constraints.MinHeight,Constraints.MinWidthvOldCreateOrder	PositionpoScreenCenterOnResize
FormResizePixelsPerInch`
TextHeight TLabel
lblCharsetLeftTop,WidthDHeightCaption&Character Set:FocusControl
cmbCharset  TLabellblFontLeftTopWidthHeightCaption&Font:FocusControlcmbFont  TLabelLabel1LeftTopHWidth HeightAnchorsakLeftakBottom CaptionLabel1  TLabellblBlockLeftbTop,WidthHeightAnchorsakTopakRight CaptionBlock:FocusControlcmbBlock  TButtonbtnOkLeft�TopBWidthdHeightAnchorsakRightakBottom CaptionInsertDefault	ModalResultTabOrder  TButton	btnCancelLeft TopBWidthdHeightAnchorsakRightakBottom Cancel	CaptionCancelModalResultTabOrder  TRVFontComboBoxcmbFontLeftxTop
Width� HeightStylecsDropDownListAnchorsakLeftakTopakRight DropDownCount
ItemHeightTabOrder OnClickcmbFontClick  TRVFontCharsetComboBox
cmbCharsetLeftxTop(Width� HeightStylecsDropDownListAnchorsakLeftakTopakRight DropDownCount
ItemHeightItems.Strings	(Default) TabOrderOnClickcmbCharsetClickAddDefaultCharset	DefaultCharsetCaptionUnicode  TRVGriddgLeftTopJWidth{Height� AnchorsakLeftakTopakRightakBottom TabOrderTabStop	
OnDblClick
dgDblClickOnEnterdgEnter
BoundsTypebbtByGridBoundsRowCountRowsVisibleColCountGridLineWidthDefaultColWidth DefaultRowHeight
OnDrawCell
dgDrawCellOnSelectCelldgSelectCellOnTopLeftChangeddgTopLeftChanged  	TComboBoxcmbBlockLeft�Top(Width� HeightStylecsDropDownListAnchorsakTopakRight DropDownCount
ItemHeightTabOrderOnClickcmbBlockClick   