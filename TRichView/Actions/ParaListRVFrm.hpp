﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ParaListRVFrm.pas' rev: 27.00 (Windows)

#ifndef ParalistrvfrmHPP
#define ParalistrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Paralistrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TBulletCharacter;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TBulletCharacter : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString Text;
	System::WideString TextW;
	bool Unicode;
	Vcl::Graphics::TFont* Font;
	__fastcall TBulletCharacter(void);
	__fastcall virtual ~TBulletCharacter(void);
};

#pragma pack(pop)

class DELPHICLASS TfrmRVParaList;
class PASCALIMPLEMENTATION TfrmRVParaList : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TGroupBox* gbLevels;
	Vcl::Stdctrls::TListBox* lstLevels;
	Rvspinedit::TRVSpinEdit* seLevels;
	Vcl::Stdctrls::TLabel* lblLevelCount;
	Vcl::Stdctrls::TGroupBox* gbListProperties;
	Vcl::Stdctrls::TComboBox* cmbNumbers;
	Vcl::Stdctrls::TLabel* lblNumber;
	Vcl::Extctrls::TNotebook* nb;
	Vcl::Stdctrls::TButton* btnFont;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgBullet;
	Vcl::Stdctrls::TButton* btnBullet;
	Vcl::Stdctrls::TButton* btnNumberFont;
	Vcl::Stdctrls::TLabel* lblNumFormat;
	Vcl::Stdctrls::TEdit* txtNumbers;
	Vcl::Stdctrls::TGroupBox* gbNumber;
	Vcl::Stdctrls::TComboBox* cmbLevel;
	Vcl::Extctrls::TPanel* Panel1;
	Vcl::Extctrls::TImage* img;
	Vcl::Stdctrls::TButton* btnImage;
	Vcl::Stdctrls::TGroupBox* gbListTextPos;
	Vcl::Stdctrls::TLabel* lblStartAt;
	Rvspinedit::TRVSpinEdit* seStartAt;
	Vcl::Stdctrls::TComboBox* cmbMarkerAlign;
	Vcl::Stdctrls::TLabel* lblAt;
	Rvspinedit::TRVSpinEdit* seMarkerIndent;
	Vcl::Stdctrls::TGroupBox* gbTextPos;
	Vcl::Stdctrls::TLabel* lblLI;
	Rvspinedit::TRVSpinEdit* seLeftIndent;
	Vcl::Stdctrls::TLabel* lblFI;
	Rvspinedit::TRVSpinEdit* seFirstIndent;
	Vcl::Stdctrls::TLabel* lblFLI;
	Vcl::Stdctrls::TGroupBox* gbPreview;
	Richview::TRichView* rv;
	Rvstyle::TRVStyle* rvs;
	Vcl::Buttons::TBitBtn* btnInsert;
	Vcl::Dialogs::TFontDialog* fd;
	Rvstyle::TRVStyle* rvs2;
	Vcl::Stdctrls::TCheckBox* cbOneLevelPreview;
	void __fastcall btnNumberFontClick(System::TObject* Sender);
	void __fastcall cmbLevelClick(System::TObject* Sender);
	void __fastcall btnInsertClick(System::TObject* Sender);
	void __fastcall seLevelsChange(System::TObject* Sender);
	void __fastcall lstLevelsClick(System::TObject* Sender);
	void __fastcall btnImageClick(System::TObject* Sender);
	void __fastcall cmbNumbersClick(System::TObject* Sender);
	void __fastcall rgBulletCustomDraw(Rvofficeradiobtn::TRVOfficeRadioGroup* Sender, int ItemIndex, Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &ARect, bool &DoDefault);
	void __fastcall btnFontClick(System::TObject* Sender);
	void __fastcall txtNumbersChange(System::TObject* Sender);
	void __fastcall rvJump(System::TObject* Sender, int id);
	void __fastcall btnBulletClick(System::TObject* Sender);
	void __fastcall rgBulletClick(System::TObject* Sender);
	void __fastcall seMarkerIndentChange(System::TObject* Sender);
	void __fastcall seLeftIndentChange(System::TObject* Sender);
	void __fastcall seFirstIndentChange(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cmbMarkerAlignClick(System::TObject* Sender);
	void __fastcall seStartAtChange(System::TObject* Sender);
	
private:
	System::UnicodeString FStoredBullet;
	System::UnicodeString FStoredNumber;
	System::UnicodeString FStoredBulletFontName;
	System::UnicodeString FStoredNumberFontName;
	System::Uitypes::TFontCharset FStoredBulletCharset;
	System::Uitypes::TFontCharset FStoredNumberCharset;
	int FStoredBulletIndex;
	bool FBulletUnicode;
	bool FUpdating;
	System::UnicodeString __fastcall FormatToEdit(const System::UnicodeString s, int LevelNo);
	System::UnicodeString __fastcall EditToFormat(const System::UnicodeString s, int LevelNo);
	void __fastcall MakeSample(void);
	
protected:
	Vcl::Controls::TControl* _btnInsert;
	Vcl::Controls::TControl* _gbListTextPos;
	Vcl::Controls::TControl* _gbListProperties;
	Vcl::Controls::TControl* _cmbNumbers;
	Vcl::Controls::TControl* _cmbLevel;
	Vcl::Controls::TControl* _cmbMarkerAlign;
	Vcl::Controls::TControl* _cbOneLevelPreview;
	Vcl::Controls::TControl* _lblStartAt;
	Vcl::Controls::TControl* _txtNumbers;
	Vcl::Controls::TControl* _lstLevels;
	bool IgnoreLevelsClick;
	
public:
	void __fastcall SetListStyle(Rvstyle::TRVListInfo* ListStyle, Rvstyle::TRVStyle* RVStyle);
	void __fastcall GetListStyle(Rvstyle::TRVListInfo* ListStyle);
	void __fastcall SetControls(int LevelNo);
	void __fastcall SetControls2(int LevelNo);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVParaList(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVParaList(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVParaList(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVParaList(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::StaticArray<TBulletCharacter*, 6> BulletChars;
}	/* namespace Paralistrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PARALISTRVFRM)
using namespace Paralistrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ParalistrvfrmHPP
