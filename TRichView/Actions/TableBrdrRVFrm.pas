{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for table and cell borders               }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit TableBrdrRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, BaseRVFrm,
  {$IFDEF USERVKSDEVTE}
  te_controls,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntStdCtrls,
  {$ENDIF}
  Dialogs, RVOfficeRadioBtn, ImgList, StdCtrls, RVColorCombo, RVTable,
  RVALocalize, RVStyle;

type
  TfrmRVTableBrdr = class(TfrmRVBase)
    gbTableBorder: TGroupBox;
    lblTableBorderColor: TLabel;
    lblTableBorderWidth: TLabel;
    lblTableBorderLightColor: TLabel;
    cmbBorderColor: TRVColorCombo;
    cmbBorderWidth: TComboBox;
    cmbBorderLightColor: TRVColorCombo;
    ImageList1: TImageList;
    rgBorderType: TRVOfficeRadioGroup;
    btnOk: TButton;
    btnCancel: TButton;
    procedure cmbBorderWidthDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure cmbBorderColorColorChange(Sender: TObject);
    procedure rgBorderTypeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cmbBorderWidthChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Updating: Boolean;
  protected
    _cmbBorderWidth, _lblTableBorderColor, _lblTableBorderLightColor: TControl;
    FRVStyle: TRVStyle;
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
    {$IFDEF USERVKSDEVTE}
    procedure tecmbWidthDrawItem(Control: TWinControl; Canvas: TCanvas; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
  public
    { Public declarations }
    ColorDialog: TColorDialog;
    procedure SetData(BorderStyle: TRVTableBorderStyle;
      ColorL, ColorD: TColor; BorderWidth: TRVStyleLength; RVStyle: TRVStyle);
    procedure GetData(var BorderStyle: TRVTableBorderStyle;
      var ColorL, ColorD: TColor; var BorderWidth: TRVStyleLength);
    procedure Localize; override;
    function AllowInvertImageListImage(il: TCustomImageList;
      Index: Integer): Boolean; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation
uses RichViewActions;
{$R *.dfm}

procedure TfrmRVTableBrdr.FormCreate(Sender: TObject);
begin
  _cmbBorderWidth := cmbBorderWidth;
  _lblTableBorderColor := lblTableBorderColor;
  _lblTableBorderLightColor := lblTableBorderLightColor;
  inherited;
  RVA_FillWidthComboBox(TRVAComboBox(_cmbBorderWidth), Canvas, False, TRVAControlPanel(ControlPanel));  
  PrepareImageList(ImageList1);
end;

procedure TfrmRVTableBrdr.SetData(BorderStyle: TRVTableBorderStyle; ColorL,
  ColorD: TColor; BorderWidth: TRVStyleLength; RVStyle: TRVStyle);
begin
  FRVStyle := RVStyle;
  if BorderStyle in [rvtbRaised, rvtbLowered] then begin
    cmbBorderColor.ChosenColor := cmbBorderColor.AutoColor;
    cmbBorderLightColor.ChosenColor := cmbBorderLightColor.AutoColor;
    end
  else begin
    cmbBorderColor.ChosenColor := ColorD;
    cmbBorderLightColor.ChosenColor := ColorL;
  end;
  if BorderWidth<=0 then begin
    rgBorderType.ItemIndex := 0;
    end
  else begin
    SetXBoxItemIndex(_cmbBorderWidth,
      RVA_GetComboBorderItemIndex(BorderWidth, False, FRVStyle,
        GetXBoxItemCount(_cmbBorderWidth), TRVAControlPanel(ControlPanel)));
    case BorderStyle of
      rvtbLowered, rvtbLoweredColor:
        rgBorderType.ItemIndex := 1;
      rvtbRaised, rvtbRaisedColor:
        rgBorderType.ItemIndex := 2;
      rvtbColor:
        rgBorderType.ItemIndex := 3;
    end;
  end;
end;

procedure TfrmRVTableBrdr.GetData(var BorderStyle: TRVTableBorderStyle;
  var ColorL, ColorD: TColor; var BorderWidth: TRVStyleLength);
begin
  if rgBorderType.ItemIndex=0 then begin
    BorderWidth := 0
    end
  else begin
    if GetXBoxItemIndex(_cmbBorderWidth)>=0 then
      BorderWidth := RVA_GetComboBorderWidth2(GetXBoxItemIndex(_cmbBorderWidth),
        False, FRVStyle, TRVAControlPanel(ControlPanel));
    if BorderWidth=0 then
      BorderWidth := 1;
    case rgBorderType.ItemIndex of
      1:
        begin
          if (cmbBorderColor.ChosenColor=cmbBorderColor.AutoColor) and
             (cmbBorderLightColor.ChosenColor=cmbBorderLightColor.AutoColor) then
            BorderStyle := rvtbLowered
          else begin
            BorderStyle := rvtbLoweredColor;
            ColorL := cmbBorderLightColor.ChosenColor;
            ColorD := cmbBorderColor.ChosenColor;
          end;
        end;
      2:
        begin
          if (cmbBorderColor.ChosenColor=cmbBorderColor.AutoColor) and
             (cmbBorderLightColor.ChosenColor=cmbBorderLightColor.AutoColor) then
            BorderStyle := rvtbRaised
          else begin
            BorderStyle := rvtbRaisedColor;
            ColorL := cmbBorderLightColor.ChosenColor;
            ColorD := cmbBorderColor.ChosenColor;
          end;
        end;
      3:
        begin
          BorderStyle := rvtbColor;
          ColorD := cmbBorderColor.ChosenColor;
        end;
    end;
  end;
end;

function TfrmRVTableBrdr.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := rgBorderType.Left+rgBorderType.Width+rgBorderType.Left;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-rgBorderType.Top-rgBorderType.Height);
  Result := True;
end;

procedure TfrmRVTableBrdr.cmbBorderColorColorChange(Sender: TObject);
begin
  if not Visible then
    exit;
  _cmbBorderWidth.Invalidate;
  if rgBorderType.ItemIndex<1 then
    rgBorderType.ItemIndex := 1;
end;

procedure TfrmRVTableBrdr.rgBorderTypeClick(Sender: TObject);
begin
  if not Visible or Updating then
    exit;
  Updating := True;
  try
    if rgBorderType.ItemIndex=0 then
      SetXBoxItemIndex(_cmbBorderWidth, -1)
    else if GetXBoxItemIndex(_cmbBorderWidth)<0 then
      SetXBoxItemIndex(_cmbBorderWidth,
        RVA_GetComboBorderItemIndex(FRVStyle.PixelsToUnits(1), False, FRVStyle,
          GetXBoxItemCount(_cmbBorderWidth), TRVAControlPanel(ControlPanel)));
    if rgBorderType.ItemIndex=3 then begin
      SetControlCaption(_lblTableBorderColor, RVA_GetS(rvam_tb_Color, ControlPanel));
      _lblTableBorderLightColor.Visible := False;
      cmbBorderLightColor.Visible := False;
      end
    else begin
      SetControlCaption(_lblTableBorderColor, RVA_GetS(rvam_tb_ShadowColor, ControlPanel));
      _lblTableBorderLightColor.Visible := True;
      cmbBorderLightColor.Visible := True;
    end;
    _cmbBorderWidth.Invalidate;
  finally
    Updating := False;
  end;
end;

procedure TfrmRVTableBrdr.FormActivate(Sender: TObject);
begin
  inherited;
  cmbBorderColor.ColorDialog := ColorDialog;
  cmbBorderLightColor.ColorDialog := ColorDialog;
  rgBorderTypeClick(Sender);
end;


procedure TfrmRVTableBrdr.cmbBorderWidthChange(Sender: TObject);
begin
  if not Visible then
    exit;
  if not Updating and (rgBorderType.ItemIndex<1) then
    rgBorderType.ItemIndex := 1;
end;

procedure TfrmRVTableBrdr.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  Caption := RVA_GetS(rvam_tb_Title, ControlPanel);
  gbTableBorder.Caption := RVA_GetSHAsIs(rvam_tb_Settings, ControlPanel);
  lblTableBorderColor.Caption := RVA_GetSAsIs(rvam_tb_Color, ControlPanel);
  lblTableBorderLightColor.Caption := RVA_GetSAsIs(rvam_tb_LightColor, ControlPanel);
  lblTableBorderWidth.Caption := RVA_GetSAsIs(rvam_tb_Width, ControlPanel);
  rgBorderType.Caption := RVA_GetS(rvam_tb_BorderType, ControlPanel);
  rgBorderType.Items[0].Caption := RVA_GetS(rvam_tb_BTNone, ControlPanel);
  rgBorderType.Items[1].Caption := RVA_GetS(rvam_tb_BTSunken, ControlPanel);
  rgBorderType.Items[2].Caption := RVA_GetS(rvam_tb_BTRaised, ControlPanel);
  rgBorderType.Items[3].Caption := RVA_GetS(rvam_tb_BTFlat, ControlPanel);
end;

function TfrmRVTableBrdr.AllowInvertImageListImage(il: TCustomImageList;
  Index: Integer): Boolean;
begin
  Result := (Index<>1) and (Index<>2);
end;

procedure TfrmRVTableBrdr.cmbBorderWidthDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var c1, c2: TColor;
begin
  case rgBorderType.ItemIndex of
    0,3:
      begin
        c1 := cmbBorderColor.ChosenColor;
        c2 := c1;
      end;
    1:
      begin
        c1 := cmbBorderColor.ChosenColor;
        c2 := cmbBorderLightColor.ChosenColor;
      end;
    else
      begin
        c2 := cmbBorderColor.ChosenColor;
        c1 := cmbBorderLightColor.ChosenColor;
      end;
  end;
  {$IFDEF USERVTNT}
  RVA_DrawCmbWidthItem2(TTntComboBox(_cmbBorderWidth).Canvas, Rect, Index, False,
    c1, c2, State, ControlPanel as TRVAControlPanel);
  {$ELSE}
  RVA_DrawCmbWidthItem2(TComboBox(_cmbBorderWidth).Canvas, Rect, Index, False,
    c1, c2, State, ControlPanel as TRVAControlPanel);
  {$ENDIF}
end;

{$IFDEF USERVKSDEVTE}
procedure TfrmRVTableBrdr.tecmbWidthDrawItem(Control: TWinControl;
  Canvas: TCanvas; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var c1, c2: TColor;
begin
  case rgBorderType.ItemIndex of
    0,3:
      begin
        c1 := cmbBorderColor.ChosenColor;
        c2 := c1;
      end;
    1:
      begin
        c1 := cmbBorderColor.ChosenColor;
        c2 := cmbBorderLightColor.ChosenColor;
      end;
    else
      begin
        c2 := cmbBorderColor.ChosenColor;
        c1 := cmbBorderLightColor.ChosenColor;
      end;
  end;
  RVA_DrawCmbWidthItem2(Canvas, Rect, Index, False,
    c1, c2, State, ControlPanel as TRVAControlPanel);
end;
{$ENDIF}

{$IFDEF RVASKINNED}
procedure TfrmRVTableBrdr.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _cmbBorderWidth then begin
    _cmbBorderWidth := NewControl;
    {$IFDEF USERVKSDEVTE}
    TTeComboBox(_cmbBorderWidth).OnDrawItem := tecmbWidthDrawItem;
    {$ENDIF}
    end
  else if OldControl=_lblTableBorderColor then
    _lblTableBorderColor := NewControl
  else if OldControl=_lblTableBorderLightColor then
    _lblTableBorderLightColor := NewControl
end;
{$ENDIF}


end.
