unit ObjectAlignRVFrm;

interface

{$R rvaalign.res}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseRVFrm, ImgList, StdCtrls, RVStyle, RVOfficeRadioBtn, RVALocalize;

type
  TfrmRVObjectAlign = class(TfrmRVBase)
    rg: TRVOfficeRadioGroup;
    btnOk: TButton;
    btnCancel: TButton;
    ImageList1: TImageList;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Localize; override;
    procedure SetVAlign(VAlign: TRVVAlign);
    function GetVAlign: TRVVAlign;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation

{$R *.DFM}

{ TfrmRVObjectAlign }


procedure TfrmRVObjectAlign.Localize;
var i: Integer;
    s: TRVALocString;
begin
  inherited Localize;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  for i := 0 to rg.Items.Count-1 do
    rg.Items[i].Caption := RVA_GetS(TRVAMessageID(
      ord(rvam_ip_VAlign1)+rg.Items[i].ImageIndex), ControlPanel);
  s := RVA_GetSH(rvam_ip_VAlignValue, ControlPanel);
  if s[Length(s)-1]=':' then
    Delete(s, Length(s)-1, 1);
  rg.Caption := s;
  Caption := RVA_GetS(rvam_va_Title, ControlPanel);
end;

function TfrmRVObjectAlign.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := rg.Left*2+rg.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-rg.Top-rg.Height);
  Result := True;
end;

function TfrmRVObjectAlign.GetVAlign: TRVVAlign;
begin
  if rg.ItemIndex<0 then
    Result := rvvaBaseline
  else
    Result := TRVVAlign(rg.Items[rg.ItemIndex].ImageIndex);
end;

procedure TfrmRVObjectAlign.SetVAlign(VAlign: TRVVAlign);
var i: Integer;
begin
  for i := 0 to rg.Items.Count-1 do
    if rg.Items[i].ImageIndex = ord(VAlign) then begin
      rg.ItemIndex := i;
      exit;
    end;
end;

procedure TfrmRVObjectAlign.FormCreate(Sender: TObject);
var bmp: TBitmap;
begin
  inherited;
  bmp := TBitmap.Create;
  bmp.LoadFromResourceName(hInstance, 'RVA_ALIGN');
  ImageList1.AddMasked(bmp, bmp.Canvas.Pixels[0,bmp.Height-1]);
  bmp.Free;
  PrepareImageList(ImageList1);
end;

end.
