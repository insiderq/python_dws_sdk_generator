
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Thai translation                                }
{                                                       }
{                                                       }
{*******************************************************}

unit RVAL_Thai;

interface
{$I RV_Defs.inc}
implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  '���ǚ', 'ૹ������', '���������', '1/6 ����', '�ԡ��', '�ش�',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', '��.', '��.', '1/6''', '�.', '�ش',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&�͡���', '&���', '�ٻ&Ẻ�', 'Ẻ&�ѡ��', '���&˹��', '&�á�', '��&�ҧ', '˹��&��ҧ�', '����&�����',
  // exit
  '�͡&',
  // top-level menus: View, Tools,
  '���&�ͧ', '����ͧ&���',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '��Ҵ', '�ѡɳ�Ẻ�ѡ��', '���͡', '�Ѵ&������', '���&�ͺ�',
  // menus: Table cell rotation
  '�����ع&����',
  // menus: Text flow, Footnotes/endnotes
  '&��ȷҧ�ͧ��ͤ���', '&�ԧ��ö',
  // ribbon tabs: tab1, tab2, view, table
  '&˹���á', '&����٧', '&����ͧ', '&���ҧ',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '��Ի���촟', 'Ẻ�ѡ�ß', '���˹��', '��¡��', '���',
  // ribbon groups: insert, background, page setup,
  '�á�', '�����ѧ', '��駤��˹�ҡ�д��',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  '���������§', '�ԧ��ö', '˹�ҵ�ҧ', '�ʴ�/��͹�', '����', '������͡',
  // ribbon groups on table tab: insert, delete, operations, borders
  '�á�', 'ź�', '��ô��Թ���', '�ͺ',
  // ribbon groups: styles 
  '�ٻẺ',
  // ribbon screen tip footer,
  '�� F1 ���ʹ���������´��������',
  // ribbon app menu: recent files, save-as menu title, print menu title
  '�͡��ù��', '�ѹ�֡��ФѴ�͡�͡���', '������ҧ��͹�������о�����͡���',
  // ribbon label: units combo
  '˹��:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&���ҧ', '���ҧ|���ҧ�͡�������',
  // TrvActionOpen
  '&�Դ�...', '�Դ�|�Դ�͡���',
  // TrvActionSave
  '&�ѹ�֡�', '�ѹ�֡�|�ѹ�֡�͡��Â',
  // TrvActionSaveAs
  '�ѹ�֡&�繂...', '�ѹ�֡�繂...|�ѹ�֡�͡���㹪�������',
  // TrvActionExport
  '&���͡�...', '���͡�|���͡�͡������ٻẺ��蹂',
  // TrvActionPrintPreview
  '������ҧ&��͹�����', '������ҧ��͹�����|�ʴ��͡�������͹���о�����͡��',
  // TrvActionPrint
  '&�����...', '�����|��駤�ҡ�þ������о�����͡��Â',
  // TrvActionQuickPrint
  '&�����', '&������ǹ', '�����|������͡���',
  // TrvActionPageSetup
  '��駤��&˹�ҡ�д��...', '��駤��˹�ҡ�д��|�ͺ��д�ɏ, ��Ҵ��д��, ����ҧ��, ��д��, �����з��¡�д�ɂ',
  // TrvActionCut
  '�Ѵ&�', '�Ѵ�|�Ѵ��ǹ������͡��й�������Ի���촂',
  // TrvActionCopy
  '&�Ѵ�͡', '�Ѵ�͡|�Ѵ��ǹ������͡��й�������Ի���촂',
  // TrvActionPaste
  '&�ҧ�', '�ҧ�|�ҧ�����Ңͧ��Ի����',
  // TrvActionPasteAsText
  '�ҧ&�繢�ͤ����', '�ҧ�繢�ͤ����|�ҧ��ͤ����ͧ��Ի���촂',  
  // TrvActionPasteSpecial
  '�ҧ&Ẻ�����...', '�ҧẺ����ɂ|�ҧ�����Ңͧ��Ի������ٻẺ����ɂ',
  // TrvActionSelectAll
  '���͡&�������', '���͡������|���͡����͡��Â',
  // TrvActionUndo
  '&��ԡ�ӂ', '��ԡ�ӂ|��Ѻ��ѧ��á�з�����ش�',
  // TrvActionRedo
  '&�ӫ��', '�ӫ�ӂ|��͹��Ѻ�����ԡ��',
  // TrvActionFind
  '&���҂...', '���Ң�ͤ�����Ш���͡��Â',
  // TrvActionFindNext
  '����&�Ѵ仂', '���ҶѴ�|���ҵ��仨ҡ����ش',
  // TrvActionReplace
  '&᷹���...', '᷹���|�������᷹����ͤ�����Ш���͡��Â',
  // TrvActionInsertFile
  '&���...', '�á���|�������ͤ����ҡ���ŧ��͡��Â���',
  // TrvActionInsertPicture
  '&�ٻ�Ҿ...', '�á�ٻ�Ҿ�|�á����ٻ�Ҿ�',
  // TRVActionInsertHLine
  '&����ǹ͹', '�á����ǹ͹�|�á����ǹ͹�',
  // TRVActionInsertHyperlink
  '&���������§�����Ե�...', '�á���������§�����Ե�|�á���������§�����Ե�',
  // TRVActionRemoveHyperlinks
  '&���������§�����Ե�', 'ź���������§�����Ե�|ź���������§�����ԵԷ�����͡�',  
  // TrvActionInsertSymbol
  '&�ѭ�ѡɳ�...', '�á�ѭ�ѡɳ�|�á�ѭ�ѡɳ�',
  // TrvActionInsertNumber
  {*} '&Number...', 'Insert Number|Inserts a number',
  // TrvActionInsertCaption
  {*} 'Insert &Caption...', 'Insert Caption|Inserts a caption for the selected object',
  // TrvActionInsertTextBox
  {*} '&Text Box', 'Insert Text Box|Inserts a text box',
  // TrvActionInsertSidenote
  {*} '&Sidenote', 'Insert Sidenote|Inserts a note displayed in a text box',
  // TrvActionInsertPageNumber
  {*} '&Page Number', 'Insert Page Number|Inserts a page number',
  // TrvActionParaList
  '&�ѭ�ѡɳ��ʴ���Ǣ������&����ӴѺ�Ţ...', '�ѭ�ѡɳ��ʴ���Ǣ����������ӴѺ�Ţ�|�����������Ǣ�����������ӴѺ�Ţ����Ѻ���˹�ҷ�����͡�',
  // TrvActionParaBullets
  '&�ѭ�ѡɳ��ʴ���Ǣ������', '�ѭ�ѡɳ��ʴ���Ǣ������|�������͂ź�ѭ�ѡɳ��ʴ���Ǣ����������Ѻ���˹��',
  // TrvActionParaNumbering
  '&�ӴѺ�Ţ', '�ӴѺ�Ţ|�������͂ź�ӴѺ�Ţ����Ѻ���˹�҂',
  // TrvActionColor
  '��&�����ѧ...', '�����ѧ|����¹�վ����ѧ�ͧ�͡��Â',
  // TrvActionFillColor
  '&�����...', '�����|����¹�վ����ѧ�ͧ��ͤ��� ���˹�� ����� ���ҧ �����͡��Â',
  // TrvActionInsertPageBreak
  '&�á�����˹�҂', '�á�����˹�҂|�á�����˹��',
  // TrvActionRemovePageBreak
  '&ź�����˹�҂', 'ź�����˹�҂|ź�����˹�҂',
  // TrvActionClearLeft
  '�Ѵ�Ǣ�ͤ���&�Դ���', '�Ѵ�Ǣ�ͤ����Դ���|�ҧ���˹�ҹ������ҹ��ҧ�ͧ�Ҿ���Դ���',
  // TrvActionClearRight
  '�Ѵ�Ǣ�ͤ���&�Դ��҂�', '�Ѵ�Ǣ�ͤ����Դ���|�ҧ���˹�ҹ������ҹ��ҧ�ͧ�Ҿ���Դ���',
  // TrvActionClearBoth
  '�Ѵ&�����', '�Ѵ����ǂ|�ҧ���˹�ҹ������ҹ��ҧ�ͧ�Ҿ���Դ��駫�����Т�҂',
  // TrvActionClearNone
  '&�Ѵ�ǻ���', '�Ѵ�ǻ���|����ͤ��������ͺ� �Ҿ�',
  // TrvActionVAlign
  '&���˹觢ͧ�ѵ��...', '���˹觢ͧ�ѵ��|����¹���˹觢ͧ�ѵ�ط�����͡�',  
  // TrvActionItemProperties
  '�س���ѵ�&�ͧ�ѵ��...', '�س���ѵԢͧ�ѵ��|��˹��س���ѵԢͧ�ѵ����ҹ����',
  // TrvActionBackground
  '&�����ѧ...', '�����ѧ|���͡�Ҿ����վ����ѧ�',
  // TrvActionParagraph
  '&���˹��...', '���˹��|����¹�ٻẺ���˹�҂',
  // TrvActionIndentInc
  '&�����������ͧ', '�����������ͧ|�����дѺ�������ͧ�ͧ���˹�҂',
  // TrvActionIndentDec
  '&Ŵ�������ͧ�', 'Ŵ�������ͧ�|Ŵ�дѺ�������ͧ�ͧ���˹�҂',
  // TrvActionWordWrap
  '&��õѴ��', '��õѴ��|��Ѻ��õѴ������Ѻ���˹�ҷ�����͡�',
  // TrvActionAlignLeft
  '�Դ&����', '�Դ����|�Ѵ�Ǣ�ͤ����Դ���',
  // TrvActionAlignRight
  '�Դ&���', '�Դ���|�Ѵ�Ǣ�ͤ����Դ���',
  // TrvActionAlignCenter
  '���&��ҧ', '��觡�ҧ|�Ѵ�Ǣ�ͤ�����觡�ҧ�',
  // TrvActionAlignJustify
  '���&�Ǉ', '����Ǉ|�Ѵ�ǪԴ��駫�����Т�҂',
  // TrvActionParaColor
  '�վ����ѧ&�ͧ���˹��...', '�վ����ѧ�ͧ���˹��|��駤���վ����ѧ�ͧ���˹�҂',
  // TrvActionLineSpacing100
  '&������ҧ 1 ��÷Ѵ', '������ҧ 1 ��÷Ѵ|��駤��������ҧ 1 ��÷Ѵ�',
  // TrvActionLineSpacing150
  '������ҧ &1.5 ��÷Ѵ', '������ҧ 1.5 ��÷Ѵ|��駤��������ҧ 1.5 ��÷Ѵ�',
  // TrvActionLineSpacing200
  '&������ҧ 2 ��÷Ѵ', '������ҧ 2 ��÷Ѵ|��駤��������ҧ 2 ��÷Ѵ�',
  // TrvActionParaBorder
  '�ͺ��о����ѧ&�ͧ���˹�� ...', '�ͺ��о����ѧ�ͧ���˹��|��駤�Ңͺ��о����ѧ�ͧ���˹�҂',
  // TrvActionInsertTable
  '&�á���ҧ�...', '&���ҧ', '�á���ҧ�|�á���ҧ����',
  // TrvActionTableInsertRowsAbove
  '�á��&��ҹ��', '�á�Ǵ�ҹ���|�á�������ҹ�����������͡�',
  // TrvActionTableInsertRowsBelow
  '�á��&��ҹ��ҧ', '�á�Ǵ�ҹ��ҧ�|�á�������ҹ��ҧ���������͡�',
  // TrvActionTableInsertColLeft
  '�á�������&��ҹ���', '�á��������ҹ���|�á������������ҹ�������������͡�',
  // TrvActionTableInsertColRight
  '�á�������&��ҹ���', '�á��������ҹ��҂|�á������������ҹ������������͡�',
  // TrvActionTableDeleteRows
  'ź��', 'ź�ǂ|ź�Ǣͧ���������͡�',
  // TrvActionTableDeleteCols
  'ź&�������', 'ź�������|ź�������ͧ���������͡�',
  // TrvActionTableDeleteTable
  'ź&���ҧ', 'ź���ҧ�|ź���ҧ',
  // TrvActionTableMergeCells
  '��ҹ&����', '��ҹ����|��ҹ���������͡�',
  // TrvActionTableSplitCells
  '�¡&����...', '�¡����|�¡���������͡�',
  // TrvActionTableSelectTable
  '���͡&���ҧ�', '���͡���ҧ�|���͡���ҧ',
  // TrvActionTableSelectRows
  '���͡&�ǂ', '���͡��|���͡�ǂ',
  // TrvActionTableSelectCols
  '���͡&�������', '���͡�������|���͡�������',
  // TrvActionTableSelectCell
  '���͡&����', '���͡���삂|���͡���삂',
  // TrvActionTableCellVAlignTop
  '�Ѵ�Դ&��ҹ��', '�Ѵ�Դ��ҹ��|�Ѵ�����ҪԴ��ҹ���',
  // TrvActionTableCellVAlignMiddle
  '�Ѵ&��觡�ҧ�', '�Ѵ��觡�ҧ��|�Ѵ�����ҡ�觡�ҧ����',
  // TrvActionTableCellVAlignBottom
  '�Ѵ�Դ&��ҹ��ҧ', '�Ѵ�Դ��ҹ��ҧ|�Ѵ�����ҪԴ��ҹ��ҧ',
  // TrvActionTableCellVAlignDefault
  '&���������鹡�èѴ���ǵ��', '���������鹡�èѴ���ǵ��|����繤��������鹡�èѴ���ǵ�駂����Ѻ���������͡',
  // TrvActionTableCellRotationNone
  '&�������������ع�', '�������������ع|��ع����������� 0 ͧ�҂',
  // TrvActionTableCellRotation90
  '������ع &90 ͧ�҂', '������ع 90 ͧ��|��ع����������� 90 ͧ�҂',
  // TrvActionTableCellRotation180
  '������ع &180 ͧ�҂', '������ع 180 ͧ��|��ع����������� 180 ͧ�҂',
  // TrvActionTableCellRotation270
  '������ع &270 ͧ�҂', '������ع 270 ͧ��|��ع����������� 270 ͧ�҂',
  // TrvActionTableProperties
  '�س���ѵ�&���ҧ...', '�س���ѵԵ��ҧ|����¹�س���ѵԵ��ҧ������͡�',
  // TrvActionTableGrid
  '�ʴ�&��鹵��ҧ�', '�ʴ���鹵��ҧ�|�ʴ����ͫ�͹��鹵��ҧ�',
  // TRVActionTableSplit
  '�¡&���ҧ', '�¡���ҧ�|�¡���ͧ���ҧ�ҡ�Ƿ�����͡�',
  // TRVActionTableToText
  '�ŧ��&��ͤ���...', '�ŧ�繢�ͤ����|�ŧ�ҡ���ҧ�繢�ͤ����',
  // TRVActionTableSort
  '&���§�ӴѺ...', '���§�ӴѺ�|���§�ӴѺ�Ǣͧ���ҧ�',
  // TrvActionTableCellLeftBorder
  '&�ͺ����', '�ͺ����|�ʴ����ͫ�͹�ͺ��ҹ���',
  // TrvActionTableCellRightBorder
  '&�ͺ���', '�ͺ���|�ʴ����ͫ�͹�ͺ��ҹ��҂',
  // TrvActionTableCellTopBorder
  '&�ͺ��', '�ͺ��|�ʴ����ͫ�͹�ͺ��ҹ���',
  // TrvActionTableCellBottomBorder
  '&�ͺ��ҧ', '�ͺ��ҧ|�ʴ����ͫ�͹�ͺ��ҹ��ҧ�',
  // TrvActionTableCellAllBorders
  '&�ͺ�ء��ҹ', '�ͺ�ء��ҹ|�ʴ����ͫ�͹�ͺ�ء��ҹ�ͧ���삂',
  // TrvActionTableCellNoBorders
  '&����բͺ�', '����բͺ�|��͹�ͺ�ͧ���������',
  // TrvActionFonts & TrvActionFontEx
  '&Ẻ�ѡ�ß...', 'Ẻ�ѡ�ß|����¹Ẻ�ѡ�âͧ��ͤ���������͡',
  // TrvActionFontBold
  '&���˹�', '���˹�|�Ӣ�ͤ���������͡�繵��˹҂',
  // TrvActionFontItalic
  '&������§', '������§|�Ӣ�ͤ���������͡�繵�����§',
  // TrvActionFontUnderline
  '&�մ�����', '�մ�����|�մ������ͤ���������͡�',
  // TrvActionFontStrikeout
  '&�մ�Ѻ', '�մ�Ѻ�|�մ�Ѻ��ͤ���������͡��',
  // TrvActionFontGrow
  '&������Ҵ�ѡ�Â', '������Ҵ�ѡ�Â|������Ҵ�ѡ�÷�����͡��鹋 10%�',
  // TrvActionFontShrink
  '&Ŵ��Ҵ�ѡ�Â', 'Ŵ��Ҵ�ѡ�Â|Ŵ��Ҵ�ѡ�÷�����͡ŧ� 10%�',
  // TrvActionFontGrowOnePoint
  '&������Ҵ�ѡ�� 1 �ش�', '������Ҵ�ѡ�� 1 �ش�|������Ҵ�ѡ�÷�����͡��� 1 �ش�',
  // TrvActionFontShrinkOnePoint
  '&Ŵ��Ҵ�ѡ�� 1 �ش�', 'Ŵ��Ҵ�ѡ�� 1 �ش�|Ŵ��Ҵ�ѡ�÷�����͡ŧ 1 �ش�',
  // TrvActionFontAllCaps
  '&��Ǿ�����˭������', '��Ǿ�����˭������|����¹�ѡ�÷�����������͡�繵�Ǿ�����˭�',
  // TrvActionFontOverline
  '&�մ��鹺�', '�մ��鹺�|�մ����˹�͢�ͤ���������͡�',
  // TrvActionFontColor
  '�բͧ&��ͤ���...', '�բͧ��ͤ���|����¹�բͧ��ͤ���������͡',
  // TrvActionFontBackColor
  '�վ����ѧ&�ͧ��ͤ���...', '�վ����ѧ�ͧ��ͤ���|����¹�վ����ѧ�ͧ��ͤ���������͡�',
  // TrvActionAddictSpell3
  '��Ǩ�ͺ&����С�', '��Ǩ�ͺ����С��|��Ǩ�ͺ����С��ӂ',
  // TrvActionAddictThesaurus3
  '��ö�&�Ըҹ', '��ö��Ըҹ|�ʹͤ���蹷���դ������������§�Ѻ�ӷ�����͡���',
  // TrvActionParaLTR
  '����仢��', '����仢�҇|��駤�ҷ�ȷҧ�ѡ�èҡ����仢������Ѻ���˹�ҷ�����͡�',
  // TrvActionParaRTL
  '���仫���', '���仫��|��駤�ҷ�ȷҧ�ѡ�èҡ���仫�������Ѻ���˹�ҷ�����͡�',
  // TrvActionLTR
  '��ͤ����ҡ����仢��', '��ͤ����ҡ����仢�҇|��駤�ҷ�ȷҧ�ѡ�èҡ����仢������Ѻ��ͤ���������͡�',
  // TrvActionRTL
  '��ͤ����ҡ���仫���', '��ͤ����ҡ���仫��|��駤�ҷ�ȷҧ�ѡ�èҡ���仫�������Ѻ��ͤ���������͡�',
  // TrvActionCharCase
  '�ѡɳе�Ǿ����', '�ѡɳе�Ǿ����|����¹��Ǿ����ͧ��ͤ���������͡�',
  // TrvActionShowSpecialCharacters
  '&����ͧ�������˹��', '����ͧ�������˹��|�ʴ����ͫ�͹����ͧ�������˹�� �� ���˹�� �� ��������ä�',
  // TrvActionSubscript
  '���&���', '�������|��Ѻ��ͤ���������͡��繵�����',
  // TrvActionSuperscript
  '���&¡�', '���¡|��Ѻ��ͤ���������͡��繵��¡�',
  // TrvActionInsertFootnote
  '&�ԧ��ö�', '�ԧ��ö�|����ԧ��ö�',
  // TrvActionInsertEndnote
  '&��ҧ�ԧ��������ͧ�', '��ҧ�ԧ��������ͧ�|�����ҧ�ԧ��������ͧ��',
  // TrvActionEditNote
  '&��䢺ѹ�֡', '��䢺ѹ�֡|���������ԧ��ö������ҧ�ԧ',
  // TrvActionHide
  '&��͹�', '��͹|��͹�����ʴ���ǹ������͡�',  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&�ٻẺ...', '�ٻẺ|�Դ��ǨѴ����ٻẺ',
  // TrvActionAddStyleTemplate
  '&�����ٻẺ...', '�����ٻẺ|���ҧ�ٻẺ��ͤ��������ٻẺ���˹������',
  // TrvActionClearFormat,
  '&��ҧẺ', '��ҧẺ|��ҧẺ��ͤ������Ẻ���˹�ҷ������ҡ��ǹ������͡',
  // TrvActionClearTextFormat,
  '��ҧ&Ẻ��ͤ���', '��ҧẺ��ͤ���|��ҧẺ�������ҡ��ͤ���������͡',
  // TrvActionStyleInspector
  '��Ǩ�ͺ&�ٻẺ', '��Ǩ�ͺ�ٻẺ|�ʴ����ͫ�͹��ǵ�Ǩ�ͺ�ٻẺ',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   '��ŧ�', '¡��ԡ�', '�Դ�', '�á�', '&�Դ�...', '&�ѹ�֡�...', '&��ҧ�', '���������', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  '����ૹ��',
  // left, top, right, bottom sides
  '����', '��', '���', '��ҧ',
  // save changes? confirm title
  '��ͧ��úѹ�֡�������¹�ŧ��� %s ���?', '�׹�ѹ�',
  // warning: losing formatting
  '%s �Ҩ���դس�ѡɳз������ʹ���ͧ�Ѻ�ٻẺ����ͧ��úѹ�֡'#13+
  '��ͧ��úѹ�֡�͡�����ٻẺ����������?',
  // RVF format name
  '�ٻẺ RichView',
  // Error messages ------------------------------------------------------------
  '�Դ��Ҵ',
  '�����Ŵ���Դ��Ҵ'#13#13'�˵ؼŷ���������:'#13'- ��������ʹѺʹع�ٻẺ�����;'#13+
  '- ���������;'#13'- ���١�Դ��ж١��ͤ����������',
  '�բ�ͼԴ��Ҵ㹡����Ŵ����Ҿ'#13#13'�˵ؼŷ���������:'#13'- ���������ʹѺʹع�ٻẺ����Ҿ���;'#13+
  '- �����������Ҿ;'#13+
  '- ����������;'#13'- ���١�Դ��ж١��ͤ����������',
  '��úѹ�֡���Դ��Ҵ'#13#13'�˵ؼŷ���������:'#13'- ����վ�鹷���ʡ�;'#13+
  '- ��ʡ��ͧ�ѹ�����¹;'#13'- �ѧ���������ʡ��红�����;'#13+
  '- ���١�Դ��ж١��ͤ����������;'#13'- ��ʡ��������',  
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  '��� RichView (*.rvf)|*.rvf',  '��� RTF (*.rtf)|*.rtf' , '��� XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  '����ͤ��� (*.txt)|*.txt', '����ͤ��� - Unicode (*.txt)|*.txt', '����ͤ�����Ǩ�ͺ�ͧ�ѵ��ѵԟ (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  '��� HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - ���ҧ���� (*.htm;*.html)|*.htm;*.html',
  // DocX
  '���ëͿ������� (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  '��ä�������ó�', '��ä��� ''%s'' ��辺',
  // 1 string replaced; Several strings replaced
  '�١᷹��� 1 �ش', '�١᷹��� %d �ش',
  // continue search from the beginning/end?
  '����͡��� ��ͧ���价��ش��������������?',
  '������ش��������͡��� ��ͧ������仨�����͡����������?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  '�����', '�ѵ��ѵ�',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '��', '��ӵ��', '�����С͡', '�������', '������������', '����Թ���', '�����', '��-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'ᴧ���', '���', '����ͧ���', '����', '���������', '����Թ', '����Թ�҇', '��-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'ᴧ', '�����͹', '�����й��', '���ǹ�ӷ���', '��ӷ���', '�����͹', '��ǧ�', '��-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  '����', '�ͧ', '����ͧ', '�������ҧ', '���Ǣ��', '��ͧ���', '����', '��-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  '����Һ�', '��ӵ��', '����ͧ��͹', '������͹', '���Ǣ����͹�', '��Ҩҧ', '���ǹ����', '���',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '���&�ʧ', '&�ѵ��ѵ�', '&���������...', '&�վ�鹰ҹ�',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  '�����ѧ', '&��:', '���˹�', '�����ѧ', '������ҧ��ͤ���',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&����Ղ', '&���˹�ҵ�ҧ', '&�Ҿ���§����', '&�Ҿ���§���', '&��觡�ҧ��ҧ',
  // Padding button
  '&��â���...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  '�����', '&����:', '&���������...', '&��â���...',
  '��س����͡�Ղ',
  // [apply to:] text, paragraph, table, cell
  '��ͤ���', '���˹��' , '���ҧ', '����',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Ẻ�ѡ�ß', 'Ẻ�ѡ�ß', '����ç�',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Ẻ�ѡ�ß:', '&��ҴẺ�ѡ��', '�ѡɳ�Ẻ�ѡ��', '&���˹�', '&������§',
  // Script, Color, Back color labels, Default charset
  '���&������ʟ:', '&��:', '�����ѧ:', '(��Ҿ�鹰ҹ�)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  '�ѡɳо����', '�ѡɳ������', '&�մ��鹺�', '�մ&�Ѻ', '��Ǿ�����˭������',
  // Sample, Sample text
  '������ҧ', '��ͤ���������ҧ',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  '������ҧ�����ҧ����ѡ��', '&������ҧ:', '&����', '&�պ',
  // Offset group-box, Offset label, Down, Up,
  '���˹���ǵ�駟', '&��Ҵ�:', '���&ŧ�', '¡&���',
  // Scaling group-box, Scaling
  '�ҵ����ǹ', '�ҵ��&��ǹ:',
  // Sub/super script group box, Normal, Sub, Super
  '���������е��¡', '&����', '���&���', '���&¡�',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  '����', '&��:', '&����:', '&��ҧ�:', '&��҇:', '&�����ҡѹ',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  '�á���������§�����ԵԂ', '���������§�����Ե�', '���&����������ʴ�:', '�������&�������', '<<���͡>>',
  // cannot open URL
  '������ö��� "%s" ��',
  // hyperlink properties button, hyperlink style
  '&��˹��ͧ�...', '&�ٻẺ:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  '���������§�����Ե�', '�ջ���', '�շ����ѧ�ӧҹ',
  // Text [color], Background [color]
  '&��ͤ���:', '&�����ѧ',
  // Underline [color], 
  '�մ&�����:',
  // Text [color], Background [color] (different hotkeys)
  '���&����:', '���&��ѧ',
  // Underline [color], Attributes group-box,
  '�մ&�����:', '�ѡɳ�',
  // Underline type: always, never, active (below the mouse)
  '&�մ�����:', '��ʹ', '���', '���ѧ�ӧҹ',
  // Same as normal check-box
  '��&���ԇ',
  // underline active links check-box
  '&�մ������ԧ����ӧҹ',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  '�á�ѭ�ѡɳ�', '&Ẻ�ѡ�ß:', '&�ش�ѡ��П:', 'Unicode',
  // Unicode block
  '&���ͤ:',  
  // Character Code, Character Unicode Code, No Character
  '���ѡ���: %d', '���ѡ���: Unicode %d', '(����յ���ѡ��)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  '�á���ҧ�', '��Ҵ���ҧ', '�ӹǹ&�������:', '�ӹǹ&��:',
  // Table layout group-box, Autosize, Fit, Manual size,
  '����ç���ҧ�', '&��Ѻ��Ҵ�ѵ��ѵ�', '�ʹաѺ&˹�ҵ�ҧ�', '��˹�&��Ҵ�ͧ',
  // Remember check-box
  '�Ӣ�Ҵ&���ҧ����',
  // VAlign Form ---------------------------------------------------------------
  '���˹�',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  '�س���ѵ�', '�Ҿ', {*}'Position and Size', '���', '���ҧ', '��', '����',
  // Image Appearance, Image Misc, Number tabs
  {*} 'Appearance', 'Miscellaneous', 'Number',
  // Box position, Box size, Box appearance tabs
  {*} 'Position', 'Size', 'Appearance',
  // Preview label, Transparency group-box, checkbox, label
  '������ҧ:', '���������', '&�����', '��&���������:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&����¹�...', {*}'&Save...', '�ѵ��ѵ�',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  '�ҧ���˹��ǵ��', '&��èѴ���˹�:',
  '��ҧ�����ҧ�ͧ��ͤ���', '�ç��ҧ�����ҧ�ͧ��ͤ���',
  '����鹺�', '��ҧ�����ҧ', '��ҧ��鹡�ҧ',
  // align to left side, align to right side
  '��ѧ��ҹ����', '��ѧ��ҹ���',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&�ʹաѺ�:', '����͂', '&�������ҧ�:', '&�����٧:', '��Ҵ��鹰ҹ: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  {*} 'Scale &proportionally', '&Reset',
  // Inside the border, border, outside the border groupboxes
  {*} 'Inside the border', 'Border', 'Outside the border',
  // Fill color, Padding (i.e. margins inside border) labels
  {*} '&Fill color:', '&Padding:',
  // Border width, Border color labels
  {*} 'Border &width:', 'Border &color:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  {*} '&Horizontal spacing:', '&Vertical spacing:',
  // Miscellaneous groupbox, Tooltip label
  {*} 'Miscellaneous', '&Tooltip:',
  // web group-box, alt text
  '���', '��ͤ���&���ͧ:',
  // Horz line group-box, color, width, style
  '����ǹ͹', '&��:', '&�������ҧ:', '&�ٻẺ:',
  // Table group-box; Table Width, Color, CellSpacing,
  '���ҧ', '&�������ҧ��������:', '&�����:', {*}'Cell &Spacing...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  {*} '&Horizontal cell padding:', '&Vertical cell padding:', 'Border and background',
  // Visible table border sides button, auto width (in combobox), auto width label
  {*} 'Visible &Border Sides...', 'Auto', 'auto',
  // Keep row content together checkbox
  {*} '&Keep content together',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  {*} 'Rotation', '&None', '&90', '&180', '&270',
  // Cell border label, Visible cell border sides button,
  {*} 'Border:', 'Visible B&order Sides...',
  // Border, CellBorders buttons
  '&�ͺ���ҧ...', '&�ͺ����...',
  // Table border, Cell borders dialog titles
  '�ͺ���ҧ�', '�ͺ����������鹇',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  '��þ����', '&���͹حҵ����������觢���˹�ҡ�д�ɂ', '�ӹǹ&�������ͧ���:',
  '�����������ͧ�����������͢��˹������',
  // top, center, bottom, default
  '&��', '&��ҧ', '&��ҧ�', '&����������',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  '��õ�駤��', '&�������ҧ����ͧ��È:', '&�٧���ҧ���:', '&�����:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '�ͺ�', '��ҹ����ͧ��繂:',  '��&��:', '�ʧ&��', '&��:',
  // Background image
  '�ٻ&�Ҿ...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  {*} 'Horizontal position', 'Vertical position', 'Position in text',
  // Position types: align, absolute, relative
  {*} 'Align', 'Absolute position', 'Relative position',
  // [Align] left side, center, right side
  {*} 'left side of box to left side of',
  {*} 'center of box to center of',
  {*} 'right side of box to right side of',
  // [Align] top side, center, bottom side
  {*} 'top side of box to top side of',
  {*} 'center of box to center of',
  {*} 'bottom side of box to bottom side of',
  // [Align] relative to
  {*} 'relative to',
  // [Position] to the right of the left side of
  {*} 'to the right of the left side of',
  // [Position] below the top side of
  {*} 'below the top side of',
  // Anchors: page, main text area (x2)
  {*} '&Page', '&Main text area', 'P&age', 'Main te&xt area',
  // Anchors: left margin, right margin
  {*} '&Left margin', '&Right margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  {*} '&Top margin', '&Bottom margin', '&Inner margin', '&Outer margin',
  // Anchors: character, line, paragraph
  {*} '&Character', 'L&ine', 'Para&graph',
  // Mirrored margins checkbox, layout in table cell checkbox
  {*} 'Mirror&ed margins', 'La&yout in table cell',
  // Above text, below text
  {*} '&Above text', '&Below text',
  // Width, Height groupboxes, Width, Height labels
  {*} 'Width', 'Height', '&Width:', '&Height:',
  // Auto height label, Auto height combobox item
  {*} 'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  {*} 'Vertical alignment', '&Top', '&Center', '&Bottom',
  // Border and background button and title
  {*} 'B&order and background...', 'Box Border and Background',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files, URL
  '�ҧẺ����ɂ', '&�ҧ��:', '�ٻẺ Rich text', '�ٻẺ HTML',
  '��ͤ����', '��ͤ��� Unicode�', '�ٻ�Ҿ Bitmap', '�Ҿ Metafile',
  '����ҿԡ', 'URL',
  // Options group-box, styles
  '������͡', '&�ٻẺ:',
  // style options: apply target styles, use source styles, ignore styles
  '���ٻẺ�ͧ�͡����������', '�ѡ���ٻẺ����ѡɳ����',
  '�ѡ���ѡɳ�, ¡��ԡ�ٻẺ',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  '��Ǣ����������ӴѺ�Ţ�', '��Ǣ������', '�ӴѺ�Ţ', '��¡���ѭ�ѡɳ���Ǣ������', '��¡���ӴѺ�Ţ',
  // Customize, Reset, None
  '&��˹��ͧ�...', '&��駤������', '����Ղ',
  // Numbering: continue, reset to, create new
  '�ӴѺ�Ţ���仂', '��駤���ӴѺ�Ţ�', '���ҧ��¡������ ������鹨ҡ�',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  '��ſҺ�', '���ѹ��', '�Ţ�ҹ�Ժ', '��ſ���ҧ', '���ѹ��ҧ',
  // Bullet type
  'ǧ���', '��', '�ѵ����',
  // Level, Start from, Continue numbering, Numbering group-box
  '&�дѺ:', '&������ҡ:', '&���仂', '�ӴѺ�Ţ�',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  '��˹���¡���ͧ', '�дѺ', '&�Ѻ�ӹǹ:', '�س���ѵԢͧ��¡��', '&��Դ�ͧ��¡��:',
  // List types: bullet, image
  '�ѭ�ѡɳ�', '�Ҿ', '�á�����Ţ�|',
  // Number format, Number, Start level from, Font button
  '&�ٻẺ����Ţ:', '����Ţ', '&������ҡ�Ţ�:', '&Ẻ�ѡ�ß...',
  // Image button, bullet character, Bullet button,
  '&�Ҿ...', '�ѡ���&�ʴ���Ǣ������', '&��Ǣ�����...',
  // Position of list text, bullet, number, image, text
  '���˹觢ͧ��¡�â�ͤ���', '���˹���Ǣ������', '���˹觵���Ţ', '���˹��Ҿ', '���˹觢�ͤ���',
  // at, left indent, first line indent, from left indent
  '&���:', '����ͧ&����:', '&�������ͧ��÷Ѵ�á�:', '�ҡ�������ͧ���',
  // [only] one level preview, preview
  '&������ҧ�дѺ˹��', '������ҧ',
  // Preview text
  '��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ���',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  '�Ѵ�Դ���', '�Ѵ�Դ��҂', '��觡�ҧ',
  // level #, this level (for combo-box)
  '�дѺ %d', '�дѺ���',
  // Marker character dialog title
  '����ѡ����ʴ���Ǣ������',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  '��鹢ͺ���˹����о����ѧ', '�ͺ', '�����ѧ',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  '��駤�ҡ��ͧ', '&��:', '&�������ҧ&��ͺ�͡:', '�������ҧ&�ͺ�:', '���Тͺ&��ͤ����..',
  // Sample, Border type group-boxes;
  '������ҧ', '��Դ�ͺ',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&����Ղ', '&�����', '&���', '&���', '˹�&����', '˹�&��¹͡',
  // Fill color group-box; More colors, padding buttons
  '�����', '���������...', '��ͧ��ҧ�����ҧ...',
  // preview text
  '��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ���',
  // title and group-box in Offsets dialog
  '�ͺ�ѡ�Ç', '��鹢ͺ��������',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  '���˹��', '��èѴ��', '&����', '&���', '&��觡�ҧ', '&�Դ�ͺ',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  '������ҧ', '&��͹�:', '&��ѧ�:', '������ҧ&��÷Ѵ:', '&��Ҵ:',
  // Indents group-box; indents: left, right, first line
  '�������ͧ', '&���:', '&���:', '&��÷Ѵ�á:',
  // indented, hanging
  '&����ͧ', '&�ǹ', '������ҧ',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '˹�����', '1.5 ��÷Ѵ', '�ͧ���', '���ҧ����', '�����͹', '���º�÷Ѵ',
  // preview text
  '��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ��� ��ͤ���',
  // tabs: Indents and spacing, Tabs, Text flow
  '�������ͧ���������ҧ', '��', '��ȷҧ�ͧ��ͤ���',
  // tab stop position label; buttons: set, delete, delete all
  '&���˹觢ͧ����ش:', '&��駤��', '&ź�', 'ź&�������',
  // tab align group; left, right, center aligns,
  '��èѴ���˹�', '�Դ&���', '�Դ&��҇', '&��觡�ҧ',
  // leader radio-group; no leader
  '����§', '(����Ղ)',
  // tab stops to be deleted, delete none, delete all labels
  '����ش������ҧ�:', '(����Ղ)', '��ҧ������',
  // Pagination group-box, keep with next, keep lines together
  '��˹��', '&�����Ѻ�Ѵ仂', '&�纺�÷Ѵ�����¡ѹ',
  // Outline level, Body text, Level #
  '&�дѺ�����ҧ:', '���ͤ���', '�дѺ %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  '������ҧ��þ����', '�ʹդ������ҧ�', '���˹��', '˹��:',
  // Invalid Scale, [page #] of #
  '��سҾ�����Ţ�ҡ 10 �֧� 500�', '�ͧ %d',
  // Hints on buttons: first, prior, next, last pages
  '˹���á', '˹�ҡ�͹', '˹�ҶѴ�', '˹���ش����',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  '������ҧ�����ҧ����', '������ҧ', '�����ҧ����', '�����ҧ��鹢ͺ���ҧ�������',
  // vertical, horizontal (x2)
  '&�ǵ��:', '&�ǹ͹�:', '��&���:', '��&�͹:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  '��鹢ͺ�', '��õ�駤��', '&��:', '��&��͹:', '��&��:',
  // Width; Border type group-box;
  '&���ҧ�:', '��Դ�ͧ��鹢ͺ',
  // Border types: none, sunken, raised, flat
  '&����Ղ', '&�غ�', '&¡�', '&�Һ�',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  '�¡', '�¡�',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&�кبӹǹ�ͧ����Ф������', '&�����������',
  // number of columns, rows, merge before
  '�ӹǹ&�������:', '�ӹǹ&��:', '&��ҹ��͹�¡�',
  // to original cols, rows check-boxes
  '�ӹǹ&�������', '�¡��&�������',
  // Add Rows form -------------------------------------------------------------
  '�����ǂ', '�ӹǹ&��:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  '��駤��˹�ҡ�д��', '˹��', '��ǡ�д����з��¡�д��',
  // margins group-box, left, right, top, bottom
  '���Тͺ (���������)', '���Тͺ (����)', '&���:', '&��:', '&���:', '&��ҧ:',
  // mirror margins check-box
  '&�����з�͹�',
  // orientation group-box, portrait, landscape
  '����ҧ��', '&�ǵ�駟', '&�ǹ͹',
  // paper group-box, paper size, default paper source
  '��д��', '&��Ҵ��д��:', '&���觡�д��:',
  // header group-box, print on the first page, font button
  '��ǡ�д��', '&��ͤ���:', '&��ǡ�д���˹���á', '&Ẻ�ѡ�ß...',
  // header alignments: left, center, right
  '&���', '&��ҧ', '&���',
  // the same for footer (different hotkeys)
  '���¡�д��', '&��ͤ���:', '���¡�д��&�˹���á', 'Ẻ&�ѡ�ß...',
  '&���', '&��ҧ', '&���',
  // page numbers group-box, start from
  '�����Ţ˹��', '&������ҡ�:',
  // hint about codes
  '�ش�ѡ��о����:'#13'&&p - �����Ţ˹��; &&P - �ӹǹ˹��; &&d - �ѹ���Ѩ�غѹ; &&t - ���һѨ�غѹ',
  // Code Page form ------------------------------------------------------------
  // title, label
  '˹�����������ͤ���', '&���͡��������������:',
  // thai, japanese, chinese (simpl), korean
  '�', '�����', '�չ (����ء��)', '�����',
  // chinese (trad), central european, cyrillic, west european
  '�չ (������)', '���û��ҧ��е��ѹ�͡', '�������', '���û���ѹ��',
  // greek, turkish, hebrew, arabic
  '��ա', '��á�', '�պ�ق', '����Ѻ',
  // baltic, vietnamese, utf-8, utf-16
  '��ŵԡ', '���´���', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  '�ѡɳС���ʴ���', '&���͡�ٻẺ�:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  '�ŧ�繢�ͤ����', '&���͡��Ǥ�蹂:',
  // line break, tab, ';', ','
  '����觺�÷Ѵ', '��', '�Ѳ�Ҥ', '����Ҥ',
  // Table sort form -----------------------------------------------------------
  // error message
  '���ҧ��Сͺ�����Ƿ��١��ҹ�������ö���§�ӴѺ��',
  // title, main options groupbox
  '���§�ӴѺ���ҧ�', '���§�ӴѺ�',
  // sort by column, case sensitive
  '&���§�ӴѺ����������:', '&�¡��е�Ǿ�����˭�-���',
  // heading row, range or rows
  '�¡��ǹ���&�ͧ�ǂ', '��㹡�èѴ���§�: �ҡ %d �֧� %d',
  // order, ascending, descending
  '�ӴѺ', '&������ҡ�', '&�ҡ仹��',
  // data type, text, number
  '��Դ', '&��ͤ���', '&����Ţ',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  {*} 'Insert Number', 'Properties', '&Counter name:', '&Numbering type:',
  // numbering groupbox, continue, start from
  {*} 'Numbering', 'C&ontinue', '&Start from:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  {*} 'Caption', '&Label:', '&Exclude label from caption',
  // position radiogroup
  {*} 'Position', '&Above selected object', '&Below selected object',
  // caption text
  {*} 'Caption &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  {*} 'Numbering', 'Figure', 'Table',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  {*} 'Visible Border Sides', 'Border',
  // Left, Top, Right, Bottom checkboxes
  {*} '&Left side', '&Top side', '&Right side', '&Bottom side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  '��Ѻ��Ҵ���������ҧ�', '��Ѻ��Ҵ�ǵ��ҧ�',
  // Hints on indents: first line, left (together with first), left (without first), right
  '����ͧ�á�', '����ͧ����', '����ͧ��', '����ͧ��ҟ',
  // Hints on lists: up one level (left), down one level (right)
  '��������дѺ�', 'Ŵ˹���дѺ�',
  // Hints for margins: bottom, left, right and top
  '�ͺ��ҧ', '�ͺ����', '�ͺ���', '�ͺ��',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  '�纪Դ����', '�纪Դ���', '�纪Դ��觡�ҧ', '�纪Դ�ȹ���',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  '����', '����ͧ����', '����ժ�ͧ��ҧ', '�������ͧ %d', '��¡�����˹��',
  // Hyperlink, Title, Subtitle
  '�ԧ��', '��������ͧ', '��������ͧ�ͧ',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '�����', '����鹺ҧ', '�����˹�', '�ҡ',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  '�����ҧ�ԧ', '�����ҧ�ԧ˹�', '�����ҧ�§�ҧ', '�����ҧ�§˹�',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  '���ͧ��ͤ���', 'HTML �ѹ��', '���� HTML', 'HTML ���',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  '�ӡѴ���� HTML', '������� HTML', '������ҧ HTML', '��Ǿ���� HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML ������', 'HTML ��ҧ�ԧ', '��ǹ���', '��ǹ����', '�Ţ˹��',
  // Caption
  {*} 'Caption',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  '��ҧ�ԧ��������ͧ', '��ҧ�ԧ�ԧ��ö', '��ͤ�����ҧ�ԧ��������ͧ', '��ͤ����ԧ��ö',
  // Sidenote Reference, Sidenote Text
  {*} 'Sidenote Reference', 'Sidenote Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  '��', '�վ����ѧ', '�����', '����������', '�������',
  // default background color, default text color, [color] same as text
  '�����������վ����ѧ', '������������Ẻ�ѡ��', '����͹�Ѻ��ͤ���',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  '����', '˹�', '���', '�ش', '�ش˹�', '�ش���',
  // underline types: thick dashed, long dashed, thick long dashed,
  '�ش���˹�', '�ش������', '�ش���˹����',
  // underline types: dash dotted, thick dash dotted,
  '�ش��Шش', '�ش��Шش˹�',
  // underline types: dash dot dotted, thick dash dot dotted
  '�ش��Шش�ش', '�ش��Шش�ش˹�',
  // sub/superscript: not, subsript, superscript
  '�����������/���¡', '�������', '���¡',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  '������Ѻ���¢��:', '������ͧ�ѹ', '����仢��', '���仫���',
  // bold, not bold
  '���˹�', '���˹�',
  // italic, not italic
  '������§', '������§',
  // underlined, not underlined, default underline
  '�մ�����', '���մ�����', '���������鹡�մ�����',
  // struck out, not struck out
  '�մ�Ѻ', '���մ�Ѻ',
  // overlined, not overlined
  '�մ��鹺�', '���մ��鹺�',
  // all capitals: yes, all capitals: no
  '����˭������', '��������˭������',
  // vertical shift: none, by x% up, by x% down
  '�������¹�ŧ����ǵ��', '��������ա %d%%', 'Ŵŧ�ա %d%%',
  // characters width [: x%]
  '������ҧ�ѡ���',
  // character spacing: none, expanded by x, condensed by x
  '������ҧ�ѡ��л���', '����������ҧ�ա %s', 'Ŵ������ҧ�ա %s',
  // charset
  'ʤ�Ի��',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  '����������Ẻ�ѡ��', '�������������˹��', '�������ش',
  // [hyperlink] highlight, default hyperlink attributes
  '��', '����������',
  // paragraph aligmnment: left, right, center, justify
  '��ͤ����Դ����', '��ͤ����Դ���', '��觡�ҧ', '�����',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  '�����٧��÷Ѵ: %d%%', '���������ҧ��÷Ѵ: %s', '�����٧��÷Ѵ: ���ҧ���� %s',
  '�����٧��÷Ѵ: �ʹշ�� %s',
  // no wrap, wrap
  '¡��ԡ��õѴ�Ӣ�鹺�÷Ѵ����', '�Ѵ�Ӣ�鹺�÷Ѵ����',
  // keep lines together: yes, no
  '�ѡ�Һ�÷Ѵ�����¡ѹ', '����ͧ�ѡ�Һ�÷Ѵ�����¡ѹ',
  // keep with next: yes, no
  '�ѡ�����˹�ҶѴ�', '����ͧ�ѡ�����˹�ҶѴ�',
  // read only: yes, no
  '��ҹ��ҹ��', '����ö�����',
  // body text, heading level x
  '�дѺ�ç��ҧ: ��ǹ��ͤ���', '�дѺ�ç��ҧ: %d',
  // indents: first line, left, right
  '������ͧ�á', '����ͧ����', '����ͧ���',
  // space before, after
  '��ͧ��ҧ��͹', '��ͧ��ҧ��ѧ',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  '��', '�����', '��', '����', '���', '��ҧ',
  // tab leader (filling character)
  '���á',
  // no, yes
  '���', '��',
  // [padding/spacing/side:] left, top, right, bottom
  '����', '��', '���', '��ҧ',
  // border: none, single, double, triple, thick inside, thick outside
  '�����', '�����', '���', '���', '˹Ҵ�ҹ�', '˹Ҵ�ҹ�͡',
  // background, border, padding, spacing [between text and border]
  '�����ѧ', '�ͺ', '����', '��ͧ�',
  // border: style, width, internal width, visible sides
  '�ٻẺ', '�������ҧ', '�����������', '��Ҵ����ͧ���',
  // style inspector -----------------------------------------------------------
  // title
  '��Ǩ�ͺ�ٻẺ',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  '�ٻẺ', '(�����)', '���˹��', 'Ẻ�ѡ��', '�س���ѵ�',
  // border and background, hyperlink, standard [style]
  '��鹢ͺ��о����ѧ', '�ԧ��', '(�ҵðҹ)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  '�ٻẺ', '�ٻẺ',
  // name, applicable to,
  '&����:', '����&�Ѻ:',
  // based on, based on (no &),
  '&����鹰ҹ�ͧ:', '����鹰ҹ�ͧ:',
  //  next style, next style (no &)
  '�ٻẺ����Ѻ&���˹�ҵ���:', '�ٻẺ����Ѻ���˹�ҵ���:',
  // quick access check-box, description and preview
  '&��Ҷ֧���ҧ�Ǵ����', '��������´���&������ҧ:',
  // [applicable to:] paragraph and text, paragraph, text
  '���˹����Т�ͤ���', '���˹��', '��ͤ���',
  // text and paragraph styles, default font
  '�ٻẺ��ͤ�������ػẺ���˹��', '����������Ẻ�ѡ��',
  // links: edit, reset, buttons: add, delete
  '���', '��駤���������', '&����', '&ź',
  // add standard style, add custom style, default style name
  '����&�ٻẺ�ҵðҹ...', '����&�ٻẺ��˹��ͧ', '�ٻẺ%d',
  // choose style
  '���͡&�ٻẺ:',
  // import, export,
  '&�����...', '&���͡...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 '�ٻẺ RichView (*.rvst)|*.rvst', '�����Ŵ���Դ��Ҵ', '�����������ٻẺ',
  // Title, group-box, import styles
  '������ٻẺ�ҡ���', '�����', '&�ٻẺ�����:',
  // existing styles radio-group: Title, override, auto-rename
  '�ٻẺ���������', '&�Ѻ', '&�����������¹����',
  // Select, Unselect, Invert,
  '&���͡', '&������͡', '&��͹��Ѻ',
  // select/unselect: all styles, new styles, existing styles
  '&�ٻẺ������', '&�ٻẺ����', '&�ٻẺ���������',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(��ҧ�ٻẺ)', '(�ٻẺ������)',
  // dialog title, prompt
  '�ٻẺ', '&���͡���ٻẺ:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&������͹', '&¡��ԡ�������', '&����㹾��ҹء���',
  // Progress messages ---------------------------------------------------------
  '���ѧ��ǹ���Ŵ %s', '���ѧ�������þ����...', '���ѧ�����˹�� %d',
  '�ŧ�ҡ RTF...',  '�ŧ��� RTF...',
  '���ѧ��ҹ %s...', '���ѧ��¹ %s...', '��ͤ���',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  {*} 'No Note', 'Footnote', 'Endnote', 'Sidenote', 'Text Box'
  );


initialization
  RVA_RegisterLanguage(
    'Thai', // english language name, do not translate
    {*} 'Thai', // native language name
    THAI_CHARSET, @Messages);

end.
