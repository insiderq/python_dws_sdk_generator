/*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Demo project for C++Builder 2009+               }
{       You can use it as a basis for your              }
{       applications.                                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************/
//---------------------------------------------------------------------------
#ifndef UnitUnicode1H
#define UnitUnicode1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "PtblRV.hpp"
#include "RichView.hpp"
#include "RichViewActions.hpp"
#include "Ruler.hpp"
#include "RVEdit.hpp"
#include "RVFontCombos.hpp"
#include "RVRuler.hpp"
#include "RVScroll.hpp"
#include "RVStyle.hpp"
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <Graphics.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Graphics.hpp>
#include "RVStyleFuncs.hpp"
#include <XPMan.hpp>
//---------------------------------------------------------------------------
class TForm3 : public TForm
{
__published:	// IDE-managed Components
        TRichViewEdit *RichViewEdit1;
        TStatusBar *StatusBar1;
        TCoolBar *CoolBar1;
        TToolBar *ToolBar1;
        TToolButton *ToolButton11;
        TToolButton *ToolButton1;
        TToolButton *ToolButton2;
        TToolButton *ToolButton3;
        TToolButton *ToolButton13;
        TToolButton *ToolButton22;
        TToolButton *ToolButton4;
        TToolButton *ToolButton32;
        TToolButton *ToolButton33;
        TToolButton *ToolButton34;
        TToolButton *ToolButton17;
        TToolButton *ToolButton18;
        TToolButton *ToolButton19;
        TToolButton *ToolButton5;
        TToolButton *ToolButton20;
        TToolButton *ToolButton21;
        TToolButton *ToolButton10;
        TToolButton *ToolButton46;
        TToolButton *ToolButton12;
        TToolButton *ToolButton47;
        TToolButton *ToolButton62;
        TToolButton *ToolButton63;
        TToolBar *ToolBar2;
        TToolButton *ToolButton41;
        TToolButton *ToolButton42;
        TToolButton *ToolButton43;
        TToolButton *ToolButton44;
        TToolButton *ToolButton29;
        TToolButton *ToolButton6;
        TToolButton *ToolButton30;
        TToolButton *ToolButton31;
        TToolButton *ToolButton35;
        TToolButton *ToolButton36;
        TToolButton *ToolButton38;
        TToolButton *ToolButton37;
        TToolButton *ToolButton49;
        TToolButton *ToolButton50;
        TToolButton *ToolButton51;
        TToolButton *ToolButton8;
        TToolButton *ToolButton7;
        TToolButton *ToolButton57;
        TToolButton *ToolButton61;
        TToolButton *ToolButton60;
        TToolButton *ToolButton53;
        TToolButton *ToolButton54;
        TToolButton *ToolButton40;
        TToolButton *ToolButton39;
        TToolBar *ToolBar3;
        TToolButton *ToolButton9;
        TToolButton *ToolButton14;
        TToolButton *ToolButton15;
        TToolButton *ToolButton16;
        TToolButton *ToolButton27;
        TToolButton *ToolButton23;
        TToolButton *ToolButton24;
        TToolButton *ToolButton45;
        TToolButton *ToolButton28;
        TToolButton *ToolButton25;
        TToolButton *ToolButton26;
        TToolBar *ToolBar4;
        TToolButton *ToolButton48;
        TToolButton *ToolButton52;
        TToolButton *ToolButton55;
        TToolButton *ToolButton56;
        TToolButton *ToolButton58;
        TToolButton *ToolButton59;
        TToolBar *ToolBar5;
        TRVFontComboBox *cmbFont;
        TRVFontSizeComboBox *cmbFontSize;
        TRVRuler *RVRuler1;
        TMainMenu *MainMenu1;
        TMenuItem *mitFile;
        TMenuItem *New1;
        TMenuItem *Load1;
        TMenuItem *Save1;
        TMenuItem *SaveAs1;
        TMenuItem *Export1;
        TMenuItem *N1;
        TMenuItem *PageSetup1;
        TMenuItem *PrintPreview1;
        TMenuItem *Print1;
        TMenuItem *N3;
        TMenuItem *mitExit;
        TMenuItem *mitEdit;
        TMenuItem *Undo1;
        TMenuItem *Redo1;
        TMenuItem *N4;
        TMenuItem *Cut1;
        TMenuItem *Copy1;
        TMenuItem *n2;
        TMenuItem *PasteSpecial1;
        TMenuItem *N5;
        TMenuItem *Find1;
        TMenuItem *FindNext1;
        TMenuItem *Replace1;
        TMenuItem *N26;
        TMenuItem *CharacterCase1;
        TMenuItem *N20;
        TMenuItem *InsertPageBreak1;
        TMenuItem *RemovePageBreak1;
        TMenuItem *N15;
        TMenuItem *SelectAll1;
        TMenuItem *mitFont;
        TMenuItem *Changefont1;
        TMenuItem *Font1;
        TMenuItem *N7;
        TMenuItem *mitFontStyle;
        TMenuItem *Bold1;
        TMenuItem *Italic1;
        TMenuItem *Underline1;
        TMenuItem *Strikeout1;
        TMenuItem *N14;
        TMenuItem *AllCapitals1;
        TMenuItem *Overline1;
        TMenuItem *mitFontSize;
        TMenuItem *ShrinkFont1;
        TMenuItem *GrowFont1;
        TMenuItem *N13;
        TMenuItem *ShrinkFontByOnePoint1;
        TMenuItem *GrowFontByOnePoint1;
        TMenuItem *extColor1;
        TMenuItem *extBackgroundColor1;
        TMenuItem *mitPara;
        TMenuItem *rvActionParagraph11;
        TMenuItem *ParagraphBorders1;
        TMenuItem *N18;
        TMenuItem *AlignLeft1;
        TMenuItem *AlignCenter1;
        TMenuItem *AlignRight1;
        TMenuItem *Justify1;
        TMenuItem *N25;
        TMenuItem *BulletsandNumbering1;
        TMenuItem *Bullets1;
        TMenuItem *Numbering1;
        TMenuItem *N16;
        TMenuItem *Leftjustify1;
        TMenuItem *N6;
        TMenuItem *DecreaseIndent1;
        TMenuItem *IncreaseIndent1;
        TMenuItem *N17;
        TMenuItem *SingleLineSpacing1;
        TMenuItem *N15LineSpacing1;
        TMenuItem *DoubleLineSpacing1;
        TMenuItem *N19;
        TMenuItem *ParagraphBackgroundColor1;
        TMenuItem *mitFormat;
        TMenuItem *Background1;
        TMenuItem *BackgroundColor1;
        TMenuItem *N22;
        TMenuItem *FillColor1;
        TMenuItem *Properties1;
        TMenuItem *mitInsert;
        TMenuItem *File1;
        TMenuItem *Picture1;
        TMenuItem *HorizontalLine1;
        TMenuItem *HypertextLink1;
        TMenuItem *InsertSymbol1;
        TMenuItem *mitTable;
        TMenuItem *InsertTable1;
        TMenuItem *N9;
        TMenuItem *InsertColumnLeft1;
        TMenuItem *InsertColumnRight1;
        TMenuItem *N8;
        TMenuItem *InsertRowAbove1;
        TMenuItem *InsertRowBelow1;
        TMenuItem *N10;
        TMenuItem *DeleteRows1;
        TMenuItem *rvActionTableDeleteCols11;
        TMenuItem *DeleteTable1;
        TMenuItem *N12;
        TMenuItem *mitTableSelect;
        TMenuItem *SelectTable1;
        TMenuItem *SelectColumns1;
        TMenuItem *SelectRows1;
        TMenuItem *SelectCell1;
        TMenuItem *N21;
        TMenuItem *mitTableAlignCellContents;
        TMenuItem *AlignCellToTheTop1;
        TMenuItem *AlignCellToTheMiddle1;
        TMenuItem *AlignCellToTheBottom1;
        TMenuItem *DefaultCellVerticalAlignment1;
        TMenuItem *mitTableCellBorders;
        TMenuItem *LeftBorder1;
        TMenuItem *rvActionTableCellTopBorder11;
        TMenuItem *rvActionTableCellRightBorder11;
        TMenuItem *rvActionTableCellBottomBorder11;
        TMenuItem *rvActionTableCellAllBorders11;
        TMenuItem *rvActionTableCellNoBorders11;
        TMenuItem *N11;
        TMenuItem *SplitCells1;
        TMenuItem *MergeCells1;
        TMenuItem *N24;
        TMenuItem *ShowGridLines1;
        TMenuItem *N23;
        TMenuItem *ableProperties1;
        TRVStyle *RVStyle1;
        TRVAControlPanel *RVAControlPanel1;
        TRVAPopupMenu *RVAPopupMenu1;
        TRVPrint *RVPrint1;
        TColorDialog *ColorDialog1;
        TPopupMenu *pmFakeDropDown;
	TButton *btnLanguage;
    TMenuItem *N27;
    TMenuItem *mitTextFlow;
    TMenuItem *NormalTextFlow1;
    TMenuItem *ClearTextFlowatLeftSide1;
    TMenuItem *ClearTextFlowatRightSide1;
    TMenuItem *ClearTextFlowatBothSides1;
    TMenuItem *N28;
    TMenuItem *ObjectPosition1;
    TMenuItem *N29;
    TMenuItem *RemoveHyperlinks1;
    TToolButton *ToolButton64;
    TToolButton *ToolButton65;
    TMenuItem *Hide1;
    TToolBar *tbUnits;
    TComboBox *cmbUnits;
	TMenuItem *Sort1;
	TMenuItem *SplitTable1;
	TMenuItem *ConverttoText1;
	TMenuItem *mitRotateCell;
	TMenuItem *NoCellRotation1;
	TMenuItem *RotateCellby90Degrees1;
	TMenuItem *RotateCellby180Degrees1;
	TMenuItem *RotateCellby270Degrees1;
	TToolButton *ToolButton66;
	TToolButton *ToolButton67;
	TToolButton *ToolButton68;
	TButton *btnSkin;
	TRVStyleTemplateComboBox *cmbStyles;
	TMenuItem *N30;
	TMenuItem *Styles1;
	TMenuItem *AddStyle1;
	TMenuItem *ClearFormat1;
	TMenuItem *ClearTextFormat1;
	TMenuItem *InspectStyles1;
	TProgressBar *ProgressBar1;
	TXPManifest *XPManifest1;
	TToolButton *ToolButton69;
	TMenuItem *N31;
	TMenuItem *InsertFootnote1;
	TMenuItem *InsertEndnote1;
	TMenuItem *Sidenote1;
	TMenuItem *InsertEndnote2;
	TMenuItem *EditNote1;
	TMenuItem *Number1;
	TMenuItem *InsertCaption1;
		void __fastcall FormCreate(TObject *Sender);
		void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
		void __fastcall mitExitClick(TObject *Sender);
		void __fastcall RichViewEdit1Jump(TObject *Sender, int id);
		void __fastcall RichViewEdit1ReadHyperlink(TCustomRichView *Sender,
		  const UnicodeString Target, const UnicodeString Extras,
		  TRVLoadFormat DocFormat, int &StyleNo, TRVTag &ItemTag,
		  TRVRawByteString &ItemName);
		void __fastcall RichViewEdit1KeyPress(TObject *Sender, wchar_t &Key);
		void __fastcall pmFakeDropDownPopup(TObject *Sender);
		void __fastcall RVAControlPanel1MarginsChanged(TrvAction *Sender,
		  TCustomRichViewEdit *Edit);
		void __fastcall RVAControlPanel1Download(TrvAction *Sender,
		  const UnicodeString Source);
	void __fastcall btnLanguageClick(TObject *Sender);
	void __fastcall cmbUnitsClick(TObject *Sender);
	void __fastcall btnSkinClick(TObject *Sender);
	void __fastcall RichViewEdit1Progress(TCustomRichView *Sender, TRVLongOperation Operation,
          TRVProgressStage Stage, BYTE PercentDone);
	void __fastcall RVPrint1SendingToPrinter(TCustomRichView *Sender, int PageCompleted,
          TRVPrintingStep Step);
private:	// User declarations
        void Localize();
        void __fastcall rvActionSave1DocumentFileChange(TObject* Sender,
        TCustomRichViewEdit* Editor, const UnicodeString FileName,
        TrvFileSaveFilter FileFormat, bool IsNew);
        void __fastcall ColorPickerShow(TObject* Sender);
		void __fastcall ColorPickerHide(TObject* Sender);
		void  UpdateLinkedControls();
		void __fastcall NoteEditorEnter(TObject* Sender);
		void __fastcall NoteEditorExit(TObject* Sender);
		void __fastcall rvActionEditNoteHide(TObject* Sender);
		void __fastcall rvActionEditNoteShowing(TrvAction* Sender, TForm* Form);
		void __fastcall rvActionEditNoteFormCreate(TrvAction* Sender, TForm* Form);
public:		// User declarations
        __fastcall TForm3(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm3 *Form3;
//---------------------------------------------------------------------------
#endif
 