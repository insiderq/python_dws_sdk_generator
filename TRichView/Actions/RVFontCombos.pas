
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       TRVFontSizeComboBox, TRVFontCharsetComboBox,    }
{       TRVFontComboBox                                 }
{                                                       }
{       Copyright (c) 2002-2007, Sergey Tkachenko       }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


{==============================================================================}
{
 TRVFontSizeComboBox - combobox for selecting sizes of font
 TRVFontCharsetComboBox - combobox for selecting character sets of font
 TRVFontComboBox - combobox with font names
 Assign FontName property of these combos and they will be filled with values
}
{==============================================================================}

{$I RV_Defs.inc}
{$I RichViewActions.inc}

unit RVFontCombos;

{------------------------------------------------------------------------------}
{

  Changes:

  v1.7.1
  XE4 support

  v1.7
  new: Integration with TRichViewEdit

  v1.6
  new: TNT controls are supported

  v1.5
  chg: TRVFontComboBox does not include fonts started with '@'

  v1.4
  fix: TRVFontComboBox stored font names in DFM (and even worse - used these
  font names)

  v1.3
  fix: correct drawing of parent background if style is "simple"
  added: TRVFontCombobox - mainly for having a combobox drawing background
     correctly
  v1.2
  chg: unit SVFontCombos is separated into two parts: RVFontCombos.pas and
       RVFontCombosReg.pas
  chg: RV prefix added

  ----------------------------------------------------------------------------

 TRVFontCharsetComboBox uses Items.Objects property itself, do not use it.
 TRVFontCharsetComboBox has additional properties:
 AddDefaultCharset: Boolean - if True, DEFAULT_CHARSET item will be added
 DefaultCharsetCaption: String - caption of list item above
 Charsets[Index: Integer]: TFontCharset - returns Charset of the Index-th item
 function IndexOfCharset(Charset: TFontCharset):Integer - returns index of
 item with specified Charset in combo, or -1 if not found
}
{------------------------------------------------------------------------------}


interface

uses
  {$IFDEF RICHVIEWDEFXE4}AnsiStrings,{$ENDIF}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$IFDEF USERVKSDEVTE}
  te_theme,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntClasses, TntStdCtrls,
  {$ENDIF}
  StdCtrls,
  RVScroll, RVXPTheme, RVEdit, RichViewActions, RVALocalize;

type
  TCustomRVFontComboBox = class({$IFDEF USERVTNT}TTntComboBox{$ELSE}TComboBox{$ENDIF})
  private
    FEditor: TCustomRVControl;
    FRichViewEdit: TCustomRichViewEdit;
    FActionFont: TrvActionFontEx;
    FUpdating, FNoFocus: Boolean;
    procedure SetRichViewEdit(Value: TCustomRichViewEdit);
    procedure SetEditor(Value: TCustomRVControl);
    procedure SetActionFont(Value: TrvActionFontEx);
    procedure DoCurTextStyleChanged(Sender: TObject);
    procedure DoSRVActiveEditorChanged(Sender: TObject);
  protected
    procedure DoExit; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure UpdateSelection; dynamic; abstract;
    procedure FocusEditor;
    property RichViewEdit: TCustomRichViewEdit read FRichViewEdit write SetRichViewEdit;
  public
    destructor Destroy; override;
  published
    property Editor: TCustomRVControl read FEditor write SetEditor;
    property ActionFont: TrvActionFontEx read FActionFont write SetActionFont;
  end;

  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVFontSizeComboBox = class(TCustomRVFontComboBox)
  private
    { Private declarations }
    PixelsPerInch: Integer;
    FFontName: TFontName;
    procedure SetFontName(const Value: TFontName);
    procedure Build;
    {$IFnDEF RICHVIEWDEFXE2}
    procedure WMEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
    {$ENDIF}
  protected
    { Protected declarations }
    function IsValidChar(Key: Char): Boolean;
    procedure KeyPress(var Key: Char); override;
    procedure UpdateSelection; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Click; override;
    property FontName: TFontName read FFontName write SetFontName;
  published
    { Published declarations }
    {$IFDEF RICHVIEWDEF6}
    property AutoComplete default False;
    {$ENDIF}    
  end;
  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVFontCharsetComboBox = class(TCustomRVFontComboBox)
  private
    { Private declarations }
    FFontName: TRVALocString;
    FAddDefaultCharset: Boolean;
    FDefaultCharsetCaption: {$IFDEF USERVTNT}WideString{$ELSE}String{$ENDIF};
    procedure SetFontName(const Value: TRVALocString);
    procedure Build;
    function GetCharsets(Index: Integer): TFontCharset;
    procedure SetAddDefaultCharset(const Value: Boolean);
    {$IFnDEF RICHVIEWDEFXE2}
    procedure WMEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
    {$ENDIF}
  protected
    { Protected declarations }
    procedure UpdateSelection; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);override;
    function IndexOfCharset(Charset: TFontCharset):Integer;
    procedure Click; override;
    property FontName: TRVALocString read FFontName write SetFontName;
    property Charsets[Index: Integer]: TFontCharset read GetCharsets;
  published
    { Published declarations }
    property AddDefaultCharset: Boolean read FAddDefaultCharset write SetAddDefaultCharset;
    property DefaultCharsetCaption: {$IFDEF USERVTNT}WideString{$ELSE}String{$ENDIF}
      read FDefaultCharsetCaption write FDefaultCharsetCaption;
  end;
  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVFontComboBox = class(TCustomRVFontComboBox)
  private
    {$IFnDEF RICHVIEWDEFXE2}
    procedure WMEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
    {$ENDIF}
  protected
    procedure CreateWnd; override;
    procedure UpdateSelection; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Click; override;
  published
    property Items stored False;
    {$IFDEF RICHVIEWDEF6}
    property AutoComplete default False;
    {$ENDIF}
  end;

implementation
uses RVTypes, RVStyle, RVItem;
{======================= TCustomRVFontComboBox ================================}
procedure TCustomRVFontComboBox.SetEditor(Value: TCustomRVControl);
begin
  if (Value<>nil) and (Value.SRVGetActiveEditor(False)=nil) then
    raise ERichViewError.Create(errInvalidRVControl);
  if Value <> FEditor then
  begin
    if FEditor <> nil then begin
      {$IFDEF RICHVIEWDEF5}
      FEditor.RemoveFreeNotification(Self);
      {$ENDIF}
      FEditor.UnregisterSRVChangeActiveEditorHandler(Self);
    end;
    FEditor := Value;
    if FEditor <> nil then begin
      FEditor.FreeNotification(Self);
      FEditor.RegisterSRVChangeActiveEditorHandler(Self, DoSRVActiveEditorChanged);
    end;
    if Editor<>nil then
      RichViewEdit := Editor.SRVGetActiveEditor(False) as TCustomRichViewEdit
    else
      RichViewEdit := nil;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.SetRichViewEdit(Value: TCustomRichViewEdit);
begin
  if Value <> FRichViewEdit then
  begin
    if FRichViewEdit <> nil then
      FRichViewEdit.UnregisterCurTextStyleChangeHandler(Self);
    FRichViewEdit := Value;
    if FRichViewEdit <> nil then
      FRichViewEdit.RegisterCurTextStyleChangeHandler(Self, DoCurTextStyleChanged);
    if not (csDesigning in ComponentState) and not (csDestroying in ComponentState) then
      UpdateSelection;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.FocusEditor;
begin
  if RichViewEdit=nil then
    exit;
  FUpdating := True;
  try
    RichViewEdit.SetFocusSilent;
  finally
    FUpdating := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.SetActionFont(Value: TrvActionFontEx);
begin
  if Value <> FActionFont then
  begin
    {$IFDEF RICHVIEWDEF5}
    if FActionFont <> nil then
      FActionFont.RemoveFreeNotification(Self);
    {$ENDIF}
    FActionFont := Value;
    if FActionFont <> nil then
      FActionFont.FreeNotification(Self);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then begin
    if AComponent = Editor then
      Editor := nil
    else if AComponent = ActionFont then
      ActionFont := nil;
  end;
end;
{------------------------------------------------------------------------------}
destructor TCustomRVFontComboBox.Destroy;
begin
  Editor := nil;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.DoCurTextStyleChanged(Sender: TObject);
begin
  UpdateSelection;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.DoSRVActiveEditorChanged(Sender: TObject);
var NewRichViewEdit: TCustomRichViewEdit;
begin
  NewRichViewEdit := Editor.SRVGetActiveEditor(False) as TCustomRichViewEdit;
  if NewRichViewEdit<>RichViewEdit then begin
    RichViewEdit := NewRichViewEdit;
    if RichViewEdit<>nil then
      UpdateSelection;
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.DoExit;
begin
  if RichViewEdit<>nil then begin
    FNoFocus := True;
    try
      Click;
    finally
      FNoFocus := False;
    end;
  end;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVFontComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_RETURN) and (RichViewEdit<>nil) then begin
    Key := 0;
    Click;
  end;
  inherited;
end;
{============================= TRVFontSizeComboBox ==============================}
function EnumFontSizes(var EnumLogFont: TEnumLogFont;
  PTextMetric: PNewTextMetric; FontType: Integer; Data: LPARAM): Integer;
  export; stdcall;
var s: String;
    i,v,v2: Integer;
begin
  if (FontType and TRUETYPE_FONTTYPE)<>0 then begin
    TRVFontSizeComboBox(Data).Items.Add('8');
    TRVFontSizeComboBox(Data).Items.Add('9');
    TRVFontSizeComboBox(Data).Items.Add('10');
    TRVFontSizeComboBox(Data).Items.Add('11');
    TRVFontSizeComboBox(Data).Items.Add('12');
    TRVFontSizeComboBox(Data).Items.Add('14');
    TRVFontSizeComboBox(Data).Items.Add('16');
    TRVFontSizeComboBox(Data).Items.Add('18');
    TRVFontSizeComboBox(Data).Items.Add('20');
    TRVFontSizeComboBox(Data).Items.Add('22');
    TRVFontSizeComboBox(Data).Items.Add('24');
    TRVFontSizeComboBox(Data).Items.Add('26');
    TRVFontSizeComboBox(Data).Items.Add('28');
    TRVFontSizeComboBox(Data).Items.Add('36');
    TRVFontSizeComboBox(Data).Items.Add('48');
    TRVFontSizeComboBox(Data).Items.Add('72');
    Result := 0;
    end
  else begin
    v := Round((EnumLogFont.elfLogFont.lfHeight-PTextMetric.tmInternalLeading)*72 /
      TRVFontSizeComboBox(Data).PixelsPerInch);
    s := IntToStr(v);
    Result := 1;
    for i := 0 to TRVFontSizeComboBox(Data).Items.Count-1 do begin
      v2 := StrToInt(TRVFontSizeComboBox(Data).Items[i]);
      if v2=v then
        exit;
      if v2>v then begin
        TRVFontSizeComboBox(Data).Items.Insert(i,s);
        exit;
      end;
    end;
    TRVFontSizeComboBox(Data).Items.Add(s);
  end;
end;
{------------------------------------------------------------------------------}
constructor TRVFontSizeComboBox.Create(AOwner: TComponent);
begin
  inherited;
  {$IFDEF RICHVIEWDEF6}
  AutoComplete := False;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFnDEF RICHVIEWDEFXE2}
procedure TRVFontSizeComboBox.WMEraseBkgnd(var Msg: TWMEraseBkgnd);
begin
  {$IFDEF USERVKSDEVTE}
  DrawControlBackground(Self, Msg.DC);
  exit;
  {$ENDIF}
  if (Assigned(RV_IsThemeActive)) and RV_IsThemeActive and RV_IsAppThemed
    and (Style=csSimple) then begin
    RV_DrawThemeParentBackground(Handle, Msg.DC, nil);
    Msg.Result := 1
    end
  else
    inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVFontSizeComboBox.Build;
var
  DC: HDC;
  OC: TNotifyEvent;
begin
  DC := GetDC(0);
  Items.BeginUpdate;
  try
    Items.Clear;
    if FontName<>'' then begin
      PixelsPerInch := GetDeviceCaps(DC, LOGPIXELSY);
      EnumFontFamilies(DC, PChar(FontName), @EnumFontSizes, LParam(Self));
      OC := OnClick;
      OnClick := nil;
      ItemIndex := Items.IndexOf(Text);
      OnClick := OC;
      if Assigned(OnClick) then
        OnClick(Self);
    end;
  finally
    Items.EndUpdate;
    ReleaseDC(0, DC);
  end;
end;
{------------------------------------------------------------------------------}
function TRVFontSizeComboBox.IsValidChar(Key: Char): Boolean;
begin
  Result := ((Key>='0') and (Key<='9')) or
            (Key={$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator) or
            ((Key < #32) and (Key <> Chr(VK_RETURN)));
end;
{------------------------------------------------------------------------------}
procedure TRVFontSizeComboBox.KeyPress(var Key: Char);
begin
  if not IsValidChar(Key) then begin
    Key := #0;
    MessageBeep(0)
  end;
  if (Key <> #0) then
    inherited KeyPress(Key);
end;
{------------------------------------------------------------------------------}
procedure TRVFontSizeComboBox.SetFontName(const Value: TFontName);
begin
  FFontName := Value;
  Build;
end;
{------------------------------------------------------------------------------}
procedure TRVFontSizeComboBox.Click;
var SizeDouble: Integer;
begin
  if RichViewEdit=nil then begin
    inherited Click;
    exit;
  end;
  if FUpdating then
    exit;
  try
    SizeDouble := Round(StrToFloat(Text)*2);
    if ActionFont<>nil then
      with ActionFont do begin
        UserInterface := False;
        ValidProperties := [rvfimSize];
        FontSizeDouble := SizeDouble;
        ExecuteTarget(RichViewEdit);
        UserInterface := True;
      end;
  except
    Beep;
  end;
  inherited Click;
  if not FNoFocus then
    FocusEditor;
end;
{------------------------------------------------------------------------------}
procedure TRVFontSizeComboBox.UpdateSelection;
var CurStyle: TFontInfo;
begin
  if (RichViewEdit<>nil) and (RichViewEdit.Style<>nil) then begin
    CurStyle := RichViewEdit.Style.TextStyles[RichViewEdit.CurTextStyleNo];
    FUpdating := True;
    try
      FontName := CurStyle.FontName;
      Text := FloatToStr(CurStyle.SizeDouble/2);
    finally
      FUpdating := False;
    end;
  end;
end;
{============================= TRVFontCharsetComboBox ===========================}
function EnumFontCharsets(var EnumLogFont: TEnumLogFontExA;
  PTextMetric: PNewTextMetricExA; FontType: Integer; Data: LPARAM): Integer;
  export; stdcall;
var s: TRVAnsiString;
    s2: String;
    l,cs: Integer;
begin
  Result := 1;
  cs := EnumLogFont.elfLogFont.lfCharSet;
  if cs<>MAC_CHARSET then begin
    l := {$IFDEF RICHVIEWDEFXE4}AnsiStrings.{$ENDIF}StrLen(EnumLogFont.elfScript);
    SetLength(s,l);
    Move(EnumLogFont.elfScript, PRVAnsiChar(s)^,  l);
    s2 := String(s);
    for l := 0 to TRVFontCharsetComboBox(Data).Items.Count-1 do begin
      if Integer(TRVFontCharsetComboBox(Data).Items.Objects[l])=cs then
        exit;
      if AnsiCompareText(TRVFontCharsetComboBox(Data).Items[l], s2)>0 then begin
        TRVFontCharsetComboBox(Data).Items.InsertObject(l,s2,TObject(cs));
        exit;
      end;
    end;
    TRVFontCharsetComboBox(Data).Items.AddObject(s2, TObject(cs));
  end;
end;
{------------------------------------------------------------------------------}
constructor TRVFontCharsetComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDefaultCharsetCaption := '(Default)';
end;
{------------------------------------------------------------------------------}
procedure TRVFontCharsetComboBox.Build;
var
  DC: HDC;
  lf: TLogFontA;
  CurrentCharset,idx: Integer;
  OC: TNotifyEvent;
begin
  DC := GetDC(0);
  Items.BeginUpdate;
  try
    if ItemIndex=-1 then
      CurrentCharset := -1
    else
      CurrentCharset := Integer(Charsets[ItemIndex]);
    Items.Clear;
    if FontName<>'' then begin
      FillChar(lf, sizeof(lf), 0);
      lf.lfCharset  := DEFAULT_CHARSET;
      Move(PRVAnsiChar(TRVAnsiString(FontName))^, lf.lfFaceName, Length(TRVAnsiString(FontName)));
      EnumFontFamiliesExA(DC, lf, @EnumFontCharsets, LParam(Self),0);
      if AddDefaultCharset then
        Items.AddObject(DefaultCharsetCaption, TObject(DEFAULT_CHARSET));
      idx := Items.IndexOfObject(TObject(CurrentCharset));
      OC := OnClick;
      OnClick := nil;
      if (idx<>-1) then
        ItemIndex := idx
      else
        if Items.Count>0 then
          if AddDefaultCharset then
            ItemIndex := Items.Count-1
          else
            ItemIndex := 0;
      OnClick := OC;
      if Assigned(OnClick) then
        OnClick(Self);
      end
    else
      if AddDefaultCharset then
        Items.AddObject(DefaultCharsetCaption, TObject(DEFAULT_CHARSET));
  finally
    Items.EndUpdate;
    ReleaseDC(0, DC);
  end;
end;
{------------------------------------------------------------------------------}
function TRVFontCharsetComboBox.GetCharsets(Index: Integer): TFontCharset;
begin
  Result := TFontCharset(Items.Objects[Index]);
end;
{------------------------------------------------------------------------------}
function TRVFontCharsetComboBox.IndexOfCharset(
  Charset: TFontCharset): Integer;
begin
  Result := Items.IndexOfObject(TObject(Charset));
end;
{------------------------------------------------------------------------------}
procedure TRVFontCharsetComboBox.SetAddDefaultCharset(const Value: Boolean);
begin
  if FAddDefaultCharset<>Value then begin
    FAddDefaultCharset := Value;
    Build;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVFontCharsetComboBox.SetFontName(const Value: TRVALocString);
begin
  FFontName := Value;
  Build;
end;
{------------------------------------------------------------------------------}
{$IFnDEF RICHVIEWDEFXE2}
procedure TRVFontCharsetComboBox.WMEraseBkgnd(var Msg: TWMEraseBkgnd);
begin
  {$IFDEF USERVKSDEVTE}
  DrawControlBackground(Self, Msg.DC);
  exit;
  {$ENDIF}
  if (Assigned(RV_IsThemeActive)) and RV_IsThemeActive and RV_IsAppThemed
    and (Style=csSimple)  then begin
    RV_DrawThemeParentBackground(Handle, Msg.DC, nil);
    Msg.Result := 1
    end
  else
    inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVFontCharsetComboBox.Click;
var LCharset: TFontCharset;
begin
  if RichViewEdit=nil then begin
    inherited Click;
    exit;
  end;
  if FUpdating or (ItemIndex<0) then
    exit;
  try
    LCharset := Charsets[ItemIndex];
    if ActionFont<>nil then
      with ActionFont do begin
        UserInterface := False;
        ValidProperties := [rvfimCharset];
        Font.Charset := LCharset;
        ExecuteTarget(RichViewEdit);
        UserInterface := True;
      end;
  except
    Beep;
  end;
  inherited Click;
  if not FNoFocus then
    FocusEditor;
end;
{------------------------------------------------------------------------------}
procedure TRVFontCharsetComboBox.UpdateSelection;
var CurStyle: TFontInfo;
begin
  if (RichViewEdit<>nil) and (RichViewEdit.Style<>nil) then begin
    CurStyle := RichViewEdit.Style.TextStyles[RichViewEdit.CurTextStyleNo];
    FUpdating := True;
    try
      FontName := CurStyle.FontName;
      ItemIndex := IndexOfCharset(CurStyle.Charset);
    finally
      FUpdating := False;
    end;
  end;
end;
{============================== TRVFontComboBox ===============================}
constructor TRVFontComboBox.Create(AOwner: TComponent);
begin
  inherited;
  {$IFDEF RICHVIEWDEF6}
  AutoComplete := False;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TRVFontComboBox.CreateWnd;
var SL: {$IFDEF USERVTNT}TTntStringList{$ELSE}TStringList{$ENDIF};
  i: Integer;
begin
  inherited;
  SL := {$IFDEF USERVTNT}TTntStringList{$ELSE}TStringList{$ENDIF}.Create;
  SL.Assign(Screen.Fonts);
  SL.BeginUpdate;
  for i := SL.Count-1 downto 0 do
    if (SL[i]='') or (SL[i][1]='@') then
      SL.Delete(i);
  SL.EndUpdate;
  Items := SL;
  SL.Free;
end;
{------------------------------------------------------------------------------}
{$IFnDEF RICHVIEWDEFXE2}
procedure TRVFontComboBox.WMEraseBkgnd(var Msg: TWMEraseBkgnd);
begin
  {$IFDEF USERVKSDEVTE}
  DrawControlBackground(Self, Msg.DC);
  exit;
  {$ENDIF}
  if (Assigned(RV_IsThemeActive)) and RV_IsThemeActive and RV_IsAppThemed
     and (Style=csSimple) then begin
    RV_DrawThemeParentBackground(Handle, Msg.DC, nil);
    Msg.Result := 1
    end
  else
    inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVFontComboBox.Click;
var FontName: TRVALocString;
begin
  if RichViewEdit=nil then begin
    inherited;
    exit;
  end;
  if FUpdating then
    exit;
  if ItemIndex<0 then
    FontName := Text
  else
    FontName := Items[ItemIndex];
  if ItemIndex<0 then
    ItemIndex := Items.IndexOf(FontName);
  if (ItemIndex<0) or (ActionFont=nil) then
      Beep
  else
    with ActionFont do begin
      UserInterface := False;
      ValidProperties := [rvfimFontName];
      Font.Name := FontName;
      ExecuteTarget(RichViewEdit);
      UserInterface := True;
    end;
  inherited Click;
  if not FNoFocus then
    FocusEditor;
end;
{------------------------------------------------------------------------------}
procedure TRVFontComboBox.UpdateSelection;
var CurStyle: TFontInfo;
begin
  if (RichViewEdit<>nil) and (RichViewEdit.Style<>nil) then begin

    CurStyle := RichViewEdit.Style.TextStyles[RichViewEdit.CurTextStyleNo];
    FUpdating := True;
    try
      Text := CurStyle.FontName;
    finally
      FUpdating := False;
    end;
  end;
end;



end.