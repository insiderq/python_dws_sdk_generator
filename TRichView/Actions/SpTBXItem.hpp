﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'SpTBXItem.pas' rev: 27.00 (Windows)

#ifndef SptbxitemHPP
#define SptbxitemHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <TB2Item.hpp>	// Pascal unit
#include <TB2Dock.hpp>	// Pascal unit
#include <TB2Toolbar.hpp>	// Pascal unit
#include <TB2ToolWindow.hpp>	// Pascal unit
#include <SpTBXSkins.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Actions.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Sptbxitem
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TSpTBXPaintStage : unsigned char { pstPrePaint, pstPostPaint };

enum DECLSPEC_DENUM TSpTBXToolbarDisplayMode : unsigned char { tbdmSelectiveCaption, tbdmImageOnly, tbdmImageAboveCaption, tbdmTextOnly };

enum DECLSPEC_DENUM TSpTBXToolbarState : unsigned char { tstResizing, tstRightAligning, tstAnchoring };

typedef System::Set<TSpTBXToolbarState, TSpTBXToolbarState::tstResizing, TSpTBXToolbarState::tstAnchoring> TSpTBXToolbarStates;

enum DECLSPEC_DENUM TSpBorderIcon : unsigned char { briSystemMenu, briMinimize, briMaximize, briClose };

typedef System::Set<TSpBorderIcon, TSpBorderIcon::briSystemMenu, TSpBorderIcon::briClose> TSpBorderIcons;

enum DECLSPEC_DENUM TTextWrapping : unsigned char { twNone, twEndEllipsis, twPathEllipsis, twWrap };

enum DECLSPEC_DENUM TSpTBXSearchItemViewerType : unsigned char { sivtNormal, sivtInmediate, sivtInmediateSkipNonVisible };

typedef void __fastcall (__closure *TSpTBXGetImageIndexEvent)(System::TObject* Sender, Vcl::Imglist::TCustomImageList* &AImageList, int &AItemIndex);

typedef void __fastcall (__closure *TSpTBXDrawEvent)(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);

typedef void __fastcall (__closure *TSpTBXDrawImageEvent)(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, Sptbxskins::TSpTBXSkinStatesType State, const TSpTBXPaintStage PaintStage, Vcl::Imglist::TCustomImageList* &AImageList, int &AImageIndex, System::Types::TRect &ARect, bool &PaintDefault);

typedef void __fastcall (__closure *TSpTBXDrawItemEvent)(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, const TSpTBXPaintStage PaintStage, bool &PaintDefault);

typedef void __fastcall (__closure *TSpTBXDrawPosEvent)(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, int X, int Y, bool &PaintDefault);

typedef void __fastcall (__closure *TSpTBXDrawTextEvent)(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ClientAreaRect, Sptbxskins::TSpTBXSkinStatesType State, System::WideString &ACaption, System::Types::TRect &CaptionRect, unsigned &CaptionFormat, bool IsTextRotated, const TSpTBXPaintStage PaintStage, bool &PaintDefault);

typedef void __fastcall (__closure *TSpTBXDrawHintEvent)(System::TObject* Sender, Vcl::Graphics::TBitmap* AHintBitmap, System::WideString &AHint, bool &PaintDefault);

typedef void __fastcall (__closure *TSpTBXItemNotificationEvent)(System::TObject* Sender, Tb2item::TTBCustomItem* Ancestor, bool Relayed, Tb2item::TTBItemChangedAction Action, int Index, Tb2item::TTBCustomItem* Item);

typedef void __fastcall (__closure *TSpTBXRadioGroupFillStringsEvent)(System::TObject* Sender, System::Classes::TStringList* Strings);

typedef void __fastcall (__closure *TSpTBXPopupEvent)(System::TObject* Sender, Tb2item::TTBView* PopupView);

typedef System::Word TSpTBXFontSize;

class DELPHICLASS TSpTBXFontSettings;
class PASCALIMPLEMENTATION TSpTBXFontSettings : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Uitypes::TColor FColor;
	System::Uitypes::TFontName FName;
	TSpTBXFontSize FSize;
	System::Uitypes::TFontStyles FStyle;
	System::Classes::TNotifyEvent FOnChange;
	void __fastcall SetColor(System::Uitypes::TColor Value);
	void __fastcall SetName(const System::Uitypes::TFontName Value);
	void __fastcall SetSize(TSpTBXFontSize Value);
	void __fastcall SetStyle(const System::Uitypes::TFontStyles Value);
	
protected:
	void __fastcall Modified(void);
	__property System::Classes::TNotifyEvent OnChange = {read=FOnChange, write=FOnChange};
	
public:
	__fastcall TSpTBXFontSettings(void);
	void __fastcall Apply(Vcl::Graphics::TFont* AFont);
	virtual void __fastcall Assign(System::Classes::TPersistent* Src);
	
__published:
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=536870911};
	__property System::Uitypes::TFontName Name = {read=FName, write=SetName};
	__property TSpTBXFontSize Size = {read=FSize, write=SetSize, default=100};
	__property System::Uitypes::TFontStyles Style = {read=FStyle, write=SetStyle, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSpTBXFontSettings(void) { }
	
};


class DELPHICLASS TSpTBXCustomDragObject;
class PASCALIMPLEMENTATION TSpTBXCustomDragObject : public Vcl::Controls::TDragObjectEx
{
	typedef Vcl::Controls::TDragObjectEx inherited;
	
private:
	System::Uitypes::TCursor FDragCursorAccept;
	System::Uitypes::TCursor FDragCursorCancel;
	Vcl::Controls::TControl* FSourceControl;
	Tb2item::TTBCustomItem* FSourceItem;
	
protected:
	virtual System::Uitypes::TCursor __fastcall GetDragCursor(bool Accepted, int X, int Y);
	virtual void __fastcall Finished(System::TObject* Target, int X, int Y, bool Accepted);
	
public:
	__fastcall virtual TSpTBXCustomDragObject(Vcl::Controls::TControl* ASourceControl, Tb2item::TTBCustomItem* AItem);
	__property System::Uitypes::TCursor DragCursorAccept = {read=FDragCursorAccept, write=FDragCursorAccept, nodefault};
	__property System::Uitypes::TCursor DragCursorCancel = {read=FDragCursorCancel, write=FDragCursorCancel, nodefault};
	__property Tb2item::TTBCustomItem* SouceItem = {read=FSourceItem};
	__property Vcl::Controls::TControl* SourceControl = {read=FSourceControl};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TSpTBXCustomDragObject(void) { }
	
};


class DELPHICLASS TSpTBXItemDragObject;
class PASCALIMPLEMENTATION TSpTBXItemDragObject : public TSpTBXCustomDragObject
{
	typedef TSpTBXCustomDragObject inherited;
	
public:
	/* TSpTBXCustomDragObject.Create */ inline __fastcall virtual TSpTBXItemDragObject(Vcl::Controls::TControl* ASourceControl, Tb2item::TTBCustomItem* AItem) : TSpTBXCustomDragObject(ASourceControl, AItem) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TSpTBXItemDragObject(void) { }
	
};


class DELPHICLASS TSpTBXCustomItemActionLink;
class PASCALIMPLEMENTATION TSpTBXCustomItemActionLink : public Tb2item::TTBCustomItemActionLink
{
	typedef Tb2item::TTBCustomItemActionLink inherited;
	
public:
	/* TBasicActionLink.Create */ inline __fastcall virtual TSpTBXCustomItemActionLink(System::TObject* AClient) : Tb2item::TTBCustomItemActionLink(AClient) { }
	/* TBasicActionLink.Destroy */ inline __fastcall virtual ~TSpTBXCustomItemActionLink(void) { }
	
};


class DELPHICLASS TSpTBXCustomControl;
class PASCALIMPLEMENTATION TSpTBXCustomControl : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
public:
	/* TCustomControl.Create */ inline __fastcall virtual TSpTBXCustomControl(System::Classes::TComponent* AOwner) : Vcl::Controls::TCustomControl(AOwner) { }
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TSpTBXCustomControl(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXCustomControl(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXCustomItem;
class PASCALIMPLEMENTATION TSpTBXCustomItem : public Tb2item::TTBCustomItem
{
	typedef Tb2item::TTBCustomItem inherited;
	
private:
	Sptbxskins::TSpGlowDirection FCaptionGlow;
	System::Uitypes::TColor FCaptionGlowColor;
	System::Classes::TAlignment FAlignment;
	bool FAnchored;
	Vcl::Controls::TControl* FControl;
	int FCustomWidth;
	int FCustomHeight;
	TSpTBXFontSettings* FFontSettings;
	int FMargins;
	int FMinHeight;
	int FMinWidth;
	bool FStretch;
	bool FToolbarStylePopup;
	bool FToolBoxPopup;
	TTextWrapping FWrapping;
	System::Classes::TNotifyEvent FOnClosePopup;
	TSpTBXPopupEvent FOnInitPopup;
	TSpTBXDrawTextEvent FOnDrawCaption;
	TSpTBXDrawHintEvent FOnDrawHint;
	TSpTBXDrawItemEvent FOnDrawItem;
	TSpTBXDrawImageEvent FOnDrawImage;
	void __fastcall FontSettingsChanged(System::TObject* Sender);
	void __fastcall SetAlignment(const System::Classes::TAlignment Value);
	void __fastcall SetAnchored(const bool Value);
	void __fastcall SetCaptionGlow(const Sptbxskins::TSpGlowDirection Value);
	void __fastcall SetCaptionGlowColor(const System::Uitypes::TColor Value);
	void __fastcall SetControl(Vcl::Controls::TControl* const Value);
	void __fastcall SetCustomWidth(int Value);
	void __fastcall SetCustomHeight(int Value);
	void __fastcall SetFontSettings(TSpTBXFontSettings* const Value);
	void __fastcall SetMargins(int Value);
	void __fastcall SetMinHeight(const int Value);
	void __fastcall SetMinWidth(const int Value);
	void __fastcall SetStretch(const bool Value);
	void __fastcall SetToolBoxPopup(const bool Value);
	void __fastcall SetWrapping(const TTextWrapping Value);
	
protected:
	DYNAMIC void __fastcall ActionChange(System::TObject* Sender, bool CheckDefaults);
	virtual bool __fastcall DialogChar(System::Word CharCode);
	virtual void __fastcall DoDrawAdjustFont(Vcl::Graphics::TFont* AFont, Sptbxskins::TSpTBXSkinStatesType State);
	virtual void __fastcall DoDrawHint(Vcl::Graphics::TBitmap* AHintBitmap, System::WideString &AHint, bool &PaintDefault);
	virtual void __fastcall DoDrawButton(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall DoDrawCaption(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ClientAreaRect, Sptbxskins::TSpTBXSkinStatesType State, System::WideString &ACaption, System::Types::TRect &CaptionRect, unsigned &CaptionFormat, bool IsTextRotated, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall DoDrawImage(Vcl::Graphics::TCanvas* ACanvas, Sptbxskins::TSpTBXSkinStatesType State, const TSpTBXPaintStage PaintStage, Vcl::Imglist::TCustomImageList* &AImageList, int &AImageIndex, System::Types::TRect &ARect, bool &PaintDefault);
	virtual void __fastcall DoPopupShowingChanged(Tb2item::TTBPopupWindow* APopupWindow, bool IsVisible);
	DYNAMIC Tb2item::TTBCustomItemActionLinkClass __fastcall GetActionLinkClass(void);
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
	virtual Tb2item::TTBPopupWindowClass __fastcall GetPopupWindowClass(void);
	virtual void __fastcall ToggleControl(void);
	virtual void __fastcall UpdateProps(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	__property System::Classes::TAlignment Alignment = {read=FAlignment, write=SetAlignment, default=2};
	__property bool Anchored = {read=FAnchored, write=SetAnchored, default=0};
	__property Sptbxskins::TSpGlowDirection CaptionGlow = {read=FCaptionGlow, write=SetCaptionGlow, default=0};
	__property System::Uitypes::TColor CaptionGlowColor = {read=FCaptionGlowColor, write=SetCaptionGlowColor, default=65535};
	__property Vcl::Controls::TControl* Control = {read=FControl, write=SetControl};
	__property int CustomWidth = {read=FCustomWidth, write=SetCustomWidth, default=-1};
	__property int CustomHeight = {read=FCustomHeight, write=SetCustomHeight, default=-1};
	__property TSpTBXFontSettings* FontSettings = {read=FFontSettings, write=SetFontSettings};
	__property int Margins = {read=FMargins, write=SetMargins, default=0};
	__property int MinHeight = {read=FMinHeight, write=SetMinHeight, default=0};
	__property int MinWidth = {read=FMinWidth, write=SetMinWidth, default=0};
	__property bool ToolbarStylePopup = {read=FToolbarStylePopup, write=FToolbarStylePopup, default=0};
	__property bool ToolBoxPopup = {read=FToolBoxPopup, write=SetToolBoxPopup, default=0};
	__property bool Stretch = {read=FStretch, write=SetStretch, default=1};
	__property System::Classes::TNotifyEvent OnClosePopup = {read=FOnClosePopup, write=FOnClosePopup};
	__property TSpTBXPopupEvent OnInitPopup = {read=FOnInitPopup, write=FOnInitPopup};
	__property TSpTBXDrawTextEvent OnDrawCaption = {read=FOnDrawCaption, write=FOnDrawCaption};
	__property TSpTBXDrawHintEvent OnDrawHint = {read=FOnDrawHint, write=FOnDrawHint};
	__property TSpTBXDrawImageEvent OnDrawImage = {read=FOnDrawImage, write=FOnDrawImage};
	__property TSpTBXDrawItemEvent OnDrawItem = {read=FOnDrawItem, write=FOnDrawItem};
	
public:
	__fastcall virtual TSpTBXCustomItem(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXCustomItem(void);
	virtual void __fastcall Click(void);
	HIDESBASE System::WideString __fastcall GetShortCutText(void);
	virtual void __fastcall InitiateAction(void);
	void __fastcall Invalidate(void);
	
__published:
	__property Caption = {default=0};
	__property Hint = {default=0};
	__property TTextWrapping Wrapping = {read=FWrapping, write=SetWrapping, default=3};
};


class DELPHICLASS TSpTBXItemViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXItemViewer : public Tb2item::TTBItemViewer
{
	typedef Tb2item::TTBItemViewer inherited;
	
private:
	TSpTBXCustomItem* __fastcall GetItem(void);
	MESSAGE void __fastcall CMHintShow(Winapi::Messages::TMessage &Message);
	void __fastcall InternalCalcSize(Vcl::Graphics::TCanvas* const Canvas, bool CalcStretch, int &AWidth, int &AHeight);
	
protected:
	System::Types::TPoint FAnchorSize;
	int FAnchorDelta;
	bool __fastcall IsOnToolBoxPopup(void);
	virtual void __fastcall DoDrawAdjustFont(Vcl::Graphics::TFont* AFont, Sptbxskins::TSpTBXSkinStatesType State);
	virtual void __fastcall DoDrawButton(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall DoDrawCaption(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ClientAreaRect, Sptbxskins::TSpTBXSkinStatesType State, System::WideString &ACaption, System::Types::TRect &CaptionRect, unsigned &CaptionFormat, bool IsTextRotated, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall DoDrawImage(Vcl::Graphics::TCanvas* ACanvas, Sptbxskins::TSpTBXSkinStatesType State, const TSpTBXPaintStage PaintStage, Vcl::Imglist::TCustomImageList* &AImageList, int &AImageIndex, System::Types::TRect &ARect, bool &PaintDefault);
	virtual void __fastcall DoDrawHint(Vcl::Graphics::TBitmap* AHintBitmap, const System::Types::TPoint &CursorPos, System::Types::TRect &CursorRect, System::WideString &AHint, bool &PaintDefault);
	DYNAMIC bool __fastcall CaptionShown(void);
	virtual bool __fastcall GetImageShown(void);
	virtual System::Types::TSize __fastcall GetImageSize(void);
	virtual System::Types::TSize __fastcall GetRightImageSize(void);
	virtual System::Uitypes::TColor __fastcall GetTextColor(Sptbxskins::TSpTBXSkinStatesType State);
	virtual void __fastcall DrawItemImage(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, int ImgIndex);
	virtual void __fastcall DrawItemRightImage(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo);
	virtual void __fastcall CalcSize(Vcl::Graphics::TCanvas* const Canvas, int &AWidth, int &AHeight);
	void __fastcall GetTextInfo(Vcl::Graphics::TCanvas* ACanvas, Sptbxskins::TSpTBXSkinStatesType State, /* out */ Sptbxskins::TSpTBXTextInfo &TextInfo);
	virtual void __fastcall Paint(Vcl::Graphics::TCanvas* const Canvas, const System::Types::TRect &ClientAreaRect, bool IsSelected, bool IsPushed, bool UseDisabledShadow);
	virtual void __fastcall InternalMouseMove(System::Classes::TShiftState Shift, int X, int Y);
	
public:
	HIDESBASE virtual System::WideString __fastcall GetCaptionText(void);
	HIDESBASE virtual System::WideString __fastcall GetHintText(void);
	HIDESBASE bool __fastcall IsToolbarStyle(void);
	__property TSpTBXCustomItem* Item = {read=GetItem};
public:
	/* TTBItemViewer.Create */ inline __fastcall virtual TSpTBXItemViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel) : Tb2item::TTBItemViewer(AView, AItem, AGroupLevel) { }
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXItemViewer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXItem;
class PASCALIMPLEMENTATION TSpTBXItem : public TSpTBXCustomItem
{
	typedef TSpTBXCustomItem inherited;
	
__published:
	__property Action;
	__property AutoCheck = {default=0};
	__property Checked = {default=0};
	__property DisplayMode = {default=0};
	__property Enabled = {default=1};
	__property GroupIndex = {default=0};
	__property HelpContext = {default=0};
	__property ImageIndex = {default=-1};
	__property Images;
	__property InheritOptions = {default=1};
	__property MaskOptions = {default=0};
	__property Options = {default=0};
	__property RadioItem = {default=0};
	__property ShortCut = {default=0};
	__property Visible = {default=1};
	__property OnClick;
	__property OnSelect;
	__property Alignment = {default=2};
	__property Anchored = {default=0};
	__property CaptionGlow = {default=0};
	__property CaptionGlowColor = {default=65535};
	__property Control;
	__property CustomWidth = {default=-1};
	__property CustomHeight = {default=-1};
	__property FontSettings;
	__property Margins = {default=0};
	__property MinHeight = {default=0};
	__property MinWidth = {default=0};
	__property OnDrawCaption;
	__property OnDrawHint;
	__property OnDrawImage;
	__property OnDrawItem;
public:
	/* TSpTBXCustomItem.Create */ inline __fastcall virtual TSpTBXItem(System::Classes::TComponent* AOwner) : TSpTBXCustomItem(AOwner) { }
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXItem(void) { }
	
};


class DELPHICLASS TSpTBXRootItem;
class PASCALIMPLEMENTATION TSpTBXRootItem : public Tb2item::TTBRootItem
{
	typedef Tb2item::TTBRootItem inherited;
	
private:
	bool FToolBoxPopup;
	TSpTBXPopupEvent FOnInitPopup;
	System::Classes::TNotifyEvent FOnClosePopup;
	void __fastcall SetToolBoxPopup(const bool Value);
	
protected:
	virtual void __fastcall DoPopupShowingChanged(Tb2item::TTBPopupWindow* APopupWindow, bool IsVisible);
	virtual Tb2item::TTBPopupWindowClass __fastcall GetPopupWindowClass(void);
	__property bool ToolBoxPopup = {read=FToolBoxPopup, write=SetToolBoxPopup, default=0};
	
public:
	__property TSpTBXPopupEvent OnInitPopup = {read=FOnInitPopup, write=FOnInitPopup};
	__property System::Classes::TNotifyEvent OnClosePopup = {read=FOnClosePopup, write=FOnClosePopup};
public:
	/* TTBCustomItem.Create */ inline __fastcall virtual TSpTBXRootItem(System::Classes::TComponent* AOwner) : Tb2item::TTBRootItem(AOwner) { }
	/* TTBCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXRootItem(void) { }
	
};


class DELPHICLASS TSpTBXSubmenuItem;
class PASCALIMPLEMENTATION TSpTBXSubmenuItem : public TSpTBXItem
{
	typedef TSpTBXItem inherited;
	
private:
	bool FHideEmptyPopup;
	bool __fastcall GetDropdownCombo(void);
	void __fastcall SetDropdownCombo(bool Value);
	
public:
	__fastcall virtual TSpTBXSubmenuItem(System::Classes::TComponent* AOwner);
	
__published:
	__property bool DropdownCombo = {read=GetDropdownCombo, write=SetDropdownCombo, default=0};
	__property bool HideEmptyPopup = {read=FHideEmptyPopup, write=FHideEmptyPopup, default=0};
	__property LinkSubitems;
	__property SubMenuImages;
	__property ToolbarStylePopup = {default=0};
	__property ToolBoxPopup = {default=0};
	__property OnPopup;
	__property OnClosePopup;
	__property OnInitPopup;
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXSubmenuItem(void) { }
	
};


class DELPHICLASS TSpTBXColorItem;
class PASCALIMPLEMENTATION TSpTBXColorItem : public TSpTBXCustomItem
{
	typedef TSpTBXCustomItem inherited;
	
private:
	System::Uitypes::TColor FColor;
	void __fastcall SetColor(System::Uitypes::TColor Value);
	
protected:
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
	
public:
	__fastcall virtual TSpTBXColorItem(System::Classes::TComponent* AOwner);
	
__published:
	__property Action;
	__property AutoCheck = {default=0};
	__property Checked = {default=0};
	__property DisplayMode = {default=0};
	__property Enabled = {default=1};
	__property GroupIndex = {default=0};
	__property HelpContext = {default=0};
	__property InheritOptions = {default=1};
	__property MaskOptions = {default=0};
	__property Options = {default=0};
	__property ShortCut = {default=0};
	__property Visible = {default=1};
	__property OnClick;
	__property OnSelect;
	__property Alignment = {default=2};
	__property Anchored = {default=0};
	__property CaptionGlow = {default=0};
	__property CaptionGlowColor = {default=65535};
	__property Control;
	__property CustomWidth = {default=-1};
	__property CustomHeight = {default=-1};
	__property FontSettings;
	__property Margins = {default=0};
	__property MinHeight = {default=0};
	__property MinWidth = {default=0};
	__property OnDrawCaption;
	__property OnDrawHint;
	__property OnDrawImage;
	__property OnDrawItem;
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=16777215};
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXColorItem(void) { }
	
};


class DELPHICLASS TSpTBXColorItemViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXColorItemViewer : public TSpTBXItemViewer
{
	typedef TSpTBXItemViewer inherited;
	
protected:
	virtual void __fastcall DoDrawImage(Vcl::Graphics::TCanvas* ACanvas, Sptbxskins::TSpTBXSkinStatesType State, const TSpTBXPaintStage PaintStage, Vcl::Imglist::TCustomImageList* &AImageList, int &AImageIndex, System::Types::TRect &ARect, bool &PaintDefault);
	virtual bool __fastcall GetImageShown(void);
	virtual System::Types::TSize __fastcall GetImageSize(void);
public:
	/* TTBItemViewer.Create */ inline __fastcall virtual TSpTBXColorItemViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel) : TSpTBXItemViewer(AView, AItem, AGroupLevel) { }
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXColorItemViewer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXCustomLabelItem;
class PASCALIMPLEMENTATION TSpTBXCustomLabelItem : public TSpTBXCustomItem
{
	typedef TSpTBXCustomItem inherited;
	
protected:
	virtual bool __fastcall DialogChar(System::Word CharCode);
	virtual void __fastcall DoDrawButton(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
	virtual void __fastcall ToggleControl(void);
	virtual void __fastcall UpdateProps(void);
	__property Alignment = {default=0};
	
public:
	__fastcall virtual TSpTBXCustomLabelItem(System::Classes::TComponent* AOwner);
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXCustomLabelItem(void) { }
	
};


class DELPHICLASS TSpTBXLabelItemViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXLabelItemViewer : public TSpTBXItemViewer
{
	typedef TSpTBXItemViewer inherited;
	
protected:
	virtual void __fastcall CalcSize(Vcl::Graphics::TCanvas* const Canvas, int &AWidth, int &AHeight);
	virtual bool __fastcall DoExecute(void);
public:
	/* TTBItemViewer.Create */ inline __fastcall virtual TSpTBXLabelItemViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel) : TSpTBXItemViewer(AView, AItem, AGroupLevel) { }
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXLabelItemViewer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXLabelItem;
class PASCALIMPLEMENTATION TSpTBXLabelItem : public TSpTBXCustomLabelItem
{
	typedef TSpTBXCustomLabelItem inherited;
	
__published:
	__property Enabled = {default=1};
	__property ImageIndex = {default=-1};
	__property Images;
	__property InheritOptions = {default=1};
	__property MaskOptions = {default=0};
	__property Options = {default=0};
	__property Visible = {default=1};
	__property OnClick;
	__property Alignment = {default=0};
	__property Anchored = {default=0};
	__property CaptionGlow = {default=0};
	__property CaptionGlowColor = {default=65535};
	__property Control;
	__property CustomWidth = {default=-1};
	__property CustomHeight = {default=-1};
	__property FontSettings;
	__property Margins = {default=0};
	__property MinHeight = {default=0};
	__property MinWidth = {default=0};
	__property OnDrawCaption;
	__property OnDrawHint;
	__property OnDrawImage;
	__property OnDrawItem;
public:
	/* TSpTBXCustomLabelItem.Create */ inline __fastcall virtual TSpTBXLabelItem(System::Classes::TComponent* AOwner) : TSpTBXCustomLabelItem(AOwner) { }
	
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXLabelItem(void) { }
	
};


class DELPHICLASS TSpTBXSeparatorItem;
class PASCALIMPLEMENTATION TSpTBXSeparatorItem : public Tb2item::TTBSeparatorItem
{
	typedef Tb2item::TTBSeparatorItem inherited;
	
protected:
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
public:
	/* TTBSeparatorItem.Create */ inline __fastcall virtual TSpTBXSeparatorItem(System::Classes::TComponent* AOwner) : Tb2item::TTBSeparatorItem(AOwner) { }
	
public:
	/* TTBCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXSeparatorItem(void) { }
	
};


class DELPHICLASS TSpTBXSeparatorItemViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXSeparatorItemViewer : public Tb2item::TTBSeparatorItemViewer
{
	typedef Tb2item::TTBSeparatorItemViewer inherited;
	
protected:
	virtual void __fastcall CalcSize(Vcl::Graphics::TCanvas* const Canvas, int &AWidth, int &AHeight);
	bool __fastcall IsStatusBarSeparator(void);
	virtual void __fastcall Paint(Vcl::Graphics::TCanvas* const Canvas, const System::Types::TRect &ClientAreaRect, bool IsSelected, bool IsPushed, bool UseDisabledShadow);
public:
	/* TTBItemViewer.Create */ inline __fastcall virtual TSpTBXSeparatorItemViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel) : Tb2item::TTBSeparatorItemViewer(AView, AItem, AGroupLevel) { }
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXSeparatorItemViewer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXRightAlignSpacerItem;
class PASCALIMPLEMENTATION TSpTBXRightAlignSpacerItem : public TSpTBXCustomLabelItem
{
	typedef TSpTBXCustomLabelItem inherited;
	
__published:
	__property ImageIndex = {default=-1};
	__property Images;
	__property MaskOptions = {default=0};
	__property Options = {default=0};
	__property OnClick;
	__property Alignment = {default=0};
	__property CaptionGlow = {default=0};
	__property CaptionGlowColor = {default=65535};
	__property CustomWidth = {default=-1};
	__property CustomHeight = {default=-1};
	__property FontSettings;
	__property OnDrawCaption;
	__property OnDrawHint;
	__property OnDrawImage;
	__property OnDrawItem;
public:
	/* TSpTBXCustomLabelItem.Create */ inline __fastcall virtual TSpTBXRightAlignSpacerItem(System::Classes::TComponent* AOwner) : TSpTBXCustomLabelItem(AOwner) { }
	
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXRightAlignSpacerItem(void) { }
	
};


class DELPHICLASS TSpTBXRadioGroupItem;
class PASCALIMPLEMENTATION TSpTBXRadioGroupItem : public Tb2item::TTBGroupItem
{
	typedef Tb2item::TTBGroupItem inherited;
	
private:
	int FDefaultIndex;
	int FLastClickedIndex;
	System::Classes::TNotifyEvent FOnClick;
	TSpTBXRadioGroupFillStringsEvent FOnFillStrings;
	System::Classes::TNotifyEvent FOnUpdate;
	
protected:
	System::Classes::TStringList* FStrings;
	virtual void __fastcall Loaded(void);
	virtual void __fastcall ItemClickEvent(System::TObject* Sender);
	virtual void __fastcall DoClick(TSpTBXItem* AItem);
	virtual void __fastcall DoFillStrings(void);
	
public:
	__fastcall virtual TSpTBXRadioGroupItem(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXRadioGroupItem(void);
	void __fastcall Recreate(void);
	__property int DefaultIndex = {read=FDefaultIndex, write=FDefaultIndex, nodefault};
	__property int LastClickedIndex = {read=FLastClickedIndex, nodefault};
	
__published:
	__property System::Classes::TNotifyEvent OnClick = {read=FOnClick, write=FOnClick};
	__property TSpTBXRadioGroupFillStringsEvent OnFillStrings = {read=FOnFillStrings, write=FOnFillStrings};
	__property System::Classes::TNotifyEvent OnUpdate = {read=FOnUpdate, write=FOnUpdate};
};


class DELPHICLASS TSpTBXSkinGroupItem;
class PASCALIMPLEMENTATION TSpTBXSkinGroupItem : public TSpTBXRadioGroupItem
{
	typedef TSpTBXRadioGroupItem inherited;
	
private:
	System::Classes::TNotifyEvent FOnSkinChange;
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	
protected:
	virtual void __fastcall DoClick(TSpTBXItem* AItem);
	virtual void __fastcall DoSkinChange(void);
	virtual void __fastcall DoFillStrings(void);
	
public:
	__fastcall virtual TSpTBXSkinGroupItem(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXSkinGroupItem(void);
	
__published:
	__property System::Classes::TNotifyEvent OnSkinChange = {read=FOnSkinChange, write=FOnSkinChange};
};


class DELPHICLASS TSpTBXSystemMenuItem;
class PASCALIMPLEMENTATION TSpTBXSystemMenuItem : public TSpTBXCustomItem
{
	typedef TSpTBXCustomItem inherited;
	
private:
	bool FMDISystemMenu;
	bool FShowSize;
	void __fastcall CommandClick(System::TObject* Sender);
	
protected:
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
	Vcl::Forms::TCustomForm* __fastcall GetSystemMenuParentForm(void);
	
public:
	__fastcall virtual TSpTBXSystemMenuItem(System::Classes::TComponent* AOwner);
	virtual void __fastcall Click(void);
	
__published:
	__property bool MDISystemMenu = {read=FMDISystemMenu, write=FMDISystemMenu, default=0};
	__property bool ShowSize = {read=FShowSize, write=FShowSize, default=1};
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXSystemMenuItem(void) { }
	
};


class DELPHICLASS TSpTBXSystemMenuItemViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXSystemMenuItemViewer : public TSpTBXItemViewer
{
	typedef TSpTBXItemViewer inherited;
	
protected:
	virtual void __fastcall CalcSize(Vcl::Graphics::TCanvas* const Canvas, int &AWidth, int &AHeight);
	virtual void __fastcall Paint(Vcl::Graphics::TCanvas* const Canvas, const System::Types::TRect &ClientAreaRect, bool IsSelected, bool IsPushed, bool UseDisabledShadow);
public:
	/* TTBItemViewer.Create */ inline __fastcall virtual TSpTBXSystemMenuItemViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel) : TSpTBXItemViewer(AView, AItem, AGroupLevel) { }
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXSystemMenuItemViewer(void) { }
	
};

#pragma pack(pop)

typedef System::Int8 TSpTBXRowColCount;

typedef void __fastcall (__closure *TSpTBXTPGetCellHint)(System::TObject* Sender, int ACol, int ARow, System::WideString &AHint);

typedef void __fastcall (__closure *TSpTBXTPDrawCellImage)(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int ACol, int ARow, bool Selected, bool HotTrack, bool Enabled);

typedef void __fastcall (__closure *TSpTBXTPCellClick)(System::TObject* Sender, int ACol, int ARow, bool &Allow);

typedef void __fastcall (__closure *TSpTBXCPGetColorInfo)(System::TObject* Sender, int ACol, int ARow, System::Uitypes::TColor &Color, System::WideString &Name);

class DELPHICLASS TSpTBXCustomToolPalette;
class PASCALIMPLEMENTATION TSpTBXCustomToolPalette : public TSpTBXCustomItem
{
	typedef TSpTBXCustomItem inherited;
	
private:
	bool FCustomImages;
	TSpTBXRowColCount FColCount;
	TSpTBXRowColCount FRowCount;
	System::Types::TPoint FSelectedCell;
	System::Classes::TNotifyEvent FOnChange;
	TSpTBXTPCellClick FOnCellClick;
	TSpTBXTPDrawCellImage FOnDrawCellImage;
	TSpTBXTPGetCellHint FOnGetCellHint;
	void __fastcall SetSelectedCell(const System::Types::TPoint &Value);
	
protected:
	virtual bool __fastcall DoCellClick(int ACol, int ARow);
	virtual void __fastcall DoChange(void);
	virtual void __fastcall DoDrawCellImage(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int ACol, int ARow, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo);
	virtual void __fastcall DoGetCellHint(int ACol, int ARow, System::WideString &AHint);
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
	virtual void __fastcall HandleClickCell(int ACol, int ARow);
	virtual void __fastcall SetColCount(TSpTBXRowColCount Value);
	virtual void __fastcall SetRowCount(TSpTBXRowColCount Value);
	__property bool CustomImages = {read=FCustomImages, write=FCustomImages, nodefault};
	__property TSpTBXRowColCount ColCount = {read=FColCount, write=SetColCount, default=1};
	__property TSpTBXRowColCount RowCount = {read=FRowCount, write=SetRowCount, default=1};
	__property System::Types::TPoint SelectedCell = {read=FSelectedCell, write=SetSelectedCell};
	__property System::Classes::TNotifyEvent OnChange = {read=FOnChange, write=FOnChange};
	__property TSpTBXTPCellClick OnCellClick = {read=FOnCellClick, write=FOnCellClick};
	__property TSpTBXTPDrawCellImage OnDrawCellImage = {read=FOnDrawCellImage, write=FOnDrawCellImage};
	__property TSpTBXTPGetCellHint OnGetCellHint = {read=FOnGetCellHint, write=FOnGetCellHint};
	
public:
	__fastcall virtual TSpTBXCustomToolPalette(System::Classes::TComponent* AOwner);
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXCustomToolPalette(void) { }
	
};


class DELPHICLASS TSpTBXToolPalette;
class PASCALIMPLEMENTATION TSpTBXToolPalette : public TSpTBXCustomToolPalette
{
	typedef TSpTBXCustomToolPalette inherited;
	
public:
	__property SelectedCell;
	
__published:
	__property ColCount = {default=1};
	__property HelpContext = {default=0};
	__property Images;
	__property Options = {default=0};
	__property RowCount = {default=1};
	__property Visible = {default=1};
	__property OnChange;
	__property OnCellClick;
	__property OnDrawCellImage;
	__property OnGetCellHint;
public:
	/* TSpTBXCustomToolPalette.Create */ inline __fastcall virtual TSpTBXToolPalette(System::Classes::TComponent* AOwner) : TSpTBXCustomToolPalette(AOwner) { }
	
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXToolPalette(void) { }
	
};


class DELPHICLASS TSpTBXToolViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXToolViewer : public TSpTBXItemViewer
{
	typedef TSpTBXItemViewer inherited;
	
private:
	int FCellHeight;
	int FCellWidth;
	int FColCount;
	int FRowCount;
	System::Types::TPoint FHotCell;
	HIDESBASE TSpTBXCustomToolPalette* __fastcall GetItem(void);
	
protected:
	int FIndent;
	bool FMouseIsDown;
	virtual void __fastcall CalcCellSize(Vcl::Graphics::TCanvas* ACanvas, int &AWidth, int &AHeight);
	virtual void __fastcall CalcSize(Vcl::Graphics::TCanvas* const Canvas, int &AWidth, int &AHeight);
	int __fastcall GetImageIndex(int Col, int Row);
	virtual System::Types::TSize __fastcall GetImageSize(void);
	virtual bool __fastcall GetImageShown(void);
	bool __fastcall GetCellAt(int X, int Y, /* out */ int &Col, /* out */ int &Row);
	virtual System::Types::TRect __fastcall GetCellRect(const System::Types::TRect &ClientAreaRect, int Col, int Row);
	System::WideString __fastcall GetCellHint(int Col, int Row);
	virtual void __fastcall DoDrawHint(Vcl::Graphics::TBitmap* AHintBitmap, const System::Types::TPoint &CursorPos, System::Types::TRect &CursorRect, System::WideString &AHint, bool &PaintDefault);
	virtual void __fastcall DrawCellImage(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int Col, int Row, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo);
	virtual void __fastcall Entering(void);
	void __fastcall InvalidateCell(int ACol, int ARow);
	virtual bool __fastcall IsCellVisible(const System::Types::TPoint &Cell);
	virtual void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	virtual void __fastcall MouseDown(System::Classes::TShiftState Shift, int X, int Y, bool &MouseDownOnMenu);
	virtual void __fastcall MouseMove(int X, int Y);
	virtual void __fastcall MouseUp(int X, int Y, bool MouseWasDownOnMenu);
	virtual void __fastcall Paint(Vcl::Graphics::TCanvas* const Canvas, const System::Types::TRect &ClientAreaRect, bool IsSelected, bool IsPushed, bool UseDisabledShadow);
	
public:
	__fastcall virtual TSpTBXToolViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel);
	__property TSpTBXCustomToolPalette* Item = {read=GetItem};
public:
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXToolViewer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXColorPalette;
class PASCALIMPLEMENTATION TSpTBXColorPalette : public TSpTBXCustomToolPalette
{
	typedef TSpTBXCustomToolPalette inherited;
	
private:
	System::Uitypes::TColor FColor;
	bool FCustomColors;
	TSpTBXCPGetColorInfo FOnGetColor;
	void __fastcall SetColor(System::Uitypes::TColor Value);
	void __fastcall SetCustomColors(const bool Value);
	
protected:
	virtual void __fastcall DoChange(void);
	virtual void __fastcall DoGetCellHint(int ACol, int ARow, System::WideString &AHint);
	virtual void __fastcall DoDrawCellImage(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int ACol, int ARow, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo);
	System::Uitypes::TColor __fastcall GetCellColor(int ACol, int ARow);
	void __fastcall GetCellInfo(int ACol, int ARow, /* out */ System::Uitypes::TColor &AColor, /* out */ System::WideString &AName);
	virtual void __fastcall SetColCount(TSpTBXRowColCount Value);
	virtual void __fastcall SetRowCount(TSpTBXRowColCount Value);
	
public:
	__fastcall virtual TSpTBXColorPalette(System::Classes::TComponent* AOwner);
	System::Types::TPoint __fastcall FindCell(System::Uitypes::TColor AColor);
	
__published:
	__property bool CustomColors = {read=FCustomColors, write=SetCustomColors, default=0};
	__property ColCount = {default=8};
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=536870911};
	__property HelpContext = {default=0};
	__property InheritOptions = {default=1};
	__property MaskOptions = {default=0};
	__property Options = {default=128};
	__property RowCount = {default=5};
	__property Visible = {default=1};
	__property OnChange;
	__property OnCellClick;
	__property OnGetCellHint;
	__property TSpTBXCPGetColorInfo OnGetColor = {read=FOnGetColor, write=FOnGetColor};
public:
	/* TSpTBXCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXColorPalette(void) { }
	
};


class DELPHICLASS TSpTBXItemCache;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXItemCache : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	Tb2dock::TTBDock* FDock;
	System::Classes::TComponentName FName;
	Tb2item::TTBCustomItem* FItem;
	int FWidth;
	int FHeight;
	int FParentWidth;
	int FParentHeight;
	System::Classes::TComponentName __fastcall GetName(void);
	
public:
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	__property Tb2item::TTBCustomItem* Item = {read=FItem, write=FItem};
	
__published:
	__property Tb2dock::TTBDock* Dock = {read=FDock, write=FDock};
	__property System::Classes::TComponentName Name = {read=GetName, write=FName};
	__property int Width = {read=FWidth, write=FWidth, default=0};
	__property int Height = {read=FHeight, write=FHeight, default=0};
	__property int ParentWidth = {read=FParentWidth, write=FParentWidth, default=0};
	__property int ParentHeight = {read=FParentHeight, write=FParentHeight, default=0};
public:
	/* TCollectionItem.Create */ inline __fastcall virtual TSpTBXItemCache(System::Classes::TCollection* Collection) : System::Classes::TCollectionItem(Collection) { }
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TSpTBXItemCache(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXItemCacheCollection;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXItemCacheCollection : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TSpTBXItemCache* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TSpTBXItemCache* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TSpTBXItemCache* const Value);
	
public:
	HIDESBASE virtual int __fastcall Add(Tb2item::TTBCustomItem* AItem);
	int __fastcall IndexOf(Tb2item::TTBCustomItem* AItem);
	__property TSpTBXItemCache* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Create */ inline __fastcall TSpTBXItemCacheCollection(System::Classes::TCollectionItemClass ItemClass) : System::Classes::TCollection(ItemClass) { }
	/* TCollection.Destroy */ inline __fastcall virtual ~TSpTBXItemCacheCollection(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXDock;
class PASCALIMPLEMENTATION TSpTBXDock : public Tb2dock::TTBDock
{
	typedef Tb2dock::TTBDock inherited;
	
private:
	bool FMoving;
	bool FResizing;
	int FPrevWidth;
	int FPrevHeight;
	TSpTBXDrawEvent FOnDrawBackground;
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	HIDESBASE MESSAGE void __fastcall WMMove(Winapi::Messages::TWMMove &Message);
	HIDESBASE MESSAGE void __fastcall WMSize(Winapi::Messages::TWMSize &Message);
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	
protected:
	virtual bool __fastcall CanResize(int &NewWidth, int &NewHeight);
	virtual void __fastcall DrawBackground(HDC DC, const System::Types::TRect &DrawRect);
	virtual void __fastcall DoDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	DYNAMIC void __fastcall Resize(void);
	virtual bool __fastcall UsingBackground(void);
	bool __fastcall UsingBitmap(void);
	
public:
	__fastcall virtual TSpTBXDock(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXDock(void);
	__property int PrevWidth = {read=FPrevWidth, nodefault};
	__property int PrevHeight = {read=FPrevHeight, nodefault};
	
__published:
	__property Color = {default=536870911};
	__property OnCanResize;
	__property TSpTBXDrawEvent OnDrawBackground = {read=FOnDrawBackground, write=FOnDrawBackground};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXDock(HWND ParentWindow) : Tb2dock::TTBDock(ParentWindow) { }
	
};


typedef System::TMetaClass* TSpTBXDockClass;

class DELPHICLASS TSpTBXToolbarView;
class PASCALIMPLEMENTATION TSpTBXToolbarView : public Tb2toolbar::TTBToolbarView
{
	typedef Tb2toolbar::TTBToolbarView inherited;
	
private:
	int FMaxSize;
	void __fastcall SetMaxSize(const int Value);
	
protected:
	int FTallestItemSize;
	int FUpdating;
	virtual void __fastcall DoUpdatePositions(System::Types::TPoint &ASize);
	
public:
	__fastcall virtual TSpTBXToolbarView(System::Classes::TComponent* AOwner, Tb2item::TTBView* AParentView, Tb2item::TTBCustomItem* AParentItem, Vcl::Controls::TWinControl* AWindow, bool AIsToolbar, bool ACustomizing, bool AUsePriorityList);
	HIDESBASE virtual void __fastcall BeginUpdate(void);
	HIDESBASE virtual void __fastcall EndUpdate(void);
	bool __fastcall IsUpdating(void);
	__property int MaxSize = {read=FMaxSize, write=SetMaxSize, nodefault};
public:
	/* TTBView.Destroy */ inline __fastcall virtual ~TSpTBXToolbarView(void) { }
	
};


class DELPHICLASS TSpTBXToolbar;
class PASCALIMPLEMENTATION TSpTBXToolbar : public Tb2toolbar::TTBCustomToolbar
{
	typedef Tb2toolbar::TTBCustomToolbar inherited;
	
private:
	bool FChevronVertical;
	bool FCompoundToolbar;
	bool FCustomizable;
	int FCustomizingCount;
	int FItemMovingCount;
	TSpTBXToolbarDisplayMode FDisplayMode;
	System::Types::TRect FLastDropMark;
	int FLastSelectableWidth;
	bool FMenuBar;
	TSpTBXDrawEvent FOnDrawBackground;
	TSpTBXItemNotificationEvent FOnItemNotification;
	void __fastcall SetDisplayMode(const TSpTBXToolbarDisplayMode Value);
	int __fastcall GetMaxSize(void);
	void __fastcall SetMaxSize(const int Value);
	HIDESBASE void __fastcall SetMenuBar(const bool Value);
	HIDESBASE Tb2item::TTBControlItem* __fastcall CreateWrapper(int Index, Vcl::Controls::TControl* Ctl);
	Tb2item::TTBControlItem* __fastcall IsAnchoredControlItem(Tb2item::TTBCustomItem* Item);
	HIDESBASE MESSAGE void __fastcall CMControlChange(Vcl::Controls::TCMControlChange &Message);
	HIDESBASE MESSAGE void __fastcall CMDialogChar(Winapi::Messages::TWMKey &Message);
	HIDESBASE MESSAGE void __fastcall CMMouseleave(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall CMHintShow(Vcl::Controls::TCMHintShow &Message);
	HIDESBASE MESSAGE void __fastcall CMTextChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	HIDESBASE MESSAGE void __fastcall WMSize(Winapi::Messages::TWMSize &Message);
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	void __fastcall SetCustomizable(const bool Value);
	
protected:
	Tb2item::TTBItemViewer* FBeginDragIV;
	TSpTBXItemCacheCollection* FAnchoredControlItems;
	TSpTBXToolbarStates FState;
	int FDefaultToolbarBorderSize;
	DYNAMIC void __fastcall Resize(void);
	virtual void __fastcall AnchorItems(bool UpdateControlItems = true);
	virtual void __fastcall RightAlignItems(void);
	virtual void __fastcall DrawNCArea(const bool DrawToDC, const HDC ADC, const HRGN Clip);
	virtual System::Uitypes::TColor __fastcall GetItemsTextColor(Sptbxskins::TSpTBXSkinStatesType State);
	virtual void __fastcall InternalDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool PaintOnNCArea, bool PaintBorders = true);
	virtual void __fastcall DoDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	DYNAMIC Tb2toolbar::TTBChevronItemClass __fastcall GetChevronItemClass(void);
	DYNAMIC Tb2dock::TTBFloatingWindowParentClass __fastcall GetFloatingWindowParentClass(void);
	virtual int __fastcall GetRightAlignMargin(void);
	DYNAMIC Tb2toolbar::TTBToolbarViewClass __fastcall GetViewClass(void);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	virtual bool __fastcall CanDragCustomize(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall DoStartDrag(Vcl::Controls::TDragObject* &DragObject);
	DYNAMIC void __fastcall DragOver(System::TObject* Source, int X, int Y, System::Uitypes::TDragState State, bool &Accept);
	virtual bool __fastcall CanItemClick(Tb2item::TTBCustomItem* Item, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall DoItemClick(Tb2item::TTBCustomItem* Item, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall DoItemNotification(Tb2item::TTBCustomItem* Ancestor, bool Relayed, Tb2item::TTBItemChangedAction Action, int Index, Tb2item::TTBCustomItem* Item);
	__property bool CompoundToolbar = {read=FCompoundToolbar, write=FCompoundToolbar, nodefault};
	
public:
	__fastcall virtual TSpTBXToolbar(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXToolbar(void);
	DYNAMIC void __fastcall DragDrop(System::TObject* Source, int X, int Y);
	DYNAMIC void __fastcall ReadPositionData(const Tb2dock::TTBReadPositionData &Data);
	DYNAMIC void __fastcall WritePositionData(const Tb2dock::TTBWritePositionData &Data);
	HIDESBASE void __fastcall BeginUpdate(void);
	HIDESBASE void __fastcall EndUpdate(void);
	bool __fastcall IsUpdating(void);
	void __fastcall BeginCustomize(void);
	void __fastcall EndCustomize(void);
	void __fastcall BeginItemMove(void);
	void __fastcall EndItemMove(void);
	virtual System::Types::TPoint __fastcall GetFloatingBorderSize(void);
	bool __fastcall IsCustomizing(void);
	bool __fastcall IsItemMoving(void);
	bool __fastcall IsVertical(void);
	__property int DefaultToolbarBorderSize = {read=FDefaultToolbarBorderSize, nodefault};
	__property int MaxSize = {read=GetMaxSize, write=SetMaxSize, default=-1};
	
__published:
	__property ActivateParent = {default=1};
	__property Align = {default=0};
	__property AutoResize = {default=1};
	__property BorderStyle = {default=1};
	__property ChevronHint = {default=0};
	__property ChevronMoveItems = {default=1};
	__property ChevronPriorityForNewItems = {default=0};
	__property CloseButton = {default=1};
	__property CloseButtonWhenDocked = {default=0};
	__property CurrentDock;
	__property DefaultDock;
	__property DockableTo = {default=15};
	__property DockMode = {default=0};
	__property DockPos = {default=-1};
	__property DockRow = {default=0};
	__property DragHandleStyle = {default=2};
	__property FloatingMode = {default=0};
	__property Font;
	__property FullSize = {default=0};
	__property HideWhenInactive = {default=1};
	__property Images;
	__property Items;
	__property LastDock;
	__property LinkSubitems;
	__property Options = {default=0};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ProcessShortCuts = {default=0};
	__property Resizable = {default=1};
	__property ShowCaption = {default=1};
	__property ShowHint;
	__property ShrinkMode = {default=2};
	__property SmoothDrag = {default=1};
	__property Stretch = {default=0};
	__property SystemFont = {default=1};
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property UpdateActions = {default=1};
	__property UseLastDock = {default=1};
	__property Visible = {default=1};
	__property Color = {default=536870911};
	__property OnClose;
	__property OnCloseQuery;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMove;
	__property OnRecreated;
	__property OnRecreating;
	__property OnDockChanged;
	__property OnDockChanging;
	__property OnDockChangingHidden;
	__property OnResize;
	__property OnShortCut;
	__property OnVisibleChanged;
	__property Caption = {default=0};
	__property Hint = {default=0};
	__property bool ChevronVertical = {read=FChevronVertical, write=FChevronVertical, default=1};
	__property bool Customizable = {read=FCustomizable, write=SetCustomizable, default=1};
	__property TSpTBXToolbarDisplayMode DisplayMode = {read=FDisplayMode, write=SetDisplayMode, default=0};
	__property bool MenuBar = {read=FMenuBar, write=SetMenuBar, default=0};
	__property TSpTBXDrawEvent OnDrawBackground = {read=FOnDrawBackground, write=FOnDrawBackground};
	__property TSpTBXItemNotificationEvent OnItemNotification = {read=FOnItemNotification, write=FOnItemNotification};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXToolbar(HWND ParentWindow) : Tb2toolbar::TTBCustomToolbar(ParentWindow) { }
	
};


typedef System::TMetaClass* TSpTBXToolbarClass;

class DELPHICLASS TSpTBXCustomToolWindow;
class PASCALIMPLEMENTATION TSpTBXCustomToolWindow : public Tb2dock::TTBCustomDockableWindow
{
	typedef Tb2dock::TTBCustomDockableWindow inherited;
	
private:
	int FMinClientWidth;
	int FMinClientHeight;
	int FMaxClientWidth;
	int FMaxClientHeight;
	TSpTBXDrawEvent FOnDrawBackground;
	int __fastcall GetClientAreaWidth(void);
	void __fastcall SetClientAreaWidth(int Value);
	int __fastcall GetClientAreaHeight(void);
	void __fastcall SetClientAreaHeight(int Value);
	HIDESBASE MESSAGE void __fastcall CMTextChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMWindowPosChanged(Winapi::Messages::TWMWindowPosMsg &Message);
	
protected:
	System::Types::TSize FBarSize;
	int FDefaultToolbarBorderSize;
	DYNAMIC Tb2dock::TTBFloatingWindowParentClass __fastcall GetFloatingWindowParentClass(void);
	virtual System::Types::TPoint __fastcall CalcSize(Tb2dock::TTBDock* ADock);
	virtual System::Types::TPoint __fastcall DoArrange(bool CanMoveControls, Tb2dock::TTBDockType PreviousDockType, bool NewFloating, Tb2dock::TTBDock* NewDock);
	virtual void __fastcall GetBaseSize(System::Types::TPoint &ASize);
	virtual void __fastcall GetMinMaxSize(int &AMinClientWidth, int &AMinClientHeight, int &AMaxClientWidth, int &AMaxClientHeight);
	virtual void __fastcall SetClientAreaSize(int AWidth, int AHeight);
	virtual void __fastcall SizeChanging(const int AWidth, const int AHeight);
	virtual void __fastcall Paint(void);
	virtual void __fastcall DrawNCArea(const bool DrawToDC, const HDC ADC, const HRGN Clip);
	virtual void __fastcall InternalDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool PaintOnNCArea, bool PaintBorders = true);
	virtual void __fastcall DoDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	__property int ClientAreaHeight = {read=GetClientAreaHeight, write=SetClientAreaHeight, nodefault};
	__property int ClientAreaWidth = {read=GetClientAreaWidth, write=SetClientAreaWidth, nodefault};
	__property int MaxClientHeight = {read=FMaxClientHeight, write=FMaxClientHeight, default=0};
	__property int MaxClientWidth = {read=FMaxClientWidth, write=FMaxClientWidth, default=0};
	__property int MinClientHeight = {read=FMinClientHeight, write=FMinClientHeight, default=32};
	__property int MinClientWidth = {read=FMinClientWidth, write=FMinClientWidth, default=32};
	__property TSpTBXDrawEvent OnDrawBackground = {read=FOnDrawBackground, write=FOnDrawBackground};
	
public:
	__fastcall virtual TSpTBXCustomToolWindow(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXCustomToolWindow(void);
	virtual System::Types::TPoint __fastcall GetFloatingBorderSize(void);
	virtual void __fastcall InvalidateBackground(bool InvalidateChildren = true);
	bool __fastcall IsVertical(void);
	DYNAMIC void __fastcall ReadPositionData(const Tb2dock::TTBReadPositionData &Data);
	DYNAMIC void __fastcall WritePositionData(const Tb2dock::TTBWritePositionData &Data);
	__property int DefaultToolbarBorderSize = {read=FDefaultToolbarBorderSize, nodefault};
	
__published:
	__property Caption = {default=0};
	__property Color = {default=536870911};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXCustomToolWindow(HWND ParentWindow) : Tb2dock::TTBCustomDockableWindow(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXToolWindow;
class PASCALIMPLEMENTATION TSpTBXToolWindow : public TSpTBXCustomToolWindow
{
	typedef TSpTBXCustomToolWindow inherited;
	
__published:
	__property ActivateParent = {default=1};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BorderStyle = {default=1};
	__property Caption = {default=0};
	__property Color = {default=536870911};
	__property CloseButton = {default=1};
	__property CloseButtonWhenDocked = {default=0};
	__property CurrentDock;
	__property DefaultDock;
	__property DockableTo = {default=15};
	__property DockMode = {default=0};
	__property DockPos = {default=-1};
	__property DockRow = {default=0};
	__property DragHandleStyle = {default=2};
	__property FloatingMode = {default=0};
	__property Font;
	__property FullSize = {default=0};
	__property HideWhenInactive = {default=1};
	__property LastDock;
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property Resizable = {default=1};
	__property ShowCaption = {default=1};
	__property ShowHint;
	__property Stretch = {default=0};
	__property SmoothDrag = {default=1};
	__property TabOrder = {default=-1};
	__property UseLastDock = {default=1};
	__property Visible = {default=1};
	__property OnClose;
	__property OnCloseQuery;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnDockChanged;
	__property OnDockChanging;
	__property OnDockChangingHidden;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMove;
	__property OnRecreated;
	__property OnRecreating;
	__property OnResize;
	__property OnVisibleChanged;
	__property ClientAreaHeight;
	__property ClientAreaWidth;
	__property MaxClientHeight = {default=0};
	__property MaxClientWidth = {default=0};
	__property MinClientHeight = {default=32};
	__property MinClientWidth = {default=32};
	__property OnDrawBackground;
public:
	/* TSpTBXCustomToolWindow.Create */ inline __fastcall virtual TSpTBXToolWindow(System::Classes::TComponent* AOwner) : TSpTBXCustomToolWindow(AOwner) { }
	/* TSpTBXCustomToolWindow.Destroy */ inline __fastcall virtual ~TSpTBXToolWindow(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXToolWindow(HWND ParentWindow) : TSpTBXCustomToolWindow(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXFloatingWindowParent;
class PASCALIMPLEMENTATION TSpTBXFloatingWindowParent : public Tb2dock::TTBFloatingWindowParent
{
	typedef Tb2dock::TTBFloatingWindowParent inherited;
	
private:
	bool FCloseButtonHover;
	bool FCloseOnAltF4;
	void __fastcall UpdateDwmNCSize(void);
	HIDESBASE MESSAGE void __fastcall CMMouseLeave(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMActivateApp(Winapi::Messages::TWMActivateApp &Message);
	HIDESBASE MESSAGE void __fastcall WMClose(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMNCMouseLeave(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMNCMouseMove(Winapi::Messages::TWMNCHitMessage &Message);
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	
protected:
	void __fastcall CancelNCHover(void);
	virtual void __fastcall CreateWnd(void);
	DYNAMIC void __fastcall DrawNCArea(const bool DrawToDC, const HDC ADC, const HRGN Clip, Tb2dock::TTBToolWindowNCRedrawWhat RedrawWhat);
	void __fastcall RedrawCloseButton(void);
	DYNAMIC void __fastcall VisibleChanging(void);
	__property bool CloseButtonHover = {read=FCloseButtonHover, nodefault};
	
public:
	__fastcall virtual TSpTBXFloatingWindowParent(System::Classes::TComponent* AOwner, int Dummy);
	__fastcall virtual ~TSpTBXFloatingWindowParent(void);
	__property bool CloseOnAltF4 = {read=FCloseOnAltF4, write=FCloseOnAltF4, nodefault};
public:
	/* TCustomForm.Create */ inline __fastcall virtual TSpTBXFloatingWindowParent(System::Classes::TComponent* AOwner) : Tb2dock::TTBFloatingWindowParent(AOwner) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXFloatingWindowParent(HWND ParentWindow) : Tb2dock::TTBFloatingWindowParent(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXPopupWindow;
class PASCALIMPLEMENTATION TSpTBXPopupWindow : public Tb2item::TTBPopupWindow
{
	typedef Tb2item::TTBPopupWindow inherited;
	
private:
	bool FPaintingClientArea;
	System::Types::TSize FMaximumImageSize;
	bool __fastcall CanDrawGutter(void);
	HIDESBASE MESSAGE void __fastcall CMHintShow(Vcl::Controls::TCMHintShow &Message);
	HIDESBASE MESSAGE void __fastcall CMShowingchanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	HIDESBASE MESSAGE void __fastcall WMNCPaint(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMPrint(Winapi::Messages::TMessage &Message);
	
protected:
	DYNAMIC Tb2item::TTBViewClass __fastcall GetViewClass(void);
	virtual void __fastcall DoPopupShowingChanged(bool IsVisible);
	virtual void __fastcall PaintBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
	
public:
	__fastcall virtual TSpTBXPopupWindow(System::Classes::TComponent* AOwner, Tb2item::TTBView* const AParentView, Tb2item::TTBCustomItem* const AItem, const bool ACustomizing);
	__fastcall virtual ~TSpTBXPopupWindow(void);
	__property System::Types::TSize MaximumImageSize = {read=FMaximumImageSize};
public:
	/* TCustomControl.Create */ inline __fastcall virtual TSpTBXPopupWindow(System::Classes::TComponent* AOwner) : Tb2item::TTBPopupWindow(AOwner) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXPopupWindow(HWND ParentWindow) : Tb2item::TTBPopupWindow(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXPopupWindowView;
class PASCALIMPLEMENTATION TSpTBXPopupWindowView : public Tb2item::TTBPopupView
{
	typedef Tb2item::TTBPopupView inherited;
	
protected:
	virtual void __fastcall AutoSize(int AWidth, int AHeight);
	
public:
	void __fastcall SetIsToolbar(const bool Value);
	
__published:
	__property IsToolbar;
public:
	/* TTBView.Create */ inline __fastcall virtual TSpTBXPopupWindowView(System::Classes::TComponent* AOwner, Tb2item::TTBView* AParentView, Tb2item::TTBCustomItem* AParentItem, Vcl::Controls::TWinControl* AWindow, bool AIsToolbar, bool ACustomizing, bool AUsePriorityList) : Tb2item::TTBPopupView(AOwner, AParentView, AParentItem, AWindow, AIsToolbar, ACustomizing, AUsePriorityList) { }
	/* TTBView.Destroy */ inline __fastcall virtual ~TSpTBXPopupWindowView(void) { }
	
};


class DELPHICLASS TSpTBXChevronItem;
class PASCALIMPLEMENTATION TSpTBXChevronItem : public Tb2toolbar::TTBChevronItem
{
	typedef Tb2toolbar::TTBChevronItem inherited;
	
protected:
	virtual Tb2item::TTBPopupWindowClass __fastcall GetPopupWindowClass(void);
	
public:
	virtual Tb2item::TTBItemViewerClass __fastcall GetItemViewerClass(Tb2item::TTBView* AView);
public:
	/* TTBChevronItem.Create */ inline __fastcall virtual TSpTBXChevronItem(System::Classes::TComponent* AOwner) : Tb2toolbar::TTBChevronItem(AOwner) { }
	
public:
	/* TTBCustomItem.Destroy */ inline __fastcall virtual ~TSpTBXChevronItem(void) { }
	
};


class DELPHICLASS TSpTBXChevronItemViewer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXChevronItemViewer : public Tb2item::TTBItemViewer
{
	typedef Tb2item::TTBItemViewer inherited;
	
protected:
	virtual void __fastcall Paint(Vcl::Graphics::TCanvas* const Canvas, const System::Types::TRect &ClientAreaRect, bool IsSelected, bool IsPushed, bool UseDisabledShadow);
	
public:
	virtual System::Uitypes::TColor __fastcall GetTextColor(Sptbxskins::TSpTBXSkinStatesType State);
public:
	/* TTBItemViewer.Create */ inline __fastcall virtual TSpTBXChevronItemViewer(Tb2item::TTBView* AView, Tb2item::TTBCustomItem* AItem, int AGroupLevel) : Tb2item::TTBItemViewer(AView, AItem, AGroupLevel) { }
	/* TTBItemViewer.Destroy */ inline __fastcall virtual ~TSpTBXChevronItemViewer(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXChevronPopupWindow;
class PASCALIMPLEMENTATION TSpTBXChevronPopupWindow : public TSpTBXPopupWindow
{
	typedef TSpTBXPopupWindow inherited;
	
private:
	HIDESBASE MESSAGE void __fastcall CMColorChanged(Winapi::Messages::TMessage &Message);
public:
	/* TSpTBXPopupWindow.CreatePopupWindow */ inline __fastcall virtual TSpTBXChevronPopupWindow(System::Classes::TComponent* AOwner, Tb2item::TTBView* const AParentView, Tb2item::TTBCustomItem* const AItem, const bool ACustomizing) : TSpTBXPopupWindow(AOwner, AParentView, AItem, ACustomizing) { }
	/* TSpTBXPopupWindow.Destroy */ inline __fastcall virtual ~TSpTBXChevronPopupWindow(void) { }
	
public:
	/* TCustomControl.Create */ inline __fastcall virtual TSpTBXChevronPopupWindow(System::Classes::TComponent* AOwner) : TSpTBXPopupWindow(AOwner) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXChevronPopupWindow(HWND ParentWindow) : TSpTBXPopupWindow(ParentWindow) { }
	
};


__interface ISpTBXPopupMenu;
typedef System::DelphiInterface<ISpTBXPopupMenu> _di_ISpTBXPopupMenu;
__interface  INTERFACE_UUID("{C576A225-6E42-49F6-96E5-712510C5D85C}") ISpTBXPopupMenu  : public System::IInterface 
{
	
public:
	virtual bool __fastcall InternalPopup(int X, int Y, bool ForceFocus, Vcl::Controls::TControl* PopupControl = (Vcl::Controls::TControl*)(0x0)) = 0 ;
};

class DELPHICLASS TSpTBXPopupMenu;
class PASCALIMPLEMENTATION TSpTBXPopupMenu : public Tb2item::TTBPopupMenu
{
	typedef Tb2item::TTBPopupMenu inherited;
	
private:
	Tb2item::TTBCustomItem* FClickedItem;
	bool FReturnClickedItemOnly;
	bool FToolBoxPopup;
	TSpTBXPopupEvent __fastcall GetOnInitPopup(void);
	void __fastcall SetOnInitPopup(const TSpTBXPopupEvent Value);
	System::Classes::TNotifyEvent __fastcall GetOnClosePopup(void);
	void __fastcall SetOnClosePopup(const System::Classes::TNotifyEvent Value);
	
protected:
	virtual bool __fastcall InternalPopup(int X, int Y, bool ForceFocus, Vcl::Controls::TControl* PopupControl = (Vcl::Controls::TControl*)(0x0));
	DYNAMIC Tb2item::TTBRootItemClass __fastcall GetRootItemClass(void);
	
public:
	virtual void __fastcall Popup(int X, int Y);
	HIDESBASE virtual Tb2item::TTBCustomItem* __fastcall PopupEx(int X, int Y, Vcl::Controls::TControl* PopupControl = (Vcl::Controls::TControl*)(0x0), bool ReturnClickedItemOnly = false);
	
__published:
	__property bool ToolBoxPopup = {read=FToolBoxPopup, write=FToolBoxPopup, default=0};
	__property TSpTBXPopupEvent OnInitPopup = {read=GetOnInitPopup, write=SetOnInitPopup};
	__property System::Classes::TNotifyEvent OnClosePopup = {read=GetOnClosePopup, write=SetOnClosePopup};
public:
	/* TTBPopupMenu.Create */ inline __fastcall virtual TSpTBXPopupMenu(System::Classes::TComponent* AOwner) : Tb2item::TTBPopupMenu(AOwner) { }
	/* TTBPopupMenu.Destroy */ inline __fastcall virtual ~TSpTBXPopupMenu(void) { }
	
private:
	void *__ISpTBXPopupMenu;	// ISpTBXPopupMenu 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {C576A225-6E42-49F6-96E5-712510C5D85C}
	operator _di_ISpTBXPopupMenu()
	{
		_di_ISpTBXPopupMenu intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator ISpTBXPopupMenu*(void) { return (ISpTBXPopupMenu*)&__ISpTBXPopupMenu; }
	#endif
	
};


class DELPHICLASS TSpTBXCompoundItemsControl;
class PASCALIMPLEMENTATION TSpTBXCompoundItemsControl : public TSpTBXCustomControl
{
	typedef TSpTBXCustomControl inherited;
	
private:
	void __fastcall DockRequestDock(System::TObject* Sender, Tb2dock::TTBCustomDockableWindow* Bar, bool &Accept);
	Tb2item::TTBRootItem* __fastcall GetRootItems(void);
	TSpTBXToolbarView* __fastcall GetView(void);
	Vcl::Imglist::TCustomImageList* __fastcall GetImages(void);
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* const Value);
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	
protected:
	TSpTBXDock* FDock;
	TSpTBXToolbar* FToolbar;
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	virtual TSpTBXDockClass __fastcall GetDockClass(void);
	virtual TSpTBXToolbarClass __fastcall GetToolbarClass(void);
	virtual Tb2item::TTBCustomItem* __fastcall GetItems(void);
	virtual void __fastcall Loaded(void);
	virtual void __fastcall SetName(const System::Classes::TComponentName Value);
	__property Vcl::Imglist::TCustomImageList* Images = {read=GetImages, write=SetImages};
	
public:
	__fastcall virtual TSpTBXCompoundItemsControl(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXCompoundItemsControl(void);
	DYNAMIC void __fastcall GetChildren(System::Classes::TGetChildProc Proc, System::Classes::TComponent* Root);
	virtual void __fastcall InvalidateBackground(bool InvalidateChildren = true);
	__property TSpTBXToolbarView* View = {read=GetView};
	
__published:
	__property Tb2item::TTBRootItem* Items = {read=GetRootItems};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXCompoundItemsControl(HWND ParentWindow) : TSpTBXCustomControl(ParentWindow) { }
	
private:
	void *__ITBItems;	// Tb2item::ITBItems 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {A5C0D7CC-3EC4-4090-A0F8-3D03271877EA}
	operator Tb2item::_di_ITBItems()
	{
		Tb2item::_di_ITBItems intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator Tb2item::ITBItems*(void) { return (Tb2item::ITBItems*)&__ITBItems; }
	#endif
	
};


class DELPHICLASS TSpTBXCompoundBar;
class PASCALIMPLEMENTATION TSpTBXCompoundBar : public TSpTBXCompoundItemsControl
{
	typedef TSpTBXCompoundItemsControl inherited;
	
private:
	TSpTBXDrawEvent FOnDrawDockBackground;
	void __fastcall DrawDockBackground(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	void __fastcall DrawToolbarBackground(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	void __fastcall DockResize(System::TObject* Sender);
	
protected:
	virtual void __fastcall DoDrawDockBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	__property TSpTBXDrawEvent OnDrawDockBackground = {read=FOnDrawDockBackground, write=FOnDrawDockBackground};
	
public:
	__fastcall virtual TSpTBXCompoundBar(System::Classes::TComponent* AOwner);
public:
	/* TSpTBXCompoundItemsControl.Destroy */ inline __fastcall virtual ~TSpTBXCompoundBar(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXCompoundBar(HWND ParentWindow) : TSpTBXCompoundItemsControl(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXButtonOptions;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXButtonOptions : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	Tb2item::TTBGroupItem* FEditableItems;
	TSpTBXItem* FCloseButton;
	TSpTBXItem* FMinimizeButton;
	TSpTBXItem* FMaximizeButton;
	TSpTBXRightAlignSpacerItem* FRightAlignSpacer;
	int FCaptionImageIndex;
	int FCloseImageIndex;
	int FMinimizeImageIndex;
	int FMaximizeImageIndex;
	int FRestoreImageIndex;
	System::WideString FCaptionLabel;
	bool FCaption;
	bool FClose;
	bool FMinimize;
	bool FMaximize;
	bool FButtonBorders;
	int FTitleBarMaxSize;
	void __fastcall SetCaptionImageIndex(int Value);
	void __fastcall SetCloseImageIndex(int Value);
	void __fastcall SetCaptionLabel(const System::WideString Value);
	void __fastcall SetMaximizeImageIndex(int Value);
	void __fastcall SetRestoreImageIndex(int Value);
	void __fastcall SetMinimizeImageIndex(int Value);
	void __fastcall SetCaption(const bool Value);
	void __fastcall SetClose(const bool Value);
	void __fastcall SetMaximize(const bool Value);
	void __fastcall SetMinimize(const bool Value);
	void __fastcall SetTitleBarMaxSize(const int Value);
	
protected:
	Vcl::Controls::TWinControl* FParentControl;
	TSpTBXToolbar* FToolbar;
	virtual void __fastcall ButtonsDrawImage(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, Sptbxskins::TSpTBXSkinStatesType State, const TSpTBXPaintStage PaintStage, Vcl::Imglist::TCustomImageList* &AImageList, int &AImageIndex, System::Types::TRect &ARect, bool &PaintDefault);
	virtual void __fastcall ButtonsDrawItem(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall ButtonsClick(System::TObject* Sender) = 0 ;
	virtual void __fastcall CreateButtons(void);
	virtual void __fastcall UpdateButtonsVisibility(void);
	virtual void __fastcall SetupButton(TSpTBXCustomItem* B);
	virtual bool __fastcall Restoring(TSpTBXCustomItem* B) = 0 ;
	
public:
	__fastcall virtual TSpTBXButtonOptions(Vcl::Controls::TWinControl* AParent);
	virtual void __fastcall SetupButtonIcon(TSpTBXCustomItem* B);
	void __fastcall MoveItemToTheLeft(Tb2item::TTBCustomItem* B);
	__property Tb2item::TTBGroupItem* EditableItems = {read=FEditableItems};
	__property TSpTBXRightAlignSpacerItem* RightAlignSpacer = {read=FRightAlignSpacer};
	__property TSpTBXItem* MinimizeButton = {read=FMinimizeButton};
	__property TSpTBXItem* MaximizeButton = {read=FMaximizeButton};
	__property TSpTBXItem* CloseButton = {read=FCloseButton};
	__property System::WideString CaptionLabel = {read=FCaptionLabel, write=SetCaptionLabel};
	
__published:
	__property bool ButtonBorders = {read=FButtonBorders, write=FButtonBorders, default=1};
	__property bool Caption = {read=FCaption, write=SetCaption, default=1};
	__property bool Close = {read=FClose, write=SetClose, default=1};
	__property bool Minimize = {read=FMinimize, write=SetMinimize, default=1};
	__property bool Maximize = {read=FMaximize, write=SetMaximize, default=1};
	__property int CaptionImageIndex = {read=FCaptionImageIndex, write=SetCaptionImageIndex, default=-1};
	__property int CloseImageIndex = {read=FCloseImageIndex, write=SetCloseImageIndex, default=-1};
	__property int MinimizeImageIndex = {read=FMinimizeImageIndex, write=SetMinimizeImageIndex, default=-1};
	__property int MaximizeImageIndex = {read=FMaximizeImageIndex, write=SetMaximizeImageIndex, default=-1};
	__property int RestoreImageIndex = {read=FRestoreImageIndex, write=SetRestoreImageIndex, default=-1};
	__property int TitleBarMaxSize = {read=FTitleBarMaxSize, write=SetTitleBarMaxSize, default=21};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSpTBXButtonOptions(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXStatusToolbar;
class PASCALIMPLEMENTATION TSpTBXStatusToolbar : public TSpTBXToolbar
{
	typedef TSpTBXToolbar inherited;
	
private:
	bool FSizeGrip;
	void __fastcall SetSizeGrip(const bool Value);
	HIDESBASE MESSAGE void __fastcall WMNCLButtonDown(Winapi::Messages::TWMNCHitMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMSetCursor(Winapi::Messages::TWMSetCursor &Message);
	
protected:
	Vcl::Forms::TCustomForm* FParentForm;
	virtual void __fastcall DoItemNotification(Tb2item::TTBCustomItem* Ancestor, bool Relayed, Tb2item::TTBItemChangedAction Action, int Index, Tb2item::TTBCustomItem* Item);
	virtual System::Uitypes::TColor __fastcall GetItemsTextColor(Sptbxskins::TSpTBXSkinStatesType State);
	virtual int __fastcall GetRightAlignMargin(void);
	System::Uitypes::TWindowState __fastcall GetParentFormWindowState(void);
	bool __fastcall IsPointInGrip(const System::Types::TPoint &P);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	
public:
	__fastcall virtual TSpTBXStatusToolbar(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXStatusToolbar(void);
	System::Types::TRect __fastcall GetGripRect(void);
	bool __fastcall NeedsSeparatorRepaint(void);
	
__published:
	__property bool SizeGrip = {read=FSizeGrip, write=SetSizeGrip, default=1};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXStatusToolbar(HWND ParentWindow) : TSpTBXToolbar(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXCustomStatusBar;
class PASCALIMPLEMENTATION TSpTBXCustomStatusBar : public TSpTBXCompoundBar
{
	typedef TSpTBXCompoundBar inherited;
	
private:
	bool __fastcall GetSizeGrip(void);
	void __fastcall SetSizeGrip(const bool Value);
	TSpTBXStatusToolbar* __fastcall GetStatusToolbar(void);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	
protected:
	System::Uitypes::TWindowState FPrevState;
	virtual bool __fastcall CanResize(int &NewWidth, int &NewHeight);
	virtual void __fastcall DoDrawDockBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall DrawSeparators(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
	virtual TSpTBXToolbarClass __fastcall GetToolbarClass(void);
	__property Align = {default=2};
	__property bool SizeGrip = {read=GetSizeGrip, write=SetSizeGrip, default=1};
	
public:
	__fastcall virtual TSpTBXCustomStatusBar(System::Classes::TComponent* AOwner);
	__property TSpTBXStatusToolbar* Toolbar = {read=GetStatusToolbar};
public:
	/* TSpTBXCompoundItemsControl.Destroy */ inline __fastcall virtual ~TSpTBXCustomStatusBar(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXCustomStatusBar(HWND ParentWindow) : TSpTBXCompoundBar(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXStatusBar;
class PASCALIMPLEMENTATION TSpTBXStatusBar : public TSpTBXCustomStatusBar
{
	typedef TSpTBXCustomStatusBar inherited;
	
__published:
	__property Align = {default=2};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ParentColor = {default=1};
	__property ParentBiDiMode = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property Visible = {default=1};
	__property OnCanResize;
	__property OnContextPopup;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnResize;
	__property OnStartDock;
	__property OnStartDrag;
	__property Images;
	__property SizeGrip = {default=1};
	__property OnDrawDockBackground;
public:
	/* TSpTBXCustomStatusBar.Create */ inline __fastcall virtual TSpTBXStatusBar(System::Classes::TComponent* AOwner) : TSpTBXCustomStatusBar(AOwner) { }
	
public:
	/* TSpTBXCompoundItemsControl.Destroy */ inline __fastcall virtual ~TSpTBXStatusBar(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXStatusBar(HWND ParentWindow) : TSpTBXCustomStatusBar(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXTitleToolbar;
class DELPHICLASS TSpTBXCustomTitleBar;
class PASCALIMPLEMENTATION TSpTBXTitleToolbar : public TSpTBXToolbar
{
	typedef TSpTBXToolbar inherited;
	
private:
	HIDESBASE MESSAGE void __fastcall WMNCCalcSize(Winapi::Messages::TWMNCCalcSize &Message);
	
protected:
	virtual System::Uitypes::TColor __fastcall GetItemsTextColor(Sptbxskins::TSpTBXSkinStatesType State);
	TSpTBXCustomTitleBar* __fastcall GetTitleBar(void);
	virtual int __fastcall GetRightAlignMargin(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
public:
	/* TSpTBXToolbar.Create */ inline __fastcall virtual TSpTBXTitleToolbar(System::Classes::TComponent* AOwner) : TSpTBXToolbar(AOwner) { }
	/* TSpTBXToolbar.Destroy */ inline __fastcall virtual ~TSpTBXTitleToolbar(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXTitleToolbar(HWND ParentWindow) : TSpTBXToolbar(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXTitleBarButtonOptions;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXTitleBarButtonOptions : public TSpTBXButtonOptions
{
	typedef TSpTBXButtonOptions inherited;
	
private:
	bool FSystemMenu;
	TSpTBXSystemMenuItem* FSystemButton;
	void __fastcall SetSystemMenu(const bool Value);
	
protected:
	TSpTBXCustomTitleBar* FTitleBar;
	virtual void __fastcall ButtonsDrawItem(System::TObject* Sender, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall ButtonsClick(System::TObject* Sender);
	virtual void __fastcall CreateButtons(void);
	virtual bool __fastcall Restoring(TSpTBXCustomItem* B);
	
public:
	__fastcall virtual TSpTBXTitleBarButtonOptions(Vcl::Controls::TWinControl* AParent);
	__property TSpTBXSystemMenuItem* SystemButton = {read=FSystemButton};
	
__published:
	__property bool SystemMenu = {read=FSystemMenu, write=SetSystemMenu, default=1};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSpTBXTitleBarButtonOptions(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TSpTBXCustomTitleBar : public TSpTBXCompoundBar
{
	typedef TSpTBXCompoundBar inherited;
	
private:
	bool FActive;
	bool FFixedSize;
	bool FFullScreenMaximize;
	bool FMouseActive;
	TSpTBXTitleBarButtonOptions* FOptions;
	void *FOldAppWndProc;
	void *FNewAppWndProc;
	HRGN FRegion;
	bool FUpdateRegionCalled;
	TSpTBXDrawEvent FOnDrawBackground;
	System::Classes::TWndMethod FOldParentFormWndProc;
	void __fastcall AppWndProc(Winapi::Messages::TMessage &Msg);
	void __fastcall NewParentFormWndProc(Winapi::Messages::TMessage &Message);
	void __fastcall UpdateRegion(void);
	void __fastcall SetActive(const bool Value);
	void __fastcall SetMouseActive(const bool Value);
	void __fastcall SetFullScreenMaximize(const bool Value);
	TSpTBXPopupEvent __fastcall GetSystemMenuPopup(void);
	void __fastcall SetSystemMenuPopup(const TSpTBXPopupEvent Value);
	System::Uitypes::TWindowState __fastcall GetWindowState(void);
	void __fastcall SetWindowState(const System::Uitypes::TWindowState Value);
	MESSAGE void __fastcall CMStyleChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall CMTextChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMSetCursor(Winapi::Messages::TWMSetCursor &Message);
	HIDESBASE MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMWindowPosChanged(Winapi::Messages::TWMWindowPosMsg &Message);
	
protected:
	Vcl::Forms::TCustomForm* FParentForm;
	virtual void __fastcall Loaded(void);
	System::Types::TPoint __fastcall GetFloatingBorderSize(void);
	virtual Tb2item::TTBCustomItem* __fastcall GetItems(void);
	virtual TSpTBXToolbarClass __fastcall GetToolbarClass(void);
	void __fastcall ChangeTitleBarState(bool Activate);
	void __fastcall UpdateSkinMetrics(void);
	virtual void __fastcall DoDrawDockBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall DoDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const TSpTBXPaintStage PaintStage, bool &PaintDefault);
	virtual void __fastcall AdjustClientRect(System::Types::TRect &Rect);
	void __fastcall GetSizeCursor(const System::Types::TPoint &MousePos, int &SizeCursor, int &SizeCode);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	__property bool Active = {read=FActive, write=SetActive, default=1};
	__property Align = {default=5};
	__property bool FixedSize = {read=FFixedSize, write=FFixedSize, default=0};
	__property bool FullScreenMaximize = {read=FFullScreenMaximize, write=SetFullScreenMaximize, default=0};
	__property TSpTBXTitleBarButtonOptions* Options = {read=FOptions, write=FOptions};
	__property System::Uitypes::TWindowState WindowState = {read=GetWindowState, write=SetWindowState, nodefault};
	__property TSpTBXDrawEvent OnDrawBackground = {read=FOnDrawBackground, write=FOnDrawBackground};
	__property TSpTBXPopupEvent OnSystemMenuPopup = {read=GetSystemMenuPopup, write=SetSystemMenuPopup};
	
public:
	__fastcall virtual TSpTBXCustomTitleBar(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXCustomTitleBar(void);
	System::Types::TRect __fastcall GetClientAreaRect(void);
	bool __fastcall IsActive(void);
	__property bool MouseActive = {read=FMouseActive, write=SetMouseActive, default=1};
	__property TSpTBXToolbar* Toolbar = {read=FToolbar};
	
__published:
	__property Caption = {default=0};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXCustomTitleBar(HWND ParentWindow) : TSpTBXCompoundBar(ParentWindow) { }
	
};


class DELPHICLASS TSpTBXTitleBar;
class PASCALIMPLEMENTATION TSpTBXTitleBar : public TSpTBXCustomTitleBar
{
	typedef TSpTBXCustomTitleBar inherited;
	
__published:
	__property Align = {default=5};
	__property Anchors = {default=3};
	__property Color = {default=-16777211};
	__property BiDiMode;
	__property Constraints;
	__property DockSite = {default=0};
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ParentColor = {default=1};
	__property ParentBiDiMode = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property Visible = {default=1};
	__property OnCanResize;
	__property OnContextPopup;
	__property OnDockDrop;
	__property OnDockOver;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGetSiteInfo;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnResize;
	__property OnStartDock;
	__property OnStartDrag;
	__property OnUnDock;
	__property Active = {default=1};
	__property FixedSize = {default=0};
	__property FullScreenMaximize = {default=0};
	__property Images;
	__property Options;
	__property OnDrawBackground;
	__property OnDrawDockBackground;
	__property OnSystemMenuPopup;
public:
	/* TSpTBXCustomTitleBar.Create */ inline __fastcall virtual TSpTBXTitleBar(System::Classes::TComponent* AOwner) : TSpTBXCustomTitleBar(AOwner) { }
	/* TSpTBXCustomTitleBar.Destroy */ inline __fastcall virtual ~TSpTBXTitleBar(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TSpTBXTitleBar(HWND ParentWindow) : TSpTBXCustomTitleBar(ParentWindow) { }
	
};


class DELPHICLASS TBitmapHint;
class PASCALIMPLEMENTATION TBitmapHint : public Vcl::Controls::THintWindow
{
	typedef Vcl::Controls::THintWindow inherited;
	
private:
	Vcl::Graphics::TBitmap* FHintBitmap;
	bool FActivating;
	HIDESBASE MESSAGE void __fastcall CMTextChanged(Winapi::Messages::TMessage &Message);
	
protected:
	virtual void __fastcall Paint(void);
	
public:
	__property bool Activating = {read=FActivating, nodefault};
	virtual void __fastcall ActivateHint(const System::Types::TRect &Rect, const System::UnicodeString AHint);
	virtual void __fastcall ActivateHintData(const System::Types::TRect &Rect, const System::UnicodeString AHint, void * AData);
public:
	/* THintWindow.Create */ inline __fastcall virtual TBitmapHint(System::Classes::TComponent* AOwner) : Vcl::Controls::THintWindow(AOwner) { }
	
public:
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TBitmapHint(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TBitmapHint(HWND ParentWindow) : Vcl::Controls::THintWindow(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word C_SpTBXRadioGroupIndex = System::Word(0x22b8);
static const System::Word CM_SPPOPUPCLOSE = System::Word(0xb457);
#define rvSpTBXDisplayMode L"DisplayMode"
static const System::Int8 CPDefaultCols = System::Int8(0x8);
static const System::Int8 CPDefaultRows = System::Int8(0x5);
extern DELPHI_PACKAGE Vcl::Graphics::TFont* SmCaptionFont;
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* SpStockHintBitmap;
extern DELPHI_PACKAGE Vcl::Controls::TImageList* MDIButtonsImgList;
extern DELPHI_PACKAGE Vcl::Controls::THintWindowClass SpTBXHintWindowClass;
static const System::Int8 CDefaultToolbarBorderSize = System::Int8(0x2);
static const System::Int8 crSpTBXNewHandPoint = System::Int8(0x64);
static const System::Int8 crSpTBXCustomization = System::Int8(0x65);
static const System::Int8 crSpTBXCustomizationCancel = System::Int8(0x66);
extern DELPHI_PACKAGE void __fastcall SpFillItemInfo(Vcl::Graphics::TCanvas* ACanvas, Tb2item::TTBItemViewer* IV, /* out */ Sptbxskins::TSpTBXMenuItemInfo &ItemInfo);
extern DELPHI_PACKAGE System::Types::TRect __fastcall SpGetBoundsRect(Tb2item::TTBItemViewer* IV, Tb2item::TTBRootItem* Root);
extern DELPHI_PACKAGE void __fastcall SpGetAllItems(Tb2item::TTBCustomItem* AParentItem, System::Classes::TStringList* ItemsList, bool ClearFirst = true);
extern DELPHI_PACKAGE System::Types::TSize __fastcall SpGetMenuMaximumImageSize(Tb2item::TTBView* View);
extern DELPHI_PACKAGE Tb2item::TTBItemViewer* __fastcall SpGetItemViewerFromPoint(Tb2item::TTBRootItem* Root, Tb2item::TTBView* View, const System::Types::TPoint &P, bool ProcessGroupItems = true);
extern DELPHI_PACKAGE Tb2item::TTBItemViewer* __fastcall SpGetNextItemSameEdge(Tb2item::TTBView* View, Tb2item::TTBItemViewer* IV, bool GoForward, TSpTBXSearchItemViewerType SearchType);
extern DELPHI_PACKAGE TSpTBXItemViewer* __fastcall SpGetFirstRightAlignSpacer(Tb2item::TTBView* View);
extern DELPHI_PACKAGE TSpTBXItemViewer* __fastcall SpGetRightAlignedItems(Tb2item::TTBView* View, System::Classes::TList* RightAlignedList, bool IsRotated, /* out */ int &VisibleTotalWidth, /* out */ int &RightAlignedTotalWidth);
extern DELPHI_PACKAGE void __fastcall SpInvalidateItem(Tb2item::TTBView* View, Tb2item::TTBCustomItem* Item);
extern DELPHI_PACKAGE Tb2item::TTBItemViewer* __fastcall SpFindItemViewer(Tb2item::TTBView* View, Tb2item::TTBCustomItem* Item);
extern DELPHI_PACKAGE Tb2item::TTBControlItem* __fastcall SpFindControlItem(Tb2item::TTBCustomItem* Item, Vcl::Controls::TControl* Ctl, bool Recurse = true);
extern DELPHI_PACKAGE void __fastcall SpGetDropPosItemViewer(Tb2item::TTBRootItem* Root, Tb2item::TTBView* View, const System::Types::TPoint &P, /* out */ Tb2item::TTBItemViewer* &DestIV, /* out */ int &DestItemPos, /* out */ System::Types::TRect &DropMark)/* overload */;
extern DELPHI_PACKAGE void __fastcall SpGetDropPosItemViewer(Tb2item::TTBRootItem* Root, Tb2item::TTBView* View, const System::Types::TPoint &P, int SourceItemPos, /* out */ Tb2item::TTBItemViewer* &DestIV, /* out */ int &DestItemPos)/* overload */;
extern DELPHI_PACKAGE int __fastcall SpGetDragHandleSize(Tb2dock::TTBCustomDockableWindow* Toolbar);
extern DELPHI_PACKAGE bool __fastcall SpIsVerticalToolbar(Tb2dock::TTBCustomDockableWindow* Toolbar);
extern DELPHI_PACKAGE bool __fastcall SpIsDockUsingBitmap(Tb2dock::TTBDock* Dock);
extern DELPHI_PACKAGE void __fastcall SpDrawXPToolbarButton(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, Sptbxskins::TSpTBXSkinStatesType State, Sptbxskins::TSpTBXComboPart ComboPart = (Sptbxskins::TSpTBXComboPart)(0x0));
extern DELPHI_PACKAGE void __fastcall SpDrawXPMenuItem(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo);
extern DELPHI_PACKAGE void __fastcall SpDrawXPMenuSeparator(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool MenuItemStyle, bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpDrawXPMenuItemImage(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Sptbxskins::TSpTBXMenuItemInfo &ItemInfo, Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex);
extern DELPHI_PACKAGE void __fastcall SpDrawXPMenuGutter(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
extern DELPHI_PACKAGE void __fastcall SpDrawXPMenuPopupWindow(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const System::Types::TRect &OpenIVRect, bool DrawGutter, int ImageSize);
extern DELPHI_PACKAGE void __fastcall SpDrawXPStatusBar(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const System::Types::TRect &AGripRect);
extern DELPHI_PACKAGE void __fastcall SpDrawXPTitleBar(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool IsActive, bool DrawBorders = true);
extern DELPHI_PACKAGE void __fastcall SpDrawXPTitleBarBody(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool IsActive, const System::Types::TPoint &BorderSize, bool DrawBody = true);
extern DELPHI_PACKAGE void __fastcall SpDrawXPDock(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Vertical = false);
extern DELPHI_PACKAGE void __fastcall SpDrawXPToolbar(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Docked, bool Floating, bool Vertical, bool PaintSkinBackground, bool PaintBorders, Sptbxskins::TSpTBXSkinComponentsType SkinComponent = (Sptbxskins::TSpTBXSkinComponentsType)(0xe))/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawXPToolbar(Tb2dock::TTBCustomDockableWindow* W, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool PaintOnNCArea, bool PaintBorders = true, Sptbxskins::TSpTBXSkinComponentsType SkinComponent = (Sptbxskins::TSpTBXSkinComponentsType)(0xe))/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawXPToolbarGrip(Tb2dock::TTBCustomDockableWindow* W, Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
extern DELPHI_PACKAGE void __fastcall SpDrawXPTooltipBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
extern DELPHI_PACKAGE System::Types::TPoint __fastcall SpCalcPopupPosition(const int X, const int Y, const int Width, const int Height, Vcl::Controls::TControl* PopupControl = (Vcl::Controls::TControl*)(0x0), bool IsVertical = false);
extern DELPHI_PACKAGE System::WideString __fastcall SpHMenuGetCaption(HMENU Menu, int Index);
extern DELPHI_PACKAGE bool __fastcall SpHMenuToTBMenuItem(HMENU Menu, Tb2item::TTBCustomItem* ParentItem);
extern DELPHI_PACKAGE int __fastcall SpShowSystemPopupMenu(Vcl::Forms::TCustomForm* ParentForm, const System::Types::TPoint &ScreenPos, bool DoDefault = true);
extern DELPHI_PACKAGE bool __fastcall SpFillSystemSpTBXPopup(Vcl::Forms::TCustomForm* ParentForm, Tb2item::TTBCustomItem* ParentItem, bool ShowSize, bool ShowMinimize, bool ShowMaximize, bool ShowClose, System::Classes::TNotifyEvent ClickEvent = 0x0);
extern DELPHI_PACKAGE int __fastcall SpShowSystemSpTBXPopupMenu(Vcl::Forms::TCustomForm* ParentForm, const System::Types::TPoint &ScreenPos, bool ShowSize, bool ShowMinimize, bool ShowMaximize, bool ShowClose, TSpTBXPopupEvent PopupEvent, bool DoDefault = true);
extern DELPHI_PACKAGE void __fastcall SpActivateDwmNC(Vcl::Controls::TWinControl* WinControl, bool Activate);
extern DELPHI_PACKAGE bool __fastcall SpIsDwmCompositionEnabled(void);
extern DELPHI_PACKAGE bool __fastcall SpCanFocus(Vcl::Controls::TWinControl* WinControl);
extern DELPHI_PACKAGE bool __fastcall SpIsFocused(Vcl::Controls::TWinControl* WinControl, /* out */ Vcl::Controls::TWinControl* &FocusedChild);
extern DELPHI_PACKAGE Vcl::Controls::TWinControl* __fastcall SpFocusFirstChild(Vcl::Controls::TWinControl* WinControl);
extern DELPHI_PACKAGE int __fastcall SpFindControl(Vcl::Controls::TWinControl* Parent, Vcl::Controls::TControl* Child);
extern DELPHI_PACKAGE Vcl::Controls::TWinControl* __fastcall SpFindParent(Vcl::Controls::TControl* Control, System::TClass ParentClass);
extern DELPHI_PACKAGE bool __fastcall SpHasBorders(Vcl::Controls::TWinControl* WinControl);
extern DELPHI_PACKAGE System::Uitypes::TWindowState __fastcall SpGetFormWindowState(Vcl::Forms::TCustomForm* F, /* out */ System::Types::TRect &RestoreBoundsRect);
extern DELPHI_PACKAGE void __fastcall SpSetFormWindowState(Vcl::Forms::TCustomForm* F, System::Uitypes::TWindowState WindowState, const System::Types::TRect &RestoreBoundsRect);
extern DELPHI_PACKAGE bool __fastcall SpGetTaskBar(/* out */ unsigned &State, /* out */ unsigned &Edge, /* out */ System::Types::TRect &Bounds);
extern DELPHI_PACKAGE void __fastcall SpRecalcNCArea(Vcl::Controls::TWinControl* WinControl);
extern DELPHI_PACKAGE void __fastcall SpCustomizeAllToolbars(System::Classes::TComponent* AParentComponent, bool Reset);
extern DELPHI_PACKAGE void __fastcall SpBeginUpdateAllToolbars(System::Classes::TComponent* AParentComponent);
extern DELPHI_PACKAGE void __fastcall SpEndUpdateAllToolbars(System::Classes::TComponent* AParentComponent);
}	/* namespace Sptbxitem */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SPTBXITEM)
using namespace Sptbxitem;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SptbxitemHPP
