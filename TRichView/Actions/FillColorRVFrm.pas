
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for choosing color for text, paragraph,  }
{       table and cell background.                      }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit FillColorRVFrm;

interface

{$I RichViewActions.inc}

{$I RV_Defs.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, BaseRVFrm, StdCtrls, RVColorGrid, ColorRVFrm, RVStyle,
  RVALocalize, RVGrids;

type
  TfrmRVFillColor = class(TfrmRVBase)
    gb: TGroupBox;
    lblColor: TLabel;
    rvcg: TRVColorGrid;
    btnColor: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    lbl: TLabel;
    cmb: TComboBox;
    ColorDialog1: TColorDialog;
    btnPadding: TButton;
    procedure btnColorClick(Sender: TObject);
    procedure rvcgColorChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure cmbClick(Sender: TObject);
    procedure btnPaddingClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    _btnPadding: TControl;
    FChosenColor: TColor;
    Updating: Boolean;
    PaddingLeft, PaddingTop, PaddingRight, PaddingBottom: TRVStyleLength;
    FPaddingLeft, FPaddingTop, FPaddingRight, FPaddingBottom: Boolean;
    { Private declarations }
    function GetCD: TColorDialog;
    procedure SetChosenColor(Value: TColor);
  protected
    function GetOkButton: TButton; override;
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _cmb, _lblColor: TControl;
    LblColorRight: Integer;
    ColorDialog: TColorDialog;
    rvs: TRVStyle;
    Indeterminate: Boolean;
    procedure SetPadding(Padding: TRVRect; UsePadding: TRVBooleanRect; rvs: TRVStyle);
    procedure GetPadding(Padding: TRVRect; UsePadding: TRVBooleanRect);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    property ChosenColor: TColor read FChosenColor write SetChosenColor;
  end;

implementation
uses FourSidesRVFrm, RichViewActions;

{$R *.dfm}

{ TfrmRVFillColor }

function TfrmRVFillColor.GetCD: TColorDialog;
begin
  if ColorDialog<>nil then
    Result := ColorDialog
  else
    Result := ColorDialog1;
end;
function TfrmRVFillColor.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gb.Left*2+gb.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gb.Top-gb.Height);
  Result := True;
end;

{------------------------------------------------------------------------------}
type
  TControlHack = class (TControl)
  end;
  
procedure TfrmRVFillColor.SetChosenColor(Value: TColor);
var rgb: Integer;
begin
  if Value<>clNone then
    Value := ColorToRGB(Value);
  if Indeterminate then begin
    SetControlCaption(_lblColor, '');
    rvcg.Indeterminate := True;
    end
  else begin
    FChosenColor := Value;
    Updating := True;
    rvcg.ChosenColor := Value;
    Updating := False;
    if rvcg.Indeterminate then begin
      TControlHack(_lblColor).ParentBiDiMode := False;
      _lblColor.BiDiMode := bdLeftToRight;
      rgb := ColorToRGB(FChosenColor);
      SetControlCaption(_lblColor, 'RGB('+
        IntToStr(rgb and $0000FF)+','+
        IntToStr((rgb and $00FF00) shr 8)+','+
        IntToStr((rgb and $FF0000) shr 16)+')');
      end
    else begin
      TControlHack(_lblColor).ParentBiDiMode := True;
      SetControlCaption(_lblColor, rvcg.GetColorName);
    end;
    if BiDiMode=bdRightToLeft then
      lblColor.Left := LblColorRight-lblColor.Width;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.btnColorClick(Sender: TObject);
begin
  if ChosenColor=clNone then
    GetCD.Color := clYellow
  else
    GetCD.Color := ColorToRGB(ChosenColor);
  if GetCD.Execute then begin
    Indeterminate := False;
    ChosenColor := GetCD.Color;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.rvcgColorChange(Sender: TObject);
begin
  if not Visible or Updating then
    exit;
  Indeterminate := rvcg.Indeterminate;
  ChosenColor := rvcg.ChosenColor;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.FormCreate(Sender: TObject);
begin
  _btnPadding := btnPadding;
  _cmb := cmb;
  _lblColor := lblColor;
  inherited;
  SetControlCaption(_lblColor, '');
  Indeterminate := True;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.btnOkClick(Sender: TObject);
begin
  if Indeterminate then
    RVA_MessageBox(RVA_GetS(rvam_fillc_PleaseSelect, ControlPanel), RVA_GetS(rvam_fillc_Title, ControlPanel), MB_ICONSTOP)
  else
    ModalResult := mrOk;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.cmbClick(Sender: TObject);
begin
  _btnPadding.Enabled := (GetXBoxItemIndex(_cmb)>=0) and
    (Integer(GetXBoxObject(_cmb, GetXBoxItemIndex(_cmb)))=3);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.btnPaddingClick(Sender: TObject);
var frm: TfrmRVFourSides;
begin
  frm := TfrmRVFourSides.Create(Application, ControlPanel);
  try
    frm.SetControlCaption(frm._gb, RVA_GetSHUnits(rvam_4s_DefTitle,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), ControlPanel));
    frm.SetValues(PaddingLeft, PaddingTop, PaddingRight, PaddingBottom,
       FPaddingLeft, FPaddingTop, FPaddingRight, FPaddingBottom, rvs,
       TRVAControlPanel(ControlPanel).UserInterface<>rvauiFull,
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
    if frm.ShowModal=mrOk then
      frm.GetValues(PaddingLeft, PaddingTop, PaddingRight, PaddingBottom,
       FPaddingLeft, FPaddingTop, FPaddingRight, FPaddingBottom, rvs,
       RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.FormActivate(Sender: TObject);
begin
  cmbClick(Sender);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.GetPadding(Padding: TRVRect; UsePadding: TRVBooleanRect);
begin
  with Padding do begin
    Left   := PaddingLeft;
    Top    := PaddingTop;
    Right  := PaddingRight;
    Bottom := PaddingBottom;
  end;

  with UsePadding do begin
    Left   := FPaddingLeft;
    Top    := FPaddingTop;
    Right  := FPaddingRight;
    Bottom := FPaddingBottom;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.SetPadding(Padding: TRVRect; UsePadding: TRVBooleanRect;
  rvs: TRVStyle);
begin
  Self.rvs := rvs;
  with Padding do begin
    PaddingLeft   := Left;
    PaddingTop    := Top;
    PaddingRight  := Right;
    PaddingBottom := Bottom;
  end;

  with UsePadding do begin
    FPaddingLeft   := Left;
    FPaddingTop    := Top;
    FPaddingRight  := Right;
    FPaddingBottom := Bottom;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVFillColor.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_fillc_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_fillc_Title, ControlPanel);
  lbl.Caption := RVA_GetSAsIs(rvam_fillc_ApplyTo, ControlPanel);
  btnColor.Caption := RVA_GetSAsIs(rvam_fillc_MoreColors, ControlPanel);
  btnPadding.Caption := RVA_GetSAsIs(rvam_fillc_Padding, ControlPanel);
  rvcg.FirstCaption := RVA_GetS(rvam_cl_Transparent, ControlPanel);
  LblColorRight := lblColor.Left+lblColor.Width;
end;

function TfrmRVFillColor.GetOkButton: TButton;
begin
  Result := btnOk;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVFillColor.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl=btnPadding then
    _btnPadding := NewControl
  else if OldControl = _cmb then
    _cmb := NewControl
  else if OldControl = _lblColor then
    _lblColor := NewControl;
end;
{$ENDIF}

end.
