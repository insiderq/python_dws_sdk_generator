{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       A dialog for object caption insertion           }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
unit ItemCaptionRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, NumSeqCustomRVFrm, StdCtrls, ExtCtrls, RVALocalize;

type
  TfrmRVItemCaption = class(TfrmRVCustomNumSeq)
    lblCaption: TLabel;
    txtCaption: TEdit;
    rgPosition: TRadioGroup;
    cbExcludeLabel: TCheckBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _txtCaption, _rgPosition, _cbExcludeLabel: TControl;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    procedure Localize; override;
  end;


implementation

{$R *.dfm}

{ TfrmRVItemCaption }

procedure TfrmRVItemCaption.FormCreate(Sender: TObject);
begin
  _rgPosition := rgPosition;
  _txtCaption := txtCaption;
  _cbExcludeLabel := cbExcludeLabel;
  inherited;
end;
{------------------------------------------------------------------------------}
function TfrmRVItemCaption.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := gbSeq.Left*2+gbSeq.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-rgPosition.Top-rgPosition.Height);
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemCaption.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_ic_Title, ControlPanel);
  lblSeqName.Caption := RVA_GetSAsIs(rvam_in_Label, ControlPanel);
  lblCaption.Caption := RVA_GetSAsIs(rvam_in_CaptionText, ControlPanel);
  cbExcludeLabel.Caption := RVA_GetSAsIs(rvam_in_ExcludeLabel, ControlPanel);
  rgPosition.Caption := RVA_GetSHAsIs(rvam_in_Position, ControlPanel);
  rgPosition.Items[0] := RVA_GetSAsIs(rvam_in_Above, ControlPanel);
  rgPosition.Items[1] := RVA_GetSAsIs(rvam_in_Below, ControlPanel);  
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVItemCaption.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl=_txtCaption then
    _txtCaption := NewControl
  else if OldControl=_rgPosition then
    _rgPosition := NewControl
  else if OldControl=_cbExcludeLabel then
    _cbExcludeLabel := NewControl
  else
    inherited;
end;
{$ENDIF}


end.
