{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Spanish (SP) translation                        }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Jordi Cornet Montes, 2003-08-22        }
{                jordicornet@ya.com                     }
{ Updated by pchelp-info, 2004-09-30                    }
{ Updated by Manuel Onate, 2008-03-24                   }
{ Updated by Alconost, 2011-03-10, 2011-09-19           }
{ Updated by Ra�l Izquierdo, 2012-08-31                 }
{ Updated by Ra�l Izquierdo, 2014-02-01                 }
{*******************************************************}

unit RVAL_Esp;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'pulgadas', 'cm', 'mm', 'c�ceros', 'p�xeles', 'puntos',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'ci', 'p�xeles'{?}, 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Archivo', '&Editar', 'F&ormato', 'Fue&nte', '&P�rrafo', '&Insertar', '&Tabla', '&Ventana', 'A&yuda',
  // exit
  '&Salir',
  // top-level menus: View, Tools,
  '&Ver', '&Herramientas',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Tama�o', 'Estilo', 'Seleccionar', 'Alinear el contenido de las celdas', 'L�neas de divisi�n',
  // menus: Table cell rotation
  '&Rotaci�n de celdas',
  // menus: Text flow, Footnotes/endnotes
  '&Flujo de &texto', '&Notas al pie',
  // ribbon tabs: tab1, tab2, view, table
  '&Inicio', '&Avanzado', '&Vista', '&Tabla',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Portapapeles', 'Fuente', 'P�rrafo', 'Lista', 'Edici�n',
  // ribbon groups: insert, background, page setup,
  'Insertar', 'Fondo', 'Configurar p�gina',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Enlaces', 'Notas al pie', 'Vistas de documento', 'Mostrar/Ocultar', 'Zoom', 'Opciones',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Insertar', 'Eliminar', 'Operaciones', 'Bordes',
  // ribbon groups: styles
  'Estilos',
  // ribbon screen tip footer,
  'Pulse F1 para m�s ayuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documentos recientes', 'Guardar una copia del documento', 'Vista previa e impresi�n del documento',
  // ribbon label: units combo
  'Unidades:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nuevo', 'Nuevo|Crea nuevo documento en blanco',
  // TrvActionOpen
  '&Abrir...', 'Abrir|Abre un documento del disco',
  // TrvActionSave
  '&Guardar', 'Guardar|Guarda el documento en el disco',
  // TrvActionSaveAs
  'Guardar &Como...', 'Guardar Como...|Guarda el documento en el disco con otro nombre',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta el documento a otro formato',
  // TrvActionPrintPreview
  '&Vista Preliminar', 'Vista Preliminar|Muestra el documento tal como ser� impreso',
  // TrvActionPrint
  '&Imprimir...', 'Imprimir|Permite configurar la impresora e imprimir el documento',

  // TrvActionQuickPrint
  '&Imprimir', '&Impresi�n r�pida', 'Imprimir|Imprime el documento',
  // TrvActionPageSetup
  'P&reparar P�gina...', 'Preparar P�gina|Establece los m�rgenes, tama�o de papel, orientaci�n, origen, encabezados y pies',
  // TrvActionCut
  'Cor&tar', 'Cortar|Corta la selecci�n y la pone en el Portapapeles',
  // TrvActionCopy
  '&Copiar', 'Copiar|Copia la selecci�n y la pone en el Portapapeles',
  // TrvActionPaste
  '&Pegar', 'Pegar|Inserta el contenido del Portapapeles',
  // TrvActionPasteAsText
  'Pegar como &texto', 'Pegar como texto|Inserta el texto del Portapapeles',
  // TrvActionPasteSpecial
  'Pegado &Especial...', 'Pegado Especial|Inserta el contenido del Portapapeles en el formato especificado',
  // TrvActionSelectAll
  'Seleccionar &Todo', 'Seleccionado Todo|Selecciona todo el documento',
  // TrvActionUndo
  '&Deshacer', 'Deshacer|Deshace la ultima acci�n',
  // TrvActionRedo
  '&Rehacer', 'Rehacer|Rehace la ultima acci�n deshecha',
  // TrvActionFind
  '&Buscar...', 'Buscar|Busca el texto especificado en el documento',
  // TrvActionFindNext
  'Buscar &Siguiente', 'Buscar Siguiente|Contin�a la �ltima b�squeda',
  // TrvActionReplace
  '&Reemplazar...', 'Reemplazar|Busca y reemplaza el texto especificado en el documento',
  // TrvActionInsertFile
  '&Archivo...', 'Insertar Archivo|Inserta el contenido del archivo en el documento',
  // TrvActionInsertPicture
  '&Imagen...', 'Insertar Imagen|Inserta una imagen del disco',
  // TRVActionInsertHLine
  '&L�nea Horizontal', 'Insertar L�nea Horizontal|Inserta una linea horizontal',
  // TRVActionInsertHyperlink
  'Hiperv�nculo...', 'Insertar Hiperv�nculo|Inserta un Hiperv�nculo',
  // TRVActionRemoveHyperlinks
  '&Eliminar hiperv�nculos', 'Eliminar hiperv�nculos|Elimina todos los hiperv�nculos en el texto seleccionado',
  // TrvActionInsertSymbol
  '&S�mbolo...', 'Insertar S�mbolo|Inserta un s�mbolo',
  // TrvActionInsertNumber
  '&N�mero...', 'Insertar N�mero|Inserta un n�mero',
  // TrvActionInsertCaption
  'Insertar leyenda...', 'Insertar leyenda|Insertar leyenda al objeto seleccionado',
  // TrvActionInsertTextBox
  'Caja de &texto', 'Insertar caja de texto|Insertar una caja de texto',
  // TrvActionInsertSidenote
  'Nota al margen', 'Insetar nota al margen|Insertar una nota mostrada en una caja de texto',
  // TrvActionInsertPageNumber
  'N�mero de &p�gina', 'Insertar n�mero de p�gina|Insertar un n�mero de p�gina',
  // TrvActionParaList
  'Vi�etas y &Numeraci�n...', 'Vi�etas y Numeraci�n|Aplica o edita vi�etas o numeraci�n en el p�rrafo seleccionado',
  // TrvActionParaBullets
  '&Vi�etas', 'Vi�etas|A�ade o elimina vi�etas al p�rrafo',
  // TrvActionParaNumbering
  '&Numeraci�n', 'Numeraci�n|A�ade o elimina numeraci�n del p�rrafo',
  // TrvActionColor
  '&Color de Fondo...', 'Color de Fondo|Cambia el color de fondo del documento',
  // TrvActionFillColor
  '&Rellenar Color...', 'Rellenar Color|Cambia el color de fondo del texto, p�rrafo, celda, tabla o documento',
  // TrvActionInsertPageBreak
  '&Insertar Salto de P�gina', 'Insertar Salto de P�gina|Inserta un salto de p�gina',
  // TrvActionRemovePageBreak
  '&Eliminar Salto de P�gina', 'Eliminar Salto de P�gina|Elimina un salto de p�gina',

  // TrvActionClearLeft
  'Limpiar el flujo de texto a la &izquierda', 'Limpiar el flujo de texto a la izquierda|Posiciona este p�rrafo debajo de cualquier imagen alineada a la izquierda',
  // TrvActionClearRight
  'Limpiar el flujo de texto a la &derecha', 'Limpiar el flujo de texto a la &derecha|Posiciona este p�rrafo debajo de cualquier imagen alineada a la derecha',
  // TrvActionClearBoth
  'Limpiar el flujo de texto por &ambos lados', 'Limpiar el flujo de texto por ambos lados|Posiciona este p�rrafo debajo de cualquier imagen alineada a la derecha o a la izquierda',
  // TrvActionClearNone
  'Flujo de texto &normal', 'Flujo de texto normal|Permite insertar texto alrededor de las im�genes alineadas a la derecha e izquierda',
  // TrvActionVAlign
  'Posici�n del &Objeto...', 'Posici�n del Objeto|Cambia la posici�n del objeto seleccionado',
  // TrvActionItemProperties
  '&Propiedades del Objeto...', 'Propiedades del Objeto|Define las propiedades del objeto activo',
  // TrvActionBackground
  '&Fondo...', 'Fondo|Selecciona imagen y color de fondo',
  // TrvActionParagraph
  '&P�rrafo...', 'P�rrafo|Cambia los atributos al p�rrafo',
  // TrvActionIndentInc
  '&Aumentar Sangr�a', 'Aumentar Sangr�a|Aumenta la sangr�a izquierda a los p�rrafos seleccionados',
  // TrvActionIndentDec
  '&Disminuir Sangr�a', 'Reducir Sangr�a|Reduce la sangr�a izquierda a los p�rrafos seleccionados',
  // TrvActionWordWrap
  '&Ajuste de L�nea', 'Ajuste de L�nea|Establece el ajuste de l�nea para los p�rrafos seleccionados',
  // TrvActionAlignLeft
  'Alineaci�n &Izquierda', 'Alineaci�n Izquierda|Alinea el texto seleccionado a la izquierda',
  // TrvActionAlignRight
  'Alineaci�n &Derecha', 'Alineaci�n Derecha|Alinea el texto seleccionado a la derecha',
  // TrvActionAlignCenter
  '&Centrar', 'Centrar|Centra el texto seleccionado',
  // TrvActionAlignJustify
  '&Justificar', 'Justificar|Alinea el texto seleccionado entre los m�rgenes izquierdo y derecho',
  // TrvActionParaColor
  'Color de &Fondo del P�rrafo...', 'Color de Fondo del P�rrafo|Establece color de fondo de los p�rrafos',
  // TrvActionLineSpacing100
  'Espaciado &Simple', 'Espaciado Simple|Establece un espaciado simple entre las l�neas',
  // TrvActionLineSpacing150
  'Espaciado &1.5', 'Espaciado 1.5|Establece un espaciado equivalente a 1.5 l�neas',
  // TrvActionLineSpacing200
  '&Doble Espaciado', 'Doble Espaciado|Establece doble espaciado entre l�neas',
  // TrvActionParaBorder
  '&Bordes y Fondo del P�rrafo...', 'Bordes y Fondo del P�rrafo|Establece bordes y fondo de los p�rrafos seleccionados',
  // TrvActionInsertTable
  '&Insertar Tabla...', '&Tabla', 'Insertar Tabla|Inserta una tabla',
  // TrvActionTableInsertRowsAbove
  'Insertar Fila &Encima', 'Insertar Fila Encima|Inserta una nueva fila encima de las celdas seleccionadas',
  // TrvActionTableInsertRowsBelow
  'Insertar Fila &Debajo', 'Insertar Fila Debajo|Inserta una nueva fila debajo de las celdas seleccionadas',
  // TrvActionTableInsertColLeft
  'Insertar Columna a la &Izquierda', 'Insertar Columna a la Izquierda|Inserta una nueva columna a la izquierda de las celdas seleccionadas',
  // TrvActionTableInsertColRight
  'Insertar Columna a la &Derecha', 'Insertar Columna a la Derecha|Inserta una nueva columna a la derecha de las celdas seleccionadas',
  // TrvActionTableDeleteRows
  'Eliminar F&ilas', 'Eliminar Filas|Elimina las filas con las celdas seleccionadas',
  // TrvActionTableDeleteCols
  'Eliminar &Columnas', 'Eliminar Columnas|Elimina las columnas con las celdas seleccionadas',
  // TrvActionTableDeleteTable
  '&Eliminar Tabla', 'Eliminar Tabla|Elimina la tabla',
  // TrvActionTableMergeCells
  '&Combinar Celdas', 'Combinar Celdas|Combina las celdas seleccionadas',
  // TrvActionTableSplitCells
  '&Dividir Celdas...', 'Dividir Celdas|Divide las celdas seleccionadas',
  // TrvActionTableSelectTable
  'Seleccionar &Tabla', 'Seleccionar Tabla|Selecciona la tabla',
  // TrvActionTableSelectRows
  'Seleccionar Fi&las', 'Seleccionar Filas|Selecciona filas',
  // TrvActionTableSelectCols
  'Seleccionar Col&umnas', 'Seleccionar Columnas|Selecciona columnas',
  // TrvActionTableSelectCell
  'Seleccionar C&elda', 'Seleccionar Celda|Selecciona celda',
  // TrvActionTableCellVAlignTop
  'Alinear Celda &Arriba', 'Alinear Celda Arriba|Alinea el contenido de la celda arriba',
  // TrvActionTableCellVAlignMiddle
  'Alinear Celda al &Centro', 'Alinear Celda al Centro|Alinea el contenido de la celda al centro',
  // TrvActionTableCellVAlignBottom
  'Alinear Celda A&bajo', 'Alinear Celda Abajo|Alinea el contenido de la celda abajo',
  // TrvActionTableCellVAlignDefault
  '&Alineaci�n Vertical Celda por Defecto', 'Alineaci�n Vertical de la Celda por Defecto|Establece la alineaci�n vertical por defecto a las celdas seleccionadas',
  // TrvActionTableCellRotationNone
  '&Sin rotaci�n de celda', 'Sin rotaci�n de celda|Rota el contenido de la celda 0�',
  // TrvActionTableCellRotation90
  'Rotar celda &90�', 'Rotar celda 90�|Rota el contenido de la celda 90�',
  // TrvActionTableCellRotation180
  'Rotar celda &180�', 'Rotar celda 180�|Rota el contenido de la celda 180�',
  // TrvActionTableCellRotation270
  'Rotar celda &270�', 'Rotar celda 270�|Rota el contenido de la celda 270�',
  // TrvActionTableProperties
  '&Propiedades Tabla...', 'Propiedades Tabla|Cambia las propiedades de la tabla seleccionada',
  // TrvActionTableGrid
  'Mostrar L�neas &Rejilla', 'Mostrar L�neas Rejilla|Muestra u oculta las l�neas de la rejilla',
  // TRVActionTableSplit
  '&Dividir tabla', 'Dividir tabla|Divide la tabla en dos comenzando desde la fila seleccionada',
  // TRVActionTableToText
  '&Convertir en texto...', 'Convertir en texto|Convierte la tabla en texto',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena las filas de la tabla',
  // TrvActionTableCellLeftBorder
  'Borde &Izquierdo', 'Borde Izquierdo|Muestra u oculta el borde izquierdo de la celda',
  // TrvActionTableCellRightBorder
  'Borde &Derecho', 'Borde Derecho|Muestra u oculta el borde derecho de la celda',
  // TrvActionTableCellTopBorder
  'Borde &Superior', 'Borde Superior|Muestra u oculta el borde superior de la celda',
  // TrvActionTableCellBottomBorder
  'Borde &Inferior', 'Borde Inferior|Muestra u oculta el borde inferior de la celda',
  // TrvActionTableCellAllBorders
  '&Todos los Bordes', 'Todos los Bordes|Muestra todos los bordes de la celda',
  // TrvActionTableCellNoBorders
  '&Sin Bordes', 'Sin Bordes|Oculta todo los bordes de la celda',
  // TrvActionFonts & TrvActionFontEx
  '&Fuente...', 'Fuente|Cambia la fuente al texto seleccionado',
  // TrvActionFontBold
  '&Negrita', 'Negrita|Cambia el estilo del texto seleccionado a negrita',
  // TrvActionFontItalic
  '&Cursiva', 'Cursiva|Cambia el estilo del texto seleccionado a cursiva',

  // TrvActionFontUnderline
  '&Subrayado', 'Subrayado|Cambia el estilo del texto seleccionado a subrayado',
  // TrvActionFontStrikeout
  '&Tachado', 'Tachado|Tacha el texto seleccionado',
  // TrvActionFontGrow
  '&Aumentar Fuente', 'Aumentar Fuente|Aumenta el tama�o del texto seleccionado un 10%',
  // TrvActionFontShrink
  'R&educir Fuente', 'Reducir Fuente|Reduce el tama�o del texto seleccionado un 10%',
  // TrvActionFontGrowOnePoint
  'A&umentar Fuente Un Punto', 'Aumentar un Punto la Fuente|Aumenta 1 punto el tama�o del texto seleccionado',
  // TrvActionFontShrinkOnePoint
  'Redu&cir Fuente Un Punto', 'Reducir un Punto la Fuente|Reduce 1 punto el tama�o del texto seleccionado',
  // TrvActionFontAllCaps
  '&Todo May�sculas', 'Todo May�sculas|Cambia todo el texto seleccionado a letras may�sculas',
  // TrvActionFontOverline
  '&Sobrel�nea', 'Sobrel�nea|A�ade una l�nea encima del texto seleccionado',
  // TrvActionFontColor
  '&Color del Texto...', 'Color del Texto|Cambia el color del texto seleccionado',
  // TrvActionFontBackColor
  'Color de F&ondo del Texto...', 'Color de Fondo del Texto|Cambia el color del fondo del texto seleccionado',
  // TrvActionAddictSpell3
  '&Revisi�n Ortogr�fica', 'Revisi�n Ortogr�fica|Revisa la ortograf�a',
  // TrvActionAddictThesaurus3
  '&Tesauro', 'Tesauro|Proporciona sin�nimos para la palabra seleccionada',
  // TrvActionParaLTR
  'Izquierda a Derecha', 'Izquierda a Derecha|Establece la direcci�n del texto para los p�rrafos seleccionados de izquierda a derecha',
  // TrvActionParaRTL
  'Derecha a Izquierda', 'Derecha a Izquierda|Establece la direcci�n del texto para los p�rrafos seleccionados de derecha a izquierda',
  // TrvActionLTR
  'Texto de Izquierda a Derecha', 'Texto de Izquierda a Derecha|Establece la direcci�n de izquierda a derecha para el texto seleccionado',
  // TrvActionRTL
  'Texto de Derecha a Izquierda', 'Texto de Derecha a Izquierda|Establece la direcci�n de derecha a izquierda para el texto seleccionado',
  // TrvActionCharCase
  'Cambiar May�sculas y Min�sculas', 'Cambiar May�sculas y Min�sculas|Cambia May�sculas y Min�sculas al texto seleccionado',
  // TrvActionShowSpecialCharacters
  '&S�mbolos no tipogr�ficos', 'S�mbolos no tipogr�ficos|Muestra u oculta s�mbolos no tipogr�ficos, como marcos de p�rrafo, tabulaciones y espacios',
  // TrvActionSubscript
  'Su&brayado','Subrayado|Convierte el texto seleccionado a subrayado',
  // TrvActionSuperscript
  'Sobrerrayado','Sobrerrayado|Convierte el texto seleccionado a sobrerrayado',
  // TrvActionInsertFootnote
  '&Nota al pie', 'Nota al pie|Inserta una nota al pie',
  // TrvActionInsertEndnote
  '&Nota final', 'Nota final|Inserta una nota final',
  // TrvActionEditNote
  'E&ditar nota', 'Editar nota|Permite editar las notas al pie o notas finales',
  '&Ocultar', 'Ocultar|Oculta o muestra el fragmento seleccionado',        
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
   '&Estilos...', 'Estilos|Abrir dialogo de manejador de estilos',
  // TrvActionAddStyleTemplate
   '&Agregar estilo...', 'Agregar estilo|Crear un nuevo estilo de texto o de parrafo',
  // TrvActionClearFormat,
   'Limpiar formato', 'Limpiar formato|Limpiar todo el formato a texto y parrafos de la selecci�n',
  // TrvActionClearTextFormat,
   'Limpiar formato de texto', 'Limpiar formato de texto|Limpiar todo el formato de text de la selecci�n',
  // TrvActionStyleInspector
   '&Inspector de estilos', 'Inspector de estilos|Muestra o esconde el inspector de estilos',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------------
   'Aceptar', 'Cancelar', 'Cerrar', 'Insertar', '&Abrir...', '&Guardar...', '&Limpiar', 'A&yuda', 'Quitar',
  // Others  -------------------------------------------------------------------
  // percents
  'porcentaje',
  // left, top, right, bottom sides
  'Lado Izquierdo', 'Lado Superior', 'Lado Derecho', 'Lado Inferior',
  // save changes? confirm title
  '�Guadar cambios a %s?', 'Confirmaci�n',
  // warning: losing formatting
  '%s puede contener caracter�sticas que no son compatibles con el formato que ha seleccionado.'#13+
  '�Desea guardar el documento en este formato?',
  // RVF format name
  'RichView Format',
  // Error messages ------------------------------------------------------------
  'Error',
  'Error cargando archivo.'#13#13'Posibles causas:'#13'- Esta aplicaci�n no soporta el formato de esta archivo;'#13+
  '- el archivo est� corrupto;'#13'- el archivo est� abierto y bloqueado por otra aplicaci�n.',
  'Error cargando el archivo de imagen.'#13#13'Posibles causas:'#13'- el archivo tiene una imagen en un formato que no es compatible con esta aplicaci�n;'#13+
  '- el archivo no contiene una imagen;'#13+
  '- el archivo est� corrupto;'#13'- el archivo est� abierto y bloqueado por otra aplicaci�n.',
  'Error al guardar archivo.'#13#13'Posibles causas:'#13'- no hay espacio en disco;'#13+
  '- el disco tiene protecci�n de escritura;'#13'- el disco removible no ha sido insertado;'#13+
  '- el archivo est� abierto y bloqueado por otra aplicaci�n;'#13'- el disco esta corrupto.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Archivos RichView (*.rvf)|*.rvf',  'Archivos RTF (*.rtf)|*.rtf' , 'Archivos XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Archivos de Texto (*.txt)|*.txt', 'Archivos de Texto Unicode (*.txt)|*.txt', 'Archivos de Texto - Autodetectar (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Archivos HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplificado (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documentos de Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'B�squeda Completada', 'Texto a buscar ''%s'' no encontrado.',
  // 1 string replaced; Several strings replaced
  '1 texto reeemplazado.', '%d textos reemplazados.',
  // continue search from the beginning/end?
  'El programa lleg� al final del documento. �Desea continuar la b�squeda desde el principio?',
  'El programa lleg� al principio del documento. �Desea continuar la b�squeda desde el final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparente', 'Autom�tico',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Negro', 'Marr�n', 'Verde Oliva', 'Verde Oscuro', 'Verde Azulado Oscuro', 'Azul Marino', 'A�il', 'Gris-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Rojo Oscuro', 'Anaranjado', 'Amarillo Oscuro', 'Verde', 'Verde Azulado', 'Azul', 'Azul Gris�ceo', 'Gris-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rojo', 'Anaranjado Claro', 'Lima', 'Verde Mar', 'Agua', 'Azul Claro', 'Violeta', 'Gris-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Oro', 'Amarillo', 'Verde Claro', 'Turquesa', 'Azul Cielo', 'Ciruela', 'Gris-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa Claro', 'Canela', 'Amarillo Claro', 'Verde Claro', 'Turquesa Claro', 'Azul P�lido', 'Lavanda', 'Blanco',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparente', '&Autom�tico', '&M�s Colores...', '&Defecto',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fondo', 'C&olor:', 'Posici�n', 'Fondo', 'Texto de muestra.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ninguno', 'Ext&endido', 'Mosaico F&ijo', '&Mosaico', 'C&entrado',
  // Padding button
  '&Separaci�n...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Color de Relleno', '&Aplicar a:', '&M�s Colores...', '&Separaci�n...',
  'Por favor, seleccione un color',
  // [apply to:] text, paragraph, table, cell
  'texto', 'p�rrafo' , 'tabla', 'celda',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Fuente', 'Fuente', 'Trazado',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fuente:', '&Tama�o', 'Estilo', '&Negrita', '&Cursiva',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Color:',  'Relleno:', '(Defecto)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efectos', 'Subrayado', '&Sobrerrayado', '&Tachado', 'Todo &may�sculas',
  // Sample, Sample text
  'Muestra', 'Texto de Muestra',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espaciado de Car�cter', '&Espaciado:', '&Expandido', '&Condensado',
  // Offset group-box, Offset label, Down, Up,
  'Posici�n', '&Posici�n:', '&Abajo', 'A&rriba',
  // Scaling group-box, Scaling
  'Escalado', 'Esc&alado:',
  // Sub/super script group box, Normal, Sub, Super
  'Subrayados y sobrerrayados','&Normal','Su&brayado','&Sobrerrayado',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Separaci�n', '&Superior:', '&Izquierda:', '&Inferior:', '&Derecha:', '&Valores iguales',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insertar Hiperv�nculo', 'Hiperv�nculo', 'T&exto:', '&Destino:', '<<selecci�n>>',
  // cannot open URL
  'No puede abrirse "%s"',
  // hyperlink properties button, hyperlink style
  '&Personalizar...', 'E&stilo:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Propiedades Hiperv�nculo', 'Colores Normal', 'Colores Activo',
  // Text [color], Background [color]
  '&Texto:', '&Fondo',
  // Underline [color]
  'Subrayado:',
  // Text [color], Background [color] (different hotkeys)
  'T&exto:', 'F&ondo',
  // Underline [color], Attributes group-box,
  'Subrayado:', 'Attributos',
  // Underline type: always, never, active (below the mouse)
  'Subrayado:', 'siempre', 'nunca', 'activo',
  // Same as normal check-box
  'Como &normal',
  // underline active links check-box
  'Link activo S&ubrayado',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insertar S�mbolo', '&Fuente:', '&Juego de Caracteres:', 'Unicode',
  // Unicode block
  '&Bloque Unicode:',  
  // Character Code, Character Unicode Code, No Character
  'Codificaci�n Caracteres: %d', 'Codificaci�n Caracteres: Unicode %d', '(sin car�cter)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Insertar Tabla', 'Tama�o tabla', 'N�mero de &columnas:', 'N�mero de &filas:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Autoajuste', '&Autoajustar al contenido', 'Ajustar tabla a la &ventana', 'Ajustar tabla &manualmente',
  // Remember check-box
  'Recordar &dimensiones para nuevas tablas',
  // VAlign Form ---------------------------------------------------------------
  'Posici�n',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propiedades', 'Imagen', 'Posici�n y tama�o', 'L�nea', 'Tabla', 'Filas', 'Celdas',
  // Image Appearance, Image Misc, Number tabs
  'Apariencia', 'Varios', 'N�mero',
  // Box position, Box size, Box appearance tabs
  'Posici�n', 'Tama�o', 'Apariencia',
  // Preview label, Transparency group-box, checkbox, label
  'Previsualizar:', 'Transparencia', '&Transparente', '&Color Transparente:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'C&ambiar...', '&Salvar...', 'Autom�tico',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alineaci�n Vertical', '&Alineaci�n:',
  'inferior, a la l�nea base del texto', 'centro, a la l�nea base del texto',
  'superior,a la parte superior de la l�nea', 'inferior, a la parte inferior de la l�nea', 'central, al centro de la l�nea',
  // align to left side, align to right side
  'a la izquierda', 'a la derecha',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Cambiar por:', 'Expandido', '&Ancho:', 'A&lto:', 'Tama�o por defecto: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Escalar &proporcionalmente', '&Reset',
  // Inside the border, border, outside the border groupboxes
  'Adentro del borde', 'Borde', 'Afuera del borde',
  // Fill color, Padding (i.e. margins inside border) labels
  'Color de relleno:', 'Espaciado:',
  // Border width, Border color labels
  'Ancho de borde :', '&Color de borde:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espacio &horizontal:', 'Espacio &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Varios', '&Tips:',
  // web group-box, alt text
  'Web', 'Texto alternativo:',
  // Horz line group-box, color, width, style
  'L�nea Horizontal', '&Color:', '&Ancho:', '&Estilo:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabla', '&Ancho:', '&Color de relleno:', '&Espacio entre celdas...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Espaciado &Horizontal de celdas:', 'Espaciado &Vertical de celdas:', 'Bordes y relleno',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Lados de &Borde Visibles...', 'Autom�tico', 'autom�tico',
  // Keep row content together checkbox
  'Mantener contenido junto',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotaci�n', '&Ninguna', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Borde:', 'Lados de b&orde visible...',
  // Border, CellBorders buttons
  '&Borde de la Tabla...', '&Borde de las Celdas...',
  // Table border, Cell borders dialog titles
  'Borde de la Tabla', 'Borde Celdas por defecto',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Imprimiendo', '&Mantener tabla en una p�gina', 'N�mero de filas de &cabecera:',
  'Las filas de cabecera se repetir�n en cada p�gina de la tabla',
  // top, center, bottom, default
  '&Superior', '&Centro', '&Inferior', '&Defecto',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Ajustes', 'Ancho preferido:', '&Alto m�nimo:', '&Color de relleno:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Borde', 'Lados visibles:',  'Color de &sombra:', '&Color destacado', 'C&olor:',
  // Background image
  'I&magen...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Posici�n horizontal', 'Posici�n vertical', 'Posici�n en el texto',
  // Position types: align, absolute, relative
  'Align', 'Posici�n absoluta', 'Posici�n relativa',
  // [Align] left side, center, right side
  'lado izquierdo de caja a lado izquierdo de',
  'centro de caja al centro de',
  'lado derecho de caja a lado derecho de',
  // [Align] top side, center, bottom side
  'lado superior de caja a lado superior de',
  'centro de caja al centro de',
  'lado inferior de caja a lado inferior de',
  // [Align] relative to
  'relativo a',
  // [Position] to the right of the left side of
  'a la derecha del lado izquierdo de',
  // [Position] below the top side of
  'debajo del lado superior de',
  // Anchors: page, main text area (x2)
  '&P�gina', '�rea de texto principal', 'P�gina', '�rea de te&xto principal',
  // Anchors: left margin, right margin
  'Margen izquierdo', 'Margen derecho',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Margen superior', 'Margen inferior', 'Margen &interno', 'Margen externo',
  // Anchors: character, line, paragraph
  '&Caracter', 'L&inea', 'Parrafo',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Marg&enes sim�tricos', 'Dise�o de celda de tabla',
  // Above text, below text
  '&Arriba del texto', 'Debajo del texto',
  // Width, Height groupboxes, Width, Height labels
  'Ancho', 'Alto', 'Ancho:', 'Alto:',
  // Auto height label, Auto height combobox item
  'Autom�tico', 'autom�tico',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alineaci�n vertical', 'Superior', 'Centrada', 'Abajo',
  // Border and background button and title
  'B&ordes y sombreado...', 'Bordes y sombreado de caja',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Pegado Especial', '&Pegar como:', 'Formato RTF', 'Formato HTML',
  'Texto', 'Texto Unicode', 'Imagen Bitmap', 'Imagen Metafile',
  'Ficheros de im�genes',  'URL',
  // Options group-box, styles
   'Opciones', 'E&stilos:',
  // style options: apply target styles, use source styles, ignore styles
   'applcar estilos a documento destino', 'mantener estilos y apariencia',
   'mantener apariencia, ignorar estilos',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Numeraci�n y Vi�etas', 'Vi�etas', 'Numerado', 'Lista con Vi�etas', 'Lista Numerada',
  // Customize, Reset, None
  '&Personalizar...', '&Restablecer', 'Ninguno',
  // Numbering: continue, reset to, create new
  'continuar numerando', 'reiniciar numeraci�n a', 'crear nueva lista, empezar desde',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Ar�bigo grande', 'Romano grande', 'Decimal', 'Ar�bigo peque�o', 'Romano grande',
  // Bullet type
  'C�rculo', 'Disco', 'Cuadrado',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nivel:', '&Iniciar desde:', '&Continuar', 'Numeraci�n',    
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personalizar Listas', 'Niveles', '&Contador:', 'Propiedades de la lista', '&Tipo lista:',
  // List types: bullet, image
  'vi�eta', 'imagen', 'Insertar N�mero|',
  // Number format, Number, Start level from, Font button
  'Formato &n�mero:', 'N�mero', '&Empezar nivel numerando desde:', '&Fuente...',
  // Image button, bullet character, Bullet button,
  '&Imagen...', 'C&aracter vi�eta', '&Vi�eta...',
  // Position of list text, bullet, number, image, text
  'Posici�n lista', 'Posici�n vi�eta', 'Posici�n N�mero', 'Posici�n imagen', 'Posici�n texto',
  // at, left indent, first line indent, from left indent
  '&a:', 'Sangr�a &izquierda:', 'Sangr�a &primera l�nea:', 'desde sangr�a izquierda',
  // [only] one level preview, preview
  'Previsualizaci�n &un nivel', 'Previsualizaci�n',
  // Preview text
  'Texto texto texto texto. Texto texto texto texto. Texto texto texto texto. Texto texto texto texto. Text texto texto texto. Texto texto texto texto.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'alineaci�n izquierda', 'alineaci�n derecha', 'centro',
  // level #, this level (for combo-box)
  'Nivel %d', 'Este Nivel',
  // Marker character dialog title
  'Editar Car�cter Vi�eta',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Borde y Fondo del P�rrafo', 'Borde', 'Fondo',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Ajustes', '&Color:', '&Ancho:', 'Anchura Int&erna:', 'Po&siciones...',
  // Sample, Border type group-boxes;
  'Muestra', 'Tipo de Borde',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ninguno', '&Simple', '&Doble', '&Triple', 'Marco &interno', 'Marco &externo',
  // Fill color group-box; More colors, padding buttons
  'Color de relleno', '&M�s Colores...', '&Separaci�n...',
  // preview text
  'Texto texto texto texto. Texto texto texto texto. Texto texto texto texto.',
  // title and group-box in Offsets dialog
  'Posiciones', 'Posiciones del borde',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'P�rrafo', 'Alineaci�n', '&Izquierda', '&Derecha', '&Centrado', '&Justificado',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espaciado', '&Antes:', '&Despu�s:', 'Espaciado de &l�nea:', 'p&or:',
  // Indents group-box; indents: left, right, first line
  'Sangr�as', 'I&zquierda:', 'De&recha:', '&Primera l�nea:',
  // indented, hanging
  '&Sangrado', '&Colgando', 'Muestra',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simple', '1.5 l�neas', 'Doble',  'Al menos', 'Justo', 'M�ltiple',
  // preview text
  'Texto texto texto texto. Texto texto texto texto. Texto texto texot texto. Texto texto texto texto. Texto texto texto texto. Texto texto texto texto.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Sangr�a y espacio', 'Tabulaciones', 'Posici�n del texto',
  // tab stop position label; buttons: set, delete, delete all
  '&Posici�n de tabulaciones:', '&Establecer', '&Borrar', 'Borrar &Todo',
  // tab align group; left, right, center aligns,
  'Alineaci�n', 'I&zquierda', '&Derecha', '&Centrar',
  // leader radio-group; no leader
  'Relleno', '(Ninguno)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulaciones para borrar:', '(Ninguna)', 'Todas.',
  // Pagination group-box, keep with next, keep lines together
  'Paginaci�n', '&Conservar con el siguiente', '&Conservar l�neas juntas',
  // Outline level, Body text, Level #
  '&Nivel de esquema:', 'Texto de cuerpo ', 'Nivel %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Vista Previa', 'Ancho P�gina', 'P�gina completa', 'P�ginas:',
  // Invalid Scale, [page #] of #
  'Por favor, introduzca un valor entre 10 y 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primera P�gina', 'P�gina Anterior', 'Pr�xima P�gina', '�ltima P�gina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espaciado Celdas', 'Espaciado', 'Entre &celdas', 'Desde el borde de la tabla hasta las celdas',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Bordes', 'Ajustes', '&Color:', 'Color &Resaltado:', '&Color sombra:',
  // Width; Border type group-box;
  '&Ancho:', 'Tipo de borde',
  // Border types: none, sunken, raised, flat
  '&Ninguno', '&Hundido', '&Levantado', '&Plano',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Dividir', 'Dividir a',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&N�mero especificado de filas y columnas', 'Celdas &iniciales',
  // number of columns, rows, merge before
  'N�mero de &columnas:', 'N�mero de &filas:', '&Combinar antes de dividir',
  // to original cols, rows check-boxes
  'Dividir a las co&lumnas iniciales', 'Dividir a las fil&as iniciales',
  // Add Rows form -------------------------------------------------------------
  'A�adir Filas', '&N�mero de filas:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Preparar P�gina', 'P�gina', 'Cabecera y Pi�',
  // margins group-box, left, right, top, bottom
  'M�rgenes (mil�metros)', 'M�rgenes (pulgadas)', '&Izquierda:', '&Superior:', '&Derecha:', '&Inferior:',
  // mirror margins check-box
  '&M�rgenes espejo',
  // orientation group-box, portrait, landscape
  'Orientaci�n', '&Vertical', '&Horizontal',
  // paper group-box, paper size, default paper source
  'Papel', 'Tam&a�o:', '&Origen:',
  // header group-box, print on the first page, font button
  'Cabecera', '&Texto:', '&Cabecera en la primera p�gina', '&Fuente...',
  // header alignments: left, center, right
  '&Izquierda', '&Centro', '&Derecha',
  // the same for footer (different hotkeys)
  'Pi�', 'Te&xto:', 'Pi� en la primera &p�gina', 'F&uente...',
  'Iz&quierda', 'Ce&ntro', 'D&erecha',
  // page numbers group-box, start from
  'N�meros de p�gina', '&Empezar desde:',
  // hint about codes
  'Combinaciones especiales de caracteres:'#13'&&p - n�mero de p�gina; &&P - contador de p�ginas; &&d - fecha actual; &&t - hora actual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'P�gina de c�digos del archivo de texto', '&Elija la codificaci�n de archivos:',
  // thai, japanese, chinese (simpl), korean
  'Tailandesa', 'Japonesa', 'China (Simplificada)', 'Coreana',
  // chinese (trad), central european, cyrillic, west european
  'China (Tradicional)', 'De Europa Central y Oriental', 'Cir�lica', 'De Europa Occidental',
  // greek, turkish, hebrew, arabic
  'Griega', 'Turca', 'Hebrea', 'Ar�biga',
  // baltic, vietnamese, utf-8, utf-16
  'B�ltica', 'Vietnamita', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estilo visual', '&Seleccione estilo:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertir en texto', 'Elija un &delimitador:',
  // line break, tab, ';', ','
  'salto de l�nea', 'tabulaci�n', 'punto y coma', 'coma',
  // Table sort form -----------------------------------------------------------
  // error message
  'No es posible ordenar una tabla con filas combinadas',
  // title, main options groupbox
  'Criterio de ordenaci�n de tabla', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar por la columna:', 'Distinguir &may�sculas de min�sculas',
  // heading row, range or rows
  '&Excluir fila de encabezado', 'Filas a ordenar: desde %d hasta %d',
  // order, ascending, descending
  'Orden', '&Ascendente', '&Descendente',
  // data type, text, number
  'Tipo', '&Texto', '&N�mero',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Insertar N�mero', 'Propiedades', 'Nombre de &Contador:', 'Tipo de &Numeraci�n:',
  // numbering groupbox, continue, start from
  'Numerando', 'C&ontinua', 'Empezar desde:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Leyenda', 'Etiqueta:', '&Excluir etiqueta de la leyenda',
  // position radiogroup
  'Posici�n', '&Arriba del objeto seleccionado', 'A&bajo del objeto seleccionado',
  // caption text
  '&Texto de leyenda:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numerando', 'Figura', 'Tabla',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Lados de borde visibles', 'Borde',
  // Left, Top, Right, Bottom checkboxes
  'Lado izquierdo', 'Lado superior', 'Lado derecho', 'Lado inferior',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Cambiar el tama�o de la columna', 'cambiar el tama�o de la fila',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Sangr�a de primera l�nea', 'Sangr�a izquierda', 'Sangr�a colgada', 'Sangr�a derecha',
  // Hints on lists: up one level (left), down one level (right)
  'Ascender un nivel', 'Descender un nivel',
  // Hints for margins: bottom, left, right and top
  'Margen inferior', 'Margen izquierdo', 'Margen derecho', 'Mergen superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulaci�n izquierda', 'Tabulaci�n derecha', 'Centrar tabulaci�n', 'Tabulaci�n decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Sangria normal', 'Sin espacio', 'Encabezado %d', 'Lista de parrafos',
  // Hyperlink, Title, Subtitle
  'Hyperenlace', 'T�tulo', 'Sub t�tulo',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Resaltado', 'Resaltado ligero', 'Resaltado intenso', 'Fuerte',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cita', 'Cita Intensa', 'Referencia sutil', 'Referencia Intensa',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Bloque de texto', 'Variable HTML', 'C�digo HTML', 'Acr�nimo HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definici�n HTML', 'HTML Keyboard', 'Ejemplo de HTML', ' HTML Sin formato',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformateado', 'Cita HTML', 'Encabezado', 'Pie de p�gina', 'N�mero de p�gina',
  // Caption
  'Leyenda',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Referencia de fin de nota', 'Referencia de pie de nota', 'Texto de nota al final', 'Texto de pie de nota',
  // Sidenote Reference, Sidenote Text
  'Referencia al margen', 'Texto al margen',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'color', 'color de fondo', 'transparente', 'default', 'color de subrayado',
  // default background color, default text color, [color] same as text
  'color de fondo por defecto', 'color de texto por defecto', 'mismo que el texto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'sencilla', 'gruesa', 'doble', 'punteada', 'gruesa punteada', 'discontinua',
  // underline types: thick dashed, long dashed, thick long dashed,
  'gruesa discontinua', 'discontinua larga', 'gruesa discontinua larga',
  // underline types: dash dotted, thick dash dotted,
  'linea punteada', 'linea delgada punteada',
  // underline types: dash dot dotted, thick dash dot dotted
  'doble linea punteada', 'doble linea gruesa punteada',
  // sub/superscript: not, subsript, superscript
  'no sub/superindice', 'subindice', 'superindice',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'Modo bi-di:', 'heredado', 'izquierda a derecha', 'derecha a izquierda',
  // bold, not bold
  'negrillas', 'sin negrillas',
  // italic, not italic
  'italica', 'no italica',
  // underlined, not underlined, default underline
  'subrayado', 'no subrayado', 'subrayado por defecto',
  // struck out, not struck out
  'remarcado', 'no remarcado',
  // overlined, not overlined
  'tachado', 'no tachado',
  // all capitals: yes, all capitals: no
  'Todo a mayusculas', 'Todo a mayusculas (apagado)',
  // vertical shift: none, by x% up, by x% down
  'sin desplazamiento vertical', 'desplazamiento por %d%% arriba', 'desplazamiento por %d%% abajo',
  // characters width [: x%]
  'ancho de caracter',
  // character spacing: none, expanded by x, condensed by x
  'espaciado normal de caracter', 'espaciado expandido por %s', 'espaciado comprimido por %s',
  // charset -> NOTE: charset <> script
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'fuente por defecto', 'parrafo por defecto', 'heredado',
  // [hyperlink] highlight, default hyperlink attributes
  'resaltado', 'por defecto',
  // paragraph aligmnment: left, right, center, justify
  'alineaci�n izquierda', 'alineaci�n derecha', 'centrado', 'justificado',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'alto de linea: %d%%', 'espacio entre lineas: %s', 'alto de linea: al menos %s', 'alto de linea: exacto %s',
  // no wrap, wrap
  'desabilitar llenar linea', 'llenar linea',
  // keep lines together: yes, no
  'conservar lineas juntas', 'no conservar lineas juntas',
  // keep with next: yes, no
  'conservar con parrafo siguiente', 'no conservar con parrafo siguiente',
  // read only: yes, no
  'solo lectura', 'editable',
  // body text, heading level x
  'outline level: body text', 'outline level: %d',
  // indents: first line, left, right
  'sangria primera linea', 'sangria izquierda', 'sangria derecha',
  // space before, after
  'Espaciado anterior', 'Espaciado posterior',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulaci�n', 'ninguna', 'alinaci�n', 'izquierda', 'derecha', 'centro',
  // tab leader (filling character)
  'caracter de relleno',
  // no, yes
   'no', 'si',
  // [padding/spacing/side:] left, top, right, bottom
  'izquierdo', 'arriba', 'derecho', 'abajo',
  // border: none, single, double, triple, thick inside, thick outside
  'ninguno', 'sencillo', 'doble', 'triple', 'borde interno', 'borde externo',
  // background, border, padding, spacing [between text and border]
  'sombreado', 'borde', 'mergen', 'espaciado',
  // border: style, width, internal width, visible sides
  'estilo', 'ancho', 'ancho interno', 'lados visibles',
  // style inspector -----------------------------------------------------------
  // title
  'Inspector de estilos',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estilo', '(Ninguno)', 'Parrafo', 'Fuente', 'Attributos',
  // border and background, hyperlink, standard [style]
  'Bordes y sombreado', 'Hyperenlace', '(Estandar)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estilos', 'Estilo',
  // name, applicable to,
  '&Nombre:', 'Aplicar a:',
  // based on, based on (no &),
  '&Basado en:', 'Basado en:',
  //  next style, next style (no &)
  'Estilo siguiente parrafo::', 'Estilo del siguiente parrafo:',
  // quick access check-box, description and preview
  'Acceso rapido', 'Descripci�n y &previsualizaci�n:',
  // [applicable to:] paragraph and text, paragraph, text
  'parrafo y texto', 'parrafo', 'texto',
  // text and paragraph styles, default font
  'Estilo de texto y parrafo', 'Fuente por defecto',
  // links: edit, reset, buttons: add, delete
  'Editar', 'Reset', '&Agregar', 'Borrar',
  // add standard style, add custom style, default style name
  'Agregar Estilo &Standar...', 'Agregar Estilo Personal', 'Estilo %d',
  // choose style
  'Escoger e&stilo:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Estilos RichView  (*.rvst)|*.rvst', 'Error cargando archivo', 'Archivo sin estilos',
  // Title, group-box, import styles
  'Importar Estilo desde archivo', 'Importar', 'I&mportar estilo:',
  // existing styles radio-group: Title, override, auto-rename
  'Existing styles', '&override', '&add renamed',
  // Select, Unselect, Invert,
  '&Seleccionar', '&Deseleccionar', '&Invertir',
  // select/unselect: all styles, new styles, existing styles
  '&Todos los estilos', 'Estilo &nuevo', 'Estilo &Existente',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Limpiar formato', 'Todos los estilos',
  // dialog title, prompt
  'Estilos', 'Es&coger estilo a aplicar:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Sin�nimos', '&Omitir todas', '&Agregar al diccionario',
  // Progress messages ---------------------------------------------------------
  'Descargando %s', 'Preparando para imprimir...', 'Imprimiendo p�gina %d',
  'Convirtiendo desde RTF...',  'Convirtiendo hacia RTF...',
  'Leyendo %s...', 'Escribiendo %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Sin nota', 'Nota al pie', 'Nota al final', 'Nota al margen', 'Caja de texto'
  );


initialization
  RVA_RegisterLanguage(
    'Spanish', // english language name, do not translate
    'Espa�ol', // native language name
    ANSI_CHARSET, @Messages);
    
end.

