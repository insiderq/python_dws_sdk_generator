﻿// This file is a copy of RVAL_ChineseBig5.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Traditional Chinese (Big5) translation          }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by Ian Chen                                }
{ Updated: 2007-11-09                                   }
{ Updated: 2011-10-18 by Alconost                       }
{ Updated: 2012-10-04 by Alconost                       }
{ Updated: 2014-04-26 by Alconost                       }
{*******************************************************}

unit RVALUTF8_ChineseBig5;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  '英吋', '公分', '公釐', 'pica (1/6 英吋)', '像素', '點數',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', '公分', '公釐', 'pc', '點數'{?}, '點',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '檔案(&F)', '編輯(&E)', '格式(&O)', '字型(&N)', '段落(&P)', '插入(&I)', '表格(&T) ', '視窗(&W)', '說明(&H)',
  // exit
  '離開(&X)',
  // top-level menus: View, Tools,
  '檢視(&V)', '工具(&L)',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '大小', '樣式', '選擇(&T)', '表格內容對齊(&N)', '表格框線(&E)',
  // menus: Table cell rotation
  '儲存格旋轉(&R)', 
  // menus: Text flow, Footnotes/endnotes
  '直書/橫書(&T)', '註腳(&F)',
  // ribbon tabs: tab1, tab2, view, table
  '首頁(&H)', '進階(&A)', '檢視(&V)', '表格(&T)',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '剪貼簿', '字型', '段落', '清單', '編輯',
  // ribbon groups: insert, background, page setup,
  '插入', '背景', '頁面設定',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  '連結', '註腳', '文件顯示', '顯示/隱藏', '縮放', '選項',
  // ribbon groups on table tab: insert, delete, operations, borders, styles
  '插入', '刪除', '作業', '框線',
  // ribbon groups: styles 
  '樣式',
  // ribbon screen tip footer,
  '按 F1 以取得更多說明',
  // ribbon app menu: recent files, save-as menu title, print menu title
  '最近使用的文件', '儲存一份文件副本', '預覽並列印文件',
  // ribbon label: units combo
  '單位:',      
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '新增(&N)', '新增|新增空白文件',
  // TrvActionOpen
  '開啟舊檔(&O)...', '開啟舊檔|由磁碟開啟文件',
  // TrvActionSave
  '儲存檔案(&S)', '儲存檔案|儲存文件至磁碟',
  // TrvActionSaveAs
  '另存新檔(&A)...', '另存新檔...|以新檔名儲存文件至磁碟',
  // TrvActionExport
  '輸出(&E)...', '輸出|輸出文件至其他樣式',
  // TrvActionPrintPreview
  '預覽列印(&R)', '預覽列印|檢視文件列印之狀態',
  // TrvActionPrint
  '列印(&P)...', '列印|印表設定及列印文件',
  // TrvActionQuickPrint
  '列印(&P)', '快速列印(&Q)', '列印|列印文件',
  // TrvActionPageSetup
  '頁面設定(&G)...', '頁面設定|設定邊界、紙張大小、輸出方向、來源、頁首及頁尾',
  // TrvActionCut
  '剪下(&T)', '剪下|剪下選擇範圍至剪貼簿',
  // TrvActionCopy
  '複製(&C)', '複製|複製選擇範圍至剪貼簿',
  // TrvActionPaste
  '貼上(&P)', '貼上|貼上（插入）剪貼簿內容',
  // TrvActionPasteAsText
  '貼上為文字(&T)', '貼上為文字|從剪貼簿插入文字',  
  // TrvActionPasteSpecial
  '選擇性貼上(&S)...', '選擇性貼上|以特殊格式貼上（插入）剪貼簿內容',
  // TrvActionSelectAll
  '全選(&A)', '全選|選擇全部文件',
  // TrvActionUndo
  '復原(&U)', '復原|復原上一個動作',
  // TrvActionRedo
  '重做(&R)', '重做|重做上一個復原動作',
  // TrvActionFind
  '尋找(&F)...', '尋找|在文件中尋找特定文字',
  // TrvActionFindNext
  '尋找下一個(&N)', '尋找下一個|繼續上一個搜尋',
  // TrvActionReplace
  '取代(&R)...', '取代|尋找並取代文件中的特定文字',
  // TrvActionInsertFile
  '插入檔案(&F)...', '插入檔案|在文件中插入檔案內容',
  // TrvActionInsertPicture
  '圖片(&P)...', '插入圖片|由磁碟中插入圖片',
  // TRVActionInsertHLine
  '水平線(&H)', '插入水平線|插入一條水平線',
  // TRVActionInsertHyperlink
  '超連結(&L)...', '插入超連結|插入超連結文字',
  // TRVActionRemoveHyperlinks
  '移除超連結(&R)', '移除超連結|移除所選文字中的所有超連結',  
  // TrvActionInsertSymbol
  '符號(&S)...', '插入符號|插入符號',
  // TrvActionInsertNumber
   '編號(&N)...', '插入編號|插入編號',
  // TrvActionInsertCaption
   '插入標題(&C)...', '插入標題|插入所選物件的標題',
  // TrvActionInsertTextBox
   '文字方塊(&T)', '插入文字方塊|插入文字方塊',
  // TrvActionInsertSidenote
   '旁註(&S)', '插入旁註|插入顯示於文字方塊的附註',
  // TrvActionInsertPageNumber
   '頁碼(&P)', '插入頁碼|插入頁碼',
  // TrvActionParaList
  '項目符號及編號(&B)...', '項目符號及編號|新增或編輯所選擇段落之項目符號或編號',
  // TrvActionParaBullets
  '項目符號(&B)', '項目符號|新增或移除段落之項目符號',
  // TrvActionParaNumbering
  '編號(&N)', '編號|新增或移除段落之編號',
  // TrvActionColor
  '背景顏色(&C)...', '背景|改變文件的背景顏色',
  // TrvActionFillColor
  '填入色彩(&F)...', '貼入色彩|改變文字、段落、儲存格或文件的背景顏',
  // TrvActionInsertPageBreak
  '插入分頁符號(&I)', '插入分頁符號|插入分頁符號',
  // TrvActionRemovePageBreak
  '移除分頁符號(&R)', '移除分頁符號|移除分頁符號',
  // TrvActionClearLeft
  '在左側清除文字流向(&L)', '在左側清除文字流向|將此段落置於靠左對齊的圖片下方',   // TrvActionClearRight
  '在右側清除文字流向(&L)', '在右側清除文字流向|將此段落置於靠右對齊的圖片下方',
  // TrvActionClearBoth
  '在兩側清除文字流向(&B)', '在兩側清除文字流向|將此段落置於任何靠左或靠右對齊的文字下方',
  // TrvActionClearNone
  '正常文字流向(&N)', '正常文字流向|允許文字圍繞著靠左和靠右對齊的圖片',
  // TrvActionVAlign
  '物件位置(&O)...', '物件位置|變更所選物件的位置',    
  // TrvActionItemProperties
  '物件屬性(&P)...', '物件屬性|定義作用中物件的屬性',
  // TrvActionBackground
  '背景(&B)...', '背景|選擇背景顏色與圖片',
  // TrvActionParagraph
  '段落(&P)...', '段落|變更段落屬性',
  // TrvActionIndentInc
  '增加縮排(&I)', '增加縮排|增加所選段落之左邊縮排',
  // TrvActionIndentDec
  '減少縮排(&D)', '減少縮排|減少所選擇段落之左邊縮排',
  // TrvActionWordWrap
  '文字換行分隔設定(&W)', '文字換行分隔設定|為所選擇的段落加入文字換行分隔設定',
  // TrvActionAlignLeft
  '靠左對齊(&L)', '靠左對齊|將所選擇的文字靠左對齊',
  // TrvActionAlignRight
  '靠右對齊(&R)', '靠右對齊|將所選擇的文字靠右對齊',
  // TrvActionAlignCenter
  '置中(&C)', '置中|將所選擇文字置中',
  // TrvActionAlignJustify
  '左右對齊(&J)', '左右對齊|將所選擇的文字左右邊界對齊',
  // TrvActionParaColor
  '段落背景色彩(&B)...', '段落背景色彩|設定段落之背景色彩',
  // TrvActionLineSpacing100
  '單行間距(&S)', '單行間距|設定單行間距',
  // TrvActionLineSpacing150
  '1.5 倍行高(&N)', '1.5 倍行高|設定間距為 1.5 倍行高',
  // TrvActionLineSpacing200
  '2 倍行高(&D)', '2 倍行高|設定 2 倍行高',
  // TrvActionParaBorder
  '段落邊框與背景(&B)...', '段落邊框與背景|設定所選擇段落的邊框與背景',
  // TrvActionInsertTable
  '插入表格(&I)...', '表格(&T)', '插入表格|插入一個新表格',
  // TrvActionTableInsertRowsAbove
  '插入上方列(&A)', '插入上方列|於所選擇儲存格上方插入一列',
  // TrvActionTableInsertRowsBelow
  '插入下方列(&B)', '插入下方列|於所選擇儲存格下方插入一列',
  // TrvActionTableInsertColLeft
  '插入左欄(&L)', '插入左欄|於所選擇儲存格左方插入一欄',
  // TrvActionTableInsertColRight
  '插入右欄(&R)', '插入右欄|於所選擇儲存格右方插入一欄',
  // TrvActionTableDeleteRows
  '刪除整列(&O)', '刪除整列|刪除所選擇儲存格的整列',
  // TrvActionTableDeleteCols
  '刪除整欄(&C)', '刪除整欄|刪除所選擇儲存格的整欄',
  // TrvActionTableDeleteTable
  '刪除表格(&D)', '刪除表格|刪除表格',
  // TrvActionTableMergeCells
  '合併儲存格(&M)', '合併儲存格|合併所選擇的儲存格',
  // TrvActionTableSplitCells
  '分割儲存格(&S)...', '分割儲存格|分割所選擇的儲存格',
  // TrvActionTableSelectTable
  '選擇表格(&T)', '選擇表格|選擇表格',
  // TrvActionTableSelectRows
  '選擇列(&W)', 'S選擇列|選擇列',
  // TrvActionTableSelectCols
  '選擇欄(&U)', '選擇欄|選擇欄',
  // TrvActionTableSelectCell
  '選擇儲存格(&E)', '選擇儲存格|選擇儲存格',
  // TrvActionTableCellVAlignTop
  '儲存格靠上對齊(&T)', '儲存格靠上對齊|儲存格內容靠上對齊',
  // TrvActionTableCellVAlignMiddle
  '儲存格置中對齊(&M)', '儲存格置中對齊|儲存格內容置中對齊',
  // TrvActionTableCellVAlignBottom
  '儲存格靠底對齊(&B)', '儲存格靠底對齊|儲存格內容靠底對齊',
  // TrvActionTableCellVAlignDefault
  '預設的儲存格垂直對齊(&D)', '預設的儲存格垂直對齊|設定預設的垂直對齊於所選擇的儲存格',
  // TrvActionTableCellRotationNone
  '無儲存格旋轉(&N)', '無儲存格旋轉|以 0 度旋轉儲存格內容',
  // TrvActionTableCellRotation90
  '90 度旋轉儲存格(&2)', '90 度旋轉儲存格|以 90 度旋轉儲存格內容',
  // TrvActionTableCellRotation180
  '180 度旋轉儲存格(&2)', '180 度旋轉儲存格|以 180 度旋轉儲存格內容',
  // TrvActionTableCellRotation270
  '270 度旋轉儲存格(&2)', '270 度旋轉儲存格|以 270 度旋轉儲存格內容',
  // TrvActionTableProperties
  '表格屬性(&P)...', '表格屬性|改變所選擇表格的屬性',
  // TrvActionTableGrid
  '顯示格線(&G)', '顯示格線|顯示或隱藏格線',
  // TRVActionTableSplit
  '分割表格(&L)', '分割表格|將表格從所選列分割為兩個表格',
  // TRVActionTableToText
  '轉換為文字(&X)...', '轉換為文字|將表格轉換為文字',
  // TRVActionTableSort
  '排序(&S)...', '排序|排序表格列',
  // TrvActionTableCellLeftBorder
  '左框線(&L)', '左框線|顯示或隱藏儲存左框線',
  // TrvActionTableCellRightBorder
  '右框線(&R)', '右框線|顯示或隱藏右框線',
  // TrvActionTableCellTopBorder
  '上框線(&T)', '上框線|顯示或隱藏上框線',
  // TrvActionTableCellBottomBorder
  '底框線(&B)', '底框線|顯示或隱藏底框線',
  // TrvActionTableCellAllBorders
  '框線(&A)', '框線|顯示或隱藏儲存格全部框線',
  // TrvActionTableCellNoBorders
  '無框線(&N)', '無框線|隱藏儲存格全部框線',
  // TrvActionFonts & TrvActionFontEx
  '字型(&F)...', '字型|變更所選擇文字的字型',
  // TrvActionFontBold
  '粗體(&B)', '粗體|變更所選擇文字為粗體',
  // TrvActionFontItalic
  '斜體(&I)', '斜體|變更所選擇文字為斜體',
  // TrvActionFontUnderline
  '底線(&U)', '底線|變更所選擇的文字為添加底線',
  // TrvActionFontStrikeout
  '刪除線(&S)', '刪除線|為所選擇文字加刪除線',
  // TrvActionFontGrow
  '加大字型(&G)', '加大字型|加大所選擇文字之高度 10%',
  // TrvActionFontShrink
  '縮小字型(&H)', '縮小字型|縮小所選擇文字之高度 10%',
  // TrvActionFontGrowOnePoint
  '加大字型一點(&R)', '加大字型一點|加大所選擇文字之高度一點',
  // TrvActionFontShrinkOnePoint
  '縮小字型一點(&N)', '縮小自型一點|縮小所選擇文字之高度一點',
  // TrvActionFontAllCaps
  '全部大寫(&A)', '全部大寫|變更所選擇文字為大寫字',
  // TrvActionFontOverline
  '上標線(&O)', '上標線|為所選擇文字加上標線',
  // TrvActionFontColor
  '文字色彩(&C)...', '文字色彩|變更所選擇文字的色彩',
  // TrvActionFontBackColor
  '文字底色(&K)...', '文字底色|改變所選擇文字的底色',
  // TrvActionAddictSpell3
  '拼字檢查(&S)', '拼字檢查|檢查拼字',
  // TrvActionAddictThesaurus3
  '同義字(&T)', '同義字|提供所選擇文字之同義字',
  // TrvActionParaLTR
  '由左至右', '由左至右|設定所選擇段落之文字由左至右顯示',
  // TrvActionParaRTL
  '由右至左', '由右至左|設定所選擇段落之文字由右至左顯示',
  // TrvActionLTR
  '文字由左至右', '文字由左至右|設定所選擇的文字由左至右顯示',
  // TrvActionRTL
  '文字由右至左', '文字由右至左|設定所選擇的文字由右至左顯示',
  // TrvActionCharCase
  '大小寫', '大小寫|變更所選擇文字的大小寫',
  '非列印字元(&N)', '非列印字元|顯示或隱藏非列印字元，例如段落標記、標示符號及空白等',
  // TrvActionSubscript
  '下標字(&B)', '下標符號|轉換選擇的文字為下標字',
  // TrvActionSuperscript
  '上標字', '上標字|轉換所選的文字為上標字',
  // TrvActionInsertFootnote
  '註腳(&F)', '註腳|插入註腳',
  // TrvActionInsertEndnote
  '章節附註(&E)', '章節附註|插入章節附註',
  // TrvActionEditNote
  '編輯附註(&D)', '編輯附註|開始編輯註腳或章節附註',
  '隱藏(&H)', '隱藏|隱藏或顯示所選片段',    
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '樣式(&S)...', '樣式|開啟樣式管理對話方塊',
  // TrvActionAddStyleTemplate
  '新增樣式(&A)...', '新增樣式|建立新文字或段落樣式',
  // TrvActionClearFormat,
  '清除格式(&C)', '清除格式|清除所選片段的所有文字與段落格式設定',
  // TrvActionClearTextFormat,
  '清除文字格式(&T)', '清除文字格式|清除所選文字的所有格式設定',
  // TrvActionStyleInspector
  '樣式檢查(&I)', '樣式檢查|顯示或隱藏 [樣式檢查]',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   '確定', '取消', '關閉', '插入', '開啟舊檔(&O)...', '儲存檔案(&S)...', '清除(&C)', '說明', '移除',
  // Others  -------------------------------------------------------------------
  // percents
  '百分比',
  // left, top, right, bottom sides
  '左邊', '上邊', '右邊', '底邊',
  // save changes? confirm title
  '儲存所做的變更至 %s?', '確認',
  // warning: losing formatting
  '%s 可能包含與所選擇的儲存格式不相容的特性.'#13+
  '你確定要使用此格式來儲存文件嗎?',
  // RVF format name
  'RichView 格式',
  // Error messages ------------------------------------------------------------
  '錯誤',
  '檔案讀取錯誤.'#13#13'可能原因:'#13'- 應用程式未支援此檔案格式;'#13+
  '- 檔案本身的毀損;'#13'- 檔案已被其他應用程式開啟或閉鎖.',
  '圖檔讀取錯誤.'#13#13'可能原因:'#13'- 應用程式未支援此檔案包含的圖檔格式;'#13+
  '- 此檔案未包含任何圖檔;'#13+
  '- 檔案本身的毀損;'#13'- 檔案已被其他應用程式開啟或閉鎖.',
  '儲存檔案錯誤.'#13#13'可能原因:'#13'- 磁碟空間不足;'#13+
  '- 磁碟為防寫狀態;'#13'- 行動式儲存裝置未插入;'#13+
  '- 檔案已被其他應用程式開啟或閉鎖;'#13'- 磁碟儲存裝置本身的損壞.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView 檔案 (*.rvf)|*.rvf',  'RTF 檔案 (*.rtf)|*.rtf' , 'XML 檔案 (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  '純文字檔 (*.txt)|*.txt', '文字檔 - 標準編碼 (*.txt)|*.txt', '文字檔 - 自動偵測 (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  '網頁檔 (*.htm;*.html)|*.htm;*.html', '網頁 (*.htm;*.html)|*.htm;*.html',
  '網頁 - 簡式 (*.htm;*.html)|*.htm;*.html',
  // DocX
   'Microsoft Word 文件 (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  '搜尋完成', '搜尋字串 ''%s'' 無符合結果.',
  // 1 string replaced; Several strings replaced
  '1 個字串被取代.', '%d 個字串被取代.',
  // continue from beginning? end?
  // continue search from the beginning/end?
  '已捲至文件的結尾，要自文件開頭繼續目前的動作嗎?',
  '已捲至文件的開頭，要自文件結尾繼續目前的動作嗎??',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  '透明', '自動選取',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '黑', '棕', '橄欖綠', '深綠', '深藍綠', '深藍', '綻青', '灰度-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  '深紅', '橘黃', '暗黃', '綠', '藍綠', '藍', '藍-灰', '灰度-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  '紅', '淡橘', '淡黃綠', '海水綠', '海藍', '淡藍', '藍紫', '灰度-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  '粉紅', '金', '黃', '亮綠', '青綠', '天空藍', '紫紅', '灰度-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  '玫瑰紅', '褐', '淡黃', '淡綠', '淡青綠', '慘藍', '淡紫', '白',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '透明(&A)', '自動選取(&A)', '更多顏色(&M)...', '預設(&D)',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  '背景', '顏色(&O):', '位置', '背景', '範本文字.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '無(&N)', '拉長(&R)', '固定實心(&I)', '實心(&T)', '置中(&E)',
  // Padding button
  '與邊框距離(&P)...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  '填入色彩', '使用於(&A):', '更多色彩(&M)...', '與邊框距離(&P)...',
  '請選擇一種顏色',
  // [apply to:] text, paragraph, table, cell
  '文字', '段落' , '表格', '儲存格',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  '字型', '字型', '外觀',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '字型(&F):', '大小(&S)', '樣式', '粗體(&B)', '斜體(&I)',
  // Script, Color, Back color labels, Default charset
  '書寫體(&R):', '色彩(&C):', '背景(&K):', '(預設)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  '效果', '底線(&U)', '上標線(&O)', '刪除線(&T)', '大寫字(&A)',
  // Sample, Sample text
  '範本', '範本文字',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  '文字間距', '間距(&S):', '擴大(&E)', '縮小(&C)',
  // Offset group-box, Offset label, Down, Up,
  '退縮', '退縮(&O):', '向下(&D)', '向上(&U)',
  // Scaling group-box, Scaling
  '水平伸縮', '伸縮級數(&A):',
  // Sub/super script group box, Normal, Sub, Super
  '下標字與上標字', '正常(&N)', '下標字(&B)', '上標字(&P)',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  '與邊框距離', '上(&T):', '左(&L):', '底(&B):', '右(&R):', '相等值(&E)',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  '插入超連結', '超連結', '文字(&E):', '目標(&T)', '<<選擇>>',
  // cannot open URL
  '無法瀏覽至 "%s"',
  // hyperlink properties button, hyperlink style
  '自訂(&C)...', '樣式(&S):',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) Colors
  '超連結屬性', '一般狀態的顏色', '作用中狀態的顏色',
  // Text [color], Background [color]
  '文字(&T):', '背景(&B)',
  // Underline [color]
  '底線(&N):', 
  // Text [color], Background [color] (different hotkeys)
  '文字(&E):', '背景(&A)',
  // Underline [color], Attributes group-box,
  '底線(&N):', '屬性',
  // Underline type: always, never, active (below the mouse)
  '底線(&U):', '永遠', '絕不', '作用中',
  // Same as normal check-box
  '如同一般(&N)',
  // underline active links check-box
  '將作用中連結加上底線(&U)',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  '插入符號', '字型(&F):', '字集(&C):', '標準編碼',
  // Unicode block
  '區塊:(&B)',  
  // Character Code, Character Unicode Code, No Character
  '字集編碼: %d', '字集編碼: 標準編碼 %d', '(無字集)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  '插入表格', '表格大小', '欄數(&C):', '列數(&R):',
  // Table layout group-box, Autosize, Fit, Manual size,
  '表格外觀', '自動大小(&A)', '調整表格至視窗大小(&W)', '手動調整表格大小(&M)',
  // Remember check-box
  '記憶規格值給新表格使用(&D)',
  // VAlign Form ---------------------------------------------------------------
  '位置',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  '屬性', '圖檔', '位置與大小', '直線', '表格', '列', '儲存格',
  // Image Appearance, Image Misc, Number tabs
   '外觀', '其他', '編號',
  // Box position, Box size, Box appearance tabs
   '位置', '大小', '外觀',
  // Preview label, Transparency group-box, checkbox, label 
  '預覽:', '透明', '透明(&T)', '透明色彩(&C):',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '變更(&H)...', '儲存(&S)...', '自動選取',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  '垂直對齊', '對齊(&A):',
  '文字基線底部', '文字基線中間',
  '靠上對齊', '靠下對齊', '置中對齊',
  // align to left side, align to right side
  '左側', '右側',    
  // Shift By label, Stretch group box, Width, Height, Default size
  '位移(&S):', '擴大', '寬度(&W):', '高度(&H):', '預設大小: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
   '按比例縮放(&P)', '重設(&R)',
  // Inside the border, border, outside the border groupboxes
   '內框線', '框線', '外框線',
  // Fill color, Padding (i.e. margins inside border) labels
   '填滿色彩(&F):', '與邊框距離(&P):',
  // Border width, Border color labels
   '框線寬度(&W):', '框線色彩(&C):',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
   '水平間距(&H):', '垂直間距(&V):',
  // Miscellaneous groupbox, Tooltip label
   '其他', '工具提示(&T):',
  // web group-box, alt text
  '網路', '替代文字(&T):',
  // Horz line group-box, color, width, style
  '水平線', '色彩(&C):', '寬度(&W):', '樣式(&S):',
  // Table group-box; Table Width, Color, CellSpacing,
  '表格', '寬度(&W):', '填入色彩(&F):', '儲存格間距(&C)...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
   '水平儲存格邊框距離(&H):', '垂直儲存格邊框距離(&V):', '框線與背景',
  // Visible table border sides button, auto width (in combobox), auto width label
   '顯示框線四周(&B)...', '自動', '自動',
  // Keep row content together checkbox
   '讓內容集中(&K)',
  // Rotation groupbox, rotations: 0, 90, 180, 270
   '旋轉', '無(&N)', '90 度(&9)', '180 度(&1)', '270 度(&2)',
  // Cell border label, Visible cell border sides button,
   '框線:', '顯示框線四周(&O)...',
  // Border, CellBorders buttons
  '表格邊框(&T)...', '儲存格邊框(&C)...',
  // Table border, Cell borders dialog titles
  '表格邊框', '預設儲存格邊框',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  '列印中', '保持表格在同一頁面(&K)', '表頭列數(&H):',
  '複製表頭列於每格表格每一頁',
  // top, center, bottom, default
  '上(&T)', '中(&C)', '底(&B)', '預設(&D)',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  '設定', '偏好寬度(&W):', '最小高度(&H):', '填入色彩(&F):',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '邊框', '可見邊:',  '陰影顏色(&O):', '亮面顏色(&L)', '顏色(&O):',
  // Background image
  '圖片(&I)...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
   '水平位置', '垂直位置', '在文字中的位置',
  // Position types: align, absolute, relative
   '對齊', '絕對位置', '相對位置',
  // [Align] left side, center, right side
   '方塊左側與下列項目左側',
   '方塊中央與下列項目中央',
   '方塊右側與下列項目右側',
  // [Align] top side, center, bottom side
   '方塊上方與下列項目上方',
   '方塊中央與下列項目中央',
   '方塊下方與下列項目下方',
  // [Align] relative to
   '相對於',
  // [Position] to the right of the left side of
   '下列項目左側的右邊',
  // [Position] below the top side of
   '下列項目上方的下方',
  // Anchors: page, main text area (x2)
   '頁面(&P)', '主要文字區域(&M)', '頁面(&A)', '主要文字區域(&X)',
  // Anchors: left margin, right margin
   '左邊界(&L)', '右邊界(&R)',
  // Anchors: top margin, bottom margin, inner margin, outer margin
   '上邊界(&T)', '下邊界(&B)', '內邊界(&I)', '外邊界(&O)',
  // Anchors: character, line, paragraph
   '字元(&C)', '行(&I)', '段落(&G)',
  // Mirrored margins checkbox, layout in table cell checkbox
   '邊界左右對稱(&E)', '表格儲存格中的版面配置(&Y)',
  // Above text, below text
   '在文字上方(&A)', '在文字下方(&B)',
  // Width, Height groupboxes, Width, Height labels
   '寬度', '高度', '寬度(&W):', '高度(&H):',
  // Auto height label, Auto height combobox item
   '自動', '自動',
  // Vertical alignment groupbox; alignments: top, center, bottom
   '垂直對齊', '靠上(&T)', '置中(&C)', '靠下(&B)',
  // Border and background button and title
   '框線與背景(&O)...', '方塊框線與背景',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  '選擇性貼上', '貼上為(&P):', 'Rich text 格式', '網頁格式',
  '純文字', '標準編碼文字', '點矩陣向量圖片', 'Metafile 圖片',
  '圖形檔案',  'URL',
  // Options group-box, styles
  '選項', '樣式(&S):',
  // style options: apply target styles, use source styles, ignore styles
  '套用目標文件的樣式', '保持樣式與外觀',
  '保持外觀，忽略樣式',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  '項目符號與編號', '已編項目符號', '已編號', '項目符號清單', '編號清單',
  // Customize, Reset, None
  '自訂(&C)...', '重設(&R)', '無',
  // Numbering: continue, reset to, create new
  '接續前一編號清單', '重新開始編號', '建立新的編號清單, 起始值為',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  '大寫希臘文', '大寫羅馬文', '小數', '小寫希臘文', '小寫羅馬文',
  // Bullet type
  '空心圓', '實心圓', '方塊',
  // Level, Start from, Continue numbering, Numbering group-box
  '層數(&L):', '起點(&S):', '繼續(&C)', '編號',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  '自訂清單', '層級', '層級數(&C):', '清單屬性', '清單型態(&L):',
  // List types: bullet, image
  '項目符號', '圖檔', '插入數字|',
  // Number format, Number, Start level from, Font button
  '編號格式(&N):', '編號', '層級起始值(&S):', '字型(&F)...',
  // Image button, bullet character, Bullet button,
  '圖檔(&I)...', '項目符號文字(&H)', '項目符號(&B)...',
  // Position of list text, bullet, number, image, text
  '清單文字位置', '項目符號位置', '編號位置', '圖檔位置', '文字位置',
  // at, left indent, first line indent, from left indent
  '在(&A):', '左邊縮排(&E):', '第一行縮排(&F):', '由左邊縮排',
  // [only] one level preview, preview
  '一層預覽(&O)', '預覽',
  // Preview text
  '文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  '靠左對齊', '靠右對齊', '置中',
  // level #, this level (for combo-box)
  '層級 %d', '這一層級',
  // Marker character dialog title
  '編輯項目符號文字',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  '段落邊框與背景', '邊框', '背景',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  '設定', '顏色(&C):', '寬度(&W):', '內部寬度(&E):', '退縮(&F)...',
  // Sample, Border type group-boxes;
  '範本', '框線型態',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '無(&N)', '單一(&S)', '雙重(&D)', '三重(&T)', '內邊加粗(&I)', '外邊加粗(&O)',
  // Fill color group-box; More colors, padding buttons
  '填入色彩', '更多色彩(&M)...', '與邊框距離(&P)...',
  // preview text
  '文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字.',
  // title and group-box in Offsets dialog
  '退縮', '邊框退縮',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  '段落', '對齊', '靠左(&L)', '靠右(&R)', '置中(&C)', '左右對齊(&J)',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  '間距', '在前(&B):', '在後(&A):', '行距(&S):', '為(&Y):',
  // Indents group-box; indents: left, right, first line
  '縮排', '左邊(&E):', '右邊(&G):', '第一行(&F):',
  // indented, hanging
  '縮排(&I)', '(&H)凸排', '範本',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '單行', '1.5 倍行高', '雙倍行高', '最小行高', '固定行高', '多倍行高',
  // preview text
  '文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字. 文字 文字 文字 文字 文字.',
  // tabs: Indents and spacing, Tabs, Text flow
  '縮排與間隙', '標示符號', '文字串',
  // tab stop position label; buttons: set, delete, delete all
  '標示停止位置(&T):', '設定(&S)', '刪除(&D)', '全部刪除(&A)',
  // tab align group; left, right, center aligns,
  '對齊', '靠左(&L)', '靠右(&R)', '置中(&C)',
  // leader radio-group; no leader
  '前置符號', '(無)',
  // tab stops to be deleted, delete none, delete all labels
  '欲刪除的標示停止符號:', '(無)', '全部',
  // Pagination group-box, keep with next, keep lines together
  '分頁', '與下段同頁(&K)', '段落中不分頁(&K)',
  // Outline level, Body text, Level #
  '大綱階層(&O):', '本文', '第 %d 層',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  '預覽列印', '頁面寬度', '整頁', '頁數:',
  // Invalid Scale, [page #] of #
  '請輸入 10 至 500 之間的數字', '之 %d',
  // Hints on buttons: first, prior, next, last pages
  '第一頁', '上一頁', '下一頁', '上一頁',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  '儲存格間距', '間距', '儲存格之間', '表格邊框至儲存格',
  // vertical, horizontal (x2)
  '垂直:(&V)', '水平:(&H)', '垂直:(&V)', '水平:(&H)', 
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  '邊框', '設定', '顏色(&C):', '亮面顏色(&L):', '陰影顏色(&C):',
  // Width; Border type group-box;
  '寬度(&W):', '框線型態',
  // Border types: none, sunken, raised, flat
  '無(&N)', '下沈(&S)', '上凸(&R)', '平滑(&F)',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  '分割', '分割至',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '選定的列與欄數(&S)', '原儲存格(&O)',
  // number of columns, rows, merge before
  '欄數(&C):', '列數(&R):', '分割前先合併(&M)',
  // to original cols, rows check-boxes
  '分割至原來欄位(&L)', '分割至原來列(&W)',
  // Add Rows form -------------------------------------------------------------
  '增加列', '列數(&C):',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  '頁面設定', '頁面', '頁首與頁尾',
  // margins group-box, left, right, top, bottom
  '邊界 (公釐)','邊界 (英吋)', '左(&L):', '上(&T):', '右(&R):', '下(&B):',
  // mirror margins check-box
  '相等邊界(&M)',
  // orientation group-box, portrait, landscape
  '輸出方向', '直式(&P)', '橫式(&A)',
  // paper group-box, paper size, default paper source
  '紙張', '大小(&Z):', '來源(&S):',
  // header group-box, print on the first page, font button
  '頁首', '文字(&T):', '第一頁頁首(&H)', '字型(&F)...',
  // header alignments: left, center, right
  '置左(&L)', '置中(&C)', '置右(&R)',
  // the same for footer (different hotkeys)
  '頁尾', '文字(&X):', '第一頁頁尾(&P)', '字型(&O)...',
  '置左(&E)', '置中(&N)', '置右(&I)',
  // page numbers group-box, start from
  '頁數', '起始值(&S):',
  // hint about codes
  '特殊字元組合:'#13'&&p - 頁數; &&P - 第幾頁; &&d - 現在日期; &&t - 現在時間.',
  // Code Page form ------------------------------------------------------------
  // title, label
  '文字檔案字碼頁', '選擇檔案編碼(&C):',
  // thai, japanese, chinese (simpl), korean
  '泰文', '日文', '中文 (簡體)', '韓文',
  // chinese (trad), central european, cyrillic, west european
  '中文 (繁體)', '中歐與東歐文字', '斯拉夫文', '西歐文字',
  // greek, turkish, hebrew, arabic
  '希臘文', '土耳其文', '希伯來文', '阿拉伯文',
  // baltic, vietnamese, utf-8, utf-16
  '波羅的海文', '越南文', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  '視覺樣式', '選擇樣式(&S):',
  // Delimiter form ------------------------------------------------------------
  // title, label
  '轉換為文字', '選擇分隔符號(&D):',
  // line break, tab, ';', ','
  '分行符號', '定位點', '分號', '逗號',
  // Table sort form -----------------------------------------------------------
  // error message
  '無法排序包含合併列的表格',
  // title, main options groupbox
  '表格排序', '排序',
  // sort by column, case sensitive
  '依欄排序(&S):', '區分大小寫(&C)',
  // heading row, range or rows
  '排除標題列(&H)', '要排序的列:從 %d 到 %d',
  // order, ascending, descending
  '順序', '遞增(&A)', '遞減(&D)',
  // data type, text, number
  '類型', '文字(&T)', '數字(&N)',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
   '插入編號', '屬性', '計數器名稱(&C):', '編號類型(&N):',
  // numbering groupbox, continue, start from
   '編號', '繼續(&O)', '開頭為(&S):',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
   '標題', '標籤(&L):', '將標籤排除於標題外(&E)',
  // position radiogroup
   '位置', '在所選物件上(&A)', '在所選物件下(&B)',
  // caption text
   '標題文字(&T):',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
   '編號', '圖表', '表格',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
   '顯示框線四周', '框線',
  // Left, Top, Right, Bottom checkboxes
   '左方(&L)', '上方(&T)', '右方(&R)', '下方(&B)',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  '調整表格欄寬', '調整表格列高',
  // Hints on indents: first line, left (together with first), left (without first), right
  '首行縮排', '左邊縮排', '吊掛式縮排', '右邊縮排',
  // Hints on lists: up one level (left), down one level (right)
  '到上一層', '到下一層',
  // Hints for margins: bottom, left, right and top
  '下邊界', '左邊界', '右邊界', '上邊界',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  '靠左對齊標記', '靠右對齊標記', '置中對齊標記', '數字對齊標記',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  '正常', '內文縮排', '無間距', '標題 %d', '清單段落',
  // Hyperlink, Title, Subtitle
  '超連結', '標題', '副標題',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '強調', '區別強調', '鮮明強調', '強調粗體',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  '引文', '鮮明引文', '區別參考', '鮮明參考',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  '區塊文字', 'HTML 變數', 'HTML 程式碼', 'HTML 縮寫',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML 定義', 'HTML 鍵盤', 'HTML 樣本', 'HTML 打字機',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML 預設格式', 'HTML 引用', '頁首', '頁尾', '頁碼',
  // Caption
   '標題',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  '章節附註參考', '註腳參考', '章節附註文字', '註腳文字',
  // Sidenote Reference, Sidenote Text
   '旁註參考', '旁註文字',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  '色彩', '背景色彩', '透明', '預設', '底線色彩',
  // default background color, default text color, [color] same as text
  '預設背景色彩', '預設文字色彩', '與文字相同',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  '單線', '粗線', '雙線', '點線', '粗點線', '虛線',
  // underline types: thick dashed, long dashed, thick long dashed,
  '粗虛線', '長虛線', '粗長虛線',
  // underline types: dash dotted, thick dash dotted,
  '虛點線', '粗虛點線',
  // underline types: dash dot dotted, thick dash dot dotted
  '虛點點線', '粗虛點點線',
  // sub/superscript: not, subsript, superscript
  '非下標/上標', '下標', '上標',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  '中東文模式:', '繼承', '由左至右', '由右至左',
  // bold, not bold
  '粗體', '非粗體',
  // italic, not italic
  '斜體', '非斜體',
  // underlined, not underlined, default underline
  '底線', '非底線', '預設底線',
  // struck out, not struck out
  '刪除線', '非刪除線',
  // overlined, not overlined
  '頂線', '非頂線',
  // all capitals: yes, all capitals: no
  '全部大寫', '關閉全部大寫',
  // vertical shift: none, by x% up, by x% down
  '無垂直位移', '向上位移 %d%%', '向下位移 %d%%',
  // characters width [: x%]
  '字元寬度',
  // character spacing: none, expanded by x, condensed by x
  '標準字距', '字距加寬 %s', '字距緊縮 %s',
  // charset
  '上下標',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  '預設字型', '預設段落', '繼承',
  // [hyperlink] highlight, default hyperlink attributes
  '醒目提示', '預設',
  // paragraph aligmnment: left, right, center, justify
  '靠左對齊', '靠右對齊', '置中對齊', '左右對齊',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  '行高:%d%%', '行距:%s', '行高: 最少 %s',
  '行高: 固定行高 %s',
  // no wrap, wrap
  '停用換行', '換行',
  // keep lines together: yes, no
  '段落中不分頁', '段落中分頁',
  // keep with next: yes, no
  '與下段同頁', '不與下段同頁',
  // read only: yes, no
  '唯讀', '可編輯',
  // body text, heading level x
  '大綱階層: 內文', '大綱階層:%d',
  // indents: first line, left, right
  '首行縮排', '左邊縮排', '右邊縮排',
  // space before, after
  '與前段距離', '與後段距離',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  '標示符號', '無', '對齊', '左', '右', '中',
  // tab leader (filling character)
  '前置字元',
  // no, yes
  '否', '是',
  // [padding/spacing/side:] left, top, right, bottom
  '左', '上', '右', '下',
  // border: none, single, double, triple, thick inside, thick outside
  '無', '單線', '雙線', '三線', '內邊加粗', '外邊加粗',
  // background, border, padding, spacing [between text and border]
  '背景', '邊框', '與邊框距離', '間距',
  // border: style, width, internal width, visible sides
  '樣式', '寬度', '內部寬度', '可見邊',
  // style inspector -----------------------------------------------------------
  // title
  '樣式檢查',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  '樣式', '(無)', '段落', '字型', '屬性',
  // border and background, hyperlink, standard [style]
  '邊框與網底', '超連結', '(標準)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  '樣式', '樣式',
  // name, applicable to,
  '名稱(&N):', '適用於(&T):',
  // based on, based on (no &),
  '根據(&B):', '根據:',
  //  next style, next style (no &)
  '供後續段落使用之樣式(&F):', '供後續段落使用之樣式:',
  // quick access check-box, description and preview
  '快速存取(&Q)', '描述與預覽(&P):',
  // [applicable to:] paragraph and text, paragraph, text
  '段落與文字', '段落', '文字',
  // text and paragraph styles, default font
  '文字與段落樣式', '預設字型',
  // links: edit, reset, buttons: add, delete
  '編輯', '重設', '新增(&A)', '刪除(&D)',
  // add standard style, add custom style, default style name
  '新增標準樣式(&S)...', '新增自訂樣式(&C)', '樣式 %d',
  // choose style
  '選擇樣式(&S):',
  // import, export,
  '匯入(&I)...', '匯出(&E)...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView 樣式 (*.rvst)|*.rvst', '載入檔案時發生錯誤', '此檔案未包含樣式',
  // Title, group-box, import styles
  '從檔案匯入樣式', '匯入', '匯入樣式(&M):',
  // existing styles radio-group: Title, override, auto-rename
  '現有樣式', '覆寫(&O)', '新增重新命名者(&A)',
  // Select, Unselect, Invert,
  '選取(&S)', '取消選取(&U)', '反轉(&I)',
  // select/unselect: all styles, new styles, existing styles
  '所有樣式(&A)', '新樣式(&N)', '現有樣式(&E)',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(清除格式)', '(所有樣式)',
  // dialog title, prompt
  '樣式', '選擇要套用的樣式(&C):',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '同義字(&S)', '全部略過(&I)', '加入字典(&A)',
  // Progress messages ---------------------------------------------------------
  '正在下載 %s', '準備列印...', '正在列印第 %d 頁',
  '從 RTF 轉換...',  '從 RTF 轉換...',
  '正在讀取 %s...', '正在寫入 %s...', '文字',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
   '無附註', '註腳', '章節附註', '旁註', '文字方塊'
  );


initialization
  RVA_RegisterLanguage(
    'Chinese (Big5)', // english language name, do not translate
     '中文 (Big5)', // native language name
    CHINESEBIG5_CHARSET, @Messages);

end.

