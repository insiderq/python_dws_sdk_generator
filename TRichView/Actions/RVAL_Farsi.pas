{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Farsi (Persian) translation                     }
{                                                       }
{       Copyright (c) Muhammad B Mamouri                }
{       mamouri@rasasoftware.com                        }
{       http://www.rasasoftware.com                     }
{                                                       }
{*******************************************************}
{ Updated: Mehrdad Esmaeili 2011-Oct-6                  }
{ Updated: Mehrdad Esmaeili 2012-Oct-17                 }
{ Updated: Mehrdad Esmaeili 2014-May-01                 }
{*******************************************************}

unit RVAL_Farsi;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  '���', '��������', '�������', '�����', '�����', '�����',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&������', '&������', '&���ȝ����', '&����', '&���ǐ���', '&���', '&����', '&�����', '&������',
  // exit
  '&����',
  // top-level menus: View, Tools,
  '&�����', '�&����',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '������', '����', '�&�����', '���� ������� ����', '�&���� ����',
  // menus: Table cell rotation
  '������� ����',
  // menus: Text flow, Footnotes/endnotes
  '��� �����', '������� ������',
  // ribbon tabs: tab1, tab2, view, table
  '����', '�������', '������', '����',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '��큝���', '����', '���ǐ���', '����', '������',
  // ribbon groups: insert, background, page setup,
  '���', '�ӝ�����', '������� ����',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  '����ϝ��', '������� ������', '���� ���', '����� / �����', '��ѐ�����', '�������',
  // ribbon groups on table tab: insert, delete, operations, borders
  '���', '���', '������', '�����',
  // ribbon groups: styles
  '�Ș ��',
  // ribbon screen tip footer,
  '���� �������� ����� ���� F1 �� ���� ����',
  // ribbon app menu: recent files, save-as menu title, print menu title
  '����� ����', '����� ����� �� ߁� �� ���', '��ԝ����� � �ǁ ���',
  // ribbon label: units combo
  '���ϝ��:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&����', '����|����� �� ��� ���� ����',
  // TrvActionOpen
  '&�������� ...', '��������|�������� �� ��� �� ��� ����',
  // TrvActionSave
  '&���', '���|��� ��� �� ��� ����',
  // TrvActionSaveAs
  '��� &�� ��� ...', '��� �� ���|��� ��� �� ��� ���� �� ��� ����',
  // TrvActionExport
  '&���� ...', '����|���� ��� �� �� ���� ����',
  // TrvActionPrintPreview
  '�&�ԝ����� �ǁ', '��ԝ����� �ǁ|����� ��� �� ����� �� �ǁ �����',
  // TrvActionPrint
  '&�ǁ ...', '�ǁ|����� ������� �ǁ � �ǁ ���',
  // TrvActionQuickPrint
  '�ǁ ����', '�ǁ ����', '�ǁ ����|�ǁ ���� ���' ,
  // TrvActionPageSetup
  '�&���� ����', '����� ����|����� ����� ������ ���С ��� ���С ���ڡ ������ � ������',
  // TrvActionCut
  '&���', '���|��� ��� ������ ��� � ���� ���� �� �� �����',
  // TrvActionCopy
  '&������', '������|������ ����� �� ��� ������ ��� � ���� ���� �� �� �����',
  // TrvActionPaste
  '&�����', '�����|��� ������� ����� �� �����',
  // TrvActionPasteAsText
  '����� �� ���� ����', '����� �� ���� ����|����� �� ��� ��� �� ���� ����',
  // TrvActionPasteSpecial
  '����� &���', '����� ���|��� ������� ����� �� ���� ���� ���� ���',
  // TrvActionSelectAll
  '������ &��', '������ ��|������ �� ���',
  // TrvActionUndo
  '&���� ���� ����� ���', '���� ���� ����� ���|�� ��� ���� ��� ����� ���',
  // TrvActionRedo
  '&������ ����� ����', '������ ����� ����|����� ������ ����� �����',
  // TrvActionFind
  '&����� ...', '�����|������ �� ��� ��� �� ���',
  // TrvActionFindNext
  '������ &����', '������ ����|����� ����� �����',
  // TrvActionReplace
  '&������� ...', '�������|����� � ������� �� ��� ��� �� ���',
  // TrvActionInsertFile
  '&���� ...', '��� ����|��� ������� ���� �� ���',
  // TrvActionInsertPicture
  '&����� ...', '��� �����|��� ����� �� ��� ����',
  // TRVActionInsertHLine
  '&�� ����', '��� �� ����|��� �� �� ����',
  // TRVActionInsertHyperlink
  '&����', '��� ����|��� ����',
  // TRVActionRemoveHyperlinks
  '��� ��ǁ��ϝ��', '��� ��ǁ���ϝ��|��� ����� ��ǁ���ϝ�� �� ��� �������',
  // TrvActionInsertSymbol
  '&����� ���', '��� ����� ���|��� ����� ���',
  // TrvActionInsertNumber
   '��� �����', '��� �����|��� ����� � �����',
  // TrvActionInsertCaption
   '��� �����...', '��� �����|��� � ����� ���� ��� ������ ���',
  // TrvActionInsertTextBox
   '���� ���', '��� ���� ���|��� � ���� ���',
  // TrvActionInsertSidenote
   '������� �����', '��� ������� �����|��� �������� �� �� � ���� ��� ����� ���� ����� ��',
  // TrvActionInsertPageNumber
   '����� ����', '��� ����� ����|��� ����� ����� ����',
  // TrvActionParaList
  '&����坐��� ��� � ������...', '����坐��� ��� � ������|���� �� ������ ����坐��� ��� �� ������ ���� ���ǐ��� �������',
  // TrvActionParaBullets
  '&����坐��� ���', '����坐��� ���|��� � ����� ����坐��� ��� ���� ���ǐ���',
  // TrvActionParaNumbering
  '&����� �����', '����� �����|����� �� ��� ����� ����� �� ���',
  // TrvActionColor
  '&�� �� ����� ...', '�� �����|����� �� ����� ���',
  // TrvActionFillColor
  '&�� ����', '�� ����|����� �� �ӝ����� ��� ���ǐ��ݡ ���� ���� �� ���',
  // TrvActionInsertPageBreak
  '&��� ��ڝ����� ����', '��� ��ڝ����� ����|�� ��ڝ����� ���� ��� ��',
  // TrvActionRemovePageBreak
  '&��� ��ڝ����� ����', '��� ��ڝ����� ����|��� ��� ����� ����',
  // TrvActionClearLeft
  '��ߝ���� ��� ����� ��� ��', '��ߝ���� ��� ����� ��� ��|���� ���� ���ǐ��� ������� �� ����� �� ������ �� �� ��� �� ���� ��� ����',
  // TrvActionClearRight
  '��ߝ���� ��� ����� ��� ����', '��ߝ���� ��� ����� ��� ����|���� ���� ���ǐ��� ������� �� ����� �� ������ �� �� ��� ���� ���� ��� ����',
  // TrvActionClearBoth
  '��ߝ���� ��� ����� ��� �� � ����', '��ߝ���� ��� ����� ��� �� � ����|���� ���� ���ǐ��� ������� �� ����� �� ������ �� �� ��� �� �� ���� ���� ��� ����',
  // TrvActionClearNone
  '��� ����� ������', '��� ����� ������|��� ������� �� ����� ������� �� �� ��� �� �� ���� ���� ������ ����� ����',
  // TrvActionVAlign
  '������ ���...', '������ ���|����� ������ ��� �������',
  // TrvActionItemProperties
  '&������ ��', '������ ��|����� ������ �� ����',
  // TrvActionBackground
  '&�� ����� ...', '�ӝ�����|������ �� � ����� �ӝ�����',
  // TrvActionParagraph
  '&���ǐ��� ...', '���ǐ���|����� ������ ���ǐ���',
  // TrvActionIndentInc
  '&������ ����ʐ�', '������ ����ʐ�|������ ����ʐ� �� ���ǐ��� �������',
  // TrvActionIndentDec
  '&���� ����ʐ�', '���� ����ʐ�|���� ����ʐ� �� ���ǐ��� �������',
  // TrvActionWordWrap
  '���� &����', '���� ����|����� ����� ���� ���� ���� ���ǐ��� ����',
  // TrvActionAlignLeft
  '���� &��', '���� ��|���� ���� ��� ������� �� ��',
  // TrvActionAlignRight
  '���� &����', '���� ����|���� ���� ��� ������� �� ����',
  // TrvActionAlignCenter
  '���� &���', '���� ���|��� ��� ���� ��� �������',
  // TrvActionAlignJustify
  '&���� ����', '���� ����|���� ��� ������� �� �� ���� �� �� ��',
  // TrvActionParaColor
  '�� &�ӝ����� ���ǐ��� ...', '�� �ӝ����� ���ǐ���|�������� �� �ӝ����� ���ǐ���',
  // TrvActionLineSpacing100
  '&����坐���� ���� ����', '����� ����� ���� ����|����� ����� ����� ���� �� ���� ��',
  // TrvActionLineSpacing150
  '����坐���� 1.5 ����� ����', '����坐���� 1.5 ����� ����|����� ���� �� ����� ������ 1.5 �� ���� ���',
  // TrvActionLineSpacing200
  '����坐���� 2 ����� ����', '����坐���� 2 ����� ����|����坐���� ���� �� 2 ����� ���� ���',
  // TrvActionParaBorder
  '�ӝ����� � &����� ���ǐ��� ...', '�ӝ����� � ����� ���ǐ���|����� ����� � �� ����� ���� ���ǐ��� �������',
  // TrvActionInsertTable
  '��� ����', '��� ����', '��� ����|��� �� ���� ����',
  // TrvActionTableInsertRowsAbove
  '��� ���� �� &����', '�� ���� �� ����|��� �� ���� ���� ����� ����� �������',
  // TrvActionTableInsertRowsBelow
  '��� ���� �� &�����', '��� ���� �� �����|��� �� ���� ���� ����� ����� �������',
  // TrvActionTableInsertColLeft
  '��� ���� �� &��', '��� ���� �� ��|��� �� ���� ���� �� ��� �� ����� �������',
  // TrvActionTableInsertColRight
  '��� ���� �� &����', '��� ���� �� ����|��� �� ���� ���� �� ��� ���� ����� �������',
  // TrvActionTableDeleteRows
  '(��� &����(��', '(��� ����(��|������� ������� ������ ��� �� ��� ��',
  // TrvActionTableDeleteCols
  '(��� ����(��', '(��� ����(��|������� ���ݝ��� ������ ��� �� ��� ��',
  // TrvActionTableDeleteTable
  '&��� ����', '��� ����|��� ����',
  // TrvActionTableMergeCells
  '&����� ������ ...', '����� ������|����� ������� �������',
  // TrvActionTableSplitCells
  '&������� ������ ...', '������� ������|��� ���� ������� �������',
  // TrvActionTableSelectTable
  '������ &����', '������ ����|������ ���� ����',
  // TrvActionTableSelectRows
  '&������ ���ݝ��', '������ ���ݝ��|������ ���� ���ݝ��',
  // TrvActionTableSelectCols
  '������ &������', '������ ������|������ ���� ������',
  // TrvActionTableSelectCell
  '������ &����', '������ ����|������ ���� ����',
  // TrvActionTableCellVAlignTop
  '���� ���� �� &����', '���� ���� �� ����|���� ���� ������� ���� �� ����',
  // TrvActionTableCellVAlignMiddle
  '���� ���� �� &���', '���� ���� �� ���|���� ���� ������� ���� �� ���',
  // TrvActionTableCellVAlignBottom
  '���� ���� �� &�����', '���� ���� �� �����|���� ���� ������� ���� �� �����',
  // TrvActionTableCellVAlignDefault
  '���� ��� ��� &����� ����', '���� ��� ��� ����� ����|������ ���� ��� ��� ����� ���� ���� �������',
  // TrvActionTableCellRotationNone
  '��� ������� ����', '��� ������� ����|��� ���� ������� ����',
  // TrvActionTableCellRotation90
  '������� ���� �� ������ 90 ����', '������� ���� �� ������ 90 ����|������� ������� ���� �� ������ 90 ����',
  // TrvActionTableCellRotation180
  '������� ���� �� ������ 180 ����', '������� ���� �� ������ 180 ����|������� ������� ���� �� ������ 180 ����',
  // TrvActionTableCellRotation270
  '������� ���� �� ������ 270 ����', '������� ���� �� ������ 270 ����|������� ������� ���� �� ������ 270 ����',
  // TrvActionTableProperties
  '&������ ���� ...', '������ ����|����� ������ ���� �������',
  // TrvActionTableGrid
  '����� ���� ������', '����� ���� ������|����� �� ���� ���� ���� ������ �� ����',
  // TRVActionTableSplit
  '����� ����', '����� ����|����� ���� �� �� ����',
  // TRVActionTableToText
  '����� �� ���...', '����� �� ���|����� ���� �� ���',
  // TRVActionTableSort
  '���ȝ����...', '���ȝ����|���ȝ���� ���ݝ��� ����',
  // TrvActionTableCellLeftBorder
  '����� &��', '����� ��|����� �� ���� ���� ����� �� ����',
  // TrvActionTableCellRightBorder
  '����� &����', '����� ����|����� �� ���� ���� ����� ���� ����',
  // TrvActionTableCellTopBorder
  '����� &����', '����� ����|����� �� ���� ���� ����� ����� ����',
  // TrvActionTableCellBottomBorder
  '����� &�����', '����� �����|����� �� ���� ���� ����� ����� ����',
  // TrvActionTableCellAllBorders
  '��� &�������', '��� �������|����� �� ���� ���� ��� �������� ������',
  // TrvActionTableCellNoBorders
  '&���� �����', '���� �����|���� ���� ��� �������� ����',
  // TrvActionFonts & TrvActionFontEx
  '&��� ...', '���|����� ��� ��� �������',
  // TrvActionFontBold
  '&����', '����|����� ��� ��� ������ ��� �� ����',
  // TrvActionFontItalic
  '&�������', '�������|����� ��� ��� ������ ��� �� �������',
  // TrvActionFontUnderline
  '&��� �� ���', '��� �� ���|����� ��� ��� ������ ��� �� ��� �� ���',
  // TrvActionFontStrikeout
  '&�� �����', '�� �����|���� ���� �� ����� ��� ��� �������',
  // TrvActionFontGrow
  '&������ ������ ���', '������ ������ ���|������ 10%� ������ ��� ��� �������',
  // TrvActionFontShrink
  '&��� ��� ���', '��� ��� ���|��� ���� 10%� ��� �������',
  // TrvActionFontGrowOnePoint
  '&������ �� ���� ������ ���', '������ �� ���� ������ ���|������ ������ ��� ������ ��� �� �� ������',
  // TrvActionFontShrinkOnePoint
  '�&�� ��� �� ���� ���', '��� ��� �� ���� ���|������ �� ���� ���� ���',
  // TrvActionFontAllCaps
  '&���� ��ѐ', '���� ��ѐ|����� ��� ���������� ��� ������� �� ���� ��ѐ',
  // TrvActionFontOverline
  '&�� ��', '�� ��|����� ���� �� ��� ��� �������',
  // TrvActionFontColor
  '&�� ��� ...', '�� ���|����� �� ��� �������',
  // TrvActionFontBackColor
  '����� �� &�ӝ�����', '�� �ӝ����� ���|����� �� �ӝ����� ��� �������',
  // TrvActionAddictSpell3
  '&������', '��� ���|������� ���',
  // TrvActionAddictThesaurus3
  '&������ ����', '������ ����|����� �ǎ� ������ ���� ���� �������',
  // TrvActionParaLTR
  '�� �� ����', '�� �� ����|��� ���ǐ��� ������� �� �� �� ���� ���� ���',
  // TrvActionParaRTL
  '���� �� ��', '���� �� ��|��� ���ǐ��� ������� �� ���� �� �� ���� ���',
  // TrvActionLTR
  '��� �� �� ����', '�� �� ����|��� ��� ������� �� �� �� ���� ���� ���',
  // TrvActionRTL
  '��� ���� �� ��', '��� ���� �� ��|��� ��� ������� �� ���� �� �� ���� ���',
  // TrvActionCharCase
  '����� �������', '����� �������|����� ����� ������� �������',
  // TrvActionShowSpecialCharacters
  '���������� &��эǁ�', '���������� ��эǁ�|����� �� ���� ���� ���������� ��эǁ�� ���� �������� ���ǐ��ݡ �ȝ�� � �������',
  // TrvActionSubscript
  '������� (Subscript)', '�������|����� ��� ������� �� ������� (Subscript)',
  // TrvActionSuperscript
  '���ǝ���� (Superscript)', '���ǝ����|����� ��� ������� �� ���ǝ���� (Superscript)',
  // TrvActionInsertFootnote
  '������ (Footnote)', '������|��� �� ������',
  // TrvActionInsertEndnote
  '������� ������ (Endnote)', '������� ������|��� ������� ������',
  // TrvActionEditNote
  '������ �������', '������ �������|������ ������ �� ������� ������',
  '����', '����|����� �� ���� ���� ���� �������',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '�Ș ��...', '�Ș ��|�������� ���������� �� ������ �Ș',
  // TrvActionAddStyleTemplate
  '������� �Ș...', '������ �Ș|����� �Ș ���� ���ǐ��� �� ���',
  // TrvActionClearFormat,
  '�ǘ ���� ����', '�ǘ ���� ����|�ǘ ���� ���� �� ��� �� ���ǐ��� ������ ���',
  // TrvActionClearTextFormat,
  '�ǘ ���� ���� ���', '�ǘ ���� ���� ���|�ǘ ���� ����� ���� ��� ��� ������ ���',
  // TrvActionStyleInspector
  '������ �Ș', '������ �Ș|����� �� ����� ����� ������ �Ș',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   '�����', '������', '����', '���', '&�������� ...', '&��� ...', '&���', '������',  'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  '����',
  // left, top, right, bottom sides
  '��� ��', '��� ����', '��� ����', '��� �����',
  // save changes? confirm title
  '��� ������� �� %s ��� ���Ͽ', '����',
  // warning: losing formatting
  '%s ���� �������� ��� �� �� ���� ������� ��� ����� ����.'#13+
  '��� ���� ���� ����� �� ��� ��� �� �� ��� ���� ����� ���Ͽ',
  // RVF format name
  '���� RichView',
  // Error messages ------------------------------------------------------------
  '���',
  '��� �� �������� ����.' + #13#13 + '����� �������:' + #13 + '- ���� ��� ���� �� ����� ��� ������ �������� ��� ���;'+#13+
  '- ���� ���� ��� ���;' + #13 + '- ���� �� ����� ������ ���� ��� � ��� ��� ���.',
  '��� �� �������� �����.' + #13#13 + '����� �������:' + #13 + '- ���� ���� �� ���� ��� �� ���� ��� ������ �������� ������.' + #13+
  '- ���� ���� ����� ����;' + #13+
  '- ���� ���� ��� ���;' + #13 + '- ���� �� ����� ������ ���� ��� � ��� ��� ���.',
  '��� �� ��� ����.' + #13#13 + '����� �������:' + #13 + '- ���� ���� ���� �����;' + #13+
  '- ���� �� ����� ����� ������ ��� ���;' + #13 + '- ����� ���� ��� ���� ��ʐ�� ���� ���.' + #13+
  '- ���� �� ����� ������ ���� ��� � ��� ��� ���' + #13 + '- ����� ���� ���� ��� ���.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Files (*.rvf)|*.rvf',  'RTF Files (*.rtf)|*.rtf' , 'XML Files (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Text Files (*.txt)|*.txt', 'Text Files - Unicode (*.txt)|*.txt', 'Text Files - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Files (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplified (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Documents (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  '����� ����� ����', '�� ����� �� ����� �� ����� ''%s'' ���� ���',
  // 1 string replaced; Several strings replaced
  '1 ���� ������ ��.', '%d ���� ������ ��.',
  // continue search from the beginning/end?
  '����� �� ������ ��� ���ϡ ����� �� ����� ����� ��Ͽ',
  '����� �� ������ ��� ���ϡ ����� �� ����� ����� ��Ͽ',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  '���� �� �����', '������',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '����', '������', '��� ������', '��� ���', '������� ���� �� ��� ����', '��� ����', '����', '�������-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  '���� ����', '������', '��� ����', '���', '������� ���� �� ���', '���', '���-�������', '�������-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  '����', '������ ����', '������', '��� ���� �� ���', '�������', '��� ����', '����', '�������-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  '�����', '�����', '���', '��� ����', '��������', '��� ������', '���� ���� �� �������', '�������-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  '��� ��', '������ ���� �� ���', '��� ����', '��� ����', '�������� ����', '��� �� ������', '���� ����', '����',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&���� �ӝ�����', '&������', '&�䐝��� ����� ...', '&��� ���',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  '�ӝ�����', '&��:', '������', '�� �����', '��� �����.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&�� ����', '����� &����', '���� &����', '�&�� ����', '&���',
  // Padding button
  '&������� ...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  '�� ���� ��', '�&���� ��:', '�&䐝��� ����� ...', '&�������',
  '���� ��� ������ ����.',
  // [apply to:] text, paragraph, table, cell
  '���', '���ǐ���' , '����', '����',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  '���', '���', '��͝����',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&���:', '�&�����', '���', '�&���', '����&���',
  // Script, Color, Back color labels, Default charset
  '&���:', '&��:', '�� �����:', '(��� ���)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  '�����', '��� �� ���', '&�� ���� ���', '&�� �� ���', '�� ���� &��ѐ',
  // Sample, Sample text
  '�����', '��� �����',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  '����� ����� ���������', '&�����:', '&��� �����', '&�����',
  // Offset group-box, Offset label, Down, Up,
  '������', '&������:', '&�����', '&����',
  // Scaling group-box, Scaling
  '����ӝ����� ����', '&����� �����:',
  // Sub/super script group box, Normal, Sub, Super
  '������� � �������� (Subscript/Superscript) :', '����', '�������', '���ǝ����',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  '����� ����� �ӝ�����', '&����:', '&��:', '&�����:', '&����:', '&������ �����',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  '��� ����', '����', '&���:', '&�����:', '<<������>>',
  // cannot open URL
  '����� ����� �� "%s" ���� �����',
  // hyperlink properties button, hyperlink style
  '&������� ....', '�Ș:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  '������ ����', '����� �����', '����� ����',
  // Text [color], Background [color]
  '&���:', '&�� �����:',
  // Underline [color]
  '�� �����:',
  // Text [color], Background [color] (different hotkeys)
  '�&��:', '�� �����:',
  // Underline [color], Attributes group-box,
  '�� �����:', '�������',
  // Underline type: always, never, active (below the mouse)
  '��� �����:', '���Ԑ�', '�� ���', '����',
  // Same as normal check-box
  '�� ��� &����',
  // underline active links check-box
  '����� ��� ����� ��� ��� ����',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  '��� ����� ���', '&���:', '&������� ��:', '������',
  // Unicode block
  '���� ������ :',  
  // Character Code, Character Unicode Code, No Character
  '�� �������: %d', '�� �������: ������ %d', '(�� ��������)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  '��� ����', '������ ����', '����� &������:', '����� &���ݝ��',
  // Table layout group-box, Autosize, Fit, Manual size,
  '��͝���� ����', '������ ��ԝ���', '��� ���� ���� �� �����', '����� ������ ���� ����',
  // Remember check-box
  '����� �� ���� ���� ���� �� ���� �����',
  // VAlign Form ---------------------------------------------------------------
  '������',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  '������', '�����', '������ � ������', '��', '����', '���ݝ��', '������',
  // Image Appearance, Image Misc, Number tabs
   '����', '������', '�����',
  // Box position, Box size, Box appearance tabs
   '������', '������', '����',
  // Preview label, Transparency group-box, checkbox, label
  '��� �����:', '������', '����', '�� ����� ���� ���� :',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&����� ...', '�����...', '�� ��ԝ���',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  '���� �����', '���� ���� :',
  '���� ����� ����� ����� �� �� ���� ���',   '���� ����� ��� ����� �� �� ���� ���',
  '���� ����� ����� ����� �� ����� ��',  '���� ����� ����� ����� �� ����� ��',  '���� ����� ��� ����� �� ��� ��',
  // align to left side, align to right side
  '���� �� ��� ��',  '���� �� ��� ����',
  // Shift By label, Stretch group box, Width, Height, Default size
  '�ǝ������ :', '���ϐ�', '���� :', '������ :', '������ ��� ���:',
  // Scale proportionally checkbox, Reset ("no stretch") button
   '����� ����� ������', '���� ���ϐ�',
  // Inside the border, border, outside the border groupboxes
   '���� ���', '���', '����� ���',
  // Fill color, Padding (i.e. margins inside border) labels
   '����� ��:', '�����:',
  // Border width, Border color labels
   '����� ���:', '�� ���:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
   '����� ����� ����:', '����� ����� �����:',
  // Miscellaneous groupbox, Tooltip label
   '������', '����� ����� (Tooltip) :',
  // web group-box, alt text
  '�� (Web)', '��� ������ :',
  // Horz line group-box, color, width, style
  '�� ����', '�� :', '����� :', '��� :',
  // Table group-box; Table Width, Color, CellSpacing,
  '����', '��� ���� :', '�� �ӝ����� ���� :', '����坐���� ������ :',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
   '����� ���� ����:', '����� ����� ����:', '��� � �� �����',
  // Visible table border sides button, auto width (in combobox), auto width label
   '��� ��� ������:', '��� ���', '��� ���',
  // Keep row content together checkbox
   '��� ����� �� ���� ����',
  // Rotation groupbox, rotations: 0, 90, 180, 270
   '����', '���� ����', '90 ����', '180 ����', '270 ����',
  // Cell border label, Visible cell border sides button,
   '���:', '��� ��� ����� ������:',
  // Border, CellBorders buttons
  '��� ��� ���� ...', '��� ��� ������ ...  ',
  // Table border, Cell borders dialog titles
  '��� ��� ����', '��� ��� ��ԝ��� ������',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  '�ǁ ����', '&���� �� �� �� ���� ���� ���� ���', '����� ������� �����',
  '����� ���ݝ��� �����',  '���ݝ��� ����� �� �� ���� �� ���� ����� ������',
  // top, center, bottom, default
  '����', '���', '�����', '��� ���',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  '�������', '����� ����� :', '����� ������ :', '����� �� :',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '��� ��� ������', '������ ������ :',  '�� ���� :', '�� ���� :', '�� ���� :',
  // Background image
  '����� �ӝ����� ...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
   '������ ����', '������ �����', '������ �� ���',
  // Position types: align, absolute, relative
   '������ ������', '������ ����', '������ ����',
  // [Align] left side, center, right side
   '��� �� ���� �� ��� ��',
   '�ј� ���� �� �ј�',
   '��� ���� ���� �� ��� ����',
  // [Align] top side, center, bottom side
   '��� ������ ���� �� ��� ������',
   '�ј� ���� �� �ј�',
   '��� ������ ���� �� ��� ������',
  // [Align] relative to
   '���� ���� ��',
  // [Position] to the right of the left side of
   '������ ���� ��� ���',
  // [Position] below the top side of
   '������ ����� ��� ������',
  // Anchors: page, main text area (x2)
   '����', '������ ��� ����', '����', '������ ��� ����',
  // Anchors: left margin, right margin
   '����� ��', '����� ����',
  // Anchors: top margin, bottom margin, inner margin, outer margin
   '����� ����', '����� �����', '����� �����', '����� ������',
  // Anchors: character, line, paragraph
   '���ǘ��', '��', '���ǐ���',
  // Mirrored margins checkbox, layout in table cell checkbox
   '����� �ژ��', '������� �� ���� ����',
  // Above text, below text
   '����� ���', '����� ���',
  // Width, Height groupboxes, Width, Height labels
   '����', '������', '����:', '������:',
  // Auto height label, Auto height combobox item
   '��Ϙ��', '��Ϙ��',
  // Vertical alignment groupbox; alignments: top, center, bottom
   '���� �����', '����', '�ј�', '�����',
  // Border and background button and title
   '��� � �� �����...', '��� � �� ����� ����',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  '����� ���...',  '����� �� ���� :',  '���� Rich Text',
  '���� HTML',  '���',  '��� Unicode',  '����� Bitmap',
  '����� Metafile',  '���� �������',
  // Options group-box, styles
  '����� ��', '�Ș ��:',
  // style options: apply target styles, use source styles, ignore styles
  '����� �Ș ��� ��� ���', '��� �Ș �� � ����',
  '��� ���� � ������ ����� �Ș ��',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  '����坐��� ��� � ������', '����坐���� ��� �� ���� ���', '����坐���� ��� �� ���� ������', '���ʝ��� ��� ���', '���ʝ��� ������',
  // Customize, Reset, None
  '&������� ��� ...', '&��� ������', '�� ����',
  // Numbering: continue, reset to, create new
  '����� �������', '��� ������ ���坐����', '����� �� ���� ���ϡ ���� ��',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  '���� ��ѐ ������', '���� ��ѐ ����', '����� �����', '���� ��� ������', '���� ��� ����',
  // Bullet type
  '�����', '����', '����',
  // Level, Start from, Continue numbering, Numbering group-box
  '��� (Level) :', '���� �� :', '�����', '������',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  '���� ����������', '�����', '&�����"', '������ ����', '��� &����:',
  // List types: bullet, image
  '����坐��� ���', '�����', '��� ���|',
  // Number format, Number, Start level from, Font button
  '���� &���:', '���', '��� ����坐���� �� ���� �� ��:', '&��� ...',
  // Image button, bullet character, Bullet button,
  '&����� ...', '&������� ����坐���', '&����坐��� ...',
  // Position of list text, bullet, number, image, text
  '������ ��� ����', '������ ����坐���', '������ ���', '������ �����', '������ ���',
  // at, left indent, first line indent, from left indent
  '&��:', '��&��ʐ� ��', '&����ʐ� ����� ��:', '�� ����ʐ� ��',
  // [only] one level preview, preview
  '��ԝ����� &�� �������', '��ԝ�����',
  // Preview text
  '��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  '���� ��', '���� ����', '���',
  // level #, this level (for combo-box)
  'Level %d', '��� �����',
  // Marker character dialog title
  '������ ������� ����坐���',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  '����� � �ӝ����� ���ǐ���', '�����', '�ӝ�����',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  '�������', '&��:', '&���:', '��� &�����:', '&������ ...',
  // Sample, Border type group-boxes;
  '�����', '��� �����',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&�� ����', '&����', '&�� �����', '&�� �����', '���� �� &����', '���� �� &����',
  // Fill color group-box; More colors, padding buttons
  '�� ���� ��', '&�䐝��� ����� ...', '&������� ...',
  // preview text
  '��� ��� ��� ��� ���. ��� ��� ��� ��� ��� . ��� ��� ��� ��� ��� .',
  // title and group-box in Offsets dialog
  '������', '������ �����',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  '���ǐ���', '����', '&��', '&����', '&���', '�� ���� ���',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  '����坐����', '&���:', '&���:', '&����� ����:', '�����:',
  // Indents group-box; indents: left, right, first line
  '����ʐ���', '�&�:', '��&��:', '����� &��:',
  // indented, hanging
  '&�� ����', '&���� ���', '�����',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '����', '1.5 ��', '�� �����', '�����', '����', '��ϐ���',
  // preview text
  '��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ��� ��� ��� ��� ���. ',
  // tabs: Indents and spacing, Tabs, Text flow
  '����ʐ� � ����坐����', '�ȝ��', '��� ���',
  // tab stop position label; buttons: set, delete, delete all
  '��� &��:', '&�����', '&���', '��� &���',
  // tab align group; left, right, center aligns,
  '����', '&��', '&����', '&���',
  // leader radio-group; no leader
  '������', '(��)',
  // tab stops to be deleted, delete none, delete all labels
  '�ȝ���� �� ���� ��� ����:', '(��)', '���.',
  // Pagination group-box, keep with next, keep lines together
  '���坐����', '����� &�� ���� ���� ���', '&���� �� ���� �� ���� ���',
  // Outline level, Body text, Level #
  '��� ���� ���:', '��� ���� ���', '��� %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  '��ԝ����� �ǁ', '��� ����', '���� ����', '�����:',
  // Invalid Scale, [page #] of #
  '���� ���� ��� 10 �� 500 ���� ����', '�� %d',
  // Hints on buttons: first, prior, next, last pages
  '����� ����', '���� ����', '���� ����', '����� ����',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  '����坐���� ����', '����坐����', '��� ������', '�� ����� ���� �� ����',
  // vertical, horizontal (x2)
  '����� :', '���� :', '����� :', '���� :',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  '�������', '�������', '&��:', '&�� ����:', '�� &����:',
  // Width; Border type group-box;
  '&���:', '��� �����',
  // Border types: none, sunken, raised, flat
  '&�� ����', '&��� ����', '&������', '&����',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  '��� ����', '��� ���� ��',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&����� ���� �� ���ݝ�� � ������', '������� �&���',
  // number of columns, rows, merge before
  '����� &������:', '����� ���ݝ��:', '&����� ��� �� ��� ����',
  // to original cols, rows check-boxes
  '��� ���� �� &������� ����', '��� ���� �� &���ݝ��� ����',
  // Add Rows form -------------------------------------------------------------
  '����� ���� ���ݝ��', '&����� ���ݝ��:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  '����� ����', '����', '������ � ������',
  // margins group-box, left, right, top, bottom
  '����� (�������)', '����� (���)', '&��:', '&����:', '&����:', '&�����:',
  // mirror margins check-box
  '&����� ������',
  // orientation group-box, portrait, landscape
  '���', '&����', '&����',
  // paper group-box, paper size, default paper source
  '����', '�&�����:', '&����:',
  // header group-box, print on the first page, font button
  '������', '&���:', '&������ �� ���� ���', '&��� ...',
  // header alignments: left, center, right
  '&��', '&���', '&����',
  // the same for footer (different hotkeys)
  '������', '&���:', '&������ �� ���� ���', '�&�� ...',
  '�&�', '��&�', '��&��',
  // page numbers group-box, start from
  '����� ����', '&���� ��:',
  // hint about codes
  '����� ��� �������:' + #13 + '&&p - ����� ����; &&P - ����� �����; &&d - ����� ����; &&t - ���� ����.',
  // Code Page form ------------------------------------------------------------
  // title, label
  '�ρ�� ���� ����', '������ ��� ��ҝ����� ����:',
  // thai, japanese, chinese (simpl), korean
  '�������', '�ǁ��', '���� �������', '�����',
  // chinese (trad), central european, cyrillic, west european
  '���� ����', '������ ����� � ����', '�������', '����� ����',
  // greek, turkish, hebrew, arabic
  '������', '����', '����', '����',
  // baltic, vietnamese, utf-8, utf-16
  '������', '�������', '������ (UTF-8)', '������ (UTF-16)',
  // Style form ----------------------------------------------------------------
  '���� ����', '������ ����:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  '����� �� ���', '������ ��� �����:',
  // line break, tab, ';', ','
  '����� ��', '��', '�������', '����',
  // Table sort form -----------------------------------------------------------
  // error message
  '����� ���ȝ���� ��� ����� �� ����� ���ݝ��� ����� ��� ���� ����� ���� �������.',
  // title, main options groupbox
  '���ȝ���� ����', '���ȝ����',
  // sort by column, case sensitive
  '���� ���� �� ���� ����:', '������ �� ���� ��ѐ � ���',
  // heading row, range or rows
  '������ ���� ���� ������', '���ȝ���� ���ݝ�� �� %d �� %d',
  // order, ascending, descending
  '�����', '�����', '�����',
  // data type, text, number
  '���', '���', '���',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
   '��� �����', '�������', '��� �������:', '��� ����� �����:',
  // numbering groupbox, continue, start from
   '����� �����', '�����', '���� ��:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
   '�����', '��� �����:', '��� ����� ��� �����',
  // position radiogroup
   '������', '�� ����� ��� �������', '�� ����� ��� �������',
  // caption text
   '��� �����:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
   '����� �����', 'Ԙ� �����: ', '���� �����: ',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
   '��� ��� ����� ������', '���',
  // Left, Top, Right, Bottom checkboxes
   '��� ��', '��� ����', '��� ����', '��� �����',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  '����� ������ ���� ����', '����� ������ ���� ����',
  // Hints on indents: first line, left (together with first), left (without first), right
  '����ʐ� ����� �� ���ǐ��� �������', '����ʐ� ��� �� ���� ��ѝ��� ���ǐ��� �������', '����ʐ� ���� ��ѝ��� ���ǐ��� ������� �� ��� �� �� ���', '����ʐ� ��� ���� ���� ��ѝ��� ���ǐ��� �������',
  // Hints on lists: up one level (left), down one level (right)
  '�� ��� ������ �� ����', '�� ��� ������� �� ����',
  // Hints for margins: bottom, left, right and top
  '����� ����� ���', '����� �� ���', '����� ���� ���', '����� ����� ���',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  '���� �� �� ��', '���� �� �� ����', '���� �� �� ���', '���� ������ ��',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  '������', '����ʐ� ������', '�� �����', '����� %d', '���� ���ǐ���',
  // Hyperlink, Title, Subtitle
  '��ǁ����', '�����', '����� ����',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '�ǘ��', '�ǘ�� ����', '�ǘ�� ���', '���',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  '��� ���', '��� ��� ���', '���� ����', '���� ���',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  '��� ���', '����� HTML', '�� HTML', '���� HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  '����� HTML', '���� ���� HTML', '����� HTML', '����� ����� HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  '�� ��� ���� ���� ��� HTML', '������� HTML', '������', '������', '����� ����',
  // Caption
   '�����',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  '���� ������� ������', '���� ������', '��� ������� ������', '��� ������',
  // Sidenote Reference, Sidenote Text
   '����� ������� �����', '��� ������� �����',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  '��', '�� �����', '����', '��� ���', '�� �����',
  // default background color, default text color, [color] same as text
  '�� ����� ��� ���', '�� ��� ��� ���', '���� ���',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'ʘ�', '����', '�������', '�� ���� ���', '�� ���� ��� ����', '�� ���� ���',
  // underline types: thick dashed, long dashed, thick long dashed,
  '�� ���� ��� ����', '�� ���� ��� ����', '�� ���� ��� ���� ����',
  // underline types: dash dotted, thick dash dotted,
  '�� ���� � ���� ���', '�� ���� � ���� ��� ����',
  // underline types: dash dot dotted, thick dash dot dotted
  '�� ���� � ���� ��� �ј���', '�� ���� � ���� ��� �ј��� ����',
  // sub/superscript: not, subsript, superscript
  '��� ���/���� ����', '�������', '��������',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  '��� ���:', '������', '�� �� ����', '���� �� ��',
  // bold, not bold
  '����', '��� ����',
  // italic, not italic
  '������', '��� ������',
  // underlined, not underlined, default underline
  '����� ���', '��� ����� ���', '����� ��� ���',
  // struck out, not struck out
  '����� ���', '��� ����� ���',
  // overlined, not overlined
  '����� �� ��', '���� �� ��',
  // all capitals: yes, all capitals: no
  '��� ���� ��ѐ', '��� ���� ��ѐ �����',
  // vertical shift: none, by x% up, by x% down
  '���� ������ �����', '������ %d%% ����', '������ %d%% �����',
  // characters width [: x%]
  '����� ���ǘ����',
  // character spacing: none, expanded by x, condensed by x
  '����� ������ ��� ���ǘ����', '����� ����� ��� ����� %s', '����� ����� ����� ��� %s',
  // charset
  '�Ә���',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  '��� ��� ���', '���ǐ��� ��� ���', '������',
  // [hyperlink] highlight, default hyperlink attributes
  '��� ����', '��� ���',
  // paragraph aligmnment: left, right, center, justify
  '���� �� ��', '���� �� ����', '��� ���', '�� ����',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  '������ ��: %d%%', '����� ��� ����: %s', '������ �� : ����� %s',
  '������ �� : ����� %s',
  // no wrap, wrap
  '���� ���� ��� ����', '���� ����',
  // keep lines together: yes, no
  '���� �� ���� �� ���� ���', '���� �� ���� �� ���� ���',
  // keep with next: yes, no
  '���� ���ǐ��� ���� ���� ���', '���� ���ǐ��� ���� ���� ���',
  // read only: yes, no
  '��� �������', '���� ������',
  // body text, heading level x
  '��� ���� ��� : ��� ���� ���', '��� ���� ��� : %d',
  // indents: first line, left, right
  '����ʐ� �� ���', '����ʐ� ��', '����ʐ� ����',
  // space before, after
  '����� �����', '����� �����',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  '�� ��', '��', '����', '��', '����', '���',
  // tab leader (filling character)
  '���ǘ�� �ј����',
  // no, yes
  '���', '���',
  // [padding/spacing/side:] left, top, right, bottom
  '��', '����', '����', '�����',
  // border: none, single, double, triple, thick inside, thick outside
  '��', 'ʘ�', '�������', '�� �����', '���� �� ����', '���� �� ����',
  // background, border, padding, spacing [between text and border]
  '�� �����', '�����', '����� �����', '����� �����',
  // border: style, width, internal width, visible sides
  '�Ș', '����', '����� �����', '��� ��� ������',
  // style inspector -----------------------------------------------------------
  // title
  '������ �Ș',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  '�Ș', '(��)', '���ǐ���', '���', '����',
  // border and background, hyperlink, standard [style]
  '����� � �� �����', '��ǁ����', '(���������)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  '�Ș ��', '�Ș',
  // name, applicable to,
  '��� :', '���� ����� ��� :',
  // based on, based on (no &),
  '�� ���� :', '�� ���� :',
  //  next style, next style (no &)
  '�Ș ���� ���ǐ��� ���� :', '�Ș ���� ���ǐ��� ���� :',
  // quick access check-box, description and preview
  '������ ����', '��� � ��� ����� :',
  // [applicable to:] paragraph and text, paragraph, text
  '���ǐ��� � ���', '���ǐ���', '���',
  // text and paragraph styles, default font
  '�Ș ��� ��� � ���ǐ���', '��� ��� ���',
  // links: edit, reset, buttons: add, delete
  '������', '��� ������', '������', '���',
  // add standard style, add custom style, default style name
  '������ �Ș ���������...', '������ �Ș ������', '�Ș %d',
  // choose style
  '������ �Ș :',
  // import, export,
  '���� ����...', '���� ����...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 '�Ș ��� RichView (*.rvst)|*.rvst', '��� �� �������� ����', '��� ���� ���� �� �Ș� ����.',
  // Title, group-box, import styles
  '���� ���� �Ș �� �� ���� ����', '���� ����', '���� ���� �Ș �� :',
  // existing styles radio-group: Title, override, auto-rename
  '�Ș ��� �����', '������ ����', '������ �� ��� ����',
  // Select, Unselect, Invert,
  '������', '��� ������', '�ژ��',
  // select/unselect: all styles, new styles, existing styles
  '��� �Ș ��', '�Ș ��� ����', '�Ș ��� �����',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(�ǘ ���� ����)', '(��� �Ș ��)',
  // dialog title, prompt
  '�Ș ��', '������ �Ș ���� ����� :',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&������', '��� �� &������ Ȑ��', '&����� �� ���� ���',
  // Progress messages ---------------------------------------------------------
  '�� ��� ������ %s', '����� ���� ���� �ǁ...', '�� ��� �ǁ ���� %d',
  '����� ���� RTF...',  '����� �� ���� RTF...',
  '������ %s...', '����� %s...', '���',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
   '���� �������', '������', '������� ������', '������� �����', '���� ���'
  );


initialization
  RVA_RegisterLanguage(
    'Farsi', // english language name, do not translate
     '�����', // native language name
    ARABIC_CHARSET, @Messages);

end.
