{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Returning language for Addict3 components       }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVAAddictLanguages;

interface

uses ad3SpellLanguages, ad3ThesaurusLanguages;

function GetAddictSpellLanguage(const s: String): TSpellLanguageType;
function GetAddictThesLanguage(const s: String): TThesaurusLanguageType;

implementation

function GetAddictSpellLanguage(const s: String): TSpellLanguageType;
begin
  if s='Czech' then
   Result := ad3SpellLanguages.ltCzech
  else if s='German' then
    Result := ad3SpellLanguages.ltGerman
  else if s='Dutch (NL)' then
    Result := ad3SpellLanguages.ltDutch
  else if s='Francais' then
    Result := ad3SpellLanguages.ltFrench
  else if s='Italian' then
    Result := ad3SpellLanguages.ltItalian
  else if Copy(s, 1, 8)='Norsk (B' then
    Result := ad3SpellLanguages.ltNorwegianBok
  else if s='Polish' then
    Result := ad3SpellLanguages.ltPolish
  else if s='Portuguese (Brazilian)' then
    Result := ad3SpellLanguages.ltBrPort
  else if s='Russian' then
    Result := ad3SpellLanguages.ltRussian
  else if s='Spanish' then
    Result := ad3SpellLanguages.ltSpanish
  else if s='Svenska' then
    Result := ad3SpellLanguages.ltSwedish
  else
    Result := ad3SpellLanguages.ltEnglish;
end;

function GetAddictThesLanguage(const s: String): TThesaurusLanguageType;
begin
  if s='Czech' then
    Result := ad3ThesaurusLanguages.ltCzech
  else if s='German' then
    Result := ad3ThesaurusLanguages.ltGerman
  else if s='Francais' then
    Result := ad3ThesaurusLanguages.ltFrench
  else if s='Italian' then
    Result := ad3ThesaurusLanguages.ltItalian
  else if Copy(s, 1, 8)='Norsk (B' then
    Result := ad3ThesaurusLanguages.ltNorwegianBok
  else if s='Polish' then
    Result := ad3ThesaurusLanguages.ltPolish
  else if s='Portuguese (Brazilian)' then
    Result := ad3ThesaurusLanguages.ltBrPort    
  else if s='Russian' then
    Result := ad3ThesaurusLanguages.ltRussian
  else if s='Spanish' then
    Result := ad3ThesaurusLanguages.ltSpanish
  else if s='Svenska' then
    Result := ad3ThesaurusLanguages.ltSwedish
  else
    Result := ad3ThesaurusLanguages.ltEnglish;
end;

end.
 