﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVADsgn.pas' rev: 27.00 (Windows)

#ifndef RvadsgnHPP
#define RvadsgnHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVSEdit.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <DesignIntf.hpp>	// Pascal unit
#include <DesignEditors.hpp>	// Pascal unit
#include <ColnEdit.hpp>	// Pascal unit
#include <VCLEditors.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvadsgn
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVALanguageProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVALanguageProperty : public Designeditors::TStringProperty
{
	typedef Designeditors::TStringProperty inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual void __fastcall GetValues(System::Classes::TGetStrProc Proc);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVALanguageProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TStringProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVALanguageProperty(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFloatPositionProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFloatPositionProperty : public Rvsedit::TRVStyleLengthProperty
{
	typedef Rvsedit::TRVStyleLengthProperty inherited;
	
protected:
	DYNAMIC System::UnicodeString __fastcall GetUnitName(System::Classes::TPersistent* Obj, const System::UnicodeString PropName);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVFloatPositionProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Rvsedit::TRVStyleLengthProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVFloatPositionProperty(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFloatSizeProperty;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFloatSizeProperty : public Rvsedit::TRVStyleLengthProperty
{
	typedef Rvsedit::TRVStyleLengthProperty inherited;
	
protected:
	DYNAMIC System::UnicodeString __fastcall GetUnitName(System::Classes::TPersistent* Obj, const System::UnicodeString PropName);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TRVFloatSizeProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Rvsedit::TRVStyleLengthProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TRVFloatSizeProperty(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Rvadsgn */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVADSGN)
using namespace Rvadsgn;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvadsgnHPP
