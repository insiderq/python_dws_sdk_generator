﻿// This file is a copy of RVAL_Blr.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Belorussian translation                         }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Updated: 2014-04-28 by Alconost                       }
{*******************************************************}

unit RVALUTF8_BLR;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'дзюймы', 'см', 'мм', 'пікі', 'пікселы', 'пункты',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'см', 'мм', 'пік', 'пікс.', 'пт.',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  '&Файл', '&Праўка', 'Фа&рмат', '&Шрыфт', '&Абзац', '&Устаўка', '&Табліца', '&Акно', '&Даведка',
  // exit
  'Вы&хад',
  // top-level menus: View, Tools,
  'В&ыгляд', 'С&эрвіс',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Памер', 'Абрыс', 'В&ыдзеліць', 'Выраўніванне ў &ячэйцы', 'Ра&мка ячэйкі',
  // menus: Table cell rotation
  'Пава&рот ячэйкі', 
  // menus: Text flow, Footnotes/endnotes
  '&Абцяканне', '&Зноскі',
  // ribbon tabs: tab1, tab2, view, table
  '&Галоўная', 'Устаўка і &фон', '&Выгляд', '&Табліца',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Буфер абмену', 'Шрыфт', 'Абзац', 'Спіс', 'Рэдагаванне',
  // ribbon groups: insert, background, page setup,
  'Устаўка', 'Фон', 'Параметры старонкі',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Сувязі', 'Зноскі', 'Рэжымы прагляду дакумента', 'Паказаць/Схаваць', 'Маштаб', 'Опцыі',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Устаўка', 'Выдаленне', 'Аперацыі', 'Рамкі',
  // ribbon groups: styles 
  'Стылі',
  // ribbon screen tip footer,
  'Націсніце F1 для даведкі',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Апошнія дакументы', 'Захаваць копію дакумента', 'Папярэдні прагляд і друк дакумента',
  // ribbon label: units combo
  'Адзінкі:',        
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Стварыць', 'Стварыць|Стварэнне новага дакумента',
  // TrvActionOpen
  '&Адкрыць...', 'Акрыць|Адкрыццё файла з дакументам',
  // TrvActionSave
  'За&хаваць', 'Захаваць|Захоўванне дакумента',
  // TrvActionSaveAs
  'Захаваць &як...', 'Захаваць як...|Захаванне дакумента пад іншай назвай, у іншым месцы або фармаце',
  // TrvActionExport
  '&Экспарт...', 'Экспарт|Экспарт дакумента ў файл пад іншай назвай, у іншае месца або фармат',
  // TrvActionPrintPreview
  'Папярэд&ні прагляд', 'Папярэдні прагляд|Прагляд дакумента ў тым выглядзе, у якім ён будзе надрукаваны',
  // TrvActionPrint
  '&Друкаваць...', 'Друкаваць|Заданне параметраў друку і друкаванне дакумента',
  // TrvActionQuickPrint
  '&Друкаваць', '&Хуткі друк', 'Друкаваць|Друкаванне дакумента',
   // TrvActionPageSetup
  'П&араметры старонкі...', 'Параметры старонкі|Устаноўка палёў, памеру і крыніцы падачы паперы, калантытулаў',  
  // TrvActionCut
  '&Выразаць', 'Выразаць|Выдаленне выдзеленага фрагмента і змяшчэнне яго ў буфер абмену',
  // TrvActionCopy
  '&Капіраваць', 'Капіраваць|Капіраванне выдзеленага фрагмента ў буфер абмену',
  // TrvActionPaste
  'Уст&авіць', 'Уставіць|Устаўка фрагмента з буферу абмену ў цякучую пазіцыю дакумента',
  // TrvActionPasteAsText
  'Уставіць як &тэкст', 'Уставіць як тэкст|Устаўка тэксту з буфера абмену',  
  // TrvActionPasteSpecial
  'Сп&ецыяльная ўстаўка...', 'Спецыяльная ўстаўка|Устаўка зместа буфера абмена ў дакумент у выбраным фармаце',
  // TrvActionSelectAll
  'В&ыдзеліць усё', 'Выдзеліць усё|Выдзяленне ўсяго дакумента',
  // TrvActionUndo
  '&Адмяніць', 'Адмяніць|Адмена апошняй аперацыі',
  // TrvActionRedo
  'Вярнуц&ь', 'Вярнуць|Адмена дзеяння каманды "Адмяніць"',
  // TrvActionFind
  '&Знайсці...', 'Знайсці|Пошук указанага тэксту ў дакуменце',
  // TrvActionFindNext
  'Знайсці &далей', 'Знайсці далей|Працяг апошняга пошуку тэксту',
  // TrvActionReplace
  '&Замяніць...', 'Замяніць|Пошук і замена ўказанага тэксту ў дакуменце',
  // TrvActionInsertFile
  '&Файл...', 'Уставіць файл|Устаўка файла ў цякучую пазіцыю дакумента',
  // TrvActionInsertPicture
  '&Малюнак...', 'Уставіць малюнак|Устаўка малюнка ў цякучую пазіцыю дакумента',
  // TRVActionInsertHLine
  '&Гарызантальная лінія', 'Уставіць гарызантальную лінію|Устаўка гарызантальнай лініі ў цякучую пазіцыю дакумента',
  // TRVActionInsertHyperlink
  'Гіп&ерспасылка...', 'Дабавіць гіперспасылку|Дабаўленне новай або рэдагаванне выдзеленай гіперспасылкі',
  // TRVActionRemoveHyperlinks
   '&Выдаліць гіперспасылкі', 'Выдаліць гіперспасылкі|Выдаліць усе гіперспасылкі ў выдзеленым тэксце',  
  // TrvActionInsertSymbol
  '&Сімвал...', 'Уставіць сімвал|Устаўка сімвала або спецыяльнага знака',
  // TrvActionInsertNumber
  '&Нумар...', 'Уставіць нумар|Устаўляе нумар',
  // TrvActionInsertCaption
  'Уставіць &подпіс...', 'Уставіць подпіс|Устаўляе подпіс для выбраных аб''ектаў',
  // TrvActionInsertTextBox
  '&Поле тэксту', 'Уставіць поле тэксту|Устаўляе поле тэксту',
  // TrvActionInsertSidenote
  '&Прыпіска', 'Уставіць прыпіску|Устаўляе зноску, якая паказваецца ў полі тэксту',
  // TrvActionInsertPageNumber
  '&Нумар старокі', 'Уставіць нумар старонкі|Устаўляе нумар старонкі',
  // TrvActionParaList
  '&Спіс...', 'Спіс|Дабаўленне або змена фармата маркераў ці нумарацыі выдзеленых абзацаў',
  // TrvActionParaBullets
  '&Маркеры', 'Маркеры|Дабаўленне або выдаленне маркіроўкі выдзеленых абзацаў',
  // TrvActionParaNumbering
  '&Нумарацыя', 'Нумарацыя|Дабаўленне або выдаленне нумарацыі выдзеленых абзацаў',
  // TrvActionColor
  '&Колер фону...', 'Колер фону|Змена колеру фону дакумента',
  // TrvActionFillColor
  '&Заліўка...', 'Заліўка|Змена колеру фону тэксту, абзаца, табліцы або ячэйкі',
  // TrvActionInsertPageBreak
  '&Разрыў старонкі', 'Разрыў старонкі|Дабаўленне разрыва старонкі ў цякучую пазіцыю дакумента',
  // TrvActionRemovePageBreak
  '&Выдаліць разрыў старонкі', 'Выдаліць разрыў старонкі|Выдаленне разрыва старонкі',
  // TrvActionClearLeft
  'Не абцякаць &левы бок', 'Не абцякаць левы бок|Пазіцыя абзаца пад аб''ектамі, выраўнянымі па левым краі',
  // TrvActionClearRight
   'Не абцякаць &правы бок', 'Не абцякаць правы бок|Пазіцыя абзаца пад аб''ектамі, выраўнянымі па правым краі',
  // TrvActionClearBoth
  'Не абцякаць &абодва бакі', 'Не абцякаць абодва бакі|Пазіцыя абзаца пад аб''ектамі, выраўнянымі па краях',
  // TrvActionClearNone
  'Абцякаць &абодва бакі', 'Абцякаць абодва бакі|Дазвол абзацу абцякаць аб''екты, выраўняныя па краях',
  // TrvActionVAlign
  '&Пазіцыя аб''екта...', 'Пазіцыя аб''екта|Змена пазіцыі выбранага аб''екта ў тэксце',
  // TrvActionItemProperties
  'Уласцівасці &аб''екта...', 'Уласцівасці аб''екта|Заданне уласцівасцей выдзеленага малюнка, лініі або табліцы',
  // TrvActionBackground
  '&Фон...', 'Фон|Выбар фонавага малюнка і заліўкі',
  // TrvActionParagraph
  '&Абзац...', 'Абзац|Змена атрыбутаў выдзеленых абзацаў',
  // TrvActionIndentInc
  'Па&вялічыць водступ', 'Павялічыць водступ|Павелічэнне левага водступа выдзеленых абзацаў',
  // TrvActionIndentDec
  'Па&меншыць водступ', 'Зменшыць водступ|Змяншэнне левага водступа выдзеленых абзацаў',
  // TrvActionWordWrap
  'П&еранос па словах', 'Перанос па словах|Дазвол або забарона аўтаматычнага пераводу радкоў унутры выдзеленых абзацаў',
  // TrvActionAlignLeft
  'Па &левым краі', 'Па левым краі|Выраўніванне выдзеленага тэксту па левым краі',
  // TrvActionAlignRight
  'Па &правым краі', 'Па правым краі|Выраўніванне выдзеленага тэксту па правым краі',
  // TrvActionAlignCenter
  'Па &цэнтры', 'Па цэнтры|Выраўніванне выдзеленага тэксту па цэнтры',
  // TrvActionAlignJustify
  'Па &шырыні', 'Па шырыні|Выраўніванне выдзеленага тэксту па левым і правым краях',
  // TrvActionParaColor
  'Колер заліўкі а&бзаца...', 'Колер заліўкі абзаца|Выбар колеру заліўкі абзаца',
  // TrvActionLineSpacing100
  '&Адзінарны інтэрвал', 'Адзінарны інтэрвал|Устаноўка адзінарнага інтэрвалу паміж радкамі абзаца',
  // TrvActionLineSpacing150
  'Пал&утарны інтэрвал', 'Палутарны інтэрвал|Устаноўка палутарнага інтэрвалу паміж радкамі абзаца',
  // TrvActionLineSpacing200
  '&Двайны інтэрвал', 'Двайны інтэрвал|Устаноўка двайнога інтэрвалу паміж радкамі абзаца',
  // TrvActionParaBorder
  '&Рамка і заліўка абзаца...', 'Рамка і заліўка абзаца|Заданне рамкі, заліўкі і палёў выдзеленых абзацаў',
  // TrvActionInsertTable
  'Уставіць &табліцу...', '&Табліца', 'Уставіць табліцу|Устаўка табліцы ў цякучую пазіцыю дакумента',
  // TrvActionTableInsertRowsAbove
  'Дабавіць радок &вышэй', 'Дабавіць радок вышэй|Дабаўленне новага радка над выдзеленымі ячэйкамі',
  // TrvActionTableInsertRowsBelow
  'Дабавіць радок &ніжэй', 'Дабавіць радок ніжэй|Дабаўленне новага радка пад выдзеленымі ячэйкамі',
  // TrvActionTableInsertColLeft
  'Дабавіць слупок з&лева', 'Дабавіць слупок злева|Дабаўленне новага слупка злева ад выдзеленых ячэек',
  // TrvActionTableInsertColRight
  'Дабавіць слупок с&права', 'Дабавіць слупок справа|Дабаўленне новага слупка справа ад выдзеленых ячэек',
  // TrvActionTableDeleteRows
  'Выдаліць ра&дкі', 'Выдаліць радкі|Выдаленне радкоў з выдзеленымі ячэйкамі',
  // TrvActionTableDeleteCols
  'Выдаліць сл&упкі', 'Выдаліць слупкі|Выдаленне слупкоў з выдзеленымі ячэйкамі',
  // TrvActionTableDeleteTable
  '&Выдаліць табліцу', 'Выдаліць табліцу|Выдаленне табліцы',
  // TrvActionTableMergeCells
  'А&б''яднаць ячэйкі', 'Аб''ядняць ячэйкі|Аб''яднанне выдзеленых ячэек у адну',
  // TrvActionTableSplitCells
  'Р&азбіць ячэйкі...', 'Разбіць ячэйкі|Разбіванне выдзеленых ячэек на зададзеную колькасць радкоў і слупкоў',
  // TrvActionTableSelectTable
  'В&ыдзеліць табліцу', 'Выдзеліць табліцу|Выдзяленне усей табліцы',
  // TrvActionTableSelectRows
  'Выдзеліць рад&кі', 'Выдзеліць радкі|Выдзяленне радкоў, утрымліваючых выдзеленыя ячэйкі',
  // TrvActionTableSelectCols
  'Выдзеліць слуп&кі', 'Выдзеліць слупкі|Выдзяленне слупкоў, утрымліваючых выдзеленыя ячэйкі',
  // TrvActionTableSelectCell
  'Выдзеліць &ячэйку', 'Выдзеліць ячэйку|Выдзяленне ячэйкі',
  // TrvActionTableCellVAlignTop
  'Выраўняць па &верхняму краю', 'Выраўняць па верхняму краю|Выраўніванне зместу ячэйкі па верхнім краі',
  // TrvActionTableCellVAlignMiddle
  '&Цэнтраваць па вертыкалі', 'Цэнтраваць па вертыкалі|Выраўніванне зместу ячэйкі па цэнтры',
  // TrvActionTableCellVAlignBottom
  'Выраўняць па &ніжняму краю', 'Выраўняць па ніжняму краю|Выраўніванне зместу ячэйкі па ніжнім краі',
  // TrvActionTableCellVAlignDefault
  'Выраўніванне па &ўмаўчанні', 'Выраўніванне па ўмаўчанні|Устанавіць вертыкальнае выраўніванне па ўмаўчанні',
  // TrvActionTableCellRotationNone
  '&Без павароту ячэйкі', 'Без павароту ячэйкі|Паварот зместу ячэйкі на 0°',
  // TrvActionTableCellRotation90
  'Пявярнуць ячэйку на &90°', 'Пявярнуць ячэйку на 90°|Паварот зместу ячэйкі на 90°',
  // TrvActionTableCellRotation180
  'Пявярнуць ячэйку на &180°', 'Пявярнуць ячэйку на 180°|Паварот зместу ячэйкі на 180°',
  // TrvActionTableCellRotation270
  'Пявярнуць ячэйку на &270°', 'Пявярнуць ячэйку на 270°|Паварот зместу ячэйкі на 270°',
  // TrvActionTableProperties
  'Ула&сцівасці табліцы...', 'Уласцівасці табліцы|Заданне ўласцівасцей табліцы',
  // TrvActionTableGrid
  'С&етка', 'Сетка|Паказ або ўкрыццё сеткі на месцы нябачных рамак ва ўсіх табліцах',
  // TRVActionTableSplit
  '&Разбіць табліцу', 'Разбіць табліцу|Разбіць табліцу на дзве часткі, пачынаючы з выдзеленага радка',
  // TRVActionTableToText
  'Канвертаваць у &тэкст...', 'Канвертаваць у тэкст|Канвертаванне табліцы ў тэкст',
  // TRVActionTableSort
  '&Сартаванне...', 'Сартаванне|Сартаванне радкоў табліцы',
  // TrvActionTableCellLeftBorder
  '&Левая мяжа', 'Левая мяжа|Паказ або ўкрыццё левай мяжы выдзеленых ячэек',
  // TrvActionTableCellRightBorder
  '&Правая мяжа', 'Правая мяжа|Паказ або ўкрыццё правай мяжы выдзеленых ячэек',
  // TrvActionTableCellTopBorder
  '&Верхняя мяжа', 'Верхняя мяжа|Паказ або ўкрыццё верхняй мяжы выдзеленых ячэек',
  // TrvActionTableCellBottomBorder
  '&Ніжняя мяжа', 'Ніжняя мяжа|Паказ або ўкрыццё ніжняй мяжы выдзеленых ячэек',
  // TrvActionTableCellAllBorders
  'У&се межы', 'Усе межы|Паказ або ўкрыццё ўсіх межаў выдзеленых ячэек',
  // TrvActionTableCellNoBorders
  '&Няма мяжы', 'Няма мяжы|Укрыццё ўсіх межаў выдзеленых ячэек',
  // TrvActionFonts & TrvActionFontEx
  '&Шрыфт...', 'Шрыфт|Змена параметраў шрыфта і адлегласці паміж сімваламі ў выдзеленым тэксце',
  // TrvActionFontBold
  'Паў&тлусты', 'Паўтлусты|Афармленне выдзеленага тэксту паўтлустым шрыфтам',
  // TrvActionFontItalic
  '&Курсіў', 'Курсіў|Афармленне выдзеленага тэксту курсівам',
  // TrvActionFontUnderline
  'Пад&крэслены', 'Падкрэслены|Падкрэсліванне выдзеленага тэксту',
  // TrvActionFontStrikeout
  '&Закрэслены', 'Закрэслены|Закрэсліванне выдзеленага тэксту',
  // TrvActionFontGrow
  'Па&вялічыць памер', 'Павялічыць памер|Павелічэнне памеру шрыфта выдзеленага тэксту на 10%',
  // TrvActionFontShrink
  'З&меншыць памер', 'Зменшыць памер|Змяншэнне памеру шрыфта выдзеленага тэксту на 10%',
  // TrvActionFontGrowOnePoint
  'Павя&лічыць памер на 1 пт', 'Павялічыць памер на 1 пт|Павелічэнне памеру шрыфта выдзеленага тэксту на 1 пункт',
  // TrvActionFontShrinkOnePoint
  'Зме&ншыць памер на 1 пт', 'Зменшыць памер на 1 пт|Змяншэнне памеру шрыфта выдзеленага тэксту на 1 пункт',
  // TrvActionFontAllCaps
  'Усе &вялікія', 'Усе вялікія|Паказ усіх літар выдзеленага тэксту як вялікіх',
  // TrvActionFontOverline
  'Вер&хняя рыска', 'Верхняя рыска|Дабаўленне гарызантальнай рыскі над выдзеленым тэкстам',
  // TrvActionFontColor
  '&Колер тэксту...', 'Колер тэксту|Змена колеру выдзеленага тэксту',
  // TrvActionFontBackColor
  'Колер з&аліўкі тэксту...', 'Колер заліўкі тэксту|Змена колеру фону выдзеленага тэксту',
  // TrvActionAddictSpell3
  '&Правапіс', 'Правапіс|Праверка арфаграфіі',
  // TrvActionAddictThesaurus3
  '&Тэзаўрус', 'Тэзаўрус|Пошук сінонімаў',
  // TrvActionParaLTR
  'Злева направа', 'Злева направа|Устаноўка накірунку тэксту злева направа для выдзеленых абзацаў',
  // TrvActionParaRTL
  'Справа налева', 'Справа налева|Устаноўка накірунку тэксту справа налева для выдзеленых абзацаў',
  // TrvActionLTR
  'Тэкст злева направа', 'Тэкст злева направа|Устаноўка накірунку выдзеленага тэксту злева направа',
  // TrvActionRTL
  'Тэкст справа налева', 'Тэкст справа налева|Устаноўка накірунку выдзеленага тэксту справа налева',
  // TrvActionCharCase
  '&Рэгістр', 'Рэгістр|Змена рэгістру літар выдзеленага тэксту',
  // TrvActionShowSpecialCharacters
  '&Недрукуемые знакі', 'Недрукуемые знакі|Паказаць або схаваць службовыя знакі, такія як сімвал канца абзаца, прабелы і табуляцыі',
  // TrvActionSubscript
  'П&адрадковы знак', 'Падрадковы знак|Канвертаванне выдзеленага тэксту ў ніжкія індэксы',
  // TrvActionSuperscript
  'Н&адрадковы знак', 'Надрадковы знак|Канвертаванне выдзеленага тэксту ў верхнія індэксы',
  // TrvActionInsertFootnote
  '&Зноска', 'Зноска|Устаўка зноскі',
  // TrvActionInsertEndnote
  '&Канцавая зноска', 'Канцавая зноска|Даданне канцавой зноскі',
  // TrvActionEditNote
  '&Рэдагаваць спасылку', 'Рэдагаваць спасылку|Рэдагаванне тэксту цякучай зноскі',
  // TrvActionHide
  '&Схаваць', 'Схаваць|Схаваць або паказаць выдзелены фрагмент',          
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Стылі...', 'Стылі|Адкрыццё акна рэдагавання стыляў',
  // TrvActionAddStyleTemplate
  '&Стварыць стыль...', 'Стварыць стыль|Стварэнне новага стылю тэксту ці абзаца',
  // TrvActionClearFormat,
  '&Ачысціць фармат', 'Ачысціць фармат|Ачышчэнне любога фарматавання з выдзеленым фрагменце',
  // TrvActionClearTextFormat,
  'Ачысціць фармат &тэксту', 'Ачысціць фармат тэксту|Ачышчэнне любога фарматавання ў выдзеленым тэксце',
  // TrvActionStyleInspector
  '&Інспектар стыляў', 'Інспектар стыляў|Паказ або ўкрыццё Інспектара стыляў',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove, -------
  'Так', 'Адмена', 'Закрыць', 'Уставіць', '&Выбраць...', '&Захаваць...', '&Ачысціць', 'Даведка', 'Выдаліць',
  // Others  -------------------------------------------------------------------
  // percent
  '%',
  // left, top, right, bottom sides
  'Левы бок', 'Верхні бок', 'Правы бок', 'Ніжні бок',
  // save changes? confirm title
  'Захаваць змены ў %s?', 'Пацверджанне',
  // warning: losing formatting
  '%s можа змяшчаць фарматаванне, якое будзе страчана пры канверсіі ў выбраны фармат файла.'#13+
  'Захаваць дакумент у гэтым фармаце?',
  // RVF format name
  'Фармат RichView',  
  // Error messages ------------------------------------------------------------
  'Памылка',
  'Памылка чытання файла.'#13#13'Магчымыя прычыны:'#13'- файл мае фармат, які не падтрымліваецца прылажэннем;'#13+
  '- файл пашкоджаны;'#13'- файл адкрыты іншым прылажэннем, якое блакавала доступ да яго.',
  'Памылка чытання графічнага файла.'#13#13'Магчымыя прычыны:'#13'- фармат малюнка не падтрымліваецца прылажэннем;'#13+
  '- файл не ўтрымлівае малюнка;'#13+
  '- файл пашкоджаны;'#13'- файл адкрыты іншым прылажэннем, якое блакавала доступ да яго.',
  'Памылка захавання файла.'#13#13'Магчымыя прычыны:'#13'- няма месца на дыску;'#13+
  '- дыск абаронены ад запісу;'#13'- дыск не ўстаўлены ў дыскавод;'#13+
  '- адкрыты іншым прылажэннем, якое блакавала доступ да яго;'#13+
  '- носьбіт інфармацыі пашкоджаны.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Файлы фармату RichView (*.rvf)|*.rvf',  'Файлы RTF (*.rtf)|*.rtf' , 'Файлы XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Тэкставыя файлы (*.txt)|*.txt', 'Тэкставыя файлы - Юнікод (*.txt)|*.txt', 'Тэкставыя файлы - аўта (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Файлы HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - упрошчаны (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Дакументы Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Пошук завершаны', 'Радок ''%s'' не знойдзены.',
  // 1 string replaced; Several strings replaced
  'Зроблена замен: 1.', 'Зроблена замен: %d',
  // continue search from the beginning/end?
  'Дасягнуты канец дакумента. Працягнуць пошук з пачатку?',
  'Дасягнуты пачатак дакумента. Працягнуць пошук з канца?',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  'Няма заліўкі', 'Аўта',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Чорны', 'Карычневы', 'Аліўкавы', 'Цёмна-зялёны',
  'Цёмна-сізы', 'Цёмна-сіні', 'Індзіга','Шэры-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Цёмна-чырвоны', 'Аранжавы', 'Цёмна-жоўты', 'Зялёны',
  'Сіне-зялёны', 'Сіні', 'Сізы', 'Шэры-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Чырвоны', 'Светла-аранжавы', 'Травяны', 'Ізумрудны',
  'Цёмна-бірузовы', 'Цёмна-блакітны', 'Фіялетавы', 'Шэры-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Ліловы', 'Залацісты', 'Жоўты', 'Ярка-зялёны',
  'Бірузовы', 'Блакітны', 'Вішнёвы', 'Шэры-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Ружовы', 'Светла-карычневы', 'Светла-жоўты', 'Бледна-зялёны',
  'Светла-бірузовы', 'Бледна-блакітны', 'Бэзавы', 'Белы',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Няма з&аліўкі', '&Аўта', '&Іншыя колеры...', '&Стандартны',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  'Фон', '&Колер:', 'Пазіцыя', 'Малюнак', 'Узор тэксту.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center, 
  '&Няма', '&Расцягнуць', '&Жорсткая мазаіка', '&Мазаіка', 'У ц&энтры',
  // Padding button
  '&Палі...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  'Заліўка', '&Прымяніць да:', '&Іншы колер...', 'П&алі...',
  'Калі ласка, выберыце колер',
  // [apply to:] text, paragraph, table, cell
  'тэксту', 'абзаца' , 'табліцы', 'ячэйкі',  
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Шрыфт', 'Шрыфт', 'Інтэрвал',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Шрыфт:', '&Памер', '&Абрыс', 'Паў&тлусты', '&Курсіў',
  // Script, Color, Back color labels, Default charset
  'Набор &сімвалаў:', '&Колер:', 'Фо&н:', '(Па ўмаўчанні)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Відазмяненне', 'Падкрэслены', '&Рыска зверху', '&Закрэслены', '&Усе вялікія',
  // Sample, Sample text
  'Узор', 'Узор тэксту',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Інтэрвал', '&Інтэрвал:', '&Разрэджаны', '&Ушчыльнены',
  // Offset group-bix, Offset label, Down, Up,
  'Ссоўванне', '&Ссоўванне:', 'У&верх', 'У&ніз',
  // Scaling group-box, Scaling
  'Маштабаванне', '&Маштаб:',
  // Sub/super script group box, Normal, Sub, Super
  'Верхнія і ніжнія індэксы', '&Звычайны', 'П&адрадковы', 'Н&адрадковы',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  'Палі', 'З&верху:', 'З&лева:', 'З&нізу:', 'С&права:',
  // Equal values check-box
  '&Аднолькавыя значэнні',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Дабаўленне гіперспасылкі', 'Гіперспасылка', '&Тэкст:', '&URL:', '<<выдзелены фрагмент>>',
  // cannot open URL
  'Памылка адкрыцця "%s"',
  // hyperlink properties button, hyperlink style 
  '&Атрыбуты...', '&Стыль:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) сolors
  'Атрыбуты спасылкі', 'Звычайныя колеры', 'Колер падсветкі',
  // Text [color], Background [color]
  '&Тэкст:', '&Фон',
  // Underline [color]
  'Пад&крэсленне:', 
  // Text [color], Background [color] (different hotkeys)
  'Т&экст:', 'Ф&он',
  // Underline [color], Attributes group-box,
  'Пад&крэсленне:', 'Атрыбуты',
  // Underline type: always, never, active (below the mouse)
  '&Падкрэсленне:', 'заўсёды', 'ніколі', 'пры навядзенні',
  // Same as normal check-box
  'Т&акія ж, як звычайныя',
  // underline active links check-box
  '&Падкрэсліваць пры навядзенні',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Сімвал', '&Шрыфт:', '&Набор сімвалаў:', 'Юнікод',
  // Unicode block
  '&Дыяпазон:',  
  // Character Code, Character Unicode Code, No Character
  'Код сімвала: %d', 'Код сімвала: Юнікод %d', '(сімвал не выбраны)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows 
  'Устаўка табліцы', 'Памер табліцы', '&Колькасць слупкоў:', 'К&олькасць радкоў:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Шырыня табліцы', '&Аўта', 'Па шырыні &акна', '&Указаная:',
  // Remember check-box
  'Па ўмаўчанні для новых &табліц',
  // VAlign Form ---------------------------------------------------------------
  'Пазіцыя',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Уласцівасці', 'Малюнак', 'Пазіцыя і памер', 'Лінія', 'Табліца', 'Радкі', 'Ячэйкі',
  // Image Appearance, Image Misc, Number tabs
  'Выгляд', 'Рознае', 'Нумар',
  // Box position, Box size, Box appearance tabs
  'Пазіцыя', 'Памер', 'Выгляд',
  // Preview label, Transparency group-box, checkbox, label
  'Прагляд:', 'Празрыстасць', '&Празрысты', 'Празрысты &колер:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Выбраць...', '&Захаваць...', 'Аўта',
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Вертыкальнае выраўніванне', 'Вы&раўняць:',
  'знізу па базавай лініі тэксту', 'у цэнтры па базавай лініі тэксту',
  'па версе радка', 'па нізе радка', 'па цэнтры радка',
  // align to left side, align to right side
  'па левым баку', 'па правым баку',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Ссунуць на:', 'Маштаб', 'Па &шырыні:', 'Па &вышыні:', 'Першапачатковы памер: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Маштабаваць &прапарцыйна', '&Скінуць',
  // Inside the border, border, outside the border groupboxes
  'Унутры рамак', 'Рамкі', 'Па-за рамкамі',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Колер заліўкі:', '&Унутраныя палі:',
  // Border width, Border color labels
  '&Шырыня рамкі:', '&Колер рамкі:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Гарызантальны інтэрвал:', '&Вертыкальны інтэрвал:',
  // Miscellaneous groupbox, Tooltip label
  'Рознае', '&Падказка:',
  // web group-box, alt text
  'Вэб', '&Замяшчаючы тэкст:',  
  // Horz line group-box, color, width, style
  'Гарызантальная лінія', '&Колер:', '&Таўшчыня:', '&Стыль:',
  // Table group-box; Table Width, Color, CellSpacing
  'Табліца', '&Шырыня:', '&Колер заліўкі:', '&Інтэрвалы паміж ячэйкамі...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Гарызантальнае поле ячэйкі:', '&Вертыкальнае поле ячэйкі:', 'Рамкі і фон',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Бачныя бакі &рамак...', 'Аўта', 'аўта',
  // Keep row content together checkbox
  '&Згрупавать кантэнт',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Паварот', '&Няма', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Рамкі:', 'Бачныя бакі р&амак...',
  // Border, CellBorders buttons
  '&Рамка табліцы...', 'Рамкі &ячэек...',
  // Table border dialog title
  'Рамка табліцы', 'Рамкі ячэек па ўмаўчанню',  
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Друкаваць', 'Забараніць перанос &радкоў на наступную старонку', 'Колькасць радкоў у &загалоўку:',
  'Радкі загалоўка будуць надрукаваны на кожнай старонцы з табліцай',
  // top, center, bottom, default
  'З&верху', 'Па &цэнтры', 'З&нізу', 'Па &ўмаўчанню',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Параметры ячэек', 'Прыкладная &шырыня:', '&Вышыня не менш:', 'Колер &заліўкі:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Рамка', 'Бачныя стораны:', '&Цёмны колер:', '&Светлы колер', 'Коле&р:',
  // Background image
  'Малюн&ак...',  
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Гарызантальная пазіцыя', 'Вертыкальная пазіцыя', 'Пазіцыя ў тэксце',
  // Position types: align, absolute, relative
  'Выраўняць', 'Абсалютная пазіцыя', 'Адносная пазіцыя',
  // [Align] left side, center, right side
  'левы бок поля па левым баку',
  'цэнтр поля па цэнтры',
  'правы бок поля па правым баку',
  // [Align] top side, center, bottom side
  'верх поля па версе',
  'цэнтр поля па цэнтры',
  'ніз поля па нізе',
  // [Align] relative to
  'адносна',
  // [Position] to the right of the left side of
  'правы бок па левым баку',
  // [Position] below the top side of
  'ніжэй верху',
  // Anchors: page, main text area (x2)
  '&Старонка', '&Галоўная вобласць тэксту', 'С&таронка', 'Галоўная вобласць тэ&ксту',
  // Anchors: left margin, right margin
  '&Левае поле', '&Правае поле',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Верхняе поле', '&Ніжняе поле', '&Унутранае поле', '&Знешняе поле',
  // Anchors: character, line, paragraph
  '&Знак', 'Л&інія', 'Пара&граф',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Люстран&ыя палі', 'Ма&кет у ячэйцы табліцы',
  // Above text, below text
  '&Над тэкстам', '&Пад тэкстам',
  // Width, Height groupboxes, Width, Height labels
  'Шырыня', 'Вышыня', '&Шырыня:', '&Вышыня:',
  // Auto height label, Auto height combobox item
  'Аўта', 'аўта',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Вертыкальнае выраўніванне', '&Верх', '&Цэнтр', '&Ніз',
  // Border and background button and title
  'Р&амкі і фон...', 'Поле рамак і фон',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Спецыяльная ўстаўка', '&Як:', 'Фармат RTF', 'Фармат HTML',
  'Нефарматаваны тэкст', 'Тэкст у кадзіроўцы Юнікод', 'Кропкавы малюнак (BMP)', 'Метафайл (WMF)',
  'Графічныя файлы', 'URL',
  // Options group-box, styles
  'Параметры', '&Стылі:',
  // style options: apply target styles, use source styles, ignore styles
  'прымяніць стылі рэдагаванага дакумента', 'захаваць стылі і выгляд',
  'захаваць выгляд, ігнараваць стылі',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Спіс', 'Маркіраваны', 'Нумараваны', 'Маркіраваны спіс', 'Нумараваны спіс',
  // Customize, Reset, None
  '&Змяніць...', '&Скінуць', 'Няма',
  // Numbering: continue, reset to, create new
  'працягнуць нумарацыю', 'пачаць нумарацыю ў спісе з ', 'стварыць новы спіс, пачынаючы з',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Вялікія літары', 'Вялікія рымскія', 'Дзесятковыя', 'Малыя літары', 'Малыя рымскія',
  // Bullet type
  'Акружнасць', 'Круг', 'Квадрат',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Узровень:', '&Пачаць з:', '&Працягнуць', '&Нумарацыя',    
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  'Змена спісу', 'Узроўні', '&Лік:', 'Уласцівасці спісу', '&Тып спісу:',
  // List types: bullet, image
  'маркер', 'малюнак', 'Уставіць нумар|', 
  // Number format, Number, Start level from, Font button
  '&Фармат нумара:', 'Нумар', '&Нумарацыя ўзроўню з:', '&Шрыфт...',
  // Image button, bullet character, Bullet button,
  '&Малюнак...', '&Знак маркера', 'З&нак...',
  // Position of list text, bullet, number, image
  'Пазіцыя маркера', 'Пазіцыя маркера', 'Пазіцыя нумара', 'Пазіцыя малюнка', 'Пазіцыя тэксту',
  // at, left indent, first line indent, from left indent
  '', '&Водступ злева:', '&Першы радок:', 'ад левага водступа',
  // [only] one level preview, preview  
  '&Папярэдні прагляд толькі аднаго ўзроўню', 'Узор',
  // Preview text  
  'Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]  
  'справа ад', 'злева ад', 'у цэнтры',
  // level #, this level (for combo-box)
  'Узровень %d', 'Гэты ўзровень',
  // Marker character dialog title
  'Выбар знака маркера',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Рамка і заліўка абзаца', 'Рамка', 'Заліўка',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Від рамкі', '&Колер:', 'Таў&шчыня:', '&Зазор:', '&Водступы...',
  // Sample, Border type group-boxes;
  'Узор', 'Тып рамкі',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Няма', 'Адз&інарная', '&Двайная', '&Трайная', 'Тоўстая &ўнутры', 'Тоўстая &звонку',
  // Fill color group-box; More colors, padding buttons
  'Колер заліўкі', '&Іншы колер...', '&Палі...',
  // preview text
  'Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст.',
  // title and group-box in Offsets dialog
  'Водступы', 'Водступы ад тэксту да рамкі',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Абзац', 'Выраўніванне', '&Левае', '&Правае', 'Па &цэнтры', 'Па &шырыні',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Інтэрвалы', 'П&ерад:', 'П&асля:', '&Між радкоў:', '&Значэнне:',
  // Indents group-box; indents: left, right, first line
  'Водступы', 'Зле&ва:', 'Сп&рава:', 'Перш&ы радок:',
  // indented, hanging
  'Вод&ступ', 'В&ыступ', 'Узор',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Адзінарны', 'Палутарны', 'Двайны', 'Не менш чым', 'Роўна', 'Множнік',
  // preview text
  'Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст. Тэкст тэкст тэкст тэкст тэкст.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Водступы і інтэрвалы', 'Табуляцыя', 'Пазіцыя на старонцы',
  // tab stop position label; buttons: set, delete, delete all
  'Па&зіцыя табуляцыі:', '&Устанавіць', 'Выд&аліць', 'Выдаліц&ь усе',
  // tab align group; left, right, center aligns,
  'Выраўніванне', '&Левае', '&Правае', 'Па &цэнтры',
  // leader radio-group; no leader
  'Запаўняльнік', '(Няма)',
  // tab stops to be deleted, delete none, delete all labels
  'Будуць выдалены:', '', 'Усе.',
  // Pagination group-box, keep with next, keep lines together
  'Разбіўка на старонкі', 'Не &адрываць ад наступнага', 'Не &разрываць абзац',
  // Outline level, Body text, Level #
  '&Узровень:', 'Асноўны тэкст', 'Узровень %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Папярэдні прагляд', 'Па шырыні старонкі', 'Старонка поўнасцю', 'Старонкі:',
  // Invalid Scale, [page #] of #
  'Калі ласка, увядзіце лічбу ад 10 да 500', 'з %d',
  // Hints on buttons: first, prior, next, last pages
  'Да першай', 'Да папярэдняй', 'Да наступнай', 'Да апошняй',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Інтэрвалы', 'Інтэрвалы', 'Паміж &ячэйкамі', 'Паміж рамкай табліцы і ячэйкамі',
  // vertical, horizontal (x2)
  '&Вертыкальны:', '&Гарызантальны:', 'В&ертыкальны:', 'Га&рызантальны:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Рамкі', 'Параметры', '&Колер:', '&Светлы колер:', 'Цёмны &колер:',
  // Width; Border type group-box;
  '&Таўшчыня:', 'Тып рамкі',
  // Border types: none, sunken, raised, flat
  '&Няма', '&Увагунтая', 'В&ыпуклая', '&Плоская',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Разбіванне ячэек', 'Разбіць на',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Указаная колькасць радкоў і слупкоў', '&Зыходныя ячэйкі',
  // number of columns, rows, merge before
  'Колькасць сл&упкоў:', 'Колькасць рад&коў:', '&Аб''яднаць перад разбіццём',
  // to original cols, rows check-boxes
  'Разбіць на зыходныя с&лупкі', 'Разбіць на зыходныя рад&кі',
  // Add Rows form -------------------------------------------------------------
  'Дабаўленне радкоў', '&Колькасць радкоў:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Параметры старонкі', 'Старонка', 'Калантытулы',
  // margins group-box, left, right, top, bottom
  'Палі (міліметры)', 'Палі (дзюймы)', '&Левае:', '&Верхняе:', '&Правае:', '&Ніжняе:',
  // mirror margins check-box
  '&Люстэркавыя палі',
  // orientation group-box, portrait, landscape
  'Арыентацыя', '&Кніжная', '&Альбомная',
  // paper group-box, paper size, default paper source
  'Папера', '&Памер:', 'П&адача:',
  // header group-box, print on the first page, font button
  'Верхні калонтытул', '&Тэкст:', '&Друкаваць на першай старонцы', '&Шрыфт...',
  // header alignments: left, center, right
  'З&лева', 'У &цэнтры', 'Сп&рава',
  // the same for footer (different hotkeys)
  'Ніжні калонтытул', 'Т&экст:', 'Друка&ваць на першай старонцы', 'Шр&ыфт...',
  '&Злева', '&У цэнтры', 'Спр&ава',
  // page numbers group-box, start from
  'Нумары старонак', '&Друкаваць з:',
  // hint about codes
  'Вы можаце выкарыстоўваць спецыяльныя спалучэнні:'#13'&&p - нумар старонкі; &&P - колькасць старонак; &&d - дата; &&t - час.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Кадзіроўка тэкставага файла', 'Выберыце кадзіроўку тэксту:',
  // thai, japanese, chinese (simpl), korean
  'Тайская', 'Японская', 'Кітайская (Спрошчаная)', 'Карэйская',
  // chinese (trad), central european, cyrillic, west european
  'Кітайская (Традыцыйная)', 'Цэнтральна- і Усходнееўрапейская', 'Кірыліца', 'Заходнееўрапейская',
  // greek, turkish, hebrew, arabic
  'Грэцкая', 'Турэцкая', 'Іўрыт', 'Арабская',
  // baltic, vietnamese, utf-8, utf-16
  'Балтыйская', 'В''етнамская', 'Юнікод (UTF-8)', 'Унікод (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Афармленне', '&Выберыце стыль:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Канверсія ў тэкст', 'Выберыце &раздзяляльнік:',
  // line break, tab, ';', ','
  'разрыў радка', 'знак табуляцыі', 'кропка з коскай', 'коска',
  // Table sort form -----------------------------------------------------------
  // error message
  'Немагчыма сартаваць табліцу з аб''яднанымі ячэйкамі',
  // title, main options groupbox
  'Сартаванне', 'Сартаванне',
  // sort by column, case sensitive
  'Сартаваць па &слупку:', 'улічваць &рэгістр літар',
  // heading row, range or rows
  'Выклюыць радок &загалоўка', 'Радкі для сартавання: ад %d да %d',
  // order, ascending, descending
  'Упарадкаваць', 'па ўзрастанні', 'па ўбыванні',
  // data type, text, number
  'Тып', '&тэкст', '&лічбы',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Уставіць нумар', 'Уласцівасці', '&Назва лічыльніка:', '&Тып нумарацыі:',
  // numbering groupbox, continue, start from
  'НУмарацыя', 'П&рацягнуць', '&Пачаць з:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Подпіс', '&Метка:', '&Выключыць метку з подпісу',
  // position radiogroup
  'Пазіцыя', '&Над выбраным аб''ектам', '&Пад выбраным аб''ектам',
  // caption text
  'Тэкст &подпісу:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Нумарацыя', 'Фігура', 'Табліца',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Бачныя бакі рамак', 'Рамкі',
  // Left, Top, Right, Bottom checkboxes
  '&Злева', '&Зверху', '&Справа', '&Знізу',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Перамяшчэнне слупка табліцы', 'Перамяшчэнне радка табліцы',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Водступ першага радка', 'Водступ злева', 'Выступ', 'Водступ справа',
  // Hints on lists: up one level (left), down one level (right)
  'Зменшыць узровень спісу', 'Павялічыць узровень спісу',
  // Hints for margins: bottom, left, right and top
  'Ніжняе поле', 'Левае поле', 'Правае поле', 'Верхняе поле',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Па левым краі', 'Па правым краі', 'Па цэнтры', 'Па раздзяляльніку',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Звычайны', 'Звычайны водступ', 'Без інтэрвала', 'Загаловак %d', 'Абзац спісу',
  // Hyperlink, Title, Subtitle
  'Гіперспасылка', 'Назва', 'Падзагаловак',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Выдзяленне', 'Слабае выдзяленне', 'Моцнае выдзяленне', 'Строгі',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Цытата 2', 'Выдзеленая цытата', 'Слабая спасылка', 'Моцная спасылка',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Цытата', 'Пераменны HTML', 'Код HTML', 'Акронім HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Азначэнне HTML', 'Клавіятура HTML', 'Узор HTML', 'Друкарская машынка HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Стандартны HTML', 'Цытата HTML', 'Верхні калантытул', 'Ніжні калантытул', 'Нумар старонкі',
  // Caption
  'Подпіс',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Знак канцавой зноскі', 'Знак зноскі', 'Тэкст канцавой зноскі', 'Тэкст зноскі',
  // Sidenote Reference, Sidenote Text
  'Знак прыпіскі', 'Тэкст прыпіскі',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'колер', 'колер фону', 'празрысты', 'звычайны', 'колер падкрэслення',
  // default background color, default text color, [color] same as text
  'звычайны колер фону', 'звычайны колер тэксту', 'як у тэксту',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'звычайнае', 'тлустае', 'двайное', 'пункцірнае', 'тлусты пункцір', 'штрыхавое',
  // underline types: thick dashed, long dashed, thick long dashed,
  'тлустае штрыхавое', 'доўгае штрыхавое', 'тлустае доўгае штрыхавое',
  // underline types: dash dotted, thick dash dotted,
  'штрыхпункцірнае', 'тлустае штрыхпункцірнае',
  // underline types: dash dot dotted, thick dash dot dotted
  'штрыхпункцірнае з 2 кропкамі', 'тлустае штрыхпункцірнае з 2 кропкамі',
  // sub/superscript: not, subsript, superscript
  'не над-/падрадковае', 'падрадковае', 'надрадковае',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'напрамак тэксту', 'па ўмаўчанні', 'злева направа', 'справа налева',
  // bold, not bold
  'тлусты', 'нятлусты',
  // italic, not italic
  'курсіў', 'не курсіў',
  // underlined, not underlined, default underline
  'падрэсленне', 'не падкрэслены', 'падрэсленне па ўмаўчанні',
  // struck out, not struck out
  'закрэсленны', 'не закрэслены',
  // overlined, not overlined
  'рыса зверху', 'без рысы зверху',
  // all capitals: yes, all capitals: no
  'усе вялікія', 'не ўсе вялікія',
  // vertical shift: none, by x% up, by x% down
  'без вертыкальнага ссоўвання', 'ссунуты на %d%% уверх', 'ссунуты на %d%% уніз',
  // characters width [: x%]
  'шырыня літар',
  // character spacing: none, expanded by x, condensed by x
  'звычайны міжсімвальны інтэрвал', 'разрэджаны на %s', 'ушчыльнены на %s',
  // charset
  'набор сімвалаў',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'звычайны шрыфт', 'звычайны абзац', 'перанятыя ўласцівасці',
  // [hyperlink] highlight, default hyperlink attributes
  'падсветка', 'звычайныя ўласцівасці',
  // paragraph aligmnment: left, right, center, justify
  'па левым краі', 'па правым краі', 'па цэнтры', 'па шырыні',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'вышыня радка: %d%%', 'інтэрвал паміж радкамі: %s', 'вышыня радка: не менш за %s',
  'вышыня радка: дакладна %s',
  // no wrap, wrap
  'забараніць перанос радкоў', 'дазволіць перанос радкоў',
  // keep lines together: yes, no
  'не разрываць абзац', 'дазволіць разрываць абзац',
  // keep with next: yes, no
  'не адрываць ад наступнага абзаца', 'дазволіць адрываць ад наступнага абзаца',
  // read only: yes, no
  'забараніць рэдагаванне', 'дазволіць рэдагаванне',
  // body text, heading level x
  'узровень структуры: асноўны тэкст', 'узровень структуры: %d',
  // indents: first line, left, right
  'водступ першага радка', 'водступ злева', 'водступ справа',
  // space before, after
  'інтэрвал перад', 'інтэрвал пасля',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'табуляцыі', 'няма', 'выраўнанне', 'па левым краі', 'па правым краі', 'па цэнтры',
  // tab leader (filling character)
  'запаўняльнік',
  // no, yes
  'не', 'так',
  // [padding/spacing/side:] left, top, right, bottom
  'злева', 'зверху', 'справа', 'знізу',
  // border: none, single, double, triple, thick inside, thick outside
  'няма', 'адзінарны', 'двайны', 'трайны', 'тлустая ўнутры', 'тлустая звонку',
  // background, border, padding, spacing [between text and border]
  'заліўка', 'рамка', 'палі', 'водступы',
  // border: style, width, internal width, visible sides
  'стыль', 'таўшчыня', 'зазор', 'бакі',
  // style inspector -----------------------------------------------------------
  // title
  'Інспектар стыляў',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Style', '(None)', 'Paragraph', 'Font', 'Attributes',
  // border and background, hyperlink, standard [style]
  'Рамка і заліўка', 'Гіперспасылка', '(стандартны)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Стылі', 'Стыль',
  // name, applicable to,
  '&Назва:', 'Прымяняецца &да:',
  // based on, based on (no &),
  '&На падставе стылю:', 'На падставе:',
  //  next style, next style (no &)
  'Стыль &наступнага абзаца:', 'Стыль наступнага абзаца:',
  // quick access check-box, description and preview
  '&Хуткі доступ', 'Апісанне і &прагляд:',
  // [applicable to:] paragraph and text, paragraph, text
  'абзаца і тэксту', 'абзаца', 'тэксту',
  // text and paragraph styles, default font
  'Стылі тэксту і абзаца', 'Стандартны шрыфт',
  // links: edit, reset, buttons: add, delete
  'Змяніць', 'Скінуць', '&Дадаць', '&Выдаліць',
  // add standard style, add custom style, default style name
  'Дадаць &стандартны стыль...', 'Дадаць &стыль', 'Стыль %d',
  // choose style
  'Выберыце &стыль:',
  // import, export,
  '&Імпарт...', '&Экспарт...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Стылі RichView Styles (*.rvst)|*.rvst', 'Памылка загрузкі файла', 'У выбраным файле няма стыляў',
  // Title, group-box, import styles
  'Імпарт стыляў з файла', 'Імпарт', '&Імпартаваць стылі:',
  // existing styles radio-group: Title, override, auto-rename
  'Наяўныя стылі', '&замяніць новымі', '&дадаць з новымі назвамі',
  // Select, Unselect, Invert,
  '&Выбраць', '&Ачысціць', '&Інвертаваць',
  // select/unselect: all styles, new styles, existing styles
  '&Усе стылі', '&Новыя стылі', '&Наяўныя стылі',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Ачысціць фармат', 'Усе стылі',
  // dialog title, prompt
  'Стылі', '&Выберыце стыль:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Сінонімы', '&Прапусціць усе', '&Дадаць у слоўнік',
  // Progress messages ---------------------------------------------------------
  'Запампоўванне %s', 'Падрыхтоўка да друку...', 'Друкуецца старонка %d',
  'Канверсія з RTF...',  'Канверсія ў RTF...',
  'Счытванне %s...', 'Запіс %s...', 'тэкст',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Няма поля', 'Зноска', 'Канцавая зноска', 'Прыпіска', 'Тэкставае поле'
  );


initialization
  RVA_RegisterLanguage(
    'Byelorussian', // english language name, do not translate
    'Беларуская', // native language name
    RUSSIAN_CHARSET, @Messages);

end.