﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'StyleImportRVFrm.pas' rev: 27.00 (Windows)

#ifndef StyleimportrvfrmHPP
#define StyleimportrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.CheckLst.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVStyleFuncs.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Styleimportrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVStyleImport;
class PASCALIMPLEMENTATION TfrmRVStyleImport : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Checklst::TCheckListBox* lst;
	Vcl::Stdctrls::TLabel* lblLst;
	Vcl::Stdctrls::TLabel* lblDescr;
	Richview::TRichView* rv;
	Vcl::Buttons::TBitBtn* btnSelect;
	Vcl::Buttons::TBitBtn* btnUnselect;
	Vcl::Menus::TPopupMenu* pm;
	Vcl::Menus::TMenuItem* mitAll;
	Vcl::Menus::TMenuItem* mitNew;
	Vcl::Menus::TMenuItem* mitOld;
	Vcl::Stdctrls::TButton* btnInvert;
	Vcl::Extctrls::TRadioGroup* rg;
	Rvstyle::TRVStyle* rvs;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall btnInvertClick(System::TObject* Sender);
	void __fastcall mitAllClick(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall btnSelectClick(System::TObject* Sender);
	void __fastcall btnUnselectClick(System::TObject* Sender);
	void __fastcall mitNewClick(System::TObject* Sender);
	void __fastcall mitOldClick(System::TObject* Sender);
	void __fastcall lstClick(System::TObject* Sender);
	
private:
	bool CheckValue;
	Rvclasses::TRVIntegerList* NewStyles;
	
public:
	Vcl::Controls::TControl* _lst;
	Vcl::Controls::TControl* _btnSelect;
	Vcl::Controls::TControl* _btnUnselect;
	Vcl::Controls::TControl* _rg;
	System::Classes::TComponent* _pm;
	Vcl::Imglist::TCustomImageList* Images;
	Vcl::Imglist::TCustomImageList* Images2;
	int FontImageIndex;
	int HyperlinkImageIndex;
	int ParaImageIndex;
	int ParaBorderImageIndex;
	Rvstylefuncs::TRVStyleTemplateImageIndices ImageIndices;
	void __fastcall Init(Rvstyle::TRVStyle* arvs, Rvstyle::TRVStyleTemplateCollection* ExistingStyleTemplates, Rvstyle::TRVStyle* descrrvs);
	DYNAMIC void __fastcall Localize(void);
	int __fastcall GetStyleTemplateIndex(Rvstyle::TRVStyleTemplate* StyleTemplate);
	int __fastcall GetIndexInCheckedItems(int Index);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVStyleImport(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVStyleImport(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVStyleImport(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVStyleImport(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Styleimportrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_STYLEIMPORTRVFRM)
using namespace Styleimportrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// StyleimportrvfrmHPP
