unit NumSeqRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  RVStyle,
  RichViewActions, RVALocalize, NumSeqCustomRVFrm, RVSpinEdit;


type
  TfrmRVNumSeq = class(TfrmRVCustomNumSeq)
    gbNumbering: TGroupBox;
    rbContinue: TRadioButton;
    rbReset: TRadioButton;
    seStartFrom: TRVSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure seStartFromChange(Sender: TObject);
  private
    { Private declarations }
    _rbContinue, _rbReset: TControl;
    FRVStyle: TRVStyle;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    procedure Localize; override;
    procedure Init(RVStyle: TRVStyle; SeqPropList: TRVALocStringList;
      const SeqName: String; SeqType: TRVSeqType);
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    procedure GetSeqProperties(var SeqName: String; var SeqType: TRVSeqType;
      var StartFrom: Integer; var Reset: Boolean);
  end;


implementation

{$R *.dfm}

{ TfrmRVNumSeq }
procedure TfrmRVNumSeq.FormCreate(Sender: TObject);
begin
  _rbContinue := rbContinue;
  _rbReset    := rbReset;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVNumSeq.Init(RVStyle: TRVStyle; SeqPropList: TRVALocStringList;
  const SeqName: String; SeqType: TRVSeqType);
begin
  inherited Init(SeqPropList, SeqName, SeqType);
  FRVStyle := RVStyle;
end;
{------------------------------------------------------------------------------}
function TfrmRVNumSeq.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gbSeq.Left*2+gbSeq.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gbNumbering.Top-gbNumbering.Height);
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVNumSeq.GetSeqProperties(var SeqName: String;
  var SeqType: TRVSeqType; var StartFrom: Integer; var Reset: Boolean);
begin
  inherited GetSeqProperties(SeqName, SeqType);
  Reset := GetRadioButtonChecked(_rbReset);
  if not seStartFrom.Indeterminate then
    StartFrom := FRVStyle.RVUnitsToUnits(seStartFrom.Value, TRVAControlPanel(ControlPanel).UnitsDisplay);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVNumSeq.Localize;
begin
  inherited;
  gbNumbering.Caption := RVA_GetSHAsIs(rvam_in_NumberingRG, ControlPanel);
  rbContinue.Caption := RVA_GetSHAsIs(rvam_in_Continue, ControlPanel);
  rbReset.Caption := RVA_GetSHAsIs(rvam_in_StartFrom, ControlPanel);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVNumSeq.seStartFromChange(Sender: TObject);
begin
  if seStartFrom.Indeterminate then
    SetRadioButtonChecked(_rbContinue, True)
  else
    SetRadioButtonChecked(_rbReset, True);
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVNumSeq.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl=_rbContinue then
    _rbContinue := NewControl
  else if OldControl=_rbReset then
    _rbReset := NewControl
  else
    inherited;
end;
{$ENDIF}


end.
