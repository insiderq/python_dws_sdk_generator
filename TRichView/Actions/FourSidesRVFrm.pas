
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for entering margins, padding, etc.      }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit FourSidesRVFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RVSpinEdit, BaseRVFrm, RVALocalize, RVStyle;

{$I RichViewActions.inc}  

type
  TfrmRVFourSides = class(TfrmRVBase)
    gb: TGroupBox;
    lblTop: TLabel;
    lblBottom: TLabel;
    seTop: TRVSpinEdit;
    seBottom: TRVSpinEdit;
    btnOk: TButton;
    btnCancel: TButton;
    lblLeft: TLabel;
    lblRight: TLabel;
    seLeft: TRVSpinEdit;
    seRight: TRVSpinEdit;
    cbEqual: TCheckBox;
    procedure cbEqualClick(Sender: TObject);
    procedure seTopChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Updating: Boolean;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _gb, _cbEqual: TControl;
    procedure SetValues(Left,Top,Right,Bottom: TRVStyleLength;
      FLeft,FTop,FRight,FBottom: Boolean; RVStyle: TRVStyle;
      OnlyPositive: Boolean; Units: TRVUnits);
    procedure SetValues2(Left,Top,Right,Bottom: TRVStyleLength;
      FLeft,FTop,FRight,FBottom: Boolean; RVStyle: TRVStyle;
      MinValue: Extended; Units: TRVUnits);
    procedure GetValues(var Left,Top,Right,Bottom: TRVStyleLength;
      var FLeft,FTop,FRight,FBottom: Boolean; RVStyle: TRVStyle;
      Units: TRVUnits);
    procedure SetMinValue(MinValue: Integer);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation

{$R *.dfm}

{ TfrmRVFourSides }

function TfrmRVFourSides.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gb.Left*2+gb.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gb.Top-gb.Height);
  Result := True;
end;

procedure TfrmRVFourSides.GetValues(var Left,Top,Right,Bottom: TRVStyleLength;
  var FLeft,FTop,FRight,FBottom: Boolean; RVStyle: TRVStyle; Units: TRVUnits);
begin
  FLeft   := not seLeft.Indeterminate;
  FRight  := not seRight.Indeterminate;
  FTop    := not seTop.Indeterminate;
  FBottom := not seBottom.Indeterminate;
  Left   := RVStyle.RVUnitsToUnits(seLeft.Value, Units);
  Right  := RVStyle.RVUnitsToUnits(seRight.Value, Units);
  Top    := RVStyle.RVUnitsToUnits(seTop.Value, Units);
  Bottom := RVStyle.RVUnitsToUnits(seBottom.Value, Units);
end;

procedure TfrmRVFourSides.SetValues(Left,Top,Right,Bottom: TRVStyleLength;
  FLeft,FTop,FRight,FBottom: Boolean; RVStyle: TRVStyle;
  OnlyPositive: Boolean; Units: TRVUnits);
begin
  UpdateLengthSpinEdit2(seLeft, Units, OnlyPositive);
  UpdateLengthSpinEdit2(seRight, Units, OnlyPositive);
  UpdateLengthSpinEdit2(seTop, Units, OnlyPositive);
  UpdateLengthSpinEdit2(seBottom, Units, OnlyPositive);
  if FLeft then
    seLeft.Value := RVStyle.GetAsRVUnits(Left, Units)
  else
    seLeft.Indeterminate := True;
  if FRight then
    seRight.Value := RVStyle.GetAsRVUnits(Right, Units)
  else
    seRight.Indeterminate := True;
  if FTop then
    seTop.Value := RVStyle.GetAsRVUnits(Top, Units)
  else
    seTop.Indeterminate := True;
  if FBottom then
    seBottom.Value := RVStyle.GetAsRVUnits(Bottom, Units)
  else
    seBottom.Indeterminate := True;

  SetCheckBoxChecked(_cbEqual, (Left=Top) and (Top=Right) and (Right=Bottom) and
    (FLeft=FTop) and (FTop=FRight) and (FRight=FBottom));
end;

procedure TfrmRVFourSides.SetValues2(Left,Top,Right,Bottom: TRVStyleLength;
  FLeft,FTop,FRight,FBottom: Boolean; RVStyle: TRVStyle;
  MinValue: Extended; Units: TRVUnits);
begin
  UpdateLengthSpinEdit2(seLeft, Units, True);
  UpdateLengthSpinEdit2(seRight, Units, True);
  UpdateLengthSpinEdit2(seTop, Units, True);
  UpdateLengthSpinEdit2(seBottom, Units, True);
  seTop.MinValue := MinValue;
  seLeft.MinValue := MinValue;
  seBottom.MinValue := MinValue;
  seRight.MinValue := MinValue;
  if FLeft then
    seLeft.Value := RVStyle.GetAsRVUnits(Left, Units)
  else
    seLeft.Indeterminate := True;
  if FRight then
    seRight.Value := RVStyle.GetAsRVUnits(Right, Units)
  else
    seRight.Indeterminate := True;
  if FTop then
    seTop.Value := RVStyle.GetAsRVUnits(Top, Units)
  else
    seTop.Indeterminate := True;
  if FBottom then
    seBottom.Value := RVStyle.GetAsRVUnits(Bottom, Units)
  else
    seBottom.Indeterminate := True;

  SetCheckBoxChecked(_cbEqual, (Left=Top) and (Top=Right) and (Right=Bottom) and
    (FLeft=FTop) and (FTop=FRight) and (FRight=FBottom));
end;

procedure TfrmRVFourSides.cbEqualClick(Sender: TObject);
var v: Integer;
begin
  if not Visible then exit;
  if GetCheckBoxChecked(_cbEqual) then begin
    v := (seLeft.AsInteger+seRight.AsInteger+seTop.AsInteger+seBottom.AsInteger) div 4;
    seLeft.Value := v;
    seTop.Value := v;
    seRight.Value := v;
    seBottom.Value := v;
  end;
end;

procedure TfrmRVFourSides.seTopChange(Sender: TObject);
begin
  if not Visible or Updating or not GetCheckBoxChecked(_cbEqual) then
    exit;
  Updating := True;
  if seLeft<>Sender then begin
    seLeft.Indeterminate := TRVSpinEdit(Sender).Indeterminate;
    if not TRVSpinEdit(Sender).Indeterminate then
      seLeft.Value := TRVSpinEdit(Sender).Value;
  end;
  if seTop<>Sender then begin
    seTop.Indeterminate := TRVSpinEdit(Sender).Indeterminate;
    if not TRVSpinEdit(Sender).Indeterminate then
      seTop.Value := TRVSpinEdit(Sender).Value;
  end;
  if seRight<>Sender then begin
    seRight.Indeterminate := TRVSpinEdit(Sender).Indeterminate;
    if not TRVSpinEdit(Sender).Indeterminate then
      seRight.Value := TRVSpinEdit(Sender).Value;
  end;
  if seBottom<>Sender then begin
    seBottom.Indeterminate := TRVSpinEdit(Sender).Indeterminate;
    if not TRVSpinEdit(Sender).Indeterminate then
      seBottom.Value := TRVSpinEdit(Sender).Value;
  end;
  Updating := False;
end;

procedure TfrmRVFourSides.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  Caption := RVA_GetS(rvam_4s_DefTitle, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_4s_DefTitle, ControlPanel);
  lblLeft.Caption := RVA_GetSAsIs(rvam_4s_Left, ControlPanel);
  lblRight.Caption := RVA_GetSAsIs(rvam_4s_Right, ControlPanel);
  lblTop.Caption := RVA_GetSAsIs(rvam_4s_Top, ControlPanel);
  lblBottom.Caption := RVA_GetSAsIs(rvam_4s_Bottom, ControlPanel);
  cbEqual.Caption := RVA_GetSAsIs(rvam_4s_EqualValues, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVFourSides.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _gb then
    _gb := NewControl
  else if OldControl = _cbEqual then
    _cbEqual := NewControl;
end;

{$ENDIF}

procedure TfrmRVFourSides.FormCreate(Sender: TObject);
begin
  _gb := gb;
  _cbEqual := cbEqual;
  inherited;
end;

procedure TfrmRVFourSides.SetMinValue(MinValue: Integer);
begin
  seLeft.MinValue := MinValue;
  seTop.MinValue := MinValue;
  seRight.MinValue := MinValue;
  seBottom.MinValue := MinValue;    
end;

end.
