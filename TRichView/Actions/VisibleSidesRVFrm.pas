{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Show/hide border sides dialog                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit VisibleSidesRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, RVALocalize,
  Dialogs, BaseRVFrm, StdCtrls;

type
  TfrmRVVisibleSides = class(TfrmRVBase)
    gb: TGroupBox;
    cbTop: TCheckBox;
    cbLeft: TCheckBox;
    cbRight: TCheckBox;
    cbBottom: TCheckBox;
    btnOk: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    _cbLeft, _cbRight, _cbTop, _cbBottom: TControl;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}    
  public
    { Public declarations }
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    procedure Localize; override;
    procedure SetSides(Left,Top,Right,Bottom,
      DefLeft,DefTop,DefRight,DefBottom: Boolean);
    procedure GetSides(var Left,Top,Right,Bottom,
      DefLeft,DefTop,DefRight,DefBottom: Boolean);
  end;


implementation

{$R *.dfm}

procedure TfrmRVVisibleSides.FormCreate(Sender: TObject);
begin
  _cbLeft   := cbLeft;
  _cbRight  := cbRight;
  _cbTop    := cbTop;
  _cbBottom := cbBottom;
  inherited;
end;
{------------------------------------------------------------------------------}
function TfrmRVVisibleSides.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := gb.Left*2+gb.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gb.Top-gb.Height);
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVVisibleSides.GetSides(var Left,Top,Right,Bottom,
  DefLeft,DefTop,DefRight,DefBottom: Boolean);

  procedure GetCheckBox(cb: TControl; var Value, Defined: Boolean);
  begin
    Defined := True;
    case GetCheckBoxState(cb) of
      cbChecked:
        Value := True;
      cbUnchecked:
        Value := False;
      cbGrayed:
        Defined := False;
    end;
  end;

begin
  GetCheckBox(_cbLeft, Left, DefLeft);
  GetCheckBox(_cbTop, Top, DefTop);
  GetCheckBox(_cbRight, Right, DefRight);
  GetCheckBox(_cbBottom, Bottom, DefBottom);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVVisibleSides.SetSides(Left,Top,Right,Bottom,
  DefLeft,DefTop,DefRight,DefBottom: Boolean);

  procedure SetCheckBox(cb: TControl; Value, Defined: Boolean);
  begin
    if Defined then
      SetCheckBoxChecked(cb, Value)
  end;

begin
  SetCheckBox(_cbLeft, Left, DefLeft);
  SetCheckBox(_cbTop, Top, DefTop);
  SetCheckBox(_cbRight, Right, DefRight);
  SetCheckBox(_cbBottom, Bottom, DefBottom);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVVisibleSides.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_vs_Title, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_vs_GB, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  cbLeft.Caption := RVA_GetSAsIs(rvam_vs_Left, ControlPanel);
  cbTop.Caption := RVA_GetSAsIs(rvam_vs_Top, ControlPanel);
  cbRight.Caption := RVA_GetSAsIs(rvam_vs_Right, ControlPanel);
  cbBottom.Caption := RVA_GetSAsIs(rvam_vs_Bottom, ControlPanel);      
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVVisibleSides.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  inherited;
  if OldControl = _cbLeft then
    _cbLeft := NewControl
  else if OldControl= _cbRight then
    _cbRight := NewControl
  else if OldControl = _cbTop then
    _cbTop := NewControl
  else if OldControl= _cbBottom then
    _cbBottom  := NewControl;
end;
{$ENDIF}


end.
