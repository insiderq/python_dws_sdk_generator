
{*******************************************************}
{                                                       }
{       RichViewActions (not included in main set)      }
{       Dialog for applying HTML-style lists            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit ListGallery2RVFrm;

{$I RichViewActions.inc}
{$IFNDEF RVDONOTUSELISTS}
{$R htmlbullets.res}
{$ENDIF}

interface

{$IFNDEF RVDONOTUSELISTS}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, BaseRVFrm, ImgList, StdCtrls, RVOfficeRadioBtn, ComCtrls,
  {$IFDEF USERVKSDEVTE}
  te_controls, te_extctrls,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntComCtrls, TntStdCtrls,
  {$ENDIF}
  CRVData, RVRVData, RichView,
  RVSpinEdit, RVEdit, RVStyle, RVStr, RVALocalize;

type
  TfrmRVListGallery2 = class(TfrmRVBase)
    il1: TImageList;
    pc: TPageControl;
    tsBullets: TTabSheet;
    tsNumbering: TTabSheet;
    rg1: TRVOfficeRadioGroup;
    rg2: TRVOfficeRadioGroup;
    lblLevel: TLabel;
    seLevel: TRVSpinEdit;
    btnOk: TButton;
    btnCancel: TButton;
    gbNumbering: TGroupBox;
    seStartFrom: TRVSpinEdit;
    rbContinue: TRadioButton;
    rbStartFrom: TRadioButton;
    procedure seStartFromChange(Sender: TObject);
    procedure rg2DblClickItem(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    frve: TCustomRichViewEdit;
    BeforeListNo, AfterListNo: Integer;
    FIndentStep: TRVStyleLength;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _rbStartFrom, _pc, _tsBullets, _tsNumbering: TControl;
    procedure Init(rve: TCustomRichViewEdit; IndentStep: TRVStyleLength);
    procedure GetListStyle(var ListStyle: TRVListInfo; var ListNo, Level, StartFrom: Integer;
      var UseStartFrom: Boolean);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;
{$ENDIF}

implementation

{$IFNDEF RVDONOTUSELISTS}

{$R *.dfm}

{ TfrmRVListGallery2 }

procedure TfrmRVListGallery2.GetListStyle(var ListStyle: TRVListInfo; var ListNo, Level, StartFrom: Integer;
      var UseStartFrom: Boolean);
begin
  if ((GetPageControlActivePageIndex(_pc)=0) and (rg1.ItemIndex<1)) or
     ((GetPageControlActivePageIndex(_pc)=1) and (rg2.ItemIndex<1)) then begin
    ListStyle := nil;
    exit;
  end;
  ListStyle := TRVListInfo.Create(nil);
  ListStyle.Standard := False;
  with ListStyle.Levels.Add do begin
    Font.Size := 12;
    case GetPageControlActivePageIndex(_pc) of
      0:
        begin
          ListType := rvlstBullet;
          case rg1.ItemIndex of
            1:
              begin
                Font.Name := 'Symbol';
                Font.Charset := SYMBOL_CHARSET;
                FormatString := RVCHAR_MIDDLEDOT;
              end;
            2:
              begin
                Font.Name := 'Courier New';
                Font.Charset := ANSI_CHARSET;
                FormatString := 'o';
              end;
            3:
              begin
                Font.Name := 'Wingdings';
                Font.Charset := SYMBOL_CHARSET;
                FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
              end;
          end;
        end;
      1:
        begin
          FormatString := '%0:s.';
          case rg2.ItemIndex of
            1: ListType := rvlstDecimal;
            2: ListType := rvlstUpperAlpha;
            3: ListType := rvlstLowerAlpha;
            4: ListType := rvlstUpperRoman;
            5: ListType := rvlstLowerRoman;
          end;
        end;
    end;
    {
    if cbCompact.Checked then begin
      LeftIndent   := FIndentStep * seLevel.AsInteger;
      MarkerIndent := FIndentStep * seLevel.AsInteger;
      FirstIndent  := FIndentStep;
      end
    else} begin
      LeftIndent   := FIndentStep * (seLevel.AsInteger+1);
      MarkerIndent := FIndentStep * seLevel.AsInteger;
      FirstIndent  := 0;
    end;
  end;
  if (BeforeListNo>=0) and ListStyle.IsSimpleEqual(frve.Style.ListStyles[BeforeListNo], True) then
    ListNo := BeforeListNo
  else if (AfterListNo>=0) and ListStyle.IsSimpleEqual(frve.Style.ListStyles[AfterListNo], True) then
    ListNo := AfterListNo
  else
    ListNo := -1;
  Level := seLevel.AsInteger;
  StartFrom := seStartFrom.AsInteger;
  UseStartFrom := GetRadioButtonChecked(_rbStartFrom) and not ((ListNo<0) and (StartFrom=1));
end;

function TfrmRVListGallery2.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := pc.Left*2+pc.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-pc.Top-pc.Height);
  Result := True;
end;

procedure TfrmRVListGallery2.Init(rve: TCustomRichViewEdit; IndentStep: TRVStyleLength);
var ListNo, Level, Dummy1: Integer;
    StartFrom, StartNo, StartOffs, EndNo, EndOffs: Integer;
    UseStartFrom: Boolean;
begin
  FIndentStep := IndentStep;
  rve := rve.TopLevelEditor;
  frve := rve;
  rve.RVData.GetSelectionBoundsEx(StartNo, StartOffs, EndNo, EndOffs, True);
  if (rve.GetListMarkerInfo(EndNo, ListNo, Level, StartFrom, UseStartFrom)>=0) and (ListNo>=0) then begin
    if UseStartFrom then
      seStartFrom.Value := StartFrom;
    SetRadioButtonChecked(_rbStartFrom, UseStartFrom);
    case rve.Style.ListStyles[ListNo].Levels[Level].ListType of
      rvlstBullet:
        begin
          SetPageControlActivePage(_pc, _tsBullets);
          if rve.Style.ListStyles[ListNo].Levels[Level].FormatString=#$B7 then
            rg1.ItemIndex := 1
          else if rve.Style.ListStyles[ListNo].Levels[Level].FormatString='o' then
            rg1.ItemIndex := 2
          else if rve.Style.ListStyles[ListNo].Levels[Level].FormatString=#$A7 then
            rg1.ItemIndex := 2;
        end;
      rvlstDecimal:
        begin
          SetPageControlActivePage(_pc, _tsNumbering);
          rg2.ItemIndex := 1;
        end;
      rvlstUpperAlpha:
        begin
          SetPageControlActivePage(_pc, _tsNumbering);
          rg2.ItemIndex := 2;
        end;
      rvlstLowerAlpha:
        begin
          SetPageControlActivePage(_pc, _tsNumbering);
          rg2.ItemIndex := 3;
        end;
      rvlstUpperRoman:
        begin
          SetPageControlActivePage(_pc, _tsNumbering);
          rg2.ItemIndex := 4;
        end;
      rvlstLowerRoman:
        begin
          SetPageControlActivePage(_pc, _tsNumbering);
          rg2.ItemIndex := 5;
        end;
    end;
    if Round(rve.Style.ListStyles[ListNo].Levels[Level].LeftIndent / FIndentStep - 1)<0 then
      seLevel.Value := 0
    else
      seLevel.Value := Round(rve.Style.ListStyles[ListNo].Levels[Level].LeftIndent / FIndentStep - 1);
  end;
  BeforeListNo := -1;
  AfterListNo  := -1;
  while (StartNo>0) and not rve.IsParaStart(StartNo) do
    dec(StartNo);
  if StartNo>0 then begin
    dec(StartNo);
    while (StartNo>0) and not rve.IsParaStart(StartNo) do
      dec(StartNo);
    if (rve.GetListMarkerInfo(StartNo, ListNo, Level, Dummy1, UseStartFrom)>=0) then
      BeforeListNo := ListNo;
  end;
  while (EndNo<rve.ItemCount-1) and not rve.IsParaStart(EndNo) do
    inc(EndNo);
  if rve.IsParaStart(EndNo) and (rve.GetListMarkerInfo(EndNo, ListNo, Level, Dummy1, UseStartFrom)>=0) then
    AfterListNo := ListNo;
end;


procedure TfrmRVListGallery2.seStartFromChange(Sender: TObject);
begin
  if Visible then
    SetRadioButtonChecked(_rbStartFrom, True);
end;

procedure TfrmRVListGallery2.rg2DblClickItem(Sender: TObject);
begin
  ModalResult := mrOk;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVListGallery2.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _rbStartFrom then
    _rbStartFrom := NewControl
  else if OldControl = _pc then begin
    _pc := NewControl;
    {$IFDEF USERVKSDEVTE}
    _tsBullets := TTePageControl(_pc).Pages[0];
    _tsNumbering := TTePageControl(_pc).Pages[1];
    {$ENDIF}
    {$IFDEF USERVTNT}
    _tsBullets := TTntPageControl(_pc).Pages[0];
    _tsNumbering := TTntPageControl(_pc).Pages[1];
    {$ENDIF}
  end;
end;
{$ENDIF}

procedure TfrmRVListGallery2.FormCreate(Sender: TObject);
var bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  bmp.Handle := CreateGrayMappedRes(hInstance, 'RVA_HTMLBULLETS');
  il1.AddMasked(bmp, bmp.Canvas.Pixels[0,bmp.Height-1]);
  bmp.Free;

  _rbStartFrom := rbStartFrom;
  _pc          := pc;
  _tsBullets   := tsBullets;
  _tsNumbering := tsNumbering;
  inherited;
  PrepareImageList(il1);
end;

procedure TfrmRVListGallery2.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_lg_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  tsBullets.Caption := RVA_GetSHAsIs(rvam_lg_BulletTab, ControlPanel);
  tsNumbering.Caption := RVA_GetSHAsIs(rvam_lg_NumTab, ControlPanel);
  rg1.Caption := RVA_GetSH(rvam_lg_BulletGB, ControlPanel);
  rg2.Caption := RVA_GetSH(rvam_lg_NumGB, ControlPanel);
  gbNumbering.Caption := RVA_GetSHAsIs(rvam_lg2_Numbering, ControlPanel);
  rg2.Items[0].Caption := RVA_GetS(rvam_lg_None, ControlPanel);
  rg2.Items[1].Caption := RVA_GetS(rvam_lg2_Decimal, ControlPanel);
  rg2.Items[2].Caption := RVA_GetS(rvam_lg2_UpperAlpha, ControlPanel);
  rg2.Items[3].Caption := RVA_GetS(rvam_lg2_LowerAlpha, ControlPanel);
  rg2.Items[4].Caption := RVA_GetS(rvam_lg2_UpperRoman, ControlPanel);
  rg2.Items[5].Caption := RVA_GetS(rvam_lg2_LowerRoman, ControlPanel);
  rg1.Items[0].Caption := RVA_GetS(rvam_lg_None, ControlPanel);
  rg1.Items[1].Caption := RVA_GetS(rvam_lg2_Disc, ControlPanel);
  rg1.Items[2].Caption := RVA_GetS(rvam_lg2_Circle, ControlPanel);
  rg1.Items[3].Caption := RVA_GetS(rvam_lg2_Square, ControlPanel);
  lblLevel.Caption := RVA_GetSAsIs(rvam_lg2_Level, ControlPanel);
  rbContinue.Caption := RVA_GetSAsIs(rvam_lg2_Continue, ControlPanel);
  rbStartFrom.Caption := RVA_GetSAsIs(rvam_lg2_StartFrom, ControlPanel);
end;

{$ENDIF}

end.