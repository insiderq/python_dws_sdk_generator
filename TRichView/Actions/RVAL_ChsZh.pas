
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Chinese (Zh) translation                        }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{       Original translator: Jiansong Liu               }
{       jiansongliu@hotmail.com                         }
{                                                       }
{*******************************************************}
{ Updated: 2008-05-20 by Brandon Rock                   }
{          2011-03-02 by Alconost                       }
{          2011-09-19 by Alconost                       }
{          2012-09-13 by Alconost                       }
{          2014-04-26 by Alconost                       }
{*******************************************************}

unit RVAL_ChsZh;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  '英寸', '厘米', '毫米', '十二点活字', '像素', '点',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '文件(&F)', '编辑(&E)', '格式(&O)', '字体(&N)', '段落(&P)', '插入(&I)', '表格(&T)', '窗口(&W)', '帮助(&H)',
  // exit
  '退出(&X)',
  // top-level menus: View, Tools,
  '查看(&V)', '工具(&L)',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '大小', '样式', '选择(&T)', '排列单元格内容(&N)', '单元格边框(&E)',
  // menus: Table cell rotation
  '旋转单元格(&R)', 
  // menus: Text flow, Footnotes/endnotes
  '正文排列(&T)', '脚注(&F)',
  // ribbon tabs: tab1, tab2, view, table
  '首页(&H)', '高级(&A)', '视图(&V)', '表格(&T)',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '剪贴板', '字体', '段落', '列表', '编辑',
  // ribbon groups: insert, background, page setup,
  '插入', '背景', '页面设置',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  '链接', '脚注', '文档视图', '显示/隐藏', '缩放', '选项',
  // ribbon groups on table tab: insert, delete, operations, borders
  '插入', '删除', '操作', '边框',
  // ribbon groups: styles 
  '样式',
  // ribbon screen tip footer,
  '按 F1 获取更多信息',
  // ribbon app menu: recent files, save-as menu title, print menu title
  '最近的文档', '保存文档副本', '预览和打印文档',
  // ribbon label: units combo
  '单位：',          
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '新建(&N)', '新建|创建空文档',
  // TrvActionOpen
  '打开(&O)...', '打开|打开磁盘上的文档',
  // TrvActionSave
  '保存(&S)', '保存|保存文档到磁盘',
  // TrvActionSaveAs
  '另存为(&A)...', '另存为...|以另一个文件名保存文档',
  // TrvActionExport
  '导出(&E)...', '导出|导出文档为其它格式',
  // TrvActionPrintPreview
  '打印预览(&R)', '打印预览|预览文档的打印格式',
  // TrvActionPrint
  '打印(&P)...', '打印|应用打印设置，打印文档',
  // TrvActionQuickPrint
  '打印(&P)', '快速打印(&Q)', '打印|打印当前文档',
  // TrvActionPageSetup
  '页面设置(&G)...', '页面设置|设置边框、纸张大小、打印方向、来源、页眉和页脚',
  // TrvActionCut
  '剪切(&T)', '剪切|删除选择内容，并加入到剪贴板',
  // TrvActionCopy
  '复制(&C)', '复制|将当前选中内容加入到剪贴板',
  // TrvActionPaste
  '粘贴(&P)', '粘贴|插入剪贴板上的内容',
  // TrvActionPasteAsText
  '粘贴为文本(&T)', '粘贴为文本|从剪贴板中插入文本',  
  // TrvActionPasteSpecial
  '粘贴特殊内容(&S)...', '粘贴特殊内容|以特别的格式插入剪贴板上的内容',
  // TrvActionSelectAll
  '全选(&A)', '全选|选择文档的全部内容',
  // TrvActionUndo
  '撤销(&U)', '撤销|撤销最近的一次操作',
  // TrvActionRedo
  '恢复(&R)', '恢复|恢复最近撤销的一次操作',
  // TrvActionFind
  '查找(&F)...', '查找|在文档中查找指定的文本',
  // TrvActionFindNext
  '查找下一个(&N)', '查找下一个|继续上一次的查找',
  // TrvActionReplace
  '替换(&R)...', '替换|在文档中查找并替换指定的文本',
  // TrvActionInsertFile
  '文件(&F)...', '插入文件|在文档中插入文件内容',
  // TrvActionInsertPicture
  '图片(&P)...', '插入图片|从磁盘上插入图片',
  // TRVActionInsertHLine
  '水平线(&H)', '插入水平线|插入水平线',
  // TRVActionInsertHyperlink
  '超链接(&L)...', '插入超链接|插入超链接',
  // TRVActionRemoveHyperlinks
  '删除超链接(&R)', '删除超链接|删除选定文本中的所有超链接',  
  // TrvActionInsertSymbol
  '符号(&S)...', '插入符号|插入符号',
  // TrvActionInsertNumber
  '编号(&N)...', '插入编号|插入一个编号',
  // TrvActionInsertCaption
  '插入说明(&C)...', '插入说明|插入选定对象的说明',
  // TrvActionInsertTextBox
  '文本框(&T)', '插入文本框|插入一个文本框',
  // TrvActionInsertSidenote
  '旁注(&S）', '插入旁注|插入一个旁注',
  // TrvActionInsertPageNumber
  '页码(&P)', '插入页码|插入一个页码',
  // TrvActionParaList
  '列表和编号(&B)...', '列表和序号|对选中的段落应用或编辑列表和编号',
  // TrvActionParaBullets
  '列表(&B)', '列表|对段落增减列表符号',
  // TrvActionParaNumbering
  '编号(&N)', '编号|对段落增减编号',
  // TrvActionColor
  '背景颜色(&C)...', '背景|改变文档背景的颜色',
  // TrvActionFillColor
  '填充颜色(&F)...', '填充颜色|改变文本、段落、单元格、表格或文档的背景颜色',
  // TrvActionInsertPageBreak
  '插入分页符(&I)', '插入分页符|插入分页符',
  // TrvActionRemovePageBreak
  '删除分页符(&R)', '删除分页符|删除分页符',
  // TrvActionClearLeft
  '清除左侧正文(&L)', '清除左侧正文|将该段落放在任意左侧对齐图片下',
  // TrvActionClearRight
  '清除右侧正文(&R)', '清除右侧正文|将该段落放在任意右侧对齐图片下',
  // TrvActionClearBoth
  '清除两侧正文(&B)', '清除两侧正文|将该段落放在任意左侧或右侧对齐图片下',
  // TrvActionClearNone
  '正常正文排列(&N)', '正常文本流程|允许正文环绕左侧和右侧对齐图片',
  // TrvActionVAlign
  '对象位置(&O)...', '对象位置|更改选定对象的位置',    
  // TrvActionItemProperties
  '对象属性(&P)...', '对象属性|定义当前对象的属性',
  // TrvActionBackground
  '背景(&B)...', '背景|选择背景颜色或图象',
  // TrvActionParagraph
  '段落(&P)...', '段落|改变段落属性',
  // TrvActionIndentInc
  '增加缩进(&I)', '增加缩进|增加选中段落的左边缩进量',
  // TrvActionIndentDec
  '减少缩进(&D)', '减少缩进|减少左边段落的缩进量',
  // TrvActionWordWrap
  '自动换行(&W)', '自动换行|改变当前段落自动换行状态',
  // TrvActionAlignLeft
  '居左(&L)', '居左|选中的文字靠左对齐',
  // TrvActionAlignRight
  '居右(&R)', '居右|选中的文字靠右对齐',
  // TrvActionAlignCenter
  '居中(&C)', '居中|选中的文字靠中间对齐',
  // TrvActionAlignJustify
  '两端对齐(&J)', '两端对齐|选中的文字两端对齐',
  // TrvActionParaColor
  '段落背景颜色(&B)...', '段落背景颜色|设置段落的背景颜色',
  // TrvActionLineSpacing100
  '单倍行距(&S)', '单倍行距|设置为单倍行距',
  // TrvActionLineSpacing150
  '1.5倍行距(&N)', '1.5倍行距|设置为1.5倍默认行距',
  // TrvActionLineSpacing200
  '双倍行距(&D)', '双倍行距|设置为2倍默认行距',
  // TrvActionParaBorder
  '段落边框和背景(&B)...', '段落边框和背景|设置当前段落的边框和背景',
  // TrvActionInsertTable
  '插入表格(&I)...', '表格(&T)', '插入表格|插入新表格',
  // TrvActionTableInsertRowsAbove
  '上方插入行(&A)', '上方插入行|在当前单元格的上方插入新行',
  // TrvActionTableInsertRowsBelow
  '下方插入行(&B)', '下方插入行|在当前单元格的下方插入新行',
  // TrvActionTableInsertColLeft
  '左边插入列(&L)', '左边插入列|在当前单元格的左边插入新列',
  // TrvActionTableInsertColRight
  '右边插入列(&R)', '右边插入列|在当前单元格的右边插入新列',
  // TrvActionTableDeleteRows
  '删除行(&O)', '删除行|删除当前选中单元格所在的行',
  // TrvActionTableDeleteCols
  '删除列(&C)', '删除列|删除当前选中单元格所在的列',
  // TrvActionTableDeleteTable
  '删除表格(&D)', '删除表格|删除整张表格',
  // TrvActionTableMergeCells
  '合并单元格(&M)', '合并单元格|合并选中的单元格',
  // TrvActionTableSplitCells
  '拆分单元格(&S)...', '拆分单元格|拆分当前选中的单元格',
  // TrvActionTableSelectTable
  '选中表格(&T)', '选中表格|选中整张表格',
  // TrvActionTableSelectRows
  '选中行(&W)', '选中行|选中行',
  // TrvActionTableSelectCols
  '选中列(&U)', '选中列|选中列',
  // TrvActionTableSelectCell
  '选中单元格(&E)', '选中单元格|选中单元格',
  // TrvActionTableCellVAlignTop
  '单元格顶端对齐(&T)', '单元格顶端对齐|单元格中的内容向顶端对齐',
  // TrvActionTableCellVAlignMiddle
  '单元格居中(&M)', '单元格内容居中|单元格中的内容居中对齐',
  // TrvActionTableCellVAlignBottom
  '单元格底部对齐(&B)', '单元格底部对齐|单元格内容向底部对齐',
  // TrvActionTableCellVAlignDefault
  '默认单元格垂直对齐方式(&D)', '默认单元格垂直对齐方式|选中的单元格设置为默认的垂直对齐方式',
  // TrvActionTableCellRotationNone
  '&不旋转单元格', '不旋转单元格|将所有单元格内容旋转 0 度',
  // TrvActionTableCellRotation90
  '将单元格旋转 &90 度', '将单元格旋转 90 度|将单元格内容旋转 90 度',
  // TrvActionTableCellRotation180
  '将单元格旋转 &180 度', '将单元格旋转 180 度|将单元格内容旋转 180 度',
  // TrvActionTableCellRotation270
  '将单元格旋转 &270 度', '将单元格旋转 270 度|将单元格内容旋转 270 度',
  // TrvActionTableProperties
  '表格属性(&P)...', '表格属性|改变选中表格的属性',
  // TrvActionTableGrid
  '显示表格线(&G)', '显示表格线|显示或隐藏表格线',
  // TRVActionTableSplit
  '拆分表格(&L)', '拆分表格|从选定行起将表格拆分为两个',
  // TRVActionTableToText
  '转换为文本(&X)...', '转换为文本|将表格转换为文本',
  // TRVActionTableSort
  '排序(&S)...', '排序|表格行排序',
  // TrvActionTableCellLeftBorder
  '左边框(&L)', '左边框|显示或隐藏单元格左边框线',
  // TrvActionTableCellRightBorder
  '右边框(&R)', '右边框|显示或隐藏单元格右边框线',
  // TrvActionTableCellTopBorder
  '上边框(&T)', '上边框|显示或隐藏单元格上边框线',
  // TrvActionTableCellBottomBorder
  '下边框(&B)', '下边框|显示或隐藏单元格下边框线',
  // TrvActionTableCellAllBorders
  '所有边框(&A)', '所有边框|显示或隐藏单元格所有边框线',
  // TrvActionTableCellNoBorders
  '无边框(&N)', '无边框|隐藏所有单元格边框线',
  // TrvActionFonts & TrvActionFontEx
  '字体(&F)...', '字体|改变选中文字的字体',
  // TrvActionFontBold
  '粗体(&B)', '粗体|改变选中字体的样式为粗体',
  // TrvActionFontItalic
  '斜体(&I)', '斜体|改变选中字体的样式为斜体',
  // TrvActionFontUnderline
  '下划线(&U)', '下划线|改变选中字体的样式为有下划线',
  // TrvActionFontStrikeout
  '删除线(&S)', '删除线|改变选中字体的样式为有删除线',
  // TrvActionFontGrow
  '增大字号(&G)', '增大字号|选中文字的字号增大10%',
  // TrvActionFontShrink
  '减小字号(&H)', '减小字号|选中文字的字号减小10%',
  // TrvActionFontGrowOnePoint
  '增高字体(&R)', '增高字体|增加选中文字的高度一个像素点',
  // TrvActionFontShrinkOnePoint
  '降低字体(&N)', '降低字体|降低选中文字的高度一个像素点',
  // TrvActionFontAllCaps
  '所有大写(&A)', '所有大写|改变选中文字中所有字母为大写',
  // TrvActionFontOverline
  '上划线(&O)', '上划线|在文字上方划线',
  // TrvActionFontColor
  '文字颜色(&C)...', '文字颜色|改变选中文字的颜色',
  // TrvActionFontBackColor
  '文字背景颜色(&K)...', '文字背景颜色|改变选中文字的背景颜色',
  // TrvActionAddictSpell3
  '拼写检查(&S)', '拼写检查|拼写检查',
  // TrvActionAddictThesaurus3
  '辞典(&T)', '辞典|查找选中词语的同义词',
  // TrvActionParaLTR
  '从左至右', '从左至右|设置段落文字方向为从左至右',
  // TrvActionParaRTL
  '从右至左', '从右至左|设置段落文字方向为从右至左',
  // TrvActionLTR
  '从左至右文字', '从左至右文字|设置文字方向为从左至右',
  // TrvActionRTL
  '从右至左文字', '从右至左文字|设置文字方向为从右至左',
  // TrvActionCharCase
  '字符大小写', '字符大小写|改变选中文字字母的大小写',
  // TrvActionShowSpecialCharacters
  '非打印字符(&N)', '非打印字符|显示或隐藏不打印的字符，例如段落符、制表符和空格',
  // TrvActionSubscript
  '下标(&B)', '下标|转换选择的文本成下标',
  // TrvActionSuperscript
  '上标(&P)', '上标|转换选择的文本成上标',    
  // TrvActionInsertFootnote
  '脚注(&F)', '脚注|插入脚注',
  // TrvActionInsertEndnote
  '尾注(&E)', '尾注|插入尾注',
  // TrvActionEditNote
  '编辑旁注(&D)', '编辑旁注|开始编辑脚注或尾注',
  '隐藏(&H)', '隐藏|隐藏或显示选定碎片',  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '样式(&S)...', '样式|开启样式管理对话框',
  // TrvActionAddStyleTemplate
  '增添样式(&A)...', '增添样式|创建新文本或段落样式',
  // TrvActionClearFormat,
  '清除格式(&C)', '清除格式|从所选段落清除所有文本和段落格式',
  // TrvActionClearTextFormat,
  '清除文本格式(&T)', '清除文本格式|从所选文本清除所有格式',
  // TrvActionStyleInspector
  '样式检查器(&I)', '样式检查器|显示或隐藏样式检查器',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   '确定', '取消', '关闭', '插入', '打开(&O)...', '保存(&S)...', '清除(&C)', '帮助', '删除',
  // Others  -------------------------------------------------------------------
  // percents
  '百分比',
  // left, top, right, bottom sides
  '左边', '顶边', '右边', '底边',
  // save changes? confirm title
  '是否保存对%s的修改？', '确认',
  // warning: losing formatting
  '%s 可能包含与选择的格式不兼容的内容，'#13+
  '是否确定以这种格式保存？',
  // RVF format name
  'RichView 格式',
  // Error messages ------------------------------------------------------------
  '错误',
  '打开文件错误。'#13#13'可能原因：'#13'- 文件格式不支持；'#13+
  '- 文件已经损坏；'#13'- 文件被其他程序打开并锁定。',
  '打开图片文件错误。'#13#13'可能原因：'#13'- 图片文件格式不支持；'#13+
  '- 文件内没有图片；'#13+
  '- 文件已经损坏；'#13'- 。文件被其他程序打开并锁定。',
  '保存文件错误。'#13#13'可能原因：'#13'- 磁盘空间不足；'#13+
  '- 磁盘被写保护；'#13'- 可移动磁盘没有插入；'#13+
  '- 文件被其他程序打开并锁定；'#13'- 磁盘已经损坏。',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView 文件 (*.rvf)|*.rvf',  'RTF 文件 (*.rtf)|*.rtf' , 'XML 文件 (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  '文本文件 (*.txt)|*.txt', '文本文件 - 双字节 (*.txt)|*.txt', '文本文件 - 自动检测 (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML 文件 (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - 简化版 (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word 文档 (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  '搜索完成', '没有找到''%s''。',
  // 1 string replaced; Several strings replaced
  '替换了1次', '共%d处被替换',
  // continue search from the beginning/end?
  '已到文档底部，从头开始搜索？',
  '已到文档头部，从底部开始搜索？',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  '透明', '自动',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '黑色', '棕色', '橄榄绿', '深绿', '深青', '深蓝', '靛蓝', '灰色-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  '深红', '橙色', '暗黄', '绿色', '青色', '蓝色', '蓝灰', '灰色-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  '红色', '亮橙', '浅绿', '海绿', '水绿', '浅蓝', '紫罗兰', '灰色-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  '粉红', '金色', '黄色', '亮绿', '青绿', '天蓝', '李色', '灰色-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  '玫瑰', '棕褐', '浅黄', '浅绿', '浅青', '灰蓝', '淡紫', '白色',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '透明(&E)', '自动(&A)', '更多颜色(&M)...', '默认(&D)...',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  '背景', '颜色(&O):', '位置', '背景', '文字示例。',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '无(&N)', '拉伸(&R)', '固定平铺(&I)', '平铺(&T)', '居中(&E)',
  // Padding button
  '填充(&P)...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  '填充颜色', '应用到(&A)：', '更多颜色(&M)...', '填充(&P)...',
  '请选择颜色',
  // [apply to:] text, paragraph, table, cell
  '文本', '段落' , '表格', '单元格',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  '字体', '字体', '布置',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '字体(&F):', '大小(&S)', '格式', '粗体(&B)', '斜体(&I)',
  // Script, Color, Back color labels, Default charset
  '字符集(&R):', '颜色(&C):', '背景(&K)：', '(默认)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  '效果', '下划线(&U)', '上划线(&O)', '删除线(&T)', '全部大写(&A)',
  // Sample, Sample text
  '示例', '示例文字',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  '字符间距', '间距(&S):', '扩展(&E)', '收缩(&C)',
  // Offset group-box, Offset label, Down, Up,
  '偏移', '偏移(&O):', '向下(&D)', '向上(&U)',
  // Scaling group-box, Scaling
  '水平缩放', '比例(&A):',
  // Sub/super script group box, Normal, Sub, Super
  '下标与上标', '正常(&N)', '下标(&B)', '上标(&P)',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  '填充', '上(&T):', '左(&L):', '下(&B):', '右(&R):', '等值(&E)',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  '插入超链接', '超链接', '文字(&E):', '目标(&T):', '<<选择>>',
  // cannot open URL
  '无法打开页面 "%s"',
  // hyperlink properties button, hyperlink style
  '自定义(&C)...', '样式(&S)：',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) 駉lors
  '自定义属性', '通常颜色', '激活颜色',
  // Text [color], Background [color]
  '文字(&T):', '背景(&B)',
  // Underline [color]
  '下划线(&N)：', 
  // Text [color], Background [color] (different hotkeys)
  '文字(&E):', '背景(&A)',
  // Underline [color], Attributes group-box,
  '下划线(&N)：', '属性',
  // Underline type: always, never, active (below the mouse)
  '下划线(&N)：', '始终', '切勿', '活动',
  // Same as normal check-box
  '和通常一致',
  // underline active links check-box
  '下划线(&U)活动链接',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  '插入符号', '字体(&F):', '字符集(&C):', 'Unicode',
  // Unicode block
  '字块：(&B):',  
  // Character Code, Character Unicode Code, No Character
  '字符代码: %d', '字符代码: Unicode %d', '(没有字符)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  '插入表格', '表格大小', '列数(&C):', '行数(&R):',
  // Table layout group-box, Autosize, Fit, Manual size,
  '表格布局', '自动调整大小(&A)', '表格大小适应窗口大小(&W)', '手工设置表格大小(&M)',
  // Remember check-box
  '为新表格记住表格维数',
  // VAlign Form ---------------------------------------------------------------
  '位置',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  '属性', '图片', '位置和大小', '线条', '表格', '行', '单元格',
  // Image Appearance, Image Misc, Number tabs
  '外观', '其他', '编号',
  // Box position, Box size, Box appearance tabs
  '位置', '大小', '外观',
  // Preview label, Transparency group-box, checkbox, label
  '预览:', '透明度', '透明(&T)', '透明颜色(&C):',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '修改(&H)...', '保存(&S)...', '自动',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline top, bottom, middle
  '垂直对齐', '对齐(&A):',
  '文本居下', '文本居中',
  '行居上', '行居下', '行居中',
  // align to left side, align to right side
  '左侧', '右侧',    
  // Shift By label, Stretch group box, Width, Height, Default size
  '转换(&S)：', '拉伸', '宽度(&W):', '高度(&H):', '默认大小: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '按比例缩放(&P)', '重置(&R)',
  // Inside the border, border, outside the border groupboxes
  '边框内', '边框', '边框外',
  // Fill color, Padding (i.e. margins inside border) labels
  '填充颜色(&F)：', '填料：',
  // Border width, Border color labels
  '边框宽度(&W)：', '边框颜色(&C)：',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '水平间距(&H)：', '垂直间距(&V)：',
  // Miscellaneous groupbox, Tooltip label
  '其他', '工具提示(&T)：',
  // web group-box, alt text
  '互联网', '替换文字(&T):',
  // Horz line group-box, color, width, style
  '水平线', '颜色(&C):', '宽度(&W):', '样式 (&S)：',
  // Table group-box; Table Width, Color, CellSpacing,
  '表格', '宽度(&W):', '填充颜色(&F)：', '单元格边距(&S)...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '水平单元格边距(&H)：', '垂直单元格边距(&V)：', '边框和背景',
  // Visible table border sides button, auto width (in combobox), auto width label
  '可见边框边(&B)...', '自动', '自动',
  // Keep row content together checkbox
  '聚拢内容(&K)',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  '旋转', '无(&N)', '&90', '&180', '&270',
  // Cell border label, Visible cell border sides button,
  '边框：', '可见边框边(B&)...',
  // Border, CellBorders buttons
  '表格边框(&T)...', '单元格边框(&C)...',
  // Table border, Cell borders dialog titles
  '表格线', '默认单元格边框',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  '打印', '保持表格在一页上(&K)', '表头行数(&H):',
  '表头在每页表格的上部重复',
  // top, center, bottom, default
  '居上(&T)', '居中(&C)', '居下(&B)', '默认(&D)',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  '设置', '最佳宽度(&W):', '最小高度(&H):', '填充颜色(&F):',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '边框', '可见边:',  '阴影颜色(&O):', '光线颜色(&L)', '颜色(&O):',
  // Background image
  '图片(&I)...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  '水平位置', '垂直位置', '文本中的位置',
  // Position types: align, absolute, relative
  '对齐', '绝对位置', '相对位置',
  // [Align] left side, center, right side
  '文本框左侧边与下述左侧边',
  '文本框中心与下述中心',
  '文本框右侧边与下述右侧边',
  // [Align] top side, center, bottom side
  '文本框顶部与下述顶部',
  '文本框中心与下述中心',
  '文本框底部与下述底部',
  // [Align] relative to
  '关于',
  // [Position] to the right of the left side of
  '至  左侧右面',
  // [Position] below the top side of
  '  顶部之下',
  // Anchors: page, main text area (x2)
  '页面(&P)', '主文本区(&M)', '页面(&A)', '主文本区(&&X)',
  // Anchors: left margin, right margin
  '左白边(&L)', '右白边(&R)',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '头白边(&T)', '脚白边(&B)', '内白边(&I)', '外白边(&O)',
  // Anchors: character, line, paragraph
  '字符(&C)', '行(&I)', '段(&G)',
  // Mirrored margins checkbox, layout in table cell checkbox
  '镜像白边(&E)', '表格单元格中的布局(&Y)',
  // Above text, below text
  '位于文字上(&A)', '位于文字下(&B)',
  // Width, Height groupboxes, Width, Height labels
  '宽度', '高度', '宽度(&W)：', '高度(&H)：',
  // Auto height label, Auto height combobox item
  '自动', '自动',
  // Vertical alignment groupbox; alignments: top, center, bottom
  '垂直对齐', '靠上(&T)', '居中(&C)', '靠下(&B)',
  // Border and background button and title
  '边框和背景(&O)...', '文本框边框和背景',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  '粘贴特殊对象', '以...粘贴(&P):', '富文本格式(RTF)', 'HTML格式',
  '文本', 'Unicode文本', '点阵图', '矢量图',
  '图片文件',  'URL', 
  // Options group-box, styles
  '选项', '样式(&S)：',
  // style options: apply target styles, use source styles, ignore styles
  '套用目标文档的样式', '保持样式和外观',
  '保持外观，忽略样式',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  '项目符号和编号', '项目符号', '编号', '项目符号列表', '编号列表',
  // Customize, Reset, None
  '自定义(&C)...', '重置(&R)', '无',
  // Numbering: continue, reset to, create new
  '继续编号', '重置编号为', '创建新列表，从...开始',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  '大写英文字母', '大写罗马数字', '十进制数字', '小写英文字母', '小写罗马数字',
  // Bullet type
  '圆圈', '实心圆圈', '方形',
  // Level, Start from, Continue numbering, Numbering group-box
  '级别(&L)：', '开始于(&S)：', '继续(&C)', '编号',    
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  '自定义列表', '层次', '数量(&C):', '列表属性', '列表类型(&L):',
  // List types: bullet, image
  '符号', '图片', '插入编号|',
  // Number format, Number, Start level from, Font button
  '编号格式(&N):', '编号', '从...开始层次编号(&S):', '字体(&F)...',
  // Image button, bullet character, Bullet button,
  '图片(&I)...', '符号字符(&H)', '符号(&B)...',
  // Position of list text, bullet, number, image, text
  '列表文字位置', '项目符号位置', '编号位置', '图片位置', '文字位置',
  // at, left indent, first line indent, from left indent
  '位置:', '左端缩进(&E):', '首行缩进(&F):', '从左缩进',
  // [only] one level preview, preview
  '单层预览(&O)', '预览',
  // Preview text
  '文本文本文本文本文本文本文本文本文本文本文本.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  '居左对齐', '居右对齐', '居中对齐',
  // level #, this level (for combo-box)
  '第%d层', '本层',
  // Marker character dialog title
  '编辑项目符号字符',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  '段落边框和背景', '边框', '背景',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  '设置', '颜色(&C):', '宽度(&W):', '内部宽度(&E):', '偏移量(&F)...',
  // Sample, Border type group-boxes;
  '示例', '边框类型',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '无(&N)', '单线(&S)', '双线(&D)', '三线(&T)', '内宽外细(&I)', '内细外宽(&O)',
  // Fill color group-box; More colors, padding buttons
  '填充颜色', '更多颜色(&M)...', '填充(&P)...',
  // preview text
  '文本文本文本文本文本文本文本文本文本文本.',
  // title and group-box in Offsets dialog
  '偏移', '边框偏移量',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  '段落', '对齐方式', '居左(&L)', '居右(&R)', '居中(&C)', '两端(&J)',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  '边距', '前端(&B):', '后端(&A):', '行间距(&S):', '倍数(&Y):',
  // Indents group-box; indents: left, right, first line
  '缩进', '左(&E):', '右(G):', '第一行(&F):',
  // indented, hanging
  '缩进(&I)', '突出(&H)', '示例',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '单倍', '1.5倍', '双倍', '至少', '精确', '多倍',
  // preview text
  '文本文本文本文本。文本文本文本文本。文本文本文本文本文本。文本文本文本文本文本。',
  // tabs: Indents and spacing, Tabs, Text flow
  '缩进和边距', '制表符', '文本流',
  // tab stop position label; buttons: set, delete, delete all
  '制表符位置(&T):', '设置(&S)', '删除(&D)', '删除全部(&A)',
  // tab align group; left, right, center aligns,
  '对齐', '居左(&L)', '居右(&R)', '居中(&C)',
  // leader radio-group; no leader
  '前导符', '(无)',
  // tab stops to be deleted, delete none, delete all labels
  '待删除的制表符:', '(无)', '全部.',
  // Pagination group-box, keep with next, keep lines together
  '页码', '和下一个保持一致(&K)', '保持所有行一致(&L)',
  // Outline level, Body text, Level #
  '级别总览(&O)：', '正文文本', '第 %d 级',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  '打印预览', '适合宽度', '整页', '页数:',
  // Invalid Scale, [page #] of #
  '请输入10至500之间的数字', '共 %d',
  // Hints on buttons: first, prior, next, last pages
  '第一页', '前一页', '下一页', '最后一页',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  '单元格空间', '边距', '单元格之间', '从表格边框到单元格',
  // vertical, horizontal (x2)
  '垂直：(&V):', '水平：(&H):', '垂直：(&E):', '水平：(&O):',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  '边框线', '设置', '颜色(&C):', '光线颜色(&L):', '阴影颜色(&H):',
  // Width; Border type group-box;
  '宽度(&W):', '边框类型',
  // Border types: none, sunken, raised, flat
  '无(&N)', '下沉(&S)', '抬起(&R)', '平面(&F)',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  '拆分', '拆分为',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '指定行和列的数目(&S)', '原始单元格(&O)',
  // number of columns, rows, merge before
  '列数量(&C):', '行数量(&R):', '拆分前合并(&M)',
  // to original cols, rows check-boxes
  '拆分为原始列(&L)', '拆分为原始行(&W)',
  // Add Rows form -------------------------------------------------------------
  '增加行', '行数量(&C):',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  '页面设置', '页面', '页眉和页脚',
  // margins group-box, left, right, top, bottom
  '页边距(毫米)', '页边距(英寸)', '左(&L):', '上(&T):', '右(&R):', '下(&B):',
  // mirror margins check-box
  '镜像边距(&M)',
  // orientation group-box, portrait, landscape
  '打印方向', '纵向(&P)', '横向(&A)',
  // paper group-box, paper size, default paper source
  '纸张', '大小(&Z):', '来源(&S):',
  // header group-box, print on the first page, font button
  '页眉', '文本(&T):', '第一页的页眉(&H)', '字体(&F)...',
  // header alignments: left, center, right 
  '左对齐(&L)', '居中对齐(&C)', '右对齐(&R)',
  // the same for footer (different hotkeys)
  '页脚', '文本(&X):', '第一页的页脚(&P)', '字体(&O)...',
  '左对齐(&E)', '居中对齐(&N)', '右对齐(&I)',
  // page numbers group-box, start from
  '页数', '从第几页开始(&S):',
  // hint about codes
  '特殊字符组合:'#13'&&p - 页号； &&P - 总页数； &&d - 当前日期； &&t - 当前时间。',
  // Code Page form ------------------------------------------------------------
  // title, label
  '文本文件代码页', '选择文件编码(&C)：',
  // thai, japanese, chinese (simpl), korean
  '泰语', '日语', '简体中文', '韩语',
  // chinese (trad), central european, cyrillic, west european
  '繁体中文', '中欧和东欧语', '西里尔文', '西欧语',
  // greek, turkish, hebrew, arabic
  '希腊语', '土耳其语', '希伯来语', '阿拉伯语',
  // baltic, vietnamese, utf-8, utf-16
  '波罗的语', '越南语', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  '视觉样式', '选择样式(&S)：',
  // Delimiter form ------------------------------------------------------------
  // title, label
  '转换为文本', '选择分隔符(&D)：',
  // line break, tab, ';', ','
  '换行符', '制表符', '分号', '逗号',
  // Table sort form -----------------------------------------------------------
  // error message
  '包含合并行的表格不能排序',
  // title, main options groupbox
  '表格排序', '排序',
  // sort by column, case sensitive
  '按列排序(&S)：', '区分大小写(&C)',
  // heading row, range or rows
  '标题行除外(&H)', '要排序的行：从 %d 到 %d',
  // order, ascending, descending
  '顺序', '升序(&A)', '降序(&D)',
  // data type, text, number
  '类型', '文本(&T)', '数字(&N)',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  '插入编号', '属性', '计数器名称(&C)：', '编号类型(&N)：',
  // numbering groupbox, continue, start from
  '编号', '继续(&O)', '始于(&S)：',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  '说明', '标签(&L)：', '从说明中去除标签(&E)',
  // position radiogroup
  '位置', '选定对象之上(&A)', '选定对象之下(&B)',
  // caption text
  '说明文字(&T)：',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  '编号', '图', '表',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  '可见边框边', '边框',
  // Left, Top, Right, Bottom checkboxes
  '左侧(&L)', '顶部(&T)', '右侧(&R)', '底部(&B)',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  '改变表格列宽', '改变表格行高',
  // Hints on indents: first line, left (together with first), left (without first), right
  '第一行缩进', '左边缩进', '突出缩进', '右边缩进',
  // Hints on lists: up one level (left), down one level (right)
  '切换到上一层', '切换到下一层',
  // Hints for margins: bottom, left, right and top
  '下边界', '左边界', '右边界', '上边界',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  '左边对齐的制表符', '右边对齐的制表符', '中间对齐的制表符', '小数点对齐的制表符',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  '正文', '正文缩进', '无间距', '标题%d', '列表段落',
  // Hyperlink, Title, Subtitle
  '超链接', '标题', '副标题',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '强调', '精巧型强调', '明显强调', '增强',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  '引用', '明显引用', '精巧型引用', '明显引用',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  '文本块', 'HTML变量', 'HTML代码', 'HTML缩写',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML定义', 'HTML键盘', 'HTML示例', 'HTML打字机',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML预设格式', 'HTML引文', '页眉', '页脚', '页号',
  // Caption
  '说明',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  '尾注引用', '脚注引用', '尾注文本', '脚注文本',
  // Sidenote Reference, Sidenote Text
  '旁注参考', '旁注文本',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  '颜色', '背景颜色', '透明', '默认', '下划线颜色',
  // default background color, default text color, [color] same as text
  '默认背景颜色', '默认文本颜色', '与文本相同s',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  '单线', '粗线', '双线', '点线', '粗点线', '虚线',
  // underline types: thick dashed, long dashed, thick long dashed,
  '粗虚线', '长虚线', '粗长虚线',
  // underline types: dash dotted, thick dash dotted,
  '划点线', '粗划点线',
  // underline types: dash dot dotted, thick dash dot dotted
  '划圆点线', '粗划圆点线',
  // sub/superscript: not, subsript, superscript
  '非下标/上标', '下标', '上标',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  '双向模式：', '继承的', '左至右', '右至左',
  // bold, not bold
  '粗体', '非粗体',
  // italic, not italic
  '倾斜', '非倾斜',
  // underlined, not underlined, default underline
  '下划线', '非下划线', '默认下划线',
  // struck out, not struck out
  '越线', '未越线',
  // overlined, not overlined
  '上划线', '非上划线',
  // all capitals: yes, all capitals: no
  '全部大写', '全部非大写',
  // vertical shift: none, by x% up, by x% down
  '无需垂直移位', '由%d%%向上移位', '由%d%%向下移位',
  // characters width [: x%]
  '字符宽度',
  // character spacing: none, expanded by x, condensed by x
  '一般字符间距', '由%s扩展的间距', '由%s紧缩的间距',
  // charset
  '脚本',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  '默认字体', '默认段落', '继承的',
  // [hyperlink] highlight, default hyperlink attributes
  '突出显示', '默认',
  // paragraph aligmnment: left, right, center, justify
  '左对齐', '右对齐', '中央对齐', '两端对齐',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  '行高：%d%%', '线之间的空格：%s', '行高：至少%s',
  '行高：实际为%s',
  // no wrap, wrap
  '停用换行', '换行',
  // keep lines together: yes, no
  '段中不分页', '请勿段中不分页',
  // keep with next: yes, no
  '和下一个段落一致', '请勿和下一个段落一致',
  // read only: yes, no
  '只读', '可编辑',
  // body text, heading level x
  '大纲级别：正文文本', '大纲级别：%d',
  // indents: first line, left, right
  '首行缩进', '左缩进', '右缩进',
  // space before, after
  '前间隔', '後间隔',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  '标签', '无', '对齐', '左', '右', '中央',
  // tab leader (filling character)
  '前导符',
  // no, yes
  '否', '是',
  // [padding/spacing/side:] left, top, right, bottom
  '左', '顶端', '右', '底端',
  // border: none, single, double, triple, thick inside, thick outside
  '无', '单线', '双线', '三线', '粗线内部', '粗线外部',
  // background, border, padding, spacing [between text and border]
  '背景', '边界', '边距', '间距',
  // border: style, width, internal width, visible sides
  '样式', '宽度', '内部宽度', '可视面',
  // style inspector -----------------------------------------------------------
  // title
  '样式检查器',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  '样式', '（无）', '段落', '字体', '属性',
  // border and background, hyperlink, standard [style]
  '边界和背景', '超链接', '（标准）',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  '样式', '样式',
  // name, applicable to,
  '名称(&N)：', '适用於(&T)：',
  // based on, based on (no &),
  '基於(&B)：', '基於：',
  //  next style, next style (no &)
  '下列段落的样式(&F)：', '下列段落的样式：',
  // quick access check-box, description and preview
  '快速访问(&Q)', '说明和预览(&P)：',
  // [applicable to:] paragraph and text, paragraph, text
  '段落和文本', '段落', '文本',
  // text and paragraph styles, default font
  '文本和段落样式', '默认字体',
  // links: edit, reset, buttons: add, delete
  '编辑', '重置', '增添(&A)', '删除(&D)',
  // add standard style, add custom style, default style name
  '增添标准样式(&S)...', '增添自定义样式(&C)', '样式%d',
  // choose style
  '选择样式(&S)：',
  // import, export,
  '导入(&I)...', '导出(&E)...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView样式(*.rvst)|*.rvst', '错误载入文件r', '本文件未包含任何样式',
  // Title, group-box, import styles
  '从文件导入样式', '导入', '导入样式(&M)：',
  // existing styles radio-group: Title, override, auto-rename
  '现有的样式', '替代(&O)', '增添重命名(&A)',
  // Select, Unselect, Invert,
  '选择(&S)', '取消选择(&U)', '转换(&I)',
  // select/unselect: all styles, new styles, existing styles
  '所有样式(&A)', '新建样式(&N)', '现有的样式(&E)',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '（清除格式）', '（所有样式）',
  // dialog title, prompt
  '样式', '选择要应用的样式(&C)：',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '同义词(&S)', '忽略全部(&I)', '加入到字典(&A)',
  // Progress messages ---------------------------------------------------------
  '正在下载%s', '正在准备打印...', '正在打印页面%d',
  '正在从RTF转换...',  '正在转换至RTF...',
  '正在读取%s...', '正在写入%s...', '文本',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  '无旁注', '脚注', '尾注', '旁注', '文本框'
  );

initialization
  RVA_RegisterLanguage(
    'Chinese (Simplified)', // english language name, do not translate
    '简体中文', // native language name
    GB2312_CHARSET, @Messages);

end.

