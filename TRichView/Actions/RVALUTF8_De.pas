﻿// This file is a copy of RVAL_De.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       German (Deutsch) translation                    }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Eike Hoffmann 2003-02-04               }
{ Updated: 2009-05-19 by Harry Stahl                    }
{          2010-10-14 by Michael Damm                   }
{          2012-08-28 by Ruediger Kabbasch              }
{          2014-01-30 by Ruediger Kabbasch              } 
{*******************************************************}

unit RVALUTF8_De;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'Inch', 'cm', 'mm', 'Pica', 'Pixel', 'Punkt',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Datei', '&Bearbeiten', 'Forma&t', '&Zeichen', '&Absatz', '&Einfügen', '&Tabelle', '&Fenster', '&Hilfe',
  // exit
  '&Beenden',
  // top-level menus: View, Tools,
  '&Ansicht', '&Werkzeuge',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Schriftgröße', 'Schriftstil', 'Markie&ren', 'Zelli&nhalte ausrichten', 'Z&ellrahmen',
  // menus: Table cell rotation
  'Zellend&rehung',
  // menus: Text flow, Footnotes/endnotes
  '&Textfluss', '&Fußnoten',
  // ribbon tabs: tab1, tab2, view, table
  '&Start', '&Erweitert', '&Ansicht', '&Tabelle',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Zwischenablage', 'Schrift', 'Absatz', 'Liste', 'Bearbeiten',
  // ribbon groups: insert, background, page setup,
  'Einfügen', 'Hintergrund', 'Seiteneinstellungen',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Links', 'Fußnoten', 'Dokumentansichten', 'Anzeigen/Verbergen', 'Zoom', 'Optionen',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Einfügen', 'Löschen', 'Aktionen', 'Rahmen',
  // ribbon groups: styles 
  'Formatvorlagen',
  // ribbon screen tip footer,
  'Für weitere Hilfe F1 drücken',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Zuletzt verwendet', 'Kopie des Dokuments sichern', 'Vorschau und Ausdruck des Dokuments',
  // ribbon label: units combo
  'Einheiten:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Neu', 'Neu|Erstellt ein leeres neues Dokument',
  // TrvActionOpen
  '&Öffnen...', 'Öffnen|Öffnet ein Dokument von der Festplatte',
  // TrvActionSave
  '&Speichern', 'Speichern|Speichert das Dokument auf Festplatte',
  // TrvActionSaveAs
  'Speichern &Unter...', 'Speichern Unter...|Speichert das Dokument mit einem neuen Namen auf Festplatte',
  // TrvActionExport
  '&Exportieren...', 'Exportieren|Exportiert das Dokument in einem anderen Format',
  // TrvActionPrintPreview
  'Druck&vorschau', 'Druckvorschau|Zeigt wie das Dokument gedruckt wird',
  // TrvActionPrint
  '&Drucken...', 'Drucken|Setzt die Druckeinstellungen und druckt das Dokument',
  // TrvActionQuickPrint
  '&Drucken', '&Sofortdruck', 'Drucken|Druckt das Dokument',
   // TrvActionPageSetup
  'S&eite einrichten..', 'Seite einrichten|Seitenränder, Papierformat, Seitenorientierung, Kopfzeilen und Fusszeilen einrichten',
  // TrvActionCut
  'Ausschnei&den', 'Ausschneiden|Schneidet den markierten Bereich aus und speichert diesen in der Zwischenablage',
  // TrvActionCopy
  '&Kopieren', 'Kopieren|Kopiert den markierten Bereich und speichert diesen in der Zwischenablage',
  // TrvActionPaste
  'E&infügen', 'Einfügen|Fügt den Inhalt der Zwischenablage ein',
  // TrvActionPasteAsText
  'Einfügen als &Text', 'Einfügen als Text|Fügt den Inhalt der Zwischenablage als Text ein',
  // TrvActionPasteSpecial
  'Einfügen al&s...', 'Einfügen als|Fügt den Inhalt der Zwischenablage im angegebenen Format ein',
  // TrvActionSelectAll
  '&Alles markieren', 'Alles markieren|Wählt das gesamte Dokument aus',
  // TrvActionUndo
  '&Rückgängig', 'Rückgängig|Macht die letzte Aktion rückgängig',
  // TrvActionRedo
  '&Wiederherstellen', 'Wiederherstellen|Wiederherstellen der letzten rückgängig gemachten Aktion',
  // TrvActionFind
  '&Suchen...', 'Suchen|Sucht nach dem angegebenen Text im Dokument',
  // TrvActionFindNext
  '&Weitersuchen', 'Weitersuchen|Fortsetzen der letzten Suchanfrage',
  // TrvActionReplace
  '&Ersetzen...', 'Ersetzen|Suchen und ersetzen des angegebenen Textes im Dokument',
  // TrvActionInsertFile
  '&Datei...', 'Datei einfügen|Fügt den Inhalt einer Datei in das Dokument ein',
  // TrvActionInsertPicture
  '&Grafik...', 'Grafik einfügen|Fügt eine Grafik von der Festplatte ein',
  // TRVActionInsertHLine
  '&Horizontale Linie', 'Horizontale Linie einfügen|Fügt eine horizontale Linie in das Dokument ein',
  // TRVActionInsertHyperlink
  'Hyper&link...', 'Hyperlink einfügen|Fügt einen Hyperlink in das Dokument ein',
  // TRVActionRemoveHyperlinks
  'Hyperlinks ent&fernen', 'Hyperlinks entfernen|Entfernt alle Hyperlinks aus dem markierten Text',
  // TrvActionInsertSymbol
  '&Symbol...', 'Symbol einfügen|Fügt ein Symbol in das Dokument ein',
  // TrvActionInsertNumber
  '&Zahl...', 'Zahl einfügen|Eine Zahl einfügen',
  // TrvActionInsertCaption
  'Bes&chriftung einfügen...', 'Beschriftung einfügen|Eine Beschriftung für das ausgewählte Objekt einfügen',
  // TrvActionInsertTextBox
  '&Textfeld', 'Textfeld einfügen|Fügt ein Textfeld ein',
  // TrvActionInsertSidenote
  '&Randnotiz', 'Randnotiz einfügen|Fügt eine Randnotiz in einem Textfeld ein',
  // TrvActionInsertPageNumber
  '&Seitenzahl', 'Seitenzahl einfügen|Fügt eine Seitenzahl ein',
  // TrvActionParaList
  '&Nummerierung und Aufzählungszeichen...', 'Nummerierung und Aufzählungszeichen|Hinzufügen oder bearbeiten der Nummerierung bzw. des Aufzählungszeichens für die markierten Absätze',
  // TrvActionParaBullets
  '&Aufzählungszeichen', 'Aufzählungszeichen|Hinzufügen oder löschen eines Aufzählungszeichens für diesen Absatz',
  // TrvActionParaNumbering
  '&Nummerierung', 'Nummerierung|Hinzufügen oder löschen einer Nummerierung für diesen Absatz',
  // TrvActionColor
  'Hintergrundf&arbe...', 'Hintergrundfarbe|Ändern der Hintergrundfarbe des Dokuments',
  // TrvActionFillColor
  '&Füllfarbe...', 'Füllfarbe|Ändern der Hintergrundfarbe für den Text, den Absatz, die Tabellenzelle, die Tabelle oder das Dokument',
  // TrvActionInsertPageBreak
  '&Seitenumbruch einfügen', 'Seitenumbruch einfügen|Fügt einen Seitenumbruch ein',
  // TrvActionRemovePageBreak
  'Seitenumbruch &löschen', 'Seitenumbruch löschen|Löscht den Seitenumbruch',
  // TrvActionClearLeft
  'Textfluss auf &linker Seite', 'Textfluss auf linker Seite|Plaziert den Absatz unter jedes links ausgerichtete Bild',
  // TrvActionClearRight
  'Textfluss auf &rechter Seite', 'Textfluss auf rechter Seite|Plaziert den Absatz unter jedes rechts ausgerichtete Bild',
  // TrvActionClearBoth
  'Textfluss auf &beiden Seiten', 'Textfluss auf beiden Seiten|Plaziert den Absatz unter jedes links oder rechts ausgerichtete Bild',
  // TrvActionClearNone
  '&Normaler Textfluss', 'Normaler Textfluss|Erlaubt den Textfluss sowohl um links als auch rechts ausgerichtete Bilder',
  // TrvActionVAlign
  '&Objekt Position...', 'Objekt Position|Ändert die Position des selektierten Objekts',
  // TrvActionItemProperties
  'Objekt &Einstellungen...', 'Objekt Einstellungen|Einstellungen für das aktive Objekt festlegen',
  // TrvActionBackground
  '&Hintergrund...', 'Hintergrund|Ändern von Hintergrundfarbe und Hintergrundbild',
  // TrvActionParagraph
  '&Absatz...', 'Absatz|Ändern der Absatzattribute',
  // TrvActionIndentInc
  '&Einzug vergrößern', 'Einzug vergrößern|Linken Einzug des markierten Absatzes vergrößern',
  // TrvActionIndentDec
  'Einzug &verkleinern', 'Einzug verkleinern|Linken Einzug des markierten Absatzes verkleinern',
  // TrvActionWordWrap
  '&Zeilenumbruch', 'Zeilenumbruch|Zeilenumbruch für den markierten Absatz ein- oder ausschalten',
  // TrvActionAlignLeft
  '&Linksbündig', 'Linksbündig|Richtet den markierten Text linksbündig aus',
  // TrvActionAlignRight
  '&Rechtsbündig', 'Rechtsbündig|Richtet den markierten Text rechtsbündig aus',
  // TrvActionAlignCenter
  '&Zentriert', 'Zentriert|Richtet den markierten Text zentriert aus',
  // TrvActionAlignJustify
  '&Blocksatz', 'Blocksatz|Richtet den markierten Text im Blocksatz aus',
  // TrvActionParaColor
  'Absatz &Hintergrundfarbe...', 'Absatz Hintergrundfarbe|Ändern der Hintergrundfarbe des Absatzes',
  // TrvActionLineSpacing100
  '&Einfacher Zeilenabstand', 'Einfacher Zeilenabstand|Den Zeilenabstand auf eine Zeile setzen ',
  // TrvActionLineSpacing150
  '1,5-facher Zeile&nabstand', '1,5-facher Zeilenabstand|Den Zeilenabstand auf 1,5 Zeilen setzen',
  // TrvActionLineSpacing200
  '&Doppelter Zeilenabstand', 'Doppelter Zeilenabstand|Den Zeilenabstand auf 2 Zeilen setzen',
  // TrvActionParaBorder
  'A&bsatz Rahmen und Hintergrundfarbe...', 'Absatz Rahmen und Hintergrundfarbe|Ändern des Rahmens und der Hintergrundfarbe für diesen Absatz',
  // TrvActionInsertTable
  'Tabelle &einfügen...', '&Tabelle', 'Tabelle einfügen|Fügt eine neue Tabelle ein',
  // TrvActionTableInsertRowsAbove
  'Zeile &oberhalb einfügen', 'Zeile oberhalb einfügen|Fügt eine neue Zeile oberhalb der markierten Zellen ein',
  // TrvActionTableInsertRowsBelow
  'Zeile &unterhalb einfügen', 'Zeile unterhalb einfügen|Fügt eine neue Zeile unterhalb der markierten Zellen ein',
  // TrvActionTableInsertColLeft
  'Spalte &links einfügen', 'Spalte links einfügen|Fügt eine neue Spalte links von den markierten Zellen ein',
  // TrvActionTableInsertColRight
  'Spalte &rechts einfügen', 'Spalte rechts einfügen|Fügt eine neue Spalte rechts von den markierten Zellen ein',
  // TrvActionTableDeleteRows
  '&Zeilen löschen', 'Zeilen löschen|Löscht die Zeilen mit den markierten Zellen',
  // TrvActionTableDeleteCols
  '&Spalten löschen', 'Spalten löschen|Löscht die Spalten mit den markierten Zellen',
  // TrvActionTableDeleteTable
  '&Tabelle löschen', 'Tabelle löschen|Löscht die Tabelle',
  // TrvActionTableMergeCells
  'Zellen &verbinden', 'Zellen verbinden|Verbindet die markierten Zellen',
  // TrvActionTableSplitCells
  'Zellen &teilen...', 'Zellen teilen|Teilt die markierten Zellen',
  // TrvActionTableSelectTable
  'Tabelle &markieren', 'Tabelle markieren|Markiert die gesamte Tabelle',
  // TrvActionTableSelectRows
  'Zeilen m&arkieren', 'Zeilen markieren|Markiert die Zeilen',
  // TrvActionTableSelectCols
  'S&palten markieren', 'Spalten markieren|Markiert die Spalten',
  // TrvActionTableSelectCell
  'Z&elle markieren', 'Zelle markieren|Markiert die Zelle',
  // TrvActionTableCellVAlignTop
  'Zellinhalt &oben ausrichten', 'Zellinhalt oben ausrichten|Richtet den Inhalt der Zelle am oberen Rand aus',
  // TrvActionTableCellVAlignMiddle
  'Zellinhalt &mittig ausrichten', 'Zellinhalt mittig ausrichten|Richtet den Inhalt der Zelle mittig aus',
  // TrvActionTableCellVAlignBottom
  'Zellinhalt &unten ausrichten', 'Zellinhalt unten ausrichten|Richtet den Inhalt der Zelle am unteren Rand aus',
  // TrvActionTableCellVAlignDefault
  '&Standard für vertikale Zellausrichtung', 'Standard für vertikale Zellausrichtung|Festlegend er Standardeinstellung für die vertikale Zellausrichtung',
  // TrvActionTableCellRotationNone
  '&Keine Zellendrehung', 'Keine Zellendrehung|Inhalt der Zelle um 0° drehen',
  // TrvActionTableCellRotation90
  'Zelle um &90° drehen', 'Zelle um 90° drehen|Inhalt der Zelle um 90° drehen',
  // TrvActionTableCellRotation180
  'Zelle um &180° drehen', 'Zelle um 180° drehen|Inhalt der Zelle um 180° drehen',
  // TrvActionTableCellRotation270
  'Zelle um &270° drehen', 'Zelle um 270° drehen|Inhalt der Zelle um 270° drehen',
  // TrvActionTableProperties
  'Tabellen&eigenschaften...', 'Tabelleneigenschaften|Ändern der Eigenschaften für die markierte Tabelle',
  // TrvActionTableGrid
  '&Gitternetzlinien anzeigen', 'Gitternetzlinien anzeigen|Ein- oder ausschalten der Gitternetzlinien für Tabellen',
  // TRVActionTableSplit
  'Tabelle tei&len', 'Tabelle teilen|Teilt die Tabelle beginnend mit der ausgewählten Zeile in zwei Tabellen',
  // TRVActionTableToText
  'In Te&xt umwandeln...', 'In Text umwandeln|Tabelle in Text umwandeln',
  // TRVActionTableSort
  '&Sortieren...', 'Sortieren|Sortiert die Zeilen der Tabelle',
  // TrvActionTableCellLeftBorder
  '&Linker Rahmen', 'Linker Rahmen|Linken Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellRightBorder
  '&Rechter Rahmen', 'Rechter Rahmen|Rechten Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellTopBorder
  '&Oberer Rahmen', 'Oberer Rahmen|Oberen Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellBottomBorder
  '&Unterer Rahmen', 'Unterer Rahmen|Unteren Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellAllBorders
  '&Zellrahmen', 'Zellrahmen|Gesamten Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellNoBorders
  '&Keine Rahmen', 'Keine Rahmen|Alle Zellrahmen ausschalten',
  // TrvActionFonts & TrvActionFontEx
  '&Schriftart...', 'Schriftart|Ändern der Schriftart für den markierten Text',
  // TrvActionFontBold
  '&Fett', 'Fett|Setzen oder löschen des Attributs Fett für den markierten Text',
  // TrvActionFontItalic
  '&Kursiv', 'Kursiv|Setzen oder löschen des Attributs Kursiv für den markierten Text',
  // TrvActionFontUnderline
  '&Unterstrichen', 'Unterstrichen|Setzen oder löschen des Attributs Unterstrichen für den markierten Text',
  // TrvActionFontStrikeout
  '&Durchgestrichen', 'Durchgestrichen|Setzen oder löschen des Attributs Durchgestrichen für den markierten Text',
  // TrvActionFontGrow
  'Schriftart ver&größern', 'Schriftart vergrößern|Höhe des markierten Texts um 10% vergrößern',
  // TrvActionFontShrink
  'Sc&hriftart verkleinern', 'Schriftart verkleinern|Höhe des markierten Texts um 10% verkleinern',
  // TrvActionFontGrowOnePoint
  'Schriftart um einen Punkt verg&rößern', 'Schriftart um einen Punkt vergrößern|Vergrößert die Höhe des markierten Texts um einen Punkt',
  // TrvActionFontShrinkOnePoint
  'Schriftart um einen Punkt verkleinern', 'Schriftart um einen Punkt verkleinern|Verkleinert die Höhe des markierten Texts um einen Punkt',
  // TrvActionFontAllCaps
  '&Alles Großbuchstaben', 'Alles Großbuchstaben|Alle Buchstaben des markierten Texts in Großbuchstaben umwandeln',
  // TrvActionFontOverline
  '&Linie über Text', 'Linie über Text|Fügt eine Linie über dem markierten Text ein',
  // TrvActionFontColor
  'Text &Farbe...', 'Text Farbe|Ändern der Farbe für den markierten Text',
  // TrvActionFontBackColor
  'Text Hinter&grund Farbe...', 'Text Hintergrund Farbe|Ändert die Hintergrundfarbe des markierten Texts',
  // TrvActionAddictSpell3
  'Recht&schreibprüfung', 'Rechtschreibprüfung|Prüft die Rechtschreibung',
  // TrvActionAddictThesaurus3
  '&Thesaurus', 'Thesaurus|Zeigt Synonyme für das markierte Wort an',
  // TrvActionParaLTR
  'Links nach Rechts', 'Links nach Rechts|Setzt Schreibrichtung Links-nach-Rechts für markierten Text',
  // TrvActionParaRTL
  'Rechts nach Links', 'Rechts nach Links|Setzt Schreibrichtung Rechts-nach-Links für markierten Text',
  // TrvActionLTR
  'Text Links nach Rechts', 'Text Links nach Rechts|Setzt Schreibrichtung Links-nach-Rechts für markierten Text',
  // TrvActionRTL
  'Text Rechts nach Links', 'Text Rechts nach Links|Setzt Schreibrichtung Rechts-nach-Links für markierten Text',
  // TrvActionCharCase
  'Groß-/Kleinschreibung', 'Groß-/Kleinschreibung|Ändert Groß-/Kleinschreibung für markierten Text',
  // TrvActionShowSpecialCharacters
  '&Nicht-druckbare Zeichen', 'Nicht-druckbare Zeichen|Ein- oder ausblenden von nicht-druckbaren Zeichen, wie z.B. Absatzmarken, Tabulatoren und Leerzeichen.',
  // TrvActionSubscript
  '&Tiefgestellt', 'Tiefgestellt|Der markierte Text wird tiefgestellt',
  // TrvActionSuperscript
  '&Hochgestellt', 'Hochgestellt|Der markierter Text wird hochgestellt',
  // TrvActionInsertFootnote
  '&Fußnote', 'Fußnote|Eine Fußnote einfügen',
  // TrvActionInsertEndnote
  '&Endnote', 'Endnote|Eine Endnote einfügen',
  // TrvActionEditNote
  'Note bearbeiten', 'Note bearbeiten|Mit der Bearbeitung von Fuß- und Endnoten beginnen',
  '&Ausblenden', 'Ausblenden|Ausblenden oder anzeigen des ausgewählten Fragments',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Formatvorlagen...', 'Formatvorlagen|Das Fenster für Formatvorlagen anzeigen',
  // TrvActionAddStyleTemplate
  'Form&atvorlage hinzufügen...', 'Formatvorlage hinzufügen|Eine neue Text- oder Absatzformatvorlage erstellen',
  // TrvActionClearFormat,
  'Formatierung lös&chen', 'Formatierung löschen|Die Text- und Absatzformatierung des ausgewählten Textes löschen',
  // TrvActionClearTextFormat,
  '&Textformat löschen', 'Textformat löschen|Die Textformatierung des ausgewählten Textes löschen',
  // TrvActionStyleInspector
  'Format&inspektor', 'Formatinspektor|Formatinspektor anzeigen/verbergen',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Ok', 'Abbrechen', 'Schließen', 'Einfügen', '&Öffnen...', '&Speichern...', '&Löschen', 'Hilfe', 'Entfernen',
  // Others  -------------------------------------------------------------------
  // percents
  'Prozent',
  // left, top, right, bottom sides
  'Linke Seite', 'Obere Seite', 'Rechte Seite', 'Untere Seite',
  // save changes? confirm title
  'Änderungen in %s speichern?', 'Bitte bestätigen',
  // warning: losing formatting
  '%s kann Funktionalitäten enthalten die mit dem gewählten Speicherformat nicht kompatibel sind.'#13+
  'Möchten Sie das Dokument wirklich in diesem Format speichern?',
  // RVF format name
  'RichView Format',
  // Error messages ------------------------------------------------------------
  'Fehler',
  'Fehler beim Laden der Datei.'#13#13'Mögliche Ursachen:'#13'- das Format dieser Datei wird nicht unterstützt;'#13+
  '- die Datei ist beschädigt;'#13'- die Datei ist gesperrt, weil sie bereits von einer anderen Anwendung verwendet wird.',
  'Fehler beim Laden der Grafikdatei.'#13#13'Mögliche Ursachen:'#13'- die Datei liegt in einem Grafikformat vor, dass nicht unterstützt wird;'#13+
  '- die Datei enthält keine Grafik;'#13+
  '- die Datei ist beschädigt;'#13'- die Datei ist gesperrt, weil sie bereits von einer anderen Anwendung verwendet wird.',
  'Fehler beim Speichern der Datei.'#13#13'Mögliche Ursachen:'#13'- kein freier Speicherplatz;'#13+
  '- das Speichermedium ist schreibgeschützt;'#13'- austauschbarer Datenträger ist nicht eingelegt;'#13+
  '- die Datei ist gesperrt, weil sie bereits von einer anderen Anwendung verwendet wird;'#13'- das Speichermedium ist beschädigt.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Dateien (*.rvf)|*.rvf',  'RichText Dateien (*.rtf)|*.rtf' , 'XML Dateien (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Text Dateien (*.txt)|*.txt', 'Text Dateien - Unicode (*.txt)|*.txt', 'Text Dateien - Automatische Auswahl (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Dateien (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Einfach (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dokumente (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Suche beendet', 'Suchtext ''%s'' nicht gefunden.',
  // 1 string replaced; Several strings replaced
  '1 Eintrag ersetzt', '%d Einträge ersetzt.',
  // continue search from the beginning/end?
  'Ende des Dokuments erreicht. Am Beginn des Dokuments fortsetzen?',
  'Anfang des Dokuments erreicht. Am Ende des Dokuments fortsetzen?',  
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Schwarz', 'Braun', 'Olivgrün', 'Dunkelgrün', 'Grünblau', 'Dunkelblau', 'Indigo', 'Grau-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Dunkelrot', 'Orange', 'Dunkelgelb', 'Grün', 'Blaugrün', 'Blau', 'Blaugrau', 'Grau-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rot', 'Hellorange', 'Limone', 'Meeresgrün', 'Aqua', 'Hellblau', 'Violett', 'Grau-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Gold', 'Gelb', 'Strahlend Grün', 'Türkis', 'Himmelblau', 'Pflaume', 'Grau-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Bräunlich', 'Hellgelb', 'Hellgrün', 'Helltürkis', 'Blassblau', 'Lila', 'Weiss',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparent', '&Auto', '&Mehr Farben...', '&Standard',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Hintergrund', 'F&arbe:', 'Position', 'Hintergrund', 'Beispieltext.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ohne', '&Gesamter Bereich', 'F&este Kacheln', 'Gek&achelt', '&Zentriert',
  // Padding button
  '&Abstand...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Füllfarbe', '&Anwenden auf:', '&Mehr Farben...', '&Abstand...',
  'Bitte eine Farbe auswählen.',
  // [apply to:] text, paragraph, table, cell
  'Text', 'Absatz' , 'Tabelle', 'Zelle',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Schriftart', 'Schriftart', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Schriftart:', 'Schrift&grad', 'Schriftsch&nitt', '&Fett', '&Kursiv',
  // Script, Color, Back color labels, Default charset
  'Sk&ript:', '&Farbe:', 'Hinter&grund:', '(Standard Zeichensatz)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekte', 'Unterstrichen', '&Linie über Text', '&Durchgestrichen', '&Alles Großbuchstaben',
  // Sample, Sample text
  'Beispiel', 'Beispieltext',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Zeichenabstand', '&Abstand:', '&Erweitert', '&Schmal',
  // Offset group-box, Offset label, Down, Up,
  'Position', '&Position:', '&Tiefgestellt', '&Hochgestellt',
  // Scaling group-box, Scaling
  'Skalierung', 'Sk&alierung:',
  // Sub/super script group box, Normal, Sub, Super
  'Hoch- und Tiefgestellt', '&Normal', '&Tiefgestellt', '&Hochgestellt',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Füllung', '&Oben:', '&Links:', '&Unten:', '&Rechts:', '&Gleiche Werte',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Hyperlink einfügen', 'Hyperlink', 'T&ext:', '&Ziel:', '<<Auswahl>>',
  // cannot open URL
  'Kann Adresse "%s" nicht öffnen.',
  // hyperlink properties button, hyperlink style
  '&Anpassen...', '&Formatvorlage:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Hyperlink Eigenschaften', 'Farbe Normal', 'Farbe Aktiv',
  // Text [color], Background [color]
  '&Text:', '&Hintergrund',
  // Underline [color]
  'U&nterstrich:',
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'H&intergrund',
  // Underline [color], Attributes group-box,
  'U&nterstrich:', 'Eigenschaften',
  // Underline type: always, never, active (below the mouse)
  '&Unterstrich:', 'immer', 'nie', 'aktiv',
  // Same as normal check-box
  'Wie &Normal',
  // underline active links check-box
  'Aktive Links &unterstreichen',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Symbol einfügen', '&Schriftart:', '&Zeichensatz:', 'Unicode',
  // Unicode block
  '&Block:',  
  // Character Code, Character Unicode Code, No Character
  'Zeichencode: %d', 'Zeichencode: Unicode %d', '(kein Zeichen)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Tabelle einfügen', 'Tabellengröße', 'Anzahl der &Spalten:', 'Anzahl der &Zeilen:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabellen Layout', 'Größe &automatisch festlegen', 'Tabellegröße an &Fenster anpassen', 'Tabellengröße &manuell festlegen',
  // Remember check-box
  'Größenanga&ben merken für neue Tabellen',
  // VAlign Form ---------------------------------------------------------------
  'Position',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Eigenschaften', 'Grafik', 'Position und Größe', 'Linie', 'Tabelle', 'Zeile', 'Zelle',
  // Image Appearance, Image Misc, Number tabs
  'Darstellung', 'Verschiedenes', 'Zahl',
  // Box position, Box size, Box appearance tabs
  'Position', 'Größe', 'Darstellung',
  // Preview label, Transparency group-box, checkbox, label
  'Vorschau:', 'Transparenz', '&Transparent', 'Transparente &Farbe:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  '&Ändern...', '&Sichern...', 'Automatisch',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikale Ausrichtung', '&Ausrichtung:',
  'Unterkante an Grundlinie des Textes', 'Mitte an Grundlinie des Textes',
  'Oberkante an Oberlinie des Textes', 'Unterkante an Unterlinie des Textes', 'Mitte an Mittelinie des Textes',
  // align to left side, align to right side
  'linke Seite', 'rechte Seite',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Ändern um:', 'Dehnen', '&Breite:', '&Höhe:', 'Standardgröße: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Proportional skalieren', 'Zu&rücksetzen',
  // Inside the border, border, outside the border groupboxes
  'Innerhalb des Rahmens', 'Rahmen', 'Außerhalb des Rahmens',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Füllfarbe:', '&Abstand:',
  // Border width, Border color labels
  'Rahmen&breite:', 'Rahmen&farbe:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontaler Abstand:', '&Vertikaler Abstand:',
  // Miscellaneous groupbox, Tooltip label
  'Verschiedenes', '&Kurzinformation:',
  // web group-box, alt text
  'Web', 'Alterna&tiver Text:',
  // Horz line group-box, color, width, style
  'Horizontale Linie', '&Farbe:', '&Breite:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabelle', '&Breite:', '&Füllfarbe:', '&Zellraum...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizontaler Zellenabstand:', '&Vertikaler Zellenabstand:', 'Rahmen und Hintergrund',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Sichtbare &Rahmenseiten...', 'Automatisch', 'automatisch',
  // Keep row content together checkbox
  '&Inhalt zusammenhalten',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Drehung', '&Keine', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Rahmen:', 'Sichtbare R&ahmenseiten...',
  // Border, CellBorders buttons
  '&Tabellenrahmen...', 'Z&ellrahmen...',
  // Table border, Cell borders dialog titles
  'Tabellenrahmen', 'Standard Zellrahmen',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Drucken', 'Tabelle auf einer &Seite', 'Anzahl der &Kopfzeilen:',
  'Tabellenkopfzeilen werden auf jeder Seite wiederholt',
  // top, center, bottom, default
  '&Oben', '&Zentriert', '&Unten', '&Standard',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Einstellungen', 'Bevorzugte &Breite:', '&Höhe mindestens:', '&Füllfarbe:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Rahmen', 'Sichtbare Seiten:',  'Sch&attenfarbe:', '&Helle Farbe', 'F&arbe:',
  // Background image
  '&Grafik...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontale Position', 'Vertikale Position', 'Position im Text',
  // Position types: align, absolute, relative
  'Ausrichtung', 'Absolute Position', 'Relative Position',
  // [Align] left side, center, right side
  'linke Seite an linker Seite von',
  'mitte an mitte von',
  'rechte Seite an rechter Seite von',
  // [Align] top side, center, bottom side
  'obere Seite an oberer Seite von',
  'mitte an mitte von',
  'untere Seite an unterer Seite von',
  // [Align] relative to
  'relativ zu',
  // [Position] to the right of the left side of
  'rechts von der linken Seite von',
  // [Position] below the top side of
  'unterhalb der oberen Seite von',
  // Anchors: page, main text area (x2)
  '&Seite', '&Haupttextbereich', 'S&eite', 'Hauptte&xtbereich',
  // Anchors: left margin, right margin
  '&Linker Rand', '&Rechter Rand',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Oberer Rand', '&Unterer Rand', '&Innerer Rand', 'Ä&ußerer Rand',
  // Anchors: character, line, paragraph
  '&Zeichen', 'Z&eile', '&Absatz',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Ränder spi&egeln', 'La&yout in der Tabellenzelle',
  // Above text, below text
  '&Oberhalb des Textes', '&Unterhalb des Textes',
  // Width, Height groupboxes, Width, Height labels
  'Breite', 'Höhe', '&Breite:', '&Höhe:',
  // Auto height label, Auto height combobox item
  'Automatisch', 'automatisch',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikale Ausrichtung', '&Oben', '&Mitte', '&Unten',
  // Border and background button and title
  'Rah&men und Hintergrund...', 'Feldrahmen und Hintergrund',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Einfügen als', '&Einfügen als:', 'Rich Text Format', 'HTML Format',
  'Text', 'Unicode Text', 'Bitmapgrafik', 'Metagrafik',
  'Grafik-Dateien', 'URL',
  // Options group-box, styles
  'Optionen', '&Formatvorlagen:',
  // style options: apply target styles, use source styles, ignore styles
  'Formatvorlagen des Zieldokuments zuweisen', 'Formatvorlagen und Erscheinung erhalten',
  'Erscheinung erhalten, Formatvorlagen ignorieren',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Nummerierung und Aufzählungszeichen', 'Aufzählungszeichen', 'Nummeriert', 'Ungeordnete Liste', 'Nummerierte Liste',
  // Customize, Reset, None
  '&Anpassen...', '&Zurücksetzen', 'Keine',
  // Numbering: continue, reset to, create new
  'Nummerierung fortsetzen', 'Nummerierung zurücksetzen auf', 'Erstelle eine neue Liste, beginnend mit',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  //'Upper Alpha', 'Upper Roman', 'Decimal', 'Lower Alpha', 'Lower Roman',
  'Groß Alpha', 'Groß Roman', 'Dezimal', 'Klein Alpha', 'Klein Roman',
  // Bullet type
  // Circle, Disc, Square,
  'Kreis', 'Scheibe', 'Viereck', 
  // Level, Start from, Continue numbering, Numbering group-box
  '&Ebene', 'Starte von:', '&Fortsetzen', 'Nummerierung',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Liste anpassen', 'Ebenen', '&Zählung:', 'Listeneigenschaften', '&Listentyp:',
  // List types: bullet, image
  'Gliederungspunkt', 'Grafik', 'Nummer einfügen|',
  // Number format, Number, Start level from, Font button
  '&Nummernformat:', 'Nummer', '&Nummerierungsebene beginnt mit:', '&Schriftart...',
  // Image button, bullet character, Bullet button,
  '&Grafik...', 'Aufzählungszeichen', '&Gliederungspunkt...',
  // Position of list text, bullet, number, image, text
  'Listentext Position', 'Aufzählungszeichenposition', 'Position der Nummerierung', 'Position der Grafik', 'Textposition',
  // at, left indent, first line indent, from left indent
  '&an:', 'L&inker Einzug:', '&Einzug der ersten Zeile:', 'vom linken Einzug',
  // [only] one level preview, preview
  '&Vorschau der ersten Ebene', 'Vorschau',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'links ausgerichtet', 'rechts ausgerichtet', 'zentriert',
  // level #, this level (for combo-box)
  'Ebene %d', 'Diese Ebene',
  // Marker character dialog title
  'Aufzählungszeichen bearbeiten',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Absatzrahmen und Hintergrund', 'Rahmen', 'Hintergrund',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Einstellungen', '&Farbe:', '&Breite:', 'Int&erne Breite:', 'O&ffsets...',
  // Sample, Border type group-boxes;
  'Beispiel', 'Rahmentyp',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ohne', '&Einfach', '&Doppelt', 'D&reifach', '&Innen dick', '&Außen dick',
  // Fill color group-box; More colors, padding buttons
  'Füllfarbe', '&Mehr Farben...', '&Abstand...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Abstände', 'Rahmenabstände',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Absatz', 'Ausrichtung', '&Links', '&Rechts', '&Zentriert', '&Blocksatz',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Abstand', '&Vor:', '&Nach:', 'Zeilen&abstand:', 'v&on:',
  // Indents group-box; indents: left, right, first line
  'Einzüge', 'L&inks:', 'R&echts:', 'E&rste Zeile:',
  // indented, hanging
  '&Eingezogen', '&Hängend', 'Beispiel',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Einfach', '1.5 Zeilen', 'Zweizeilig', 'Mindestens', 'Genau', 'Mehrfach',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Einzüge und Abstände', 'Tabulatoren', 'Textfluss',
  // tab stop position label; buttons: set, delete, delete all
  '&Tabulator Position:', '&Setzen', '&Löschen', '&Alle löschen',
  // tab align group; left, right, center aligns,
  'Ausrichtung', '&Links', '&Rechts', '&Zentriert',
  // leader radio-group; no leader
  'Führungslinie', '(Keine)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulatoren zu löschen:', '(Keine)', 'Alle.',
  // Pagination group-box, keep with next, keep lines together
  'Zeilen- und Seitenumbruch', '&Absätze nicht trennen', '&Zeilen nicht trennen',
  // Outline level, Body text, Level #
  '&Gliederungsebene:', 'Textkörper', 'Ebene %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Druckvorschau', 'Seitenbreite', 'Ganze Seite', 'Seiten:',
  // Invalid Scale, [page #] of #
  'Bitte geben Sie eine Nummer zwischen 10 und 500 ein', 'von %d',
  // Hints on buttons: first, prior, next, last pages
  'Erste Seite', 'Vorherige Seite', 'Nächste Seite', 'Letzte Seite',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Zellraum', 'Abstand', '&Zwischen den Zellen', 'Vom Tabellenrahmen zu den Zellen',
  // vertical, horizontal (x2)
  '&Vertikal:', '&Horizontal:', 'V&ertikal:', 'H&orizontal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Rahmen', 'Einstellungen', '&Farbe:', '&Helle Farbe:', 'Scha&ttenfarbe:',
  // Width; Border type group-box;
  '&Breite:', 'Rahmentyp',
  // Border types: none, sunken, raised, flat
  '&Kein', '&Eingesunken', '&Erhoben', '&Glatt',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Teilen', 'Teilen in',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Bitte Anzahl der Zeilen und Spalten eingeben', '&Originalzellen',
  // number of columns, rows, merge before
  'Anzahl der &Spalten:', 'Anzahl der &Zeilen:', '&Verbinden vor dem Teilen',
  // to original cols, rows check-boxes
  'Teilen wie Origina&lspalten', 'Teilen wie O&riginalzeilen',
  // Add Rows form -------------------------------------------------------------
  'Zeilen hinzufügen', 'Anza&hl der Zeilen:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Seite einrichten', 'Seite', 'Kopf- und Fusszeile',
  // margins group-box, left, right, top, bottom
  'Ränder (Millimeter)', 'Ränder (Zoll)', '&Links:', '&Oben:', '&Rechts:', '&Unten:',
  // mirror margins check-box
  '&Seitenränder spiegeln',
  // orientation group-box, portrait, landscape
  'Orientierung', '&Hochformat', 'Qu&erformat',
  // paper group-box, paper size, default paper source
  'Papier', '&Größe:', '&Quelle:',
  // header group-box, print on the first page, font button
  'Kopfzeile', '&Text:', '&Kopfzeile auf der ersten Seite', '&Schriftart...',
  // header alignments: left, center, right
  '&Linksbündig', '&Zentriert', '&Rechtsbündig',
  // the same for footer (different hotkeys)
  '&Fusszeile', 'Te&xt:', 'Fusszeile auf der ersten Seite', 'S&chriftart...',
  'L&inksbündig', 'Z&entriert', 'Rec&htsbündig',
  // page numbers group-box, start from
  'Seitennummerierung', '&Beginnt bei:',
  // hint about codes
  'Spezielle Zeichenkombinationen:'#13'&&p - Seitennummer; &&P - Seitenanzahl; &&d - Aktuelles Datum; &&t - Aktuelle Zeit.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Codepage für Textdatei', 'Zei&chenkodierung für Datei auswählen:',
  // thai, japanese, chinese (simpl), korean
  'Thailändisch', 'Japanisch', 'Chinesisch (Vereinfacht)', 'Koreanisch',
  // chinese (trad), central european, cyrillic, west european
  'Chinesisch (Traditionell)', 'Zentral- und Osteuropa', 'Kyrillisch', 'Westeuropäisch',
  // greek, turkish, hebrew, arabic
  'Griechisch', 'Türkisch', 'Hebräisch', 'Arabisch',
  // baltic, vietnamese, utf-8, utf-16
  'Baltisch', 'Vietnamesisch', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Erscheinung', '&Stil auswählen:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'In Text umwandeln', 'Trennzeichen &wählen:',
  // line break, tab, ';', ','
  'Zeilenumbruch', 'Tabulator', 'Semikolon', 'Komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'Eine Tabelle mit verbundenen Zeilen kann nicht sortiert werden',
  // title, main options groupbox
  'Tabelle sortieren', 'Sortieren',
  // sort by column, case sensitive
  'Nach &Spalte sortieren:', 'S&chreibweise beachten',
  // heading row, range or rows
  'Kopfzeile aussc&ließen', 'Sortiere Zeilen: von %d bis %d',
  // order, ascending, descending
  'Reihenfolge', '&Aufsteigend', 'Absteigen&d',
  // data type, text, number
  'Typ', '&Text', '&Zahl',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Zahl einfügen', 'Eigenschaften', '&Zählername:', '&Nummerierungstyp:',
  // numbering groupbox, continue, start from
  'Nummerierung', '&Fortsetzen', '&Beginnen von:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Beschriftung', '&Bezeichnung:', 'Be&zeichnung nicht in der Beschriftung verwenden',
  // position radiogroup
  'Position', '&Oberhalb des ausgewählten Objekts', '&Unterhalb des ausgewählten Objekts',
  // caption text
  'Beschriftungs&text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Nummerierung', 'Abbildung', 'Tabelle',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Sichtbare Rahmenseiten', 'Rahmen',
  // Left, Top, Right, Bottom checkboxes
  '&Linke Seite', '&Obere Seite', '&Rechte Seite', '&Untere Seite',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Größe der Tabellenspalte ändern', 'Größe der Tabellenzeile ändern',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Erste Zeile einrücken', 'Einzug links', 'Hängender Einzug', 'Einzug rechts',
  // Hints on lists: up one level (left), down one level (right)
  'Eine Ebene höher verschieben', 'Eine Ebene niedriger verschieben',
  // Hints for margins: bottom, left, right and top
  'Unterer Seitenrand', 'Linker Seitenrand', 'Rechter Seitenrand', 'Oberer Seitenrand',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Am Tabulator links ausrichten', 'Am Tabulator rechts ausrichten', 'Am Tabulator zentriert ausrichten', 'Am Dezimalkomma ausrichten',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Standard', 'Standard eingerückt', 'Kein Leerraum', 'Überschrift %d', 'Listenabsatz',
  // Hyperlink, Title, Subtitle
  'Hyperlink', 'Titel', 'Untertitel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Hervorhebung', 'Schwache Hervorhebung', 'Intensive Hervorhebung', 'Fett',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Zitat', 'Intensives Zitat', 'Schwacher Verweis', 'Intensiver Verweis',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blocksatz', 'HTML Variable', 'HTML Code', 'HTML Akronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Definition', 'HTML Tastatur', 'HTML Beispiel', 'HTML Schreibmaschine',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Vorformatiert', 'HTML Zitat', 'Kopf', 'Fuß', 'Seitenzahl',
  // Caption
  'Beschriftung',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Verweis Endnote', 'Verweis Fußnote', 'Text Endnote', 'Text Fußnote',
  // Sidenote Reference, Sidenote Text
  'Randnotiz Referenz', 'Randnotiz Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'Farbe', 'Hintergrundfarbe', 'transparent', 'Standard', 'Farbe Unterstreichung',
  // default background color, default text color, [color] same as text
  'Standardhintergrundfarbe', 'Standardtextfarbe', 'wie Text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'einfach', 'dick', 'doppelt', 'gepunkted', 'dick gepunkted', 'gestrichelt',
  // underline types: thick dashed, long dashed, thick long dashed,
  'dick gestrichelt', 'lang gestrichelt', 'dick und lang gestrichelt',
  // underline types: dash dotted, thick dash dotted,
  'Strich-Punkt', 'Strich-Punkt dick',
  // underline types: dash dot dotted, thick dash dot dotted
  'Strich-Punkt-Punkt', 'Strich-Punkt-Punkt dick',
  // sub/superscript: not, subsript, superscript
  'nicht tief-/hochgestellt', 'tiefgestellt', 'hochgestellt',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'Bi-di Modus:', 'übernommen', 'links nach rechts', 'rechts nach links',
  // bold, not bold
  'fett', 'nicht fett',
  // italic, not italic
  'kursiv', 'nicht kursiv',
  // underlined, not underlined, default underline
  'unterstrichen', 'nicht unterstrichen', 'Standardunterstreichung',
  // struck out, not struck out
  'durchgestrichen', 'nicht durchgestrichen',
  // overlined, not overlined
  'überstrichen', 'nicht überstrichen',
  // all capitals: yes, all capitals: no
  'alles in Kapitälchen', 'Kapitälchen für alles aus',
  // vertical shift: none, by x% up, by x% down
  'ohne vertikalen Versatz', 'versetzt um %d%% nach oben', 'versetzt um %d%% nach unten',
  // characters width [: x%]
  'Zeichenbreite',
  // character spacing: none, expanded by x, condensed by x
  'Standardzeichenabstand', 'Abstand erweitert um %s', 'Abstand verringert um %s',
  // charset
  'Skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'Standardschrift', 'Standardabsatz', 'Übernommen',
  // [hyperlink] highlight, default hyperlink attributes
  'Hervorhebung', 'Standard',
  // paragraph aligmnment: left, right, center, justify
  'Ausrichtung links', 'Ausrichtung rechts', 'Zentriert', 'Blocksatz',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'Zeilenhöhe: %d%%', 'Abstand zwischen den Zeilen: %s', 'Zeilenhöhe: mindestens %s',
  'Zeilenhöhe: exakt %s',
  // no wrap, wrap
  'Zeilenumbruch deaktiviert', 'Zeilenumbruch',
  // keep lines together: yes, no
  'Zeilen zusammenhalten', 'Zeilen nicht zusammenhalten',
  // keep with next: yes, no
  'mit nächsten Absatz zusammenhalten', 'nicht mit nächsten Absatz zusammenhalten',
  // read only: yes, no
  'nur-lesen', 'änderbar',
  // body text, heading level x
  'Gliederungsebene: Text', 'Gliederungsebene: %d',
  // indents: first line, left, right
  'Einzug erste Zeile', 'Einzug links', 'Einzug rechts',
  // space before, after
  'Abstand davor', 'Abstand danach',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'Tabulatoren', 'ohne', 'Ausrichtung', 'links', 'rechts', 'zentriert',
  // tab leader (filling character)
  'Füllzeichen',
  // no, yes
  'Nein', 'Ja',
  // [padding/spacing/side:] left, top, right, bottom
  'links', 'oben', 'rechts', 'unten',
  // border: none, single, double, triple, thick inside, thick outside
  'ohne', 'einfach', 'doppelt', 'dreifach', 'innen dick', 'außen dick',
  // background, border, padding, spacing [between text and border]
  'Hintergrund', 'Rahmen', 'Textabstand', 'Zellenabstand',
  // border: style, width, internal width, visible sides
  'Stil', 'Breite', 'Innere Breite', 'Sichtbare Seiten',
  // style inspector -----------------------------------------------------------
  // title
  'Formatinspektor',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Ohne)', 'Absatz', 'Schrift', 'Eigenschaften',
  // border and background, hyperlink, standard [style]
  'Rahmen und Hintergrund', 'Hyperlink', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Formatvorlagen', 'Formatvorlage',
  // name, applicable to,
  '&Name:', 'Anwendbar &auf:',
  // based on, based on (no &),
  '&Basiert auf:', 'Basiert auf:',
  //  next style, next style (no &)
  'Formatvorlage für &nachfolgenden Absatz:', 'Formatvorlage für nachfolgenden Absatz:',
  // quick access check-box, description and preview
  'Sch&nellzugriff', 'Beschreibung und &Vorschau:',
  // [applicable to:] paragraph and text, paragraph, text
  'Absatz und Text', 'Absatz', 'Text',
  // text and paragraph styles, default font
  'Text- und Absatzformatvorlagen', 'Standardschrift',
  // links: edit, reset, buttons: add, delete
  'Bearbeiten', 'Zurücksetzen', '&Hinzufügen', '&Löschen',
  // add standard style, add custom style, default style name
  '&Standardformatvorlage hinzufügen...', 'Benutzerformatvorlage hin&zufügen', 'Formatvorlage %d',
  // choose style
  'Formatvorlage aus&wählen:',
  // import, export,
  '&Importieren...', '&Exportieren...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView-Formatvorlagen (*.rvst)|*.rvst', 'Fehler beim Laden der Datei', 'Diese Datei enthält keine Formatvorlagen',
  // Title, group-box, import styles
  'Formatvorlagen aus Datei importieren', 'Importieren', 'Formatvorlagen i&mportieren:',
  // existing styles radio-group: Title, override, auto-rename
  'Vorhandene Formatvorlagen', 'ü&berschreiben', 'u&mbenennen und hinzufügen',
  // Select, Unselect, Invert,
  '&Auswählen', 'Aus&wahl aufheben', '&Umkehren',
  // select/unselect: all styles, new styles, existing styles
  '&Alle Formatvorlagen', '&Neue Formatvorlagen', '&Vorhandene Formatvorlagen',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Format löschen', 'Alle Formatvorlagen',
  // dialog title, prompt
  'Formatvorlagen', '&Bitte wählen Sie die Formatvorlage:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonyme', '&Alle ignorieren', '&Zum Wörterbuch hinzufügen',
  // Progress messages ---------------------------------------------------------
  'Lade %s', 'Vorbereitung für Ausdruck...', 'Drucke Seite %d',
  'Umwandeln von RTF...',  'Umwandeln in RTF...',
  'Lese %s...', 'Schreibe %s...', 'Text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ohne Notiz', 'Fußnote', 'Endnote', 'Randnotiz', 'Textfeld'
  );


initialization
  RVA_RegisterLanguage(
    'German', // english language name, do not translate
    'Deutsch', // native language name
    ANSI_CHARSET, @Messages);

end.
