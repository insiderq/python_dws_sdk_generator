
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for applying lists                       }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit ListGalleryRVFrm;

interface

{$IFNDEF RVDONOTUSELISTS}

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, ActnList,
  Dialogs, BaseRVFrm, ComCtrls, StdCtrls, RVOfficeRadioBtn, RVStyle,
  {$IFDEF USERVKSDEVTE}
  te_theme, te_controls, te_extctrls,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntComCtrls,
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  Vcl.Themes,
  {$ENDIF}
  PtblRV, RVReport, ImgList, RVSpinEdit, RVClasses, RVEdit, RVStr, CRVData,
  RichView,
  RVALocalize;

{
 Initially, all RVStyles on this form are measured in pixels, and all indents
 are multiple of 24.
 Then the form is created, they are adjusted according to the action's IndentStep
 (this correction automatically converts to twips, if necessary)
 Action's ModifiedTemplates are measured in RVA_UnitsProgram.
 However, list styles taken from the editor (StyleIndices[][]<>-1) are measured
 in Editor.Style.Units.                      

}

type
  TfrmRVListGallery = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    pc: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    rg1: TRVOfficeRadioGroup;
    btnEdit1: TButton;
    RVStyle2: TRVStyle;
    il: TImageList;
    helper: TRVReportHelper;
    btnReset: TButton;
    rvstmp: TRVStyle;
    RVStyle1: TRVStyle;
    rg2: TRVOfficeRadioGroup;
    btnEdit2: TButton;
    il2: TImageList;
    il3: TImageList;
    cmbNumbering: TComboBox;
    seStartFrom: TRVSpinEdit;
    rvsTemplate1: TRVStyle;
    rvsTemplate2: TRVStyle;
    rvstmp2: TRVStyle;
    procedure FormCreate(Sender: TObject);
    procedure rg1Click(Sender: TObject);
    procedure btnEdit1Click(Sender: TObject);
    procedure btnEdit2Click(Sender: TObject);
    procedure rg2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rg2DblClickItem(Sender: TObject);
    procedure cmbNumberingClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
  private
    { Private declarations }
    SourceRVStyle: TRVStyle;
    StyleIndices: array [0..1] of TRVIntegerList;
    StartFromList: TRVIntegerList;
    FMainPage, FMainIndex: Integer;
    _btnEdit1, _btnEdit2, _btnReset,
    _cmbNumbering, _pc, _ts1, _ts2, _btnOk: TControl;
    FIndentStep: TRVStyleLength;
    procedure CreateThumbnail(bmp: TBitmap; ListNo: Integer; RVStyle1: TRVStyle);
    procedure EditList(RVStyle: TRVStyle; Index: Integer);
    procedure InitBullets;
    function GetRVStyle(PageNo: Integer): TRVStyle;
    function GetRVStyleTemplate(PageNo: Integer): TRVStyle;
    function Getrg(PageNo: Integer): TRVOfficeRadioGroup;
    procedure AdjustIndents(ListStyles: TRVListInfos);
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    Action: TAction;
    constructor CreateEx(AOwner: TComponent; AAction: TAction);
    procedure GetListStyle(var ListStyle: TRVListInfo; var ListNo: Integer; var StartFrom: Integer;
      var UseStartFrom: Boolean);
    procedure InitStyles(rve: TCustomRichViewEdit);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSELISTS}

uses RVRVData, ParaListRVFrm, RichViewActions;

const ThumbWidth = 67;
      ThumbHeight = 93;
      LongText = '--------------------------------------------------------------------------------------------------------';


{$R *.dfm}

function GetBackColor: TColor;
begin
  Result := clWindow;
  {$IFDEF USERVKSDEVTE}
  if GetThemeLink(nil)<>nil then
    Result := GetThemeLink(nil).SysColors[clWindow];
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  if StyleServices.Enabled then
    Result := StyleServices.GetStyleColor(scEdit);
  {$ENDIF}
end;

function GetFontColor: TColor;
begin
  Result := clWindowText;
  {$IFDEF USERVKSDEVTE}
  if GetThemeLink(nil)<>nil then
    Result := GetThemeLink(nil).SysColors[clWindowText];
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  if StyleServices.Enabled then
    Result := StyleServices.GetStyleFontColor(sfEditBoxTextNormal);
  {$ENDIF}
end;


{ TfrmRVListGallery }

procedure TfrmRVListGallery.CreateThumbnail(bmp: TBitmap; ListNo: Integer;
  RVStyle1: TRVStyle);


var h,level,i: Integer;
begin
  bmp.Canvas.Brush.Color := clWindow; //GetThemedColor(clWindow);

  bmp.Canvas.FillRect(Bounds(0,0,bmp.Width,bmp.Height));

  level := 0;
  if RVStyle1.ListStyles[ListNo].Levels.Count=0 then
    exit;

  rvstmp.ListStyles.Clear;
  rvstmp.ListStyles.Add.Assign(RVStyle1.ListStyles[ListNo]);

  for i := 0 to rvstmp.ListStyles[0].Levels.Count-1 do begin
   rvstmp.ListStyles[0].Levels[i].LeftIndent := Round(rvstmp.ListStyles[0].Levels[i].LeftIndent/2);
   rvstmp.ListStyles[0].Levels[i].MarkerIndent := Round(rvstmp.ListStyles[0].Levels[i].MarkerIndent/2);
   rvstmp.ListStyles[0].Levels[i].FirstIndent := Round(rvstmp.ListStyles[0].Levels[i].FirstIndent/2);
  end;
  ListNo := 0;

  helper.RichView.Clear;
  helper.RichView.Style := rvstmp;

  for i := 0 to 2 do begin
    helper.RichView.SetListMarkerInfo(-1, ListNo, level, 1, 0, False);
    helper.RichView.AddNL(LongText, 0, -1);
    helper.RichView.SetAddParagraphMode(False);
    helper.RichView.AddNL(LongText, 0, 0);
    helper.RichView.SetAddParagraphMode(True);
    if not rvstmp.ListStyles[ListNo].OneLevelPreview then
      inc(level);
    if level = rvstmp.ListStyles[ListNo].Levels.Count then
      level := 0;
  end;

  helper.Init(Canvas, bmp.Width);
  helper.FormatNextPage(1000);
  h := helper.EndAt;
  if h<il.Height then
    h := il.Height;
  helper.rv.ClientHeight := h;
//  w := il.Width;
  helper.rv.ClientWidth := MulDiv(helper.RichView.ClientWidth, h, il.Height);
  helper.DrawPreview(1, bmp.Canvas, Rect(0,0,il.Width,il.Height));
  //helper.DrawPage(1, bmp.Canvas, True, bmp.Height);
end;

procedure TfrmRVListGallery.EditList(RVStyle: TRVStyle; Index: Integer);
var frm: TfrmRVParaList;
    r: Boolean;
    rvs: TRVStyle;
    OldUnits: TRVStyleUnits;
begin
  if Index<1 then
    exit;

  if StyleIndices[GetPageControlActivePageIndex(_pc)][Index-1] >= 0 then
    rvs := SourceRVStyle
  else
    rvs := RVStyle;
  frm := TfrmRVParaList.Create(Application, ControlPanel);
  frm.SetListStyle(RVStyle.ListStyles[Index-1], rvs);
  r := frm.ShowModal=mrOk;
  if r then begin
    frm.GetListStyle(rvstmp.ListStyles[0]);
    if rvstmp.ListStyles[0].HasNumbering then
      SetPageControlActivePage(_pc, _ts2)
    else
      SetPageControlActivePage(_pc, _ts1);
    Getrg(GetPageControlActivePageIndex(_pc)).ItemIndex := Index;
    frm.GetListStyle(TrvActionParaList(Action).ModifiedTemplates[GetPageControlActivePageIndex(_pc)].ListStyles[Index-1]);
    if rvs = SourceRVStyle then begin
      StyleIndices[GetPageControlActivePageIndex(_pc)][Index-1] := -2; // measured in SourceRVStyle.Units
      if SourceRVStyle.Units<>TRVAControlPanel(ControlPanel).UnitsProgram then begin
        OldUnits := TrvActionParaList(Action).ModifiedTemplates[GetPageControlActivePageIndex(_pc)].Units;
        TrvActionParaList(Action).ModifiedTemplates[GetPageControlActivePageIndex(_pc)].Units := SourceRVStyle.Units;
        try
          TrvActionParaList(Action).ModifiedTemplates[GetPageControlActivePageIndex(_pc)].ListStyles[Index-1].ConvertToDifferentUnits(TRVAControlPanel(ControlPanel).UnitsProgram);
        finally
          TrvActionParaList(Action).ModifiedTemplates[GetPageControlActivePageIndex(_pc)].Units := OldUnits;
        end;
      end
      end
    else
      StyleIndices[GetPageControlActivePageIndex(_pc)][Index-1] := -1; // measured in ControlPanel.UnitsProgram
    frm.GetListStyle(GetRVStyle(GetPageControlActivePageIndex(_pc)).ListStyles[Index-1]);
  end;
  frm.Free;
  if r then
    ModalResult := mrOk;
end;

procedure TfrmRVListGallery.FormCreate(Sender: TObject);
var bmp: TBitmap;
    r: TRect;
    i: Integer;
    Act: TrvActionParaList;
begin
  _btnEdit1 := btnEdit1;
  _btnEdit2 := btnEdit2;
  _btnReset := btnReset;
  _cmbNumbering := cmbNumbering;
  _pc  := pc;
  _ts1 := ts1;
  _ts2 := ts2;
  _btnOk := btnOk;
  inherited;
  StartFromList := TRVIntegerList.CreateEx(7,-1);
  for i := 0 to 1 do begin
    StyleIndices[i] := TRVIntegerList.CreateEx(7,-1);
  end;

  InitBullets;
  if FIndentStep<>0 then begin
    AdjustIndents(rvsTemplate1.ListStyles);
    AdjustIndents(rvsTemplate2.ListStyles);
  end;

  RVStyle1.TextStyles[0].Color := clBtnShadow;
  RVStyle1.TextStyles[0].BackColor := clBtnFace;
  RVStyle2.TextStyles[0].Color := clBtnShadow;
  RVStyle2.TextStyles[0].BackColor := clBtnFace;
  rvsTemplate1.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
  rvsTemplate1.TextStyles[0].Color := clBtnShadow;
  rvsTemplate1.TextStyles[0].BackColor := clBtnFace;
  rvsTemplate2.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
  rvsTemplate2.TextStyles[0].Color := clBtnShadow;
  rvsTemplate2.TextStyles[0].BackColor := clBtnFace;

  Act := Action as TrvActionParaList;

  if Act.ModifiedTemplates[0]=nil then begin
    Act.ModifiedTemplates[0] := TRVStyle.Create(nil);
    Act.ModifiedTemplates[0].Units := TRVAControlPanel(ControlPanel).UnitsProgram;
    Act.ModifiedTemplates[0].TextStyles.Clear;
    Act.ModifiedTemplates[0].ParaStyles.Clear;
    Act.ModifiedTemplates[0].ListStyles := rvsTemplate1.ListStyles;
  end;
  if Act.ModifiedTemplates[1]=nil then begin
    Act.ModifiedTemplates[1] := TRVStyle.Create(nil);
    Act.ModifiedTemplates[1].Units := TRVAControlPanel(ControlPanel).UnitsProgram;
    Act.ModifiedTemplates[1].TextStyles.Clear;
    Act.ModifiedTemplates[1].ParaStyles.Clear;
    Act.ModifiedTemplates[1].ListStyles := rvsTemplate2.ListStyles;
  end;

  RVStyle1.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
  RVStyle1.ListStyles := Act.ModifiedTemplates[0].ListStyles;
  RVStyle2.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
  RVStyle2.ListStyles := Act.ModifiedTemplates[1].ListStyles;

  rvstmp.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
  rvstmp.ParaStyles := RVStyle1.ParaStyles;
  rvstmp.TextStyles := RVStyle1.TextStyles;

  helper.RichView.Style := RVStyle1;
  helper.RichView.LeftMargin := 0;
  helper.RichView.TopMargin := 0;
  helper.RichView.RightMargin := 0;
  helper.RichView.BottomMargin := 0;

  r := Rect(0,0,il.Width,il.Height);

  bmp := TBitmap.Create;
  bmp.Width := il.Width;
  bmp.Height := il.Height;

  bmp.Canvas.Brush.Color := GetBackColor;
  bmp.Canvas.FillRect(r);
  bmp.Canvas.Font.Name := 'Arial';
  bmp.Canvas.Font.Size := 12;
  bmp.Canvas.Font.Color := GetFontColor;

  {$IFDEF USERVTNT}
  DrawTextW(bmp.Canvas.Handle, PWideChar(RVA_GetS(rvam_lg_None)), -1, r,
    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
  {$ELSE}
  DrawText(bmp.Canvas.Handle, RVA_GetPC(rvam_lg_None, ControlPanel), -1, r,
    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
  {$ENDIF}
  il.AddMasked(bmp, clNone);
  il2.AddMasked(bmp, clNone);
  il3.AddMasked(bmp, clNone);
  bmp.Free;

  for i := 0 to RVStyle1.ListStyles.Count-1 do begin
    bmp := TBitmap.Create;
    bmp.Width := il.Width;
    bmp.Height := il.Height;
    bmp.Canvas.Brush.Color := clWindow;
    bmp.Canvas.FillRect(r);
    CreateThumbnail(bmp,i,RVStyle1);
    il.AddMasked(bmp, clNone);
    bmp.Free;
  end;

  for i := 0 to RVStyle2.ListStyles.Count-1 do begin
    bmp := TBitmap.Create;
    bmp.Width := il.Width;
    bmp.Height := il.Height;
    bmp.Canvas.Brush.Color := clWindow;
    bmp.Canvas.FillRect(r);
    CreateThumbnail(bmp,i,RVStyle2);
    il2.AddMasked(bmp, clNone);
    bmp.Free;
  end;

  {
  for i := 0 to RVStyle3.ListStyles.Count-1 do begin
    bmp := TBitmap.Create;
    bmp.Width := il.Width;
    bmp.Height := il.Height;
    bmp.Canvas.Brush.Color := GetThemedColor(clWindow);
    bmp.Canvas.FillRect(r);
    CreateThumbnail(bmp,i,RVStyle3);
    il3.AddMasked(bmp, clWindow);
    bmp.Free;
  end;
  }
  (_pc as TWinControl).Handle;
  AdjustFormSize;
end;

procedure TfrmRVListGallery.FormDestroy(Sender: TObject);
var i : Integer;
begin
  for i := 0 to 1 do begin
    StyleIndices[i].Free;
    StyleIndices[i] := nil;
  end;
  StartFromList.Free;
  StartFromList := nil;
  inherited;
end;

procedure TfrmRVListGallery.rg1Click(Sender: TObject);
var en: Boolean;
begin
  _btnEdit1.Enabled := rg1.ItemIndex>0;
  if GetPageControlActivePageIndex(_pc)=0 then begin
    en := (rg1.ItemIndex>0);
    if en then begin
      rvstmp2.ListStyles := RVStyle1.ListStyles;
      if StyleIndices[0][rg1.ItemIndex-1]>=0 then begin
        rvstmp2.Units := SourceRVStyle.Units;
        rvstmp2.ConvertToDifferentUnits(TRVAControlPanel(ControlPanel).UnitsProgram);
        end
      else
        rvstmp2.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
      en := not rvstmp2.ListStyles[rg1.ItemIndex-1].IsSimpleEqual(rvsTemplate1.ListStyles[rg1.ItemIndex-1], False);
    end;
    _btnReset.Enabled := en;
  end;
end;

procedure TfrmRVListGallery.rg2Click(Sender: TObject);
var en: Boolean;
begin
  if GetPageControlActivePageIndex(_pc)=1 then begin
    en := (rg2.ItemIndex>0);
    if en then begin
      rvstmp2.ListStyles := RVStyle2.ListStyles;
      if StyleIndices[1][rg2.ItemIndex-1]>=0 then begin
        rvstmp2.Units := SourceRVStyle.Units;
        rvstmp2.ConvertToDifferentUnits(TRVAControlPanel(ControlPanel).UnitsProgram);
        end
      else
        rvstmp2.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
      en := not rvstmp2.ListStyles[rg2.ItemIndex-1].IsSimpleEqual(rvsTemplate2.ListStyles[rg2.ItemIndex-1], False);
    end;
    _btnReset.Enabled := en;
  end;
  if rg2.ItemIndex<0 then
    exit;
  ClearXBoxItems(_cmbNumbering);
  if rg2.ItemIndex=0 then begin
    _cmbNumbering.Enabled := False;
    seStartFrom.Enabled := False;
    seStartFrom.Indeterminate := True;
    _btnEdit2.Enabled := False;
    exit;
  end;
  if StyleIndices[1][rg2.ItemIndex-1]>=0 then begin
    XBoxItemsAddObject(_cmbNumbering, RVA_GetS(rvam_lg_NumContinue, ControlPanel), TObject(1));
    XBoxItemsAddObject(_cmbNumbering, RVA_GetS(rvam_lg_NumReset, ControlPanel), TObject(2));
  end;
  XBoxItemsAddObject(_cmbNumbering, RVA_GetS(rvam_lg_NumCreate, ControlPanel), TObject(3));
  _cmbNumbering.Enabled := True;
  if StyleIndices[1][rg2.ItemIndex-1]>=0 then begin
    if StartFromList[rg2.ItemIndex-1]<0 then begin
      SetXBoxItemIndex(_cmbNumbering, 0);
      seStartFrom.Indeterminate := True;      
      seStartFrom.Enabled := False;
      end
    else begin
      SetXBoxItemIndex(_cmbNumbering, 1);
      seStartFrom.Value := StartFromList[rg2.ItemIndex-1];
      seStartFrom.Enabled := True;
    end
    end
  else begin
    SetXBoxItemIndex(_cmbNumbering, 0);
    seStartFrom.Value := 1;
    seStartFrom.Enabled := True;
  end;
  _btnEdit2.Enabled := rg2.ItemIndex>0;
end;

procedure TfrmRVListGallery.cmbNumberingClick(Sender: TObject);
begin
  if (GetXBoxItemIndex(_cmbNumbering)<0) or
     (Integer(GetXBoxObject(_cmbNumbering, GetXBoxItemIndex(_cmbNumbering)))=1) then begin
    seStartFrom.Indeterminate := True;
    seStartFrom.Enabled := False;
    exit;
  end;
  if (Integer(GetXBoxObject(_cmbNumbering, GetXBoxItemIndex(_cmbNumbering)))=2) and
     (StartFromList[rg2.ItemIndex-1]>=0) then
    seStartFrom.Value := StartFromList[rg2.ItemIndex-1]
  else
    seStartFrom.Value := 1;
  seStartFrom.Enabled := True;
end;

procedure TfrmRVListGallery.btnEdit1Click(Sender: TObject);
begin
  EditList(RVStyle1, rg1.ItemIndex)
end;

procedure TfrmRVListGallery.btnEdit2Click(Sender: TObject);
begin
  EditList(RVStyle2, rg2.ItemIndex)
end;

procedure TfrmRVListGallery.InitBullets;
var i: Integer;
begin
  with rvsTemplate1.ListStyles[0] do begin
    Levels[0].FormatString := {$IFDEF RVUNICODESTR}#$0076{$ELSE}#$76{$ENDIF};
    Levels[1].FormatString := {$IFDEF RVUNICODESTR}#$0077{$ELSE}#$D8{$ENDIF}; // intentionally different
    Levels[2].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
    Levels[3].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
    Levels[4].FormatString := {$IFDEF RVUNICODESTR}#$00AD{$ELSE}#$A8{$ENDIF}; // intentionally different
    {$IFDEF RVUNICODESTR}
    Levels[4].Font.Name := RVFONT_WINGDINGS;
    {$ENDIF}
    Levels[5].FormatString := {$IFDEF RVUNICODESTR}#$0077{$ELSE}#$D8{$ENDIF}; // intentionally different
    Levels[6].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
    Levels[7].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
    Levels[8].FormatString := {$IFDEF RVUNICODESTR}#$00A8{$ELSE}#$A8{$ENDIF};
  end;
  with rvsTemplate1.ListStyles[1] do begin
    Levels[0].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
    Levels[1].FormatString := 'o';
    Levels[2].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
    Levels[3].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
    Levels[4].FormatString := 'o';
    Levels[5].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
    Levels[6].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
    Levels[7].FormatString := 'o';
    Levels[8].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
  end;
  for i := 2 to 4 do
    with rvsTemplate1.ListStyles[i] do begin
      Levels[1].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
      Levels[2].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
      Levels[3].FormatString := 'o';
      Levels[4].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
      Levels[5].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
      Levels[6].FormatString := 'o';
      Levels[7].FormatString := {$IFDEF RVUNICODESTR}#$00A7{$ELSE}#$A7{$ENDIF};
      Levels[8].FormatString := {$IFDEF RVUNICODESTR}#$00B7{$ELSE}#$B7{$ENDIF};
    end;
  rvsTemplate1.ListStyles[2].Levels[0].FormatString := 'q';
  rvsTemplate1.ListStyles[3].Levels[0].FormatString := {$IFDEF RVUNICODESTR}#$00A4{$ELSE}#$FC{$ENDIF}; // intentionally different
  rvsTemplate1.ListStyles[4].Levels[0].FormatString := {$IFDEF RVUNICODESTR}#$0077{$ELSE}#$D8{$ENDIF}; // intentionally different
end;

procedure TfrmRVListGallery.GetListStyle(var ListStyle: TRVListInfo;
  var ListNo: Integer; var StartFrom: Integer;
  var UseStartFrom: Boolean);
begin
  if Getrg(GetPageControlActivePageIndex(_pc)).ItemIndex<=0 then
    ListStyle := nil
  else begin
    ListStyle := GetRVStyle(GetPageControlActivePageIndex(_pc)).ListStyles[Getrg(GetPageControlActivePageIndex(_pc)).ItemIndex-1];
    ListNo := StyleIndices[GetPageControlActivePageIndex(_pc)][Getrg(GetPageControlActivePageIndex(_pc)).ItemIndex-1];
    if ListNo=-1 then // do not convert if -2, or >= 0
      ListStyle.ConvertToDifferentUnits(SourceRVStyle.Units);
    if GetPageControlActivePageIndex(_pc)=1 then
      case Integer(GetXBoxObject(_cmbNumbering, GetXBoxItemIndex(_cmbNumbering))) of
        1:
          begin
            UseStartFrom := False;
            StartFrom := 1;
          end;
        2:
          begin
            UseStartFrom := True;
            StartFrom := seStartFrom.AsInteger;
          end;
        3:
          begin
            StartFrom := seStartFrom.AsInteger;
            UseStartFrom := seStartFrom.AsInteger<>1;
            ListNo := -1;
          end;
      end;
  end;
end;

function TfrmRVListGallery.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := _pc.Left*2+_pc.Width;
  Height := _btnOk.Top+_btnOk.Height+(_btnOk.Top-_pc.Top-_pc.Height);
  Result := True;
end;

procedure TfrmRVListGallery.InitStyles(rve: TCustomRichViewEdit);
var StartNo, StartNo2, EndNo, StartOffs, EndOffs: Integer;

   procedure SetStyle(PageNo, Index, ListNo, StartFrom: Integer);
   var bmp: TBitmap;
   begin
     StyleIndices[PageNo][Index] := ListNo;
     if PageNo=1 then
       StartFromList[Index] := StartFrom;
     bmp := TBitmap.Create;
     bmp.Width := il.Width;
     bmp.Height := il.Height;
     bmp.Canvas.Brush.Color := clWindow;
     bmp.Canvas.FillRect(Rect(0,0,il.Width,il.Height));
     rvstmp.ConvertToDifferentUnits(rve.Style.Units);
     case PageNo of
       0:
         begin
           RVStyle1.ListStyles[Index] := rve.Style.ListStyles[ListNo];
           CreateThumbnail(bmp, Index, RVStyle1);
           il.ReplaceMasked(Index+1, bmp, clNone);
         end;
       1:
         begin
           RVStyle2.ListStyles[Index] := rve.Style.ListStyles[ListNo];
           CreateThumbnail(bmp, Index, RVStyle2);
           il2.ReplaceMasked(Index+1, bmp, clNone);
         end;
     end;
     rvstmp.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
     rvstmp.ParaStyles := RVStyle1.ParaStyles;
     rvstmp.TextStyles := RVStyle1.TextStyles;
     bmp.Free;
   end;

   procedure PlaceStyle(ItemNo, ListNo, StartFrom: Integer; UseStartFrom: Boolean; var PageNo, Index: Integer);
   var i: Integer;
   begin
     if ListNo<0 then
       exit;
     if not UseStartFrom or (ItemNo<StartNo) then
       StartFrom := -1;
     if rve.Style.ListStyles[ListNo].HasNumbering then
       PageNo := 1
     else
       PageNo := 0;
     for i := 0 to 6 do
       if StyleIndices[PageNo][i]=ListNo then
         exit;
     Index := StrToIntDef(rve.Style.ListStyles[ListNo].StyleName, 7)-1;
     if StyleIndices[PageNo][Index]<0 then begin
       SetStyle(PageNo, Index, ListNo, StartFrom);
       exit;
     end;
     for i := 6 downto 0 do
       if StyleIndices[PageNo][i]<0 then begin
         Index := i;
         SetStyle(PageNo, Index, ListNo, StartFrom);
         exit;
       end;
   end;
var i, ListNo, StartFrom, Dummy1, Dummy3: Integer;
    UseStartFrom: Boolean;
begin
  SourceRVStyle := rve.Style;
  FMainPage := 0;
  FMainIndex:= -1;
  rve := rve.TopLevelEditor;
  rve.RVData.GetSelectionBoundsEx(StartNo, StartOffs, EndNo, EndOffs, True);
  if rve.GetListMarkerInfo(EndNo, ListNo, Dummy1, StartFrom, UseStartFrom)>=0 then
     PlaceStyle(EndNo, ListNo, StartFrom, UseStartFrom, FMainPage, FMainIndex);
  while (StartNo>0) and not rve.IsParaStart(StartNo) do
    dec(StartNo);
  StartNo2 := StartNo;
  if StartNo2>0 then
    dec(StartNo2);
  while (StartNo2>0) and not rve.IsParaStart(StartNo2) do
    dec(StartNo2);
  while (EndNo<rve.ItemCount-1) and not rve.IsParaStart(EndNo) do
    inc(EndNo);
  for i := StartNo2 to EndNo do
    if rve.IsParaStart(i) and
       (rve.GetListMarkerInfo(i, ListNo, Dummy1, StartFrom, UseStartFrom)>=0) then
      PlaceStyle(i, ListNo, StartFrom, UseStartFrom, Dummy1, Dummy3);
  case FMainPage of
    0:
      SetPageControlActivePage(_pc, _ts1);
    1:
      SetPageControlActivePage(_pc, _ts2);
  end;
  GetRG(FMainPage).ItemIndex := FmainIndex+1;
  rg1.OnClick(nil);
  rg2.OnClick(nil);  
end;

procedure TfrmRVListGallery.rg2DblClickItem(Sender: TObject);
begin
  ModalResult := mrOk;
end;

function TfrmRVListGallery.Getrg(PageNo: Integer): TRVOfficeRadioGroup;
begin
  case PageNo of
    0: Result := rg1;
    1: Result := rg2;
    else Result := nil;
  end;
end;

function TfrmRVListGallery.GetRVStyle(PageNo: Integer): TRVStyle;
begin
  case PageNo of
    0: Result := RVStyle1;
    1: Result := RVStyle2;
    else Result := nil;
  end;
end;

function TfrmRVListGallery.GetRVStyleTemplate(PageNo: Integer): TRVStyle;
begin
  case PageNo of
    0: Result := rvsTemplate1;
    1: Result := rvsTemplate2;
    else Result := nil;
  end;
end;

procedure TfrmRVListGallery.btnResetClick(Sender: TObject);
var PageNo, ListNo: Integer;
    bmp: TBitmap;
begin
  PageNo := GetPageControlActivePageIndex(_pc);
  ListNo := GetRG(PageNo).ItemIndex-1;
  if ListNo<0 then
    exit;
  GetRVStyle(PageNo).ListStyles[ListNo] :=
    GetRVStyleTemplate(PageNo).ListStyles[ListNo];
  TrvActionParaList(Action).ModifiedTemplates[PageNo].ListStyles[ListNo] :=
    GetRVStyleTemplate(PageNo).ListStyles[ListNo];
  bmp := TBitmap.Create;
  bmp.Width := il.Width;
  bmp.Height := il.Height;
  bmp.Canvas.Brush.Color := clWindow;
  bmp.Canvas.FillRect(Rect(0,0,bmp.Width,bmp.Height));
  CreateThumbnail(bmp, ListNo, GetRVStyle(PageNo));
  GetRG(PageNo).Images.ReplaceMasked(ListNo+1, bmp, clNone);
  bmp.Free;
  StyleIndices[PageNo][ListNo] := -1;
  GetRG(PageNo).OnClick(nil);
end;

procedure TfrmRVListGallery.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_lg_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  ts1.Caption := RVA_GetSHAsIs(rvam_lg_BulletTab, ControlPanel);
  ts2.Caption := RVA_GetSHAsIs(rvam_lg_NumTab, ControlPanel);
  rg1.Caption := RVA_GetSH(rvam_lg_BulletGB, ControlPanel);
  rg2.Caption := RVA_GetSH(rvam_lg_NumGB, ControlPanel);
  btnEdit1.Caption := RVA_GetSAsIs(rvam_lg_Customize, ControlPanel);
  btnEdit2.Caption := RVA_GetSAsIs(rvam_lg_Customize, ControlPanel);
  btnReset.Caption := RVA_GetSAsIs(rvam_lg_Reset, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVListGallery.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl=_btnEdit1 then
    _btnEdit1 := NewControl
  else if OldControl=_btnEdit2 then
    _btnEdit2 := NewControl
  else if OldControl=_btnReset then
    _btnReset := NewControl
  else if OldControl=_cmbNumbering then
    _cmbNumbering := NewControl
  else if OldControl=btnOk then
    _btnOk := NewControl   
  else if OldControl=_pc then begin
    _pc := NewControl;
    {$IFDEF USERVKSDEVTE}
    _ts1 := TTePageControl(_pc).Pages[0];
    _ts2 := TTePageControl(_pc).Pages[1];
    {$ENDIF}
    {$IFDEF USERVTNT}
    _ts1 := TTntPageControl(_pc).Pages[0];
    _ts2 := TTntPageControl(_pc).Pages[1];
    {$ENDIF}
  end;
end;
{$ENDIF}

procedure TfrmRVListGallery.AdjustIndents(ListStyles: TRVListInfos);
var i, j: Integer;
begin
  for i := 0 to ListStyles.Count-1 do
    with ListStyles[i] do
      for j := 0 to Levels.Count-1 do
        with Levels[j] do begin
          Assert(LeftIndent mod 24 = 0);
          Assert(FirstIndent mod 24 = 0);
          Assert(MarkerIndent mod 24 = 0);
          LeftIndent := Round(LeftIndent*FIndentStep/24);
          FirstIndent := Round(FirstIndent*FIndentStep/24);
          MarkerIndent := Round(MarkerIndent*FIndentStep/24);
        end;
end;

constructor TfrmRVListGallery.CreateEx(AOwner: TComponent; AAction: TAction);
begin
  FIndentStep :=  TrvActionParaList(AACtion).IndentStep;
  Action := AAction;
  inherited Create(AOwner, TrvActionParaList(AACtion).GetControlPanel);
end;

{$ENDIF}

end.
