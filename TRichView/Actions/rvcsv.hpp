﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'rvcsv.pas' rev: 27.00 (Windows)

#ifndef RvcsvHPP
#define RvcsvHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGetText.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvcsv
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE Rvtable::TRVTableItemInfo* __fastcall LoadCSV(const System::UnicodeString FileName, Richview::TCustomRichView* rv, char Separator, bool UseQuotes, int TextStyleNo, int ParaNo);
extern DELPHI_PACKAGE bool __fastcall SaveTableToCSVStreamA(Rvtable::TRVTableItemInfo* table, char Separator, char CellLineBreakDelim, bool EqualDelimCount, System::Classes::TStream* Stream);
extern DELPHI_PACKAGE bool __fastcall SaveTableToCSVFileA(Rvtable::TRVTableItemInfo* table, char Separator, char CellLineBreakDelim, bool EqualDelimCount, const System::UnicodeString FileName);
extern DELPHI_PACKAGE bool __fastcall SaveCurrentTableToCSVStreamA(Rvedit::TCustomRichViewEdit* rve, char Separator, char CellLineBreakDelim, bool EqualDelimCount, System::Classes::TStream* Stream);
extern DELPHI_PACKAGE bool __fastcall SaveCurrentTableToCSVFileA(Rvedit::TCustomRichViewEdit* rve, char Separator, char CellLineBreakDelim, bool EqualDelimCount, const System::UnicodeString FileName);
}	/* namespace Rvcsv */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCSV)
using namespace Rvcsv;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvcsvHPP
