﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVOfficeRadioBtn.pas' rev: 27.00 (Windows)

#ifndef RvofficeradiobtnHPP
#define RvofficeradiobtnHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvofficeradiobtn
{
//-- type declarations -------------------------------------------------------
typedef Vcl::Imglist::TCustomImageList TRVImageList;

enum DECLSPEC_DENUM TORBTextPosition : unsigned char { orbtpBottom, orbtpTop };

class DELPHICLASS TRVOfficeRadioButton;
typedef void __fastcall (__closure *TORBCustomDrawEvent)(TRVOfficeRadioButton* Sender, Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &ARect, bool &DoDefault);

class DELPHICLASS TRVOfficeRadioGroup;
typedef void __fastcall (__closure *TORGCustomDrawEvent)(TRVOfficeRadioGroup* Sender, int ItemIndex, Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &ARect, bool &DoDefault);

class PASCALIMPLEMENTATION TRVOfficeRadioButton : public Vcl::Stdctrls::TRadioButton
{
	typedef Vcl::Stdctrls::TRadioButton inherited;
	
private:
	System::Uitypes::TColor FFillColor;
	int FTextAreaHeight;
	System::Uitypes::TColor FSelColor;
	int FSelWidth;
	bool FDisabled3d;
	System::Uitypes::TImageIndex FImageIndex;
	Vcl::Imglist::TChangeLink* FImageChangeLink;
	TORBTextPosition FTextPosition;
	TORBCustomDrawEvent FOnCustomDraw;
	Vcl::Imglist::TCustomImageList* FImages;
	bool FSquare;
	unsigned FThemeEdit;
	unsigned FThemeRadio;
	bool FUseXPThemes;
	bool FLargeSelection;
	MESSAGE void __fastcall WMThemeChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall CNDrawItem(Winapi::Messages::TWMDrawItem &Msg);
	void __fastcall DrawTo(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &ARect, bool AFocused);
	HIDESBASE MESSAGE void __fastcall CNCommand(Winapi::Messages::TWMCommand &Msg);
	MESSAGE void __fastcall BMSetCheck(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Msg);
	MESSAGE void __fastcall CMDenySubclassing(Winapi::Messages::TMessage &Msg);
	void __fastcall SetFillColor(System::Uitypes::TColor Value);
	void __fastcall SetTextAreaHeight(const int Value);
	void __fastcall SetSelColor(const System::Uitypes::TColor Value);
	void __fastcall SetSelWidth(const int Value);
	void __fastcall SetDisabled3d(const bool Value);
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* const Value);
	void __fastcall SetImageIndex(const System::Uitypes::TImageIndex Value);
	void __fastcall ImageListChange(System::TObject* Sender);
	void __fastcall SetTextPosition(TORBTextPosition Value);
	bool __fastcall DoCustomDraw(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall SetSquare(const bool Value);
	void __fastcall CreateThemeHandle(void);
	void __fastcall FreeThemeHandle(void);
	void __fastcall SetUseXPThemes(const bool Value);
	void __fastcall SetLargeSelection(const bool Value);
	
protected:
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall CreateWnd(void);
	
public:
	__fastcall virtual TRVOfficeRadioButton(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVOfficeRadioButton(void);
	
__published:
	__property System::Uitypes::TColor FillColor = {read=FFillColor, write=SetFillColor, default=-16777211};
	__property System::Uitypes::TColor SelColor = {read=FSelColor, write=SetSelColor, default=-16777203};
	__property int SelWidth = {read=FSelWidth, write=SetSelWidth, default=2};
	__property int TextAreaHeight = {read=FTextAreaHeight, write=SetTextAreaHeight, default=30};
	__property bool Disabled3D = {read=FDisabled3d, write=SetDisabled3d, default=1};
	__property Vcl::Imglist::TCustomImageList* Images = {read=FImages, write=SetImages};
	__property System::Uitypes::TImageIndex ImageIndex = {read=FImageIndex, write=SetImageIndex, default=-1};
	__property bool Square = {read=FSquare, write=SetSquare, default=0};
	__property bool LargeSelection = {read=FLargeSelection, write=SetLargeSelection, default=0};
	__property TORBTextPosition TextPosition = {read=FTextPosition, write=SetTextPosition, default=0};
	__property TORBCustomDrawEvent OnCustomDraw = {read=FOnCustomDraw, write=FOnCustomDraw};
	__property bool UseXPThemes = {read=FUseXPThemes, write=SetUseXPThemes, default=1};
	__property TabStop = {default=0};
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVOfficeRadioButton(HWND ParentWindow) : Vcl::Stdctrls::TRadioButton(ParentWindow) { }
	
};


class DELPHICLASS TOfficeGroupItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOfficeGroupItem : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	bool FEnabled;
	System::UnicodeString FHint;
	System::Uitypes::TColor FFillColor;
	System::Classes::THelpContext FHelpContext;
	System::Classes::THelpType FHelpType;
	System::UnicodeString FHelpKeyword;
	System::Uitypes::TImageIndex FImageIndex;
	Vcl::Controls::TCaption FCaption;
	void __fastcall SetEnabled(bool Value);
	void __fastcall SetFillColor(System::Uitypes::TColor Value);
	void __fastcall SetHelpContext(System::Classes::THelpContext Value);
	void __fastcall SetHelpKeyword(System::UnicodeString Value);
	void __fastcall SetHelpType(System::Classes::THelpType Value);
	void __fastcall SetHint(System::UnicodeString Value);
	void __fastcall SetImageIndex(System::Uitypes::TImageIndex Value);
	void __fastcall SetCaption(const Vcl::Controls::TCaption Value);
	
public:
	__fastcall virtual TOfficeGroupItem(System::Classes::TCollection* Collection);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	
__published:
	__property System::Uitypes::TColor FillColor = {read=FFillColor, write=SetFillColor, default=-16777211};
	__property System::Uitypes::TImageIndex ImageIndex = {read=FImageIndex, write=SetImageIndex, default=-1};
	__property bool Enabled = {read=FEnabled, write=SetEnabled, default=1};
	__property System::UnicodeString Hint = {read=FHint, write=SetHint};
	__property System::Classes::THelpContext HelpContext = {read=FHelpContext, write=SetHelpContext, default=0};
	__property System::UnicodeString HelpKeyword = {read=FHelpKeyword, write=SetHelpKeyword};
	__property System::Classes::THelpType HelpType = {read=FHelpType, write=SetHelpType, default=1};
	__property Vcl::Controls::TCaption Caption = {read=FCaption, write=SetCaption, stored=true};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TOfficeGroupItem(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TOfficeGroupItems;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOfficeGroupItems : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TOfficeGroupItem* operator[](int Index) { return Items[Index]; }
	
private:
	TRVOfficeRadioGroup* FRadioGroup;
	HIDESBASE TOfficeGroupItem* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TOfficeGroupItem* Value);
	
protected:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual void __fastcall Update(System::Classes::TCollectionItem* Item);
	
public:
	__fastcall TOfficeGroupItems(TRVOfficeRadioGroup* RadioGroup);
	HIDESBASE TOfficeGroupItem* __fastcall Add(void);
	HIDESBASE TOfficeGroupItem* __fastcall Insert(int Index);
	__property TOfficeGroupItem* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TOfficeGroupItems(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TRVOfficeRadioGroup : public Vcl::Stdctrls::TCustomGroupBox
{
	typedef Vcl::Stdctrls::TCustomGroupBox inherited;
	
private:
	System::Classes::TList* FButtons;
	TOfficeGroupItems* FItems;
	int FItemIndex;
	int FColumns;
	bool FReading;
	bool FUpdating;
	int FMargin;
	Vcl::Imglist::TCustomImageList* FImages;
	bool FDisabled3d;
	int FSelWidth;
	int FTextAreaHeight;
	System::Uitypes::TColor FSelColor;
	TORBTextPosition FTextPosition;
	TORGCustomDrawEvent FOnCustomDraw;
	System::Classes::TNotifyEvent FOnDblClickItem;
	bool FSquare;
	bool FUseXPThemes;
	unsigned FTheme;
	void __fastcall ArrangeButtons(void);
	void __fastcall ButtonClick(System::TObject* Sender);
	void __fastcall SetItems(TOfficeGroupItems* Value);
	void __fastcall UpdateButtons(void);
	void __fastcall UpdateButtonsGlbl(bool Repaint);
	void __fastcall UpdateButton(int Index);
	HIDESBASE MESSAGE void __fastcall CMEnabledChanged(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall CMFontChanged(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall WMSize(Winapi::Messages::TWMSize &Msg);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Msg);
	HIDESBASE MESSAGE void __fastcall WMPaint(Winapi::Messages::TWMPaint &Msg);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMThemeChanged(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Winapi::Messages::TMessage &Msg);
	MESSAGE void __fastcall CMDenySubclassing(Winapi::Messages::TMessage &Msg);
	void __fastcall SetMargin(int Value);
	void __fastcall SetButtonCount(int Value);
	void __fastcall SetColumns(int Value);
	void __fastcall SetItemIndex(int Value);
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* Value);
	void __fastcall SetDisabled3d(bool Value);
	void __fastcall SetSelColor(System::Uitypes::TColor Value);
	void __fastcall SetSelWidth(int Value);
	void __fastcall SetTextAreaHeight(int Value);
	void __fastcall SetTextPosition(TORBTextPosition Value);
	void __fastcall DoCustomDraw(TRVOfficeRadioButton* Sender, Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &ARect, bool &DoDefault);
	void __fastcall SetCustomDraw(TORGCustomDrawEvent Value);
	int __fastcall GetAnother(int Index, System::Word Key);
	void __fastcall SetSquare(const bool Value);
	void __fastcall SetUseXPThemes(const bool Value);
	void __fastcall PerformEraseBackground(HDC DC, bool FullPainting);
	void __fastcall CreateThemeHandle(void);
	void __fastcall FreeThemeHandle(void);
	int __fastcall GetThemeState(void);
	System::Types::TRect __fastcall GetCaptionRect(NativeUInt DC);
	
protected:
	virtual void __fastcall Loaded(void);
	virtual void __fastcall ReadState(System::Classes::TReader* Reader);
	virtual bool __fastcall CanModify(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall CreateWnd(void);
	virtual void __fastcall Paint(void);
	
public:
	__fastcall virtual TRVOfficeRadioGroup(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVOfficeRadioGroup(void);
	DYNAMIC void __fastcall FlipChildren(bool AllLevels);
	DYNAMIC void __fastcall GetChildren(System::Classes::TGetChildProc Proc, System::Classes::TComponent* Root);
	
__published:
	__property int ItemIndex = {read=FItemIndex, write=SetItemIndex, default=-1};
	__property int Columns = {read=FColumns, write=SetColumns, default=1};
	__property TOfficeGroupItems* Items = {read=FItems, write=SetItems};
	__property int Margin = {read=FMargin, write=SetMargin, default=10};
	__property Vcl::Imglist::TCustomImageList* Images = {read=FImages, write=SetImages};
	__property System::Uitypes::TColor SelColor = {read=FSelColor, write=SetSelColor, default=-16777203};
	__property int SelWidth = {read=FSelWidth, write=SetSelWidth, default=2};
	__property int TextAreaHeight = {read=FTextAreaHeight, write=SetTextAreaHeight, default=30};
	__property bool Disabled3D = {read=FDisabled3d, write=SetDisabled3d, default=1};
	__property TORBTextPosition TextPosition = {read=FTextPosition, write=SetTextPosition, default=0};
	__property bool Square = {read=FSquare, write=SetSquare, default=0};
	__property TORGCustomDrawEvent OnCustomDraw = {read=FOnCustomDraw, write=SetCustomDraw};
	__property System::Classes::TNotifyEvent OnDblClickItem = {read=FOnDblClickItem, write=FOnDblClickItem};
	__property bool UseXPThemes = {read=FUseXPThemes, write=SetUseXPThemes, default=1};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property DragKind = {default=0};
	__property Constraints;
	__property ParentBiDiMode = {default=1};
	__property OnEndDock;
	__property OnStartDock;
	__property Caption = {default=0};
	__property Color = {default=-16777211};
	__property Ctl3D;
	__property DragCursor = {default=-12};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ParentColor = {default=1};
	__property ParentCtl3D = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnStartDrag;
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVOfficeRadioGroup(HWND ParentWindow) : Vcl::Stdctrls::TCustomGroupBox(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word CM_DENYSUBCLASSING = System::Word(0xce3);
}	/* namespace Rvofficeradiobtn */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVOFFICERADIOBTN)
using namespace Rvofficeradiobtn;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvofficeradiobtnHPP
