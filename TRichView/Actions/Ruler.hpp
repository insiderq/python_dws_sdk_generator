﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Ruler.pas' rev: 27.00 (Windows)

#ifndef RulerHPP
#define RulerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Ruler
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRulerType : unsigned char { rtHorizontal, rtVertical };

enum DECLSPEC_DENUM TTabAlign : unsigned char { taLeftAlign, taCenterAlign, taRightAlign, taDecimalAlign, taWordBarAlign };

enum DECLSPEC_DENUM TIndentGraphic : unsigned char { igNone, igUp, igDown, igRectangle, igLevelDec1, igLevelDec2, igLevelInc1, igLevelInc2 };

enum DECLSPEC_DENUM TIndentType : unsigned char { itBoth, itFirst, itLeft, itRight };

enum DECLSPEC_DENUM TLevelType : unsigned char { ltLevelDec, ltLevelInc };

enum DECLSPEC_DENUM TMarginType : unsigned char { mtTop, mtBottom, mtLeft, mtRight };

enum DECLSPEC_DENUM TBiDiModeRuler : unsigned char { bmUseBiDiMode, bmLeftToRight, bmRightToLeft };

enum DECLSPEC_DENUM TRulerUnits : unsigned char { ruInches, ruCentimeters, ruMillimeters, ruPicas, ruPixels, ruPoints };

enum DECLSPEC_DENUM THitItem : unsigned char { hiNone, hiOnTab, hiOnBothIndent, hiOnFirstIndent, hiOnLeftIndent, hiOnRightIndent, hiOnTopMargin, hiOnBottomMargin, hiOnLeftMargin, hiOnRightMargin, hiOnTopMarginGrip, hiOnBottomMarginGrip, hiOnLeftMarginGrip, hiOnRightMarginGrip, hiOnColumn, hiOnRow, hiOnTopBorder, hiOnBottomBorder, hiOnLeftBorder, hiOnRightBorder, hiOnLevelDec, hiOnLevelInc };

typedef System::Set<THitItem, THitItem::hiNone, THitItem::hiOnLevelInc> THitItems;

enum DECLSPEC_DENUM TDragItem : unsigned char { diNone, diBorder, diColumn, diIndent, diMargin, diMarginGrip, diRow, diTab };

enum DECLSPEC_DENUM TTableGraphic : unsigned char { tgNone, tgBorder, tgColumn, tgRow };

enum DECLSPEC_DENUM TBorderType : unsigned char { btTop, btBottom, btLeft, btRight };

enum DECLSPEC_DENUM TTablePosition : unsigned char { tpAbsolute, tpFromFirstIndent, tpFromLeftIndent, tpFromMargin };

enum DECLSPEC_DENUM TIndentOption : unsigned char { ioExtraShadow, ioKeepWithinMargins, ioShowFirstIndent, ioShowHangingIndent, ioShowLeftIndent, ioShowRightIndent, ioSnapToRuler };

typedef System::Set<TIndentOption, TIndentOption::ioExtraShadow, TIndentOption::ioSnapToRuler> TIndentOptions;

enum DECLSPEC_DENUM TLevelGraphic : unsigned char { lgType1, lgType2 };

enum DECLSPEC_DENUM TListEditorOption : unsigned char { leoAdjustable, leoLevelAdjustable };

typedef System::Set<TListEditorOption, TListEditorOption::leoAdjustable, TListEditorOption::leoLevelAdjustable> TListEditorOptions;

enum DECLSPEC_DENUM TMarginOption : unsigned char { moAdjustable, moSnapToRuler };

typedef System::Set<TMarginOption, TMarginOption::moAdjustable, TMarginOption::moSnapToRuler> TMarginOptions;

enum DECLSPEC_DENUM TRulerItem : unsigned char { riNone, riTabLeftAlign, riTabCenterAlign, riTabRightAlign, riTabDecimalAlign, riTabWordBarAlign };

typedef System::Set<TRulerItem, TRulerItem::riNone, TRulerItem::riTabWordBarAlign> TRulerItems;

enum DECLSPEC_DENUM TRulerOption : unsigned char { roAutoUpdatePrinterWidth, roItemsShowLastPos, roUseDefaultPrinterWidth, roScaleRelativeToMargin, roClipScaleAtPageSize, roClipTableAtPageSize, roSuppressScaleAtMarginGrips };

typedef System::Set<TRulerOption, TRulerOption::roAutoUpdatePrinterWidth, TRulerOption::roSuppressScaleAtMarginGrips> TRulerOptions;

enum DECLSPEC_DENUM TRulerTextItem : unsigned char { rtiHintColumnMove, rtiHintIndentFirst, rtiHintIndentLeft, rtiHintIndentHanging, rtiHintIndentRight, rtiHintLevelDec, rtiHintLevelInc, rtiHintMarginBottom, rtiHintMarginLeft, rtiHintMarginRight, rtiHintMarginTop, rtiHintRowMove, rtiHintTabCenter, rtiHintTabDecimal, rtiHintTabLeft, rtiHintTabRight, rtiHintTabWordBar, rtiMenuTabCenter, rtiMenuTabDecimal, rtiMenuTabLeft, rtiMenuTabRight, rtiMenuTabWordBar };

enum DECLSPEC_DENUM TAddTabOnLeftClick : unsigned char { taDisabled, taEnabled, taOnlyBetweenMargins };

enum DECLSPEC_DENUM TTabOption : unsigned char { toAdvancedTabs, toShowDefaultTabs, toSnapToRuler, toDontShowDefaultTabsBeforeLeftIndent };

typedef System::Set<TTabOption, TTabOption::toAdvancedTabs, TTabOption::toDontShowDefaultTabsBeforeLeftIndent> TTabOptions;

enum DECLSPEC_DENUM TTableEditorOption : unsigned char { teoAdjustable, teoSnapToRuler };

typedef System::Set<TTableEditorOption, TTableEditorOption::teoAdjustable, TTableEditorOption::teoSnapToRuler> TTableEditorOptions;

enum DECLSPEC_DENUM TTableEditorVisible : unsigned char { tevNever, tevOnlyWhenActive };

typedef System::Word TZoomRange;

typedef System::Extended TRulerFloat;

struct DECLSPEC_DRECORD TDragInfo
{
public:
	TDragItem Item;
	TDragItem DblItem;
	int Index;
	int Offset;
};


struct DECLSPEC_DRECORD THitInfo
{
public:
	int Index;
	THitItems HitItems;
};


struct DECLSPEC_DRECORD TIndent
{
public:
	TIndentGraphic Graphic;
	int Left;
	System::Extended Position;
	int Top;
};


struct DECLSPEC_DRECORD TMargin
{
public:
	int Grip;
	System::Extended Position;
};


struct DECLSPEC_DRECORD TTableBorder
{
public:
	int Left;
	int Top;
	System::Extended Position;
};


typedef System::StaticArray<TIndent, 4> TIndentArray;

enum DECLSPEC_DENUM TSkinType : unsigned char { stNoSkin, stSkin1, stSkin2 };

typedef void __fastcall (__closure *TBorderClickEvent)(System::TObject* Sender, TBorderType Border);

typedef void __fastcall (__closure *TColumnClickEvent)(System::TObject* Sender, int Column);

typedef void __fastcall (__closure *TIndentClickEvent)(System::TObject* Sender, TIndentType Indent);

typedef void __fastcall (__closure *TMarginClickEvent)(System::TObject* Sender, TMarginType Margin);

typedef void __fastcall (__closure *TRowClickEvent)(System::TObject* Sender, int Row);

typedef void __fastcall (__closure *TRulerCustomDrawEvent)(System::TObject* Sender, Vcl::Graphics::TCanvas* const ACanvas, const System::Types::TRect &ARect, bool &DefaultDraw);

typedef void __fastcall (__closure *TRulerItemClickEvent)(System::TObject* Sender, int X);

typedef void __fastcall (__closure *TRulerItemMoveEvent)(System::TObject* Sender, int X, bool Removing);

typedef void __fastcall (__closure *TRulerTextChangedEvent)(System::TObject* Sender, TRulerTextItem Item);

typedef void __fastcall (__closure *TTabClickEvent)(System::TObject* Sender, int Tab);

typedef Vcl::Graphics::TFont TScaleFont;

class DELPHICLASS TTab;
class PASCALIMPLEMENTATION TTab : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	TTabAlign FAlign;
	System::Uitypes::TColor FColor;
	bool FDeleting;
	int FLeft;
	System::Extended FPosition;
	int FTop;
	System::UnicodeString FLeader;
	System::Extended __fastcall GetPixels(void);
	System::Extended __fastcall GetPoints(void);
	TTabAlign __fastcall GetRTLAlign(void);
	void __fastcall SetAlign(const TTabAlign Value);
	void __fastcall SetColor(const System::Uitypes::TColor Value);
	void __fastcall SetPosition(const System::Extended Value);
	void __fastcall SetPixels(const System::Extended Value);
	void __fastcall SetPoints(const System::Extended Value);
	
protected:
	__property System::Extended Pixels = {read=GetPixels, write=SetPixels};
	__property TTabAlign RTLAlign = {read=GetRTLAlign, nodefault};
	
public:
	__fastcall virtual TTab(System::Classes::TCollection* Collection);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property int Left = {read=FLeft, write=FLeft, nodefault};
	__property System::Extended Points = {read=GetPoints, write=SetPoints};
	__property int Top = {read=FTop, write=FTop, nodefault};
	
__published:
	__property TTabAlign Align = {read=FAlign, write=SetAlign, nodefault};
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, nodefault};
	__property System::Extended Position = {read=FPosition, write=SetPosition};
	__property System::UnicodeString Leader = {read=FLeader, write=FLeader};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TTab(void) { }
	
};


class DELPHICLASS TTabs;
class DELPHICLASS TCustomRuler;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTabs : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TTab* operator[](int Index) { return Items[Index]; }
	
private:
	bool FBlockDoTabChanged;
	TCustomRuler* FRuler;
	TTab* __fastcall GetTab(int Index);
	void __fastcall SetTab(int Index, TTab* const Value);
	
protected:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	void __fastcall SortTabs(void);
	virtual void __fastcall Update(System::Classes::TCollectionItem* Item);
	
public:
	__fastcall TTabs(TCustomRuler* Ruler);
	HIDESBASE TTab* __fastcall Add(void);
	__property TTab* Items[int Index] = {read=GetTab, write=SetTab/*, default*/};
	__property TCustomRuler* Ruler = {read=FRuler};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TTabs(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRulerCell;
class PASCALIMPLEMENTATION TRulerCell : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	System::Extended FCellWidth;
	System::Extended FDragBoundary;
	System::Extended FFirstIndent;
	int FLeft;
	System::Extended FLeftIndent;
	System::Extended FRightIndent;
	int __fastcall GetDragLimit(void);
	System::Extended __fastcall GetPosition(void);
	void __fastcall SetCellWidth(const System::Extended Value);
	void __fastcall SetDragBoundary(const System::Extended Value);
	void __fastcall SetLeft(const int Value);
	void __fastcall SetPosition(const System::Extended Value);
	
protected:
	__property int DragLimit = {read=GetDragLimit, nodefault};
	__property System::Extended FirstIndent = {read=FFirstIndent, write=FFirstIndent};
	__property int Left = {read=FLeft, write=SetLeft, nodefault};
	__property System::Extended LeftIndent = {read=FLeftIndent, write=FLeftIndent};
	__property System::Extended Position = {read=GetPosition, write=SetPosition};
	__property System::Extended RightIndent = {read=FRightIndent, write=FRightIndent};
	
public:
	__fastcall virtual TRulerCell(System::Classes::TCollection* Collection);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property System::Extended DragBoundary = {read=FDragBoundary, write=SetDragBoundary};
	
__published:
	__property System::Extended CellWidth = {read=FCellWidth, write=SetCellWidth};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TRulerCell(void) { }
	
};


class DELPHICLASS TRulerCells;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRulerCells : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TRulerCell* operator[](int Index) { return Items[Index]; }
	
private:
	TCustomRuler* FRuler;
	TRulerCell* __fastcall GetCell(int Index);
	void __fastcall SetCell(int Index, TRulerCell* const Value);
	
protected:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual void __fastcall Update(System::Classes::TCollectionItem* Item);
	
public:
	__fastcall TRulerCells(TCustomRuler* AOwner);
	HIDESBASE TRulerCell* __fastcall Add(void);
	__property TRulerCell* Items[int Index] = {read=GetCell, write=SetCell/*, default*/};
	__property TCustomRuler* Ruler = {read=FRuler};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TRulerCells(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRulerRow;
class PASCALIMPLEMENTATION TRulerRow : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	System::Extended FRowHeight;
	int FTop;
	System::Extended __fastcall GetPosition(void);
	void __fastcall SetRowHeight(const System::Extended Value);
	void __fastcall SetTop(const int Value);
	void __fastcall SetPosition(const System::Extended Value);
	
protected:
	__property int Top = {read=FTop, write=SetTop, nodefault};
	__property System::Extended Position = {read=GetPosition, write=SetPosition};
	
public:
	__fastcall virtual TRulerRow(System::Classes::TCollection* Collection);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
__published:
	__property System::Extended RowHeight = {read=FRowHeight, write=SetRowHeight};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TRulerRow(void) { }
	
};


class DELPHICLASS TRulerRows;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRulerRows : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TRulerRow* operator[](int Index) { return Items[Index]; }
	
private:
	TCustomRuler* FRuler;
	TRulerRow* __fastcall GetRow(int Index);
	void __fastcall SetRow(int Index, TRulerRow* const Value);
	
protected:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual void __fastcall Update(System::Classes::TCollectionItem* Item);
	
public:
	__fastcall TRulerRows(TCustomRuler* AOwner);
	HIDESBASE TRulerRow* __fastcall Add(void);
	__property TRulerRow* Items[int Index] = {read=GetRow, write=SetRow/*, default*/};
	__property TCustomRuler* Ruler = {read=FRuler};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TRulerRows(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRulerListEditor;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRulerListEditor : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	bool FActive;
	TLevelGraphic FLevelGraphic;
	int FListLevel;
	TListEditorOptions FOptions;
	TCustomRuler* FRuler;
	void __fastcall SetOptions(const TListEditorOptions Value);
	void __fastcall SetActive(const bool Value);
	void __fastcall SetLevelGraphic(const TLevelGraphic Value);
	
protected:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	void __fastcall Invalidate(void);
	
public:
	__fastcall virtual TRulerListEditor(TCustomRuler* AOwner);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property bool Active = {read=FActive, write=SetActive, nodefault};
	__property int ListLevel = {read=FListLevel, write=FListLevel, nodefault};
	__property TCustomRuler* Ruler = {read=FRuler};
	
__published:
	__property TLevelGraphic LevelGraphic = {read=FLevelGraphic, write=SetLevelGraphic, default=0};
	__property TListEditorOptions Options = {read=FOptions, write=SetOptions, default=3};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRulerListEditor(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRulerTableEditor;
class PASCALIMPLEMENTATION TRulerTableEditor : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	bool FActive;
	System::Uitypes::TColor FBackGroundColor;
	System::StaticArray<TTableBorder, 4> FBorders;
	System::Extended FBorderHSpacing;
	System::Extended FBorderVSpacing;
	System::Extended FBorderWidth;
	System::Extended FCellBorderWidth;
	System::Extended FCellHPadding;
	System::Extended FCellVPadding;
	System::Extended FCellHSpacing;
	System::Extended FCellVSpacing;
	int FCellIndex;
	System::Uitypes::TCursor FDragCursor;
	int FDraggedColumn;
	System::Extended FDraggedDelta;
	int FDraggedRow;
	bool FDraggedWithShift;
	int FDragLast;
	int FDragStart;
	System::Extended FFirstIndent;
	System::Uitypes::TColor FForeGroundColor;
	System::Extended FLeftIndent;
	TTableEditorOptions FOptions;
	int FPaintLimitBottom;
	int FPaintLimitTop;
	System::Extended FRightIndent;
	int FRowIndex;
	TCustomRuler* FRuler;
	TRulerCells* FRulerCells;
	TIndentArray FRulerIndents;
	TRulerRows* FRulerRows;
	int FTableOffset;
	TTablePosition FTablePosition;
	bool FUseDragBoundaries;
	TTableEditorVisible FVisible;
	System::Types::TRect __fastcall GetBorderRect(const TBorderType BorderType);
	System::Types::TRect __fastcall GetCellRect(const int Index);
	int __fastcall GetColumnIndexAt(const int X, const int Y);
	int __fastcall GetNextValidCell(const int Index);
	int __fastcall GetOffset(void);
	int __fastcall GetPrevValidCell(const int Index);
	int __fastcall GetPrevValidRow(const int Index);
	int __fastcall GetRowIndexAt(const int X, const int Y);
	System::Extended __fastcall GetTableHeight(void);
	System::Extended __fastcall GetTableLeft(void);
	System::Extended __fastcall GetTableTop(void);
	System::Extended __fastcall GetTableWidth(void);
	int __fastcall GetTotalCellSpacing(const bool FromBorder, const bool FromLeft);
	int __fastcall GetTotalRowSpacing(const bool FromBorder, const bool FromTop);
	int __fastcall KeepColumnsSeparated(const int Index, const int Left);
	int __fastcall KeepRowsSeparated(const int Index, const int Top);
	int __fastcall KeepWithinCurrentCell(const TIndentType IT, const int X);
	int __fastcall LastValidCellIndex(void);
	int __fastcall LastValidRowIndex(void);
	int __fastcall RTLAdjust(int X, int Offset);
	void __fastcall SetActive(const bool Value);
	void __fastcall SetBackGroundColor(const System::Uitypes::TColor Value);
	void __fastcall SetBorderHSpacing(const System::Extended Value);
	void __fastcall SetBorderVSpacing(const System::Extended Value);
	void __fastcall SetBorderWidth(const System::Extended Value);
	void __fastcall SetCellBorderWidth(const System::Extended Value);
	void __fastcall SetCellHSpacing(const System::Extended Value);
	void __fastcall SetCellVSpacing(const System::Extended Value);
	void __fastcall SetCellIndex(const int Value);
	void __fastcall SetCellPading(const System::Extended Value);
	void __fastcall SetCellHPadding(const System::Extended Value);
	void __fastcall SetCellVPadding(const System::Extended Value);
	void __fastcall SetDragCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetFirstIndent(const System::Extended Value);
	void __fastcall SetForeGroundColor(const System::Uitypes::TColor Value);
	void __fastcall SetLeftIndent(const System::Extended Value);
	void __fastcall SetOptions(const TTableEditorOptions Value);
	void __fastcall SetPaintLimitBottom(const int Value);
	void __fastcall SetPaintLimitTop(const int Value);
	void __fastcall SetRightIndent(const System::Extended Value);
	void __fastcall SetRowIndex(const int Value);
	void __fastcall SetRulerCells(TRulerCells* const Value);
	void __fastcall SetRulerRows(TRulerRows* const Value);
	void __fastcall SetTableHeight(const System::Extended Value);
	void __fastcall SetTableLeft(const System::Extended Value);
	void __fastcall SetTableOffset(const int Value);
	void __fastcall SetTablePosition(const TTablePosition Value);
	void __fastcall SetTableTop(const System::Extended Value);
	void __fastcall SetTableWidth(const System::Extended Value);
	void __fastcall SetVisible(const TTableEditorVisible Value);
	void __fastcall UpdateIndentPosition(const TIndentType IT, const int XPos);
	
protected:
	int __fastcall CalculateCurrentIndentPosition(const TIndentType IT);
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	void __fastcall Invalidate(void);
	__property int Offset = {read=GetOffset, nodefault};
	
public:
	__fastcall virtual TRulerTableEditor(TCustomRuler* AOwner);
	__fastcall virtual ~TRulerTableEditor(void);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property System::Extended CellPadding = {write=SetCellPading};
	__property int DraggedColumn = {read=FDraggedColumn, nodefault};
	__property System::Extended DraggedDelta = {read=FDraggedDelta};
	__property int DraggedRow = {read=FDraggedRow, nodefault};
	__property bool DraggedWithShift = {read=FDraggedWithShift, nodefault};
	__property int PaintLimitBottom = {read=FPaintLimitBottom, write=SetPaintLimitBottom, nodefault};
	__property int PaintLimitTop = {read=FPaintLimitTop, write=SetPaintLimitTop, nodefault};
	__property TCustomRuler* Ruler = {read=FRuler};
	__property bool UseDragBoundaries = {read=FUseDragBoundaries, write=FUseDragBoundaries, nodefault};
	
__published:
	__property bool Active = {read=FActive, write=SetActive, nodefault};
	__property System::Uitypes::TColor BackGroundColor = {read=FBackGroundColor, write=SetBackGroundColor, default=-16777201};
	__property System::Extended BorderHSpacing = {read=FBorderHSpacing, write=SetBorderHSpacing};
	__property System::Extended BorderVSpacing = {read=FBorderVSpacing, write=SetBorderVSpacing};
	__property System::Extended BorderWidth = {read=FBorderWidth, write=SetBorderWidth};
	__property System::Extended CellBorderWidth = {read=FCellBorderWidth, write=SetCellBorderWidth};
	__property System::Extended CellHSpacing = {read=FCellHSpacing, write=SetCellHSpacing};
	__property System::Extended CellVSpacing = {read=FCellVSpacing, write=SetCellVSpacing};
	__property int CellIndex = {read=FCellIndex, write=SetCellIndex, nodefault};
	__property System::Extended CellHPadding = {read=FCellHPadding, write=SetCellHPadding};
	__property System::Extended CellVPadding = {read=FCellVPadding, write=SetCellVPadding};
	__property TRulerCells* Cells = {read=FRulerCells, write=SetRulerCells};
	__property System::Uitypes::TCursor DragCursor = {read=FDragCursor, write=SetDragCursor, default=-14};
	__property System::Extended FirstIndent = {read=FFirstIndent, write=SetFirstIndent};
	__property System::Uitypes::TColor ForeGroundColor = {read=FForeGroundColor, write=SetForeGroundColor, default=-16777200};
	__property System::Extended LeftIndent = {read=FLeftIndent, write=SetLeftIndent};
	__property TTableEditorOptions Options = {read=FOptions, write=SetOptions, default=1};
	__property System::Extended RightIndent = {read=FRightIndent, write=SetRightIndent};
	__property int RowIndex = {read=FRowIndex, write=SetRowIndex, nodefault};
	__property TRulerRows* Rows = {read=FRulerRows, write=SetRulerRows};
	__property System::Extended TableLeft = {read=GetTableLeft, write=SetTableLeft};
	__property System::Extended TableHeight = {read=GetTableHeight, write=SetTableHeight};
	__property int TableOffset = {read=FTableOffset, write=SetTableOffset, nodefault};
	__property TTablePosition TablePosition = {read=FTablePosition, write=SetTablePosition, default=3};
	__property System::Extended TableTop = {read=GetTableTop, write=SetTableTop};
	__property System::Extended TableWidth = {read=GetTableWidth, write=SetTableWidth};
	__property TTableEditorVisible Visible = {read=FVisible, write=SetVisible, default=1};
};


class DELPHICLASS TIndentSettings;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TIndentSettings : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Uitypes::TCursor FDragCursor;
	TIndentOptions FOptions;
	TCustomRuler* FRuler;
	void __fastcall SetDragCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetOptions(const TIndentOptions Value);
	
public:
	__fastcall virtual TIndentSettings(TCustomRuler* AOwner);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property TCustomRuler* Ruler = {read=FRuler};
	
__published:
	__property System::Uitypes::TCursor DragCursor = {read=FDragCursor, write=SetDragCursor, default=-12};
	__property TIndentOptions Options = {read=FOptions, write=SetOptions, default=62};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TIndentSettings(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TMarginSettings;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TMarginSettings : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Uitypes::TCursor FDragCursor;
	System::Uitypes::TColor FGripColor;
	int FGripSize;
	TMarginOptions FOptions;
	TCustomRuler* FRuler;
	void __fastcall SetDragCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetGripColor(const System::Uitypes::TColor Value);
	void __fastcall SetGripSize(const int Value);
	void __fastcall SetOptions(const TMarginOptions Value);
	
public:
	__fastcall virtual TMarginSettings(TCustomRuler* AOwner);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property TCustomRuler* Ruler = {read=FRuler};
	
__published:
	__property System::Uitypes::TCursor DragCursor = {read=FDragCursor, write=SetDragCursor, default=-9};
	__property System::Uitypes::TColor GripColor = {read=FGripColor, write=SetGripColor, default=-16777200};
	__property int GripSize = {read=FGripSize, write=SetGripSize, default=5};
	__property TMarginOptions Options = {read=FOptions, write=SetOptions, default=1};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TMarginSettings(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TTabSettings;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTabSettings : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TAddTabOnLeftClick FAddTabOnLeftClick;
	TTabAlign FDefaultTabAlign;
	System::Uitypes::TCursor FDeleteCursor;
	System::Uitypes::TCursor FDragCursor;
	TTabOptions FOptions;
	TCustomRuler* FRuler;
	void __fastcall SetDeleteCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetDragCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetOptions(const TTabOptions Value);
	
public:
	__fastcall virtual TTabSettings(TCustomRuler* AOwner);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property TCustomRuler* Ruler = {read=FRuler};
	
__published:
	__property TAddTabOnLeftClick AddTabOnLeftClick = {read=FAddTabOnLeftClick, write=FAddTabOnLeftClick, default=2};
	__property TTabAlign DefaultTabAlign = {read=FDefaultTabAlign, write=FDefaultTabAlign, default=0};
	__property System::Uitypes::TCursor DeleteCursor = {read=FDeleteCursor, write=SetDeleteCursor, default=-1};
	__property System::Uitypes::TCursor DragCursor = {read=FDragCursor, write=SetDragCursor, default=-12};
	__property TTabOptions Options = {read=FOptions, write=SetOptions, default=10};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TTabSettings(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRulerTexts;
class PASCALIMPLEMENTATION TRulerTexts : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	typedef System::StaticArray<System::UnicodeString, 22> _TRulerTexts__1;
	
	
private:
	TRulerTextChangedEvent FOnRulerTextChanged;
	TCustomRuler* FRuler;
	_TRulerTexts__1 FStrings;
	System::UnicodeString __fastcall GetString(int Index);
	void __fastcall SetString(int Index, const System::UnicodeString S);
	
protected:
	DYNAMIC void __fastcall DoRulerTextChanged(TRulerTextItem Item);
	
public:
	__fastcall virtual TRulerTexts(TCustomRuler* AOwner);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__property TRulerTextChangedEvent OnRulerTextChanged = {read=FOnRulerTextChanged, write=FOnRulerTextChanged};
	
__published:
	__property System::UnicodeString HintColumnMove = {read=GetString, write=SetString, index=0};
	__property System::UnicodeString HintIndentFirst = {read=GetString, write=SetString, index=1};
	__property System::UnicodeString HintIndentLeft = {read=GetString, write=SetString, index=2};
	__property System::UnicodeString HintIndentHanging = {read=GetString, write=SetString, index=3};
	__property System::UnicodeString HintIndentRight = {read=GetString, write=SetString, index=4};
	__property System::UnicodeString HintLevelDec = {read=GetString, write=SetString, index=5};
	__property System::UnicodeString HintLevelInc = {read=GetString, write=SetString, index=6};
	__property System::UnicodeString HintMarginBottom = {read=GetString, write=SetString, index=7};
	__property System::UnicodeString HintMarginLeft = {read=GetString, write=SetString, index=8};
	__property System::UnicodeString HintMarginRight = {read=GetString, write=SetString, index=9};
	__property System::UnicodeString HintMarginTop = {read=GetString, write=SetString, index=10};
	__property System::UnicodeString HintRowMove = {read=GetString, write=SetString, index=11};
	__property System::UnicodeString HintTabCenter = {read=GetString, write=SetString, index=12};
	__property System::UnicodeString HintTabDecimal = {read=GetString, write=SetString, index=13};
	__property System::UnicodeString HintTabLeft = {read=GetString, write=SetString, index=14};
	__property System::UnicodeString HintTabRight = {read=GetString, write=SetString, index=15};
	__property System::UnicodeString HintTabWordBar = {read=GetString, write=SetString, index=16};
	__property System::UnicodeString MenuTabCenter = {read=GetString, write=SetString, index=17};
	__property System::UnicodeString MenuTabDecimal = {read=GetString, write=SetString, index=18};
	__property System::UnicodeString MenuTabLeft = {read=GetString, write=SetString, index=19};
	__property System::UnicodeString MenuTabRight = {read=GetString, write=SetString, index=20};
	__property System::UnicodeString MenuTabWordBar = {read=GetString, write=SetString, index=21};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRulerTexts(void) { }
	
};


class PASCALIMPLEMENTATION TCustomRuler : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
private:
	TBiDiModeRuler FBiDiModeRuler;
	System::Extended FDefaultTabWidth;
	int FDiff;
	bool FFlat;
	TIndentArray FIndents;
	TIndentSettings* FIndentSettings;
	TIndentArray FIndentTraces;
	int FInset;
	int FItemSeparation;
	TRulerListEditor* FListEditor;
	System::StaticArray<TMargin, 4> FMargins;
	TMarginSettings* FMarginSettings;
	System::Uitypes::TColor FMarginColor;
	int FMaxTabs;
	System::Extended FMultiPixels;
	System::Extended FMultiPoints;
	bool FOldParentBackground;
	bool FOldShowHint;
	TRulerCustomDrawEvent FOnBeforeDrawBackground;
	System::Classes::TNotifyEvent FOnBiDiModeChanged;
	System::Classes::TNotifyEvent FOnIndentChanged;
	TIndentClickEvent FOnIndentClick;
	TIndentClickEvent FOnIndentDblClick;
	System::Classes::TNotifyEvent FOnMarginChanged;
	TMarginClickEvent FOnMarginClick;
	TMarginClickEvent FOnMarginDblClick;
	System::Classes::TNotifyEvent FOnPageHeightChanged;
	System::Classes::TNotifyEvent FOnPageWidthChanged;
	TRulerItemMoveEvent FOnRulerItemMove;
	System::Classes::TNotifyEvent FOnRulerItemRelease;
	TRulerItemClickEvent FOnRulerItemSelect;
	System::Classes::TNotifyEvent FOnTabChanged;
	TTabClickEvent FOnTabClick;
	TTabClickEvent FOnTabDblClick;
	TBorderClickEvent FOnTableBorderClick;
	TBorderClickEvent FOnTableBorderDblClick;
	System::Classes::TNotifyEvent FOnTableColumnChanged;
	TColumnClickEvent FOnTableColumnClick;
	TColumnClickEvent FOnTableColumnDblClick;
	System::Classes::TNotifyEvent FOnTableRowChanged;
	TRowClickEvent FOnTableRowClick;
	TRowClickEvent FOnTableRowDblClick;
	System::UnicodeString FOrgHint;
	bool FOvrHint;
	System::Extended FPageHeight;
	System::Extended FPageWidth;
	Vcl::Menus::TPopupMenu* FPopupMenu;
	System::Extended FPrinterHeight;
	System::Extended FPrinterWidth;
	System::Uitypes::TColor FRulerColor;
	System::Uitypes::TColor FRulerColorPageEnd;
	TRulerOptions FRulerOptions;
	TRulerTexts* FRulerTexts;
	TRulerType FRulerType;
	int FScreenRes;
	TRulerTableEditor* FTableEditor;
	TTabs* FTabs;
	TTabSettings* FTabSettings;
	TTab* FTabTrace;
	Vcl::Extctrls::TTimer* FTimer;
	TRulerUnits FUnitsDisplay;
	TRulerUnits FUnitsProgram;
	TZoomRange FZoom;
	TSkinType FSkinType;
	int FIndSaturation;
	int FMargSaturation;
	int FRulSaturation;
	System::StaticArray<Vcl::Graphics::TBitmap*, 13> FRulerBmp;
	System::StaticArray<System::Uitypes::TColor, 15> FMargColors;
	System::StaticArray<System::Uitypes::TColor, 9> FMargBoundColors;
	System::StaticArray<System::Uitypes::TColor, 10> FRulerColors;
	System::Uitypes::TColor FIndentColor;
	HIDESBASE MESSAGE void __fastcall CMFontChanged(Winapi::Messages::TMessage &Message);
	System::Extended __fastcall GetBottomMargin(void);
	System::Extended __fastcall GetDefaultTabWidth(void);
	System::Extended __fastcall GetEndMargin(void);
	int __fastcall GetEndMarginGrip(void);
	System::Extended __fastcall GetFirstIndent(void);
	System::Types::TRect __fastcall GetIndentRect(TIndentType IndentType);
	System::Extended __fastcall GetLeftIndent(void);
	System::Extended __fastcall GetLeftMargin(void);
	System::Types::TRect __fastcall GetLevelRect(TLevelType LevelType);
	System::Types::TRect __fastcall GetMarginGripRect(TMarginType MarginType);
	System::Types::TRect __fastcall GetMarginRect(TMarginType MarginType);
	Vcl::Graphics::TFont* __fastcall GetPaintFont(void);
	System::Extended __fastcall GetRightIndent(void);
	System::Extended __fastcall GetRightMargin(void);
	System::Extended __fastcall GetStartMargin(void);
	int __fastcall GetStartMarginGrip(void);
	int __fastcall GetTabIndexAt(int X, int Y);
	System::Extended __fastcall GetTopMargin(void);
	int __fastcall HitItemsToIndex(THitItems HitItems);
	void __fastcall PaintHDefaultTabs(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintHIndent(Vcl::Graphics::TCanvas* Canvas, TIndentGraphic Graphic, int X, int Y, bool HighLight, bool ExtraShadow, bool Dimmed);
	void __fastcall PaintHIndents(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintHMargins(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintHMarkers(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintHOutline(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintHTab(Vcl::Graphics::TCanvas* Canvas, TTabAlign Graphic, int X, int Y);
	void __fastcall PaintHTableGraphic(Vcl::Graphics::TCanvas* Canvas, TTableGraphic Graphic, const System::Types::TRect &R);
	void __fastcall PaintHTableGraphics(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintHTabs(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintVMargins(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintVMarkers(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintVOutline(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall PaintVTableGraphic(Vcl::Graphics::TCanvas* Canvas, TTableGraphic Graphic, const System::Types::TRect &R);
	void __fastcall PaintVTableGraphics(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	void __fastcall ProcessParentBackground(bool B);
	int __fastcall RTLAdjust(int X, int Offset);
	System::Types::TRect __fastcall RTLAdjustRect(const System::Types::TRect &R);
	void __fastcall SetBiDiModeRuler(const TBiDiModeRuler Value);
	void __fastcall SetBottomMargin(System::Extended Value);
	void __fastcall SetDefaultTabWidth(const System::Extended Value);
	void __fastcall SetFirstIndent(System::Extended Value);
	void __fastcall SetFlat(const bool Value);
	void __fastcall SetIndentSettings(TIndentSettings* const Value);
	void __fastcall SetInset(const int Value);
	void __fastcall SetItemSeparation(const int Value);
	void __fastcall SetLeftIndent(System::Extended Value);
	void __fastcall SetLeftMargin(System::Extended Value);
	void __fastcall SetListEditor(TRulerListEditor* const Value);
	void __fastcall SetMarginColor(const System::Uitypes::TColor Value);
	void __fastcall SetMarginSettings(TMarginSettings* const Value);
	void __fastcall SetMaxTabs(const int Value);
	void __fastcall SetPageHeight(const System::Extended Value);
	void __fastcall SetPageWidth(const System::Extended Value);
	void __fastcall SetRightIndent(System::Extended Value);
	void __fastcall SetRightMargin(System::Extended Value);
	void __fastcall SetRulerColor(const System::Uitypes::TColor Value);
	void __fastcall SetRulerColorPageEnd(const System::Uitypes::TColor Value);
	void __fastcall SetRulerOptions(const TRulerOptions Value);
	void __fastcall SetRulerTexts(TRulerTexts* const Value);
	void __fastcall SetScreenRes(const int Value);
	void __fastcall SetTableEditor(TRulerTableEditor* const Value);
	void __fastcall SetTabs(TTabs* const Value);
	void __fastcall SetTabSettings(TTabSettings* const Value);
	void __fastcall SetTopMargin(System::Extended Value);
	void __fastcall SetUnitsDisplay(const TRulerUnits Value);
	void __fastcall SetUnitsProgram(const TRulerUnits Value);
	void __fastcall SetZoom(const TZoomRange Value);
	void __fastcall SetSkinType(const TSkinType SType);
	void __fastcall SetRulSaturation(const int NewSaturation);
	void __fastcall SetIndSaturation(const int NewSaturation);
	void __fastcall SetMargSaturation(const int NewSaturation);
	void __fastcall SetIndentColor(const System::Uitypes::TColor NewColor);
	void __fastcall UpdateMargSkin(void);
	void __fastcall UpdateIndSkin(void);
	void __fastcall UpdateRulerColors(void);
	void __fastcall TimerProc(System::TObject* Sender);
	HIDESBASE MESSAGE void __fastcall WMWindowPosChanged(Winapi::Messages::TWMWindowPosMsg &Message);
	
protected:
	TDragInfo FDragInfo;
	virtual void __fastcall SetRulerType(const TRulerType Value);
	DYNAMIC bool __fastcall DoBeforeDrawBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
	DYNAMIC void __fastcall DoBiDiModeChanged(void);
	DYNAMIC void __fastcall DoIndentChanged(void);
	DYNAMIC void __fastcall DoIndentClick(TIndentType Indent);
	DYNAMIC void __fastcall DoIndentDblClick(TIndentType Indent);
	DYNAMIC void __fastcall DoLevelButtonDown(int Direction);
	DYNAMIC void __fastcall DoLevelButtonUp(int Direction);
	DYNAMIC void __fastcall DoMarginChanged(void);
	DYNAMIC void __fastcall DoMarginClick(TMarginType Margin);
	DYNAMIC void __fastcall DoMarginDblClick(TMarginType Margin);
	DYNAMIC void __fastcall DoPageHeightChanged(void);
	DYNAMIC void __fastcall DoPageWidthChanged(void);
	DYNAMIC void __fastcall DoRulerItemMove(TDragItem DragItem, int X, bool Removing);
	DYNAMIC void __fastcall DoRulerItemRelease(TDragItem DragItem);
	DYNAMIC void __fastcall DoRulerItemSelect(TDragItem DragItem, int X);
	DYNAMIC void __fastcall DoTabClick(int Tab);
	DYNAMIC void __fastcall DoTabDblClick(int Tab);
	DYNAMIC void __fastcall DoTabChanged(void);
	DYNAMIC void __fastcall DoTableBorderClick(TBorderType Border);
	DYNAMIC void __fastcall DoTableBorderDblClick(TBorderType Border);
	DYNAMIC void __fastcall DoTableColumnChanged(void);
	DYNAMIC void __fastcall DoTableColumnClick(int Column);
	DYNAMIC void __fastcall DoTableColumnDblClick(int Column);
	DYNAMIC void __fastcall DoTableRowChanged(void);
	DYNAMIC void __fastcall DoTableRowClick(int Row);
	DYNAMIC void __fastcall DoTableRowDblClick(int Row);
	int __fastcall GetClosestOnRuler(int X, bool FromMargin);
	virtual void __fastcall Loaded(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall OverrideHint(const System::UnicodeString NewHint);
	virtual void __fastcall Paint(void);
	void __fastcall PaintHorizontalRuler(Vcl::Graphics::TCanvas* Canvas);
	void __fastcall PaintVerticalRuler(Vcl::Graphics::TCanvas* Canvas);
	System::Extended __fastcall PointsToUnits(const System::Extended Value);
	void __fastcall PopupClick(System::TObject* Sender);
	void __fastcall RestoreHint(void);
	System::Extended __fastcall UnitsToPoints(const System::Extended Value);
	virtual void __fastcall UpdatePageDimensions(void);
	virtual bool __fastcall UseRTL(void);
	int __fastcall ZoomAndRound(const System::Extended Value);
	__property TBiDiModeRuler BiDiModeRuler = {read=FBiDiModeRuler, write=SetBiDiModeRuler, default=0};
	__property System::Extended BottomMargin = {read=GetBottomMargin, write=SetBottomMargin};
	__property System::Extended DefaultTabWidth = {read=GetDefaultTabWidth, write=SetDefaultTabWidth};
	__property System::Extended EndMargin = {read=GetEndMargin};
	__property int EndMarginGrip = {read=GetEndMarginGrip, nodefault};
	__property System::Extended FirstIndent = {read=GetFirstIndent, write=SetFirstIndent};
	__property bool Flat = {read=FFlat, write=SetFlat, nodefault};
	__property TIndentSettings* IndentSettings = {read=FIndentSettings, write=SetIndentSettings};
	__property int Inset = {read=FInset, write=SetInset, default=10};
	__property int ItemSeparation = {read=FItemSeparation, write=SetItemSeparation, nodefault};
	__property System::Extended LeftIndent = {read=GetLeftIndent, write=SetLeftIndent};
	__property System::Extended LeftMargin = {read=GetLeftMargin, write=SetLeftMargin};
	__property TRulerListEditor* ListEditor = {read=FListEditor, write=SetListEditor};
	__property System::Uitypes::TColor MarginColor = {read=FMarginColor, write=SetMarginColor, default=-16777192};
	__property TMarginSettings* MarginSettings = {read=FMarginSettings, write=SetMarginSettings};
	__property int MaxTabs = {read=FMaxTabs, write=SetMaxTabs, default=32};
	__property TRulerCustomDrawEvent OnBeforeDrawBackground = {read=FOnBeforeDrawBackground, write=FOnBeforeDrawBackground};
	__property System::Classes::TNotifyEvent OnBiDiModeChanged = {read=FOnBiDiModeChanged, write=FOnBiDiModeChanged};
	__property System::Classes::TNotifyEvent OnIndentChanged = {read=FOnIndentChanged, write=FOnIndentChanged};
	__property TIndentClickEvent OnIndentClick = {read=FOnIndentClick, write=FOnIndentClick};
	__property TIndentClickEvent OnIndentDblClick = {read=FOnIndentDblClick, write=FOnIndentDblClick};
	__property System::Classes::TNotifyEvent OnMarginChanged = {read=FOnMarginChanged, write=FOnMarginChanged};
	__property TMarginClickEvent OnMarginClick = {read=FOnMarginClick, write=FOnMarginClick};
	__property TMarginClickEvent OnMarginDblClick = {read=FOnMarginDblClick, write=FOnMarginDblClick};
	__property System::Classes::TNotifyEvent OnPageHeightChanged = {read=FOnPageHeightChanged, write=FOnPageHeightChanged};
	__property System::Classes::TNotifyEvent OnPageWidthChanged = {read=FOnPageWidthChanged, write=FOnPageWidthChanged};
	__property TRulerItemMoveEvent OnRulerItemMove = {read=FOnRulerItemMove, write=FOnRulerItemMove};
	__property System::Classes::TNotifyEvent OnRulerItemRelease = {read=FOnRulerItemRelease, write=FOnRulerItemRelease};
	__property TRulerItemClickEvent OnRulerItemSelect = {read=FOnRulerItemSelect, write=FOnRulerItemSelect};
	__property System::Classes::TNotifyEvent OnTabChanged = {read=FOnTabChanged, write=FOnTabChanged};
	__property TTabClickEvent OnTabClick = {read=FOnTabClick, write=FOnTabClick};
	__property TTabClickEvent OnTabDblClick = {read=FOnTabDblClick, write=FOnTabDblClick};
	__property TBorderClickEvent OnTableBorderClick = {read=FOnTableBorderClick, write=FOnTableBorderClick};
	__property TBorderClickEvent OnTableBorderDblClick = {read=FOnTableBorderDblClick, write=FOnTableBorderDblClick};
	__property System::Classes::TNotifyEvent OnTableColumnChanged = {read=FOnTableColumnChanged, write=FOnTableColumnChanged};
	__property TColumnClickEvent OnTableColumnClick = {read=FOnTableColumnClick, write=FOnTableColumnClick};
	__property TColumnClickEvent OnTableColumnDblClick = {read=FOnTableColumnDblClick, write=FOnTableColumnDblClick};
	__property System::Classes::TNotifyEvent OnTableRowChanged = {read=FOnTableRowChanged, write=FOnTableRowChanged};
	__property TRowClickEvent OnTableRowClick = {read=FOnTableRowClick, write=FOnTableRowClick};
	__property TRowClickEvent OnTableRowDblClick = {read=FOnTableRowDblClick, write=FOnTableRowDblClick};
	__property TRulerOptions Options = {read=FRulerOptions, write=SetRulerOptions, default=4};
	__property System::Extended RightIndent = {read=GetRightIndent, write=SetRightIndent};
	__property System::Extended RightMargin = {read=GetRightMargin, write=SetRightMargin};
	__property System::Uitypes::TColor RulerColor = {read=FRulerColor, write=SetRulerColor, default=-16777211};
	__property System::Uitypes::TColor RulerColorPageEnd = {read=FRulerColorPageEnd, write=SetRulerColorPageEnd, default=-16777201};
	__property TRulerTexts* RulerTexts = {read=FRulerTexts, write=SetRulerTexts};
	__property TRulerType RulerType = {read=FRulerType, write=SetRulerType, default=0};
	__property System::Extended StartMargin = {read=GetStartMargin};
	__property int StartMarginGrip = {read=GetStartMarginGrip, nodefault};
	__property TRulerTableEditor* TableEditor = {read=FTableEditor, write=SetTableEditor};
	__property TTabs* Tabs = {read=FTabs, write=SetTabs};
	__property TTabSettings* TabSettings = {read=FTabSettings, write=SetTabSettings};
	__property System::Extended TopMargin = {read=GetTopMargin, write=SetTopMargin};
	__property TRulerUnits UnitsDisplay = {read=FUnitsDisplay, write=SetUnitsDisplay, default=1};
	__property TRulerUnits UnitsProgram = {read=FUnitsProgram, write=SetUnitsProgram, default=1};
	__property TZoomRange Zoom = {read=FZoom, write=SetZoom, default=100};
	__property TSkinType SkinType = {read=FSkinType, write=SetSkinType, default=0};
	__property int RulerSaturation = {read=FRulSaturation, write=SetRulSaturation, default=250};
	__property int IndentSaturation = {read=FIndSaturation, write=SetIndSaturation, default=250};
	__property int MarginSaturation = {read=FMargSaturation, write=SetMargSaturation, default=250};
	__property System::Uitypes::TColor IndentColor = {read=FIndentColor, write=SetIndentColor, default=-16777201};
	
public:
	__fastcall virtual TCustomRuler(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomRuler(void);
	void __fastcall AddRichEdit98Tab(const System::Extended Value, TTabAlign Alignment);
	System::Extended __fastcall UnitsPerInch(TRulerUnits Units);
	THitInfo __fastcall GetHitTestInfoAt(int X, int Y);
	System::Extended __fastcall PixsToUnits(const System::Extended Value);
	System::Extended __fastcall UnitsToPixs(const System::Extended Value);
	System::Extended __fastcall ZPixsToUnits(const System::Extended Value);
	System::Extended __fastcall ZUnitsToPixs(const System::Extended Value);
	__property System::Extended MultiPixels = {read=FMultiPixels};
	__property System::Extended MultiPoints = {read=FMultiPoints};
	__property System::Extended PageHeight = {read=FPageHeight, write=SetPageHeight};
	__property System::Extended PageWidth = {read=FPageWidth, write=SetPageWidth};
	__property int ScreenRes = {read=FScreenRes, write=SetScreenRes, nodefault};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRuler(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


class DELPHICLASS TRuler;
class PASCALIMPLEMENTATION TRuler : public TCustomRuler
{
	typedef TCustomRuler inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property Constraints;
	__property DragKind = {default=0};
	__property BiDiModeRuler = {default=0};
	__property BottomMargin = {default=0};
	__property Color = {default=-16777211};
	__property DefaultTabWidth = {default=0};
	__property DragCursor = {default=-12};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property FirstIndent = {default=0};
	__property Flat;
	__property Font;
	__property Hint = {default=0};
	__property IndentSettings;
	__property Inset = {default=10};
	__property LeftIndent = {default=0};
	__property LeftMargin = {default=0};
	__property MarginColor = {default=-16777192};
	__property MarginSettings;
	__property IndentColor = {default=-16777201};
	__property SkinType = {default=0};
	__property RulerSaturation = {default=250};
	__property IndentSaturation = {default=250};
	__property MarginSaturation = {default=250};
	__property MaxTabs = {default=32};
	__property OnBeforeDrawBackground;
	__property OnBiDiModeChanged;
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnIndentChanged;
	__property OnIndentClick;
	__property OnIndentDblClick;
	__property OnMarginChanged;
	__property OnMarginClick;
	__property OnMarginDblClick;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnPageWidthChanged;
	__property OnRulerItemMove;
	__property OnRulerItemRelease;
	__property OnRulerItemSelect;
	__property OnStartDock;
	__property OnStartDrag;
	__property OnTabChanged;
	__property OnTabClick;
	__property OnTabDblClick;
	__property OnTableBorderClick;
	__property OnTableBorderDblClick;
	__property OnTableColumnChanged;
	__property OnTableColumnClick;
	__property OnTableColumnDblClick;
	__property Options = {default=4};
	__property ParentBackground;
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property RightIndent = {default=0};
	__property RightMargin = {default=0};
	__property RulerColor = {default=-16777211};
	__property RulerColorPageEnd = {default=-16777201};
	__property RulerTexts;
	__property RulerType = {default=0};
	__property ShowHint;
	__property Tabs;
	__property TabSettings;
	__property TopMargin = {default=0};
	__property Visible = {default=1};
	__property UnitsDisplay = {default=1};
	__property UnitsProgram = {default=1};
	__property Zoom = {default=100};
public:
	/* TCustomRuler.Create */ inline __fastcall virtual TRuler(System::Classes::TComponent* AOwner) : TCustomRuler(AOwner) { }
	/* TCustomRuler.Destroy */ inline __fastcall virtual ~TRuler(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRuler(HWND ParentWindow) : TCustomRuler(ParentWindow) { }
	
};


class DELPHICLASS TVRuler;
class PASCALIMPLEMENTATION TVRuler : public TCustomRuler
{
	typedef TCustomRuler inherited;
	
public:
	__fastcall virtual TVRuler(System::Classes::TComponent* AOwner);
	__property MaxTabs = {default=0};
	__property RulerType = {default=1};
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Constraints;
	__property DragKind = {default=0};
	__property BottomMargin = {default=0};
	__property Color = {default=-16777211};
	__property DragCursor = {default=-12};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Flat;
	__property Font;
	__property Hint = {default=0};
	__property Inset = {default=10};
	__property MarginColor = {default=-16777192};
	__property MarginSettings;
	__property IndentColor = {default=-16777201};
	__property SkinType = {default=0};
	__property RulerSaturation = {default=250};
	__property IndentSaturation = {default=250};
	__property MarginSaturation = {default=250};
	__property OnBeforeDrawBackground;
	__property OnBiDiModeChanged;
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnMarginChanged;
	__property OnMarginClick;
	__property OnMarginDblClick;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnPageHeightChanged;
	__property OnRulerItemMove;
	__property OnRulerItemRelease;
	__property OnRulerItemSelect;
	__property OnStartDock;
	__property OnStartDrag;
	__property OnTableBorderClick;
	__property OnTableBorderDblClick;
	__property OnTableRowClick;
	__property OnTableRowDblClick;
	__property Options = {default=4};
	__property ParentBackground;
	__property ParentColor = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property RulerColor = {default=-16777211};
	__property RulerColorPageEnd = {default=-16777201};
	__property ShowHint;
	__property TopMargin = {default=0};
	__property Visible = {default=1};
	__property UnitsDisplay = {default=1};
	__property UnitsProgram = {default=1};
	__property Zoom = {default=100};
public:
	/* TCustomRuler.Destroy */ inline __fastcall virtual ~TVRuler(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TVRuler(HWND ParentWindow) : TCustomRuler(ParentWindow) { }
	
};


class DELPHICLASS TCustomRulerItemSelector;
class PASCALIMPLEMENTATION TCustomRulerItemSelector : public Vcl::Extctrls::TCustomPanel
{
	typedef Vcl::Extctrls::TCustomPanel inherited;
	
private:
	TRulerItem FActiveItem;
	TRulerItems FAvailableItems;
	System::Uitypes::TColor FBevelHighLightColor;
	System::Uitypes::TColor FBevelShadowColor;
	TRulerTextChangedEvent FOldRulerTextChanged;
	System::Classes::TNotifyEvent FOldRulerBiDiModeChanged;
	System::Classes::TNotifyEvent FOnMouseLeave;
	System::Classes::TNotifyEvent FOnMouseEnter;
	TCustomRuler* FRuler;
	void __fastcall AssignRulerEvents(void);
	void __fastcall RestoreRulerEvents(void);
	void __fastcall RulerTextChanged(System::TObject* Sender, TRulerTextItem Item);
	void __fastcall RulerBiDiModeChanged(System::TObject* Sender);
	void __fastcall SetActiveItem(const TRulerItem Value);
	void __fastcall SetAvailableItems(const TRulerItems Value);
	void __fastcall SetBevelHighLightColor(System::Uitypes::TColor Value);
	void __fastcall SetBevelShadowColor(System::Uitypes::TColor Value);
	void __fastcall SetRuler(TCustomRuler* const Value);
	HIDESBASE MESSAGE void __fastcall CMMouseEnter(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall CMMouseLeave(Winapi::Messages::TMessage &Message);
	void __fastcall UpdateHint(void);
	
protected:
	virtual void __fastcall Loaded(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall Paint(void);
	
public:
	__fastcall virtual TCustomRulerItemSelector(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomRulerItemSelector(void);
	__property TRulerItem ActiveItem = {read=FActiveItem, write=SetActiveItem, nodefault};
	__property TRulerItems AvailableItems = {read=FAvailableItems, write=SetAvailableItems, default=14};
	__property System::Uitypes::TColor BevelHighLightColor = {read=FBevelHighLightColor, write=SetBevelHighLightColor, default=-16777196};
	__property System::Uitypes::TColor BevelShadowColor = {read=FBevelShadowColor, write=SetBevelShadowColor, default=-16777200};
	__property TCustomRuler* Ruler = {read=FRuler, write=SetRuler};
	__property System::Classes::TNotifyEvent OnMouseEnter = {read=FOnMouseEnter, write=FOnMouseEnter};
	__property System::Classes::TNotifyEvent OnMouseLeave = {read=FOnMouseLeave, write=FOnMouseLeave};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRulerItemSelector(HWND ParentWindow) : Vcl::Extctrls::TCustomPanel(ParentWindow) { }
	
};


class DELPHICLASS TRulerItemSelector;
class PASCALIMPLEMENTATION TRulerItemSelector : public TCustomRulerItemSelector
{
	typedef TCustomRulerItemSelector inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property AvailableItems = {default=14};
	__property BevelEdges = {default=15};
	__property BevelHighLightColor = {default=-16777196};
	__property BevelInner = {default=2};
	__property BevelKind = {default=0};
	__property BevelOuter = {default=1};
	__property BevelShadowColor = {default=-16777200};
	__property BevelWidth = {default=1};
	__property BorderWidth = {default=0};
	__property BorderStyle = {default=0};
	__property Color = {default=-16777201};
	__property Constraints;
	__property Ctl3D;
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property FullRepaint = {default=1};
	__property Locked = {default=0};
	__property Padding;
	__property ParentBackground = {default=1};
	__property ParentColor = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property Ruler;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property Visible = {default=1};
	__property OnAlignInsertBefore;
	__property OnAlignPosition;
	__property OnCanResize;
	__property OnClick;
	__property OnConstrainedResize;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDockDrop;
	__property OnDockOver;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGetSiteInfo;
	__property OnMouseActivate;
	__property OnMouseDown;
	__property OnMouseEnter;
	__property OnMouseLeave;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnResize;
	__property OnStartDock;
	__property OnStartDrag;
	__property OnUnDock;
public:
	/* TCustomRulerItemSelector.Create */ inline __fastcall virtual TRulerItemSelector(System::Classes::TComponent* AOwner) : TCustomRulerItemSelector(AOwner) { }
	/* TCustomRulerItemSelector.Destroy */ inline __fastcall virtual ~TRulerItemSelector(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRulerItemSelector(HWND ParentWindow) : TCustomRulerItemSelector(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE THitItems AllBorders;
extern DELPHI_PACKAGE THitItems AllIndents;
extern DELPHI_PACKAGE THitItems AllLevels;
extern DELPHI_PACKAGE THitItems AllMargins;
extern DELPHI_PACKAGE THitItems AllMarginGrips;
extern DELPHI_PACKAGE THitItems AllTable;
extern DELPHI_PACKAGE THitItems AllTabs;
#define DefaultIndentOptions (System::Set<TIndentOption, TIndentOption::ioExtraShadow, TIndentOption::ioSnapToRuler>() << TIndentOption::ioKeepWithinMargins << TIndentOption::ioShowFirstIndent << TIndentOption::ioShowHangingIndent << TIndentOption::ioShowLeftIndent << TIndentOption::ioShowRightIndent )
#define DefaultListOptions (System::Set<TListEditorOption, TListEditorOption::leoAdjustable, TListEditorOption::leoLevelAdjustable>() << TListEditorOption::leoAdjustable << TListEditorOption::leoLevelAdjustable )
#define DefaultMarginOptions (System::Set<TMarginOption, TMarginOption::moAdjustable, TMarginOption::moSnapToRuler>() << TMarginOption::moAdjustable )
#define DefaultRulerItems (System::Set<TRulerItem, TRulerItem::riNone, TRulerItem::riTabWordBarAlign>() << TRulerItem::riTabLeftAlign << TRulerItem::riTabCenterAlign << TRulerItem::riTabRightAlign )
#define DefaultRulerOptions (System::Set<TRulerOption, TRulerOption::roAutoUpdatePrinterWidth, TRulerOption::roSuppressScaleAtMarginGrips>() << TRulerOption::roUseDefaultPrinterWidth )
#define DefaultTableOptions (System::Set<TTableEditorOption, TTableEditorOption::teoAdjustable, TTableEditorOption::teoSnapToRuler>() << TTableEditorOption::teoAdjustable )
#define DefaultTabOptions (System::Set<TTabOption, TTabOption::toAdvancedTabs, TTabOption::toDontShowDefaultTabsBeforeLeftIndent>() << TTabOption::toShowDefaultTabs << TTabOption::toDontShowDefaultTabsBeforeLeftIndent )
extern DELPHI_PACKAGE System::StaticArray<System::Uitypes::TColor, 10> DefRulerColors;
}	/* namespace Ruler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RULER)
using namespace Ruler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RulerHPP
