
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Finnish translation ver 1.21   (suomi)          }
{       UPDATE with minor corrections                   }
{                                                       }
{       Translated by fred.thomas-50ch75r@yopmail.com   }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Updated: 2014-02-25                                   }
{*******************************************************}

unit RVAL_Fin;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'tuumaa', 'cm', 'mm', 'picaa', 'pikseli�', 'pistett�',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Tiedosto', '&Muokkaa', 'M&uotoilu', '&Fontti', '&Kappale', 'Li&s�ys', 'T&aulukko', '&Ikkuna', '&Ohje',
  // exit
  '&Lopeta',
  // top-level menus: View, Tools,
  '&N�kym�', '&Ty�kalut',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Koko', 'Tyyli', '&Valitse taulukko', 'Pystytasaus', 'Solureunat',
  // menus: Table cell rotation
  'Solun &kierto', 
  // menus: Text flow, Footnotes/endnotes
  'Tekstin rivity&s', '&Viitteet',
  // ribbon tabs: tab1, tab2, view, table
  '&Aloitus', '&Lis�asetukset', '&N�kym�', 'T&aulukko',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Leikep�yt�', 'Fontti', 'Kappale', 'Luettelo', 'Muokkaus',
  // ribbon groups: insert, background, page setup,
  'Lis��', 'Vesileima', 'Sivun asetukset',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Linkit', 'Viitteet', 'Asiakirjan�kym�t', 'N�yt�/piilota', 'Zoomaus', 'Lis�valinnat',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Lis��', 'Poista', 'Toiminnot', 'Reunat',
  // ribbon groups: styles 
  'Tyylit',
  // ribbon screen tip footer,
  'Lis�ohjeita saat painamalla F1',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Viimeksi k�ytetyt tiedostot', 'Tallenna nimell�', 'Esikatselu ja tulostus',
  // ribbon label: units combo
  'Yksikk�:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Uusi', 'Uusi|Luo tyhj� asiakirja',
  // TrvActionOpen
  '&Avaa...', 'Avaa|Avaa olemassa oleva asiakirja',
  // TrvActionSave
  '&Tallenna', 'Tallenna|Tallenna nykyinen asiakirja',
  // TrvActionSaveAs
  'Tallenna &nimell�...', 'Tallenna nimell�...|Tallenna nykyinen asiakirja uudella nimell�',
  // TrvActionExport
  '&Vie...', 'Vie|Vie asiakirja eri tiedostomuotoon',
  // TrvActionPrintPreview
  '&Esikatselu', 'Esikatselu|N�yt� asiakirja tulostusmuodossaan',
  // TrvActionPrint
  'T&ulostus...', 'Tulostus|M��rit� tulostusasetukset ja tulosta asiakirja',
  // TrvActionQuickPrint
  'Tulosta &heti', 'Tulosta heti', 'Tulosta|Tulosta asiakirja',
  // TrvActionPageSetup
  '&Sivun asetukset...', 'Sivun asetukset|Aseta reunukset, paperikoko, suunta ja tunnisteet',
  // TrvActionCut
  'Le&ikkaa', 'Leikkaa|Siirr� valittu alue leikep�yd�lle',
  // TrvActionCopy
  '&Kopioi', 'Kopioi|Kopioi valittu alue leikep�yd�lle',
  // TrvActionPaste
  '&Liit�', 'Liit�|Liit� leikep�yd�n sis�ll�n',
  // TrvActionPasteAsText
  'Liit� &teksti', 'Liit� teksti|Liit� leikep�yd�n sis�lt� muotoilemattomana tekstin�',  
  // TrvActionPasteSpecial
  'Liit� &m��r�ten...', 'Liit� m��r�ten|Liit� leikep�yd�n sis�lt� m��ritetyss� muodossa',
  // TrvActionSelectAll
  'Valitse &kaikki', 'Valitse kaikki|Valitse koko asiakirja',
  // TrvActionUndo
  'Ku&moa', 'Kumoa|Kumoa viimeisin toiminto',
  // TrvActionRedo
  'Tee &uudelleen', 'Tee uudelleen|Peru viimeisin kumoaminen',
  // TrvActionFind
  '&Etsi...', 'Etsi|Etsi m��ritetty� teksti',
  // TrvActionFindNext
  'Etsi &seuraava', 'Etsi seuraava|Jatka viimeisint� hakua',
  // TrvActionReplace
  'Kor&vaa...', 'Korvaa|Etsi ja korvaa m��ritetty teksti',
  // TrvActionInsertFile
  'Lis�� &tiedosto...', 'Lis�� tiedosto|Lis�� tiedoston sis�lt�',
  // TrvActionInsertPicture
  'Lis�� &kuva...', 'Lis�� kuva|Lis�� kuvatiedosto',
  // TRVActionInsertHLine
  '&Vaakaviiva', 'Lis�� vaakaviiva|Lis�� vaakasuora viiva',
  // TRVActionInsertHyperlink
  '&Hyperlinkki...', 'Lis�� hyperlinkki|Lis�� hypertekstilinkki',
  // TRVActionRemoveHyperlinks
  '&Poista hyperlinkit', 'Poista hyperlinkit|Poista valitusta tekstist� kaikki hyperlinkit',  
  // TrvActionInsertSymbol
  '&Symboli...', 'Lis�� symboli|Lis�� symboli',
  // TrvActionInsertNumber
  '&Numero...', 'Lis�� numero|Lis�� numero',
  // TrvActionInsertCaption
  'Lis�� &teksti...', 'Lis�� teksti|Lis�� valittuun kohteeseen teksti',
  // TrvActionInsertTextBox
  '&Tekstiruutu', 'Lis�� tekstiruutu|Lis�� tekstiruutu',
  // TrvActionInsertSidenote
  '&Reunahuomautus', 'Lis�� reunahuomautus|Lis�� tekstilaatikossa esitett�v� huomautus',
  // TrvActionInsertPageNumber
  '&Sivunumero', 'Lis�� sivunumero|Lis�� sivunumero',
  // TrvActionParaList
  '&Luettelomerkit ja numerointi...', 'Luettelomerkit ja numerointi|K�yt� tai muokkaa valittujen kappaleiden luettelomerkkej� tai numerointia',
  // TrvActionParaBullets
  '&Luettelomerkit', 'Luettelomerkit|Lis�� tai poista kappaleesta luettelomerkit',
  // TrvActionParaNumbering
  '&Numerointi', 'Numerointi|Lis�� tai poista kappaleesta numerointi',
  // TrvActionColor
  '&Taustav�ri...', 'Taustav�ri|Muuta asiakirjan taustav�ri�',
  // TrvActionFillColor
  '&T�ytt�...', 'T�ytt�|Muuta tekstin, solujen, kappaleiden, taulukoiden tai asiakirjan taustav�ri�',
  // TrvActionInsertPageBreak
  '&Lis�� sivunvaihto', 'Lis�� sivunvaihto|Lis�� sivunvaihto',
  // TrvActionRemovePageBreak
  '&Poista sivunvaihto', 'Poista sivunvaihto|Poista sivunvaihto',
  // TrvActionClearLeft
  'Rivit� &vasemmalle', 'Rivit� vasemmalle|Rivit� teksti vasemmalle v�ltt�en kuvat',
  // TrvActionClearRight
  'Rivit� &oikealle', 'Rivit� oikealle|Rivit� teksti oikealle v�ltt�en kuvat',
  // TrvActionClearBoth
  'Rivit� &molemmat', 'Rivit� &molemmat|Rivit� teksti v�ltt�en kuvat',
  // TrvActionClearNone
  'Tiukka &rivitys', 'Tiukka rivitys|Rivit� teksti kuvien molemmille puolille',
  // TrvActionVAlign
  '&Sijainti...', 'Sijainti|Muuta valitun kohteen sijaintia',  
  // TrvActionItemProperties
  '&Ominaisuudet...', 'Ominaisuudet|Muuta valitun kohteen ominaisuuksia',
  // TrvActionBackground
  'Tausta&v�ri...', 'Taustav�ri|Valitse taustakuva ja -v�ri',
  // TrvActionParagraph
  '&Kappale...', 'Kappale|Muuta kappaleen asetteluja',
  // TrvActionIndentInc
  '&Lis�� sisennyst�', 'Lis�� sisennyst�|Lis�� valituiden kappaleiden sisennyst�',
  // TrvActionIndentDec
  '&V�henn� sisennyst�', 'V�henn� sisennyst�|V�henn� valituiden kappaleiden sisennyst�',
  // TrvActionWordWrap
  'Automaattinen &rivitys', 'Automaattinen rivitys|Automaattinen rivitys p��lle/pois',
  // TrvActionAlignLeft
  'Tasaa &vasemmalle', 'Tasaa vasemmalle|Tasaa valittu teksti vasemmalle',
  // TrvActionAlignRight
  'Tasaa &oikealle', 'Tasaa oikealle|Tasaa valittu teksti oikealle',
  // TrvActionAlignCenter
  '&Keskit�', 'Keskit�|Keskit� valittu teksti',
  // TrvActionAlignJustify
  '&Tasaa', 'Tasaa|Tasaa valittu teksti molempiin reunoihin',
  // TrvActionParaColor
  'Kappaleiden tausta&v�ri...', 'Kappaleiden taustav�ri|Aseta kappaleiden taustav�ri',
  // TrvActionLineSpacing100
  'Riviv�li &1', 'Riviv�li 1|Aseta riviv�liksi 1.0',
  // TrvActionLineSpacing150
  'Riviv�li 1.&5', 'Riviv�li 1.5|Aseta riviv�liksi 1.5',
  // TrvActionLineSpacing200
  'Riviv�li &2', 'Riviv�li 2|Aseta riviv�liksi 2.0',
  // TrvActionParaBorder
  '&Reunat ja taustav�ri...', 'Reunat ja taustav�ri|Aseta valituiden kappaleiden reunaviivat ja taustav�ri',
  // TrvActionInsertTable
  '&Lis�� taulukko...', '&Taulukko', 'Lis�� taulukko|Lis�� uusi taulukko',
  // TrvActionTableInsertRowsAbove
  'Lis�� rivi &yl�puolelle', 'Lis�� rivi yl�puolelle|Lis�� rivi valittujen solujen yl�puolelle',
  // TrvActionTableInsertRowsBelow
  'Lis�� rivi &alapuolelle', 'Lis�� rivi alapuolelle|Lis�� rivi valittujen solujen alapuolelle',
  // TrvActionTableInsertColLeft
  'Lis�� sarake &vasemmalle', 'Lis�� sarake vasemmalle|Lis�� sarake valittujen solujen vasemmalle puolelle',
  // TrvActionTableInsertColRight
  'Lis�� sarake &oikealle', 'Lis�� sarake oikealle|Lis�� sarake valittujen solujen oikealle puolelle',
  // TrvActionTableDeleteRows
  'Poista &rivit', 'Poista rivit|Poista rivit, joilla valitut solut sijaitsevat',
  // TrvActionTableDeleteCols
  'Poista &sarakkeet', 'Poista sarakkeet|Poista sarakkeet, joilla valitut solut sijaitsevat',
  // TrvActionTableDeleteTable
  '&Poista taulukko', 'Poista taulukko|Poista valittu taulukko',
  // TrvActionTableMergeCells
  '&Yhdist� solut', 'Yhdist� solut|Yhdist� valitut solut',
  // TrvActionTableSplitCells
  '&Jaa solut...', 'Jaa solut|Jaa valitut solut',
  // TrvActionTableSelectTable
  'Valitse &taulukko', 'Valitse taulukko|Valitse koko taulukko',
  // TrvActionTableSelectRows
  'Valitse r&ivit', 'Valitse rivit|Valitse rivit',
  // TrvActionTableSelectCols
  'Valitse sara&kkeet', 'Valitse sarakkeet|Valitse sarakkeet',
  // TrvActionTableSelectCell
  'Valitse &solu', 'Valitse solu|Valitse solu',
  // TrvActionTableCellVAlignTop
  'Pystytasaus &yl�s', 'Pystytasaus yl�s|Tasaa solusis�lt� yl�s',
  // TrvActionTableCellVAlignMiddle
  'Pystytasaus &keskelle', 'Pystytasaus keskelle|Keskit� solusis�lt� pystysuunnassa',
  // TrvActionTableCellVAlignBottom
  'Pystytasaus &alas', 'Pystytasaus alas|Tasaa solusis�lt� alas',
  // TrvActionTableCellVAlignDefault
  'Solujen &pystytasaus', 'Solujen pystytasaus|Aseta valittujen solujen pystytasaus',
  // TrvActionTableCellRotationNone
   '&Ei kiertoa', 'Ei kiertoa|Kierr� solun sis�lt�� 0�',
  // TrvActionTableCellRotation90
  'Kierr� solua &90�', 'Kierr� solua 90�|Kierr� solun sis�lt�� 90�',
  // TrvActionTableCellRotation180
   'Kierr� solua &180�', 'Kierr� solua 180�|Kierr� solun sis�lt�� 180�',
  // TrvActionTableCellRotation270
   'Kierr� solua &270�', 'Kierr� solua 270�|Kierr� solun sis�lt�� 270�',  
  // TrvActionTableProperties
  'Taulukon &ominaisuudet...', 'Taulukon ominaisuudet|Muuta valitun taulukon ominaisuuksia',
  // TrvActionTableGrid
  'N�yt� &ruudukko', 'N�yt� ruudukko|N�yt�/piilota apuruudukko',
  // TRVActionTableSplit
  '&Jaa taulukko', 'Jaa taulukko|Jaa taulukko kahtia alkaen valitusta rivist�',
  // TRVActionTableToText
   'Muunna te&kstiksi...', 'Muunna tekstiksi|Muunna taulukko tekstiksi',
  // TRVActionTableSort
   '&Lajittele...', 'Lajittele|Lajittele taulukon rivit',
  // TrvActionTableCellLeftBorder
  '&Vasen reuna', 'Vasen reuna|N�yt�/piilota solun vasen reunaviiva',
  // TrvActionTableCellRightBorder
  '&Oikea reuna', 'Oikea reuna|N�yt�/piilota solun oikea reunaviiva',
  // TrvActionTableCellTopBorder
  '&Yl�reuna', 'Yl�reuna|N�yt�/piilota solun yl�reunaviiva',
  // TrvActionTableCellBottomBorder
  '&Alareuna', 'Alareuna|N�yt�/piilota solun alareunaviiva',
  // TrvActionTableCellAllBorders
  '&Kaikki reunat', 'Kaikki reunat|N�yt� solun kaikki reunaviivat',
  // TrvActionTableCellNoBorders
  '&Ei reunoja', 'Ei reunoja|Piilota solun kaikki reunaviivat',
  // TrvActionFonts & TrvActionFontEx
  '&Fontti...', 'Fontti|Muuta valitun tekstin fonttia',
  // TrvActionFontBold
  '&Lihavointi', 'Lihavointi|Lihavoi valittu teksti',
  // TrvActionFontItalic
  '&Kursivointi', 'Kursivointi|Kursivoi valittu teksti',
  // TrvActionFontUnderline
  '&Alleviivaus', 'Alleviivaus|Alleviivaa valittu teksti',
  // TrvActionFontStrikeout
  '&Yliviivaus', 'Yliviivaus|Yliviivaa valittu teksti',
  // TrvActionFontGrow
  '&Suurenna fonttia', 'Suurenna fonttia|Suurenna valitun tekstin kokoa 10%',
  // TrvActionFontShrink
  '&Pienenn� fonttia', 'Pienenn� fonttia|Pienenn� valitun tekstin kokoa 10%',
  // TrvActionFontGrowOnePoint
  'S&uurenna pisteell�', 'Suurenna pisteell�|Suurenna valitun tekstin kokoa 1 pisteell�',
  // TrvActionFontShrinkOnePoint
  'P&ienenn� pisteell�', 'Pienenn� pisteell�|Pienenn� valitun tekstin kokoa 1 pisteell�',
  // TrvActionFontAllCaps
  'Is&ot kirjaimet', 'Isot kirjaimet|Muuta valitun tekstin kaikki kirjaimet isoiksi',
  // TrvActionFontOverline
  'Yl�&viiva', 'Yl�viiva|Lis�� valitun tekstin yl�puolelle viiva',
  // TrvActionFontColor
  'Teksti&v�ri...', 'Tekstiv�ri|Muuta valitun tekstin v�ri�',
  // TrvActionFontBackColor
  'Tekstin taustav&�ri...', 'Tekstin taustav�ri|Muuta valitun tekstin taustav�ri�',
  // TrvActionAddictSpell3
  '&Oikoluku', 'Oikoluku|Tarkista kirjoitusvirheet',
  // TrvActionAddictThesaurus3
  '&Sanakirja', 'Sanakirja|Etsi valitulle sanalle synonyymej�',
  // TrvActionParaLTR
  'Vasemmalta oikealle', 'Vasemmalta oikealle|Aseta valituiden kappaleiden tekstisuunta vasemmalta oikealle',
  // TrvActionParaRTL
  'Oikealta vasemmalle', 'Oikealta vasemmalle|Aseta valituiden kappaleiden tekstisuunta vasemmalta oikealle',
  // TrvActionLTR
  'Vasemmalta oikealle -teksti', 'Vasemmalta oikealle -teksti|Aseta valitun tekstin suunta vasemmalta oikealle',
  // TrvActionRTL
  'Oikealta vasemmalle -teksti', 'Oikealta vasemmalle -teksti|Aseta valitun tekstin suunta oikealta vasemmalle',
  // TrvActionCharCase
  'Merkkikoko', 'Merkkikoko|Muuta valitun tekstin kirjainkokoa (isot/pienet)',
  // TrvActionShowSpecialCharacters
  '&Tulostumattomat merkit', 'Tulostumattomat merkit|N�yt�/piilota tulostumattomat merkit, kuten tabuloinnit ja v�lily�nnit',
  // TrvActionSubscript
  '&Alaindeksi', 'Alaindeksi|Muuta valittu teksti alaindeksiksi',
  // TrvActionSuperscript
  '&Yl�indeksi', 'Yl�indeksi|Muuta valittu teksti yl�indeksiksi',
  // TrvActionInsertFootnote
  '&Alaviite', 'Alaviite|Lis�� alaviite',
  // TrvActionInsertEndnote
  '&Loppuviite', 'Loppuviite|Lis�� loppuviite',
  // TrvActionEditNote
  '&Muokkaa viitteit�', 'Muokkaa viitteit�|Muokkaa ala- ja loppuviitteit�',
  // TrvActionHide
  '&Piilota', 'Piilota|N�yt�/piilota valittu osio',  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Tyylit...', 'Tyylit|Avaa tyylien hallintaikkuna',
  // TrvActionAddStyleTemplate
  '&Lis�� tyyli...', 'Lis�� tyyli|Luo uusi teksti- tai kappaletyyli',
  // TrvActionClearFormat,
  '&Poista muotoilu', 'Poista muotoilu|Poista valitulta alueelta kaikki teksti- tai kappalemuotoilut',
  // TrvActionClearTextFormat,
  'P&oista tekstimuotoilu', 'Poista tekstimuotoilu|Poista valitulta alueelta kaikki tekstimuotoilut',
  // TrvActionStyleInspector
   'Tyyli&selain', 'Tyyliselain|N�yt� tai piilota tyyliselain',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Peruuta', 'Sulje', 'Lis��', '&Avaa...', '&Tallenna...', 'T&yhjenn�', 'Ohje', 'Poista',
  // Others  -------------------------------------------------------------------
  // percents
  'prosenttia',
  // left, top, right, bottom sides
  'Vasen reuna', 'Yl�reuna', 'Oikea reuna', 'Alareuna',
  // save changes? confirm title
  'Tiedostoa %s on muutettu. Haluatko tallentaa muutokset?', 'Muutosten tallennus',
  // warning: losing formatting
  'Varoitus: %s saattaa sis�lt�� ominaisuuksia, joita valittu tallennusmuoto ei tue.'#13+
  'Haluatko silti tallentaa t�ss� muodossa?',
  // RVF format name
  'RichView -tallennusmuoto',
  // Error messages ------------------------------------------------------------
  'Virhe',
  'Virhe avattaessa tiedostoa.'#13#13'Mahdollisia syit�:'#13'- sovellus ei tue kyseist� tiedostotyyppi�;'#13+
  '- tiedosto on viallinen;'#13'- jokin toinen sovellus on avannut ja lukinnut tiedoston.',
  'Virhe avattaessa kuvatiedostoa.'#13#13'Mahdollisia syit�:'#13'- sovellus ei tue kyseist� tiedostotyyppi�;'#13+
  '- tiedosto ei ole kuvatiedosto;'#13+
  '- tiedosto on viallinen;'#13'- jokin toinen sovellus on avannut ja lukinnut tiedoston.',
  'Virhe tallennettaessa tiedostoa.'#13#13'Mahdollisia syit�:'#13'- riitt�m�t�n vapaa levytila;'#13+
  '- tallennusmedia on kirjoitussuojattu;'#13'- asemassa ei ole siirrett�v�� tallennusmediaa;'#13+
  '- jokin toinen sovellus on avannut ja lukinnut tiedoston;'#13'- tallennusmedia on viallinen.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView-tiedostot (*.rvf)|*.rvf',  'RTF-tiedostot (*.rtf)|*.rtf' , 'XML-tiedostot (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Tekstitiedostot (*.txt)|*.txt', 'Tekstitiedostot - Unicode (*.txt)|*.txt', 'Tekstitiedostot - Automaattitunnistus (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML-tiedostot (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'yksinkertaiset HTML-tiedostot (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word -asiakirja (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Etsint� lopetettu.', 'Kohdetta ''%s'' ei l�ydy.',
  // 1 string replaced; Several strings replaced
  'Korvattu yksi merkkijono.', 'Korvattu %d merkkijonoa.',
  // continue search from the beginning/end?
  'Haku suoritettu loppuun asti, jatketaanko asiakirjan alusta?',
  'Haku suoritettu alkuun asti, jatketaanko asiakirjan lopusta?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'L�pin�kyv�', 'Automaattinen',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Musta', 'Ruskea', 'Oliivinvihre�', 'Tummanvihre�', 'Tumma sinivihre�', 'Tummansininen', 'Indigo', 'Tummanharmaa',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tummanpunainen', 'Oranssi', 'Keltavihre�', 'Vihre�', 'Sinivihre�', 'Sininen', 'Siniharmaa', 'Harmaa-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Punainen', 'Vaaleanoranssi', 'Lime', 'Merenvihre�', 'Akvamariini', 'Vaaleansininen', 'Purppura', 'Harmaa-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Magenta', 'Kulta', 'Keltainen', 'Kirkkaanvihre�', 'Turkoosi', 'Taivaansininen', 'Luumu', 'Kirkkaanharmaa',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Roosa', 'Keltaisenruskea', 'Vaaleankeltainen', 'Vaaleanvihre�', 'Vaalea turkoosi', 'Kalpeansininen', 'Laventeli', 'Valkoinen',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&L�pin�kyv�', '&Automaattinen', '&Muu v�ri...', '&Oletus',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Tausta', 'V&�ri:', 'Sijainti', 'Tausta', 'Malliteksti',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ei k�yt�ss�', '&Venytetty', 'Kii&nte� vierekk�in', 'V&ierekk�in', '&Keskitetty',
  // Padding button
  '&Marginaalit...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'T�ytt�v�ri', '&K�yt� kohteeseen:', '&Muu v�ri...', 'Mar&ginaalit...',
  'Valitse v�ri',
  // [apply to:] text, paragraph, table, cell
  'teksti', 'kappale' , 'taulukko', 'solu',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Fontti', 'Fontti', 'Asettelu',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fontti:', '&Koko', 'Tyyli', '&Lihavoitu', 'K&ursivoitu',
  // Script, Color, Back color labels, Default charset
  'Ko&mentojono:', '&V�ri', '&Tausta:', '(Oletus)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Tehosteet', '&Alleviivaus', 'Yl�&viiva', '&Yliviivaus', '&Isot kirjaimet',
  // Sample, Sample text
  'Malli', 'Malliteksti',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Merkkiv�lit', '&V�listys:', '&Laajennettu', '&Tiivistetty',
  // Offset group-box, Offset label, Down, Up,
  'Pystysuuntainen siirtym�', '&Siirtym�:', '&Alas', '&Yl�s',
  // Scaling group-box, Scaling
  'Skaalaus', 'S&kaalaus:',
  // Sub/super script group box, Normal, Sub, Super
  'Indeksit', 'Ta&vallinen', 'Ala&indeksi', 'Yl&�indeksi',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Marginaalit', '&Yl�reuna:', '&Vasen:', '&Alareuna:', '&Oikea:', '&Synkronoi',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
'Lis�� hyperlinkki', 'Hyperlinkki', 'T&eksti:', '&Kohde:', '<<valinta>>',
  // cannot open URL
  'Ei voi avata sivua "%s"',
  // hyperlink properties button, hyperlink style 
  '&Mukauta...', '&Tyyli:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Hyperlinkin ominaisuudet', 'Normaaliv�rit', 'Aktiivisen v�rit',
  // Text [color], Background [color]
  '&Teksti:', 'T&austa',
  // Underline [color]
  '&Alleviivaus:', 
  // Text [color], Background [color] (different hotkeys)
  'T&eksti:', 'Ta&usta',
  // Underline [color], Attributes group-box,
  '&Alleviivaus:', 'M��ritteet',
  // Underline type: always, never, active (below the mouse)
  '&Alleviivaus:', 'Aina', 'Ei k�yt�ss�', 'Aktiivinen',  
  // Same as normal check-box
  'Kuten &normaalisti',
  // underline active links check-box
  '&Alleviivaa aktiiviset hyperlinkit',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Lis�� symboli', '&Fontti:', '&Merkist�:', 'Unicode',
  // Unicode block
  '&Lohko:',  
  // Character Code, Character Unicode Code, No Character
  'Merkkinumero: %d', 'Merkkinumero: Unicode %d', '(ei merkki�)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Lis�� taulukko', 'Taulukon koko', '&Sarakkeiden m��r�:', '&Rivien m��r�:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Taulukon asettelu', '&Automaattinen koko', '&Sovita ikkunaan', '&Mukautettu koko',
  // Remember check-box
  'Muista asettelut luotaessa &uusia taulukoita',
  // VAlign Form ---------------------------------------------------------------
  'Sijainti',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Ominaisuudet', 'Kuva', 'Sijainti ja koko', 'Rivi', 'Taulukko', 'Rivit', 'Solut',
  // Image Appearance, Image Misc, Number tabs
  'Ulkoasu', 'Muu', 'Numero',
  // Box position, Box size, Box appearance tabs
  'Sijainti', 'Koko', 'Ulkoasu',
  // Preview label, Transparency group-box, checkbox, label
  'Esikatselu:', 'L�pin�kyv�', '&L�pin�kyv�', 'L�pin�kyv� &v�ri:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Muu v�ri...', '&Tallenna...', 'Automaattinen',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Pystysuuntainen keskitys', '&Keskit�:',
  'alareuna perusviivalle', 'keskikohta perusviivalle',
  'yl�reuna rivin yl�s', 'alareuna rivin alas', 'keskikohta rivin keskelle',
  // align to left side, align to right side
  'vasemmalle', 'oikealle',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Siirtym�:', 'Sovita', '&Leveys:', '&Korkeus:', 'Oletuskoko: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Sovita', '&Palauta',
  // Inside the border, border, outside the border groupboxes
  'Reunuksen sis�puoli', 'Reunus', 'Reunuksen ulkopuoli',
  // Fill color, Padding (i.e. margins inside border) labels
  '&T�ytt�v�ri:', '&Marginaalit:',
  // Border width, Border color labels
  'Reunan &leveys:', 'Reunan &v�ri:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Vaakav�li:', '&Pystyv�li:',
  // Miscellaneous groupbox, Tooltip label
  'Muut', '&Vihje:',
  // web group-box, alt text
  'Selain', 'Vaihtoehtoinen &teksti:',
  // Horz line group-box, color, width, style
  'Vaakaviiva', '&V�ri:', '&Leveys:', '&Tyyli:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Taulukko', '&Leveys:', '&T�ytt�v�ri:', '&Solun riviv�li...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Vaakamarginaali:', '&Pystymarginaali:', 'Reunat ja tausta',
  // Visible table border sides button, auto width (in combobox), auto width label
  '&N�yt� taulukon reunat...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Pid� rivi yhdess�',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Kierto', '&Ei kiertoa', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Reunat:', '&N�yt� solujen reunat...',
  // Border, CellBorders buttons
  '&Taulukon reunat...', 'Solujen &reunat...',
  // Table border, Cell borders dialog titles
  'Taulukon reuna', 'Solujen reunat',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tulostus', '&Est� solujen jakaantuminen kahdelle sivulle', '&Ylh��ll� toistettavat rivit:',
  'Ylh��ll� toistettavat rivit toistetaan jokaisella tulostussivulla.',
  // top, center, bottom, default
  '&Yl�reuna', '&Keskell�', '&Alareuna', '&Oletus',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Asetukset', 'Suositeltu &leveys:', '&Minimikorkeus:', '&T�ytt�v�ri:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Reunat', 'N�kyv�t:',  'Va&rjon v�ri:', 'Valon v&�ri', '&V�ri:',
  // Background image
  '&Kuva...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Vaakasijainti', 'Pystysijainti', 'Sijainti teksteiss�',
  // Position types: align, absolute, relative
  'Kohdistettu', 'Absoluuttinen', 'Suhteellinen',
  // [Align] left side, center, right side
  'Vasen reuna seuraavan kohteen vasempaan reunaan:',
  'Keskipiste keskelle kohdetta',
  'Oikea reuna seuraavan kohteen oikeaan reunaan:',
  // [Align] top side, center, bottom side
  'Yl�reuna seuraavan kohteen yl�reunaan:',
  'Keskipiste keskelle kohdetta',
  'Alareuna seuraavan kohteen alareunaan:',
  // [Align] relative to
  'Suhteessa kohteeseen',
  // [Position] to the right of the left side of
  'Seuraavan kohteen oikealle puolelle:',
  // [Position] below the top side of
  'Seuraavan kohteen yl�reunaan:',
  // Anchors: page, main text area (x2)
  '&Sivu', '&Leip�teksti', 'Si&vu', 'Le&ip�teksti',
  // Anchors: left margin, right margin
  '&Vasen reunus', '&Oikea reunus',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Yl�reunus', '&Alareunus', '&Sisempi reunus', '&Ulompi reunus',
  // Anchors: character, line, paragraph
  '&Merkki', '&Rivi', '&Kappale',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&Peilikuvareunukset', '&Asettelu taulukon solussa',
  // Above text, below text
  'Tekstin &p��ll�', 'Tekstin &alla',
  // Width, Height groupboxes, Width, Height labels
  'Leveys', 'Korkeus', '&Leveys:', '&Korkeus:',
  // Auto height label, Auto height combobox item
  'Automaattinen', 'automaattinen',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Pystytasaus', '&Yl�s', '&Keskelle', '&Alas',
  // Border and background button and title
  '&Reunus ja t�ytt�...', 'Laatikon reunus ja t�ytt�',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Liit� m��r�ten', '&Liitet��n:', 'Muotoiltu teksti (RTF)', 'HTML-muoto',
  'Muotoilematon teksti', 'Muotoilematon Unicode-teksti', 'Kuva (bittikartta)', 'Kuva (Windows-metatiedosto)',
  'Kuva (EMF-muotoinen)',  'URL-osoite',
  // Options group-box, styles
  'Lis�valinnat', '&Tyylit:',
  // style options: apply target styles, use source styles, ignore styles
  'k�yt� valittuja tyylej�', 's�ilyt� tyylit ja ulkoasu',
  's�ilyt� ulkoasu, �l� huomioi tyylej�',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Luettelomerkit ja numerointi', 'Luettelomerkit', 'Numerointi', 'Luettelomerkkikirjasto', 'Numerointikirjasto',
  // Customize, Reset, None
  '&Mukauta...', '&Palauta', 'Ei mit��n',
  // Numbering: continue, reset to, create new
  'jatka numerointia', 'palauta numeroksi', 'luo uusi, aloita numerosta',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'A, B, C, ...', 'I, II, III, ...', '1, 2, 3, ...', 'a, b, c, ...', 'i, ii, iii, ...',
  // Bullet type
  'Ympyr�', 'Pallo', 'Neli�',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Taso:', '&Alkaen:', '&Jatka', 'Numerointi',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Mukautettu luettelo', 'Tasot', '&M��r�:', 'Ominaisuudet', '&Tyyppi:',
  // List types: bullet, image
  'Symboli', 'Kuva', 'Lis�� numero|',
  // Number format, Number, Start level from, Font button
  '&Numeron muotoilu:', 'Numero', '&Aloita numerointi tasosta:', '&Fontti...',
  // Image button, bullet character, Bullet button,
  '&Kuva...', '&Symboli', '&Luettelomerkki...',
  // Position of list text, bullet, number, image, text
  'Merkkikirjaston sijoittelu', 'Luettelomerkin sijoittelu', 'Numeron sijoittelu', 'Kuvan sijoittelu', 'Tekstin sijoittelu',
  // at, left indent, first line indent, from left indent
  'pos.', '&Vasen sisennys:', '&Ensimm�inen rivi:', '(Lis�sisennys)',
  // [only] one level preview, preview
  '&Yksitasoinen esikatselu', 'Esikatselu',
  // Preview text
  'Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Tasaa vasempaan', 'Tasaa oikeaan', 'Keskit�',
  // level #, this level (for combo-box)
  'Taso %d', 'Nykyinen taso',
  // Marker character dialog title
  'Mukautettu luettelo',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Kappaleiden reunat ja taustat', 'Reunat', 'Tausta',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Asetukset', '&V�ri:', '&Leveys:', '&Sis�inen leveys:', 'Teksti&reunukset...',
  // Sample, Border type group-boxes;
  'Esikatselu', 'Reunaviivatyyppi:',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ei mit��n', '&Yksi', '&Kaksi', 'K&olme', '&Paksu sis�reuna', 'Paksu &ulkoreuna',
  // Fill color group-box; More colors, padding buttons
  'T�ytt�', '&Muu v�ri...', '&Marginaalit...',
  // preview text
  'Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti.',
  // title and group-box in Offsets dialog
  'Tekstireunukset', 'Reunukset',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Kappale', 'Tasaus', '&Vasen', '&Oikea', '&Keskit�', '&Tasattu',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'V�lit', '&Yl�reuna:', '&Alareuna:', 'Riviv�li:', 'S&uuruus:',
  // Indents group-box; indents: left, right, first line
  'Sisennys', 'Vase&n:', 'O&ikea:', '&Ensimm�inen rivi:',
  // indented, hanging
  '&Sisennetty', '&Riippuva', 'Esikatselu',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '1.0', '1.5', '2.0', 'V�hint��n', 'Kiinte�', 'Mukautettu',
  // preview text
  'Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Sisennykset ja v�lit', 'Sarkaimet', 'Tekstin rivitys',
  // tab stop position label; buttons: set, delete, delete all
  '&Sijainti:', '&Aseta', '&Poista', 'Poista ka&ikki',
  // tab align group; left, right, center aligns,
  'Tasaus', '&Vasen', '&Oikea', '&Keskit�',
  // leader radio-group; no leader
  'T�ytt�merkki', '(Ei mit��n)',
  // tab stops to be deleted, delete none, delete all labels
  'Poistettavat sarkaimet:', '(Ei mit��n)', 'Kaikki',
  // Pagination group-box, keep with next, keep lines together
  'Tekstin rivitys', '&Est� rivijako seuraavalle sivulle', '&Sido seuraavaan',
  // Outline level, Body text, Level #
  '&J�sennystaso:', 'Leip�teksti', 'Taso %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Esikatselu', 'Sivun leveys', 'Yksi sivu', 'Sivut:',
  // Invalid Scale, [page #] of #
  'Sy�t� luku v�lilt� 10 - 500', '/ %d',
  // Hints on buttons: first, prior, next, last pages
  'Ensimm�inen', 'Edellinen', 'Seuraava', 'Viimeinen',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Solujen oletusv�listykset', 'V�lit', '&Solujen v�liss�', 'Taulukon reunuksen ja solujen v�liss�',
  // vertical, horizontal (x2)
  '&Pystysuunta:', '&Vaakasuunta:', 'P&ystysuunta:', 'V&aakasuunta:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Reunat', 'Asetukset', '&V�ri:', 'V&alov�ri:', 'Varjov&�ri:',
  // Width; Border type group-box;
  '&Leveys:', 'Reunatyyppi:',
  // Border types: none, sunken, raised, flat
  '&Ei mit��n', '&Upotettu', '&Korotettu', '&Tasainen',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Jaa', 'Jaa useisiin',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Valitse rivien ja sarakkeiden m��r�t', '&Alkuper�iset solut',
  // number of columns, rows, merge before
  '&Sarakkeita:', '&Rivej�:', '&Yhdist� ennen jakamista',
  // to original cols, rows check-boxes
  'A&lkuper�isen sarakejaon mukaisiksi', 'Al&kuper�isen rivijaon mukaisiksi',
  // Add Rows form -------------------------------------------------------------
  'Lis�� rivej�', 'Rivien m��r�:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Sivun asetukset', 'Sivu', 'Tunnisteet',
  // margins group-box, left, right, top, bottom
  'Reunukset (millimetri�)', 'Reunukset (tuumaa)', 'Va&sen:', '&Yl�reuna:', '&Oikea:', '&Alareuna:',
  // mirror margins check-box
  'P&eilikuvareunukset',
  // orientation group-box, portrait, landscape
  'Suunta', '&Pysty', '&Vaaka',
  // paper group-box, paper size, default paper source
  'Paperi', '&Koko:', '&L�hdelokero:',
  // header group-box, print on the first page, font button
  'Yl�tunniste', '&Teksti:', '&Ensimm�isen sivun yl�tunniste', '&Fontti...',
  // header alignments: left, center, right
  '&Vasemmalla', '&Keskell�', '&Oikealla',
  // the same for footer (different hotkeys)
  'Alatunniste', 'Te&ksti:', 'Ensimm�isen sivun alat&unniste', 'Fo&ntti...',
  'Va&semmalla', 'Keskell&�', 'Oikea&lla',
  // page numbers group-box, start from
  'Sivunumerot', '&Alkaen:',
  // hint about codes
  'Sallitut erikoislyhenteet:'#13'&&p - sivunumero; &&P - sivum��r�; &&d - tulostusp�iv�; &&t - tulostusaika',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Tiedostomuoto', '&Valitse merkist�koodaus:',
  // thai, japanese, chinese (simpl), korean
  'Thaimaalainen', 'Japanilainen', 'Kiinalainen (Yksinkertaistettu)', 'Korealainen',
  // chinese (trad), central european, cyrillic, west european
  'Kiinalainen (Perinteinen)', 'Keski- ja it�eurooppalainen', 'Kyrillinen', 'L�nsimainen',
  // greek, turkish, hebrew, arabic
  'Kreikkalainen', 'Turkkilainen', 'Heprealainen', 'Arabialainen',
  // baltic, vietnamese, utf-8, utf-16
  'Balttilainen', 'Vietnamilainen', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
   'Ulkoasu', '&Valitse tyyli:',
  // Delimiter form ------------------------------------------------------------
  // title, label
   'Muunna tekstiksi', 'Valitse &erotin:',
  // line break, tab, ';', ','
  'v�li', 'sarkain', 'puolipiste', 'pilkku',
  // Table sort form -----------------------------------------------------------
  // error message
   'Yhdistettyj� rivej� sis�lt�v�� taulukkoa ei voida lajitella',
  // title, main options groupbox
   'Taulukon lajittelu', 'Lajittelu',
  // sort by column, case sensitive
   '&Lajitteluperuste sarake:', '&Sama kirjainkoko',
  // heading row, range or rows
  'Tiedot sis�lt�v�t &otsikon', 'Lajiteltavat rivit:  %d - %d',
  // order, ascending, descending
  'J�rjestys', '&Nouseva', '&Laskeva',
  // data type, text, number
  'Tyyppi', '&Teksti', '&Luku',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Lis�� numero', 'Ominaisuudet', '&Laskurin nimi:', '&Numerointityyppi:',
  // numbering groupbox, continue, start from
  'Numerointi', '&Jatka', '&Alkaen luvusta:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Tunniste', '&Laji:', '&Piilota laji',
  // position radiogroup
  'Sijainti', 'Valitun kohteen &yl�puolelle', 'Valitun kohteen &alapuolelle',
  // caption text
  '&Tekstitunniste:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numero', 'Kuva', 'Taulukko',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'N�yt� reunukset', 'Reunus',
  // Left, Top, Right, Bottom checkboxes
  '&Vasemmalla', '&Yll�', '&Oikealla', '&Alla',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Sarakekoon uudelleenm��ritys', 'Rivikoon uudelleenm��ritys',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Ensimm�isen rivin sisennys', 'Vasen sisennys', 'Riippuva', 'Oikea sisennys',
  // Hints on lists: up one level (left), down one level (right)
  'Yksi taso ylemm�s', 'Yksi taso  alemmas',
  // Hints for margins: bottom, left, right and top
  'Alareunus', 'Oikea reunus', 'Vasen reunus', 'Yl�reunus',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Vasempaan tasaava sarkain', 'Oikeaan tasaava sarkain', 'Keskityssarkain', 'Desimaalisarkain',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Tavallinen', 'Tavallinen sisennys', 'Ei v�lej�', 'Otsikko %d', 'Luettelo',
  // Hyperlink, Title, Subtitle
  'Hyperlinkki', 'Otsikko', 'Aliotsikko',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Korostus', 'Heikko korostus', 'Voimakas korostus', 'Vahva',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Sitaatti', 'Korostettu sitaatti', 'Alaviite', 'Korostettu viite',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Lohkoteksti', 'HTML-muuttuja', 'HTML-koodi', 'HTML-akronyymi',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-m��ritys', 'HTML-n�pp�imist�', 'HTML-n�yte', 'HTML-kirjoituskone',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML-esimuotoiltu', 'HTML-sitaatti', 'Yl�tunniste', 'Alatunniste', 'Sivunumero',
  // Caption
  'Tekstitunniste',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Loppuviite', 'Alaviite', 'Loppuviitteen teksti', 'Alaviitteen teksti',
  // Sidenote Reference, Sidenote Text
  'Reunahuomautus', 'Huomautusteksti',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'v�ri', 'taustav�ri', 'l�pikuultava', 'oletus', 'alleviivausv�ri',
  // default background color, default text color, [color] same as text
  'oletustaustav�ri', 'oletustekstiv�ri', 'kuten teksti',
  // underline types: single, thick, double, dotted, thick dotted, dashed
   'yksinkertainen', 'paksu', 'kaksoisviiva', 'pisteviiva', 'paksu pisteviiva', 'katkoviiva',
  // underline types: thick dashed, long dashed, thick long dashed,
  'paksu katkoviiva', 'pitk� katkoviiva', 'pitk� paksu katkoviiva',
  // underline types: dash dotted, thick dash dotted,
   'pistekatkoviiva', 'paksu pistekatkoviiva',
  // underline types: dash dot dotted, thick dash dot dotted
  'kaksipistekatkoviiva', 'paksu kaksipistekatkoviiva',
  // sub/superscript: not, subsript, superscript
  'ei indeksej�', 'alaindeksi', 'yl�indeksi',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'kirjoitussuunta:', 'peritty', 'vasemmalta oikealle', 'oikealta vasemmalle',
  // bold, not bold
  'lihavointi', 'ei lihavointia',
  // italic, not italic
  'kursivointi', 'ei kursivointia',
  // underlined, not underlined, default underline
  'alleviivaus', 'ei alleviivausta', 'oletusalleviivaus',
  // struck out, not struck out
  'korostus', 'ei korostusta',
  // overlined, not overlined
  'yliviivaus', 'ei yliviivausta',
  // all capitals: yes, all capitals: no
  'isoin kirjaimin', 'ei isoin kirjaimin',
  // vertical shift: none, by x% up, by x% down
  'ei pystysiirtym��', 'pystysiirtym� %d%% yl�s', 'pystysiirtym� %d%% alas',
  // characters width [: x%]
  'merkkileveys',
  // character spacing: none, expanded by x, condensed by x
  'tavallinen kirjasinv�li', 'harvennettu kirjasinv�li %s', 'tiivistetty kirjasinv�li %s',
  // charset
  'merkist�',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'oletusfontti', 'oletuskappale', 'peritty',
  // [hyperlink] highlight, default hyperlink attributes
  'korostus', 'oletus',
  // paragraph aligmnment: left, right, center, justify
  'tasaa vasemmalle', 'tasaa oikealle', 'keskitetty', 'tasaa molemmat reunat',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'rivikorkeus: %d%%', 'riviv�li: %s', 'rivikorkeus: v�hint��n %s',
  'rivikorkeus: tasan %s',
  // no wrap, wrap
  'ei rivityst�', 'automaattinen rivitys',
  // keep lines together: yes, no
  'pid� rivit yhdess�', '�l� pid� rivej� yhdess�',
  // keep with next: yes, no
  'sido seuraavaan kappaleeseen', '�l� sido seuraavaan kappaleeseen',
  // read only: yes, no
  'vain luku', 'muokkaus sallittu',
  // body text, heading level x
  'ulkoreunus: leip�teksti', 'ulkoreunus: %d',
  // indents: first line, left, right
  'ensimm�isen rivin sisennys', 'vasen sisennys', 'oikea sisennys',
  // space before, after
  'v�lily�nti eteen', 'v�lily�nti j�lkeen',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'sarkaimet', 'ei sarkaimia', 'tasaa', 'vasen', 'oikea', 'keskit�',
  // tab leader (filling character)
  'sarkainmerkki',
  // no, yes
  'ei', 'kyll�',
  // [padding/spacing/side:] left, top, right, bottom
  'vasen', 'yl�reuna', 'oikea', 'alareuna',
  // border: none, single, double, triple, thick inside, thick outside
  'ei reunaviivaa', 'yksinkertainen', 'kaksoisviiva', 'kolmoisviiva', 'paksu sis�puolella', 'paksu ulkopuolella',
  // background, border, padding, spacing [between text and border]
  'tausta', 'reuna', 'marginaalit', 'v�lialue',
  // border: style, width, internal width, visible sides
  'tyyli', 'leveys', 'sis�inen leveys', 'n�yt� reunat',
  // style inspector -----------------------------------------------------------
  // title
  'Tyyliselain',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Tyyli', '(ei k�yt�ss�)', 'Kappale', 'Fontti', 'M��ritteet',
  // border and background, hyperlink, standard [style]
  'Reunat ja tausta', 'Hyperlinkki', '(vakio)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Tyylit', 'Tyyli',
  // name, applicable to,
  '&Nimi:', '&K�ytett�viss� kohteissa:',
  // based on, based on (no &),
  '&Pohjautuu tyyliin:', 'Pohjautuen:',
  //  next style, next style (no &)
 '&Seuraavassa kappaleessa k�ytett�v� tyyli:', 'Seuraavassa kappaleessa k�ytett�v� tyyli:',
  // quick access check-box, description and preview
  '&Pikasiirtym�', 'Kuvaus ja esikatselu:',
  // [applicable to:] paragraph and text, paragraph, text
  'Kappale ja teksti', 'Kappale', 'Teksti',
  // text and paragraph styles, default font
  'Teksti- ja kappaletyyli', 'Oletusfontti',
  // links: edit, reset, buttons: add, delete
  'Muokkaa', 'Palauta', '&Lis��', '&Poista',
  // add standard style, add custom style, default style name
  'Lis�� &vakiotyyli...', 'Lis�� &mukautettu tyyli', 'Tyyli %d',
  // choose style
  'Valitse &k�ytett�v� tyyli:',
  // import, export,
  '&Tuo...', '&Vie...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView-tyylit (*.rvst)|*.rvst', 'Virhe ladattaessa tiedostoa', 'Tiedosto ei ole tyylitiedosto',
  // Title, group-box, import styles
  'Tuo tyylit tiedostosta', 'Tuonti', '&Tuo tyylit:',
  // existing styles radio-group: Title, override, auto-rename
  'Olemassaolevat tyylit', '&korvaa', '&lis�� uudestaannimettyn�',
  // Select, Unselect, Invert,
  '&Valitse', '&Poista valinta', '&Valitse k��nteisesti',
  // select/unselect: all styles, new styles, existing styles
  '&Kaikki tyylit', '&Uudet tyylit', '&Olemassaolevat tyylit',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Poista muotoilut', 'Kaikki tyylit',
  // dialog title, prompt
  'Tyylit', '&Valitse k�ytett�v� tyyli:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonyymit', '&Ohita kaikki', '&Lis�� sanastoon',
  // Progress messages ---------------------------------------------------------
  'Ladataan %s', 'Valmistellaan tulostusta...', 'Tulostetaan sivua %d',
  'Muunnetaan RTF-muodosta...',  'Muunnetaan RTF-muotoon...',
  'Luetaan %s...', 'Kirjoitetaan %s...', 'teksti',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ei merkint��', 'Alaviite', 'Loppuviite', 'Reunahuomautus', 'Tekstiruutu'  
  );


initialization
  RVA_RegisterLanguage(
    'Finnish', // english language name, do not translate
    'Suomi', // native language name
    ANSI_CHARSET, @Messages);

end.
