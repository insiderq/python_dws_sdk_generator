﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVNormalize.pas' rev: 27.00 (Windows)

#ifndef RvnormalizeHPP
#define RvnormalizeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvnormalize
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall NormalizeRichView(Crvdata::TCustomRVData* RVData, int FirstItemNo = 0x0, bool Recursive = true);
}	/* namespace Rvnormalize */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVNORMALIZE)
using namespace Rvnormalize;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvnormalizeHPP
