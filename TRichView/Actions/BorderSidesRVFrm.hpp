﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'BorderSidesRVFrm.pas' rev: 27.00 (Windows)

#ifndef BordersidesrvfrmHPP
#define BordersidesrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Bordersidesrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVBorderSides;
class PASCALIMPLEMENTATION TfrmRVBorderSides : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Rvstyle::TRVStyle* rvs;
	Vcl::Comctrls::TPageControl* PageControl1;
	Vcl::Comctrls::TTabSheet* ts1;
	Vcl::Comctrls::TTabSheet* ts2;
	Vcl::Stdctrls::TGroupBox* gbSample;
	Vcl::Buttons::TSpeedButton* btnLeft;
	Vcl::Buttons::TSpeedButton* btnRight;
	Vcl::Buttons::TSpeedButton* btnTop;
	Vcl::Buttons::TSpeedButton* btnBottom;
	Richview::TRichView* rv;
	Vcl::Stdctrls::TGroupBox* gbSettings;
	Vcl::Stdctrls::TLabel* lblColor;
	Vcl::Stdctrls::TLabel* lblWidth;
	Rvcolorcombo::TRVColorCombo* cmbColor;
	Vcl::Stdctrls::TComboBox* cmbWidth;
	void __fastcall cmbWidthDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall FormKeyDown(System::TObject* Sender, System::Word &Key, System::Classes::TShiftState Shift);
	void __fastcall btnClick(System::TObject* Sender);
	void __fastcall cmbColorColorChange(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
public:
	Vcl::Controls::TControl* _cmbWidth;
	Vcl::Controls::TControl* _btnLeft;
	Vcl::Controls::TControl* _btnTop;
	Vcl::Controls::TControl* _btnRight;
	Vcl::Controls::TControl* _btnBottom;
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVBorderSides(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVBorderSides(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVBorderSides(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVBorderSides(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Bordersidesrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BORDERSIDESRVFRM)
using namespace Bordersidesrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BordersidesrvfrmHPP
