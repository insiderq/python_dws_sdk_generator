﻿// This file is a copy of RVAL_Sv.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Swedish (Svenska) translation                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Mikael Collin, 2003-01-29              }
{                mikael.collin@robmatic.se              }
{                                                       }
{ Updates: 2014-04-26: by Alconost                      }
{          2012-09-23: by Alconost                      }
{          2011-09-19: by Alconost                      }
{          2011-03-10: by Alconost                      }
{          2007-11-14:                                  }
{          2004-04-26: Updated with tabs...             }
{          2003-08-19: Minor update.                    }
{          2003-08-14: Updated "bidi"-text, charcase.   }
{          2003-04-19: Added "Page Setup".              }
{          2003-01-30: Corrected the colornames.        }
{*******************************************************}

unit RVALUTF8_Sv;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'tum', 'cm', 'mm', 'pc', 'pixlar', 'pt',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc',  'pixlar', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Arkiv', '&Redigera', 'F&ormat', 'T&ecken', '&Stycke', '&Infoga', '&Tabell', '&Fönster', '&Hjälp',
  // exit
  'A&vsluta',
  // top-level menus: View, Tools,
  '&Visa', 'Verk&tyg',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Storlek', 'Stil', 'Väl&j', 'Justera &cellens innehåll', 'C&ellkanter',
  // menus: Table cell rotation
  'Cell&rotation',   
  // menus: Text flow, Footnotes/endnotes
  '&Textflöde', '&Fotnoter',
  // ribbon tabs: tab1, tab2, view, table
  '&Hem', '&Avancerat', '&Vy', '&Tabell',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Urklipp', 'Font', 'Paragraf', 'Lista', 'Redigering',
  // ribbon groups: insert, background, page setup,
  'Infoga', 'Bakgrund', 'Sidinställningar',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Länkar', 'Fotnoter', 'Dokumentvyer', 'Visa/Göm', 'Zooma', 'Alternativ',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Infoga', 'Radera', 'Operationer', 'Kanter',
  // ribbon groups: styles 
  'Stilar',
  // ribbon screen tip footer,
  'Tryck på F1 för mer hjälp',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Senast använda dokument', 'Spara en kopia på dokumentet', 'Förhandsgranska och skriv ut dokumentet',
  // ribbon label: units combo
  'Enheter:',          
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nytt', 'Nytt|Skapar ett nytt dokument',
  // TrvActionOpen
  '&Öppna...', 'Öppna|Öppnar ett dokument från disk',
  // TrvActionSave
  '&Spara', 'Spara|Sparar dokumentet till disk',
  // TrvActionSaveAs
  'Spara so&m...', 'Spara som...|Sparar dokumentet till disk med ett nytt namn',
  // TrvActionExport
  '&Exportera...', 'Exportera|Exporterar dokumentet till ett annat format',
  // TrvActionPrintPreview
  'För&handsgranska', 'Förhandsgranska|Visar dokumentet som det kommer att se ut när det är utskrivet',
  // TrvActionPrint
  '&Utskrift...', 'Utskrift|Låter dig ställa in inställningar för skrivare och skriva ut dokumentet',
  // TrvActionQuickPrint
  '&Skriv ut', '&Snabbutskrift', 'Skriv ut|Skriver ut dokumentet',
   // TrvActionPageSetup
  'Utskriftsforma&t...', 'Utskriftsformat|Sätter marginaler, pappersstorlek, orintering, källa, sidhuvud och sidfot',
  // TrvActionCut
  '&Klipp ut', 'Klipp ut|Klipper ut det som är markerat och lägger det i urklipp',
  // TrvActionCopy
  'K&opiera', 'Kopiera|Kopierar det som är markerat och lägger det i urklipp',
  // TrvActionPaste
  'K&listra in', 'Klistra in|Lägger in det som finns i urklipp',
  // TrvActionPasteAsText
  'Klistra in som &text', 'Klistra in som text|Infogar text från urklipp',  
  // TrvActionPasteSpecial
  'Klistra in spe&cial...', 'Klistra in special|Lägger in det som finns i urklipp med valt format',
  // TrvActionSelectAll
  '&Markera allt', 'Markera allt|Markerar allt i dokumentet',
  // TrvActionUndo
  '&Ångra', 'Ångra|Ångrar den senaste förändringen',
  // TrvActionRedo
  'G&ör om', 'Gör om|Gör om den senast ångrade förändringen',
  // TrvActionFind
  '&Sök...', 'Sök|Letar efter en specificerad text i dokumentet',
  // TrvActionFindNext
  'Sök &nästa', 'Sök nästa|Fortsätter den senaste sökningen',
  // TrvActionReplace
  '&Ersätt...', 'Ersätt|Letar efter och ersätter en specificerad text i dokumentet',
  // TrvActionInsertFile
  '&Fil...', 'Infoga fil|Lägger till innehållet från en fil i dokumentet',
  // TrvActionInsertPicture
  '&Bild...', 'Infoga bild|Lägger till en bild från disk',
  // TRVActionInsertHLine
  'Ho&risontell linje', 'Infoga horisontell Line|Infogar en horisontell linje',
  // TRVActionInsertHyperlink
  '&Hyperlänk...', 'Infoga hyperlänk|Infogar en hyperlänk',
  // TRVActionRemoveHyperlinks
  '&Ta bort hyperlänkar', 'Ta bort hyperlänkar|Tar bort alla hyperlänkar i den markerade texten',  
  // TrvActionInsertSymbol
  '&Symbol...', 'Infoga symbol|Infogar symbol',
  // TrvActionInsertNumber
  '&Nummer...', 'Infoga nummer|Infogar ett nummer',
  // TrvActionInsertCaption
  'Infoga &rubrik...', 'Infoga rubrik|Infogar en rubrik för markerat objekt',
  // TrvActionInsertTextBox
  '&Textfält', 'Infoga textfält|Infogar ett textfält',
  // TrvActionInsertSidenote
  '&Kommentar', 'Infoga kommentar|Infogar en kommentar som visas i ett textfält',
  // TrvActionInsertPageNumber
  '&Sidnummer', 'Infogar sidnummer|Infogar ett sidnummer',
  // TrvActionParaList
  'Punkter och &numrering...', 'Punkter och numrering|Lägger till eller ändrar punkter och numrering för markerade stycken',
  // TrvActionParaBullets
  '&Punkter', 'Punkter|Lägger till eller tar bort punkter i stycket',
  // TrvActionParaNumbering
  '&Numrering', 'Numrering|Lägger till eller tar bort numrering i stycket',
  // TrvActionColor
  'Bakgrunds&färg...', 'Bakgrundsfärg|Väljer en bakgrundsfärg för dokumentet',
  // TrvActionFillColor
  '&Fyllningsfärg...', 'Fyllningsfärg|Väljer bakgrundsfärg för text, stycke, cell, tabell eller dokument',
  // TrvActionInsertPageBreak
  '&Infoga sidbrytning', 'Infoga sidbrytning|Infogar sidbrytning',
  // TrvActionRemovePageBreak
  '&Ta bort sidbrytning', 'Ta bort sidbrytning|Tar bort sidbrytning',
  // TrvActionClearLeft
  'Rensa textflödet på &vänster sida', 'Rensa textflödet på vänster sida|Placerar denna paragraf under alla vänsterställda bilder',
  // TrvActionClearRight
  'Rensa textflödet på &höger sida', 'Rensa textflödet på höger sida|Placerar denna paragraf under alla högerställda bilder',
  // TrvActionClearBoth
  'Rensa textflödet på &båda sidor', 'Rensa textflödet på båda sidor|Placerar denna paragraf under alla vänster- eller högerställda bilder',
  // TrvActionClearNone
  '&Normalt textflöde', 'Normalt textflöde|Tillåter textflöde runt både vänster- och högerställda bilder',
  // TrvActionVAlign
  '&Objektposition...', 'Objektposition|Ändrar position på det valda objektet',    
  // TrvActionItemProperties
  'Objekt&egenskaper...', 'Objektegenskaper|Definerar egenskaper för det aktiva objektet',
  // TrvActionBackground
  '&Bakgrund...', 'Bakgrund|Väljer bakgrundsfärg och bild',
  // TrvActionParagraph
  '&Stycke...', 'Stycke|Ändrar inställningarna för stycket',
  // TrvActionIndentInc
  '&Öka indrag', 'Öka indrag|Ökar indraget för markerade stycken',
  // TrvActionIndentDec
  '&Minska indrag', 'Minska indrag|Minskar indraget för markerade stycken',
  // TrvActionWordWrap
  '&Radbrytning', 'Radbrytning|Växlar mellan radbrytning av ord för markerade stycken',
  // TrvActionAlignLeft
  '&Vänsterjustera', 'Vänsterjustera|Justerar den markerade texten till vänster',
  // TrvActionAlignRight
  '&Högerjustera', 'Högerjustera|Justerar den markerade texten till höger',
  // TrvActionAlignCenter
  '&Centrera', 'Centrera|Centrerar den markerade texten',
  // TrvActionAlignJustify
  '&Marginaljustera', 'Marginal justera|Justerar den markerade texten mot både vänster och höger sida',
  // TrvActionParaColor
  '&Bakgrundsfärg för stycke...', 'Bakgrundsfärg för stycke|Anger bakgrundsfärgen för stycken',
  // TrvActionLineSpacing100
  '&Enkelt radavstånd', 'Enkelt radavstång|Anger enkelt radavstånd',
  // TrvActionLineSpacing150
  '1.5 ra&davstånd', '1.5 radavstånd|Anger 1,5 i radavstånd',
  // TrvActionLineSpacing200
  '&Dubbelt radavstånd', 'Dubbelt radavstånd|Anger dubbelt radavstånd',
  // TrvActionParaBorder
  '&Kantlinjer och bakgrund för stycke...', 'Kantlinjer och bakgrund för stycke|Anger kantlinjer och bakgrund för markerade stycken',
  // TrvActionInsertTable
  '&Infoga tabell...', '&Tabell', 'Infoga tabell|Infogar en ny tabell',
  // TrvActionTableInsertRowsAbove
  'Infoga rad &ovanför', 'Infoga rad ovanför|Infogar rad ovanför de markerade cellerna',
  // TrvActionTableInsertRowsBelow
  'Infoga rad &under', 'Infoga rad under|Infogar en ny rad under de markerade cellerna',
  // TrvActionTableInsertColLeft
  'Infoga kolumn till &vänster', 'Infoga kolumn till vänster|Infogar en ny kolumn till vänster om de markerade cellerna',
  // TrvActionTableInsertColRight
  'Infoga kolumn till &höger', 'Infoga kolumn till höger|Infogar en ny kolumn till höger om de markerade cellerna',
  // TrvActionTableDeleteRows
  'Ta bort r&ader', 'Ta bort rader|Tar bort de markerade raderna',
  // TrvActionTableDeleteCols
  'Ta bort &kolumner', 'Ta bort kolumner|Tar bort kolumner med markerade celler',
  // TrvActionTableDeleteTable
  '&Ta bort tabell', 'Ta bort tabell|Tar bort tabell',
  // TrvActionTableMergeCells
  '&Sammanfoga celler', 'Sammanfoga celler|Sammanfogar markerade celler',
  // TrvActionTableSplitCells
  '&Dela celler...', 'Dela celler|Delar markerade celler',
  // TrvActionTableSelectTable
  'Markera &tabell', 'Markera tabell|Markerar tabell',
  // TrvActionTableSelectRows
  'Markera rad&er', 'Markera rader|Markerar rader',
  // TrvActionTableSelectCols
  'Markera kol&umner', 'Markera kolumner|Markerar kolumner',
  // TrvActionTableSelectCell
  'Markera c&ell', 'Markera cell|Markerar cell',
  // TrvActionTableCellVAlignTop
  'Justera cell &överkant', 'Justera cell mot överkant|Justerar cellens innehåll mot överkanten',
  // TrvActionTableCellVAlignMiddle
  'Justera cell mot &mitten', 'Justera cell mot mitten|Justerar cellens innehåll mot mitten',
  // TrvActionTableCellVAlignBottom
  'Justera cell &nederkant', 'Justera cell mot nederkant|Justerar cellens innehåll mot nederkanten',
  // TrvActionTableCellVAlignDefault
  '&Standard celljustering vertikalt', 'Standard celljustering vertikalt|Sätter standard celljustering vertikalt för de markerade cellerna',
  // TrvActionTableCellRotationNone
  '&Ingen cellrotering', 'Ingen cellrotering|Roterar cellens innehåll 0°',
  // TrvActionTableCellRotation90
  'Rotera cellen &90°', 'Rotera cellen 90°|Roterar cellens innehåll 90°',
  // TrvActionTableCellRotation180
  'Rotera cellen &180°', 'Rotera cellen 180°|Roterar cellens innehåll 180°',
  // TrvActionTableCellRotation270
  'Rotera cellen &270°', 'Rotera cellen 270°|Roterar cellens innehåll 270°',
  // TrvActionTableProperties
  'Tabell &egenskaper...', 'Tabellegenskaper|Ändrar på egenskaperna för den markerade tabellen',
  // TrvActionTableGrid
  'Visa &alla kantlinjer', 'Visa alla kantlinjer|Visar eller gömmer tabellens kantlinjer',
  // TRVActionTableSplit
  'De&la tabell', 'Dela tabell|Delar tabellen i två tabeller med början från den markerade raden',
  // TRVActionTableToText
  'Konvertera till te&xt...', 'Konvertera till text|Konverterar tabell till text',
  // TRVActionTableSort
  '&Sortera...', 'Sortera|Sorterar tabellens rader',
  // TrvActionTableCellLeftBorder
  '&Vänster kantlinje', 'Vänster kantlinje|Visar eller gömmer cellens vänstra kantlinje',
  // TrvActionTableCellRightBorder
  '&Höger kantlinje', 'Höger kantlinje|Visar eller gömmer cellens högra kantlinje',
  // TrvActionTableCellTopBorder
  '&Övre kantlinje', 'Övre kantlinje|Visar eller gömmer cellens övre kantlinje',
  // TrvActionTableCellBottomBorder
  '&Nedre kantlinje', 'Nedre kantlinje|Visar eller gömmer cellens nedre kantlinje',
  // TrvActionTableCellAllBorders
  '&Yttre kantlinje', 'Yttre kantlinje|Visar eller gömmer cellens alla kantlinjer',
  // TrvActionTableCellNoBorders
  '&Ingen kantlinje', 'Ingen kantlinje|Gömmer cellens alla kantlinjer',
  // TrvActionFonts & TrvActionFontEx
  '&Tecken...', 'Tecken|Ändrar teckensnitt på den markerade texten',
  // TrvActionFontBold
  '&Fet', 'Fet|Ändrar stilen på den markerade texten till fet',
  // TrvActionFontItalic
  '&Kursiv', 'Kursiv|Ändrar stilen på den markerade texten till kursiv',
  // TrvActionFontUnderline
  '&Understruken', 'Understruken|Ändrar stilen på den markerade texten till understruken',
  // TrvActionFontStrikeout
  '&Överstruken', 'Överstruken|Stryker över markerad text',
  // TrvActionFontGrow
  '&Förstora teckensnitt', 'Förstora teckensnitt|Förstorar teckensnitt på markerad text med 10%',
  // TrvActionFontShrink
  '&Minska teckensnitt', 'Minska teckensnitt|Minskar teckensnitt på markerad text med 10%',
  // TrvActionFontGrowOnePoint
  'F&örstora teckensnittet med 1 punkt', 'Förstora teckensnittet med 1 punkt|Förstorar teckensnittet med 1 punkt',
  // TrvActionFontShrinkOnePoint
  'M&inska teckensnittet med 1 punkt', 'Minska teckensnittet med 1 punkt|Minskar teckensnittet med 1 punkt',
  // TrvActionFontAllCaps
  '&Stora tecken', 'Stora tecken|Ändrar alla markerade tecken till stora tecken',
  // TrvActionFontOverline
  '&Linje ovanför', 'Linje ovanför|Ritar en linje ovanför markerad text',
  // TrvActionFontColor
  'Text&färg...', 'Textfärg|Ändrar färgen på markerad text',
  // TrvActionFontBackColor
  'Textba&kgrundsfärg...', 'Textbakgrundsfärg|Ändrar bakgrundsfärg på markerad text',
  // TrvActionAddictSpell3
  '&Stavningskontroll', 'Stavningskontroll|Kontrollerar stavningen',
  // TrvActionAddictThesaurus3
  '&Synonymer', 'Synonymer|Visar synonymer för markerat ord',
  // TrvActionParaLTR
  'Vänster till höger', 'Vänster till höger|Sätter vänster-till-höger textorientering för de markerade styckena',
  // TrvActionParaRTL
  'Höger till vänster', 'Höger till vänster|Sätter höger-till-vänster textorientering för de markerade styckena',
  // TrvActionLTR
  'Text från vänster till höger', 'Text från vänster till höger|Sätter vänster-till-höger orientering på markerad text',
  // TrvActionRTL
  'Text från höger till vänster', 'Text från höger till vänster|Sätter höger-till-vänster orientering på markerad text',
  // TrvActionCharCase
  'Skiftläge', 'Skiftläge|Ändrar mellan stora och små bokstäver på markerad text',
  // TrvActionShowSpecialCharacters
  '&Icke utskrivbara tecken', 'Icke utskrivbara tecken|Visar eller döljer icke utskrivbara tecken, som paragrafmarkeringar, tabbar och blanksteg',
  // TrvActionSubscript
  'I&ndex', 'Index|Konverterar den markerade texten till index',
  // TrvActionSuperscript
  'Exponent', 'Exponent|Konverterar den markerade texten till exponent',
  // TrvActionInsertFootnote
  '&Fotnot', 'Fotnot|Infogar en fotnot',
  // TrvActionInsertEndnote
  '&Slutnot', 'Slutnot|Infogar en slutnot',
  // TrvActionEditNote
  'R&edigera not', 'Redigera not|Påbörjar redigering av fotnoten eller slutnoten',
  '&Dölj', 'Dölj|Döljer eller visar det markerade fragmentet',    
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stilar...', 'Stilar|Öppnar stilhanteringsdialogen',
  // TrvActionAddStyleTemplate
  '&Lägg till stil...', 'Lägg till stil|Skapar en ny text eller paragrafstil',
  // TrvActionClearFormat,
  '&Rensa format', 'Rensa format|Rensar all text och paragrafformatering från det markerade fragmentet',
  // TrvActionClearTextFormat,
  'Rensa &textformat', 'Rensa textformat|Rensar all formatering från den markerade texten',
  // TrvActionStyleInspector
  'Stil&inspektör', 'Stilinspektör|Visar eller gömmer stilinspektören',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'OK', 'Avbryt', 'Stäng', 'Infoga', '&Öppna...', '&Spara...', '&Rensa', 'Hjälp', 'Ta bort',
  // Others  -------------------------------------------------------------------
  // percents
  'procent',
  // left, top, right, bottom sides
  'Vänster sida', 'Över', 'Höger sida', 'Under',
  // save changes? confirm title
  'Spara ändringar till %s?', 'Bekräfta',
  // warning: losing formatting
  '%s kan innehålla saker som inte är kompatibla med det valda formatet.'#13+
  'Vill du ändå spara i det här formatet?',
  // RVF format name
  'RichView-format',
  // Error messages ------------------------------------------------------------
  'Fel',
  'Fel när fil skulle laddas.'#13#13'Tänkbara anledningar:'#13'- formatet på den här filen stöds inte av det här programmet;'#13+
  '- filen är korrupt;'#13'- filen är öppnad och låst av ett annat program.',
  'Fel när bild skulle laddas.'#13#13'Tänkbara anledningar:'#13'- bilden är i ett format som inte stöds av det här programmet;'#13+
  '- filen är inte en bild;'#13+
  '- filen är korrupt;'#13'- filen är öppnad och låst av ett annat program.',
  'Del när fil skulle sparas.'#13#13'Tänkbara anledningar:'#13'- det finns inte tillräckligt mycke plats på hårddisken;'#13+
  '- disken är skrivskyddad;'#13'- flyttbart media är inte inserted;'#13+
  '- filen är öppnad och låst av ett annat program;'#13'- disken är korrupt.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView-filer (*.rvf)|*.rvf',  'RTF-filer (*.rtf)|*.rtf' , 'XML-filer (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Textfiler (*.txt)|*.txt', 'Textfiler - Unicode (*.txt)|*.txt', 'Textfiler - Automatisk (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML-filer (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Förenklad (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word-dokument (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Sökningen klar', 'Söksträngen ''%s'' kunde inte hittas.',
  // 1 string replaced; Several strings replaced
  '1 sträng ersatt.', '%d strängar ersatta.',
  // continue search from the beginning/end?
  'Slutet på dokumentet har nåtts, vill du fortsätta ifrån början?',
  'Början på dokumentet har nåtts, vill du fortsätta ifrån slutet?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Genomskinlig', 'Automatisk',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Svart', 'Brun', 'Khakigrön', 'Mörkgrön', 'Mörkblågrön', 'Mörkblå', 'Indigoblå', 'Grå-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Mörkröd', 'Orange', 'Olivgrön', 'Grön', 'Blågrön', 'Blå', 'Blågrå', 'Grå-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Röd', 'Ljusorange', 'Gröngul', 'Havsgrön', 'Turkos', 'Klarblå', 'Lila', 'Grå-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Guld', 'Gul', 'Ljusgrön', 'Ljus turkos', 'Himmelsblå', 'Plommon', 'Grå-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosenröd', 'Aprikos', 'Ljusgul', 'Pastellgrön', 'Pastellblå', 'Ljusblå', 'Lavendel', 'Vit',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'G&enomskinlig', '&Automatisk', '&Fler färger...', 'F&örvalt värde',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Bakgrund', 'F&ärg:', 'Position', 'Bakgrund', 'Exempel text.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ingen', '&Hela ytan', 'F&ixerad delning', '&Dela', 'C&entrera',
  // Padding button
  '&Utfyllnad...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Fyllningsfärg', '&Använd på:', '&Fler färger...', '&Utfyllnad...',
  'Välj en färg',
  // [apply to:] text, paragraph, table, cell
  'text', 'stycke', 'tabell', 'cell',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Font', 'Font', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Teckensnitt:', '&Storlek', 'Stil', '&Fet', '&Kursiv',
  // Script, Color, Back color labels, Default charset
  'Sk&ript:', '&Färg:', 'Ba&kgrund:', '(Förvalt värde)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekter', 'Understruken', '&Överstruken', '&Genomstruken', '&Versaler',
  // Sample, Sample text
  'Exempel', 'Exempeltext',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Teckenavstånd', '&Avstånd:', '&Expanderat', '&Ihoptryckt',
  // Offset group-box, Offset label, Down, Up,
  'Offset', '&Offset:', '&Ner', '&Upp',
  // Scaling group-box, Scaling
  'Skaländring', 'Sk&aländring:',
  // Sub/super script group box, Normal, Sub, Super
  'Index och exponent', '&Normal', 'In&dex', 'E&xponent',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Utfyllnad', '&Ovanför:', '&Vänster:', '&Under:', '&Höger:', '&Samma värden',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Infoga hyperlänk', 'Hyperlänk', 'T&ext:', '&Mål', '<<markering>>',
  // cannot open URL
  'Kan inte gå till "%s"',
  // hyperlink properties button, hyperlink style
  '&Skräddarsy...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Länkattribut', 'Normala färger', 'Aktiva färger',
  // Text [color], Background [color]
  '&Text:', '&Bakgrund',
  // Underline [color]
  'U&nderstryk:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'B&akgrund',
  // Underline [color], Attributes group-box,
  'U&nderstryk:', 'Attribut',
  // Underline type: always, never, active (below the mouse)
  '&Understryk:', 'alltid', 'aldrig', 'aktiv',
  // Same as normal check-box
  'Som &förvald',
  // underline active links check-box
  '&Understryk aktiva länkar',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Infoga symbol', '&Teckensnitt:', '&Teckenuppsättning:', 'Unicode',
  // Unicode block
  '&Block:',
  // Character Code, Character Unicode Code, No Character
  'Teckenkod: %d', 'Teckenkod: Unicode %d', '(inga tecken)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Infoga tabell', 'Tabellstorlek', 'Antal &kolumner:', 'Antal &rader:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabellayout', '&Automatisk storlek', 'Anpassa tabellen till &fönstret', 'Ändra tabellstorlek &manuellt',
  // Remember check-box
  'Kom ihåg &dimensionerna för nya tabeller',
  // VAlign Form ---------------------------------------------------------------
  'Position',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Egenskaper', 'Bild', 'Position och storlek', 'Linje', 'Tabell', 'Rader', 'Celler',
  // Image Appearance, Image Misc, Number tabs
  'Utseende', 'Övrigt', 'Nummer',
  // Box position, Box size, Box appearance tabs
  'Position', 'Storlek', 'Utseende',
  // Preview label, Transparency group-box, checkbox, label
  'Förhandsgranska:', 'Genomskinlighet', '&Genomskinlig', 'Genomskinlig &färg:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'Ä&ndra...', '&Spara...', 'Automatiskt',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikal justering', '&Justera:',
  'undre kanten till baslinjen av texten', 'mitten till baslinjen av texten',
  'övre kanten till topplinjen av texten', 'undre kanten till baslinjen av texten', 'mittenkanten till mittenlinjen av texten',
  // align to left side, align to right side
  'vänster sida', 'höger sida',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Förskjut:', 'Sträck ut', '&Bredd:', '&Höjd:', 'Förvald storlek: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Skala om &proportionerligt', '&Nollställ',
  // Inside the border, border, outside the border groupboxes
  'Inom markeringen', 'Kant', 'Utanför markeringen',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Fyllfärg:', '&Spaltfyllnad:',
  // Border width, Border color labels
  'Kant&bredd:', 'Kant&färg:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horisontalt mellanrum:', '&Vertikalt mellanrum:',
  // Miscellaneous groupbox, Tooltip label
  'Övrigt', '&Tooltip:',
  // web group-box, alt text
  'Webb', 'Alternativ &text:',
  // Horz line group-box, color, width, style
  'Horisontell linje', '&Färg:', '&Bredd:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabell', '&Bredd:', '&Fyllnadsfärg:', '&Cell&avstånd...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horisontell cellutfyllnad:', '&Vertikal cellutfyllnad:', 'Kant och bakgrund',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Synliga &kantsidor...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Behåll innehåll tillsammans',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotation', '&Ingen', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Kant:', 'Synliga &kantsidor...',
  // Border, CellBorders buttons
  '&Tabellkanter...', '&Cellkanter...',
  // Table border, Cell borders dialog titles
  'Tabellkanter', 'Förvalda cellkanter',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Skriver ut', '&Behåll tabellen på en sida', 'Antalet &huvudrader:',
  'Huvudraderna visas på varje sida med tabellen',
  // top, center, bottom, default
  'Mot &överkant', '&Centrera', 'Mot &nederkant', '&Förvalt värde',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Inställningar', 'Föredragen &bredd:', '&Minska höjd:', '&Fyllningsfärg:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kantlinje', 'Synliga sidor:', 'Skuggf&ärg:', '&Ljus färg', 'F&ärg:',
  // Background image
  'B&ild...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horisontell position', 'Vertikal position', 'Position i text',
  // Position types: align, absolute, relative
  'Justering', 'Absolut position', 'Relativ position',
  // [Align] left side, center, right side
  'vänster sida av fältet till vänster sida av',
  'mitten av fältet till mitten av',
  'höger sida av fältet till höger sida av',
  // [Align] top side, center, bottom side
  'ovanför fältet till ovanför',
  'mitten av fältet till mitten av',
  'nedanför fältet till nedanför',
  // [Align] relative to
  'relativt till',
  // [Position] to the right of the left side of
  'till höger om vänstra sidan av',
  // [Position] below the top side of
  'nedanför ovansidan av',
  // Anchors: page, main text area (x2)
  '&Sida', '&Huvudtextfält', 'S&ida', 'Huvudte&xtfält',
  // Anchors: left margin, right margin
  '&Vänstermarginalen', '&Högermarginalen',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Toppmarginal', '&Bottenmarginal', '&Innermarginal', '&Yttermarginal',
  // Anchors: character, line, paragraph
  '&Bokstav', 'L&inje', 'Para&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Spegelvän&da marginaler', 'La&yout i tabellcell',
  // Above text, below text
  '&Ovanför text', '&Nedanför text',
  // Width, Height groupboxes, Width, Height labels
  'Bredd', 'Höjd', '&Bredd:', '&Höjd:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikal justering', '&Topp', '&Mitten', '&Botten',
  // Border and background button and title
  'Kan&t och bakgrund...', 'Fältkant och bakgrund',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Klistra in special', '&Klistra in som:', 'RichText-format', 'HTML-format',
  'Text', 'Unicode-text', 'Bitmappsbild', 'Metafile-bild',
  'Grafiska filer', 'URL',
  // Options group-box, styles
  'Alternativ', '&Stilar:',
  // style options: apply target styles, use source styles, ignore styles
  'tillämpa måldokumentets stilar', 'behåll stilar och utseende',
  'behåll utseende, ignorera stilar',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Punkter och numrering', 'Punkter', 'Numrerad', 'Punktlista', 'Numrerad lista',
  // Customize, Reset, None
  '&Anpassa...', '&Rensa', 'Ingen',
  // Numbering: continue, reset to, create new
  'fortsätt numrering', 'sätt numrering till', 'skapa en ny lista, starta på',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Alfanumeriska gemener', 'Romerska gemener', 'Numrerade', 'Alfanumeriska versaler', 'Romerska versaler',
  // Bullet type
  'Cirkel', 'Disk', 'Fyrkant',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nivå:', '&Starta från:', '&Fortsätt', 'Numrering',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Anpassa lista', 'Nivåer', '&Antal:', 'Listegenskaper', '&Listtyp:',
  // List types: bullet, image
  'punkter', 'bild', 'Infoga nummer|',
  // Number format, Number, Start level from, Font button
  '&Nummerformat:', 'Nummer', '&Starta nivånumrering från:', '&Teckensnitt...',
  // Image button, bullet character, Bullet button,
  '&Bild...', 'Punktt&ecken', '&Punkter...',
  // Position of list text, bullet, number, image, text
  'Listans textposition', 'Punktposition', 'Nummerposition', 'Bildposition', 'Textposition',
  // at, left indent, first line indent, from left indent
  '&på:', 'V&änster indrag:', 'Indrag på &första raden:', 'indrag från vänster',
  // [only] one level preview, preview
  '&Förhandsgranska en nivå', 'Förhandsgranska',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'vänsterjustera', 'högerjustera', 'centrera',
  // level #, this level (for combo-box)
  'Nivå %d', 'Denna nivå',
  // Marker character dialog title
  'Ändra tecken för punktlista',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Kantlinje och bakgrund för stycke', 'Kantlinje', 'Bakgrund',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Inställningar', '&Färg:', '&Bredd:', 'Int&ern bredd:', 'O&ffset...',
  // Sample, Border type group-boxes;
  'Exempel', 'Typ av kantlinje',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ingen', '&Enkelt', '&Dubbel', '&Trippel', 'Tjock &insida', 'Tjock &utsida',
  // Fill color group-box; More colors, padding buttons
  'Fyllningsfärg', '&Fler färger...', '&Utfyllnad...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Offset', 'Offset på kantlinje',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Stycke', 'Justera', '&Vänster', '&Höger', '&Centrera', '&Marginaljustera',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Avstånd', '&Före:', '&Efter:', 'Rad&avstånd:', 'me&d:',
  // Indents group-box; indents: left, right, first line
  'Indrag', 'V&änster:', 'H&öger:', '&Första raden:',
  // indented, hanging
  '&Indrag', '&Hängande', 'Exempel',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Enkel', '1.5 rader', 'Dubbel', 'Åtminstone', 'Exakt', 'Multipel',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indrag och avstånd', 'Tabbar', 'Textflöde',
  // tab stop position label; buttons: set, delete, delete all
  '&Standardavstånd:', 'A&nge', 'Rad&era', 'Radera &alla',
  // tab align group; left, right, center aligns,
  'Justering', '&Vänster', '&Höger', '&Centrerad',
  // leader radio-group; no leader
  'Utfyllnadstecken', '(Inget)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabbstopp som ska tas bort:', '(Ingen)', 'Alla.',
  // Pagination group-box, keep with next, keep lines together
  'Numrering', '&Håll ihop med nästa', 'Håll ihop &rader',
  // Outline level, Body text, Level #
  '&Konturnivå:', 'Bodytext', 'Nivå %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Förhandsgranska', 'Sidbredd', 'Helsida', 'Sidor:',
  // Invalid Scale, [page #] of #
  'Ange ett nummer från 10 till 500', 'av %d',
  // Hints on buttons: first, prior, next, last pages
  'Förstasidan', 'Föregående sida', 'Nästa sida', 'Sista sidan',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Cellavstånd', 'Avstånd', '&Mellan celler', 'Från tabellkanten till cellerna',
  // vertical, horizontal (x2)
  '&Vertikal:', '&Horisontell:', 'V&ertikal:', 'H&orisontell:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kantlinjer', 'Inställningar', '&Färg:', '&Ljus färg:', '&Skuggfärg:',
  // Width; Border type group-box;
  '&Bredd:', 'Typ av kantlinjer',
  // Border types: none, sunken, raised, flat
  '&Ingen', '&Nedsänkt', '&Upphöjd', '&Plan',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Dela', 'Dela till',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Specificera antalet rader och kolumner', '&Originalceller',
  // number of columns, rows, merge before
  'Antal &kolumner:', 'Antal &rader:', '&Sammanfoga för delning',
  // to original cols, rows check-boxes
  'Dela till original ko&lumner', 'Dela till original ra&der',
  // Add Rows form -------------------------------------------------------------
  'Lägg till &rader', 'Rad&antal:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Utskriftsformat', 'Sida', 'Sidhuvud och sidfot',
  // margins group-box, left, right, top, bottom
  'Marginaler (millimeter)', 'Marginaler (tum)', '&Vänster:', '&Överkant:', '&Höger:', '&Nederkant:',
  // mirror margins check-box
  '&Speglade marginaler',
  // orientation group-box, portrait, landscape
  'Riktning', 'S&tående', '&Liggande',
  // paper group-box, paper size, default paper source
  'Papper', 'St&orlek:', '&Källa:',
  // header group-box, print on the first page, font button
  'Sidhuvud', '&Text:', '&Sidhuvud på förstasidan', 'Tec&kensnitt...',
  // header alignments: left, center, right
  '&Vänster', '&Centrera', '&Höger',
  // the same for footer (different hotkeys)
  'Sidfot', 'Te&xt:', 'Sidfot på f&örstasidan', 'T&eckensnitt...',
  'V&änster', 'Ce&ntrera', 'Hö&ger',
  // page numbers group-box, start from
  'Sidnummer', '&Börja på',
  // hint about codes
  'Kombinationer för specialtecken:'#13'&&p - sidnummer; &&P - antal sidor; &&d - dagens datum; &&t - aktuell tid.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Kodsida för textfil', '&Välj filens kodning:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japanska', 'Kinesiska (Förenklad)', 'Koreanska',
  // chinese (trad), central european, cyrillic, west european
  'Kinesiska (Traditionell)', 'Central- och östeuropeisk', 'Kyrillisk', 'Västeuropeisk',
  // greek, turkish, hebrew, arabic
  'Grekiska', 'Turkiska', 'Hebreiska', 'Arabiska',
  // baltic, vietnamese, utf-8, utf-16
  'Baltiska', 'Vietnamesiska', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Visuell stil', '&Välj stil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Konvertera till text', 'Välj &avgränsning:',
  // line break, tab, ';', ','
  'radbrytning', 'tabb', 'semikolon', 'komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'En tabell innehållandes sammanfogade rader kan ej sorteras',
  // title, main options groupbox
  'Tabellsortering', 'Sortering',
  // sort by column, case sensitive
  '&Sortera efter kolumn:', 'S&kiftlägeskänslig',
  // heading row, range or rows
  'Excludera &rubrikrad', 'Rader att sortera: från %d till %d',
  // order, ascending, descending
  'Ordning', 'Sti&gande', '&Fallande',
  // data type, text, number
  'Typ', '&Text', '&Nummer',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Infoga nummer', 'Egenskaper', '&Räknarnamn:', '&Numreringstyp:',
  // numbering groupbox, continue, start from
  'Numrering', 'F&ortsätt', '&Starta från:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Rubrik', '&Etikett:', '&Exkludera etikett från rubrik',
  // position radiogroup
  'Position', '&Ovanför markerat objekt', '&Nedanför markerat objekt',
  // caption text
  'Rubrik&text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numrering', 'Figur', 'Tabell',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Synliga kantsidor', 'Kant',
  // Left, Top, Right, Bottom checkboxes
  '&Vänster sida', '&Topp', '&Höger sida', '&Botten',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Ändra storlek på kolumnen', 'Ändra storlek på raden',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Indrag på första raden', 'Indrag från vänster', 'Hängande indrag', 'Indrag från höger',
  // Hints on lists: up one level (left), down one level (right)
  'Upp en nivå', 'Ner en nivå',
  // Hints for margins: bottom, left, right and top
  'Undre marginal', 'Vänster marginal', 'Höger marginal', 'Övre marginal',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Vänsterjusterad tab', 'Högerjusterad tab', 'Centrerad tab', 'Decimaljusterad tab',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normalt indrag', 'Inget mellanrum', 'Rubrik %d', 'Liststycke',
  // Hyperlink, Title, Subtitle
  'Hyperlänk', 'Titel', 'Undertitel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Betoning', 'Diskret betoning', 'Intensiv betoning', 'Stark',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citat', 'Intensivt citat', 'Diskret referens', 'Intensiv referens',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Textblock', 'HTML-variabel', 'HTML-kod', 'HTML-akronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-definition', 'HTML-tangentbord', 'HTML-test', 'HTML-skrivmaskin',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Förformaterad HTML', 'HTML-citation', 'Rubrik', 'Fotnot', 'Sidnummer',
  // Caption
  'Rubrik',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Slutnotsreferens', 'Fotnotsreferens', 'Slutnotstext', 'Fotnotstext',
  // Sidenote Reference, Sidenote Text
  'Referens sidnot', 'Text sidnot',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'färg', 'bakgrundsfärg', 'transparent', 'förvald', 'understrecksfärg',
  // default background color, default text color, [color] same as text
  'förvald bakgrundsfärg', 'förvald textfärg', 'samma som text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'enkel', 'fet', 'dubbel', 'prickad', 'prickad', 'streckad',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tjockt streckad', 'långt streckad', 'tjockt och långt streckad',
  // underline types: dash dotted, thick dash dotted,
  'streckprickad', 'tjockt streckprickad',
  // underline types: dash dot dotted, thick dash dot dotted
  'streck och prickprickad', 'tjockt streck och prickprickad',
  // sub/superscript: not, subsript, superscript
  'ej under/superskript', 'underskript', 'superskript',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di-läge:', 'nedärvt', 'vänster till höger', 'höger till vänster',
  // bold, not bold
  'fet', 'ej fet',
  // italic, not italic
  'kursiv', 'ej kursiv',
  // underlined, not underlined, default underline
  'understreckad', 'ej understreckad', 'förvald understreckad',
  // struck out, not struck out
  'överkryssad', 'ej överkryssad',
  // overlined, not overlined
  'överstruken', 'ej överstruken',
  // all capitals: yes, all capitals: no
  'endast versaler', 'versaler av',
  // vertical shift: none, by x% up, by x% down
  'utan vertikal förskjutning', 'förskjuten uppåt med %d%%', 'förskjuten nedåt med %d%%',
  // characters width [: x%]
  'bokstavsbredd',
  // character spacing: none, expanded by x, condensed by x
  'normalt mellanrum', 'ökat mellanrum med %s', 'minskat mellanrum med %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'förvalt typsnitt', 'förvald paragraf', 'nedärvt',
  // [hyperlink] highlight, default hyperlink attributes
  'markera', 'förvalda',
  // paragraph aligmnment: left, right, center, justify
  'vänsterställt', 'högerställt', 'centrerat', 'justerat',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'radhöjd: %d%%', 'radmellanrum: %s', 'radhöjd: åtminstone %s',
  'radhöjd: exakt %s',
  // no wrap, wrap
  'radbrytning av', 'radbrytning',
  // keep lines together: yes, no
  'håll ihop raderna', 'håll ej ihop raderna',
  // keep with next: yes, no
  'håll ihop med nästa stycke', 'håll inte ihop med nästa stycke',
  // read only: yes, no
  'skrivskyddad', 'redigerbar',
  // body text, heading level x
  'konturlinjenivå: brödtext', 'konturlinjenivå: %d',
  // indents: first line, left, right
  'första raden indragen', 'vänster indrag', 'höger indrag',
  // space before, after
  'mellanslag före', 'mellanslag efter',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabbar', 'inga', 'justering', 'vänster', 'höger', 'centrerad',
  // tab leader (filling character)
  'leader',
  // no, yes
  'nej', 'ja',
  // [padding/spacing/side:] left, top, right, bottom
  'vänster', 'topp', 'höger', 'botten',
  // border: none, single, double, triple, thick inside, thick outside
  'ingen', 'enkel', 'dubbel', 'trippel', 'fet invändigt', 'fet utvändigt',
  // background, border, padding, spacing [between text and border]
  'bakgrund', 'ram', 'spaltfyllnad', 'mellanrum',
  // border: style, width, internal width, visible sides
  'stil', 'bredd', 'intern bredd', 'synliga sidor',
  // style inspector -----------------------------------------------------------
  // title
  'Stilinspektör',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Ingen)', 'Stycke', 'Teckensnitt', 'Attribut',
  // border and background, hyperlink, standard [style]
  'Ram och bakgrund', 'Hyperlänk', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stilar', 'Stil',
  // name, applicable to,
  '&Namn:', 'Tillämplig &på:',
  // based on, based on (no &),
  '&Baserad på:', 'Baserad på:',
  //  next style, next style (no &)
  'Stil för &följande stycke:', 'Stil för följande paragraf:',
  // quick access check-box, description and preview
  '&Snabbtillgång', 'Beskrivning och &förhandsgranskning:',
  // [applicable to:] paragraph and text, paragraph, text
  'stycke och text', 'stycke', 'text',
  // text and paragraph styles, default font
  'Text och styckestilar', 'Förvalt teckensnitt',
  // links: edit, reset, buttons: add, delete
  'Redigera', 'Nollställ', '&Lägg till', '&Radera',
  // add standard style, add custom style, default style name
  'Lägg till &standardstil...', 'Lägg till &anpassad stil', 'Stil %d',
  // choose style
  'Välj &stil:',
  // import, export,
  '&Importera...', '&Exportera...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView-stilar (*.rvst)|*.rvst', 'Fel vid filladdning', 'Filen innehåller inga stilar',
  // Title, group-box, import styles
  'Importera stilar från fil', 'Importera', 'I&mportera stilar:',
  // existing styles radio-group: Title, override, auto-rename
  'Existerande stilar', '&upphäv', '&lägg till ändrat namn',
  // Select, Unselect, Invert,
  '&Markera', '&Avmarkera', '&Invertera',
  // select/unselect: all styles, new styles, existing styles
  '&Alla stilar', '&Nya stilar', '&Existerande stilar',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Rensa format', 'Alla stilar',
  // dialog title, prompt
  'Stilar', '&Välj stil för tillämpning:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonymer', '&Ignorera Alla', '&Lägg till i lexikon',
  // Progress messages ---------------------------------------------------------
  'Laddar ner %s', 'Förbereder för utskrift...', 'Skriver ut sidan %d',
  'Konverterar från RTF...',  'Konverterar till RTF...',
  'Läser %s...', 'Skriver %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ingen notering', 'Fotnot', 'Slutnot', 'Sidonot', 'Textfält'  
  );


initialization
  RVA_RegisterLanguage(
    'Swedish', // english language name, do not translate
    'Svenska', // native language name
    ANSI_CHARSET, @Messages);

end.

