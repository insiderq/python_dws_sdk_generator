﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'HypRVFrm.pas' rev: 27.00 (Windows)

#ifndef HyprvfrmHPP
#define HyprvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVStyleFuncs.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Hyprvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVHyp;
class PASCALIMPLEMENTATION TfrmRVHyp : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TLabel* lblText;
	Vcl::Stdctrls::TEdit* txtText;
	Vcl::Stdctrls::TLabel* lblTarget;
	Vcl::Stdctrls::TEdit* txtTarget;
	Vcl::Stdctrls::TButton* btnHypProp;
	Vcl::Stdctrls::TLabel* lblStyle;
	Vcl::Stdctrls::TComboBox* cmbStyle;
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall txtTargetChange(System::TObject* Sender);
	void __fastcall btnHypPropClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _btnOk;
	void __fastcall UpdateOKButton(void);
	
public:
	Vcl::Controls::TControl* _txtText;
	Vcl::Controls::TControl* _txtTarget;
	Vcl::Controls::TControl* _cmbStyle;
	Vcl::Controls::TControl* _lblStyle;
	Vcl::Controls::TControl* _btnHypProp;
	System::Uitypes::TColor TextColor;
	System::Uitypes::TColor HoverColor;
	System::Uitypes::TColor BackColor;
	System::Uitypes::TColor UnderlineColor;
	System::Uitypes::TColor HoverBackColor;
	System::Uitypes::TColor HoverUnderlineColor;
	bool Underline;
	Rvstyle::TRVHoverEffects Effects;
	DYNAMIC void __fastcall Localize(void);
	void __fastcall Init(Rvedit::TCustomRichViewEdit* rve, Rvstyle::TRVStyleTemplate* StyleTemplate = (Rvstyle::TRVStyleTemplate*)(0x0));
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVHyp(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVHyp(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVHyp(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVHyp(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Hyprvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_HYPRVFRM)
using namespace Hyprvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// HyprvfrmHPP
