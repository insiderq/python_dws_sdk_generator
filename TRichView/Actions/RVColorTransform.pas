//Some procs from ImageTransform unit
//Copyright(c) original version by Rivarez(rivarez@list.ru)
unit RVColorTransform;

{$I RV_Defs.inc}

interface

uses Windows, Graphics;

procedure SaturationBitmap(Const  Bmp :TBitmap; Const  Amount :Integer);
procedure SaturationColors(Var  Colors :Array of TColor; Const  Amount :Integer);
procedure ColorizeBitmapAlter(Const  Bmp :TBitmap; Const  CColor :TColor);
procedure ColorizeColorsAlter(Var  Colors :Array of TColor; Const  CColor :TColor);
procedure InvertBitmap(bmp: TBitmap);
procedure DrawAlphaBitmap(bmp, target: TBitmap; X,Y: Integer);

implementation
uses Math;

Type  TRGBArray   = Array[0..32768] of TRGBTriple;
      TRGBAArray   = Array[0..32768] of TRGBQuad;
      TGraysArray = Array[0..767]   of Integer;
      TAlphaArray = Array[0..255]   of Word;
      TTableArray = Array[0..255]   of TRGBTriple;

Function IntToByte(Const i :Integer) :Byte;
Begin
  If i>255 then     Result:=255
  Else  If i<0 then Result:=0
  Else              Result:=i;
end;

Procedure PrepareGraysAndAlpha(Var  Grays :TGraysArray; Var  Alpha :TAlphaArray; Const  Amount :Integer);
Var  Gray, x, i :Integer;
Begin
  x:=0;
  For i:=0 to 255 do
    begin
      Alpha[i]:=(i*Amount) shr 8;
      Gray:=i-Alpha[i];
      Grays[x]:=Gray; Inc(x);
      Grays[x]:=Gray; Inc(x);
      Grays[x]:=Gray; Inc(x);
    end;
end;

//Note: use only pf24bit or pf32bit PixelFormat bitmaps
Procedure SaturationBitmap(Const  Bmp :TBitmap; Const  Amount :Integer);
Var
  pY :^TRGBArray;
  pY2 :^TRGBAArray;  
  Gray, x, y :Integer;
  Grays :TGraysArray;
  Alpha :TAlphaArray;
Begin
  PrepareGraysAndAlpha(Grays,Alpha,Amount);
  With Bmp do
    case PixelFormat of
      pf24bit:
        For y:=0 to Height-1 do begin
          pY:=ScanLine[y];
          For x:=0 to Width-1 do
            With pY^[x] do begin
              Gray:=Grays[rgbtRed+rgbtGreen+rgbtBlue];
              rgbtRed:=IntToByte(Gray+Alpha[rgbtRed]);
              rgbtGreen:=IntToByte(Gray+Alpha[rgbtGreen]);
              rgbtBlue:=IntToByte(Gray+Alpha[rgbtBlue]);
            end;
        end;
      pf32bit:
        For y:=0 to Height-1 do begin
          pY2:=ScanLine[y];
          For x:=0 to Width-1 do
            With pY2^[x] do begin
              Gray:=Grays[rgbRed+rgbGreen+rgbBlue];
              rgbRed:=IntToByte(Gray+Alpha[rgbRed]);
              rgbGreen:=IntToByte(Gray+Alpha[rgbGreen]);
              rgbBlue:=IntToByte(Gray+Alpha[rgbBlue]);
            end;
        end;
    end;
end;

//Note: use only pf24bit PixelFormat bitmaps
Procedure InvertBitmap(Bmp :TBitmap);
Var
  pY :^TRGBArray;
  y,x: Integer;
Begin
  With Bmp do
    For y:=0 to Height-1 do
      begin
        pY:=ScanLine[y];
        For x:=0 to Width-1 do
          With pY^[x] do
            begin
              rgbtRed   := not rgbtRed;
              rgbtGreen := not rgbtGreen;
              rgbtBlue  := not rgbtBlue;
            end;
      end;
end;

Procedure SaturationColors(Var  Colors :Array of TColor; Const  Amount :Integer);
Var
  Gray, i :Integer;
  r, g, b :TColor;
  Grays :TGraysArray;
  Alpha :TAlphaArray;
Begin
  PrepareGraysAndAlpha(Grays,Alpha,Amount);
  For i:=0 to High(Colors) do
    begin
      r:=Lo(Colors[i]); g:=Lo(Colors[i] shr 8 ); b:=Lo(Colors[i] shr 16);
      Gray:=Grays[r+g+b];
      r:=IntToByte(Gray+Alpha[r]);
      g:=IntToByte(Gray+Alpha[g]);
      b:=IntToByte(Gray+Alpha[b]);
      Colors[i]:=r or (g shl 8) or (b shl 16);
    end;
end;

Procedure PrepareTable(Var  Table :TTableArray; CColor :TColor);
Var  i, j :Integer;
     r, g, b :TColor;
Begin
  CColor:=ColorToRGB(CColor);
  r:=Lo(CColor); g:=Lo(CColor shr 8); b:=Lo(CColor shr 16);
  r:=r shr 2; g:=g shr 2; b:=b shr 2;
  For i:=0 to 255 do  With Table[i] do
    begin
      j:=i-(i shr 3);
      rgbtRed:=IntToByte(j+r);
      rgbtGreen:=IntToByte(j+g);
      rgbtBlue:=IntToByte(j+b);
    end;
end;

//Alternate way to Colorize - lighter colors in CColor param gives the better result than standard Colorize method
//but processing with darker colors in CColor gives the lighter result. Therefore processing with some colors need
//the saturation increase.
//Notes:
//    - to take a "clean" colorize effect(like in graphics editors) with a coloured bitmap, you must set the bitmap
//      Saturation to null before(to convert a colour bitmap to greyscale);
//    - use only pf24bit and pf32bit PixelFormat bitmaps
Procedure ColorizeBitmapAlter(Const  Bmp :TBitmap; Const  CColor :TColor);
Var
  x, y :Integer;
  pY :^TRGBArray;
  pY2:^TRGBAArray;
  TransR, TransG, TransB :TColor;
  Table :TTableArray;
Begin
  PrepareTable(Table,CColor);
  TransR:=-1; TransG:=-1; TransB:=-1;
  With Bmp do
    begin
      If Transparent then
        begin
          TransR:=Lo(TransparentColor); TransG:=Lo(TransparentColor shr 8); TransB:=Lo(TransparentColor shr 16);
        end;
      case PixelFormat of
        pf24bit:
          for y:=0 to Height-1 do
          begin
            pY:=ScanLine[y];
            For x:=0 to Width-1 do
              With pY^[x] do
                If (rgbtRed<>TransR) or (rgbtGreen<>TransG) or (rgbtBlue<>TransB) then
                  begin
                    rgbtRed:=Table[rgbtRed].rgbtRed;
                    rgbtGreen:=Table[rgbtGreen].rgbtGreen;
                    rgbtBlue:=Table[rgbtBlue].rgbtBlue;
                    //avoiding getting transparent color from non transparent
                    If (rgbtRed=TransR) and (rgbtGreen=TransG) and (rgbtBlue=TransB) then
                      If rgbtBlue<>0 then dec(rgbtBlue) Else inc(rgbtBlue);
                  end;
          end;
        pf32bit:
          for y:=0 to Height-1 do
          begin
            pY2:=ScanLine[y];
            For x:=0 to Width-1 do
              With pY2^[x] do
                If (rgbRed<>TransR) or (rgbGreen<>TransG) or (rgbBlue<>TransB) then
                  begin
                    rgbRed:=Table[rgbRed].rgbtRed;
                    rgbGreen:=Table[rgbGreen].rgbtGreen;
                    rgbBlue:=Table[rgbBlue].rgbtBlue;
                    //avoiding getting transparent color from non transparent
                    If (rgbRed=TransR) and (rgbGreen=TransG) and (rgbBlue=TransB) then
                        If rgbBlue<>0 then dec(rgbBlue) Else inc(rgbBlue);
                  end;
          end;
      end;
    end;
end;

Procedure ColorizeColorsAlter(Var  Colors :Array of TColor; Const  CColor :TColor);
Var  i :Integer;
     r, g, b :TColor;
     Table :TTableArray;
Begin
  PrepareTable(Table,CColor);
  For i:=0 to High(Colors) do
    begin
      r:=Lo(Colors[i]); g:=Lo(Colors[i] shr 8 ); b:=Lo(Colors[i] shr 16);
      r:=Table[r].rgbtRed; g:=Table[g].rgbtGreen; b:=Table[b].rgbtBlue;
      Colors[i]:=r or (g shl 8) or (b shl 16);
    end;
end;

procedure DrawAlphaBitmap(bmp, target: TBitmap; X,Y: Integer);
var  pYdst:^TRGBArray;
     pcYdst: PRGBTriple;
     pYsrc:^TRGBAArray;
     pcYsrc: PRGBQuad;
     Y2,X2,XIndex,YIndex: Integer;
begin
  if (bmp.PixelFormat<>pf32Bit) or (target.PixelFormat<>pf24bit) or
     (X>=target.Width) or (Y>=target.Height) then
    exit;
  X2 := Min(X+bmp.Width, target.Width)-1;
  Y2 := Min(Y+bmp.Height, target.Height)-1;
  for YIndex := Y to Y2 do begin
    pYdst := target.ScanLine[YIndex];
    pYsrc := bmp.ScanLine[YIndex-Y];
    for XIndex := X to X2 do begin
      pcYdst := @pYdst[XIndex];
      pcYsrc := @pYsrc[XIndex-X];
      pcYdst.rgbtRed := Min(255, Round((pcYsrc.rgbRed*pcYsrc.rgbReserved+
        pcYdst.rgbtRed*(255-pcYsrc.rgbReserved))/255));
      pcYdst.rgbtGreen := Min(255, Round((pcYsrc.rgbGreen*pcYsrc.rgbReserved+
        pcYdst.rgbtGreen*(255-pcYsrc.rgbReserved))/255));
      pcYdst.rgbtBlue := Min(255, Round((pcYsrc.rgbBlue*pcYsrc.rgbReserved+
        pcYdst.rgbtBlue*(255-pcYsrc.rgbReserved))/255));
    end;
  end;
end;

end.