unit TableSortRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseRVFrm, StdCtrls, ExtCtrls, RVALocalize;

type
  TfrmRVTableSort = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    rgOrder: TRadioGroup;
    rgDataType: TRadioGroup;
    gb: TGroupBox;
    lblCol: TLabel;
    cmbCol: TComboBox;
    cbCaseSensitive: TCheckBox;
    cbHeading: TCheckBox;
    lblRows: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cbHeadingClick(Sender: TObject);
  private
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _cmbCol, _cbHeading: TControl;
    _rgOrder, _rgDataType, _cbCaseSensitive, _lblRows: TControl;
    MinRowNumber, FirstRow, LastRow: Integer;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    procedure UpdateRowsLabel;
    procedure Localize; override;
  end;


implementation

uses RichViewActions;

{$R *.DFM}


{$IFDEF RVASKINNED}
procedure TfrmRVTableSort.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _rgOrder then
    _rgOrder := NewControl
  else if OldControl = _rgDataType then
    _rgDataType := NewControl
  else if OldControl = _cmbCol then
    _cmbCol := NewControl
  else if OldControl = _cbCaseSensitive then
    _cbCaseSensitive := NewControl
  else if OldControl = _cbHeading then
    _cbHeading := NewControl
  else if OldControl = _lblRows then
    _lblRows := NewControl;
end;
{$ENDIF}

procedure TfrmRVTableSort.FormCreate(Sender: TObject);
begin
  _rgOrder    := rgOrder;
  _rgDataType := rgDataType;
  _cmbCol     := cmbCol;
  _cbCaseSensitive := cbCaseSensitive;
  _cbHeading  := cbHeading;
  _lblRows    := lblRows;
  inherited;
end;

function TfrmRVTableSort.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := gb.Left*2+gb.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-rgDataType.Top-rgDataType.Height);
  Result := True;
end;

procedure TfrmRVTableSort.UpdateRowsLabel;
var r: Integer;
begin
  r := FirstRow;
  if GetCheckBoxChecked(_cbHeading) then
    inc(r);
  SetControlCaption(_lblRows,
    RVAFormat(RVA_GetS(rvam_ts_Rows, ControlPanel), [r+1, LastRow+1]));
end;

procedure TfrmRVTableSort.cbHeadingClick(Sender: TObject);
begin
  UpdateRowsLabel;
end;

procedure TfrmRVTableSort.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_ts_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  gb.Caption := RVA_GetSAsIs(rvam_ts_MainOptionsGB, ControlPanel);
  lblCol.Caption := RVA_GetSAsIs(rvam_ts_SortByCol, ControlPanel);
  cbCaseSensitive.Caption := RVA_GetSAsIs(rvam_ts_CaseSensitive, ControlPanel);
  cbHeading.Caption := RVA_GetSAsIs(rvam_ts_ExcludeHeadingRow, ControlPanel);
  rgOrder.Caption := RVA_GetSHAsIs(rvam_ts_OrderRG, ControlPanel);
  rgOrder.Items[0] := RVA_GetSAsIs(rvam_ts_Ascending, ControlPanel);
  rgOrder.Items[1] := RVA_GetSAsIs(rvam_ts_Descending, ControlPanel);
  rgDataType.Caption := RVA_GetSHAsIs(rvam_ts_DataType, ControlPanel);
  rgDataType.Items[0] := RVA_GetSAsIs(rvam_ts_Text, ControlPanel);
  rgDataType.Items[1] := RVA_GetSAsIs(rvam_ts_Number, ControlPanel);
end;

end.
