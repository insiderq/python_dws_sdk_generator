
{*******************************************************}
{                                                       }
{       RichViewActions (ThemeEngine version)           }
{       Demo project.                                   }
{       You can use it as a basis for your              }
{       applications.                                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{       ThemeEngine (c) KSDev http://www.ksdev.com      }
{                                                       }
{*******************************************************}


unit Unit1TE;

interface

{
  Remove dots from the defines below to enable support for Gif and Png.

  GifImage
    For Delphi 4-2006: TGifImage by Anders Melander
      http://www.torry.net/vcl/graphics/gif/gifimage.exe (original)
      http://www.trichview.com/resources/thirdparty/gifimage.zip (update)
    For Delphi 2007+, the built in TGifImage

  PngObject/PngImage
    For Delphi 4-2007: TPngObject by Gustavo Huffenbacher Daud)
      http://www.trichview.com/resources/thirdparty/pngimage.zip
    For Delphi 2009+, the built in TPngImage
}

{.$DEFINE USE_GIFIMAGE}
{.$DEFINE USE_PNGOBJECT}

{$I RV_Defs.inc}

uses
  Windows, Messages, SysUtils,
  Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls,
  ActnList, StdActns, ImgList,
  Menus, ShellApi, Printers, ToolWin, RVUni, RVTypes,

  {$IFDEF USE_GIFIMAGE}
    {$IFDEF RICHVIEWDEF2007}
    GifImg, RVGifAnimate2007,
    {$ELSE}
    GifImage, RVGifAnimate,
    {$ENDIF}
  {$ENDIF}
  {$IFDEF USE_PNGOBJECT}
  PngImage,
  {$ENDIF}

  RVScroll, RichView, RVEdit,
  RVStyle, PtblRV, CRVFData, RVTable, RVItem, CRVData, RVFuncs,

  RichViewActions, RVFontCombos, RVALocalize, te_engine,
  te_controls, te_switcher, te_forms, te_extctrls, te_scrolladapt;



type
  TForm1 = class(TForm)
    RVStyle1: TRVStyle;
    StatusBar1: TTeStatusBar;
    RVAControlPanel1: TRVAControlPanel;
    RVPrint1: TRVPrint;
    ColorDialog1: TColorDialog;
    TeThemeEngine1: TTeThemeEngine;
    TeForm1: TTeForm;
    RVAPopupMenu11: TRVAPopupMenu;
    TeControlBar1: TTeControlBar;
    ToolBar1: TTeToolBar;
    ToolButton11: TTeSpeedButton;
    ToolButton1: TTeSpeedButton;
    ToolButton2: TTeSpeedButton;
    ToolButton13: TTeSpeedButton;
    ToolButton22: TTeSpeedButton;
    ToolButton32: TTeSpeedButton;
    ToolButton33: TTeSpeedButton;
    ToolButton17: TTeSpeedButton;
    ToolButton18: TTeSpeedButton;
    ToolButton19: TTeSpeedButton;
    ToolButton20: TTeSpeedButton;
    ToolButton21: TTeSpeedButton;
    ToolButton46: TTeSpeedButton;
    ToolButton12: TTeSpeedButton;
    ToolButton47: TTeSpeedButton;
    ToolBar2: TTeToolBar;
    ToolButton41: TTeSpeedButton;
    ToolButton42: TTeSpeedButton;
    ToolButton43: TTeSpeedButton;
    ToolButton44: TTeSpeedButton;
    ToolButton29: TTeSpeedButton;
    ToolButton30: TTeSpeedButton;
    ToolButton31: TTeSpeedButton;
    ToolButton36: TTeSpeedButton;
    ToolButton38: TTeSpeedButton;
    ToolButton49: TTeSpeedButton;
    ToolButton50: TTeSpeedButton;
    ToolButton51: TTeSpeedButton;
    ToolButton8: TTeSpeedButton;
    ToolButton57: TTeSpeedButton;
    ToolButton61: TTeSpeedButton;
    ToolButton53: TTeSpeedButton;
    ToolButton54: TTeSpeedButton;
    ToolButton39: TTeSpeedButton;
    ToolBar3: TTeToolBar;
    ToolButton9: TTeSpeedButton;
    ToolButton14: TTeSpeedButton;
    ToolButton15: TTeSpeedButton;
    ToolButton16: TTeSpeedButton;
    ToolButton23: TTeSpeedButton;
    ToolButton24: TTeSpeedButton;
    ToolButton45: TTeSpeedButton;
    ToolButton25: TTeSpeedButton;
    ToolButton26: TTeSpeedButton;
    ToolBar4: TTeToolBar;
    ToolButton48: TTeSpeedButton;
    ToolButton52: TTeSpeedButton;
    ToolButton55: TTeSpeedButton;
    ToolButton56: TTeSpeedButton;
    ToolButton58: TTeSpeedButton;
    ToolButton59: TTeSpeedButton;
    ToolBar5: TTeToolBar;
    cmbFont: TTeComboBox;
    cmbFontSize: TTeSpinEdit;
    TeSpeedDivider1: TTeSpeedDivider;
    Button1: TTeButton;
    TeMenuBar1: TTeMenuBar;
    mitFile: TTeItem;
    New1: TTeItem;
    Load1: TTeItem;
    Save1: TTeItem;
    SaveAs1: TTeItem;
    Export1: TTeItem;
    N1: TTeItem;
    PageSetup1: TTeItem;
    PrintPreview1: TTeItem;
    Print1: TTeItem;
    N3: TTeItem;
    mitExit: TTeItem;
    mitEdit: TTeItem;
    Undo1: TTeItem;
    Redo1: TTeItem;
    N4: TTeItem;
    Cut1: TTeItem;
    Copy1: TTeItem;
    n2: TTeItem;
    PasteSpecial1: TTeItem;
    N5: TTeItem;
    Find1: TTeItem;
    FindNext1: TTeItem;
    Replace1: TTeItem;
    N26: TTeItem;
    CharacterCase1: TTeItem;
    N20: TTeItem;
    InsertPageBreak1: TTeItem;
    RemovePageBreak1: TTeItem;
    N15: TTeItem;
    SelectAll1: TTeItem;
    mitFont: TTeItem;
    Changefont1: TTeItem;
    Font1: TTeItem;
    N7: TTeItem;
    mitFontStyle: TTeItem;
    Bold1: TTeItem;
    Italic1: TTeItem;
    Underline1: TTeItem;
    Strikeout1: TTeItem;
    N14: TTeItem;
    AllCapitals1: TTeItem;
    Overline1: TTeItem;
    mitFontSize: TTeItem;
    ShrinkFont1: TTeItem;
    GrowFont1: TTeItem;
    N13: TTeItem;
    ShrinkFontByOnePoint1: TTeItem;
    GrowFontByOnePoint1: TTeItem;
    extColor1: TTeItem;
    extBackgroundColor1: TTeItem;
    mitPara: TTeItem;
    rvActionParagraph11: TTeItem;
    ParagraphBorders1: TTeItem;
    N18: TTeItem;
    AlignLeft1: TTeItem;
    AlignCenter1: TTeItem;
    AlignRight1: TTeItem;
    Justify1: TTeItem;
    N25: TTeItem;
    BulletsandNumbering1: TTeItem;
    Bullets1: TTeItem;
    Numbering1: TTeItem;
    N16: TTeItem;
    Leftjustify1: TTeItem;
    N6: TTeItem;
    DecreaseIndent1: TTeItem;
    IncreaseIndent1: TTeItem;
    N17: TTeItem;
    SingleLineSpacing1: TTeItem;
    N15LineSpacing1: TTeItem;
    DoubleLineSpacing1: TTeItem;
    N19: TTeItem;
    ParagraphBackgroundColor1: TTeItem;
    mitFormat: TTeItem;
    Background1: TTeItem;
    BackgroundColor1: TTeItem;
    N22: TTeItem;
    FillColor1: TTeItem;
    Properties1: TTeItem;
    mitInsert: TTeItem;
    File1: TTeItem;
    Picture1: TTeItem;
    HorizontalLine1: TTeItem;
    HypertextLink1: TTeItem;
    InsertSymbol1: TTeItem;
    mitTable: TTeItem;
    InsertTable1: TTeItem;
    N9: TTeItem;
    InsertColumnLeft1: TTeItem;
    InsertColumnRight1: TTeItem;
    N8: TTeItem;
    InsertRowAbove1: TTeItem;
    InsertRowBelow1: TTeItem;
    N10: TTeItem;
    DeleteRows1: TTeItem;
    rvActionTableDeleteCols11: TTeItem;
    DeleteTable1: TTeItem;
    N12: TTeItem;
    mitTableSelect: TTeItem;
    SelectTable1: TTeItem;
    SelectColumns1: TTeItem;
    SelectRows1: TTeItem;
    SelectCell1: TTeItem;
    N21: TTeItem;
    mitTableAlignCellContents: TTeItem;
    AlignCellToTheTop1: TTeItem;
    AlignCellToTheMiddle1: TTeItem;
    AlignCellToTheBottom1: TTeItem;
    DefaultCellVerticalAlignment1: TTeItem;
    mitTableCellBorders: TTeItem;
    LeftBorder1: TTeItem;
    rvActionTableCellTopBorder11: TTeItem;
    rvActionTableCellRightBorder11: TTeItem;
    rvActionTableCellBottomBorder11: TTeItem;
    rvActionTableCellAllBorders11: TTeItem;
    rvActionTableCellNoBorders11: TTeItem;
    N11: TTeItem;
    SplitCells1: TTeItem;
    MergeCells1: TTeItem;
    N24: TTeItem;
    ShowGridLines1: TTeItem;
    N23: TTeItem;
    ableProperties1: TTeItem;
    TeSpeedDivider2: TTeSpeedDivider;
    TeSpeedDivider3: TTeSpeedDivider;
    TeSpeedDivider4: TTeSpeedDivider;
    TeSpeedDivider5: TTeSpeedDivider;
    TeSpeedDivider6: TTeSpeedDivider;
    TeSpeedDivider7: TTeSpeedDivider;
    TeSpeedDivider8: TTeSpeedDivider;
    TeSpeedDivider9: TTeSpeedDivider;
    TeSpeedDivider10: TTeSpeedDivider;
    TeSpeedDivider11: TTeSpeedDivider;
    TeSpeedDivider12: TTeSpeedDivider;
    TeSpeedDivider13: TTeSpeedDivider;
    TeSpeedDivider14: TTeSpeedDivider;
    TeSpeedDivider15: TTeSpeedDivider;
    TeThemeSwitcher1: TTeThemeSwitcher;
    TeThemeList1: TTeThemeList;
    TeSpeedButton1: TTeSpeedButton;
    TeSpeedDivider16: TTeSpeedDivider;
    RichViewEdit1: TRichViewEdit;
    TeaScrollAdapter1: TTeaScrollAdapter;
    TeSpeedButton2: TTeSpeedButton;
    TeSpeedDivider17: TTeSpeedDivider;
    TeSpeedButton3: TTeSpeedButton;
    CustomItem1: TTeItem;
    CustomItem2: TTeItem;
    CustomItem3: TTeItem;
    CustomItem4: TTeItem;
    CustomItem5: TTeItem;
    CustomItem6: TTeItem;
    CustomItem7: TTeItem;
    CustomItem8: TTeItem;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure mitExit_oldClick(Sender: TObject);
    procedure RichViewEdit1Jump(Sender: TObject; id: Integer);
    procedure RichViewEdit1ReadHyperlink(Sender: TCustomRichView;
      const Target, Extras: String; DocFormat: TRVLoadFormat; var StyleNo,
      ItemTag: Integer; var ItemName: TRVRawByteString);
    procedure cmbFont1Click(Sender: TObject);
    procedure cmbKeyPress(Sender: TObject; var Key: Char);
    procedure RichViewEdit1CurTextStyleChanged(Sender: TObject);
    procedure cmbFontSize1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RVAControlPanel1CreateTeForm(Sender: TForm; teForm: TTeForm);
    procedure RichViewEdit1WriteHyperlink(Sender: TCustomRichView;
      id: Integer; RVData: TCustomRVData; ItemNo: Integer;
      SaveFormat: TRVSaveFormat; var Target, Extras: String);
    procedure RichViewEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton12ChevronClick(Sender: TObject);
    procedure RVAControlPanel1Download(Sender: TrvAction;
      const Source: String);
  private
    { Private declarations }
    UpdatingCombos: Boolean;
    procedure DoApplicationHint(Sender: TObject);
    procedure ColorPickerShow(Sender: TObject);
    procedure ColorPickerHide(Sender: TObject);
    procedure rvActionSave1DocumentFileChange(Sender: TObject;
      Editor: TCustomRichViewEdit; const FileName: String;
      FileFormat: TrvFileSaveFilter; IsNew: Boolean);
    procedure Localize;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses dmActions;
{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Almost all these assignments could be done at design time in the Object Inspector
  // But in this demo we do not want to modify rvActionsResource
  // (and we recommend to use a copy of it in your applications)

  rvActionsResource.rvActionSave1.OnDocumentFileChange := rvActionSave1DocumentFileChange;

  // Code for making color-picking buttons stay pressed while a
  // color-picker window is visible.
  rvActionsResource.rvActionColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionParaColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionParaColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionFontColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionFontColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionFontBackColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionFontBackColor1.OnHideColorPicker := ColorPickerHide;

  // Delphi 4 and 5 do not have ActionComponent property for actions.
  // Coloring actions have a substitution - CallerControl property
  // It is ignored in Delphi 6 and 7
  rvActionsResource.rvActionParaColor1.CallerControl := ToolButton39;
  rvActionsResource.rvActionFontBackColor1.CallerControl := ToolButton38;
  rvActionsResource.rvActionFontColor1.CallerControl := ToolButton36;

  {$IFDEF RICHVIEWDEF6}
  // AutoComplete feature causes OnClick generation when editing combo-box's text.
  // Since in OnClick we move input focus in RichViewEdit1, this effect is
  // undesirable
  cmbFont.AutoComplete := False;
  {$ENDIF}

  Localize;

  // Loading initial file via ActionOpen (allowing to update user interface)
  rvActionsResource.rvActionOpen1.LoadFile(RichViewEdit1,
    ExtractFilePath(Application.ExeName) + 'readme.rvf', ffiRVF);

  {
  // alternative way to start
    rvActionsResource.rvActionNew1.ExecuteTarget(RichViewEdit1);
  }
  Application.OnHint := DoApplicationHint;
end;


{------------------- Working with document ------------------------------------}

// When document is created, saved, loaded...
procedure TForm1.rvActionSave1DocumentFileChange(Sender: TObject;
  Editor: TCustomRichViewEdit; const FileName: String;
  FileFormat: TrvFileSaveFilter; IsNew: Boolean);
var s: String;
begin
  s := ExtractFileName(FileName);
  rvActionsResource.rvActionPrint1.Title := s;
  rvActionsResource.rvActionQuickPrint1.Title := s;
  if IsNew then
    s := s+' (*)';
  TeForm1.Caption := s + ' - RichViewActionsTest';
end;

// Prompt for saving...
procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := rvActionsResource.rvActionSave1.CanCloseDoc(RichViewEdit1);
end;

procedure TForm1.mitExit_oldClick(Sender: TObject);
begin
  Close;
end;

{--------------- Working with color-picking buttons ---------------------------}

// Code for making color-picking buttons pressed while
// a color-picker window is visible.
procedure TForm1.ColorPickerShow(Sender: TObject);
begin
  {$IFDEF RICHVIEWDEF6}
  if (Sender as TAction).ActionComponent is TToolButton then
    TToolButton(TAction(Sender).ActionComponent).Down := True;
  {$ELSE}
  if TrvActionCustomColor(Sender).CallerControl<>nil then
    TToolButton(TrvActionCustomColor(Sender).CallerControl).Down := True;
  {$ENDIF};
end;

procedure TForm1.ColorPickerHide(Sender: TObject);
begin
  {$IFDEF RICHVIEWDEF6}
  if (Sender as TAction).ActionComponent is TToolButton then
    TToolButton(TAction(Sender).ActionComponent).Down := False;
  {$ELSE}
  if TrvActionCustomColor(Sender).CallerControl<>nil then
    TToolButton(TrvActionCustomColor(Sender).CallerControl).Down := False;
  {$ENDIF}
end;

{-------------------------- Table size popup ----------------------------------}

procedure TForm1.ToolButton12ChevronClick(Sender: TObject);
begin
   rvActionsResource.rvActionInsertTable1.ShowTableSizeDialog(RichViewEdit1,
     ToolButton12);
end;

{-------------- Set of events for processing hypertext jumps ------------------}

// Hyperlink click
procedure TForm1.RichViewEdit1Jump(Sender: TObject; id: Integer);
begin
  rvActionsResource.rvActionInsertHyperlink1.GoToLink(RichViewEdit1, id);
end;

// Importing hyperlink from RTF
procedure TForm1.RichViewEdit1ReadHyperlink(Sender: TCustomRichView;
  const Target, Extras: String; DocFormat: TRVLoadFormat; var StyleNo,
  ItemTag: Integer; var ItemName: TRVRawByteString);
var URL: String;
begin
  if DocFormat=rvlfURL then
    StyleNo :=
      rvActionsResource.rvActionInsertHyperlink1.GetHyperlinkStyleNo(RichViewEdit1);
  URL := rvActionsResource.rvActionInsertHyperlink1.EncodeTarget(Target);
  ItemTag := Integer(StrNew(PChar(URL)));
end;

// Exporting hyperlink to RTF and HTML
procedure TForm1.RichViewEdit1WriteHyperlink(Sender: TCustomRichView;
  id: Integer; RVData: TCustomRVData; ItemNo: Integer;
  SaveFormat: TRVSaveFormat; var Target, Extras: String);
begin
  Target := PChar(RVData.GetItemTag(ItemNo));
end;

// URL detection, hyperlink closing
procedure TForm1.RichViewEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_SPACE, VK_RETURN] then begin
    rvActionsResource.rvActionInsertHyperlink1.DetectURL(RichViewEdit1);
    rvActionsResource.rvActionInsertHyperlink1.TerminateHyperlink(RichViewEdit1);
  end;
end;

{----------------------- Working with combo-boxes -----------------------------}

// Current font is changed. Updating the combo-boxes.
procedure TForm1.RichViewEdit1CurTextStyleChanged(Sender: TObject);
var CurStyle: TFontInfo;
begin
  UpdatingCombos := True;
  try
    CurStyle := RVStyle1.TextStyles[RichViewEdit1.CurTextStyleNo];
    cmbFont.FontValue.Name := CurStyle.FontName;
    cmbFontSize.Text := IntToStr(CurStyle.Size);
  finally
    UpdatingCombos := False;
  end;
end;

// Applying the font name
procedure TForm1.cmbFont1Click(Sender: TObject);
var FontName: String;
begin
  if cmbFont.ItemIndex < 0 then
    FontName := cmbFont.Text
  else
    FontName := cmbFont.Items[cmbFont.ItemIndex];
  if UpdatingCombos then
    exit;
  if cmbFont.ItemIndex < 0 then
    cmbFont.ItemIndex := cmbFont.Items.IndexOf(FontName);
  if cmbFont.ItemIndex<0 then
  begin
    Beep
  end
  else
    with rvActionsResource.rvActionFontEx1 do
    begin
      UserInterface := False;
      ValidProperties := [rvfimFontName];
      Font.Name := FontName;
      Execute;
      UserInterface := True;
    end;
  RichViewEdit1.SetFocus;
end;

// Changing the font size
procedure TForm1.cmbFontSize1Click(Sender: TObject);
var FontSize: Integer;
begin
  if UpdatingCombos then
    exit;
  try
    FontSize := StrToInt(cmbFontSize.Text);
    with rvActionsResource.rvActionFontEx1 do begin
      UserInterface := False;
      ValidProperties := [rvfimSize];
      Font.Size := FontSize;
      Execute;
      UserInterface := True;
    end;
  except
    Beep;
  end;
  RichViewEdit1.SetFocus;
end;

procedure TForm1.cmbKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then begin
    Key := #0;
    TComboBox(Sender).OnClick(Sender);
  end;
end;

{---------------------------- Localization ------------------------------------}
// Converts String to WideString using CodePage of UI language
function _GetWideString(const s: String): WideString;
begin
  {$IFDEF RVUNICODESTR}
  Result := s;
  {$ELSE}
  Result := RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(
    RVU_Charset2CodePage(RVA_GetCharset), s));
  {$ENDIF}
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if RVA_ChooseLanguage then
    Localize;
end;

procedure TForm1.Localize;
begin
  // Fonts
  Font.Charset            := RVA_GetCharset;
  StatusBar1.Font.Charset := RVA_GetCharset;
  {$IFDEF RICHVIEWDEF6}
  Screen.HintFont.Charset := RVA_GetCharset;
  {$ENDIF}
  te_forms.CodePage    := RVU_Charset2CodePage(RVA_GetCharset);
  // Localizing all actions on rvActionsResource
  RVA_LocalizeForm(rvActionsResource);
  // Localizing all actions on this form
  RVA_LocalizeForm(Self);
  // Localizing menus (not all menus in this demo are translated)
  mitFile.Caption := _GetWideString(RVA_GetS(rvam_menu_File));
  mitEdit.Caption := _GetWideString(RVA_GetS(rvam_menu_Edit));
  mitFont.Caption := _GetWideString(RVA_GetS(rvam_menu_Font));
  mitPara.Caption := _GetWideString(RVA_GetS(rvam_menu_Para));
  mitFormat.Caption := _GetWideString(RVA_GetS(rvam_menu_Format));
  mitInsert.Caption := _GetWideString(RVA_GetS(rvam_menu_Insert));
  mitTable.Caption := _GetWideString(RVA_GetS(rvam_menu_Table));
  mitExit.Caption := _GetWideString(RVA_GetS(rvam_menu_Exit));

  mitFontSize.Caption := _GetWideString(RVA_GetS(rvam_menu_FontSize));
  mitFontStyle.Caption := _GetWideString(RVA_GetS(rvam_menu_FontStyle));
  mitTableSelect.Caption := _GetWideString(RVA_GetS(rvam_menu_TableSelect));
  mitTableCellBorders.Caption := _GetWideString(RVA_GetS(rvam_menu_TableCellBorders));
  mitTableAlignCellContents.Caption := _GetWideString(RVA_GetS(rvam_menu_TableCellAlign));
  // In your application, you can use either TrvActionFonts or TrvActionFontEx
  rvActionsResource.rvActionFonts1.Caption := rvActionsResource.rvActionFonts1.Caption+' (Standard)';
  rvActionsResource.rvActionFontEx1.Caption := rvActionsResource.rvActionFontEx1.Caption+' (Advanced)';

//  RVAddictSpell31.UILanguage := GetAddictSpellLanguage(RVA_GetLanguageName);
//  RVThesaurus31.UILanguage := GetAddictThesLanguage(RVA_GetLanguageName);
end;

procedure TForm1.DoApplicationHint(Sender: TObject);
begin
  StatusBar1.Panels[1].Text := _GetWideString(GetLongHint(Application.Hint));
end;

procedure TForm1.RVAControlPanel1Download(Sender: TrvAction;
  const Source: String);
begin
  if Source='' then
    Application.Hint := ''
  else
    Application.Hint := 'Downloading '+Source+'...';
end;

procedure TForm1.RVAControlPanel1CreateTeForm(Sender: TForm;
  teForm: TTeForm);
begin
  teForm.Shadow.Enabled := True;
end;

initialization

{$IFDEF USE_GIFIMAGE}
  RegisterClasses([TGifImage]);
  // uncomment the line below if Gif does not appear in Insert | Image
  //TPicture.RegisterFileFormat('gif', 'Gif Image', TGifImage);
  RV_RegisterHTMLGraphicFormat(TGifImage);
{$ENDIF}
{$IFDEF USE_PNGOBJECT}
  {$IFDEF RICHVIEWDEF2009}
  RegisterClass(TPngImage);
  RV_RegisterHTMLGraphicFormat(TPngImage);
  RV_RegisterPngGraphic(TPngImage);
  {$ELSE}
  RegisterClass(TPngObject);
  RV_RegisterHTMLGraphicFormat(TPngObject);
  RV_RegisterPngGraphic(TPngObject);  
  {$ENDIF}
{$ENDIF}


end.
