unit RVAPopupActionBar;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses Classes, Controls, Graphics, RichViewActions,
     ActnList, ActnPopup,
     RVEdit, RVUni;

type
  TRVAPopupActionBar = class;

  TRVALiveSpellGetSuggestionsEvent = procedure (Sender: TRVAPopupActionBar;
    Edit: TCustomRichViewEdit; const Word: String; StyleNo: Integer;
    Suggestions: TStrings) of object;
  TRVALiveSpellIgnoreAllEvent = procedure (Sender: TRVAPopupActionBar;
    Edit: TCustomRichViewEdit; const Word: String; StyleNo: Integer) of object;
  TRVALiveSpellAddEvent = procedure (Sender: TRVAPopupActionBar;
    Edit: TCustomRichViewEdit; const Word: String; StyleNo: Integer) of object;
  TRVALiveSpellReplaceEvent = procedure (Sender: TRVAPopupActionBar;
    Edit: TCustomRichViewEdit; const Word, Correction: String; StyleNo: Integer) of object;

  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVAPopupActionBar = class (TPopupActionBar, IRVAPopupMenu)
  private
    FPopupMenuHelper: TRVAPopupMenuHelper;
    FOnLiveSpellGetSuggestions: TRVALiveSpellGetSuggestionsEvent;
    FOnLiveSpellAdd: TRVALiveSpellAddEvent;
    FOnLiveSpellIgnoreAll: TRVALiveSpellIgnoreAllEvent;
    FOnLiveSpellWordReplace: TRVALiveSpellReplaceEvent;
    function GetControlPanel: TRVAControlPanel;
    procedure SetControlPanel(Value: TRVAControlPanel);    
  protected
    { IRVAPopupMenu }
    function GetPopupComponent: TComponent;
    function GetActionList: TCustomActionList;
    procedure SetActionList(const Value: TCustomActionList);
    function GetMaxSuggestionsCount: Integer;
    procedure SetMaxSuggestionsCount(Value: Integer);
    procedure ClearItems;
    procedure AddActionItem(Action: TAction; const Caption: String='');
    procedure AddSeparatorItem;
    procedure DeleteLastSeparatorItem;
    function GetItemCaption(Sender: TObject): string;
    function GetItemIndex(Sender: TObject): Integer;
    {$IFNDEF RVDONOTUSELIVESPELL}
    procedure AddClickItem(Click: TNotifyEvent; const Caption: String;
      Charset: TFontCharset; Enabled, Default: Boolean);
    function WantLiveSpellGetSuggestions: Boolean;
    procedure LiveSpellGetSuggestions(Edit: TCustomRichViewEdit; const Word: String; StyleNo: Integer;
      Suggestions: TStrings);
    procedure LiveSpellWordReplace(Edit: TCustomRichViewEdit; const Word, Correction: String; StyleNo: Integer);
    function WantLiveSpellIgnoreAll: Boolean;
    procedure LiveSpellIgnoreAll(Edit: TCustomRichViewEdit; const Word: String; StyleNo: Integer);
    function WantLiveSpellAdd: Boolean;
    procedure LiveSpellAdd(Edit: TCustomRichViewEdit; const Word: String; StyleNo: Integer);
    {$ENDIF}
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Popup(X,Y: Integer); override;
  published
    property ActionList: TCustomActionList read GetActionList write SetActionList;
    property OnLiveSpellGetSuggestions: TRVALiveSpellGetSuggestionsEvent
      read FOnLiveSpellGetSuggestions write FOnLiveSpellGetSuggestions;
    property OnLiveSpellIgnoreAll: TRVALiveSpellIgnoreAllEvent
      read FOnLiveSpellIgnoreAll write FOnLiveSpellIgnoreAll;
    property OnLiveSpellAdd: TRVALiveSpellAddEvent
      read FOnLiveSpellAdd write FOnLiveSpellAdd;
    property OnLiveSpellWordReplace: TRVALiveSpellReplaceEvent
      read FOnLiveSpellWordReplace write FOnLiveSpellWordReplace;
    property MaxSuggestionsCount: Integer read GetMaxSuggestionsCount write SetMaxSuggestionsCount default 0;
    property ControlPanel: TRVAControlPanel read GetControlPanel write SetControlPanel;    
  end;

implementation

{ TRVAPopupActionBar }

procedure TRVAPopupActionBar.AddActionItem(Action: TAction;
  const Caption: String);
var mi: TRVAPopupMenuItem;
begin
  mi := TRVAPopupMenuItem.Create(Self);
  mi.Action := Action;
  if Caption<>'' then
    mi.Caption := Caption;
  Items.Add(mi);
end;

procedure TRVAPopupActionBar.AddSeparatorItem;
var mi: TRVAPopupMenuItem;
begin
  if (Items.Count=0) or (Items[Items.Count-1].Caption='-') then
    exit;
  mi := TRVAPopupMenuItem.Create(Self);
  mi.Caption := '-';
  Items.Add(mi);
end;

procedure TRVAPopupActionBar.ClearItems;
begin
  while Items.Count>0 do
    Items.Items[0].Free;
end;

constructor TRVAPopupActionBar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPopupMenuHelper := TRVAPopupMenuHelper.Create(Self);
  MaxSuggestionsCount := 0;
end;

procedure TRVAPopupActionBar.DeleteLastSeparatorItem;
begin
  if (Items.Count>0) and (Items[Items.Count-1].Caption='-') then
    Items.Items[Items.Count-1].Free;
end;

destructor TRVAPopupActionBar.Destroy;
begin
  FPopupMenuHelper.Free;;
  inherited Destroy;
end;

function TRVAPopupActionBar.GetActionList: TCustomActionList;
begin
  Result := FPopupMenuHelper.ActionList;
end;

function TRVAPopupActionBar.GetItemCaption(Sender: TObject): string;
begin
  Result := (Sender as TRVAPopupMenuItem).Caption;
end;

function TRVAPopupActionBar.GetItemIndex(Sender: TObject): Integer;
begin
  Result := Items.IndexOf(Sender as TRVAPopupMenuItem);
end;

function TRVAPopupActionBar.GetMaxSuggestionsCount: Integer;
begin
  Result := FPopupMenuHelper.MaxSuggestionsCount;
end;

function TRVAPopupActionBar.GetPopupComponent: TComponent;
begin
  Result := GetRichViewEditFromPopupComponent(PopupComponent);
end;

procedure TRVAPopupActionBar.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation=opRemove) and (FPopupMenuHelper<>nil) then begin
     if AComponent=FPopupMenuHelper.ActionList then
       ActionList := nil
     else if AComponent=FPopupMenuHelper.ControlPanel then
       ControlPanel := nil;
  end;
end;

function TRVAPopupActionBar.GetControlPanel: TRVAControlPanel;
begin
  Result := FPopupMenuHelper.ControlPanel;
end;

procedure TRVAPopupActionBar.SetControlPanel(Value: TRVAControlPanel);
begin
  if Value <> FPopupMenuHelper.ControlPanel then
  begin
    {$IFDEF RICHVIEWDEF5}
    if FPopupMenuHelper.ControlPanel <> nil then
      FPopupMenuHelper.ControlPanel.RemoveFreeNotification(Self);
    {$ENDIF}
    FPopupMenuHelper.ControlPanel := Value;
    if FPopupMenuHelper.ControlPanel<> nil then
      FPopupMenuHelper.ControlPanel.FreeNotification(Self);
  end;
end;

procedure TRVAPopupActionBar.Popup(X, Y: Integer);
begin
  FPopupMenuHelper.PreparePopup(X, Y);
  inherited;
end;

procedure TRVAPopupActionBar.SetActionList(const Value: TCustomActionList);
begin
  if Value <> FPopupMenuHelper.ActionList then
  begin
    if FPopupMenuHelper.ActionList <> nil then
      FPopupMenuHelper.ActionList.RemoveFreeNotification(Self);
    FPopupMenuHelper.ActionList := Value;
    if FPopupMenuHelper.ActionList<> nil then
      FPopupMenuHelper.ActionList.FreeNotification(Self);
  end;
end;

procedure TRVAPopupActionBar.SetMaxSuggestionsCount(Value: Integer);
begin
  FPopupMenuHelper.MaxSuggestionsCount := Value;
end;

{$IFNDEF RVDONOTUSELIVESPELL}
procedure TRVAPopupActionBar.AddClickItem(Click: TNotifyEvent;
  const Caption: String; Charset: TFontCharset; Enabled, Default: Boolean);
var mi: TRVAPopupMenuItem;
begin
  mi := TRVAPopupMenuItem.Create(Self);
  mi.Caption :=   Caption;
  mi.Default := Default and not RVVista;
  mi.Enabled := Enabled;
  mi.OnClick := Click;
  Items.Add(mi);
end;

function TRVAPopupActionBar.WantLiveSpellGetSuggestions: Boolean;
begin
  Result := Assigned(FOnLiveSpellGetSuggestions);
end;

procedure TRVAPopupActionBar.LiveSpellGetSuggestions(Edit: TCustomRichViewEdit;
  const Word: String; StyleNo: Integer; Suggestions: TStrings);
begin
  if WantLiveSpellGetSuggestions then
    FOnLiveSpellGetSuggestions(Self, Edit, Word, StyleNo, Suggestions);
end;

procedure TRVAPopupActionBar.LiveSpellWordReplace(Edit: TCustomRichViewEdit;
  const Word, Correction: String; StyleNo: Integer);
begin
  if Assigned(FOnLiveSpellWordReplace) then
    FOnLiveSpellWordReplace(Self, Edit, Word, Correction, StyleNo);
end;

function TRVAPopupActionBar.WantLiveSpellIgnoreAll: Boolean;
begin
  Result := Assigned(FOnLiveSpellIgnoreAll);
end;

procedure TRVAPopupActionBar.LiveSpellIgnoreAll(Edit: TCustomRichViewEdit;
  const Word: String; StyleNo: Integer);
begin
  if WantLiveSpellIgnoreAll then
    FOnLiveSpellIgnoreAll(Self, Edit, Word, StyleNo);
end;

function TRVAPopupActionBar.WantLiveSpellAdd: Boolean;
begin
  Result := Assigned(FOnLiveSpellAdd);
end;

procedure TRVAPopupActionBar.LiveSpellAdd(Edit: TCustomRichViewEdit;
  const Word: String; StyleNo: Integer);
begin
  if WantLiveSpellAdd then
    FOnLiveSpellAdd(Self, Edit, Word, StyleNo);
end;
{$ENDIF}


end.
