﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'SplitRVFrm.pas' rev: 27.00 (Windows)

#ifndef SplitrvfrmHPP
#define SplitrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Splitrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVSplit;
class PASCALIMPLEMENTATION TfrmRVSplit : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* panUnmerge;
	Vcl::Stdctrls::TCheckBox* cbUnmergeRows;
	Vcl::Stdctrls::TCheckBox* cbUnmergeCols;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TGroupBox* GroupBox1;
	Vcl::Stdctrls::TRadioButton* rbSplit;
	Vcl::Stdctrls::TRadioButton* rbUnmerge;
	Vcl::Stdctrls::TGroupBox* panSplit;
	Vcl::Stdctrls::TLabel* Label1;
	Vcl::Stdctrls::TLabel* Label2;
	Rvspinedit::TRVSpinEdit* seColumns;
	Rvspinedit::TRVSpinEdit* seRows;
	Vcl::Stdctrls::TCheckBox* cbMerge;
	void __fastcall rbClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
public:
	Vcl::Controls::TControl* _rbSplit;
	Vcl::Controls::TControl* _rbUnmerge;
	Vcl::Controls::TControl* _panUnmerge;
	Vcl::Controls::TControl* _panSplit;
	Vcl::Controls::TControl* _cbUnmergeRows;
	Vcl::Controls::TControl* _cbUnmergeCols;
	Vcl::Controls::TControl* _cbMerge;
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVSplit(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVSplit(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVSplit(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVSplit(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Splitrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SPLITRVFRM)
using namespace Splitrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SplitrvfrmHPP
