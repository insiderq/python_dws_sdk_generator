﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVALocRuler.pas' rev: 27.00 (Windows)

#ifndef RvalocrulerHPP
#define RvalocrulerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <RVRuler.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvalocruler
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall RVALocalizeRuler(Rvruler::TRVRuler* Ruler, Richviewactions::TRVAControlPanel* ControlPanel = (Richviewactions::TRVAControlPanel*)(0x0));
}	/* namespace Rvalocruler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVALOCRULER)
using namespace Rvalocruler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvalocrulerHPP
