{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Polish translation                              }
{                                                       }
{       Copyright (c) 2002-2003, Sergey Tkachenko       }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated: 2004-10-04 by Grzegorz Rewucki            }
{                           rewucki@rewucki.pl          }
{                           http://www.rewucki.pl       }
{ Updated: 2010-03-14 by Tomasz Trejderowski            }
{ Updated: 2010-04-06 by Bogus�aw Kaleta                }
{ Updated: 2011-02-09 by Grzegorz Rewucki               }
{ Updated: 2011-09-15 by Grzegorz Rewucki               }
{ Updated: 2012-08-31 by Grzegorz Rewucki               }
{ Updated: 2014-04-20 by Grzegorz Rewucki               }
{*******************************************************}

unit RVAL_Polish;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'cale', 'cm', 'mm', 'pica', 'piksele', 'punkty',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pkt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Plik', '&Edycja', 'F&ormat', '&Czcionka', '&Akapit', 'W&staw', '&Tabela', '&Okno', 'Pomo&c',
  // exit
  'Za&ko�cz',
  // top-level menus: View, Tools,
  '&Widok', '&Narz�dzia',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '&Rozmiar', '&Styl czcionki', '&Zaznacz', 'Wyr�w&nanie kom�rek', 'Obra&mowanie kom�rek',
  // menus: Table cell rotation
  'Obracanie kom�&rek',
  // menus: Text flow, Footnotes/endnotes
  'Prz&ep�yw tekstu', '&Przypisy',
  // ribbon tabs: tab1, tab2, view, table
  'Narz�dzia &g��wne', 'Z&aawansowane', 'Wid&ok', '&Tabela',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Schowek', 'Czcionka', 'Akapit', 'Lista', 'Edytowanie',
  // ribbon groups: insert, background, page setup,
  'Wstawianie', 'T�o strony', 'Ustawienia strony',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options,
  '��cza', 'Przypisy', 'Widoki dokumentu', 'Pokazywanie/ukrywanie', 'Powi�kszenie', 'Opcje',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Wstaw', 'Usu�', 'Operacje', 'Obramowanie',
  // ribbon groups: styles
  'Style',
  // ribbon screen tip footer,
  'F1 - pomoc',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Niedawno u�ywane dokumenty', 'Zapisz kopi� dokumentu', 'Wy�wietl podgl�d dokumentu i wydrukuj go',
  // ribbon label: units combo
  'Miara:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nowy', 'Nowy|Tworzy nowy dokument',
  // TrvActionOpen
  '&Otw�rz...', 'Otw�rz|Otwiera istniej�cy dokument',
  // TrvActionSave
  '&Zapisz', 'Zapisz|Zapisuje aktywny dokument',
  // TrvActionSaveAs
  'Zapisz j&ako...', 'Zapisz jako...|Zapisuje aktywny dokument pod now� nazw�',
  // TrvActionExport
  '&Eksportuj...', 'Exportuj|Eksportuje aktywny dokument do innego formatu',
  // TrvActionPrintPreview
  '&Podgl�d wydruku', 'Podgl�d wydruku|Wy�wietla pe�ne strony',
  // TrvActionPrint
  '&Drukuj...', 'Drukuj|Ustawia w�a�ciwo�ci drukowania i drukuje aktywny dokument',
  // TrvActionQuickPrint
  '&Drukuj', '&Szybkie drukowanie', 'Drukuj|Drukuje aktywny dokument',
  // TrvActionPageSetup
  '&Ustawienia strony...', 'Ustawienia strony|Zmienia ustawienia uk�adu strony',
  // TrvActionCut
  'Wyt&nij', 'Wytnij|Wycina zaznaczony fragment i umieszcza go w Schowku',
  // TrvActionCopy
  '&Kopiuj', 'Kopiuj|Kopiuje zaznaczony fragment i umieszcza go w Schowku',
  // TrvActionPaste
  'Wkl&ej', 'Wklej|Wstawia zawarto�� Schowka',
  // TrvActionPasteAsText
  'Wklej jako &tekst', 'Wklej jako tekst|Wstawia zawarto�� schowka jako tekst',
  // TrvActionPasteSpecial
  'Wklej &specjalnie...', 'Wklej specjalnie|Wstawia zawarto�� schowka umo�liwiaj�c wyb�r formatu',
  // TrvActionSelectAll
  'Z&aznacz wszystko', 'Zaznacz wszystko|Zaznacza ca�y dokument',
  // TrvActionUndo
  'Co&fnij', 'Cofnij|Cofa ostatni� czynno��',
  // TrvActionRedo
  '&Pon�w', 'Pon�w|Ponawia ostatnio cofni�t� czynno��',
  // TrvActionFind
  '&Znajd�...', 'Znajd�|Znajduje podany tekst',
  // TrvActionFindNext
  'Znajd� &nast�pny', 'Znajd� nast�pny|Powtarza ostatnie wyszukiwanie',
  // TrvActionReplace
  'Za&mie�...', 'Zamie�|Znajduje i zamienia podany tekst',
  // TrvActionInsertFile
  '&Plik...', 'Wstaw plik|Wstawia zawarto�� pliku',
  // TrvActionInsertPicture
  '&Obraz...', 'Wstaw obraz|Wstawia obraz',
  // TRVActionInsertHLine
  '&Linia pozioma', 'Wstaw lini� poziom�|Wstawia lini� poziom�',
  // TRVActionInsertHyperlink
  '&Hiper��cze...', 'Wstaw hiper��cze|Wstawia hiper��cze',
  // TRVActionRemoveHyperlinks
  '&Usu� odno�niki', 'Usu� odno�niki|Usuwa wszystkie odno�niki w zaznaczonym tek�cie',
  // TrvActionInsertSymbol
  '&Symbol...', 'Wstaw symbol|Wstawia symbol',
  // TrvActionInsertNumber
  '&Licznik...', 'Wstaw licznik|Wstawia licznik',
  // TrvActionInsertCaption
  'Wstaw &podpis...', 'Wstaw podpis|Wstawia podpis wybranego obiektu',
  // TrvActionInsertTextBox
  'Przypis &s�siadujacy', 'Wstaw przypis s�siaduj�cy|Wstawia przypis s�siaduj�cy z miejcem wstawienia',
  // TrvActionInsertSidenote
  'Przypis &boczny', 'Wstaw przypis boczny|Wstawia przypis boczny',
  // TrvActionInsertPageNumber
  '&Numer strony', 'Wstaw numer strony|Wstawia numer strony',
  // TrvActionParaList
  'Wy&punktowanie i numerowanie...', 'Wypunktowanie i numerowanie|Zmienia ustawienia wypunktowania i numerowania w zaznaczonych akapitach',
  // TrvActionParaBullets
  '&Wypunktowanie', 'Wypunktowanie|Dodaje b�d� usuwa wypunktowanie akapitu',
  // TrvActionParaNumbering
  '&Numerowanie', 'Numerowanie|Dodaje b�d� usuwa numerowanie akapitu',
  // TrvActionColor
  'Kolor &t�a...', 'Kolor t�a|Zmienia ustawienia koloru t�a dokumentu',
  // TrvActionFillColor
  'Kolor &wype�nienia...', 'Kolor wype�nienia|Zmienia kolor wype�nienia zaznaczonego elementu',
  // TrvActionInsertPageBreak
  'Wstaw znak podzia�&u strony', 'Wstaw znak podzia�u strony|Wstawia znak podzia�u strony',
  // TrvActionRemovePageBreak
  '&Usu� znak podzia�u strony', 'Usu� znak podzia�u strony|Usuwa znak podzia�u strony',
  // TrvActionClearLeft
  'Usu� przep�yw tekstu po &lewej stronie', 'Usu� przep�yw tekstu po lewej stronie|Umieszcza ten paragraf poni�ej dowolnego obrazka z wyr�wnaniem do lewego marginesu',
  // TrvActionClearRight
  'Usu� przep�yw tekstu po &prawej stronie', 'Usu� przep�yw tekstu po prawej stronie|Umieszcza ten paragraf poni�ej dowolnego obrazka z wyr�wnaniem do prawego marginesu',
  // TrvActionClearBoth
  'Usu� przep�yw tekstu po &obu stronach', 'Usu� przep�yw tekstu po obu stronach|Umieszcza ten paragraf poni�ej dowolnego obrazka z wyr�wnaniem do obu margines�w',
  // TrvActionClearNone
  '&Normalny prze�yw tekstu', 'Normalny prze�yw tekstu|Normalny przep�yw tekstu wok� obrazk�w z wyr�wnaniem do obu margines�w',
  // TrvActionVAlign
  'Pozycja &obiektu...', 'Pozycja obiektu...|Zmienia pozycj� wybranego obiektu',
  // TrvActionItemProperties
  '&W�a�ciwo�ci obiektu...', 'W�a�ciwo�ci obiektu|Definiuje w�a�ciwo�ci wybranego obiektu',
  // TrvActionBackground
  '&T�o...', 'T�o|Zmienia efekty t�a',
  // TrvActionParagraph
  '&Akapit...', 'Akapit|Zmienia ustawienia akapitu',
  // TrvActionIndentInc
  'Zwi�ksz wci�cie', 'Zwi�ksz wci�cie|Zwi�ksza wci�cie zaznaczonych akapit�w',
  // TrvActionIndentDec
  'Zmniejsz wci�cie', 'Zmniejsz wci�cie|Zmniejsza wci�cie zaznaczonych akapit�w',
  // TrvActionWordWrap
  '&Zawijaj tekst', 'Zawijaj tekst|Prze��cza zawijanie tekstu w zaznaczonych akapitach',
  // TrvActionAlignLeft
  'Wyr�wnaj do &lewej', 'Wyr�wnaj do lewej|Wyr�wnuje zaznaczony tekst do lewej',
  // TrvActionAlignRight
  'Wyr�wnaj do &prawej', 'Wyr�wnaj do prawej|Wyr�wnuje zaznaczony tekst do prawej',
  // TrvActionAlignCenter
  'Wy�r&odkuj', 'Wy�rodkuj|Wyr�wnuje zaznaczony tekst do �rodka',
  // TrvActionAlignJustify
  'Wy&justuj', 'Wyjustuj|Wyr�wnuje zaznaczony tekst do prawej i lewej jednocze�nie',
  // TrvActionParaColor
  'Kolor t�a akapitu...', 'Kolor t�a akapitu|Zmienia kolor t�a zaznaczonych akapit�w',
  // TrvActionLineSpacing100
  'Odst�p &pojedynczy', 'Odst�p pojedynczy|Ustawia pojedynczy odst�p miedzy wierszami',
  // TrvActionLineSpacing150
  'Odst�p &1.5', 'Odst�p 1.5|Ustawia odst�p 1.5 mi�dzy wierszami',
  // TrvActionLineSpacing200
  '&Podw�jny odst�p', 'Podw�jny odst�p|Ustawia powd�jny odst�p mi�dzy wierszami',
  // TrvActionParaBorder
  '&Obramowanie i t�o akapitu...', 'Obramowanie i t�o akapitu|Ustawia obramowanie i t�o zaznaczonych akapit�w',
  // TrvActionInsertTable
  'Wstaw t&abel�...', '&Tabela', 'Wstaw tabel�|Wstawia now� tabel�',
  // TrvActionTableInsertRowsAbove
  'Wstaw wi&ersz powy�ej', 'Wstaw wiersz powy�ej|Wstawia wiersz powy�ej zaznaczonych kom�rek',
  // TrvActionTableInsertRowsBelow
  'Wstaw &wiersz poni�ej', 'Wstaw wiersz poni�ej|Wstawia wiersz poni�ej zaznaczonych kom�rek',
  // TrvActionTableInsertColLeft
  'Wstaw k&olumn� po lewej', 'Wstaw kolumn� po lewej|Wstawia kolumn� po lewej stronie zaznaczonych kom�rek',
  // TrvActionTableInsertColRight
  'Wstaw &kolumn� po prawej', 'Wstaw kolumn� po prawej|Wstawia kolumn� po prawej stronie zaznaczonych kom�rek',
  // TrvActionTableDeleteRows
  'Usu� wi&ersze', 'Usu� wiersze|Usuwa wiersze zawieraj�ce zaznaczone kom�rki',
  // TrvActionTableDeleteCols
  'Usu� &kolumny', 'Usu� kolumny|Usuwa kolumny zawieraj�ce zaznaczone kom�rki',
  // TrvActionTableDeleteTable
  'Usu� &tabel�', 'Usu� tabel�|Usuwa zaznaczon� tabel�',
  // TrvActionTableMergeCells
  'S&calaj kom�rki', 'Scalaj kom�rki|Scala zaznaczone kom�rki',
  // TrvActionTableSplitCells
  'Podzi&el kom�rki...', 'Podziel kom�rki|Dzieli zaznaczone kom�rki',
  // TrvActionTableSelectTable
  'Zaznacz &tabel�', 'Zaznacz tabel�|Zaznacza tabel�',
  // TrvActionTableSelectRows
  'Zaznacz &wiersze', 'Zaznacz wiersze|Zaznacza wiersze',
  // TrvActionTableSelectCols
  'Zaznacz k&olumny', 'Zaznacz kolumny|Zaznacza kolumny',
  // TrvActionTableSelectCell
  'Zaznacz &kom�rk�', 'Zaznacz kom�rk�|Zaznacza kom�rk�',
  // TrvActionTableCellVAlignTop
  'Wyr�wnaj kom�rki do &g�ry', 'Wyr�wnaj kom�rki do g�ry|Wyr�wnuje zawarto�� zaznaczonych kom�rek do g�ry',
  // TrvActionTableCellVAlignMiddle
  'Wyr�wnaj kom�rki do �r&odka', 'Wyr�wnaj kom�rki do �rodka|Wyr�wnuje zawarto�� zaznaczonych kom�rek do �rodka',
  // TrvActionTableCellVAlignBottom
  'Wyr�wnaj kom�rki do &do�u', 'Wyr�wnaj kom�rki do do�u|Wyr�wnuje zawarto�� zaznaczonych kom�rek do do�u',
  // TrvActionTableCellVAlignDefault
  '&Domy�lne wyr�wnanie pionowe', 'Domy�lne wyr�wnanie pionowe|Ustawia domy�lne wyr�wnywanie zaznaczonych kom�rek w pionie',
  // TrvActionTableCellRotationNone
  '&Nie obracaj kom�rek', 'Nie obracaj kom�rek|Nie obraca zawarto�ci kom�rek',
  // TrvActionTableCellRotation90
  'Obr�� kom�rki o &90�', 'Obr�� kom�rki o 90�|Obraca zawarto�� kom�rek o 90�',
  // TrvActionTableCellRotation180
  'Obr�� kom�rki o &180�', 'Obr�� kom�rki o 180�|Obraca zawarto�� kom�rek o 180�',
  // TrvActionTableCellRotation270
  'Obr�� kom�rki o &270�', 'Obr�� kom�rki o 270�|Obraca zawarto�� kom�rek o 270�',
  // TrvActionTableProperties
  '&W�a�ciwo�ci tabeli...', 'W�a�ciwo�ci tabeli|Zmienia w�a�ciwo�ci zaznaczonej tabeli',
  // TrvActionTableGrid
  'Poka� &linie siatki', 'Poka� linie siatki|Pokazuje lub ukrywa linie siatki zaznaczonej tabeli',
  // TRVActionTableSplit
  'Podzie&l tabel�', 'Podziel tabel�|Dzieli tabel� na dwie osobne tabele zaczynaj�c od wskazanego wiersza',
  // TRVActionTableToText
  'Przekszta�� w te&kst...', 'Przekszta�� w tekst|Przekszta�ca tabel� w tekst',
  // TRVActionTableSort
  '&Sortuj...', 'Sortuj|Sortuje wiersze tabeli',
  // TrvActionTableCellLeftBorder
  'Kraw�d� &lewa', 'Kraw�d� lewa|Pokazuje lub ukrywa lew� kraw�d� zaznaczonych kom�rek',
  // TrvActionTableCellRightBorder
  'Kraw�d� &prawa', 'Kraw�d� prawa|Pokazuje lub ukrywa praw� kraw�d� zaznaczonych kom�rek',
  // TrvActionTableCellTopBorder
  'Kraw�d� &g�rna', 'Kraw�d� g�rna|Pokazuje lub ukrywa g�rn� kraw�d� zaznaczonych kom�rek',
  // TrvActionTableCellBottomBorder
  'Kraw�d� &dolna', 'Kraw�d� dolna|Pokazuje lub ukrywa doln� kraw�d� zaznaczonych kom�rek',
  // TrvActionTableCellAllBorders
  '&Wszystkie kraw�dzie', 'Wszystkie kraw�dzie|Pokazuje lub ukrywa wszystkie kraw�dzie zaznaczonych kom�rek',
  // TrvActionTableCellNoBorders
  '&Brak kraw�dzi', 'Brak kraw�dzi|Ukrywa wszystkie kraw�dzie zaznaczonych kom�rek',
  // TrvActionFonts & TrvActionFontEx
  '&Czcionka...', 'Czcionka|Zmienia czcionk� zaznaczonego tekstu',
  // TrvActionFontBold
  '&Pogrubienie', 'Pogrubienie|Zmienia styl pogrubienia zaznaczonego tekstu',
  // TrvActionFontItalic
  '&Kursywa', 'Kursywa|Zmienia styl pochylenia zaznaczonego tekstu',
  // TrvActionFontUnderline
  'Podk&re�lenie', 'Podkre�lenie|Zmienia styl podkre�lenia zaznaczonego tekstu',
  // TrvActionFontStrikeout
  'Prze&kre�lenie', 'Przekre�lenie|Zmienia styl przekre�lenia zaznaczonego tekstu',
  // TrvActionFontGrow
  '&Powieksz czcionk�', 'Powieksz czcionk�|Powi�ksza wielko�� czcionki zaznaczonego tekstu o 10%',
  // TrvActionFontShrink
  'P&omniejsz czcionk�', 'Pomniejsz czcionk�|Zmniejsza wielko�� czcionki zaznaczonego tekstu o 10%',
  // TrvActionFontGrowOnePoint
  '&Powieksz czcionk� o 1 pt', 'Powieksz czcionk� o 1 pt|Powi�ksza wielko�� czcionki zaznaczonego tekstu o 1 punkt',
  // TrvActionFontShrinkOnePoint
  'P&omniejsz czcionk� o 1 pt', 'Pomniejsz czcionk� o 1 pt|Zmniejsza wielko�� czcionki zaznaczonego tekstu o 1 punkt',
  // TrvActionFontAllCaps
  '&Wielkie litery', 'Wielkie litery|Zmienia wszystkie litery zaznaczonego tekstu na wielkie',
  // TrvActionFontOverline
  '&Nadkre�lenie', 'Nadkre�lenie|Zmienia styl nadkre�lenia zaznaczonego tekstu',
  // TrvActionFontColor
  'Kolor &tekstu...', 'Kolor tekstu|Zmienia kolor zaznaczonego tekstu',
  // TrvActionFontBackColor
  'Kolor t�a te&kstu...', 'Kolor t�a tekstu|Zmienia kolor t�a zaznaczonego tekstu',
  // TrvActionAddictSpell3
  '&Pisownia', 'Pisownia|Sprawdza pisowni�',
  // TrvActionAddictThesaurus3
  '&Tezaurus', 'Tezaurus|Wyszukuje synonimy zaznaczonego s�owa',
  // TrvActionParaLTR
  'Akapit z lewej do prawej', 'Akapit z lewej do prawej|Ustawia kierunek tekstu z lewej do prawej w zaznaczonych akapitach',
  // TrvActionParaRTL
  'Akapit z prawej do lewej', 'Akapit z prawej do lewej|Ustawia kierunek tekstu z prawej do lewej w zaznaczonych akapitach',
  // TrvActionLTR
  'Tekst z lewej do prawej', 'Tekst z lewej do prawej|Ustawia kierunek z lewej do prawej w zaznaczonym tek�cie',
  // TrvActionRTL
  'Tekst z prawej do lewej', 'Tekst z prawej do lewej|Ustawia kierunek z prawej do lewej w zaznaczonym tek�cie',
  // TrvActionCharCase
  'Wielko�� znak�w', 'Wielko�� znak�w|Zmienia wielko�� znak�w w zaznaczonym tek�cie',
  // TrvActionShowSpecialCharacters
  'Znaki &formatowania', 'Znaki formatowania|Pokazuje b�d� ukrywa znaki formatowania',
  // TrvActionSubscript
  'Indeks &dolny', 'Indeks dolny|Zmienia styl indeksu dolnego zaznaczonego tekstu',
  // TrvActionSuperscript
  'Indeks &g�rny', 'Indeks g�rny|Zmienia styl indeksu g�rnego zaznaczonego tekstu',
  // TrvActionInsertFootnote
  '&Przypis dolny', 'Przypis dolny|Wstawia przypis dolny',
  // TrvActionInsertEndnote
  'Przypis &ko�cowy', 'Przypis ko�cowy|Wstawia przypis ko�cowy',
  // TrvActionEditNote
  'E&dytuj tre�� przypisu', 'Edytuj tre�� przypisu|Rozpoczyna edycj� tre�ci przypisu',
  'Ukryj', 'Ukryj|Ukrywa lub pokazuje zaznaczony fragment',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Style...', 'Style|Otwiera okno zarz�dzania stylami',
  // TrvActionAddStyleTemplate
  'Dod&aj styl...', 'Dodaj styl|Tworzy nowy styl tekstu lub akapitu',
  // TrvActionClearFormat,
  'Wy&czy�� formatowanie', 'Wyczy�� formatowanie|Usuwa formatowanie tekstu i akapitu z zaznaczonego fragmentu',
  // TrvActionClearTextFormat,
  'Wyczy�� formatowanie &tekstu', 'Wyczy�� formatowanie tekstu|Usuwa formatowanie z zaznaczonego tekstu',
  // TrvActionStyleInspector
  '&Inspektor styl�w', 'Inspektor styl�w|Pokazuje lub ukrywa inspektora styl�w',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Anuluj', 'Zamknij', 'Wstaw', '&Otw�rz...', '&Zapisz...', '&Wyczy��', 'Pomoc', 'Usu�',
  // Others  -------------------------------------------------------------------
  // percents,
  'procent',
  // left, top, right, bottom sides
  'Lewa', 'G�ra', 'Prawa', 'D�',
  // save changes? confirm title
  'Zapisz zmiany do %s?', 'Potwierd�',
  // warning: losing formatting
  '%s mo�e zawiera� funkcje, kt�re nie s� zgodne z wybranym formatem.'#13+
  'Czy chcesz zapisa� dokument w tym formacie?',
  // RVF format name
  'RVF format',
  // Error messages ------------------------------------------------------------
  'B��d',
  'B��d wczytywania pliku.'#13#13'Przypuszczalne powody:'#13'- format wczytywanego pliku nie jest obs�ugiwany;'#13+
  '- plik jest uszkodzony;'#13'- plik jest w u�yciu.',
  'B��d wczytywania obrazu.'#13#13'Przypuszczalne powody:'#13'- format wczytywanego obrazu nie jest obs�ugiwany;'#13+
  '- plik nie zawiera obrazu;'#13+
  '- plik jest uszkodzony;'#13'- plik jest w u�yciu.',
  'B��d zapisu pliku.'#13#13'Przypuszczalne powody:'#13'- brak miejsca na dysku;'#13+
  '- dysk jest zabezpieczony przed zapisem;'#13'- dysk wymmienny nie zosta� w�o�ony;'#13+
  '- plik jest w u�yciu;'#13'- no�nik jest uszkodzony.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Pliki RVF (*.rvf)|*.rvf',  'Pliki RTF (*.rtf)|*.rtf' , 'Pliki XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Pliki tekstowe(*.txt)|*.txt', 'Pliki tekstowe - Unicode (*.txt)|*.txt', 'Pliki tekstowe - wykryj automatycznie (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Pliki HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - upro�� (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Dokument programu Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Wyszukiwanie zako�czone', 'Wyszukiwany tekst ''%s'' nie zosta� znaleziony.',
  // 1 string replaced; Several strings replaced
  'Zastapiono 1 ci�g znak�w.', 'Liczba zast�pionych ci�g�w znak�w wynosi: %d.',
  // continue search from the beginning/end?
  'Osi�gni�to koniec dokumentu, rozpocz�� od pocz�tku?',
  'Osi�gni�to pocz�tek dokumentu, rozpocz�� od ko�ca?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Przezroczysty', 'Automatyczny',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Czarny', 'Br�zowy', 'Oliwkowy', 'Ciemnozielony', 'Szarob��kitny', 'Ciemnoniebieski', 'Indygo', 'Szary 80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Ciemnoczerwony', 'Pomara�czowy', 'Ciemno��ty', 'Zielony', 'Ciemnob��kitny', 'Niebieski', 'Niebieskoszary', 'Szary 50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Czerwony', 'Jasnopomara�czowy', 'Zielony', 'Morski', 'B��kitny', 'Jasnoniebieski', 'Fioletowy', 'Szary 40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'R�owy', 'Z�oty', '��ty', 'Jasnozielony', 'Turkusowy', 'Lazurowy', '�liwkowy', 'Szary 25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'R�any', 'Be�owy', 'Jasno��ty', 'Bladozielony', 'Pastelowob��kitny', 'Bladoniebieski', 'Liliowy', 'Bia�y',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Przez&roczysty', '&Automatyczny', '&Wi�cej kolor�w...', '&Domy�lny',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'T�o', 'K&olor:', 'Pozycja obrazu', 'T�o', 'Tekst tekst tekst tekst tekst.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Brak', '&Rozci�gnij', '&Na zak�adk�', '&S�siaduj�co', '&Centralnie',
  // Padding button
  '&Marginesy...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Kolor wype�nienia', '&Zastosuj do:', '&Wi�cej kolor�w...', '&Marginesy...',
  'Prosz� wybra� kolor',
  // [apply to:] text, paragraph, table, cell
  'tekst', 'akapit' , 'tabela', 'kom�rka',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Czcionka', 'C&zcionka', '&Odst�py mi�dzy znakami',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Czcionka:', '&Rozmiar:', 'Styl czcionki', '&Pogrubienie', '&Kursywa',
  // Script, Color, Back color labels, Default charset
  '&Skrypt:', 'Ko&lor:', '&T�o:', '(Domy�lny)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efekty', 'Podkre�lenie', '&Nadkre�lenie', 'Prz&ekre�lenie', '&Wielkie litery',
  // Sample, Sample text
  'Podgl�d', 'Przyk�adowy tekst',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Ods�p', 'O&dst�p:', '&Rozstrzelone', 'Z&ag�szczony',
  // Offset group-box, Offset label, Down, Up,
  'Po�o�enie', '&Po�o�enie:', 'O&bni�one', 'Pod&niesione',
  // Scaling group-box, Scaling
  'Skalowanie', 'Ska&la:',
  // Sub/super script group box, Normal, Sub, Super
  'Indeks g�ny i dolny', 'Nor&malny', 'Indeks doln&y', 'Indeks &g�rny',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Marginesy', '&G�rny:', '&Lewy:', '&Dolny:', '&Prawy:', '&Jednakowe',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Hiper��cze', 'Hiper��cze', '&Etykieta:', '&Lokalizacja:', '<<Wyb�r>>',
  // cannot open URL
  'Nieudane przej�cie do "%s"',
  // hyperlink properties button, hyperlink style
  '&Dostosuj...', '&Style:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Atrybuty hiper��cza', 'Niewaktywne', 'Aktywne',
  // Text [color], Background [color]
  'T&ekst:', '&T�o',
  // Underline [color]
  'Podk&re�lenie:',
  // Text [color], Background [color] (different hotkeys)
  'Te&kst:', 'T�&o',
  // Underline [color], Attributes group-box,
  'Podk&re�lenie:', 'Atrybuty',
  // Underline type: always, never, active (below the mouse)
  'Podk&re�lenie:', 'zawsze', 'nigdy', 'aktywne',
  // Same as normal check-box
  'Takie same jak &nieaktywne',
  // underline active links check-box
  '&Podkre�l aktywne ��cza',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Wstaw symbol', 'C&zcionka:', '&Kodowanie:', 'Unicode',
  // Unicode block
  '&Blok:',
  // Character Code, Character Unicode Code, No Character
  'Kod znaku: %d', 'Kod znaku: Unicode %d', '(brak)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Wstaw tabel�', 'Rozmiar tabeli', 'Liczba &kolumn:', 'Liczba &wierszy:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Zachowanie autodopasowania', '&Automatyczne', 'Autodopasowanie do &okna', 'Dopasowanie &manualne',
  // Remember check-box
  '&Ustaw jako domy�lne dla nowych tabel',
  // VAlign Form ---------------------------------------------------------------
  'Pozycja',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'W�a�ciwo�ci', 'Obraz', 'Rozmiar i pozycja', '&Linie', 'T&abela', '&Wiersze', '&Kom�rki',
  // Image Appearance, Image Misc, Number tabs
  'Wygl�d', 'Inne', 'Licznik',
  // Box position, Box size, Box appearance tabs
  'Pozycja', 'Rozmiar', 'Wygl�d',
  // Preview label, Transparency group-box, checkbox, label
  'Podgl�d:', 'Wype�nienie', '&Przezroczysty', '&Kolor:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Z&mie�...', '&Zapisz...', 'Automatyczny',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Wyr�wnywanie', '&Wyr�wnaj:',
  'do do�u', 'do �rodka',
  'g�ra do g�rnej cz�ci linii', 'd� do dolnej cz�ci linii', '�rodek do �rodkowej cz�ci linii',
  // align to left side, align to right side
  'do lewej strony', 'do prawej strony',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Przesuni�cie:', 'Rozmiar', 'S&zeroko��:', 'W&ysoko��:', 'Domy�lny rozmiar: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Skaluj proporcjonalnie', '&Resetuj',
  // Inside the border, border, outside the border groupboxes
  'Obramowanie', 'Kraw�dzie', 'Odst�py',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Kolor:', '&Szeroko��:',
  // Border width, Border color labels
  'S&zeroko��:', 'Kolo&r:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'P&oziomy:', 'P&ionowy:',
  // Miscellaneous groupbox, Tooltip label
  'Inne', '&Podpowied�:',
  // web group-box, alt text
  'Sie� Web', '&Tekst alternatywny:',
  // Horz line group-box, color, width, style
  'Linia pozioma', '&Kolor:', '&Szeroko��:', '&Styl:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabela', 'Sz&eroko��:', 'K&olor wype�nienia:', '&Odst�py kom�rek...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Marginesy poziome kom�rek:', 'M&arginesy pionowe kom�rek:', 'Kraw�dzie i t�o',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Wi&doczne kraw�dzie...', 'Automatyczna', 'automatyczna',
  // Keep row content together checkbox
  'Zachowaj wi&ersze razem',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotacja', '&Brak', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Kraw�dzie:', 'Widocz&ne kraw�dzie...',
  // Border, CellBorders buttons
  'K&raw�dzie tabeli...', 'Kraw�dz&ie kom�rek...',
  // Table border, Cell borders dialog titles
  'Kraw�dzie tabeli', 'Kraw�dzie kom�rek',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Drukowanie', '&Zachowj na jednej stronie', 'Liczba wiersz&y nag��wka:',
  'Wiersze nag��wka b�d� powtarzane na ka�dej stronie',
  // top, center, bottom, default
  'Do &g�ry', 'Do �ro&dka', 'Do do�&u', 'Do&my�lnie',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Ustawienia', 'Preferowana &szeroko��:', 'Preferowana w&ysoko��:', 'Kolor wype�&nienia:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kraw�dzie', 'Widoczne kraw�dzie:',  'Kolor &cienia:', 'Kolor �w&iat�a', 'Ko&lor:',
  // Background image
  '&T�o...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Pozycja pozioma', 'Pozycja pionowa', 'Pozycja w tek�cie',
  // Position types: align, absolute, relative
  'Wyr�wnanie', 'Po�o�enie bezwzgl�dne', 'Po�o�enie wzgl�dne',
  // [Align] left side, center, right side
  'lewy bok ramki do lewego boku',
  '�rodek ramki do �rodka',
  'prawy bok ramki do prawego boku',
  // [Align] top side, center, bottom side
  'g�rny bok ramki do g�rnego boku',
  '�rodek ramki do �rodka',
  'dolny bok ramki do dolnego boku',
  // [Align] relative to
  'wzgl�dem',
  // [Position] to the right of the left side of
  'od prawego boku ramki do lewego boku',
  // [Position] below the top side of
  'poni�ej g�rnekgo boku',
  // Anchors: page, main text area (x2)
  '&Strony', '&Obszaru tekstu', 'St&rony', 'Obszaru &tekstu',
  // Anchors: left margin, right margin
  '&Lewego marginesu', '&Prawego marginesu',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&G�rnego marginesu', '&Dolnego marginesu', '&Wewn�trznego marginesu', 'Z&ewn�trznego marginesu',
  // Anchors: character, line, paragraph
  '&Znacznika', '&Linii', '&Akapitu',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&Marginesy odwr�cne', '&Uk�ad w kom�rce tabeli',
  // Above text, below text
  '&Przed tekstem', '&Za tekstem',
  // Width, Height groupboxes, Width, Height labels
  'Szeroko��', 'Wysoko��', 'Szero&ko��:', 'W&ysoko��:',
  // Auto height label, Auto height combobox item
  'Automatyczna', 'automatycznie',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Wyr�wnaie pionowe', 'Do &g�ry', 'Do �ro&dka', 'Do do�&u',
  // Border and background button and title
  '&Kraw�dzie i t�o...', 'Kraw�dzie i t�o',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Wklej specjalnie', '&Wstaw jako:', 'Format RTF', 'Format HTML',
  'Tekst', 'Tekst unicode', 'Obraz - bitmapa', 'Obraz - metaplik',
  'Pliki graficzne',  'Adres',
  // Options group-box, styles
  'Opcje', '&Style:',
  // style options: apply target styles, use source styles, ignore styles
  'zastosuj style dokumentu docelowego', 'zachowaj style i wygl�d',
  'zachowaj wygl�d, ignoruj style',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Wypunktowanie i numerowanie', '&Wypunktowane', '&Numerowane', 'Lista punktator�w', 'Lista numerator�w',
  // Customize, Reset, None
  '&Dostosuj...', '&Zresetuj', '(Brak)',
  // Numbering: continue, reset to, create new
  'kontynuuj numerowanie', 'zresetuj numerowanie, zaczynaj�c do', 'utw�rz now� list�, zaczynaj�c od',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Du�e litery', 'Du�e rzymskie', 'Liczby', 'Ma�e litery', 'Ma�e rzymskie',
  // Bullet type
  'Ko�o', 'Dysk', 'Kwadrat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Poziom:', '&Zacznij od:', '&Kontynuuj', 'Numerowanie',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Dostosuj list�', 'Poziomy', '&Liczba:', 'W�a�ciwo�ci listy', '&Typ listy:',
  // List types: bullet, image
  'Wypunktowanie', 'Obraz', 'Wstaw numeracj�|',
  // Number format, Number, Start level from, Font button
  'Format &numeracji:', 'Numeracja', '&Rozpocznij poziom od:', '&Czcionka...',
  // Image button, bullet character, Bullet button,
  '&Obraz...', 'Zn&ak wypunktowania', '&Znak...',
  // Position of list text, bullet, number, image, text
  'Pozycje', 'Pozycja punktora', 'Pozycja numeru', 'Pozycja obrazu', 'Pozycja tekstu',
  // at, left indent, first line indent, from left indent
  '&od:', 'Lewe &wci�cie:', '&Pierwszy wiersz:', 'od lewego wci�cia',
  // [only] one level preview, preview
  'Podgl�d &jednego poziomu', 'Podgl�d',
  // Preview text
  'Tekst tekst tekst tekst tekst. Text tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'wyr�wnaj do lewej', 'wyr�wnaj do prawej', 'wy�rodkuj',
  // level #, this level (for combo-box)
  'Poziom %d', 'Ten poziom',
  // Marker character dialog title
  'Edytuj znak punktora',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Obramowanie i t�o akapitu', '&Kraw�dzie', '&T�o',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Ustawienia', 'Ko&lor:', '&Szeroko��:', 'O&dst�p:', '&Marginesy...',
  // Sample, Border type group-boxes;
  'Podgl�d', 'Rodzaj kraw�dzi',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Brak', '&Pojedyncza', 'Pod&w�jna', 'Pot&r�jna', 'Sklej w&ewn�trznie', 'Sklej &zewn�trznie',
  // Fill color group-box; More colors, padding buttons
  'Kolor wype�nienia', '&Wi�cej kolor�w...', '&Marginesy...',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // title and group-box in Offsets dialog
  'Marginesy wewn�trzne', 'Marginesy',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Akapit', 'Wyr�wnanie do', '&lewej', 'pr&awej', '�ro&dka', 'lewej &i prawej',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Odst�py', 'P&rzed:', '&Po:', 'Mi�d&zy wierszami:', '&Co:',
  // Indents group-box; indents: left, right, first line
  'Wci�cia', 'Od l&ewej:', '&Od prawej:', 'Pierwszy &wiersz:',
  // indented, hanging
  'Wci�&ty', 'Wy&suni�ty', 'Podgl�d',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Pojedyncze', '1.5 wiersza', 'Podw�jne', 'Przynajmniej', 'Dok�adnie', 'Wielokrotne',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Wci�cia i odst�p&y', 'Ta&bulatory', 'Podzia� tekst&u',
  // tab stop position label; buttons: set, delete, delete all
  'Po�o�enie &tabulatora:', '&Ustaw', 'U&su�', 'Usu� &wszystkie',
  // tab align group; left, right, center aligns,
  'Wyr�wnanie', 'Do &lewej', 'Do pr&awej', 'Do �ro&dka',
  // leader radio-group; no leader
  '&Znaki wiod�ce', 'Brak',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulatory do usuni�cia:', '(Brak)', 'Wszystkie.',
  // Pagination group-box, keep with next, keep lines together
  'Podzia� na strony', '&Razem z nast�pnym', 'Zachowaj wi&ersze razem',
  // Outline level, Body text, Level #
  '&Poziom konspektu:', 'Tekst podstawowy', 'Poziom %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Podgl�d wydruku', 'Szeroko�� strony', 'Ca�a strona', 'Strony:',
  // Invalid Scale, [page #] of #
  'Podaj liczb� pomi�dzy 10 a 500', 'z %d',
  // Hints on buttons: first, prior, next, last pages
  'Piewrsza strona', 'Poprzednia strona', 'Nast�pna strona', 'Ostatnia strona',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Odst�py kom�rek', 'Odst�py', '&mi�dzy kom�rkami', 'od kraw�dzi tabeli',
  // vertical, horizontal (x2)
  '&Pionowy:', 'P&oziomy:', 'P&ionowy:', 'Po&ziomy:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kraw�dzie', 'Ustawienia', 'K&olor:', 'Kolor �w&iat�a:', 'Kolor &cienia:',
  // Width; Border type group-box;
  '&Szeroko��:', 'Rodzaj kraw�dzi',
  // Border types: none, sunken, raised, flat
  '&Brak', '&Zapadni�ta', '&Wypuk�a', '&P�aska',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Podziel', 'Podziel na',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Podaj liczb� wierszy i kolumn', '&Roz��czne kom�rki',
  // number of columns, rows, merge before
  'Liczba &kolumn:', 'Liczba &wierszy:', '&Scal kom�rki przed podzia�em',
  // to original cols, rows check-boxes
  'Roz��cz k&olumny', 'Roz��cz wi&ersze',
  // Add Rows form -------------------------------------------------------------
  'Dodaj wiersze', '&Liczba wierszy:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Ustawienia strony', 'Stron&a', 'Nag��wek &i stopka',
  // margins group-box, left, right, top, bottom
  'Marginesy (milimetry)', 'Marginesy (cale)', 'L&ewy:', '&G�rny:', '&Prawy:', 'D&olny:',
  // mirror margins check-box
  'Marginesy lustr&zane',
  // orientation group-box, portrait, landscape
  'Orientacja', 'P&ionowa', 'Pozio&ma',
  // paper group-box, paper size, default paper source
  'Papier', 'Rozmia&r:', '�r�&d�o:',
  // header group-box, print on the first page, font button
  'Nag��wek', '&Tekst:', '&Nag��wek na pierwszej stronie', '&Czcionka...',
  // header alignments: left, center, right
  'Do &lewej', 'Do �&rodka', 'Do &prawej',
  // the same for footer (different hotkeys)
  'Stopka', 'Te&kst:', '&Stopka na pierwszej stronie', 'C&zcionka...',
  'Do l&ewej', 'Do �r&odka', 'Do prawe&j',
  // page numbers group-box, start from
  'Numery stron', 'Zacznij od nu&meru:',
  // hint about codes
  'Znaki specjalne:'#13'&&p - numer strony; &&P - liczba stron; &&d - bie��ca data; &&t - bie��cy czas.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Strona kodowa pliku tekstowego', '&Wybierz spos�b kodowania pliku:',
  // thai, japanese, chinese (simpl), korean
  'Tajski', 'Japo�ski', 'Chi�ski (uproszczony)', 'Korea�ski',
  // chinese (trad), central european, cyrillic, west european
  'Chi�ski (tradycyjny)', '�rodkowoeuropejski', 'Cyrylica', 'Zachodnioeuropejski',
  // greek, turkish, hebrew, arabic
  'Grecki', 'Turecki', 'Hebrajski', 'Arabski',
  // baltic, vietnamese, utf-8, utf-16
  'Ba�tycki', 'Wietnamski', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Styl', 'Wybierz &styl:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Przekszta�� w tekst', 'Wy&bierz separator:',
  // line break, tab, ';', ','
  'podzia� linii', 'tabulacja', '�rednik', 'przecinek',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabele zawieraj�ce scalone wiersze nie m�g� by� sortowane.',
  // title, main options groupbox
  'Sortowanie tabeli', 'Sortuj',
  // sort by column, case sensitive
  '&Sortuj wg kolumny:', 'Rozr�niaj &wielko�� znak�w',
  // heading row, range or rows
  'Pomi� wiersz &nag��wka', 'Sortuj wiersze: od %d do %d',
  // order, ascending, descending
  'Porz�dek', '&Rosn�cy', '&Malej�cy',
  // data type, text, number
  'Typ danych', '&Tekst', '&Liczba',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Wstaw licznik', 'W�a�ciwo�ci', '&Nazwa licznika:', 'Nu&merowanie:',
  // numbering groupbox, continue, start from
  'Numerowanie', 'K&ontynuuj', '&Rozpocznij od:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Podpis', '&Etykieta:', 'W&yklucz etykiet� z podpisu',
  // position radiogroup
  'P&o�o�enie', 'Po&wy�ej wybranego obiektu', 'Po&nie�ej wybranego obiektu',
  // caption text
  '&Podpis:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Licznik', 'Obraz', 'Tabela',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Widoczne kraw�dzie', 'Kraw�dzie',
  // Left, Top, Right, Bottom checkboxes
  '&Lewa', '&G�rna', '&Prawa', '&Dolna',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Szeroko�� kolumny', 'Wysoko�� wiersza',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Wci�cie pierwszego wiersza', 'Wci�cie z lewej', 'Wysuni�cie', 'Wci�cie z prawej',
  // Hints on lists: up one level (left), down one level (right)
  'Poziom do g�ry', 'Poziom w d�',
  // Hints for margins: bottom, left, right and top
  'Dolny margines', 'Lewy margines', 'Prawy margines', 'G�rny margines',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulator lewy', 'Tabulator prawy', 'Tabulator �rodkowy', 'Tabulator dziesi�tny',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normalny', 'Wci�cie normalne', 'Bez odst�p�w', 'Nag��wek %d', 'Akapit z list�',
  // Hyperlink, Title, Subtitle
  'Hiper��cze', 'Tytu�', 'Podtytu�',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Uwydatnienie', 'Wyr�nienie delikatne', 'Wyr�nienie intensywne', 'Wzmocnienie',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cytat', 'Cytat intnsywny', 'Odwo�anie delikatne', 'Odwo�anie intensywne',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Tekst blokowy', 'HTML - zmienna', 'HTML - kod', 'HTML - akronim',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML - definicja', 'HTML - klawiatura', 'HTML - przyk�ad', 'HTML - sta�a szeroko��',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML - wst�pnie sformatowany', 'HTML - cytat', 'Nag��wek', 'Stopka', 'Numer strony',
  // Caption
  'Podpis',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Odsy�acz przypisu ko�cowego', 'Odsy�acz przypisu dolnego', 'Tekst przypisu ko�cowego', 'Tekst przypisu dolnego',
  // Sidenote Reference, Sidenote Text
  'Odsy�acz przypisu bocznego', 'Tekst przypisu bocznego',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'kolor', 'kolor t�a', 'przezroczysty', 'domy�lny', 'kolor podkre�lenie',
  // default background color, default text color, [color] same as text
  'domy�lny kolor t�a', 'domy�lny kolor tekstu', 'taki sam jak tekst',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'pojedyncze', 'grube', 'podw�jne', 'wykropkowane', 'grube kropki', 'kreski',
  // underline types: thick dashed, long dashed, thick long dashed,
  'grube kreski', 'd�ugie kreski', 'd�ugie grube kreski',
  // underline types: dash dotted, thick dash dotted,
  'kropka kreska', 'gruba kropka kreska',
  // underline types: dash dot dotted, thick dash dot dotted
  'kreska kropka kropka', 'gruba kreska kropka kropka',
  // sub/superscript: not, subsript, superscript
  'bez indeksu', 'indeks dolny', 'indeks g�rny',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'kierunek:', 'odziedziczony', 'z lewej do prawej', 'z prawej do lewej',
  // bold, not bold
  'pogrubienie', 'bez pogrubienia',
  // italic, not italic
  'kursywa', 'bez kursywy',
  // underlined, not underlined, default underline
  'podkre�lenie', 'bez podkre�lenia', 'domy�lne podkre�lenie',
  // struck out, not struck out
  'przekre�lenie', 'bez przekre�lenia',
  // overlined, not overlined
  'nadkre�lenie', 'bez nadkre�lenia',
  // all capitals: yes, all capitals: no
  'kapitaliki', 'bez kapitalik�w',
  // vertical shift: none, by x% up, by x% down
  'bez przesuni�cia', 'podniesiony o %d%%', 'opuszczony o %d%%',
  // characters width [: x%]
  'szeroko�� znak�w',
  // character spacing: none, expanded by x, condensed by x
  'normalny odst�p', 'odst�p rozstrzelony o %s', 'odst�p zag�szczony o %s',
  // charset
  'skrypt',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'domy�lna', 'domy�lny', 'odziedziczone',
  // [hyperlink] highlight, default hyperlink attributes
  'pod�wietlenie', 'domy�lne',
  // paragraph aligmnment: left, right, center, justify
  'do lewej', 'do prawej', 'do �rodka', 'wyjustowany',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'wysoko�� wiersza: %d%%', 'odst�p mi�dzy wierszami: %s', 'wysoko�� wiersza: przynajmniej %s',
  'wysoko�� wiersza: dok��dnie %s',
  // no wrap, wrap
  'bez zawijania wierszy', 'zawijanie wierszy',
  // keep lines together: yes, no
  'zachowaj wiersze razem', 'nie zachowuj wierszy razem',
  // keep with next: yes, no
  'zachowaj razem z nast�pnym akapitem', 'nie zachowuj razem z nast�pnym akapitem',
  // read only: yes, no
  'tylko do odczytu', 'do edycji',
  // body text, heading level x
  'poziom konspektu: tre��', 'poziom konspektu: %d',
  // indents: first line, left, right
  'wci�cie pierwszego wiersza', 'lewe wci�cie', 'prawe wci�cie',
  // space before, after
  'odst�p przed', 'odst�p po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulatory', 'brak', 'wyr�wnanie', 'do lewej', 'do prawej', 'do �rodka',
  // tab leader (filling character)
  'znak wiod�cy',
  // no, yes
  'nie', 'tak',
  // [padding/spacing/side:] left, top, right, bottom
  'lewy', 'g�rny', 'prawy', 'dolny',
  // border: none, single, double, triple, thick inside, thick outside
  'brak', 'pojedyncze', 'podw�jne', 'potr�jne', 'sklejone wewn�trznie', 'sklejone zewn�trznie',
  // background, border, padding, spacing [between text and border]
  't�o', 'kraw�d�', 'margines', 'odst�p',
  // border: style, width, internal width, visible sides
  'styl', 'szeroko��', 'wewn�trzna szeroko��', 'widoczne kraw�dzie',
  // style inspector -----------------------------------------------------------
  // title
  'Inspektor styl�w',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Styl', '(brak)', 'Akapit', 'Czcionka', 'Atrybuty',
  // border and background, hyperlink, standard [style]
  'Obramowanie i t�o', 'Hiper��cze', '(Standardowy)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Style', 'Styl',
  // name, applicable to,
  '&Nazwa:', 'Stosowany &do:',
  // based on, based on (no &),
  '&Oparty na:', 'Oparty na:',
  //  next style, next style (no &)
  'Styl nast�pnego &akapitu:', 'Styl nast�pnego akapitu:',
  // quick access check-box, description and preview
  '&Szybki dost�p', 'Opis i &podgl�d:',
  // [applicable to:] paragraph and text, paragraph, text
  'akapitu i tekstu', 'akapitu', 'tekstu',
  // text and paragraph styles, default font
  'Style tekstu i akapitu', 'Domy�lna czcionka',
  // links: edit, reset, buttons: add, delete
  'Zmie�', 'Przywr��', '&Dodaj', '&Usu�',
  // add standard style, add custom style, default style name
  'Dodaj styl &standardowy...', 'Dodaj styl &w�asny', 'Styl %d',
  // choose style
  'Wybierz &styl:',
  // import, export,
  '&Import...', '&Eksport...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Style RichView (*.rvst)|*.rvst', 'B��d wczytywania pliku', 'Plik nie zawiera �adnych styl�w',
  // Title, group-box, import styles
  'Import styl�w z pliku', 'Import', 'I&mport styl�w:',
  // existing styles radio-group: Title, override, auto-rename
  'Je�li styl istnieje to', '&nadpisz', '&dodaj pod zmienion� nazw�',
  // Select, Unselect, Invert,
  '&Zaznacz', '&Odznacz', 'Od&wr��',
  // select/unselect: all styles, new styles, existing styles
  '&Wszystkie style', '&Nowe style', '&Istniej�ce style',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Wyczy�� wirmatowanie', 'Wszystkie style',
  // dialog title, prompt
  'Style', '&Wybierz style:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonimy', 'Pom&i� wszystkie', 'Dod&aj do S�ownika',
  // Progress messages ---------------------------------------------------------
  'Pobieranie %s', 'Przygotowanie do druku...', 'Drukowanie strony %d',
  'Konwertowanie z RTF...',  'Konwertowanie do RTF...',
  'Odczytywanie %s...', 'Zapisywanie %s...', 'tekst',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Brak przypisu', 'Przypis dolny', 'Przypis ko�cowy', 'Przypis boczny', 'Przypis s�siaduj�cy'
  );
initialization
  RVA_RegisterLanguage(
    'Polish', // english language name, do not translate
    'Polski', // native language name
    EASTEUROPE_CHARSET, @Messages);
end.

