{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       French Translation                              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Boualem Koudri, 2003-02-07             }
{ Modified by :  JJM,Michael REMY, Jean-Philippe RENOU  }
{                Damien Griessinger: 2011-02-20         }
{                                    2012-09-08         }
{                Gilles Vasseur:     2014-02-03         }
{*******************************************************}

unit RVAL_Fr;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'pouces', 'cm', 'mm', 'picas', 'pixels', 'points',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
   '''''', 'cm', 'mm', 'pc', 'pixels', 'pt', 
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fichier', '�&dition', 'Forma&t', '&Police', 'P&aragraphe', '&Insertion', 'Ta&bleau', 'Fe&n�tre', 'Aid&e',
  // exit
  '&Quitter',
  // top-level menus: View, Tools,
  '&Affichage', '&Outils',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Ta&ille', 'S&tyle', 'S�lecti&on', 'Alig&nement du contenu de la cellule', 'Bordure de la c&ellule',
  // menus: Table cell rotation
  '&Rotation de la cellule',
  // menus: Text flow, Footnotes/endnotes
  '&Encha�nements', '&Notes', 
  // ribbon tabs: tab1, tab2, view, table
  'Acc&ueil', '&Avanc�', 'A&ffichage', '&Tableau', 
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Presse-papier', 'Police', 'Paragraphe', 'Liste', '�dition',
  // ribbon groups: insert, background, page setup,
  'Ins�rer', 'Trame de fond', 'Mise en page', 
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Liens', 'Notes de bas de page', 'Affichages document', 'Afficher/Masquer', 'Zoom', 'Options',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Ins�rer', 'Supprimer', 'Op�rations', 'Bordures', 
  // ribbon groups: styles 
  'Styles',
  // ribbon screen tip footer,
  'Appuyez sur F1 pour de l''aide.',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documents r�cents', 'Enregistrer une copie du document', 'Aper�u et impression du document',
  // ribbon label: units combo
  'Unit�s:', 
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nouveau', 'Nouveau|Cr�e une nouvelle page vierge',
  // TrvActionOpen
  '&Ouvrir...', 'Ouvrir|Ouvre un document � partir du disque',
  // TrvActionSave
  '&Enregistrer', 'Enregistrer|Enregistre le document en cours',
  // TrvActionSaveAs
  'Enregistrer &sous...', 'Enregistrer sous...|Enregistre le document sous un nouveau nom',
  // TrvActionExport
  'Expor&ter...', 'Exporter|Exporte le document vers un autre format',
  // TrvActionPrintPreview
  'A&per�u avant impression', 'Aper�u avant l''impression|Montre le document avant l''impression',
  // TrvActionPrint
  '&Imprimer...', 'Imprimer|Configure l''imprimante et imprime le document',
  // TrvActionQuickPrint
  '&Imprimer', 'Impression ra&pide', 'Imprimer|Imprime le document', 
   // TrvActionPageSetup
  'Options de pa&ge...', 'D�finir les options de page|D�finit les marges, la taille du papier, l''orientation, la source, le haut et le bas de page',
  // TrvActionCut
  'C&ouper', 'Couper|Coupe la s�lection (vers le presse-papier) ',
  // TrvActionCopy
  '&Copier', 'Copier|Copie la s�lection dans le presse-papier',
  // TrvActionPaste
  'Co&ller', 'Coller|Ins�re le contenu du presse-papier',
  // TrvActionPasteAsText
  'Coller en tant que &texte', 'Coller comme texte|Ins�re le texte du presse-papier', // dg
  // TrvActionPasteSpecial
  'Collage &sp�cial...', 'Collage sp�cial|Ins�re le contenu du presse-papier au format sp�cifi�',
  // TrvActionSelectAll
  '&Tout s�lectionner ', 'Tout s�lectionner|S�lectionne tout le document',
  // TrvActionUndo
  '&Annuler', 'Annuler|Annule la derni�re action',
  // TrvActionRedo
  'Re&faire', 'Refaire|R�p�te la derni�re action annul�e',
  // TrvActionFind
  '&Rechercher...', 'Rechercher|Recherche le texte sp�cifi� dans le document',
  // TrvActionFindNext
  'Rechercher &suivant', 'Rechercher le suivant|Continue la derni�re recherche',
  // TrvActionReplace
  'Re&mplacer...', 'Remplacer|Trouve et remplace le texte sp�cifi� dans le document',
  // TrvActionInsertFile
  '&Fichier...', 'Ins�rer un fichier|Ins�re le contenu d''un fichier dans le document',
  // TrvActionInsertPicture
  '&Image...', 'Ins�rer une image|Ins�re une image � partir d''un fichier',
  // TRVActionInsertHLine
  '&Ligne horizontale', 'Ins�re une ligne horizontale',
  // TRVActionInsertHyperlink
  'Lien &hypertexte...', 'Ins�re un lien hypertexte ',
  // TRVActionRemoveHyperlinks
  'Supp&rimer les liens hypertextes', 'Supprimer les liens hypertextes|Supprime tous les liens hypertextes dans le texte s�lectionn�',
  // TrvActionInsertSymbol
  '&Symbole...', 'Ins�rer un symbole',
  // TrvActionInsertNumber
  '&Num�ro...', 'Ins�rer un num�ro|Ins�re un num�ro',
  // TrvActionInsertCaption
  'Ins�rer une &l�gende...', 'Ins�rer une l�gende|Ins�re une l�gende � l''objet s�lectionn�',
  // TrvActionInsertTextBox
  'Bo�te de &texte', 'Ins�rer une bo�te de texte|Ins�re une bo�te de texte',
  // TrvActionInsertSidenote
  '&Annotation', 'Ins�rer une annotation|Ins�re une note dans une bo�te de texte',
  // TrvActionInsertPageNumber
  '&Num�ro de page', 'Ins�rer un num�ro de page|Ins�re un num�ro de page',
  // TrvActionParaList
  'P&uces et num�rotation ...', 'Puces et num�rotation|Applique les puces et la num�rotation au paragraphe s�lectionn� ',
  // TrvActionParaBullets
  '&Puces', 'Puces|Ajoute ou supprime les puces du paragraphe',
  // TrvActionParaNumbering
  '&Num�rotation', 'Num�rotation|Ajoute ou supprime la num�rotation du paragraphe',
  // TrvActionColor
  'Couleur &arri�re-plan...', 'Couleur de l''arri�re-plan|Change la couleur de l''arri�re-plan du document',
  // TrvActionFillColor
  'Couleur de &remplissage...', 'Remplir avec la couleur|Change la couleur du texte (paragraphe, cellule, tableau ou document)',
  // TrvActionInsertPageBreak
  '&Ins�rer un saut de page', 'Ins�rer un saut de page',
  // TrvActionRemovePageBreak
  '&Supprimer le saut de page', 'Supprimer le saut de page',
  // TrvActionClearLeft
  // 'Clear Text Flow at &Left Side', 'Clear Text Flow at Left Side|Places this paragraph below any left-aligned picture',
  'Effacer l''encha�nement du c�t� &gauche','Effacer l''encha�nement du c�t� gauche|Place le paragraphe sous toute image align�e � gauche',
  // TrvActionClearRight
  // 'Clear Text Flow at &Right Side', 'Clear Text Flow at Right Side|Places this paragraph below any right-aligned picture',
  'Effacer l''encha�nement du c�t� &droit','Effacer l''encha�nement du c�t� droit|Place le paragraphe sous toute image align�e � droite',
  // TrvActionClearBoth
  // 'Clear Text Flow at &Both Sides', 'Clear Text Flow at Both Sides|Places this paragraph below any left- or right-aligned picture',
  'Effacer l''encha�nement des deux &c�t�s','Effacer l''encha�nement des deux c�t�s|Place le paragraphe sous toute image (plac�e � droite ou � gauche)',
  // TrvActionClearNone
  // '&Normal Text Flow', 'Normal Text Flow|Allows text flow around both left- and right-aligned pictures',
  '&Encha�nement normal', 'Encha�nement normal|Autorise un encha�nement autour des images',
  // TrvActionVAlign
  'Position de l''&objet...', 'Position de l''objet|Modifie la position de l''objet choisi',
  // TrvActionItemProperties
  '&Propri�t�s de l''objet...', 'Propri�t�s de l''objet|D�finit les propri�t�s de l''objet actif',
  // TrvActionBackground
  '&Arri�re-plan...', 'Arri�re-plan|Choisit un arri�re-plan couleur et image',
  // TrvActionParagraph
  '&Paragraphe...', 'Paragraphe|Change les attributs du paragraphe',
  // TrvActionIndentInc
  '&Augmenter l''espacement', 'Augmenter l''espacement|Augmente l''espacement gauche du paragraphe s�lectionn�',
  // TrvActionIndentDec
  '&Diminuer l''espacement', 'Diminuer l''espacement|Diminue l''espacement gauche du paragraphe s�lectionn�',
  // TrvActionWordWrap
  '&Retour automatique � la ligne ', 'Retour automatique � la ligne |Inverse le retour automatique � la ligne pour les paragraphes choisis ',
  // TrvActionAlignLeft
  'Alignement � &gauche', 'Alignement � gauche|Aligne le texte s�lectionn� � gauche',
  // TrvActionAlignRight
  'Alignement � &droite', 'Alignement � droite|Aligne le texte s�lectionn� � droite',
  // TrvActionAlignCenter
  'Alignement &centr�', 'Alignement centr�|Centre le texte s�lectionn�',
  // TrvActionAlignJustify
  '&Justifi�', 'Alignement justifi�|Aligne le texte s�lectionn� des deux c�t�s (gauche et droite)',
  // TrvActionParaColor
  'C&ouleur d''arri�re-plan du paragraphe...', 'Couleur d''arri�re-plan du paragraphe|D�finit la couleur d''arri�re-plan du paragraphe',
  // TrvActionLineSpacing100
  'Interlignage &simple ', 'Interlignage simple |Fixe l''interlignage � simple',
  // TrvActionLineSpacing150
  '1.5 &Interlignage ', 'Interlignage 1.5 |Fixe l''interlignage � 1.5',
  // TrvActionLineSpacing200
  'Interlignage &double', 'Interlignage double|Fixe l''interlignage � double',
  // TrvActionParaBorder
  '&Bordures et fond du paragraphe...', 'Bordures et fond du paragraphe|D�finit les bordures et le fond du texte s�lectionn�',
  // TrvActionInsertTable
  '&Ins�rer un tableau...', '&Tableau', 'Ins�rer un tableau|Ins�re un nouveau tableau',
  // TrvActionTableInsertRowsAbove
  'Ins�rer une ligne &au-dessus', 'Ins�rer une ligne au-dessus|Ins�re une nouvelle ligne au-dessus des cellules s�lectionn�es',
  // TrvActionTableInsertRowsBelow
  'Ins�rer une ligne au-&dessous', 'Ins�rer une ligne au-dessous|Ins�re une nouvelle ligne au-dessous des cellules s�lectionn�es',
  // TrvActionTableInsertColLeft
  'Ins�rer une colonne � &gauche ', 'Ins�rer une colonne � gauche|Ins�re une nouvelle colonne � gauche des cellules s�lectionn�es',
  // TrvActionTableInsertColRight
  'Ins�rer une colonne � &droite', 'Ins�rer une colonne � droite|Ins�re une nouvelle colonne � droite des cellules s�lectionn�es',
  // TrvActionTableDeleteRows
  'Supprimer les &lignes', 'Supprimer les lignes|Supprime les lignes avec les cellules s�lectionn�es',
  // TrvActionTableDeleteCols
  'Supprimer les &colonnes', 'Supprimer les colonnes|Supprime les colonnes avec les cellules s�lectionn�es',
  // TrvActionTableDeleteTable
  '&Supprimer le tableau', 'Supprimer le tableau',
  // TrvActionTableMergeCells
  '&Fusionner les cellules', 'Fusionner les cellules|Fusionne les cellules s�lectionn�es',
  // TrvActionTableSplitCells
  'Fra&ctionner les cellules...', 'Fractionner les cellules|Fractionne les cellules s�lectionn�es',
  // TrvActionTableSelectTable
  'S�&lectionner le tableau', 'S�lectionner le tableau',
  // TrvActionTableSelectRows
  'S�lectionner les l&ignes', 'S�lectionner les lignes',
  // TrvActionTableSelectCols
  'S�lectionner les c&olonnes', 'S�lectionner les colonnes',
  // TrvActionTableSelectCell
  'S�lectionner les cell&ules', 'S�lectionner les cellules',
  // TrvActionTableCellVAlignTop
  'Aligner les cellules en &haut', 'Aligner les cellules en haut|Aligne le contenu de la cellule en haut',
  // TrvActionTableCellVAlignMiddle
  'Aligner les cellules au &milieu', 'Aligner les cellules au milieu|Aligne le contenu de la cellule au milieu',
  // TrvActionTableCellVAlignBottom
  'Aligner les cellules en &bas', 'Aligner les cellules en bas|Aligne le contenu de la cellule en bas',
  // TrvActionTableCellVAlignDefault
  'Alignement vertical des cellules par &d�faut', 'Alignement vertical des cellules par d�faut|Applique l''alignement vertical du contenu des cellules par d�faut',
  // TrvActionTableCellRotationNone
  '&Pas de rotation de cellule', 'Pas de rotation de cellule|Fait pivoter le contenu de la cellule � 0�',
  // TrvActionTableCellRotation90
  'Rotation de la cellule � &90�', 'Rotation de la cellule � 90�|Fait pivoter le contenu de la cellule � 90�',
  // TrvActionTableCellRotation180
  'Rotation de la cellule � &180�', 'Rotation de la cellule � 180�|Fait pivoter le contenu de la cellule � 180�',
  // TrvActionTableCellRotation270
  'Rotation de la cellule � &270�', 'Rotation de la cellule � 270�|Fait pivoter le contenu de la cellule � 270�',
  // TrvActionTableProperties
  '&Propri�t�s du tableau...', 'Propri�t�s du tableau|Change les propri�t�s du tableau s�lectionn�',
  // TrvActionTableGrid
  'Montrer la &grille ', 'Montrer la grille |Montre ou cache les lignes de la grille ',
  // TRVActionTableSplit
  'Fractionner le tab&leau', 'Fractionner le tableau|Fractionne le tableau en deux tableaux d�butant par la ligne s�lectionn�e',
  // TRVActionTableToText
  'Convertir en Te&xte...', 'Convertir en texte|Convertit le tableau en texte',
  // TRVActionTableSort
  '&Trier...', 'Trier|Trie les lignes du tableau',
  // TrvActionTableCellLeftBorder
  '&Bordure gauche', 'Bordure gauche|Montre ou cache la bordure  � gauche de la cellule',
  // TrvActionTableCellRightBorder
  'B&ordure droite', 'Bordure droite|Montre ou cache la bordure � droite de la cellule',
  // TrvActionTableCellTopBorder
  'Bo&rdure de haut', 'Bordure de haut|Montre ou cache la bordure en haut de la cellule',
  // TrvActionTableCellBottomBorder
  'Bor&dure du bas', 'Bordure du bas|Montre ou cache la bordure en bas de la cellule',
  // TrvActionTableCellAllBorders
  '&Toutes les bordures', 'Toutes les bordures|Montre toutes les bordures de la cellule',
  // TrvActionTableCellNoBorders
  '&Aucune bordure', 'Aucune bordure|Cache toutes les bordures de la cellule',
  // TrvActionFonts & TrvActionFontEx
  '&Police...', 'Police|Change la police du texte s�lectionn�',
  // TrvActionFontBold
  '&Gras', 'Gras|Change le style du texte s�lectionn� en gras',
  // TrvActionFontItalic
  '&Italique', 'Italique|Change le style du texte s�lectionn� en italique',
  // TrvActionFontUnderline
  '&Soulign�', 'Soulign�|Souligne le texte s�lectionn�',
  // TrvActionFontStrikeout
  '&Barr�', 'Barr�|Barre le texte s�lectionn�',
  // TrvActionFontGrow
  '&Augmenter le corps', 'Augmenter le corps|Augmente la taille du texte s�lectionn� de 10%',
  // TrvActionFontShrink
  '&Diminuer le corps', 'Diminuer le corps|Diminue la taille du texte s�lectionn� de 10%',
  // TrvActionFontGrowOnePoint
  'Ag&randir le corps d''un point', 'Agrandir le corps d''un point|Agrandit la taille de la police du texte s�lectionn� d''un point',
  // TrvActionFontShrinkOnePoint
  'Di&minuer la taille du corps d''un point', 'Diminuer la taille du corps d''un point|Diminue la taille de la police du texte s�lectionn� d''un point',
  // TrvActionFontAllCaps
  '&Tout en majuscules', 'Tout en majuscules|Change tous les caract�res du texte s�lectionn� en majuscules',
  // TrvActionFontOverline
  '&Ligne au dessus', 'Ligne au dessus|Ajoute une ligne au-dessus du texte s�lectionn�',
  // TrvActionFontColor
  'Couleur du &texte...', 'Couleur du texte|Change la couleur du texte s�lectionn�',
  // TrvActionFontBackColor
  'Couleur d''arri�re-&plan du texte...', 'Couleur d''arri�re-plan du texte|Change la couleur d''arri�re-plan du texte s�lectionn�',
  // TrvActionAddictSpell3
  '&V�rification orthographique', 'V�rification orthographique',
  // TrvActionAddictThesaurus3
  '&Synonymes', 'Synonymes|Fournit les synonymes du mot s�lectionn�',
  // TrvActionParaLTR
  'De gauche � droite', 'De gauche � droite|�crit de gauche � droite pour le(s) paragraphe(s) s�lectionn�(s)',
  // TrvActionParaRTL
  'De droite � gauche', 'De droite � gauche|�crit de droite � gauche pour le(s) paragraphe(s) s�lectionn�(s)',
  // TrvActionLTR
  'Texte de gauche � droite', 'De gauche � droite|�crit de droite � gauche pour le texte s�lectionn�',
  // TrvActionRTL
  'Texte de droite � gauche', 'De droite � gauche|�crit de droite � gauche pour le texte s�lectionn�',
  // TrvActionCharCase
  'Casse de caract�res', 'Casse de caract�res|Change la casse du texte s�lectionn�',
  // TrvActionShowSpecialCharacters
  'Caract�res &non imprimables', 'Caract�res non imprimables|Cache ou affiche les caract�res non imprimables, comme les marques de paragraphes, les tabulations et les espaces',
  // TrvActionSubscript
  'Indice', 'Indice|Place le texte s�lectionn� en indice',
  // TrvActionSuperscript
  'Exposant', 'Exposant|Place le texte s�lectionn� en exposant',
  // TrvActionInsertFootnote
  'Note de &bas de page', 'Note de bas de page|Ins�re une note de bas de page',
  // TrvActionInsertEndnote
  'Note de &fin', 'Note de fin|Ins�re une note de fin', 
  // TrvActionEditNote
  '�&diter la note', '�diter la note|�dite la note de bas de page ou la note de fin',
  '&Masquer', 'Masquer|Masque ou affiche la partie s�lectionn�e', 
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Styles...', 'Styles|Ouvre la fen�tre de gestion des styles',
  // TrvActionAddStyleTemplate
  '&Ajouter un style...', 'Ajouter un style|Cr�e un nouveau style de texte ou de paragraphe',
  // TrvActionClearFormat,
  'Effa&cer la mise en forme', 'Effacer la mise en forme|Efface toute la mise en forme des textes et des paragraphes s�lectionn�s',
  // TrvActionClearTextFormat,
  'Effacer la mise en forme du &texte', 'Effacer la mise en forme|Efface toute la mise en forme du texte s�lectionn�',
  // TrvActionStyleInspector
  '&Inspecteur de styles', 'Inspecteur de styles|Affiche ou masque l''inspecteur de styles',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Valider', 'Annuler', 'Quitter', 'Ins�rer', '&Ouvrir...', '&Enregistrer...', 'E&ffacer', 'Aid&e', 'Enlever',
  // Others  -------------------------------------------------------------------
  // percents
  '%',
  // left, top, right, bottom sides
  'C�t� gauche', 'C�t� haut', 'C�t� droit', 'C�t� bas',
  // save changes? confirm title
  'Enregistrer les modifications de %s?', 'Confirmer',
  // warning: losing formatting
  '%s peut contenir des caract�ristiques non compatibles avec le format choisi.'#13+
  'Souhaitez-vous r�ellement enregistrer ce document avec le format choisi?',
  // RVF format name
  'Format Richview',
  // Error messages ------------------------------------------------------------
  'Erreur',
  'Erreur durant le chargement du fichier.'#13#13'Les raisons possibles:'#13'- le format de ce fichier n''est pas support� par cette application;'#13+
  '- Le fichier est corrompu ;'#13'- le fichier est ouvert par une autre application.',
  'Erreur de chargement d''image du fichier.'#13#13'Raisons possibles:'#13'- le fichier contient des images de format non support� par l''application;'#13+
  '- le fichier ne contient pas d''image;'#13+
  '- le fichier est corrompu;'#13'- le fichier est ouvert par une autre application.',
  'Erreur d''enregistrement du fichier.'#13#13'Raisons possibles:'#13'- espace disque non suffisant;'#13+
  '- le disque est prot�g� en �criture;'#13'- le disque n''est pas ins�r�;'#13+
  '- le fichier est ouvert par une autre application;'#13'- le disque est corrompu.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'fichier RichView  (*.rvf)|*.rvf',  'fichier RTF (*.rtf)|*.rtf' , 'fichier XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'fichier Texte (*.txt)|*.txt', 'fichier Texte - Unicode (*.txt)|*.txt', 'fichier Texte - autod�tection (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'fichier HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - simplifi� (*.htm;*.html)|*.htm;*.html',
  // DocX
  'fichier Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Recherche termin�e.', 'Cha�ne''%s'' non trouv�e.',
  // 1 string replaced; Several strings replaced
  '1 cha�ne remplac�e.', '%d cha�nes remplac�es.',
  // continue search from the beginning/end?
  'La fin du document est atteinte. Continuer en reprenant au d�but ?',
  'Le d�but du document est atteint. Continuer en reprenant � la fin ?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Automatique',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Noir', 'Marron', 'Vert Olive', 'Vert fonc�', 'Vert canard fonc�', 'Bleu fonc�', 'Indigo', 'Gris-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Rouge fonc�', 'Orange', 'Jaune fonc�', 'Vert', 'Vert canard', 'Bleu', 'Bleu-gris', 'Gris-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rouge', 'Orange clair ', 'Chaux ', 'Vert de mer ', 'Bleu-vert', 'Bleu clair ', 'Violet', 'Gris-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rose', 'Dor�', 'Jaune', 'Vert clair', 'Turquoise', 'Bleu ciel', 'Prune', 'Gris-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Ocre', 'Jaune clair', 'Vert clair', 'Turquoise clair', 'Bleu clair', 'Lavande', 'Blanc',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&Transparent', '&Automatique', 'Autres &couleurs...', '&D�faut',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Arri�re-plan', 'C&ouleur:', 'Position', 'Arri�re-plan', 'Texte exemple.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Aucun', '&Fen�tre pleine', 'Titres &fix�s', '&Titr�', 'C&entr�',
  // Padding button
  '&Marges ...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Couleur de remplissage', '&Appliquer �:', 'Autres &couleurs...', '&Marges...',
  'Veuillez s�lectionner une couleur', // [apply to:] text, paragraph, table, cellule
  'Texte', 'Paragraphe' , 'Tableau', 'Cellule',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Police', 'Police', 'Disposition',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fonte:', '&Taille', 'Style', '&Gras', '&Italique',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Couleur:', 'Arri�re-p&lan:', '(par d�faut)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effets', 'So&ulign�', '&Surlign�', '&Barr�', '&Majuscules ',
  // Sample, Sample text
  'Exemple', 'Texte exemple',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espacement de caract�res', '&Espacement:', 'Au&gment� ', '&Condens�',
  // Offset group-box, Offset label, Down, Up,
  'Excentrage ', '&Excentrage :', '&Au-dessous', 'A&u-dessus',
  // Scaling group-box, Scaling
  'Graduation', 'Graduation:',
  // Sub/super script group box, Normal, Sub, Super
  'Indices et exposants', '&Normal', '&Indice', 'E&xposant',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Marges int�rieures', '&Haut:', '&Gauche:', '&Bas:', '&Droite:', '&Valeurs �gales',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Ins�rer un lien hypertexte', 'Lien hypertexte', 'T&exte:', '&Cible', '<<s�lection>>',
  // cannot open URL
  'Navigation non possible sur "%s"',
  // hyperlink properties button, hyperlink style
  '&Personnalisation...', '&Style:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Propri�t�s du lien', 'Couleur normale', 'Couleurs actives',
  // Text [color], Background [color]
  '&Texte', '&Arri�re-plan',
  // Underline [color]
  'So&ulign�:',
  // Text [color], Background [color] (different hotkeys)
  'T&exte:', '&Arri�re-plan',
  // Underline [color], Attributes group-box,
  'So&ulign�:', 'Attributs',
  // Underline type: always, never, active (below the mouse)
  'So&ulign�:', 'toujours', 'jamais', 'actif',
  // Same as normal check-box
  'Comme &normal',
  // underline active links check-box
  'So&uligner les liens actifs',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Ins�rer un symbole', '&Police :', '&Jeu de caract�res:', 'Unicode',
  // Unicode block
  '&Bloc:',
  // Character Code, Character Unicode Code, No Character
  ' Code caract�re: %d', 'Code caract�re: Unicode %d', '(pas de caract�re)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Ins�rer un tableau', 'Taille du tableau', 'Nombre de &colonnes:', 'Nombre de &lignes:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Disposition du tableau', '&Taille automatique', 'Adaptation de la taille du tableau � la fen�tre ', 'Taille manuelle du tableau',
  // Remember check-box
  'Sauvegarder les dimensions pour les nouveaux tableaux',
  // VAlign Form ---------------------------------------------------------------
  'Emplacement', 
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propri�t�s', 'Image', 'Position et taille', 'Ligne', 'Tableau', 'Lignes', 'Cellules',
  // Image Appearance, Image Misc, Number tabs
  'Apparence', 'Divers', 'Num�ro',
  // Box position, Box size, Box appearance tabs
  'Position', 'Taille', 'Apparence',
  // Preview label, Transparency group-box, checkbox, label
  'Aper�u:', 'Transparence', '&Transparent', '&Couleur transparente:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'C&hanger...', '&Sauvegarder...', 'Automatique',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alignement vertical', '&Alignement:',
  'bas de la ligne du texte', 'milieu de la ligne du texte',
  'haut - vers la ligne du haut', 'bas - vers la ligne du bas', 'milieu - vers la ligne du milieu',
  // align to left side, align to right side
  'c�t� gauche', 'c�t� droit', 
  // Shift By label, Stretch group box, Width, Height, Default size
  '&D�cal� de:', '�tirement', '&Largeur:', '&Hauteur:', 'Taille par d�faut: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '�chelle &proportionnelle', '&RAZ',
  // Inside the border, border, outside the border groupboxes
  'Dans la bordure', 'Bordure', 'Hors de la bordure',
  // Fill color, Padding (i.e. margins inside border) labels
  'Couleur de &remplissage:', '&Marge int�rieure:',
  // Border width, Border color labels
  '&Largeur de bordure:', '&Couleur de bordure:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espacement &horizontal:', 'Espacement &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Divers', '&Infobulle:',
  // web group-box, alt text
  'Web', '&Texte alternatif:',
  // Horz line group-box, color, width, style
  'Ligne horizontale', '&Couleur:', '&Largeur:', '&Style:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tableau', '&Largeur:', '&Remplir avec une couleur:', '&Espacement des cellules...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Marge &horizontale de cellule:', 'Marge &verticale de cellule:', 'Bordure et fond',
  // Visible table border sides button, auto width (in combobox), auto width label
  'C�t�s visibles de &bordure...', 'Automatique', 'automatique',
  // Keep row content together checkbox
  '&Lier le contenu',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotation', '&Aucune', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Bordure:', 'C�t�s visibles de &bordure...',
  // Border, CellBorders buttons
  '&Bordures de tableau...', 'B&ordures des cellules...',
  // Table border, Cell borders dialog titles
  'Bordures de tableau', 'Bordures des cellules par d�faut',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Impression', '&Garder le tableau en une seule page', '&Nombres de lignes de titre:',
  'Titre de ligne r�p�t� dans chaque page du tableau',
  // top, center, bottom, default
  '&Haut', '&Centr�', '&Bas', '&D�faut',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Configuration', '&Largeur pr�f�r�e:', '&Taille minimum:', '&Remplir couleur:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '&Bordure', 'C�t�s &visibles:',  '&Couleur de l''ombre:', 'C&ouleur de la lumi�re', 'Co&uleur:',
  // Background image
  '&Image...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Position horizontale', 'Position verticale', 'Position dans le texte',
  // Position types: align, absolute, relative
  'Aligner', 'Position absolue', 'Position relative',
  // [Align] left side, center, right side
  'gauche de la bo�te vers c�t� gauche de',
  'centre de la bo�te vers centre de',
  'droite de la bo�te vers c�t� droit de',
  // [Align] top side, center, bottom side
  'haut de la bo�te vers haut de',
  'centre de la bo�te vers centre de',
  'bas de la bo�te vers bas de',
  // [Align] relative to
  'relatif �',
  // [Position] to the right of the left side of
  '� droite du c�t� gauche de',
  // [Position] below the top side of
  'sous le sommet de',
  // Anchors: page, main text area (x2)
  '&Page', '&Espace du texte principal', 'P&age', 'Espace du t&exte principal',
  // Anchors: left margin, right margin
  'Marge &gauche', 'Marge &droite',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Marge du &haut', 'Marge du &bas', 'Marge &int�rieure', 'Marge e&xt�rieure',
  // Anchors: character, line, paragraph
  '&Caract�re', 'L&igne', 'Para&graphe',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Marg&es en miroir', '&Disposition dans cellule',
  // Above text, below text
  '&Au-dessus du texte', '&Sous le texte',
  // Width, Height groupboxes, Width, Height labels
  'Largeur', 'Hauteur', '&Largeur:', '&Hauteur:',
  // Auto height label, Auto height combobox item
  'Automatique', 'automatique',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alignement vertical', '&Sommet', '&Centre', '&Bas',
  // Border and background button and title
  'B&ordure et fond...', 'Bo�te pour la bordure et le fond',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Collage sp�cial', '&Coller en tant que:', 'Format RTF', 'Format HTML',
  'Texte', 'Texte Unicode', 'Image Bitmap', 'Image Metafile',
  'Fichier Image', 'URL',
  // Options group-box, styles
  'Options', '&Styles:',
  // style options: apply target styles, use source styles, ignore styles
  'Appliquer les styles au document cible', 'Garder les styles et l''apparence',
  'Conserver l''apparence, mais ignorer les styles',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Puces et num�rotation', 'Puces', 'Num�rotation', 'Liste de puces', 'Liste de num�rotation',
  // Customize, Reset, None
  '&Personnaliser...', '&Actualiser', 'Aucune',
  // Numbering: continue, reset to, create new
  'Poursuivre la num�rotation', 'Reprendre la num�rotation �', 'Cr�er une nouvelle liste en commen�ant par',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Lettres - majuscules', 'Chiffres romains - majuscules', 'Num�rotation', 'Lettres - minuscules', 'Chiffres romains - minuscules', 
  // Bullet type
  'Cercle', 'Disque', 'Carr�', 
  // Level, Start from, Continue numbering, Numbering group-box
  '&Niveau:', '&� partir de:', '&Continuer', 'Num�rotation', 
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Liste personnalis�e', 'Niveau', '&Compteur:', 'Propri�t�s de la liste', '&Type de liste:',
  // List types: bullet, image
  'Puces', 'Image', 'Ins�rer un nombre',
  // Number format, Number, Start level from, Font button
  '&Format du num�ro:', 'Num�ro', '&Commencer le niveau de num�rotation par:', '&Police...',
  // Image button, bullet character, Bullet button,
  '&Image...', 'C&aract�re pour puce', '&Puce...',
  // Position of list text, bullet, number, image, text
  'Position de la liste de texte', 'Position des puces', 'Position des num�ros', 'Position d''image', 'Position du texte',
  // at, left indent, first line indent, from left indent
  '&� :', 'Espace � gauche:', '&Espace premi�re ligne:', 'de l''espace gauche',
  // [only] one level preview, preview
  '&Aper�u niveau 1', 'Aper�u',
  // Preview text
  'Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Alignement � gauche', 'Alignement � droite', 'Centr�',
  // level #, this level (for combo-box)
  'niveau %d', 'ce niveau',
  // Marker character dialog title
  '�diter les caract�res de puces',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Bordures et arri�re-plan du paragraphe', 'Bordures', 'Arri�re-plan',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Configuration', '&Couleur:', '&Largeur:', 'Larg&eur int�rieure:', 'Excentrages ...',
  // Sample, Border type group-boxes;
  'Exemple', 'Type de bordure',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Aucun', '&Simple', '&Double', '&Triple', 'Int�rieur �pais', 'Ext�rieur �pais',
  // Fill color group-box; More colors, padding buttons
  'Remplir avec une couleur', '&Autres couleurs...', '&Marges...',
  // preview text
  'Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte.',
  // title and group-box in Offsets dialog
  'Excentrages', 'Excentrages des bordures',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragraphe', 'Alignement', '&Gauche', '&Droite', '&Centr�', '&Justification',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espacement', '&Avant:', 'A&pr�s:', 'E&spacement de lignes:', '&Valeur:',
  // Indents group-box; indents: left, right, first line
  'Indentation', 'G&auche:', 'Droi&te:', '&Premi�re ligne:',
  // indented, hanging
  '&Indent�e', '&Accroch�e', 'Exemple',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simple', '1.5 ligne', 'Double', 'Au moins', 'Exactement', 'Multiple',
  // preview text
  'Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indentation et espacement', 'Tabulations', 'Encha�nement du texte',
  // tab stop position label; buttons: set, delete, delete all
  '&Positionner une tabulation stop:', '&D�finir', '&Effacer', '&Tout effacer',
  // tab align group; left, right, center aligns,
  'Alignement', '&Gauche', '&Droite', '&Centr�',
  // leader radio-group; no leader
  'Principal', '(Aucun)',
  // tab stops to be deleted, delete none, delete all labels
  'Effacer la tabulation de fin:', '(Aucune)', 'Toutes',
  // Pagination group-box, keep with next, keep lines together
  'Pagination', '&Assembler avec la suivante', '&Grouper les lignes',
  // Outline level, Body text, Level #
  '&Niveau hi�rarchique:', 'Corps du texte', 'Niveau %d', // dg
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Aper�u avant l''impression', 'Largeur de la page', 'Page enti�re', 'Pages:',
  // Invalid Scale, [page #] of #
  'Veuillez entrer un nombre entre 10 et 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Premi�re page', 'Page pr�c�dente', 'Page suivante', 'Derni�re page',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espacement des cellules', 'Espacement', 'E&ntre cellules', 'Des bordures du tableau aux cellules',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color

  'Bordures', 'Configuration', '&Couleur:', '&Couleur de lumi�re:', '&Couleur d''ombre:',
  // Width; Border type group-box;
  '&Largeur:', 'Type de bordure',
  // Border types: none, sunken, raised, flat
  '&Aucune', '&Creuse', '&Ombr�e', '&Plate',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Fractionnement', 'Fractionnement',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Sp�cifier le nombre de lignes et de colonnes', 'Cellules &originales',
  // number of columns, rows, merge before
  'Nombre de &colonnes:', 'Nombre de &lignes:', '&Fusion avant de se d�doubler',
  // to original cols, rows check-boxes
  'fractionnement des colonnes originales', 'fractionnement des lignes originales',
  // Add Rows form -------------------------------------------------------------
  'Ajouter des lignes', 'Nombre de &lignes:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Format', 'Page', 'En-t�te et pied de page',
  // margins group-box, left, right, top, bottom
  'Marges (millim�tres)', 'Marges (pouces)', '&Gauche:', '&Haut:', '&Droite:', '&Bas:',
  // mirror margins check-box
  '&Marges ',
  // orientation group-box, portrait, landscape
  'Orientation', '&Portrait', 'P&aysage',
  // paper group-box, paper size, default paper source
  'Papier', '&Taille:', '&Source:',
  // header group-box, print on the first page, font button
  'En-t�te', '&Texte:', '&En-t�te de premi�re page', '&Police...',
  // header alignments: left, center, right
  '&Gauche', '&Centr�', '&Droit',
  // the same for footer (different hotkeys)
  'Bas de page', 'Te&xte', 'Bas de page sur premi�re &page', '&Police...',
  '&Gauche', '&Centr�', '&Droit',
  // page numbers group-box, start from
  'Nombre de pages', '&� partir de',
  // hint about codes
  'Caract�res sp�ciaux:'#13'&&p - Page ; &&P - Total ; &&d - Date ; &&t - Heure.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Page de code du fichier texte', '&Choisissez l''encodage du fichier:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japonais', 'Chinois simplifi�', 'Cor�en',
  // chinese (trad), central european, cyrillic, west european
  'Chinois traditionnel', 'Europe centrale et orientale', 'Cyrillique', 'Europe de l''Ouest',
  // greek, turkish, hebrew, arabic
  'Grec', 'Turc', 'H�breu', 'Arabe',
  // baltic, vietnamese, utf-8, utf-16
  'Balte', 'Vietnamien', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Style visuel', '&S�lectionner le style:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertir en texte', 'Choisissez le &d�limiteur:',
  // line break, tab, ';', ','
  'saut de ligne', 'tabulation', 'point-virgule', 'virgule',
  // Table sort form -----------------------------------------------------------
  // error message
  'Le tableau contient des lignes fusionn�es qui ne peuvent pas �tre tri�es',
  // title, main options groupbox
  'Trier le tableau', 'Trier',
  // sort by column, case sensitive
  '&Trier par colonne:', 'Respecter la &casse',
  // heading row, range or rows
  'Exclure la ligne de &titre', 'Lignes � trier: de %d � %d',
  // order, ascending, descending
  'Ordre', 'Croiss&ant', '&D�croissant',
  // data type, text, number
  'Type', '&Texte', '&Nombre',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Insertion d''un num�ro', 'Propri�t�s', 'Nom du &compteur:', 'Type de &num�rotation:',
  // numbering groupbox, continue, start from
  'Num�rotation', 'C&ontinuer', '&D�marrer de:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'L�gende', '&�tiquette:', '&Exclure l''�tiquette de l�gende ',
  // position radiogroup
  'Position', '&Au-dessus de l''objet s�lectionn�', '&Sous l''objet s�lectionn�',
  // caption text
  '&Texte de l�gende:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Num�rotation', 'Figure', 'Tableau',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'C�t�s visibles de bordure', 'Bordure',
  // Left, Top, Right, Bottom checkboxes
  'C�t� &gauche', '&Sommet', 'C�t� &droit', '&Bas',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Redimensionner la colonne', 'Redimensionner la ligne',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Indenter la premi�re ligne', 'Indentation � gauche', 'Indentation � gauche (sauf premi�re)', 'Indentation � droite',
  // Hints on lists: up one level (left), down one level (right)
  'Hausser d''un niveau (gauche)', 'Abaisser d''un niveau (droite)',
  // Hints for margins: bottom, left, right and top
  'Bas', 'Gauche', 'Droite', 'Haut',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Aligner le tableau � gauche', 'Aligner le tableau � droite', 'Centrer le tableau', 'Alignement pr�cis',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Retrait normal', 'Aucun espacement', 'Titre %d', 'Paragraphe de liste',
  // Hyperlink, Title, Subtitle
  'Lien hypertexte', 'Titre', 'Sous-titre',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Emphase', 'Emphase p�le', 'Emphase intense', 'Gras',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citation', 'Citation intense', 'R�f�rence p�le', 'R�f�rence intense',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Bloc de texte', 'Variable HTML', 'Code HTML', 'Acronyme HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'D�finition HTML', 'Clavier HTML', 'Exemple HTML', 'Machine � �crire HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML pr�format�', 'Citation HTML', 'En-t�te', 'Pied de page', 'Num�ro de page',
  // Caption
  'L�gende',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Appel de note de fin', 'Appel de note de pied de page', 'Note de fin de page', 'Note de pied de page',
  // Sidenote Reference, Sidenote Text
  'R�f�rence d''annotation', 'Texte d''annotation',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'couleur', 'couleur d''arri�re-plan', 'transparent', 'par d�faut', 'couleur du soulignement',
  // default background color, default text color, [color] same as text
  'couleur d''arri�re-plan par d�faut', 'couleur de texte par d�faut', 'comme le texte',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'simple', '�pais', 'double', 'pointill�', 'pointill� �pais', 'tirets',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tirets �pais', 'tirets longs', 'tirets �pais longs',
  // underline types: dash dotted, thick dash dotted,
  'tirets en pointill�', 'tirets en pointill� �pais',
  // underline types: dash dot dotted, thick dash dot dotted
  'alternance tirets et points', 'alternance tirets et points �pais',
  // sub/superscript: not, subsript, superscript
  'non indice/exposant', 'indice', 'exposant',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mode:', 'h�rit�', 'gauche � droite', 'droite � gauche',
  // bold, not bold
  'gras', 'pas en gras',
  // italic, not italic
  'italique', 'pas d''italique',
  // underlined, not underlined, default underline
  'soulign�', 'non soulign�', 'soulignement par d�faut',
  // struck out, not struck out
  'biff�', 'non biff�',
  // overlined, not overlined
  'surlign�', 'non surlign�',
  // all capitals: yes, all capitals: no
  'tout en capitales',  'rien en capitales',
  // vertical shift: none, by x% up, by x% down
  'sans d�calage vertical', 'd�calage par %d%% vers le haut', 'd�calage par %d%% vers le bas',
  // characters width [: x%]
  'largeur des caract�res',
  // character spacing: none, expanded by x, condensed by x
  'espacement normal des caract�res', 'espacement �largi par %s', 'espacement condens� par %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'police par d�faut', 'paragraphe par d�faut', 'h�rit�s',
  // [hyperlink] highlight, default hyperlink attributes
  'mettre en surbrillance', 'par d�faut',
  // paragraph aligmnment: left, right, center, justify
  'align� � gauche', 'align� � droite', 'centr�', 'justifi�',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'hauteur de la ligne: %d%%', 'espace entre les lignes: %s', 'hauteur de la ligne: au minimum %s',
  'hauteur de la ligne: exactement %s',
  // no wrap, wrap
  'habillage de la ligne d�sactiv�', 'habillage de la ligne',
  // keep lines together: yes, no
  'conserver les lignes ensemble', 'ne pas conserver les lignes ensemble',
  // keep with next: yes, no
  'conserver avec le paragraphe suivant', 'ne pas conserver avec le paragraphe suivant',
  // read only: yes, no
  'lecture seule', 'modifiable',
  // body text, heading level x
  'niveau hi�rarchique: corps de texte', 'niveau hi�rarchique: %d',
  // indents: first line, left, right
  'retrait de la premi�re ligne', 'retrait � gauche', 'retrait � droite',
  // space before, after
  'espace avant', 'espace apr�s',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulations', 'aucun', 'alignement', 'gauche', 'droite', 'centrer',
  // tab leader (filling character)
  'Points de suite',
  // no, yes
  'non', 'oui',
  // [padding/spacing/side:] left, top, right, bottom
  'gauche', 'haut', 'droite', 'bas',
  // border: none, single, double, triple, thick inside, thick outside
  'aucune', 'simple', 'double', 'triple', '�paisse � l''int�rieur', '�paisse � l''ext�rieur',
  // background, border, padding, spacing [between text and border]
  'arri�re-plan', 'bordure', 'marge int�rieure', 'espacement',
  // border: style, width, internal width, visible sides
  'style', '�paisseur', '�paisseur interne', 'c�t�s visibles',
  // style inspector -----------------------------------------------------------
  // title
  'Inspecteur de styles',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Style', '(Aucun)', 'Paragraphe', 'Police', 'Attributs',
  // border and background, hyperlink, standard [style]
  'Bordure et arri�re-plan', 'Lien hypertexte', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Styles', 'Style',
  // name, applicable to,
  '&Nom:', 'Applicable &�:',
  // based on, based on (no &),
  'Sur la &base de:', 'Sur la base de:',
  //  next style, next style (no &)
  'Style pour le para&graphe suivant:', 'Style pour le paragraphe suivant:',
  // quick access check-box, description and preview
  '&Acc�s rapide', 'Description et a&per�u:',
  // [applicable to:] paragraph and text, paragraph, text
  'paragraphe et texte', 'paragraphe', 'texte',
  // text and paragraph styles, default font
  'Styles de texte et paragraphes', 'Police par d�faut',
  // links: edit, reset, buttons: add, delete
  'Modifier', 'R�initialiser', '&Ajouter', '&Supprimer',
  // add standard style, add custom style, default style name
  'Ajouter un style &standard...', 'Ajouter un style &personnalis�', 'Style %d',
  // choose style
  'Choisir le &style:',
  // import, export,
  '&Importer...', '&Exporter...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Styles RichView (*.rvst)|*.rvst', 'Erreur pendant le chargement du fichier', 'Ce fichier ne contient pas de styles',
  // Title, group-box, import styles
  'Importer les styles d''un fichier', 'Importer', 'I&mporter des styles:',
  // existing styles radio-group: Title, override, auto-rename
  'Styles existants', '&remplacer', '&ajouter en renommant',
  // Select, Unselect, Invert,
  '&S�lectionner', '&D�s�lectionner', '&Inverser',
  // select/unselect: all styles, new styles, existing styles
  '&Tous les Styles', '&Nouveaux Styles', 'Styles &existants',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Effacer la mise en forme)', '(Tous les styles)',
  // dialog title, prompt
  'Styles', '&Choisissez le style � appliquer:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonymes', '&Ignorer tous', '&Ajouter au dictionnaire',
  // Progress messages ---------------------------------------------------------
  'T�l�chargement %s', 'Pr�paration pour l''impression...', 'Impression de la page %d',
  'Conversion de RTF...',  'Conversion en RTF...',
  'Lecture %s...', '�criture %s...', 'texte',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Pas de note', 'Pied de page', 'Note de fin', 'Annotation', 'Bo�te de texte'  
  );

initialization
  RVA_RegisterLanguage(
    'French', // english language name, do not translate
    'Fran�ais', // native language name
    ANSI_CHARSET, @Messages);

end.

