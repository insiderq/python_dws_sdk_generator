
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for entering one numeric value           }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit IntegerRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RVSpinEdit, BaseRVFrm, RVALocalize;

type
  TfrmRVInteger = class(TfrmRVBase)
    GroupBox1: TGroupBox;
    lbl: TLabel;
    se: TRVSpinEdit;
    btnOk: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
  private
    function GetValue: Integer;
    procedure SetValue(const Value: Integer);
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _lbl : TControl;
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    property Value: Integer read GetValue write SetValue;
  end;

implementation

{$R *.dfm}

{ TTfrmInteger }

function TfrmRVInteger.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := GroupBox1.Left*2+GroupBox1.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-GroupBox1.Top-GroupBox1.Height);
  Result := True;
end;

function TfrmRVInteger.GetValue: Integer;
begin
  Result := se.AsInteger;
end;

procedure TfrmRVInteger.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVInteger.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _lbl then
    _lbl := NewControl;
end;
{$ENDIF}

procedure TfrmRVInteger.SetValue(const Value: Integer);
begin
  se.Value := Value;
end;

procedure TfrmRVInteger.FormCreate(Sender: TObject);
begin
  _lbl := lbl;
  inherited;
end;

end.
