
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Czech translation                               }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Andrle Pavel 2003-04-09                }
{ Updated by: Pavel Polivka   2007-11-12                }
{             Ji�� P�ibil     2011-02-19                }
{             Alconost        2012-11-02                }
{             Alconost        2014-05-01                }
{*******************************************************}

unit RVAL_Czech;
{$I RV_Defs.inc}    
interface
    
implementation
uses Windows, RVALocalize;
    
const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'palce', 'cm', 'mm', 'pica', 'pixely', 'body',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  'Soubor', '�pravy', 'Form�t', 'P�smo', 'Odstavec', 'Vlo�it', 'Tabulka', 'Okno', 'N�pov�da',
  // exit
  'Konec',
  // top-level menus: View, Tools,
  '&Zobrazit', 'N�&stroje',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Velikost', 'Styl', 'Vybra&t', 'Zarov&n�n� bu�ky', 'Ohrani�&en� bu�ky',
  // menus: Table cell rotation
  'Oto�en� &Bu�ky',   
  // menus: Text flow, Footnotes/endnotes
  'Obt�k�n� textu', 'Pozn�mky pod �arou a vysv�tlivky',
  // ribbon tabs: tab1, tab2, view, table
  'Dom�', 'Pokro�il�', 'Zobrazen�', 'Tabulka',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Schr�nka', 'P�smo', 'Odstavec', 'Seznamy', '�pravy',
  // ribbon groups: insert, background, page setup,
  'Vlo�en�', 'Pozad�', 'Str�nka',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Odkazy', 'Pozn�mky pod �arou', 'Zobrazen� dokument�', 'Zobrazit/Skr�t', 'Lupa', 'Nastaven�',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Vlo�it', 'Odstranit', '�pravy', 'Ohrani�en�',
  // ribbon groups: styles 
  'Styly',
  // ribbon screen tip footer,
  'Stiskn�te F1 pro n�pov�du',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Naposledy otev�en�', 'Ulo�it jako...', 'N�hled a tisk dokumentu',
  // ribbon label: units combo
  'Jednotky:',       
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'Nov�', 'Nov�|Vytvo�� nov� dokument',
  // TrvActionOpen
  'Otev��t...', 'Otev��t|Otev�e existuj�c� dokument',
  // TrvActionSave
  'Ulo�it', 'Ulo�it|Ulo�� dokument',
  // TrvActionSaveAs
  'Ulo�it jako...', 'Ulo�it jako...|Ulo�� dokument pod nov�m n�zvem',
  // TrvActionExport
  'Export...', 'Export|Export dokumentu do souboru jin�ho form�tu',
  // TrvActionPrintPreview
  'N�hled', 'N�hled|Zobraz� dokument ve tvaru, v jak�m bude vytisknut',
  // TrvActionPrint
  'Tisk', 'Tisk|Vytiskne dokument na zvolen� tisk�rn�',
  // TrvActionQuickPrint
  'Tisk', 'Rychl� tisk', 'Rychl� tisk|Okam�it� vytiskne dokument na standardn� tisk�rn�',
   // TrvActionPageSetup
  'Vzhled str�nky...', 'Vzhled str�nky|Nastav� okraje dokumentu, velikost pap�ru, orientaci, zdroj, z�hlav� a z�pat�',
  // TrvActionCut
  'Vyjmout', 'Vyjmout|Odebere vybran� text nebo objekt a vlo�� jej do schr�nky',
  // TrvActionCopy
  'Kop�rovat', 'Kop�rovat|Zkop�ruje vybran� text nebo objekt do schr�nky',
  // TrvActionPaste
  'Vlo�it', 'Vlo�it|Vlo�� text nebo objekt ze schr�nky na vybran� m�sto',
  // TrvActionPasteAsText
  'Vlo�it jako text', 'Vlo�it jako text|Vlo�� obsah schr�nky ve form�tu prost�ho textu',  
  // TrvActionPasteSpecial
  'Vlo�it speci�ln�...', 'Vlo�it speci�ln�|Vlo�� obsah schr�nky ve speci�ln�m form�tu',
  // TrvActionSelectAll
  'Vybrat v�e', 'Vybrat v�e|Vybere v�echny polo�ky dokumentu',
  // TrvActionUndo
  'Zp�t', 'Zp�t|Vr�t� zp�t posledn� akci',
  // TrvActionRedo
  'Znovu', 'Znovu|Provede znovu posledn� vr�cenou akci',
  // TrvActionFind
  'Naj�t...', 'Naj�t|Vyhled� textov� �et�zec v dokumentu',
  // TrvActionFindNext
  'Naj�t dal��', 'Naj�t dal��|Vyhled� n�sleduj�c� v�skyt hledan�ho textu v dokumentu',
  // TrvActionReplace
  'Nahradit...', 'Nahradit|Nahrad� hledan� text v dokumentu jin�m',
  // TrvActionInsertFile
  'Soubor...', 'Vlo�it soubor|Vlo�� obsah souboru do dokumentu',
  // TrvActionInsertPicture
  'Obr�zek...', 'Vlo�it obr�zek|Vlo�� obr�zek do dokumentu',
  // TRVActionInsertHLine
  'Odd�lova�', 'Vlo�it odd�lova�|Vlo�� vodorovnou odd�lovac� ��ru',
  // TRVActionInsertHyperlink
  'Hypertextov� odkaz...', 'Vlo�it hypertextov� odkaz|Vlo�� hypertextov� odkaz na WWW str�nku nebo soubor',
  // TRVActionRemoveHyperlinks
  'Odstranit hypertextov� odkazy', 'Odstranit hypertextov� odkazy|Odstran� v�echny hypertextov� odkazy z ozna�en�ho textu',  
  // TrvActionInsertSymbol
  'Symbol...', 'Vlo�it symbol|Vlo�� speci�ln� znak v�b�rem z tabulky znak�',
  // TrvActionInsertNumber
  '&��slo...', 'Vlo�it ��slo|Vlo�� ��slo',
  // TrvActionInsertCaption
  'Vlo�it &Popisek...', 'Vlo�it popisek|Vlo�� popisek k vzbran�mu objektu',
  // TrvActionInsertTextBox
  '&Textov� Pole', 'Vlo�it Textov� Pole|Vlo�� textov� pole',
  // TrvActionInsertSidenote
  '&Pozn�mka na okraj', 'Vlo�it Pozn�mku na Okraj|Vlo�� pozn�mku na okraj',
  // TrvActionInsertPageNumber
  '&��slo str�nky', 'Vlo�it ��slo str�nky|Vlo�� ��slo str�nky',
  // TrvActionParaList
  'Odr�ky a ��slov�n�...', 'Odr�ky a ��slov�n�|Definice stylu odr�ek a ��slov�n� vybran�ho odstavce',
  // TrvActionParaBullets
  'Odr�ky', 'Odr�ky|Vlo�� nebo odebere odr�ku pro vybran� odstavec',
  // TrvActionParaNumbering
  '��slov�n�', '��slov�n�|Vlo�� nebo odebere ��slov�n� pro vybran� odstavec',
  // TrvActionColor
  'Barva pozad� dokumentu...', 'Barva pozad� dokumentu|Zm�n� barvy pozad� dokumentu',
  // TrvActionFillColor
  'Barva pozad�...', 'Barva pozad�|Zm�n� barvu pozad� textu, odstavce, bu�ky, tabulky nebo dokumentu',
  // TrvActionInsertPageBreak
  'Vlo�it odd�lova� str�nky', 'Vlo�it odd�lova� str�nky|Vlo�� odd�lova� str�nky',
  // TrvActionRemovePageBreak
  'Odstranit odd�lova� str�nky', 'Odstranit odd�lova� str�nky|Odstran� odd�lova� str�nky',
  // TrvActionClearLeft
  'Neobt�kat text zleva', 'Neobt�kat text zleva|Um�st� odstavec pod jak�koliv obr�zek zarovnan� vlevo',
  // TrvActionClearRight
  'Neobt�kat text zprava', 'Neobt�kat text zprava|Um�st� odstavec pod jak�koliv obr�zek zarovnan� pravo',
  // TrvActionClearBoth
  'Neobt�kat text zleva ani zprava', 'Neobt�kat text zleva ani zprava|Um�st� odstavec pod jak�koliv obr�zek zarovnan� vlevo nebo vpravo',
  // TrvActionClearNone
  '&Norm�ln� obt�k�n� textu', 'Norm�ln� obt�k�n� textu|Umo�n� obt�k�n� textu kolem obr�zk� zarovnan�ch vlevo i vpravo',
  // TrvActionVAlign
  '&Pozice objektu...', 'Pozice objektu|Zm�n� pozici vybran�ho objektu',    
  // TrvActionItemProperties
  'Vlastnosti objektu...', 'Vlastnosti objektu|Nastav� vlastnosti vybran�ho objektu',
  // TrvActionBackground
  'Pozad� dokumentu...', 'Pozad� dokumentu|Nastav� obr�zek, barvu pozad� dokumentu',
  // TrvActionParagraph
  'Odstavec...', 'Odstavec|Nastaven� vlastnost� vybran�ho odstavce',
  // TrvActionIndentInc
  'Zv�t�it odsazen�', 'Zv�t�it odsazen�|Zv�t�� vzd�lenost vybran�ho odstavce od lev�ho okraje str�nky',
  // TrvActionIndentDec
  'Zmen�it odsazen�', 'Zmen�it odsazen�|Zmen�� vzd�lenost vybran�ho odstavce od lev�ho okraje str�nky',
  // TrvActionWordWrap
  'Zalamov�n� ��dku', 'Zalamov�n� ��dku|Nastav� nebo zru�� zalamov�n� ��dku vybran�ho odstavce',
  // TrvActionAlignLeft
  'Zarovnat vlevo', 'Zarovnat vlevo|Zarovn� text vybran�ho odstavce k lev�mu okraji str�nky',
  // TrvActionAlignRight
  'Zarovnat vpravo', 'Zarovnat vpravo|Zarovn� text vybran�ho odstavce k prav�mu okraji str�nky',
  // TrvActionAlignCenter
  'Zarovnat na st�ed', 'Zarovnat na st�ed|Zarovn� text vybran�ho odstavce na st�ed str�nky',
  // TrvActionAlignJustify
  'Zarovnat do bloku', 'Zarovnat do bloku|Zarovn� text vybran�ho odstavce do bloku',
  // TrvActionParaColor
  'Barva pozad� odstavce...', 'Barva pozad� odstavce|Zm�n� barvu pozad� vybran�ho odstavce',
  // TrvActionLineSpacing100
  'Jednoduch� ��dkov�n�', 'Jednoduch� ��dkov�n�|Nastav� jednoduch� ��dkov�n� odstavce',
  // TrvActionLineSpacing150
  '��dkov�n� 1,5 ��dku', '��dkov�n� 1,5 ��dku|Nastav� ��dkov�n� odstavce na v��ku 1,5 n�sobek v��ky ��dku',
  // TrvActionLineSpacing200
  'Dvojit� ��dkov�n�', 'Dvojit� ��dkov�n�|Nastav� ��dkov�n� odstavce na dvojitou v��ku ��dku',
  // TrvActionParaBorder
  'Ohrani�en� a pozad� odstavce...', 'Ohrani�en� a pozad� odstavce|Nastav� ohrani�en� a pozad� vybran�ho odstavce',
  // TrvActionInsertTable
  'Vlo�it tabulku...', 'Tabulka', 'Vlo�it tabulku|Vlo�� novou tabulku',
  // TrvActionTableInsertRowsAbove
  'Vlo�it ��dek p�ed', 'Vlo�it ��dek p�ed|Vlo�� nov� ��dek do tabulky p�ed vybranou bu�ku',
  // TrvActionTableInsertRowsBelow
  'Vlo�it ��dek za', 'Vlo�it ��dek za|Vlo�� nov� ��dek do tabulky za vybranou bu�ku',
  // TrvActionTableInsertColLeft
  'Vlo�it sloupec vlevo', 'Vlo�it sloupec vlevo|Vlo�� sloupec vlevo od vybran� bu�ky',
  // TrvActionTableInsertColRight
  'Vlo�it sloupec vpravo', 'Vlo�it sloupec vpravo|Vlo�� sloupec vpravo od vybran� bu�ky',
  // TrvActionTableDeleteRows
  'Odstranit ��dky', 'Odstranit ��dky|Odstran� vybran� ��dky z tabulky',
  // TrvActionTableDeleteCols
  'Odstranit sloupce', 'Odstranit sloupce|Odstran� vybran� sloupce z tabulky',
  // TrvActionTableDeleteTable
  'Odstranit tabulku', 'Odstranit tabulku|Odstran� tabulku',
  // TrvActionTableMergeCells
  'Slou�it bu�ky', 'Slou�it bu�ky|Slou�� vybran� bu�ky v jednu',
  // TrvActionTableSplitCells
  'Rozd�lit bu�ku...', 'Rozd�lit bu�ku|Rozd�l� bu�ku na po�adovan� po�et bu�ek',
  // TrvActionTableSelectTable
  'Vybrat tabulku', 'Vybrat tabulku|Vybere celou tabulku',
  // TrvActionTableSelectRows
  'Vybrat ��dek', 'Vybrat ��dek|Provede v�b�r cel�ho ��dku',
  // TrvActionTableSelectCols
  'Vybrat sloupec', 'Vybrat sloupec|Provede v�b�r cel�ho sloupce',
  // TrvActionTableSelectCell
  'Vybrat bu�ku', 'Vybrat bu�ku|Provede v�b�r cel� bu�ky',
  // TrvActionTableCellVAlignTop

  'Zarovnat bu�ku nahoru', 'Zarovnat bu�ku nahoru|Zarovn� zobrazen� text v bu�ce k jej�mu horn�mu okraji',
  // TrvActionTableCellVAlignMiddle
  'Zarovnat bu�ku na st�ed', 'Zarovnat bu�ku na st�ed|Zarovn� zobrazen� text v bu�ce na st�ed',
  // TrvActionTableCellVAlignBottom
  'Zarovnat bu�ku dol�', 'Zarovnat bu�ku dol�|Zarovn� zobrazen� text v bu�ce k jej�mu doln�mu okraji',
  // TrvActionTableCellVAlignDefault
  'Standardn� zarovn�n� bu�ky', 'Standardn� zarovn�n� bu�ky|Nastav� standardn� svisl� zarovn�n� bu�ky',
  // TrvActionTableCellRotationNone
  '&Bez oto�en� bu�ky', 'Bez oto�en� bu�ky|Oto�� obsah bu�ky o 0�',
  // TrvActionTableCellRotation90
  'Oto�it bu�ku o &90�', 'Oto�it bu�ku o 90�|Oto�� obsah bu�ky o 90�',
  // TrvActionTableCellRotation180
  'Oto�it bu�ku o &180�', 'Oto�it bu�ku o 180�|Oto�� obsah bu�ky oy 180�',
  // TrvActionTableCellRotation270
  'Oto�it bu�ku o &270�', 'Oto�it bu�ku o 270�|Oto�� obsah bu�ky oy 270�',
  // TrvActionTableProperties
  'Vlastnosti tabulky...', 'Vlastnosti tabulky|Nastav� vlastnosti tabulky',
  // TrvActionTableGrid
  'Zobrazit m��ku', 'Zobrazit m��ku|Zobraz� nebo skryje m��ku tabulky',
  // TRVActionTableSplit
  'Ro&zd�lit tabulku', 'Rozd�lit tabulku|Rozd�l� tabulku na dv� samostatn� od vybran�ho ��dku',
  // TRVActionTableToText
  'P�ev�st na te&xt...', 'P�ev�st na text|P�evede tabulku na text',
  // TRVActionTableSort
  '&Se�adit...', 'Se�adit|Se�ad� ��dky tabulky',
  // TrvActionTableCellLeftBorder
  'Ohrani�en� vlevo', 'Ohrani�en� vlevo|Zobraz� nebo skryje lev� ohrani�en� bu�ky',
  // TrvActionTableCellRightBorder
  'Ohrani�en� vpravo', 'Ohrani�en� vpravo|Zobraz� nebo skryje prav� ohrani�en� bu�ky',
  // TrvActionTableCellTopBorder
  'Ohrani�en� naho�e', 'Ohrani�en� naho�e|Zobraz� nebo skryje horn� ohrani�en� bu�ky',
  // TrvActionTableCellBottomBorder
  'Ohrani�en� dole', 'Ohrani�en� dole|Zobraz� nebo skryje spodn� ohrani�en� bu�ky',
  // TrvActionTableCellAllBorders
  'Ohrani�en� dokola', 'Ohrani�en� dokola|Zobraz� nebo skryje ohrani�en� cel� bu�ky',
  // TrvActionTableCellNoBorders
  'Bez ohrani�en�', 'Bez ohrani�en�|Skryje cel� ohrani�en� vybran� bu�ky',
  // TrvActionFonts & TrvActionFontEx
  'P�smo...', 'P�smo|Zm�na nastaven� p�sma vybran�ho textu',
  // TrvActionFontBold
  'Tu�n�', 'Tu�n�|Nastav� nebo zru�� tu�n� zobrazen� vybran�ho textu',
  // TrvActionFontItalic
  'Kurz�va', 'Kurz�va|Nastav� nebo zru�� zobrazen� vybran�ho textu kurz�vou',
  // TrvActionFontUnderline
  'Podtr�en�', 'Podtr�en�|Nastav� nebo zru�� podtr�en� vybran�ho textu',
  // TrvActionFontStrikeout
  'P�e�krtnut�', 'P�e�krtnut�|Nastav� nebo zru�� p�e�krtnut� vybran�ho textu',
  // TrvActionFontGrow
  'Zv�t�it p�smo', 'Zv�t�it p�smo|Zv�t�� v��ku vybran�ho textu o 10%',
  // TrvActionFontShrink
  'Zmen�it p�smo', 'Zmen�it p�smo|Zmen�� v��ku vybran�ho textu o 10%',
  // TrvActionFontGrowOnePoint
  'Zv�t�it p�smo o 1 bod', 'Zv�t�it p�smo o 1 bod|Zv�t�� v��ku vybran�ho textu o 1 bod',
  // TrvActionFontShrinkOnePoint
  'Zmen�it p�smo o 1 bod', 'Zmen�it p�smo o 1 bod|Zmen�� v��ku vybran�ho textu o 1 bod',
  // TrvActionFontAllCaps
  'V�echna velk�', 'V�echna velk�|Zm�n� styl p�sma vybran�ho textu na v�echny velk� znaky',
  // TrvActionFontOverline
  'Nadtr�en�', 'Nadtr�en�|Nastav� nebo zru�� vodorovnou linku nad vybran�m textem',
  // TrvActionFontColor
  'Barva p�sma...', 'Barva p�sma|Zm�n� barvu vybran�ho textu',
  // TrvActionFontBackColor
  'Barva pozad� p�sma...', 'Barva pozad� p�sma|Zm�n� barvu pozad� vybran�ho textu',
  // TrvActionAddictSpell3
  'Kontrola pravopisu', 'Kontrola pravopisu|Provede kontrolu pravopisu vybran�ho textu',
  // TrvActionAddictThesaurus3
  'Synonyma', 'Synonyma|Nab�dne synonyma pro vybran� slovo',
  // TrvActionParaLTR
  'Zleva doprava', 'Zleva doprava|Nastav� sm�r textu na sm�r zleva doprava',
  // TrvActionParaRTL
  'Zprava doleva', 'Zprava doleva|Nastav� sm�r textu na sm�r zprava doleva',
  // TrvActionLTR
  'Text zleva doprava', 'Text zleva doprava|Pro vybran� text nastav� sm�r textu na sm�r zleva doprava',
  // TrvActionRTL
  'Text zprava doleva', 'Text zprava doleva|Pro vybran� text nastav� sm�r textu na sm�r zprava doleva',
  // TrvActionCharCase
  'Mal� / velk� p�smena', 'Mal� / velk� p�smena|Zm�n� velikost p�smen vybran�ho textu',
  // TrvActionShowSpecialCharacters
  '&Netisknuteln� znaky', 'Netisknuteln� znaky|Zobrazit nebo skr�t netisknuteln� znaky, nap�. znaky odstavce, znaky tabul�toru a mezery',
  // TrvActionSubscript
  'Do&ln� index', 'Doln� index|Konvertovat vybran� text na doln� index',
  // TrvActionSuperscript
  'Horn� index', 'Horn� index|Konvertovat vybran� text na horn� index',
  // TrvActionInsertFootnote
  'Pozn�mka pod �arou', 'Pozn�mka pod �arou|Vlo�� pozn�mku pod �arou',
  // TrvActionInsertEndnote
  'Vysv�tlivka', 'Vysv�tlivka|Vlo�� vysv�tlivku na konec dokumentu',
  // TrvActionEditNote
  'Editovat pozn�mku/vysv�tlivku', 'Editovat pozn�mku/vysv�tlivku|Umo�n� editovat pozn�mku pod �arou nebo vysv�tlivku',
  '&Skr�t', 'Skr�t|Skryje nebo zobraz� ozna�en� text',            
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Styly...', 'Styly|Otev�e dialog spr�vy styl�',
  // TrvActionAddStyleTemplate
  '&P�idat styl...', 'P�idat styl|Vytvo�� nov� styl textu nebo odstavce',
  // TrvActionClearFormat,
  '&Vymazat form�t', 'Vymazat form�t|Vyma�e ve�ker� form�tov�n� textu a odstavce z vybran�ho fragmentu',
  // TrvActionClearTextFormat,
  'Vymazat form�t &textu', 'Vymazat form�t textu|Vyma�e ve�ker� form�tov�n� vybran�ho textu',
  // TrvActionStyleInspector
  'Kontrola &styl�', 'Kontrola styl�|Zobraz� nebo skryje kontrolu styl�',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Storno', 'Zav��t', 'Vlo�it', 'Otev��t...', 'Ulo�it...', 'Vymazat', 'N�pov�da', 'Odstranit',
  // Others  -------------------------------------------------------------------
  // percents
  'procenta',
  // left, top, right, bottom sides
  'Lev� strana', 'Horn� strana', 'Prav� strana', 'Spodn� strana',
  // save changes? confirm title
  'Ulo�it proveden� zm�ny do %s?', 'Potvrzen�',
  // warning: losing formatting
  '%s obsa�en� objekt nebo form�tov�n� nen� podporov�no ve vybran�m form�tu souboru.'#13+
  'Opravdu chcete pro ulo�en� pou��t tento form�t?',
  // RVF format name
  'RichView Form�t',
  // Error messages ------------------------------------------------------------
  'Chyba',
  'Chyba p�i na��t�n� souboru.'#13#13'Mo�n� p���iny:'#13'- form�t otev�ran�ho souboru nen� podporov�n v t�to aplikaci;'#13+
  '- soubor je po�kozen;'#13'- soubor je otev�en a uzamknut jinou aplikac�.',
  'Chyba p�i na��t�n� obr�zku.'#13#13'Mo�n� p���iny:'#13'- form�t obr�zku nen� podporov�n touto aplikac�;'#13+
  '- vybran� soubor neobsahuje obr�zek;'#13+
  '- soubor je po�kozen;'#13'- soubor je otev�en a uzamknut jinou aplikac�.',
  'Chyba p�i ukl�d�n� souboru.'#13#13'Mo�n� p���iny:'#13'- nen� m�sto na disku;'#13+
  '- disk je chr�n�n proti z�pisu;'#13'- v�m�nn� m�dium nen� vlo�eno;'#13+
  '- soubor je otev�en a uzamknut jinou aplikac�;'#13'- v�m�nn� m�dium je po�kozeno.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView soubory (*.rvf)|*.rvf',  'RTF soubory (*.rtf)|*.rtf' , 'XML soubory (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Textov� soubory (*.txt)|*.txt', 'Textov� soubory - Unicode (*.txt)|*.txt', 'Textov� soubory - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML soubory (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - zjednodu�en� (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Dokumenty Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Hled�n� dokon�eno', 'Hledan� �et�zec ''%s'' nenalezen.',
  // 1 string replaced; Several strings replaced
  '1 �et�zec nahrazen.', '%d �et�zc� nahrazeno.',
  // continue from beginning? end?
  // continue search from the beginning/end?
  'Byl dosa�en konec dokumentu, chcete pokra�ovat od za��tku?',
  'Byl dosa�en za��tek dokumentu, chcete pokra�ovat od konce?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'nen�', 'automatick�',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '�ern�', 'Hn�d�', 'Olivov� zelen�', 'Tmav� zelen�', 'Tmav� �edozelen�', 'Tmav� modr�', 'Indigov� mod�', '�ed�-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tmav� �erven�', 'Oran�ov�', 'Tmav� �lut�', 'Zelen�', '�edozelen�', 'Modr�', 'Modro�ed�', '�ed�-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  '�erven�', 'Sv�tle oran�ov�', '�lutozelen�', 'Mo�sk� zele�', 'Akvamarinov�', 'Sv�tle modr�', 'Fialov�', '�ed�-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'R��ov�', 'Zlat�', '�lut�', 'Jasn� zelen�', 'Tyrkysov�', 'Nebesk� mod�', '�vestkov�', '�ed�-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Sv�tle r��ov�', '�lutooran�ov�', 'Sv�tle �lut�', 'Sv�tle zelen�', 'Sv�tle tyrkysov�', 'Bled� modr�', 'Lavandulov�', 'B�l�',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'nen�', 'automatick�', 'v�ce barev...', 'standardn�',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Pozad�', 'Barva:', 'Pozice obr�zku', 'N�hled', 'Uk�zka textu.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '��dn�', 'cel� okno', 'ukotven� dla�dice', 'dla�dice', 'na st�ed',
  // Padding button
  'Odsazen� textu...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Barva pozad�', 'Pou��t na:', 'V�ce barev...', 'Odsazen� textu...',
  'Pros�m vyberte barvu',
  // [apply to:] text, paragraph, table, cell
  'text', 'odstavec' , 'tabulku', 'bu�ku',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'P�smo', 'P�smo', 'Prolo�en� znak�',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'P�smo:', 'Velikost:', '�ez p�sma', 'Tu�n�', 'Kurz�va',
  // Script, Color, Back color labels, Default charset
  'Skript:', 'Barva:', 'Po&zad�:', '(Standardn�)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Styl', 'Podtr�en�', 'Nadtr�en�', 'P�e�krtnut�', 'V�echna velk�',
  // Sample, Sample text
  'N�hled', 'Uk�zkov� text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Mezery mezi znaky', 'O kolik:', 'roz���en�', 'z��en�',
  // Offset group-box, Offset label, Down, Up,
  'Um�st�n�', 'O kolik:', 'sn�en�', 'zv��en�',
  // Scaling group-box, Scaling
  'Prolo�en� znak�', 'M���tko:',
  // Sub/super script group box, Normal, Sub, Super
  'Doln� a horn� indexy', '&Norm�n�', 'D&oln� index', 'Hor&n� index',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Odsazen� textu', 'Naho�e:', 'Vlevo:', 'Dole:', 'Vpravo:', 'V�echny stejn�',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Vlo�it hypertextov� odkaz', 'Odkaz', 'Text:', 'C�l', '<<v�b�r>>',
  // cannot open URL
  'Nelze p�ej�t na "%s"',
  // hyperlink properties button, hyperlink style
  '&Vlastnosti...', '&Styl:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Vlastnosti hypertextov�ho odkazu', 'Barva odkazu', 'Aktivn� barva',
  // Text [color], Background [color]
  'Text:', 'Pozad�',
  // Underline [color]
  'P&odtr�en�:', 
  // Text [color], Background [color] (different hotkeys)
  'Text:', 'Pozad�',
  // Underline [color], Attributes group-box,
  'P&odtr�en�:', 'Atributy',
  // Underline type: always, never, active (below the mouse)
  '&Podtr�en�:', 'v�dy', 'nikdy', 'aktivn�',
  // Same as normal check-box
  'Standardn�',
  // underline active links check-box
  '&Podtrhnout aktivn� odkazy',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Vlo�it symbol', 'P�smo:', 'Znakov� sada:', 'Unicode',
  // Unicode block
  '&Blok:',  
  // Character Code, Character Unicode Code, No Character
  'K�d znaku: %d', 'K�d znaku: Unicode %d', '(nen� znak)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Vlo�it tabulku', 'Velikost tabulky', 'Po�et sloupc�:', 'Po�et ��dk�:',
  // Table layout group-box, Autosize, Fit, Manual size,
  '���ka tabulky', 'Automatick�', 'Rozt�hnout dle okna', 'Manu�ln� nastaven�',
  // Remember check-box
  'Pou��t jako vzor pro novou tabulku',
  // VAlign Form ---------------------------------------------------------------
  'Pozice',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Vlastnosti', 'Obr�zek', 'Pozice a Rozm�r', '��ra', 'Tabulka', '��dek', 'Bu�ka',
  // Image Appearance, Image Misc, Number tabs
  'Vzhled', 'R�zn�', '��slo',
  // Box position, Box size, Box appearance tabs
  'Pozice', 'Rozm�r', 'Vzhled',
  // Preview label, Transparency group-box, checkbox, label
  'N�hled:', 'Transparentn�', 'Transparentn�', 'Transparentn� barva:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Zm�nit...', '&Ulo�it...', 'automatick�',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Svisl� zarovn�n�', 'Zarovn�n�:',
  'spodn� hrana objektu na ��a�� textu', 'st�ed objektu na ��a�� textu',
  'horn� hrana objektu na horn� dota�nici textu', 'doln� hrana objektu na doln� dota�nici textu', 'st�ed objektu na st�ed ��dku',
  // align to left side, align to right side
  'vlevo', 'vpravo',      
  // Shift By label, Stretch group box, Width, Height, Default size
  'Posunout o:', 'Velikost', '���ka:', 'V��ka:', 'Standardn� velikost: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Zachovat &proporce', '&Reset',
  // Inside the border, border, outside the border groupboxes
  'Uvnit� okraje', 'Okraj', 'Vn� okraje',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Barva v�pln�:', '&Odsazen� obsahu:',
  // Border width, Border color labels
  '���ka &okraje:', 'Barva &okraje:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Vodorovn� mezery:', '&Svisl� mezery:',
  // Miscellaneous groupbox, Tooltip label
  'R�zn�', '&Tip k n�stroji:',
  // web group-box, alt text
  'Web', 'Alternativn� &text:',
  // Horz line group-box, color, width, style
  'Odd�lovac� ��ra', 'Barva:', '���ka:', 'Styl:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabulka', '���ka:', 'Barva v�pln�:', 'Odsazen� bu�ky...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Vodorovn� odsadit obsah bu�ky:', '&Svisle odsadit obsah bu�ky:', 'Okraj a pozad�',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Viditeln� &strany okraje...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Zachovat obsah pohromad�',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Oto�en�', '&��dn�', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Okraj:', 'Viditeln� &strany okraje...',
  // Border, CellBorders buttons
  'Ohrani�en� tabulky...', 'Ohrani�en� bu�ky...',
  // Table border, Cell borders dialog titles
  'Ohrani�en� tabulky', 'Ohrani�en� bu�ky',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tisk', 'Tisk tabulky na jedn� str�nce', 'Po�et hlavi�kov�ch ��dk�:',
  'Hlavi�kov� ��dky budou opakov�ny na ka�d� str�nce tabulky',
  // top, center, bottom, default
  'nahoru', 'na st�ed', 'dol�', 'standardn�',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Nastaven�', 'Up�ed�ost�ovan� ���ka:', 'Minim�ln� v��ka:', 'Barva v�pln�:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Ohrani�en�', 'Viditeln� strany:',  'Barva st�nu:', 'Barva sv�tla', 'Barva:',
  // Background image
  'Obr�zek...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Vodorovn� pozice', 'Svisl� pozice', 'Pozice v textu',
  // Position types: align, absolute, relative
  'Zarovnat', 'Absolutn� pozice', 'Relativn� pozice',
  // [Align] left side, center, right side
  'lev� strana pole k lev� stran�',
  'st�ed pole ke st�edu',
  'prav� strana pole k prav� stran�',
  // [Align] top side, center, bottom side
  'horn� strana pole k horn� stran�',
  'st�ed pole ke st�edu',
  'doln� strana pole k doln� stran�',
  // [Align] relative to
  'relativn� k',
  // [Position] to the right of the left side of
  'vpravo od lev� strany',
  // [Position] below the top side of
  'pod horn� stranou',
  // Anchors: page, main text area (x2)
  '&Str�nka', '&Hlavn� textov� oblast', 'S&tr�nka', 'Hlavn�  te&xtov� oblast',
  // Anchors: left margin, right margin
  '&Lev� okraj', '&Prav� okraj',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Horn� okraj', '&Doln� okraj', '&Vnit�n� okraj', '&Vn�j�� okraj',
  // Anchors: character, line, paragraph
  '&Znak', '�&�dek', 'Odsta&vec',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Zrcadl&en� okraje', 'Rozlo�&en� v bu�ce tabulky',
  // Above text, below text
  '&Nad textem', '&Pod textem',
  // Width, Height groupboxes, Width, Height labels
  '���ka', 'V��ka', '&���ka:', '&V��ka:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Svisl� zarovn�n�', '&Naho�e', '&St�ed', '&Dole',
  // Border and background button and title
  'O&kraj a pozad�...', 'Okraj a Pozad� Pole',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Vlo�it speci�ln�', 'Vlo�it jako:', 'Rich text form�t', 'HTML form�t',
  'Text', 'Unicode text', 'Bitmapov� obr�zek', 'Obr�zkov� metasoubor',
  'Grafick� soubor',  'URL',
  // Options group-box, styles
  'Mo�nosti', '&Styly:',
  // style options: apply target styles, use source styles, ignore styles
  'pou��t styly c�lov�ho dokumentu', 'zachovat styly a vzhled',
  'zachovat vzhled, ignorovat styly',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Odr�ky a ��slov�n�', 'Odr�ky', '��slov�n�', 'Odr�ky', '��slov�n�',
  // Customize, Reset, None
  'Vlastn�...', 'Obnovit', 'nen�',
  // Numbering: continue, reset to, create new
  'nav�zat na p�edchoz�', 'pokra�ovat v ��slov�n� od', 'za��t znovu ��slovat od',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'velk� p�smena (A, B, �)', 'velk� ��msk� ��slice (I, II, �)', 'arabsk� ��slice (1, 2, �)', 'mal� p�smena (a, b, �)', 'mal� ��msk� ��slice (i, ii, �)',
  // Bullet type
  'pr�zdn� kole�ko', 'pln� kole�ko', '�tvere�ek',
  // Level, Start from, Continue numbering, Numbering group-box
  '�rove�:', 'Za��t od:', 'Pokra�ovat', '��slov�n�',    
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Vlastn� nastaven�', '�rove�', 'Po�et:', 'Vlastnosti', 'Typ:',
  // List types: bullet, image
  'odr�ka', 'obr�zek', 'vlo�it ��slo|',
  // Number format, Number, Start level from, Font button
  'Form�t ��sla:', '��slo', 'Za��t ��slov�n� �rovn� od:', 'P�smo...',
  // Image button, bullet character, Bullet button,
  'Obr�zek...', 'Znak odr�ky', 'Odr�ka...',
  // Position of list text, bullet, number, image, text
  'Um�st�n� textu', 'Um�st�n� odr�ky', 'Um�st�n� ��sla', 'Um�st�n� obr�zku', 'Um�st�n� p�sma',
  // at, left indent, first line indent, from left indent
  'na:', 'Odsazen� zleva:', 'Odsazen� prvn�ho ��dku:', 'od odsazen� zleva',
  // [only] one level preview, preview
  'N�hled jedn� �rovn�', 'N�hled',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'zarovnat vlevo', 'zarovnat vpravo', 'zarovnat na st�ed',
  // level #, this level (for combo-box)
  '�rove� %d', 'Tato �rove�',
  // Marker character dialog title
  'Zm�na znaku odr�ky',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Ohrani�en� a pozad� odstavce', 'Ohrani�en�', 'Pozad�',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Nastaven�', 'Barva:', '���ka:', '���ka r�mu:', 'Odsazen� textu...',
  // Sample, Border type group-boxes;
  'N�hled', 'Typ ohrani�en�',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '��dn�', 'jednoduch�', 'dvojit�', 'trojit�', 'siln� uvnit�', 'siln� vn�',
  // Fill color group-box; More colors, padding buttons
  'V�pl�', 'V�ce barev...', 'Odsazen� textu...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Odsazen� textu', 'Odsazen� textu',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Odstavec', 'Zarovn�n�', 'vlevo', 'vpravo', 'na st�ed', 'do bloku',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Mezery', 'P�ed:', 'Za:', '��dkov�n�:', 'N�sobky:',
  // Indents group-box; indents: left, right, first line
  'Odsazen�', 'Zleva:', 'Zprava:', 'Prvn� ��dek:',
  // indented, hanging
  'odsazen�', 'p�edsazen�', 'N�hled',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'jednoduch�', '1,5 ��dku', 'dvojit�', 'nejm�n�', 'p�esn�', 'n�sobky',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Odsazen� a mezery', 'Zar�ky', 'Tok textu',
  // tab stop position label; buttons: set, delete, delete all
  '&Pozice zar�ky:', '&Nastavit', '&Smazat', 'Smazat &v�e',
  // tab align group; left, right, center aligns,
  'Zarovn�n�', '&Vlevo', 'Vp&ravo', '&Na st�ed',
  // leader radio-group; no leader
  'Vodic� znak', '(��dn�)',
  // tab stops to be deleted, delete none, delete all labels
  'Vymazat tyto zar�ky:', '(��dn�)', 'V�e',
  // Pagination group-box, keep with next, keep lines together
  'Str�nkov�n�', 'Sv�zat s &n�sleduj�c�m', '&Sv�zat ��dky',
  // Outline level, Body text, Level #
  '�rove� osnovy:', 'T�lo textu', '�rove� %d',    
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'N�hled p�ed tiskem', '���ka str�nky', 'Cel� str�nka', 'Str�nka:',
  // Invalid Scale, [page #] of #
  'Pros�m vlo�te ��slo od 10 do 500', 'z %d',
  // Hints on buttons: first, prior, next, last pages
  'Prvn� str�nka', 'P�edchoz� str�nka', 'N�sleduj�c� str�nka', 'Posledn� str�nka',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Odsazen� bu�ky', 'Mezery', '&P�ed bu�kou', 'Od r�mu tabulky po bu�ku',
  // vertical, horizontal (x2)
  '&Vertik�ln�:', '&Horizont�ln�:', 'V&ertik�ln�:', 'H&orizont�ln�:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Ohrani�en�', 'Nastaven�', 'Barva:', 'Barva sv�tla:', 'Barva st�nu:',
  // Width; Border type group-box;
  '���ka:', 'Typ ohrani�en�',
  // Border types: none, sunken, raised, flat
  '��dn�', 'vtla�en�', 'vystoupl�', 'plovouc�',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Rozd�len� bu�ky', 'Rozd�lit na',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Ur�en� po�tu sloupc� a ��dk�', 'Origin�ln� bu�ky',
  // number of columns, rows, merge before
  'Po�et sloupc�:', 'Po�et ��dk�:', 'Slou�it p�ed rozd�len�m',
  // to original cols, rows check-boxes
  'Rozd�lit na origin�ln� sloupce', 'Rozd�lit na origin�ln� ��dky',
  // Add Rows form -------------------------------------------------------------
  'Vlo�it ��dky', 'Po�et ��dk�:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Vzhled str�nky', 'Str�nka', 'Z�hlav� a z�pat�',
  // margins group-box, left, right, top, bottom
  'Okraje (v milimetrech)', 'Okraje (v palc�ch)', 'V&levo:', '&Naho�e:', 'V&pravo:', '&Dole:',
  // mirror margins check-box
  '&Zrcadlov� okraje',
  // orientation group-box, portrait, landscape
  'Orientace', 'Na &v��ku', 'Na &���ku',
  // paper group-box, paper size, default paper source
  'Pap�r', 'V&elikost:', 'Zd&roj:',
  // header group-box, print on the first page, font button
  'Z�hlav�', '&Text:', '&Z�hlav� na prvn� str�nce', '&P�smo...',
  // header alignments: left, center, right
  'v&levo', 'na &st�ed', 'vp&ravo',
  // the same for footer (different hotkeys)
  'Z�pat�', 'T&ext:', 'Z&�pat� na prvn� str�nce', 'P&�smo...',
  '&vlevo', '&na st�ed', 'vp&ravo',
  // page numbers group-box, start from
  '��slov�n� str�nek', 'Z&a��t od:',
  // hint about codes
  'Kombinace speci�ln�ch znak�:'#13'&&p - ��slo str�nky; &&P - po�et str�nek; &&d - aktu�ln� datum; &&t - aktu�ln� �as.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Znakov� str�nka', '&Vyberte k�dov�n� souboru:',
  // thai, japanese, chinese (simpl), korean
  'Thaj�tina', 'Japon�tina', '��n�tina (zjednodu�en�)', 'Korej�tina',
  // chinese (trad), central european, cyrillic, west european
  '��n�tina (tradi�n�)', 'St�edn� a v�chodn� Evropa', 'Azbuka', 'Z�padn� Evropa',
  // greek, turkish, hebrew, arabic
  '�e�tina', 'Ture�tina', 'Hebrej�tina', 'Arab�tina',
  // baltic, vietnamese, utf-8, utf-16
  'Baltsk� jazyky', 'Vietnam�tina', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vizu�ln� styl', '&Vybrat styl:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'P�ev�st na text', 'vybrat &odd�lova�:',
  // line break, tab, ';', ','
  'konec ��dku', 'tabul�tor', 'st�edn�k', '��rka',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabulka obsahuj�c� slou�en� ��dky nem��e b�t se�azena',
  // title, main options groupbox
  'Se�adit tabulku', 'Se�adit',
  // sort by column, case sensitive
  '&Se�adit podle sloupce:', '&Rozli�ovat mal� a velk�',
  // heading row, range or rows
  'Krom� ��dku z�hlav�', '��dky k se�azen�: od %d po %d',
  // order, ascending, descending
  'Po�ad�', '&Vzestupn�', '&Sestupn�',
  // data type, text, number
  'Typ', '&Text', '&��slo',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Vlo�it ��slo', 'Vlastnosti', '&N�zev po��tadla:', '&Typ ��slov�n�:',
  // numbering groupbox, continue, start from
  '��slov�n�', 'P&okra�ovat', '&Za��t od:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Popisek', '&�t�tek:', '&Vylou�it �t�tek z popisku',
  // position radiogroup
  'Pozice', '&Nad vybran�m objektem', '&Pod vybran�m objektem',
  // caption text
  'Text &popisku:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  '��slov�n�', '��slo', 'Tabulka',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Viditeln� strany okraj�', 'Okraj',
  // Left, Top, Right, Bottom checkboxes
  '&Lev� strana', '&Horn� strana', '&Prav� strana', '&Doln� strana',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Zm�nit velikost sloupce tabulky', 'Zm�nit velikost ��dku tabulky',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Odsazen� prvn�ho ��dku', 'Odsazen� zleva', 'P�edsazen� prvn�ho ��dku', 'Odsazen� zprava',
  // Hints on lists: up one level (left), down one level (right)
  'O �rove� v��', 'O �rove� n�',
  // Hints for margins: bottom, left, right and top
  'Spodn� okraj', 'Lev� okraj', 'Prav� okraj', 'Horn� okraj',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Lev� zar�ka', 'Prav� zar�ka', 'Zar�ka na st�ed', 'Desetinn� zar�ka',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Norm�ln�', 'Norm�ln� odsazen�', 'Bez mezer', 'Nadpis %d', 'Odstavec se seznamem',
  // Hyperlink, Title, Subtitle
  'Hypertextov� odkaz', 'Nadpis', 'Podnadpis',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Zd�razn�n�', 'Zd�razn�n� � jemn�', 'Zd�razn�n� � intenzivn�', 'Siln�',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citace', 'Citace � intenzivn�', 'Odkaz � jemn�', 'Odkaz � intenzivn�',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Text v bloku', 'Prom�nn� HTML', 'K�d HTML', 'Akronym HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definice HTML', 'Kl�vesnice HTML', 'Vzorek HTML', 'Psac� stroj HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Form�tovan� v HTML', 'Cit�t HTML', 'Z�hlav�', 'Z�pat�', '��slo str�nky',
  // Caption
  'Popisek',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Odkaz na vysv�tlivky', 'Odkaz na pozn�mku pod �arou', 'Text vysv�tlivky', 'Text pozn�mky pod �arou',
  // Sidenote Reference, Sidenote Text
  'Pozn�mka na okraj', 'Text pozn�mky na okraj',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'barva', 'barva pozad�', 'pr�hledn�', 'v�choz�', 'barva podtr�en�',
  // default background color, default text color, [color] same as text
  'v�choz� barva pozad�', 'v�choz� barva textu', 'stejn� jako text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'jednoduch�', 'siln�', 'dvojit�', 'te�kovan�', 'siln� te�kovan�', 'p�eru�ovan�',
  // underline types: thick dashed, long dashed, thick long dashed,
  'siln� p�eru�ovan�', 'dlouh� ��rkovan�', 'siln� dlouh� ��rkovan�',
  // underline types: dash dotted, thick dash dotted,
  'te�ka a ��ra', 'siln� te�ka a ��ra',
  // underline types: dash dot dotted, thick dash dot dotted
  '��ra te�ka te�ka', 'siln� ��ra te�ka te�ka',
  // sub/superscript: not, subsript, superscript
  '��dn� doln�/horn� index', 'doln� index', 'horn� index',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'obousm�rn� re�im:', 'd�di�n�', 'zleva doprava', 'zprava doleva',
  // bold, not bold
  'tu�n�', 'ne tu�n�',
  // italic, not italic
  'kurz�va', 'ne kurz�va',
  // underlined, not underlined, default underline
  'podtr�en�', 'ne podtr�en�', 'v�choz� podtr�en�',
  // struck out, not struck out
  'p�e�krtnut�', 'ne p�e�krtnut�',
  // overlined, not overlined
  'nadtr�en�', 'ne nadtr�en�',
  // all capitals: yes, all capitals: no
  'v�echna velk�', 'vypnout v�echna velk�',
  // svisl� posun: ��dn�, o x% nahoru, o x% dol�
  'bez svisl�ho posunu', 'posunuto o %d%% nahoru', 'posunuto o %d%% dol�',
  // characters width [: x%]
  '���ka znak�',
  // character spacing: none, expanded by x, condensed by x
  'norm�ln� prolo�en� znak�', 'prolo�en� roz���eno o %s', 'prolo�en� z��eno o %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'v�choz� p�smo', 'v�choz� odstavec', 'd�di�n�',
  // [hyperlink] highlight, default hyperlink attributes
  'zv�raznit', 'v�choz�',
  // paragraph aligmnment: left, right, center, justify
  'zarovnan� doleva', 'zarovnan� doprava', 'zarovnan� na st�ed', 'zarovnan� do bloku',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'v��ka ��dku: %d%%', 'mezera mezi ��dky: %s', 'v��ka ��dku: nejm�n� %s',
  'line height: exactly %s',
  // no wrap, wrap
  'zalomen� ��dk� vypnuto', 'zalomen� ��dk�',
  // keep lines together: yes, no
  'sv�zat ��dky', 'nesvazovat ��dky',
  // keep with next: yes, no
  'sv�zat s n�sleduj�c�m odstavcem', 'nesvazovat s n�sleduj�c�m odstavcem',
  // read only: yes, no
  'pouze pro �ten�', 'editovateln�',
  // body text, heading level x
  '�rove� osnovy: z�kladn� text', '�rove� osnovy: %d',
  // indents: first line, left, right
  'odsazen� prvn�ho ��dku', 'lev� odsazen�', 'prav� odsazen�',
  // space before, after
  'mezera p�ed', 'mezera po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabul�tory', '��dn�', 'zarovnat', 'doleva', 'doprava', 'na st�ed',
  // tab leader (filling character)
  'vodic� znak',
  // no, yes
  'ne', 'ano',
  // [padding/spacing/side:] left, top, right, bottom
  'lev�', 'horn�', 'prav�', 'doln�',
  // border: none, single, double, triple, thick inside, thick outside
  '��dn�', 'jednoduch�', 'dvojit�', 'trojit�', 'siln� uvnit�', 'siln� vn�',
  // background, border, padding, spacing [between text and border]
  'pozad�', 'ohrani�en�', 'odsazen� obsahu', 'mezery',
  // border: style, width, internal width, visible sides
  'styl', '���ka', 'vnit�n� ���ka', 'viditeln� strany',
  // style inspector -----------------------------------------------------------
  // title
  'Kontrola styl�',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Styl', '(��dn�)', 'Odstavec', 'P�smo', 'Atributy',
  // border and background, hyperlink, standard [style]
  'Ohrani�en� a pozad�', 'Hypertextov� odkaz', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Styly', 'Styl',
  // name, applicable to,
  '&N�zev:', 'Plat� &pro:',
  // based on, based on (no &),
  '&Zalo�eno na:', 'zalo�eno na:',
  //  next style, next style (no &)
  'Styl pro &n�sleduj�c� odstavec:', 'Styl pro n�sleduj�c� odstavec:',
  // quick access check-box, description and preview
  '&Rychl� p��stup', 'Popis a &n�hled:',
  // [applicable to:] paragraph and text, paragraph, text
  'odstavec a text', 'odstavec', 'text',
  // text and paragraph styles, default font
  'Styly textu a odstavce', 'V�choz� p�smo',
  // links: edit, reset, buttons: add, delete
  'Upravit', 'Resetovat', '&P�idat', '&Smazat',
  // add standard style, add custom style, default style name
  'P�idat &standardn� styl...', 'P�idat &vlastn� styl', 'Styl %d',
  // choose style
  'Vybrat &styl:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Styly RichView (*.rvst)|*.rvst', 'Chyba p�i otev�r�n� souboru', 'Tento soubor neobsahuje ��dn� styly',
  // Title, group-box, import styles
  'Import styl� ze souboru', 'Import', 'I&mportovat styly:',
  // existing styles radio-group: Title, override, auto-rename
  'Existuj�c� styly', '&p�epsat', '&p�idat p�ejmenovan�',
  // Select, Unselect, Invert,
  '&Vybrat', '&Zu�it v�b�r', '&Invertovat',
  // select/unselect: all styles, new styles, existing styles
  '&V�echny styly', '&Nov� styly', '&Existuj�c� styly',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Vymazat form�tov�n�)', '(V�echny styly)',
  // dialog title, prompt
  'Styly', '&Vybrat styl k pou�it�:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Synonyma', '&Ignorovat v�e', '&P�idat do slovn�ku',
  // Progress messages ---------------------------------------------------------
  'Stahuji %s', 'P�ipravuji k tisku...', 'Tisknu str�nku %d',
  'Konvertuji z RTF...',  'Konvertuji do RTF...',
  'Na��t�m %s...', 'Zapisuji %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  '��dn� pozn�mka', 'Pozn�mka pod �arou', 'Pozn�mka na konec', 'Pozn�mka na okraj', 'Textov� pole'
  );
    
    
initialization
  RVA_RegisterLanguage(
    'Czech', // english language name, do not translate
    '�e�tina', // native language name
    EASTEUROPE_CHARSET, @Messages);
    
end.
