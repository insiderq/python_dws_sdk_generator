﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'InsSymbolRVFrm.pas' rev: 27.00 (Windows)

#ifndef InssymbolrvfrmHPP
#define InssymbolrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVFontCombos.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVGrids.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Inssymbolrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TZoomPanel;
class PASCALIMPLEMENTATION TZoomPanel : public Vcl::Extctrls::TPanel
{
	typedef Vcl::Extctrls::TPanel inherited;
	
private:
	HIDESBASE MESSAGE void __fastcall WMNCHitTest(Winapi::Messages::TWMNCHitTest &Message);
	MESSAGE void __fastcall CMDenySubclassing(Winapi::Messages::TMessage &Msg);
	
protected:
	virtual void __fastcall Paint(void);
	
public:
	int DefWidth;
	System::UnicodeString FontName;
	System::Uitypes::TFontCharset FontCharset;
	bool AlwaysUnicode;
	Rvtypes::TRVAnsiString TextA;
	Rvtypes::TRVUnicodeString TextW;
public:
	/* TCustomPanel.Create */ inline __fastcall virtual TZoomPanel(System::Classes::TComponent* AOwner) : Vcl::Extctrls::TPanel(AOwner) { }
	
public:
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TZoomPanel(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TZoomPanel(HWND ParentWindow) : Vcl::Extctrls::TPanel(ParentWindow) { }
	
};


class DELPHICLASS TfrmRVInsertSymbol;
class PASCALIMPLEMENTATION TfrmRVInsertSymbol : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TLabel* lblCharset;
	Vcl::Stdctrls::TLabel* lblFont;
	Rvfontcombos::TRVFontComboBox* cmbFont;
	Rvfontcombos::TRVFontCharsetComboBox* cmbCharset;
	Rvgrids::TRVGrid* dg;
	Vcl::Stdctrls::TLabel* Label1;
	Vcl::Stdctrls::TComboBox* cmbBlock;
	Vcl::Stdctrls::TLabel* lblBlock;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall dgDrawCell(System::TObject* Sender, int ACol, int ARow, const System::Types::TRect &ARect, bool Selected);
	void __fastcall cmbFontClick(System::TObject* Sender);
	void __fastcall cmbCharsetClick(System::TObject* Sender);
	void __fastcall dgSelectCell(System::TObject* Sender);
	void __fastcall dgTopLeftChanged(System::TObject* Sender);
	void __fastcall FormResize(System::TObject* Sender);
	void __fastcall dgEnter(System::TObject* Sender);
	void __fastcall cmbBlockClick(System::TObject* Sender);
	void __fastcall dgDblClick(System::TObject* Sender);
	
private:
	bool FUseBlocks;
	int FontHeight;
	System::UnicodeString FontName;
	System::Uitypes::TFontCharset FontCharset;
	int Offset;
	int Endpos;
	void __fastcall AdjustControlsCoords(void);
	void __fastcall CalcFontHeight(void);
	
protected:
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	
public:
	TZoomPanel* Panel;
	bool FAlwaysUnicode;
	Vcl::Controls::TControl* _btnOk;
	Vcl::Controls::TControl* _lblCharset;
	Vcl::Controls::TControl* _lblBlock;
	Vcl::Controls::TControl* _lblFont;
	Vcl::Controls::TControl* _Label1;
	void __fastcall Init(System::Word Char, const System::UnicodeString AFontName, System::Uitypes::TFontCharset ACharset, bool AlwaysUnicode);
	void __fastcall GetInfo(System::Word &Char, System::UnicodeString &AFontName, System::Uitypes::TFontCharset &ACharset);
	void __fastcall SetOptions(bool AllowUnicode, bool AllowANSI, bool UseBlocks);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVInsertSymbol(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVInsertSymbol(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVInsertSymbol(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVInsertSymbol(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word CM_DENYSUBCLASSING = System::Word(0xce3);
}	/* namespace Inssymbolrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_INSSYMBOLRVFRM)
using namespace Inssymbolrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// InssymbolrvfrmHPP
