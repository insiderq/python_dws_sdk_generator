�
 TFRMRVSPACING 0  TPF0�TfrmRVSpacingfrmRVSpacingLeftTopAHelpContext�f CaptionCell SpacingClientHeight� ClientWidthnPixelsPerInch`
TextHeight �	TGroupBoxgbLeftTopWidtheHeight� Caption	 Spacing  �TLabellblTopLeft
Top<Width2Caption&Horizontal:  �TLabel	lblBottomLeft
Top`Width&Caption
&Vertical:  �TLabellblLeftLeft� Top<Width2CaptionH&orizontal:  �TLabellblRightLeft� Top`Width&Caption
V&ertical:  �TLabelLabel6Left� TopWidth� Height'	AlignmenttaCenterAutoSizeCaptionFrom table border to cellsLayouttlCenterWordWrap	  �TBevelBevel1Left� TopWidth
HeightlShape
bsLeftLine  �TRVSpinEditseTopLeftoTop7MaxValue       �@MinValue       ���  �TRVSpinEditseBottomLeftoTop[MaxValue       �@MinValue       ���OnChangeseBottomChange  �TRVSpinEditseLeftLeft!Top7MaxValue       �@MinValue       ���OnChangeseLeftChange  �TRVSpinEditseRightLeft!Top[MaxValue       �@MinValue       ���OnChangeseRightChange  �	TCheckBoxcbEqualLeftTop�   	TCheckBoxcbCellSpacingLeft
TopWidth� HeightCaption&Between cellsChecked	State	cbCheckedTabOrderOnClickcbCellSpacingClick   �TButtonbtnOkLeft� Top�   �TButton	btnCancelLeftTop�    