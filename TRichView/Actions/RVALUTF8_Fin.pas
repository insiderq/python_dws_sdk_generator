﻿// This file is a copy of RVAL_Fin.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Finnish translation ver 1.21   (suomi)          }
{       UPDATE with minor corrections                   }
{                                                       }
{       Translated by fred.thomas-50ch75r@yopmail.com   }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Updated: 2014-02-25                                   }
{*******************************************************}

unit RVALUTF8_Fin;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'tuumaa', 'cm', 'mm', 'picaa', 'pikseliä', 'pistettä',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Tiedosto', '&Muokkaa', 'M&uotoilu', '&Fontti', '&Kappale', 'Li&säys', 'T&aulukko', '&Ikkuna', '&Ohje',
  // exit
  '&Lopeta',
  // top-level menus: View, Tools,
  '&Näkymä', '&Työkalut',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Koko', 'Tyyli', '&Valitse taulukko', 'Pystytasaus', 'Solureunat',
  // menus: Table cell rotation
  'Solun &kierto', 
  // menus: Text flow, Footnotes/endnotes
  'Tekstin rivity&s', '&Viitteet',
  // ribbon tabs: tab1, tab2, view, table
  '&Aloitus', '&Lisäasetukset', '&Näkymä', 'T&aulukko',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Leikepöytä', 'Fontti', 'Kappale', 'Luettelo', 'Muokkaus',
  // ribbon groups: insert, background, page setup,
  'Lisää', 'Vesileima', 'Sivun asetukset',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Linkit', 'Viitteet', 'Asiakirjanäkymät', 'Näytä/piilota', 'Zoomaus', 'Lisävalinnat',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Lisää', 'Poista', 'Toiminnot', 'Reunat',
  // ribbon groups: styles 
  'Tyylit',
  // ribbon screen tip footer,
  'Lisäohjeita saat painamalla F1',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Viimeksi käytetyt tiedostot', 'Tallenna nimellä', 'Esikatselu ja tulostus',
  // ribbon label: units combo
  'Yksikkö:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Uusi', 'Uusi|Luo tyhjä asiakirja',
  // TrvActionOpen
  '&Avaa...', 'Avaa|Avaa olemassa oleva asiakirja',
  // TrvActionSave
  '&Tallenna', 'Tallenna|Tallenna nykyinen asiakirja',
  // TrvActionSaveAs
  'Tallenna &nimellä...', 'Tallenna nimellä...|Tallenna nykyinen asiakirja uudella nimellä',
  // TrvActionExport
  '&Vie...', 'Vie|Vie asiakirja eri tiedostomuotoon',
  // TrvActionPrintPreview
  '&Esikatselu', 'Esikatselu|Näytä asiakirja tulostusmuodossaan',
  // TrvActionPrint
  'T&ulostus...', 'Tulostus|Määritä tulostusasetukset ja tulosta asiakirja',
  // TrvActionQuickPrint
  'Tulosta &heti', 'Tulosta heti', 'Tulosta|Tulosta asiakirja',
  // TrvActionPageSetup
  '&Sivun asetukset...', 'Sivun asetukset|Aseta reunukset, paperikoko, suunta ja tunnisteet',
  // TrvActionCut
  'Le&ikkaa', 'Leikkaa|Siirrä valittu alue leikepöydälle',
  // TrvActionCopy
  '&Kopioi', 'Kopioi|Kopioi valittu alue leikepöydälle',
  // TrvActionPaste
  '&Liitä', 'Liitä|Liitä leikepöydän sisällön',
  // TrvActionPasteAsText
  'Liitä &teksti', 'Liitä teksti|Liitä leikepöydän sisältö muotoilemattomana tekstinä',  
  // TrvActionPasteSpecial
  'Liitä &määräten...', 'Liitä määräten|Liitä leikepöydän sisältö määritetyssä muodossa',
  // TrvActionSelectAll
  'Valitse &kaikki', 'Valitse kaikki|Valitse koko asiakirja',
  // TrvActionUndo
  'Ku&moa', 'Kumoa|Kumoa viimeisin toiminto',
  // TrvActionRedo
  'Tee &uudelleen', 'Tee uudelleen|Peru viimeisin kumoaminen',
  // TrvActionFind
  '&Etsi...', 'Etsi|Etsi määritettyä teksti',
  // TrvActionFindNext
  'Etsi &seuraava', 'Etsi seuraava|Jatka viimeisintä hakua',
  // TrvActionReplace
  'Kor&vaa...', 'Korvaa|Etsi ja korvaa määritetty teksti',
  // TrvActionInsertFile
  'Lisää &tiedosto...', 'Lisää tiedosto|Lisää tiedoston sisältö',
  // TrvActionInsertPicture
  'Lisää &kuva...', 'Lisää kuva|Lisää kuvatiedosto',
  // TRVActionInsertHLine
  '&Vaakaviiva', 'Lisää vaakaviiva|Lisää vaakasuora viiva',
  // TRVActionInsertHyperlink
  '&Hyperlinkki...', 'Lisää hyperlinkki|Lisää hypertekstilinkki',
  // TRVActionRemoveHyperlinks
  '&Poista hyperlinkit', 'Poista hyperlinkit|Poista valitusta tekstistä kaikki hyperlinkit',  
  // TrvActionInsertSymbol
  '&Symboli...', 'Lisää symboli|Lisää symboli',
  // TrvActionInsertNumber
  '&Numero...', 'Lisää numero|Lisää numero',
  // TrvActionInsertCaption
  'Lisää &teksti...', 'Lisää teksti|Lisää valittuun kohteeseen teksti',
  // TrvActionInsertTextBox
  '&Tekstiruutu', 'Lisää tekstiruutu|Lisää tekstiruutu',
  // TrvActionInsertSidenote
  '&Reunahuomautus', 'Lisää reunahuomautus|Lisää tekstilaatikossa esitettävä huomautus',
  // TrvActionInsertPageNumber
  '&Sivunumero', 'Lisää sivunumero|Lisää sivunumero',
  // TrvActionParaList
  '&Luettelomerkit ja numerointi...', 'Luettelomerkit ja numerointi|Käytä tai muokkaa valittujen kappaleiden luettelomerkkejä tai numerointia',
  // TrvActionParaBullets
  '&Luettelomerkit', 'Luettelomerkit|Lisää tai poista kappaleesta luettelomerkit',
  // TrvActionParaNumbering
  '&Numerointi', 'Numerointi|Lisää tai poista kappaleesta numerointi',
  // TrvActionColor
  '&Taustaväri...', 'Taustaväri|Muuta asiakirjan taustaväriä',
  // TrvActionFillColor
  '&Täyttö...', 'Täyttö|Muuta tekstin, solujen, kappaleiden, taulukoiden tai asiakirjan taustaväriä',
  // TrvActionInsertPageBreak
  '&Lisää sivunvaihto', 'Lisää sivunvaihto|Lisää sivunvaihto',
  // TrvActionRemovePageBreak
  '&Poista sivunvaihto', 'Poista sivunvaihto|Poista sivunvaihto',
  // TrvActionClearLeft
  'Rivitä &vasemmalle', 'Rivitä vasemmalle|Rivitä teksti vasemmalle välttäen kuvat',
  // TrvActionClearRight
  'Rivitä &oikealle', 'Rivitä oikealle|Rivitä teksti oikealle välttäen kuvat',
  // TrvActionClearBoth
  'Rivitä &molemmat', 'Rivitä &molemmat|Rivitä teksti välttäen kuvat',
  // TrvActionClearNone
  'Tiukka &rivitys', 'Tiukka rivitys|Rivitä teksti kuvien molemmille puolille',
  // TrvActionVAlign
  '&Sijainti...', 'Sijainti|Muuta valitun kohteen sijaintia',  
  // TrvActionItemProperties
  '&Ominaisuudet...', 'Ominaisuudet|Muuta valitun kohteen ominaisuuksia',
  // TrvActionBackground
  'Tausta&väri...', 'Taustaväri|Valitse taustakuva ja -väri',
  // TrvActionParagraph
  '&Kappale...', 'Kappale|Muuta kappaleen asetteluja',
  // TrvActionIndentInc
  '&Lisää sisennystä', 'Lisää sisennystä|Lisää valituiden kappaleiden sisennystä',
  // TrvActionIndentDec
  '&Vähennä sisennystä', 'Vähennä sisennystä|Vähennä valituiden kappaleiden sisennystä',
  // TrvActionWordWrap
  'Automaattinen &rivitys', 'Automaattinen rivitys|Automaattinen rivitys päälle/pois',
  // TrvActionAlignLeft
  'Tasaa &vasemmalle', 'Tasaa vasemmalle|Tasaa valittu teksti vasemmalle',
  // TrvActionAlignRight
  'Tasaa &oikealle', 'Tasaa oikealle|Tasaa valittu teksti oikealle',
  // TrvActionAlignCenter
  '&Keskitä', 'Keskitä|Keskitä valittu teksti',
  // TrvActionAlignJustify
  '&Tasaa', 'Tasaa|Tasaa valittu teksti molempiin reunoihin',
  // TrvActionParaColor
  'Kappaleiden tausta&väri...', 'Kappaleiden taustaväri|Aseta kappaleiden taustaväri',
  // TrvActionLineSpacing100
  'Riviväli &1', 'Riviväli 1|Aseta riviväliksi 1.0',
  // TrvActionLineSpacing150
  'Riviväli 1.&5', 'Riviväli 1.5|Aseta riviväliksi 1.5',
  // TrvActionLineSpacing200
  'Riviväli &2', 'Riviväli 2|Aseta riviväliksi 2.0',
  // TrvActionParaBorder
  '&Reunat ja taustaväri...', 'Reunat ja taustaväri|Aseta valituiden kappaleiden reunaviivat ja taustaväri',
  // TrvActionInsertTable
  '&Lisää taulukko...', '&Taulukko', 'Lisää taulukko|Lisää uusi taulukko',
  // TrvActionTableInsertRowsAbove
  'Lisää rivi &yläpuolelle', 'Lisää rivi yläpuolelle|Lisää rivi valittujen solujen yläpuolelle',
  // TrvActionTableInsertRowsBelow
  'Lisää rivi &alapuolelle', 'Lisää rivi alapuolelle|Lisää rivi valittujen solujen alapuolelle',
  // TrvActionTableInsertColLeft
  'Lisää sarake &vasemmalle', 'Lisää sarake vasemmalle|Lisää sarake valittujen solujen vasemmalle puolelle',
  // TrvActionTableInsertColRight
  'Lisää sarake &oikealle', 'Lisää sarake oikealle|Lisää sarake valittujen solujen oikealle puolelle',
  // TrvActionTableDeleteRows
  'Poista &rivit', 'Poista rivit|Poista rivit, joilla valitut solut sijaitsevat',
  // TrvActionTableDeleteCols
  'Poista &sarakkeet', 'Poista sarakkeet|Poista sarakkeet, joilla valitut solut sijaitsevat',
  // TrvActionTableDeleteTable
  '&Poista taulukko', 'Poista taulukko|Poista valittu taulukko',
  // TrvActionTableMergeCells
  '&Yhdistä solut', 'Yhdistä solut|Yhdistä valitut solut',
  // TrvActionTableSplitCells
  '&Jaa solut...', 'Jaa solut|Jaa valitut solut',
  // TrvActionTableSelectTable
  'Valitse &taulukko', 'Valitse taulukko|Valitse koko taulukko',
  // TrvActionTableSelectRows
  'Valitse r&ivit', 'Valitse rivit|Valitse rivit',
  // TrvActionTableSelectCols
  'Valitse sara&kkeet', 'Valitse sarakkeet|Valitse sarakkeet',
  // TrvActionTableSelectCell
  'Valitse &solu', 'Valitse solu|Valitse solu',
  // TrvActionTableCellVAlignTop
  'Pystytasaus &ylös', 'Pystytasaus ylös|Tasaa solusisältö ylös',
  // TrvActionTableCellVAlignMiddle
  'Pystytasaus &keskelle', 'Pystytasaus keskelle|Keskitä solusisältö pystysuunnassa',
  // TrvActionTableCellVAlignBottom
  'Pystytasaus &alas', 'Pystytasaus alas|Tasaa solusisältö alas',
  // TrvActionTableCellVAlignDefault
  'Solujen &pystytasaus', 'Solujen pystytasaus|Aseta valittujen solujen pystytasaus',
  // TrvActionTableCellRotationNone
   '&Ei kiertoa', 'Ei kiertoa|Kierrä solun sisältöä 0°',
  // TrvActionTableCellRotation90
  'Kierrä solua &90°', 'Kierrä solua 90°|Kierrä solun sisältöä 90°',
  // TrvActionTableCellRotation180
   'Kierrä solua &180°', 'Kierrä solua 180°|Kierrä solun sisältöä 180°',
  // TrvActionTableCellRotation270
   'Kierrä solua &270°', 'Kierrä solua 270°|Kierrä solun sisältöä 270°',  
  // TrvActionTableProperties
  'Taulukon &ominaisuudet...', 'Taulukon ominaisuudet|Muuta valitun taulukon ominaisuuksia',
  // TrvActionTableGrid
  'Näytä &ruudukko', 'Näytä ruudukko|Näytä/piilota apuruudukko',
  // TRVActionTableSplit
  '&Jaa taulukko', 'Jaa taulukko|Jaa taulukko kahtia alkaen valitusta rivistä',
  // TRVActionTableToText
   'Muunna te&kstiksi...', 'Muunna tekstiksi|Muunna taulukko tekstiksi',
  // TRVActionTableSort
   '&Lajittele...', 'Lajittele|Lajittele taulukon rivit',
  // TrvActionTableCellLeftBorder
  '&Vasen reuna', 'Vasen reuna|Näytä/piilota solun vasen reunaviiva',
  // TrvActionTableCellRightBorder
  '&Oikea reuna', 'Oikea reuna|Näytä/piilota solun oikea reunaviiva',
  // TrvActionTableCellTopBorder
  '&Yläreuna', 'Yläreuna|Näytä/piilota solun yläreunaviiva',
  // TrvActionTableCellBottomBorder
  '&Alareuna', 'Alareuna|Näytä/piilota solun alareunaviiva',
  // TrvActionTableCellAllBorders
  '&Kaikki reunat', 'Kaikki reunat|Näytä solun kaikki reunaviivat',
  // TrvActionTableCellNoBorders
  '&Ei reunoja', 'Ei reunoja|Piilota solun kaikki reunaviivat',
  // TrvActionFonts & TrvActionFontEx
  '&Fontti...', 'Fontti|Muuta valitun tekstin fonttia',
  // TrvActionFontBold
  '&Lihavointi', 'Lihavointi|Lihavoi valittu teksti',
  // TrvActionFontItalic
  '&Kursivointi', 'Kursivointi|Kursivoi valittu teksti',
  // TrvActionFontUnderline
  '&Alleviivaus', 'Alleviivaus|Alleviivaa valittu teksti',
  // TrvActionFontStrikeout
  '&Yliviivaus', 'Yliviivaus|Yliviivaa valittu teksti',
  // TrvActionFontGrow
  '&Suurenna fonttia', 'Suurenna fonttia|Suurenna valitun tekstin kokoa 10%',
  // TrvActionFontShrink
  '&Pienennä fonttia', 'Pienennä fonttia|Pienennä valitun tekstin kokoa 10%',
  // TrvActionFontGrowOnePoint
  'S&uurenna pisteellä', 'Suurenna pisteellä|Suurenna valitun tekstin kokoa 1 pisteellä',
  // TrvActionFontShrinkOnePoint
  'P&ienennä pisteellä', 'Pienennä pisteellä|Pienennä valitun tekstin kokoa 1 pisteellä',
  // TrvActionFontAllCaps
  'Is&ot kirjaimet', 'Isot kirjaimet|Muuta valitun tekstin kaikki kirjaimet isoiksi',
  // TrvActionFontOverline
  'Ylä&viiva', 'Yläviiva|Lisää valitun tekstin yläpuolelle viiva',
  // TrvActionFontColor
  'Teksti&väri...', 'Tekstiväri|Muuta valitun tekstin väriä',
  // TrvActionFontBackColor
  'Tekstin taustav&äri...', 'Tekstin taustaväri|Muuta valitun tekstin taustaväriä',
  // TrvActionAddictSpell3
  '&Oikoluku', 'Oikoluku|Tarkista kirjoitusvirheet',
  // TrvActionAddictThesaurus3
  '&Sanakirja', 'Sanakirja|Etsi valitulle sanalle synonyymejä',
  // TrvActionParaLTR
  'Vasemmalta oikealle', 'Vasemmalta oikealle|Aseta valituiden kappaleiden tekstisuunta vasemmalta oikealle',
  // TrvActionParaRTL
  'Oikealta vasemmalle', 'Oikealta vasemmalle|Aseta valituiden kappaleiden tekstisuunta vasemmalta oikealle',
  // TrvActionLTR
  'Vasemmalta oikealle -teksti', 'Vasemmalta oikealle -teksti|Aseta valitun tekstin suunta vasemmalta oikealle',
  // TrvActionRTL
  'Oikealta vasemmalle -teksti', 'Oikealta vasemmalle -teksti|Aseta valitun tekstin suunta oikealta vasemmalle',
  // TrvActionCharCase
  'Merkkikoko', 'Merkkikoko|Muuta valitun tekstin kirjainkokoa (isot/pienet)',
  // TrvActionShowSpecialCharacters
  '&Tulostumattomat merkit', 'Tulostumattomat merkit|Näytä/piilota tulostumattomat merkit, kuten tabuloinnit ja välilyönnit',
  // TrvActionSubscript
  '&Alaindeksi', 'Alaindeksi|Muuta valittu teksti alaindeksiksi',
  // TrvActionSuperscript
  '&Yläindeksi', 'Yläindeksi|Muuta valittu teksti yläindeksiksi',
  // TrvActionInsertFootnote
  '&Alaviite', 'Alaviite|Lisää alaviite',
  // TrvActionInsertEndnote
  '&Loppuviite', 'Loppuviite|Lisää loppuviite',
  // TrvActionEditNote
  '&Muokkaa viitteitä', 'Muokkaa viitteitä|Muokkaa ala- ja loppuviitteitä',
  // TrvActionHide
  '&Piilota', 'Piilota|Näytä/piilota valittu osio',  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Tyylit...', 'Tyylit|Avaa tyylien hallintaikkuna',
  // TrvActionAddStyleTemplate
  '&Lisää tyyli...', 'Lisää tyyli|Luo uusi teksti- tai kappaletyyli',
  // TrvActionClearFormat,
  '&Poista muotoilu', 'Poista muotoilu|Poista valitulta alueelta kaikki teksti- tai kappalemuotoilut',
  // TrvActionClearTextFormat,
  'P&oista tekstimuotoilu', 'Poista tekstimuotoilu|Poista valitulta alueelta kaikki tekstimuotoilut',
  // TrvActionStyleInspector
   'Tyyli&selain', 'Tyyliselain|Näytä tai piilota tyyliselain',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Peruuta', 'Sulje', 'Lisää', '&Avaa...', '&Tallenna...', 'T&yhjennä', 'Ohje', 'Poista',
  // Others  -------------------------------------------------------------------
  // percents
  'prosenttia',
  // left, top, right, bottom sides
  'Vasen reuna', 'Yläreuna', 'Oikea reuna', 'Alareuna',
  // save changes? confirm title
  'Tiedostoa %s on muutettu. Haluatko tallentaa muutokset?', 'Muutosten tallennus',
  // warning: losing formatting
  'Varoitus: %s saattaa sisältää ominaisuuksia, joita valittu tallennusmuoto ei tue.'#13+
  'Haluatko silti tallentaa tässä muodossa?',
  // RVF format name
  'RichView -tallennusmuoto',
  // Error messages ------------------------------------------------------------
  'Virhe',
  'Virhe avattaessa tiedostoa.'#13#13'Mahdollisia syitä:'#13'- sovellus ei tue kyseistä tiedostotyyppiä;'#13+
  '- tiedosto on viallinen;'#13'- jokin toinen sovellus on avannut ja lukinnut tiedoston.',
  'Virhe avattaessa kuvatiedostoa.'#13#13'Mahdollisia syitä:'#13'- sovellus ei tue kyseistä tiedostotyyppiä;'#13+
  '- tiedosto ei ole kuvatiedosto;'#13+
  '- tiedosto on viallinen;'#13'- jokin toinen sovellus on avannut ja lukinnut tiedoston.',
  'Virhe tallennettaessa tiedostoa.'#13#13'Mahdollisia syitä:'#13'- riittämätön vapaa levytila;'#13+
  '- tallennusmedia on kirjoitussuojattu;'#13'- asemassa ei ole siirrettävää tallennusmediaa;'#13+
  '- jokin toinen sovellus on avannut ja lukinnut tiedoston;'#13'- tallennusmedia on viallinen.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView-tiedostot (*.rvf)|*.rvf',  'RTF-tiedostot (*.rtf)|*.rtf' , 'XML-tiedostot (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Tekstitiedostot (*.txt)|*.txt', 'Tekstitiedostot - Unicode (*.txt)|*.txt', 'Tekstitiedostot - Automaattitunnistus (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML-tiedostot (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'yksinkertaiset HTML-tiedostot (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word -asiakirja (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Etsintä lopetettu.', 'Kohdetta ''%s'' ei löydy.',
  // 1 string replaced; Several strings replaced
  'Korvattu yksi merkkijono.', 'Korvattu %d merkkijonoa.',
  // continue search from the beginning/end?
  'Haku suoritettu loppuun asti, jatketaanko asiakirjan alusta?',
  'Haku suoritettu alkuun asti, jatketaanko asiakirjan lopusta?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Läpinäkyvä', 'Automaattinen',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Musta', 'Ruskea', 'Oliivinvihreä', 'Tummanvihreä', 'Tumma sinivihreä', 'Tummansininen', 'Indigo', 'Tummanharmaa',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tummanpunainen', 'Oranssi', 'Keltavihreä', 'Vihreä', 'Sinivihreä', 'Sininen', 'Siniharmaa', 'Harmaa-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Punainen', 'Vaaleanoranssi', 'Lime', 'Merenvihreä', 'Akvamariini', 'Vaaleansininen', 'Purppura', 'Harmaa-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Magenta', 'Kulta', 'Keltainen', 'Kirkkaanvihreä', 'Turkoosi', 'Taivaansininen', 'Luumu', 'Kirkkaanharmaa',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Roosa', 'Keltaisenruskea', 'Vaaleankeltainen', 'Vaaleanvihreä', 'Vaalea turkoosi', 'Kalpeansininen', 'Laventeli', 'Valkoinen',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&Läpinäkyvä', '&Automaattinen', '&Muu väri...', '&Oletus',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Tausta', 'V&äri:', 'Sijainti', 'Tausta', 'Malliteksti',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ei käytössä', '&Venytetty', 'Kii&nteä vierekkäin', 'V&ierekkäin', '&Keskitetty',
  // Padding button
  '&Marginaalit...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Täyttöväri', '&Käytä kohteeseen:', '&Muu väri...', 'Mar&ginaalit...',
  'Valitse väri',
  // [apply to:] text, paragraph, table, cell
  'teksti', 'kappale' , 'taulukko', 'solu',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Fontti', 'Fontti', 'Asettelu',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fontti:', '&Koko', 'Tyyli', '&Lihavoitu', 'K&ursivoitu',
  // Script, Color, Back color labels, Default charset
  'Ko&mentojono:', '&Väri', '&Tausta:', '(Oletus)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Tehosteet', '&Alleviivaus', 'Ylä&viiva', '&Yliviivaus', '&Isot kirjaimet',
  // Sample, Sample text
  'Malli', 'Malliteksti',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Merkkivälit', '&Välistys:', '&Laajennettu', '&Tiivistetty',
  // Offset group-box, Offset label, Down, Up,
  'Pystysuuntainen siirtymä', '&Siirtymä:', '&Alas', '&Ylös',
  // Scaling group-box, Scaling
  'Skaalaus', 'S&kaalaus:',
  // Sub/super script group box, Normal, Sub, Super
  'Indeksit', 'Ta&vallinen', 'Ala&indeksi', 'Yl&äindeksi',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Marginaalit', '&Yläreuna:', '&Vasen:', '&Alareuna:', '&Oikea:', '&Synkronoi',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
'Lisää hyperlinkki', 'Hyperlinkki', 'T&eksti:', '&Kohde:', '<<valinta>>',
  // cannot open URL
  'Ei voi avata sivua "%s"',
  // hyperlink properties button, hyperlink style 
  '&Mukauta...', '&Tyyli:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Hyperlinkin ominaisuudet', 'Normaalivärit', 'Aktiivisen värit',
  // Text [color], Background [color]
  '&Teksti:', 'T&austa',
  // Underline [color]
  '&Alleviivaus:', 
  // Text [color], Background [color] (different hotkeys)
  'T&eksti:', 'Ta&usta',
  // Underline [color], Attributes group-box,
  '&Alleviivaus:', 'Määritteet',
  // Underline type: always, never, active (below the mouse)
  '&Alleviivaus:', 'Aina', 'Ei käytössä', 'Aktiivinen',  
  // Same as normal check-box
  'Kuten &normaalisti',
  // underline active links check-box
  '&Alleviivaa aktiiviset hyperlinkit',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Lisää symboli', '&Fontti:', '&Merkistö:', 'Unicode',
  // Unicode block
  '&Lohko:',  
  // Character Code, Character Unicode Code, No Character
  'Merkkinumero: %d', 'Merkkinumero: Unicode %d', '(ei merkkiä)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Lisää taulukko', 'Taulukon koko', '&Sarakkeiden määrä:', '&Rivien määrä:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Taulukon asettelu', '&Automaattinen koko', '&Sovita ikkunaan', '&Mukautettu koko',
  // Remember check-box
  'Muista asettelut luotaessa &uusia taulukoita',
  // VAlign Form ---------------------------------------------------------------
  'Sijainti',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Ominaisuudet', 'Kuva', 'Sijainti ja koko', 'Rivi', 'Taulukko', 'Rivit', 'Solut',
  // Image Appearance, Image Misc, Number tabs
  'Ulkoasu', 'Muu', 'Numero',
  // Box position, Box size, Box appearance tabs
  'Sijainti', 'Koko', 'Ulkoasu',
  // Preview label, Transparency group-box, checkbox, label
  'Esikatselu:', 'Läpinäkyvä', '&Läpinäkyvä', 'Läpinäkyvä &väri:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Muu väri...', '&Tallenna...', 'Automaattinen',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Pystysuuntainen keskitys', '&Keskitä:',
  'alareuna perusviivalle', 'keskikohta perusviivalle',
  'yläreuna rivin ylös', 'alareuna rivin alas', 'keskikohta rivin keskelle',
  // align to left side, align to right side
  'vasemmalle', 'oikealle',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Siirtymä:', 'Sovita', '&Leveys:', '&Korkeus:', 'Oletuskoko: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Sovita', '&Palauta',
  // Inside the border, border, outside the border groupboxes
  'Reunuksen sisäpuoli', 'Reunus', 'Reunuksen ulkopuoli',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Täyttöväri:', '&Marginaalit:',
  // Border width, Border color labels
  'Reunan &leveys:', 'Reunan &väri:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Vaakaväli:', '&Pystyväli:',
  // Miscellaneous groupbox, Tooltip label
  'Muut', '&Vihje:',
  // web group-box, alt text
  'Selain', 'Vaihtoehtoinen &teksti:',
  // Horz line group-box, color, width, style
  'Vaakaviiva', '&Väri:', '&Leveys:', '&Tyyli:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Taulukko', '&Leveys:', '&Täyttöväri:', '&Solun riviväli...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Vaakamarginaali:', '&Pystymarginaali:', 'Reunat ja tausta',
  // Visible table border sides button, auto width (in combobox), auto width label
  '&Näytä taulukon reunat...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Pidä rivi yhdessä',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Kierto', '&Ei kiertoa', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Reunat:', '&Näytä solujen reunat...',
  // Border, CellBorders buttons
  '&Taulukon reunat...', 'Solujen &reunat...',
  // Table border, Cell borders dialog titles
  'Taulukon reuna', 'Solujen reunat',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tulostus', '&Estä solujen jakaantuminen kahdelle sivulle', '&Ylhäällä toistettavat rivit:',
  'Ylhäällä toistettavat rivit toistetaan jokaisella tulostussivulla.',
  // top, center, bottom, default
  '&Yläreuna', '&Keskellä', '&Alareuna', '&Oletus',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Asetukset', 'Suositeltu &leveys:', '&Minimikorkeus:', '&Täyttöväri:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Reunat', 'Näkyvät:',  'Va&rjon väri:', 'Valon v&äri', '&Väri:',
  // Background image
  '&Kuva...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Vaakasijainti', 'Pystysijainti', 'Sijainti teksteissä',
  // Position types: align, absolute, relative
  'Kohdistettu', 'Absoluuttinen', 'Suhteellinen',
  // [Align] left side, center, right side
  'Vasen reuna seuraavan kohteen vasempaan reunaan:',
  'Keskipiste keskelle kohdetta',
  'Oikea reuna seuraavan kohteen oikeaan reunaan:',
  // [Align] top side, center, bottom side
  'Yläreuna seuraavan kohteen yläreunaan:',
  'Keskipiste keskelle kohdetta',
  'Alareuna seuraavan kohteen alareunaan:',
  // [Align] relative to
  'Suhteessa kohteeseen',
  // [Position] to the right of the left side of
  'Seuraavan kohteen oikealle puolelle:',
  // [Position] below the top side of
  'Seuraavan kohteen yläreunaan:',
  // Anchors: page, main text area (x2)
  '&Sivu', '&Leipäteksti', 'Si&vu', 'Le&ipäteksti',
  // Anchors: left margin, right margin
  '&Vasen reunus', '&Oikea reunus',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Yläreunus', '&Alareunus', '&Sisempi reunus', '&Ulompi reunus',
  // Anchors: character, line, paragraph
  '&Merkki', '&Rivi', '&Kappale',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&Peilikuvareunukset', '&Asettelu taulukon solussa',
  // Above text, below text
  'Tekstin &päällä', 'Tekstin &alla',
  // Width, Height groupboxes, Width, Height labels
  'Leveys', 'Korkeus', '&Leveys:', '&Korkeus:',
  // Auto height label, Auto height combobox item
  'Automaattinen', 'automaattinen',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Pystytasaus', '&Ylös', '&Keskelle', '&Alas',
  // Border and background button and title
  '&Reunus ja täyttö...', 'Laatikon reunus ja täyttö',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Liitä määräten', '&Liitetään:', 'Muotoiltu teksti (RTF)', 'HTML-muoto',
  'Muotoilematon teksti', 'Muotoilematon Unicode-teksti', 'Kuva (bittikartta)', 'Kuva (Windows-metatiedosto)',
  'Kuva (EMF-muotoinen)',  'URL-osoite',
  // Options group-box, styles
  'Lisävalinnat', '&Tyylit:',
  // style options: apply target styles, use source styles, ignore styles
  'käytä valittuja tyylejä', 'säilytä tyylit ja ulkoasu',
  'säilytä ulkoasu, älä huomioi tyylejä',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Luettelomerkit ja numerointi', 'Luettelomerkit', 'Numerointi', 'Luettelomerkkikirjasto', 'Numerointikirjasto',
  // Customize, Reset, None
  '&Mukauta...', '&Palauta', 'Ei mitään',
  // Numbering: continue, reset to, create new
  'jatka numerointia', 'palauta numeroksi', 'luo uusi, aloita numerosta',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'A, B, C, ...', 'I, II, III, ...', '1, 2, 3, ...', 'a, b, c, ...', 'i, ii, iii, ...',
  // Bullet type
  'Ympyrä', 'Pallo', 'Neliö',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Taso:', '&Alkaen:', '&Jatka', 'Numerointi',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Mukautettu luettelo', 'Tasot', '&Määrä:', 'Ominaisuudet', '&Tyyppi:',
  // List types: bullet, image
  'Symboli', 'Kuva', 'Lisää numero|',
  // Number format, Number, Start level from, Font button
  '&Numeron muotoilu:', 'Numero', '&Aloita numerointi tasosta:', '&Fontti...',
  // Image button, bullet character, Bullet button,
  '&Kuva...', '&Symboli', '&Luettelomerkki...',
  // Position of list text, bullet, number, image, text
  'Merkkikirjaston sijoittelu', 'Luettelomerkin sijoittelu', 'Numeron sijoittelu', 'Kuvan sijoittelu', 'Tekstin sijoittelu',
  // at, left indent, first line indent, from left indent
  'pos.', '&Vasen sisennys:', '&Ensimmäinen rivi:', '(Lisäsisennys)',
  // [only] one level preview, preview
  '&Yksitasoinen esikatselu', 'Esikatselu',
  // Preview text
  'Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Tasaa vasempaan', 'Tasaa oikeaan', 'Keskitä',
  // level #, this level (for combo-box)
  'Taso %d', 'Nykyinen taso',
  // Marker character dialog title
  'Mukautettu luettelo',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Kappaleiden reunat ja taustat', 'Reunat', 'Tausta',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Asetukset', '&Väri:', '&Leveys:', '&Sisäinen leveys:', 'Teksti&reunukset...',
  // Sample, Border type group-boxes;
  'Esikatselu', 'Reunaviivatyyppi:',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ei mitään', '&Yksi', '&Kaksi', 'K&olme', '&Paksu sisäreuna', 'Paksu &ulkoreuna',
  // Fill color group-box; More colors, padding buttons
  'Täyttö', '&Muu väri...', '&Marginaalit...',
  // preview text
  'Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti.',
  // title and group-box in Offsets dialog
  'Tekstireunukset', 'Reunukset',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Kappale', 'Tasaus', '&Vasen', '&Oikea', '&Keskitä', '&Tasattu',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Välit', '&Yläreuna:', '&Alareuna:', 'Riviväli:', 'S&uuruus:',
  // Indents group-box; indents: left, right, first line
  'Sisennys', 'Vase&n:', 'O&ikea:', '&Ensimmäinen rivi:',
  // indented, hanging
  '&Sisennetty', '&Riippuva', 'Esikatselu',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '1.0', '1.5', '2.0', 'Vähintään', 'Kiinteä', 'Mukautettu',
  // preview text
  'Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti. Teksti teksti teksti teksti teksti.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Sisennykset ja välit', 'Sarkaimet', 'Tekstin rivitys',
  // tab stop position label; buttons: set, delete, delete all
  '&Sijainti:', '&Aseta', '&Poista', 'Poista ka&ikki',
  // tab align group; left, right, center aligns,
  'Tasaus', '&Vasen', '&Oikea', '&Keskitä',
  // leader radio-group; no leader
  'Täyttömerkki', '(Ei mitään)',
  // tab stops to be deleted, delete none, delete all labels
  'Poistettavat sarkaimet:', '(Ei mitään)', 'Kaikki',
  // Pagination group-box, keep with next, keep lines together
  'Tekstin rivitys', '&Estä rivijako seuraavalle sivulle', '&Sido seuraavaan',
  // Outline level, Body text, Level #
  '&Jäsennystaso:', 'Leipäteksti', 'Taso %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Esikatselu', 'Sivun leveys', 'Yksi sivu', 'Sivut:',
  // Invalid Scale, [page #] of #
  'Syötä luku väliltä 10 - 500', '/ %d',
  // Hints on buttons: first, prior, next, last pages
  'Ensimmäinen', 'Edellinen', 'Seuraava', 'Viimeinen',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Solujen oletusvälistykset', 'Välit', '&Solujen välissä', 'Taulukon reunuksen ja solujen välissä',
  // vertical, horizontal (x2)
  '&Pystysuunta:', '&Vaakasuunta:', 'P&ystysuunta:', 'V&aakasuunta:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Reunat', 'Asetukset', '&Väri:', 'V&aloväri:', 'Varjov&äri:',
  // Width; Border type group-box;
  '&Leveys:', 'Reunatyyppi:',
  // Border types: none, sunken, raised, flat
  '&Ei mitään', '&Upotettu', '&Korotettu', '&Tasainen',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Jaa', 'Jaa useisiin',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Valitse rivien ja sarakkeiden määrät', '&Alkuperäiset solut',
  // number of columns, rows, merge before
  '&Sarakkeita:', '&Rivejä:', '&Yhdistä ennen jakamista',
  // to original cols, rows check-boxes
  'A&lkuperäisen sarakejaon mukaisiksi', 'Al&kuperäisen rivijaon mukaisiksi',
  // Add Rows form -------------------------------------------------------------
  'Lisää rivejä', 'Rivien määrä:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Sivun asetukset', 'Sivu', 'Tunnisteet',
  // margins group-box, left, right, top, bottom
  'Reunukset (millimetriä)', 'Reunukset (tuumaa)', 'Va&sen:', '&Yläreuna:', '&Oikea:', '&Alareuna:',
  // mirror margins check-box
  'P&eilikuvareunukset',
  // orientation group-box, portrait, landscape
  'Suunta', '&Pysty', '&Vaaka',
  // paper group-box, paper size, default paper source
  'Paperi', '&Koko:', '&Lähdelokero:',
  // header group-box, print on the first page, font button
  'Ylätunniste', '&Teksti:', '&Ensimmäisen sivun ylätunniste', '&Fontti...',
  // header alignments: left, center, right
  '&Vasemmalla', '&Keskellä', '&Oikealla',
  // the same for footer (different hotkeys)
  'Alatunniste', 'Te&ksti:', 'Ensimmäisen sivun alat&unniste', 'Fo&ntti...',
  'Va&semmalla', 'Keskell&ä', 'Oikea&lla',
  // page numbers group-box, start from
  'Sivunumerot', '&Alkaen:',
  // hint about codes
  'Sallitut erikoislyhenteet:'#13'&&p - sivunumero; &&P - sivumäärä; &&d - tulostuspäivä; &&t - tulostusaika',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Tiedostomuoto', '&Valitse merkistökoodaus:',
  // thai, japanese, chinese (simpl), korean
  'Thaimaalainen', 'Japanilainen', 'Kiinalainen (Yksinkertaistettu)', 'Korealainen',
  // chinese (trad), central european, cyrillic, west european
  'Kiinalainen (Perinteinen)', 'Keski- ja itäeurooppalainen', 'Kyrillinen', 'Länsimainen',
  // greek, turkish, hebrew, arabic
  'Kreikkalainen', 'Turkkilainen', 'Heprealainen', 'Arabialainen',
  // baltic, vietnamese, utf-8, utf-16
  'Balttilainen', 'Vietnamilainen', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
   'Ulkoasu', '&Valitse tyyli:',
  // Delimiter form ------------------------------------------------------------
  // title, label
   'Muunna tekstiksi', 'Valitse &erotin:',
  // line break, tab, ';', ','
  'väli', 'sarkain', 'puolipiste', 'pilkku',
  // Table sort form -----------------------------------------------------------
  // error message
   'Yhdistettyjä rivejä sisältävää taulukkoa ei voida lajitella',
  // title, main options groupbox
   'Taulukon lajittelu', 'Lajittelu',
  // sort by column, case sensitive
   '&Lajitteluperuste sarake:', '&Sama kirjainkoko',
  // heading row, range or rows
  'Tiedot sisältävät &otsikon', 'Lajiteltavat rivit:  %d - %d',
  // order, ascending, descending
  'Järjestys', '&Nouseva', '&Laskeva',
  // data type, text, number
  'Tyyppi', '&Teksti', '&Luku',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Lisää numero', 'Ominaisuudet', '&Laskurin nimi:', '&Numerointityyppi:',
  // numbering groupbox, continue, start from
  'Numerointi', '&Jatka', '&Alkaen luvusta:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Tunniste', '&Laji:', '&Piilota laji',
  // position radiogroup
  'Sijainti', 'Valitun kohteen &yläpuolelle', 'Valitun kohteen &alapuolelle',
  // caption text
  '&Tekstitunniste:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numero', 'Kuva', 'Taulukko',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Näytä reunukset', 'Reunus',
  // Left, Top, Right, Bottom checkboxes
  '&Vasemmalla', '&Yllä', '&Oikealla', '&Alla',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Sarakekoon uudelleenmääritys', 'Rivikoon uudelleenmääritys',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Ensimmäisen rivin sisennys', 'Vasen sisennys', 'Riippuva', 'Oikea sisennys',
  // Hints on lists: up one level (left), down one level (right)
  'Yksi taso ylemmäs', 'Yksi taso  alemmas',
  // Hints for margins: bottom, left, right and top
  'Alareunus', 'Oikea reunus', 'Vasen reunus', 'Yläreunus',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Vasempaan tasaava sarkain', 'Oikeaan tasaava sarkain', 'Keskityssarkain', 'Desimaalisarkain',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Tavallinen', 'Tavallinen sisennys', 'Ei välejä', 'Otsikko %d', 'Luettelo',
  // Hyperlink, Title, Subtitle
  'Hyperlinkki', 'Otsikko', 'Aliotsikko',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Korostus', 'Heikko korostus', 'Voimakas korostus', 'Vahva',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Sitaatti', 'Korostettu sitaatti', 'Alaviite', 'Korostettu viite',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Lohkoteksti', 'HTML-muuttuja', 'HTML-koodi', 'HTML-akronyymi',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-määritys', 'HTML-näppäimistö', 'HTML-näyte', 'HTML-kirjoituskone',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML-esimuotoiltu', 'HTML-sitaatti', 'Ylätunniste', 'Alatunniste', 'Sivunumero',
  // Caption
  'Tekstitunniste',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Loppuviite', 'Alaviite', 'Loppuviitteen teksti', 'Alaviitteen teksti',
  // Sidenote Reference, Sidenote Text
  'Reunahuomautus', 'Huomautusteksti',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'väri', 'taustaväri', 'läpikuultava', 'oletus', 'alleviivausväri',
  // default background color, default text color, [color] same as text
  'oletustaustaväri', 'oletustekstiväri', 'kuten teksti',
  // underline types: single, thick, double, dotted, thick dotted, dashed
   'yksinkertainen', 'paksu', 'kaksoisviiva', 'pisteviiva', 'paksu pisteviiva', 'katkoviiva',
  // underline types: thick dashed, long dashed, thick long dashed,
  'paksu katkoviiva', 'pitkä katkoviiva', 'pitkä paksu katkoviiva',
  // underline types: dash dotted, thick dash dotted,
   'pistekatkoviiva', 'paksu pistekatkoviiva',
  // underline types: dash dot dotted, thick dash dot dotted
  'kaksipistekatkoviiva', 'paksu kaksipistekatkoviiva',
  // sub/superscript: not, subsript, superscript
  'ei indeksejä', 'alaindeksi', 'yläindeksi',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'kirjoitussuunta:', 'peritty', 'vasemmalta oikealle', 'oikealta vasemmalle',
  // bold, not bold
  'lihavointi', 'ei lihavointia',
  // italic, not italic
  'kursivointi', 'ei kursivointia',
  // underlined, not underlined, default underline
  'alleviivaus', 'ei alleviivausta', 'oletusalleviivaus',
  // struck out, not struck out
  'korostus', 'ei korostusta',
  // overlined, not overlined
  'yliviivaus', 'ei yliviivausta',
  // all capitals: yes, all capitals: no
  'isoin kirjaimin', 'ei isoin kirjaimin',
  // vertical shift: none, by x% up, by x% down
  'ei pystysiirtymää', 'pystysiirtymä %d%% ylös', 'pystysiirtymä %d%% alas',
  // characters width [: x%]
  'merkkileveys',
  // character spacing: none, expanded by x, condensed by x
  'tavallinen kirjasinväli', 'harvennettu kirjasinväli %s', 'tiivistetty kirjasinväli %s',
  // charset
  'merkistö',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'oletusfontti', 'oletuskappale', 'peritty',
  // [hyperlink] highlight, default hyperlink attributes
  'korostus', 'oletus',
  // paragraph aligmnment: left, right, center, justify
  'tasaa vasemmalle', 'tasaa oikealle', 'keskitetty', 'tasaa molemmat reunat',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'rivikorkeus: %d%%', 'riviväli: %s', 'rivikorkeus: vähintään %s',
  'rivikorkeus: tasan %s',
  // no wrap, wrap
  'ei rivitystä', 'automaattinen rivitys',
  // keep lines together: yes, no
  'pidä rivit yhdessä', 'älä pidä rivejä yhdessä',
  // keep with next: yes, no
  'sido seuraavaan kappaleeseen', 'älä sido seuraavaan kappaleeseen',
  // read only: yes, no
  'vain luku', 'muokkaus sallittu',
  // body text, heading level x
  'ulkoreunus: leipäteksti', 'ulkoreunus: %d',
  // indents: first line, left, right
  'ensimmäisen rivin sisennys', 'vasen sisennys', 'oikea sisennys',
  // space before, after
  'välilyönti eteen', 'välilyönti jälkeen',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'sarkaimet', 'ei sarkaimia', 'tasaa', 'vasen', 'oikea', 'keskitä',
  // tab leader (filling character)
  'sarkainmerkki',
  // no, yes
  'ei', 'kyllä',
  // [padding/spacing/side:] left, top, right, bottom
  'vasen', 'yläreuna', 'oikea', 'alareuna',
  // border: none, single, double, triple, thick inside, thick outside
  'ei reunaviivaa', 'yksinkertainen', 'kaksoisviiva', 'kolmoisviiva', 'paksu sisäpuolella', 'paksu ulkopuolella',
  // background, border, padding, spacing [between text and border]
  'tausta', 'reuna', 'marginaalit', 'välialue',
  // border: style, width, internal width, visible sides
  'tyyli', 'leveys', 'sisäinen leveys', 'näytä reunat',
  // style inspector -----------------------------------------------------------
  // title
  'Tyyliselain',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Tyyli', '(ei käytössä)', 'Kappale', 'Fontti', 'Määritteet',
  // border and background, hyperlink, standard [style]
  'Reunat ja tausta', 'Hyperlinkki', '(vakio)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Tyylit', 'Tyyli',
  // name, applicable to,
  '&Nimi:', '&Käytettävissä kohteissa:',
  // based on, based on (no &),
  '&Pohjautuu tyyliin:', 'Pohjautuen:',
  //  next style, next style (no &)
 '&Seuraavassa kappaleessa käytettävä tyyli:', 'Seuraavassa kappaleessa käytettävä tyyli:',
  // quick access check-box, description and preview
  '&Pikasiirtymä', 'Kuvaus ja esikatselu:',
  // [applicable to:] paragraph and text, paragraph, text
  'Kappale ja teksti', 'Kappale', 'Teksti',
  // text and paragraph styles, default font
  'Teksti- ja kappaletyyli', 'Oletusfontti',
  // links: edit, reset, buttons: add, delete
  'Muokkaa', 'Palauta', '&Lisää', '&Poista',
  // add standard style, add custom style, default style name
  'Lisää &vakiotyyli...', 'Lisää &mukautettu tyyli', 'Tyyli %d',
  // choose style
  'Valitse &käytettävä tyyli:',
  // import, export,
  '&Tuo...', '&Vie...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView-tyylit (*.rvst)|*.rvst', 'Virhe ladattaessa tiedostoa', 'Tiedosto ei ole tyylitiedosto',
  // Title, group-box, import styles
  'Tuo tyylit tiedostosta', 'Tuonti', '&Tuo tyylit:',
  // existing styles radio-group: Title, override, auto-rename
  'Olemassaolevat tyylit', '&korvaa', '&lisää uudestaannimettynä',
  // Select, Unselect, Invert,
  '&Valitse', '&Poista valinta', '&Valitse käänteisesti',
  // select/unselect: all styles, new styles, existing styles
  '&Kaikki tyylit', '&Uudet tyylit', '&Olemassaolevat tyylit',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Poista muotoilut', 'Kaikki tyylit',
  // dialog title, prompt
  'Tyylit', '&Valitse käytettävä tyyli:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonyymit', '&Ohita kaikki', '&Lisää sanastoon',
  // Progress messages ---------------------------------------------------------
  'Ladataan %s', 'Valmistellaan tulostusta...', 'Tulostetaan sivua %d',
  'Muunnetaan RTF-muodosta...',  'Muunnetaan RTF-muotoon...',
  'Luetaan %s...', 'Kirjoitetaan %s...', 'teksti',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ei merkintää', 'Alaviite', 'Loppuviite', 'Reunahuomautus', 'Tekstiruutu'  
  );


initialization
  RVA_RegisterLanguage(
    'Finnish', // english language name, do not translate
    'Suomi', // native language name
    ANSI_CHARSET, @Messages);

end.
