﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TableBrdrRVFrm.pas' rev: 27.00 (Windows)

#ifndef TablebrdrrvfrmHPP
#define TablebrdrrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tablebrdrrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVTableBrdr;
class PASCALIMPLEMENTATION TfrmRVTableBrdr : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gbTableBorder;
	Vcl::Stdctrls::TLabel* lblTableBorderColor;
	Vcl::Stdctrls::TLabel* lblTableBorderWidth;
	Vcl::Stdctrls::TLabel* lblTableBorderLightColor;
	Rvcolorcombo::TRVColorCombo* cmbBorderColor;
	Vcl::Stdctrls::TComboBox* cmbBorderWidth;
	Rvcolorcombo::TRVColorCombo* cmbBorderLightColor;
	Vcl::Controls::TImageList* ImageList1;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgBorderType;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	void __fastcall cmbBorderWidthDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall cmbBorderColorColorChange(System::TObject* Sender);
	void __fastcall rgBorderTypeClick(System::TObject* Sender);
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall cmbBorderWidthChange(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	bool Updating;
	
protected:
	Vcl::Controls::TControl* _cmbBorderWidth;
	Vcl::Controls::TControl* _lblTableBorderColor;
	Vcl::Controls::TControl* _lblTableBorderLightColor;
	Rvstyle::TRVStyle* FRVStyle;
	
public:
	Vcl::Dialogs::TColorDialog* ColorDialog;
	void __fastcall SetData(Rvtable::TRVTableBorderStyle BorderStyle, System::Uitypes::TColor ColorL, System::Uitypes::TColor ColorD, Rvstyle::TRVStyleLength BorderWidth, Rvstyle::TRVStyle* RVStyle);
	void __fastcall GetData(Rvtable::TRVTableBorderStyle &BorderStyle, System::Uitypes::TColor &ColorL, System::Uitypes::TColor &ColorD, Rvstyle::TRVStyleLength &BorderWidth);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall AllowInvertImageListImage(Vcl::Imglist::TCustomImageList* il, int Index);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVTableBrdr(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVTableBrdr(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVTableBrdr(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVTableBrdr(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tablebrdrrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TABLEBRDRRVFRM)
using namespace Tablebrdrrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TablebrdrrvfrmHPP
