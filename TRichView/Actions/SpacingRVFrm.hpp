﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'SpacingRVFrm.pas' rev: 27.00 (Windows)

#ifndef SpacingrvfrmHPP
#define SpacingrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <FourSidesRVFrm.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Spacingrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVSpacing;
class PASCALIMPLEMENTATION TfrmRVSpacing : public Foursidesrvfrm::TfrmRVFourSides
{
	typedef Foursidesrvfrm::TfrmRVFourSides inherited;
	
__published:
	Vcl::Stdctrls::TLabel* Label6;
	Vcl::Extctrls::TBevel* Bevel1;
	Vcl::Stdctrls::TCheckBox* cbCellSpacing;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cbCellSpacingClick(System::TObject* Sender);
	HIDESBASE void __fastcall seTopChange(System::TObject* Sender);
	void __fastcall seBottomChange(System::TObject* Sender);
	void __fastcall seLeftChange(System::TObject* Sender);
	void __fastcall seRightChange(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _cbCellSpacing;
	
public:
	DYNAMIC void __fastcall Localize(void);
	void __fastcall SetSpacing(Rvstyle::TRVStyleLength BorderHSpacing, Rvstyle::TRVStyleLength CellHSpacing, Rvstyle::TRVStyleLength BorderVSpacing, Rvstyle::TRVStyleLength CellVSpacing, Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVUnits Units);
	void __fastcall GetSpacing(Rvstyle::TRVStyleLength &BorderHSpacing, Rvstyle::TRVStyleLength &CellHSpacing, Rvstyle::TRVStyleLength &BorderVSpacing, Rvstyle::TRVStyleLength &CellVSpacing, bool &FBorderHSpacing, bool &FCellHSpacing, bool &FBorderVSpacing, bool &FCellVSpacing, Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVUnits Units);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVSpacing(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Foursidesrvfrm::TfrmRVFourSides(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVSpacing(System::Classes::TComponent* AOwner, int Dummy) : Foursidesrvfrm::TfrmRVFourSides(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVSpacing(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVSpacing(HWND ParentWindow) : Foursidesrvfrm::TfrmRVFourSides(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Spacingrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SPACINGRVFRM)
using namespace Spacingrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SpacingrvfrmHPP
