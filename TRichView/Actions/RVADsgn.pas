
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Property editors for RichViewActions            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVADsgn;

interface

{$I RV_Defs.inc}

uses SysUtils, RVALocalize, Classes, RVSEdit, RVStyle, Graphics,
  {$IFDEF RICHVIEWDEFXE2}
  UITypes,
  {$ENDIF}
  {$IFDEF RICHVIEWDEF6}
  DesignIntf, DesignEditors, ColnEdit, VCLEditors
  {$ELSE}
  DsgnIntf
  {$ENDIF};

type

  TRVALanguageProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  {$IFDEF RICHVIEWDEF5}
  {$IFNDEF RVDONOTUSESEQ}
  TRVFloatPositionProperty = class (TRVStyleLengthProperty)
  protected
    function GetUnitName(Obj: TPersistent; const PropName: String): String; override;
  end;

  TRVFloatSizeProperty = class (TRVStyleLengthProperty)
  protected
    function GetUnitName(Obj: TPersistent; const PropName: String): String; override;
  end;
  {$ENDIF}
  {$ENDIF}


procedure Register;

implementation
uses
  {$IFNDEF RVDONOTUSESEQ}
  RVFloatingBox, RVFloatingPos,
  {$ENDIF}
  RichViewActions;

{$IFDEF RICHVIEWDEF5}
var OldGetUnitNameFunc: TRVGetUnitNameFunc = nil;
{$ENDIF}

{$IFDEF RICHVIEWDEF5}
{$IFNDEF RVDONOTUSESEQ}
{============================== TRVFloatPositionProperty ======================}
function TRVFloatPositionProperty.GetUnitName(Obj: TPersistent; const PropName: String): String;
begin
  if (Obj is TRVBoxPosition) then begin
    if CompareText(PropName, 'HorizontalOffset')=0 then
      case TRVBoxPosition(Obj).HorizontalPositionKind of
        rvfpkAlignment: Result := '(not used)';
        rvfpkPercentPosition: Result := '/ 1000 %';
        else Result := inherited GetUnitName(Obj, PropName);
      end
    else if CompareText(PropName, 'VerticalOffset')=0 then
      case TRVBoxPosition(Obj).VerticalPositionKind of
        rvfpkAlignment: Result := '(not used)';
        rvfpkPercentPosition: Result := '/ 1000 %';
        else Result := inherited GetUnitName(Obj, PropName);
      end
    end
  else
    Result := inherited GetUnitName(Obj, PropName);
end;
{========================= TRVFloatSizeProperty ===============================}
function TRVFloatSizeProperty.GetUnitName(Obj: TPersistent; const PropName: String): String;
begin
  if (Obj is TRVBoxProperties) then begin
    if CompareText(PropName, 'Width')=0 then
      case TRVBoxProperties(Obj).WidthType of
        rvbwtAbsolute: Result := inherited GetUnitName(Obj, PropName);
        else           Result :=  '/ 1000 %';
      end
    else if CompareText(PropName, 'Height')=0 then
      case TRVBoxProperties(Obj).HeightType of
        rvbhtAuto:     Result := '(not used)';
        rvbhtAbsolute: Result := inherited GetUnitName(Obj, PropName);
        else           Result := '/ 1000 %';

      end
    end
  else
    Result := inherited GetUnitName(Obj, PropName);
end;
{$ENDIF}
{$ENDIF}
{=========================== TRVALanguageProperty =============================}
function TRVALanguageProperty.GetAttributes: TPropertyAttributes;
begin
  Result := (inherited GetAttributes) + [paValueList];
end;
{------------------------------------------------------------------------------}
procedure TRVALanguageProperty.GetValues(Proc: TGetStrProc);
begin
  RVA_EnumLanguages(Proc);
end;
{------------------------------------------------------------------------------}
procedure TRVALanguageProperty.SetValue(const Value: string);
begin
  inherited;
end;
{==============================================================================}
{$IFDEF RICHVIEWDEF5}
function RVAGetUnitName(Obj: TPersistent; const PropName: String): String;
begin
  if (Obj is TrvActionIndent) or
     {$IFNDEF RVDONOTUSELISTS}
     (Obj is TrvActionParaList) or
     (Obj is TrvActionCustomParaListSwitcher) or
     {$ENDIF}
     (Obj is TrvActionInsertPicture) or
     (Obj is TrvActionInsertHLine) or
     {$IFNDEF RVDONOTUSESTYLETEMPLATES}
     (Obj is TrvActionStyleTemplates) or
     (Obj is TrvActionNew) or
     {$ENDIF}
     {$IFNDEF RVDONOTUSESEQ}
     (Obj is TrvActionCustomInsertSidenote) or
     {$ENDIF}
     (Obj is TrvActionInsertTable)
  then
    if TrvAction(obj).GetControlPanel.UnitsProgram=rvstuPixels then
      Result := 'px'
    else
      Result := 'tw'
  {$IFNDEF RVDONOTUSESEQ}
  else if Obj is TRVABoxProperties then
    Result := RVAGetUnitName(TRVABoxProperties(Obj).Owner, PropName)
  else if Obj is TRVABoxPosition then
    Result := RVAGetUnitName(TRVABoxPosition(Obj).Owner, PropName)
  {$ENDIF}
  else
    if Assigned(OldGetUnitNameFunc) then
      Result := OldGetUnitNameFunc(Obj, PropName)
    else
      Result := '';
end;
{$ENDIF}
{==============================================================================}
procedure Register;
begin
  {$IFDEF RICHVIEWDEFXE}
  RegisterPropertyEditor(TypeInfo(TFontName), TRVAControlPanel,'', TFontNameProperty);
  RegisterPropertyEditor(TypeInfo(TFontName), TRVAFont,'',  TFontNameProperty);
  {$ENDIF}
  RegisterPropertyEditor(TypeInfo(TRVALanguageName), nil,'',  TRVALanguageProperty);
  {$IFDEF RICHVIEWDEF5}
  {$IFNDEF RVDONOTUSESEQ}
  RegisterPropertyEditor(TypeInfo(TRVFloatPosition), TRVBoxPosition, '',  TRVFloatPositionProperty);
  RegisterPropertyEditor(TypeInfo(TRVFloatSize), TRVBoxProperties, '',  TRVFloatSizeProperty);
  {$ENDIF}
  {$ENDIF}
end;

{$IFDEF RICHVIEWDEF5}

initialization
  OldGetUnitNameFunc := RVGetUnitNameFunc;
  RVGetUnitNameFunc := RVAGetUnitName;
finalization
  RVGetUnitNameFunc := OldGetUnitNameFunc;

{$ENDIF}


end.
