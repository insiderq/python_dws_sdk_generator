{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Insert-table dialog                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit InsTableRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils,
  Classes, Graphics, Controls, Forms, RVStyle,
  Dialogs, StdCtrls, BaseRVFrm, Buttons, RVSpinEdit, RVALocalize;

type
  TfrmRVInsTable = class(TfrmRVBase)
    gbSize: TGroupBox;
    lblnCols: TLabel;
    lblnRows: TLabel;
    cbRemember: TCheckBox;
    btnOk: TButton;
    btnCancel: TButton;
    seColumns: TRVSpinEdit;
    seRows: TRVSpinEdit;
    gbLayout: TGroupBox;
    rbFitContents: TRadioButton;
    rbFitWindow: TRadioButton;
    rbFitManual: TRadioButton;
    seWidth: TRVSpinEdit;
    cmbWidthType: TComboBox;
    procedure rbFitManualClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbWidthTypeClick(Sender: TObject);
  private
    { Private declarations }
    _rbFitContents, _rbFitWindow, _rbFitManual,
    _cmbWidthType: TControl;
    function GetBestWidth: TRVStyleLength;
    function GetColumns: integer;
    function GetRows: integer;
    procedure SetBestWidth(const Value: TRVStyleLength);
    procedure SetColumns(const Value: integer);
    procedure SetRows(const Value: integer);
    procedure CheckEnabled;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _cbRemember: TControl;
    OptimalBestWidth: TRVStyleLength;
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    property Columns: integer read GetColumns write SetColumns;
    property Rows: integer read GetRows write SetRows;
    property BestWidth: TRVStyleLength read GetBestWidth write SetBestWidth;
  end;

implementation

uses RichViewActions;

{$R *.dfm}

{ TInsTable }

procedure TfrmRVInsTable.CheckEnabled;
begin
  seWidth.Enabled := GetRadioButtonChecked(_rbFitManual);
  if seWidth.Enabled then
    cmbWidthTypeClick(nil);
  _cmbWidthType.Enabled := GetRadioButtonChecked(_rbFitManual);
end;

function TfrmRVInsTable.GetBestWidth: TRVStyleLength;
begin
  if GetRadioButtonChecked(_rbFitContents) then
    result := 0
  else if GetRadioButtonChecked(_rbFitWindow) then
    result := -100
  else begin
    if GetXBoxItemIndex(_cmbWidthType) = 0 then
      result := -seWidth.AsInteger
    else
      result := TRVStyle.RVUnitsToUnitsEx(seWidth.Value,
        TRVAControlPanel(ControlPanel).UnitsDisplay,
        TRVAControlPanel(ControlPanel).UnitsProgram);
  end;
  if Result<-100 then
    Result := -1;
end;

function TfrmRVInsTable.GetColumns: integer;
begin
  result := seColumns.AsInteger;
end;

function TfrmRVInsTable.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gbSize.Left*2+gbSize.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-cbRemember.Top-cbRemember.Height);
  Result := True;
end;

function TfrmRVInsTable.GetRows: integer;
begin
  result := seRows.AsInteger;
end;

procedure TfrmRVInsTable.SetBestWidth(const Value: TRVStyleLength);
begin
  if Value = 0 then
    SetRadioButtonChecked(_rbFitContents, True)
  else if Value = -100 then
    SetRadioButtonChecked(_rbFitWindow, True)
  else begin
    SetRadioButtonChecked(_rbFitManual, True);
    if Value > 0 then begin
      SetXBoxItemIndex(_cmbWidthType, 1);
      UpdateLengthSpinEdit(seWidth, False, True);
      seWidth.Value := TRVStyle.GetAsRVUnitsEx(Value,
        TRVAControlPanel(ControlPanel).UnitsProgram,
        TRVAControlPanel(ControlPanel).UnitsDisplay);
      end
    else begin
      SetXBoxItemIndex(_cmbWidthType, 0);
      UpdatePercentSpinEdit(seWidth, 1);
      seWidth.Value := -Value;
    end;
  end;
end;

procedure TfrmRVInsTable.SetColumns(const Value: integer);
begin
  seColumns.Value := Value;
end;

procedure TfrmRVInsTable.SetRows(const Value: integer);
begin
  seRows.Value := Value;
end;

procedure TfrmRVInsTable.rbFitManualClick(Sender: TObject);
begin
  CheckEnabled;
end;

procedure TfrmRVInsTable.FormCreate(Sender: TObject);
begin
  _rbFitContents := rbFitContents;
  _rbFitWindow := rbFitWindow;
  _rbFitManual := rbFitManual;
  _cbRemember := cbRemember;
  _cmbWidthType := cmbWidthType;
  inherited;
  SetXBoxItemIndex(_cmbWidthType, 0);
  CheckEnabled;
end;

procedure TfrmRVInsTable.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_Insert, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  Caption := RVA_GetS(rvam_it_Title, ControlPanel);
  gbSize.Caption := RVA_GetSHAsIs(rvam_it_TableSize, ControlPanel);
  lblnCols.Caption := RVA_GetSAsIs(rvam_it_nCols, ControlPanel);
  lblnRows.Caption := RVA_GetSAsIs(rvam_it_nRows, ControlPanel);
  gbLayout.Caption := RVA_GetSHAsIs(rvam_it_TableLayout, ControlPanel);
  rbFitContents.Caption := RVA_GetSAsIs(rvam_it_Autosize, ControlPanel);
  rbFitWindow.Caption := RVA_GetSAsIs(rvam_it_Fit, ControlPanel);
  rbFitManual.Caption := RVA_GetSAsIs(rvam_it_Manual, ControlPanel);
  cbRemember.Caption := RVA_GetSAsIs(rvam_it_Remember, ControlPanel);
  cmbWidthType.Items[0] := RVA_GetSAsIs(rvam_Percents, ControlPanel);
  cmbWidthType.Items[1] := RVA_GetUnitsNameAsIs(TRVAControlPanel(ControlPanel).UnitsDisplay,
    True, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVInsTable.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _rbFitContents then
    _rbFitContents := NewControl
  else if OldControl= _rbFitWindow then
    _rbFitWindow := NewControl
  else if OldControl= _rbFitManual then
    _rbFitManual := NewControl
  else if OldControl= _cbRemember then
    _cbRemember := NewControl
  else if OldControl = _cmbWidthType then
    _cmbWidthType := NewControl;
end;
{$ENDIF}

procedure TfrmRVInsTable.cmbWidthTypeClick(Sender: TObject);
begin
  if GetXBoxItemIndex(_cmbWidthType) = 1 then begin
    UpdateLengthSpinEdit(seWidth, False, True);
    seWidth.Value := TRVStyle.GetAsRVUnitsEx(OptimalBestWidth,
      TRVAControlPanel(ControlPanel).UnitsProgram,
      TRVAControlPanel(ControlPanel).UnitsDisplay);
    end
  else begin
    UpdatePercentSpinEdit(seWidth, 1);
    seWidth.Value := 100;
  end;
end;

end.
