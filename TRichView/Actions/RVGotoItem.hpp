﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVGoToItem.pas' rev: 27.00 (Windows)

#ifndef RvgotoitemHPP
#define RvgotoitemHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvgotoitem
{
//-- type declarations -------------------------------------------------------
typedef bool __fastcall (*TCheckItemFunction)(Crvdata::TCustomRVData* RVData, int ItemNo, void * UserData);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool __fastcall GoToNextItemEx(Rvedit::TCustomRichViewEdit* rve, void * UserData, TCheckItemFunction Func, bool IncludeCurrentItem, bool Select = false);
extern DELPHI_PACKAGE bool __fastcall GoToPrevItemEx(Rvedit::TCustomRichViewEdit* rve, void * UserData, TCheckItemFunction Func, bool IncludeCurrentItem, bool Select = false);
extern DELPHI_PACKAGE bool __fastcall GoToNextHyperlink(Rvedit::TCustomRichViewEdit* rve, bool Select = false);
extern DELPHI_PACKAGE bool __fastcall GoToPrevHyperlink(Rvedit::TCustomRichViewEdit* rve, bool Select = false);
extern DELPHI_PACKAGE bool __fastcall GoToNextItem(Rvedit::TCustomRichViewEdit* rve, int *ItemTypes, const int ItemTypes_High, bool IncludeCurrentItem, bool Select = false);
extern DELPHI_PACKAGE bool __fastcall GoToPrevItem(Rvedit::TCustomRichViewEdit* rve, int *ItemTypes, const int ItemTypes_High, bool IncludeCurrentItem, bool Select = false);
}	/* namespace Rvgotoitem */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVGOTOITEM)
using namespace Rvgotoitem;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvgotoitemHPP
