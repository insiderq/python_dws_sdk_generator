
{*******************************************************}
{                                                       }
{       RichViewActions (ThemeEngine version)           }
{       Demo project.                                   }
{       You can use it as a basis for your              }
{       applications.                                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{       ThemeEngine (c) KSDev http://www.ksdev.com      }
{                                                       }
{*******************************************************}

program ActionTest_TE;

uses
  Forms,
  dmActions in 'dmActions.pas' {rvActionsResource: TDataModule},
  Unit1TE in 'Unit1TE.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TrvActionsResource, rvActionsResource);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
