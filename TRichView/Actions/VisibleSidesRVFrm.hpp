﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'VisibleSidesRVFrm.pas' rev: 27.00 (Windows)

#ifndef VisiblesidesrvfrmHPP
#define VisiblesidesrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Visiblesidesrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVVisibleSides;
class PASCALIMPLEMENTATION TfrmRVVisibleSides : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TCheckBox* cbTop;
	Vcl::Stdctrls::TCheckBox* cbLeft;
	Vcl::Stdctrls::TCheckBox* cbRight;
	Vcl::Stdctrls::TCheckBox* cbBottom;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _cbLeft;
	Vcl::Controls::TControl* _cbRight;
	Vcl::Controls::TControl* _cbTop;
	Vcl::Controls::TControl* _cbBottom;
	
public:
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	DYNAMIC void __fastcall Localize(void);
	void __fastcall SetSides(bool Left, bool Top, bool Right, bool Bottom, bool DefLeft, bool DefTop, bool DefRight, bool DefBottom);
	void __fastcall GetSides(bool &Left, bool &Top, bool &Right, bool &Bottom, bool &DefLeft, bool &DefTop, bool &DefRight, bool &DefBottom);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVVisibleSides(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVVisibleSides(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVVisibleSides(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVVisibleSides(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Visiblesidesrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_VISIBLESIDESRVFRM)
using namespace Visiblesidesrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// VisiblesidesrvfrmHPP
