﻿// This file is a copy of RVAL_Lith.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Lithuanian translation                          }
{                                                       }
{       Copyright (c) Marius Žemaitis                   }
{       ze.markos@gmail.com; marius@lvat.lt;            }
{       http://ze.markos.googlepages.com                }
{                                                       }
{*******************************************************}
{ Updated: 2014-02-04                                   }
{*******************************************************}

unit RVALUTF8_Lith;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'coliai','cm', 'mm', 'cicerai', 'pikseliai', 'taškai',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  'col', 'cm', 'mm', 'cc', 'pks', 't',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Failas', '&Redaguoti', 'F&ormatas', 'Šr&iftas', '&Pastraipa', 'Į&terpti', '&Lentelė', 'L&angas', 'Pa&galba',
  // exit
  'Iš&eiti',
  // top-level menus: View, Tools,
  '&Rodyti', 'Įra&nkiai',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Dydis', 'Stilius', 'Pažy&mėti', '&Lygiuoti Ląstelės Turinį', 'Ląstelės Rėmėliai',
  // menus: Table cell rotation
  'Ląstelės &Sukimas',
  // menus: Text flow, Footnotes/endnotes
  '&Teksto Srautas', '&Pastabos/Galinės Išnašos',
  // ribbon tabs: tab1, tab2, view, table
  '&Pradžia', '&Papildomi', '&Vaizdas', '&Lentelė',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Iškarpinė', 'Šriftas', 'Pastraipa', 'Sąrašai', 'Redagavimas',
  // ribbon groups: insert, background, page setup,
  'Įterpimas', 'Pagrindas', 'Lapo nuotatos',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Saitai', 'Išnašos', 'Dokumento vaizdas', 'Rodyti/Slėpti', 'Mąstelis',  'Nustatymai',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Įterpimas', 'Trynimas', 'Operacijos', 'Kraštinės',
  // ribbon groups: styles 
  'Stiliai',
  // ribbon screen tip footer,
  'Spauskite F1 iškviesti pagalbos langą',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Paskutiniai dokumentai', 'Išsaugoti dokumento kopiją', 'Peržiūrėti ir spausdinti dokumentą',
  // ribbon label: units combo
  'Vienetai:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Naujas', 'Naujas|Sukuriamas naujas tuščias dokumentas',
  // TrvActionOpen
  '&Atverti...', 'Atverti|Atveriamas dokumentas iš disko',
  // TrvActionSave
  '&Saugoti', 'Saugoti|Saugojamas dokumentas į diską',
  // TrvActionSaveAs
  'Saugoti &Kaip...', 'Saugoti Kaip...|Saugojamas dokumentas į diską nauju vardu',
  // TrvActionExport
  '&Eksportuoti...', 'Eksportuoti|Eksportuojamas dokumentas kitame formate',
  // TrvActionPrintPreview
  'Spausdinimo &Peržiūra', 'Spausdinimo Peržiūra|Parodoma kaip bus išspausdintas dokumentas',
  // TrvActionPrint
  '&Spausdinti...', 'Spausdinti|Derinamas spausdintuvas ir spausdinamas dokumentas',
  // TrvActionQuickPrint
  '&Spausdinti', 'Spausdinti iš&kart', 'Spausdinti|Spausdinamas dokumentas',
  // TrvActionPageSetup
  '&Lapo Nustatymai...', 'Lapo Nustatymai|Nustatomos ribos, lapo dydis, orientacija, šaltinis, antraštė ir išnašos',
  // TrvActionCut
  'Iš&kirpti', 'Iškirpti|Iškerpama pažymėta dalis ir įsimenama iškarpų atmintinėje',
  // TrvActionCopy
  'Į&siminti', 'Įsiminti|Įsimenama pažymėta dalis ir padedama į iškarpų atmintinę',
  // TrvActionPaste
  '&Terpti', 'Terpti|Į dokumentą įterpiamas iškarpų atmintinės turinys',
  // TrvActionPasteAsText
  'Terpti kaip T&ekstą', 'Terpti kaip tekstą|Įterpiamas tekstas iš iškarpinės',
  // TrvActionPasteSpecial
  'Te&rpti Ypatingai...', 'Terpti Ypatingai|Į dokumentą įterpiamas iškarpų atmintinės turinys nustatytame formate',
  // TrvActionSelectAll
  'Pažymėti &Visą', 'Pažymėti Visą|Pažymimas visas dokumentas',
  // TrvActionUndo
  '&Atšaukti', 'Atšaukti|Ignoruojamas, atšaukiamas paskutinis veiksmas',
  // TrvActionRedo
  '&Pakartoti', 'Pakartoti|Kartojamas paskutinis atšauktas veiksmas',
  // TrvActionFind
  '&Rasti...', 'Rasti|Dokumente ieškoma konkretaus teksto',
  // TrvActionFindNext
  'Rasti &Toliau', 'Rasti Toliau|Tęsiama paskutinė paieška',
  // TrvActionReplace
  'S&ukeisti...', 'Sukeisti|Randamas ir pakeičiamas nurodytas dokumento tekstas',
  // TrvActionInsertFile
  '&Failas...', 'Įterpti failą|Į dokumentą įterpiamas failo turinys',
  // TrvActionInsertPicture
  '&Paveikslas...', 'Įterpti paveikslą|Įterpiamas paveikslas iš disko',
  // TRVActionInsertHLine
  '&Horizontali Linija', 'Įterpti Horizontalią Liniją|Įterpiama horizontali linija',
  // TRVActionInsertHyperlink
  'Į&terpti Saitą...', 'Įterpti Saitą|Įterpiamas saitas (hipertekstinė nuoroda)',
  // TRVActionRemoveHyperlinks
  '&Naikinti Saitus', 'Naikinti Saitus|Panaikinami visi saitai pažymėtame tekste',
  // TrvActionInsertSymbol
  'Įterpti Ž&enklą...', 'Įterpti Ženklą|Įterpiamas ženklas',
  // TrvActionInsertNumber
  'Įterpti &Skaičių...', 'Įterpti Skaičių|Įterpiamas skaičius (skaičiavimas)',
  // TrvActionInsertCaption
  'Įterpti &Antraštę...', 'Įterpti Antraštę|Įterpiama antraštė pasirinktam objektui',
  // TrvActionInsertTextBox
  '&Teksto Laukelis', 'Teksto Laukelis|Įterpiama tekstinė žymė laukelyje',
  // TrvActionInsertSidenote
  'Įterpti Ž&inutę', 'Įterpti Žinutę|Įterpti žinutę teksto laukelyje',
  // TrvActionInsertPageNumber
  '&Puslapio numeris', 'Puslapio numeris|Įterpiamas puslapio numeris',
  // TrvActionParaList
  'Žymėjimas ir N&umeravimas...', 'Žymėjimas ir Numeravimas|Nustatomi arba redaguojami žymėjimas ir numeravimas pažymėtoms pastraipoms',
  // TrvActionParaBullets
  'Žy&mėjimas', 'Žymėjimas|Įdedama arba naikinama pastraipos žymėjimas',
  // TrvActionParaNumbering
  '&Numeravimas', 'Numeravimas|Įdedamas arba naikinamas pastraipos numeravimas',
  // TrvActionColor
  'Pagrindo &Spalva...', 'Pagrindo Spalva|Keičiama dokumento pagrindo spalva',
  // TrvActionFillColor
  'Užpil&do spalva...', 'Užpildo Spalva|Keičiama teksto, pastraipos, ląstelių, lentelės ar pastraipos pagrindo spalva',
  // TrvActionInsertPageBreak
  'Į&terpti Lapo Perskyrimą', 'Įterpti Lapo Perskyrimą|Perskiriamas lapas į du atskirus lapus',
  // TrvActionRemovePageBreak
  'Pa&naikinti Lapo Perskyrimą', 'Panaikitni Lapo Perskyrimą|Panaikinamas dviejų lapų išskyrimas',
  // TrvActionClearLeft
  'Panaikinti Teksto Srautą &Kairėje Pusėje', 'Panaikinti Teksto Srautą Kairėje Pusėje|Šios pastraipos pozicija bus žemiau bet kurio paveiksliuko, esančio kairėje pusėje',
  // TrvActionClearRight
  'Panaikinti Teksto Srautą &Dešinėje Pusėje', 'Panaikinti Teksto Srautą Dešinėje Pusėje|Šios pastraipos pozicija bus žemiau bet kurio paveiksliuko, esančio dešinėje pusėje',
  // TrvActionClearBoth
  'Panaikinti Teksto Srautą &Abiejose Pusėse', 'Panaikinti Teksto Srautą Abiejose Pusėse|Šios pastraipos pozicija bus žemiau bet kurio paveiksliuko, esančio dešinėje ar kairėje pusėje',
  // TrvActionClearNone
  '&Normalus Teksto Srautas', 'Normalus Teksto Srautas|Leidžiama rašyti tekstą aplink dešinėje ir kairėje pusėje esančius paveiksliukus',
  // TrvActionVAlign
  '&Objekto Pozicija...', 'Objekto Pozicija|Keičiama pasirinkto objekto pozicija',
  // TrvActionItemProperties
  'Objekto Sa&vybės...', 'Objekto Savybės|Nustatomos aktyvaus objekto savybės',
  // TrvActionBackground
  'Pa&grindo Fonas...', 'Pagrindo fonas|Parenkama pagrindo spalva ir fono paveikslas',
  // TrvActionParagraph
  '&Pastraipa...', 'Pastraipa|Keičiami pastraipos parametrai',
  // TrvActionIndentInc
  'Pa&didinti Įtrauką', 'Padidinti Įtrauką|Pastraipa traukiama toliau nuo kairio krašto',
  // TrvActionIndentDec
  'Su&mažinti Įtrauką', 'Sumažinti Įtrauką|Pastraipa traukiama arčiau kairio krašto',
  // TrvActionWordWrap
  'Ž&odžių Kėlimas', 'Žodžių Kėlimas|Perjungiama pažymėtų pastraipų žodžių kėlimas',
  // TrvActionAlignLeft
  'Lygiuoti Pagal &Kairę', 'Lygiuoti Pagal Kairę|Lygiuoja pažymėtą tekstą pagal kairį kraštą',
  // TrvActionAlignRight
  'Lygiuoti Pagal &Dešinę', 'Lygiuoti Pagal Dešinę|Lygiuoja pažymėtą tekstą pagal dešinį kraštą',
  // TrvActionAlignCenter
  'Lygiuoti &Centre', 'Lygiuoti Centre|Centruojamas pažymėtas tekstas',
  // TrvActionAlignJustify
  'Lygiuoti &Abiem Kraštais', 'Lygiuoti Abiem Kraštais|Lygiuoja pažymėtą tekstą pagal kairį ir dešinį kraštą kartu',
  // TrvActionParaColor
  'Pastraipos &Pagrindo Spalva...', 'Pastraipos Pagrindo Spalva|Nustatoma pastraipos pagrindo spalva',
  // TrvActionLineSpacing100
  '&Vienos Eilutės Tarpai', 'Vienos Eilutės Tarpai|Tarpai tarp eilučių lygūs vienai eilutei',
  // TrvActionLineSpacing150
  '1.5 Ei&lutės Tarpai', '1.5 Eilutės Tarpai|Tarpai tarp eilučių lygūs 1.5 eilutės',
  // TrvActionLineSpacing200
  'Dvigu&bi Eilučių Tarpai', 'Dvigubi Eilučių Tarpai|Padvigubinami tarpai tarp eilučių',
  // TrvActionParaBorder
  'Pastraipų &Rėmelis ir Pagrindas...', 'Pastraipų Rėmelis ir Pagrindas|Nustatomi rėmeliai ir pagrindas pažymėtoms pastraipoms',
  // TrvActionInsertTable
  'Įterpti &Lentelę...', '&Lentelė', 'Įterpti Lentelę|Įterpiama nauja lentelė',
  // TrvActionTableInsertRowsAbove
  'Įterpti Eilutę &Viršuje', 'Įterpti Eilutę Viršuje|Įterpiama nauja eilutė pažymėtos ląstelės viršuje',
  // TrvActionTableInsertRowsBelow
  'Įterpti Eilutę &Apačioje', 'Įterpti Eilutę Apačioje|Įterpiama nauja eilutė pažymėtos ląstelės apačioje',
  // TrvActionTableInsertColLeft
  'Įterpti Stulpelį &Kairėje', 'Įterpti Stulpelį Kairėje|Įterpiamas naujas stulpelis pažymėtų lastelių kairėje',
  // TrvActionTableInsertColRight
  'Įterpti Stulpelį &Dešinėje', 'Įterpti Stulpelį Dešinėje|Įterpiamas naujas stulpelis pažymėtų ląstelių dešinėje',
  // TrvActionTableDeleteRows
  'Pašalinti &Eilutes', 'Pašalinti Eilutes|Pašalinti iš lentelės eilutes su pažymėtomis ląstelėmis',
  // TrvActionTableDeleteCols
  'Pašalinti St&ulpelius', 'Pašalinti Stulpelius|Pašalinti iš lentelės stulpelius su pažymėtomis ląstelėmis',
  // TrvActionTableDeleteTable
  '&Trinti Lentelę', 'Trinti Lentelę|Pašalinti visą lentelę',
  // TrvActionTableMergeCells
  'Su&jungti Ląsteles', 'Sujungti Ląsteles|Sujungti pažymėtas ląsteles į vieną',
  // TrvActionTableSplitCells
  'Per&skelti Ląsteles...', 'Perskelti Ląsteles|Perskelti pažymėtas ląsteles',
  // TrvActionTableSelectTable
  'Pažymėti &Lentelę', 'Pažymėti Lentelę|Pažymima visa lentelė',
  // TrvActionTableSelectRows
  'Pažymėti &Eilutes', 'Pažymėti Eilutes|Pažymimos lentelės eilutės',
  // TrvActionTableSelectCols
  'Pažymėti S&tulpelius', 'Pažymėti Stulpelius|Pažymimi lentelės stulpeliai',
  // TrvActionTableSelectCell
  'Pažymėti Lą&stelę', 'Pažymėti Ląstelę|Pažymimas ląstelės turinys',
  // TrvActionTableCellVAlignTop
  'Lygiuoti Ląstelę Pagal &Viršų', 'Lygiuoti Ląstelę Pagal Viršų|Lygiuoti ląstelės turinį jos viršuje',
  // TrvActionTableCellVAlignMiddle
  'Lygiuoti Ląstelę Pagal Vi&durį', 'Lygiuoti Ląstelę Pagal Vidurį|Lygiuoti ląstelės turinį jos viduryje',
  // TrvActionTableCellVAlignBottom
  'Lygiuoti Ląstelę Pagal &Apačią', 'Lygiuoti Ląstelę Pagal Apačią|Lygiuoti ląstelės turinį jos apačioje',
  // TrvActionTableCellVAlignDefault
  '&Nustatytas Vertikalus Ląstelių Lygiavimas', 'Nustatytas Vertikalus Ląstelių Lygiavimas|Nustatomas pastovus vertikalus ląstelių lygiavimas',
  // TrvActionTableCellRotationNone
  '&Jokio Sukimo', 'Jokio sukimo|Pasukti ląstelės turinį per 0°',
  // TrvActionTableCellRotation90
  'Pasukti Ląstelę per &90°', 'Pasukti Ląstelę per 90°|Pasukti ląstelės turinį per 90°',
  // TrvActionTableCellRotation180
  'Pasukti Ląstelę per &180°', 'Pasukti Ląstelę per 180°|Pasukti ląstelės turinį per 180°',
  // TrvActionTableCellRotation270
  'Pasukti Ląstelę per &270°', 'Pasukti Ląstelę per 270°|Pasukti ląstelės turinį per 270°',
  // TrvActionTableProperties
  'Lentelės &Savybės...', 'Lentelės Savybės|Keičiamos pažymėtos lentelės savybės ir nustatymai',
  // TrvActionTableGrid
  'Rodyti &Tinklelį', 'Rodyti Tinklelį|Rodomas arba paslepiamas lentelės tinklelis',
  // TRVActionTableSplit
  'Pers&kelti Lentelę', 'Perskelti Lentelę|Lentelė perskeliama į dvi lenteles pradedant nuo pažymėtos eilutės',
  // TRVActionTableToText
  'K&onvertuoti į Tekstą...', 'Konvertuoti į Tekstą|Lentelė konvertuojama į tekstą',
  // TRVActionTableSort
  '&Rikiuoti...', 'Rikiuoti|Rikiuojamos lentelės eilutės',
  // TrvActionTableCellLeftBorder
  'Briauna &Kairėje', 'Briauna Kairėje|Rodoma arba slepiama kairioji ląstelės briauna',
  // TrvActionTableCellRightBorder
  'Briauna &Dešinėje', 'Briauna Dešinėje|Rodoma arba slepiam dešinioji ląstelės briauna',
  // TrvActionTableCellTopBorder
  '&Viršutinė Briauna', 'Viršutinė Briauna|Rodoma arba slepiama ląstelės briauna viršuje',
  // TrvActionTableCellBottomBorder
  '&Apatinė Briauna', 'Apatinė Briauna|Rodoma arba slepiama ląstelės briauna apačioje',
  // TrvActionTableCellAllBorders
  'V&isos Briaunos', 'Visos Briaunas|Rodomos arba slepiamos ląstelės briaunos',
  // TrvActionTableCellNoBorders
  '&Be Briaunų', 'Be Briaunų|Slepiamos visos ląstelės briaunos',
  // TrvActionFonts & TrvActionFontEx
  'Šri&ftas...', 'Šriftas|Keičiamas pažymėto teksto šriftas',
  // TrvActionFontBold
  'Pa&ryškintas', 'Paryškintas|Pažymėto teksto stilius keičiamas į paryškintą (pastorintą)',
  // TrvActionFontItalic
  '&Kursyvas', 'Kursyvas|Pažymėto teksto stilius keičiamas į pakreiptą',
  // TrvActionFontUnderline
  'Pa&brauktas', 'Pabrauktas|Pažymėto teksto stilius keičiamas į pabrauktą',
  // TrvActionFontStrikeout
  '&Perbrauktas', 'Perbrauktas|Perbraukiamas pažymėtas tekstas',
  // TrvActionFontGrow
  '&Didinti Šriftą', 'Didinti Šriftą|Pažymėto teksto raidžių aukštis didinamas 10%',
  // TrvActionFontShrink
  '&Mažinti Šriftą', 'Mažinti Šriftą|Pažymėto teksto raidžių aukštis mažinamas 10%',
  // TrvActionFontGrowOnePoint
  'D&idinti šriftą vienu punktu', 'Didinti šriftą vienu punktu|Pažymėto teksto raidžių aukštis didinamas vienu punktu',
  // TrvActionFontShrinkOnePoint
  'M&ažinti šriftą vienu punktu', 'Mažinti šriftą vienu punktu|Pažymėto teksto raidžių aukštis mažinamas vienu punktu',
  // TrvActionFontAllCaps
  '&Visos Didžiosios', 'Visos Didžiosios|Pažymėto teksto raidės keičiamos į didžiąsias',
  // TrvActionFontOverline
  '&Užbraukimas', 'Užbraukimas|Pažymėto teksto viršuje piešiama linija',
  // TrvActionFontColor
  '&Teksto Spalva...', 'Teksto Spalva|Keičiama pažymėto teksto spalva',
  // TrvActionFontBackColor
  'Teksto Pa&grindo Spalva...', 'Teksto Pagrindo Spalva|Keičiama pažymėto teksto pagrindo spalva',
  // TrvActionAddictSpell3
  'Patikrinti &Rašybą', 'Patikrinti Rašybą|Tikrinama rašyba tekste',
  // TrvActionAddictThesaurus3
  '&Sinonimų Žodynas', 'Sinonimų Žodynas|Pasiūlomi sinonimai pažymėtame tekste',
  // TrvActionParaLTR
  'Iš Kairės į Dešinę', 'Iš Kairės į Dešinę|Pažymėtų pastraipų tekstui nustatoma Kairės-Dešinės kryptis',
  // TrvActionParaRTL
  'Iš Dešinės į Kairę', 'Iš Dešinės į Kairę|Pažymėtų pastraipų tekstui nustatoma Dešinės-Kairės kryptis',
  // TrvActionLTR
  'Iš Kairės į Dešinę Tekste', 'Tekstas iš Kairės į Dešinę|Pažymėtam tekstui nustatoma Kairės-Dešinės kryptis',
  // TrvActionRTL
  'Iš Dešinės į Kairę Tekste', 'Tekstas iš Dešinės į Kairę|Pažymėtam tekstui nustatoma Dešinės-Kairės kryptis',
  // TrvActionCharCase
  'Rašyba ABC/abc/Abc', 'Keisti Rašybą ABC/abc/Abc|Keičiama pažymėto teksto simbolių rašyba ABC/abc/Abc',
  // TrvActionShowSpecialCharacters
  '&Nespausdintini Simboliai', 'Nespausdintini Simboliai|Rodomi arba slepiami nespausdintini simboliai, tokie kaip pastraipų žymės, tabuliatoriai ir tarpai',
  // TrvActionSubscript
  'Ap&atinis indeksas', 'Indeksas|Pažymėtas tekstas keičiamas į apatinį indeksą',
  // TrvActionSuperscript
  '&Viršutinis indeksas', 'Indeksas|Pažymėtas tekstas keičiamas į viršutinį indeksą',
  // TrvActionInsertFootnote
  '&Pastabos', 'Pastabos|Įrašyti pastabą',
  // TrvActionInsertEndnote
  'Galinės &Išnašos', 'Galinės Išnašos|Įrašyti galines išnašas',
  // TrvActionEditNote
  'Re&daguoti Pastabas', 'Redaguoti Pastabas|Keičiamos pastabos ir išnašos',
  '&Slėpti', 'Slėpti|Slepiamas ar rodomas pasirinktas fragmentas',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stiliai...', 'Stiliai|Atveriamas stilių valdymo langas',
  // TrvActionAddStyleTemplate
  'Pri&dėti Stilių...', 'Pridėti Stilių|Sukuriamas naujas pastraipos ir teksto stilius',
  // TrvActionClearFormat,
  'Iš&valyti Formatavimą', 'Išvalyti Formatavimą|Išvalomi visi teksto ir pastraipos formatavimai pažymėtame fragmente',
  // TrvActionClearTextFormat,
  'Išvalyti &Teksto Formatavimą', 'Išvalyti Teksto Formatavimą|Išvalomi visi formatavimai iš pažymėto teksto',
  // TrvActionStyleInspector
  'Stiliaus T&ikrintojas', 'Stiliaus Tikrintojas|Rodomas ar slepiamas Stiliaus Tikrintojas',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Gerai', 'Atšaukti', 'Uždaryti', 'Įterpti', '&Atverti...', '&Saugoti...', '&Valyti', '&Pagalba', 'Pa&naikinti',
  // Others  -------------------------------------------------------------------
  // percents
  'procentai',
  // left, top, right, bottom sides
  'Kairė Pusė', 'Viršutinė Pusė', 'Dešinė Pusė', 'Apatinė Pusė',
  // save changes? confirm title
  'Išsaugoti keitimus į %s?', 'Patvirtinti',
  // warning: losing formatting
  '%s gali turėti savybių kurios yra nesuderinamos su pasirinktu formatu.'#13+
  'Ar jūs norite išsaugoti dokumentą šiame formate?',
  // RVF format name
  'RichView Formatas',
  // Error messages ------------------------------------------------------------
  'Klaida',
  'Klaida atveriant failą.'#13#13'Galimos priežastys:'#13'- failo formatas yra nesuprantamas šiai programai;'#13+
  '- failas yra sugadintas;'#13'- failas jau yra atvertas ir užrakintas kitoje programoje.',
  'Klaida atveriant paveikslo failą.'#13#13'Galimos priežastys:'#13'- paveikslo failo formatas yra nesuprantamas šiai programai;'#13+
  '- faile nėra paveikslo;'#13+
  '- failas yra sugadintas;'#13'- failas jau yra atvertas ir užrakintas kitoje programoje.',
  'Klaida saugant failą.'#13#13'Galimos priežastys:'#13'- nėra vietos diske;'#13+
  '- diskas yra tik skaitymui;'#13'- nėra įdėtos informacijos laikmenos;'#13+
  '- failas jau yra atvertas ir užrakintas kitoje programoje;'#13'- informacijos laikmena yra sugadinta.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Failai (*.rvf)|*.rvf',  'RTF Failai (*.rtf)|*.rtf' , 'XML Failai (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Teksto Failai (*.txt)|*.txt', 'Teksto Failai - Unicode (*.txt)|*.txt', 'Teksto Failai - Automatinis nustatymas (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Failai (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Supaprastintas (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dokumentai (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Paieška Baigta', 'Ieškoma eilutė ''%s'' nerasta.',
  // 1 string replaced; Several strings replaced
  '1 eilutė sukeista.', '%d eilučių sukeista.',
  // continue search from the beginning/end?
  'Dokumento pabaiga pasiekta, tęsti nuo dokumento pradžios?',
  'Dokumento pradžia pasiekta, tęsti nuo dokumento pabaigos?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Permatoma', 'Nustatytoji',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Juoda', 'Ruda', 'Rusvai žalia', 'Tamsiai Žalia', 'Tamsiai Žalsva-Mėlyna', 'Tamsiai Mėlyna', 'Indigo', 'Pilka-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tamsiai Raudona', 'Oranžinė', 'Tamsiai Geltona', 'Žalia', 'Žalsva-Mėlyna', 'Mėlyna', 'Pilkai-Mėlyna', 'Pilka-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Raudona', 'Lengvai Oranžinė', 'Gelsva', 'Jūrinė Žalia', 'Akva', 'Lengvai Mėlyna', 'Violetinė', 'Pilka-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rožinė', 'Auksinė', 'Geltona', 'Šviesiai Žalia', 'Turkio', 'Dangaus', 'Tamsiai Violetinė', 'Pilka-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rožių', 'Gelsvai Ruda', 'Šviesiai Geltona', 'Lengvai Žalia', 'Lengvai Melsva', 'Blyškiai Mėlyna', 'Šviesiai Violetinė', 'Balta',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Per&matoma', '&Automatinė', '&Daugiau Spalvų...', '&Nustatyta',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Pagrindo fonas', '&Spalva:', 'Pozicija', 'Pagrindo vaizdas', 'Pavyzdys.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Jokia', 'Iš&plėsta', '&Fiksuota vieta', '&Išklotine', '&Centre',
  // Padding button
  '&Poslinkis...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Užpildo Spalva', 'Kam &aktyvuoti:', '&Daugiau Spalvų...', '&Poslinkis...',
  'Pasirinkite spalvą',
  // [apply to:] text, paragraph, table, cell
  'tekstui', 'pastraipai' , 'lentelei', 'ląstelei',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Šriftas', 'Šriftas', 'Išdėstymas',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Š&riftas:', '&Dydis', '&Stilius', 'St&orintas', '&Kursyvas',
  // Script, Color, Back color labels, Default charset
  '&Indeksas:', '&Spalva:', '&Fonas:', '(Nustatytas)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efektai', '&Pabraukimas', '&Užbrauktas', 'Per&brauktas', '&Visos Didžiosios',
  // Sample, Sample text
  'Pavyzdys', 'Pavyzdinis tekstas',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Tarpai tarp raidžių', '&Tarpai:', '&Išplėstas', '&Sutrauktas',
  // Offset group-box, Offset label, Down, Up,
  'Poslinkis', '&Poslinkis:', 'Ž&emyn', '&Viršun',
  // Scaling group-box, Scaling
  'Mąstelis', 'P&roporcijos:',
  // Sub/super script group box, Normal, Sub, Super
  'Apatinis ir viršutinis indeksai', '&Normalus', 'Ap&atinis', 'V&iršutinis',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Poslinkis', '&Viršus:', '&Kairė:', '&Apačia:', '&Dešinė:', 'V&ienodi Dydžiai',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Įterpti Hipernuorodą', 'Hipernuoroda', 'Tekstas:', 'Nuoroda:', '<<pasirinkimas>>',
  // cannot open URL
  'Negalime susisiekti su "%s"',
  // hyperlink properties button, hyperlink style
  '&Derinti...', '&Stilius:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ńolors
  'Hipernuorodos Savybės', 'Normalios spalvos', 'Aktyvios spalvos',
  // Text [color], Background [color]
  '&Teksto:', '&Fono',
  // Underline [color]
  'Pa&braukimas:',
  // Text [color], Background [color] (different hotkeys)
  'T&eksto:', 'F&ono',
  // Underline [color], Attributes group-box,
  'Pa&braukimas:', 'Požymiai',
  // Underline type: always, never, active (below the mouse)
  '&Pabraukimas:', 'visada', 'niekada', 'aktyvintas',
  // Same as normal check-box
  'Tokia kaip &normali',
  // underline active links check-box
  'Pa&braukti aktyvias nuorodas',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Įterpti ženklą', 'Š&riftas:', 'Ž&enklų sąrašas:', 'Unicode',
  // Unicode block
  '&Blokas:',
  // Character Code, Character Unicode Code, No Character
  'Ženklo kodas: %d', 'Ženklo kodas: Unicode %d', '(jokio ženklo)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Įterpti Lentelę', 'Lentelės dydis', '&Stulpelių skaičius:', '&Eilučių skaičius:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Lentelės planas', '&Autoplėtimasis', 'Sutapdinti su &lango dydžiu', 'Nustatyti matmenis',
  // Remember check-box
  'Įsiminti &matmenis naujoms lentelėms',
  // VAlign Form ---------------------------------------------------------------
  'Pozicija',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Savybės', 'Paveikslas', 'Vieta ir Dydis', 'Linija', 'Lentelė', 'Eilutės', 'Ląstelės',
  // Image Appearance, Image Misc, Number tabs
  'Išvaizda', 'Įvairūs', 'Skaičius',
  // Box position, Box size, Box appearance tabs
  'Vieta', 'Dydis', 'Išvaizda',
  // Preview label, Transparency group-box, checkbox, label
  'Peržiūra:', 'Permatomumas', '&Permatoma', 'P&ermatoma spalva:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Pa&keisti...', 'Iš&saugoti...', 'Autom.',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikalus lygiavimas', 'L&ygiuoti:',
  'apačioje pagal teksto liniją', 'viduryje pagal teksto liniją',
  'viršuje pagal eilutės viršų', 'apačioje pagal eilutės apačią', 'viduryje pagal eilutės vidurį',
  // align to left side, align to right side
  'kairėje pusėje', 'dešinėje pusėje',
  // Shift By label, Stretch group box, Width, Height, Default size
  'P&astumti per:', 'Ištempti', 'Pl&otis:', '&Aukštis:', 'Nustatytas dydis: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Išdidinti &proporcingai', 'P&radinė būsena',
  // Inside the border, border, outside the border groupboxes
  'Viduje kraštų', 'Rėmelis', 'Išorėje kraštų',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Užpildo spalva:', '&Poslinkis:',
  // Border width, Border color labels
  'Rėmelio &storis:', 'Rėmelio s&palva:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontalus tarpelis:', '&Vertikalus tarpelis:',
  // Miscellaneous groupbox, Tooltip label
  'Įvairūs', '&Patarimas:',
  // web group-box, alt text
  'Tinklas', 'Pagalbinis &tekstas:',
  // Horz line group-box, color, width, style
  'Horizontali linija', '&Spalva:', '&Plotis:', 'S&tilius:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Lentelė', '&Plotis:', 'Užpil&do spalva:', '&Tarpai tarp ląstelių...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizontalus ląstelės poslinkis:', '&Vertikalus ląstelės poslinkis:', 'Kraštinės ir fonas',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Matomos &kraštinės...', 'Autom.', 'autom.',
  // Keep row content together checkbox
  'Iš&laikyti turinį vientisą',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Pasukimas', '&Jokio', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Kraštinė:', 'Matomos &kraštinės...',
  // Border, CellBorders buttons
  '&Lentelės Rėmelis...', 'Ląstelių &Briaunos...',
  // Table border, Cell borders dialog titles
  'Lentelės Rėmelis', 'Nustatytos Ląstelių Briaunos',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Spausdinimas', 'Talpinti Lentelę Viename La&pe', '&Antraštinių Eilučių skaičius',
  'Antraštinės eilutės kartojamos kiekviename lentelės lape',
  // top, center, bottom, default
  '&Viršus', '&Centras', '&Apačia', '&Nustatyta',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Nustatymai', 'Pageidaujamas &plotis:', '&Aukštis mažiausiai:', 'Užpil&do spalva:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Briauna', 'Matomos briaunos:',  'Šešėlio &spalva:', '&Lengva spalva', '&Spalva:',
  // Background image
  '&Fono Paveikslas...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontali padėtis', 'Vertikali padėtis', 'Vieta tekste',
  // Position types: align, absolute, relative
  'Lygiuota', 'Absoliuti padėtis', 'Santykinė padėtis',
  // [Align] left side, center, right side
  'kairėje lauko pusėje kairėje',
  'centruota lauke centre',
  'dešinėje lauko pusėje dešinėje',
  // [Align] top side, center, bottom side
  'viršuje lauko viršuje',
  'centruota lauke centre',
  'apačioje lauko apačioje',
  // [Align] relative to
  'santykinai su',
  // [Position] to the right of the left side of
  'iš dešinės kairėje krašto pusėje',
  // [Position] below the top side of
  'žemiau viršutinio krašto',
  // Anchors: page, main text area (x2)
  '&Lapas', '&Pagrindinė teksto sritis', 'L&apas', 'Pagrindinė te&ksto sritis',
  // Anchors: left margin, right margin
  '&Kairinė riba', '&Dešinioji riba',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Viršutinė riba', '&Apatinė riba', 'V&idinė riba', 'Iš&orinė riba',
  // Anchors: character, line, paragraph
  'Ž&enklas', 'L&inija', 'Pa&straipa',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Veid&rodinės ribos', 'Iš&dėstymas lentelės ląstelėje',
  // Above text, below text
  '&Viršuje teksto', '&Apačioje teksto',
  // Width, Height groupboxes, Width, Height labels
  'Plotis', 'Aukšitis', '&Plotis:', '&Aukštis:',
  // Auto height label, Auto height combobox item
  'Autom.', 'autom.',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikalus lygiavimas', '&Viršus', '&Centras', '&Apačia',
  // Border and background button and title
  '&Rėmelis ir splava...', 'Lauko Rėmelis ir Spalva',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Terpti ypatingai', '&Įterpti kaip:', '"Rich text" formatas', 'HTML formatas',
  'Tekstas', 'Unicode tekstas', '"Bitmap" paveikslas', '"Metafile" paveikslas',
  'Grafiniai failai', 'Nuoroda',
  // Options group-box, styles
  'Nustatymai', '&Stiliai:',
  // style options: apply target styles, use source styles, ignore styles
  'pritaikyti nurodyto dokumento stilius', 'išlaikyti stilius ir išvaizdą',
  'išlaikyti išvaizdą, ignoruoti stilius',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Žymėjimas ir Numeravimas', 'Žymėjimas', 'Numeravimas', 'Žymėjimų sąrašai', 'Numeruoti sąrašai',
  // Customize, Reset, None
  '&Prisitaikyti...', '&Naujai', 'Joks',
  // Numbering: continue, reset to, create new
  'tęsti numeravimą', 'nustatyti naują numeravimą į', 'sukurti naują sąrašą, pradedant nuo',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Didžiosios Raidės', 'Didžiosios Romėniškos', 'Dešimtainis', 'Mažosios Raidės', 'Mažosios Romėniškos',
  // Bullet type
  'Aspkritimai', 'Ratukai', 'Kvadratukai',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Lygis:', '&Pradėti nuo:', '&Tęsti', 'Numeravimas',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Keisti sąrašo žymes', 'Lygiai', '&Skaičius:', 'Sąrašo savybės', '&Sąrašo tipas:',
  // List types: bullet, image
  'žymėjimas', 'paveikslas', 'Įterpti Skaičių|',
  // Number format, Number, Start level from, Font button
  '&Skaičių formatas:', 'Skaičius', '&Pradėti lygio numeravimą nuo:', '&Šriftas...',
  // Image button, bullet character, Bullet button,
  '&Paveikslas...', 'Žymės ž&enklas', 'Ž&ymėjimas...',
  // Position of list text, bullet, number, image, text
  'Teksto sąraše pozicija', 'Žymėjimo pozicija', 'Skaičiaus pozicija', 'Paveikslo pozicija', 'Teksto pozicija',
  // at, left indent, first line indent, from left indent
  '&nuo:', '&Kairioji įtrauka:', '&Pirmos eilutės įtrauka:', 'nuo kairiosios įtraukos',
  // [only] one level preview, preview
  '&Vieno lygio peržiūra', 'Peržiūra',
  // Preview text
  'Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'lygiuoti kairėje', 'lygiuoti dešinėje', 'centre',
  // level #, this level (for combo-box)
  'Lygis %d', 'Šis lygis',
  // Marker character dialog title
  'Keisti Žymėjimo Ženklą',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Pastraipos Rėmelis ir Pagrindas', 'Rėmelis', 'Pagrindas',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Nustatymai', '&Spalva:', '&Plotis:', 'Vidinis p&lotis:', 'P&oslinkis...',
  // Sample, Border type group-boxes;
  'Pavyzdys', 'Rėmelio tipas',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Jokio', '&Viengubas', '&Dvigubas', '&Trigubas', 'Storas vid&uje', 'Storas &išorėje',
  // Fill color group-box; More colors, padding buttons
  'Užpildo spalva', '&Daugiau spalvų...', '&Poslinkis...',
  // preview text
  'Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text.',
  // title and group-box in Offsets dialog
  'Poslinkis', 'Rėmelio poslinkis',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Pastraipa', 'Lygiavimas', '&Kairėje', '&Dešinėje', '&Centre', '&Kraštuose',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Tarpai', 'P&rieš:', 'P&o:', 'Eilučių &tarpai:', 'per:',
  // Indents group-box; indents: left, right, first line
  'Įtraukos', 'K&airioji:', 'D&ešinioji:', '&Pirmos eilutės:',
  // indented, hanging
  'Įtra&ukta', 'Ka&banti', 'Pavyzdys',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Vienos eilutės', '1.5 eilutės', 'Dviejų eilučių', 'Bent', 'Tiksliai', 'Didesnis',
  // preview text
  'Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Įtraukos ir išdėstymas', 'Tabuliatoriai', 'Teksto srautas',
  // tab stop position label; buttons: set, delete, delete all
  '&Tabuliatoriaus sustojimai:', '&Nustatyti', '&Naikinti', 'Panaikinti &Visus',
  // tab align group; left, right, center aligns,
  'Lygiavimas', '&Kairėje', '&Dešinėje', '&Centre',
  // leader radio-group; no leader
  'Punktyras', '(jokio)',
  // tab stops to be deleted, delete none, delete all labels
  'Naikinami tabuliatoriai:', '(jokio)', 'Visi.',
  // Pagination group-box, keep with next, keep lines together
  'Puslapių numeracija', '&Kartu su tekstu', '&Išlaikyti eilutes kartu',
  // Outline level, Body text, Level #
  '&Bendras lygis:', 'Pirminis Tekstas', 'Lygis %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Spausdinimo Peržiūra', 'Lapo plotis', 'Visas lapas', 'Lapai:',
  // Invalid Scale, [page #] of #
  'Įveskite skičių nuo 10 iki 500', 'iš %d',
  // Hints on buttons: first, prior, next, last pages
  'Pirmas Lapas', 'Ankstesnis Lapas', 'Sekantis Lapas', 'Paskutinis Lapas',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Tarpai tarp ląstelių', 'Tarpai', '&Tarp ląstelių', 'Nuo lentelės rėmelio iki ląstelių',
  // vertical, horizontal (x2)
  '&Vertikaliai:', '&Horizontaliai:', 'V&ertikaliai:', 'H&orizontaliai:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Rėmeliai', 'Nustatymai', '&Spalva:', '&Šviesi spalva:', 'Šešėlio &spalva:',
  // Width; Border type group-box;
  '&Plotis:', 'Rėmelio tipas',
  // Border types: none, sunken, raised, flat
  '&Joks', 'Įd&ubęs', '&Iškilęs', '&Plokščias',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Perskelti', 'Perskelti į',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&nurodytą kiekį eilučių ir stulpelių', '&Originalias ląsteles',
  // number of columns, rows, merge before
  '&Stulpelių skaičius:', '&Eilučių skaičius:', '&Sujungti prieš perskeliant',
  // to original cols, rows check-boxes
  'Perskelti į originalius s&tulpelius', 'Perskelti į originalias ei&lutes',
  // Add Rows form -------------------------------------------------------------
  'Įterpti Eilutes', 'Eilučių &skaičius:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Lapo nustatymai', 'Lapas', 'Antraštė ir Išnašos',
  // margins group-box, left, right, top, bottom
  'Ribos (millimetrais)', 'Ribos (coliais)', '&Kairė:', '&Viršus:', '&Dešinė:', '&Apačia:',
  // mirror margins check-box
  '&Veidrodinės ribos',
  // orientation group-box, portrait, landscape
  'Orientacija', '&Portretas', 'Pei&zažas',
  // paper group-box, paper size, default paper source
  'Popierius', '&Dydis:', 'Šalt&inis:',
  // header group-box, print on the first page, font button
  'Antraštė', '&Tekstas:', '&Antraštė pirmame lape', 'Š&riftas...',
  // header alignments: left, center, right
  '&Kairėje', '&Centre', '&Dešinėje',
  // the same for footer (different hotkeys)
  'Išnašos', 'Te&kstas:', 'Išnašos pirmame &lape', 'Šri&ftas...',
  'K&airėje', 'Ce&ntre', 'D&ešinėje',
  // page numbers group-box, start from
  'Lapų numeravimas', '&Pradėti nuo:',
  // hint about codes
  'Ypatingos ženklų kombinacijos:'#13'&&p - lapo numeris; &&P - lapų skaičius; &&d - dabartinė data; &&t - dabartinis laikas.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Tekstinio Failo Koduotė', '&Pasirinkite failo kodavimą:',
  // thai, japanese, chinese (simpl), korean
  'Tai', 'Japonų', 'Kinų (Supaprastintas)', 'Korėjiečių',
  // chinese (trad), central european, cyrillic, west european
  'Kinų (Tradicinis)', 'Centrinės ir Rytų Europos', 'Kirilica', 'Vakarų Europos',
  // greek, turkish, hebrew, arabic
  'Graikų', 'Turkų', 'Hebrajų', 'Arabų',
  // baltic, vietnamese, utf-8, utf-16
  'Baltų', 'Vietnamiečių', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vaizdinis Stilius', '&Pasirinkite stilių:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Konvertuoti į Tekstą', 'Pasirinkite &skyriklį:',
  // line break, tab, ';', ','
  'nauja eilutė', 'tabuliacija', 'kabliataškis', 'kablelis',
  // Table sort form -----------------------------------------------------------
  // error message
  'Lentelė turinti sujungtas eilutes negali būti surikiuota',
  // title, main options groupbox
  'Lentelės Rikiavimas', 'Rikiavimas',
  // sort by column, case sensitive
  '&Rikiavimas pagal stulpelį:', '&Didžiąsias ir mažąsias raides skiriantis',
  // heading row, range or rows
  'Nerikiuoti &viršutinės eilutės', 'Rikiuoti eilutes: nuo %d iki %d',
  // order, ascending, descending
  'Tvarka', '&Didėjanti', '&Mažėjanti',
  // data type, text, number
  'Tipas', '&Tekstas', '&Skaičius',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Įterpti Skaičių', 'Savybės', '&Skaitliuko vardas:', '&Numeravimo tipas:',
  // numbering groupbox, continue, start from
  'Numeravimas', '&Tęsti', '&Pradėti nuo:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Antraštė', '&Etiketė:', '&Neįtraukti etiketės į antraštę',
  // position radiogroup
  'Vieta', '&Virš pasirinkto objekto', 'Ž&emiau pasirinkto objekto',
  // caption text
  'Antraštės &tekstas:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numeravimas', 'Diagrama', 'Lentelė',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Matomos rėmelio kraštinės', 'Rėmelis',
  // Left, Top, Right, Bottom checkboxes
  '&Kairė kraštinė', '&Viršutinė kraštinė', '&Dėšinė kraštinė', '&Apatinė kraštinė',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Keisti Lentelės Stulpelio Dydį(ilgį)', 'Keisti Lentelės Eilutės Dydį(aukštį)',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Pirmos Eilutės Įtrauka', 'Kairioji Įtrauka', 'Kabanti Įtrauka', 'Dešinioji Įtrauka',
  // Hints on lists: up one level (left), down one level (right)
  'Vienu lygiu aukščiau', 'Vienu lygiu žemiau',
  // Hints for margins: bottom, left, right and top
  'Apatinė riba', 'Kairioji riba', 'Dešinioji riba', 'Viršutinė riba',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Kairiojo Lygiavimo Tabuliatorius', 'Dešinio Lygiavimo Tabuliatorius', 'Vidurio (Centre) Lygiavimo Tabuliatorius', '(De)Šimtųjų Lygiavimo Tabuliatorius',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normalus', 'Normalus Įtrauktas', 'Jokių tarpų', 'Antraštė %d', 'Sąrašo Pastraipa',
  // Hyperlink, Title, Subtitle
  'Nuoroda', 'Pavadinimas', 'Paantraštė',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Išryškinimas', 'Subtilus Išryškinimas', 'Intensyvus Išryškinimas', 'Stiprus',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citata', 'Ryški Citata', 'Subtilus Paminėjimas', 'Ryškus Paminėjimas',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blokinis Tekstas', 'HTML Kintamasis', 'HTML Kodas', 'HTML Akronimas',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Apibrėžimas', 'HTML Klaviatūra', 'HTML Pavyzdys', 'HTML Rašomoji-mašinėlė',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatuotas', 'HTML Citavimas', 'Antraštė', 'Apačia', 'Puslapio Numeris',
  // Caption
  'Antraštė',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Išnašų Nuorodos', 'Pastabų Nuorodos', 'Išnašų Tekstas', 'Pastabų Tekstas',
  // Sidenote Reference, Sidenote Text
  'Žinutės Nuoroda', 'Žinutės Tekstas',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'spalva', 'fono spalva', 'permatoma', 'numatytoji', 'pabraukimo spalva',
  // default background color, default text color, [color] same as text
  'numatytoji fono spalva', 'numatytoji teksto spalva', 'tokia pati kaip teksto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'viengubas', 'storas', 'dvigubas', 'taškuotas', 'storas taškuotas', 'brūkšniuotas',
  // underline types: thick dashed, long dashed, thick long dashed,
  'storas brūkšniuotas', 'ilgi brūkšniai', 'stori ilgi brūkšniai',
  // underline types: dash dotted, thick dash dotted,
  'brūkšnys-taškas', 'storas brūkšnys-taškas',
  // underline types: dash dot dotted, thick dash dot dotted
  'brūkšnys-taškas taškuotas', 'storas brūkšnys-taškas taškuotas',
  // sub/superscript: not, subsript, superscript
  'be apat./virš. indekso', 'apatinis indeksas', 'viršutinis indeksas',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di būdas:', 'paveldėtas', 'iš kairės į dešinę', 'iš dešinės į kairę',
  // bold, not bold
  'storintas', 'nestorintas',
  // italic, not italic
  'kursyvas', 'nebe kursyvas',
  // underlined, not underlined, default underline
  'pabrauktas', 'nepabrauktas', 'numatytasis pabraukimas',
  // struck out, not struck out
  'perbrauktas', 'neperbrauktas',
  // overlined, not overlined
  'užbrauktas', 'neužbrauktas',
  // all capitals: yes, all capitals: no
  'visos didžiosios', 'nebe visos didžiosios',
  // vertical shift: none, by x% up, by x% down
  'be vertikalaus poslinkio', 'paslinkta per %d%% viršun', 'paslinkta per %d%% apačion',
  // characters width [: x%]
  'simbolių plotis',
  // character spacing: none, expanded by x, condensed by x
  'normalūs simbolių tarpai', 'tarpai praplėsti per %s', 'tarpai sutraukti per %s',
  // charset
  'skriptas',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'numatytasis šriftas', 'numatytoji pastraipa', 'paveldėta',
  // [hyperlink] highlight, default hyperlink attributes
  'pažymėta', 'numatyta',
  // paragraph aligmnment: left, right, center, justify
  'kairysis lygiavimas', 'dešinysis lygiavimas', 'centruota', 'abipusė lygiuotė',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'eilutės aukštis: %d%%', 'tarpas tarp eilučių: %s', 'eilutės aukštis: bent %s',
  'eilutės aukštės: tiksliai %s',
  // no wrap, wrap
  'eilučių kėlimas uždrastas', 'eilučių kėlimas',
  // keep lines together: yes, no
  'išlaikyti eilutes kartu', 'nelaikyti eilučių kartu',
  // keep with next: yes, no
  'išlaikyti su sekančia pastraipa', 'nelaikyti su sekančia pastraipa',
  // read only: yes, no
  'tik skaitymas', 'redaguojamas',
  // body text, heading level x
  'bendras lygis: pirminis tekstas', 'lygis: %d',
  // indents: first line, left, right
  'pirmos eilutės įtrauka', 'kairinė įtrauka', 'dešininė įtrauka',
  // space before, after
  'tarpas prieš', 'tarpas po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabuliatoriai', 'jokių', 'vieta', 'kairė', 'dešinė', 'centras',
  // tab leader (filling character)
  'skirtukas',
  // no, yes
  'ne', 'taip',
  // [padding/spacing/side:] left, top, right, bottom
  'kairė', 'viršus', 'dešinė', 'apačia',
  // border: none, single, double, triple, thick inside, thick outside
  'jokio', 'viengubas', 'dvigubas', 'trigubas', 'storas viduje', 'storas išorėje',
  // background, border, padding, spacing [between text and border]
  'fonas', 'kraštas', 'atitraukimas', 'tarpas',
  // border: style, width, internal width, visible sides
  'stilius', 'plotis', 'vidinis plotis', 'matomi kraštai',
  // style inspector -----------------------------------------------------------
  // title
  'Styliaus Tikrintojas',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stilius', '(Jokio)', 'Pastraipa', 'Šriftas', 'Požymiai',
  // border and background, hyperlink, standard [style]
  'Kraštai ir fonas', 'Nuoroda', '(Standartinis)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stiliai', 'Stilius',
  // name, applicable to,
  '&Vardas:', 'Pri&taikomas:',
  // based on, based on (no &),
  '&Paremtas:', 'Paremtas:',
  //  next style, next style (no &)
  'Se&kančios pastraipos stilius:', 'Sekančios pastraipos stilius:',
  // quick access check-box, description and preview
  '&Greitoji prieiga', 'Aprašy&mas ir peržiūra:',
  // [applicable to:] paragraph and text, paragraph, text
  'pastraipa ir tekstas', 'pastraipa', 'tekstas',
  // text and paragraph styles, default font
  'Teksto ir Pastraipos Stiliai', 'Numatytasis Šriftas',
  // links: edit, reset, buttons: add, delete
  'Redaguoti', 'Naujai nustatyti', 'Pri&dėti', 'Iš&trinti',
  // add standard style, add custom style, default style name
  'Pridėti &Standartinį Stilių...', 'Pridėti &Individulų Stilių', 'Stilius %d',
  // choose style
  'Pasirinkti &stilių:',
  // import, export,
  '&Importuoti...', '&Eksportuoti...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Stiliai (*.rvst)|*.rvst', 'Klaida užkraunant failą', 'Šiame faile nėra jokių stilių',
  // Title, group-box, import styles
  'Importuoti Stilius iš Failo', 'Importuoti', 'I&mportuoti stilius:',
  // existing styles radio-group: Title, override, auto-rename
  'Esantys stiliai', '&perrašyti', 'pri&dėti pervardintą',
  // Select, Unselect, Invert,
  'Paž&ymėti', 'A&tžymėti', '&Invertuoti',
  // select/unselect: all styles, new styles, existing styles
  '&Visi Stiliai', '&Naujas Stilius', '&Esantys Stiliai',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Valyti Formatavimą', 'Visi Stiliai',
  // dialog title, prompt
  'Stiliai', '&Pasirinktas stilius taikymui:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Sinonimai', '&Ignoruoti Visus', 'Į&rašyti į Žodyną',
  // Progress messages ---------------------------------------------------------
  'Siunčiama %s', 'Ruošiamasi spausdinti...', 'Spausdinamas lapas %d',
  'Konvertuojama iš RTF...',  'Konvertuojama į RTF...',
  'Skaitoma %s...', 'Rašoma %s...', 'tekstas',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Jokių pastabų', 'Pastaba', 'Išnaša', 'Žinutė', 'Teksto laukelis'
  );


initialization
  RVA_RegisterLanguage(
    'Lithuanian', // english language name, do not translate
    'Lietuvių',   // native language name
    BALTIC_CHARSET, @Messages);

end.
