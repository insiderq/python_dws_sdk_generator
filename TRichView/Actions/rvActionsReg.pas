
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Design-time support for RichViewActions         }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit rvActionsReg;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Classes,
  Windows,
  SysUtils,
  Messages,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  Menus,
  StdCtrls,
  ExtCtrls,
  ActnList,
  Registry,
  RichViewActions,
  {$IFDEF USEGLYFX}
  dmActionsGlyFX,
  {$ELSE}{$IFDEF USEGLYFXALPHA}
  dmActionsGlyFXAlpha,
  {$ELSE}{$IFDEF USEGLYFXAERO}
  dmActionsGlyFXAero,
  {$ELSE}{$IFDEF USEGLYFXAEROALPHA}
  dmActionsGlyFXAeroAlpha,
  {$ELSE}{$IFDEF USEFUGUEICONS}
  dmActionsFugue,
  {$ELSE}
  dmActions,
  {$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}
  {$IFDEF USERVADDICT3}
  dmActionsAddict3,
  {$ENDIF}
  {$IFDEF RVAUSEHTMLLIST}
  dmActionsEx, RVAHTMLList,
  {$ENDIF}
  {$IFDEF RICHVIEWDEF6}
  DesignEditors, DesignIntf,
  {$ELSE}
  DsgnIntf,
  {$ENDIF}
  {$IFDEF RICHVIEWDEF2006}
  RVAPopupActionBar,
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE3}
  Actions,
  {$ENDIF}
  RVADsgn
  ;

procedure Register;

implementation
                                                   
procedure SaveVersion;
var reg: TRegistry;
begin
  reg := TRegistry.Create;
  try
    reg.OpenKey('Software\TRichView', True);
    reg.WriteString('Actions_Version', 'v4.8.2');
    reg.WriteString('Actions_Download', 'http://www.trichview.com/resources/actions/richviewactionsb.zip');
    reg.CloseKey;
  except
  end;
  reg.Free;
end;

procedure Register;
begin
  SaveVersion;

  RegisterActions('RVE Custom', [TrvActionEvent], nil);

  RegisterActions('RVE File', [
    TrvActionNew,  TrvActionOpen, TrvActionSave, TrvActionExport, TrvActionSaveAs,
    TrvActionPrintPreview, TrvActionPrint, TrvActionQuickPrint, TrvActionPageSetup
    ], TrvActionsResource);

  RegisterActions('RVE Edit', [

    TrvActionCut, TrvActionCopy,
    TrvActionPaste, TrvActionPasteAsText, TrvActionPasteSpecial,
    TrvActionUndo, TrvActionRedo,
    TrvActionSelectAll,
    TrvActionFind, TrvActionFindNext, TrvActionReplace,
    TrvActionCharCase
    ], TrvActionsResource);

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  RegisterActions('RVE Styles', [
    TrvActionStyleTemplates, TrvActionAddStyleTemplate, TrvActionClearFormat,
    TrvActionClearTextFormat,
    TrvActionStyleInspector
    ], TrvActionsResource);
  {$ENDIF}

  RegisterActions('RVE Text', [
    TrvActionFonts, TrvActionFontEx,
    TrvActionFontBold, TrvActionFontItalic,
    TrvActionFontUnderline, TrvActionFontStrikeout,
    TrvActionFontGrow, TrvActionFontShrink,
    TrvActionFontGrowOnePoint, TrvActionFontShrinkOnePoint,
    TrvActionFontAllCaps, TrvActionFontOverline,
    TrvActionFontColor, TrvActionFontBackColor,
    TrvActionSubscript, TrvActionSuperscript,
    TrvActionHide
    ], TrvActionsResource);

  RegisterActions('RVE Paragraph', [
    TrvActionParagraph, TrvActionParaBorder,
    TrvActionWordWrap,
    TrvActionAlignLeft, TrvActionAlignRight, TrvActionAlignCenter, TrvActionAlignJustify,
    TrvActionIndentInc, TrvActionIndentDec,
    TrvActionParaColor,
    TrvActionLineSpacing100, TrvActionLineSpacing150, TrvActionLineSpacing200,
    TrvActionClearLeft, TrvActionClearRight, TrvActionClearBoth, TrvActionClearNone
    ], TrvActionsResource);

  RegisterActions('RVE List', [
    TrvActionParaList, TrvActionParaBullets, TrvActionParaNumbering
    ], TrvActionsResource);

  RegisterActions('RVE BiDi', [
    TrvActionTextRTL, TrvActionTextLTR,
    TrvActionParaRTL, TrvActionParaLTR
    ], TrvActionsResource);

  {$IFDEF RVAUSEHTMLLIST}
  RegisterActions('RVE List', [
    TrvActionParaListHTML
    ], TrvActionsResource);
  {$ENDIF}

  RegisterActions('RVE Insert', [
    TrvActionInsertFile, TrvActionInsertPicture, TrvActionInsertHLine,
    {$IFNDEF RVDONOTUSESEQ}
    TrvActionInsertNumber, TrvActionInsertCaption,
    {$ENDIF}
    TrvActionInsertHyperlink, TrvActionInsertSymbol, TrvActionInsertText
    ], TrvActionsResource);

  {$IFNDEF RVDONOTUSESEQ}
  RegisterActions('RVE References', [
    TrvActionInsertFootnote, TrvActionInsertEndnote, TrvActionInsertSidenote,
    TrvActionInsertTextBox,
    TrvActionEditNote
    ], TrvActionsResource);
  {$ENDIF}

  RegisterActions('RVE Miscellaneous', [
    TrvActionColor, TrvActionBackground, TrvActionFillColor,
    TrvActionInsertPageBreak, TrvActionRemovePageBreak,
    TrvActionRemoveHyperlinks,
    TrvActionItemProperties, TrvActionVAlign,
    TrvActionShowSpecialCharacters
    ], TrvActionsResource);


  RegisterActions('RVE Table', [
    TrvActionInsertTable, TrvActionTableInsertRowsBelow, TrvActionTableInsertRowsAbove,
    TrvActionTableInsertColLeft, TrvActionTableInsertColRight,
    TrvActionTableDeleteRows, TrvActionTableDeleteCols, TrvActionTableDeleteTable,
    TrvActionTableMergeCells, TrvActionTableSplitCells,
    TrvActionTableSelectTable, TrvActionTableSelectRows, TrvActionTableSelectCols,
    TrvActionTableSelectCell,
    TrvActionTableCellVAlignTop, TrvActionTableCellVAlignMiddle,
    TrvActionTableCellVAlignBottom, TrvActionTableCellVAlignDefault,
    TrvActionTableCellRotationNone, TrvActionTableCellRotation90,
    TrvActionTableCellRotation180, TrvActionTableCellRotation270,
    TrvActionTableCellLeftBorder, TrvActionTableCellRightBorder,
    TrvActionTableCellTopBorder, TrvActionTableCellBottomBorder,
    TrvActionTableCellAllBorders, TrvActionTableCellNoBorders,
    TrvActionTableSplit, TrvActionTableToText, TrvActionTableSort,
    TrvActionTableProperties, TrvActionTableGrid
    ], TrvActionsResource);

  {$IFDEF USERVADDICT3}
  RegisterActions('RVE Spell Check',
    [TrvActionAddictSpell3, TrvActionAddictThesaurus3],
    TrvActionsResourceAddict3);
  {$ENDIF}

  RegisterComponents('RichView', [TRVAControlPanel,
    {$IFDEF USETB2K}TRVATBPopupMenu, {$ENDIF}
    {$IFDEF USETBX}TRVATBXPopupMenu, {$ENDIF}
    {$IFDEF USESPTBX}TRVASPTBXPopupMenu, {$ENDIF}
    {$IFDEF RICHVIEWDEF2006}TRVAPopupActionBar,{$ENDIF}
    TRVAPopupMenu]);

end;


end.