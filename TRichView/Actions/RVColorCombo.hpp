﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVColorCombo.pas' rev: 27.00 (Windows)

#ifndef RvcolorcomboHPP
#define RvcolorcomboHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <ColorRVFrm.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvcolorcombo
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVColorCombo;
class PASCALIMPLEMENTATION TRVColorCombo : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
private:
	unsigned FThemeEdit;
	unsigned FThemeCombo;
	Colorrvfrm::TfrmColor* frm;
	System::Uitypes::TColor FAutoColor;
	bool FUseXPThemes;
	bool FHot;
	bool FIndeterminate;
	System::Classes::TNotifyEvent FOnColorChange;
	System::Uitypes::TColor FChosenColor;
	Vcl::Dialogs::TColorDialog* FColorDialog;
	System::UnicodeString FAutoCaption;
	System::UnicodeString FTransparentCaption;
	System::UnicodeString FDefaultCaption;
	System::Classes::TComponent* FControlPanel;
	void __fastcall SetControlPanel(System::Classes::TComponent* Value);
	void __fastcall ColorPickerDestroy(System::TObject* Sender);
	int __fastcall GetMinHeight(void);
	MESSAGE void __fastcall WMGetDlgCode(Winapi::Messages::TWMNoParams &Msg);
	MESSAGE void __fastcall WMThemeChanged(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall WMNCPaint(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall CMMouseEnter(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall CMMouseLeave(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall CMEnabledChanged(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Winapi::Messages::TMessage &Message);
	void __fastcall CreateThemeHandle(void);
	void __fastcall FreeThemeHandle(void);
	void __fastcall SetUseXPThemes(const bool Value);
	void __fastcall DrawStandard(void);
	void __fastcall DrawThemed(void);
	void __fastcall NCPaintThemed(void);
	void __fastcall DrawStyled(void);
	void __fastcall NCPaintStyled(void);
	void __fastcall SetIndeterminate(const bool Value);
	void __fastcall SetChosenColor(const System::Uitypes::TColor Value);
	void __fastcall SetColorDialog(Vcl::Dialogs::TColorDialog* const Value);
	bool __fastcall StoreAutoCaption(void);
	bool __fastcall StoreTransparentCaption(void);
	System::UnicodeString __fastcall GetCaption(void);
	
protected:
	virtual void __fastcall Paint(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall DoEnter(void);
	DYNAMIC void __fastcall DoExit(void);
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	DYNAMIC void __fastcall Resize(void);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	virtual void __fastcall CreateWnd(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TRVColorCombo(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVColorCombo(void);
	__property System::Uitypes::TColor ChosenColor = {read=FChosenColor, write=SetChosenColor, default=536870911};
	
__published:
	__property System::Uitypes::TColor AutoColor = {read=FAutoColor, write=FAutoColor, default=536870911};
	__property TabStop = {default=1};
	__property bool UseXPThemes = {read=FUseXPThemes, write=SetUseXPThemes, default=1};
	__property bool Indeterminate = {read=FIndeterminate, write=SetIndeterminate, nodefault};
	__property System::Classes::TNotifyEvent OnColorChange = {read=FOnColorChange, write=FOnColorChange};
	__property Vcl::Dialogs::TColorDialog* ColorDialog = {read=FColorDialog, write=SetColorDialog};
	__property System::UnicodeString TransparentCaption = {read=FTransparentCaption, write=FTransparentCaption, stored=StoreTransparentCaption};
	__property System::UnicodeString AutoCaption = {read=FAutoCaption, write=FAutoCaption, stored=StoreAutoCaption};
	__property System::UnicodeString DefaultCaption = {read=FDefaultCaption, write=FDefaultCaption};
	__property System::Classes::TComponent* ControlPanel = {read=FControlPanel, write=SetControlPanel};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property DragKind = {default=0};
	__property Constraints;
	__property ParentBiDiMode = {default=1};
	__property TabOrder = {default=-1};
	__property OnEndDock;
	__property OnStartDock;
	__property Caption = {default=0};
	__property DragCursor = {default=-12};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ParentColor = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnStartDrag;
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVColorCombo(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvcolorcombo */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCOLORCOMBO)
using namespace Rvcolorcombo;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvcolorcomboHPP
