﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'InsTableRVFrm.pas' rev: 27.00 (Windows)

#ifndef InstablervfrmHPP
#define InstablervfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Instablervfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVInsTable;
class PASCALIMPLEMENTATION TfrmRVInsTable : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gbSize;
	Vcl::Stdctrls::TLabel* lblnCols;
	Vcl::Stdctrls::TLabel* lblnRows;
	Vcl::Stdctrls::TCheckBox* cbRemember;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Rvspinedit::TRVSpinEdit* seColumns;
	Rvspinedit::TRVSpinEdit* seRows;
	Vcl::Stdctrls::TGroupBox* gbLayout;
	Vcl::Stdctrls::TRadioButton* rbFitContents;
	Vcl::Stdctrls::TRadioButton* rbFitWindow;
	Vcl::Stdctrls::TRadioButton* rbFitManual;
	Rvspinedit::TRVSpinEdit* seWidth;
	Vcl::Stdctrls::TComboBox* cmbWidthType;
	void __fastcall rbFitManualClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cmbWidthTypeClick(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _rbFitContents;
	Vcl::Controls::TControl* _rbFitWindow;
	Vcl::Controls::TControl* _rbFitManual;
	Vcl::Controls::TControl* _cmbWidthType;
	Rvstyle::TRVStyleLength __fastcall GetBestWidth(void);
	int __fastcall GetColumns(void);
	int __fastcall GetRows(void);
	void __fastcall SetBestWidth(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetColumns(const int Value);
	void __fastcall SetRows(const int Value);
	void __fastcall CheckEnabled(void);
	
public:
	Vcl::Controls::TControl* _cbRemember;
	Rvstyle::TRVStyleLength OptimalBestWidth;
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	__property int Columns = {read=GetColumns, write=SetColumns, nodefault};
	__property int Rows = {read=GetRows, write=SetRows, nodefault};
	__property Rvstyle::TRVStyleLength BestWidth = {read=GetBestWidth, write=SetBestWidth, nodefault};
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVInsTable(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVInsTable(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVInsTable(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVInsTable(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Instablervfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_INSTABLERVFRM)
using namespace Instablervfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// InstablervfrmHPP
