﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ParaRVFrm.pas' rev: 27.00 (Windows)

#ifndef PararvfrmHPP
#define PararvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Pararvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVPara;
class PASCALIMPLEMENTATION TfrmRVPara : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* ts1;
	Vcl::Comctrls::TTabSheet* ts2;
	Vcl::Comctrls::TTabSheet* ts3;
	Rvofficeradiobtn::TRVOfficeRadioGroup* gbAlignment;
	Vcl::Stdctrls::TGroupBox* gbIndents;
	Vcl::Stdctrls::TLabel* lblLeft;
	Vcl::Stdctrls::TLabel* lblRight;
	Vcl::Stdctrls::TLabel* lblFirstLine;
	Vcl::Extctrls::TBevel* Bevel1;
	Rvspinedit::TRVSpinEdit* seLeftIndent;
	Rvspinedit::TRVSpinEdit* seRightIndent;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbNegative;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbPositive;
	Rvspinedit::TRVSpinEdit* seFirstLineIndent;
	Vcl::Stdctrls::TGroupBox* gbSpacing;
	Vcl::Stdctrls::TLabel* lblBefore;
	Vcl::Stdctrls::TLabel* lblAfter;
	Vcl::Stdctrls::TLabel* lblLineSpacing;
	Vcl::Extctrls::TBevel* Bevel3;
	Vcl::Stdctrls::TLabel* lblBy;
	Rvspinedit::TRVSpinEdit* seSpaceBefore;
	Rvspinedit::TRVSpinEdit* seSpaceAfter;
	Rvspinedit::TRVSpinEdit* seLineSpacingValue;
	Vcl::Stdctrls::TComboBox* cmbLineSpacing;
	Vcl::Stdctrls::TGroupBox* gbSample;
	Richview::TRichView* rv;
	Vcl::Controls::TImageList* ImageList1;
	Rvstyle::TRVStyle* rvs;
	Vcl::Stdctrls::TGroupBox* gbPagination;
	Vcl::Stdctrls::TCheckBox* cbKeepLinesTogether;
	Vcl::Stdctrls::TCheckBox* cbKeepWithNext;
	Vcl::Controls::TImageList* ilSmallTabs;
	Vcl::Controls::TImageList* ilTabs;
	Vcl::Stdctrls::TListBox* lstTabs;
	Vcl::Stdctrls::TButton* btnDelete;
	Vcl::Stdctrls::TButton* btnDeleteAll;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgTabAlign;
	Vcl::Extctrls::TRadioGroup* rgLeader;
	Rvspinedit::TRVSpinEdit* seTabPos;
	Vcl::Stdctrls::TLabel* lblTabPos;
	Vcl::Stdctrls::TButton* btnSet;
	Vcl::Stdctrls::TLabel* lblDelTabs;
	Vcl::Stdctrls::TLabel* lblDelTabList;
	Vcl::Controls::TImageList* ilListTabs;
	Vcl::Stdctrls::TLabel* lblOutlineLevel;
	Vcl::Stdctrls::TComboBox* cmbOutline;
	Vcl::Stdctrls::TLabel* lblTabPosMU;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall seFirstLineIndentChange(System::TObject* Sender);
	void __fastcall cmbLineSpacingClick(System::TObject* Sender);
	void __fastcall seLineSpacingValueChange(System::TObject* Sender);
	void __fastcall seChange(System::TObject* Sender);
	void __fastcall gbAlignmentClick(System::TObject* Sender);
	void __fastcall gbAlignmentDblClickItem(System::TObject* Sender);
	void __fastcall rbFLClick(System::TObject* Sender);
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall seLeftIndentChange(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall lstTabsDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall lstTabsClick(System::TObject* Sender);
	void __fastcall seTabPosChange(System::TObject* Sender);
	void __fastcall btnDeleteAllClick(System::TObject* Sender);
	void __fastcall btnDeleteClick(System::TObject* Sender);
	void __fastcall btnSetClick(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _lstTabs;
	Vcl::Controls::TControl* _rgLeader;
	Vcl::Controls::TControl* _lblDelTabList;
	Vcl::Controls::TControl* _btnSet;
	Vcl::Controls::TControl* _btnDelete;
	Vcl::Controls::TControl* _btnDeleteAll;
	Vcl::Controls::TControl* _ts2;
	bool IgnoreChanges;
	bool FDeleteAllTabs;
	Rvclasses::TRVIntegerList* FTabsToDelete;
	Rvstyle::TRVTabInfos* FTabs;
	bool DoNotResetLeader;
	bool FHanging;
	void __fastcall UpdateSample(void);
	void __fastcall SetTabs(Rvstyle::TRVTabInfos* Value);
	void __fastcall UpdateTabList(int NewIndex);
	void __fastcall DrawTabItem(Vcl::Graphics::TCanvas* Canvas, int Index, const System::Types::TRect &R, bool Selected);
	void __fastcall UpdateDelTabList(void);
	
public:
	bool SimpleTabManagement;
	Vcl::Controls::TControl* _cmbLineSpacing;
	Vcl::Controls::TControl* _cmbOutlineLevel;
	Vcl::Controls::TControl* _cbKeepWithNext;
	Vcl::Controls::TControl* _cbKeepLinesTogether;
	bool TabsModified;
	DYNAMIC void __fastcall Localize(void);
	void __fastcall HideTabsPage(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	__property bool DeleteAllTabs = {read=FDeleteAllTabs, write=FDeleteAllTabs, nodefault};
	__property Rvclasses::TRVIntegerList* TabsToDelete = {read=FTabsToDelete};
	__property Rvstyle::TRVTabInfos* Tabs = {read=FTabs, write=SetTabs};
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVPara(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVPara(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVPara(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVPara(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TfrmRVPara* frmRVPara;
}	/* namespace Pararvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PARARVFRM)
using namespace Pararvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PararvfrmHPP
