﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVRuler.pas' rev: 27.00 (Windows)

#ifndef RvrulerHPP
#define RvrulerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Ruler.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvruler
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVRulerOption : unsigned char { rvroAutoAdjustInset, rvroAutoAdjustMargins, rvroAutoAdjustPageSize };

typedef System::Set<TRVRulerOption, TRVRulerOption::rvroAutoAdjustInset, TRVRulerOption::rvroAutoAdjustPageSize> TRVRulerOptions;

class DELPHICLASS TRVRuler;
class PASCALIMPLEMENTATION TRVRuler : public Ruler::TRuler
{
	typedef Ruler::TRuler inherited;
	
private:
	Vcl::Controls::TControlCanvas* FControlCanvas;
	int FFirstIndent;
	int FLeftIndent;
	int FRightIndent;
	bool FInPlaceEditorSaved;
	Vcl::Graphics::TPenStyle FLineStyle;
	Rvedit::TCustomRichViewEdit* FRichViewEdit;
	int FSaveX;
	bool FUpdatingTabs;
	int FRulerInset;
	TRVRulerOptions FRVRulerOptions;
	void __fastcall AssignRichViewEditEvents(void);
	void __fastcall DrawMarkLine(void);
	bool __fastcall GetListIndents(System::Extended &FI, System::Extended &LI);
	void __fastcall RichViewEditCaretMove(System::TObject* Sender);
	void __fastcall RichViewEditHScrolled(System::TObject* Sender);
	void __fastcall RichViewEditVScrolled(System::TObject* Sender);
	void __fastcall RichViewEditParaStyleConversion(Rvedit::TCustomRichViewEdit* Sender, int StyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	void __fastcall RichViewEditResize(System::TObject* Sender);
	void __fastcall RestoreRichViewEditEvents(void);
	void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* const Value);
	void __fastcall SetRVRulerOptions(const TRVRulerOptions Value);
	System::Extended __fastcall GetMulti(Rvstyle::TRVStyle* RVStyle);
	MESSAGE void __fastcall WMUpdateTableEditor(Winapi::Messages::TMessage &Msg);
	
protected:
	bool FUpdating;
	virtual void __fastcall RichViewEditCurParaStyleChanged(System::TObject* Sender);
	DYNAMIC void __fastcall DoBiDiModeChanged(void);
	DYNAMIC void __fastcall DoIndentChanged(void);
	DYNAMIC void __fastcall DoLevelButtonUp(int Direction);
	DYNAMIC void __fastcall DoMarginChanged(void);
	DYNAMIC void __fastcall ApplyMarginsToRV(void);
	DYNAMIC void __fastcall DoPageWidthChanged(void);
	DYNAMIC void __fastcall DoTabChanged(void);
	DYNAMIC void __fastcall DoRulerItemMove(Ruler::TDragItem DragItem, int X, bool Removing);
	DYNAMIC void __fastcall DoRulerItemRelease(Ruler::TDragItem DragItem);
	DYNAMIC void __fastcall DoRulerItemSelect(Ruler::TDragItem DragItem, int X);
	DYNAMIC void __fastcall DoTableColumnChanged(void);
	DYNAMIC void __fastcall DoTableRowChanged(void);
	virtual void __fastcall Loaded(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TRVRuler(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVRuler(void);
	void __fastcall AdjustPosition(void);
	void __fastcall UpdateRulerIndents(void);
	void __fastcall UpdateRulerMargins(void);
	void __fastcall UpdateTableEditor(void);
	void __fastcall UpdateListEditor(void);
	
__published:
	__property Inset = {default=0};
	__property Vcl::Graphics::TPenStyle LineStyle = {read=FLineStyle, write=FLineStyle, default=2};
	__property ListEditor;
	__property OnTableRowClick;
	__property OnTableRowDblClick;
	__property Rvedit::TCustomRichViewEdit* RichViewEdit = {read=FRichViewEdit, write=SetRichViewEdit};
	__property TRVRulerOptions RVRulerOptions = {read=FRVRulerOptions, write=SetRVRulerOptions, default=7};
	__property TableEditor;
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVRuler(HWND ParentWindow) : Ruler::TRuler(ParentWindow) { }
	
};


class DELPHICLASS TRVRulerItemSelector;
class PASCALIMPLEMENTATION TRVRulerItemSelector : public Ruler::TRulerItemSelector
{
	typedef Ruler::TRulerItemSelector inherited;
	
public:
	/* TCustomRulerItemSelector.Create */ inline __fastcall virtual TRVRulerItemSelector(System::Classes::TComponent* AOwner) : Ruler::TRulerItemSelector(AOwner) { }
	/* TCustomRulerItemSelector.Destroy */ inline __fastcall virtual ~TRVRulerItemSelector(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVRulerItemSelector(HWND ParentWindow) : Ruler::TRulerItemSelector(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
#define DefaultRVRulerOptions (System::Set<TRVRulerOption, TRVRulerOption::rvroAutoAdjustInset, TRVRulerOption::rvroAutoAdjustPageSize>() << TRVRulerOption::rvroAutoAdjustInset << TRVRulerOption::rvroAutoAdjustMargins << TRVRulerOption::rvroAutoAdjustPageSize )
static const System::Word WM_UPDATETABLEEDITOR = System::Word(0x401);
}	/* namespace Rvruler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVRULER)
using namespace Rvruler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvrulerHPP
