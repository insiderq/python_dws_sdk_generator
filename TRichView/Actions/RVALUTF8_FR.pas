﻿// This file is a copy of RVAL_Fr.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       French Translation                              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Boualem Koudri, 2003-02-07             }
{ Modified by :  JJM,Michael REMY, Jean-Philippe RENOU  }
{                Damien Griessinger: 2011-02-20         }
{                                    2012-09-08         }
{                Gilles Vasseur:     2014-02-03         }
{*******************************************************}

unit RVALUTF8_Fr;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'pouces', 'cm', 'mm', 'picas', 'pixels', 'points',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
   '''''', 'cm', 'mm', 'pc', 'pixels', 'pt', 
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fichier', 'É&dition', 'Forma&t', '&Police', 'P&aragraphe', '&Insertion', 'Ta&bleau', 'Fe&nêtre', 'Aid&e',
  // exit
  '&Quitter',
  // top-level menus: View, Tools,
  '&Affichage', '&Outils',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Ta&ille', 'S&tyle', 'Sélecti&on', 'Alig&nement du contenu de la cellule', 'Bordure de la c&ellule',
  // menus: Table cell rotation
  '&Rotation de la cellule',
  // menus: Text flow, Footnotes/endnotes
  '&Enchaînements', '&Notes', 
  // ribbon tabs: tab1, tab2, view, table
  'Acc&ueil', '&Avancé', 'A&ffichage', '&Tableau', 
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Presse-papier', 'Police', 'Paragraphe', 'Liste', 'Édition',
  // ribbon groups: insert, background, page setup,
  'Insérer', 'Trame de fond', 'Mise en page', 
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Liens', 'Notes de bas de page', 'Affichages document', 'Afficher/Masquer', 'Zoom', 'Options',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Insérer', 'Supprimer', 'Opérations', 'Bordures', 
  // ribbon groups: styles 
  'Styles',
  // ribbon screen tip footer,
  'Appuyez sur F1 pour de l''aide.',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documents récents', 'Enregistrer une copie du document', 'Aperçu et impression du document',
  // ribbon label: units combo
  'Unités:', 
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nouveau', 'Nouveau|Crée une nouvelle page vierge',
  // TrvActionOpen
  '&Ouvrir...', 'Ouvrir|Ouvre un document à partir du disque',
  // TrvActionSave
  '&Enregistrer', 'Enregistrer|Enregistre le document en cours',
  // TrvActionSaveAs
  'Enregistrer &sous...', 'Enregistrer sous...|Enregistre le document sous un nouveau nom',
  // TrvActionExport
  'Expor&ter...', 'Exporter|Exporte le document vers un autre format',
  // TrvActionPrintPreview
  'A&perçu avant impression', 'Aperçu avant l''impression|Montre le document avant l''impression',
  // TrvActionPrint
  '&Imprimer...', 'Imprimer|Configure l''imprimante et imprime le document',
  // TrvActionQuickPrint
  '&Imprimer', 'Impression ra&pide', 'Imprimer|Imprime le document', 
   // TrvActionPageSetup
  'Options de pa&ge...', 'Définir les options de page|Définit les marges, la taille du papier, l''orientation, la source, le haut et le bas de page',
  // TrvActionCut
  'C&ouper', 'Couper|Coupe la sélection (vers le presse-papier) ',
  // TrvActionCopy
  '&Copier', 'Copier|Copie la sélection dans le presse-papier',
  // TrvActionPaste
  'Co&ller', 'Coller|Insère le contenu du presse-papier',
  // TrvActionPasteAsText
  'Coller en tant que &texte', 'Coller comme texte|Insère le texte du presse-papier', // dg
  // TrvActionPasteSpecial
  'Collage &spécial...', 'Collage spécial|Insère le contenu du presse-papier au format spécifié',
  // TrvActionSelectAll
  '&Tout sélectionner ', 'Tout sélectionner|Sélectionne tout le document',
  // TrvActionUndo
  '&Annuler', 'Annuler|Annule la dernière action',
  // TrvActionRedo
  'Re&faire', 'Refaire|Répète la dernière action annulée',
  // TrvActionFind
  '&Rechercher...', 'Rechercher|Recherche le texte spécifié dans le document',
  // TrvActionFindNext
  'Rechercher &suivant', 'Rechercher le suivant|Continue la dernière recherche',
  // TrvActionReplace
  'Re&mplacer...', 'Remplacer|Trouve et remplace le texte spécifié dans le document',
  // TrvActionInsertFile
  '&Fichier...', 'Insérer un fichier|Insère le contenu d''un fichier dans le document',
  // TrvActionInsertPicture
  '&Image...', 'Insérer une image|Insère une image à partir d''un fichier',
  // TRVActionInsertHLine
  '&Ligne horizontale', 'Insère une ligne horizontale',
  // TRVActionInsertHyperlink
  'Lien &hypertexte...', 'Insère un lien hypertexte ',
  // TRVActionRemoveHyperlinks
  'Supp&rimer les liens hypertextes', 'Supprimer les liens hypertextes|Supprime tous les liens hypertextes dans le texte sélectionné',
  // TrvActionInsertSymbol
  '&Symbole...', 'Insérer un symbole',
  // TrvActionInsertNumber
  '&Numéro...', 'Insérer un numéro|Insère un numéro',
  // TrvActionInsertCaption
  'Insérer une &légende...', 'Insérer une légende|Insère une légende à l''objet sélectionné',
  // TrvActionInsertTextBox
  'Boîte de &texte', 'Insérer une boîte de texte|Insère une boîte de texte',
  // TrvActionInsertSidenote
  '&Annotation', 'Insérer une annotation|Insère une note dans une boîte de texte',
  // TrvActionInsertPageNumber
  '&Numéro de page', 'Insérer un numéro de page|Insère un numéro de page',
  // TrvActionParaList
  'P&uces et numérotation ...', 'Puces et numérotation|Applique les puces et la numérotation au paragraphe sélectionné ',
  // TrvActionParaBullets
  '&Puces', 'Puces|Ajoute ou supprime les puces du paragraphe',
  // TrvActionParaNumbering
  '&Numérotation', 'Numérotation|Ajoute ou supprime la numérotation du paragraphe',
  // TrvActionColor
  'Couleur &arrière-plan...', 'Couleur de l''arrière-plan|Change la couleur de l''arrière-plan du document',
  // TrvActionFillColor
  'Couleur de &remplissage...', 'Remplir avec la couleur|Change la couleur du texte (paragraphe, cellule, tableau ou document)',
  // TrvActionInsertPageBreak
  '&Insérer un saut de page', 'Insérer un saut de page',
  // TrvActionRemovePageBreak
  '&Supprimer le saut de page', 'Supprimer le saut de page',
  // TrvActionClearLeft
  // 'Clear Text Flow at &Left Side', 'Clear Text Flow at Left Side|Places this paragraph below any left-aligned picture',
  'Effacer l''enchaînement du côté &gauche','Effacer l''enchaînement du côté gauche|Place le paragraphe sous toute image alignée à gauche',
  // TrvActionClearRight
  // 'Clear Text Flow at &Right Side', 'Clear Text Flow at Right Side|Places this paragraph below any right-aligned picture',
  'Effacer l''enchaînement du côté &droit','Effacer l''enchaînement du côté droit|Place le paragraphe sous toute image alignée à droite',
  // TrvActionClearBoth
  // 'Clear Text Flow at &Both Sides', 'Clear Text Flow at Both Sides|Places this paragraph below any left- or right-aligned picture',
  'Effacer l''enchaînement des deux &côtés','Effacer l''enchaînement des deux côtés|Place le paragraphe sous toute image (placée à droite ou à gauche)',
  // TrvActionClearNone
  // '&Normal Text Flow', 'Normal Text Flow|Allows text flow around both left- and right-aligned pictures',
  '&Enchaînement normal', 'Enchaînement normal|Autorise un enchaînement autour des images',
  // TrvActionVAlign
  'Position de l''&objet...', 'Position de l''objet|Modifie la position de l''objet choisi',
  // TrvActionItemProperties
  '&Propriétés de l''objet...', 'Propriétés de l''objet|Définit les propriétés de l''objet actif',
  // TrvActionBackground
  '&Arrière-plan...', 'Arrière-plan|Choisit un arrière-plan couleur et image',
  // TrvActionParagraph
  '&Paragraphe...', 'Paragraphe|Change les attributs du paragraphe',
  // TrvActionIndentInc
  '&Augmenter l''espacement', 'Augmenter l''espacement|Augmente l''espacement gauche du paragraphe sélectionné',
  // TrvActionIndentDec
  '&Diminuer l''espacement', 'Diminuer l''espacement|Diminue l''espacement gauche du paragraphe sélectionné',
  // TrvActionWordWrap
  '&Retour automatique à la ligne ', 'Retour automatique à la ligne |Inverse le retour automatique à la ligne pour les paragraphes choisis ',
  // TrvActionAlignLeft
  'Alignement à &gauche', 'Alignement à gauche|Aligne le texte sélectionné à gauche',
  // TrvActionAlignRight
  'Alignement à &droite', 'Alignement à droite|Aligne le texte sélectionné à droite',
  // TrvActionAlignCenter
  'Alignement &centré', 'Alignement centré|Centre le texte sélectionné',
  // TrvActionAlignJustify
  '&Justifié', 'Alignement justifié|Aligne le texte sélectionné des deux côtés (gauche et droite)',
  // TrvActionParaColor
  'C&ouleur d''arrière-plan du paragraphe...', 'Couleur d''arrière-plan du paragraphe|Définit la couleur d''arrière-plan du paragraphe',
  // TrvActionLineSpacing100
  'Interlignage &simple ', 'Interlignage simple |Fixe l''interlignage à simple',
  // TrvActionLineSpacing150
  '1.5 &Interlignage ', 'Interlignage 1.5 |Fixe l''interlignage à 1.5',
  // TrvActionLineSpacing200
  'Interlignage &double', 'Interlignage double|Fixe l''interlignage à double',
  // TrvActionParaBorder
  '&Bordures et fond du paragraphe...', 'Bordures et fond du paragraphe|Définit les bordures et le fond du texte sélectionné',
  // TrvActionInsertTable
  '&Insérer un tableau...', '&Tableau', 'Insérer un tableau|Insère un nouveau tableau',
  // TrvActionTableInsertRowsAbove
  'Insérer une ligne &au-dessus', 'Insérer une ligne au-dessus|Insère une nouvelle ligne au-dessus des cellules sélectionnées',
  // TrvActionTableInsertRowsBelow
  'Insérer une ligne au-&dessous', 'Insérer une ligne au-dessous|Insère une nouvelle ligne au-dessous des cellules sélectionnées',
  // TrvActionTableInsertColLeft
  'Insérer une colonne à &gauche ', 'Insérer une colonne à gauche|Insère une nouvelle colonne à gauche des cellules sélectionnées',
  // TrvActionTableInsertColRight
  'Insérer une colonne à &droite', 'Insérer une colonne à droite|Insère une nouvelle colonne à droite des cellules sélectionnées',
  // TrvActionTableDeleteRows
  'Supprimer les &lignes', 'Supprimer les lignes|Supprime les lignes avec les cellules sélectionnées',
  // TrvActionTableDeleteCols
  'Supprimer les &colonnes', 'Supprimer les colonnes|Supprime les colonnes avec les cellules sélectionnées',
  // TrvActionTableDeleteTable
  '&Supprimer le tableau', 'Supprimer le tableau',
  // TrvActionTableMergeCells
  '&Fusionner les cellules', 'Fusionner les cellules|Fusionne les cellules sélectionnées',
  // TrvActionTableSplitCells
  'Fra&ctionner les cellules...', 'Fractionner les cellules|Fractionne les cellules sélectionnées',
  // TrvActionTableSelectTable
  'Sé&lectionner le tableau', 'Sélectionner le tableau',
  // TrvActionTableSelectRows
  'Sélectionner les l&ignes', 'Sélectionner les lignes',
  // TrvActionTableSelectCols
  'Sélectionner les c&olonnes', 'Sélectionner les colonnes',
  // TrvActionTableSelectCell
  'Sélectionner les cell&ules', 'Sélectionner les cellules',
  // TrvActionTableCellVAlignTop
  'Aligner les cellules en &haut', 'Aligner les cellules en haut|Aligne le contenu de la cellule en haut',
  // TrvActionTableCellVAlignMiddle
  'Aligner les cellules au &milieu', 'Aligner les cellules au milieu|Aligne le contenu de la cellule au milieu',
  // TrvActionTableCellVAlignBottom
  'Aligner les cellules en &bas', 'Aligner les cellules en bas|Aligne le contenu de la cellule en bas',
  // TrvActionTableCellVAlignDefault
  'Alignement vertical des cellules par &défaut', 'Alignement vertical des cellules par défaut|Applique l''alignement vertical du contenu des cellules par défaut',
  // TrvActionTableCellRotationNone
  '&Pas de rotation de cellule', 'Pas de rotation de cellule|Fait pivoter le contenu de la cellule à 0°',
  // TrvActionTableCellRotation90
  'Rotation de la cellule à &90°', 'Rotation de la cellule à 90°|Fait pivoter le contenu de la cellule à 90°',
  // TrvActionTableCellRotation180
  'Rotation de la cellule à &180°', 'Rotation de la cellule à 180°|Fait pivoter le contenu de la cellule à 180°',
  // TrvActionTableCellRotation270
  'Rotation de la cellule à &270°', 'Rotation de la cellule à 270°|Fait pivoter le contenu de la cellule à 270°',
  // TrvActionTableProperties
  '&Propriétés du tableau...', 'Propriétés du tableau|Change les propriétés du tableau sélectionné',
  // TrvActionTableGrid
  'Montrer la &grille ', 'Montrer la grille |Montre ou cache les lignes de la grille ',
  // TRVActionTableSplit
  'Fractionner le tab&leau', 'Fractionner le tableau|Fractionne le tableau en deux tableaux débutant par la ligne sélectionnée',
  // TRVActionTableToText
  'Convertir en Te&xte...', 'Convertir en texte|Convertit le tableau en texte',
  // TRVActionTableSort
  '&Trier...', 'Trier|Trie les lignes du tableau',
  // TrvActionTableCellLeftBorder
  '&Bordure gauche', 'Bordure gauche|Montre ou cache la bordure  à gauche de la cellule',
  // TrvActionTableCellRightBorder
  'B&ordure droite', 'Bordure droite|Montre ou cache la bordure à droite de la cellule',
  // TrvActionTableCellTopBorder
  'Bo&rdure de haut', 'Bordure de haut|Montre ou cache la bordure en haut de la cellule',
  // TrvActionTableCellBottomBorder
  'Bor&dure du bas', 'Bordure du bas|Montre ou cache la bordure en bas de la cellule',
  // TrvActionTableCellAllBorders
  '&Toutes les bordures', 'Toutes les bordures|Montre toutes les bordures de la cellule',
  // TrvActionTableCellNoBorders
  '&Aucune bordure', 'Aucune bordure|Cache toutes les bordures de la cellule',
  // TrvActionFonts & TrvActionFontEx
  '&Police...', 'Police|Change la police du texte sélectionné',
  // TrvActionFontBold
  '&Gras', 'Gras|Change le style du texte sélectionné en gras',
  // TrvActionFontItalic
  '&Italique', 'Italique|Change le style du texte sélectionné en italique',
  // TrvActionFontUnderline
  '&Souligné', 'Souligné|Souligne le texte sélectionné',
  // TrvActionFontStrikeout
  '&Barré', 'Barré|Barre le texte sélectionné',
  // TrvActionFontGrow
  '&Augmenter le corps', 'Augmenter le corps|Augmente la taille du texte sélectionné de 10%',
  // TrvActionFontShrink
  '&Diminuer le corps', 'Diminuer le corps|Diminue la taille du texte sélectionné de 10%',
  // TrvActionFontGrowOnePoint
  'Ag&randir le corps d''un point', 'Agrandir le corps d''un point|Agrandit la taille de la police du texte sélectionné d''un point',
  // TrvActionFontShrinkOnePoint
  'Di&minuer la taille du corps d''un point', 'Diminuer la taille du corps d''un point|Diminue la taille de la police du texte sélectionné d''un point',
  // TrvActionFontAllCaps
  '&Tout en majuscules', 'Tout en majuscules|Change tous les caractères du texte sélectionné en majuscules',
  // TrvActionFontOverline
  '&Ligne au dessus', 'Ligne au dessus|Ajoute une ligne au-dessus du texte sélectionné',
  // TrvActionFontColor
  'Couleur du &texte...', 'Couleur du texte|Change la couleur du texte sélectionné',
  // TrvActionFontBackColor
  'Couleur d''arrière-&plan du texte...', 'Couleur d''arrière-plan du texte|Change la couleur d''arrière-plan du texte sélectionné',
  // TrvActionAddictSpell3
  '&Vérification orthographique', 'Vérification orthographique',
  // TrvActionAddictThesaurus3
  '&Synonymes', 'Synonymes|Fournit les synonymes du mot sélectionné',
  // TrvActionParaLTR
  'De gauche à droite', 'De gauche à droite|Écrit de gauche à droite pour le(s) paragraphe(s) sélectionné(s)',
  // TrvActionParaRTL
  'De droite à gauche', 'De droite à gauche|Écrit de droite à gauche pour le(s) paragraphe(s) sélectionné(s)',
  // TrvActionLTR
  'Texte de gauche à droite', 'De gauche à droite|Écrit de droite à gauche pour le texte sélectionné',
  // TrvActionRTL
  'Texte de droite à gauche', 'De droite à gauche|Écrit de droite à gauche pour le texte sélectionné',
  // TrvActionCharCase
  'Casse de caractères', 'Casse de caractères|Change la casse du texte sélectionné',
  // TrvActionShowSpecialCharacters
  'Caractères &non imprimables', 'Caractères non imprimables|Cache ou affiche les caractères non imprimables, comme les marques de paragraphes, les tabulations et les espaces',
  // TrvActionSubscript
  'Indice', 'Indice|Place le texte sélectionné en indice',
  // TrvActionSuperscript
  'Exposant', 'Exposant|Place le texte sélectionné en exposant',
  // TrvActionInsertFootnote
  'Note de &bas de page', 'Note de bas de page|Insère une note de bas de page',
  // TrvActionInsertEndnote
  'Note de &fin', 'Note de fin|Insère une note de fin', 
  // TrvActionEditNote
  'É&diter la note', 'Éditer la note|Édite la note de bas de page ou la note de fin',
  '&Masquer', 'Masquer|Masque ou affiche la partie sélectionnée', 
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Styles...', 'Styles|Ouvre la fenêtre de gestion des styles',
  // TrvActionAddStyleTemplate
  '&Ajouter un style...', 'Ajouter un style|Crée un nouveau style de texte ou de paragraphe',
  // TrvActionClearFormat,
  'Effa&cer la mise en forme', 'Effacer la mise en forme|Efface toute la mise en forme des textes et des paragraphes sélectionnés',
  // TrvActionClearTextFormat,
  'Effacer la mise en forme du &texte', 'Effacer la mise en forme|Efface toute la mise en forme du texte sélectionné',
  // TrvActionStyleInspector
  '&Inspecteur de styles', 'Inspecteur de styles|Affiche ou masque l''inspecteur de styles',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Valider', 'Annuler', 'Quitter', 'Insérer', '&Ouvrir...', '&Enregistrer...', 'E&ffacer', 'Aid&e', 'Enlever',
  // Others  -------------------------------------------------------------------
  // percents
  '%',
  // left, top, right, bottom sides
  'Côté gauche', 'Côté haut', 'Côté droit', 'Côté bas',
  // save changes? confirm title
  'Enregistrer les modifications de %s?', 'Confirmer',
  // warning: losing formatting
  '%s peut contenir des caractéristiques non compatibles avec le format choisi.'#13+
  'Souhaitez-vous réellement enregistrer ce document avec le format choisi?',
  // RVF format name
  'Format Richview',
  // Error messages ------------------------------------------------------------
  'Erreur',
  'Erreur durant le chargement du fichier.'#13#13'Les raisons possibles:'#13'- le format de ce fichier n''est pas supporté par cette application;'#13+
  '- Le fichier est corrompu ;'#13'- le fichier est ouvert par une autre application.',
  'Erreur de chargement d''image du fichier.'#13#13'Raisons possibles:'#13'- le fichier contient des images de format non supporté par l''application;'#13+
  '- le fichier ne contient pas d''image;'#13+
  '- le fichier est corrompu;'#13'- le fichier est ouvert par une autre application.',
  'Erreur d''enregistrement du fichier.'#13#13'Raisons possibles:'#13'- espace disque non suffisant;'#13+
  '- le disque est protégé en écriture;'#13'- le disque n''est pas inséré;'#13+
  '- le fichier est ouvert par une autre application;'#13'- le disque est corrompu.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'fichier RichView  (*.rvf)|*.rvf',  'fichier RTF (*.rtf)|*.rtf' , 'fichier XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'fichier Texte (*.txt)|*.txt', 'fichier Texte - Unicode (*.txt)|*.txt', 'fichier Texte - autodétection (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'fichier HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - simplifié (*.htm;*.html)|*.htm;*.html',
  // DocX
  'fichier Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Recherche terminée.', 'Chaîne''%s'' non trouvée.',
  // 1 string replaced; Several strings replaced
  '1 chaîne remplacée.', '%d chaînes remplacées.',
  // continue search from the beginning/end?
  'La fin du document est atteinte. Continuer en reprenant au début ?',
  'Le début du document est atteint. Continuer en reprenant à la fin ?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Automatique',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Noir', 'Marron', 'Vert Olive', 'Vert foncé', 'Vert canard foncé', 'Bleu foncé', 'Indigo', 'Gris-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Rouge foncé', 'Orange', 'Jaune foncé', 'Vert', 'Vert canard', 'Bleu', 'Bleu-gris', 'Gris-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rouge', 'Orange clair ', 'Chaux ', 'Vert de mer ', 'Bleu-vert', 'Bleu clair ', 'Violet', 'Gris-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rose', 'Doré', 'Jaune', 'Vert clair', 'Turquoise', 'Bleu ciel', 'Prune', 'Gris-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Ocre', 'Jaune clair', 'Vert clair', 'Turquoise clair', 'Bleu clair', 'Lavande', 'Blanc',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&Transparent', '&Automatique', 'Autres &couleurs...', '&Défaut',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Arrière-plan', 'C&ouleur:', 'Position', 'Arrière-plan', 'Texte exemple.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Aucun', '&Fenêtre pleine', 'Titres &fixés', '&Titré', 'C&entré',
  // Padding button
  '&Marges ...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Couleur de remplissage', '&Appliquer à:', 'Autres &couleurs...', '&Marges...',
  'Veuillez sélectionner une couleur', // [apply to:] text, paragraph, table, cellule
  'Texte', 'Paragraphe' , 'Tableau', 'Cellule',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Police', 'Police', 'Disposition',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fonte:', '&Taille', 'Style', '&Gras', '&Italique',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Couleur:', 'Arrière-p&lan:', '(par défaut)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effets', 'So&uligné', '&Surligné', '&Barré', '&Majuscules ',
  // Sample, Sample text
  'Exemple', 'Texte exemple',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espacement de caractères', '&Espacement:', 'Au&gmenté ', '&Condensé',
  // Offset group-box, Offset label, Down, Up,
  'Excentrage ', '&Excentrage :', '&Au-dessous', 'A&u-dessus',
  // Scaling group-box, Scaling
  'Graduation', 'Graduation:',
  // Sub/super script group box, Normal, Sub, Super
  'Indices et exposants', '&Normal', '&Indice', 'E&xposant',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Marges intérieures', '&Haut:', '&Gauche:', '&Bas:', '&Droite:', '&Valeurs égales',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insérer un lien hypertexte', 'Lien hypertexte', 'T&exte:', '&Cible', '<<sélection>>',
  // cannot open URL
  'Navigation non possible sur "%s"',
  // hyperlink properties button, hyperlink style
  '&Personnalisation...', '&Style:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Propriétés du lien', 'Couleur normale', 'Couleurs actives',
  // Text [color], Background [color]
  '&Texte', '&Arrière-plan',
  // Underline [color]
  'So&uligné:',
  // Text [color], Background [color] (different hotkeys)
  'T&exte:', '&Arrière-plan',
  // Underline [color], Attributes group-box,
  'So&uligné:', 'Attributs',
  // Underline type: always, never, active (below the mouse)
  'So&uligné:', 'toujours', 'jamais', 'actif',
  // Same as normal check-box
  'Comme &normal',
  // underline active links check-box
  'So&uligner les liens actifs',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insérer un symbole', '&Police :', '&Jeu de caractères:', 'Unicode',
  // Unicode block
  '&Bloc:',
  // Character Code, Character Unicode Code, No Character
  ' Code caractère: %d', 'Code caractère: Unicode %d', '(pas de caractère)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Insérer un tableau', 'Taille du tableau', 'Nombre de &colonnes:', 'Nombre de &lignes:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Disposition du tableau', '&Taille automatique', 'Adaptation de la taille du tableau à la fenêtre ', 'Taille manuelle du tableau',
  // Remember check-box
  'Sauvegarder les dimensions pour les nouveaux tableaux',
  // VAlign Form ---------------------------------------------------------------
  'Emplacement', 
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriétés', 'Image', 'Position et taille', 'Ligne', 'Tableau', 'Lignes', 'Cellules',
  // Image Appearance, Image Misc, Number tabs
  'Apparence', 'Divers', 'Numéro',
  // Box position, Box size, Box appearance tabs
  'Position', 'Taille', 'Apparence',
  // Preview label, Transparency group-box, checkbox, label
  'Aperçu:', 'Transparence', '&Transparent', '&Couleur transparente:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'C&hanger...', '&Sauvegarder...', 'Automatique',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alignement vertical', '&Alignement:',
  'bas de la ligne du texte', 'milieu de la ligne du texte',
  'haut - vers la ligne du haut', 'bas - vers la ligne du bas', 'milieu - vers la ligne du milieu',
  // align to left side, align to right side
  'côté gauche', 'côté droit', 
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Décalé de:', 'Étirement', '&Largeur:', '&Hauteur:', 'Taille par défaut: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Échelle &proportionnelle', '&RAZ',
  // Inside the border, border, outside the border groupboxes
  'Dans la bordure', 'Bordure', 'Hors de la bordure',
  // Fill color, Padding (i.e. margins inside border) labels
  'Couleur de &remplissage:', '&Marge intérieure:',
  // Border width, Border color labels
  '&Largeur de bordure:', '&Couleur de bordure:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espacement &horizontal:', 'Espacement &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Divers', '&Infobulle:',
  // web group-box, alt text
  'Web', '&Texte alternatif:',
  // Horz line group-box, color, width, style
  'Ligne horizontale', '&Couleur:', '&Largeur:', '&Style:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tableau', '&Largeur:', '&Remplir avec une couleur:', '&Espacement des cellules...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Marge &horizontale de cellule:', 'Marge &verticale de cellule:', 'Bordure et fond',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Côtés visibles de &bordure...', 'Automatique', 'automatique',
  // Keep row content together checkbox
  '&Lier le contenu',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotation', '&Aucune', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Bordure:', 'Côtés visibles de &bordure...',
  // Border, CellBorders buttons
  '&Bordures de tableau...', 'B&ordures des cellules...',
  // Table border, Cell borders dialog titles
  'Bordures de tableau', 'Bordures des cellules par défaut',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Impression', '&Garder le tableau en une seule page', '&Nombres de lignes de titre:',
  'Titre de ligne répété dans chaque page du tableau',
  // top, center, bottom, default
  '&Haut', '&Centré', '&Bas', '&Défaut',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Configuration', '&Largeur préférée:', '&Taille minimum:', '&Remplir couleur:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '&Bordure', 'Côtés &visibles:',  '&Couleur de l''ombre:', 'C&ouleur de la lumière', 'Co&uleur:',
  // Background image
  '&Image...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Position horizontale', 'Position verticale', 'Position dans le texte',
  // Position types: align, absolute, relative
  'Aligner', 'Position absolue', 'Position relative',
  // [Align] left side, center, right side
  'gauche de la boîte vers côté gauche de',
  'centre de la boîte vers centre de',
  'droite de la boîte vers côté droit de',
  // [Align] top side, center, bottom side
  'haut de la boîte vers haut de',
  'centre de la boîte vers centre de',
  'bas de la boîte vers bas de',
  // [Align] relative to
  'relatif à',
  // [Position] to the right of the left side of
  'à droite du côté gauche de',
  // [Position] below the top side of
  'sous le sommet de',
  // Anchors: page, main text area (x2)
  '&Page', '&Espace du texte principal', 'P&age', 'Espace du t&exte principal',
  // Anchors: left margin, right margin
  'Marge &gauche', 'Marge &droite',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Marge du &haut', 'Marge du &bas', 'Marge &intérieure', 'Marge e&xtérieure',
  // Anchors: character, line, paragraph
  '&Caractère', 'L&igne', 'Para&graphe',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Marg&es en miroir', '&Disposition dans cellule',
  // Above text, below text
  '&Au-dessus du texte', '&Sous le texte',
  // Width, Height groupboxes, Width, Height labels
  'Largeur', 'Hauteur', '&Largeur:', '&Hauteur:',
  // Auto height label, Auto height combobox item
  'Automatique', 'automatique',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alignement vertical', '&Sommet', '&Centre', '&Bas',
  // Border and background button and title
  'B&ordure et fond...', 'Boîte pour la bordure et le fond',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Collage spécial', '&Coller en tant que:', 'Format RTF', 'Format HTML',
  'Texte', 'Texte Unicode', 'Image Bitmap', 'Image Metafile',
  'Fichier Image', 'URL',
  // Options group-box, styles
  'Options', '&Styles:',
  // style options: apply target styles, use source styles, ignore styles
  'Appliquer les styles au document cible', 'Garder les styles et l''apparence',
  'Conserver l''apparence, mais ignorer les styles',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Puces et numérotation', 'Puces', 'Numérotation', 'Liste de puces', 'Liste de numérotation',
  // Customize, Reset, None
  '&Personnaliser...', '&Actualiser', 'Aucune',
  // Numbering: continue, reset to, create new
  'Poursuivre la numérotation', 'Reprendre la numérotation à', 'Créer une nouvelle liste en commençant par',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Lettres - majuscules', 'Chiffres romains - majuscules', 'Numérotation', 'Lettres - minuscules', 'Chiffres romains - minuscules', 
  // Bullet type
  'Cercle', 'Disque', 'Carré', 
  // Level, Start from, Continue numbering, Numbering group-box
  '&Niveau:', '&À partir de:', '&Continuer', 'Numérotation', 
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Liste personnalisée', 'Niveau', '&Compteur:', 'Propriétés de la liste', '&Type de liste:',
  // List types: bullet, image
  'Puces', 'Image', 'Insérer un nombre',
  // Number format, Number, Start level from, Font button
  '&Format du numéro:', 'Numéro', '&Commencer le niveau de numérotation par:', '&Police...',
  // Image button, bullet character, Bullet button,
  '&Image...', 'C&aractère pour puce', '&Puce...',
  // Position of list text, bullet, number, image, text
  'Position de la liste de texte', 'Position des puces', 'Position des numéros', 'Position d''image', 'Position du texte',
  // at, left indent, first line indent, from left indent
  '&à :', 'Espace à gauche:', '&Espace première ligne:', 'de l''espace gauche',
  // [only] one level preview, preview
  '&Aperçu niveau 1', 'Aperçu',
  // Preview text
  'Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Alignement à gauche', 'Alignement à droite', 'Centré',
  // level #, this level (for combo-box)
  'niveau %d', 'ce niveau',
  // Marker character dialog title
  'Éditer les caractères de puces',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Bordures et arrière-plan du paragraphe', 'Bordures', 'Arrière-plan',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Configuration', '&Couleur:', '&Largeur:', 'Larg&eur intérieure:', 'Excentrages ...',
  // Sample, Border type group-boxes;
  'Exemple', 'Type de bordure',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Aucun', '&Simple', '&Double', '&Triple', 'Intérieur épais', 'Extérieur épais',
  // Fill color group-box; More colors, padding buttons
  'Remplir avec une couleur', '&Autres couleurs...', '&Marges...',
  // preview text
  'Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte.',
  // title and group-box in Offsets dialog
  'Excentrages', 'Excentrages des bordures',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragraphe', 'Alignement', '&Gauche', '&Droite', '&Centré', '&Justification',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espacement', '&Avant:', 'A&près:', 'E&spacement de lignes:', '&Valeur:',
  // Indents group-box; indents: left, right, first line
  'Indentation', 'G&auche:', 'Droi&te:', '&Première ligne:',
  // indented, hanging
  '&Indentée', '&Accrochée', 'Exemple',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simple', '1.5 ligne', 'Double', 'Au moins', 'Exactement', 'Multiple',
  // preview text
  'Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte. Texte texte texte texte texte.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indentation et espacement', 'Tabulations', 'Enchaînement du texte',
  // tab stop position label; buttons: set, delete, delete all
  '&Positionner une tabulation stop:', '&Définir', '&Effacer', '&Tout effacer',
  // tab align group; left, right, center aligns,
  'Alignement', '&Gauche', '&Droite', '&Centré',
  // leader radio-group; no leader
  'Principal', '(Aucun)',
  // tab stops to be deleted, delete none, delete all labels
  'Effacer la tabulation de fin:', '(Aucune)', 'Toutes',
  // Pagination group-box, keep with next, keep lines together
  'Pagination', '&Assembler avec la suivante', '&Grouper les lignes',
  // Outline level, Body text, Level #
  '&Niveau hiérarchique:', 'Corps du texte', 'Niveau %d', // dg
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Aperçu avant l''impression', 'Largeur de la page', 'Page entière', 'Pages:',
  // Invalid Scale, [page #] of #
  'Veuillez entrer un nombre entre 10 et 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Première page', 'Page précédente', 'Page suivante', 'Dernière page',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espacement des cellules', 'Espacement', 'E&ntre cellules', 'Des bordures du tableau aux cellules',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color

  'Bordures', 'Configuration', '&Couleur:', '&Couleur de lumière:', '&Couleur d''ombre:',
  // Width; Border type group-box;
  '&Largeur:', 'Type de bordure',
  // Border types: none, sunken, raised, flat
  '&Aucune', '&Creuse', '&Ombrée', '&Plate',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Fractionnement', 'Fractionnement',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Spécifier le nombre de lignes et de colonnes', 'Cellules &originales',
  // number of columns, rows, merge before
  'Nombre de &colonnes:', 'Nombre de &lignes:', '&Fusion avant de se dédoubler',
  // to original cols, rows check-boxes
  'fractionnement des colonnes originales', 'fractionnement des lignes originales',
  // Add Rows form -------------------------------------------------------------
  'Ajouter des lignes', 'Nombre de &lignes:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Format', 'Page', 'En-tête et pied de page',
  // margins group-box, left, right, top, bottom
  'Marges (millimètres)', 'Marges (pouces)', '&Gauche:', '&Haut:', '&Droite:', '&Bas:',
  // mirror margins check-box
  '&Marges ',
  // orientation group-box, portrait, landscape
  'Orientation', '&Portrait', 'P&aysage',
  // paper group-box, paper size, default paper source
  'Papier', '&Taille:', '&Source:',
  // header group-box, print on the first page, font button
  'En-tête', '&Texte:', '&En-tête de première page', '&Police...',
  // header alignments: left, center, right
  '&Gauche', '&Centré', '&Droit',
  // the same for footer (different hotkeys)
  'Bas de page', 'Te&xte', 'Bas de page sur première &page', '&Police...',
  '&Gauche', '&Centré', '&Droit',
  // page numbers group-box, start from
  'Nombre de pages', '&À partir de',
  // hint about codes
  'Caractères spéciaux:'#13'&&p - Page ; &&P - Total ; &&d - Date ; &&t - Heure.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Page de code du fichier texte', '&Choisissez l''encodage du fichier:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japonais', 'Chinois simplifié', 'Coréen',
  // chinese (trad), central european, cyrillic, west european
  'Chinois traditionnel', 'Europe centrale et orientale', 'Cyrillique', 'Europe de l''Ouest',
  // greek, turkish, hebrew, arabic
  'Grec', 'Turc', 'Hébreu', 'Arabe',
  // baltic, vietnamese, utf-8, utf-16
  'Balte', 'Vietnamien', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Style visuel', '&Sélectionner le style:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertir en texte', 'Choisissez le &délimiteur:',
  // line break, tab, ';', ','
  'saut de ligne', 'tabulation', 'point-virgule', 'virgule',
  // Table sort form -----------------------------------------------------------
  // error message
  'Le tableau contient des lignes fusionnées qui ne peuvent pas être triées',
  // title, main options groupbox
  'Trier le tableau', 'Trier',
  // sort by column, case sensitive
  '&Trier par colonne:', 'Respecter la &casse',
  // heading row, range or rows
  'Exclure la ligne de &titre', 'Lignes à trier: de %d à %d',
  // order, ascending, descending
  'Ordre', 'Croiss&ant', '&Décroissant',
  // data type, text, number
  'Type', '&Texte', '&Nombre',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Insertion d''un numéro', 'Propriétés', 'Nom du &compteur:', 'Type de &numérotation:',
  // numbering groupbox, continue, start from
  'Numérotation', 'C&ontinuer', '&Démarrer de:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Légende', '&Étiquette:', '&Exclure l''étiquette de légende ',
  // position radiogroup
  'Position', '&Au-dessus de l''objet sélectionné', '&Sous l''objet sélectionné',
  // caption text
  '&Texte de légende:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numérotation', 'Figure', 'Tableau',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Côtés visibles de bordure', 'Bordure',
  // Left, Top, Right, Bottom checkboxes
  'Côté &gauche', '&Sommet', 'Côté &droit', '&Bas',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Redimensionner la colonne', 'Redimensionner la ligne',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Indenter la première ligne', 'Indentation à gauche', 'Indentation à gauche (sauf première)', 'Indentation à droite',
  // Hints on lists: up one level (left), down one level (right)
  'Hausser d''un niveau (gauche)', 'Abaisser d''un niveau (droite)',
  // Hints for margins: bottom, left, right and top
  'Bas', 'Gauche', 'Droite', 'Haut',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Aligner le tableau à gauche', 'Aligner le tableau à droite', 'Centrer le tableau', 'Alignement précis',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Retrait normal', 'Aucun espacement', 'Titre %d', 'Paragraphe de liste',
  // Hyperlink, Title, Subtitle
  'Lien hypertexte', 'Titre', 'Sous-titre',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Emphase', 'Emphase pâle', 'Emphase intense', 'Gras',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citation', 'Citation intense', 'Référence pâle', 'Référence intense',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Bloc de texte', 'Variable HTML', 'Code HTML', 'Acronyme HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Définition HTML', 'Clavier HTML', 'Exemple HTML', 'Machine à écrire HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML préformaté', 'Citation HTML', 'En-tête', 'Pied de page', 'Numéro de page',
  // Caption
  'Légende',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Appel de note de fin', 'Appel de note de pied de page', 'Note de fin de page', 'Note de pied de page',
  // Sidenote Reference, Sidenote Text
  'Référence d''annotation', 'Texte d''annotation',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'couleur', 'couleur d''arrière-plan', 'transparent', 'par défaut', 'couleur du soulignement',
  // default background color, default text color, [color] same as text
  'couleur d''arrière-plan par défaut', 'couleur de texte par défaut', 'comme le texte',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'simple', 'épais', 'double', 'pointillé', 'pointillé épais', 'tirets',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tirets épais', 'tirets longs', 'tirets épais longs',
  // underline types: dash dotted, thick dash dotted,
  'tirets en pointillé', 'tirets en pointillé épais',
  // underline types: dash dot dotted, thick dash dot dotted
  'alternance tirets et points', 'alternance tirets et points épais',
  // sub/superscript: not, subsript, superscript
  'non indice/exposant', 'indice', 'exposant',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mode:', 'hérité', 'gauche à droite', 'droite à gauche',
  // bold, not bold
  'gras', 'pas en gras',
  // italic, not italic
  'italique', 'pas d''italique',
  // underlined, not underlined, default underline
  'souligné', 'non souligné', 'soulignement par défaut',
  // struck out, not struck out
  'biffé', 'non biffé',
  // overlined, not overlined
  'surligné', 'non surligné',
  // all capitals: yes, all capitals: no
  'tout en capitales',  'rien en capitales',
  // vertical shift: none, by x% up, by x% down
  'sans décalage vertical', 'décalage par %d%% vers le haut', 'décalage par %d%% vers le bas',
  // characters width [: x%]
  'largeur des caractères',
  // character spacing: none, expanded by x, condensed by x
  'espacement normal des caractères', 'espacement élargi par %s', 'espacement condensé par %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'police par défaut', 'paragraphe par défaut', 'hérités',
  // [hyperlink] highlight, default hyperlink attributes
  'mettre en surbrillance', 'par défaut',
  // paragraph aligmnment: left, right, center, justify
  'aligné à gauche', 'aligné à droite', 'centré', 'justifié',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'hauteur de la ligne: %d%%', 'espace entre les lignes: %s', 'hauteur de la ligne: au minimum %s',
  'hauteur de la ligne: exactement %s',
  // no wrap, wrap
  'habillage de la ligne désactivé', 'habillage de la ligne',
  // keep lines together: yes, no
  'conserver les lignes ensemble', 'ne pas conserver les lignes ensemble',
  // keep with next: yes, no
  'conserver avec le paragraphe suivant', 'ne pas conserver avec le paragraphe suivant',
  // read only: yes, no
  'lecture seule', 'modifiable',
  // body text, heading level x
  'niveau hiérarchique: corps de texte', 'niveau hiérarchique: %d',
  // indents: first line, left, right
  'retrait de la première ligne', 'retrait à gauche', 'retrait à droite',
  // space before, after
  'espace avant', 'espace après',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulations', 'aucun', 'alignement', 'gauche', 'droite', 'centrer',
  // tab leader (filling character)
  'Points de suite',
  // no, yes
  'non', 'oui',
  // [padding/spacing/side:] left, top, right, bottom
  'gauche', 'haut', 'droite', 'bas',
  // border: none, single, double, triple, thick inside, thick outside
  'aucune', 'simple', 'double', 'triple', 'épaisse à l''intérieur', 'épaisse à l''extérieur',
  // background, border, padding, spacing [between text and border]
  'arrière-plan', 'bordure', 'marge intérieure', 'espacement',
  // border: style, width, internal width, visible sides
  'style', 'épaisseur', 'épaisseur interne', 'côtés visibles',
  // style inspector -----------------------------------------------------------
  // title
  'Inspecteur de styles',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Style', '(Aucun)', 'Paragraphe', 'Police', 'Attributs',
  // border and background, hyperlink, standard [style]
  'Bordure et arrière-plan', 'Lien hypertexte', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Styles', 'Style',
  // name, applicable to,
  '&Nom:', 'Applicable &à:',
  // based on, based on (no &),
  'Sur la &base de:', 'Sur la base de:',
  //  next style, next style (no &)
  'Style pour le para&graphe suivant:', 'Style pour le paragraphe suivant:',
  // quick access check-box, description and preview
  '&Accès rapide', 'Description et a&perçu:',
  // [applicable to:] paragraph and text, paragraph, text
  'paragraphe et texte', 'paragraphe', 'texte',
  // text and paragraph styles, default font
  'Styles de texte et paragraphes', 'Police par défaut',
  // links: edit, reset, buttons: add, delete
  'Modifier', 'Réinitialiser', '&Ajouter', '&Supprimer',
  // add standard style, add custom style, default style name
  'Ajouter un style &standard...', 'Ajouter un style &personnalisé', 'Style %d',
  // choose style
  'Choisir le &style:',
  // import, export,
  '&Importer...', '&Exporter...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Styles RichView (*.rvst)|*.rvst', 'Erreur pendant le chargement du fichier', 'Ce fichier ne contient pas de styles',
  // Title, group-box, import styles
  'Importer les styles d''un fichier', 'Importer', 'I&mporter des styles:',
  // existing styles radio-group: Title, override, auto-rename
  'Styles existants', '&remplacer', '&ajouter en renommant',
  // Select, Unselect, Invert,
  '&Sélectionner', '&Désélectionner', '&Inverser',
  // select/unselect: all styles, new styles, existing styles
  '&Tous les Styles', '&Nouveaux Styles', 'Styles &existants',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Effacer la mise en forme)', '(Tous les styles)',
  // dialog title, prompt
  'Styles', '&Choisissez le style à appliquer:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonymes', '&Ignorer tous', '&Ajouter au dictionnaire',
  // Progress messages ---------------------------------------------------------
  'Téléchargement %s', 'Préparation pour l''impression...', 'Impression de la page %d',
  'Conversion de RTF...',  'Conversion en RTF...',
  'Lecture %s...', 'Écriture %s...', 'texte',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Pas de note', 'Pied de page', 'Note de fin', 'Annotation', 'Boîte de texte'  
  );

initialization
  RVA_RegisterLanguage(
    'French', // english language name, do not translate
    'Français', // native language name
    ANSI_CHARSET, @Messages);

end.

