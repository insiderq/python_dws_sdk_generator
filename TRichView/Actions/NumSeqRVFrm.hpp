﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'NumSeqRVFrm.pas' rev: 27.00 (Windows)

#ifndef NumseqrvfrmHPP
#define NumseqrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <NumSeqCustomRVFrm.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Numseqrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVNumSeq;
class PASCALIMPLEMENTATION TfrmRVNumSeq : public Numseqcustomrvfrm::TfrmRVCustomNumSeq
{
	typedef Numseqcustomrvfrm::TfrmRVCustomNumSeq inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gbNumbering;
	Vcl::Stdctrls::TRadioButton* rbContinue;
	Vcl::Stdctrls::TRadioButton* rbReset;
	Rvspinedit::TRVSpinEdit* seStartFrom;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall seStartFromChange(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _rbContinue;
	Vcl::Controls::TControl* _rbReset;
	Rvstyle::TRVStyle* FRVStyle;
	
public:
	DYNAMIC void __fastcall Localize(void);
	HIDESBASE void __fastcall Init(Rvstyle::TRVStyle* RVStyle, System::Classes::TStringList* SeqPropList, const System::UnicodeString SeqName, Rvstyle::TRVSeqType SeqType);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	HIDESBASE void __fastcall GetSeqProperties(System::UnicodeString &SeqName, Rvstyle::TRVSeqType &SeqType, int &StartFrom, bool &Reset);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVNumSeq(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Numseqcustomrvfrm::TfrmRVCustomNumSeq(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVNumSeq(System::Classes::TComponent* AOwner, int Dummy) : Numseqcustomrvfrm::TfrmRVCustomNumSeq(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVNumSeq(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVNumSeq(HWND ParentWindow) : Numseqcustomrvfrm::TfrmRVCustomNumSeq(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Numseqrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_NUMSEQRVFRM)
using namespace Numseqrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// NumseqrvfrmHPP
