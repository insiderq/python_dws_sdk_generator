﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'StylesRVFrm.pas' rev: 27.00 (Windows)

#ifndef StylesrvfrmHPP
#define StylesrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <System.IniFiles.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVStyleFuncs.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Stylesrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVStyles;
class PASCALIMPLEMENTATION TfrmRVStyles : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Comctrls::TTreeView* tv;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TLabel* lblName;
	Vcl::Stdctrls::TEdit* txtStyleName;
	Vcl::Stdctrls::TComboBox* cmbStyleType;
	Vcl::Stdctrls::TLabel* lblStyleType;
	Vcl::Stdctrls::TLabel* lblParent;
	Vcl::Stdctrls::TComboBox* cmbParent;
	Vcl::Stdctrls::TLabel* lblNext;
	Vcl::Stdctrls::TComboBox* cmbNext;
	Vcl::Stdctrls::TLabel* lblDescr;
	Richview::TRichView* rv;
	Vcl::Stdctrls::TButton* btnImport;
	Rvstyle::TRVStyle* rvs;
	Vcl::Controls::TImageList* ImageList1;
	Vcl::Buttons::TBitBtn* btnAdd;
	Vcl::Menus::TPopupMenu* pmAdd;
	Vcl::Menus::TMenuItem* mitAddCustom;
	Vcl::Menus::TMenuItem* mitAddStandard;
	Vcl::Stdctrls::TButton* btnDelete;
	Vcl::Stdctrls::TCheckBox* cbQuickAccess;
	Vcl::Stdctrls::TButton* btnExport;
	Rvstyle::TRVStyle* rvsImport;
	Richview::TRichView* rvImport;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall tvChange(System::TObject* Sender, Vcl::Comctrls::TTreeNode* Node);
	void __fastcall btnAddClick(System::TObject* Sender);
	void __fastcall mitAddCustomClick(System::TObject* Sender);
	void __fastcall cmbParentClick(System::TObject* Sender);
	void __fastcall btnDeleteClick(System::TObject* Sender);
	void __fastcall cmbStyleTypeClick(System::TObject* Sender);
	void __fastcall mitFontClick(System::TObject* Sender);
	void __fastcall rvJump(System::TObject* Sender, int id);
	void __fastcall mitParagraphClick(System::TObject* Sender);
	void __fastcall mitBorderClick(System::TObject* Sender);
	void __fastcall txtStyleNameExit(System::TObject* Sender);
	void __fastcall mitAddStandardClick(System::TObject* Sender);
	void __fastcall mitHyperlinkClick(System::TObject* Sender);
	void __fastcall cmbNextClick(System::TObject* Sender);
	void __fastcall cbQuickAccessClick(System::TObject* Sender);
	void __fastcall btnExportClick(System::TObject* Sender);
	void __fastcall btnImportClick(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _txtStyleName;
	Vcl::Controls::TControl* _cmbStyleType;
	Vcl::Controls::TControl* _cmbParent;
	Vcl::Controls::TControl* _cmbNext;
	Vcl::Controls::TControl* _btnDelete;
	Vcl::Controls::TControl* _btnAdd;
	Vcl::Controls::TControl* _cbQuickAccess;
	Vcl::Controls::TControl* _btnOk;
	Vcl::Controls::TControl* _tv;
	System::Classes::TComponent* _pmAdd;
	Rvstyle::TRVStyleTemplate* CurrentStyleTemplate;
	Vcl::Comctrls::TTreeNode* CurrentNode;
	Vcl::Dialogs::TSaveDialog* SaveDialog;
	Vcl::Dialogs::TOpenDialog* OpenDialog;
	Rvstyle::TRVStyleTemplateCollection* __fastcall GetStyleTemplates(void);
	void __fastcall DisplayStyle(void);
	void __fastcall UpdateParentCombo(void);
	void __fastcall UpdateNextCombo(void);
	void __fastcall MakePreview(void);
	
protected:
	Rvstyle::TRVStyle* OriginalRVStyle;
	Rvstyle::TRVStyleTemplateCollection* FStandardStyleTemplates;
	
public:
	bool UseCustomImages;
	Rvstylefuncs::TRVStyleTemplateImageIndices ImageIndices;
	Vcl::Imglist::TCustomImageList* Images;
	int FontImageIndex;
	int HyperlinkImageIndex;
	int ParaImageIndex;
	int ParaBorderImageIndex;
	int RootImageIndex;
	void __fastcall Init(Rvedit::TCustomRichViewEdit* rv, Rvstyle::TRVStyleTemplateCollection* StandardStyleTemplates);
	void __fastcall BuildTree(Rvstyle::TRVStyleTemplate* SelectedStyleTemplate);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	__property Rvstyle::TRVStyleTemplateCollection* StyleTemplates = {read=GetStyleTemplates};
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVStyles(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVStyles(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVStyles(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVStyles(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Stylesrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_STYLESRVFRM)
using namespace Stylesrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// StylesrvfrmHPP
