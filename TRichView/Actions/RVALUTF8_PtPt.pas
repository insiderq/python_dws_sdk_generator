﻿// This file is a copy of RVAL_PtPt.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Portuguese (Portuguese) translation             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by:	Gilberto Fonte 2012-07-26			        }
{ Updated by: 		Gilberto Fonte 2012-08-31			        }
{*******************************************************}

unit RVALUTF8_PtPt;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =                            
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'polegadas', 'cm', 'mm', 'picas', 'pixeis', 'pontos',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pi', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Ficheiro', '&Editar', 'F&ormatar', 'Tipo de &letra', '&Parágrafo', '&Inserir', '&Tabela', '&Janela', '&Ajuda',
  // exit
  'Sai&r',
  // top-level menus: View, Tools,
  '&Ver', 'Ferramen&tas',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Tamanho', 'Estilo', 'Selec&ionar', 'Ali&nhamento de Células', 'Limites de Cé&lulas',
  // menus: Table cell rotation
  '&Orientação de Célula',    
  // menus: Text flow, Footnotes/endnotes
  '&Fluxo de texto', 'Notas de &Rodapé',
  // ribbon tabs: tab1, tab2, view, table
  '&Base', '&Avançado', '&Ver', '&Tabela',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Área de Transferência', 'Tipo de Letra', 'Parágrafo', 'Lista', 'Editar',
  // ribbon groups: insert, background, page setup,
  'Inserir', 'Fundo', 'Configurar Página',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Ligações', 'Notas de Rodapé', 'Vistas de Documento', 'Mostrar/Ocultar', 'Zoom', 'Opções',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserir', 'Eliminar', 'Operações', 'Limites',
  // ribbon groups: styles 
  'Estilos',
  // ribbon screen tip footer,
  'Prima F1 para obter mais ajuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Recentes', 'Guardar documento', 'Pré-visualizar e imprimir',
  // ribbon label: units combo
  'Unidades:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Novo', 'Novo|Cria um novo documento em branco',
  // TrvActionOpen
  '&Abrir...', 'Abrir|Abre um documento',
  // TrvActionSave
  '&Guardar', 'Guardar|Guarda o documento',
  // TrvActionSaveAs
  'Guardar &Como...', 'Guardar Como...|Guarda o documento com um novo nome',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta o documento para outro formato',
  // TrvActionPrintPreview
  'Pré-vi&sualizar Impressão', 'Pré-visualizar Impressão|Mostrar o documento como será impresso',
  // TrvActionPrint
  '&Imprimir...', 'Imprimir|Configurar a impressão e imprime o documento',
  // TrvActionQuickPrint
  '&Imprimir', '&Impressão rápida', 'Imprimir|Imprime o documento',
   // TrvActionPageSetup
  'Configurar Pá&gina...', 'Configurar Página|Configurar margens, tamanho de papel, orientação, fonte, cabeçalho e rodapé',
  // TrvActionCut
  'Cor&tar', 'Cortar|Corta a seleção para a Área de Transferência',
  // TrvActionCopy
  '&Copiar', 'Copiar|Copia a seleção para a Área de Transferência',
  // TrvActionPaste
  'Co&lar', 'Colar|Colar o conteúdo da Área de Transferência',
  // TrvActionPasteAsText
  'Colar como &Texto', 'Colar como Texto|Colar apenas o texto do conteúdo da Área de Transferência',
  // TrvActionPasteSpecial
  'Colar E&special...', 'Colar Especial|Colar o conteúdo da Área de Transferência no formato especificado',
  // TrvActionSelectAll
  'Selecionar &Tudo', 'Selecionar Tudo|Seleciona todos os itens',
  // TrvActionUndo
  '&Anular', 'Anular|Anula a última ação',
  // TrvActionRedo
  '&Refazer', 'Refazer|Refaz a última ação anulada',
  // TrvActionFind
  '&Localizar...', 'Localizar|Localiza texto no documento',
  // TrvActionFindNext
  'Localizar &Seguinte', 'Localizar Seguinte|Continua a última localização',
  // TrvActionReplace
  '&Substituir...', 'Substituir|Substitui o texto especificado no documento',
  // TrvActionInsertFile
  '&Ficheiro...', 'Inserir Ficheiro|Inserir o conteúdo do ficheiro no documento',
  // TrvActionInsertPicture
  '&Imagem...', 'Inserir Imagem|Inserir uma imagem',
  // TRVActionInsertHLine
  'Linha &Horizontal', 'Inserir Linha Horizontal|Inserir uma linha horizontal',
  // TRVActionInsertHyperlink
  '&Hiperligação...', 'Inserir Hiperligação|Inserir uma hiperligação',
  // TRVActionRemoveHyperlinks
  '&Remover Hiperligação', 'Remove Hiperligação|Remove todas as hiperligações no texto selecionado',
  // TrvActionInsertSymbol
  '&Símbolo...', 'Inserir Símbolo|Inserir símbolo',
  // TrvActionInsertNumber
  {*} '&Number...', 'Insert Number|Inserts a number',
  // TrvActionInsertCaption
  {*} 'Insert &Caption...', 'Insert Caption|Inserts a caption for the selected object',
  // TrvActionInsertTextBox
  {*} '&Text Box', 'Insert Text Box|Inserts a text box',
  // TrvActionInsertSidenote
  {*} '&Sidenote', 'Insert Sidenote|Inserts a note displayed in a text box',
  // TrvActionInsertPageNumber
  {*} '&Page Number', 'Insert Page Number|Inserts a page number',
  // TrvActionParaList
  '&Marcas e Numeração...', 'Marcas e Numeração|Aplica ou edita marcas ou numerações para os parágrafos selecionados',
  // TrvActionParaBullets
  '&Marcas', 'Marcas|Adicionar ou remove marcas ao parágrafo',
  // TrvActionParaNumbering
  '&Numeração', 'Numeração|Adicionar ou remove numeração ao parágrafo',
  // TrvActionColor
  'Cor da &Pagina...', 'Cor da Página|Aplica a cor para fundo do documento',
  // TrvActionFillColor
  '&Sombreado...', 'Sombreado|Aplica a cor de fundo do texto, parágrafo, célula, tabela ou documento',
  // TrvActionInsertPageBreak
  '&Inserir Quebra de Página', 'Inserir Quebra de Página|Inserir uma quebra de página',
  // TrvActionRemovePageBreak
  '&Remover Quebra de Página', 'Remover Quebra de Página|Remove a quebra de página',
  // TrvActionClearLeft
  'Limpar Fluxo do Texto do Lado &Esquerdo', 'Limpar Fluxo do Texto do Lado Esquerdo|Posiciona este parágrafo abaixo de qualquer figura alinhada pela esquerda',
  // TrvActionClearRight
  'Limpar Fluxo do Texto do Lado &Direito', 'Limpar Fluxo do Texto do Lado Direito|Posiciona este parágrafo abaixo de qualquer figura alinhada pela direita',
  // TrvActionClearBoth
  'Limpar Fluxo do Texto de &Ambos os Lados', 'Limpar Fluxo do Texto de &Ambos os Lados|Posiciona este parágrafo abaixo de qualquer',
  // TrvActionClearNone
  'Fluxo de Texto &Normal', 'Fluxo do Texto Normal|Texto ao redor de figuras alinhadas pela esquerda ou pela direita',
  // TrvActionVAlign
  'Posição do &Objeto...', 'Posição do Objeto|Muda a posição do objeto selecionado',
  // TrvActionItemProperties
  '&Propriedades de Objeto...', 'Propriedades de Objeto|Define propriedades do objeto ativo',
  // TrvActionBackground
  '&Fundo...', 'Fundo|Escolhe a cor e imagem de fundo',
  // TrvActionParagraph
  '&Parágrafo...', 'Parágrafo|Muda os atributos do parágrafo',
  // TrvActionIndentInc
  '&Aumentar Avanço', 'Aumentar Avanço|Aumentar avanço à esquerda dos parágrafos selecionados',
  // TrvActionIndentDec
  '&Diminuir Avanço', 'Diminuir Avanço|Diminuir avanço à esquerda dos parágrafos selecionados',
  // TrvActionWordWrap
  '&Quebra de Linha', 'Quebra de Linha|Liga/Desliga quebra de linha para os parágrafos selecionados',
  // TrvActionAlignLeft
  'Alinhar à &Esquerda', 'Alinhar à Esquerda|Alinha o texto selecionado à esquerda',
  // TrvActionAlignRight
  'Alinhar à &Direita', 'Alinhar à Direita|Alinha o texto selecionado à direita',
  // TrvActionAlignCenter
  'Alinhar ao &Centro', 'Alinhar ao &Centro|Centrar o texto selecionado',
  // TrvActionAlignJustify
  '&Justificar', 'Justificar|Alinha o texto selecionado à direita e à esquerda',
  // TrvActionParaColor
  'Cor de Fu&ndo do Parágrafo...', 'Cor de Fundo do Parágrafo|Configurar cor de fundo de parágrafos',
  // TrvActionLineSpacing100
  'Espaçamento de Linha &Simples', 'Espaçamento de Linha Simples|Configurar espaçamento de linha simples',
  // TrvActionLineSpacing150
  'Espaçamento de 1,5 Lin&has', 'Espaçamento de 1,5 Lin&has|Configurar espaçamento de 1,5 linhas',
  // TrvActionLineSpacing200
  'Espaçamento de Linha &Duplo', 'Espaçamento de Linha Duplo|Configurar espaçamento de linha duplo',
  // TrvActionParaBorder
  '&Limites e Sombreado...', 'Limites e Sombreado|Configurar limites e cor de fundo para os parágrafos selecionados',
  // TrvActionInsertTable
  '&Inserir Tabela...', '&Tabela', 'Inserir Tabela|Inserir uma nova tabela',
  // TrvActionTableInsertRowsAbove
  'Inserir Linha &Acima', 'Inserir Linha Acima|Inserir nova linha acima das células selecionadas',
  // TrvActionTableInsertRowsBelow
  'Inserir Linha A&baixo', 'Inserir Linha Abaixo|Inserir nova linha abaixo das células selecionadas',
  // TrvActionTableInsertColLeft
  'Inserir Coluna à &Esquerda', 'Inserir Coluna à Esquerda|Inserir nova coluna à esquerda das células selecionadas',
  // TrvActionTableInsertColRight
  'Inserir Coluna à &Direita', 'Inserir Coluna à Direita|Inserir nova coluna à direita das células selecionadas',
  // TrvActionTableDeleteRows
  'Eliminar L&inhas', 'Eliminar Linhas|Elimina linhas contendo as células selecionadas',
  // TrvActionTableDeleteCols
  'Eliminar &Colunas', 'Eliminar Colunas|Elimina colunas contendo as células selecionadas',
  // TrvActionTableDeleteTable
  '&Eliminar Tabela', 'Eliminar Tabela|Elimina a tabela',
  // TrvActionTableMergeCells
  '&Unir Células', 'Unir Células|Intercala as células selecionadas',
  // TrvActionTableSplitCells
  '&Dividir Células...', 'Dividir Células|Divide as células selecionadas',
  // TrvActionTableSelectTable
  'Selecionar &Tabela', 'Selecionar Tabela|Seleciona a tabela',
  // TrvActionTableSelectRows
  'Selecionar Lin&has', 'Selecionar Linhas|Seleciona linhas',
  // TrvActionTableSelectCols
  'Selecionar Col&unas', 'Selecionar Colunas|Seleciona colunas',
  // TrvActionTableSelectCell
  'Selecionar Cé&lula', 'Selecionar Célula|Selecionar célula',
  // TrvActionTableCellVAlignTop
  'Alinhar Célula em &Cima', 'Alinhar Célula em Cima|Alinha conteúdo na parte superior da célula',
  // TrvActionTableCellVAlignMiddle
  'Alinhar Célula ao C&entro', 'Alinhar Célula no Centro|Alinha conteúdo na parte central da célula',
  // TrvActionTableCellVAlignBottom
  'Alinhar Célula em &Baixo', 'Alinhar Célula em Baixo|Alinha conteúdo na parte inferior da célula',
  // TrvActionTableCellVAlignDefault
  'Alinhamento Vertical de Célula &Normal', 'Alinhamento Vertical de Célula Normal|Configurar o alinhamento vertical normal para as células selecionadas',
  // TrvActionTableCellRotationNone
  '&Sem Orientação de Célula', 'Sem Orientação de Célula|Orienta o conteúdo da célula para 0°',
  // TrvActionTableCellRotation90
  'Orientação de Célula a &90°', 'Orientação Célula para 90°|Orienta o conteúdo da célula para 90°',
  // TrvActionTableCellRotation180
  'Orientação de Célula a &180°', 'Orientação Célula para 180°|Orienta o conteúdo da célula para 180°',
  // TrvActionTableCellRotation270
  'Orientação de Célula a &270°', 'Orientação Célula para 270°|Orienta o conteúdo da célula para 270°',
  // TrvActionTableProperties
  '&Propriedades da Tabela...', 'Propriedades da Tabela|Alterar as propriedades da tabela selecionada',
  // TrvActionTableGrid
  'Mostrar Linhas de &Grelha', 'Mostrar Linhas de Grelha|Mostrar ou ocultar as linhas de grelha na tabela',
  // TRVActionTableSplit
  'Di&vidir Tabela', 'Dividir Tabela|Divide a tabela em duas tabelas começando na linha selecionada',
  // TRVActionTableToText
  'Converter em Te&xto...', 'Converter em Texto|Converte a tabela em texto normal',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena as linhas da tabela',
  // TrvActionTableCellLeftBorder
  'Limite &Esquerdo', 'Limite Esquerda|Mostrar ou Ocultar o limite esquerdo da célula',
  // TrvActionTableCellRightBorder
  'Limite &Direita', 'Limite Direita|Mostrar ou Ocultar o limite direito da célula',
  // TrvActionTableCellTopBorder
  'Limite &Superior', 'Limite Superior|Mostrar ou Ocultar o limite superior da célula',
  // TrvActionTableCellBottomBorder
  'Limite &Inferior', 'Limite Inferior|Mostrar ou Ocultar o limite inferior da célula',
  // TrvActionTableCellAllBorders
  '&Todos os Limites', 'Todos os Limites|Mostrar ou Ocultar todos os limites da célula',
  // TrvActionTableCellNoBorders
  '&Sem Limites', 'Sem Limites|Ocultar todos os limites da células',
  // TrvActionFonts & TrvActionFontEx
  'Tipo de &Letra...', 'Tipo de Letra|Alterar o tipo de letra do texto selecionado',
  // TrvActionFontBold
  '&Negrito', 'Negrito|Alterar o estilo do texto selecionado para negrito',
  // TrvActionFontItalic
  '&Itálico', 'Itálico|Alterar o estilo do texto selecionado para itálico',
  // TrvActionFontUnderline
  '&Sublinhado', 'Sublinhado|Alterar o estilo do texto selecionado para sublinhado',
  // TrvActionFontStrikeout
  '&Rasurado', 'Rasurado|Desenha um linha no meio do texto selecionado',
  // TrvActionFontGrow
  '&Aumentar Tipo de Letra', 'Aumentar Tipo de Letra|Aumentar o tamanho do tipo de letra selecionado em 10%',
  // TrvActionFontShrink
  'D&iminuir Tipo de Letra', 'Diminuir Tipo de Letra|Diminuir o tamanho do tipo de letra selecionado em 10%',
  // TrvActionFontGrowOnePoint
  'A&umentar Tipo de Letra em Um Ponto', 'Aumentar Tipo de Letra em Um Ponto|Aumentar o tamanho do tipo de letra selecionado em 1 ponto',
  // TrvActionFontShrinkOnePoint
  'D&iminuir Tipo de Letra em Um Ponto', 'Diminuir Tipo de Letra em Um Ponto|Diminuir o tamanho do tipo de letra selecionado em 1 ponto',
  // TrvActionFontAllCaps
  '&Tudo Maiúsculas', 'Tudo Maiúsculas|Alterar todos os caracteres do texto selecionado para maiúsculas',
  // TrvActionFontOverline
  '&SobreLinha', 'SobreLinha|Adicionar linha sobre o texto selecionado',
  // TrvActionFontColor
  '&Cor do Tipo de Letra...', 'Cor do Tipo de Letra|Alterar a cor do texto selecionado',
  // TrvActionFontBackColor
  'Cor de &Realce do Texto...', 'Cor de Realce do Texto|Alterar a cor de fundo do texto selecionado',
  // TrvActionAddictSpell3
  'Verificar &Ortografia', 'Verificar Ortografia|Verifica a ortografia',
  // TrvActionAddictThesaurus3
  '&Sinónimos', 'Sinónimos|Lista de sinónimos para a palavra selecionada',
  // TrvActionParaLTR
  'Esquerda para Direita', 'Esquerda para Direita|Configurar direção do texto da esquerda para direita para parágrafos selecionados',
  // TrvActionParaRTL
  'Direita para Esquerda', 'Direita para Esquerda|Configurar direção do texto da direita para esquerda para parágrafos selecionados',
  // TrvActionLTR
  'Texto Esquerda para Direita', 'Texto Esquerda para Direita|Configurar direção do texto da esquerda para direita para texto selecionado',
  // TrvActionRTL
  'Texto Direita para Esquerda', 'Texto Direita para Esquerda|Configurar direção do texto da direita para esquerda para texto selecionado',
  // TrvActionCharCase
  'Maiúsculas/Minúsculas', 'Maiúsculas/Minúsculas|Alterar para maiúsculas ou minúsculas do texto selecionado',
  // TrvActionShowSpecialCharacters
  'Mostrar &Tudo', 'Mostrar Tudo|Mostrar ou ocultar símbolos ocultos',
  // TrvActionSubscript
  '&Inferior à linha', 'Inferior à linha|Cria letras pequenas por baixo da linha de texto',
  // TrvActionSuperscript
  '&Superior à linha', 'Superior à linha|Cria letras pequenas por cima da linha de texto',
  'Nota de &Rodapé', 'Nota de Rodapé|Inserir uma nota de rodapé',
  // TrvActionInsertEndnote
  'Nota de &Fim', 'Nota de Fim|Inserir uma nota de fim',
  // TrvActionEditNote
  'E&ditar Nota', 'Editar Nota|Editar uma nota de rodapé ou de fim',
  '&Ocultar', 'Ocultar|Ocultar ou Mostrar o texto selecionado',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Estilos...', 'Estilos|Abrir a janela de estilos',
  // TrvActionAddStyleTemplate
  '&Adicionar Estilo...', 'Adicionar Estilo|Criar um novo estilo de texto ou parágrafo',
  // TrvActionClearFormat,
  '&Limpar formatação', 'Limpar Formatação|Limpar todas as formatações no texto ou parágrafo selecionado',
  // TrvActionClearTextFormat,
  'Limpar &Formato de texto', 'Limpar Formato de Texto|Limpar todas as formatações no texto selecionado',
  // TrvActionStyleInspector
  '&Inspetor de Estilos', 'Inspetor de Estilos|Abre ou fecha o inspetor de estilos',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Cancelar', 'Fechar', 'Inserir', '&Abrir...', '&Guardar...', '&Limpar', 'Ajuda', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'percentagem',
  // left, top, right, bottom sides
  'Esquerdo', 'Superior', 'Direito', 'Inferior',
  // save changes? confirm title
  'Deseja guardar as alterações no documento %s?', 'Confirmação',
  // warning: losing formatting
  '%s pode conter recursos não compatíveis com o tipo de ficheiro escolhido.'#13+
  'Deseja guardar o documento neste tipo de ficheiro?',
  // RVF format name
  'Tipo RichView',
  // Error messages ------------------------------------------------------------
  'Erro',
  'Erro ao abrir o ficheiro.'#13#13'Causas possíveis:'#13'- tipo de ficheiro não compatível;'#13+
  '- o ficheiro está corrompido;'#13'- o ficheiro está aberto noutra aplicação.',
  'Erro ao abrir a imagem.'#13#13'Causas possíveis:'#13'- tipo de ficheiro não compatível;'#13+
  '- o ficheiro não é uma imagem;'#13+
  '- o ficheiro está corrompido;'#13'- o ficheiro está aberto noutra aplicação.',
  'Erro ao guardar o ficheiro.'#13#13'Causas possíveis:'#13'- sem espaço em disco;'#13+
  '- disco está protegido contra escrita;'#13'- disco amovível não inserido;'#13+
  '- o ficheiro esta aberto noutra aplicação;'#13'- disco amovível está corrompido.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Ficheiros RichView (*.rvf)|*.rvf',  'Ficheiros RTF (*.rtf)|*.rtf' , 'Ficheiros XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Ficheiros Texto (*.txt)|*.txt', 'Ficheiros Texto - Unicode (*.txt)|*.txt', 'Ficheiros Texto - Deteção automática (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Ficheiros HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'Ficheiros HTML - Simplificado (*.htm;*.html)|*.htm;*.html',
  // DocX
  {*} 'Microsoft Word Documents (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Pesquisa Completa', 'O texto ''%s'' não foi encontrado.',
  // 1 string replaced; Several strings replaced
  '1 item substituído.', '%d itens substituídos.',
  // continue search from the beginning/end?
  'O final do documento foi atingido, continuar do início?',
  'O início do documento foi atingido, continuar do final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparente', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Preto', 'Castanho', 'Verde Azeitona', 'Verde Escuro', 'Azul Petróleo Escuro', 'Azul escuro', 'Indigo', 'Cinza-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Vermelho Escuro', 'Laranja', 'Amarelo Escuro', 'Verde', 'Azul Petróleo', 'Azul', 'Azul-Cinza', 'Cinza-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vermelho', 'Laranja Claro', 'Verde Lima', 'Verde Mar', 'Azul Água', 'Azul Claro', 'Violeta', 'Cinza-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa Choque', 'Dourado', 'Amarelo', 'Verde Brilhante', 'Turquesa', 'Azul Céu', 'Ameixa', 'Cinza-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa', 'Castanho Claro', 'Amarelo Claro', 'Verde Claro', 'Turquesa Claro', 'Azul Pálido', 'Lilás', 'Branco',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparente', '&Auto', '&Mais Cores...', '&Cores Padrão',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fundo', 'C&or:', 'Posição', 'Fundo', 'Texto de exemplo.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nenhum', '&Expandido', 'Lado a Lado F&ixo', '&Lado a Lado', 'C&entrado',
  // Padding button
  '&Ajustar...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Cor Preenchimento', '&Aplicar em:', '&Mais Cores...', '&Ajustar...',
  'Favor selecionar uma cor',
  // [apply to:] text, paragraph, table, cell
  'texto', 'parágrafo' , 'tabela', 'célula',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Tipo de Letra', 'Tipo de Letra', 'Apresentação',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Tipo de Letra:', '&Tamanho', 'Estilo do Tipo de Letra', '&Negrito', '&Itálico',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Cor do Tipo de Letra:', 'Cor do &realce:', '(Padrão)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efeitos', 'Sublinhado', '&Linha superior', 'R&asurado', '&Maiúsculas',
  // Sample, Sample text
  'Pré-visualizar', 'Texto de exemplo',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espaçamento entre Caracteres', '&Por:', '&Expandido', '&Comprimido',
  // Offset group-box, Offset label, Down, Up,
  'Posicionamento', '&Por:', '&Rebaixado', '&Elevado',
  // Scaling group-box, Scaling
  'Escala', '&De:',
  // Sub/super script group box, Normal, Sub, Super
  'Efeitos', '&Normal', '&Inferior à linha', '&Superior à linha',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Ajuste', '&Superior:', '&Esquerda:', '&Inferior:', '&Direita:', 'Valores &iguais',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Inserir Hiperligação', 'Hiperligação', 'T&exto:', '&Destino', '<<seleção>>',
  // cannot open URL
  'Impossível abrir o endereço "%s"',
  // hyperlink properties button, hyperlink style
  '&Personalizar...', '&Estilo:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Atributos de Hiperlink', 'Cores normal', 'Cores ativo',
  // Text [color], Background [color]
  '&Texto:', '&Fundo',
  // Underline [color]
  'S&ublinhado:',   
  // Text [color], Background [color] (different hotkeys)
  'T&exto:', 'F&undo',
  // Underline [color], Attributes group-box,
  'S&ublinhado:', 'Atributos',
  // Underline type: always, never, active (below the mouse)
  'S&ublinhado:', 'sempre', 'nunca', 'ativo',
  // Same as normal check-box
  'Como &normal',
  // underline active links check-box
  '&Sublinhar os links ativos',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Inserir Símbolo', '&Tipo de Letra:', 'Subconjunto:', 'Unicode',
  // Unicode block
  '&Bloco:',
  // Character Code, Character Unicode Code, No Character
  'Cód. Caractere: %d', 'Cód. Caractere: Unicode %d', '(sem caractere)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Inserir Tabela', 'Tamanho Tabela', 'Número de &colunas:', 'Número de &linhas:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Comportamento do ajuste automático', 'Ajuste &Automático', 'Ajuste automático à &janela', 'Largura &manual',
  // Remember check-box
  'Lembrar &dimensões de novas tabelas',
  // VAlign Form ---------------------------------------------------------------
  'Posição',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriedades', 'Imagem', {*}'Position and Size', 'Linha', 'Tabela', 'Linhas', 'Células',
  // Image Appearance, Image Misc, Number tabs
  {*} 'Appearance', 'Miscellaneous', 'Number',
  // Box position, Box size, Box appearance tabs
  {*} 'Position', 'Size', 'Appearance',
  // Preview label, Transparency group-box, checkbox, label
  'Pré-visualização:', 'Transparência', '&Transparente', '&Cor transparente:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'Al&terar...', {*}'&Save...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alinhamento vertical', '&Alinhamento:',
  'inferior à linha de texto', 'meio da linha de texto',
  'superior à linha de texto', 'em linha com o texto', 'meio com o centro da linha de texto',
  // align to left side, align to right side
  'lado esquerdo', 'lado direito',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Deslocação de:', 'Ajustar', '&Largura:', '&Altura:', 'Tamanho padrão: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  {*} 'Scale &proportionally', '&Reset',
  // Inside the border, border, outside the border groupboxes
  {*} 'Inside the border', 'Border', 'Outside the border',
  // Fill color, Padding (i.e. margins inside border) labels
  {*} '&Fill color:', '&Padding:',
  // Border width, Border color labels
  {*} 'Border &width:', 'Border &color:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  {*} '&Horizontal spacing:', '&Vertical spacing:',
  // Miscellaneous groupbox, Tooltip label
  {*} 'Miscellaneous', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Texto alternativo:',
  // Horz line group-box, color, width, style
  'Linha horizontal', '&Cor:', '&Largura:', '&Estilo:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabela', '&Largura:', 'Cor &preenchimento:', 'Espaçamento entre &células...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  {*} '&Horizontal cell padding:', '&Vertical cell padding:', 'Border and background',
  // Visible table border sides button, auto width (in combobox), auto width label
  {*} 'Visible &Border Sides...', 'Auto', 'auto',
  // Keep row content together checkbox
  {*} '&Keep content together',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  {*} 'Rotation', '&None', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  {*} 'Border:', 'Visible B&order Sides...',
  // Border, CellBorders buttons
  'Limite de &Tabela...', 'Limites das &Células...',
  // Table border, Cell borders dialog titles
  'Limite da Tabela', 'Limites das Célula',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Impressão', '&Manter tabela numa página', 'Número de linhas de &cabeçalho:',
  'Linhas de cabeçalho são repetidas no iníco de cada página',
  // top, center, bottom, default
  'Em &Cima', '&Centrado', 'Em &Baixo', '&Normal',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Configurações', '&Largura Preferida:', '&Altura mínima:', 'Cor &Preenchimento:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Limites', 'Limites visíveis:',  'C&or sombra:', 'Cor &luz', 'C&or:',
  // Background image
  '&Imagem...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  {*} 'Horizontal position', 'Vertical position', 'Position in text',
  // Position types: align, absolute, relative
  {*} 'Align', 'Absolute position', 'Relative position',
  // [Align] left side, center, right side
  {*} 'left side of box to left side of',
  {*} 'center of box to center of',
  {*} 'right side of box to right side of',
  // [Align] top side, center, bottom side
  {*} 'top side of box to top side of',
  {*} 'center of box to center of',
  {*} 'bottom side of box to bottom side of',
  // [Align] relative to
  {*} 'relative to',
  // [Position] to the right of the left side of
  {*} 'to the right of the left side of',
  // [Position] below the top side of
  {*} 'below the top side of',
  // Anchors: page, main text area (x2)
  {*} '&Page', '&Main text area', 'P&age', 'Main te&xt area',
  // Anchors: left margin, right margin
  {*} '&Left margin', '&Right margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  {*} '&Top margin', '&Bottom margin', '&Inner margin', '&Outer margin',
  // Anchors: character, line, paragraph
  {*} '&Character', 'L&ine', 'Para&graph',
  // Mirrored margins checkbox, layout in table cell checkbox
  {*} 'Mirror&ed margins', 'La&yout in table cell',
  // Above text, below text
  {*} '&Above text', '&Below text',
  // Width, Height groupboxes, Width, Height labels
  {*} 'Width', 'Height', '&Width:', '&Height:',
  // Auto height label, Auto height combobox item
  {*} 'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  {*} 'Vertical alignment', '&Top', '&Center', '&Bottom',
  // Border and background button and title
  {*} 'B&order and background...', 'Box Border and Background',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Colar Especial', '&Colar como:', 'Texto formatado (RTF)', 'Formato HTML',
  'Texto não formatado', 'Texto unicode não formatado', 'Imagem bitmap', 'Imagem metafile',
  'Ficheiros gráficos', 'Endereço',
  // Options group-box, styles
  'Opções', '&Estilos:',
  // style options: apply target styles, use source styles, ignore styles
  'aplicar estilos ao documento', 'manter estilos e aparência',
  'manter a aparência, ignorar os estilos',  
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Marcas e Numeração', 'Marcas', 'Numeração', 'Listas com Marcas', 'Listas numeradas',
  // Customize, Reset, None
  '&Personalizar...', '&Predefinir', 'Nenhum',
  // Numbering: continue, reset to, create new
  'continuar numeração', 'reiniciar numeração em', 'cria uma nova lista',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Letras Maiúsculas', 'Números Romanos Maiúsculos', 'Decimal', 'Letras Minúsculas', 'Números Romanos Minúsculos',
  // Bullet type
  'Círculo', 'Disco', 'Quadrado',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nível:', '&Iniciar em:', '&Continuar', 'Numeração',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personaliza Lista', 'Níveis', '&Quant.:', 'Propriedades da Lista', 'Tipo de &lista:',
  // List types: bullet, image
  'marcas', 'imagem', 'Inserir Número|',
  // Number format, Number, Start level from, Font button
  'Formato dos &Números:', 'Números', 'Nível &inicial a partir de :', '&Tipo de Letra...',
  // Image button, bullet character, Bullet button,
  '&Imagem...', 'C&aractere da Marca', '&Marca...',
  // Position of list text, bullet, number, image, text
  'Posição do texto de lista', 'Posição da marca', 'Posição da numeração', 'Posição da imagem', 'Posição do texto',
  // at, left indent, first line indent, from left indent
  '&em:', 'Avanço &esquerda:', 'Avanço &primeira linha:', 'do avanço esquerdo',
  // [only] one level preview, preview
  'Pré-visualizar &um nível', 'Pré-visualizar',
  // Preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'alinha à esquerda', 'alinha à direita', 'centrado',
  // level #, this level (for combo-box)
  'Nível %d', 'Este nível',
  // Marker character dialog title
  'Alterar Marca',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Limites e Sombreado do Parágrafo', 'Limites', 'Sombreado',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Configurações', '&Cor:', '&Largura:', 'Largura int&erna:', 'D&eslocamento...',
  // Sample, Border type group-boxes;
  'Exemplo', 'Tipo de limite',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nenhum', '&Simples', '&Duplo', '&Triplo', 'Espesso &interno', 'Espesso &externo',
  // Fill color group-box; More colors, padding buttons
  'Cor de preenchimento', '&Mais cores...', '&Margem...',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // title and group-box in Offsets dialog
  'Deslocamento', 'Deslocado do limite',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Parágrafo', 'Alinhamento', '&Esquerda', '&Direita', '&Centrado', '&Justificado',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espaçamento', '&Antes:', '&Depois:', '&Entre Linhas:', 'E&m:',
  // Indents group-box; indents: left, right, first line
  'Avanço', '&Esquerda:', '&Direita:', '&Primeira linha:',
  // indented, hanging
  '&Avançado', '&Pendente', 'Pré-visualizar',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simples', '1,5 linhas', 'Duplo', 'Pelo menos', 'Exatamente', 'Múltiplo',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Avanços e Espaçamento', 'Tabulações', 'Fluxo de texto',
  // tab stop position label; buttons: set, delete, delete all
  'Posição de &tabulação:', '&Definir', '&Limpar', 'Limpar &Tudo',
  // tab align group; left, right, center aligns,
  'Alinhamento', 'À& esquerda', 'À &direita', 'Ao &centro',
  // leader radio-group; no leader
  'Carácter de Preenchimento', '(Nenhum)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulações a desmarcar:', '(Nenhuma)', 'Todas.',
  // Pagination group-box, keep with next, keep lines together
  'Paginação', '&Manter com o seguinte', 'Manter &linhas juntas',
  // Outline level, Body text, Level #
  '&Nível do contorno:', 'Corpo de Texto', 'Nível %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Pré-visualizar Impressão', 'Largura da Página', 'Página Inteira', 'Páginas:',
  // Invalid Scale, [page #] of #
  'Por favor introduza um número entre 10 e 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primeira Página', 'Página Anterior', 'Página Seguinte', 'Última Página',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espaçamento de Células', 'Espaçamento', 'E&ntre células', 'Do limite da tabela até as células',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Limites', 'Configurações', '&Cor:', 'Cor &Luz:', '&Cor sombra:',
  // Width; Border type group-box;
  '&Largura:', 'Tipo de limite',
  // Border types: none, sunken, raised, flat
  '&Nenhum', '&Baixo relevo', '&Alto relevo', '&Liso',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Dividir células', 'Dividir para',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Número e&specificado', 'Células &originais',
  // number of columns, rows, merge before
  'Número de &colunas:', 'Número de &linhas:', '&Unir células antes de as dividir',
  // to original cols, rows check-boxes
  'Dividir para co&lunas originais', 'Dividir para li&nhas originais',
  // Add Rows form -------------------------------------------------------------
  'Adicionar Linhas', 'Número de &Linhas:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Configurar de Página', 'Página', 'Cabeçalho e Rodapé',
  // margins group-box, left, right, top, bottom
  'Margens (milímetros)', 'Margens (polegadas)', '&Esquerda:', '&Superior:', '&Direita:', '&Inferior:',
  // mirror margins check-box
  'Espelhar &margens',
  // orientation group-box, portrait, landscape
  'Orientação', '&Vertical', '&Horizontal',
  // paper group-box, paper size, default paper source
  'Papel', 'T&amanho:', '&Origem:',
  // header group-box, print on the first page, font button
  'Cabeçalho', '&Texto:', 'C&abeçalho na primeira página', '&Fonte...',
  // header alignments: left, center, right
  '&Esquerda', '&Centrado', '&Direita',
  // the same for footer (different hotkeys)
  'Rodapé', 'Te&xto:', 'Rodapé na primeira &página', 'F&onte...',
  'E&squerda', 'Ce&ntraliza', 'D&ireita',
  // page numbers group-box, start from
  'Números de página', 'Inicia e&m:',
  // hint about codes
  'Combinações de caracteres especiais:'#13'&&p - número de página; &&P - total de páginas; &&d - data atual; &&t - hora atual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Código de Página do Ficheiro de Texto', '&Escolha a codificação do ficheiro:',
  // thai, japanese, chinese (simpl), korean
  'Tailandês', 'Japonês', 'Chinês (Simplificado)', 'Coreano',
  // chinese (trad), central european, cyrillic, west european
  'Chinês (Tradicional)', 'Europeu Central e Oriental', 'Cirílico', 'Europeu Ocidental',
  // greek, turkish, hebrew, arabic
  'Greco', 'Turco', 'Hebreu', 'Arábe',
  // baltic, vietnamese, utf-8, utf-16
  'Báltico', 'Vietnamês', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estilo Visual', '&Selecione o estilo:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Converter em Texto', 'Selecione o &delimitador:',
  // line break, tab, ';', ','
  'quebra de linha', 'tabulação', 'ponto e vírgula', 'vírgula',
  // Table sort form -----------------------------------------------------------
  // error message
  'Não é possível ordenar uma tabela com linhas unidas',
  // title, main options groupbox
  'Ordenar Tabela', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar por coluna:', 'Diferenciar &maiúsculas de minúsculas',
  // heading row, range or rows
  'Eliminar linha de &cabeçalho', 'Linhas a serem ordenadas: de %d até %d',
  // order, ascending, descending
  'Ordem', '&Ascendente', '&Descendente',
  // data type, text, number
  'Tipo', '&Texto', '&Número',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  {*} 'Insert Number', 'Properties', '&Counter name:', '&Numbering type:',
  // numbering groupbox, continue, start from
  {*} 'Numbering', 'C&ontinue', '&Start from:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  {*} 'Caption', '&Label:', '&Exclude label from caption',
  // position radiogroup
  {*} 'Position', '&Above selected object', '&Below selected object',
  // caption text
  {*} 'Caption &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  {*} 'Numbering', 'Figure', 'Table',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  {*} 'Visible Border Sides', 'Border',
  // Left, Top, Right, Bottom checkboxes
  {*} '&Left side', '&Top side', '&Right side', '&Bottom side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Mover a Coluna da Tabela', 'Mover a Linha da Tabela',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Avanço da Primeira Linha', 'Avanço Esquerdo', 'Avanço', 'Avanço Direito',
  // Hints on lists: up one level (left), down one level (right)
  'Um nível acima', 'Um nível abaixo',
  // Hints for margins: bottom, left, right and top
  'Margem inferior', 'Margem esquerda', 'Margem direita', 'Margem superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulação à Esquerda', 'Tabulação à Direita', 'Tabulação Centrada', 'Tabulação Alinhada ao Ponto Decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Avanço Normal', 'Sem Espaçamento', 'Cabeçalho %d', 'Parágrafo da lista',
  // Hyperlink, Title, Subtitle
  'Hiperligação', 'Título', 'Subtítulo',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Ênfase', 'Ênfase Discreto', 'Ênfase Intenso', 'Forte',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citação', 'Citação Intensa', 'Referência Discreta', 'Referência Intensa',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Texto de Bloco', 'Variável HTML', 'Código HTML', 'Acrónimo HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definição HTML', 'Teclado HTML', 'Exemplo de HTML', 'Máquina de Escrever HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Pré-formatado', 'Citação HTML', 'Cabeçalho', 'Rodapé', 'Numero de Página',
  // Caption
  {*} 'Caption',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Ref. de Nota de Fim', 'Ref. de Nota de Rodapé', 'Texto de Nota de Fim', 'Texto de Nota de Rodapé',
  // Sidenote Reference, Sidenote Text
  {*} 'Sidenote Reference', 'Sidenote Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'cor', 'cor de fundo', 'transparente', 'por defeito', 'cor do sublinhado',
  // default background color, default text color, [color] same as text
  'cor de fundo por defeito', 'cor de texto por defeito', 'como texto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'simples', 'grosso', 'duplo', 'pontilhado', 'pontilhado grosso', 'tracejado',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tracejado grosso', 'tracejado longo', 'tracejado longo grosso',
  // underline types: dash dotted, thick dash dotted,
  'traço pontilhado', 'traço pontilhado grosso ',
  // underline types: dash dot dotted, thick dash dot dotted
  'ponto-traço-traco', 'ponto-traço-traco grosso',
  // sub/superscript: not, subsript, superscript
  'sem inferior/superior à linha', 'inferior à linha', 'superior à linha',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mode:', 'inherited', 'esquerda para a direita', 'direita para a esquerda',
  // bold, not bold
  'negrito', 'sem negrito',
  // italic, not italic
  'itálico', 'sem itálico',
  // underlined, not underlined, default underline
  'sublinhado', 'sem sublinhado', 'sublinhado por defeito',
  // struck out, not struck out
  'rasurado', 'sem rasurado',
  // overlined, not overlined
  'linha superior', 'sem linha superior',
  // all capitals: yes, all capitals: no
  'Em maiúsculas', 'Sem maiúsculas',
  // vertical shift: none, by x% up, by x% down
  'Sem deslocamento vertical', 'Elevado por %d%%', 'Rebaixado por %d%%',  
  // characters width [: x%]
  'largura dos caracteres',
  // character spacing: none, expanded by x, condensed by x
  'espaçamento caracteres normal', 'espaçamento caracteres expandido por %s', 'espaçamento caracteres comprimido por %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'tipo de letra por defeito', 'paragrafo por defeito', 'atributos base',
  // [hyperlink] highlight, default hyperlink attributes
  'realce', 'por defeito',
  // paragraph aligmnment: left, right, center, justify
  'alinhar à esquerda', 'alinhar à direita', 'ao centro', 'justificar',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altura da linha: %d%%', 'espaçamento entre linhas: %s', 'altura da linha: pelo menos %s',
  'altura da linha: exatamente %s',
  // no wrap, wrap
  'sem quebra de linhas', 'com quebra de linhas',
  // keep lines together: yes, no
  'manter linhas juntas', 'não manter as linhas juntas',
  // keep with next: yes, no
  'manter com próximo parágrafo', 'não manter com próximo parágrafo',
  // read only: yes, no
  'só de leitura', 'editável',
  // body text, heading level x
  'nível de contorno: corpo de texto', 'nível: %d',
  // indents: first line, left, right
  'avanço de primeira linha', 'avanço à esquerda', 'avanço à direita',
  // space before, after
  'espaçamento antes', 'espaçamento depois',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulações', 'nenhum', 'alinhamento', 'esquerda', 'direita', 'centro',
  // tab leader (filling character)
  'leader',
  // no, yes
  'não', 'sim',
  // [padding/spacing/side:] left, top, right, bottom
  'esquerda', 'cima', 'direita', 'baixo',
  // border: none, single, double, triple, thick inside, thick outside
  'nenhum', 'simples', 'duplo', 'triplo', 'grosso por dentro', 'grosso por fora',
  // background, border, padding, spacing [between text and border]
  'fundo', 'limite', 'espaçamento', 'espaçamento entre o texto e o limite',
  // border: style, width, internal width, visible sides
  'estilo', 'largura', 'largura interna', 'lados visiveis',
  // style inspector -----------------------------------------------------------
  // title
  'Inspetor de Estilos',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estilo', '(Nenhum)', 'Parágrafo', 'Tipo de letra', 'propriedades',
  // border and background, hyperlink, standard [style]
  'Limites e sombreados', 'Hiperligação', '(Padrão)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estilos', 'Estilo',
  // name, applicable to,
  '&Nome:', 'Aplicar &a:',
  // based on, based on (no &),
  '&Baseado em:', 'Baseado em:',
  //  next style, next style (no &)
  'Estilo para o &parágrafo seguinte:', 'Estilo para o parágrafo seguinte:',
  // quick access check-box, description and preview
  '&Adicionar aos estilos rápidos', 'Descrição e &pré-visualização:',
  // [applicable to:] paragraph and text, paragraph, text
  'parágrafo e texto', 'parágrafo', 'texto',
  // text and paragraph styles, default font
  'Estilo de Texto e Paragrafo', 'Tipo de letra por defeito',
  // links: edit, reset, buttons: add, delete
  'Editar', 'Reiniciar', '&Adicionar', '&Eliminar',
  // add standard style, add custom style, default style name
  'Adicionar um estilo &modelo...', 'Adicionar um estilo &personalizado', 'Estilo %d',
  // choose style
  'Escolher &estilo:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Styles (*.rvst)|*.rvst', 'Erro ao carregar o ficheiro', 'Este ficheiro não contém estilos',
  // Title, group-box, import styles
  'Importar Estilos de Ficheiro', 'Importar', 'Importar estilos:',
  // existing styles radio-group: Title, override, auto-rename
  'Estilos existentes' , '&substituir', '&auto renomear',
  // Select, Unselect, Invert,
  '&Selecionar', '&Desselecionar', '&Inverter',
  // select/unselect: all styles, new styles, existing styles
  '&Todos os estilos', '&Novos estilos', '&Estilos Existentes',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Limpar tudo', 'Todos os Estilos',
  // dialog title, prompt
  'Estilos', '&Escolher o estilo a aplicar:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sinónimos', '&Ignorar Todos', '&Adicionar ao Dicionário',
  // Progress messages ---------------------------------------------------------
  'A descarregar %s', 'A preparar a impressão...', 'A imprimir a página %d',
  'A converter de RTF...',  'A converter para RTF...',
  'A ler %s...', 'A escrever %s...', 'texto',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  {*} 'No Note', 'Footnote', 'Endnote', 'Sidenote', 'Text Box'  
  );


initialization
  RVA_RegisterLanguage(
    'Portuguese (Portuguese)', // english language name, do not translate
    {*} 'Portuguese (Portuguese)', // native language name
    ANSI_CHARSET, @Messages);

end.