{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Slovak translation                              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Miroslav �tipkala 2003-05-20           }
{                znalec@znalec.sk                       }
{                www.znalec.sk                          }
{                                                       }
{ Updated: 2006-12-16                                   }
{ Updated: 2011-10-17                                   }
{*******************************************************}
{ Translated by: RoTurSoft, s.r.o. 2013-10-09           }
{                info@rotursoft.sk                      }
{                www.rotursoft.sk                       }
{ Updated: 2014-03-20                                   }
{*******************************************************}

unit RVAL_Slovak;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'palce(Inch)', 'cm', 'mm', 'Pica', 'Pixely', 'Body',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&S�bor', '�pr&avy', '&Form�t', '&P�smo', '&Odstavec', '&Vlo�i�', '&Tabu�ka', '&Okno', '&N�poveda',
  // exit
  '&Koniec',
  // top-level menus: View, Tools,
  '&Zobrazi�', '&N�stroje',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Ve�kos�', '�t�l', 'Vybr&a�', 'Zarovnana&nie obsahu bunky', 'Ohrani�&enie bunky',
  // menus: Table cell rotation
  '&Rot�cia bunky',   
  // menus: Text flow, Footnotes/endnotes
  'Obteka� &Text', '&Pozn�mky pod �arou',
  // ribbon tabs: tab1, tab2, view, table
  '&Domov', '&Pokro�il�', '&Zobrazenie', '&Tabu�ka',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Schr�nka', 'P�smo', 'Odstavec', 'Zoznamy', '�pravy',
  // ribbon groups: insert, background, page setup,
  'Vlo�i�', 'Pozadie', 'Str�nka nastavenie',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Odkazy', 'Pozn�mky pod �iarou', 'Zobrazenie dokumentov', 'Zobrazi�/Skry�', 'Lupa', 'Nastavenie',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Vlo�i�', 'Odstr�ni�', '�pravy', 'Ohrani�enie',
  // ribbon groups: styles 
  '�t�ly',
  // ribbon screen tip footer,
  'Stla�te F1 pre n�povedu',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Naposledy pou�it�', 'Ulo�i� ako', 'N�h�ad a tla� dokumentu',
  // ribbon label: units combo
  'Jednotky:',          
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nov�', 'Nov�|Vytvor� nov� dokument',
  // TrvActionOpen
  '&Otvori�...', ' Otvori� |Otvor� existuj�ci dokument',
  // TrvActionSave
  '&Ulo�i�', 'Ulo�i�|Ulo�� dokument',
  // TrvActionSaveAs
  'Ul&o�i� ako...', 'Ulo�i� ako...|Ulo�� dokument pod nov�m n�zvom',
  // TrvActionExport
  '&Export...', 'Export|Export dokumentu do s�boru in�ho form�tu',
  // TrvActionPrintPreview
  'N�&h�ad', 'N�h�ad|Zobraz� dokument v tvare, v akom bude vytla�en�',
  // TrvActionPrint
  '&Tla�...', 'Tla�|Vytla�� dokument na tla�iare�',
  // TrvActionQuickPrint
  '&R�chla tla�', 'R�chla tla�', 'Tla�|Vytla�� dokument na tla�iare�',
   // TrvActionPageSetup
  '&Vzh�ad str�nky', 'Vzh�ad str�nky|Nastav� okraje dokumentu, ve�kos� papiera, orient�ciu, zdroj, z�hlavie a p�ta',
  // TrvActionCut
  'V&ystrihn��', 'Vystrihn��|Vystrihne vybran� text, alebo objekt a vlo�� ho do schr�nky',
  // TrvActionCopy
  '&Kop�rova�', 'Kop�rova�|Skop�ruje vybran� text alebo objekt do schr�nky',
  // TrvActionPaste
  '&Prilepi�', 'Prilepi�|Vlo�� text alebo objekt zo schr�nky na vybran� miesto',
  // TrvActionPasteAsText
  'Prilepi� ako &Text', 'Prilepi� ako Text|Vlo�� obsah schr�nky ve form�te jednoduch�ho textu',  
  // TrvActionPasteSpecial
  'Prilepi� �&peci�lne...', 'Prilepi� �peci�lne|Vlo�� obsah schr�nky v �peci�lnom form�te',
  // TrvActionSelectAll
  '&Vybra� v�etko', 'Vybra� v�etko|Vyberie v�etky polo�ky dokumentu',
  // TrvActionUndo
  '&Sp�', 'Sp�|Vr�ti sp� posledn� akciu',
  // TrvActionRedo
  '&Znovu', 'Znovu|Prevedie znovu posledn� vr�ten� akciu',
  // TrvActionFind
  '&H�ada�...', 'H�ada�|Vyh�ad� textov� re�azec v dokumente',
  // TrvActionFindNext
  '&H�ada� �alej', 'H�ada� �alej|Vyh�ad� nasleduj�ci v�skyt h�adan�ho textu v dokumente',
  // TrvActionReplace
  '&Nahradi�...', 'Nahradi�|Nahrad� h�adan� text v dokumente in�m textom',
  // TrvActionInsertFile
  '&S�bor...', 'Vlo�i� S�bor|Vlo�� obsah S�boru do dokumentu',
  // TrvActionInsertPicture
  '&Obr�zok...', 'Vlo�i� obr�zok|Vlo�� obr�zok do dokumentu',
  // TRVActionInsertHLine
  'Od&de�ova�', 'Vlo�i� odde�ova�|Vlo�� vodorovn� odde�ovaciu �iaru',
  // TRVActionInsertHyperlink
  'H&ypertextov� odkaz...', 'Vlo�i� hypertextov� odkaz|Vlo�� hypertextov� odkaz na WWW str�nku alebo S�bor',
  // TRVActionRemoveHyperlinks
  'O&dstr�ni� hypertextov� odkazy', 'Odstr�ni� hypertextov� odkazy|Odstr�ni v�etky hypertextov� odkazy z ozna�en�ho textu',  
  // TrvActionInsertSymbol
  '&Symbol...', 'Vlo�i� symbol|Vlo�� �peci�lny znak v�berom z tabu�ky znakov',
  // TrvActionInsertNumber
  '&��slo...', 'Vlo�i� ��slo|Vlo�i� ��slo',
  // TrvActionInsertCaption
  'Vlo�i� &popis...', 'Vlo�i� popis|Vlo�i� popis pre zvolen� objekt',
  // TrvActionInsertTextBox
  '&Textov� oblas�', 'Vlo�i� textov� oblas�|Vlo�i� textov� oblas�',
  // TrvActionInsertSidenote
  '&Bo�n� pozn�mka', 'Vlo�i� bo�n� pozn�mku|Vlo�� pozn�mku do textov�ho po�a zobrazen�ho na bo�nej strane',
  // TrvActionInsertPageNumber
  '��slo stran&y', 'Vlo�i� ��slo strany|Vlo�� ��slo strany',
  // TrvActionParaList
  'Od&r�ky a ��slov�nie...', 'Odr�ky a ��slov�nie|Defin�cie �t�lu odr�ok a ��slovania vybran�ho odstavca',
  // TrvActionParaBullets
  'Od&r�ky', 'Odr�ky|Vlo�� alebo odoberie odr�ku pre vybran� odstavec',
  // TrvActionParaNumbering
  '��s&lovanie', '��slovanie|Vlo�� alebo odoberie ��slovanie pre vybran� odstavec',
  // TrvActionColor
  '&Farba pozadia dokumentu...', 'Farba pozadia dokumentu|Zmen� farbu pozadia dokumentu',
  // TrvActionFillColor
  'Farba &pozadia...', 'Farba pozadia|Zmen� farbu pozadia textu, odstavca, bunky, tabu�ky, alebo dokumentu',
  // TrvActionInsertPageBreak
  '&Vlo�i� odde�ova� str�nky', 'Vlo�i� odde�ova� str�nky|Vlo�� odde�ova� str�nky',
  // TrvActionRemovePageBreak
  '&Odstr�ni� odde�ova� str�nky', 'Odstr�ni� odde�ova� str�nky|Odstr�ni odde�ova� str�nky',
  // TrvActionClearLeft
  'Neobteka� text z &�ava', 'Neobteka� text z�ava|Umiestni odstavec pod ak�ko�vek obr�zok zarovnan� v�avo',
  // TrvActionClearRight
  'Neobteka� text z &prava', 'Neobteka� text zprava|Umiestni odstavec pod ak�ko�vek obr�zok zarovnan� vpravo',
  // TrvActionClearBoth
  'Neo&bteka� text z�ava ani zprava', 'Neobteka� text z�ava ani zprava|Umiestni odstavec pod ak�ko�vek obr�zok zarovnan� v�avo alebo vpravo',
  // TrvActionClearNone
  '&Norm�lne obtekanie textu', 'Norm�lne obtekanie textu|Umo�n� obtekanie textu okolo obr�zkov zarovnan�ch v�avo aj vpravo',
  // TrvActionVAlign
  'Po&z�cia objektu...', 'Poz�cia objektu|Zmen� poz�ciu vybran�ho objektu',    
  // TrvActionItemProperties
  '&Vlastnosti objektu...', 'Vlastnosti objektu|Nastav� vlastnosti vybran�ho objektu',
  // TrvActionBackground
  '&Pozadie dokumentu...', 'Pozadie dokumentu|Nastav� obr�zok, farbu pozadia dokumentu',
  // TrvActionParagraph
  '&Odstavec...', 'Odstavec|Nastavenie vlastnost� vybran�ho odstavca',
  // TrvActionIndentInc
  'Z&v��i� odsadenie', ' Zv��i� odsadenie| Zv��� vzdialenos� vybran�ho odstavca od �av�ho okraja str�nky',
  // TrvActionIndentDec
  'Z&men�i� odsadenie', 'Zmen�i� odsadenie|Zmen�� vzdialenos� vybran�ho odstavca od �av�ho okraja str�nky',
  // TrvActionWordWrap
  'Za&lamovanie riadku', 'Zalamovanie riadku|Nastav� alebo zru�� zalamovanie riadku vybran�ho odstavca',
  // TrvActionAlignLeft
  'Zarovna� v&�avo', 'Zarovna� v�avo|Zarovn� text vybran�ho odstavca k �av�mu okraju str�nky',
  // TrvActionAlignRight
  'Zarovna� v&pravo', 'Zarovna� vpravo|Zarovn� text vybran�ho odstavca k prav�mu okraju str�nky',
  // TrvActionAlignCenter
  'Zarovna� na &stred', 'Zarovna� na stred|Zarovn� text vybran�ho odstavca na stred str�nky',
  // TrvActionAlignJustify
  'Zarovna� do &bloku', 'Zarovna� do bloku|Zarovn� text vybran�ho odstavca do bloku',
  // TrvActionParaColor
  '&Farba pozadia odstavca...', 'Farba pozadia odstavca|Zmen� farbu pozadia vybran�ho odstavca',
  // TrvActionLineSpacing100
  '&Jednoduch� riadkovanie', 'Jednoduch� riadkovanie |Nastav� jednoduch� riadkovanie odstavca',
  // TrvActionLineSpacing150
  '&Riadkovanie 1,5 riadku ', ' Riadkovanie 1.5 riadku|Nastav� riadkovanie odstavca na v��ku 1.5 n�sobku v��ky riadku',
  // TrvActionLineSpacing200
  '&Dvojit� riadkovanie', 'Dvojit� riadkovanie|Nastav� riadkovanie odstavca na dvojit� v��ku riadku',
  // TrvActionParaBorder
  '&Ohrani�enie a pozadie odstavca...', 'Ohrani�enie a pozadie odstavca|Nastav� ohrani�enie a pozadie vybran�ho odstavca',
  // TrvActionInsertTable
  '&Vlo�i� tabu�ku...', '&Tabu�ka', 'Vlo�i� tabu�ku|Vlo�� nov� tabu�ku',
  // TrvActionTableInsertRowsAbove
  'Vlo�i� &riadok pred', 'Vlo�i� riadok pred|Vlo�� nov� riadok do tabu�ky pred vybran� bunku',
  // TrvActionTableInsertRowsBelow
  'Vlo�i� riadok &za', 'Vlo�i� riadok za|Vlo�� nov� riadok do tabu�ky za vybran� bunku',
  // TrvActionTableInsertColLeft
  'Vlo�i� st�pec v&�avo', 'Vlo�i� st�pec v�avo|Vlo�� st�pec v�avo od vybranej bunky',
  // TrvActionTableInsertColRight
  'Vlo�i� st�pec v&pravo', 'Vlo�i� st�pec vpravo|Vlo�� st�pec vpravo od vybranej bunky',
  // TrvActionTableDeleteRows
  'Odstr�ni� r&iadky', 'Odstr�ni� riadky|Odstr�ni vybran� riadky z tabu�ky',
  // TrvActionTableDeleteCols
  'Odstr�ni� &st�pce', 'Odstr�ni� st�pce|Odstr�ni vybran� st�pce z tabu�ky',
  // TrvActionTableDeleteTable
  '&Odstr�ni� tabu�ku', 'Odstr�ni� tabu�ku|Odstr�ni tabu�ku',
  // TrvActionTableMergeCells
  '&Zl��it bunky', 'Zl��i� bunky|Zl��i vybran� bunky do jednej',
  // TrvActionTableSplitCells
  '&Rozdeli� bunku...', 'Rozdeli� bunku|Rozdel� bunku na po�adovan� po�et buniek',
  // TrvActionTableSelectTable
  '&Vybra� tabu�ku', 'Vybra� tabu�ku|Vyberie cel� tabu�ku',
  // TrvActionTableSelectRows
  'Vybra� &riadok', 'Vybra� riadok|Urob� v�ber cel�ho riadku',
  // TrvActionTableSelectCols
  'Vybra� &st�pec', 'Vybra� st�pec| Urob� v�ber cel�ho st�pca',
  // TrvActionTableSelectCell
  'Vybra� &bunku', 'Vybra� bunku| Urob� v�ber celej bunky',
  // TrvActionTableCellVAlignTop
  'Zarovna� bunku &hore', 'Zarovna� bunku hore|Zarovn� zobrazovan� text v bunke k jej horn�mu okraju',
  // TrvActionTableCellVAlignMiddle
  'Zarovna� bunku na &stred', 'Zarovna� bunku na stred|Zarovn� zobrazovan� text v bunke na stred',
  // TrvActionTableCellVAlignBottom
  'Zarovna� bunku &dolu', 'Zarovna� bunku dolu|Zarovn� zobrazovan� text v bunke k jej doln�mu okraju',
  // TrvActionTableCellVAlignDefault
  '�tandartn� &zarovnanie bunky', ' �tandartn� zarovnanie bunky|Nastav� �tandartn� zvisl� zarovnanie bunky',
  // TrvActionTableCellRotationNone
  '&Bez rot�cie bunky', 'Bez rot�cie bunky|Rot�cia obsahu bunky o 0�',
  // TrvActionTableCellRotation90
  'Rot�cia bunky o &90�', 'Rot�cia bunky o 90�|Rot�cia obsahu bunky o 90�',
  // TrvActionTableCellRotation180
  'Rot�cia bunky o &180�', 'Rot�cia bunky o 180�|Rot�cia obsahu bunky o 180�',
  // TrvActionTableCellRotation270
  'Rot�cia bunky o &270�', 'Rot�cia bunky o 270�|Rot�cia obsahu bunky o 270�',
  // TrvActionTableProperties
  '&Vlastnosti tabu�ky...', 'Vlastnosti tabu�ky|Nastav� vlastnosti tabu�ky',
  // TrvActionTableGrid
  'Zobrazi� &mrie�ku', 'Zobrazit mrie�ku|Zobraz� alebo skryje mrie�ku tabu�ky',
  // TRVActionTableSplit
  'Roz&deli� tabu�ku', 'Rozdeli� tabu�ku|Rozdel� tabu�ku na dve tabu�ky od vybran�ho riadku',
  // TRVActionTableToText
  'Prevod do te&xtu...', 'Prevod do textu |Prevedie tabu�ku na text',
  // TRVActionTableSort
  '&Zoradi�...', 'Zoradi�|Zorad� riadky v tabu�ke',
  // TrvActionTableCellLeftBorder
  'Ohrani�enie v&�avo', 'Ohrani�enie v�avo|Zobraz�, alebo skryje �av� ohrani�enie bunky',
  // TrvActionTableCellRightBorder
  'Ohrani�enie v&pravo', 'Ohrani�enie vpravo|Zobraz�, alebo skryje prav� ohrani�enie bunky',
  // TrvActionTableCellTopBorder
  'Ohrani�enie &hore', 'Ohrani�enie hore|Zobraz�, alebo skryje horn� ohrani�enie bunky',
  // TrvActionTableCellBottomBorder
  'Ohrani�enie &dole', 'Ohrani�enie dole|Zobraz�, alebo skryje spodn� ohrani�enie bunky',
  // TrvActionTableCellAllBorders
  'Ohrani�enie d&okola', 'Ohrani�enie dokola|Zobraz�, alebo skryje ohrani�enie celej bunky',
  // TrvActionTableCellNoBorders
  '&Bez ohrani�enia', 'Bez ohrani�enia|Skryje cel� ohrani�enie vybranej bunky',
  // TrvActionFonts & TrvActionFontEx
  '&P�smo...', 'P�smo|Zmena nastavenia p�sma vybran�ho textu',
  // TrvActionFontBold
  '&Tu�n�', 'Tu�n�|Nastav�, alebo zru�� tu�n� zobrazovanie vybran�ho textu',
  // TrvActionFontItalic
  '&Kurz�va', 'Kurz�va|Nastav�, alebo zru�� zobrazovanie vybran�ho textu kurz�vou',
  // TrvActionFontUnderline
  'Po&d�iarknut�', 'Pod�iarknut�|Nastav�, alebo zru�� pod�iarknutie vybran�ho textu',
  // TrvActionFontStrikeout
  'P&re�krtnut�', 'Pre�krtnut�|Nastav�, alebo zru�� pre�krtnutie vybran�ho textu',
  // TrvActionFontGrow
  'Z&v��i� p�smo', 'Zv��i� p�smo|Zv��� v��ku vybran�ho textu o 10%',
  // TrvActionFontShrink
  'Z&men�i� p�smo', 'Zmen�i� p�smo|Zmen�� v��ku vybran�ho textu o 10%',
  // TrvActionFontGrowOnePoint
  'Zv&��i� p�smo o 1 bod', 'Zv��it p�smo o 1 bod|Zv��� v��ku vybran�ho textu o 1 bod',
  // TrvActionFontShrinkOnePoint
  'Z&men�i� p�smo o 1 bod', 'Zmen�i� p�smo o 1 bod|Zmen�� v��ku vybran�ho textu o 1 bod',
  // TrvActionFontAllCaps
  '&V�etky ve�k�', 'V�etky ve�k�|Zmen� �t�l p�sma vybran�ho textu na v�etky ve�k� znaky',
  // TrvActionFontOverline
  'Nad&�iarknut�', ' Nad�iarknut�|Nastav�, alebo zru�� vodorovn� linku nad vybran�m textom',
  // TrvActionFontColor
  '&Farba p�sma...', 'Farba p�sma|Zmen� farbu vybran�ho textu',
  // TrvActionFontBackColor
  'Farba &pozadia p�sma...', 'Farba pozadia p�sma|Zmen� farbu pozadia vybran�ho textu',
  // TrvActionAddictSpell3
  '&Kontrola pravopisu', 'Kontrola pravopisu|Prevedie kontrolu pravopisu vybran�ho textu',
  // TrvActionAddictThesaurus3
  '&Synonym�', 'Synonyma|Pon�kne synonyma pre vybran� slovo',
  // TrvActionParaLTR
  'Z�ava do prava', 'Z�ava do Prava|Nastav� smer textu z�ava -do- prava',
  // TrvActionParaRTL
  'Zprava do �ava', 'Zprava do �ava|Nastav� smer textu zprava -do- �ava',
  // TrvActionLTR
  'Text z�ava do prava', 'Text z�ava do prava|Nastav� smer textu z�ava -do- prava pre ozna�en� text',
  // TrvActionRTL
  'Text zprava do �ava', 'Text zprava do �ava|Nastav� smer textu zprava -do- �ava pre ozna�en� text',
  // TrvActionCharCase
  'Ve�k� / mal� p�smen�', 'Ve�k� / mal� p�smen�|Zmen� ve�kos� p�smen vybran�ho textu',
  // TrvActionShowSpecialCharacters
  '&Netla�ite�n� znaky', 'Netla�ite�n� znaky|Zobrazit alebo skry� netla�ite�n� znaky, napr. znaky odstavcov, znaky tabul�torov a medzery',
  // TrvActionSubscript
  '&Horn� index', 'Horn� index|Prevedie vybran� text na horn� index - subscript',
  // TrvActionSuperscript
  '&Doln� index', 'Doln� index|Prevedie vybran� text na doln� index - superscript',
  // TrvActionInsertFootnote
  '&Pozn�mka pod �iarou', 'Pozn�mka pod �iarou|Vlo�� pozn�mku pod �iarou',
  // TrvActionInsertEndnote
  '&Vysvetlivka', 'Vysvetlivka|Vlo�� vysvetlivku',
  // TrvActionEditNote
  'E&ditova� pozn�mku/vysvetlivku', 'E&ditova� pozn�mku/vysvetlivku|Umo��uje editova� pozn�mku/vysvetlivku',
  '&Skry�', 'Skry�|Skryje alebo zobraz� ozna�en� fragment',            
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&�t�ly...', '�t�ly|Otvori� spr�vcu �t�lov',
  // TrvActionAddStyleTemplate
  'Prid&a� �t�ly...', 'Prida� �t�ly|Vytvori� nov� text alebo odstavcov� �t�l',
  // TrvActionClearFormat,
  'Vymaza� &form�t', 'Vymaza� form�t|Vymaza� cel� text a form�tovanie odseku z vybran�ho fragmentu',
  // TrvActionClearTextFormat,
  'Vymaza� form�t &textu', 'Vymaza� form�tovanie ozna�en�ho textu',
  // TrvActionStyleInspector
  '&In�pektor �t�lov', 'In�pektor �t�lov|Zobraz� alebo skryje in�pektora �t�lov',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Storno', 'Zavrie�', 'Vlo�i�', '&Otvori�...', '&Ulo�i�...', '&Vymaza�', 'N�poveda', 'Odobra�',
  // Others  -------------------------------------------------------------------
  // percents
  'percent�',
  // left, top, right, bottom sides
  '�av� strana', 'Horn� strana', 'Prav� strana', 'Spodn� strana',
  // save changes? confirm title
  'Ulo�i� uroben� zmeny do %s?', 'Potvrdi�',
  // warning: losing formatting
  '%s obsiahnut� objekt, alebo form�tovanie nie je podporovan� vo vybranom form�te s�boru.'#13+
  'Naozaj chcete pre ulo�enie pou�i� tento form�t?',
  // RVF format name
  'RichView Form�t',
  // Error messages ------------------------------------------------------------
  'Chyba',
  'Chyba pri na��tavan� s�boru.'#13#13'Mo�n� pr��iny:'#13'- form�t otv�ran�ho s�boru nie je podporovan� v tejto aplik�cii;'#13+
  '- S�bor je po�koden�;'#13'- S�bor je otvoren� a uzamknut� inou aplik�ciou.',
  'Chyba pri na��tavan� obr�zku.'#13#13'Mo�n� pr��iny:'#13'- form�t obr�zku nie je podporovan� touto aplik�ciou;'#13+
  '- vybran� s�bor neobsahuje obr�zok;'#13+
  '- S�bor je po�koden�;'#13'- S�bor je otvoren� a uzamknut� inou aplik�ciou.',
  'Chyba pri ukladan� S�boru.'#13#13'Mo�n� pr��iny:'#13'- nie je miesto na disku;'#13+
  '- disk je chr�nen� proti z�pisu;'#13'- v�menn� m�dium nie je vlo�en�;'#13+
  '- S�bor je otvoren� a uzamknut� inou aplik�ciou;'#13'- v�menn� m�dium je po�koden�.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView S�bory (*.rvf)|*.rvf',  'RTF S�bory (*.rtf)|*.rtf' , 'XML S�bory (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Textov� S�bory (*.txt)|*.txt', 'Textov� S�bory - Unicode (*.txt)|*.txt', 'Textov� S�bory - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML S�bory (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - zjednodu�en� (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Dokumenty Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'H�adanie dokon�en�', 'H�adan� re�azec ''%s'' nen�jden�.',
  // 1 string replaced; Several strings replaced
  '1 re�azec nahraden�.', '%d re�azcov nahraden�ch.',
  // continue search from the beginning/end?
  'Byl dosiahnut� koniec dokumentu, chcete pokra�ova� od za�iatku?',
  'Byl dosiahnut� za�iatok dokumentu, chcete pokra�ovat od konca?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'nie je', 'automatick�',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '�ierna', 'Hned�', 'Olivovo zelen�', 'Tmavo zelen�', 'Tmavo �edo zelen�', 'Tmavo modr�', 'Indigovo modr�', '�ed�-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tmavo �erven�', 'Oran�ov�', 'Tmavo �lt�', 'Zelen�', '�edo zelen�', 'Modr�', 'Modro �ed�', '�ed�-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  '�erven�', 'Svetlo oran�ov�', '�lto zelen�', 'Morsk� zele�', 'Akvamar�nov�', 'Svetlo modr�', 'Fialov�', '�ed�-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Ru�ov�', 'Zlat�', 'Zlat�', 'Jasno zelen�', 'Tyrkysov�', 'Nebesky modr�', 'Slivkov�', '�ed�-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Svetlo ru�ov�', '�lto oran�ov�', 'Svetlo �lt�', 'Svetlo zelen�', 'Svetlo tyrkysov�', 'Bledo modr�', 'Levandulov�', 'Biela',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparentn�', '&Automatick�', '&Viac farieb...', '�&tandardn�',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Pozadie', 'F&arba:', 'Poz�cia obr�zku', 'N�h�ad', 'Uk�ka textu.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '�iadny', 'Cel� okno', 'Ukotven� dla�d&ice', '&Dla�dice', 'Na &stred',
  // Padding button
  'Odsadenie text&u...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Farba pozadia', 'Pou�i� n&a:', 'Viac farie&b...', 'Odsadenie text&u...',
  'Pros�m, vyberte farbu',
  // [apply to:] text, paragraph, table, cell
  'text', 'odstavec' , 'tabu�ku', 'bunku',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'P�smo', 'P�smo', 'Prelo�enie znakov',
  // Font Name, Font Sizo labels, Font Style group-box, Bold, Italic check-boxes
  '&P�smo:', '&Ve�kos�:', '&Rez p�sma', '&Tu�n�', '&Kurz�va',
  // Script, Color, Back color labels, Default charset
  'Sk&ript:', '&Farba:', 'Poza&die:', '(�tandardn�)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  '�t�l', '&Pod�iarknut�', '&Nad�iarknut�', 'P&re�krtnut�', 'V�et&ko ve�k�',
  // Sample, Sample text
  'N�h�ad', 'Uk�kov� text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Medzery medzi znakmi', 'O &ko�ko:', '&Roz��ren�', '&Z��en�',
  // Offset group-box, Offset label, Down, Up,
  'Umiestnenie', 'O ko�ko:', '&Zn�en�', 'Z&v��en�',
  // Scaling group-box, Scaling
  'Prelo�enie znakov', 'Me&r�tko:',
  // Sub/super script group box, Normal, Sub, Super
  'Horn� a doln� index', '&Norm�lne', 'Su&bscript(Horn� index)', 'Su&perscript (Doln� index)',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Odsadenie textu', '&Hore:', 'V&�avo:', '&Dole:', 'V&pravo:', '&V�etko rovnako',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Vlo�i� hypertextov� odkaz', 'Odkaz', 'T&ext:', 'C&ie�', '<<v�ber>>',
  // cannot open URL
  'Ned� sa prejs� na "%s"',
  // hyperlink properties button, hyperlink style
  '&Vlastnosti...', '�&t�l:', 
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Vlastnosti hypertextov�ho odkazu', 'Farba odkazu', 'Akt�vna farba',
  // Text [color], Background [color]
  '&Text:', '&Pozadie',
  // Underline [color]
  'Pod�iak&nut�:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'Po&zadie',
  // Underline [color], Attributes group-box,
  'Pod�iark&nut�:', 'Atrib�ty',
  // Underline type: always, never, active (below the mouse)
  'Pod�iarkn&utie:', 'v�dy', 'nikdy', 'akt�vny',
  // Same as normal check-box
  'Ako &Norm�lne',
  // underline active links check-box
  'Pod�iarkn&�� akt�vne odkazy',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Vlo�i� symbol', '&P�smo:', '&Znakov� sada:', 'Unicode',
  // Unicode block
  '&Blok:',  
  // Character Code, Character Unicode Code, No Character
  'K�d znaku: %d', 'K�d znaku: Unicode %d', '(nie je znak)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table sizo group-box, nCols, nRows, 
  'Vlo�i� tabu�ku', 'Ve�kos� tabu�ky', 'Po�et &st�pcov:', 'Po�et &riadkov:',
  // Table layout group-box, Autosizo, Fit, Manual sizo,
  '��rka tabu�ky', '&Automatick�', '&Roz�iahnu� pod�a okna', '&Manu�lne nastavenie',
  // Remember check-box
  'Pou�i� ako &vzor pre nov� tabu�ku',
  // VAlign Form ---------------------------------------------------------------
  'Poz�cia',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Vlastnosti', 'Obr�zok', 'Umiestnenie a ve�kos�', '�iara', 'Tabu�ka', 'Riadok', 'Bunka',
  // Image Appearance, Image Misc, Number tabs
  'Vzh�ad', 'R�zne', '��sla',
  // Box position, Box size, Box appearance tabs
  'Poz�cia', 'Ve�kos�', 'Vzh�ad',
  // Preview label, Transparency group-box, checkbox, label
  'N�h�ad:', 'Transparentn�', '&Transparentn�', 'Transparentn� &farba:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Zmeni�...', '&Ulo�i�...', 'Automatick�',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Zvisl� zarovn�vanie', 'Z&arovn�vanie:',
  'zarovna� k spodn�mu okraju textu', 'zarovna� na stred s textom',
  'horn� hrana objektu na hornej l�nii textu', 'doln� hrana objektu na dolnej l�nii textu', 'stred objektu na stred riadku',
  // align to left side, align to right side
  '�av� strana', 'prav� strana',      
  // Shift By label, Stretch group box, Width, Height, Default sizo
  '&Posun�� o:', 'Ve�kos�', '�&�rka:', '&V��ka:', '�tandardn� ve�kos�: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Mierka &proporcion�lna', '&Obnovi�',
  // Inside the border, border, outside the border groupboxes
  'Vo vn�tri okraju', 'Okraj', 'Zvonkaj�ej strany okraju',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Farba v�plne:', '&V�pl�:',
  // Border width, Border color labels
  '��rka okraja:', 'Farba &okraja:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizont�lny odstup:', '&Vertik�lny odstup:',
  // Miscellaneous groupbox, Tooltip label
  'R�zne', '&Tooltip:',
  // web group-box, alt text
  'Web', 'Alternat�vny &text:',  
  // Horz line group-box, color, width, style
  'Odde�ovacia �iara', '&Farba:', '�&�rka:', '�t�&l:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabu�ka', '�&�rka:', '&Farba v�plne:', '&Odsadenie bunky...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizont�lny odstup buniek:', '&Vertik�lny odstup buniek:', 'Okraj a pozadie',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Vidite�n� strany &okrajov...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Zachova� obsah spolu',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rot�cia', '�&iadna', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Okraj:', 'Vidite�n� strany &okrajov...',
  // Border, CellBorders buttons
  'Ohrani�enie &tabu�ky...', 'Ohrani�enie &bunky...',
  // Table border, Cell borders dialog titles
  'Ohrani�enie tabu�ky', 'Ohrani�enie bunky',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tla�', 'Tla� tab&u�ky na jednej str�nke', 'Po�et &hlavi�kov�ch riadkov:',
  'Hlavi�kov� riadky bud� opakovan� na ka�dej str�nke tabu�ky',
  // top, center, bottom, default
  '&Hore', 'Na &stred', '&Dolu', '�tanda&rd',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Nastavenie', 'Uprednost�ovan� &��rka:', 'Minim�lna &v��ka:', 'Farba v&�plne:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Ohrani�enie', 'Vidite�n� strany:',  'Fa&rba tie�a:', 'Farba &svetla', 'F&arba:',
  // Background image
  'Ob&r�zok...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizont�lna poz�cia', 'Vertik�lna poz�cia', 'Poz�cia v texte',
  // Position types: align, absolute, relative
  'Zarovnanie', 'Absol�tna poz�cia', 'Relat�vna poz�cia',
  // [Align] left side, center, right side
  '�av� strana oblasti k �avej strane',
  'stred oblasti k stredu',
  'prav� strana oblasti k pravej strane',
  // [Align] top side, center, bottom side
  'horn� strana oblasti k hornej strane',
  'stred oblasti k stredu',
  'spodn� strana oblasti k spodnej strane',
  // [Align] relative to
  'relat�vne k',
  // [Position] to the right of the left side of
  'napravo k �avej strane',
  // [Position] below the top side of
  'dole horn� stranu',
  // Anchors: page, main text area (x2)
  '&Strana', '&Hlavn� oblas� textu', 'Str&ana', 'Hlavn� oblas� te&xtu',
  // Anchors: left margin, right margin
  '�av� okraj', 'P&rav� okraj',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Horn� okraj', '&Doln� okraj', '&Vn�torn� okraj', 'V&onkaj�� okraj',
  // Anchors: character, line, paragraph
  '&Znak', 'R&iadok', 'Odsek',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Zrkadlovit� okraje', 'Vrstva v bunke tabu�ky',
  // Above text, below text
  'N&ad textom', 'Po&d textom',
  // Width, Height groupboxes, Width, Height labels
  '��rka', 'V��ka', '&��rka:', '&V��ka:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertik�lne zarovnanie', '&Hore', '&Stred', '&Dole',
  // Border and background button and title
  '&Okraj a pozadie...', 'Okraj a pozadie oblasti',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Vlo�i� �peci�lne', '&Vlo�i� ako:', 'Rich text form�t', 'HTML form�t',
  'Text', 'Unicode text', 'Bitmapov� obr�zok', 'Obr�zkov� meta S�bor',
  'Grafick� s�bor',  'URL',
  // Options group-box, styles
  'Mo�nosti', '&�t�ly:',
  // style options: apply target styles, use source styles, ignore styles
  'aplikova� �t�ly na cie�ov� dokument', 'zachova� �t�ly a vzh�ad',
  'zachova� vzh�ad, ignorova� �t�ly',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Odr�ky a ��slovanie', 'Odr�ky', '��slovanie', 'Odr�ky', '��slovanie',
  // Customizo, Reset, None
  '&Vlastn�...', '&Obnovi�', 'nie je',
  // Numbering: continue, reset to, create new
  'naviaza� na predch�dzaj�ce', 'pokra�ova� v ��slovan� od', 'za�a� znovu ��slova� od',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Ve�k� p�smen� (A, B, �)', 'Ve�k� r�mske ��slice (I, II, �)', 'Arabsk� ��slice (1, 2, �)', 'Mal� p�smen� (a, b, �)', 'Mal� r�mske ��slice (i, ii, �)',
  // Bullet type
  'Kruh', 'Disk', '�tvorec',
  // Level, Start from, Continue numbering, Numbering group-box
  '�r&ove�:', 'Za&�a� od:', '&Pokra�ova�', '��slovanie',
  // Customizo List Form -------------------------------------------------------
  // Title, Levels, Level count, List preperties, List type,
  'Vlastn� nastavenie', '�rove�', 'Po�et:', 'Vlastnosti', 'Typ:',
  // List types: bullet, image
  'Odr�ka', 'Obr�zok', 'Vlo�i� ��slo|',
  // Number format, Number, Start level from, Font button
  'For&m�t ��sla:', '��slo', 'Za�a� ��&slovanie �rovne od:', '&P�smo...',
  // Image button, bullet character, Bullet button,
  '&Obr�zok...', '&Znak odr�ky', 'O&dr�ka...',
  // Position of list text, bullet, number, image, text
  'Umiestnenie textu', 'Umiestnenie odr�ky', ' Umiestnenie ��sla', ' Umiestnenie obr�zku', ' Umiestnenie p�sma',
  // at, left indent, first line indent, from left indent
  '&na:', 'Odsadenie z&�ava:', 'Odsadenie pr&v�ho riadku:', 'od odsadenie z�ava',
  // [only] one level preview, preview
  '&N�h�ad jednej �rovne', 'N�h�ad',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'zarovna� v�avo', 'zarovna� vpravo', 'zarovna� na stred',
  // level #, this level (for combo-box)
  '�rove� %d', 'T�to �rove�',
  // Marker character dialog title
  'Zmena znaku odr�ky',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Ohrani�enie a pozadie odstavca', 'Ohrani�enie', 'Pozadie',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Nastavenie', '&Farba:', '�&�rka:', '��rka &r�mu:', 'Odsadenie &textu...',
  // Sample, Border type group-boxes;
  'N�h�ad', 'Typ ohrani�enia',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&�iadne', '&Jednoduch�', '&Dvojit�', '&Trojit�', 'Siln� vo &vn�tri', 'Siln� &okolo',
  // Fill color group-box; More colors, padding buttons
  'V�pl�', '&Viac farieb...', '&Odsadenie textu...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Odsadenie textu', 'Odsadenie textu',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Odstavec', 'Zarovnanie', 'v&�avo', 'v&pravo', 'na &stred', 'do &bloku',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Medzery', '&Pred:', '&Za:', '&Riadkovanie:', '&N�sobky:',
  // Indents group-box; indents: left, right, first line
  'Odsadenie', 'Z&�ava:', 'S&prava:', 'Pr&v� riadok:',
  // indented, hanging
  '&Odsaden�', '&Predsaden�', 'N�h�ad',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Jednoduch�', '1.5 riadku', 'Dvojit�', 'Minim�lne', 'Presne', 'N�sobky',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Odsadenie a medzery', 'Zar�ky', 'Tok textu',
  // tab stop position label; buttons: set, delete, delete all
  '&Poz�cia zar�ky:', '&Nastavi�', '&Zmaza�', 'Zmaza� &v�etko',
  // tab align group; left, right, center aligns,
  'Zarovnanie', 'V&�avo', 'V&pravo', 'Na &stred',
  // leader radio-group; no leader
  'Vodiaci znak', '(�iadny)',
  // tab stops to be deleted, delete none, delete all labels
  'Vymaza� tieto zar�ky:', '(�iadne)', 'V�etky.',
  // Pagination group-box, keep with next, keep lines together
  'Str�nkovanie', '&Zviaza� s nasleduj�cim', 'Zviaza� &riadky',
  // Outline level, Body text, Level #
  '�&rove� osnovy:', 'Telo textu', '�rove� %d',    
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'N�h�ad pred tla�ou', '��rka str�nky', 'Cel� str�nka', 'Str�nka:',
  // Invalid Scale, [page #] of #
  'Pros�m vlo�te ��slo od 10 do 500', 'z %d',
  // Hints on buttons: first, prior, next, last pages
  'Prv� str�nka', 'Predch�dzaj�ca str�nka', 'Nasleduj�ca str�nka', 'Posledn� str�nka',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Odsadenie bunky', 'Medzery', '&Pred bunkou', 'Od r�mu tabu�ky po bunku',
  // vertical, horizontal (x2)
  '&Vertik�lne:', '&Horizont�lne:', 'V&ertik�lne:', 'H&orizont�lne:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Ohrani�enie', 'Nastavenie', '&Farba:', 'Farba &svetl�:', 'Farba &tie�a:',
  // Width; Border type group-box;
  '��rka:', 'Typ ohrani�enia',
  // Border types: none, sunken, raised, flat
  '�&iadne', '&Vtla�en�', 'V&yst�pen�', '&Pl�vaj�ce',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Rozdelenie bunky', 'Rozdeli� na',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Ur�enie po�tu st�pcov a riadkov', '&Origin�lne bunky',
  // number of columns, rows, merge before
  'Po�et &st�pcov:', 'Po�et &riadkov:', '&Zl��i� pred rozdelen�m',
  // to original cols, rows check-boxes
  'Rozdeli� na origin�&lne st�pce', 'Rozdeli� na origin�lne riad&ky',
  // Add Rows form -------------------------------------------------------------
  'Vlo�i� riadky', 'Po&�et riadkov:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Vzh�ad str�nky', 'Str�nka', 'Z�hlavie a z�p�tie',
  // margins group-box, left, right, top, bottom
  'Okraje (v milimetroch)',  'Okraje (inches)', 'V&�avo:', '&Hore:', 'V&pravo:', '&Dole:',
  // mirror margins check-box
  '&Zrkadlov� okraje',
  // orientation group-box, portrait, landscape
  'Orient�cia', 'Na &v��ku', 'Na &��rku',
  // paper group-box, paper sizo, default paper source
  'Papier', 'V&e�kos�:', 'Zd&roj:',
  // header group-box, print on the first page, font button
  'Z�hlavie', '&Text:', '&Z�hlavie na prvej str�nke', '&P�smo...',
  // header alignments: left, center, right
  'v&�avo', 'na &stred', 'v&pravo',
  // the same for footer (different hotkeys)
  'P�ta', 'T&ext:', 'Z&�p�tie na prvej str�nke', 'P&�smo...',
  '&v�avo', '&na stred', 'vp&ravo',
  // page numbers group-box, start from
  '��slovanie str�nok', 'Za&�a� od:',
  // hint about codes
  'Kombin�cia �peci�lnych znakov:'#13'&&p - ��slo str�nky; &&P - po�et str�nok; &&d - aktu�lny d�tum; &&t - aktu�lny �as.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'K�dov� str�nka pre textov� s�bor', 'Vyberte s�bor dek�dovanie:',
  // thai, japanese, chinese (simpl), korean
  'Thajsko', 'Japonsko', '��na (zjednodu�en�)', 'Korea',
  // chinese (trad), central european, cyrillic, west european
  '��n�tina (tradi�n�)', 'Stredn� a v�chodn� Eur�pa', 'Cyrilika', 'Z�padn� Europa',
  // greek, turkish, hebrew, arabic
  'Gr��tina', 'Ture�tina', 'Hebrej�ina', 'Arab�ina',
  // baltic, vietnamese, utf-8, utf-16
  'Balticko', 'Vietnam�ina', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vizu�lny �t�l', '&Vyber �t�l:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Prevod do textu', 'Vybra� &oddelova�:',
  // line break, tab, ';', ','
  'koniec riadku', 'tab', 'bodko �iarka', '�iarka',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabu�ku obsahuj�cu zl��en� riadky nie je mo�n� zoradi�',
  // title, main options groupbox
  'Tabu�ku zoradi�', 'Zoradi�',
  // sort by column, case sensitive
  '&Zoradi� pod�a st�pca:', '&Ve�k� a mal� p�smen�',
  // heading row, range or rows
  'Vyl��i� &hlavi�ku riadka', 'Riadky na zoradenie: od %d do %d',
  // order, ascending, descending
  'Objedn�vky', '&Vzostupne', '&Zostupne',
  // data type, text, number
  'Typ', '&Text', '�&�slo',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Vlo�i� ��slo', 'Vlastnosti', 'Meno ��slovania:', '&Typ ��slovania:',
  // numbering groupbox, continue, start from
  '��slovanie', 'P&okra�ova�', '&Za�a� od:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Popis', '&Ozna�enie:', '&Vyl��i� ozna�enie z popisu',
  // position radiogroup
  'Poz�cia', '&Nad zvolen�m objektom', '&Pod zvolen�m objektom',
  // caption text
  'Popisn� &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  '��slovanie', 'Figure', 'Tabu�ka',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Vidite�n� strany okrajov', 'Okraj',
  // Left, Top, Right, Bottom checkboxes
  '&�av� strana', '&Vrchn� strana', 'P&rav� strana', '&Spodn� strana',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Zmenit ve�kos� st�pca tabu�ky', 'Zmenit ve�kos� riadku tabu�ky',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Odsadenie prv�ho riadku', 'Odsadenie z�ava', 'Predsadenie prv�ho riadku', 'Odsadenie vpravo',
  // Hints on lists: up one level (left), down one level (right)
  'O �rove� vy��ie', 'O �rove� ni��ie',
  // Hints for margins: bottom, left, right and top
  'Spodn� okraj', '�av� okraj', 'Prav� okraj', 'Horn� okraj',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  '�av� zar�ka', 'Prav� zar�ka', 'Zar�ka na stred', 'Desatinn� zar�ka',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Norm�lny', 'Norm�lne odsadenie', '�iadny priestor', 'Z�hlavie %d', 'Zoznam odsekov',
  // Hyperlink, Title, Subtitle
  'Hypertextov� odkaz', 'Nadpis', 'Podnadpis',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Zd�raznenie', 'Zd�raznenie - jemn�', 'Zd�raznenie - intenz�vne', 'Siln�',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cit�cia', 'Cit�cia - intenz�vne', 'Odkaz - jemn�', 'Odkaz - intenz�vny',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Text v bloku', 'Premenn� HTML', 'K�d HTML', 'Akronym HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Defin�cia HTML', 'Kl�vesnica HTML', 'Vzorka HTML', 'P�sac� stroj HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Form�tovan� v HTML', 'Cit�t HTML', 'Hlavi�ka', 'P�ta', '��slo strany',
  // Caption
  'Popis',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Odkaz na vysvetlivky', 'Odkaz na pozn�mku pod �iarou', 'Text vysvetlivky', 'Text pozn�mky pod �iarou',
  // Sidenote Reference, Sidenote Text
  'Odkazy bo�nej pozn�mky', 'Text bo�nej pozn�mky',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'farba', 'farba pozadia', 'prieh�adn�', 'v�chodz�', 'farba pod�iarknutia',
  // default background color, default text color, [color] same as text
  'v�chodzia farba pozadia', 'v�chodzia farba textu', 'rovnak� ako text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'jednoduch�', 'siln�', 'dvojit�', 'bodkovan�', 'silne bodkovan�', 'preru�ovan�',
  // underline types: thick dashed, long dashed, thick long dashed,
  'siln� preru�ovan�', 'dlh� preru�ovan�', 'siln� dlh� preru�ovan�',
  // underline types: dash dotted, thick dash dotted,
  'bodka a �iara', 'siln� bodka a �iara',
  // underline types: dash dot dotted, thick dash dot dotted
  '�iara bodka bodka', 'siln� �iara bodka bodka',
  // sub/superscript: not, subsript, superscript
  '�iadny doln�/horn� index', 'doln� index', 'horn� index',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'obojsmern� re�im:', 'zdeden�', 'z�ava do prava', 'zprava do �ava',
  // bold, not bold
  'tu�n�', 'netu�n�',
  // italic, not italic
  'kurz�va', 'nekurz�va',
  // underlined, not underlined, default underline
  'pod�iarkn��', 'nepod�iarkn��', 'v�chodzie pod�iarknutie',
  // struck out, not struck out
  'pre�krtnut�', 'nepre�krtnut�',
  // overlined, not overlined
  'nad�iarknut�', 'nenad�iarknut�',
  // all capitals: yes, all capitals: no
  'v�etky ve�k�', 'v�etk� ve�k� vypnut�',
  // vertical shift: none, by x% up, by x% down
  'bez zvisl�ho posunu', 'posunut� hore o %d%%', 'posunut� dole o %d%%',
  // characters width [: x%]
  '��rka znakov',
  // character spacing: none, expanded by x, condensed by x
  'norm�lne rozlo�enie znakov', 'rozlo�enie roz��ren� o %s', 'rozlo�enie z��en� o %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'v�chodzie p�smo', 'v�chodz� odstavec', 'zdeden� atrib�ty',
  // [hyperlink] highlight, default hyperlink attributes
  'zv�razni�', 'v�chodzie',
  // paragraph aligmnment: left, right, center, justify
  'zarovna� do�ava', 'zarovna� doprava', 'zarovna� na stred', 'zarovna� na ��rku',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'v��ka riadku: %d%%', 'medzera medzi riadkami: %s', 'v��ka riadku: aspo� %s',
  'v��ka riadku: presne %s',
  // no wrap, wrap
  'zarovnanie riadku vypnut�', 'zarovnanie riadku',
  // keep lines together: yes, no
  'udr�at riadky spolu', 'neudr�a� riadky spolu',
  // keep with next: yes, no
  'zachova� s nasleduj�cim odstavcom', 'nezachova� s nasleduj�cim odstavcom',
  // read only: yes, no
  'len pre ��tanie', 'upravite�n�',
  // body text, heading level x
  '�rove� osnovy: telo textu', '�rove� osnovy: %d',
  // indents: first line, left, right
  'odsadenie prv�ho riadku', '�av� odsadenie', 'prav� odsadenie',
  // space before, after
  'medzera pred', 'medzera po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabul�tory', '�iadne', 'zarovnanie', 'v�avo', 'vpravo', 'na stred',
  // tab leader (filling character)
  'vodiaci znak',
  // no, yes
  'nie', '�no',
  // [padding/spacing/side:] left, top, right, bottom
  'v�avo', 'hore', 'vpravo', 'dole',
  // border: none, single, double, triple, thick inside, thick outside
  '�iadne', 'jednoduch�', 'dvojit�', 'trojit�', 'hrubo vn�tri', 'hrubo zvonka',
  // background, border, padding, spacing [between text and border]
  'pozadie', 'okraj', 'v�pl�', 'rozostup',
  // border: style, width, internal width, visible sides
  '�t�l', '��rka', 'vn�torn� ��rka', 'vidite�n� strany',
  // style inspector -----------------------------------------------------------
  // title
  'In�pektor �t�lov',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  '�t�l', '(�iadny)', 'Odstavec', 'P�smo', 'Atrib�ty',
  // border and background, hyperlink, standard [style]
  'Okraj a pozadie', 'Hyperlink', '(�tandard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  '�t�ly', '�t�l',
  // name, applicable to,
  '&N�zov:', 'Aplikova� na:',
  // based on, based on (no &),
  '�t�l zalo�en� n&a:', '�t�l zalo�en� na:',
  //  next style, next style (no &)
  '�t�l pre nasleduj�ce odstavec:', '�t�l pre nasleduj�ce odstavec:',
  // quick access check-box, description and preview
  'R�&chly pr�stup', '&Popis a n�h�ad:',
  // [applicable to:] paragraph and text, paragraph, text
  'odstavec a text', 'odstavec', 'text',
  // text and paragraph styles, default font
  'Text a �t�ly odstavcov', 'V�chodzie p�smo',
  // links: edit, reset, buttons: add, delete
  '�pravy', 'V�chodzie', 'Prid&a�', '&Vymaza�',
  // add standard style, add custom style, default style name
  'Prida� &�tandardn� �t�l...', 'Prida� &u�ivate�sk� �t�l', '�t�l %d',
  // choose style
  'Zvo�te &�t�l:',
  // import, export,
  '&Importova�...', '&Exportova�...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView �t�ly (*.rvst)|*.rvst', 'Chyba otvorenia s�boru', 'Tento s�bor neobsahuje �t�ly',
  // Title, group-box, import styles
  'Importova� �t�ly zo s�boru', 'Import', 'I&mportova� �t�ly:',
  // existing styles radio-group: Title, override, auto-rename
  'Existuj�ce �t�ly', '&prep�sa�', '&prida� premenovan�',
  // Select, Unselect, Invert,
  '&Ozna�i�', 'Od&zna�i�', 'Zameni� ozna�enie',
  // select/unselect: all styles, new styles, existing styles
  '&V�etky �t�ly', '&Nov� �t�ly', '&Existuj�ce �t�ly',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Vymaza� form�t)', '(V�etky �t�ly)',
  // dialog title, prompt
  '�t�ly', 'Zvo�te �t�l na pou�itie:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Synonym�', '&Ignorova� v�etko', '&Prida� do slovn�ka',
  // Progress messages ---------------------------------------------------------
  'Preberanie %s', 'Pr�prava pre tla�...', 'Tla� strany %d',
  'Konverzia z RTF...',  'Konverzia do RTF...',
  '��tanie %s...', 'Z�pis %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Bez pozn�mky', 'Pozn�mka v p�te', 'Z�vere�n� pozn�mka', 'Bo�n� pozn�mka', 'Text oblas�'
  );


initialization
  RVA_RegisterLanguage(
    'Slovak', // english language name, do not translate
    'Sloven�ina', // native language name
    EASTEUROPE_CHARSET, @Messages);

end.
