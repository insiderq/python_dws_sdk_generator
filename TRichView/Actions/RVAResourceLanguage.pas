unit RVAResourceLanguage;

interface

implementation

{$I RV_Defs.inc}
{$I RichViewActions.inc}

{$IFNDEF RVA_NO_RESOURCELANGUAGE}

uses Windows, RVALocalize;

resourcestring
  rsrvam_Empty = '';
  rsrvam_unit_Inches = 'inches';
  rsrvam_unit_MM = 'cm';
  rsrvam_unit_CM = 'mm';
  rsrvam_unit_Picas = 'picas';
  rsrvam_unit_Pixels = 'pixels';
  rsrvam_unit_Points = 'points';
  rsrvam_unitN_Inches = '''''';
  rsrvam_unitN_MM = 'cm';
  rsrvam_unitN_CM = 'mm';
  rsrvam_unitN_Picas = 'pc';
  rsrvam_unitN_Pixels = 'px';
  rsrvam_unitN_Points = 'pt';
  rsrvam_menu_File = '&File';
  rsrvam_menu_Edit = '&Edit';
  rsrvam_menu_Format = 'F&ormat';
  rsrvam_menu_Font = 'Fo&nt';
  rsrvam_menu_Para = '&Paragraph';
  rsrvam_menu_Insert = '&Insert';
  rsrvam_menu_Table = '&Table';
  rsrvam_menu_Window = '&Window';
  rsrvam_menu_Help = '&Help';
  rsrvam_menu_Exit = 'E&xit';
  rsrvam_menu_View = '&View';
  rsrvam_menu_Tools = 'Too&ls';
  rsrvam_menu_FontSize = 'Size';
  rsrvam_menu_FontStyle = 'Style';
  rsrvam_menu_TableSelect = 'Selec&t';
  rsrvam_menu_TableCellAlign = 'Alig&n Cell Contents';
  rsrvam_menu_TableCellBorders = 'C&ell Borders';
  rsrvam_menu_TableCellRotation = 'Cell &Rotation';
  rsrvam_menu_TextFlow = '&Text Flow';
  rsrvam_menu_Notes = '&Footnotes';
  rsrvam_ribt_MainTab1 = '&Home';
  rsrvam_ribt_MainTab2 = '&Advanced';
  rsrvam_ribt_View = '&View';
  rsrvam_ribt_Table = '&Table';
  rsrvam_ribg_Clipboard = 'Clipboard';
  rsrvam_ribg_Font = 'Font';
  rsrvam_ribg_Paragraph = 'Paragraph';
  rsrvam_ribg_List = 'List';
  rsrvam_ribg_Edit = 'Editing';
  rsrvam_ribg_Insert = 'Insert';
  rsrvam_ribg_Background = 'Background';
  rsrvam_ribg_PageLayout = 'Page Setup';
  rsrvam_ribg_Links = 'Links';
  rsrvam_ribg_Notes = 'Footnotes';
  rsrvam_ribg_DocumentViews = 'Document Views';
  rsrvam_ribg_ShowHide = 'Show/Hide';
  rsrvam_ribg_Zoom = 'Zoom';
  rsrvam_ribg_Options = 'Options';
  rsrvam_ribg_TableInsert = 'Insert';
  rsrvam_ribg_TableDelete = 'Delete';
  rsrvam_ribg_TableOperations = 'Operations';
  rsrvam_ribg_TableBorders = 'Borders';
  rsrvam_ribg_Styles = 'Styles';
  rsrvam_rib_ScreenTipFooter = 'Press F1 for more help';
  rsrvam_rib_RecentFiles = 'Recent documents';
  rsrvam_rib_SaveAsTitle = 'Save a copy of the document';
  rsrvam_rib_PrintTitle = 'Preview and print the document';
  rsrvam_lbl_Units = 'Units:';
  rsrvam_act_New = '&New';
  rsrvam_act_NewH = 'New|Creates a new blank document';
  rsrvam_act_Open = '&Open...';
  rsrvam_act_OpenH = 'Open|Opens a document from disk';
  rsrvam_act_Save = '&Save';
  rsrvam_act_SaveH = 'Save|Saves the document to disk';
  rsrvam_act_SaveAs = 'Save &As...';
  rsrvam_act_SaveAsH = 'Save As...|Saves the document to disk with a new name';
  rsrvam_act_Export = '&Export...';
  rsrvam_act_ExportH = 'Export|Exports the document to another format';
  rsrvam_act_Preview = 'P&rint Preview';
  rsrvam_act_PreviewH = 'Print Preview|Shows the document as it will be printed';
  rsrvam_act_Print = '&Print...';
  rsrvam_act_PrintH = 'Print|Changes printing settings and prints the document';
  rsrvam_act_QuickPrint = '&Print';
  rsrvam_act_QuickPrint2 = '&Quick print';
  rsrvam_act_QuickPrintH = 'Print|Prints the document';
  rsrvam_act_PageSetup = 'Pa&ge Setup...';
  rsrvam_act_PageSetupH = 'Page Setup|Sets margins, paper size, orientation, source, headers and footers';
  rsrvam_act_Cut = 'Cu&t';
  rsrvam_act_CutH = 'Cut|Cuts the selection and puts it on the Clipboard';
  rsrvam_act_Copy = '&Copy';
  rsrvam_act_CopyH = 'Copy|Copies the selection and puts it on the Clipboard';
  rsrvam_act_Paste = '&Paste';
  rsrvam_act_PasteH = 'Paste|Inserts the Clipboard contents';
  rsrvam_act_PasteAsText = 'Paste as &Text';
  rsrvam_act_PasteAsTextH = 'Paste as Text|Inserts text from the Clipboard';
  rsrvam_act_PasteSpecial = 'Paste &Special...';
  rsrvam_act_PasteSpecialH = 'Paste Special|Inserts the Clipboard contents in the specified format';
  rsrvam_act_SelectAll = 'Select &All';
  rsrvam_act_SelectAllH = 'Select All|Selects all document';
  rsrvam_act_Undo = '&Undo';
  rsrvam_act_UndoH = 'Undo|Reverts the last action';
  rsrvam_act_Redo = '&Redo';
  rsrvam_act_RedoH = 'Redo|Redoes the last undone action';
  rsrvam_act_Find = '&Find...';
  rsrvam_act_FindH = 'Find|Finds specified text in the document';
  rsrvam_act_FindNext = 'Find &Next';
  rsrvam_act_FindNextH = 'Find Next|Continues the last search';
  rsrvam_act_Replace = '&Replace...';
  rsrvam_act_ReplaceH = 'Replace|Finds and replaces specified text in the document';
  rsrvam_act_InsertFile = '&File...';
  rsrvam_act_InsertFileH = 'Insert File|Inserts contents of file in the document';
  rsrvam_act_InsertPicture = '&Picture...';
  rsrvam_act_InsertPictureH = 'Insert Picture|Inserts a picture from disk';
  rsrvam_act_InsertHLine = '&Horizontal Line';
  rsrvam_act_InsertHLineH = 'Insert Horizontal Line|Inserts a horizontal line';
  rsrvam_act_Hyperlink = 'Hyper&link...';
  rsrvam_act_HyperlinkH = 'Insert Hyperlink|Inserts a hypertext link';
  rsrvam_act_RemoveHyperlinks = '&Remove Hyperlinks';
  rsrvam_act_RemoveHyperlinksH = 'Remove Hyperlinks|Removes all hyperlinks in the selected text';
  rsrvam_act_InsertSymbol = '&Symbol...';
  rsrvam_act_InsertSymbolH = 'Insert Symbol|Inserts a symbol';
  rsrvam_act_InsertNumber = '&Number...';
  rsrvam_act_InsertNumberH = 'Insert Number|Inserts a number';
  rsrvam_act_InsertItemCaption = 'Insert &Caption';
  rsrvam_act_InsertItemCaptionH = 'Insert Caption|Inserts a caption for the selected object';
  rsrvam_act_InsertTextBox = '&Text Box';
  rsrvam_act_InsertTextBoxH = 'Insert Text Box|Inserts a text box';
  rsrvam_act_InsertSidenote = '&Sidenote';
  rsrvam_act_InsertSidenoteH = 'Insert Sidenote|Inserts a note displayed in a text box';
  rsrvam_act_InsertPageNumber = '&Page Number';
  rsrvam_act_InsertPageNumberH = 'Insert Page Number|Inserts a page number';
  rsrvam_act_ParaList = '&Bullets and Numbering...';
  rsrvam_act_ParaListH = 'Bullets and Numbering|Applies or edits bullets or numbering for selected paragraphs';
  rsrvam_act_Bullets = '&Bullets';
  rsrvam_act_BulletsH = 'Bullets|Adds or removes bullets for the paragraph';
  rsrvam_act_Numbering = '&Numbering';
  rsrvam_act_NumberingH = 'Numbering|Adds or removes numberings for the paragraph';
  rsrvam_act_Color = 'Background &Color...';
  rsrvam_act_ColorH = 'Background|Changes a background color of the document';
  rsrvam_act_FillColor = '&Fill Color...';
  rsrvam_act_FillColorH = 'Fill Color|Changes background color of  text, paragraph, cell, table or document';
  rsrvam_act_InsertPageBreak = '&Insert Page Break';
  rsrvam_act_InsertPageBreakH = 'Insert Page Break|Inserts a page break';
  rsrvam_act_RemovePageBreak = '&Remove Page Break';
  rsrvam_act_RemovePageBreakH = 'Remove Page Break|Removes the page break';
  rsrvam_act_ClearLeft = 'Clear Text Flow at &Left Side';
  rsrvam_act_ClearLeftH = 'Clear Text Flow at Left Side|Places this paragraph below any left-aligned picture';
  rsrvam_act_ClearRight = 'Clear Text Flow at &Right Side';
  rsrvam_act_ClearRightH = 'Clear Text Flow at Right Side|Places this paragraph below any right-aligned picture';
  rsrvam_act_ClearBoth = 'Clear Text Flow at &Both Sides';
  rsrvam_act_ClearBothH = 'Clear Text Flow at Both Sides|Places this paragraph below any left- or right-aligned picture';
  rsrvam_act_ClearNone = '&Normal Text Flow';
  rsrvam_act_ClearNoneH = 'Normal Text Flow|Allows text flow around both left- and right-aligned pictures';
  rsrvam_act_VAlign = '&Object Position...';
  rsrvam_act_VAlignH = 'Object Position|Changes the position of the chosen object';
  rsrvam_act_ItemProperties = 'Object &Properties...';
  rsrvam_act_ItemPropertiesH = 'Object Properties|Defines properties of the active object';
  rsrvam_act_Background = '&Background...';
  rsrvam_act_BackgroundH = 'Background|Chooses background color and image';
  rsrvam_act_Paragraph = '&Paragraph...';
  rsrvam_act_ParagraphH = 'Paragraph|Changes the paragraph attributes';
  rsrvam_act_IndentInc = '&Increase Indent';
  rsrvam_act_IndentIncH = 'Increase Indent|Increases left indent of the selected paragraphs';
  rsrvam_act_IndentDec = '&Decrease Indent';
  rsrvam_act_IndentDecH = 'Decrease Indent|Dereases left indent of the selected paragraphs';
  rsrvam_act_WordWrap = '&Word Wrap';
  rsrvam_act_WordWrapH = 'Word Wrap|Toggles word wrapping for the selected paragraphs';
  rsrvam_act_AlignLeft = 'Align &Left';
  rsrvam_act_AlignLeftH = 'Align Left|Aligns the selected text to the left';
  rsrvam_act_AlignRight = 'Align &Right';
  rsrvam_act_AlignRightH = 'Align Right|Aligns the selected text to the right';
  rsrvam_act_AlignCenter = 'Align &Center';
  rsrvam_act_AlignCenterH = 'Align Center|Centers the selected text';
  rsrvam_act_AlignJustify = '&Justify';
  rsrvam_act_AlignJustifyH = 'Justify|Aligns the selected text to both left and right sides';
  rsrvam_act_ParaColor = 'Paragraph &Background Color...';
  rsrvam_act_ParaColorH = 'Paragraph Background Color|Sets background color of paragraphs';
  rsrvam_act_LS100 = '&Single Line Spacing';
  rsrvam_act_LS100H = 'Single Line Spacing|Sets single line spacing';
  rsrvam_act_LS150 = '1.5 Li&ne Spacing';
  rsrvam_act_LS150H = '1.5 Line Spacing|Sets line spacing equal to 1.5 lines';
  rsrvam_act_LS200 = '&Double Line Spacing';
  rsrvam_act_LS200H = 'Double Line Spacing|Sets double line spacing';
  rsrvam_act_ParaBorder = 'Paragraph &Borders and Background...';
  rsrvam_act_ParaBorderH = 'Paragraph Borders and Background|Sets borders and backround for the selected paragraphs';
  rsrvam_act_InsertTable = '&Insert Table...';
  rsrvam_act_InsertTable2 = '&Table';
  rsrvam_act_InsertTableH = 'Insert Table|Inserts a new table';
  rsrvam_act_TableInsertRowsAbove = 'Insert Row &Above';
  rsrvam_act_TableInsertRowsAboveH = 'Insert Row Above|Inserts a new row above the selected cells';
  rsrvam_act_TableInsertRowsBelow = 'Insert Row &Below';
  rsrvam_act_TableInsertRowsBelowH = 'Insert Row Below|Inserts a new row below the selected cells';
  rsrvam_act_TableInsertColsLeft = 'Insert Column &Left';
  rsrvam_act_TableInsertColsLeftH = 'Insert Column Left|Insert a new column to the left of selected cells';
  rsrvam_act_TableInsertColsRight = 'Insert Column &Right';
  rsrvam_act_TableInsertColsRightH = 'Insert Column Right|Insert a new column to the right of selected cells';
  rsrvam_act_TableDeleteRows = 'Delete R&ows';
  rsrvam_act_TableDeleteRowsH = 'Delete Rows|Deletes rows with the selected cells';
  rsrvam_act_TableDeleteCols = 'Delete &Columns';
  rsrvam_act_TableDeleteColsH = 'Delete Columns|Deletes columns with the selected cells';
  rsrvam_act_TableDeleteTable = '&Delete Table';
  rsrvam_act_TableDeleteTableH = 'Delete Table|Deletes the table';
  rsrvam_act_TableMergeCells = '&Merge Cells';
  rsrvam_act_TableMergeCellsH = 'Merge Cells|Merges the selected cells';
  rsrvam_act_TableSplitCells = '&Split Cells...';
  rsrvam_act_TableSplitCellsH = 'Split Cells|Splits the selected cells';
  rsrvam_act_TableSelectTable = 'Select &Table';
  rsrvam_act_TableSelectTableH = 'Select Table|Selects the table';
  rsrvam_act_TableSelectRows = 'Select Ro&ws';
  rsrvam_act_TableSelectRowsH = 'Select Rows|Selects rows';
  rsrvam_act_TableSelectCols = 'Select Col&umns';
  rsrvam_act_TableSelectColsH = 'Select Columns|Selects columns';
  rsrvam_act_TableSelectCell = 'Select C&ell';
  rsrvam_act_TableSelectCellH = 'Select Cell|Selects cell';
  rsrvam_act_TableCellVAlignTop = 'Align Cell to the &Top';
  rsrvam_act_TableCellVAlignTopH = 'Align Cell to the Top|Aligns cell contents to the top';
  rsrvam_act_TableCellVAlignMiddle = 'Align Cell to the &Middle';
  rsrvam_act_TableCellVAlignMiddleH = 'Align Cell to the Middle|Aligns cell contents to the middle';
  rsrvam_act_TableCellVAlignBottom = 'Align Cell to the &Bottom';
  rsrvam_act_TableCellVAlignBottomH = 'Align Cell to the Bottom|Aligns cell contents to the bottom';
  rsrvam_act_TableCellVAlignDefault = '&Default Cell Vertical Alignment';
  rsrvam_act_TableCellVAlignDefaultH = 'Default Cell Vertical Alignment|Sets default vertical alignment for the selected cells';
  rsrvam_act_TableCellRotationNone = '&No Cell Rotation';
  rsrvam_act_TableCellRotationNoneH = 'No Cell Rotation|Rotates cell contents by 0°';
  rsrvam_act_TableCellRotation90 = 'Rotate Cell by &90°';
  rsrvam_act_TableCellRotation90H = 'Rotate Cell by 90°|Rotates cell contents by 90°';
  rsrvam_act_TableCellRotation180 = 'Rotate Cell by &180°';
  rsrvam_act_TableCellRotation180H = 'Rotate Cell by 180°|Rotates cell contents by 180°';
  rsrvam_act_TableCellRotation270 = 'Rotate Cell by &270°';
  rsrvam_act_TableCellRotation270H = 'Rotate Cell by 270°|Rotates cell contents by 270°';
  rsrvam_act_TableProperties = 'Table &Properties...';
  rsrvam_act_TablePropertiesH = 'Table Properties|Changes properties for the selected table';
  rsrvam_act_TableGrid = 'Show &Grid Lines';
  rsrvam_act_TableGridH = 'Show Grid Lines|Shows or hides table grid lines';
  rsrvam_act_TableSplit = 'Sp&lit Table';
  rsrvam_act_TableSplitH = 'Split Table|Splits the table into two tables starting from the selected row';
  rsrvam_act_TableToText = 'Convert to Te&xt...';
  rsrvam_act_TableToTextH = 'Convert to Text|Converts the table to text';
  rsrvam_act_TableSort = '&Sort...';
  rsrvam_act_TableSortH = 'Sort|Sorts the table rows';
  rsrvam_act_TableCellLeftBorder = '&Left Border';
  rsrvam_act_TableCellLeftBorderH = 'Left Border|Shows or hides left cell border';
  rsrvam_act_TableCellRightBorder = '&Right Border';
  rsrvam_act_TableCellRightBorderH = 'Right Border|Shows or hides right cell border';
  rsrvam_act_TableCellTopBorder = '&Top Border';
  rsrvam_act_TableCellTopBorderH = 'Top Border|Shows or hides top cell border';
  rsrvam_act_TableCellBottomBorder = '&Bottom Border';
  rsrvam_act_TableCellBottomBorderH = 'Bottom Border|Shows or hides bottom cell border';
  rsrvam_act_TableCellAllBorders = '&All Borders';
  rsrvam_act_TableCellAllBordersH = 'All Borders|Shows or hides all cell borders';
  rsrvam_act_TableCellNoBorders = '&No Borders';
  rsrvam_act_TableCellNoBordersH = 'No Borders|Hides all cell borders';
  rsrvam_act_Font = '&Font...';
  rsrvam_act_FontH = 'Font|Changes font of the selected text';
  rsrvam_act_Bold = '&Bold';
  rsrvam_act_BoldH = 'Bold|Changes the style of selected text to bold';
  rsrvam_act_Italic = '&Italic';
  rsrvam_act_ItalicH = 'Italic|Changes the style of selected text to italic';
  rsrvam_act_Underline = '&Underline';
  rsrvam_act_UnderlineH = 'Underline|Changes the style of selected text to underline';
  rsrvam_act_StrikeOut = '&Strikeout';
  rsrvam_act_StrikeOutH = 'Strikeout|Strikes out the selected text';
  rsrvam_act_FontGrow = '&Grow Font';
  rsrvam_act_FontGrowH = 'Grow Font|Grows height of the selected text by 10%';
  rsrvam_act_FontShrink = 'S&hrink Font';
  rsrvam_act_FontShrinkH = 'Shrink Font|Shrinks height of the selected text by 10%';
  rsrvam_act_FontGrow1Pt = 'G&row Font by One Point';
  rsrvam_act_FontGrow1PtH = 'Grow Font by One Point|Grows height of the selected text by 1 point';
  rsrvam_act_FontShrink1Pt = 'Shri&nk Font by One Point';
  rsrvam_act_FontShrink1PtH = 'Shrink Font by One Point|Shrinks height of the selected text by 1 point';
  rsrvam_act_AllCaps = '&All Capitals';
  rsrvam_act_AllCapsH = 'All Capitals|Changes all characters of the selected text to capital letters';
  rsrvam_act_Overline = '&Overline';
  rsrvam_act_OverlineH = 'Overline|Adds line over the selected text';
  rsrvam_act_TextColor = 'Text &Color...';
  rsrvam_act_TextColorH = 'Text Color|Changes color of the selected text';
  rsrvam_act_TextBackColor = 'Text Bac&kground Color...';
  rsrvam_act_TextBackColorH = 'Text Background Color|Changes background color of the selected text';
  rsrvam_act_Spell = '&Spell Check';
  rsrvam_act_SpellH = 'Spell Check|Checks the spelling';
  rsrvam_act_Thesaurus = '&Thesaurus';
  rsrvam_act_ThesaurusH = 'Thesaurus|Provides synonyms for the selected word';
  rsrvam_act_ParaLTR = 'Left to Right';
  rsrvam_act_ParaLTRH = 'Left to Right|Sets left-to-right text direction for the selected paragraphs';
  rsrvam_act_ParaRTL = 'Right to Left';
  rsrvam_act_ParaRTLH = 'Right to Left|Sets right-to-left text direction for the selected paragraphs';
  rsrvam_act_TextLTR = 'Left to Right Text';
  rsrvam_act_TextLTRH = 'Left to Right Text|Sets left-to-right direction for the selected text';
  rsrvam_act_TextRTL = 'Right to Left Text';
  rsrvam_act_TextRTLH = 'Right to Left Text|Sets right-to-left direction for the selected text';
  rsrvam_act_CharCase = 'Character Case';
  rsrvam_act_CharCaseH = 'Character Case|Changes case of the selected text';
  rsrvam_act_ShowSpecialCharacters = '&Non-printing Characters';
  rsrvam_act_ShowSpecialCharactersH = 'Non-printing Characters|Shows or hides non-printing characters, such as paragraph marks, tabs and spaces';
  rsrvam_act_Subscript = 'Su&bscript';
  rsrvam_act_SubscriptH = 'Subscript|Converts the selected text to subscript';
  rsrvam_act_Superscript = 'Superscript';
  rsrvam_act_SuperscriptH = 'Superscript|Converts the selected text to superscript';
  rsrvam_act_InsertFootnote = '&Footnote';
  rsrvam_act_InsertFootnoteH = 'Footnote|Inserts a footnote';
  rsrvam_act_InsertEndnote = '&Endnote';
  rsrvam_act_InsertEndnoteH = 'Endnote|Inserts an endnote';
  rsrvam_act_EditNote = 'E&dit Note';
  rsrvam_act_EditNoteH = 'Edit Note|Starts editing the footnote or endnote';
  rsrvam_act_Hide = '&Hide';
  rsrvam_act_HideH = 'Hide|Hides or shows the selected fragment';
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  rsrvam_act_Styles = '&Styles...';
  rsrvam_act_StylesH = 'Styles|Opens the style management dialog';
  rsrvam_act_AddStyle = '&Add Style...';
  rsrvam_act_AddStyleH = 'Add style|Creates a new text or paragraph style basing on the selected fragment';
  rsrvam_act_ClearFormat = '&Clear Format';
  rsrvam_act_ClearFormatH = 'Clear Format|Clears all text and paragraph formatting from the selected fragment';
  rsrvam_act_ClearTextFormat = 'Clear &Text Format';
  rsrvam_act_ClearTextFormatH = 'Clear Text Format|Clears all formatting from the selected text';
  rsrvam_act_StyleInspector = 'Style &Inspector';
  rsrvam_act_CStyleInspectorH = 'Style Inspector|Shows or hides the Style Inspector';
  {$ENDIF}
  rsrvam_btn_OK = 'OK';
  rsrvam_btn_Cancel = 'Cancel';
  rsrvam_btn_Close = 'Close';
  rsrvam_btn_Insert = 'Insert';
  rsrvam_btn_Open = '&Open...';
  rsrvam_btn_Save = '&Save...';
  rsrvam_btn_Clear = '&Clear';
  rsrvam_btn_Help = 'Help';
  rsrvam_btn_Remove = 'Remove';  
  rsrvam_Percents = 'percent';
  rsrvam_LeftSide = 'Left Side';
  rsrvam_TopSide = 'Top Side';
  rsrvam_RightSide = 'Right Side';
  rsrvam_BottomSide = 'Bottom Side';
  rsrvam_SaveChanges = 'Save changes to %s?';
  rsrvam_Confirm = 'Confirm';
  rsrvam_LostFormat = '%s may contain features that are not compatible with the chosen saving format.'#13'Do you want to save the document in this format?';
  rsrvam_RVF = 'RichView Format';
  rsrvam_err_Title = 'Error';
  rsrvam_err_ErrorLoadingFile = 'Error loading file.'#13''#13'Possible reasons:'#13'- format of this file is not supported by this application;'#13'- the file is corrupted;'#13'- the file is opened and locked by another application.';
  rsrvam_err_ErrorLoadingImageFile = 'Error loading image file.'#13''#13'Possible reasons:'#13'- the file has image in format which is not supported by this application;'#13'- the file does not contain an image;'#13'- the file is corrupted;'#13'- the file is opened and locked by another application.';
  rsrvam_err_ErrorSavingFile = 'Error saving file.'#13''#13'Possible reasons:'#13'- out of disk space;'#13'- disk is write-protected;'#13'- removable media is not inserted;'#13'- the file is opened and locked by another application;'#13'- disk media is corrupted.';
  rsrvam_flt_RVF = 'RichView Files (*.rvf)|*.rvf';
  rsrvam_flt_RTF = 'RTF Files (*.rtf)|*.rtf';
  rsrvam_flt_XML = 'XML Files (*.xml)|*.xml';
  rsrvam_flt_TextAnsi = 'Text Files (*.txt)|*.txt';
  rsrvam_flt_TextUnicode = 'Text Files - Unicode (*.txt)|*.txt';
  rsrvam_flt_TextAuto = 'Text Files - Autodetect (*.txt)|*.txt';
  rsrvam_flt_HTMLOpen = 'HTML Files (*.htm;*.html)|*.htm;*.html';
  rsrvam_flt_HTMLCSS = 'HTML (*.htm;*.html)|*.htm;*.html';
  rsrvam_flt_HTMLPlain = 'HTML - Simplified (*.htm;*.html)|*.htm;*.html';
  rsrvam_flt_DocX = 'Microsoft Word Documents (*.docx)|*.docx';
  rsrvam_src_Complete = 'Search Complete';
  rsrvam_src_NotFound = 'Search string ''%s'' not found.';
  rsrvam_src_1Replaced = '1 string replaced.';
  rsrvam_src_NReplaced = '%d strings replaced.';
  rsrvam_src_ContinueFromStart = 'The end of the document is reached, continue from the beginning?';
  rsrvam_src_ContinueFromEnd = 'The beginning of the document is reached, continue from the end?';
  rsrvam_cl_Transparent = 'Transparent';
  rsrvam_cl_Auto = 'Auto';
  rsrvam_cl_Black = 'Black';
  rsrvam_cl_Brown = 'Brown';
  rsrvam_cl_OliveGreen = 'Olive Green';
  rsrvam_cl_DarkGreen = 'Dark Green';
  rsrvam_cl_DarkTeal = 'Dark Teal';
  rsrvam_cl_DarkBlue = 'Dark Blue';
  rsrvam_cl_Indigo = 'Indigo';
  rsrvam_cl_Gray80 = 'Gray-80%';
  rsrvam_cl_DarkRed = 'Dark Red';
  rsrvam_cl_Orange = 'Orange';
  rsrvam_cl_DarkYellow = 'Dark Yellow';
  rsrvam_cl_Green = 'Green';
  rsrvam_cl_Teal = 'Teal';
  rsrvam_cl_Blue = 'Blue';
  rsrvam_cl_Blue_Gray = 'Blue-Gray';
  rsrvam_cl_Gray50 = 'Gray-50%';
  rsrvam_cl_Red = 'Red';
  rsrvam_cl_LightOrange = 'Light Orange';
  rsrvam_cl_Lime = 'Lime';
  rsrvam_cl_SeaGreen = 'Sea Green';
  rsrvam_cl_Aqua = 'Aqua';
  rsrvam_cl_LightBlue = 'Light Blue';
  rsrvam_cl_Violet = 'Violet';
  rsrvam_cl_Grey = 'Grey-40%';
  rsrvam_cl_Pink = 'Pink';
  rsrvam_cl_Gold = 'Gold';
  rsrvam_cl_Yellow = 'Yellow';
  rsrvam_cl_BrightGreen = 'Bright Green';
  rsrvam_cl_Turquoise = 'Turquoise';
  rsrvam_cl_SkyBlue = 'Sky Blue';
  rsrvam_cl_Plum = 'Plum';
  rsrvam_cl_Gray25 = 'Gray-25%';
  rsrvam_cl_Rose = 'Rose';
  rsrvam_cl_Tan = 'Tan';
  rsrvam_cl_LightYellow = 'Light Yellow';
  rsrvam_cl_LightGreen = 'Light Green';
  rsrvam_cl_LightTurquoise = 'Light Turquoise';
  rsrvam_cl_PaleBlue = 'Pale Blue';
  rsrvam_cl_Lavender = 'Lavender';
  rsrvam_cl_White = 'White';
  rsrvam_cpcl_Transparent = 'Tr&ansparent';
  rsrvam_cpcl_Auto = '&Auto';
  rsrvam_cpcl_More = '&More Colors...';
  rsrvam_cpcl_Default = '&Default';
  rsrvam_back_Title = 'Background';
  rsrvam_back_Color = 'C&olor:';
  rsrvam_back_Position = 'Position';
  rsrvam_back_Background = 'Background';
  rsrvam_back_SampleText = 'Sample text.';
  rsrvam_back_None = '&None';
  rsrvam_back_FullWindow = 'St&retched';
  rsrvam_back_FixedTiles = 'F&ixed Tiles';
  rsrvam_back_Tiles = '&Tiles';
  rsrvam_back_Center = 'C&enter';
  rsrvam_back_Padding = '&Padding...';
  rsrvam_fillc_Title = 'Fill Color';
  rsrvam_fillc_ApplyTo = '&Apply to:';
  rsrvam_fillc_MoreColors = '&More colors...';
  rsrvam_fillc_Padding = '&Padding...';
  rsrvam_fillc_PleaseSelect = 'Please select a color';
  rsrvam_fillc_Text = 'text';
  rsrvam_fillc_Paragraph = 'paragraph';
  rsrvam_fillc_Table = 'table';
  rsrvam_fillc_Cell = 'cell';
  rsrvam_font_Title = 'Font';
  rsrvam_font_FontTab = 'Font';
  rsrvam_font_LayoutTab = 'Layout';
  rsrvam_font_FontName = '&Font:';
  rsrvam_font_FontSize = '&Size';
  rsrvam_font_FontStyle = 'Style';
  rsrvam_font_Bold = '&Bold';
  rsrvam_font_Italic = '&Italic';
  rsrvam_font_Script = 'Sc&ript:';
  rsrvam_font_Color = '&Color:';
  rsrvam_font_BackColor = 'Bac&kground:';
  rsrvam_font_DefaultCharset = '(Default)';
  rsrvam_font_Effects = 'Effects';
  rsrvam_font_Underline = 'Underline';
  rsrvam_font_Overline = '&Overline';
  rsrvam_font_Strikethrough = 'S&trikethrough';
  rsrvam_font_AllCaps = '&All caps';
  rsrvam_font_Sample = 'Sample';
  rsrvam_font_SampleText = 'Sample text';
  rsrvam_font_SpacingH = 'Character Spacing';
  rsrvam_font_Spacing = '&Spacing:';
  rsrvam_font_Expanded = '&Expanded';
  rsrvam_font_Condensed = '&Condensed';
  rsrvam_font_OffsetH = 'Vertical offset';
  rsrvam_font_Offset = '&Offset:';
  rsrvam_font_Down = '&Down';
  rsrvam_font_Up = '&Up';
  rsrvam_font_ScalingH = 'Scaling';
  rsrvam_font_Scaling = 'Sc&aling:';
  rsrvam_font_ScriptH = 'Subscripts and superscripts';
  rsrvam_font_SSNorm = '&Normal';
  rsrvam_font_SSSub = 'Su&bscript';
  rsrvam_font_SSSuper = 'Su&perscript';
  rsrvam_4s_DefTitle = 'Padding';
  rsrvam_4s_Top = '&Top:';
  rsrvam_4s_Left = '&Left:';
  rsrvam_4s_Bottom = '&Bottom:';
  rsrvam_4s_Right = '&Right:';
  rsrvam_4s_EqualValues = '&Equal values';
  rsrvam_hl_Title = 'Insert Hyperlink';
  rsrvam_hl_GBTitle = 'Hyperlink';
  rsrvam_hl_Text = 'T&ext:';
  rsrvam_hl_Target = '&Target';
  rsrvam_hl_Selection = '<<selection>>';
  rsrvam_hl_CannotNavigate = 'Cannot navigate to "%s"';
  rsrvam_hl_HypProperties = '&Customize...';
  rsrvam_hl_HypStyle = '&Style:';
  rsrvam_hp_Title = 'Hyperlink Attributes';
  rsrvam_hp_GBNormal = 'Normal colors';
  rsrvam_hp_GBActive = 'Active colors';
  rsrvam_hp_Text = '&Text:';
  rsrvam_hp_TextBack = '&Background';
  rsrvam_hp_UnderlineColor = 'U&nderline:';
  rsrvam_hp_HoverText = 'T&ext:';
  rsrvam_hp_HoverTextBack = 'B&ackground';
  rsrvam_hp_HoverUnderlineColor = 'Un&derline:';
  rsrvam_hp_GBAttributes = 'Attributes';
  rsrvam_hp_UnderlineType = '&Underline:';
  rsrvam_hp_UAlways = 'always';
  rsrvam_hp_UNever = 'never';
  rsrvam_hp_UActive = 'active';
  rsrvam_hp_AsNormal = 'As &normal';
  rsrvam_hp_UnderlineActiveLinks = '&Underline active links';
  rsrvam_is_Title = 'Insert Symbol';
  rsrvam_is_Font = '&Font:';
  rsrvam_is_Charset = '&Character set:';
  rsrvam_is_Unicode = 'Unicode';
  rsrvam_is_Block = '&Block:';
  rsrvam_is_CharCode = 'Character code: %d';
  rsrvam_is_UCharCode = 'Character code: Unicode %d';
  rsrvam_is_NoChar = '(no character)';
  rsrvam_it_Title = 'Insert Table';
  rsrvam_it_TableSize = 'Table size';
  rsrvam_it_nCols = 'Number of &columns:';
  rsrvam_it_nRows = 'Number of &rows:';
  rsrvam_it_TableLayout = 'Table layout';
  rsrvam_it_Autosize = '&Autosize';
  rsrvam_it_Fit = 'Fit &window';
  rsrvam_it_Manual = 'C&ustom size';
  rsrvam_it_Remember = 'Remember &dimensions for new tables';
  rsrvam_va_Title = 'Position';
  rsrvam_ip_Title = 'Properties';
  rsrvam_ip_ImageTab = 'Image';
  rsrvam_ip_LayoutTab = 'Size and position';
  rsrvam_ip_LineTab = 'Line';
  rsrvam_ip_TableTab = 'Table';
  rsrvam_ip_RowsTab = 'Rows';
  rsrvam_ip_CellsTab = 'Cells';
  rsrvam_ip_ImgAppTab = 'Appearance';
  rsrvam_ip_ImgMiscTab = 'Miscellaneous';
  rsrvam_ip_SeqTab = 'Number';
  rsrvam_ip_BoxPosTab = 'Position';
  rsrvam_ip_BoxSizeTab = 'Size';
  rsrvam_ip_BoxAppTab = 'Appearance';
  rsrvam_ip_Preview = 'Preview:';
  rsrvam_ip_Transparency = 'Transparency';
  rsrvam_ip_Transparent = '&Transparent';
  rsrvam_ip_TrColor = 'Transparent &color:';
  rsrvam_ip_Change = 'C&hange...';
  rsrvam_ip_ImgSave = '&Save...';
  rsrvam_ip_TrAutoColor = 'Auto';
  rsrvam_ip_VAlign = 'Vertical alignment';
  rsrvam_ip_VAlignValue = '&Align:';
  rsrvam_ip_VAlign1 = 'bottom to base line of text';
  rsrvam_ip_VAlign2 = 'middle to base line of text';
  rsrvam_ip_VAlign3 = 'top to line top';
  rsrvam_ip_VAlign4 = 'bottom to line bottom';
  rsrvam_ip_VAlign5 = 'middle to line middle';
  rsrvam_ip_VAlign6 = 'to left side';
  rsrvam_ip_VAlign7 = 'to right side';
  rsrvam_ip_ShiftBy = '&Shift by:';
  rsrvam_ip_Stretch = 'Stretch';
  rsrvam_ip_Width = '&Width:';
  rsrvam_ip_Height = '&Height:';
  rsrvam_ip_DefaultSize = 'Default size: %d x %d';
  rsrvam_ip_ImgScaleProportionally = 'Scale &proportionally';
  rsrvam_ip_ImgSizeReset = '&Reset';
  rsrvam_ip_ImgInsideBorderGB = 'Inside the border';
  rsrvam_ip_ImgBorderGB = 'Border';
  rsrvam_ip_ImgOutsideBorderGB = 'Outside the border';
  rsrvam_ip_ImgFillColor = '&Fill color:';
  rsrvam_ip_ImgPadding = '&Padding:';
  rsrvam_ip_ImgBorderWidth = 'Border &width:';
  rsrvam_ip_ImgBorderColor = 'Border &color:';
  rsrvam_ip_ImgHSpacing = '&Horizontal spacing:';
  rsrvam_ip_ImgVSpacing = '&Vertical spacing:';
  rsrvam_ip_ImgMisc = 'Miscellaneous';
  rsrvam_ip_ImgHint = '&Tooltip:';
  rsrvam_ip_Web = 'Web';
  rsrvam_ip_Alt = '&Alternate text:';
  rsrvam_ip_HorzLine = 'Horizontal line';
  rsrvam_ip_HLColor = '&Color:';
  rsrvam_ip_HLWidth = '&Width:';
  rsrvam_ip_HLStyle = '&Style:';
  rsrvam_ip_TableGB = 'Table';
  rsrvam_ip_TableWidth = '&Width:';
  rsrvam_ip_TableColor = '&Fill color:';
  rsrvam_ip_CellSpacing = 'Cell &Spacing...';
  rsrvam_ip_CellHPadding = 'H&orizontal cell padding:';
  rsrvam_ip_CellVPadding = '&Vertical cell padding:';
  rsrvam_ip_TableBorderGB = 'Border and background';
  rsrvam_ip_TableVisibleBorders = 'Visible &border sides...';
  rsrvam_ip_TableAutoSizeLbl = 'Auto';
  rsrvam_ip_TableAutoSize = 'auto';
  rsrvam_ip_RowKeepTogether = '&Keep content together';
  rsrvam_ip_CellRotationGB = 'Rotation';
  rsrvam_ip_CellRotation0 = '&None';
  rsrvam_ip_CellRotation90 = '&90�';
  rsrvam_ip_CellRotation180 = '&180�';
  rsrvam_ip_CellRotation270 = '&270�';
  rsrvam_ip_CellBorderLbl = 'Border:';
  rsrvam_ip_CellVisibleBorders = 'Visible b&order sides...';
  rsrvam_ip_TableBorder = '&Table border...';
  rsrvam_ip_CellBorder = '&Cell borders...';
  rsrvam_ip_TableBorderTitle = 'Table Border';
  rsrvam_ip_CellBorderTitle = 'Default Cells Borders';
  rsrvam_ip_TablePrinting = 'Printing';
  rsrvam_ip_KeepOnPage = '&Don''t allow columns to break across pages';
  rsrvam_ip_HeadingRows = 'Number of &heading rows:';
  rsrvam_ip_HeadingRowsTip = 'Heading rows are repeated on each page of the table';
  rsrvam_ip_VATop = '&Top';
  rsrvam_ip_VACenter = '&Center';
  rsrvam_ip_VABottom = '&Bottom';
  rsrvam_ip_VADefault = '&Default';
  rsrvam_ip_CellSettings = 'Settings';
  rsrvam_ip_CellBestWidth = 'Preferred &width:';
  rsrvam_ip_CellBestHeight = '&Height at least:';
  rsrvam_ip_CellFillColor = '&Fill color:';
  rsrvam_ip_CellBorderGB = 'Border';
  rsrvam_ip_VisibleSides = 'Visible sides:';
  rsrvam_ip_CellShadowColor = 'Shadow co&lor:';
  rsrvam_ip_CellLightColor = '&Light color';
  rsrvam_ip_CellBorderColor = 'Co&lor:';
  rsrvam_ip_BackgroundImage = 'P&icture...';
  rsrvam_ip_BoxHPosGB = 'Horizontal position';
  rsrvam_ip_BoxVPosGB = 'Vertical position';
  rsrvam_ip_BoxPosInTextGB = 'Position in text';
  rsrvam_ip_BoxPosAlign = 'Align';
  rsrvam_ip_BoxPosAbsolute = 'Absolute position';
  rsrvam_ip_BoxPosRelative = 'Relative position';
  rsrvam_ip_BoxHPosLeft = 'left side of box to left side of';
  rsrvam_ip_BoxHPosCenter = 'center of box to center of';
  rsrvam_ip_BoxHPosRight = 'right side of box to right side of';
  rsrvam_ip_BoxVPosTop = 'top side of box to top side of';
  rsrvam_ip_BoxVPosCenter = 'center of box to center of';
  rsrvam_ip_BoxVPosBottom = 'bottom side of box to bottom side of';
  rsrvam_ip_BoxPosRelativeTo = 'relative to';
  rsrvam_ip_BoxPosToTheRightOfLeftSideOf = 'to the right of the left side of';
  rsrvam_ip_BoxPosBelowTheTopSideOf = 'below the top side of';
  rsrvam_ip_BoxAnchorPage = '&Page';
  rsrvam_ip_BoxAnchorMainTextArea = '&Main text area';
  rsrvam_ip_BoxAnchorPage2 = 'P&age';
  rsrvam_ip_BoxAnchorMainTextArea2 = 'Main te&xt area';
  rsrvam_ip_BoxAnchorLeftMargin = '&Left margin';
  rsrvam_ip_BoxAnchorRightMargin = '&Right margin';
  rsrvam_ip_BoxAnchorTopMargin = '&Top margin';
  rsrvam_ip_BoxAnchorBottomMargin = '&Bottom margin';
  rsrvam_ip_BoxAnchorInnerMargin = '&Inner margin';
  rsrvam_ip_BoxAnchorOuterMargin = '&Outer margin';
  rsrvam_ip_BoxAnchorCharacter = '&Character';
  rsrvam_ip_BoxAnchorLine = 'Li&ne';
  rsrvam_ip_BoxAnchorPara = 'Para&graph';
  rsrvam_ip_BoxMirroredMargins = 'Mirror&ed margins';
  rsrvam_ip_BoxLayoutInTableCell = 'La&yout in table cell';
  rsrvam_ip_BoxAboveText = '&Above text';
  rsrvam_ip_BoxBelowText = '&Below text';
  rsrvam_ip_BoxWidthGB = 'Width';
  rsrvam_ip_BoxHeightGB = 'Height';
  rsrvam_ip_BoxWidth = '&Width:';
  rsrvam_ip_BoxHeight = '&Height:';
  rsrvam_ip_BoxHeightAutoLbl = 'Auto';
  rsrvam_ip_BoxHeightAuto = 'auto';
  rsrvam_ip_BoxVAlign = 'Vertical alignment';
  rsrvam_ip_BoxVAlignTop = '&Top';
  rsrvam_ip_BoxVAlignCenter = '&Center';
  rsrvam_ip_BoxVAlignBottom = '&Bottom';
  rsrvam_ip_BoxBorderAndBack = 'B&order and background...';
  rsrvam_ip_BoxBorderAndBackTitle = 'Box Border and Background';
  rsrvam_ps_Title = 'Paste Special';
  rsrvam_ps_Label = '&Paste as:';
  rsrvam_ps_RTF = 'Rich text format';
  rsrvam_ps_HTML = 'HTML format';
  rsrvam_ps_Text = 'Text';
  rsrvam_ps_UnicodeText = 'Unicode text';
  rsrvam_ps_BMP = 'Bitmap picture';
  rsrvam_ps_WMF = 'Metafile picture';
  rsrvam_ps_GraphicFiles = 'Graphic files';
  rsrvam_ps_URL = 'URL';
  rsrvam_ps_Options = 'Options';
  rsrvam_ps_Styles = '&Styles:';
  rsrvam_ps_Styles1 = 'apply styles of the target document';
  rsrvam_ps_Styles2 = 'keep styles and appearance';
  rsrvam_ps_Styles3 = 'keep appearance, ignore styles';
  rsrvam_lg_Title = 'Bullets and Numbering';
  rsrvam_lg_BulletTab = 'Bulleted';
  rsrvam_lg_NumTab = 'Numbered';
  rsrvam_lg_BulletGB = 'Bulleted lists';
  rsrvam_lg_NumGB = 'Numbered lists';
  rsrvam_lg_Customize = '&Customize...';
  rsrvam_lg_Reset = '&Reset';
  rsrvam_lg_None = 'None';
  rsrvam_lg_NumContinue = 'continue numbering';
  rsrvam_lg_NumReset = 'reset numbering to';
  rsrvam_lg_NumCreate = 'create a new list, starting from';
  rsrvam_lg2_UpperAlpha = 'Upper Alpha';
  rsrvam_lg2_UpperRoman = 'Upper Roman';
  rsrvam_lg2_Decimal = 'Decimal';
  rsrvam_lg2_LowerAlpha = 'Lower Alpha';
  rsrvam_lg2_LowerRoman = 'Lower Roman';
  rsrvam_lg2_Circle = 'Circle';
  rsrvam_lg2_Disc = 'Disc';
  rsrvam_lg2_Square = 'Square';
  rsrvam_lg2_Level = '&Level:';
  rsrvam_lg2_StartFrom = '&Start from:';
  rsrvam_lg2_Continue = '&Continue';
  rsrvam_lg2_Numbering = 'Numbering';
  rsrvam_cul_Title = 'Customize List';
  rsrvam_cul_Levels = 'Levels';
  rsrvam_cul_LevelCount = '&Count:';
  rsrvam_cul_ListProperties = 'List properties';
  rsrvam_cul_ListType = '&List type:';
  rsrvam_cul_ListTypeBullet = 'bullet';
  rsrvam_cul_ListTypeImage = 'image';
  rsrvam_cul_InsertNumberHint = 'Insert Number|';
  rsrvam_cul_NumberFormat = '&Number format:';
  rsrvam_cul_Number = 'Number';
  rsrvam_cul_StartFrom = '&Start level numbering from:';
  rsrvam_cul_Font = '&Font...';
  rsrvam_cul_Image = '&Image...';
  rsrvam_cul_BulletCharacter = 'Bullet c&haracter';
  rsrvam_cul_Bullet = '&Bullet...';
  rsrvam_cul_ListTextPos = 'List text position';
  rsrvam_cul_BulletPos = 'Bullet position';
  rsrvam_cul_NumPos = 'Number position';
  rsrvam_cul_ImagePos = 'Image position';
  rsrvam_cul_TextPos = 'Text position';
  rsrvam_cul_At = '&at:';
  rsrvam_cul_LeftIndent = 'L&eft indent:';
  rsrvam_cul_FirstIndent = '&First line indent:';
  rsrvam_cul_FromLeftIndent = 'from left indent';
  rsrvam_cul_OneLevelPreview = '&One level preview';
  rsrvam_cul_Preview = 'Preview';
  rsrvam_cul_PreviewText = 'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.';
  rsrvam_cul_Left = 'align left';
  rsrvam_cul_Right = 'align right';
  rsrvam_cul_Center = 'center';
  rsrvam_cul_LevelNo = 'Level %d';
  rsrvam_cul_ThisLevel = 'This level';
  rsrvam_cul_BulletTitle = 'Edit Bullet Character';
  rsrvam_pbb_Title = 'Paragraph Border and Background';
  rsrvam_pbb_BorderTab = 'Border';
  rsrvam_pbb_BackgroundTab = 'Background';
  rsrvam_pbb_Settings = 'Settings';
  rsrvam_pbb_Color = '&Color:';
  rsrvam_pbb_Width = '&Width:';
  rsrvam_pbb_InternalWidth = 'Int&ernal width:';
  rsrvam_pbb_Offsets = 'Text &margins...';
  rsrvam_pbb_Sample = 'Sample';
  rsrvam_pbb_BorderType = 'Border type';
  rsrvam_pbb_BTNone = '&None';
  rsrvam_pbb_BTSingle = '&Single';
  rsrvam_pbb_BTDouble = '&Double';
  rsrvam_pbb_BTTriple = '&Triple';
  rsrvam_pbb_BTThickInside = 'Thick &inside';
  rsrvam_pbb_BTThickOutside = 'Thick &outside';
  rsrvam_pbb_FillColor = 'Fill color';
  rsrvam_pbb_MoreColors = '&More colors...';
  rsrvam_pbb_Padding = '&Padding...';
  rsrvam_pbb_PreviewText = 'Text text text text text. Text text text text text. Text text text text text.';
  rsrvam_pbb_OffsetsTitle = 'Text Margins';
  rsrvam_pbb_OffsetsGB = 'Border offsets';
  rsrvam_par_Title = 'Paragraph';
  rsrvam_par_Alignment = 'Alignment';
  rsrvam_par_AlLeft = '&Left';
  rsrvam_par_AlRight = '&Right';
  rsrvam_par_AlCenter = '&Center';
  rsrvam_par_AlJustify = '&Justify';
  rsrvam_par_Spacing = 'Spacing';
  rsrvam_par_Before = '&Before:';
  rsrvam_par_After = '&After:';
  rsrvam_par_LineSpacing = 'Line &spacing:';
  rsrvam_par_By = 'a&t:';
  rsrvam_par_Indents = 'Indentation';
  rsrvam_par_Left = 'L&eft:';
  rsrvam_par_Right = 'Ri&ght:';
  rsrvam_par_FirstLine = '&First line:';
  rsrvam_par_Indented = '&Indented';
  rsrvam_par_Hanging = '&Hanging';
  rsrvam_par_Sample = 'Sample';
  rsrvam_par_LS_100 = 'Single';
  rsrvam_par_150 = '1.5 lines';
  rsrvam_par_200 = 'Double';
  rsrvam_par_AtLeast = 'At least';
  rsrvam_par_Exactly = 'Exactly';
  rsrvam_par_Multiple = 'Multiple';
  rsrvam_par_Preview = 'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.';
  rsrvam_par_MainTab = 'Indents and spacing';
  rsrvam_par_TabsTab = 'Tabs';
  rsrvam_par_TextFlowTab = 'Text flow';
  rsrvam_par_TabStopPos = '&Tab stop position:';
  rsrvam_par_btnSet = '&Set';
  rsrvam_par_Delete = '&Delete';
  rsrvam_par_DeleteAll = 'Delete &all';
  rsrvam_par_TabAlign = 'Alignment';
  rsrvam_par_TabAlignLeft = '&Left';
  rsrvam_par_TabAlignRight = '&Right';
  rsrvam_par_TabAlignCenter = '&Center';
  rsrvam_par_Leader = 'Leader';
  rsrvam_par_LeaderNone = '(None)';
  rsrvam_par_TabsToBeDeleted = 'Tab stops to be deleted:';
  rsrvam_par_TabDelNone = '(None)';
  rsrvam_par_TabDelAll = 'All.';
  rsrvam_par_Pagination = 'Pagination';
  rsrvam_par_KeepWithNext = '&Keep with next';
  rsrvam_par_KeepLinesTogether = '&Keep lines together';
  rsrvam_par_OutlineLevel = '&Outline level:';
  rsrvam_par_OL_BodyText = 'Body Text';
  rsrvam_par_OL_Level = 'Level %d';
  rsrvam_pp_Title = 'Print Preview';
  rsrvam_pp_PageWidth = 'Page width';
  rsrvam_pp_FullPage = 'Full page';
  rsrvam_pp_Pages = 'Pages:';
  rsrvam_pp_InvalidScale = 'Please enter number from 10 to 500';
  rsrvam_pp_OfNo = 'of %d';
  rsrvam_pp_First = 'First Page';
  rsrvam_pp_Prior = 'Prior Page';
  rsrvam_pp_Next = 'Next Page';
  rsrvam_pp_Last = 'Last Page';
  rsrvam_cs_Title = 'Cell Spacing';
  rsrvam_cs_GB = 'Spacing';
  rsrvam_cs_BetweenCells = 'Between &cells';
  rsrvam_cs_FromTableToCell = 'Between table border and cells';
  rsrvam_cs_Vert1 = '&Vertical:';
  rsrvam_cs_Horz1 = '&Horizontal:';
  rsrvam_cs_Vert2 = 'Ve&rtical:';
  rsrvam_cs_Horz2 = 'H&orizontal:';
  rsrvam_tb_Title = 'Borders';
  rsrvam_tb_Settings = 'Settings';
  rsrvam_tb_Color = '&Color:';
  rsrvam_tb_LightColor = '&Light color:';
  rsrvam_tb_ShadowColor = 'Shadow &color:';
  rsrvam_tb_Width = '&Width:';
  rsrvam_tb_BorderType = 'Border type';
  rsrvam_tb_BTNone = '&None';
  rsrvam_tb_BTSunken = '&Sunken';
  rsrvam_tb_BTRaised = '&Raised';
  rsrvam_tb_BTFlat = '&Flat';
  rsrvam_spl_Title = 'Split';
  rsrvam_spl_SplitTo = 'Split to';
  rsrvam_spl_Specified = '&Specified number of rows and columns';
  rsrvam_spl_Original = '&Original cells';
  rsrvam_spl_nCols = 'Number of &columns:';
  rsrvam_spl_nRows = 'Number of &rows:';
  rsrvam_spl_Merge = '&Merge before splitting';
  rsrvam_spl_OriginalCols = 'Split to original co&lumns';
  rsrvam_spl_OriginalRows = 'Split to original ro&ws';
  rsrvam_ar_Title = 'Add Rows';
  rsrvam_ar_Prompt = 'Row &count:';
  rsrvam_pg_Title = 'Page Setup';
  rsrvam_pg_PageTab = 'Page';
  rsrvam_pg_HFTab = 'Header and Footer';
  rsrvam_pg_MarginsMM = 'Margins (millimeters)';
  rsrvam_pg_MarginsInch = 'Margins (inches)';
  rsrvam_pg_Left = '&Left:';
  rsrvam_pg_Top = '&Top:';
  rsrvam_pg_Right = '&Right:';
  rsrvam_pg_Bottom = '&Bottom:';
  rsrvam_pg_MirrorMargins = '&Mirror margins';
  rsrvam_pg_Orientation = 'Orientation';
  rsrvam_pg_Portrait = '&Portrait';
  rsrvam_pg_Landscape = 'L&andscape';
  rsrvam_pg_Paper = 'Paper';
  rsrvam_pg_Size = 'Si&ze:';
  rsrvam_pg_Source = '&Source:';
  rsrvam_pg_Header = 'Header';
  rsrvam_pg_HText = '&Text:';
  rsrvam_pg_HOnFirstPage = '&Header on the first page';
  rsrvam_pg_HFont = '&Font...';
  rsrvam_pg_HLeft = '&Left';
  rsrvam_pg_HCenter = '&Center';
  rsrvam_pg_HRight = '&Right';
  rsrvam_pg_Footer = 'Footer';
  rsrvam_pg_FText = 'Te&xt:';
  rsrvam_pg_FOnFirstPage = 'Footer on the first &page';
  rsrvam_pg_FFont = 'F&ont...';
  rsrvam_pg_FLeft = 'L&eft';
  rsrvam_pg_FCenter = 'Ce&nter';
  rsrvam_pg_FRight = 'R&ight';
  rsrvam_pg_PageNumbers = 'Page numbers';
  rsrvam_pg_StartFrom = '&Start from:';
  rsrvam_pg_Codes = 'Special character combinations:'#13'&&p - page number; &&P - count of pages; &&d - current date; &&t - current time.';
  rsrvam_ccp_Title = 'Text File Code Page';
  rsrvam_ccp_Label = '&Choose the file encoding:';
  rsrvam_ccp_Thai = 'Thai';
  rsrvam_ccp_Japanese = 'Japanese';
  rsrvam_ccp_ChineseSimplified = 'Chinese (Simplified)';
  rsrvam_ccp_Korean = 'Korean';
  rsrvam_ccp_ChineseTraditional = 'Chinese (Traditional)';
  rsrvam_ccp_CentralEuropean = 'Central and East European';
  rsrvam_ccp_Cyrillic = 'Cyrillic';
  rsrvam_ccp_WestEuropean = 'West European';
  rsrvam_ccp_Greek = 'Greek';
  rsrvam_ccp_Turkish = 'Turkish';
  rsrvam_ccp_Hebrew = 'Hebrew';
  rsrvam_ccp_Arabic = 'Arabic';
  rsrvam_ccp_Baltic = 'Baltic';
  rsrvam_ccp_Vietnamese = 'Vietnamese';
  rsrvam_ccp_UTF8 = 'Unicode (UTF-8)';
  rsrvam_ccp_UTF16 = 'Unicode (UTF-16)';
  rsrvam_cvs_Title = 'Visual Style';
  rsrvam_cvs_Label = '&Select style:';
  rsrvam_cd_Title = 'Convert to Text';
  rsrvam_cd_Label = 'Choose &delimiter:';
  rsrvam_cd_LineBreak = 'line break';
  rsrvam_cd_Tab = 'tab';
  rsrvam_cd_Semicolon = 'semicolon';
  rsrvam_cd_Comma = 'comma';
  rsrvam_ts_Error = 'A table containing merged rows cannot be sorted';
  rsrvam_ts_Title = 'Table Sort';
  rsrvam_ts_MainOptionsGB = 'Sort';
  rsrvam_ts_SortByCol = '&Sort by column:';
  rsrvam_ts_CaseSensitive = '&Case sensitive';
  rsrvam_ts_ExcludeHeadingRow = 'Exclude &heading row';
  rsrvam_ts_Rows = 'Rows to sort: from %d to %d';
  rsrvam_ts_OrderRG = 'Order';
  rsrvam_ts_Ascending = '&Ascending';
  rsrvam_ts_Descending = '&Descending';
  rsrvam_ts_DataType = 'Type';
  rsrvam_ts_Text = '&Text';
  rsrvam_ts_Number = '&Number';
  rsrvam_in_Title = 'Insert Number';
  rsrvam_in_PropGB = 'Properties';
  rsrvam_in_CounterName = '&Counter name:';
  rsrvam_in_NumType = '&Numbering type:';
  rsrvam_in_NumberingRG = 'Numbering';
  rsrvam_in_Continue = 'C&ontinue';
  rsrvam_in_StartFrom = '&Start from:';
  rsrvam_ic_Title = 'Caption';
  rsrvam_in_Label = '&Label:';
  rsrvam_in_ExcludeLabel = '&Exclude label from caption';
  rsrvam_in_Position = 'Position';
  rsrvam_in_Above = '&Above selected object';
  rsrvam_in_Below = '&Below selected object';
  rsrvam_in_CaptionText = 'Caption &text:';
  rsrvam_seq_Numbering = 'Numbering';
  rsrvam_seq_Figure = 'Figure';
  rsrvam_seq_Table = 'Table';
  rsrvam_vs_Title = 'Visible Border Sides';
  rsrvam_vs_GB = 'Border';
  rsrvam_vs_Left = '&Left side';
  rsrvam_vs_Top = '&Top side';
  rsrvam_vs_Right = '&Right side';
  rsrvam_vs_Bottom = '&Bottom side';
  rsrvam_ruler_ColumnMove = 'Resize Table Column';
  rsrvam_ruler_RowMove = 'Resize Table Row';
  rsrvam_ruler_FirstIndent = 'First Line Indent';
  rsrvam_ruler_LeftIndent = 'Left Indent';
  rsrvam_ruler_HangingIndent = 'Hanging Indent';
  rsrvam_ruler_RightIndent = 'Right Indent';
  rsrvam_ruler_listlevel_Dec = 'Promote One Level';
  rsrvam_ruler_listlevel_Inc = 'Demote One Level';
  rsrvam_ruler_MarginBottom = 'Bottom Margin';
  rsrvam_ruler_MarginLeft = 'Left Margin';
  rsrvam_ruler_MarginRight = 'Right Margin';
  rsrvam_ruler_MarginTop = 'Top Margin';
  rsrvam_ruler_tab_Left = 'Left Aligned Tab';
  rsrvam_ruler_tab_Right = 'Right Aligned Tab';
  rsrvam_ruler_tab_Center = 'Center Aligned Tab';
  rsrvam_ruler_tab_Decimal = 'Decimal Aligned Tab';
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  rsrvam_ss_Normal = 'Normal';
  rsrvam_ss_NormalIndent = 'Normal Indent';
  rsrvam_ss_NoSpacing = 'No Spacing';
  rsrvam_ss_HeadingN = 'Heading %d';
  rsrvam_ss_ListParagraph = 'List Paragraph';
  rsrvam_ss_Hyperlink = 'Hyperlink';
  rsrvam_ss_Title = 'Title';
  rsrvam_ss_Subtitle = 'Subtitle';
  rsrvam_ss_Emphasis = 'Emphasis';
  rsrvam_ss_SubtleEmphasis = 'Subtle Emphasis';
  rsrvam_ss_IntenseEmphasis = 'Intense Emphasis';
  rsrvam_ss_Strong = 'Strong';
  rsrvam_ss_Quote = 'Quote';
  rsrvam_ss_IntenseQuote = 'Intense Quote';
  rsrvam_ss_SubtleReference = 'Subtle Reference';
  rsrvam_ss_IntenseReference = 'Intense Reference';
  rsrvam_ss_BlockText = 'Block Text';
  rsrvam_ss_HTMLVariable = 'HTML Variable';
  rsrvam_ss_HTMLCode = 'HTML Code';
  rsrvam_ss_HTMLAcronym = 'HTML Acronym';
  rsrvam_ss_HTMLDefinition = 'HTML Definition';
  rsrvam_ss_HTMLKeyboard = 'HTML Keyboard';
  rsrvam_ss_HTMLSample = 'HTML Sample';
  rsrvam_ss_HTMLTypewriter = 'HTML Typewriter';
  rsrvam_ss_HTMLPreformatted = 'HTML Preformatted';
  rsrvam_ss_HTMLCite = 'HTML Cite';
  rsrvam_ss_Header = 'Header';
  rsrvam_ss_Footer = 'Footer';
  rsrvam_ss_PageNumber = 'Page Number';
  rsrvam_ss_Caption = 'Caption';
  rsrvam_ss_EndnoteReference = 'Endnote Reference';
  rsrvam_ss_FootnoteReference = 'Footnote Reference';
  rsrvam_ss_EndnoteText = 'Endnote Text';
  rsrvam_ss_FootnoteText = 'Footnote Text';
  rsrvam_ss_SidenoteReference = 'Sidenote Reference';
  rsrvam_ss_SidenoteText = 'Sidenote Text';
  rsrvam_sd_Color = 'color';
  rsrvam_sd_BackColor = 'background color';
  rsrvam_sd_Transparent = 'transparent';
  rsrvam_sd_DefColor = 'default';
  rsrvam_sd_UnderlineColor = 'underline color';
  rsrvam_sd_DefBackColor = 'default background color';
  rsrvam_sd_DefTextColor = 'default text color';
  rsrvam_sd_ColorSameAsText = 'same as text';
  rsrvam_sd_ut_Single = 'single';
  rsrvam_sd_ut_Thick = 'thick';
  rsrvam_sd_ut_Double = 'double';
  rsrvam_sd_ut_Dotted = 'dotted';
  rsrvam_sd_ut_ThickDotted = 'thick dotted';
  rsrvam_sd_ut_Dashed = 'dashed';
  rsrvam_sd_ut_ThickDashed = 'thick dashed';
  rsrvam_sd_ut_LongDashed = 'long dashed';
  rsrvam_sd_ut_ThickLongDashed = 'thick long dashed';
  rsrvam_sd_ut_DashDotted = 'dash dotted';
  rsrvam_sd_ut_ThickDashDotted = 'thick dash dotted';
  rsrvam_sd_ut_DashDotDotted = 'dash dot dotted';
  rsrvam_sd_ut_ThickDashDotDotted = 'thick dash dot dotted';
  rsrvam_sd_sss_None = 'not sub/superscript';
  rsrvam_sd_sss_Sub = 'subscript';
  rsrvam_sd_sss_Super = 'superscript';
  rsrvam_sd_BiDi = 'bi-di mode:';
  rsrvam_sd_bidi_Undefined = 'inherited';
  rsrvam_sd_bidi_LTR = 'left to right';
  rsrvam_sd_RTL = 'right to left';
  rsrvam_sd_Bold = 'bold';
  rsrvam_sd_NotBold = 'not bold';
  rsrvam_sd_Italic = 'italic';
  rsrvam_sd_NotItalic = 'not italic';
  rsrvam_sd_Underlined = 'underlined';
  rsrvam_sd_NotUnderlined = 'not underlined';
  rsrvam_sd_DefUnderline = 'default underline';
  rsrvam_sd_StruckOut = 'struck out';
  rsrvam_sd_NotStruckOut = 'not struck out';
  rsrvam_sd_Overlined = 'overlined';
  rsrvam_sd_NotOverlined = 'not overlined';
  rsrvam_sd_AllCapitals = 'all capitals';
  rsrvam_sd_AllCapitalsOff = 'all capitals off';
  rsrvam_sd_VShiftNone = 'without vertical shift';
  rsrvam_sd_VShiftUp = 'shifted by %d%% up';
  rsrvam_sd_VShiftDown = 'shifted by %d%% down';
  rsrvam_sd_CharScaleX = 'characters width';
  rsrvam_sd_CharSpacingNone = 'normal character spacing';
  rsrvam_sd_CharSpacingExp = 'spacing expanded by %s';
  rsrvam_sd_CharSpacingCond = 'spacing condensed by %s';
  rsrvam_sd_Charset = 'script';
  rsrvam_sd_DefFont = 'default font';
  rsrvam_sd_DefPara = 'default paragraph';
  rsrvam_sd_Inherited = 'inherited';
  rsrvam_sd_Highlight = 'highlight';
  rsrvam_sd_DefHyperlink = 'default';
  rsrvam_sd_al_Left = 'left aligned';
  rsrvam_sd_al_Right = 'right aligned';
  rsrvam_sd_al_Centered = 'centered';
  rsrvam_sd_al_Justify = 'justified';
  rsrvam_sd_ls_Percent = 'line height: %d%%';
  rsrvam_sd_ls_Spacing = 'space between lines: %s';
  rsrvam_sd_ls_AtLeast = 'line height: at least %s';
  rsrvam_sd_ls_Exactly = 'line height: exactly %s';
  rsrvam_sd_NoWrap = 'line wrapping disabled';
  rsrvam_sd_Wrap = 'line wrapping';
  rsrvam_sd_KeepLinesTogether = 'keep lines together';
  rsrvam_sd_DoNotKeepLinesTogether = 'do not keep lines together';
  rsrvam_sd_KeepWithNext = 'keep with next paragraph';
  rsrvam_sd_DoNotKeepWithNext = 'do not keep with next paragraph';
  rsrvam_sd_ReadOnly = 'read-only';
  rsrvam_sd_NotReadOnly = 'editable';
  rsrvam_sd_BodyText = 'outline level: body text';
  rsrvam_sd_HeadingLevel = 'outline level: %d';
  rsrvam_sd_FirstLineIndent = 'first line indent';
  rsrvam_sd_LeftIndent = 'left indent';
  rsrvam_sd_RightIndent = 'right indent';
  rsrvam_sd_SpaceBefore = 'space before';
  rsrvam_sd_SpaceAfter = 'space after';
  rsrvam_sd_Tabs = 'tabs';
  rsrvam_sd_NoTabs = 'none';
  rsrvam_sd_TabAlign = 'align';
  rsrvam_sd_TabLeft = 'left';
  rsrvam_sd_TabRight = 'right';
  rsrvam_sd_TabCenter = 'center';
  rsrvam_sd_Leader = 'leader';
  rsrvam_sd_No = 'no';
  rsrvam_sd_Yes = 'yes';
  rsrvam_sd_Left = 'left';
  rsrvam_sd_Top = 'top';
  rsrvam_sd_Right = 'right';
  rsrvam_sd_Bottom = 'bottom';
  rsrvam_sd_brdr_None = 'none';
  rsrvam_sd_brdr_Single = 'single';
  rsrvam_sd_brdr_Double = 'double';
  rsrvam_sd_brdr_Triple = 'triple';
  rsrvam_sd_brdr_ThickInside = 'thick inside';
  rsrvam_sd_brdr_ThickOutside = 'thick outside';
  rsrvam_sd_Background = 'background';
  rsrvam_sd_Border = 'border';
  rsrvam_sd_Padding = 'padding';
  rsrvam_sd_Spacing = 'spacing';
  rsrvam_sd_BorderStyle = 'style';
  rsrvam_sd_BorderWidth = 'width';
  rsrvam_sd_BorderIWidth = 'internal width';
  rsrvam_sd_VisibleSides = 'visible sides';
  rsrvam_si_Title = 'Style Inspector';
  rsrvam_si_Style = 'Style';
  rsrvam_si_NoStyle = '(None)';
  rsrvam_si_Para = 'Paragraph';
  rsrvam_si_Font = 'Font';
  rsrvam_si_Attributes = 'Attributes';
  rsrvam_si_BB = 'Border and background';
  rsrvam_si_Hyperlink = 'Hyperlink';
  rsrvam_si_StandardStyle = '(Standard)';
  rsrvam_st_Title = 'Styles';
  rsrvam_st_GBStyle = 'Style';
  rsrvam_st_Name = '&Name:';
  rsrvam_st_ApplicableTo = 'Applicable &to:';
  rsrvam_st_Parent = '&Based on:';
  rsrvam_st_Parent2 = 'Based on:';
  rsrvam_st_Next = 'Style for &following paragraph:';
  rsrvam_st_Next2 = 'Style for following paragraph:';
  rsrvam_st_QuickAccess = '&Quick access';
  rsrvam_st_Descr = 'Description and &preview:';
  rsrvam_st_ParaTextStyle = 'paragraph and text';
  rsrvam_st_ParaStyle = 'paragraph';
  rsrvam_st_TextStyle = 'text';
  rsrvam_st_ParaAndTextStyles = 'Text and Paragraph Styles';
  rsrvam_st_DefFont = 'Default Font';
  rsrvam_st_Edit = 'Edit';
  rsrvam_st_Reset = 'Reset';
  rsrvam_st_Add = '&Add';
  rsrvam_st_Delete = '&Delete';
  rsrvam_st_AddStandard = 'Add &Standard Style...';
  rsrvam_st_AddCustom = 'Add &Custom Style';
  rsrvam_st_DefStyleName = 'Style %d';
  rsrvam_st_ChooseStyle = 'Choose &style:';
  rsrvam_st_Import = '&Import...';
  rsrvam_st_Export = '&Export...';
  rsrvam_sti_RVStylesFilter = 'RichView Styles (*.rvst)|*.rvst';
  rsrvam_sti_LoadError = 'Error loading file';
  rsrvam_sti_NoStyles = 'This file contains no styles';
  rsrvam_sti_Title = 'Import Styles from File';
  rsrvam_sti_GB = 'Import';
  rsrvam_sti_List = 'I&mport styles:';
  rsrvam_sti_RG = 'Existing styles';
  rsrvam_sti_Override = '&override';
  rsrvam_sti_Rename = '&add renamed';
  rsrvam_sti_Select = '&Select';
  rsrvam_sti_Unselect = '&Unselect';
  rsrvam_sti_Invert = '&Invert';
  rsrvam_sti_SelectAll = '&All Styles';
  rsrvam_sti_SelectNew = '&New Styles';
  rsrvam_sti_SelectOld = '&Existing Styles';
  rsrvam_scb_ClearFormat = 'Clear Format';
  rsrvam_scb_MoreStyles = 'All Styles';
  rsrvam_scb_DlgTitle = 'Styles';
  rsrvam_scb_ChooseStyle = '&Choose style to apply:';
  {$ENDIF}
  rsrvam_spell_Synonyms = '&Synonyms';
  rsrvam_spell_IgnoreAll = '&Ignore All';
  rsrvam_spell_AddToDictionary = '&Add to Dictionary';
  rsrvam_msg_Downloading = 'Downloading %s';
  rsrvam_msg_PrintStart = 'Preparing for printing...';
  rsrvam_msg_Printing = 'Printing the page %d';
  rsrvam_msg_ConvertFromRTF = 'Converting from Rich text format...';
  rsrvam_msg_ConvertToRTF = 'Converting to Rich text format...';
  rsrvam_msg_ReadingFmt = 'Reading %s...';
  rsrvam_msg_WritingFmt = 'Writing %s...';
  rsrvam_msg_FmtText = 'text';
  rsrvam_note_None = 'No Note';
  rsrvam_note_Footnote = 'Footnote';
  rsrvam_note_Endnote = 'Endnote';
  rsrvam_note_Sidenote = 'Sidenote';
  rsrvam_note_TextBox = 'Text Box';

const
  Messages: TRVAMessages =
  (
    rsrvam_Empty,
    // units (names)
    rsrvam_unit_Inches, rsrvam_unit_MM, rsrvam_unit_CM, rsrvam_unit_Picas,
    rsrvam_unit_Pixels, rsrvam_unit_Points,
    // units (N units)
    rsrvam_unitN_Inches, rsrvam_unitN_MM, rsrvam_unitN_CM, rsrvam_unitN_Picas,
    rsrvam_unitN_Pixels, rsrvam_unitN_Points,
    // menus
    rsrvam_menu_File, rsrvam_menu_Edit, rsrvam_menu_Format, rsrvam_menu_Font,
    rsrvam_menu_Para, rsrvam_menu_Insert, rsrvam_menu_Table, rsrvam_menu_Window,
    rsrvam_menu_Help, rsrvam_menu_Exit, rsrvam_menu_View, rsrvam_menu_Tools,
    rsrvam_menu_FontSize, rsrvam_menu_FontStyle, rsrvam_menu_TableSelect,
    rsrvam_menu_TableCellAlign, rsrvam_menu_TableCellBorders, rsrvam_menu_TableCellRotation,
    rsrvam_menu_TextFlow, rsrvam_menu_Notes,
    // ribbon
    rsrvam_ribt_MainTab1, rsrvam_ribt_MainTab2, rsrvam_ribt_View, rsrvam_ribt_Table,
    rsrvam_ribg_Clipboard, rsrvam_ribg_Font, rsrvam_ribg_Paragraph, rsrvam_ribg_List,
    rsrvam_ribg_Edit, rsrvam_ribg_Insert, rsrvam_ribg_Background, rsrvam_ribg_PageLayout,
    rsrvam_ribg_Links, rsrvam_ribg_Notes,
    rsrvam_ribg_DocumentViews, rsrvam_ribg_ShowHide, rsrvam_ribg_Zoom, rsrvam_ribg_Options,
    rsrvam_ribg_TableInsert, rsrvam_ribg_TableDelete,
    rsrvam_ribg_TableOperations, rsrvam_ribg_TableBorders, rsrvam_ribg_Styles,
    rsrvam_rib_ScreenTipFooter, rsrvam_rib_RecentFiles, rsrvam_rib_SaveAsTitle,
    rsrvam_rib_PrintTitle,
    rsrvam_lbl_Units,
    // actions
    rsrvam_act_New, rsrvam_act_NewH,
    rsrvam_act_Open, rsrvam_act_OpenH,
    rsrvam_act_Save, rsrvam_act_SaveH,
    rsrvam_act_SaveAs, rsrvam_act_SaveAsH,
    rsrvam_act_Export, rsrvam_act_ExportH,
    rsrvam_act_Preview, rsrvam_act_PreviewH,
    rsrvam_act_Print, rsrvam_act_PrintH,
    rsrvam_act_QuickPrint, rsrvam_act_QuickPrint2, rsrvam_act_QuickPrintH,
    rsrvam_act_PageSetup, rsrvam_act_PageSetupH,
    rsrvam_act_Cut, rsrvam_act_CutH,
    rsrvam_act_Copy, rsrvam_act_CopyH,
    rsrvam_act_Paste, rsrvam_act_PasteH,
    rsrvam_act_PasteAsText, rsrvam_act_PasteAsTextH,
    rsrvam_act_PasteSpecial, rsrvam_act_PasteSpecialH,
    rsrvam_act_SelectAll, rsrvam_act_SelectAllH,
    rsrvam_act_Undo, rsrvam_act_UndoH,
    rsrvam_act_Redo, rsrvam_act_RedoH,
    rsrvam_act_Find, rsrvam_act_FindH,
    rsrvam_act_FindNext, rsrvam_act_FindNextH,
    rsrvam_act_Replace, rsrvam_act_ReplaceH,
    rsrvam_act_InsertFile, rsrvam_act_InsertFileH,
    rsrvam_act_InsertPicture, rsrvam_act_InsertPictureH,
    rsrvam_act_InsertHLine, rsrvam_act_InsertHLineH,
    rsrvam_act_Hyperlink, rsrvam_act_HyperlinkH,
    rsrvam_act_RemoveHyperlinks, rsrvam_act_RemoveHyperlinksH,
    rsrvam_act_InsertSymbol, rsrvam_act_InsertSymbolH,
    rsrvam_act_InsertNumber, rsrvam_act_InsertNumberH,
    rsrvam_act_InsertItemCaption, rsrvam_act_InsertItemCaptionH,
    rsrvam_act_InsertTextBox, rsrvam_act_InsertTextBoxH,
    rsrvam_act_InsertSidenote, rsrvam_act_InsertSidenoteH,
    rsrvam_act_InsertPageNumber, rsrvam_act_InsertPageNumberH,
    rsrvam_act_ParaList, rsrvam_act_ParaListH,
    rsrvam_act_Bullets, rsrvam_act_BulletsH,
    rsrvam_act_Numbering, rsrvam_act_NumberingH,
    rsrvam_act_Color, rsrvam_act_ColorH,
    rsrvam_act_FillColor, rsrvam_act_FillColorH,
    rsrvam_act_InsertPageBreak, rsrvam_act_InsertPageBreakH,
    rsrvam_act_RemovePageBreak, rsrvam_act_RemovePageBreakH,
    rsrvam_act_ClearLeft, rsrvam_act_ClearLeftH,
    rsrvam_act_ClearRight, rsrvam_act_ClearRightH,
    rsrvam_act_ClearBoth, rsrvam_act_ClearBothH,
    rsrvam_act_ClearNone, rsrvam_act_ClearNoneH,
    rsrvam_act_VAlign, rsrvam_act_VAlignH,                
    rsrvam_act_ItemProperties, rsrvam_act_ItemPropertiesH,
    rsrvam_act_Background, rsrvam_act_BackgroundH,
    rsrvam_act_Paragraph, rsrvam_act_ParagraphH,
    rsrvam_act_IndentInc, rsrvam_act_IndentIncH,
    rsrvam_act_IndentDec, rsrvam_act_IndentDecH,
    rsrvam_act_WordWrap, rsrvam_act_WordWrapH,
    rsrvam_act_AlignLeft, rsrvam_act_AlignLeftH,
    rsrvam_act_AlignRight, rsrvam_act_AlignRightH,
    rsrvam_act_AlignCenter, rsrvam_act_AlignCenterH,
    rsrvam_act_AlignJustify, rsrvam_act_AlignJustifyH,
    rsrvam_act_ParaColor, rsrvam_act_ParaColorH,
    rsrvam_act_LS100, rsrvam_act_LS100H,
    rsrvam_act_LS150, rsrvam_act_LS150H,
    rsrvam_act_LS200, rsrvam_act_LS200H,
    rsrvam_act_ParaBorder, rsrvam_act_ParaBorderH,
    rsrvam_act_InsertTable, rsrvam_act_InsertTable2, rsrvam_act_InsertTableH,
    rsrvam_act_TableInsertRowsAbove, rsrvam_act_TableInsertRowsAboveH,
    rsrvam_act_TableInsertRowsBelow, rsrvam_act_TableInsertRowsBelowH,
    rsrvam_act_TableInsertColsLeft, rsrvam_act_TableInsertColsLeftH,
    rsrvam_act_TableInsertColsRight, rsrvam_act_TableInsertColsRightH,
    rsrvam_act_TableDeleteRows, rsrvam_act_TableDeleteRowsH,
    rsrvam_act_TableDeleteCols, rsrvam_act_TableDeleteColsH,
    rsrvam_act_TableDeleteTable, rsrvam_act_TableDeleteTableH,
    rsrvam_act_TableMergeCells, rsrvam_act_TableMergeCellsH,
    rsrvam_act_TableSplitCells, rsrvam_act_TableSplitCellsH,
    rsrvam_act_TableSelectTable, rsrvam_act_TableSelectTableH,
    rsrvam_act_TableSelectRows, rsrvam_act_TableSelectRowsH,
    rsrvam_act_TableSelectCols, rsrvam_act_TableSelectColsH,
    rsrvam_act_TableSelectCell, rsrvam_act_TableSelectCellH,
    rsrvam_act_TableCellVAlignTop, rsrvam_act_TableCellVAlignTopH,
    rsrvam_act_TableCellVAlignMiddle, rsrvam_act_TableCellVAlignMiddleH,
    rsrvam_act_TableCellVAlignBottom, rsrvam_act_TableCellVAlignBottomH,
    rsrvam_act_TableCellVAlignDefault, rsrvam_act_TableCellVAlignDefaultH,
    rsrvam_act_TableCellRotationNone, rsrvam_act_TableCellRotationNoneH,
    rsrvam_act_TableCellRotation90, rsrvam_act_TableCellRotation90H,
    rsrvam_act_TableCellRotation180, rsrvam_act_TableCellRotation180H,
    rsrvam_act_TableCellRotation270, rsrvam_act_TableCellRotation270H,
    rsrvam_act_TableProperties, rsrvam_act_TablePropertiesH,
    rsrvam_act_TableGrid, rsrvam_act_TableGridH,
    rsrvam_act_TableSplit, rsrvam_act_TableSplitH,
    rsrvam_act_TableToText, rsrvam_act_TableToTextH,
    rsrvam_act_TableSort, rsrvam_act_TableSortH,
    rsrvam_act_TableCellLeftBorder, rsrvam_act_TableCellLeftBorderH,
    rsrvam_act_TableCellRightBorder, rsrvam_act_TableCellRightBorderH,
    rsrvam_act_TableCellTopBorder, rsrvam_act_TableCellTopBorderH,
    rsrvam_act_TableCellBottomBorder, rsrvam_act_TableCellBottomBorderH,
    rsrvam_act_TableCellAllBorders, rsrvam_act_TableCellAllBordersH,
    rsrvam_act_TableCellNoBorders, rsrvam_act_TableCellNoBordersH,
    rsrvam_act_Font, rsrvam_act_FontH,
    rsrvam_act_Bold, rsrvam_act_BoldH,
    rsrvam_act_Italic, rsrvam_act_ItalicH,
    rsrvam_act_Underline, rsrvam_act_UnderlineH,
    rsrvam_act_StrikeOut, rsrvam_act_StrikeOutH,
    rsrvam_act_FontGrow, rsrvam_act_FontGrowH,
    rsrvam_act_FontShrink, rsrvam_act_FontShrinkH,
    rsrvam_act_FontGrow1Pt, rsrvam_act_FontGrow1PtH,
    rsrvam_act_FontShrink1Pt, rsrvam_act_FontShrink1PtH,
    rsrvam_act_AllCaps, rsrvam_act_AllCapsH,
    rsrvam_act_Overline, rsrvam_act_OverlineH,
    rsrvam_act_TextColor, rsrvam_act_TextColorH,
    rsrvam_act_TextBackColor, rsrvam_act_TextBackColorH,
    rsrvam_act_Spell, rsrvam_act_SpellH,
    rsrvam_act_Thesaurus, rsrvam_act_ThesaurusH,
    rsrvam_act_ParaLTR, rsrvam_act_ParaLTRH,
    rsrvam_act_ParaRTL, rsrvam_act_ParaRTLH,
    rsrvam_act_TextLTR, rsrvam_act_TextLTRH,
    rsrvam_act_TextRTL, rsrvam_act_TextRTLH,
    rsrvam_act_CharCase, rsrvam_act_CharCaseH,
    rsrvam_act_ShowSpecialCharacters, rsrvam_act_ShowSpecialCharactersH,
    rsrvam_act_Subscript, rsrvam_act_SubscriptH,
    rsrvam_act_Superscript, rsrvam_act_SuperscriptH,
    rsrvam_act_InsertFootnote, rsrvam_act_InsertFootnoteH,
    rsrvam_act_InsertEndnote, rsrvam_act_InsertEndnoteH,
    rsrvam_act_EditNote, rsrvam_act_EditNoteH,
    rsrvam_act_Hide, rsrvam_act_HideH,
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    rsrvam_act_Styles, rsrvam_act_StylesH,
    rsrvam_act_AddStyle, rsrvam_act_AddStyleH,
    rsrvam_act_ClearFormat, rsrvam_act_ClearFormatH,
    rsrvam_act_ClearTextFormat, rsrvam_act_ClearTextFormatH,
    rsrvam_act_StyleInspector, rsrvam_act_CStyleInspectorH,
    {$ENDIF}
    // buttons
    rsrvam_btn_OK, rsrvam_btn_Cancel, rsrvam_btn_Close, rsrvam_btn_Insert,
    rsrvam_btn_Open, rsrvam_btn_Save, rsrvam_btn_Clear, rsrvam_btn_Help,
    rsrvam_btn_Remove,
    // others
    rsrvam_Percents,
    rsrvam_LeftSide, rsrvam_TopSide, rsrvam_RightSide, rsrvam_BottomSide,
    rsrvam_SaveChanges, rsrvam_Confirm, rsrvam_LostFormat,
    rsrvam_RVF,
    // error messages
    rsrvam_err_Title, rsrvam_err_ErrorLoadingFile, rsrvam_err_ErrorLoadingImageFile,
    rsrvam_err_ErrorSavingFile,
    // file filters
    rsrvam_flt_RVF, rsrvam_flt_RTF, rsrvam_flt_XML, rsrvam_flt_TextAnsi, rsrvam_flt_TextUnicode,
    rsrvam_flt_TextAuto, rsrvam_flt_HTMLOpen, rsrvam_flt_HTMLCSS, rsrvam_flt_HTMLPlain,
    rsrvam_flt_DocX,
    // search and replace
    rsrvam_src_Complete, rsrvam_src_NotFound, rsrvam_src_1Replaced, rsrvam_src_NReplaced,
    rsrvam_src_ContinueFromStart, rsrvam_src_ContinueFromEnd,
    // colors
    rsrvam_cl_Transparent, rsrvam_cl_Auto,
    rsrvam_cl_Black, rsrvam_cl_Brown, rsrvam_cl_OliveGreen, rsrvam_cl_DarkGreen,
    rsrvam_cl_DarkTeal, rsrvam_cl_DarkBlue, rsrvam_cl_Indigo, rsrvam_cl_Gray80,
    rsrvam_cl_DarkRed, rsrvam_cl_Orange, rsrvam_cl_DarkYellow, rsrvam_cl_Green,
    rsrvam_cl_Teal, rsrvam_cl_Blue, rsrvam_cl_Blue_Gray, rsrvam_cl_Gray50,
    rsrvam_cl_Red, rsrvam_cl_LightOrange, rsrvam_cl_Lime, rsrvam_cl_SeaGreen,
    rsrvam_cl_Aqua, rsrvam_cl_LightBlue, rsrvam_cl_Violet, rsrvam_cl_Grey,
    rsrvam_cl_Pink, rsrvam_cl_Gold, rsrvam_cl_Yellow, rsrvam_cl_BrightGreen,
    rsrvam_cl_Turquoise, rsrvam_cl_SkyBlue, rsrvam_cl_Plum, rsrvam_cl_Gray25,
    rsrvam_cl_Rose, rsrvam_cl_Tan, rsrvam_cl_LightYellow, rsrvam_cl_LightGreen,
    rsrvam_cl_LightTurquoise, rsrvam_cl_PaleBlue, rsrvam_cl_Lavender, rsrvam_cl_White,
    // color picker buttons
    rsrvam_cpcl_Transparent, rsrvam_cpcl_Auto, rsrvam_cpcl_More, rsrvam_cpcl_Default,
    // background form
    rsrvam_back_Title, rsrvam_back_Color, rsrvam_back_Position, rsrvam_back_Background, rsrvam_back_SampleText,
    rsrvam_back_None, rsrvam_back_FullWindow, rsrvam_back_FixedTiles, rsrvam_back_Tiles,
    rsrvam_back_Center, rsrvam_back_Padding,
    // fill color form
    rsrvam_fillc_Title, rsrvam_fillc_ApplyTo, rsrvam_fillc_MoreColors, rsrvam_fillc_Padding,
    rsrvam_fillc_PleaseSelect,
    rsrvam_fillc_Text, rsrvam_fillc_Paragraph, rsrvam_fillc_Table, rsrvam_fillc_Cell,
    // font form
    rsrvam_font_Title, rsrvam_font_FontTab, rsrvam_font_LayoutTab,
    rsrvam_font_FontName, rsrvam_font_FontSize, rsrvam_font_FontStyle,
    rsrvam_font_Bold, rsrvam_font_Italic, rsrvam_font_Script, rsrvam_font_Color, rsrvam_font_BackColor,
    rsrvam_font_DefaultCharset, rsrvam_font_Effects, rsrvam_font_Underline,
    rsrvam_font_Overline, rsrvam_font_Strikethrough, rsrvam_font_AllCaps,
    rsrvam_font_Sample, rsrvam_font_SampleText,
    rsrvam_font_SpacingH, rsrvam_font_Spacing, rsrvam_font_Expanded, rsrvam_font_Condensed,
    rsrvam_font_OffsetH, rsrvam_font_Offset, rsrvam_font_Down, rsrvam_font_Up,
    rsrvam_font_ScalingH, rsrvam_font_Scaling,
    rsrvam_font_ScriptH, rsrvam_font_SSNorm, rsrvam_font_SSSub, rsrvam_font_SSSuper,
    // 4 sides form
    rsrvam_4s_DefTitle, rsrvam_4s_Top, rsrvam_4s_Left, rsrvam_4s_Bottom, rsrvam_4s_Right, rsrvam_4s_EqualValues,
    // hyperlink form
    rsrvam_hl_Title, rsrvam_hl_GBTitle, rsrvam_hl_Text, rsrvam_hl_Target, rsrvam_hl_Selection,
    rsrvam_hl_CannotNavigate,
    rsrvam_hl_HypProperties, rsrvam_hl_HypStyle,
    // hyperlink properties form
    rsrvam_hp_Title, rsrvam_hp_GBNormal, rsrvam_hp_GBActive,
    rsrvam_hp_Text, rsrvam_hp_TextBack, rsrvam_hp_UnderlineColor,
    rsrvam_hp_HoverText, rsrvam_hp_HoverTextBack, rsrvam_hp_HoverUnderlineColor,
    rsrvam_hp_GBAttributes, rsrvam_hp_UnderlineType,
    rsrvam_hp_UAlways, rsrvam_hp_UNever, rsrvam_hp_UActive,
    rsrvam_hp_AsNormal, rsrvam_hp_UnderlineActiveLinks,
    // insert symbol form
    rsrvam_is_Title, rsrvam_is_Font, rsrvam_is_Charset, rsrvam_is_Unicode,
    rsrvam_is_Block,
    rsrvam_is_CharCode, rsrvam_is_UCharCode, rsrvam_is_NoChar,
    // insert table form
    rsrvam_it_Title, rsrvam_it_TableSize, rsrvam_it_nCols, rsrvam_it_nRows,
    rsrvam_it_TableLayout, rsrvam_it_Autosize, rsrvam_it_Fit, rsrvam_it_Manual,
    rsrvam_it_Remember,
    // valign form
    rsrvam_va_Title,
    // item properties form
    rsrvam_ip_Title, rsrvam_ip_ImageTab, rsrvam_ip_LayoutTab, rsrvam_ip_LineTab,
    rsrvam_ip_TableTab, rsrvam_ip_RowsTab, rsrvam_ip_CellsTab,
    rsrvam_ip_ImgAppTab, rsrvam_ip_ImgMiscTab, rsrvam_ip_SeqTab,
    rsrvam_ip_BoxPosTab, rsrvam_ip_BoxSizeTab, rsrvam_ip_BoxAppTab,
    rsrvam_ip_Preview, rsrvam_ip_Transparency, rsrvam_ip_Transparent, rsrvam_ip_TrColor,
    rsrvam_ip_Change, rsrvam_ip_ImgSave, rsrvam_ip_TrAutoColor,
    rsrvam_ip_VAlign, rsrvam_ip_VAlignValue, rsrvam_ip_VAlign1, rsrvam_ip_VAlign2,
    rsrvam_ip_VAlign3, rsrvam_ip_VAlign4, rsrvam_ip_VAlign5,
    rsrvam_ip_VAlign6, rsrvam_ip_VAlign7,
    rsrvam_ip_ShiftBy,
    rsrvam_ip_Stretch, rsrvam_ip_Width, rsrvam_ip_Height, rsrvam_ip_DefaultSize,
    rsrvam_ip_ImgScaleProportionally, rsrvam_ip_ImgSizeReset,
    rsrvam_ip_ImgInsideBorderGB, rsrvam_ip_ImgBorderGB, rsrvam_ip_ImgOutsideBorderGB,
    rsrvam_ip_ImgFillColor, rsrvam_ip_ImgPadding,
    rsrvam_ip_ImgBorderWidth, rsrvam_ip_ImgBorderColor,
    rsrvam_ip_ImgHSpacing, rsrvam_ip_ImgVSpacing,
    rsrvam_ip_ImgMisc, rsrvam_ip_ImgHint,
    rsrvam_ip_Web, rsrvam_ip_Alt,
    rsrvam_ip_HorzLine, rsrvam_ip_HLColor, rsrvam_ip_HLWidth, rsrvam_ip_HLStyle,
    rsrvam_ip_TableGB, rsrvam_ip_TableWidth, rsrvam_ip_TableColor, rsrvam_ip_CellSpacing,
    rsrvam_ip_CellHPadding, rsrvam_ip_CellVPadding,
    rsrvam_ip_TableBorderGB, rsrvam_ip_TableVisibleBorders,
    rsrvam_ip_TableAutoSizeLbl, rsrvam_ip_TableAutoSize,
    rsrvam_ip_RowKeepTogether,
    rsrvam_ip_CellRotationGB, rsrvam_ip_CellRotation0, rsrvam_ip_CellRotation90,
    rsrvam_ip_CellRotation180, rsrvam_ip_CellRotation270,
    rsrvam_ip_CellBorderLbl, rsrvam_ip_CellVisibleBorders,
    rsrvam_ip_TableBorder, rsrvam_ip_CellBorder,
    rsrvam_ip_TableBorderTitle, rsrvam_ip_CellBorderTitle,
    rsrvam_ip_TablePrinting, rsrvam_ip_KeepOnPage, rsrvam_ip_HeadingRows, rsrvam_ip_HeadingRowsTip,
    rsrvam_ip_VATop, rsrvam_ip_VACenter, rsrvam_ip_VABottom, rsrvam_ip_VADefault,
    rsrvam_ip_CellSettings, rsrvam_ip_CellBestWidth, rsrvam_ip_CellBestHeight,
    rsrvam_ip_CellFillColor, rsrvam_ip_CellBorderGB, rsrvam_ip_VisibleSides,
    rsrvam_ip_CellShadowColor, rsrvam_ip_CellLightColor, rsrvam_ip_CellBorderColor,
    rsrvam_ip_BackgroundImage,
    rsrvam_ip_BoxHPosGB, rsrvam_ip_BoxVPosGB, rsrvam_ip_BoxPosInTextGB,
    rsrvam_ip_BoxPosAlign, rsrvam_ip_BoxPosAbsolute, rsrvam_ip_BoxPosRelative,
    rsrvam_ip_BoxHPosLeft, rsrvam_ip_BoxHPosCenter, rsrvam_ip_BoxHPosRight,
    rsrvam_ip_BoxVPosTop, rsrvam_ip_BoxVPosCenter, rsrvam_ip_BoxVPosBottom,
    rsrvam_ip_BoxPosRelativeTo,  rsrvam_ip_BoxPosToTheRightOfLeftSideOf, rsrvam_ip_BoxPosBelowTheTopSideOf,
    rsrvam_ip_BoxAnchorPage, rsrvam_ip_BoxAnchorMainTextArea,
    rsrvam_ip_BoxAnchorPage2, rsrvam_ip_BoxAnchorMainTextArea2,    
    rsrvam_ip_BoxAnchorLeftMargin, rsrvam_ip_BoxAnchorRightMargin,
    rsrvam_ip_BoxAnchorTopMargin, rsrvam_ip_BoxAnchorBottomMargin,
    rsrvam_ip_BoxAnchorInnerMargin, rsrvam_ip_BoxAnchorOuterMargin,
    rsrvam_ip_BoxAnchorCharacter, rsrvam_ip_BoxAnchorLine, rsrvam_ip_BoxAnchorPara,
    rsrvam_ip_BoxMirroredMargins, rsrvam_ip_BoxLayoutInTableCell,
    rsrvam_ip_BoxAboveText, rsrvam_ip_BoxBelowText,
    rsrvam_ip_BoxWidthGB, rsrvam_ip_BoxHeightGB,
    rsrvam_ip_BoxWidth, rsrvam_ip_BoxHeight,
    rsrvam_ip_BoxHeightAutoLbl, rsrvam_ip_BoxHeightAuto, rsrvam_ip_BoxVAlign,
    rsrvam_ip_BoxVAlignTop, rsrvam_ip_BoxVAlignCenter, rsrvam_ip_BoxVAlignBottom,
    rsrvam_ip_BoxBorderAndBack, rsrvam_ip_BoxBorderAndBackTitle,
    // paste special form
    rsrvam_ps_Title, rsrvam_ps_Label, rsrvam_ps_RTF, rsrvam_ps_HTML, rsrvam_ps_Text,
    rsrvam_ps_UnicodeText, rsrvam_ps_BMP, rsrvam_ps_WMF, rsrvam_ps_GraphicFiles,
    rsrvam_ps_URL,
    rsrvam_ps_Options, rsrvam_ps_Styles,
    rsrvam_ps_Styles1, rsrvam_ps_Styles2, rsrvam_ps_Styles3,
    // list gallery form
    rsrvam_lg_Title, rsrvam_lg_BulletTab, rsrvam_lg_NumTab, rsrvam_lg_BulletGB, rsrvam_lg_NumGB,
    rsrvam_lg_Customize, rsrvam_lg_Reset, rsrvam_lg_None,
    rsrvam_lg_NumContinue, rsrvam_lg_NumReset, rsrvam_lg_NumCreate,
    // list gallery 2 form (HTML)
    rsrvam_lg2_UpperAlpha, rsrvam_lg2_UpperRoman, rsrvam_lg2_Decimal, rsrvam_lg2_LowerAlpha, rsrvam_lg2_LowerRoman,
    rsrvam_lg2_Circle, rsrvam_lg2_Disc, rsrvam_lg2_Square,
    rsrvam_lg2_Level, rsrvam_lg2_StartFrom, rsrvam_lg2_Continue, rsrvam_lg2_Numbering,
    // customize list form
    rsrvam_cul_Title, rsrvam_cul_Levels, rsrvam_cul_LevelCount, rsrvam_cul_ListProperties, rsrvam_cul_ListType,
    rsrvam_cul_ListTypeBullet, rsrvam_cul_ListTypeImage, rsrvam_cul_InsertNumberHint,
    rsrvam_cul_NumberFormat, rsrvam_cul_Number, rsrvam_cul_StartFrom, rsrvam_cul_Font,
    rsrvam_cul_Image, rsrvam_cul_BulletCharacter, rsrvam_cul_Bullet,
    rsrvam_cul_ListTextPos, rsrvam_cul_BulletPos, rsrvam_cul_NumPos, rsrvam_cul_ImagePos, rsrvam_cul_TextPos,
    rsrvam_cul_At, rsrvam_cul_LeftIndent, rsrvam_cul_FirstIndent, rsrvam_cul_FromLeftIndent,
    rsrvam_cul_OneLevelPreview, rsrvam_cul_Preview, rsrvam_cul_PreviewText,
    rsrvam_cul_Left, rsrvam_cul_Right, rsrvam_cul_Center,
    rsrvam_cul_LevelNo, rsrvam_cul_ThisLevel, rsrvam_cul_BulletTitle,
    // paragraph border and backgound form
    rsrvam_pbb_Title, rsrvam_pbb_BorderTab, rsrvam_pbb_BackgroundTab,
    rsrvam_pbb_Settings, rsrvam_pbb_Color, rsrvam_pbb_Width, rsrvam_pbb_InternalWidth,
    rsrvam_pbb_Offsets, rsrvam_pbb_Sample, rsrvam_pbb_BorderType,
    rsrvam_pbb_BTNone, rsrvam_pbb_BTSingle, rsrvam_pbb_BTDouble, rsrvam_pbb_BTTriple,
    rsrvam_pbb_BTThickInside, rsrvam_pbb_BTThickOutside,
    rsrvam_pbb_FillColor, rsrvam_pbb_MoreColors, rsrvam_pbb_Padding,
    rsrvam_pbb_PreviewText,
    rsrvam_pbb_OffsetsTitle, rsrvam_pbb_OffsetsGB,
    // paragraph form
    rsrvam_par_Title, rsrvam_par_Alignment, rsrvam_par_AlLeft, rsrvam_par_AlRight,
    rsrvam_par_AlCenter, rsrvam_par_AlJustify, rsrvam_par_Spacing,
    rsrvam_par_Before, rsrvam_par_After, rsrvam_par_LineSpacing, rsrvam_par_By,
    rsrvam_par_Indents, rsrvam_par_Left, rsrvam_par_Right, rsrvam_par_FirstLine,
    rsrvam_par_Indented, rsrvam_par_Hanging, rsrvam_par_Sample,
    rsrvam_par_LS_100, rsrvam_par_150, rsrvam_par_200,
    rsrvam_par_AtLeast, rsrvam_par_Exactly, rsrvam_par_Multiple,
    rsrvam_par_Preview,
    rsrvam_par_MainTab, rsrvam_par_TabsTab, rsrvam_par_TextFlowTab,
    rsrvam_par_TabStopPos, rsrvam_par_btnSet, rsrvam_par_Delete, rsrvam_par_DeleteAll,
    rsrvam_par_TabAlign,
    rsrvam_par_TabAlignLeft, rsrvam_par_TabAlignRight, rsrvam_par_TabAlignCenter,
    rsrvam_par_Leader, rsrvam_par_LeaderNone, rsrvam_par_TabsToBeDeleted,
    rsrvam_par_TabDelNone, rsrvam_par_TabDelAll,
    rsrvam_par_Pagination, rsrvam_par_KeepWithNext, rsrvam_par_KeepLinesTogether,
    rsrvam_par_OutlineLevel, rsrvam_par_OL_BodyText, rsrvam_par_OL_Level,
    // preview form
    rsrvam_pp_Title, rsrvam_pp_PageWidth, rsrvam_pp_FullPage, rsrvam_pp_Pages,
    rsrvam_pp_InvalidScale, rsrvam_pp_OfNo,
    rsrvam_pp_First, rsrvam_pp_Prior, rsrvam_pp_Next, rsrvam_pp_Last,
    // cell spacing form
    rsrvam_cs_Title, rsrvam_cs_GB, rsrvam_cs_BetweenCells, rsrvam_cs_FromTableToCell,
    rsrvam_cs_Vert1, rsrvam_cs_Horz1, rsrvam_cs_Vert2, rsrvam_cs_Horz2,
    // table borders form
    rsrvam_tb_Title, rsrvam_tb_Settings, rsrvam_tb_Color, rsrvam_tb_LightColor,
    rsrvam_tb_ShadowColor, rsrvam_tb_Width, rsrvam_tb_BorderType,
    rsrvam_tb_BTNone, rsrvam_tb_BTSunken, rsrvam_tb_BTRaised, rsrvam_tb_BTFlat,
    // split cells form
    rsrvam_spl_Title, rsrvam_spl_SplitTo, rsrvam_spl_Specified, rsrvam_spl_Original,
    rsrvam_spl_nCols, rsrvam_spl_nRows, rsrvam_spl_Merge, rsrvam_spl_OriginalCols,
    rsrvam_spl_OriginalRows,
    // add rows form
    rsrvam_ar_Title, rsrvam_ar_Prompt,
    // page setup form
    rsrvam_pg_Title, rsrvam_pg_PageTab, rsrvam_pg_HFTab,
    rsrvam_pg_MarginsMM, rsrvam_pg_MarginsInch, rsrvam_pg_Left, rsrvam_pg_Top, rsrvam_pg_Right, rsrvam_pg_Bottom,
    rsrvam_pg_MirrorMargins,
    rsrvam_pg_Orientation, rsrvam_pg_Portrait, rsrvam_pg_Landscape,
    rsrvam_pg_Paper, rsrvam_pg_Size, rsrvam_pg_Source,
    rsrvam_pg_Header, rsrvam_pg_HText, rsrvam_pg_HOnFirstPage, rsrvam_pg_HFont,
    rsrvam_pg_HLeft, rsrvam_pg_HCenter, rsrvam_pg_HRight,
    rsrvam_pg_Footer, rsrvam_pg_FText, rsrvam_pg_FOnFirstPage, rsrvam_pg_FFont,
    rsrvam_pg_FLeft, rsrvam_pg_FCenter, rsrvam_pg_FRight,
    rsrvam_pg_PageNumbers, rsrvam_pg_StartFrom,
    rsrvam_pg_Codes,
    // choose code page form
    rsrvam_ccp_Title, rsrvam_ccp_Label,
    rsrvam_ccp_Thai, rsrvam_ccp_Japanese, rsrvam_ccp_ChineseSimplified,
    rsrvam_ccp_Korean, rsrvam_ccp_ChineseTraditional, rsrvam_ccp_CentralEuropean,
    rsrvam_ccp_Cyrillic, rsrvam_ccp_WestEuropean, rsrvam_ccp_Greek,
    rsrvam_ccp_Turkish, rsrvam_ccp_Hebrew, rsrvam_ccp_Arabic,
    rsrvam_ccp_Baltic, rsrvam_ccp_Vietnamese, rsrvam_ccp_UTF8, rsrvam_ccp_UTF16,
    // choose style form
    rsrvam_cvs_Title, rsrvam_cvs_Label,
    // choose delimiter form
    rsrvam_cd_Title, rsrvam_cd_Label,
    rsrvam_cd_LineBreak, rsrvam_cd_Tab, rsrvam_cd_Semicolon, rsrvam_cd_Comma,
    // table sort form
    rsrvam_ts_Error, rsrvam_ts_Title, rsrvam_ts_MainOptionsGB,
    rsrvam_ts_SortByCol, rsrvam_ts_CaseSensitive,
    rsrvam_ts_ExcludeHeadingRow, rsrvam_ts_Rows,
    rsrvam_ts_OrderRG, rsrvam_ts_Ascending, rsrvam_ts_Descending,
    rsrvam_ts_DataType, rsrvam_ts_Text, rsrvam_ts_Number,
   // insert number form
    rsrvam_in_Title, rsrvam_in_PropGB, rsrvam_in_CounterName, rsrvam_in_NumType,
    rsrvam_in_NumberingRG, rsrvam_in_Continue, rsrvam_in_StartFrom,
    // insert caption form
    rsrvam_ic_Title, rsrvam_in_Label, rsrvam_in_ExcludeLabel, rsrvam_in_Position,
    rsrvam_in_Above, rsrvam_in_Below, rsrvam_in_CaptionText,
    // default sequences
    rsrvam_seq_Numbering, rsrvam_seq_Figure, rsrvam_seq_Table,
    // visible sides form
    rsrvam_vs_Title, rsrvam_vs_GB, rsrvam_vs_Left, rsrvam_vs_Top, rsrvam_vs_Right, rsrvam_vs_Bottom,
    // ruler
    rsrvam_ruler_ColumnMove, rsrvam_ruler_RowMove,
    rsrvam_ruler_FirstIndent, rsrvam_ruler_LeftIndent,
    rsrvam_ruler_HangingIndent, rsrvam_ruler_RightIndent,
    rsrvam_ruler_listlevel_Dec, rsrvam_ruler_listlevel_Inc,
    rsrvam_ruler_MarginBottom, rsrvam_ruler_MarginLeft, rsrvam_ruler_MarginRight, rsrvam_ruler_MarginTop,
    rsrvam_ruler_tab_Left, rsrvam_ruler_tab_Right, rsrvam_ruler_tab_Center,
    rsrvam_ruler_tab_Decimal,
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    // standard styles
    rsrvam_ss_Normal, rsrvam_ss_NormalIndent, rsrvam_ss_NoSpacing, rsrvam_ss_HeadingN,
    rsrvam_ss_ListParagraph, rsrvam_ss_Hyperlink, rsrvam_ss_Title, rsrvam_ss_Subtitle,
    rsrvam_ss_Emphasis, rsrvam_ss_SubtleEmphasis, rsrvam_ss_IntenseEmphasis,
    rsrvam_ss_Strong, rsrvam_ss_Quote, rsrvam_ss_IntenseQuote,
    rsrvam_ss_SubtleReference, rsrvam_ss_IntenseReference, rsrvam_ss_BlockText,
    rsrvam_ss_HTMLVariable, rsrvam_ss_HTMLCode, rsrvam_ss_HTMLAcronym,
    rsrvam_ss_HTMLDefinition, rsrvam_ss_HTMLKeyboard, rsrvam_ss_HTMLSample,
    rsrvam_ss_HTMLTypewriter, rsrvam_ss_HTMLPreformatted, rsrvam_ss_HTMLCite,
    rsrvam_ss_Header, rsrvam_ss_Footer, rsrvam_ss_PageNumber, rsrvam_ss_Caption,
    rsrvam_ss_EndnoteReference, rsrvam_ss_FootnoteReference, rsrvam_ss_EndnoteText,
    rsrvam_ss_FootnoteText, rsrvam_ss_SidenoteReference, rsrvam_ss_SidenoteText,
    // style description
    rsrvam_sd_Color, rsrvam_sd_BackColor, rsrvam_sd_Transparent, rsrvam_sd_DefColor,
    rsrvam_sd_UnderlineColor,
    rsrvam_sd_DefBackColor, rsrvam_sd_DefTextColor, rsrvam_sd_ColorSameAsText,
    rsrvam_sd_ut_Single, rsrvam_sd_ut_Thick, rsrvam_sd_ut_Double, rsrvam_sd_ut_Dotted,
    rsrvam_sd_ut_ThickDotted, rsrvam_sd_ut_Dashed, rsrvam_sd_ut_ThickDashed,
    rsrvam_sd_ut_LongDashed, rsrvam_sd_ut_ThickLongDashed, rsrvam_sd_ut_DashDotted,
    rsrvam_sd_ut_ThickDashDotted, rsrvam_sd_ut_DashDotDotted, rsrvam_sd_ut_ThickDashDotDotted,
    rsrvam_sd_sss_None, rsrvam_sd_sss_Sub, rsrvam_sd_sss_Super,
    rsrvam_sd_BiDi, rsrvam_sd_bidi_Undefined, rsrvam_sd_bidi_LTR, rsrvam_sd_RTL,
    rsrvam_sd_Bold, rsrvam_sd_NotBold,
    rsrvam_sd_Italic, rsrvam_sd_NotItalic,
    rsrvam_sd_Underlined, rsrvam_sd_NotUnderlined, rsrvam_sd_DefUnderline,
    rsrvam_sd_StruckOut, rsrvam_sd_NotStruckOut,
    rsrvam_sd_Overlined, rsrvam_sd_NotOverlined,
    rsrvam_sd_AllCapitals, rsrvam_sd_AllCapitalsOff,
    rsrvam_sd_VShiftNone, rsrvam_sd_VShiftUp, rsrvam_sd_VShiftDown,
    rsrvam_sd_CharScaleX,
    rsrvam_sd_CharSpacingNone, rsrvam_sd_CharSpacingExp, rsrvam_sd_CharSpacingCond,
    rsrvam_sd_Charset,
    rsrvam_sd_DefFont, rsrvam_sd_DefPara, rsrvam_sd_Inherited,
    rsrvam_sd_Highlight, rsrvam_sd_DefHyperlink,
    rsrvam_sd_al_Left, rsrvam_sd_al_Right, rsrvam_sd_al_Centered, rsrvam_sd_al_Justify,
    rsrvam_sd_ls_Percent, rsrvam_sd_ls_Spacing, rsrvam_sd_ls_AtLeast, rsrvam_sd_ls_Exactly,
    rsrvam_sd_NoWrap, rsrvam_sd_Wrap,
    rsrvam_sd_KeepLinesTogether, rsrvam_sd_DoNotKeepLinesTogether,
    rsrvam_sd_KeepWithNext, rsrvam_sd_DoNotKeepWithNext,
    rsrvam_sd_ReadOnly, rsrvam_sd_NotReadOnly,
    rsrvam_sd_BodyText, rsrvam_sd_HeadingLevel,
    rsrvam_sd_FirstLineIndent, rsrvam_sd_LeftIndent, rsrvam_sd_RightIndent,
    rsrvam_sd_SpaceBefore, rsrvam_sd_SpaceAfter,
    rsrvam_sd_Tabs, rsrvam_sd_NoTabs, rsrvam_sd_TabAlign,
    rsrvam_sd_TabLeft, rsrvam_sd_TabRight, rsrvam_sd_TabCenter, rsrvam_sd_Leader,
    rsrvam_sd_No, rsrvam_sd_Yes,
    rsrvam_sd_Left, rsrvam_sd_Top, rsrvam_sd_Right, rsrvam_sd_Bottom,
    rsrvam_sd_brdr_None, rsrvam_sd_brdr_Single, rsrvam_sd_brdr_Double,
    rsrvam_sd_brdr_Triple, rsrvam_sd_brdr_ThickInside, rsrvam_sd_brdr_ThickOutside,
    rsrvam_sd_Background, rsrvam_sd_Border, rsrvam_sd_Padding, rsrvam_sd_Spacing,
    rsrvam_sd_BorderStyle, rsrvam_sd_BorderWidth, rsrvam_sd_BorderIWidth, rsrvam_sd_VisibleSides,
    // style inspector
    rsrvam_si_Title,
    rsrvam_si_Style, rsrvam_si_NoStyle, rsrvam_si_Para, rsrvam_si_Font, rsrvam_si_Attributes,
    rsrvam_si_BB, rsrvam_si_Hyperlink, rsrvam_si_StandardStyle,
    // styles form
    rsrvam_st_Title, rsrvam_st_GBStyle,
    rsrvam_st_Name, rsrvam_st_ApplicableTo,
    rsrvam_st_Parent, rsrvam_st_Parent2, rsrvam_st_Next, rsrvam_st_Next2,
    rsrvam_st_QuickAccess, rsrvam_st_Descr,
    rsrvam_st_ParaTextStyle, rsrvam_st_ParaStyle, rsrvam_st_TextStyle,
    rsrvam_st_ParaAndTextStyles, rsrvam_st_DefFont,
    rsrvam_st_Edit, rsrvam_st_Reset, rsrvam_st_Add, rsrvam_st_Delete,
    rsrvam_st_AddStandard, rsrvam_st_AddCustom, rsrvam_st_DefStyleName,
    rsrvam_st_ChooseStyle,
    rsrvam_st_Import, rsrvam_st_Export,
    // style import form
    rsrvam_sti_RVStylesFilter, rsrvam_sti_LoadError, rsrvam_sti_NoStyles,
    rsrvam_sti_Title, rsrvam_sti_GB, rsrvam_sti_List,
    rsrvam_sti_RG, rsrvam_sti_Override, rsrvam_sti_Rename,
    rsrvam_sti_Select, rsrvam_sti_Unselect, rsrvam_sti_Invert,
    rsrvam_sti_SelectAll, rsrvam_sti_SelectNew, rsrvam_sti_SelectOld,
    // style combo-box
    rsrvam_scb_ClearFormat, rsrvam_scb_MoreStyles,
    rsrvam_scb_DlgTitle, rsrvam_scb_ChooseStyle,
    {$ENDIF}
    // spelling check and thesaurus
    rsrvam_spell_Synonyms, rsrvam_spell_IgnoreAll, rsrvam_spell_AddToDictionary,
    // progress messages
    rsrvam_msg_Downloading, rsrvam_msg_PrintStart, rsrvam_msg_Printing,
    rsrvam_msg_ConvertFromRTF, rsrvam_msg_ConvertToRTF, rsrvam_msg_ReadingFmt,
    rsrvam_msg_WritingFmt, rsrvam_msg_FmtText,
    // edit note
    rsrvam_note_None, rsrvam_note_Footnote, rsrvam_note_Endnote,
    rsrvam_note_Sidenote, rsrvam_note_TextBox
  );


initialization
  RVA_RegisterLanguage('Resource language', 'Resource language', ANSI_CHARSET, @Messages);
{$ENDIF}

end.
