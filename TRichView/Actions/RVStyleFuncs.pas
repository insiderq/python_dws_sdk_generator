unit RVStyleFuncs;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
     Windows, Messages, SysUtils, Forms, Graphics, Classes, Controls, StdCtrls,
     {$IFDEF USERVTNT}
     TntStdCtrls,
     {$ENDIF}
     {$IFDEF RICHVIEWDEFXE2}
     Vcl.Themes,
     {$ENDIF}
     ImgList, RVALocalize,
     RVRVData,
     RVStyle, RVScroll, FontRVFrm, ParaRVFrm, ParaBrdrRVFrm, CRVData,
     HypHoverPropRVFrm,
     RichView, RVEdit, RVTypes;

{$IFNDEF RVDONOTUSESTYLETEMPLATES}
const
  RVMINSTYLETEMPLATECOMBOCOUNT = 10;

type
  TRVStyleTemplateImageIndices = array [TRVStyleTemplateKind] of Integer;

  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVStyleTemplateComboBox = class({$IFDEF USERVTNT}TTntCustomComboBox{$ELSE}TCustomComboBox{$ENDIF})
  private
    FShowClearFormat, FShowAllStyles, FUpdating: Boolean;
    FSmartHeadings: Boolean;
    FRichViewEdit: TCustomRichViewEdit;
    FEditor: TCustomRVControl;
    FImages: TCustomImageList;
    FTextStyleImageIndex, FParaStyleImageIndex, FParaTextStyleImageIndex,
    FClearFormatImageIndex, FAllStylesImageIndex: Integer;
    FOnClickAllStyles: TNotifyEvent;
    FTemporaryShowAllState: Integer;
    FControlPanel: TComponent;
    procedure SetRichViewEdit(Value: TCustomRichViewEdit);
    procedure SetEditor(Value: TCustomRVControl);
    procedure SetImages(Value: TCustomImageList);
    procedure SetControlPanel(Value: TComponent);
    procedure CNCommand(var Message: TWMCommand); message CN_COMMAND;
    procedure DoUpdateSelection(Sender: TObject);
    procedure DoFill(Sender: TObject);
    procedure DoSRVChangeActiveEditor(Sender: TObject);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure MeasureItem(Index: Integer; var Height: Integer); override;
    procedure DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState); override;
    property RichViewEdit: TCustomRichViewEdit read FRichViewEdit write SetRichViewEdit;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Fill;
    procedure UpdateSelection;
    procedure Click; override;
    procedure Localize;
  published
    property Editor: TCustomRVControl read FEditor write SetEditor;
    property ControlPanel: TComponent read FControlPanel write SetControlPanel;
    property ShowAllStyles: Boolean read FShowAllStyles write FShowAllStyles default False;
    property ShowClearFormat: Boolean read FShowClearFormat write FShowClearFormat default True;
    property SmartHeadings: Boolean read FSmartHeadings write FSmartHeadings default True;
    property Images: TCustomImageList read FImages write SetImages;
    property TextStyleImageIndex: Integer read FTextStyleImageIndex write FTextStyleImageIndex default -1;
    property ParaStyleImageIndex: Integer read FParaStyleImageIndex write FParaStyleImageIndex default -1;
    property ParaTextStyleImageIndex: Integer read FParaTextStyleImageIndex write FParaTextStyleImageIndex default -1;
    property ClearFormatImageIndex: Integer read FClearFormatImageIndex write FClearFormatImageIndex default -1;
    property AllStylesImageIndex: Integer read FAllStylesImageIndex write FAllStylesImageIndex default -1;
    property OnClickAllStyles: TNotifyEvent read FOnClickAllStyles write FOnClickAllStyles;
    property Anchors;
    property BiDiMode;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount default 10;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property ItemHeight default 40;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
    {$IFDEF RICHVIEWDEF5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDock;
    property OnStartDrag;
  end;

  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TRVStyleTemplateListBox = class({$IFDEF USERVTNT}TTntCustomListBox{$ELSE}TCustomListBox{$ENDIF})
  private
    FShowClearFormat, FShowAllStyles, FUpdating: Boolean;
    FSmartHeadings: Boolean;
    FRichViewEdit: TCustomRichViewEdit;
    FEditor: TCustomRVControl;    
    FImages: TCustomImageList;
    FTextStyleImageIndex, FParaStyleImageIndex, FParaTextStyleImageIndex,
    FClearFormatImageIndex, FAllStylesImageIndex: Integer;
    FOnClickAllStyles: TNotifyEvent;
    FControlPanel: TComponent;
    procedure SetRichViewEdit(Value: TCustomRichViewEdit);
    procedure SetEditor(Value: TCustomRVControl);    
    procedure SetImages(Value: TCustomImageList);
    procedure SetControlPanel(Value: TComponent);
    procedure DoUpdateSelection(Sender: TObject);
    procedure DoFill(Sender: TObject);
    procedure DoSRVChangeActiveEditor(Sender: TObject);            
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState); override;
    property RichViewEdit: TCustomRichViewEdit read FRichViewEdit write SetRichViewEdit;    
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;    
    procedure Fill;
    procedure UpdateSelection;
    procedure Click; override;
    procedure Localize;
  published
    property Editor: TCustomRVControl read FEditor write SetEditor;
    property ControlPanel: TComponent read FControlPanel write SetControlPanel;
    property ShowAllStyles: Boolean read FShowAllStyles write FShowAllStyles default False;
    property ShowClearFormat: Boolean read FShowClearFormat write FShowClearFormat default True;
    property SmartHeadings: Boolean read FSmartHeadings write FSmartHeadings default True;
    property Images: TCustomImageList read FImages write SetImages;
    property TextStyleImageIndex: Integer read FTextStyleImageIndex write FTextStyleImageIndex default -1;
    property ParaStyleImageIndex: Integer read FParaStyleImageIndex write FParaStyleImageIndex default -1;
    property ParaTextStyleImageIndex: Integer read FParaTextStyleImageIndex write FParaTextStyleImageIndex default -1;
    property ClearFormatImageIndex: Integer read FClearFormatImageIndex write FClearFormatImageIndex default -1;
    property AllStylesImageIndex: Integer read FAllStylesImageIndex write FAllStylesImageIndex default -1;
    property OnClickAllStyles: TNotifyEvent read FOnClickAllStyles write FOnClickAllStyles;
    property Align;
    property Anchors;
    property BiDiMode;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property ItemHeight default 40;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnClick;
    {$IFDEF RICHVIEWDEF5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDock;
    property OnStartDrag;
  end;

procedure StyleTemplateToFontForm(frm: TfrmRVFont; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
procedure StyleTemplateFromFontForm(frm: TfrmRVFont; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
procedure StyleTemplateToHyperlinkForm(frm: TfrmRVHypHoverProp; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle);
procedure StyleTemplateFromHyperlinkForm(frm: TfrmRVHypHoverProp; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle);
procedure StyleTemplateToParaForm(frm: TfrmRVPara; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
procedure StyleTemplateFromParaForm(frm: TfrmRVPara; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; TabsWereDefined: Boolean; ControlPanel: TComponent);
procedure StyleTemplateToParaBorderForm(frm: TfrmRVParaBrdr; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
procedure StyleTemplateFromParaBorderForm(frm: TfrmRVParaBrdr; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);

function DescribeStyleFont(TextStyle: TCustomRVFontInfo; Properties: TRVFontInfoProperties;
  RVStyle: TRVStyle; Root: Boolean; var Descr: TRVALocString;
  ControlPanel: TComponent): Boolean;
function DescribeStyleHyperlink(TextStyle: TCustomRVFontInfo; Properties: TRVFontInfoProperties;
  RVStyle: TRVStyle; Root: Boolean; var Descr: TRVALocString;
  ControlPanel: TComponent): Boolean;
function DescribeStylePara(ParaStyle: TCustomRVParaInfo; Properties: TRVParaInfoProperties;
  RVStyle: TRVStyle; Root: Boolean; var Descr: TRVALocString; ControlPanel: TComponent): Boolean;
function DescribeStyleParaBorder(ParaStyle: TCustomRVParaInfo; Properties: TRVParaInfoProperties;
  RVStyle: TRVStyle; Root: Boolean; var Descr: TRVALocString;
  ControlPanel: TComponent): Boolean;

procedure RVStyleTemplatesToStrings(StyleTemplates: TRVStyleTemplateCollection;
  Strings: TRVALocStrings;
  AssignObjects: Boolean; Kinds: TRVStyleTemplateKinds;
  ExcludeThisStyle: TRVStyleTemplate; OnlyPotintialParents: Boolean;
  ControlPanel: TComponent);
function RVFindStyleTemplateInStrings(Strings: TRVALocStrings;
  StyleTemplate: TRVStyleTemplate): Integer;
function RVFindStyleTemplateNameInStrings(Strings: TRVALocStrings;
  const Name: TRVStyleTemplateName): Integer;

procedure RVDisplayCurrentStyles(rve: TCustomRichViewEdit; rv: TCustomRichView;
  Images: TCustomImageList; FontImageIndex, ParaImageIndex: Integer;
  ControlPanel: TComponent);

procedure RVDisplayStyleTemplate(StyleTemplate: TRVStyleTemplate;
  rvs: TRVStyle;
  rv: TCustomRichView; Images: TCustomImageList;
  FontImageIndex, HyperlinkImageIndex, ParaImageIndex, ParaBorderImageIndex: Integer;
  Editable: Boolean; ControlPanel: TComponent);

procedure RVDisplayStyleTemplateBasic(StyleTemplate: TRVStyleTemplate;
  rv: TCustomRichView; Images: TCustomImageList;
  ImageIndices: TRVStyleTemplateImageIndices; ControlPanel: TComponent);


const RVFontDialogProps: TRVFontInfoProperties =
  [
    rvfiFontName, rvfiSize, rvfiCharset,
    rvfiBold, rvfiItalic, rvfiUnderline, rvfiStrikeout,
    rvfiOverline, rvfiAllCaps, rvfiSubSuperScriptType,
    rvfiVShift, rvfiColor, rvfiBackColor,
    rvfiCharScale,
    rvfiBiDiMode, rvfiCharSpacing,
    rvfiUnderlineType, rvfiUnderlineColor
  ];
const RVFontHyperlinkProps: TRVFontInfoProperties =
  [ rvfiHoverBackColor, rvfiHoverColor, rvfiHoverUnderlineColor, rvfiHoverUnderline ];
const RVParaDialogProps: TRVParaInfoProperties =
  [
    rvpiFirstIndent, rvpiLeftIndent, rvpiRightIndent,
    rvpiSpaceBefore, rvpiSpaceAfter, rvpiAlignment,
    rvpiLineSpacing, rvpiLineSpacingType,
    rvpiNoWrap, rvpiReadOnly,
    rvpiKeepLinesTogether, rvpiKeepWithNext, rvpiTabs,
    rvpiBiDiMode, rvpiOutlineLevel
  ];
const RVParaBorderDialogProps: TRVParaInfoProperties =
  [rvpiBackground_Color..rvpiBorder_Vis_Bottom];

type
  TRVStandardStyle = (
    rvssnNormal, rvssnNormalIndent, rvssnNoSpacing, rvssnHeadingN, rvssnListParagraph,
    rvssnHyperlink, rvssnTitle, rvssnSubtitle,
    rvssnEmphasis, rvssnSubtleEmphasis, rvssnIntenseEmphasis, rvssnStrong,
    rvssnQuote, rvssnIntenseQuote, rvssnSubtleReference, rvssnIntenseReference,
    rvssnBlockText,
    rvssnHTMLVariable, rvssnHTMLCode, rvssnHTMLAcronym, rvssnHTMLDefinition,
    rvssnHTMLKeyboard, rvssnHTMLSample, rvssnHTMLTypewriter, rvssnHTMLPreformatted,
    rvssnHTMLCite,
    rvssnHeader, rvssnFooter, rvssnPageNumber, rvssnCaption,
    rvssnEndnoteReference, rvssnFootnoteReference, rvssnEndnoteText, rvssnFootnoteText,
    rvssnSidenoteReference, rvssnSidenoteText
   );
  TRVStandardStyles = set of TRVStandardStyle;

// do not localize RVStandardStyleNames!
const RVStandardStyleNames: array [TRVStandardStyle] of TRVAnsiString =
  ('Normal', 'Normal Indent', 'No Spacing', 'heading %d', 'List Paragraph',
   'Hyperlink', 'Title', 'Subtitle',
   'Emphasis', 'Subtle Emphasis', 'Intense Emphasis', 'Strong',
   'Quote', 'Intense Quote', 'Subtle Reference', 'Intense Reference',
   'Block Text',
   'HTML Variable', 'HTML Code', 'HTML Acronym', 'HTML Definition',
   'HTML Keyboard', 'HTML Sample', 'HTML Typewriter', 'HTML Preformatted',
   'HTML Cite',
   'header', 'footer', 'page number', 'caption',
   'endnote reference', 'footnote reference', 'endnote text', 'footnote text',
   'Sidenote Reference', 'Sidenote Text');

type
  TRVStandardStylesRec = record
    Styles: TRVStandardStyles;
    HeadingLevels: set of Byte;
  end;

function RVGetStyleType(Name: TRVStyleTemplateName; var StyleType: TRVStandardStyle;
  var Level: Integer): Boolean;
function RVGetStandardStyleName(StyleType: TRVStandardStyle; Level: Integer;
  ControlPanel: TComponent): TRVALocString;
function RVGetStyleName(const Name: TRVStyleTemplateName; MarkStandard: Boolean;
  ControlPanel: TComponent): TRVALocString;
procedure RVSetStandardStyleDefaults(StyleType: TRVStandardStyle; Level: Integer;
  StyleTemplate: TRVStyleTemplate; RVStyle: TRVStyle;
  StyleTemplates, StandardStyleTemplates: TRVStyleTemplateCollection;
  ControlPanel: TComponent);
procedure RVFillStandardStyleInfo(StyleTemplates: TRVStyleTemplateCollection;
  var Info: TRVStandardStylesRec);

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSESTYLETEMPLATES}

uses RichViewActions, BaseRVFrm, RVClasses, RVStr, RVFuncs, RVItem;

procedure StyleTemplateToFontForm(frm: TfrmRVFont; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
var  ValidProperties: TRVFontInfoProperties;
     TextStyle: TCustomRVFontInfo;
  {.......................................................}
  procedure SetCB(Prop: TRVFontInfoProperty; Checked: Boolean; cb: TControl;
    frm: TfrmRVFont);
  begin
    if Prop in ValidProperties then
      frm.SetCheckBoxChecked(cb, Checked)
    else
      frm.SetCheckBoxState(cb, cbGrayed);
  end;
  {......................................................}
begin
  frm.rvs.Units := RVStyle.Units;
  frm.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
  ValidProperties := StyleTemplate.ValidTextProperties;
  TextStyle := StyleTemplate.TextStyle;
  SetCB(rvfiBold, fsBold in TextStyle.Style, frm._cbB, frm);
  SetCB(rvfiItalic, fsItalic in TextStyle.Style, frm._cbI, frm);
  SetCB(rvfiStrikeOut, fsStrikeOut in TextStyle.Style, frm._cbS, frm);
  SetCB(rvfiOverline, rvfsOverline in TextStyle.StyleEx, frm._cbO, frm);
  SetCB(rvfiAllCaps, rvfsAllCaps in TextStyle.StyleEx, frm._cbAC, frm);

  if rvfiUnderline in ValidProperties then
    if fsUnderline in TextStyle.Style then begin
      if rvfiUnderlineType in ValidProperties then
        frm.SetXBoxItemIndex(frm._cmbUnderline, ord(TextStyle.UnderlineType)+1)
      else
        frm.SetXBoxItemIndex(frm._cmbUnderline, 1);
      end
    else begin
      frm.SetXBoxItemIndex(frm._cmbUnderline, 0);
      frm.cmbUnderlineColor.Enabled := False;
    end;
  if (rvfiUnderlineColor in ValidProperties) then
    if not frm.cmbUnderlineColor.Enabled then
      frm.cmbUnderlineColor.Indeterminate := True
    else
      frm.cmbUnderlineColor.ChosenColor := TextStyle.UnderlineColor;

  if rvfiFontName in ValidProperties then
    frm.cmbFont.Text := TextStyle.FontName
  else
    frm.cmbFont.Text := '';
  if rvfiColor in ValidProperties then
    frm.cmbColor.ChosenColor := TextStyle.Color;
  if rvfiBackColor in ValidProperties then
    frm.cmbBackColor.ChosenColor := TextStyle.BackColor;
  frm.cmbFont.ItemIndex := frm.cmbFont.Items.IndexOf(frm.cmbFont.Text);
  frm.cmbFontClick(nil);
  if rvfiSize in ValidProperties then
    frm.cmbSize.Text := FloatToStr(TextStyle.SizeDouble / 2)
  else
    frm.cmbSize.Text := '';
  if rvfiCharset in ValidProperties then
    frm.cmbCharset.ItemIndex := frm.cmbCharset.IndexOfCharset(TextStyle.Charset)
  else
    frm.cmbCharset.ItemIndex := -1;
  if rvfiCharSpacing in ValidProperties then
    if TextStyle.CharSpacing>=0 then begin
      frm.seSpacing.Value := RVStyle.GetAsRVUnits(TextStyle.CharSpacing,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
      frm.rbExp.Checked := True;
      end
      else begin
        frm.seSpacing.Value := RVStyle.GetAsRVUnits(-TextStyle.CharSpacing,
          RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
        frm.rbCond.Checked := True;
      end;
  if rvfiCharScale in ValidProperties then
    frm.seCharScale.Value := TextStyle.CharScale;
  if rvfiVShift  in ValidProperties then begin
    if TextStyle.VShift<0 then
      frm.rbDown.Checked := True
    else if TextStyle.VShift>0 then
      frm.rbUp.Checked := True;
    frm.seShift.Value := abs(TextStyle.VShift);
  end;
  if rvfiSubSuperScriptType in ValidProperties then
    case TextStyle.SubSuperScriptType of
      rvsssNormal:
        frm.rbNormal.Checked := True;
      rvsssSubscript:
        frm.rbSub.Checked := True;
      rvsssSuperScript:
        frm.rbSuper.Checked := True;
    end;
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateFromFontForm(frm: TfrmRVFont; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
var ValidProperties: TRVFontInfoProperties;
  {.......................................................}
  procedure GetCB(Prop: TRVFontInfoProperty; cb: TControl;
    var Style: TFontStyles; OneStyle: TFontStyle;
    frm: TfrmRVFont);
  begin
    if frm.GetCheckBoxState(cb)<>cbGrayed then begin
      ValidProperties := ValidProperties+[Prop];
      if frm.GetCheckBoxChecked(cb) then
        Style := Style+[OneStyle];
    end;
  end;

  procedure GetCB2(Prop: TRVFontInfoProperty; cb: TControl;
    var Style: TRVFontStyles; OneStyle: TRVFontStyle;
    frm: TfrmRVFont);
  begin
    if frm.GetCheckBoxState(cb)<>cbGrayed then begin
      ValidProperties := ValidProperties+[Prop];
      if frm.GetCheckBoxChecked(cb) then
        Style := Style+[OneStyle];
    end;
  end;

var AStyle: TFontStyles;
    AStyleEx: TRVFontStyles;
begin
  ValidProperties := [];
  AStyle := [];
  AStyleEx := [];
  GetCB(rvfiBold,      frm._cbB, AStyle, fsBold, frm);
  GetCB(rvfiItalic,    frm._cbI, AStyle, fsItalic, frm);
  GetCB(rvfiStrikeOut, frm._cbS, AStyle, fsStrikeOut, frm);
  GetCB2(rvfiOverline, frm._cbO, AStyleEx, rvfsOverline, frm);
  GetCB2(rvfiAllCaps,  frm._cbAC, AStyleEx, rvfsAllCaps, frm);
  if frm.GetXBoxItemIndex(frm._cmbUnderline)>=0 then begin
    Include(ValidProperties, rvfiUnderline);
    if frm.GetXBoxItemIndex(frm._cmbUnderline)>0 then begin
      Include(AStyle, fsUnderline);
      Include(ValidProperties, rvfiUnderlineType);
      StyleTemplate.TextStyle.UnderlineType := TRVUnderlineType(frm.GetXBoxItemIndex(frm._cmbUnderline)-1);
    end;
  end;
  if frm.cmbUnderlineColor.Enabled and not frm.cmbUnderlineColor.Indeterminate then begin
    Include(ValidProperties, rvfiUnderlineColor);
    StyleTemplate.TextStyle.UnderlineColor := frm.cmbUnderlineColor.ChosenColor;
  end;

  StyleTemplate.TextStyle.Style := AStyle;
  StyleTemplate.TextStyle.StyleEx := AStyleEx;
  if frm.cmbFont.Text<>'' then begin
    Include(ValidProperties, rvfiFontName);
    StyleTemplate.TextStyle.FontName := frm.cmbFont.Text;
  end;
  if frm.cmbSize.Text<>'' then begin
    try
      StyleTemplate.TextStyle.SizeDouble := Round(StrToFloat(frm.cmbSize.Text)*2);
        Include(ValidProperties, rvfiSize);
    except
    end;
  end;
  if (frm.cmbCharset.ItemIndex>=0) and (frm.cmbCharset.Charsets[frm.cmbCharset.ItemIndex]<>DEFAULT_CHARSET) then begin
    Include(ValidProperties, rvfiCharset);
    StyleTemplate.TextStyle.Charset := frm.cmbCharset.Charsets[frm.cmbCharset.ItemIndex];
  end;
  if not frm.seSpacing.Indeterminate then begin
    Include(ValidProperties, rvfiCharSpacing);
    if frm.rbCond.Checked then
      StyleTemplate.TextStyle.CharSpacing := -frm.rvs.RVUnitsToUnits(
        frm.seSpacing.Value, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)))
    else
      StyleTemplate.TextStyle.CharSpacing := frm.rvs.RVUnitsToUnits(
        frm.seSpacing.Value, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  end;
  if not frm.seCharScale.Indeterminate then begin
    Include(ValidProperties, rvfiCharScale);
    StyleTemplate.TextStyle.CharScale := frm.seCharScale.AsInteger;
  end;
  if not frm.seShift.Indeterminate then begin
    Include(ValidProperties, rvfiVShift);
    if frm.rbUp.Checked then
      StyleTemplate.TextStyle.VShift := frm.seShift.AsInteger
    else
      StyleTemplate.TextStyle.VShift := -frm.seShift.AsInteger;
  end;
  if not frm.cmbColor.Indeterminate then begin
    StyleTemplate.TextStyle.Color := frm.cmbColor.ChosenColor;
    Include(ValidProperties, rvfiColor);
  end;
  if not frm.cmbBackColor.Indeterminate then begin
    StyleTemplate.TextStyle.BackColor := frm.cmbBackColor.ChosenColor;
    Include(ValidProperties, rvfiBackColor);
  end;

  Include(ValidProperties, rvfiSubSuperScriptType);
  if frm.rbNormal.Checked then
    StyleTemplate.TextStyle.SubSuperScriptType := rvsssNormal
  else if frm.rbSuper.Checked then
    StyleTemplate.TextStyle.SubSuperScriptType := rvsssSuperscript
  else if frm.rbSub.Checked then
    StyleTemplate.TextStyle.SubSuperScriptType := rvsssSubscript
  else
    Exclude(ValidProperties, rvfiSubSuperScriptType);

  if TRVAControlPanel(ControlPanel).UserInterface=rvauiHTML then begin
    Include(ValidProperties, rvfiVShift);
    StyleTemplate.TextStyle.VShift := 0;
    Include(ValidProperties, rvfiCharScale);
    StyleTemplate.TextStyle.CharScale := 0;
  end;
  StyleTemplate.ValidTextProperties := StyleTemplate.ValidTextProperties-RVFontDialogProps;
  StyleTemplate.ValidTextProperties := StyleTemplate.ValidTextProperties+ValidProperties;
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateToHyperlinkForm(frm: TfrmRVHypHoverProp;
  StyleTemplate: TRVStyleTemplate; RVStyle: TRVStyle);
begin
  if rvfiHoverColor in StyleTemplate.ValidTextProperties then
    frm.cmbHoverText.ChosenColor  := StyleTemplate.TextStyle.HoverColor;
  if rvfiHoverBackColor in StyleTemplate.ValidTextProperties then
    frm.cmbHoverTextBack.ChosenColor := StyleTemplate.TextStyle.HoverBackColor;
  if rvfiHoverUnderlineColor in StyleTemplate.ValidTextProperties then
    frm.cmbHoverUnderlineColor.ChosenColor := StyleTemplate.TextStyle.HoverUnderlineColor;
  if rvfiHoverUnderline in StyleTemplate.ValidTextProperties then
    frm.SetCheckBoxChecked(frm._cbHoverUnderline, rvheUnderline in StyleTemplate.TextStyle.HoverEffects);
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateFromHyperlinkForm(frm: TfrmRVHypHoverProp; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle);
var ValidProperties: TRVFontInfoProperties;
begin
  ValidProperties := [];
  if not frm.cmbHoverText.Indeterminate then begin
    Include(ValidProperties, rvfiHoverColor);
    StyleTemplate.TextStyle.HoverColor := frm.cmbHoverText.ChosenColor;
  end;
  if not frm.cmbHoverTextBack.Indeterminate then begin
    Include(ValidProperties, rvfiHoverBackColor);
    StyleTemplate.TextStyle.HoverBackColor := frm.cmbHoverTextBack.ChosenColor;
  end;
  if not frm.cmbHoverUnderlineColor.Indeterminate then begin
    Include(ValidProperties, rvfiHoverUnderlineColor);
    StyleTemplate.TextStyle.HoverUnderlineColor := frm.cmbHoverUnderlineColor.ChosenColor;
  end;
  case frm.GetCheckBoxState(frm._cbHoverUnderline) of
    cbUnchecked:
      begin
        Include(ValidProperties, rvfiHoverUnderline);
        StyleTemplate.TextStyle.HoverEffects := StyleTemplate.TextStyle.HoverEffects-[rvheUnderline];
      end;
    cbChecked:
      begin
        Include(ValidProperties, rvfiHoverUnderline);
        StyleTemplate.TextStyle.HoverEffects := StyleTemplate.TextStyle.HoverEffects+[rvheUnderline];
      end;
  end;
  StyleTemplate.ValidTextProperties := StyleTemplate.ValidTextProperties-RVFontHyperlinkProps;
  StyleTemplate.ValidTextProperties := StyleTemplate.ValidTextProperties+ValidProperties;
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateToParaForm(frm: TfrmRVPara; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
var ValidProperties: TRVParaInfoProperties;
begin
  ValidProperties := StyleTemplate.ValidParaProperties;
  frm.rvs.Units := RVStyle.Units;
  if rvpiAlignment in ValidProperties then
    frm.gbAlignment.ItemIndex := ord(StyleTemplate.ParaStyle.Alignment);
  if rvpiFirstIndent in ValidProperties then begin
    if StyleTemplate.ParaStyle.FirstIndent>0 then
      frm.rbPositive.Checked := True
    else if StyleTemplate.ParaStyle.FirstIndent<0 then
      frm.rbNegative.Checked := True;
    frm.seFirstLineIndent.Value := RVStyle.GetAsRVUnits(Abs(StyleTemplate.ParaStyle.FirstIndent),
      TRVAControlPanel(ControlPanel).UnitsDisplay);
  end;
  if rvpiLeftIndent in ValidProperties then
    frm.seLeftIndent.Value := RVStyle.GetAsRVUnits(StyleTemplate.ParaStyle.LeftIndent,
      TRVAControlPanel(ControlPanel).UnitsDisplay);
  if rvpiRightIndent in ValidProperties then
    frm.seRightIndent.Value := RVStyle.GetAsRVUnits(StyleTemplate.ParaStyle.RightIndent,
      TRVAControlPanel(ControlPanel).UnitsDisplay);
  if rvpiSpaceBefore in ValidProperties then
    frm.seSpaceBefore.Value := RVStyle.GetAsRVUnits(StyleTemplate.ParaStyle.SpaceBefore,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  if rvpiSpaceAfter in ValidProperties then
    frm.seSpaceAfter.Value := RVStyle.GetAsRVUnits(StyleTemplate.ParaStyle.SpaceAfter,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  if rvpiOutlineLevel in ValidProperties then
    if (StyleTemplate.ParaStyle.OutlineLevel>=0) and (StyleTemplate.ParaStyle.OutlineLevel<=10) then
      frm.SetXBoxItemIndex(frm._cmbOutlineLevel, StyleTemplate.ParaStyle.OutlineLevel);
  if rvpiLineSpacing in ValidProperties then
    case StyleTemplate.ParaStyle.LineSpacingType of
      rvlsPercent:
        case StyleTemplate.ParaStyle.LineSpacing of
          100:
            frm.SetXBoxItemIndex(frm._cmbLineSpacing, 0);
          150:
            frm.SetXBoxItemIndex(frm._cmbLineSpacing, 1);
          200:
            frm.SetXBoxItemIndex(frm._cmbLineSpacing, 2);
          else begin
            frm.SetXBoxItemIndex(frm._cmbLineSpacing, 5);
            frm.seLineSpacingValue.Value := StyleTemplate.ParaStyle.LineSpacing / 100;
          end;
        end;
      rvlsLineHeightAtLeast, rvlsLineHeightExact:
        begin
          if StyleTemplate.ParaStyle.LineSpacingType=rvlsLineHeightAtLeast then
            frm.SetXBoxItemIndex(frm._cmbLineSpacing, 3)
          else
            frm.SetXBoxItemIndex(frm._cmbLineSpacing, 4);
          frm.UpdateLengthSpinEdit(frm.seLineSpacingValue, True, True);
          frm.seLineSpacingValue.MinValue := frm.seLineSpacingValue.Increment*5;
          frm.seLineSpacingValue.Value := RVStyle.GetAsRVUnits(StyleTemplate.ParaStyle.LineSpacing,
            RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
        end;
    end
  else
    frm.SetXBoxItemIndex(frm._cmbLineSpacing, 5);
  //frm.rv.LeftMargin := rve.LeftMargin  div 2;
  //frm.rv.RightMargin := rve.RightMargin div 2;
  if rvpiKeepWithNext in ValidProperties then
    frm.SetCheckBoxChecked(frm._cbKeepWithNext, rvpaoKeepWithNext in StyleTemplate.ParaStyle.Options);
  if rvpiKeepLinesTogether in ValidProperties then
    frm.SetCheckBoxChecked(frm._cbKeepLinesTogether, rvpaoKeepLinesTogether in StyleTemplate.ParaStyle.Options);
  if rvpiTabs in ValidProperties then
    frm.Tabs := StyleTemplate.ParaStyle.Tabs;
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateFromParaForm(frm: TfrmRVPara; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; TabsWereDefined: Boolean; ControlPanel: TComponent);
  {.........................................................}
  procedure SetOption(Value: Boolean; Option: TRVParaOption);
  begin
    if Value then
      StyleTemplate.ParaStyle.Options := StyleTemplate.ParaStyle.Options+[Option]
    else
      StyleTemplate.ParaStyle.Options := StyleTemplate.ParaStyle.Options-[Option];
  end;
  {.........................................................}

var ValidProperties: TRVParaInfoProperties;
begin
  ValidProperties := [];
  if frm.gbAlignment.ItemIndex>=0 then begin
    Include(ValidProperties, rvpiAlignment);
    StyleTemplate.ParaStyle.Alignment :=  TRVAlignment(frm.gbAlignment.ItemIndex);
  end;
  if not frm.seSpaceBefore.Indeterminate then begin
    Include(ValidProperties, rvpiSpaceBefore);
    StyleTemplate.ParaStyle.SpaceBefore := RVStyle.RVUnitsToUnits(frm.seSpaceBefore.Value,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  end;
  if not frm.seSpaceAfter.Indeterminate then begin
    Include(ValidProperties, rvpiSpaceAfter);
    StyleTemplate.ParaStyle.SpaceAfter :=  RVStyle.RVUnitsToUnits(frm.seSpaceAfter.Value,
      RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  end;
  if not frm.seLeftIndent.Indeterminate then begin
    Include(ValidProperties, rvpiLeftIndent);
    StyleTemplate.ParaStyle.LeftIndent :=  RVStyle.RVUnitsToUnits(frm.seLeftIndent.Value,
      TRVAControlPanel(ControlPanel).UnitsDisplay);
  end;
  if not frm.seRightIndent.Indeterminate then begin
    Include(ValidProperties, rvpiRightIndent);
    StyleTemplate.ParaStyle.RightIndent :=  RVStyle.RVUnitsToUnits(frm.seRightIndent.Value,
      TRVAControlPanel(ControlPanel).UnitsDisplay);
  end;
  if not frm.seFirstLineIndent.Indeterminate then begin
    Include(ValidProperties, rvpiFirstIndent);
    if frm.rbNegative.Checked then
      StyleTemplate.ParaStyle.FirstIndent := -RVStyle.RVUnitsToUnits(frm.seFirstLineIndent.Value,
        TRVAControlPanel(ControlPanel).UnitsDisplay)
    else
      StyleTemplate.ParaStyle.FirstIndent := RVStyle.RVUnitsToUnits(frm.seFirstLineIndent.Value,
        TRVAControlPanel(ControlPanel).UnitsDisplay);
  end;
  if frm.GetXBoxItemIndex(frm._cmbOutlineLevel)>=0 then begin
    StyleTemplate.ParaStyle.OutlineLevel := frm.GetXBoxItemIndex(frm._cmbOutlineLevel);
    Include(ValidProperties, rvpiOutlineLevel);
  end;
  Include(ValidProperties, rvpiLineSpacing);
  StyleTemplate.ParaStyle.LineSpacingType := rvlsPercent;
  case frm.GetXBoxItemIndex(frm._cmbLineSpacing) of
    0: StyleTemplate.ParaStyle.LineSpacing := 100;
    1: StyleTemplate.ParaStyle.LineSpacing := 150;
    2: StyleTemplate.ParaStyle.LineSpacing := 200;
    3: begin
         StyleTemplate.ParaStyle.LineSpacingType := rvlsLineHeightAtLeast;
         if frm.seLineSpacingValue.Indeterminate then
           Exclude(ValidProperties, rvpiLineSpacing)
         else
           StyleTemplate.ParaStyle.LineSpacing := RVStyle.RVUnitsToUnits(
             frm.seLineSpacingValue.AsInteger, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
       end;
    4: begin
         StyleTemplate.ParaStyle.LineSpacingType := rvlsLineHeightExact;
         if frm.seLineSpacingValue.Indeterminate then
           Exclude(ValidProperties, rvpiLineSpacing)
         else
           StyleTemplate.ParaStyle.LineSpacing := RVStyle.RVUnitsToUnits(
             frm.seLineSpacingValue.Value, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
       end;
    5: begin
        if frm.seLineSpacingValue.Indeterminate then
          Exclude(ValidProperties, rvpiLineSpacing)
        else
          StyleTemplate.ParaStyle.LineSpacing := Round(frm.seLineSpacingValue.Value * 100);
       end;
  end;
  if frm.GetCheckBoxState(frm._cbKeepWithNext)<>cbGrayed then begin
    SetOption(frm.GetCheckBoxChecked(frm._cbKeepWithNext), rvpaoKeepWithNext);
    Include(ValidProperties, rvpiKeepWithNext);
  end;
  if frm.GetCheckBoxState(frm._cbKeepLinesTogether)<>cbGrayed then begin
    SetOption(frm.GetCheckBoxChecked(frm._cbKeepLinesTogether), rvpaoKeepLinesTogether);
    Include(ValidProperties, rvpiKeepLinesTogether);
  end;
  if frm.TabsModified or TabsWereDefined then begin
    Include(ValidProperties, rvpiTabs);
    StyleTemplate.ParaStyle.Tabs.Clear;
    StyleTemplate.ParaStyle.Tabs.AddFrom(frm.Tabs);
  end;
  StyleTemplate.ValidParaProperties := StyleTemplate.ValidParaProperties-RVParaDialogProps;
  StyleTemplate.ValidParaProperties := StyleTemplate.ValidParaProperties+ValidProperties;
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateToParaBorderForm(frm: TfrmRVParaBrdr; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
var ValidProperties: TRVParaInfoProperties;
    idx: Integer;
    br: TRVBooleanRect;
begin
  ValidProperties := StyleTemplate.ValidParaProperties;
  frm.rvs.Units := RVStyle.Units;
  frm.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;

  if rvpiBorder_Width in ValidProperties then begin
    idx := RVA_GetComboBorderItemIndex(StyleTemplate.ParaStyle.Border.Width, False,
      RVStyle, frm.GetXBoxItemCount(frm._cmbWidth), TRVAControlPanel(ControlPanel));
    frm.SetXBoxItemIndex(frm._cmbWidth, idx);
  end;
  if rvpiBorder_Color in ValidProperties then
    frm.cmbColor.ChosenColor := StyleTemplate.ParaStyle.Border.Color;
  if rvpiBackground_Color in ValidProperties then begin
    frm.BCIndeterminate := False;
    frm.BackColor := StyleTemplate.ParaStyle.Background.Color;
  end;
  if rvpiBorder_Style in ValidProperties then
    if ord(StyleTemplate.ParaStyle.Border.Style)<= frm.rgBorderType.Items.Count then
      frm.rgBorderType.ItemIndex := ord(StyleTemplate.ParaStyle.Border.Style)
    else
      frm.rgBorderType.ItemIndex := 2; // double
  if rvpiBorder_InternalWidth in ValidProperties then
    frm.seIW.Value :=
      RVStyle.GetAsRVUnits(StyleTemplate.ParaStyle.Border.InternalWidth,
        RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)));
  br := TRVBooleanRect.Create(False);
  br.Left   := rvpiBorder_BO_Left   in ValidProperties;
  br.Top    := rvpiBorder_BO_Top    in ValidProperties;
  br.Right  := rvpiBorder_BO_Right  in ValidProperties;
  br.Bottom := rvpiBorder_BO_Bottom in ValidProperties;
  frm.SetBO(StyleTemplate.ParaStyle.Border.BorderOffsets, br);
  br.Left   := rvpiBackground_BO_Left   in ValidProperties;
  br.Top    := rvpiBackground_BO_Top    in ValidProperties;
  br.Right  := rvpiBackground_BO_Right  in ValidProperties;
  br.Bottom := rvpiBackground_BO_Bottom in ValidProperties;
  frm.SetPadding(StyleTemplate.ParaStyle.Background.BorderOffsets, br);
  br.Free;
  if rvpiBorder_Vis_Left in ValidProperties then begin
    frm.SetButtonFlat(frm._btnLeft, False);
    frm.SetButtonDown(frm._btnLeft, StyleTemplate.ParaStyle.Border.VisibleBorders.Left);
  end;
  if rvpiBorder_Vis_Top in ValidProperties then begin
    frm.SetButtonFlat(frm._btnTop, False);
    frm.SetButtonDown(frm._btnTop, StyleTemplate.ParaStyle.Border.VisibleBorders.Top);
  end;
  if rvpiBorder_Vis_Right in ValidProperties then begin
    frm.SetButtonFlat(frm._btnRight, False);
    frm.SetButtonDown(frm._btnRight, StyleTemplate.ParaStyle.Border.VisibleBorders.Right);
  end;
  if rvpiBorder_Vis_Bottom in ValidProperties then begin
    frm.SetButtonFlat(frm._btnBottom, False);
    frm.SetButtonDown(frm._btnBottom, StyleTemplate.ParaStyle.Border.VisibleBorders.Bottom);
  end;
end;
{------------------------------------------------------------------------------}
procedure StyleTemplateFromParaBorderForm(frm: TfrmRVParaBrdr; StyleTemplate: TRVStyleTemplate;
  RVStyle: TRVStyle; ControlPanel: TComponent);
var br: TRVBooleanRect;
    ValidProperties: TRVParaInfoProperties;
begin
  ValidProperties := [];
  if frm.GetXBoxItemIndex(frm._cmbWidth)>=0 then begin
    Include(ValidProperties,rvpiBorder_Width);
    StyleTemplate.ParaStyle.Border.Width := RVA_GetComboBorderWidth2(frm.GetXBoxItemIndex(frm._cmbWidth),
      False, frm.rvs, TRVAControlPanel(ControlPanel));
  end;
  if not frm.cmbColor.Indeterminate then begin
    Include(ValidProperties,rvpiBorder_Color);
    StyleTemplate.ParaStyle.Border.Color := frm.cmbColor.ChosenColor;
  end;
  if not frm.BCIndeterminate then begin
    Include(ValidProperties,rvpiBackground_Color);
    StyleTemplate.ParaStyle.Background.Color := frm.BackColor;
  end;
  if frm.rgBorderType.ItemIndex>=0 then begin
    Include(ValidProperties,rvpiBorder_Style);
    StyleTemplate.ParaStyle.Border.Style := TRVBorderStyle(frm.rgBorderType.ItemIndex);
  end;
  if not frm.seIW.Indeterminate then begin
    Include(ValidProperties,rvpiBorder_InternalWidth);
    StyleTemplate.ParaStyle.Border.InternalWidth := frm.rvs.RVUnitsToUnits(
      frm.seIW.Value, RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)));
  end;
  br := TRVBooleanRect.Create(False);
  frm.GetBO(StyleTemplate.ParaStyle.Border.BorderOffsets, br);
  if br.Left then
    Include(ValidProperties, rvpiBorder_BO_Left);
  if br.Top then
    Include(ValidProperties, rvpiBorder_BO_Top);
  if br.Right then
    Include(ValidProperties, rvpiBorder_BO_Right);
  if br.Bottom then
    Include(ValidProperties, rvpiBorder_BO_Bottom);
  frm.GetPadding(StyleTemplate.ParaStyle.Background.BorderOffsets, br);
  if br.Left then
    Include(ValidProperties, rvpiBackground_BO_Left);
  if br.Top then
    Include(ValidProperties, rvpiBackground_BO_Top);
  if br.Right then
    Include(ValidProperties, rvpiBackground_BO_Right);
  if br.Bottom then
    Include(ValidProperties, rvpiBackground_BO_Bottom);
  br.Free;

  if not frm.GetButtonFlat(frm._btnLeft) then begin
    Include(ValidProperties, rvpiBorder_Vis_Left);
    StyleTemplate.ParaStyle.Border.VisibleBorders.Left := frm.GetButtonDown(frm._btnLeft);
  end;
  if not frm.GetButtonFlat(frm._btnTop) then begin
    Include(ValidProperties, rvpiBorder_Vis_Top);
    StyleTemplate.ParaStyle.Border.VisibleBorders.Top := frm.GetButtonDown(frm._btnTop);
  end;
  if not frm.GetButtonFlat(frm._btnRight) then begin
    Include(ValidProperties, rvpiBorder_Vis_Right);
    StyleTemplate.ParaStyle.Border.VisibleBorders.Right := frm.GetButtonDown(frm._btnRight);
  end;
  if not frm.GetButtonFlat(frm._btnBottom) then begin
    Include(ValidProperties, rvpiBorder_Vis_Bottom);
    StyleTemplate.ParaStyle.Border.VisibleBorders.Bottom := frm.GetButtonDown(frm._btnBottom);
  end;
  StyleTemplate.ValidParaProperties := StyleTemplate.ValidParaProperties-RVParaBorderDialogProps;
  StyleTemplate.ValidParaProperties := StyleTemplate.ValidParaProperties+ValidProperties;
end;
{==============================================================================}
const
  RVCHAR_LRM       = {$IFDEF RVUNICODESTR}#$200E{$ELSE}#$FD{$ENDIF};
//  RVCHAR_RLM       = {$IFDEF RVUNICODESTR}#$200F{$ELSE}#$FE{$ENDIF};

function DescribeColor(Color: TColor; const DefColorName: TRVALocString;
  ControlPanel: TRVAControlPanel): TRVALocString;
var i: Integer;
begin
  case Color of
    clNone:
      begin
        Result := DefColorName;
        exit;
      end;
    clWindow:
      begin
        Result := RVA_GetS(rvam_sd_DefBackColor, ControlPanel);
        exit;
      end;
    clWindowText:
      begin
        Result := RVA_GetS(rvam_sd_DefTextColor, ControlPanel);
        exit;
      end;
  end;
  for i := 0 to High(ControlPanel.ColorNames) do
    if Color = ControlPanel.ColorNames[i].Color then begin
      {$IFDEF RVUNICODESTR}
      Result := LowerCase(ControlPanel.ColorNames[i].Name);
      {$ELSE}
      {$IFDEF USERVTNT}
      Result := ControlPanel.ColorNames[i].Name;
      {$ELSE}
      Result := ControlPanel.ColorNames[i].Name;
      {$ENDIF}
      {$ENDIF}
      exit;
    end;
  Color := ColorToRGB(Color);
  Result := 'RGB('+
        IntToStr(Color and $0000FF)+','+
        IntToStr((Color and $00FF00) shr 8)+','+
        IntToStr((Color and $FF0000) shr 16)+')';
  if (RVA_GetCharset(ControlPanel)=ARABIC_CHARSET) or
    (RVA_GetCharset(ControlPanel)=HEBREW_CHARSET) then
     Result := Result+RVCHAR_LRM;
end;
{------------------------------------------------------------------------------}
function DescribeValue(Val: TRVStyleLength; RVStyle: TRVStyle; Units: TRVUnits;
  ControlPanel: TRVAControlPanel): TRVALocString;
var v: TRVLength;
begin
  v := RVStyle.GetAsRVUnits(Val, Units);
  if Units=rvuPixels then
    Result := IntToStr(Round(v))
  else
    Result := FloatToStr(v);
  Result := Result +RVA_GetNBSP(ControlPanel)+RVA_GetUnitsName(Units, True, ControlPanel);
end;
{------------------------------------------------------------------------------}
function DescribeStyleFont(TextStyle: TCustomRVFontInfo; Properties: TRVFontInfoProperties;
  RVStyle: TRVStyle; Root: Boolean; var Descr: TRVALocString;
  ControlPanel: TComponent): Boolean;
var s: TRVALocString;
    Delim: TRVALocString;
    First: Boolean;
  {................................}
  procedure Add(const s2: TRVALocString);
  begin
    if s<>'' then begin
      if s2<>'' then
        s := s+Delim+s2
      end
    else
      s := s2;
  end;
  {................................}
  procedure AddStyle(Prop: TRVFontInfoProperty; Style: TFontStyle;
    Msg: TRVAMessageID);
  begin
    if Prop in Properties then
    if Style in TextStyle.Style then
      Add(RVA_GetS(Msg, ControlPanel))
    else if not Root then
      Add(RVA_GetS(succ(Msg), ControlPanel));
  end;
  {................................}
  procedure AddStyleEx(Prop: TRVFontInfoProperty; Style: TRVFontStyle;
    Msg: TRVAMessageID);
  begin
    if Prop in Properties then
    if Style in TextStyle.StyleEx then
      Add(RVA_GetS(Msg, ControlPanel))
    else if not Root then
      Add(RVA_GetS(succ(Msg), ControlPanel));
  end;
  {................................}
  function GetUnderlineTypeStr(Value: TRVUnderlineType): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_ut_Single)+ord(Value)), ControlPanel);
  end;
  {................................}
  function GetSubSuperScriptTypeStr(Value: TRVSubSuperScriptType): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_sss_None)+ord(Value)), ControlPanel);
  end;
  {................................}
  function GetBiDiModeStr(Value: TRVBiDiMode): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_bidi_Undefined)+ord(Value)), ControlPanel);
  end;
  {................................}
begin
  Delim := RVA_GetComma(ControlPanel)+' ';
  s := '';
  if (rvfiFontName in Properties) and (not Root or (AnsiCompareText(TextStyle.FontName, 'Arial') <>0))   then
    Add(TextStyle.FontName);
  if (rvfiSize in Properties) and (not Root or (TextStyle.Size<>10)) then
    Add(FloatToStr(TextStyle.SizeDouble/2)+RVA_GetNBSP(ControlPanel)+RVA_GetUnitsName(rvuPoints, True, ControlPanel));
  if (rvfiCharset in Properties) and (not Root or (TextStyle.Charset<>DEFAULT_CHARSET)) then
    Add(RVA_GetS(rvam_sd_Charset, ControlPanel)+':'+IntToStr(TextStyle.Charset));
  AddStyle(rvfiBold, fsBold, rvam_sd_Bold);
  AddStyle(rvfiItalic, fsItalic, rvam_sd_Italic);
  if rvfiUnderline in Properties then begin
    if fsUnderline in TextStyle.Style then begin
      Add(RVA_GetS(rvam_sd_Underlined, ControlPanel));
      First := True;
      if rvfiUnderlineType in Properties then begin
        if First then
          s := s+' (';
        s := s+GetUnderlineTypeStr(TextStyle.UnderlineType);
        First := False;
      end;
      if rvfiUnderlineColor in Properties then begin
        if not First then
          s := s+Delim
        else
          s := s+' (';
        s := s+RVA_GetS(rvam_sd_Color, ControlPanel)+': '+
          DescribeColor(TextStyle.UnderlineColor, RVA_GetS(rvam_sd_ColorSameAsText, ControlPanel), TRVAControlPanel(ControlPanel));
        First := False;
      end;
      if not First then
        s := s+')';
      end
    else if not Root then
      Add(RVA_GetS(rvam_sd_NotUnderlined, ControlPanel));
  end;
  AddStyle(rvfiStrikeout, fsStrikeout, rvam_sd_StruckOut);
  AddStyleEx(rvfiOverline, rvfsOverline, rvam_sd_Overlined);
  AddStyleEx(rvfiAllCaps, rvfsAllCaps, rvam_sd_AllCapitals);
  if (rvfiSubSuperScriptType in Properties) and (not Root or (TextStyle.SubSuperscriptType<>rvsssNormal)) then
    Add(GetSubSuperScriptTypeStr(TextStyle.SubSuperscriptType));
  if (rvfiVShift in Properties) and (not Root or (TextStyle.VShift<>0))  then begin
    if TextStyle.VShift=0 then
      Add(RVA_GetS(rvam_sd_VShiftNone, ControlPanel))
    else if TextStyle.VShift>0 then
      Add(RVAFormat(RVA_GetS(rvam_sd_VShiftUp, ControlPanel), [TextStyle.VShift]))
    else
      Add(RVAFormat(RVA_GetS(rvam_sd_VShiftDown, ControlPanel), [-TextStyle.VShift]))
  end;
  if (rvfiCharScale in Properties) and (not Root or (TextStyle.CharScale<>100))  then
    Add(RVA_GetS(rvam_sd_CharScaleX, ControlPanel)+': '+IntToStr(TextStyle.CharScale)+'%');
  If (rvfiCharSpacing in Properties) and (not Root or (TextStyle.CharSpacing<>0))  then
    if TextStyle.CharSpacing=0 then
      Add(RVA_GetS(rvam_sd_CharSpacingNone, ControlPanel))
    else if TextStyle.CharSpacing>0 then
      Add(RVAFormat(RVA_GetS(rvam_sd_CharSpacingExp, ControlPanel),
        [DescribeValue(TextStyle.CharSpacing, RVStyle,
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel))]))
    else
      Add(RVAFormat(RVA_GetS(rvam_sd_CharSpacingCond, ControlPanel),
        [DescribeValue(-TextStyle.CharSpacing, RVStyle,
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel))]));
  if (rvfiColor in Properties) and (not Root or (TextStyle.Color<>clWindowText)) then
    Add(RVA_GetS(rvam_sd_Color, ControlPanel)+': '+DescribeColor(TextStyle.Color, '?', TRVAControlPanel(ControlPanel)));
  if (rvfiBackColor in Properties) and (not Root or (TextStyle.BackColor<>clNone))  then
    Add(RVA_GetS(rvam_sd_BackColor, ControlPanel)+': ' +
      DescribeColor(TextStyle.BackColor, RVA_GetS(rvam_sd_Transparent, ControlPanel), TRVAControlPanel(ControlPanel)));
  if (rvfiBiDiMode in Properties) and (not Root or (TextStyle.BiDiMode<>rvbdUnspecified))  then
    Add(RVA_GetS(rvam_sd_BiDi, ControlPanel)+': ' +GetBiDiModeStr(TextStyle.BiDiMode));
  Result := s<>'';
  if s='' then
    if Root then
      s := RVA_GetS(rvam_sd_DefFont, ControlPanel)
    else
      s := RVA_GetS(rvam_sd_Inherited, ControlPanel)
  else
    if Root then
      s := RVA_GetS(rvam_sd_DefFont, ControlPanel)+' + '+s
    else
      s := '+ '+s;
  Descr := s;

end;
{------------------------------------------------------------------------------}
function DescribeStyleHyperlink(TextStyle: TCustomRVFontInfo;
  Properties: TRVFontInfoProperties; RVStyle: TRVStyle; Root: Boolean;
  var Descr: TRVALocString; ControlPanel: TComponent): Boolean;
var s: TRVALocString;
    First: Boolean;
    Delim: TRVALocString;
  {................................}
  procedure Add(const s2: TRVALocString);
  begin
    if s<>'' then begin
      if s2<>'' then
        s := s+Delim+s2
      end
    else
      s := s2;
  end;
  {................................}
  procedure AddSub(const s2: TRVALocString);
  begin
    if not First then
      s := s+Delim
    else
      First := False;
    s := s+s2;
  end;
  {................................}
begin
  Delim := RVA_GetComma(ControlPanel)+' ';
  s := '';
  if Properties * [rvfiHoverBackColor, rvfiHoverColor, rvfiHoverUnderlineColor,
    rvfiHoverUnderline]<>[] then begin
    Add(RVA_GetS(rvam_sd_Highlight, ControlPanel)+ ' (');
    First := True;
    if (rvfiHoverColor in Properties) and (not Root or (TextStyle.HoverColor<>clNone)) then
      AddSub(RVA_Gets(rvam_sd_Color, ControlPanel)+ ': '+
        DescribeColor(TextStyle.HoverColor, RVA_GetS(rvam_sd_DefColor, ControlPanel), TRVAControlPanel(ControlPanel)));
    if (rvfiHoverBackColor in Properties) and not (Root and (TextStyle.HoverBackColor=clNone)) then
      AddSub(RVA_GetS(rvam_sd_BackColor, ControlPanel)+ ': '+
        DescribeColor(TextStyle.HoverBackColor, RVA_GetS(rvam_sd_Transparent, ControlPanel), TRVAControlPanel(ControlPanel)));
    if (rvfiHoverUnderlineColor in Properties) and (not Root or (TextStyle.HoverUnderlineColor<>clNone)) then
      AddSub(RVA_GetS(rvam_sd_UnderlineColor, ControlPanel)+ ': '+
        DescribeColor(TextStyle.HoverUnderlineColor, RVA_GetS(rvam_sd_DefColor, ControlPanel), TRVAControlPanel(ControlPanel)));
    if (rvfiHoverUnderline in Properties) and (not Root or (rvheUnderline in TextStyle.HoverEffects)) then
      if rvheUnderline in TextStyle.HoverEffects then
        AddSub(RVA_GetS(rvam_sd_Underlined, ControlPanel))
      else
        AddSub(RVA_GetS(rvam_sd_DefUnderline, ControlPanel));
    s := s+')';
    if First then
      s := '';
  end;
  Result := s<>'';
  if s='' then
    if Root then
      s := RVA_GetS(rvam_sd_DefHyperlink, ControlPanel)
    else
      s := RVA_GetS(rvam_sd_Inherited, ControlPanel)
  else
    if Root then
      s := RVA_GetS(rvam_sd_DefHyperlink, ControlPanel)+' + '+s
    else
      s := '+ '+s;
  Descr := s;
end;
{------------------------------------------------------------------------------}
function DescribeStylePara(ParaStyle: TCustomRVParaInfo;
  Properties: TRVParaInfoProperties; RVStyle: TRVStyle; Root: Boolean;
  var Descr: TRVALocString; ControlPanel: TComponent): Boolean;
var s, Delim: TRVALocString;
    i: Integer;
  {................................}
  procedure Add(const s2: TRVALocString);
  begin
    if s<>'' then begin
      if s2<>'' then
        s := s+Delim+s2
      end
    else
      s := s2;
  end;
  {................................}
  procedure AddOption(Prop: TRVParaInfoProperty; Option: TRVParaOption;
    Msg: TRVAMessageId);
  begin
    if Prop in Properties then
    if Option in ParaStyle.Options then
      Add(RVA_GetS(Msg, ControlPanel))
    else if not Root then
      Add(RVA_GetS(succ(Msg), ControlPanel));
  end;
  {................................}
  function GetAlignmentStr(Value: TRVAlignment): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_al_Left)+ord(Value)), ControlPanel);
  end;
  {................................}
  function GetLineSpacingStr(Value: TRVLineSpacingType): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_ls_Percent)+ord(Value)), ControlPanel);
  end;
  {................................}
  function GetBiDiModeStr(Value: TRVBiDiMode): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_bidi_Undefined)+ord(Value)), ControlPanel);
  end;
  {................................}
  function GetTabAlignStr(Value: TRVTabAlign): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_TabLeft)+ord(Value)), ControlPanel);
  end;
  {................................}
begin
  Delim := RVA_GetComma(ControlPanel)+' ';
  s := '';
  if (rvpiOutlineLevel in Properties) and (not Root or (ParaStyle.OutlineLevel<>0)) then
    if ParaStyle.OutlineLevel<=0 then
      Add(RVA_GetS(rvam_sd_BodyText, ControlPanel))
    else
      Add(RVAFormat(RVA_GetS(rvam_sd_HeadingLevel, ControlPanel), [ParaStyle.OutlineLevel]));
  if (rvpiAlignment in Properties) and (not Root or (ParaStyle.Alignment<>rvaLeft)) then
    Add(GetAlignmentStr(ParaStyle.Alignment));
  if (rvpiFirstIndent in Properties) and (not Root or (ParaStyle.FirstIndent<>0)) then
    Add(RVA_Gets(rvam_sd_FirstLineIndent, ControlPanel)+': '+DescribeValue(ParaStyle.FirstIndent, RVStyle,
      TRVAControlPanel(ControlPanel).UnitsDisplay, TRVAControlPanel(ControlPanel)));
  if (rvpiLeftIndent in Properties) and (not Root or (ParaStyle.LeftIndent<>0)) then
    Add(RVA_Gets(rvam_sd_LeftIndent, ControlPanel)+': '+
      DescribeValue(ParaStyle.LeftIndent, RVStyle,
        TRVAControlPanel(ControlPanel).UnitsDisplay, TRVAControlPanel(ControlPanel)));
  if (rvpiRightIndent in Properties) and (not Root or (ParaStyle.RightIndent<>0)) then
    Add(RVA_Gets(rvam_sd_RightIndent, ControlPanel)+': '+
      DescribeValue(ParaStyle.RightIndent, RVStyle,
        TRVAControlPanel(ControlPanel).UnitsDisplay, TRVAControlPanel(ControlPanel)));
  if (rvpiSpaceBefore in Properties) and (not Root or (ParaStyle.SpaceBefore<>0)) then
    Add(RVA_Gets(rvam_sd_SpaceBefore, ControlPanel)+': '+
      DescribeValue(ParaStyle.SpaceBefore,
        RVStyle, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)),
        TRVAControlPanel(ControlPanel)));
  if (rvpiSpaceAfter in Properties) and (not Root or (ParaStyle.SpaceAfter<>0)) then
    Add(RVA_Gets(rvam_sd_SpaceAfter, ControlPanel)+': '+
      DescribeValue(ParaStyle.SpaceAfter, RVStyle,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel)));
  if (rvpiLineSpacing in Properties) and (not Root or (ParaStyle.LineSpacing<>100) or (ParaStyle.LineSpacingType<>rvlsPercent)) then
    if ParaStyle.LineSpacingType=rvlsPercent then
      Add(RVAFormat(GetLineSpacingStr(rvlsPercent), [ParaStyle.LineSpacing]))
    else
      Add(RVAFormat(GetLineSpacingStr(ParaStyle.LineSpacingType),
        [DescribeValue(ParaStyle.LineSpacing, RVStyle,
         RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel))]));
  AddOption(rvpiNoWrap, rvpaoNoWrap, rvam_sd_NoWrap);
  AddOption(rvpiKeepLinesTogether, rvpaoKeepLinesTogether, rvam_sd_KeepLinesTogether);
  AddOption(rvpiKeepWithNext, rvpaoKeepWithNext, rvam_sd_KeepWithNext);
  AddOption(rvpiReadOnly, rvpaoReadOnly, rvam_sd_ReadOnly);
  if (rvpiBiDiMode in Properties) and (not Root or (ParaStyle.BiDiMode<>rvbdUnspecified)) then
    Add(RVA_GetS(rvam_sd_BiDi, ControlPanel)+': ' +GetBiDiModeStr(ParaStyle.BiDiMode));
  if (rvpiTabs in Properties) and (not Root or (ParaStyle.Tabs.Count<>0)) then begin
    Add(RVA_GetS(rvam_sd_Tabs, ControlPanel)+ ' (');
    if ParaStyle.Tabs.Count=0 then
      s := s+RVA_GetS(rvam_sd_NoTabs, ControlPanel)
    else
      for i := 0 to ParaStyle.Tabs.Count-1 do begin
        if i<>0 then
          s := s+Delim;
        s := s+DescribeValue(ParaStyle.Tabs[i].Position, RVStyle,
          TRVAControlPanel(ControlPanel).UnitsDisplay,
          TRVAControlPanel(ControlPanel))+
          ' '+RVA_GetS(rvam_sd_TabAlign, ControlPanel)+': '+
          GetTabAlignStr(ParaStyle.Tabs[i].Align);
        if ParaStyle.Tabs[i].Leader<>'' then
          s := s+' '+RVA_GetS(rvam_sd_Leader, ControlPanel)+': "'+ParaStyle.Tabs[i].Leader+'"';
      end;
    s := s+')';
  end;
  Result := s<>'';
  if s='' then
    if Root then
      s := RVA_GetS(rvam_sd_DefPara, ControlPanel)
    else
      s := RVA_GetS(rvam_sd_Inherited, ControlPanel)
  else
    if Root then
      s := RVA_GetS(rvam_sd_DefPara, ControlPanel)+' + '+s
    else
      s := '+ '+s;
  Descr := s;
end;
{------------------------------------------------------------------------------}
function DescribeStyleParaBorder(ParaStyle: TCustomRVParaInfo;
  Properties: TRVParaInfoProperties;
  RVStyle: TRVStyle; Root: Boolean; var Descr: TRVALocString;
  ControlPanel: TComponent): Boolean;
var s, Delim: TRVALocString;
    First: Boolean;
    noyes: array[Boolean] of TRVALocString;
  {................................}
  procedure Add(const s2: TRVALocString);
  begin
    if s<>'' then begin
      if s2<>'' then
        s := s+Delim+s2
      end
    else
      s := s2;
  end;
  {................................}
  procedure AddSub(const s2: TRVALocString);
  begin
    if not First then
      s := s+Delim
    else
      First := False;
    s := s+s2;
  end;
  {................................}
  function GetBorderStyleStr(Value: TRVBorderStyle): TRVALocString;
  begin
    Result := RVA_GetS(TRVAMessageID(ord(rvam_sd_brdr_None)+ord(Value)), ControlPanel);
  end;
  {................................}
  procedure AddRect(Offs: TRVRect; FirstProp: TRVParaInfoProperty);
  begin
    // properties are listed in the order: left top right bottom
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Left, ControlPanel)+':'+DescribeValue(Offs.Left, RVStyle,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel));
    inc(FirstProp);
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Top, ControlPanel)+':'+DescribeValue(Offs.Top, RVStyle,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel));
    inc(FirstProp);
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Right, ControlPanel)+':'+DescribeValue(Offs.Right, RVStyle,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel));
    inc(FirstProp);
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Bottom, ControlPanel)+':'+DescribeValue(Offs.Bottom, RVStyle,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel));
  end;
  {................................}
  procedure AddBooleanRect(R: TRVBooleanRect; FirstProp: TRVParaInfoProperty);
  begin
    // properties are listed in the order: left top right bottom
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Left, ControlPanel)+':'+noyes[R.Left];
    inc(FirstProp);
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Top, ControlPanel)+':'+noyes[R.Top];
    inc(FirstProp);
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Right, ControlPanel)+':'+noyes[R.Right];
    inc(FirstProp);
    if FirstProp in Properties then
      s := s+' '+RVA_GetS(rvam_sd_Bottom, ControlPanel)+':'+noyes[R.Bottom];
  end;
  {................................}
begin
  Delim := RVA_GetComma(ControlPanel)+' ';
  noyes[False] := RVA_GetS(rvam_sd_No, ControlPanel);
  noyes[True] := RVA_GetS(rvam_sd_Yes, ControlPanel);
  s := '';
  if (Properties*[rvpiBackground_Color..rvpiBackground_BO_Bottom])<>[] then begin
    Add(RVA_GetS(rvam_sd_Background, ControlPanel)+' (');
    First := True;
    if (rvpiBackground_Color in Properties) and (not Root or (ParaStyle.Background.Color<>clNone)) then
      AddSub(RVA_GetS(rvam_sd_Color, ControlPanel)+': '+
        DescribeColor(ParaStyle.Background.Color,
          RVA_GetS(rvam_sd_Transparent, ControlPanel), TRVAControlPanel(ControlPanel)));
    if ((Properties*[rvpiBackground_BO_Left..rvpiBackground_BO_Bottom])<>[]) and
      (not Root or not ParaStyle.Background.BorderOffsets.IsAllEqual(0)) then begin
      AddSub(RVA_GetS(rvam_sd_Padding, ControlPanel)+':');
      AddRect(ParaStyle.Background.BorderOffsets, rvpiBackground_BO_Left);
    end;
    s := s+')';
    if First then
      s := '';
  end;
  Descr := s;
  s := '';
  if (Properties*[rvpiBorder_Color..rvpiBorder_Vis_Bottom])<>[] then begin
    Add(RVA_GetS(rvam_sd_Border, ControlPanel)+' (');
    First := True;
    if (rvpiBorder_Color in Properties) and (not Root or (ParaStyle.Border.Color<>clWindowText)) then
      AddSub(RVA_GetS(rvam_sd_Color, ControlPanel)+': '+
        DescribeColor(ParaStyle.Border.Color, RVA_GetS(rvam_sd_Transparent, ControlPanel),  TRVAControlPanel(ControlPanel)));
    if (rvpiBorder_Style in Properties) and (not Root or (ParaStyle.Border.Style<>rvbNone)) then
      AddSub(RVA_GetS(rvam_sd_BorderStyle, ControlPanel)+': '+GetBorderStyleStr(ParaStyle.Border.Style));
    if (rvpiBorder_Width in Properties) and (not Root or (ParaStyle.Border.Width<>RVStyle.PixelsToUnits(1))) then
      AddSub(RVA_GetS(rvam_sd_BorderWidth, ControlPanel)+': '+
        DescribeValue(ParaStyle.Border.Width, RVStyle,
          RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel)));
    if (rvpiBorder_InternalWidth in Properties) and (not Root or (ParaStyle.Border.InternalWidth<>RVStyle.PixelsToUnits(1)))  then
      AddSub(RVA_GetS(rvam_sd_BorderIWidth, ControlPanel)+': '+
        DescribeValue(ParaStyle.Border.InternalWidth, RVStyle,
          RVA_GetBorderUnits(TRVAControlPanel(ControlPanel)), TRVAControlPanel(ControlPanel)));
    if ((Properties*[rvpiBorder_BO_Left..rvpiBorder_BO_Bottom])<>[]) and
       (not Root or not ParaStyle.Border.BorderOffsets.IsAllEqual(0)) then begin
      AddSub(RVA_GetS(rvam_sd_Spacing, ControlPanel)+':');
      AddRect(ParaStyle.Border.BorderOffsets, rvpiBorder_BO_Left);
    end;
    if ((Properties*[rvpiBorder_Vis_Left..rvpiBorder_Vis_Bottom])<>[]) and
       (not Root or not ParaStyle.Border.VisibleBorders.IsAllEqual(True))  then begin
      AddSub(RVA_GetS(rvam_sd_VisibleSides, ControlPanel)+':');
      AddBooleanRect(ParaStyle.Border.VisibleBorders, rvpiBorder_Vis_Left);
    end;
    s := s+')';
    if First then
      s := '';
  end;
  if Descr<>'' then
    if s='' then
      s := Descr
    else
      s := Descr+Delim+s;
  Result := s<>'';
  if s='' then
    if Root then
      s := RVA_GetS(rvam_sd_DefPara, ControlPanel)
    else
      s := RVA_GetS(rvam_sd_Inherited, ControlPanel)
  else
    if Root then
      s := RVA_GetS(rvam_sd_DefPara, ControlPanel)+' + '+s
    else
      s := '+ '+s;
  Descr := s;
end;
{------------------------------------------------------------------------------}
const
  STYLE_TEXT_NORMAL = 0;
  STYLE_TEXT_HEADING = 1;
  STYLE_TEXT_STRONG = 2;
  STYLE_TEXT_STYLENAME = 3;
  STYLE_PARA_NORMAL = 0;
  STYLE_PARA_HEADING = 1;
procedure BuildStylesRVStyle(rvs: TRVStyle; ControlPanel: TRVAControlPanel);
var i: Integer;
begin
  rvs.TextStyles.Clear;
  with rvs.TextStyles.Add do begin // normal text
    Color := clInfoText;
    Size := ControlPanel.DialogFontSize;
  end;
  with rvs.TextStyles.Add do begin // headings
    Style := [fsBold];
    Color := clBtnText;
    Size := ControlPanel.DialogFontSize+2;
  end;
  with rvs.TextStyles.Add do begin // strong
    Style := [fsBold];
    Color := clInfoText;
    Size := ControlPanel.DialogFontSize;
  end;
  with rvs.TextStyles.Add do begin // style name
    Style := [fsBold, fsItalic];
    Color := clInfoText;
    Size := ControlPanel.DialogFontSize;
  end;
  rvs.ParaStyles.Clear;
  rvs.ParaStyles.Add;  // normal para
  with rvs.ParaStyles.Add do begin // heading
    Background.Color := clBtnFace;
    Background.BorderOffsets.SetAll(2);
    SpaceBefore := 4;
    SpaceAfter := 4;
    LeftIndent := 4;
    RightIndent := 4;
  end;
  for i := 0 to rvs.TextStyles.Count-1 do
    with rvs.TextStyles[i] do begin
      FontName := ControlPanel.DialogFontName;
      Charset := RVA_GetCharset(ControlPanel);
      {$IFDEF USERVTNT}
      Unicode := True;
      {$ENDIF}
    end;
end;
{------------------------------------------------------------------------------}
procedure RVDisplayCurrentStyles(rve: TCustomRichViewEdit; rv: TCustomRichView;
  Images: TCustomImageList; FontImageIndex, ParaImageIndex: Integer;
  ControlPanel: TComponent);
var s: TRVALocString;
    TextStyle: TFontInfo;
    ParaStyle: TParaInfo;
    StyleTemplate, ParaStyleTemplate: TRVStyleTemplate;
    ParaNo: Integer;
begin
  rv.Color := clInfoBk;
  BuildStylesRVStyle(rv.Style, ControlPanel as TRVAControlPanel);
  rv.Clear;
  ParaNo := STYLE_PARA_HEADING;
  s := RVA_GetS(rvam_si_Para, ControlPanel);
  if (Images<>nil) and (ParaImageIndex>=0) and (ParaImageIndex<Images.Count) then begin
    rv.AddBulletEx('', ParaImageIndex, Images, ParaNo);
    rv.SetItemVAlign(rv.ItemCount-1, rvvaAbsMiddle);
    ParaNo := -1;
    s := ' '+s;
  end;
  RVAAddRV(rv, s, STYLE_TEXT_HEADING, ParaNo);
  ParaStyle := rve.Style.ParaStyles[rve.CurParaStyleNo];
  if rve.UseStyleTemplates then begin
    ParaStyleTemplate := rve.Style.StyleTemplates.FindItemById(ParaStyle.StyleTemplateId);
    RVAAddRV(rv, RVA_GetS(rvam_si_Style, ControlPanel)+': ', STYLE_TEXT_STRONG, STYLE_PARA_NORMAL);
    ParaStyleTemplate.UpdateModifiedParaStyleProperties(ParaStyle);
    if ParaStyleTemplate<>nil then
      RVAAddRV(rv, RVGetStyleName(ParaStyleTemplate.Name, False, ControlPanel), STYLE_TEXT_STYLENAME, -1)
    else
      RVAAddRV(rv, RVA_GetS(rvam_si_NoStyle, ControlPanel), STYLE_TEXT_NORMAL, -1);
    end
  else begin
    ParaStyleTemplate := nil;
    ParaStyleTemplate.UpdateModifiedParaStyleProperties(ParaStyle);
  end;
  DescribeStylePara(ParaStyle, ParaStyle.ModifiedProperties, rve.Style,
    ((ParaStyleTemplate=nil) or (ParaStyleTemplate.Kind=rvstkText)), s, ControlPanel);
  RVAAddRV(rv, RVA_GetS(rvam_si_Attributes, ControlPanel)+': ', STYLE_TEXT_STRONG, STYLE_PARA_NORMAL);
  RVAAddRV(rv, s, STYLE_TEXT_NORMAL, -1);
  if DescribeStyleParaBorder(ParaStyle, ParaStyle.ModifiedProperties, rve.Style,
    ((ParaStyleTemplate=nil) or (ParaStyleTemplate.Kind=rvstkText)), s, ControlPanel) then begin
    RVAAddRV(rv, RVA_GetS(rvam_si_BB, ControlPanel)+': ', STYLE_TEXT_STRONG, STYLE_PARA_NORMAL);
    RVAAddRV(rv, s, STYLE_TEXT_NORMAL, -1);
  end;

  ParaNo := STYLE_PARA_HEADING;
  s := RVA_GetS(rvam_si_Font, ControlPanel);
  if (Images<>nil) and (FontImageIndex>=0) and (FontImageIndex<Images.Count) then begin
    rv.AddBulletEx('', FontImageIndex, Images, ParaNo);
    rv.SetItemVAlign(rv.ItemCount-1, rvvaAbsMiddle);    
    ParaNo := -1;
    s := ' '+s;
  end;
  RVAAddRV(rv, s, STYLE_TEXT_HEADING, ParaNo);
  TextStyle := rve.Style.TextStyles[rve.CurTextStyleNo];
  if rve.UseStyleTemplates then begin
    StyleTemplate := rve.Style.StyleTemplates.FindItemById(TextStyle.StyleTemplateId);
    RVAAddRV(rv, RVA_GetS(rvam_si_Style, ControlPanel)+': ', STYLE_TEXT_STRONG, STYLE_PARA_NORMAL);
    StyleTemplate.UpdateModifiedTextStyleProperties(TextStyle, ParaStyleTemplate{, False});
    if StyleTemplate<>nil then
      RVAAddRV(rv, RVGetStyleName(StyleTemplate.Name, False, ControlPanel), STYLE_TEXT_STYLENAME, -1)
    else
      RVAAddRV(rv, RVA_GetS(rvam_si_NoStyle, ControlPanel), STYLE_TEXT_NORMAL, -1);
    end
  else begin
    StyleTemplate := nil;
    StyleTemplate.UpdateModifiedTextStyleProperties(TextStyle, ParaStyleTemplate{, False});
  end;
  DescribeStyleFont(TextStyle, TextStyle.ModifiedProperties, rve.Style,
    ((StyleTemplate=nil) or (StyleTemplate.Kind=rvstkPara)) and
    ((ParaStyleTemplate=nil) or (ParaStyleTemplate.Kind=rvstkText)), s, ControlPanel);
  RVAAddRV(rv, RVA_GetS(rvam_si_Attributes, ControlPanel)+': ', STYLE_TEXT_STRONG, STYLE_PARA_NORMAL);
  RVAAddRV(rv, s, STYLE_TEXT_NORMAL, -1);
  if DescribeStyleHyperlink(TextStyle, TextStyle.ModifiedProperties, rve.Style,
    ((StyleTemplate=nil) or (StyleTemplate.Kind=rvstkPara)) and
    ((ParaStyleTemplate=nil) or (ParaStyleTemplate.Kind=rvstkText)), s, ControlPanel) then begin
    RVAAddRV(rv, RVA_GetS(rvam_si_Hyperlink, ControlPanel)+': ', STYLE_TEXT_STRONG, STYLE_PARA_NORMAL);
    RVAAddRV(rv, s, STYLE_TEXT_NORMAL, -1);
  end;
  rv.Format;
end;
{------------------------------------------------------------------------------}
function RVGetStyleType(Name: TRVStyleTemplateName; var StyleType: TRVStandardStyle;
  var Level: Integer): Boolean;
var i: Integer;
    s: TRVAnsiString;
    j: TRVStandardStyle;
begin
  Result := False;
  Level := -1;
  if Name='' then
    exit;
  for i :=1 to Length(Name) do
    if (Ord(Name[i])>127) or (Name[i]='%') then
      exit;
  i := Length(Name);
  if (Name[i]>='0') and (Name[i]<='9') then
    Level := Ord(Name[i])-Ord('0');
  s := TRVAnsiString(Name);
  if (Level in [1..9]) and
    ({$IFDEF RICHVIEWDEF2009}AnsiStrings.{$ENDIF}Format(RVStandardStyleNames[rvssnHeadingN], [Level])=s) then begin
    Result := True;
    StyleType := rvssnHeadingN;
    exit;
  end;
  if Level>=0 then
    exit;
  for j := Low(TRVStandardStyle) to High(TRVStandardStyle) do
    if s=RVStandardStyleNames[j] then begin
      Result := True;
      StyleType := j;
      exit;
    end;
end;
{------------------------------------------------------------------------------}
function RVGetStandardStyleName(StyleType: TRVStandardStyle; Level: Integer;
  ControlPanel: TComponent): TRVALocString;
begin
  Result := RVAFormat(RVA_GetS(TRVAMessageID(ord(rvam_ss_Normal)+ord(StyleType)), ControlPanel),
    [Level]);
end;
{------------------------------------------------------------------------------}
function RVGetStyleName(const Name: TRVStyleTemplateName;
  MarkStandard: Boolean; ControlPanel: TComponent): TRVALocString;
var
  StyleType: TRVStandardStyle;
  Level: Integer;
begin
  if RVGetStyleType(Name, StyleType, Level) then begin
    Result := RVGetStandardStyleName(StyleType, Level, ControlPanel);
    if MarkStandard then
      Result := Result + ' '+RVA_GetS(rvam_si_StandardStyle, ControlPanel);
    end
  else
    Result := {$IFDEF USERVTNT}GetTNTStr{$ENDIF}(Name);
end;
{------------------------------------------------------------------------------}
procedure RVSetStandardStyleDefaults(StyleType: TRVStandardStyle; Level: Integer;
  StyleTemplate: TRVStyleTemplate; RVStyle: TRVStyle;
  StyleTemplates, StandardStyleTemplates: TRVStyleTemplateCollection;
  ControlPanel: TComponent);
const STEPTW = 360;
      SMALLSTEPTW = 60;
var Step, SmallStep: TRVStyleLength;
  StdStyleTemplate: TRVStyleTemplate;
  Units: TRVStyleUnits;
begin
  if StyleTemplates=nil then
    StyleTemplates := RVStyle.StyleTemplates;
  StyleTemplate.Name := String(Format(RVStandardStyleNames[StyleType], [Level]));
  StdStyleTemplate := nil;
  if StandardStyleTemplates<>nil then
    StdStyleTemplate := StandardStyleTemplates.FindItemByName(StyleTemplate.Name);
  if StdStyleTemplate<>nil then begin
    StyleTemplate.Assign(StdStyleTemplate);
    if (RVStyle<>nil) and (TRVAControlPanel(ControlPanel).UnitsProgram<>RVStyle.Units) then begin
      Units := RVStyle.Units;
      RVStyle.Units := TRVAControlPanel(ControlPanel).UnitsProgram;
      StyleTemplate.ParaStyle.ConvertToDifferentUnits(Units);
      StyleTemplate.TextStyle.ConvertToDifferentUnits(Units);
      RVStyle.Units := Units;
    end
    end
  else begin
    if RVStyle<>nil then begin
      Step := RVStyle.TwipsToUnits(STEPTW);
      SmallStep := RVStyle.TwipsToUnits(SMALLSTEPTW);
      end
    else begin
      Step := TRVStyle.TwipsToUnitsEx(STEPTW, rvstuPixels);
      SmallStep := TRVStyle.TwipsToUnitsEx(SMALLSTEPTW, rvstuPixels);
    end;
    StyleTemplate.TextStyle.Reset;
    StyleTemplate.ParaStyle.Reset;
    StyleTemplate.ValidTextProperties := [];
    StyleTemplate.ValidParaProperties := [];
    case StyleType of
      rvssnNormal:
        begin
          StyleTemplate.Kind := rvstkPara;
        end;
      rvssnNormalIndent:
        begin
          StyleTemplate.Kind := rvstkPara;
          StyleTemplate.ParaStyle.LeftIndent := Step;
          StyleTemplate.ValidParaProperties := [rvpiLeftIndent];
        end;
      rvssnNoSpacing:
        begin
          StyleTemplate.Kind := rvstkPara;
          StyleTemplate.ValidParaProperties := [rvpiLineSpacing, rvpiLineSpacingType];
        end;
      rvssnHeadingN:
        begin
          StyleTemplate.Kind := rvstkParaText;
          if StyleTemplates.NormalStyleTemplate<>nil then
            StyleTemplate.NextId := StyleTemplates.NormalStyleTemplate.Id;
          StyleTemplate.ParaStyle.OutlineLevel := Level;
          StyleTemplate.ParaStyle.Options := [rvpaoKeepWithNext];
          StyleTemplate.ParaStyle.SpaceBefore := SmallStep*4;
          StyleTemplate.ParaStyle.SpaceAfter := SmallStep;
          StyleTemplate.ValidParaProperties := [rvpiOutlineLevel, rvpiKeepWithNext,
            rvpiSpaceBefore, rvpiSpaceAfter];
          case Level of
            1:
              begin
                StyleTemplate.TextStyle.Size := 16;
                StyleTemplate.TextStyle.Style := [fsBold];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold];
              end;
            2:
              begin
                StyleTemplate.TextStyle.Size := 14;
                StyleTemplate.TextStyle.Style := [fsBold, fsItalic];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold, rvfiItalic];
              end;
            3:
              begin
                StyleTemplate.TextStyle.Size := 13;
                StyleTemplate.TextStyle.Style := [fsBold];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold];
              end;
            4:
              begin
                StyleTemplate.TextStyle.Size := 12;
                StyleTemplate.TextStyle.Style := [fsBold, fsItalic];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold, rvfiItalic];
              end;
            5:
              begin
                StyleTemplate.TextStyle.Size := 11;
                StyleTemplate.TextStyle.Style := [fsBold, fsItalic];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold, rvfiItalic];
              end;
            6:
              begin
                StyleTemplate.TextStyle.Size := 11;
                StyleTemplate.TextStyle.Style := [fsBold];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold];
              end;
            7:
              begin
                StyleTemplate.TextStyle.Size := 10;
                StyleTemplate.TextStyle.Style := [fsBold];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold];
              end;
            8:
              begin
                StyleTemplate.TextStyle.Size := 10;
                StyleTemplate.TextStyle.Style := [fsItalic];
                StyleTemplate.ValidTextProperties := [rvfiSize, rvfiItalic];
              end;
            9:
              begin
                // default
              end;
          end;
        end;
      rvssnListParagraph:
        begin
          StyleTemplate.Kind := rvstkPara;
          StyleTemplate.ValidParaProperties := [rvpiSpaceBefore, rvpiSpaceAfter];
        end;
      rvssnHyperlink:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Color := clBlue;
          StyleTemplate.TextStyle.Style := [fsUnderline];
          StyleTemplate.ValidTextProperties := [rvfiColor, rvfiUnderline, rvfiJump];
        end;
      rvssnTitle:
        begin
          StyleTemplate.Kind := rvstkParaText;
          if StyleTemplates.NormalStyleTemplate<>nil then
            StyleTemplate.NextId := StyleTemplates.NormalStyleTemplate.Id;
          StyleTemplate.ParaStyle.SpaceBefore := SmallStep*4;
          StyleTemplate.ParaStyle.SpaceAfter := SmallStep;
          StyleTemplate.ParaStyle.Alignment := rvaCenter;
          StyleTemplate.ValidParaProperties := [rvpiSpaceBefore, rvpiSpaceAfter,
            rvpiAlignment];
          StyleTemplate.TextStyle.Size := 20;
          StyleTemplate.TextStyle.Style := [fsBold];
          StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold];
        end;
      rvssnSubtitle:
        begin
          StyleTemplate.Kind := rvstkParaText;
          if StyleTemplates.NormalStyleTemplate<>nil then
            StyleTemplate.NextId := StyleTemplates.NormalStyleTemplate.Id;        
          StyleTemplate.ParaStyle.Alignment := rvaCenter;
          StyleTemplate.ValidParaProperties := [rvpiAlignment];
          StyleTemplate.TextStyle.Size := 12;
          StyleTemplate.TextStyle.Style := [fsItalic];
          StyleTemplate.ValidTextProperties := [rvfiSize, rvfiItalic];
        end;
      rvssnEmphasis:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Style := [fsItalic];
          StyleTemplate.ValidTextProperties := [rvfiItalic];
        end;
      rvssnSubtleEmphasis:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Color := clGray;
          StyleTemplate.TextStyle.Style := [fsItalic];
          StyleTemplate.ValidTextProperties := [rvfiItalic, rvfiColor];
        end;
      rvssnIntenseEmphasis:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Style := [fsItalic, fsBold];
          StyleTemplate.ValidTextProperties := [rvfiItalic, rvfiBold];
        end;
      rvssnStrong:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Style := [fsBold];
          StyleTemplate.ValidTextProperties := [rvfiBold];
        end;
      rvssnBlockText:
        begin
          StyleTemplate.Kind := rvstkParaText;
          StyleTemplate.TextStyle.Style := [fsItalic];
          StyleTemplate.ValidTextProperties := [rvfiItalic];
          StyleTemplate.ParaStyle.SpaceBefore := SmallStep*2;
          StyleTemplate.ParaStyle.SpaceAfter := SmallStep*2;
          StyleTemplate.ParaStyle.LeftIndent := Step;
          StyleTemplate.ParaStyle.RightIndent := Step;
          StyleTemplate.ParaStyle.Border.Width := 1;
          StyleTemplate.ParaStyle.Border.Style := rvbSingle;
          StyleTemplate.ParaStyle.Border.BorderOffsets.Left := SmallStep;
          StyleTemplate.ParaStyle.Border.BorderOffsets.Top := SmallStep;
          StyleTemplate.ParaStyle.Border.BorderOffsets.Right := SmallStep;
          StyleTemplate.ParaStyle.Border.BorderOffsets.Bottom := SmallStep;
          StyleTemplate.ValidParaProperties := [rvpiLeftIndent, rvpiRightIndent,
            rvpiSpaceBefore, rvpiSpaceAfter, rvpiBorder_Style, rvpiBorder_Width,
            rvpiBorder_BO_Left, rvpiBorder_BO_Right, rvpiBorder_BO_Top, rvpiBorder_BO_Bottom];
        end;
      rvssnQuote:
        begin
          StyleTemplate.Kind := rvstkParaText;
          if StyleTemplates.NormalStyleTemplate<>nil then
            StyleTemplate.NextId := StyleTemplates.NormalStyleTemplate.Id;
          StyleTemplate.TextStyle.Style := [fsItalic];
          StyleTemplate.ValidTextProperties := [rvfiItalic];
        end;
      rvssnIntenseQuote:
        begin
          StyleTemplate.Kind := rvstkParaText;
          if StyleTemplates.NormalStyleTemplate<>nil then
            StyleTemplate.NextId := StyleTemplates.NormalStyleTemplate.Id;
          StyleTemplate.TextStyle.Style := [fsItalic, fsBold];
          StyleTemplate.ValidTextProperties := [rvfiItalic, rvfiBold];
          StyleTemplate.ParaStyle.LeftIndent := Step;
          StyleTemplate.ParaStyle.RightIndent := Step;
          StyleTemplate.ValidParaProperties := [rvpiLeftIndent, rvpiRightIndent];
        end;
      rvssnSubtleReference:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Style := [fsUnderline];
          StyleTemplate.ValidTextProperties := [rvfiUnderline];
        end;
      rvssnIntenseReference:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Style := [fsBold, fsUnderline];
          StyleTemplate.ValidTextProperties := [rvfiBold, rvfiUnderline];
        end;
      rvssnHTMLVariable,  rvssnHTMLDefinition, rvssnHTMLCite:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.Style := [fsItalic];
          StyleTemplate.ValidTextProperties := [rvfiItalic];
        end;
      rvssnHTMLCode, rvssnHTMLKeyboard, rvssnHTMLSample, rvssnHTMLTypewriter:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.FontName := 'Courier New';
          StyleTemplate.ValidTextProperties := [rvfiFontName];
        end;
      rvssnHTMLAcronym, rvssnPageNumber:
        begin
          StyleTemplate.Kind := rvstkText;
        end;
      rvssnHTMLPreformatted:
        begin
          StyleTemplate.Kind := rvstkParaText;
          StyleTemplate.TextStyle.FontName := 'Courier New';
          StyleTemplate.ValidTextProperties := [rvfiFontName];
          StyleTemplate.ParaStyle.Options := [rvpaoNoWrap];
          StyleTemplate.ValidParaProperties := [rvpiNoWrap];
        end;
      rvssnCaption:
        begin
          StyleTemplate.Kind := rvstkPara;
          StyleTemplate.ValidTextProperties := [rvfiSize, rvfiBold];
          StyleTemplate.TextStyle.Size := 9;
          StyleTemplate.TextStyle.Style := [fsBold];
          if StyleTemplates.NormalStyleTemplate<>nil then
            StyleTemplate.NextId := StyleTemplates.NormalStyleTemplate.Id;          
        end;
      rvssnHeader, rvssnFooter, rvssnEndnoteText, rvssnFootnoteText, rvssnSidenoteText:
        begin
          StyleTemplate.Kind := rvstkParaText;
        end;
      rvssnEndnoteReference, rvssnFootnoteReference, rvssnSidenoteReference:
        begin
          StyleTemplate.Kind := rvstkText;
          StyleTemplate.TextStyle.SubSuperScriptType := rvsssSuperScript;
          StyleTemplate.ValidTextProperties := [rvfiSubSuperScriptType];
        end;
    end;
  end;                                                              
  if (StyleTemplates.NormalStyleTemplate<>nil) and
    (StyleTemplate.Kind<>rvstkText) and not (StyleType in [rvssnNormal, rvssnNoSpacing]) then
    StyleTemplate.ParentId := StyleTemplates.NormalStyleTemplate.Id;
end;
{------------------------------------------------------------------------------}
procedure RVFillStandardStyleInfo(StyleTemplates: TRVStyleTemplateCollection;
  var Info: TRVStandardStylesRec);
var i, Level: Integer;
    StyleType: TRVStandardStyle;
begin
  Info.Styles := [];
  Info.HeadingLevels := [];
  for i := 0 to StyleTemplates.Count-1 do
    if RVGetStyleType(StyleTemplates[i].Name, StyleType, Level) then begin
      Include(Info.Styles, StyleType);
      case StyleType of
        rvssnHeadingN:
          begin
            if Level in [1..9] then
              Include(Info.HeadingLevels, Byte(Level));
          end;
      end;
    end;
  if Info.HeadingLevels=[] then
    Exclude(Info.Styles, rvssnHeadingN);

end;
{------------------------------------------------------------------------------}
procedure RVStyleTemplatesToStrings(StyleTemplates: TRVStyleTemplateCollection;
  Strings: TRVALocStrings;
  AssignObjects: Boolean; Kinds: TRVStyleTemplateKinds;
  ExcludeThisStyle: TRVStyleTemplate; OnlyPotintialParents: Boolean;
  ControlPanel: TComponent);
var i: Integer;
    Name: TRVALocString;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    {$IFDEF RICHVIEWCBDEF3}
    Strings.Capacity := StyleTemplates.Count;
    {$ENDIF}
    for i := 0 to StyleTemplates.Count-1 do
      if (StyleTemplates[i].Kind in Kinds) and (StyleTemplates[i]<>ExcludeThisStyle) and
         ((ExcludeThisStyle=nil) or not ExcludeThisStyle.IsAncestorFor(StyleTemplates[i])) then begin
        Name := RVGetStyleName(StyleTemplates[i].Name, True, ControlPanel);
        if AssignObjects then
          Strings.AddObject(Name, StyleTemplates[i])
        else
          Strings.Add(Name);
    end;
  finally
    Strings.EndUpdate;
  end;
end;

function RVFindStyleTemplateInStrings(Strings: TRVALocStrings; StyleTemplate: TRVStyleTemplate): Integer;
var i: Integer;
begin
  for i := 0 to Strings.Count-1 do
    if Strings.Objects[i]=StyleTemplate then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;

function RVFindStyleTemplateNameInStrings(Strings: TRVALocStrings;
  const Name: TRVStyleTemplateName): Integer;
var i: Integer;
begin
  for i := 0 to Strings.Count-1 do
    if (Strings.Objects[i]<>nil) and
       (TRVStyleTemplate(Strings.Objects[i]).Name=Name) then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;

procedure RVDisplayStyleTemplate(StyleTemplate: TRVStyleTemplate;
  rvs: TRVStyle; rv: TCustomRichView; Images: TCustomImageList;
  FontImageIndex, HyperlinkImageIndex, ParaImageIndex, ParaBorderImageIndex: Integer;
  Editable: Boolean; ControlPanel: TComponent);
  {....................................................}
  procedure AddHeading(const Caption: TRVALocString; ImageIndex: Integer);
  begin
    if (Images<>nil) and (ImageIndex>=0) then begin
      rv.AddBulletEx('', ImageIndex, Images, 0);
      rv.SetItemVAlign(rv.ItemCount-1, rvvaAbsMiddle);
      RVAAddRV(rv, ' '+Caption+' ', 1, -1);
      end
    else
      RVAAddRV(rv, Caption+' ', 1, 0);
  end;
  {....................................................}
  procedure AddContent(const s: TRVALocString; NotEmpty: Boolean; Command: Integer);
  begin
    if Editable then begin
      RVAAddRV(rv, s+' [', 0, -1);
      RVAAddRV(rv, RVA_GetS(rvam_st_Edit, ControlPanel), 2, -1, RVAMakeTag(IntToStr(Command)));
      if NotEmpty then begin
        RVAAddRV(rv, ' | ', 0, -1);
        RVAAddRV(rv, RVA_GetS(rvam_st_Reset, ControlPanel), 2, -1, RVAMakeTag(IntToStr(Command+1)));
      end;
      RVAAddRV(rv, ']', 0, -1);
      end
    else
      RVAAddRV(rv, s, 0, -1);
  end;
  {....................................................}
  function GetParaPreviewString: TRVALocString;
  begin
    {$IFDEF USELOREMIPSUM}
    Result := LoremIpsum1;
    {$ELSE}
    Result := RVA_GetS(rvam_par_Preview, ControlPanel);
    {$ENDIF}
  end;

var s: TRVALocString;
    NotEmpty: Boolean;
begin
  if StyleTemplate.Kind in [rvstkParaText, rvstkText, rvstkPara] then begin
    AddHeading(RVA_GetS(rvam_si_Font, ControlPanel)+':', FontImageIndex);
    NotEmpty := DescribeStyleFont(StyleTemplate.TextStyle,
      StyleTemplate.ValidTextProperties, rvs,
      (StyleTemplate.Parent=nil) and (StyleTemplate.Kind<>rvstkText),
      s, ControlPanel);
    AddContent(s, NotEmpty, 1);

    AddHeading(RVA_GetS(rvam_si_Hyperlink, ControlPanel)+':', HyperlinkImageIndex);
    NotEmpty := DescribeStyleHyperlink(StyleTemplate.TextStyle,
      StyleTemplate.ValidTextProperties, rvs,
      (StyleTemplate.Parent=nil) and (StyleTemplate.Kind<>rvstkText),
      s, ControlPanel);
    AddContent(s, NotEmpty, 7);
  end;
  if StyleTemplate.Kind in [rvstkParaText, rvstkPara] then begin
    AddHeading(RVA_GetS(rvam_si_Para, ControlPanel)+':', ParaImageIndex);
    NotEmpty := DescribeStylePara(StyleTemplate.ParaStyle,
      StyleTemplate.ValidParaProperties, rvs,
      StyleTemplate.Parent=nil, s, ControlPanel);
    AddContent(s, NotEmpty, 3);

    AddHeading(RVA_GetS(rvam_si_BB, ControlPanel)+': ', ParaBorderImageIndex);
    NotEmpty := DescribeStyleParaBorder(StyleTemplate.ParaStyle,
      StyleTemplate.ValidParaProperties, rvs,
      StyleTemplate.Parent=nil, s, ControlPanel);
    AddContent(s, NotEmpty, 5);
  end;
  rv.AddBreak;
  rvs.TextStyles[4].Reset;
  rvs.TextStyles[4].ModifiedProperties := [];
  rvs.TextStyles[4].Assign(StyleTemplate);
  rvs.ParaStyles[2].Reset;
  rvs.ParaStyles[2].ModifiedProperties := [];
  if StyleTemplate.Kind = rvstkText then begin
    RVAAddRV(rv, '>>>   ', 3, 1);
    RVAAddRV(rv, RVA_GetS(rvam_font_SampleText, ControlPanel), 4, -1);
    RVAAddRV(rv, '   <<<', 3, -1);
    end
  else begin
    RVAAddRV(rv, GetParaPreviewString, 3, 0);
    RVAAddRV(rv, GetParaPreviewString, 4, 2);
    RVAAddRV(rv, GetParaPreviewString, 3, 0);
    rvs.ParaStyles[2].Assign(StyleTemplate);
  end;
end;
{------------------------------------------------------------------------------}
procedure RVDisplayStyleTemplateBasic(StyleTemplate: TRVStyleTemplate;
  rv: TCustomRichView; Images: TCustomImageList;
  ImageIndices: TRVStyleTemplateImageIndices; ControlPanel: TComponent);
begin
  rv.AddBulletEx('', ImageIndices[StyleTemplate.Kind], Images, 1);
  rv.SetItemVAlign(rv.ItemCount-1, rvvaAbsMiddle);
  RVAAddRV(rv, ' '+RVGetStyleName(StyleTemplate.Name, False, ControlPanel), 5, -1);
  if StyleTemplate.Parent<>nil then
    RVAAddRV(rv, RVA_GetS(rvam_st_Parent2, ControlPanel)+' '+
      RVGetStyleName(StyleTemplate.Parent.Name, False, ControlPanel), 0, 0);
  if (StyleTemplate.Next<>nil) and (StyleTemplate.Next<>StyleTemplate) then
    RVAAddRV(rv, RVA_GetS(rvam_st_Next2, ControlPanel)+' '+
      RVGetStyleName(StyleTemplate.Next.Name, False, ControlPanel), 0, 0);
end;

{============================ TRVStyleTemplateComboBox ========================}
function GetControlPanel(ControlPanel: TComponent): TRVAControlPanel;
begin
  if ControlPanel<>nil then
    Result := ControlPanel as TRVAControlPanel
  else
    Result := MainRVAControlPanel;
end;

constructor TRVStyleTemplateComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FShowClearFormat := True;
  FSmartHeadings := True;
  ItemHeight := 40;
  Style := csOwnerDrawVariable;
  DropDownCount := 10;
  FTextStyleImageIndex := -1;
  FParaStyleImageIndex := -1;
  FParaTextStyleImageIndex := -1;
  FClearFormatImageIndex := -1;
  FAllStylesImageIndex := -1;
end;
constructor TRVStyleTemplateListBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FShowClearFormat := True;
  FSmartHeadings := True;
  ItemHeight := 40;
  Style := lbOwnerDrawFixed;
  FTextStyleImageIndex := -1;
  FParaStyleImageIndex := -1;
  FParaTextStyleImageIndex := -1;
  FClearFormatImageIndex := -1;
  FAllStylesImageIndex := -1;
end;
{------------------------------------------------------------------------------}
destructor TRVStyleTemplateComboBox.Destroy;
begin
  Editor := nil;
  inherited;
end;

destructor TRVStyleTemplateListBox.Destroy;
begin
  Editor := nil;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then begin
    if AComponent = FEditor then
      Editor := nil
    else if AComponent = FImages then
      Images := nil
    else if AComponent = FControlPanel then
      ControlPanel := nil;
  end;
end;
procedure TRVStyleTemplateListBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then begin
    if AComponent = Editor then
      Editor := nil
    else if AComponent = FImages then
      Images := nil
    else if AComponent = FControlPanel then
      ControlPanel := nil;      
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.SetEditor(Value: TCustomRVControl);
begin
  if (Value<>nil) and (Value.SRVGetActiveEditor(False)=nil) then
    raise ERichViewError.Create(errInvalidRVControl);
  if FEditor<>nil then begin
    {$IFDEF RICHVIEWDEF5}
    FEditor.RemoveFreeNotification(Self);
    {$ENDIF}
    FEditor.UnregisterStyleTemplatesChangeHandler(Self);
    FEditor.UnregisterSRVChangeActiveEditorHandler(Self);
  end;
  FEditor := Value;
  if FEditor<>nil then begin
    FEditor.FreeNotification(Self);
    FEditor.RegisterStyleTemplatesChangeHandler(Self, DoFill);
    FEditor.RegisterSRVChangeActiveEditorHandler(Self, DoSRVChangeActiveEditor);
    RichViewEdit := FEditor.SRVGetActiveEditor(False) as TCustomRichViewEdit
    end
  else
    RichViewEdit := nil;
end;
procedure TRVStyleTemplateListBox.SetEditor(Value: TCustomRVControl);
begin
  if (Value<>nil) and (Value.SRVGetActiveEditor(False)=nil) then
    raise ERichViewError.Create(errInvalidRVControl);
  if FEditor<>nil then begin
    {$IFDEF RICHVIEWDEF5}
    FEditor.RemoveFreeNotification(Self);
    {$ENDIF}
    FEditor.UnregisterStyleTemplatesChangeHandler(Self);
    FEditor.UnregisterSRVChangeActiveEditorHandler(Self);
  end;
  FEditor := Value;
  if FEditor<>nil then begin
    FEditor.FreeNotification(Self);
    FEditor.RegisterStyleTemplatesChangeHandler(Self, DoFill);
    FEditor.RegisterSRVChangeActiveEditorHandler(Self, DoSRVChangeActiveEditor);
    RichViewEdit := FEditor.SRVGetActiveEditor(False) as TCustomRichViewEdit
    end
  else
    RichViewEdit := nil;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.SetRichViewEdit(Value: TCustomRichViewEdit);
begin
  if FRichViewEdit<>nil then begin
    FRichViewEdit.UnregisterCurTextStyleChangeHandler(Self);
    FRichViewEdit.UnregisterCurParaStyleChangeHandler(Self);
  end;
  FRichViewEdit := Value;
  if FRichViewEdit<>nil then begin
    FRichViewEdit.RegisterCurTextStyleChangeHandler(Self, DoUpdateSelection);
    FRichViewEdit.RegisterCurParaStyleChangeHandler(Self, DoUpdateSelection);
  end;
  if not (csDesigning in ComponentState) then begin
    Fill;
    UpdateSelection;
  end;
end;
procedure TRVStyleTemplateListBox.SetRichViewEdit(Value: TCustomRichViewEdit);
begin
  if FRichViewEdit<>nil then begin
    FRichViewEdit.UnregisterCurTextStyleChangeHandler(Self);
    FRichViewEdit.UnregisterCurParaStyleChangeHandler(Self);
  end;
  FRichViewEdit := Value;
  if FRichViewEdit<>nil then begin
    FRichViewEdit.FreeNotification(Self);
    FRichViewEdit.RegisterCurTextStyleChangeHandler(Self, DoUpdateSelection);
    FRichViewEdit.RegisterCurParaStyleChangeHandler(Self, DoUpdateSelection);
  end;
  if not (csDesigning in ComponentState) then begin
    Fill;
    UpdateSelection;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.SetImages(Value: TCustomImageList);
begin
  {$IFDEF RICHVIEWDEF5}
  if FImages<>nil then
    FImages.RemoveFreeNotification(Self);
  {$ENDIF}
  FImages := Value;
  if FImages<>nil then
    FImages.FreeNotification(Self);
end;
procedure TRVStyleTemplateListBox.SetImages(Value: TCustomImageList);
begin
  {$IFDEF RICHVIEWDEF5}
  if FImages<>nil then
    FImages.RemoveFreeNotification(Self);
  {$ENDIF}
  FImages := Value;
  if FImages<>nil then
    FImages.FreeNotification(Self);
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.SetControlPanel(Value: TComponent);
begin
  if (Value<>nil) and not (Value is TRVAControlPanel) then
    raise ERichViewError.Create(errInvalidControlPanel);
  if Value <> FControlPanel then
  begin
    {$IFDEF RICHVIEWDEF5}
    if FControlPanel <> nil then
      FControlPanel.RemoveFreeNotification(Self);
    {$ENDIF}
    FControlPanel := Value;
    if FControlPanel <> nil then
      FControlPanel.FreeNotification(Self);
  end;
end;

procedure TRVStyleTemplateListBox.SetControlPanel(Value: TComponent);
begin
  if (Value<>nil) and not (Value is TRVAControlPanel) then
    raise ERichViewError.Create(errInvalidControlPanel);
  if Value <> FControlPanel then
  begin
    {$IFDEF RICHVIEWDEF5}
    if FControlPanel <> nil then
      FControlPanel.RemoveFreeNotification(Self);
    {$ENDIF}
    FControlPanel := Value;
    if FControlPanel <> nil then
      FControlPanel.FreeNotification(Self);
  end;
end;
{------------------------------------------------------------------------------}
function MarkQuickStyles(StyleTemplates: TRVStyleTemplateCollection;
  QuickStyles: TRVIntegerList; SmartHeadings: Boolean; MaxHeadingLevel: Integer): Integer;
var i: Integer;
    StyleType: TRVStandardStyle;
    Level: Integer;
begin
  Result := 0;
  if SmartHeadings then begin
    for i := 0 to StyleTemplates.Count-1 do
      if QuickStyles[i]=0 then
        if RVGetStyleType(StyleTemplates[i].Name, StyleType, Level) and
          (StyleType = rvssnHeadingN) then begin
          if Level <= MaxHeadingLevel then begin
            QuickStyles[i] := 1;
            inc(Result);
          end;
          end
        else if StyleTemplates[i].QuickAccess then begin
          QuickStyles[i] := 1;
          inc(Result);
        end;
    end
  else
    for i := 0 to StyleTemplates.Count-1 do
      if StyleTemplates[i].QuickAccess and (QuickStyles[i]=0) then begin
        QuickStyles[i] := 1;
        inc(Result);
      end;
end;
{------------------------------------------------------------------------------}
function MarkStylesUsedInText(StyleTemplates: TRVStyleTemplateCollection;
  TextStyles: TFontInfos; QuickStyles: TRVIntegerList): Integer;
var i, idx: Integer;
    id, newid: TRVStyleTemplateId;
begin
  Result := 0;
  id := -1;
  for i := 0 to TextStyles.Count-1 do begin
    newid := TextStyles[i].StyleTemplateId;
    if (newid>0) and (newid<>id) then begin
      id := newid;
      idx := StyleTemplates.FindById(id);
      if (idx>=0) and (QuickStyles[idx]=0) then begin
        QuickStyles[idx] := 1;
        inc(Result);
      end;
    end;
  end;
end;
{------------------------------------------------------------------------------}
function MarkStylesUsedInPara(StyleTemplates: TRVStyleTemplateCollection;
  ParaStyles: TParaInfos; QuickStyles: TRVIntegerList; SmartHeadings: Boolean): Integer;
var i, idx: Integer;
    id, newid: TRVStyleTemplateId;
    StyleType: TRVStandardStyle;
    Level, MaxLevel: Integer;
begin
  Result := 0;
  id := -1;
  MaxLevel := 0;
  for i := 0 to ParaStyles.Count-1 do begin
    newid := ParaStyles[i].StyleTemplateId;
    if (newid>0) and (newid<>id) then begin
      id := newid;
      idx := StyleTemplates.FindById(id);
      if idx>=0 then begin
        if SmartHeadings and
          RVGetStyleType(StyleTemplates[idx].Name, StyleType, Level) and
          (StyleType=rvssnHeadingN) and (Level>MaxLevel) then
          MaxLevel := Level;
        if QuickStyles[idx]=0 then begin
          QuickStyles[idx] := 1;
          inc(Result);
        end;
      end;
    end;
  end;
  if SmartHeadings and (MaxLevel>=3) then
    inc(Result, MarkQuickStyles(StyleTemplates, QuickStyles, SmartHeadings, MaxLevel+1));
end;
{------------------------------------------------------------------------------}
procedure ListFill(Items: TRVALocStrings; RichViewEdit: TCustomRichViewEdit;
  ShowAllStyles, ShowClearFormat, SmartHeadings: Boolean;
  ControlPanel: TRVAControlPanel);
var QuickStyles: TRVIntegerList;
    i, DisplayCount: Integer;
    StyleTemplates: TRVStyleTemplateCollection;
    SL: TRVALocStringList;
begin
  Items.Clear;
  if (RichViewEdit=nil) or (RichViewEdit.Style=nil) then
    exit;
  StyleTemplates := RichViewEdit.Style.StyleTemplates;
  DisplayCount := StyleTemplates.Count;
  if DisplayCount=0 then
    exit;
  QuickStyles := nil;
  try
    if not ShowAllStyles then begin
      QuickStyles := TRVIntegerList.CreateEx(StyleTemplates.Count, 0);
      DisplayCount := MarkQuickStyles(StyleTemplates, QuickStyles, SmartHeadings, 3);
      if DisplayCount<StyleTemplates.Count then
        inc(DisplayCount,
          MarkStylesUsedInText(StyleTemplates, RichViewEdit.Style.TextStyles, QuickStyles));
      if DisplayCount<StyleTemplates.Count then
        inc(DisplayCount,
          MarkStylesUsedInPara(StyleTemplates, RichViewEdit.Style.ParaStyles, QuickStyles, SmartHeadings));
    end;
    Items.BeginUpdate;
    try
      if ShowClearFormat then
        Items.Add(RVA_GetS(rvam_scb_ClearFormat, ControlPanel));
      SL := TRVALocStringList.Create;
      try
        for i := 0 to StyleTemplates.Count-1 do
          if (QuickStyles=nil) or (QuickStyles[i]<>0) then
            SL.AddObject(RVGetStyleName(StyleTemplates[i].Name, False, ControlPanel), StyleTemplates[i]);
        SL.Sort;
        for i := 0 to SL.Count-1 do
          Items.AddObject(SL[i], SL.Objects[i]);
      finally
        SL.Free;
      end;
       if DisplayCount<StyleTemplates.Count then
         Items.AddObject(
           RVA_GetS(rvam_scb_MoreStyles, ControlPanel) + ' (+'+IntToStr(StyleTemplates.Count-DisplayCount)+')',
           TObject(1));
    finally
      Items.EndUpdate;
    end;
  finally
    QuickStyles.Free;
  end;
end;

procedure TRVStyleTemplateComboBox.Fill;
begin
  if csDestroying in ComponentState then
    exit;
  ListFill(Items, RichViewEdit, ShowAllStyles, ShowClearFormat, SmartHeadings,
    GetControlPanel(ControlPanel));
end;

procedure TRVStyleTemplateListBox.Fill;
begin
  if csDestroying in ComponentState then
    exit;
  ListFill(Items, RichViewEdit, ShowAllStyles, ShowClearFormat, SmartHeadings,
    GetControlPanel(ControlPanel));
end;

procedure TRVStyleTemplateComboBox.DoFill(Sender: TObject);
begin
  Fill;
end;

procedure TRVStyleTemplateListBox.DoFill(Sender: TObject);
begin
  Fill;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.DoSRVChangeActiveEditor(Sender: TObject);
var NewRichViewEdit: TCustomRichViewEdit;
begin
  if Editor=nil then
    exit;
  NewRichViewEdit := Editor.SRVGetActiveEditor(False) as TCustomRichViewEdit;
  if NewRichViewEdit<>RichViewEdit then begin
    RichViewEdit := NewRichViewEdit;
    if RichViewEdit<>nil then begin
      Fill;
      UpdateSelection;
    end;
  end;
end;
procedure TRVStyleTemplateListBox.DoSRVChangeActiveEditor(Sender: TObject);
var NewRichViewEdit: TCustomRichViewEdit;
begin
  if Editor=nil then
    exit;
  NewRichViewEdit := Editor.SRVGetActiveEditor(False) as TCustomRichViewEdit;
  if NewRichViewEdit<>RichViewEdit then begin
    RichViewEdit := NewRichViewEdit;
    if RichViewEdit<>nil then begin
      Fill;
      UpdateSelection;
    end;
  end;
end;
{------------------------------------------------------------------------------}
function ListUpdateSelection(Items: TRVALocStrings; RichViewEdit: TCustomRichViewEdit;
  var Updating: Boolean; Depth: Integer; var ItemIndex: Integer;
  ShowAllStyles, ShowClearFormat, SmartHeadings: Boolean;
  ControlPanel: TRVAControlPanel): Boolean;
var id: TRVStyleTemplateId;
    i, Index: Integer;
begin
  Result := False;
  if (RichViewEdit=nil) or (RichViewEdit.Style=nil) or (Depth>1) then
    exit;
  Id := RichViewEdit.Style.TextStyles[RichViewEdit.CurTextStyleNo].StyleTemplateId;
  if Id<0 then
    Id := RichViewEdit.Style.ParaStyles[RichViewEdit.CurParaStyleNo].StyleTemplateId;
  Updating := True;
  try
    Index := -1;
    if Id>=0 then begin
      for i := 0 to Items.Count-1 do
        if (Integer(Items.Objects[i])<>0) and
          (Integer(Items.Objects[i])<>1) and
          (TRVStyleTemplate(Items.Objects[i]).Id = Id) then begin
          Index := i;
          break
        end;
      if Index<0 then begin
        ListFill(Items, RichViewEdit, ShowAllStyles, ShowClearFormat, SmartHeadings,
          ControlPanel);
        Result := ListUpdateSelection(Items, RichViewEdit, Updating, Depth+1,
          ItemIndex, ShowAllStyles, ShowClearFormat, SmartHeadings, ControlPanel);
        exit;
      end;
    end;
    ItemIndex := Index;
    Result := True;
  finally
    Updating := False;
  end;
end;

procedure TRVStyleTemplateComboBox.UpdateSelection;
var AItemIndex: Integer;
begin
  FUpdating := True;
  try
    if ListUpdateSelection(Items, RichViewEdit, FUpdating, 0, AItemIndex,
      ShowAllStyles, ShowClearFormat, SmartHeadings, GetControlPanel(ControlPanel)) then
      ItemIndex := AItemIndex;
  finally
    FUpdating := False;
  end;
end;

procedure TRVStyleTemplateListBox.UpdateSelection;
var AItemIndex: Integer;
begin
  FUpdating := True;
  try
    if ListUpdateSelection(Items, RichViewEdit, FUpdating, 0, AItemIndex,
      ShowAllStyles, ShowClearFormat, SmartHeadings, GetControlPanel(ControlPanel)) then
      ItemIndex := AItemIndex;
  finally
    FUpdating := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.DoUpdateSelection(Sender: TObject);
begin
  UpdateSelection;
end;

procedure TRVStyleTemplateListBox.DoUpdateSelection(Sender: TObject);
begin
  UpdateSelection;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.Click;
var Index: Integer;
    StyleType: TRVStandardStyle;
    Level: Integer;
begin
  if FUpdating or (RichViewEdit=nil) then
    exit;
  if ItemIndex>=0 then begin
    if Integer(Items.Objects[ItemIndex])=0 then
      RichViewEdit.ApplyStyleTemplate(-1)
    else if Integer(Items.Objects[ItemIndex])=1 then begin
      if Assigned (OnClickAllStyles) then
        OnClickAllStyles(Self)
      else begin
        ShowAllStyles := True;
        DroppedDown := False;
        Fill;
        UpdateSelection;
        ShowAllStyles := False;
        FTemporaryShowAllState := 1;
        PostMessage(Handle, CB_SHOWDROPDOWN, 1, 0);
      end;
      exit;
      end
    else begin
      Index := RichViewEdit.Style.StyleTemplates.FindById(
        TRVStyleTemplate(Items.Objects[ItemIndex]).Id);
      if Index>=0 then begin
        RichViewEdit.ApplyStyleTemplate(Index);
        if SmartHeadings and
          RVGetStyleType(RichViewEdit.Style.StyleTemplates[Index].Name, StyleType, Level) and
          (StyleType=rvssnHeadingN) and (Level>2) then
          Fill;
      end;
    end;
    UpdateSelection;
  end;
  inherited;
  RichViewEdit.SetFocusSilent;
end;

procedure TRVStyleTemplateListBox.Click;
var Index: Integer;
begin
  if FUpdating or (RichViewEdit=nil) then
    exit;
  if ItemIndex>=0 then begin
    if Integer(Items.Objects[ItemIndex])=0 then
      RichViewEdit.ApplyStyleTemplate(-1)
    else if Integer(Items.Objects[ItemIndex])=1 then begin
      if Assigned (OnClickAllStyles) then
        OnClickAllStyles(Self)
      else begin
        ShowAllStyles := True;
        Fill;
        UpdateSelection;
        ShowAllStyles := False;
      end;
      UpdateSelection;
      exit;
      end
    else begin
      Index := RichViewEdit.Style.StyleTemplates.FindById(
        TRVStyleTemplate(Items.Objects[ItemIndex]).Id);
      if Index>=0 then
        RichViewEdit.ApplyStyleTemplate(Index);
    end;
    UpdateSelection;
  end;
  inherited;
  RichViewEdit.SetFocusSilent;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.CNCommand(var Message: TWMCommand);
begin
  case Message.NotifyCode of
    CBN_DROPDOWN:
      begin
        case FTemporaryShowAllState of
          1: FTemporaryShowAllState := 2;
          2:
            begin
              FUpdating := True;
              try
                Fill;
                UpdateSelection;
              finally
                FUpdating := False;
                FTemporaryShowAllState := 0;
              end;
            end;
        end;
      end;
  end;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateComboBox.MeasureItem(Index: Integer; var Height: Integer);
var DC: HDC;
    Canvas: TCanvas;
begin
  if Index<0 then begin
    DC := CreateCompatibleDC(0);
    Canvas := TCanvas.Create;
    Canvas.Handle := DC;
    try
      Canvas.Font := Font;
      Height := Canvas.TextHeight('Wg')+2;
      if (Images<>nil) and (Images.Height>Height) then
        Height := Images.Height;
    finally
      Canvas.Handle := 0;
      Canvas.Free;
      DeleteDC(DC);
    end;
    end
  else
    Height := ItemHeight;
end;
{------------------------------------------------------------------------------}
procedure ListDrawItem(Canvas: TCanvas; Items: TRVALocStrings; Index: Integer;
  RichViewEdit: TCustomRichViewEdit; Rect: TRect; State: TOwnerDrawState;
  Images: TCustomImageList;
  ClearFormatImageIndex, AllStylesImageIndex,
  TextStyleImageIndex, ParaStyleImageIndex, ParaTextStyleImageIndex: Integer;
  DrawPreview: Boolean; ControlPanel: TRVAControlPanel);
var TextStyle: TFontInfo;
    ParaStyle: TParaInfo;
    ImageIndex, dx, dy, x, CanvasState: Integer;
    R, R2, R3: TRect;
    RTL: Boolean;
    DTOptions: Integer;
    {$IFDEF RICHVIEWDEFXE2}
    Details: TThemedElementDetails;
    TextColor: TColor;
    {$ENDIF}
    BackColor: TColor;
    TransparentBackground: Boolean;
   {...............................................}
   function GetImageIndex: Integer;
   begin
     if Images=nil then begin
        Result := -1;
        exit;
     end;
     if Integer(Items.Objects[Index])=0 then
       Result := ClearFormatImageIndex
     else if Integer(Items.Objects[Index])=1 then
       Result := AllStylesImageIndex
     else
       case (Items.Objects[Index] as TRVStyleTemplate).Kind of
         rvstkText:
           Result := TextStyleImageIndex;
         rvstkPara:
           Result := ParaStyleImageIndex;
         else
           Result := ParaTextStyleImageIndex;
       end;
   end;
   {...............................................}
   procedure AdjustRect(var R: TRect; const RMax: TRect);
   begin
     if R.Left<RMax.Left then
       R.Left := RMax.Left;
     if R.Top<RMax.Top then
       R.Top := RMax.Top;
     if R.Right>RMax.Right then
       R.Right := RMax.Right;
     if R.Bottom>RMax.Bottom then
       R.Bottom := RMax.Bottom;
   end;
   {...............................................}
begin
  RTL := RVA_GetCharset(ControlPanel)=ARABIC_CHARSET;
  R := Rect;
  Canvas.Brush.Style := bsSolid;
  {$IFDEF RICHVIEWDEFXE2}
  if StyleServices.Enabled then
    BackColor := StyleServices.GetStyleColor(scListBox)
  else
   {$ENDIF}
    BackColor := clWindow;
  Canvas.Brush.Color := BackColor;
  TransparentBackground := True;
  if odSelected in State then begin
    if odFocused in State then begin
      Canvas.Font.Color := clHighlightText;
      Canvas.Brush.Color := clHighlight;
      end
    else begin
      Canvas.Font.Color := clWindowText;
      Canvas.Brush.Color := clBtnFace;
    end;
  end;
  Canvas.FillRect(Rect);
  if Index<0 then
    exit;  
  ImageIndex := GetImageIndex;
  dx := 2;
  if ImageIndex>=0 then begin
    dy := (Rect.Bottom-Rect.Top-Images.Height) div 2;
    if dy>dx then
      dx := dy;
    if dx>5 then
      dx := 5;
    if not RTL then
      x := R.Left+dx
    else
      x := R.Right-dx-Images.Width;
    Images.Draw(Canvas, x, R.Top+dy, ImageIndex);
    dx := dx*2+Images.Width;
  end;
  if not RTL then
    inc(R.Left, dx)
  else
    dec(R.Right, dx);
  CanvasState := RichViewEdit.Style.GraphicInterface.SaveCanvasState(Canvas);
  with Rect do
    RichViewEdit.Style.GraphicInterface.IntersectClipRect(Canvas, Left, Top, Right, Bottom);
  try
    if DrawPreview then
    if (Integer(Items.Objects[Index])<>0) and (Integer(Items.Objects[Index])<>1) then begin
       TextStyle := RichViewEdit.Style.TextStyles.Add;
       ParaStyle := RichViewEdit.Style.ParaStyles.Add;
       try
         R2 := R;
         (Items.Objects[Index] as TRVStyleTemplate).ApplyToParaStyle(ParaStyle);
         dx := RichViewEdit.Style.GetAsPixels(ParaStyle.LeftIndent);
         if dx>24 then
           dx := 24;
         inc(R2.Left, dx);
         dx := RichViewEdit.Style.GetAsPixels(ParaStyle.RightIndent);
         if dx>24 then
           dx := 24;
         dec(R2.Right, dx);
         inc(R2.Top, RichViewEdit.Style.GetAsPixels(ParaStyle.SpaceBefore));
         dec(R2.Bottom, RichViewEdit.Style.GetAsPixels(ParaStyle.SpaceAfter));
         R.Left := R2.Left;
         if not (odSelected in State) and (ParaStyle.Background.Color<>clNone) then begin
           R3 := R2;
           ParaStyle.Background.PrepareDraw(R2, RichViewEdit.Style);
           AdjustRect(R3, R);
           ParaStyle.Background.Draw(R3, Canvas, False, rvcmColor,
             RichViewEdit.Style.GraphicInterface);
         end;
         if (odSelected in State) and (ParaStyle.Border.Color<>clNone) then
           if odFocused in State then
             ParaStyle.Border.Color := clHighlightText
           else
             ParaStyle.Border.Color := clWindowText;
         if (ParaStyle.Border.Color<>clNone) and (ParaStyle.Border.Width<>0) and
           (ParaStyle.Border.Style<>rvbNone) then begin
           R3 := R2;
           ParaStyle.Border.Draw(R3, Canvas, RichViewEdit.Style,
             RichViewEdit.Style.GraphicInterface);
         end;
         (Items.Objects[Index] as TRVStyleTemplate).ApplyToTextStyle(TextStyle, nil);
         TextStyle.Apply(Canvas, rvbdUnspecified, False, True, nil, True, False,
           RichViewEdit.Style);
         TextStyle.ApplyColor(Canvas, RichViewEdit.Style, [], False, rvcmColor);
         if (TextStyle.BackColor<>clNone) or (ParaStyle.Background.Color<>clNone) then
           TransparentBackground := False;
       finally
         TextStyle.Free;
         ParaStyle.Free;
       end;
    end
    else begin
      if not (odSelected in State) then begin
        if Integer(Items.Objects[Index])=0 then
          dy := Rect.Bottom-1
        else
          dy := Rect.Top;
        Canvas.Pen.Color := clBtnShadow;
        Canvas.Pen.Width := 1;
        Canvas.Pen.Style := psSolid;
        Canvas.MoveTo(Rect.Left, dy);
        Canvas.LineTo(Rect.Right, dy);
      end;
      DrawPreview := False;
    end;
    if odSelected in State then begin
      if odFocused in State then
        Canvas.Font.Color := clHighlightText
      else
        Canvas.Font.Color := clWindowText;
      Canvas.Brush.Style := bsClear;
    end;
    if Canvas.Font.Charset=DEFAULT_CHARSET then
      Canvas.Font.Charset := RVA_GetCharset(ControlPanel);
    if Canvas.TextHeight('Wg')>Rect.Bottom-Rect.Top then
      Canvas.Font.Height := Rect.Bottom-Rect.Top-4;
    DTOptions := DT_VCENTER or DT_SINGLELINE;
    if RTL then
      DTOptions := DTOptions or DT_RIGHT;

    {$IFDEF RICHVIEWDEFXE2}
    if not (odSelected in State) and StyleServices.Enabled then begin
      Details := StyleServices.GetElementDetails(teEditTextNormal);
      TextColor := Canvas.Font.Color;
      if not DrawPreview or
        (TransparentBackground and (Abs(RV_GetLuminance(TextColor)-RV_GetLuminance(BackColor))<RVALUM_THRESHOLD)) then
        if not StyleServices.GetElementColor(Details, ecTextColor, TextColor) or
          (TextColor = clNone) then
          TextColor := Canvas.Font.Color;
      if TransparentBackground then
        StyleServices.DrawText(Canvas.Handle, Details, PChar(Items[Index]), R,
          TTextFormatFlags(DTOptions), TextColor)
      else
        DrawText(Canvas.Handle, PChar(Items[Index]), -1, R, DTOptions);
      end
    else
    {$ENDIF}
    begin
      if not (odSelected in State) and DrawPreview and TransparentBackground and
        (Abs(RV_GetLuminance(Canvas.Font.Color)-RV_GetLuminance(BackColor))<RVALUM_THRESHOLD) then
        Canvas.Font.Color := clWindowText;
      {$IFDEF USERVTNT}
      DrawTextW(Canvas.Handle, PWideChar(Items[Index]), -1, R, DTOptions);
      {$ELSE}
      DrawText(Canvas.Handle, PChar(Items[Index]), -1, R, DTOptions);
      {$ENDIF}
    end;
  finally
    RichViewEdit.Style.GraphicInterface.RestoreCanvasState(Canvas, CanvasState);
  end;
end;

procedure TRVStyleTemplateComboBox.DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  Canvas.Font := Font;
  ListDrawItem(Canvas, Items, Index, RichViewEdit, Rect, State,
    Images, ClearFormatImageIndex, AllStylesImageIndex,
    TextStyleImageIndex, ParaStyleImageIndex, ParaTextStyleImageIndex,
    ItemHeight<=Rect.Bottom-Rect.Top, GetControlPanel(ControlPanel));
  if odFocused in State then
    DrawFocusRect(Canvas.Handle, Rect);
end;

procedure TRVStyleTemplateListBox.DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  Canvas.Font := Font;
  ListDrawItem(Canvas, Items, Index, RichViewEdit, Rect, State,
    Images, ClearFormatImageIndex, AllStylesImageIndex,
    TextStyleImageIndex, ParaStyleImageIndex, ParaTextStyleImageIndex,
    ItemHeight<=Rect.Bottom-Rect.Top, GetControlPanel(ControlPanel));
  if odFocused in State then
    DrawFocusRect(Canvas.Handle, Rect);
end;
{------------------------------------------------------------------------------}
{
procedure ListLocalize(Items: TRVALocStrings);
var i: Integer;
begin
  Items.BeginUpdate;
  for i := 0 to Items.Count-1 do
    if Integer(Items.Objects[i])=0 then
      Items[i] := RVA_GetS(rvam_scb_ClearFormat)
    else if Integer(Items.Objects[i])=1 then
      Items[i] := RVA_GetS(rvam_scb_MoreStyles)
    else
      Items[i] := RVGetStyleName((Items.Objects[i] as TRVStyleTemplate).Name, False);
  Items.EndUpdate;
end;
}
procedure TRVStyleTemplateComboBox.Localize;
begin
  Fill;
  UpdateSelection;
end;


procedure TRVStyleTemplateListBox.Localize;
begin
  Fill;
  UpdateSelection;
end;

{$ENDIF}

end.
