unit HypHoverPropRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  BaseRVFrm, RVColorCombo, RVALocalize;

type
  TfrmRVHypHoverProp = class(TfrmRVBase)
    gbHover: TGroupBox;
    lblHoverText: TLabel;
    lblHoverTextBack: TLabel;
    lblHoverUnderlineColor: TLabel;
    cmbHoverText: TRVColorCombo;
    cmbHoverTextBack: TRVColorCombo;
    cbAsNormal: TCheckBox;
    cmbHoverUnderlineColor: TRVColorCombo;
    gbEffects: TGroupBox;
    btnOk: TButton;
    btnCancel: TButton;
    cbHoverUnderline: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure cbAsNormalClick(Sender: TObject);
  private
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _cbAsNormal, _cbHoverUnderline: TControl;
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;


implementation
uses RichViewActions;

{$R *.dfm}

{ TfrmRVHypHoverProp }
procedure TfrmRVHypHoverProp.FormCreate(Sender: TObject);
begin
  _cbAsNormal := cbAsNormal;
  _cbHoverUnderline := cbHoverUnderline;
  inherited;
  cmbHoverText.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
  cmbHoverTextBack.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
  cmbHoverUnderlineColor.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
end;



function TfrmRVHypHoverProp.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := gbEffects.Left*2+gbEffects.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gbEffects.Top-gbEffects.Height);
  Result := True;
end;

procedure TfrmRVHypHoverProp.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_hp_Title, ControlPanel);
  gbHover.Caption := RVA_GetSHAsIs(rvam_hp_GBActive, ControlPanel);
  gbEffects.Caption := RVA_GetSHAsIs(rvam_hp_GBAttributes, ControlPanel);
  lblHoverText.Caption := RVA_GetSAsIs(rvam_hp_HoverText, ControlPanel);
  lblHoverTextBack.Caption := RVA_GetSAsIs(rvam_hp_HoverTextBack, ControlPanel);
  lblHoverUnderlineColor.Caption := RVA_GetSAsIs(rvam_hp_HoverUnderlineColor, ControlPanel);
  cmbHoverUnderlineColor.DefaultCaption := RVA_GetS(rvam_ip_TrAutoColor, ControlPanel);
  cmbHoverText.DefaultCaption := RVA_GetS(rvam_ip_TrAutoColor, ControlPanel);
  cbAsNormal.Caption := RVA_GetSAsIs(rvam_hp_AsNormal, ControlPanel);
  cbHoverUnderline.Caption := RVA_GetSAsIs(rvam_hp_UnderlineActiveLinks, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVHypHoverProp.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  inherited;
  if OldControl=_cbAsNormal then
   _cbAsNormal := NewControl
  else if OldControl= _cbHoverUnderline then
   _cbHoverUnderline := NewControl
end;
{$ENDIF}



procedure TfrmRVHypHoverProp.cbAsNormalClick(Sender: TObject);
begin
  if GetCheckBoxChecked(_cbAsNormal) then begin
    cmbHoverText.Indeterminate := True;
    cmbHoverTextBack.Indeterminate := True;
    cmbHoverUnderlineColor.Indeterminate := True;
  end;
  cmbHoverText.Enabled := not GetCheckBoxChecked(_cbAsNormal);
  cmbHoverTextBack.Enabled := not GetCheckBoxChecked(_cbAsNormal);
  cmbHoverUnderlineColor.Enabled := not GetCheckBoxChecked(_cbAsNormal);
end;

end.
