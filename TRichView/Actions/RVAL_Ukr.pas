
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Ukrainian translation                           }
{                                                       }
{       Translated by Gennady Ostapenko                 }
{       GennadyGO@ukr.net                               }
{                                                       }
{       Updated by: Michael Mostovoy                    }
{                   Artem Bogorodichenko                }
{                                                       }
{*******************************************************}
{ Updated: 2014-03-20                                   }
{ Created: 2003-11-12                                   }
{*******************************************************}

unit RVAL_Ukr;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  '�����', '��', '��', '���', '�����', '������',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', '��', '��', '��', '���.', '��',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  '&����', '&������', '�&�����', '&�����', '&�����', '&�������', '&�������', '�&����', '&������',
  // exit
  '��&���',
  // top-level menus: View, Tools,
  '��&����', '�&����',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '�����', '�����', '���&�����', '����������� � &������', '&����� ������',
  // menus: Table cell rotation
  '�&�������� ������',
  // menus: Text flow, Footnotes/endnotes
  '&��������', '&�������',
  // ribbon tabs: tab1, tab2, view, table
  '&�������', '������� �� &���', '&���', '&�������',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '����� �����', '�����', '�����', '������', '�����������',
  // ribbon groups: insert, background, page setup,
  '�������', '���', '��������� �������',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  '���''���', '�������', '������ ��������� ���������', '�������� ��� ���������', '�������', '���������',
  // ribbon groups on table tab: insert, delete, operations, borders
  '�������', '���������', '��������', '���',
  // ribbon groups: styles
  '����',
  // ribbon screen tip footer,
  '��������� F1 ��� ������',
  // ribbon app menu: recent files, save-as menu title, print menu title
  '������� ���������', '�������� ���� ���������', '���������� �������� �� ����',
  // ribbon label: units combo
  '�������:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&��������', '��������|��������� ������ ���������',
  // TrvActionOpen
  '&³������...', '³������|³������� ����������� ���������',
  // TrvActionSave
  '&��������', '��������|���������� ���������',
  // TrvActionSaveAs
  '�������� &��...', '�������� ��...|���������� ��������� �� ����� ������, � ������ ���� ��� ������',
  // TrvActionExport
  '&�������...', '�������|������� ��������� � ���� � ����� ������, � ���� ���� ��� � ����� ������',
  // TrvActionPrintPreview
  '����&������ ��������', '���������� ��������|�������� ��������� � ���� ������, � ����� �� ���� ������������',
  // TrvActionPrint
  '&����...', '����|������� ��������� ����� � ���� ���������',
  // TrvActionQuickPrint
  '&����', '&������� ����', '����|���� ���������',
   // TrvActionPageSetup
  '�&�������� �������...', '��������� �������|������������ ����, ������ � ������� ������ ������, �����������',
  // TrvActionCut
  '&�������', '�������|��������� ��������� ��������� � ����������� ���� � ����� �����',
  // TrvActionCopy
  '&��������', '��������|��������� ��������� ��������� � ����� �����',
  // TrvActionPaste
  '���&�����', '��������|������� ��������� � ������ ����� � ������� ������� ���������',
  // TrvActionPasteAsText
  '�������� �� &�����', '�������� �� �����|������� ������ � ������ �����',  
  // TrvActionPasteSpecial
  '��&�������� �������...', '���������� �������|������� ����� ������ ����� � �������� � ��������� ������',
  // TrvActionSelectAll
  '�&������ ���', '������� ���|�������� ������ ���������',
  // TrvActionUndo
  '³�&�����', '³������|³���� �������� ��������',
  // TrvActionRedo
  '��������&�', '���������|�������� ������� ������� ��',
  // TrvActionFind
  '&�����...', '�����|����� ��������� ������ � ��������',
  // TrvActionFindNext
  '������ &���', '������ ���|����������� ���������� ������ ������',
  // TrvActionReplace
  '&�������...', '�������|����� �� ����� ��������� ������ � ��������',
  // TrvActionInsertFile
  '&����...', '�������� ����|������� ����� � ������� ������� ���������',
  // TrvActionInsertPicture
  '���&����...', '�������� �������|������� ������� � ������� ������� ���������',
  // TRVActionInsertHLine
  '&������������� ����', '�������� ������������� ����|������� ������������� ���� � ������� ������� ���������',
  // TRVActionInsertHyperlink
  '��������&�...', '������ ���������|��������� ������ ��� ����������� ��������� ���������',
  // TRVActionRemoveHyperlinks
  '&�������� ���������', '�������� ���������|��������� ��� �������� � ��������� ������',  
  // TrvActionInsertSymbol
  '&������...', '�������� ������|������� ������� ��� ������������ �����',
  // TrvActionInsertNumber
  '��&���...', '������ �����|��������� ������',
  // TrvActionInsertCaption
  '&������ �����...', '������ �����|��������� ����� �������� ��''����',
  // TrvActionInsertTextBox
  '&�������� ����', '������ �������� ����|��������� ���������� ���� � ������� ������� ���������',
  // TrvActionInsertSidenote
  '������� � �&����', '������ ������� � �����|��������� �������, ��� ���������������� � ���������� ���, � ������� ������� ���������',
  // TrvActionInsertPageNumber
  '����� ��&�����', '������ ����� �������|��������� ������ ������� � ������� ������� ���������',
  // TrvActionParaList
  '&������...', '������|��������� ��� ���� ������� ������� ��� ��������� �������� �������',
  // TrvActionParaBullets
  '&�������', '�������|��������� ��� ��������� ���������� �������� �������',
  // TrvActionParaNumbering
  '&���������', '���������|��������� ��� ��������� ��������� �������� �������',
  // TrvActionColor
  '&���� ����...', '���� ����|���� ������� ���� ���������',
  // TrvActionFillColor
  '&�������...', '�������|���� ������� ���� ������, ������, ������� ��� ������',
  // TrvActionInsertPageBreak
  '&������ �������', '������ �������|��������� ������� ������� � ������� ������� ���������.',
  // TrvActionRemovePageBreak
  '&�������� ������ �������', '�������� ������ �������|��������� ������� �������',
  // TrvActionClearLeft
  '�� ������� &���� ��', '�� ������� ���� ��|��������� ������ �� ��''������, �� �������� �� ����� ����',
  // TrvActionClearRight
  '�� ������� &������ ��', '�� ������� ������ ��|��������� ������ �� ��''������, �� �������� �� ������� ����',
  // TrvActionClearBoth
  '�� ������� &������ ����', '�� ������� ������ ����|��������� ������ �� ��''������, �� �������� �� �����',
  // TrvActionClearNone
  '�&������ ������ ����', '������� ������ ����|����� ������ ������� ��''����, �� �������� �� �����',
  // TrvActionVAlign
  '&��������� ��''����...', '��������� ��''����|��������� ��������� ��������� ��''���� � �����',
  // TrvActionItemProperties
  '���������� &��''����...', '���������� ��''����|���������� ������������ ��������� �������, ���� ��� �������',
  // TrvActionBackground
  '&���...', '���|���� �������� ������� �� �������',
  // TrvActionParagraph
  '&�����...', '�����|���� �������� �������� �������',
  // TrvActionIndentInc
  '�&������� ������', '�������� ������|��������� ����� ������� �������� �������',
  // TrvActionIndentDec
  '�&������� ������', '�������� ������|��������� ����� ������� �������� �������',
  // TrvActionWordWrap
  '�&������ �� ������', '������� �� ������|����� ��� �������� ������������� �������� ����� ��������� �������� �������',
  // TrvActionAlignLeft
  '�� &����� ����', '�� ����� ����|����������� ��������� ������ �� ����� ����',
  // TrvActionAlignRight
  '�� &������� ����', '�� ������� ����|����������� ��������� ������ �� ������� ����',
  // TrvActionAlignCenter
  '�� &������', '�� ������|����������� ��������� ������ �� ������',
  // TrvActionAlignJustify
  '�� &������', '�� ������|����������� ��������� ������ �� ����� � ������� �����',
  // TrvActionParaColor
  '���� ������� �&�����...', '���� ������� ������|���� ������� ������� ������',
  // TrvActionLineSpacing100
  '&��������� ��������', '��������� ��������|������������ ���������� ��������� �� ������� ������',
  // TrvActionLineSpacing150
  '���&������� ��������', '���������� ��������|������������ ����������� ��������� ����� ������� ������',
  // TrvActionLineSpacing200
  '��&������ ��������', '�������� ��������|������������ ��������� ��������� �� ������� ������',
  // TrvActionParaBorder
  '&����� � ������� ������...', '����� � ������� ������|������������ �����, ������� � ����� �������� �������',
  // TrvActionInsertTable
  '�������� &�������...', '&�������', '�������� �������|������� ������� � ������� ������� ���������',
  // TrvActionTableInsertRowsAbove
  '������ ����� &����', '������ ����� ����|������� ������ ����� ��� ��������� ��������',
  // TrvActionTableInsertRowsBelow
  '������ ����� &�����', '������ ����� �����|������� ������ ����� �� ��������� ��������',
  // TrvActionTableInsertColLeft
  '������ ������� �&���', '������ ������� ����|������� ���� ������� ���� �� �������� ������',
  // TrvActionTableInsertColRight
  '������ ������� �&�����', '������ ������� ������|������� ���� ������� ������ �� �������� ������',
  // TrvActionTableDeleteRows
  '�������� &�����', '�������� �����|��������� ����� � ��������� ��������',
  // TrvActionTableDeleteCols
  '�������� �&������', '�������� �������|��������� ������� � ��������� ��������',
  // TrvActionTableDeleteTable
  '���&����� �������', '�������� �������|��������� �������',
  // TrvActionTableMergeCells
  '�&�''������ ������', '��''������ ������|��''������� �������� ������ � ����',
  // TrvActionTableSplitCells
  '��&����� ������...', '������� ������|���� �������� ������ �� ������ ������� ����� � �������',
  // TrvActionTableSelectTable
  '���&����� �������', '������� �������|�������� ���� �������',
  // TrvActionTableSelectRows
  '������� ���&��', '������� �����|�������� �����, �� ������ ������� ������',
  // TrvActionTableSelectCols
  '������� ����&���', '������� �������|�������� �������, �� ������ ������� ������',
  // TrvActionTableSelectCell
  '������� ���&����', '������� ������|�������� ������',
  // TrvActionTableCellVAlignTop
  '�������� �� &��������� ����', '�������� �� ��������� ����|����������� ����� ������ �� ��������� ����',
  // TrvActionTableCellVAlignMiddle
  '&���������� �� ��������', '���������� �� ��������|����������� ����� ������ �� ������',
  // TrvActionTableCellVAlignBottom
  '�������� �� &�������� ����', '�������� �� �������� ����|����������� ����� ������ �� �������� ����',
  // TrvActionTableCellVAlignDefault
  '����������� �� ������&�������', '����������� �� �������������|���������� ����������� ����������� �� �������������',
  // TrvActionTableCellRotationNone
  '&��� ���������', '��� ���������|�� �������� ���� ������',
  // TrvActionTableCellRotation90
  '�������� ������ �� &90�', '�������� ������ �� 90�|�������� ���� ������ �� 90�',
  // TrvActionTableCellRotation180
  '�������� ������ �� &180�', '�������� ������ �� 180�|�������� ���� ������ �� 180�',
  // TrvActionTableCellRotation270
  '�������� ������ �� &270�', '�������� ������ �� 270�|�������� ���� ������ �� 270�',
  // TrvActionTableProperties
  '���������� &�������...', '���������� �������|������� ������������ �������',
  // TrvActionTableGrid
  '�&����', 'ѳ���|����� ��� ���������� ���� �� ���� ��������� ����� � ��� ��������',
  // TRVActionTableSplit
  '&�������� �������', '�������� �������|������� ������� �� �� �������, ��������� � �������� �����',
  // TRVActionTableToText
  '����������� � &�����...', '����������� � �����|���������� ������� � �����',
  // TRVActionTableSort
  '&����������...', '����������|����� ����� �������',
  // TrvActionTableCellLeftBorder
  '&˳�� �������', '˳�� �������|����� ��� ���������� ��� ������� �������� ������',
  // TrvActionTableCellRightBorder
  '&����� �������', '����� �������|����� ��� ���������� ����� ������� �������� ������',
  // TrvActionTableCellTopBorder
  '&������ �������', '������ �������|����� ��� ���������� ������� ������� �������� ������',
  // TrvActionTableCellBottomBorder
  '&����� �������', '����� �������|����� ��� ���������� ������ ������� �������� ������',
  // TrvActionTableCellAllBorders
  '�&� �������', '�� �������|����� ��� ���������� ��� ������� �������� ������',
  // TrvActionTableCellNoBorders
  '&���� �������', '���� �������|���������� ��� ������� �������� ������',
  // TrvActionFonts & TrvActionFontEx
  '&�����...', '�����|���� ��������� ������ � ������� �� ��������� � ��������� �����',
  // TrvActionFontBold
  '����&������', '����������|���������� ��������� ������ ���������� �������',
  // TrvActionFontItalic
  '&������', '������|���������� ��������� ������ ��������',
  // TrvActionFontUnderline
  'ϳ��&��������', 'ϳ����������|ϳ���������� ��������� ������',
  // TrvActionFontStrikeout
  '&�����������', '�����������|����������� ��������� ������',
  // TrvActionFontGrow
  '�&������� �����', '�������� �����|��������� ������ ������ ��������� ������ �� 10%',
  // TrvActionFontShrink
  '�&������� �����', '�������� �����|��������� ������ ������ ��������� ������ �� 10%',
  // TrvActionFontGrowOnePoint
  '��&������ ����� �� 1 ��', '�������� ����� �� 1 ��|��������� ������ ������ ��������� ������ �� 1 �����',
  // TrvActionFontShrinkOnePoint
  '���&����� ����� �� 1 ��', '�������� ����� �� 1 ��|��������� ������ ������ ��������� ������ �� 1 �����',
  // TrvActionFontAllCaps
  '�� &�����', '�� �����|����� ��� ���� ��������� ������ �� �������',
  // TrvActionFontOverline
  '���&��� �����', '������ �����|������� ������������� ����� ��� �������� �������',
  // TrvActionFontColor
  '&���� ������...', '���� ������|���� ������� ��������� ������',
  // TrvActionFontBackColor
  '���� �&������ ������...', '���� ������� ������|���� ������� ���� ��������� ������',
  // TrvActionAddictSpell3
  '&��������', '��������|�������� ����������',
  // TrvActionAddictThesaurus3
  '&��������', '��������|����� ��������',
  // TrvActionParaLTR
  '���� �������', '���� �������|������������ ������� ������ ���� ������� ��� �������� �������',
  // TrvActionParaRTL
  '������ �����', '������ �����|������������ ������� ������ ������ ����� ��� �������� �������',
  // TrvActionLTR
  '����� ���� �������', '����� ���� �������|������������ ������� ��������� ������ ���� �������',
  // TrvActionRTL
  '����� ������ �����', '����� ������ �����|������������ ������� ��������� ������ ������ �����',
  // TrvActionCharCase
  '&������', '������|���� ������� ���� ��������� ������',
  // TrvActionShowSpecialCharacters
  '�����, �� &�� ����������', '�����, �� &�� ����������|�������� ��� ��������� ������� �����, ��� �� ������ ���� ������, ����� ��� ���������.',
  // TrvActionSubscript
  '������ ������', '������ ������|���������� �������� ����� �� ����� �������',
  // TrvActionSuperscript
  '������� ������', '������� ������|���������� �������� ����� �� ������ �������',
  // TrvActionInsertFootnote
  '&�������', '�������|��������� ������� � ��������',
  // TrvActionInsertEndnote
  '&ʳ����� �������', 'ʳ����� �������|��������� ������ ������� � ��������',
  // TrvActionEditNote
  '&���������� �������', '���������� �������|����������� ������ ������� �������',
  // TrvActionHide
  '&���������', '���������|��������� ��� �������� �������� �������� ���������',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&����...', '����|³������� ���� ����������� �����',
  // TrvActionAddStyleTemplate
  '&������ �����...', '������ �����|��������� ������ ����� �� ������ �������� ������',
  // TrvActionClearFormat,
  '&�������� ������', '�������� ������|�������� �� ������������ �������� ���������',
  // TrvActionClearTextFormat,
  '�������� ������ &������', '�������� ������ ������|�������� ������������ ������ �������� ���������',
  // TrvActionStyleInspector
  '&��������� �����', '��������� �����|�������� ��� ��������� ���� ����������� �����',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  '��', '³����', '�������', '��������', '&�������...', '&��������...', '&��������', '������', '��������',
  // Others  -------------------------------------------------------------------
  '%',
  // left, top, right, bottom sides
  '˳�� �������', '������ �������', '����� �������', '����� �������',
  // save changes? confirm title
  '�������� ���� � %s?', 'ϳ�����������',
  // warning: losing formatting
  '%s ���� ������ ������������, ��� ���� �������� ��� ������������ � �������� ������ �����.'#13+
  '�������� �������� � ����� ������?',
  // RVF format name
  '������ RichView',
  // Error messages ------------------------------------------------------------
  '�������',
  '������� ������� �����.'#13#13'������ �������:'#13'- ���� �� ������, ���� �� ����������� ���������;'#13+
  '- ���� �����������;'#13'- ���� ������� ����� ���������, �� ��������� ������ �� �����.',
  '������� ������� ����� � �����������.'#13#13'������ �������:'#13'- ���� ������ ���������� � ������, ���� �� ����������� ���������;'#13+
  '- ���� �� ������ ����������;'#13+
  '- ���� �����������;'#13'- ���� ������� ����� ���������, �� ��������� ������ �� �����.',
  '������� ���������� �����.'#13#13'������ �������:'#13'- �� ��������� ���� �� �����;'#13+
  '- ���� ��������� �� ������;'#13'- ���� �� ��������� � ��������;'#13+
  '- ���� ������� ����� ���������, ��� ��������� ������ �� �����;'#13+
  '- ���� ���������� �����������.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  '����� ������� RichView (*.rvf)|*.rvf',  '����� RTF (*.rtf)|*.rtf' , '����� XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  '������� ����� (*.txt)|*.txt', '������� ����� - ������ (*.txt)|*.txt', '������� ����� - ���� (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  '����� HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - ��������� (*.htm;*.html)|*.htm;*.html',
  // DocX
  '��������� Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  '����� ���������', '����� ''%s'' �� ��������.',
  // 1 string replaced; Several strings replaced
  '�������� ����: 1.', '�������� ����: %d',
  // continue search from the beginning/end?
  '��������� ���� ���������, ���������� ����� � �������?',
  '��������� ������� ���������, ���������� ����� � ����?',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  '��� �������', '����',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  '������', '����������', '���������', '�����-�������',
  '�����-�����', '�����-�����', '������','ѳ���-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  '�����-��������', '���������', '�����-������', '�������',
  '�����-�������', '�����', '�����', 'ѳ���-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  '��������', '�����-���������', '����''����', '�����������',
  '�����-��������', '�����-���������', 'Գ��������', 'ѳ���-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  '˳�����', '���������', '������', '�������-�������',
  '���������', '���������', '��������', 'ѳ���-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  '�������', '�����-����������', '�����-������', '����-�������',
  '�����-��������', '����-���������', '��������', '�����',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '��� �&������', '&����', '&���� �������...', '&�����������',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  '���', '&����:', '���������', '����������', '������� ������.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&����', '&����������', '&Գ������� ������', '&������', '�� �&�����',
  // Padding button
  '&����...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  '�������', '&����������� ��:', '&����� ����...', '�&���...',
  '���� �����, ������� ����',
  // [apply to:] text, paragraph, table, cell
  '������', '������' , '�������', '������',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  '�����', '�����', '��������',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&�����:', '&�����', '&���������', '����&������', '&������',
  // Script, Color, Back color labels, Default charset
  '���� &�������:', '&����:', '&���:', '(�� �������������)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  '�����������', 'ϳ����������', '&����� ������', '&�����������', '&�� �����',
  // Sample, Sample text
  '�������', '������� ������',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  '��������', '&��������:', '&����������', '&����������',
  // Offset group-bix, Offset label, Down, Up,
  '�������', '&�������:', '�&���', '&�����',
  // Scaling group-box, Scaling
  '�������������', '&�������:',
  // Sub/super script group box, Normal, Sub, Super
  '������� �� ������ ������', '&���������', '������ ������', '������� ������',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  '����', '�&�����:', '�&���:', '�&����:', '�&�����:',
  // Equal values check-box
  '&������� ��������',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  '��������� ���������', '���������', '&�����:', '&URL:', '<<�������� ��������>>',
  // cannot open URL
  '������� �������� "%s"',
  // hyperlink properties button, hyperlink style
  '&��������...', '&�����:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  '�������� ���������', '�������� �������', '������� �������',
  // Text [color], Background [color]
  '&�����:', '&���',
  // Underline [color]
  'ϳ�&���������:',
  // Text [color], Background [color] (different hotkeys)
  '�&����:', '�&��',
  // Underline [color], Attributes group-box,
  'ϳ�&���������:', '��������',
  // Underline type: always, never, active (below the mouse)
  '&ϳ����������:', '������', '������', '��� ���������',
  // Same as normal check-box
  '�&�� �, �� ��������',
  // underline active links check-box
  '&ϳ����������� ��� ���������',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  '������', '&�����:', '&���� �������:', '������',
  // Unicode block
  '&����:',  
  // Character Code, Character Unicode Code, No Character
  '��� �������: %d', '��� �������: ������ %d', '(������ �� �������)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows
  '������� �������', '����� �������', '&ʳ������ �������:', '�&������� �����:',
  // Table layout group-box, Autosize, Fit, Manual size,
  '������ �������', '&����', '�� ������ &����', '���&����:',
  // Remember check-box
  '�� ������������� ��� ����� &�������',
  // VAlign Form ---------------------------------------------------------------
  '���������',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  '����������', '�������', '����� � ���������', '˳���', '�������', '�����', '������',
  // Image Appearance, Image Misc, Number tabs
  '������', 'г���', '�����',
  // Box position, Box size, Box appearance tabs
  '���������', '�����', '������',
  // Preview label, Transparency group-box, checkbox, label
  '��������:', '���������', '&��������', '�������� &����:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&�������...', '&��������...', '����',
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  '����������� �����������', '��&������:',
  '��� �� ������ ���� ������', '����� �� ������ ���� ������',
  '�� ��������� ���� �����', '�� �������� ���� �����', '�� ������ �����',
  // align to left side, align to right side
  '�� ����� ����', '�� ������� ����',
  // Shift By label, Stretch group box, Width, Height, Default size
  '��&����� ��:', '�������', '�� &������:', '�� &�����:', '���������� �����: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '��&������ ������������ �����', '�&������',
  // Inside the border, border, outside the border groupboxes
  '��������� ���', '����', '������� ���',
  // Fill color, Padding (i.e. margins inside border) labels
  '&���� ����:', '&����:',
  // Border width, Border color labels
  '&������ �����:', '���� &�����:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '³����� �� &����������:', '³����� �� &��������:',
  // Miscellaneous groupbox, Tooltip label
  'г���', '&ϳ������:',
  // web group-box, alt text
  '���', '&����� �������:',
  // Horz line group-box, color, width, style
  '������������� ����', '&����:', '&�������:', '&�����:',
  // Table group-box; Table Width, Color, CellSpacing
  '�������', '&������:', '&���� �������:', '&��������� �� ��������...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&������������� ���� ������:', '&����������� ���� ������:', '����� � �������',
  // Visible table border sides button, auto width (in combobox), auto width label
  '����� �&������ �����...', '����', '����',
  // Keep row content together checkbox
  '�&� ��������� ���� �����',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  '���������', '&����', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  '�����:', '����� ���� &�����...',
  // More, Border, CellBorders buttons
  '&����� �������...', '����� ��&����...',
  // Table border dialog title
  '����� �������', '����� ������ �� �������������',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  '����', '���������� ������� &����� �� �������� �������', 'ʳ������ ����� � &���������:',
  '����� ��������� ������ ����������� �� ������ ������� � ��������',
  // top, center, bottom, default
  '�&�����', '�� &������', '�&����', '������&������',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  '��������� ������', '��������� &������:', '&������ �� ����:', '���� &�������:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  '�����', '����� �������:', '&������ ����:', '&������ ����', '���&�:',
  // Background image
  '�����&��...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  '�� ����������', '�� ��������', '��������� � �����',
  // Position types: align, absolute, relative
  '��������', '��������� ���������', '³������ ���������',
  // [Align] left side, center, right side
  '��� ������� �� ����� ����',
  '�������� �� ��������',
  '����� ������� �� ������� ����',
  // [Align] top side, center, bottom side
  '���� �� �����',
  '�������� �� ��������',
  '��� �� ����',
  // [Align] relative to
  '�������:',
  // [Position] to the right of the left side of
  '������ �� ����� ����:',
  // [Position] below the top side of
  '����� ��������� ����:',
  // Anchors: page, main text area (x2)
  '&�������', '&������ ������', '�����&��', '�&����� ������',
  // Anchors: left margin, right margin
  '&������ ����', '&������� ����',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '���&������ ����', '&�������� ����', '��&��������� ����', '����&������� ����',
  // Anchors: character, line, paragraph
  '&�������', '�&�����', '&������',
  // Mirrored margins checkbox, layout in table cell checkbox
  '����&������ ����', '&³������ ������ �������',
  // Above text, below text
  '&����� �������', '��&�� �������',
  // Width, Height groupboxes, Width, Height labels
  '������', '������', '&������:', '&������:',
  // Auto height label, Auto height combobox item
  '����', '����',
  // Vertical alignment groupbox; alignments: top, center, bottom
  '����������� �����������', '�&�����', '�� &������', '�&����',
  // Border and background button and title
  '&����� � �������..', '����� � ������� ���������� ����',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  '���������� �������', '�&�:', '������ RTF', '������ HTML',
  '�������������� �����', '����� � ��������� ������', '�������� ������� (BMP)', '�������� (WMF)',
  '����� �� ���������', 'URL',
  // Options group-box, styles
  '���������', '&����:',
  // style options: apply target styles, use source styles, ignore styles
  '����������� ���� � ������������ ���������', '�������� ���� �� ��������� ������ ������',
  '�������� ��������� ������, ���������� ����',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  '������', '����������', '�����������', '���������� ������', '����������� ������',
  // Customize, Reset, None
  '&������...', '&�������', 'ͳ',
  // Numbering: continue, reset to, create new
  '���������� ���������', '������ ��������� � ������ � ', '�������� ����� ������, ��������� �',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  '�������� �����', '�������� ������', '��������', '��� �����', '��� ������',
  // Bullet type
  '����������', '����', '�������',
  // Level, Start from, Continue numbering, Numbering group-box
  '&г����:', '&������ �:', '&����������', '���������',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  '���� ������', 'г���', '&ʳ������:', '���������� ������', '&��� ������:',
  // List types: bullet, image
  '������', '�������', '�������� �����|',
  // Number format, Number, Start level from, Font button
  '&������ ������:', '�����', '&��������� ���� �:', '&�����...',
  // Image button, bullet character, Bullet button,
  '&�������...', '&���� �������', '�&���...',
  // Position of list text, bullet, number, image
  '��������� �������', '��������� �������', '��������� ������', '��������� �������', '��������� ������',
  // at, left indent, first line indent, from left indent
  '', '&³����� ����:', '&������ �����:', '�� ����� �������',
  // [only] one level preview, preview
  '&���������� �������� ����� ������ ����', '�������',
  // Preview text
  '����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  '������ ��', '���� ��', '� �����',
  // level #, this level (for combo-box)
  'г���� %d', '��� �����',
  // Marker character dialog title
  '���� ����� �������',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  '����� � ������� ������', '�����', '�������',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  '��� �����', '&����:', '���&����:', '&�����:', '&³������...',
  // Sample, Border type group-boxes;
  '�������', '��� �����',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&����', '��&������', '��&�����', '��&�����', '������ &���������', '������ ��&����',
  // Fill color group-box; More colors, padding buttons
  '���� �������', '&����� ����...', '&����...',
  // preview text
  '����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����.',
  // title and group-box in Offsets dialog
  '³������', '³������ �� ������ �� �����',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  '�����', '�����������', '&˳��', '&�����', '�� &������', '�� &������',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  '���������', '�&����:', '�&����:', '&̳���������:', '&��������:',
  // Indents group-box; indents: left, right, first line
  '³������', '��&��:', '��&����:', '����&�� �����:',
  // indented, hanging
  '³��&���', '��&����', '�������',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  '���������', '����������', '��������', '����������', '�����', '�������',
  // preview text
  '����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����. ����� ����� ����� ����� �����.',
  // tabs: Indents and spacing, Tabs, Text flow
  '³������ �� ���������', '���������', '��������� �� �������',
  // tab stop position label; buttons: set, delete, delete all
  '��&����� ����������:', '�&���������', '���&�����', '&�������� ��',
  // tab align group; left, right, center aligns,
  '�����������', '&˳��', '&�����', '�� &������',
  // leader radio-group; no leader
  '���������', '(����)',
  // tab stops to be deleted, delete none, delete all labels
  '������ ��������:', '', '��.',
  // Pagination group-box, keep with next, keep lines together
  '���� �� �������', '�� &�������� �� ����������', '�� &��������� �����',
  // Outline level, Body text, Level #
  '&г����:', '�������� �����', 'г���� %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  '���������� ��������', '�� ������ �������', '������� ��������', '�������:',
  // Invalid Scale, [page #] of #
  '���� �����, ������ ����� �� 10 �� 500', '� %d',
  // Hints on buttons: first, prior, next, last pages
  '�� �����', '�� ����������', '�� ��������', '�� ��������',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  '���������', '���������', '̳� &��������', '̳� ������ ������� � ��������',
  // vertical, horizontal (x2)
  '&������������:', '&��������������:', '�&�����������:', '��&������������:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  '�����', '���������', '&����:', '&������ ����:', '�&����� ����:',
  // Width; Border type group-box;
  '&�������:', '��� �����',
  // Border types: none, sunken, raised, flat
  '&����', '&��������', '�&������', '&������',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  '�������� ������', '������� ��',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&������� ������� ����� � �������', '&�������� ������',
  // number of columns, rows, merge before
  'ʳ������ �&������:', 'ʳ������ &�����:', '&��''������ ����� ���������',
  // to original cols, rows check-boxes
  '������� �� �������� ��&�����', '������� �� �������� ���&��',
  // Add Rows form -------------------------------------------------------------
  '��������� �����', '&ʳ������ �����:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  '��������� �������', '�������', '�����������',
  // margins group-box, left, right, top, bottom
  '���� (�������)', '���� (�����)', '&˳��:', '&������:', '&�����:', '&�����:',
  // mirror margins check-box
  '�&��������� ����',
  // orientation group-box, portrait, landscape
  '��������', '&������', '&��������',
  // paper group-box, paper size, default paper source
  '����', '&�����:', '�&�����:',
  // header group-box, print on the first page, font button
  '������� ����������', '&�����:', '&��������� �� ������ �������', '&�����...',
  // header alignments: left, center, right
  '�&���', '�� &������', '��&����',
  // the same for footer (different hotkeys)
  '������ ����������', '�&����:', '����&����� �� ������ �������', '��&���...',
  '&����', '&�� ������', '���&���',
  // page numbers group-box, start from
  '������ �������', '��&���� �:',
  // hint about codes
  '�� ������ ��������������� ���������� ����������:'#13'&&p - ����� �������; &&P - ������� �������; &&d - ����; &&t - ���.',
  // Code Page form ------------------------------------------------------------
  // title, label
  '������ ������� �����', '&������ ��������� ������� �����:',
  // thai, japanese, chinese (simpl), korean
  '�������', '��������', '��������� (��������)', '���������',
  // chinese (trad), central european, cyrillic, west european
  '��������� (����������)', '���������� �� ������ ������', '��������', '�����������������',
  // greek, turkish, hebrew, arabic
  '�������', '��������', '�����', '��������',
  // baltic, vietnamese, utf-8, utf-16
  '���������', '�''���������', '������ (UTF-8)', '������ (UTF-16)',
  // Style form ----------------------------------------------------------------
  '����������', '&������ ����� ����������:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  '������������ � �����', '������ &���������:',
  // line break, tab, ';', ','
  '������ ������', '������ ���������', '������ � �����', '����',
  // Table sort form -----------------------------------------------------------
  // error message
  '��������� �������� ���������� ������� � �ᒺ������� ��������',
  // title, main options groupbox
  '���������� �������', '����������',
  // sort by column, case sensitive
  '&��������� �� ���������:', '� &����������� �������',
  // heading row, range or rows
  '��������� ����� &���������', '����� ��� ����������: � %d �� %d',
  // order, ascending, descending
  '��������������', '�� �&���������', '�� �&�������',
  // data type, text, number
  '���', '&�����', '&�����',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  '�����', '����������', '����� &���������:', '&��� ���������:',
  // numbering groupbox, continue, start from
  '���������', '&����������', '�&����� �:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  '�����', '&ϳ����:', '&��������� ����� � �����',
  // position radiogroup
  '���������', '&��� ������� ��''�����', '�&�� ������� ��''�����',
  // caption text
  '�&���� �����:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  '���������', '�������', '�������',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  '����� ������� �����', '�����',
  // Left, Top, Right, Bottom checkboxes
  '&˳�� �������', '&������ �������', '&����� �������', '&����� �������',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  '������ �������', '������ �����',
  // Hints on indents: first line, left (together with first), left (without first), right
  '³����� ������� �����', '³����� ����', '������', '³����� ������',
  // Hints on lists: up one level (left), down one level (right)
  '�������� ����� ������', '�������� ����� ������',
  // Hints for margins: bottom, left, right and top
  '����� ����', '˳�� ����', '����� ����', '������ ����',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  '�� ����� ����', '�� ������� ����', '�� ������', '�� ����������',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  '���������', '��������� ������', '��� ���������', '��������� %d', '����� ������',
  // Hyperlink, Title, Subtitle
  'ó������������', '�����', 'ϳ����������',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '��������', '������ ��������', '������ ��������', '�������',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  '������ 2', '������� ������', '������ ���������', '������ ���������',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  '������', '������ HTML', '��� HTML', '������� HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  '���������� HTML', '��������� HTML', '������ HTML', '���������� ������� HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  '����������� HTML', '������ HTML', '������� ����������', '������ ����������',
  '����� �������',
  // Caption
  '����� ��''����',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  '���� ������ �������', '���� �������', '����� ������ �������', '����� �������',
  // Sidenote Reference, Sidenote Text
  '���� ������� � �����', '����� ������� � �����',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  '����', '���� ����', '��������', '���������', '���� �����������',
  // default background color, default text color, [color] same as text
  '��������� ���� ����', '��������� ���� ������', '�� � ������',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  '��������', '�����', '�������', '���������', '����� ���������', '��������',
  // underline types: thick dashed, long dashed, thick long dashed,
  '����� ��������', '����� ��������', '����� ����� ��������',
  // underline types: dash dotted, thick dash dotted,
  '������������', '����� ������������',
  // underline types: dash dot dotted, thick dash dot dotted
  '������������ � 2 ��������', '����� ������������ � 2 ��������',
  // sub/superscript: not, subsript, superscript
  '�� ���������/��������', '��������', '���������',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  '�������� ������', '�� �������������', '���� �������', '������ �����',
  // bold, not bold
  '����������', '�� ����������',
  // italic, not italic
  '������', '�� ������',
  // underlined, not underlined, default underline
  '������������', '�� �����������', '����������� �� �������������',
  // struck out, not struck out
  '�����������', '�� �����������',
  // overlined, not overlined
  '���� ������', '��� ���� ������',
  // all capitals: yes, all capitals: no
  '�� ��������', '�� �� ��������',
  // vertical shift: none, by x% up, by x% down
  '��� ������������� �����', '�������� �� %d ������', '�������� �� %d ������',
  // characters width [: x%]
  '������ ����',
  // character spacing: none, expanded by x, condensed by x
  '�������� ����������� �������', '���������� �� %s', '���������� �� %s',
  // charset
  '���� �������',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  '��������� �����', '��������� �����', '����������� ����������',
  // [hyperlink] highlight, default hyperlink attributes
  '�������', '�������� ����������',
  // paragraph aligmnment: left, right, center, justify
  '�� ����� ����', '�� ������� ����', '�� ������', '�� ������',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  '������ �����: %d', '������� �� �������: %s', '������ �����: �� ����� %s',
  '������ �����: ���� %s',
  // no wrap, wrap
  '���������� ������� �����', '��������� ������� �����',
  // keep lines together: yes, no
  '�� ��������� �����', '��������� ��������� �����',
  // keep with next: yes, no
  '�� �������� �� ���������� ������', '��������� �������� �� ���������� ������',
  // read only: yes, no
  '���������� �����������', '��������� �����������',
  // body text, heading level x
  '����� ���������: �������� �����', '����� ���������: %d',
  // indents: first line, left, right
  '������ ������� �����', '������ ����', '������ ������',
  // space before, after
  '������� �����', '������� ����',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  '���������', '��', '�����������', '�� ����� ����', '�� ������� ����', '�� ������',
  // tab leader (filling character)
  '����������',
  // no, yes
  '��', '���',
  // [padding/spacing/side:] left, top, right, bottom
  '������', '������', '��������', '�����',
  // border: none, single, double, triple, thick inside, thick outside
  '��', '��������', '�������', '�������', '������ ���������', '������ �����',
  // background, border, padding, spacing [between text and border]
  '�������', '�����', '����', '�������',
  // border: style, width, internal width, visible sides
  '�����', '�������', '�������', '�������',
  // style inspector -----------------------------------------------------------
  // title
  '��������� �����',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  '�����', '(����)', '�����', '�����', '����������',
  // border and background, hyperlink, standard [style]
  '����� � �������', 'ó������������', '(�����������)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  '����', '�����',
  // name, applicable to,
  '&�����:', '&����� ����������� ��:',
  // based on, based on (no &),
  '����&���� ��:', '�������� ��:',
  //  next style, next style (no &)
  '&����� ���������� ������:', '����� ���������� ������:',
  // quick access check-box, description and preview
  '&������� ������', '���� � ��&������:',
  // [applicable to:] paragraph and text, paragraph, text
  '������� � ������', '�������', '������',
  // text and paragraph styles, default font
  '���� ������ �� �������', '�������� �����',
  // links: edit, reset, buttons: add, delete
  '������', '�������', '&������', '&��������',
  // add standard style, add custom style, default style name
  '������ &����������� �����...', '&������ �����', '����� %d',
  // choose style
  '������� &�����:',
  // import, export,
  '&������...', '&�������...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  '���� RichView (*.rvst)|*.rvst', '������� ������� �����', '� �������� ���� ������� ����',
  // Title, group-box, import styles
  '������ ����� � �����', '������', '&����������� ����:',
  // existing styles radio-group: Title, override, auto-rename
  '��� ������ ����', '&������� ������', '&������ � ������ �������',
  // Select, Unselect, Invert,
  '�&�����', '&��������', '&�����������',
  // select/unselect: all styles, new styles, existing styles
  '&�� ����', '&��� ����', '&������� ����',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '�������� ������', '�� ����',
  // dialog title, prompt
  '����', '&������ �����:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&��������', '&���������� ���', '&������ � �������',
  // Progress messages ---------------------------------------------------------
  '������������� %s', '��� ��������� �� �����...', '��������� ������� %d',
  '������������ � RTF...', '������������ � RTF...',
  '�������: %s...', '�����: %s...', '�����',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  '���� �������', '�������', 'ʳ����� �������', '������� � �����', '�������� ����'
  );


initialization
  RVA_RegisterLanguage(
    'Ukrainian', // english language name, do not translate
    '���������', // native language name
    RUSSIAN_CHARSET, @Messages);

end.
