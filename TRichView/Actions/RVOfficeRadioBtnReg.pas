
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Design-time support for TRVOfficeRadioButton,   }
{       TRVOfficeRadioGroup                             }
{                                                       }
{       Copyright (c) 2002-2003, Sergey Tkachenko       }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVOfficeRadioBtnReg;

interface

{$I RV_Defs.inc}

uses
  {$IFDEF RICHVIEWDEF6}
  DesignIntf, DesignEditors,
  {$ELSE}
  DsgnIntf,
  {$ENDIF}
  Windows, Messages, SysUtils, Classes, Controls, Forms, RVOfficeRadioBtn,
  ShellApi;
{$IFDEF RICHVIEWCBDEF3}
type
  TRVComponentEditor = TDefaultEditor;
  {----------------------------------------------------------}
  TRVORGEditor = class(TRVComponentEditor)
  protected
    {$IFDEF RICHVIEWDEF6}
    procedure EditProperty(const PropertyEditor: IProperty;
      var Continue: Boolean); override;
    {$ELSE}
     procedure EditProperty(PropertyEditor : TPropertyEditor;
       var Continue, FreeEditor : Boolean); override;
    {$ENDIF}
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

  procedure Register;
{$ENDIF}

implementation
{$IFDEF RICHVIEWCBDEF3}
{============================= TRVORGEditor ===================================}
function TRVORGEditor.GetVerbCount: Integer;
begin
  Result := 2;
end;
{------------------------------------------------------------------------------}
function TRVORGEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0:
      Result := 'Items Editor...';
    1:
      Result := 'Visit TRICHVIEW.COM';
    else
      Result := '';
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVORGEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0:
      Edit;
    1:
      ShellExecute(0, 'open', 'http://www.trichview.com?From_OfficeRadioMenu', nil, nil, SW_NORMAL);
  end;
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF6}
procedure TRVORGEditor.EditProperty(const PropertyEditor: IProperty;
      var Continue: Boolean); 
{$ELSE}
procedure TRVORGEditor.EditProperty(PropertyEditor : TPropertyEditor;
       var Continue, FreeEditor : Boolean);
{$ENDIF}
var
  PropName: string;
begin
  PropName := PropertyEditor.GetName;
  if CompareText(PropertyEditor.GetName, 'Items')=0 then begin
    PropertyEditor.Edit;
    Continue := False;
  end;
end;
{==============================================================================}
procedure Register;
begin
  RegisterComponents('RichView Misc', [TRVOfficeRadioButton, TRVOfficeRadioGroup]);
  RegisterComponentEditor(TRVOfficeRadioGroup, TRVORGEditor);
end;
{$ENDIF}

end.
