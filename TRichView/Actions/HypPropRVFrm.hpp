﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'HypPropRVFrm.pas' rev: 27.00 (Windows)

#ifndef HypproprvfrmHPP
#define HypproprvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <HypHoverPropRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Hypproprvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVHypProp;
class PASCALIMPLEMENTATION TfrmRVHypProp : public Hyphoverproprvfrm::TfrmRVHypHoverProp
{
	typedef Hyphoverproprvfrm::TfrmRVHypHoverProp inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gbNormal;
	Vcl::Stdctrls::TLabel* lblText;
	Rvcolorcombo::TRVColorCombo* cmbText;
	Rvcolorcombo::TRVColorCombo* cmbTextBack;
	Vcl::Stdctrls::TLabel* lblTextBack;
	Vcl::Stdctrls::TLabel* lblUnderlineColor;
	Rvcolorcombo::TRVColorCombo* cmbUnderlineColor;
	Vcl::Stdctrls::TLabel* lblUnderline;
	Vcl::Stdctrls::TComboBox* cmbUnderline;
	HIDESBASE void __fastcall cbAsNormalClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cmbTextBackColorChange(System::TObject* Sender);
	
public:
	Vcl::Controls::TControl* _cmbUnderline;
	DYNAMIC void __fastcall Localize(void);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVHypProp(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Hyphoverproprvfrm::TfrmRVHypHoverProp(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVHypProp(System::Classes::TComponent* AOwner, int Dummy) : Hyphoverproprvfrm::TfrmRVHypHoverProp(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVHypProp(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVHypProp(HWND ParentWindow) : Hyphoverproprvfrm::TfrmRVHypHoverProp(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Hypproprvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_HYPPROPRVFRM)
using namespace Hypproprvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// HypproprvfrmHPP
