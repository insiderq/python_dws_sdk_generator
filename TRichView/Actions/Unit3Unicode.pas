
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Demo project.                                   }
{       You can use it as a basis for your              }
{       applications.                                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit Unit3Unicode;

interface

{$I RV_Defs.inc}          // contains defines about Delphi compiler versions

uses
  Windows, Messages, SysUtils,
  Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls,
  ActnList, StdActns, ImgList,
  Menus, ShellApi, Printers, ToolWin, RVUni,
  RVScroll, RichView, RVEdit,
  RVStyle, PtblRV, CRVFData, RVTable, RVItem, CRVData, RVFuncs, IMouse,
  // RVAAddictLanguages,
  RichViewActions, RVALocRuler, RVFontCombos, RVALocalize, Ruler, RVRuler,
  GifImg, RVGifAnimate2007,
  PngImage, RVTypes, RVStyleFuncs, RVGrHandler;

type
  TForm3 = class(TForm)
    RichViewEdit1: TRichViewEdit;
    RVStyle1: TRVStyle;
    StatusBar1: TStatusBar;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton13: TToolButton;
    ToolButton22: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    ToolBar2: TToolBar;
    ToolButton41: TToolButton;
    ToolButton42: TToolButton;
    ToolButton43: TToolButton;
    ToolButton44: TToolButton;
    ToolButton49: TToolButton;
    ToolButton50: TToolButton;
    ToolButton51: TToolButton;
    ToolButton53: TToolButton;
    ToolButton54: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolBar3: TToolBar;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton23: TToolButton;
    ToolButton24: TToolButton;
    ToolButton25: TToolButton;
    ToolButton26: TToolButton;
    ToolButton28: TToolButton;
    ToolButton8: TToolButton;
    ToolButton27: TToolButton;
    ToolButton29: TToolButton;
    ToolButton30: TToolButton;
    ToolButton31: TToolButton;
    ToolButton32: TToolButton;
    ToolButton33: TToolButton;
    ToolButton34: TToolButton;
    ToolButton35: TToolButton;
    ToolButton36: TToolButton;
    ToolButton37: TToolButton;
    ToolButton38: TToolButton;
    ToolButton39: TToolButton;
    ToolButton40: TToolButton;
    ToolButton45: TToolButton;
    RVAControlPanel1: TRVAControlPanel;
    ToolButton46: TToolButton;
    ToolButton47: TToolButton;
    ToolBar4: TToolBar;
    ToolButton48: TToolButton;
    ToolButton52: TToolButton;
    ToolButton55: TToolButton;
    ToolButton56: TToolButton;
    ToolButton58: TToolButton;
    ToolButton59: TToolButton;
    ToolButton57: TToolButton;
    ToolButton60: TToolButton;
    ToolButton61: TToolButton;
    ToolBar5: TToolBar;
    cmbFont: TRVFontComboBox;
    RVAPopupMenu1: TRVAPopupMenu;
    RVPrint1: TRVPrint;
    ColorDialog1: TColorDialog;
    btnLanguage: TButton;
    cmbFontSize: TRVFontSizeComboBox;
    RVRuler1: TRVRuler;
    ToolButton62: TToolButton;
    ToolButton63: TToolButton;
    pmFakeDropDown: TPopupMenu;
    ToolButton64: TToolButton;
    ToolButton65: TToolButton;
    MainMenu1: TMainMenu;
    mitFile: TMenuItem;
    New1: TMenuItem;
    Load1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    Export1: TMenuItem;
    N1: TMenuItem;
    PageSetup1: TMenuItem;
    PrintPreview1: TMenuItem;
    Print1: TMenuItem;
    N3: TMenuItem;
    mitExit: TMenuItem;
    mitEdit: TMenuItem;
    Undo1: TMenuItem;
    Redo1: TMenuItem;
    N4: TMenuItem;
    Cut1: TMenuItem;
    Copy1: TMenuItem;
    n2: TMenuItem;
    rvActionPasteAsText11: TMenuItem;
    PasteSpecial1: TMenuItem;
    N5: TMenuItem;
    Find1: TMenuItem;
    FindNext1: TMenuItem;
    Replace1: TMenuItem;
    N26: TMenuItem;
    CharacterCase1: TMenuItem;
    N20: TMenuItem;
    InsertPageBreak1: TMenuItem;
    RemovePageBreak1: TMenuItem;
    N15: TMenuItem;
    SelectAll1: TMenuItem;
    mitFont: TMenuItem;
    Changefont1: TMenuItem;
    Font1: TMenuItem;
    N7: TMenuItem;
    mitFontStyle: TMenuItem;
    Bold1: TMenuItem;
    Italic1: TMenuItem;
    Underline1: TMenuItem;
    Strikeout1: TMenuItem;
    N14: TMenuItem;
    AllCapitals1: TMenuItem;
    Overline1: TMenuItem;
    N27: TMenuItem;
    rvActionSubscript11: TMenuItem;
    rvActionSuperscript11: TMenuItem;
    mitFontSize: TMenuItem;
    ShrinkFont1: TMenuItem;
    GrowFont1: TMenuItem;
    N13: TMenuItem;
    ShrinkFontByOnePoint1: TMenuItem;
    GrowFontByOnePoint1: TMenuItem;
    extColor1: TMenuItem;
    extBackgroundColor1: TMenuItem;
    mitPara: TMenuItem;
    rvActionParagraph11: TMenuItem;
    ParagraphBorders1: TMenuItem;
    N18: TMenuItem;
    AlignLeft1: TMenuItem;
    AlignCenter1: TMenuItem;
    AlignRight1: TMenuItem;
    Justify1: TMenuItem;
    N25: TMenuItem;
    BulletsandNumbering1: TMenuItem;
    Bullets1: TMenuItem;
    Numbering1: TMenuItem;
    N16: TMenuItem;
    Leftjustify1: TMenuItem;
    N6: TMenuItem;
    DecreaseIndent1: TMenuItem;
    IncreaseIndent1: TMenuItem;
    N17: TMenuItem;
    SingleLineSpacing1: TMenuItem;
    N15LineSpacing1: TMenuItem;
    DoubleLineSpacing1: TMenuItem;
    N28: TMenuItem;
    mitTextFlow: TMenuItem;
    ClearTextFlowatLeftSide1: TMenuItem;
    ClearTextFlowatRightSide1: TMenuItem;
    ClearTextFlowatBothSides1: TMenuItem;
    NormalTextFlow1: TMenuItem;
    N19: TMenuItem;
    ParagraphBackgroundColor1: TMenuItem;
    mitFormat: TMenuItem;
    Background1: TMenuItem;
    BackgroundColor1: TMenuItem;
    N22: TMenuItem;
    FillColor1: TMenuItem;
    N29: TMenuItem;
    RemoveHyperlinks1: TMenuItem;
    N30: TMenuItem;
    Properties1: TMenuItem;
    ObjectPosition1: TMenuItem;
    mitInsert: TMenuItem;
    File1: TMenuItem;
    Picture1: TMenuItem;
    HorizontalLine1: TMenuItem;
    HypertextLink1: TMenuItem;
    InsertSymbol1: TMenuItem;
    mitTable: TMenuItem;
    InsertTable1: TMenuItem;
    N9: TMenuItem;
    InsertColumnLeft1: TMenuItem;
    InsertColumnRight1: TMenuItem;
    N8: TMenuItem;
    InsertRowAbove1: TMenuItem;
    InsertRowBelow1: TMenuItem;
    N10: TMenuItem;
    DeleteRows1: TMenuItem;
    rvActionTableDeleteCols11: TMenuItem;
    DeleteTable1: TMenuItem;
    N12: TMenuItem;
    mitTableSelect: TMenuItem;
    SelectTable1: TMenuItem;
    SelectColumns1: TMenuItem;
    SelectRows1: TMenuItem;
    SelectCell1: TMenuItem;
    N21: TMenuItem;
    mitTableAlignCellContents: TMenuItem;
    AlignCellToTheTop1: TMenuItem;
    AlignCellToTheMiddle1: TMenuItem;
    AlignCellToTheBottom1: TMenuItem;
    DefaultCellVerticalAlignment1: TMenuItem;
    mitTableCellBorders: TMenuItem;
    LeftBorder1: TMenuItem;
    rvActionTableCellTopBorder11: TMenuItem;
    rvActionTableCellRightBorder11: TMenuItem;
    rvActionTableCellBottomBorder11: TMenuItem;
    rvActionTableCellAllBorders11: TMenuItem;
    rvActionTableCellNoBorders11: TMenuItem;
    N11: TMenuItem;
    SplitCells1: TMenuItem;
    MergeCells1: TMenuItem;
    N24: TMenuItem;
    ShowGridLines1: TMenuItem;
    N23: TMenuItem;
    ableProperties1: TMenuItem;
    mitHelp: TMenuItem;
    mitHelpTOC: TMenuItem;
    tbUnits: TToolBar;
    cmbUnits: TComboBox;
    mitTableCellRotation: TMenuItem;
    NoCellRotation1: TMenuItem;
    RotateCellby90Degrees1: TMenuItem;
    RotateCellby180Degrees1: TMenuItem;
    RotateCellby270Degrees1: TMenuItem;
    btnSkin: TButton;
    ConverttoText1: TMenuItem;
    Sort1: TMenuItem;
    SplitTable1: TMenuItem;
    ToolButton68: TToolButton;
    ToolButton69: TToolButton;
    ToolButton70: TToolButton;
    ProgressBar1: TProgressBar;
    Styles1: TMenuItem;
    AddStyle1: TMenuItem;
    ClearFormat1: TMenuItem;
    ClearTextFormat1: TMenuItem;
    InspectStyles1: TMenuItem;
    N31: TMenuItem;
    cmbStyles: TRVStyleTemplateComboBox;
    N32: TMenuItem;
    InsertFootnote1: TMenuItem;
    InsertEndnote1: TMenuItem;
    EditNote1: TMenuItem;
    InsertSidenote1: TMenuItem;
    Number1: TMenuItem;
    extBox1: TMenuItem;
    InsertCaption1: TMenuItem;
    ToolButton71: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure mitExitClick(Sender: TObject);
    procedure RichViewEdit1Jump(Sender: TObject; id: Integer);
    procedure RichViewEdit1ReadHyperlink(Sender: TCustomRichView;
      const Target, Extras: String; DocFormat: TRVLoadFormat; var StyleNo: Integer;
      var ItemTag: TRVTag; var ItemName: TRVRawByteString);
    procedure btnLanguageClick(Sender: TObject);
    procedure RVAControlPanel1MarginsChanged(Sender: TrvAction;
      Edit: TCustomRichViewEdit);
    procedure RVAControlPanel1Download(Sender: TrvAction;
      const Source: String);
    procedure pmFakeDropDownPopup(Sender: TObject);
    procedure RichViewEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure mitHelpTOCClick(Sender: TObject);
    procedure RichViewEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cmbUnitsClick(Sender: TObject);
    procedure btnSkinClick(Sender: TObject);
    procedure RichViewEdit1Progress(Sender: TCustomRichView;
      Operation: TRVLongOperation; Stage: TRVProgressStage; PercentDone: Byte);
    procedure RVPrint1SendingToPrinter(Sender: TCustomRichView;
      PageCompleted: Integer; Step: TRVPrintingStep);
  private
    { Private declarations }
    procedure ColorPickerShow(Sender: TObject);
    procedure ColorPickerHide(Sender: TObject);
    procedure rvActionSave1DocumentFileChange(Sender: TObject;
      Editor: TCustomRichViewEdit; const FileName: String;
      FileFormat: TrvFileSaveFilter; IsNew: Boolean);
    procedure rvActionEditNoteFormCreate(Sender: TrvAction; Form: TForm);
    procedure rvActionEditNoteShowing(Sender: TrvAction; Form: TForm);
    procedure rvActionEditNoteHide(Sender: TObject);
    procedure NoteEditorEnter(Sender: TObject);
    procedure NoteEditorExit(Sender: TObject);
    procedure UpdateLinkedControls;
    procedure Localize;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses dmActionsAlpha;
{$R *.dfm}

function GetFullFileName(const FileName: String): String;
begin
  Result := ExtractFilePath(Application.ExeName)+FileName;
  if FileExists(Result) then
    exit;
  Result := ExtractFilePath(Application.ExeName)+'..\'+FileName;
  if FileExists(Result) then
    exit;
  Result := ExtractFilePath(Application.ExeName)+'..\..\'+FileName;
  if FileExists(Result) then
    exit;
  Result := '';
end;

procedure TForm3.FormCreate(Sender: TObject);
var FileName: String;
begin
  cmbFont.Font.Name := RVAControlPanel1.DialogFontName;
  cmbFontSize.Font.Name := RVAControlPanel1.DialogFontName;
  cmbUnits.Font.Name := RVAControlPanel1.DialogFontName;
  cmbStyles.Font.Name := RVAControlPanel1.DialogFontName;

  RichViewEdit1.RTFReadProperties.UnicodeMode := rvruOnlyUnicode;

  // Almost all these assignments could be done at design time in the Object Inspector
  // But in this demo we do not want to modify rvActionsResource
  // (and we recommend to use a copy of it in your applications)

  rvActionsResource.rvActionSave1.OnDocumentFileChange := rvActionSave1DocumentFileChange;

  // initializing TrvActionEditNote action
  rvActionsResource.rvActionEditNote1.Control := RichViewEdit1;
  rvActionsResource.rvActionEditNote1.OnFormCreate := rvActionEditNoteFormCreate;
  rvActionsResource.rvActionEditNote1.OnShowing := rvActionEditNoteShowing;
  rvActionsResource.rvActionEditNote1.OnHide := rvActionEditNoteHide;

  // linking combo boxes and style inspector to RichViewEdit1
  UpdateLinkedControls;

  // Code for making color-picking buttons stay pressed while a
  // color-picker window is visible.
  rvActionsResource.rvActionColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionParaColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionParaColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionFontColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionFontColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionFontBackColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionFontBackColor1.OnHideColorPicker := ColorPickerHide;

  cmbUnits.ItemIndex := ord(RVAControlPanel1.UnitsDisplay);

  Localize;

  // The ruler initialization might set Modify flag, clearing it
  RichViewEdit1.Modified := False;
  // Loading initial file via ActionOpen
  FileName := GetFullFileName('readme.rvf');
  if FileName<>'' then
    rvActionsResource.rvActionOpen1.LoadFile(RichViewEdit1, FileName, ffiRVF)
  else
    rvActionsResource.rvActionNew1.ExecuteTarget(RichViewEdit1);
end;


{------------------- Working with document ------------------------------------}

// When document is created, saved, loaded...
procedure TForm3.rvActionSave1DocumentFileChange(Sender: TObject;
  Editor: TCustomRichViewEdit; const FileName: String;
  FileFormat: TrvFileSaveFilter; IsNew: Boolean);
var s: String;
begin
  s := ExtractFileName(FileName);
  rvActionsResource.rvActionPrint1.Title := s;
  rvActionsResource.rvActionQuickPrint1.Title := s;
  if IsNew then
    s := s+' (*)';
  Caption := s + ' - RichViewActionsTest';
end;

// Prompt for saving...
procedure TForm3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := rvActionsResource.rvActionSave1.CanCloseDoc(RichViewEdit1);
end;

procedure TForm3.mitExitClick(Sender: TObject);
begin
  Close;
end;

{--------------- Working with color-picking buttons ---------------------------}

// Code for making color-picking buttons pressed while
// a color-picker window is visible.
procedure TForm3.ColorPickerShow(Sender: TObject);
begin
  if (Sender as TAction).ActionComponent is TToolButton then
    TToolButton(TAction(Sender).ActionComponent).Down := True;
end;

procedure TForm3.ColorPickerHide(Sender: TObject);
begin
  if (Sender as TAction).ActionComponent is TToolButton then
    TToolButton(TAction(Sender).ActionComponent).Down := False;
end;

{-------------- Set of events for processing hypertext jumps ------------------}

// Hyperlink click
procedure TForm3.RichViewEdit1Jump(Sender: TObject; id: Integer);
begin
  rvActionsResource.rvActionInsertHyperlink1.GoToLink(RichViewEdit1, id);
end;

// Importing hyperlink from RTF, HTML, or accepting from drag&drop
procedure TForm3.RichViewEdit1ReadHyperlink(Sender: TCustomRichView;
  const Target, Extras: String; DocFormat: TRVLoadFormat; var StyleNo: Integer;
  var ItemTag: TRVTag; var ItemName: TRVRawByteString);
begin
  if DocFormat=rvlfURL then
    StyleNo :=
      rvActionsResource.rvActionInsertHyperlink1.GetHyperlinkStyleNo(RichViewEdit1);
  ItemTag := rvActionsResource.rvActionInsertHyperlink1.EncodeTarget(Target);
end;


// URL detection on typing
procedure TForm3.RichViewEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {
  // uncomment if you use Addict3
  if Key = VK_RETURN then
    RVA_Addict3AutoCorrect(RichViewEdit1);
  }
  if (Key=VK_RETURN) then begin
    rvActionsResource.rvActionInsertHyperlink1.DetectURL(RichViewEdit1);
    rvActionsResource.rvActionInsertHyperlink1.TerminateHyperlink(RichViewEdit1);
  end;    
end;

procedure TForm3.RichViewEdit1KeyPress(Sender: TObject; var Key: Char);
begin
  {
  // uncomment if you use Addict3
  if (Key in ['''', #13, #9]) or (Pos(Key, RichViewEdit1.Delimiters)<>0) then
    RVA_Addict3AutoCorrect(RichViewEdit1);
  }
  if CharInSet(Key, [#9, ' ', ',', ';']) then begin
    rvActionsResource.rvActionInsertHyperlink1.DetectURL(RichViewEdit1);
    rvActionsResource.rvActionInsertHyperlink1.TerminateHyperlink(RichViewEdit1);
  end;
end;

{----------------------- Insert table popup      -----------------------------}
{ We use a trick: insert-table button has style tbsDropDown and assigned
  DropDownMenu (pmFakeDropDown). This menu is empty, but shows table size
  popup instead of itself }
procedure TForm3.pmFakeDropDownPopup(Sender: TObject);
begin
  rvActionsResource.rvActionInsertTable1.ShowTableSizeDialog(RichViewEdit1,
    ToolButton12);
end;

{---------------------------- Ruler ------------------------------------}

procedure TForm3.RVAControlPanel1MarginsChanged(Sender: TrvAction;
  Edit: TCustomRichViewEdit);
begin
  RVRuler1.UpdateRulerMargins;
end;

{---------------------------- Localization ------------------------------------}

procedure TForm3.btnLanguageClick(Sender: TObject);
begin
  if RVA_ChooseLanguage then
    Localize;
end;

procedure TForm3.Localize;
var i: Integer;
begin
  // Fonts
  Font.Charset            := RVA_GetCharset;
  StatusBar1.Font.Charset := RVA_GetCharset;
  Screen.HintFont.Charset := RVA_GetCharset;
  Screen.MenuFont.Charset := RVA_GetCharset;
  // Localizing all actions on rvActionsResource
  RVA_LocalizeForm(rvActionsResource);
  // Localizing all actions on this form
  RVA_LocalizeForm(Self);
  // Localizing ruler
  RVALocalizeRuler(RVRuler1);
  // Units
  i := cmbUnits.ItemIndex;
  RVA_TranslateUnits(cmbUnits.Items);
  cmbUnits.ItemIndex := i;
  for i := 0 to CoolBar1.Bands.Count - 1 do
    if CoolBar1.Bands[i].Control = tbUnits then
      CoolBar1.Bands[i].Text := RVA_GetS(rvam_lbl_Units);
  // Styles
  rvActionsResource.rvActionStyleInspector1.UpdateInfo;
  cmbStyles.Localize;
  // Localizing menus
  mitFile.Caption := RVA_GetS(rvam_menu_File);
  mitEdit.Caption := RVA_GetS(rvam_menu_Edit);
  mitFont.Caption := RVA_GetS(rvam_menu_Font);
  mitPara.Caption := RVA_GetS(rvam_menu_Para);
  mitFormat.Caption := RVA_GetS(rvam_menu_Format);
  mitInsert.Caption := RVA_GetS(rvam_menu_Insert);
  mitTable.Caption := RVA_GetS(rvam_menu_Table);
  mitExit.Caption := RVA_GetS(rvam_menu_Exit);
  mitHelp.Caption := RVA_GetS(rvam_menu_Help);
  mitHelpTOC.Caption := RVA_GetS(rvam_menu_Help);

  mitFontSize.Caption := RVA_GetS(rvam_menu_FontSize);
  mitFontStyle.Caption := RVA_GetS(rvam_menu_FontStyle);
  mitTextFlow.Caption := RVA_GetS(rvam_menu_TextFlow);
  mitTableSelect.Caption := RVA_GetS(rvam_menu_TableSelect);
  mitTableCellBorders.Caption := RVA_GetS(rvam_menu_TableCellBorders);
  mitTableAlignCellContents.Caption := RVA_GetS(rvam_menu_TableCellAlign);
  mitTableCellRotation.Caption := RVA_GetS(rvam_menu_TableCellRotation);
  // In your application, you can use either TrvActionFonts or TrvActionFontEx
  rvActionsResource.rvActionFonts1.Caption := rvActionsResource.rvActionFonts1.Caption+' (Standard)';
  rvActionsResource.rvActionFontEx1.Caption := rvActionsResource.rvActionFontEx1.Caption+' (Advanced)';

  {
  // uncomment if you use Addict3. It's assumed that RVAddictSpell31
  // and RVThesaurus31 are on this form.
  RVAddictSpell31.UILanguage := GetAddictSpellLanguage(RVA_GetLanguageName);
  RVThesaurus31.UILanguage := GetAddictThesLanguage(RVA_GetLanguageName);
  }
end;
{-------------------- Progress messages ---------------------------------------}
// Downloading
procedure TForm3.RVAControlPanel1Download(Sender: TrvAction;
  const Source: String);
begin
  if Source='' then
    Application.Hint := ''
  else
    Application.Hint := RVAFormat(RVA_GetS(rvam_msg_Downloading), [Source]);
end;
// Reading/writing
procedure TForm3.RichViewEdit1Progress(Sender: TCustomRichView;
  Operation: TRVLongOperation; Stage: TRVProgressStage; PercentDone: Byte);
begin
  case Stage of
    rvpstgStarting:
      begin
        ProgressBar1.Left := btnSkin.Left - ProgressBar1.Width - 8;
        ProgressBar1.Top :=  btnSkin.Top + (btnSkin.Height-ProgressBar1.Height) div 2;
        ProgressBar1.Position := 0;
        ProgressBar1.Visible := True;
        Application.Hint := RVA_GetProgressMessage(Operation);
      end;
    rvpstgRunning:
      begin
        ProgressBar1.Position := PercentDone;
        ProgressBar1.Update;
      end;
    rvpstgEnding:
      begin
        ProgressBar1.Position := 100;
        ProgressBar1.Update;
        Application.Hint := '';
        ProgressBar1.Visible := False;
      end;
  end;
end;
// Printing
procedure TForm3.RVPrint1SendingToPrinter(Sender: TCustomRichView;
  PageCompleted: Integer; Step: TRVPrintingStep);
begin
  Application.Hint := RVA_GetPrintingMessage(PageCompleted, Step);
end;

{-------------------------------- Misc. ---------------------------------------}

procedure TForm3.mitHelpTOCClick(Sender: TObject);
var HelpFileName: TRVUnicodeString;
begin
  HelpFileName := GetFullFileName('Help\RichViewActions.chm');
  if HelpFileName<>'' then
    RvHtmlHelp(HelpFileName);
end;

procedure TForm3.cmbUnitsClick(Sender: TObject);
begin
// --- 1. Units displayed to the user
  // units for displaying in dialogs
  RVAControlPanel1.UnitsDisplay := TRVUnits(cmbUnits.ItemIndex);
  // units for displaying in the ruler
  RVRuler1.UnitsDisplay := TRulerUnits(cmbUnits.ItemIndex);
  // units for page setup dialog
  if RVAControlPanel1.UnitsDisplay in [rvuPixels, rvuCentimeters, rvuMillimeters] then
    rvActionsResource.rvActionPageSetup1.MarginsUnits := rvpmuMillimeters
  else
    rvActionsResource.rvActionPageSetup1.MarginsUnits := rvpmuInches;
// --- 2. units for internal measurement
  // units of measurement for RichViewActions
  if RVAControlPanel1.UnitsDisplay=rvuPixels then begin
    RVA_ConvertToPixels(rvActionsResource);
    RVAControlPanel1.UnitsProgram := rvstuPixels;
    end
  else begin
    RVA_ConvertToTwips(rvActionsResource);
    RVAControlPanel1.UnitsProgram := rvstuTwips;
  end;
  // units of measurement for the current document
  if RVStyle1.Units<>RVAControlPanel1.UnitsProgram then begin
    RichViewEdit1.ConvertDocToDifferentUnits(RVAControlPanel1.UnitsProgram);
    RVStyle1.ConvertToDifferentUnits(RVAControlPanel1.UnitsProgram);
    RichViewEdit1.Change;
  end;
  RichViewEdit1.SetFocus;
end;
{------------------------------- Notes ----------------------------------------}
procedure TForm3.rvActionEditNoteFormCreate(Sender: TrvAction; Form: TForm);
begin
  rvActionsResource.rvActionEditNote1.SubDocEditor.OnEnter := NoteEditorEnter;
  rvActionsResource.rvActionEditNote1.SubDocEditor.OnExit := NoteEditorExit;
  rvActionsResource.rvActionEditNote1.SubDocEditor.PopupMenu := RVAPopupMenu1;
  Form.Align := alBottom;
  Form.Height := 100;
  Form.Parent := Self;
end;

procedure TForm3.rvActionEditNoteShowing(Sender: TrvAction; Form: TForm);
begin
  RichViewEdit1.ForceFieldHighlight := True;
end;

procedure TForm3.rvActionEditNoteHide(Sender: TObject);
begin
  RichViewEdit1.ForceFieldHighlight := False;
  if RichViewEdit1.CanFocus then
    RichViewEdit1.SetFocus;
end;

procedure TForm3.NoteEditorEnter(Sender: TObject);
begin
  RVAControlPanel1.DefaultControl := rvActionsResource.rvActionEditNote1.SubDocEditor;
  UpdateLinkedControls;
end;

procedure TForm3.NoteEditorExit(Sender: TObject);
begin
  RVAControlPanel1.DefaultControl := RichViewEdit1;
  UpdateLinkedControls;
end;

procedure TForm3.UpdateLinkedControls;
begin
  rvActionsResource.rvActionStyleInspector1.Control := RVAControlPanel1.DefaultControl;
  cmbFont.Editor := RVAControlPanel1.DefaultControl;
  cmbFontSize.Editor := RVAControlPanel1.DefaultControl;
  cmbStyles.Editor := RVAControlPanel1.DefaultControl;
end;
{----------------------- UI Styles (Delphi XE2+ -------------------------------}
procedure TForm3.btnSkinClick(Sender: TObject);
begin
  RVA_ChooseStyle;
end;

{---------------------- Live spelling with Addict 3 ---------------------------}
(*
// Add these events if you use Addict3
// (Assuming that you have RVAddictSpell31: TRVAddictSpell3 on the form)

// RichViewEdit1.OnSpellingCheck event
procedure TForm3.RichViewEdit1SpellingCheck(Sender: TCustomRichView;
  const AWord: String; StyleNo: Integer; var Misspelled: Boolean);
begin
  Misspelled := not RVAddictSpell31.WordAcceptable(AWord);
end;

// RVAddictSpell31.OnParserIgnoreWord: if Ignore All or Add buttons were pressed
// in spellchecker dialog, removing underlines from the ignored word
procedure TForm3.RVAddictSpell31ParserIgnoreWord(Sender: TObject;
  Before: Boolean; State: Integer);
begin
  if not Before and (State in [IgnoreState_IgnoreAll, IgnoreState_Add]) then
    RichViewEdit1.LiveSpellingValidateWord(RVAddictSpell31.CurrentWord);
end;

// Besides, if you want spelling check starting when loading document, call
// RichViewEdit1.StartLiveSpelling in rvActionFileOpen.OnOpenFile event
*)


end.