﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'BaseRVFrm.pas' rev: 27.00 (Windows)

#ifndef BaservfrmHPP
#define BaservfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.CheckLst.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Baservfrm
{
//-- type declarations -------------------------------------------------------
typedef Vcl::Forms::TForm TfrmRVBaseBase;

typedef Vcl::Comctrls::TTreeView TRVATreeView;

typedef Vcl::Comctrls::TTreeNode TRVATreeNode;

typedef Vcl::Menus::TPopupMenu TRVADialogPopupMenu;

class DELPHICLASS TfrmRVBase;
class PASCALIMPLEMENTATION TfrmRVBase : public Vcl::Forms::TForm
{
	typedef Vcl::Forms::TForm inherited;
	
__published:
	void __fastcall FormCreate(System::TObject* Sender);
	
private:
	void __fastcall btnHelpClick(System::TObject* Sender);
	
protected:
	virtual void __fastcall SetUseXPThemes(const bool Value);
	void __fastcall LocalizeSpecialControls(void);
	Vcl::Stdctrls::TButton* __fastcall FindButtonWithModalResult(System::Uitypes::TModalResult ModalResult);
	void __fastcall AddHelpButton(void);
	DYNAMIC Vcl::Stdctrls::TButton* __fastcall GetOkButton(void);
	Vcl::Stdctrls::TButton* __fastcall GetCancelButton(void);
	
public:
	System::Classes::TComponent* ControlPanel;
	__fastcall TfrmRVBase(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel);
	DYNAMIC void __fastcall Localize(void);
	bool __fastcall GetRadioButtonChecked(Vcl::Controls::TControl* rb);
	void __fastcall SetRadioButtonChecked(Vcl::Controls::TControl* rb, bool Checked);
	bool __fastcall GetCheckBoxChecked(Vcl::Controls::TControl* cb);
	void __fastcall SetCheckBoxChecked(Vcl::Controls::TControl* cb, bool Checked);
	bool __fastcall GetListBoxChecked(Vcl::Controls::TControl* lst, int ItemIndex);
	void __fastcall SetListBoxChecked(Vcl::Controls::TControl* lst, int ItemIndex, bool Checked);
	Vcl::Stdctrls::TCheckBoxState __fastcall GetCheckBoxState(Vcl::Controls::TControl* cb);
	void __fastcall SetCheckBoxState(Vcl::Controls::TControl* cb, Vcl::Stdctrls::TCheckBoxState State);
	void __fastcall SetFormCaption(const System::UnicodeString Caption);
	void __fastcall SetControlCaption(Vcl::Controls::TControl* Control, const System::UnicodeString Caption);
	System::UnicodeString __fastcall GetEditText(Vcl::Controls::TControl* Edit);
	void __fastcall SetEditSelText(Vcl::Controls::TControl* Edit, const System::UnicodeString s);
	Vcl::Graphics::TFont* __fastcall GetEditFont(Vcl::Controls::TControl* Edit);
	int __fastcall GetEditSelStart(Vcl::Controls::TControl* Edit);
	int __fastcall GetEditSelLength(Vcl::Controls::TControl* Edit);
	void __fastcall SetEditSelStart(Vcl::Controls::TControl* Edit, int Value);
	void __fastcall SetEditSelLength(Vcl::Controls::TControl* Edit, int Value);
	int __fastcall GetXBoxItemCount(Vcl::Controls::TControl* Box);
	void __fastcall ClearXBoxItems(Vcl::Controls::TControl* Box);
	void __fastcall XBoxItemsAddObject(Vcl::Controls::TControl* Box, const System::UnicodeString s, System::TObject* obj, bool SysLanguage = false);
	System::TObject* __fastcall GetXBoxObject(Vcl::Controls::TControl* Box, int Index);
	void __fastcall SetXBoxItems(Vcl::Controls::TControl* Box, System::Classes::TStrings* Items);
	int __fastcall GetXBoxItemIndex(Vcl::Controls::TControl* Box);
	System::Classes::TStrings* __fastcall CreateXBoxItemsCopy(Vcl::Controls::TControl* Box);
	int __fastcall GetPageControlActivePageIndex(Vcl::Controls::TControl* pc);
	Vcl::Controls::TWinControl* __fastcall GetPageControlActivePage(Vcl::Controls::TControl* pc);
	void __fastcall SetPageControlActivePage(Vcl::Controls::TControl* pc, Vcl::Controls::TControl* Page);
	void __fastcall SetXBoxItemIndex(Vcl::Controls::TControl* Box, int ItemIndex);
	void __fastcall SetListBoxSorted(Vcl::Controls::TControl* Box);
	void __fastcall SetButtonDown(Vcl::Controls::TControl* SpeedButton, bool Value);
	bool __fastcall GetButtonDown(Vcl::Controls::TControl* SpeedButton);
	void __fastcall SetButtonFlat(Vcl::Controls::TControl* SpeedButton, bool Value);
	bool __fastcall GetButtonFlat(Vcl::Controls::TControl* SpeedButton);
	System::Classes::TNotifyEvent __fastcall GetButtonClick(Vcl::Controls::TControl* SpeedButton);
	void __fastcall HideTabSheet(Vcl::Controls::TControl* ts);
	bool __fastcall IsTabSheetVisible(Vcl::Controls::TControl* ts);
	void __fastcall UpdateLengthSpinEdit(Rvspinedit::TRVSpinEdit* se, bool FineUnits, bool OnlyPositive);
	void __fastcall UpdateLengthSpinEdit2(Rvspinedit::TRVSpinEdit* se, Rvstyle::TRVUnits Units, bool OnlyPositive);
	void __fastcall UpdatePercentSpinEdit(Rvspinedit::TRVSpinEdit* se, int MinValue = 0x0);
	void __fastcall UpdateSpinEditEx(Rvspinedit::TRVSpinEdit* se, int MinValue, int MaxValue, int Digits);
	__property bool UseXPThemes = {write=SetUseXPThemes, nodefault};
	DYNAMIC bool __fastcall AllowInvertImageListImage(Vcl::Imglist::TCustomImageList* il, int Index);
	void __fastcall PrepareImageList(Vcl::Imglist::TCustomImageList* il);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	void __fastcall AdjustFormSize(void);
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVBase(System::Classes::TComponent* AOwner, int Dummy) : Vcl::Forms::TForm(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVBase(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVBase(HWND ParentWindow) : Vcl::Forms::TForm(ParentWindow) { }
	
};


typedef System::TMetaClass* TfrmRVBaseClass;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::WideString __fastcall _GetWideString(const System::UnicodeString s, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall _GetString(const System::WideString s, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVAAddRV(Richview::TCustomRichView* rv, const System::UnicodeString s, int StyleNo = 0x0, int ParaNo = 0x0, const Rvstyle::TRVTag Tag = Rvstyle::TRVTag());
}	/* namespace Baservfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BASERVFRM)
using namespace Baservfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BaservfrmHPP
