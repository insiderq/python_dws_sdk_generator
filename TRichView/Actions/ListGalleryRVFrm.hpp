﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ListGalleryRVFrm.pas' rev: 27.00 (Windows)

#ifndef ListgalleryrvfrmHPP
#define ListgalleryrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RVReport.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Listgalleryrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVListGallery;
class PASCALIMPLEMENTATION TfrmRVListGallery : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* ts1;
	Vcl::Comctrls::TTabSheet* ts2;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rg1;
	Vcl::Stdctrls::TButton* btnEdit1;
	Rvstyle::TRVStyle* RVStyle2;
	Vcl::Controls::TImageList* il;
	Rvreport::TRVReportHelper* helper;
	Vcl::Stdctrls::TButton* btnReset;
	Rvstyle::TRVStyle* rvstmp;
	Rvstyle::TRVStyle* RVStyle1;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rg2;
	Vcl::Stdctrls::TButton* btnEdit2;
	Vcl::Controls::TImageList* il2;
	Vcl::Controls::TImageList* il3;
	Vcl::Stdctrls::TComboBox* cmbNumbering;
	Rvspinedit::TRVSpinEdit* seStartFrom;
	Rvstyle::TRVStyle* rvsTemplate1;
	Rvstyle::TRVStyle* rvsTemplate2;
	Rvstyle::TRVStyle* rvstmp2;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall rg1Click(System::TObject* Sender);
	void __fastcall btnEdit1Click(System::TObject* Sender);
	void __fastcall btnEdit2Click(System::TObject* Sender);
	void __fastcall rg2Click(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall rg2DblClickItem(System::TObject* Sender);
	void __fastcall cmbNumberingClick(System::TObject* Sender);
	void __fastcall btnResetClick(System::TObject* Sender);
	
private:
	Rvstyle::TRVStyle* SourceRVStyle;
	System::StaticArray<Rvclasses::TRVIntegerList*, 2> StyleIndices;
	Rvclasses::TRVIntegerList* StartFromList;
	int FMainPage;
	int FMainIndex;
	Vcl::Controls::TControl* _btnEdit1;
	Vcl::Controls::TControl* _btnEdit2;
	Vcl::Controls::TControl* _btnReset;
	Vcl::Controls::TControl* _cmbNumbering;
	Vcl::Controls::TControl* _pc;
	Vcl::Controls::TControl* _ts1;
	Vcl::Controls::TControl* _ts2;
	Vcl::Controls::TControl* _btnOk;
	Rvstyle::TRVStyleLength FIndentStep;
	void __fastcall CreateThumbnail(Vcl::Graphics::TBitmap* bmp, int ListNo, Rvstyle::TRVStyle* RVStyle1);
	void __fastcall EditList(Rvstyle::TRVStyle* RVStyle, int Index);
	void __fastcall InitBullets(void);
	Rvstyle::TRVStyle* __fastcall GetRVStyle(int PageNo);
	Rvstyle::TRVStyle* __fastcall GetRVStyleTemplate(int PageNo);
	Rvofficeradiobtn::TRVOfficeRadioGroup* __fastcall Getrg(int PageNo);
	void __fastcall AdjustIndents(Rvstyle::TRVListInfos* ListStyles);
	
public:
	Vcl::Actnlist::TAction* Action;
	__fastcall TfrmRVListGallery(System::Classes::TComponent* AOwner, Vcl::Actnlist::TAction* AAction);
	void __fastcall GetListStyle(Rvstyle::TRVListInfo* &ListStyle, int &ListNo, int &StartFrom, bool &UseStartFrom);
	void __fastcall InitStyles(Rvedit::TCustomRichViewEdit* rve);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVListGallery(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVListGallery(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVListGallery(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVListGallery(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Listgalleryrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_LISTGALLERYRVFRM)
using namespace Listgalleryrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ListgalleryrvfrmHPP
