﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVALocalize.pas' rev: 27.00 (Windows)

#ifndef RvalocalizeHPP
#define RvalocalizeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVALUTF8_EngUS.hpp>	// Pascal unit
#include <RVALUTF8_Rus.hpp>	// Pascal unit
#include <RVALUTF8_Sv.hpp>	// Pascal unit
#include <RVALUTF8_De.hpp>	// Pascal unit
#include <RVALUTF8_Ita.hpp>	// Pascal unit
#include <RVALUTF8_Fr.hpp>	// Pascal unit
#include <RVALUTF8_PtBr.hpp>	// Pascal unit
#include <RVALUTF8_Czech.hpp>	// Pascal unit
#include <RVALUTF8_Dutch.hpp>	// Pascal unit
#include <RVALUTF8_Esp.hpp>	// Pascal unit
#include <RVALUTF8_Ukr.hpp>	// Pascal unit
#include <RVALUTF8_Turkish.hpp>	// Pascal unit
#include <RVALUTF8_BLR.hpp>	// Pascal unit
#include <RVALUTF8_ChsZh.hpp>	// Pascal unit
#include <RVALUTF8_Hun.hpp>	// Pascal unit
#include <RVALUTF8_Polish.hpp>	// Pascal unit
#include <RVALUTF8_Rom.hpp>	// Pascal unit
#include <RVALUTF8_Fin.hpp>	// Pascal unit
#include <RVALUTF8_Bul.hpp>	// Pascal unit
#include <RVALUTF8_Hindi.hpp>	// Pascal unit
#include <RVALUTF8_Danish.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvalocalize
{
//-- type declarations -------------------------------------------------------
typedef System::UnicodeString TRVALocString;

typedef System::WideChar * PRVALocChar;

typedef System::Classes::TStrings TRVALocStrings;

typedef System::Classes::TStringList TRVALocStringList;

struct DECLSPEC_DRECORD TRVAColorRecord
{
public:
	System::WideChar *Name;
	System::Uitypes::TColor Color;
};


enum DECLSPEC_DENUM TRVAMessageID : unsigned short { rvam_Empty, rvam_unit_Inches, rvam_unit_MM, rvam_unit_CM, rvam_unit_Picas, rvam_unit_Pixels, rvam_unit_Points, rvam_unitN_Inches, rvam_unitN_MM, rvam_unitN_CM, rvam_unitN_Picas, rvam_unitN_Pixels, rvam_unitN_Points, rvam_menu_File, rvam_menu_Edit, rvam_menu_Format, rvam_menu_Font, rvam_menu_Para, rvam_menu_Insert, rvam_menu_Table, rvam_menu_Window, rvam_menu_Help, rvam_menu_Exit, rvam_menu_View, rvam_menu_Tools, rvam_menu_FontSize, rvam_menu_FontStyle, rvam_menu_TableSelect, rvam_menu_TableCellAlign, rvam_menu_TableCellBorders, rvam_menu_TableCellRotation, rvam_menu_TextFlow, rvam_menu_Notes, rvam_ribt_MainTab1, rvam_ribt_MainTab2, rvam_ribt_View, rvam_ribt_Table, rvam_ribg_Clipboard, rvam_ribg_Font, 
	rvam_ribg_Paragraph, rvam_ribg_List, rvam_ribg_Edit, rvam_ribg_Insert, rvam_ribg_Background, rvam_ribg_PageLayout, rvam_ribg_Links, rvam_ribg_Notes, rvam_ribg_DocumentViews, rvam_ribg_ShowHide, rvam_ribg_Zoom, rvam_ribg_Options, rvam_ribg_TableInsert, rvam_ribg_TableDelete, rvam_ribg_TableOperations, rvam_ribg_TableBorders, rvam_ribg_Styles, rvam_rib_ScreenTipFooter, rvam_rib_RecentFiles, rvam_rib_SaveAsTitle, rvam_rib_PrintTitle, rvam_lbl_Units, rvam_act_New, rvam_act_NewH, rvam_act_Open, rvam_act_OpenH, rvam_act_Save, rvam_act_SaveH, rvam_act_SaveAs, rvam_act_SaveAsH, rvam_act_Export, rvam_act_ExportH, rvam_act_Preview, rvam_act_PreviewH, rvam_act_Print, rvam_act_PrintH, rvam_act_QuickPrint, rvam_act_QuickPrint2, rvam_act_QuickPrintH, rvam_act_PageSetup, 
	rvam_act_PageSetupH, rvam_act_Cut, rvam_act_CutH, rvam_act_Copy, rvam_act_CopyH, rvam_act_Paste, rvam_act_PasteH, rvam_act_PasteAsText, rvam_act_PasteAsTextH, rvam_act_PasteSpecial, rvam_act_PasteSpecialH, rvam_act_SelectAll, rvam_act_SelectAllH, rvam_act_Undo, rvam_act_UndoH, rvam_act_Redo, rvam_act_RedoH, rvam_act_Find, rvam_act_FindH, rvam_act_FindNext, rvam_act_FindNextH, rvam_act_Replace, rvam_act_ReplaceH, rvam_act_InsertFile, rvam_act_InsertFileH, rvam_act_InsertPicture, rvam_act_InsertPictureH, rvam_act_InsertHLine, rvam_act_InsertHLineH, rvam_act_Hyperlink, rvam_act_HyperlinkH, rvam_act_RemoveHyperlinks, rvam_act_RemoveHyperlinksH, rvam_act_InsertSymbol, rvam_act_InsertSymbolH, rvam_act_InsertNumber, rvam_act_InsertNumberH, 
	rvam_act_InsertItemCaption, rvam_act_InsertItemCaptionH, rvam_act_InsertTextBox, rvam_act_InsertTextBoxH, rvam_act_InsertSidenote, rvam_act_InsertSidenoteH, rvam_act_InsertPageNumber, rvam_act_InsertPageNumberH, rvam_act_ParaList, rvam_act_ParaListH, rvam_act_Bullets, rvam_act_BulletsH, rvam_act_Numbering, rvam_act_NumberingH, rvam_act_Color, rvam_act_ColorH, rvam_act_FillColor, rvam_act_FillColorH, rvam_act_InsertPageBreak, rvam_act_InsertPageBreakH, rvam_act_RemovePageBreak, rvam_act_RemovePageBreakH, rvam_act_ClearLeft, rvam_act_ClearLeftH, rvam_act_ClearRight, rvam_act_ClearRightH, rvam_act_ClearBoth, rvam_act_ClearBothH, rvam_act_ClearNone, rvam_act_ClearNoneH, rvam_act_VAlign, rvam_act_VAlignH, rvam_act_ItemProperties, rvam_act_ItemPropertiesH, 
	rvam_act_Background, rvam_act_BackgroundH, rvam_act_Paragraph, rvam_act_ParagraphH, rvam_act_IndentInc, rvam_act_IndentIncH, rvam_act_IndentDec, rvam_act_IndentDecH, rvam_act_WordWrap, rvam_act_WordWrapH, rvam_act_AlignLeft, rvam_act_AlignLeftH, rvam_act_AlignRight, rvam_act_AlignRightH, rvam_act_AlignCenter, rvam_act_AlignCenterH, rvam_act_AlignJustify, rvam_act_AlignJustifyH, rvam_act_ParaColor, rvam_act_ParaColorH, rvam_act_LS100, rvam_act_LS100H, rvam_act_LS150, rvam_act_LS150H, rvam_act_LS200, rvam_act_LS200H, rvam_act_ParaBorder, rvam_act_ParaBorderH, rvam_act_InsertTable, rvam_act_InsertTable2, rvam_act_InsertTableH, rvam_act_TableInsertRowsAbove, rvam_act_TableInsertRowsAboveH, rvam_act_TableInsertRowsBelow, rvam_act_TableInsertRowsBelowH, 
	rvam_act_TableInsertColsLeft, rvam_act_TableInsertColsLeftH, rvam_act_TableInsertColsRight, rvam_act_TableInsertColsRightH, rvam_act_TableDeleteRows, rvam_act_TableDeleteRowsH, rvam_act_TableDeleteCols, rvam_act_TableDeleteColsH, rvam_act_TableDeleteTable, rvam_act_TableDeleteTableH, rvam_act_TableMergeCells, rvam_act_TableMergeCellsH, rvam_act_TableSplitCells, rvam_act_TableSplitCellsH, rvam_act_TableSelectTable, rvam_act_TableSelectTableH, rvam_act_TableSelectRows, rvam_act_TableSelectRowsH, rvam_act_TableSelectCols, rvam_act_TableSelectColsH, rvam_act_TableSelectCell, rvam_act_TableSelectCellH, rvam_act_TableCellVAlignTop, rvam_act_TableCellVAlignTopH, rvam_act_TableCellVAlignMiddle, rvam_act_TableCellVAlignMiddleH, rvam_act_TableCellVAlignBottom, 
	rvam_act_TableCellVAlignBottomH, rvam_act_TableCellVAlignDefault, rvam_act_TableCellVAlignDefaultH, rvam_act_TableCellRotationNone, rvam_act_TableCellRotationNoneH, rvam_act_TableCellRotation90, rvam_act_TableCellRotation90H, rvam_act_TableCellRotation180, rvam_act_TableCellRotation180H, rvam_act_TableCellRotation270, rvam_act_TableCellRotation270H, rvam_act_TableProperties, rvam_act_TablePropertiesH, rvam_act_TableGrid, rvam_act_TableGridH, rvam_act_TableSplit, rvam_act_TableSplitH, rvam_act_TableToText, rvam_act_TableToTextH, rvam_act_TableSort, rvam_act_TableSortH, rvam_act_TableCellLeftBorder, rvam_act_TableCellLeftBorderH, rvam_act_TableCellRightBorder, rvam_act_TableCellRightBorderH, rvam_act_TableCellTopBorder, rvam_act_TableCellTopBorderH, 
	rvam_act_TableCellBottomBorder, rvam_act_TableCellBottomBorderH, rvam_act_TableCellAllBorders, rvam_act_TableCellAllBordersH, rvam_act_TableCellNoBorders, rvam_act_TableCellNoBordersH, rvam_act_Font, rvam_act_FontH, rvam_act_Bold, rvam_act_BoldH, rvam_act_Italic, rvam_act_ItalicH, rvam_act_Underline, rvam_act_UnderlineH, rvam_act_StrikeOut, rvam_act_StrikeOutH, rvam_act_FontGrow, rvam_act_FontGrowH, rvam_act_FontShrink, rvam_act_FontShrinkH, rvam_act_FontGrow1Pt, rvam_act_FontGrow1PtH, rvam_act_FontShrink1Pt, rvam_act_FontShrink1PtH, rvam_act_AllCaps, rvam_act_AllCapsH, rvam_act_Overline, rvam_act_OverlineH, rvam_act_TextColor, rvam_act_TextColorH, rvam_act_TextBackColor, rvam_act_TextBackColorH, rvam_act_Spell, rvam_act_SpellH, rvam_act_Thesaurus, 
	rvam_act_ThesaurusH, rvam_act_ParaLTR, rvam_act_ParaLTRH, rvam_act_ParaRTL, rvam_act_ParaRTLH, rvam_act_TextLTR, rvam_act_TextLTRH, rvam_act_TextRTL, rvam_act_TextRTLH, rvam_act_CharCase, rvam_act_CharCaseH, rvam_act_ShowSpecialCharacters, rvam_act_ShowSpecialCharactersH, rvam_act_Subscript, rvam_act_SubscriptH, rvam_act_Superscript, rvam_act_SuperscriptH, rvam_act_InsertFootnote, rvam_act_InsertFootnoteH, rvam_act_InsertEndnote, rvam_act_InsertEndnoteH, rvam_act_EditNote, rvam_act_EditNoteH, rvam_act_Hide, rvam_act_HideH, rvam_act_Styles, rvam_act_StylesH, rvam_act_AddStyle, rvam_act_AddStyleH, rvam_act_ClearFormat, rvam_act_ClearFormatH, rvam_act_ClearTextFormat, rvam_act_ClearTextFormatH, rvam_act_StyleInspector, rvam_act_CStyleInspectorH, rvam_btn_OK, 
	rvam_btn_Cancel, rvam_btn_Close, rvam_btn_Insert, rvam_btn_Open, rvam_btn_Save, rvam_btn_Clear, rvam_btn_Help, rvam_btn_Remove, rvam_Percents, rvam_LeftSide, rvam_TopSide, rvam_RightSide, rvam_BottomSide, rvam_SaveChanges, rvam_Confirm, rvam_LostFormat, rvam_RVF, rvam_err_Title, rvam_err_ErrorLoadingFile, rvam_err_ErrorLoadingImageFile, rvam_err_ErrorSavingFile, rvam_flt_RVF, rvam_flt_RTF, rvam_flt_XML, rvam_flt_TextAnsi, rvam_flt_TextUnicode, rvam_flt_TextAuto, rvam_flt_HTMLOpen, rvam_flt_HTMLCSS, rvam_flt_HTMLPlain, rvam_flt_DocX, rvam_src_Complete, rvam_src_NotFound, rvam_src_1Replaced, rvam_src_NReplaced, rvam_src_ContinueFromStart, rvam_src_ContinueFromEnd, rvam_cl_Transparent, rvam_cl_Auto, rvam_cl_Black, vam_cl_Brown, vam_cl_OliveGreen, 
	rvam_cl_DarkGreen, rvam_cl_DarkTeal, rvam_cl_DarkBlue, rvam_cl_Indigo, rvam_cl_Gray80, rvam_cl_DarkRed, rvam_cl_Orange, rvam_cl_DarkYellow, rvam_cl_Green, rvam_cl_Teal, rvam_cl_Blue, rvam_cl_Blue_Gray, rvam_cl_Gray50, rvam_cl_Red, rvam_cl_LightOrange, rvam_cl_Lime, rvam_cl_SeaGreen, rvam_cl_Aqua, rvam_cl_LightBlue, rvam_cl_Violet, rvam_cl_Grey, rvam_cl_Pink, rvam_cl_Gold, rvam_cl_Yellow, rvam_cl_BrightGreen, rvam_cl_Turquoise, rvam_cl_SkyBlue, rvam_cl_Plum, rvam_cl_Gray25, rvam_cl_Rose, rvam_cl_Tan, rvam_cl_LightYellow, rvam_cl_LightGreen, rvam_cl_LightTurquoise, rvam_cl_PaleBlue, rvam_cl_Lavender, rvam_cl_White, rvam_cpcl_Transparent, rvam_cpcl_Auto, rvam_cpcl_More, rvam_cpcl_Default, rvam_back_Title, rvam_back_Color, rvam_back_Position, 
	rvam_back_Background, rvam_back_SampleText, rvam_back_None, rvam_back_FullWindow, rvam_back_FixedTiles, rvam_back_Tiles, rvam_back_Center, rvam_back_Padding, rvam_fillc_Title, rvam_fillc_ApplyTo, rvam_fillc_MoreColors, rvam_fillc_Padding, rvam_fillc_PleaseSelect, rvam_fillc_Text, rvam_fillc_Paragraph, rvam_fillc_Table, rvam_fillc_Cell, rvam_font_Title, rvam_font_FontTab, rvam_font_LayoutTab, rvam_font_FontName, rvam_font_FontSize, rvam_font_FontStyle, rvam_font_Bold, rvam_font_Italic, rvam_font_Script, rvam_font_Color, rvam_font_BackColor, rvam_font_DefaultCharset, rvam_font_Effects, rvam_font_Underline, rvam_font_Overline, rvam_font_Strikethrough, rvam_font_AllCaps, rvam_font_Sample, rvam_font_SampleText, rvam_font_SpacingH, rvam_font_Spacing, 
	rvam_font_Expanded, rvam_font_Condensed, rvam_font_OffsetH, rvam_font_Offset, rvam_font_Down, rvam_font_Up, rvam_font_ScalingH, rvam_font_Scaling, rvam_font_ScriptH, rvam_font_SSNorm, rvam_font_SSSub, rvam_font_SSSuper, rvam_4s_DefTitle, rvam_4s_Top, rvam_4s_Left, rvam_4s_Bottom, rvam_4s_Right, rvam_4s_EqualValues, rvam_hl_Title, rvam_hl_GBTitle, rvam_hl_Text, rvam_hl_Target, rvam_hl_Selection, rvam_hl_CannotNavigate, rvam_hl_HypProperties, rvam_hl_HypStyle, rvam_hp_Title, rvam_hp_GBNormal, rvam_hp_GBActive, rvam_hp_Text, rvam_hp_TextBack, rvam_hp_UnderlineColor, rvam_hp_HoverText, rvam_hp_HoverTextBack, rvam_hp_HoverUnderlineColor, rvam_hp_GBAttributes, rvam_hp_UnderlineType, rvam_hp_UAlways, rvam_hp_UNever, rvam_hp_UActive, rvam_hp_AsNormal, 
	rvam_hp_UnderlineActiveLinks, rvam_is_Title, rvam_is_Font, rvam_is_Charset, rvam_is_Unicode, rvam_is_Block, rvam_is_CharCode, rvam_is_UCharCode, rvam_is_NoChar, rvam_it_Title, rvam_it_TableSize, rvam_it_nCols, rvam_it_nRows, rvam_it_TableLayout, rvam_it_Autosize, rvam_it_Fit, rvam_it_Manual, rvam_it_Remember, rvam_va_Title, rvam_ip_Title, rvam_ip_ImageTab, rvam_ip_LayoutTab, rvam_ip_LineTab, rvam_ip_TableTab, rvam_ip_RowsTab, rvam_ip_CellsTab, rvam_ip_ImgAppTab, rvam_ip_ImgMiscTab, rvam_ip_SeqTab, rvam_ip_BoxPosTab, rvam_ip_BoxSizeTab, rvam_ip_BoxAppTab, rvam_ip_Preview, rvam_ip_Transparency, rvam_ip_Transparent, rvam_ip_TrColor, rvam_ip_Change, rvam_ip_ImgSave, rvam_ip_TrAutoColor, rvam_ip_VAlign, rvam_ip_VAlignValue, rvam_ip_VAlign1, rvam_ip_VAlign2, 
	rvam_ip_VAlign3, rvam_ip_VAlign4, rvam_ip_VAlign5, rvam_ip_VAlign6, rvam_ip_VAlign7, rvam_ip_ShiftBy, rvam_ip_Stretch, rvam_ip_Width, rvam_ip_Height, rvam_ip_DefaultSize, rvam_ip_ImgScaleProportionally, rvam_ip_ImgSizeReset, rvam_ip_ImgInsideBorderGB, rvam_ip_ImgBorderGB, rvam_ip_ImgOutsideBorderGB, rvam_ip_ImgFillColor, rvam_ip_ImgPadding, rvam_ip_ImgBorderWidth, rvam_ip_ImgBorderColor, rvam_ip_ImgHSpacing, rvam_ip_ImgVSpacing, rvam_ip_ImgMisc, rvam_ip_ImgHint, rvam_ip_Web, rvam_ip_Alt, rvam_ip_HorzLine, rvam_ip_HLColor, rvam_ip_HLWidth, rvam_ip_HLStyle, rvam_ip_TableGB, rvam_ip_TableWidth, rvam_ip_TableColor, rvam_ip_CellSpacing, rvam_ip_CellHPadding, rvam_ip_CellVPadding, rvam_ip_TableBorderGB, rvam_ip_TableVisibleBorders, rvam_ip_TableAutoSizeLbl, 
	rvam_ip_TableAutoSize, rvam_ip_RowKeepTogether, rvam_ip_CellRotationGB, rvam_ip_CellRotation0, rvam_ip_CellRotation90, rvam_ip_CellRotation180, rvam_ip_CellRotation270, rvam_ip_CellBorderLbl, rvam_ip_CellVisibleBorders, rvam_ip_TableBorder, rvam_ip_CellBorder, rvam_ip_TableBorderTitle, rvam_ip_CellBorderTitle, rvam_ip_TablePrinting, rvam_ip_KeepOnPage, rvam_ip_HeadingRows, rvam_ip_HeadingRowsTip, rvam_ip_VATop, rvam_ip_VACenter, rvam_ip_VABottom, rvam_ip_VADefault, rvam_ip_CellSettings, rvam_ip_CellBestWidth, rvam_ip_CellBestHeight, rvam_ip_CellFillColor, rvam_ip_CellBorderGB, rvam_ip_VisibleSides, rvam_ip_CellShadowColor, rvam_ip_CellLightColor, rvam_ip_CellBorderColor, rvam_ip_BackgroundImage, rvam_ip_BoxHPosGB, rvam_ip_BoxVPosGB, rvam_ip_BoxPosInTextGB, 
	rvam_ip_BoxPosAlign, rvam_ip_BoxPosAbsolute, rvam_ip_BoxPosRelative, rvam_ip_BoxHPosLeft, rvam_ip_BoxHPosCenter, rvam_ip_BoxHPosRight, rvam_ip_BoxVPosTop, rvam_ip_BoxVPosCenter, rvam_ip_BoxVPosBottom, rvam_ip_BoxPosRelativeTo, rvam_ip_BoxPosToTheRightOfLeftSideOf, rvam_ip_BoxPosBelowTheTopSideOf, rvam_ip_BoxAnchorPage, rvam_ip_BoxAnchorMainTextArea, rvam_ip_BoxAnchorPage2, rvam_ip_BoxAnchorMainTextArea2, rvam_ip_BoxAnchorLeftMargin, rvam_ip_BoxAnchorRightMargin, rvam_ip_BoxAnchorTopMargin, rvam_ip_BoxAnchorBottomMargin, rvam_ip_BoxAnchorInnerMargin, rvam_ip_BoxAnchorOuterMargin, rvam_ip_BoxAnchorCharacter, rvam_ip_BoxAnchorLine, rvam_ip_BoxAnchorPara, rvam_ip_BoxMirroredMargins, rvam_ip_BoxLayoutInTableCell, rvam_ip_BoxAboveText, rvam_ip_BoxBelowText, 
	rvam_ip_BoxWidthGB, rvam_ip_BoxHeightGB, rvam_ip_BoxWidth, rvam_ip_BoxHeight, rvam_ip_BoxHeightAutoLbl, rvam_ip_BoxHeightAuto, rvam_ip_BoxVAlign, rvam_ip_BoxVAlignTop, rvam_ip_BoxVAlignCenter, rvam_ip_BoxVAlignBottom, rvam_ip_BoxBorderAndBack, rvam_ip_BoxBorderAndBackTitle, rvam_ps_Title, rvam_ps_Label, rvam_ps_RTF, rvam_ps_HTML, rvam_ps_Text, rvam_ps_UnicodeText, rvam_ps_BMP, rvam_ps_WMF, rvam_ps_GraphicFiles, rvam_ps_URL, rvam_ps_Options, rvam_ps_Styles, rvam_ps_Styles1, rvam_ps_Styles2, rvam_ps_Styles3, rvam_lg_Title, rvam_lg_BulletTab, rvam_lg_NumTab, rvam_lg_BulletGB, rvam_lg_NumGB, rvam_lg_Customize, rvam_lg_Reset, rvam_lg_None, rvam_lg_NumContinue, rvam_lg_NumReset, rvam_lg_NumCreate, rvam_lg2_UpperAlpha, rvam_lg2_UpperRoman, rvam_lg2_Decimal, 
	rvam_lg2_LowerAlpha, rvam_lg2_LowerRoman, rvam_lg2_Circle, rvam_lg2_Disc, rvam_lg2_Square, rvam_lg2_Level, rvam_lg2_StartFrom, rvam_lg2_Continue, rvam_lg2_Numbering, rvam_cul_Title, rvam_cul_Levels, rvam_cul_LevelCount, rvam_cul_ListProperties, rvam_cul_ListType, rvam_cul_ListTypeBullet, rvam_cul_ListTypeImage, rvam_cul_InsertNumberHint, rvam_cul_NumberFormat, rvam_cul_Number, rvam_cul_StartFrom, rvam_cul_Font, rvam_cul_Image, rvam_cul_BulletCharacter, rvam_cul_Bullet, rvam_cul_ListTextPos, rvam_cul_BulletPos, rvam_cul_NumPos, rvam_cul_ImagePos, rvam_cul_TextPos, rvam_cul_At, rvam_cul_LeftIndent, rvam_cul_FirstIndent, rvam_cul_FromLeftIndent, rvam_cul_OneLevelPreview, rvam_cul_Preview, rvam_cul_PreviewText, rvam_cul_Left, rvam_cul_Right, rvam_cul_Center, 
	rvam_cul_LevelNo, rvam_cul_ThisLevel, rvam_cul_BulletTitle, rvam_pbb_Title, rvam_pbb_BorderTab, rvam_pbb_BackgroundTab, rvam_pbb_Settings, rvam_pbb_Color, rvam_pbb_Width, rvam_pbb_InternalWidth, rvam_pbb_Offsets, rvam_pbb_Sample, rvam_pbb_BorderType, rvam_pbb_BTNone, rvam_pbb_BTSingle, rvam_pbb_BTDouble, rvam_pbb_BTTriple, rvam_pbb_BTThickInside, rvam_pbb_BTThickOutside, rvam_pbb_FillColor, rvam_pbb_MoreColors, rvam_pbb_Padding, rvam_pbb_PreviewText, rvam_pbb_OffsetsTitle, rvam_pbb_OffsetsGB, rvam_par_Title, rvam_par_Alignment, rvam_par_AlLeft, rvam_par_AlRight, rvam_par_AlCenter, rvam_par_AlJustify, rvam_par_Spacing, rvam_par_Before, rvam_par_After, rvam_par_LineSpacing, rvam_par_By, rvam_par_Indents, rvam_par_Left, rvam_par_Right, rvam_par_FirstLine, 
	rvam_par_Indented, rvam_par_Hanging, rvam_par_Sample, rvam_par_LS_100, rvam_par_150, rvam_par_200, rvam_par_AtLeast, rvam_par_Exactly, rvam_par_Multiple, rvam_par_Preview, rvam_par_MainTab, rvam_par_TabsTab, rvam_par_TextFlowTab, rvam_par_TabStopPos, rvam_par_btnSet, rvam_par_Delete, rvam_par_DeleteAll, rvam_par_TabAlign, rvam_par_TabAlignLeft, rvam_par_TabAlignRight, rvam_par_TabAlignCenter, rvam_par_Leader, rvam_par_LeaderNone, rvam_par_TabsToBeDeleted, rvam_par_TabDelNone, rvam_par_TabDelAll, rvam_par_Pagination, rvam_par_KeepWithNext, rvam_par_KeepLinesTogether, rvam_par_OutlineLevel, rvam_par_OL_BodyText, rvam_par_OL_Level, rvam_pp_Title, rvam_pp_PageWidth, rvam_pp_FullPage, rvam_pp_Pages, rvam_pp_InvalidScale, rvam_pp_OfNo, rvam_pp_First, 
	rvam_pp_Prior, rvam_pp_Next, rvam_pp_Last, rvam_cs_Title, rvam_cs_GB, rvam_cs_BetweenCells, rvam_cs_FromTableToCell, rvam_cs_Vert1, rvam_cs_Horz1, rvam_cs_Vert2, rvam_cs_Horz2, rvam_tb_Title, rvam_tb_Settings, rvam_tb_Color, rvam_tb_LightColor, rvam_tb_ShadowColor, rvam_tb_Width, rvam_tb_BorderType, rvam_tb_BTNone, rvam_tb_BTSunken, rvam_tb_BTRaised, rvam_tb_BTFlat, rvam_spl_Title, rvam_spl_SplitTo, rvam_spl_Specified, rvam_spl_Original, rvam_spl_nCols, rvam_spl_nRows, rvam_spl_Merge, rvam_spl_OriginalCols, rvam_spl_OriginalRows, rvam_ar_Title, rvam_ar_Prompt, rvam_pg_Title, rvam_pg_PageTab, rvam_pg_HFTab, rvam_pg_MarginsMM, rvam_pg_MarginsInch, rvam_pg_Left, rvam_pg_Top, rvam_pg_Right, rvam_pg_Bottom, rvam_pg_MirrorMargins, rvam_pg_Orientation, 
	rvam_pg_Portrait, rvam_pg_Landscape, rvam_pg_Paper, rvam_pg_Size, rvam_pg_Source, rvam_pg_Header, rvam_pg_HText, rvam_pg_HOnFirstPage, rvam_pg_HFont, rvam_pg_HLeft, rvam_pg_HCenter, rvam_pg_HRight, rvam_pg_Footer, rvam_pg_FText, rvam_pg_FOnFirstPage, rvam_pg_FFont, rvam_pg_FLeft, rvam_pg_FCenter, rvam_pg_FRight, rvam_pg_PageNumbers, rvam_pg_StartFrom, rvam_pg_Codes, rvam_ccp_Title, rvam_ccp_Label, rvam_ccp_Thai, rvam_ccp_Japanese, rvam_ccp_ChineseSimplified, rvam_ccp_Korean, rvam_ccp_ChineseTraditional, rvam_ccp_CentralEuropean, rvam_ccp_Cyrillic, rvam_ccp_WestEuropean, rvam_ccp_Greek, rvam_ccp_Turkish, rvam_ccp_Hebrew, rvam_ccp_Arabic, rvam_ccp_Baltic, rvam_ccp_Vietnamese, rvam_ccp_UTF8, rvam_ccp_UTF16, rvam_cvs_Title, rvam_cvs_Label, rvam_cd_Title, 
	rvam_cd_Label, rvam_cd_LineBreak, rvam_cd_Tab, rvam_cd_Semicolon, rvam_cd_Comma, rvam_ts_Error, rvam_ts_Title, rvam_ts_MainOptionsGB, rvam_ts_SortByCol, rvam_ts_CaseSensitive, rvam_ts_ExcludeHeadingRow, rvam_ts_Rows, rvam_ts_OrderRG, rvam_ts_Ascending, rvam_ts_Descending, rvam_ts_DataType, rvam_ts_Text, rvam_ts_Number, rvam_in_Title, rvam_in_PropGB, rvam_in_CounterName, rvam_in_NumType, rvam_in_NumberingRG, rvam_in_Continue, rvam_in_StartFrom, rvam_ic_Title, rvam_in_Label, rvam_in_ExcludeLabel, rvam_in_Position, rvam_in_Above, rvam_in_Below, rvam_in_CaptionText, rvam_seq_Numbering, rvam_seq_Figure, rvam_seq_Table, rvam_vs_Title, rvam_vs_GB, rvam_vs_Left, rvam_vs_Top, rvam_vs_Right, rvam_vs_Bottom, rvam_ruler_ColumnMove, rvam_ruler_RowMove, 
	rvam_ruler_FirstIndent, rvam_ruler_LeftIndent, rvam_ruler_HangingIndent, rvam_ruler_RightIndent, rvam_ruler_listlevel_Dec, rvam_ruler_listlevel_Inc, rvam_ruler_MarginBottom, rvam_ruler_MarginLeft, rvam_ruler_MarginRight, rvam_ruler_MarginTop, rvam_ruler_tab_Left, rvam_ruler_tab_Right, rvam_ruler_tab_Center, rvam_ruler_tab_Decimal, rvam_ss_Normal, rvam_ss_NormalIndent, rvam_ss_NoSpacing, rvam_ss_HeadingN, rvam_ss_ListParagraph, rvam_ss_Hyperlink, rvam_ss_Title, rvam_ss_Subtitle, rvam_ss_Emphasis, rvam_ss_SubtleEmphasis, rvam_ss_IntenseEmphasis, rvam_ss_Strong, rvam_ss_Quote, rvam_ss_IntenseQuote, rvam_ss_SubtleReference, rvam_ss_IntenseReference, rvam_ss_BlockText, rvam_ss_HTMLVariable, rvam_ss_HTMLCode, rvam_ss_HTMLAcronym, rvam_ss_HTMLDefinition, 
	rvam_ss_HTMLKeyboard, rvam_ss_HTMLSample, rvam_ss_HTMLTypewriter, rvam_ss_HTMLPreformatted, rvam_ss_HTMLCite, rvam_ss_Header, rvam_ss_Footer, rvam_ss_PageNumber, rvam_ss_Caption, rvam_ss_EndnoteReference, rvam_ss_FootnoteReference, rvam_ss_EndnoteText, rvam_ss_FootnoteText, rvam_ss_SidenoteReference, rvam_ss_SidenoteText, rvam_sd_Color, rvam_sd_BackColor, rvam_sd_Transparent, rvam_sd_DefColor, rvam_sd_UnderlineColor, rvam_sd_DefBackColor, rvam_sd_DefTextColor, rvam_sd_ColorSameAsText, rvam_sd_ut_Single, rvam_sd_ut_Thick, rvam_sd_ut_Double, rvam_sd_ut_Dotted, rvam_sd_ut_ThickDotted, rvam_sd_ut_Dashed, rvam_sd_ut_ThickDashed, rvam_sd_ut_LongDashed, rvam_sd_ut_ThickLongDashed, rvam_sd_ut_DashDotted, rvam_sd_ut_ThickDashDotted, rvam_sd_ut_DashDotDotted, 
	rvam_sd_ut_ThickDashDotDotted, rvam_sd_sss_None, rvam_sd_sss_Sub, rvam_sd_sss_Super, rvam_sd_BiDi, rvam_sd_bidi_Undefined, rvam_sd_bidi_LTR, rvam_sd_RTL, rvam_sd_Bold, rvam_sd_NotBold, rvam_sd_Italic, rvam_sd_NotItalic, rvam_sd_Underlined, rvam_sd_NotUnderlined, rvam_sd_DefUnderline, rvam_sd_StruckOut, rvam_sd_NotStruckOut, rvam_sd_Overlined, rvam_sd_NotOverlined, rvam_sd_AllCapitals, rvam_sd_AllCapitalsOff, rvam_sd_VShiftNone, rvam_sd_VShiftUp, rvam_sd_VShiftDown, rvam_sd_CharScaleX, rvam_sd_CharSpacingNone, rvam_sd_CharSpacingExp, rvam_sd_CharSpacingCond, rvam_sd_Charset, rvam_sd_DefFont, rvam_sd_DefPara, rvam_sd_Inherited, rvam_sd_Highlight, rvam_sd_DefHyperlink, rvam_sd_al_Left, rvam_sd_al_Right, rvam_sd_al_Centered, rvam_sd_al_Justify, 
	rvam_sd_ls_Percent, rvam_sd_ls_Spacing, rvam_sd_ls_AtLeast, rvam_sd_ls_Exactly, rvam_sd_NoWrap, rvam_sd_Wrap, rvam_sd_KeepLinesTogether, rvam_sd_DoNotKeepLinesTogether, rvam_sd_KeepWithNext, rvam_sd_DoNotKeepWithNext, rvam_sd_ReadOnly, rvam_sd_NotReadOnly, rvam_sd_BodyText, rvam_sd_HeadingLevel, rvam_sd_FirstLineIndent, rvam_sd_LeftIndent, rvam_sd_RightIndent, rvam_sd_SpaceBefore, rvam_sd_SpaceAfter, rvam_sd_Tabs, rvam_sd_NoTabs, rvam_sd_TabAlign, rvam_sd_TabLeft, rvam_sd_TabRight, rvam_sd_TabCenter, rvam_sd_Leader, rvam_sd_No, rvam_sd_Yes, rvam_sd_Left, rvam_sd_Top, rvam_sd_Right, rvam_sd_Bottom, rvam_sd_brdr_None, rvam_sd_brdr_Single, rvam_sd_brdr_Double, rvam_sd_brdr_Triple, rvam_sd_brdr_ThickInside, rvam_sd_brdr_ThickOutside, rvam_sd_Background, 
	rvam_sd_Border, rvam_sd_Padding, rvam_sd_Spacing, rvam_sd_BorderStyle, rvam_sd_BorderWidth, rvam_sd_BorderIWidth, rvam_sd_VisibleSides, rvam_si_Title, rvam_si_Style, rvam_si_NoStyle, rvam_si_Para, rvam_si_Font, rvam_si_Attributes, rvam_si_BB, rvam_si_Hyperlink, rvam_si_StandardStyle, rvam_st_Title, rvam_st_GBStyle, rvam_st_Name, rvam_st_ApplicableTo, rvam_st_Parent, rvam_st_Parent2, rvam_st_Next, rvam_st_Next2, rvam_st_QuickAccess, rvam_st_Descr, rvam_st_ParaTextStyle, rvam_st_ParaStyle, rvam_st_TextStyle, rvam_st_ParaAndTextStyles, rvam_st_DefFont, rvam_st_Edit, rvam_st_Reset, rvam_st_Add, rvam_st_Delete, rvam_st_AddStandard, rvam_st_AddCustom, rvam_st_DefStyleName, rvam_st_ChooseStyle, rvam_st_Import, rvam_st_Export, rvam_sti_RVStylesFilter, 
	rvam_sti_LoadError, rvam_sti_NoStyles, rvam_sti_Title, rvam_sti_GB, rvam_sti_List, rvam_sti_RG, rvam_sti_Override, rvam_sti_Rename, rvam_sti_Select, rvam_sti_Unselect, rvam_sti_Invert, rvam_sti_SelectAll, rvam_sti_SelectNew, rvam_sti_SelectOld, rvam_scb_ClearFormat, rvam_scb_MoreStyles, rvam_scb_DlgTitle, rvam_scb_ChooseStyle, rvam_spell_Synonyms, rvam_spell_IgnoreAll, rvam_spell_AddToDictionary, rvam_msg_Downloading, rvam_msg_PrintStart, rvam_msg_Printing, rvam_msg_ConvertFromRTF, rvam_msg_ConvertToRTF, rvam_msg_ReadingFmt, rvam_msg_WritingFmt, rvam_msg_FmtText, rvam_note_None, rvam_note_Footnote, rvam_note_Endnote, rvam_note_Sidenote, rvam_note_TextBox };

typedef System::StaticArray<System::WideChar *, 1131> TRVAMessages;

typedef TRVAMessages *PRVAMessages;

typedef System::UnicodeString TRVALanguageName;

class DELPHICLASS TRVALanguageInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVALanguageInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString LanguageNameEnglish;
	System::UnicodeString LanguageNameNative;
	System::Uitypes::TFontCharset Charset;
	TRVAMessages *Messages;
	System::UnicodeString HelpFile;
	NativeInt Tag;
public:
	/* TObject.Create */ inline __fastcall TRVALanguageInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVALanguageInfo(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 RVAColorCount = System::Int8(0x28);
extern DELPHI_PACKAGE System::StaticArray<TRVAColorRecord, 40> RVADefColorsNames;
static const System::WideChar RVCHAR_ARABICCOMMA = (System::WideChar)(0x60c);
static const System::WideChar RVCHAR_ARMENIANCOMMA = (System::WideChar)(0x55d);
static const System::WideChar RVCHAR_FULLWIDTHCOMMA_GB2312 = (System::WideChar)(0xff0c);
static const System::WideChar RVCHAR_FULLWIDTHCOMMA_BIG5 = (System::WideChar)(0xff0c);
extern DELPHI_PACKAGE int RVADefaultLanguageIndex;
extern DELPHI_PACKAGE void __fastcall RVA_RegisterLanguage(const TRVALanguageName LanguageNameEnglish, const TRVALanguageName LanguageNameNative, System::Uitypes::TFontCharset Charset, PRVAMessages Messages, const System::UnicodeString HelpFile = System::UnicodeString());
extern DELPHI_PACKAGE System::Uitypes::TFontCharset __fastcall RVA_GetCharset(System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetS(TRVAMessageID MsgID, System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetSAsIs(TRVAMessageID MsgID, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetSH(TRVAMessageID MsgID, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetSHAsIs(TRVAMessageID MsgID, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetComma(System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetCommaAsIs(System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetNBSP(System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetSHUnits(TRVAMessageID MsgID, Rvstyle::TRVUnits Units, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetSHUnitsAsIs(TRVAMessageID MsgID, Rvstyle::TRVUnits Units, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::WideChar * __fastcall RVA_GetPC(TRVAMessageID MsgID, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVA_SwitchLanguage(const TRVALanguageName LanguageName, System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
extern DELPHI_PACKAGE TRVALanguageName __fastcall RVA_GetLanguageName(System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetUnitsNameAsIs(Rvstyle::TRVUnits Units, bool AfterNumber, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetUnitsName(Rvstyle::TRVUnits Units, bool AfterNumber, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVA_TranslateUnits(System::Classes::TStrings* SL, System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetHelpFile(System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE NativeInt __fastcall RVA_GetLanguageTag(System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall RVA_SetLanguageTag(const TRVALanguageName LanguageName, NativeInt Tag);
extern DELPHI_PACKAGE void __fastcall RVA_EnumLanguages(System::Classes::TGetStrProc Proc);
extern DELPHI_PACKAGE void __fastcall RVA_FillLanguageList(System::Classes::TStrings* sl, bool English = true, bool Native = false);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetPrintingMessage(int PageCompleted, Richview::TRVPrintingStep Step, System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetProgressMessage(Rvstyle::TRVLongOperation Operation, System::Classes::TComponent* ControlPanel = (System::Classes::TComponent*)(0x0));
}	/* namespace Rvalocalize */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVALOCALIZE)
using namespace Rvalocalize;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvalocalizeHPP
