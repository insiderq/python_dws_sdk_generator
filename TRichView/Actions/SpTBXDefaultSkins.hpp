﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'SpTBXDefaultSkins.pas' rev: 27.00 (Windows)

#ifndef SptbxdefaultskinsHPP
#define SptbxdefaultskinsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <SpTBXSkins.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Sptbxdefaultskins
{
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<System::Uitypes::TColor, 4> TSpTBXAluminumColors;

class DELPHICLASS TSpTBXAluminumSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXAluminumSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
private:
	void __fastcall SetDefaultColorScheme(const Sptbxskins::TSpTBXLunaScheme Value);
	
protected:
	TSpTBXAluminumColors FColors;
	Sptbxskins::TSpTBXLunaScheme FDefaultColorScheme;
	System::Uitypes::TColor FLightMetalColor;
	System::Uitypes::TColor FDarkMetalColor;
	int FRoughness;
	virtual void __fastcall FillColors(void);
	virtual System::Uitypes::TColor __fastcall GetBrushMetalColor(Sptbxskins::TSpTBXSkinComponentsType Component, Sptbxskins::TSpTBXSkinStatesType State);
	
public:
	__fastcall virtual TSpTBXAluminumSkin(void);
	__fastcall virtual ~TSpTBXAluminumSkin(void);
	virtual void __fastcall FillOptions(void);
	virtual void __fastcall PaintBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, Sptbxskins::TSpTBXSkinComponentsType Component, Sptbxskins::TSpTBXSkinStatesType State, bool Background, bool Borders, bool Vertical = false, System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() );
	__property System::Uitypes::TColor LightMetalColor = {read=FLightMetalColor, write=FLightMetalColor, nodefault};
	__property System::Uitypes::TColor DarkMetalColor = {read=FDarkMetalColor, write=FDarkMetalColor, nodefault};
	__property Sptbxskins::TSpTBXLunaScheme DefaultColorScheme = {read=FDefaultColorScheme, write=SetDefaultColorScheme, default=3};
};

#pragma pack(pop)

class DELPHICLASS TSpTBXAthenSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXAthenSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXAthenSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXAthenSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXDreamSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXDreamSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXDreamSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXDreamSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXEosSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXEosSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXEosSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXEosSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXHumanSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXHumanSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
	virtual void __fastcall PaintBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, Sptbxskins::TSpTBXSkinComponentsType Component, Sptbxskins::TSpTBXSkinStatesType State, bool Background, bool Borders, bool Vertical = false, System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() );
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXHumanSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXHumanSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXLeopardSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXLeopardSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
	virtual void __fastcall PaintBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, Sptbxskins::TSpTBXSkinComponentsType Component, Sptbxskins::TSpTBXSkinStatesType State, bool Background, bool Borders, bool Vertical = false, System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() );
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXLeopardSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXLeopardSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXXitoSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXXitoSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXXitoSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXXitoSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXOfficeXPSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXOfficeXPSkin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
public:
	virtual void __fastcall FillOptions(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXOfficeXPSkin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXOfficeXPSkin(void) { }
	
};

#pragma pack(pop)

typedef System::StaticArray<System::Uitypes::TColor, 23> TSpTBXOffice2003Colors;

class DELPHICLASS TSpTBXOffice2003Skin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXOffice2003Skin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
private:
	void __fastcall SetDefaultColorScheme(const Sptbxskins::TSpTBXLunaScheme Value);
	
protected:
	TSpTBXOffice2003Colors FColors;
	Sptbxskins::TSpTBXLunaScheme FDefaultColorScheme;
	virtual void __fastcall FillColors(void);
	
public:
	__fastcall virtual TSpTBXOffice2003Skin(void);
	virtual void __fastcall FillOptions(void);
	__property Sptbxskins::TSpTBXLunaScheme DefaultColorScheme = {read=FDefaultColorScheme, write=SetDefaultColorScheme, default=3};
public:
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXOffice2003Skin(void) { }
	
};

#pragma pack(pop)

typedef System::StaticArray<System::Uitypes::TColor, 18> TSpTBXOffice2007Colors;

class DELPHICLASS TSpTBXOffice2007Skin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXOffice2007Skin : public Sptbxskins::TSpTBXSkinOptions
{
	typedef Sptbxskins::TSpTBXSkinOptions inherited;
	
protected:
	TSpTBXOffice2007Colors FColors;
	virtual void __fastcall FillColors(void) = 0 ;
	
public:
	virtual void __fastcall FillOptions(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXOffice2007Skin(void) : Sptbxskins::TSpTBXSkinOptions() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXOffice2007Skin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXOffice2007BlueSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXOffice2007BlueSkin : public TSpTBXOffice2007Skin
{
	typedef TSpTBXOffice2007Skin inherited;
	
protected:
	virtual void __fastcall FillColors(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXOffice2007BlueSkin(void) : TSpTBXOffice2007Skin() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXOffice2007BlueSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXOffice2007BlackSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXOffice2007BlackSkin : public TSpTBXOffice2007Skin
{
	typedef TSpTBXOffice2007Skin inherited;
	
protected:
	virtual void __fastcall FillColors(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXOffice2007BlackSkin(void) : TSpTBXOffice2007Skin() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXOffice2007BlackSkin(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXOffice2007SilverSkin;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXOffice2007SilverSkin : public TSpTBXOffice2007Skin
{
	typedef TSpTBXOffice2007Skin inherited;
	
protected:
	virtual void __fastcall FillColors(void);
public:
	/* TSpTBXSkinOptions.Create */ inline __fastcall virtual TSpTBXOffice2007SilverSkin(void) : TSpTBXOffice2007Skin() { }
	/* TSpTBXSkinOptions.Destroy */ inline __fastcall virtual ~TSpTBXOffice2007SilverSkin(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Sptbxdefaultskins */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SPTBXDEFAULTSKINS)
using namespace Sptbxdefaultskins;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SptbxdefaultskinsHPP
