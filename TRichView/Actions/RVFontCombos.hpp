﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVFontCombos.pas' rev: 27.00 (Windows)

#ifndef RvfontcombosHPP
#define RvfontcombosHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvfontcombos
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TCustomRVFontComboBox;
class PASCALIMPLEMENTATION TCustomRVFontComboBox : public Vcl::Stdctrls::TComboBox
{
	typedef Vcl::Stdctrls::TComboBox inherited;
	
private:
	Rvscroll::TCustomRVControl* FEditor;
	Rvedit::TCustomRichViewEdit* FRichViewEdit;
	Richviewactions::TrvActionFontEx* FActionFont;
	bool FUpdating;
	bool FNoFocus;
	void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* Value);
	void __fastcall SetEditor(Rvscroll::TCustomRVControl* Value);
	void __fastcall SetActionFont(Richviewactions::TrvActionFontEx* Value);
	void __fastcall DoCurTextStyleChanged(System::TObject* Sender);
	void __fastcall DoSRVActiveEditorChanged(System::TObject* Sender);
	
protected:
	DYNAMIC void __fastcall DoExit(void);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC void __fastcall UpdateSelection(void) = 0 ;
	void __fastcall FocusEditor(void);
	__property Rvedit::TCustomRichViewEdit* RichViewEdit = {read=FRichViewEdit, write=SetRichViewEdit};
	
public:
	__fastcall virtual ~TCustomRVFontComboBox(void);
	
__published:
	__property Rvscroll::TCustomRVControl* Editor = {read=FEditor, write=SetEditor};
	__property Richviewactions::TrvActionFontEx* ActionFont = {read=FActionFont, write=SetActionFont};
public:
	/* TCustomComboBox.Create */ inline __fastcall virtual TCustomRVFontComboBox(System::Classes::TComponent* AOwner) : Vcl::Stdctrls::TComboBox(AOwner) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRVFontComboBox(HWND ParentWindow) : Vcl::Stdctrls::TComboBox(ParentWindow) { }
	
};


class DELPHICLASS TRVFontSizeComboBox;
class PASCALIMPLEMENTATION TRVFontSizeComboBox : public TCustomRVFontComboBox
{
	typedef TCustomRVFontComboBox inherited;
	
private:
	int PixelsPerInch;
	System::Uitypes::TFontName FFontName;
	void __fastcall SetFontName(const System::Uitypes::TFontName Value);
	void __fastcall Build(void);
	
protected:
	bool __fastcall IsValidChar(System::WideChar Key);
	DYNAMIC void __fastcall KeyPress(System::WideChar &Key);
	DYNAMIC void __fastcall UpdateSelection(void);
	
public:
	__fastcall virtual TRVFontSizeComboBox(System::Classes::TComponent* AOwner);
	DYNAMIC void __fastcall Click(void);
	__property System::Uitypes::TFontName FontName = {read=FFontName, write=SetFontName};
	
__published:
	__property AutoComplete = {default=0};
public:
	/* TCustomRVFontComboBox.Destroy */ inline __fastcall virtual ~TRVFontSizeComboBox(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVFontSizeComboBox(HWND ParentWindow) : TCustomRVFontComboBox(ParentWindow) { }
	
};


class DELPHICLASS TRVFontCharsetComboBox;
class PASCALIMPLEMENTATION TRVFontCharsetComboBox : public TCustomRVFontComboBox
{
	typedef TCustomRVFontComboBox inherited;
	
private:
	System::UnicodeString FFontName;
	bool FAddDefaultCharset;
	System::UnicodeString FDefaultCharsetCaption;
	void __fastcall SetFontName(const System::UnicodeString Value);
	void __fastcall Build(void);
	System::Uitypes::TFontCharset __fastcall GetCharsets(int Index);
	void __fastcall SetAddDefaultCharset(const bool Value);
	
protected:
	DYNAMIC void __fastcall UpdateSelection(void);
	
public:
	__fastcall virtual TRVFontCharsetComboBox(System::Classes::TComponent* AOwner);
	int __fastcall IndexOfCharset(System::Uitypes::TFontCharset Charset);
	DYNAMIC void __fastcall Click(void);
	__property System::UnicodeString FontName = {read=FFontName, write=SetFontName};
	__property System::Uitypes::TFontCharset Charsets[int Index] = {read=GetCharsets};
	
__published:
	__property bool AddDefaultCharset = {read=FAddDefaultCharset, write=SetAddDefaultCharset, nodefault};
	__property System::UnicodeString DefaultCharsetCaption = {read=FDefaultCharsetCaption, write=FDefaultCharsetCaption};
public:
	/* TCustomRVFontComboBox.Destroy */ inline __fastcall virtual ~TRVFontCharsetComboBox(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVFontCharsetComboBox(HWND ParentWindow) : TCustomRVFontComboBox(ParentWindow) { }
	
};


class DELPHICLASS TRVFontComboBox;
class PASCALIMPLEMENTATION TRVFontComboBox : public TCustomRVFontComboBox
{
	typedef TCustomRVFontComboBox inherited;
	
protected:
	virtual void __fastcall CreateWnd(void);
	DYNAMIC void __fastcall UpdateSelection(void);
	
public:
	__fastcall virtual TRVFontComboBox(System::Classes::TComponent* AOwner);
	DYNAMIC void __fastcall Click(void);
	
__published:
	__property Items = {stored=false};
	__property AutoComplete = {default=0};
public:
	/* TCustomRVFontComboBox.Destroy */ inline __fastcall virtual ~TRVFontComboBox(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVFontComboBox(HWND ParentWindow) : TCustomRVFontComboBox(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvfontcombos */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVFONTCOMBOS)
using namespace Rvfontcombos;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvfontcombosHPP
