//---------------------------------------------------------------------------

#include <basepch.h>
#pragma hdrstop
USEFORMNS("TableBrdrRVFrm.pas", Tablebrdrrvfrm, frmRVTableBrdr);
USEFORMNS("TableSizeRVFrm.pas", Tablesizervfrm, frmRVTableSize);
USEFORMNS("SplitRVFrm.pas", Splitrvfrm, frmRVSplit);
USEFORMNS("StyleImportRVFrm.pas", Styleimportrvfrm, frmRVStyleImport);
USEFORMNS("TableBackRVFrm.pas", Tablebackrvfrm, frmRVTableBack);
USEFORMNS("VisibleSidesRVFrm.pas", Visiblesidesrvfrm, frmRVVisibleSides);
USEFORMNS("SpacingRVFrm.pas", Spacingrvfrm, frmRVSpacing);
USEFORMNS("ListGallery2RVFrm.pas", Listgallery2rvfrm, frmRVListGallery2);
USEFORMNS("ListGalleryRVFrm.pas", Listgalleryrvfrm, frmRVListGallery);
USEFORMNS("ListRVFrm.pas", Listrvfrm, frmRVList);
USEFORMNS("IntegerRVFrm.pas", Integerrvfrm, frmInteger);
USEFORMNS("ItemPropRVFrm.pas", Itemproprvfrm, frmRVItemProp);
USEFORMNS("PageSetupRVFrm.pas", Pagesetuprvfrm, frmRVPageSetup);
USEFORMNS("ParaBrdrRVFrm.pas", Parabrdrrvfrm, frmRVParaBrdr);
USEFORMNS("ParaListRVFrm.pas", Paralistrvfrm, frmRVParaList);
USEFORMNS("NumSeqCustomRVFrm.pas", Numseqcustomrvfrm, frmRVCustomNumSeq);
USEFORMNS("NumSeqRVFrm.pas", Numseqrvfrm, frmRVNumSeq);
USEFORMNS("ObjectAlignRVFrm.pas", Objectalignrvfrm, frmRVObjectAlign);
USEFORMNS("ColorRVFrm.pas", Colorrvfrm, frmColor);
USEFORMNS("dmActions.pas", Dmactions, rvActionsResource); /* TDataModule: File Type */
USEFORMNS("BaseRVFrm.pas", Baservfrm, frmRVBase);
USEFORMNS("BorderSidesRVFrm.pas", Bordersidesrvfrm, frmRVBorderSides);
USEFORMNS("FillColorRVFrm.pas", Fillcolorrvfrm, frmRVFillColor);
USEFORMNS("HypRVFrm.pas", Hyprvfrm, frmRVHyp);
USEFORMNS("InsSymbolRVFrm.pas", Inssymbolrvfrm, frmRVInsertSymbol);
USEFORMNS("InsTableRVFrm.pas", Instablervfrm, frmInsTable);
USEFORMNS("FontRVFrm.pas", Fontrvfrm, frmRVFont);
USEFORMNS("FourSidesRVFrm.pas", Foursidesrvfrm, frmRVFourSides);
USEFORMNS("HypPropRVFrm.pas", Hypproprvfrm, frmRVHypProp);
USEFORMNS("ParaRVFrm.pas", Pararvfrm, frmRVPara);
USEFORMNS("PreviewRVFrm.pas", Previewrvfrm, frmRVPreview);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------


#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*)
{
	return 1;
}
//---------------------------------------------------------------------------
