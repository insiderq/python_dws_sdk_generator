﻿// This file is a copy of RVAL_Ukr.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Ukrainian translation                           }
{                                                       }
{       Translated by Gennady Ostapenko                 }
{       GennadyGO@ukr.net                               }
{                                                       }
{       Updated by: Michael Mostovoy                    }
{                   Artem Bogorodichenko                }
{                                                       }
{*******************************************************}
{ Updated: 2014-03-20                                   }
{ Created: 2003-11-12                                   }
{*******************************************************}

unit RVALUTF8_Ukr;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'дюйми', 'см', 'мм', 'піки', 'пікселі', 'пункти',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'см', 'мм', 'пік', 'пікс.', 'пт',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  '&Файл', '&Правка', 'Ф&ормат', '&Шрифт', '&Абзац', '&Вставка', '&Таблиця', 'В&ікно', '&Довідка',
  // exit
  'Ви&хід',
  // top-level menus: View, Tools,
  'Ви&гляд', 'С&ервіс',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Розмір', 'Стиль', 'Вид&ілити', 'Вирівнювання в &комірці', '&Рамка комірки',
  // menus: Table cell rotation
  'О&бертання комірки',
  // menus: Text flow, Footnotes/endnotes
  '&Обтікання', '&Виноски',
  // ribbon tabs: tab1, tab2, view, table
  '&Головна', 'Вставка та &фон', '&Вид', '&Таблиця',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Буфер обміну', 'Шрифт', 'Абзац', 'Список', 'Редагування',
  // ribbon groups: insert, background, page setup,
  'Вставка', 'Фон', 'Параметри сторінки',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Звя''зки', 'Виноски', 'Режими перегляду документа', 'Показати або приховати', 'Масштаб', 'Параметри',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Вставка', 'Видалення', 'Операції', 'Межі',
  // ribbon groups: styles
  'Стилі',
  // ribbon screen tip footer,
  'Натисніть F1 для довідки',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Останні документи', 'Зберегти копію документа', 'Попередній перегляд та друк',
  // ribbon label: units combo
  'Одиниці:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Створити', 'Створити|Створення нового документу',
  // TrvActionOpen
  '&Відкрити...', 'Відкрити|Відкриття збереженого документу',
  // TrvActionSave
  '&Зберегти', 'Зберегти|Збереження документу',
  // TrvActionSaveAs
  'Зберегти &як...', 'Зберегти як...|Збереження документу під іншою назвою, в іншому місці або форматі',
  // TrvActionExport
  '&Експорт...', 'Експорт|Експорт документу в файл з іншою назвою, в інше місце або в інший формат',
  // TrvActionPrintPreview
  'Попе&редній перегляд', 'Попередній перегляд|Перегляд документу в тому вигляді, в якому він буде надрукований',
  // TrvActionPrint
  '&Друк...', 'Друк|Задання параметрів друку і друк документу',
  // TrvActionQuickPrint
  '&Друк', '&Швидкий друк', 'Друк|Друк документу',
   // TrvActionPageSetup
  'П&араметри сторінки...', 'Параметри сторінки|Встановлення полів, розміру і джерела подачі паперу, колонтитулів',
  // TrvActionCut
  '&Вирізати', 'Вирізати|Вилучення виділеного фрагменту і перенесення його в буфер обміну',
  // TrvActionCopy
  '&Копіювати', 'Копіювати|Копіювання виділеного фрагменту в буфер обміну',
  // TrvActionPaste
  'Вст&авити', 'Вставити|Вставка фрагменту з буферу обміну в поточну позицію документу',
  // TrvActionPasteAsText
  'Вставити як &текст', 'Вставити як текст|Вставка текста з буферу обміну',  
  // TrvActionPasteSpecial
  'Сп&еціальна вставка...', 'Спеціальна вставка|Вставка змісту буфера обміну в документ у вибраному форматі',
  // TrvActionSelectAll
  'В&иділити все', 'Виділити все|Виділення всього документу',
  // TrvActionUndo
  'Від&мінити', 'Відмінити|Відміна останньої операції',
  // TrvActionRedo
  'Повернут&и', 'Повернути|Повторює останню відмінену дію',
  // TrvActionFind
  '&Пошук...', 'Пошук|Пошук вказаного тексту в документі',
  // TrvActionFindNext
  'Шукати &далі', 'Шукати далі|Продовження останнього пошуку тексту',
  // TrvActionReplace
  '&Замінити...', 'Замінити|Пошук та заміна вказаного тексту в документі',
  // TrvActionInsertFile
  '&Файл...', 'Вставити файл|Вставка файлу в поточну позицію документу',
  // TrvActionInsertPicture
  'Мал&юнок...', 'Вставити малюнок|Вставка малюнку в поточну позицію документу',
  // TRVActionInsertHLine
  '&Горизонтальна лінія', 'Вставити горизонтальну лінію|Вставка горизонтальної лінії в поточну позицію документу',
  // TRVActionInsertHyperlink
  'Посиланн&я...', 'Додати посилання|Додавання нового або редагування виділеного посилання',
  // TRVActionRemoveHyperlinks
  '&Видалити посилання', 'Видалити посилання|Видалення всіх посилань з виділеного тексту',  
  // TrvActionInsertSymbol
  '&Символ...', 'Вставити символ|Вставка символу або спеціального знаку',
  // TrvActionInsertNumber
  'Но&мер...', 'Додати номер|Додавання номера',
  // TrvActionInsertCaption
  '&Додати назву...', 'Додати назву|Додавання назви обраного об''єкту',
  // TrvActionInsertTextBox
  '&Текстове поле', 'Додати текстове поле|Додавання текстового поля в поточну позицію документа',
  // TrvActionInsertSidenote
  'Виноска в р&амці', 'Додати виноску в рамці|Додавання виноски, яка відображатиметься в текстовому полі, в поточну позицію документа',
  // TrvActionInsertPageNumber
  'Номер ст&орінки', 'Додати номер сторінки|Додавання номера сторінки в поточну позицію документа',
  // TrvActionParaList
  '&Список...', 'Список|Додавання або зміна формату маркерів або нумерації виділених абзаців',
  // TrvActionParaBullets
  '&Маркери', 'Маркери|Додавання або вилучення маркування виділених абзаців',
  // TrvActionParaNumbering
  '&Нумерація', 'Нумерація|Додавання або вилучення нумерації виділених абзаців',
  // TrvActionColor
  '&Колір фону...', 'Колір фону|Зміна кольору фону документу',
  // TrvActionFillColor
  '&Заливка...', 'Заливка|Зміна кольору фону тексту, абзацу, таблиці або комірки',
  // TrvActionInsertPageBreak
  '&Розрив сторінки', 'Розрив сторінки|Додавання розриву сторінки в поточну позицію документу.',
  // TrvActionRemovePageBreak
  '&Вилучити розрив сторінки', 'Вилучити розрив сторінки|Вилучення розриву сторінки',
  // TrvActionClearLeft
  'Не обтікати &лівий бік', 'Не обтікати лівий бік|Розміщення абзацу під об''єктами, що вирівнені по лівому краю',
  // TrvActionClearRight
  'Не обтікати &правий бік', 'Не обтікати правий бік|Розміщення абзацу під об''єктами, що вирівнені по правому краю',
  // TrvActionClearBoth
  'Не обтікати &обидва боки', 'Не обтікати обидва боки|Розміщення абзацу під об''єктами, що вирівнені по краях',
  // TrvActionClearNone
  'О&бтікати обидва боки', 'Обтікати обидва боки|Дозвіл абзацу обтікати об''єкти, що вирівняні по краях',
  // TrvActionVAlign
  '&Положення об''єкту...', 'Положення об''єкту|Задавання положення вибраного об''єкту в тексті',
  // TrvActionItemProperties
  'Властивості &об''єкту...', 'Властивості об''єкту|Визначення властивостей виділеного малюнку, лінії або таблиці',
  // TrvActionBackground
  '&Фон...', 'Фон|Вибір фонового малюнку та заливки',
  // TrvActionParagraph
  '&Абзац...', 'Абзац|Зміна атрибутів виділених абзаців',
  // TrvActionIndentInc
  'З&більшити відступ', 'Збільшити відступ|Збільшення лівого відступу виділених абзаців',
  // TrvActionIndentDec
  'З&меншити відступ', 'Зменшити відступ|Зменшення лівого відступу виділених абзаців',
  // TrvActionWordWrap
  'П&еренос по словам', 'Перенос по словам|Дозвіл або заборона автоматичного переводу рядків всередині виділених абзаців',
  // TrvActionAlignLeft
  'По &лівому краю', 'По лівому краю|Вирівнювання виділеного тексту по лівому краю',
  // TrvActionAlignRight
  'По &правому краю', 'По правому краю|Вирівнювання виділеного тексту по правому краю',
  // TrvActionAlignCenter
  'По &центру', 'По центру|Вирівнювання виділеного тексту по центру',
  // TrvActionAlignJustify
  'По &ширині', 'По ширині|Вирівнювання виділеного тексту по лівому й правому краям',
  // TrvActionParaColor
  'Колір заливки а&бзацу...', 'Колір заливки абзацу|Вибір кольору заливки абзацу',
  // TrvActionLineSpacing100
  '&Одинарний інтервал', 'Одинарний інтервал|Встановлення одинарного інтервалу між рядками абзацу',
  // TrvActionLineSpacing150
  'Пол&уторний інтервал', 'Полуторний інтервал|Встановлення полуторного интервалу между рядками абзацу',
  // TrvActionLineSpacing200
  'По&двійний інтервал', 'Подвійний інтервал|Встановлення подвійного інтервалу між рядками абзацу',
  // TrvActionParaBorder
  '&Рамка і заливка абзацу...', 'Рамка і заливка абзацу|Встановлення рамки, заливки і полей виділених абзаців',
  // TrvActionInsertTable
  'Вставити &таблицю...', '&Таблиця', 'Вставити таблицю|Вставка таблиці в поточну позицію документу',
  // TrvActionTableInsertRowsAbove
  'Додати рядок &вище', 'Додати рядок вище|Додання нового рядка над виділеними комірками',
  // TrvActionTableInsertRowsBelow
  'Додати рядок &нижче', 'Додати рядок нижче|Додання нового рядка під виділеними комірками',
  // TrvActionTableInsertColLeft
  'Додати колонку з&ліва', 'Додати колонку зліва|Додання нової колонки зліва від виділених комірок',
  // TrvActionTableInsertColRight
  'Додати колонку с&права', 'Додати колонку справа|Додання нової колонки справа від виділених комірок',
  // TrvActionTableDeleteRows
  'Вилучити &рядки', 'Вилучити рядки|Вилучення рядків з виділеними комірками',
  // TrvActionTableDeleteCols
  'Вилучити к&олонки', 'вилучити колонки|Вилучення колонок з виділеними комірками',
  // TrvActionTableDeleteTable
  'Вил&учити таблицю', 'Вилучити таблицю|Вилучення таблиці',
  // TrvActionTableMergeCells
  'О&б''єднати комірки', 'Об''єднати комірки|Об''єднання виділених комірок в одну',
  // TrvActionTableSplitCells
  'По&ділити комірки...', 'Поділити комірки|Поділ виділених комірок на задану кількість рядків і колонок',
  // TrvActionTableSelectTable
  'Вид&ілити таблицю', 'Виділити таблицю|Виділення цілої таблиці',
  // TrvActionTableSelectRows
  'Виділити ряд&ки', 'Виділити рядки|Виділення рядків, що містять виділені комірки',
  // TrvActionTableSelectCols
  'Виділити коло&нки', 'Виділити колонки|Виділення колонок, що містять виділені комірки',
  // TrvActionTableSelectCell
  'Виділити ком&ірку', 'Виділити комірку|Виділення комірки',
  // TrvActionTableCellVAlignTop
  'Вирівняти по &верхньому краю', 'Вирівняти по верхньому краю|Вирівнювання змісту комірки по верхньому краю',
  // TrvActionTableCellVAlignMiddle
  '&Центрувати по вертикалі', 'Центрувати по вертикалі|Вирівнювання змісту комірки по центру',
  // TrvActionTableCellVAlignBottom
  'Вирівняти по &нижньому краю', 'Вирівняти по нижньому краю|Вирівнювання змісту комірки по нижньому краю',
  // TrvActionTableCellVAlignDefault
  'Вирівнювання за замовч&уванням', 'Вирівнювання за замовчуванням|Встановити вертикальне вирівнювання за замовчуванням',
  // TrvActionTableCellRotationNone
  '&Без обертання', 'Без обертання|Не обертати вміст комірки',
  // TrvActionTableCellRotation90
  'Обертати комірку на &90°', 'Обертати комірку на 90°|Обертати вміст комірки на 90°',
  // TrvActionTableCellRotation180
  'Обертати комірку на &180°', 'Обертати комірку на 180°|Обертати вміст комірки на 180°',
  // TrvActionTableCellRotation270
  'Обертати комірку на &270°', 'Обертати комірку на 270°|Обертати вміст комірки на 270°',
  // TrvActionTableProperties
  'Властивості &таблиці...', 'Властивості таблиці|Задання властивостей таблиці',
  // TrvActionTableGrid
  'С&ітка', 'Сітка|Показ або приховання сітки на місці невидимих рамок у всіх таблицях',
  // TRVActionTableSplit
  '&Розділити таблицю', 'Розділити таблицю|Розділяє таблицю на дві таблиці, починаючи з обраного рядку',
  // TRVActionTableToText
  'Перетворити в &текст...', 'Перетворити в текст|Перетворює таблицю у текст',
  // TRVActionTableSort
  '&Сортування...', 'Сортування|Сортує рядки таблиці',
  // TrvActionTableCellLeftBorder
  '&Ліва границя', 'Ліва границі|Показ або приховання лівої границі виділених комірок',
  // TrvActionTableCellRightBorder
  '&Права границя', 'Права границя|Показ або приховання правої границі виділених комірок',
  // TrvActionTableCellTopBorder
  '&Верхня границя', 'Верхня границя|Показ або приховання верхньої границі виділених комірок',
  // TrvActionTableCellBottomBorder
  '&Нижня границя', 'Нижня границя|Показ або приховання нижньої границі виділених комірок',
  // TrvActionTableCellAllBorders
  'В&сі границі', 'Всі границі|Показ або приховання всіх границь виділених комірок',
  // TrvActionTableCellNoBorders
  '&Немає границь', 'Немає границь|Приховання всіх границь виділених комірок',
  // TrvActionFonts & TrvActionFontEx
  '&Шрифт...', 'Шрифт|Зміна параметрів шрифту і відстані між символами у виділеному тексті',
  // TrvActionFontBold
  'Напів&жирний', 'Напівжирний|Оформлення виділеного тексту напівжирним шрифтом',
  // TrvActionFontItalic
  '&Курсив', 'Курсив|Оформлення виділеного тексту курсивом',
  // TrvActionFontUnderline
  'Підк&реслений', 'Підкреслений|Підкреслення виділеного тексту',
  // TrvActionFontStrikeout
  '&Закреслений', 'Закреслений|Закреслення виділеного тексту',
  // TrvActionFontGrow
  'З&більшити розмір', 'Збільшити размір|Збільшення розміру шрифту виділеного тексту на 10%',
  // TrvActionFontShrink
  'З&меншити розмір', 'Зменшити размір|Зменшення розміру шрифту виділеного тексту на 10%',
  // TrvActionFontGrowOnePoint
  'Збі&льшити розмір на 1 пт', 'Збільшити розмір на 1 пт|Збільшення розміру шрифту виділеного тексту на 1 пункт',
  // TrvActionFontShrinkOnePoint
  'Зме&ншити розмір на 1 пт', 'Зменшити розмір на 1 пт|Зменшення розміру шрифту виділеного тексту на 1 пункт',
  // TrvActionFontAllCaps
  'Всі &великі', 'Всі великі|Показ всіх букв виділеного тексту як великих',
  // TrvActionFontOverline
  'Вер&хня риска', 'Верхня риска|Додання горизонтальної риски над виділеним текстом',
  // TrvActionFontColor
  '&Колір тексту...', 'Колір тексту|Зміна кольору виділеного тексту',
  // TrvActionFontBackColor
  'Колір з&аливки тексту...', 'Колір заливки тексту|Зміна кольору фону виділеного тексту',
  // TrvActionAddictSpell3
  '&Правопис', 'Правопис|Перевірка орфографії',
  // TrvActionAddictThesaurus3
  '&Тезаурус', 'Тезаурус|Пошук синонімів',
  // TrvActionParaLTR
  'Зліва направо', 'Зліва направо|Встановлення напряму тексту зліва направо для виділених абзаців',
  // TrvActionParaRTL
  'Справа наліво', 'Справа наліво|Встановлення напряму тексту справа наліво для виділених абзаців',
  // TrvActionLTR
  'Текст зліва направо', 'Текст зліва направо|Встановлення напряму виділеного тексту зліва направо',
  // TrvActionRTL
  'Текст справа наліво', 'Текст справа наліво|Встановлення напряму виділеного тексту справа наліво',
  // TrvActionCharCase
  '&Регістр', 'Регістр|Зміна регістру букв виділеного тексту',
  // TrvActionShowSpecialCharacters
  'Знаки, що &не друкуються', 'Знаки, що &не друкуються|Показати або приховати службові знаки, такі як символ кінця абзацу, пробіл або табуляція.',
  // TrvActionSubscript
  'Нижній індекс', 'Нижній індекс|Перетворює виділений текст на нижні індекси',
  // TrvActionSuperscript
  'Верхній індекс', 'Верхній індекс|Перетворює виділений текст на верхні індекси',
  // TrvActionInsertFootnote
  '&Виноска', 'Виноска|Додавання виноски в документ',
  // TrvActionInsertEndnote
  '&Кінцева виноска', 'Кінцева виноска|Додавання кінцевої виноски в документ',
  // TrvActionEditNote
  '&Редагувати виноску', 'Редагувати виноску|Редагування тексту поточної виноски',
  // TrvActionHide
  '&Приховати', 'Приховати|Приховати або показати виділений фрагмент документу',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Стилі...', 'Стилі|Відкриття вікна редагування стилів',
  // TrvActionAddStyleTemplate
  '&Додати стиль...', 'Додати стиль|Створення новогу стилю на підставі обраного тексту',
  // TrvActionClearFormat,
  '&Очистити формат', 'Очистити формат|Видалити усі форматування обраного фрагменту',
  // TrvActionClearTextFormat,
  'Очистити формат &тексту', 'Очистити формат тексту|Видалити форматування тексту обраного фрагменту',
  // TrvActionStyleInspector
  '&Інспектор стилів', 'Інспектор стилів|Показати або приховати вікно «Інспектора стилів»',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'ОК', 'Відміна', 'Закрити', 'Вставити', '&Вибрати...', '&Зберегти...', '&Очистити', 'Довідка', 'Видалити',
  // Others  -------------------------------------------------------------------
  '%',
  // left, top, right, bottom sides
  'Ліва сторона', 'Верхня сторона', 'Права сторона', 'Нижня сторона',
  // save changes? confirm title
  'Зберегти зміни в %s?', 'Підтвердження',
  // warning: losing formatting
  '%s може містити форматування, яке буде втрачено при перетворенні в вибраний формат файлу.'#13+
  'Зберегти документ в цьому форматі?',
  // RVF format name
  'Формат RichView',
  // Error messages ------------------------------------------------------------
  'Помилка',
  'Помилка читання файлу.'#13#13'Можливі причини:'#13'- файл має формат, який не підтримується програмою;'#13+
  '- файл пошкоджений;'#13'- файл відкрито іншою програмою, що блокувала доступ до нього.',
  'Помилка читання файлу з зображенням.'#13#13'Можливі причини:'#13'- файл містить зображення в форматі, який не підтримується програмою;'#13+
  '- файл не містить зображення;'#13+
  '- файл пошкоджений;'#13'- файл відкрито іншою програмою, що блокувала доступ до нього.',
  'Помилка збереження файлу.'#13#13'Можливі причини:'#13'- не достатньо місця на диску;'#13+
  '- диск захищений від запису;'#13'- диск не вставлено в дисковод;'#13+
  '- файл відкрито іншою програмою, яка блокувала доступ до нього;'#13+
  '- носій информації пошкоджений.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Файли формату RichView (*.rvf)|*.rvf',  'Файли RTF (*.rtf)|*.rtf' , 'Файли XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Текстові файли (*.txt)|*.txt', 'Текстові файли - Юнікод (*.txt)|*.txt', 'Текстові файли - авто (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Файли HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - спрощений (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Документи Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Пошук завершено', 'Текст ''%s'' не знайдено.',
  // 1 string replaced; Several strings replaced
  'Здійснено замін: 1.', 'Здійснено замін: %d',
  // continue search from the beginning/end?
  'Досягнуто кінця документа, продовжити пошук з початку?',
  'Досягнуто початку документа, продовжити пошук з кінця?',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  'Без заливки', 'Авто',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Чорний', 'Коричневий', 'Оливковий', 'Темно-зелений',
  'Темно-сизий', 'Темно-синій', 'Індиго','Сірий-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Темно-червоний', 'Оранжевий', 'Темно-жовтий', 'Зелений',
  'Синьо-зелений', 'Синій', 'Сизий', 'Сірий-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Червоний', 'Світло-оранжевий', 'Трав''яний', 'Смарагдовий',
  'Темно-бірюзовий', 'Темно-блакитний', 'Фіолетовий', 'Сірий-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Ліловий', 'Золотавий', 'Жовтий', 'Яскраво-зелений',
  'Бірюзовий', 'Блакитний', 'Вишневий', 'Сірий-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Рожевий', 'Світло-коричневий', 'Світло-жовтий', 'Блідо-зелений',
  'Світло-бірюзовий', 'Блідо-блакитний', 'Бузковий', 'Білий',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Без з&аливки', '&Авто', '&Інші кольори...', '&Стандартний',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  'Фон', '&Колір:', 'Положення', 'Зображення', 'Приклад тексту.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Немає', '&Розгорнути', '&Фіксована мозаїка', '&Мозаїка', 'По ц&ентру',
  // Padding button
  '&Поля...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  'Заливка', '&Застосувати до:', '&Інший колір...', 'П&оля...',
  'Будь ласка, виберіть колір',
  // [apply to:] text, paragraph, table, cell
  'тексту', 'абзацу' , 'таблиці', 'комірки',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Шрифт', 'Шрифт', 'Інтервал',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Шрифт:', '&Розмір', '&Написання', 'Напів&жирний', '&Курсив',
  // Script, Color, Back color labels, Default charset
  'Набір &символів:', '&Колір:', '&Фон:', '(За замовчуванням)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Видозмінення', 'Підкреслений', '&Риска зверху', '&Закреслений', '&Всі великі',
  // Sample, Sample text
  'Приклад', 'Приклад тексту',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Інтервал', '&Інтервал:', '&Розріджений', '&Ущільнений',
  // Offset group-bix, Offset label, Down, Up,
  'Зміщення', '&Зміщення:', 'В&низ', '&Вгору',
  // Scaling group-box, Scaling
  'Масштабування', '&Масштаб:',
  // Sub/super script group box, Normal, Sub, Super
  'Верхній та нижній індекс', '&Звичайний', 'Нижній індекс', 'Верхній індекс',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  'Поля', 'З&верху:', 'З&ліва:', 'З&низу:', 'С&права:',
  // Equal values check-box
  '&Однакові значення',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Додавання посилання', 'Посилання', '&Текст:', '&URL:', '<<виділений фрагмент>>',
  // cannot open URL
  'Помилка відкриття "%s"',
  // hyperlink properties button, hyperlink style
  '&Атрибути...', '&Стиль:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) сolors
  'Атрибути посилання', 'Звичайні кольори', 'Кольори підсвітки',
  // Text [color], Background [color]
  '&Текст:', '&Фон',
  // Underline [color]
  'Під&креслення:',
  // Text [color], Background [color] (different hotkeys)
  'Т&екст:', 'Ф&он',
  // Underline [color], Attributes group-box,
  'Під&креслення:', 'Атрибути',
  // Underline type: always, never, active (below the mouse)
  '&Підкреслення:', 'завжди', 'ніколи', 'при наведенні',
  // Same as normal check-box
  'Т&акі ж, як звичайні',
  // underline active links check-box
  '&Підкреслювати при наведенні',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Символ', '&Шрифт:', '&Набір символів:', 'Юнікод',
  // Unicode block
  '&Блок:',  
  // Character Code, Character Unicode Code, No Character
  'Код символу: %d', 'Код символу: Юнікод %d', '(символ не вибрано)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows
  'Вставка таблиці', 'Розмір таблиці', '&Кількість колонок:', 'К&ількість рядків:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Ширина таблиці', '&Авто', 'По ширині &вікна', 'Вка&зана:',
  // Remember check-box
  'За замовчуванням для нових &таблиць',
  // VAlign Form ---------------------------------------------------------------
  'Положення',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Властивості', 'Малюнок', 'Розмір і положення', 'Лінія', 'Таблиця', 'Рядки', 'Комірки',
  // Image Appearance, Image Misc, Number tabs
  'Вигляд', 'Різне', 'Номер',
  // Box position, Box size, Box appearance tabs
  'Положення', 'Розмір', 'Вигляд',
  // Preview label, Transparency group-box, checkbox, label
  'Перегляд:', 'Прозорість', '&Прозорий', 'Прозорий &колір:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Вибрати...', '&Зберегти...', 'Авто',
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Вертикальне вирівнювання', 'Ви&рівняти:',
  'низ по базовій лінії тексту', 'центр по базовій лінії тексту',
  'по верхньому краю рядка', 'по нижньому краю рядка', 'по центру рядка',
  // align to left side, align to right side
  'по лівому краю', 'по правому краю',
  // Shift By label, Stretch group box, Width, Height, Default size
  'Змі&стити на:', 'Масштаб', 'По &ширині:', 'По &висоті:', 'Початковий розмір: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Зб&ерігати співвідношення сторін', 'С&кинути',
  // Inside the border, border, outside the border groupboxes
  'Усередині межі', 'Межа', 'Навколо межі',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Колір фону:', '&Поля:',
  // Border width, Border color labels
  '&Ширина рамки:', 'Колір &рамки:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Відступ по &горизонталі:', 'Відступ по &вертикалі:',
  // Miscellaneous groupbox, Tooltip label
  'Різне', '&Підказка:',
  // web group-box, alt text
  'Веб', '&Текст замісник:',
  // Horz line group-box, color, width, style
  'Горизонтальна лінія', '&Колір:', '&Товщина:', '&Стиль:',
  // Table group-box; Table Width, Color, CellSpacing
  'Таблиця', '&Ширина:', '&Колір заливки:', '&Інтервали між комірками...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Горизонтальні поля комірок:', '&Вертикальні поля комірок:', 'Рамка і заливка',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Видимі с&торони рамки...', 'Авто', 'авто',
  // Keep row content together checkbox
  'Н&е розривати вміст рядків',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Обертання', '&Нема', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Рамки:', 'Видимі боки &рамки...',
  // More, Border, CellBorders buttons
  '&Рамка таблиці...', 'Рамки ко&мірок...',
  // Table border dialog title
  'Рамка таблиці', 'Рамки комірок за замовчуванням',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Друк', 'Заборонити перенос &рядків на наступну сторінку', 'Кількість рядків в &заголовку:',
  'Рядки заголовку будуть надруковані на кожній сторінці з таблицею',
  // top, center, bottom, default
  'З&верху', 'По &центру', 'З&низу', 'Замовч&ування',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Параметри комірок', 'Приблизна &ширина:', '&Висота не менш:', 'Колір &заливки:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Рамка', 'Видимі сторони:', '&Темний колір:', '&Світлий колір', 'Колі&р:',
  // Background image
  'Малюн&ок...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'По горизонталі', 'По вертикалі', 'Положення в тексті',
  // Position types: align, absolute, relative
  'Вирівняти', 'Абсолютне положення', 'Відносне положення',
  // [Align] left side, center, right side
  'ліву сторону по лівому боці',
  'середину по середині',
  'праву сторону по правому боці',
  // [Align] top side, center, bottom side
  'верх по верху',
  'середину по середині',
  'низ по низу',
  // [Align] relative to
  'відносно:',
  // [Position] to the right of the left side of
  'вправо від лівого краю:',
  // [Position] below the top side of
  'нижче верхнього краю:',
  // Anchors: page, main text area (x2)
  '&Сторінки', '&Області тексту', 'Сторін&ки', 'О&бласті тексту',
  // Anchors: left margin, right margin
  '&Левого поля', '&Правого поля',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Вер&хнього поля', '&Нижнього поля', 'Вн&утрішнього поля', 'Зовн&ішнього поля',
  // Anchors: character, line, paragraph
  '&Символу', 'С&троки', '&Абзаца',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Дзер&кальні поля', '&Відносно комірки таблиці',
  // Above text, below text
  '&Перед текстом', 'По&за текстом',
  // Width, Height groupboxes, Width, Height labels
  'Ширина', 'Высота', '&Ширина:', '&Высота:',
  // Auto height label, Auto height combobox item
  'Авто', 'авто',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Вертикальне вирівнювання', 'З&верху', 'По &центру', 'З&низу',
  // Border and background button and title
  '&Рамка і заливка..', 'Рамка і заливка текстового поля',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Спеціальна вставка', 'Я&к:', 'Формат RTF', 'Формат HTML',
  'Неформатований текст', 'Текст в кодуванні Юнікод', 'Точковий малюнок (BMP)', 'Метафайл (WMF)',
  'Файли за малюнками', 'URL',
  // Options group-box, styles
  'Параметри', '&Стилі:',
  // style options: apply target styles, use source styles, ignore styles
  'застосувати стилі з редагованого документа', 'зберегти стилі та зовнішній вигляд тексту',
  'зберегти зовнішній вигляд, ігнорувати стилі',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Список', 'Маркований', 'Нумерований', 'Маркований список', 'Нумерований список',
  // Customize, Reset, None
  '&Змінити...', '&Скинути', 'Ні',
  // Numbering: continue, reset to, create new
  'продовжити нумерацію', 'почати нумерацію в списку з ', 'створити новий список, починаючи з',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Прописні літери', 'Прописні римські', 'Десяткові', 'Малі літери', 'Малі римські',
  // Bullet type
  'Окружність', 'Коло', 'Квадрат',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Рівень:', '&Почати з:', '&Продовжити', 'Нумерація',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  'Зміна списку', 'Рівні', '&Кількість:', 'Властивості списку', '&Тип списку:',
  // List types: bullet, image
  'маркер', 'малюнок', 'Вставити номер|',
  // Number format, Number, Start level from, Font button
  '&Формат номера:', 'Номер', '&Нумерація рівня з:', '&Шрифт...',
  // Image button, bullet character, Bullet button,
  '&Малюнок...', '&Знак маркера', 'З&нак...',
  // Position of list text, bullet, number, image
  'Положення маркера', 'Положення маркера', 'Положення номера', 'Положення малюнку', 'Положення тексту',
  // at, left indent, first line indent, from left indent
  '', '&Відступ зліва:', '&Перший рядок:', 'від лівого відступу',
  // [only] one level preview, preview
  '&Попередній перегляд тільки одного рівня', 'Приклад',
  // Preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'справа від', 'зліва від', 'в центрі',
  // level #, this level (for combo-box)
  'Рівень %d', 'Цей рівень',
  // Marker character dialog title
  'Вибір знаку маркера',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Рамка і заливка абзацу', 'Рамка', 'Заливка',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Вид рамки', '&Колір:', 'Тов&щина:', '&Зазор:', '&Відступи...',
  // Sample, Border type group-boxes;
  'Приклад', 'Тип рамки',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Немає', 'Од&инарна', 'По&двійна', 'По&трійна', 'Товста &всередині', 'Товста зз&овні',
  // Fill color group-box; More colors, padding buttons
  'Колір заливки', '&Інший колір...', '&Поля...',
  // preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // title and group-box in Offsets dialog
  'Відступи', 'Відступи від тексту до рамки',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Абзац', 'Вирівнювання', '&Ліве', '&Праве', 'По &центру', 'По &ширині',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Інтервали', 'П&еред:', 'П&ісля:', '&Міжрядковий:', '&Значення:',
  // Indents group-box; indents: left, right, first line
  'Відступи', 'Злі&ва:', 'Сп&рава:', 'Перш&ий рядок:',
  // indented, hanging
  'Відс&туп', 'Ви&ступ', 'Приклад',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Одинарний', 'Полуторний', 'Подвійний', 'Щонайменше', 'Точно', 'Множник',
  // preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Відступи та інтервали', 'Табуляція', 'Розміщення на сторінці',
  // tab stop position label; buttons: set, delete, delete all
  'По&зиція таблуляції:', 'В&становити', 'Вил&учити', '&Вилучити всі',
  // tab align group; left, right, center aligns,
  'Вирівнювання', '&Ліве', '&Праве', 'По &центру',
  // leader radio-group; no leader
  'Заповнити', '(Немає)',
  // tab stops to be deleted, delete none, delete all labels
  'Будуть вилучені:', '', 'Всі.',
  // Pagination group-box, keep with next, keep lines together
  'Поділ на сторінки', 'Не &відривати від наступного', 'Не &розривати абзац',
  // Outline level, Body text, Level #
  '&Рівень:', 'Основний текст', 'Рівень %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Попередній перегляд', 'По ширині сторінки', 'Сторінка повністю', 'Сторінки:',
  // Invalid Scale, [page #] of #
  'Будь ласка, введіть число від 10 до 500', 'з %d',
  // Hints on buttons: first, prior, next, last pages
  'До першої', 'До попередньої', 'До наступної', 'До останньої',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Інтервали', 'Інтервали', 'Між &комірками', 'Між рамкою таблиці і комірками',
  // vertical, horizontal (x2)
  '&Вертикальний:', '&Горизонтальний:', 'В&ертикальний:', 'Го&ризонтальний:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Рамки', 'Параметри', '&Колір:', '&Світлий колір:', 'Т&емний колір:',
  // Width; Border type group-box;
  '&Товщина:', 'Тип рамки',
  // Border types: none, sunken, raised, flat
  '&Немає', '&Вдавлена', 'В&ипукла', '&Пласка',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Розбивка комірок', 'Розбити на',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Вказану кількість рядків і колонок', '&Початкові комірки',
  // number of columns, rows, merge before
  'Кількість к&олонок:', 'Кількість &рядків:', '&Об''єднати перед розбивкою',
  // to original cols, rows check-boxes
  'Розбити на початкові ко&лонки', 'Разбити на початкові ряд&ки',
  // Add Rows form -------------------------------------------------------------
  'Додавання рядків', '&Кількість рядків:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Параметри сторінки', 'Сторінка', 'Колонтитули',
  // margins group-box, left, right, top, bottom
  'Поля (міліметри)', 'Поля (дюйми)', '&Ліве:', '&Верхнє:', '&Праве:', '&Нижнє:',
  // mirror margins check-box
  'Д&зеркальні поля',
  // orientation group-box, portrait, landscape
  'Орієнтація', '&Книжна', '&Альбомна',
  // paper group-box, paper size, default paper source
  'Папір', '&Розмір:', 'П&одача:',
  // header group-box, print on the first page, font button
  'Верхній колонтитул', '&Текст:', '&Друкувати на першій сторінці', '&Шрифт...',
  // header alignments: left, center, right
  'З&ліва', 'По &центру', 'Сп&рава',
  // the same for footer (different hotkeys)
  'Нижній колонтитул', 'Т&екст:', 'Друк&увати на першій сторінці', 'Шр&ифт...',
  '&Зліва', '&По центру', 'Спр&ава',
  // page numbers group-box, start from
  'Номери сторінок', 'По&чати з:',
  // hint about codes
  'Ви можете використовувати спеціальні сполучення:'#13'&&p - номер сторінки; &&P - кількість сторінок; &&d - дата; &&t - час.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Кодова сторінка файлу', '&Оберіть кодування символів файлу:',
  // thai, japanese, chinese (simpl), korean
  'Тайська', 'Японська', 'Китайська (спрощена)', 'Корейська',
  // chinese (trad), central european, cyrillic, west european
  'Китайська (традиційна)', 'Центральної та Східної Європи', 'Кирилиця', 'Західноєвропейська',
  // greek, turkish, hebrew, arabic
  'Грецька', 'Турецька', 'Іврит', 'Арабська',
  // baltic, vietnamese, utf-8, utf-16
  'Балтійська', 'В''єтнамська', 'Юнікод (UTF-8)', 'Юнікод (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Оформлення', '&Оберіть стиль оформлення:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Перетворення в текст', 'Оберіть &роздільник:',
  // line break, tab, ';', ','
  'розрив строки', 'символ табуляції', 'крапка з комою', 'кома',
  // Table sort form -----------------------------------------------------------
  // error message
  'Неможливо виконати сортування таблиці з об’єднаними комірками',
  // title, main options groupbox
  'Сортування таблиць', 'Сортування',
  // sort by column, case sensitive
  '&Сортувати по стовпчику:', 'З &урахуванням регістру',
  // heading row, range or rows
  'Виключити рядок &заголовка', 'Рядки для сортування: з %d по %d',
  // order, ascending, descending
  'Упорядковувати', 'за з&ростанням', 'по у&буванню',
  // data type, text, number
  'Тип', '&текст', '&числа',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Номер', 'Властивості', 'Назва &лічильника:', '&Тип нумерації:',
  // numbering groupbox, continue, start from
  'Нумерація', '&Продовжити', 'П&очати з:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Назва', '&Підпис:', '&Виключити підпис з назви',
  // position radiogroup
  'Положення', '&Над обраним об''єктом', 'П&ід обраним об''єктом',
  // caption text
  'Т&екст назви:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Нумерація', 'Малюнок', 'Таблица',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Видимі сторони рамки', 'Рамка',
  // Left, Top, Right, Bottom checkboxes
  '&Ліва сторона', '&Верхня сторона', '&Права сторона', '&Нижня сторона',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Ширина колонки', 'Висота рядка',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Відступ першого рядка', 'Відступ зліва', 'Виступ', 'Відступ справа',
  // Hints on lists: up one level (left), down one level (right)
  'Зменшити рівень списку', 'Збільшити рівень списку',
  // Hints for margins: bottom, left, right and top
  'Нижнє поле', 'Ліве поле', 'Праве поле', 'Верхнє поле',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'По лівому краю', 'По правому краю', 'По центру', 'По роздільнику',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Звичайний', 'Звичайний відступ', 'Без інтервалу', 'Заголовок %d', 'Абзац списку',
  // Hyperlink, Title, Subtitle
  'Гіперпосилання', 'Назва', 'Підзаголовок',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Виділення', 'Слабке виділення', 'Сильне виділення', 'Строгий',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Цитата 2', 'Виділена цитата', 'Слабке посилання', 'Сильне посилання',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Цитата', 'Змінний HTML', 'Код HTML', 'Акронім HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Визначення HTML', 'Клавіатура HTML', 'Зразок HTML', 'Друкарська машинка HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Стандартний HTML', 'Цитата HTML', 'Верхній колонтитул', 'Нижній колонтитул',
  'Номер сторінки',
  // Caption
  'Назва об''єкту',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Знак кінцевої виноски', 'Знак виноски', 'Текст кінцевої виноски', 'Текст виноски',
  // Sidenote Reference, Sidenote Text
  'Знак виноски в рамці', 'Текст виноски в рамці',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'колір', 'колір фону', 'прозорий', 'звичайний', 'колір підкреслення',
  // default background color, default text color, [color] same as text
  'звичайний колір фону', 'звичайний колір тексту', 'як у тексту',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'звичайне', 'жирне', 'подвійне', 'пунктирне', 'жирне пунктирне', 'штрихове',
  // underline types: thick dashed, long dashed, thick long dashed,
  'жирне штрихове', 'довге штрихове', 'жирне довге штрихове',
  // underline types: dash dotted, thick dash dotted,
  'штріхпунктірне', 'жирне штріхпунктірне',
  // underline types: dash dot dotted, thick dash dot dotted
  'штріхпунктірне з 2 крапками', 'жирне штріхпунктірне з 2 крапками',
  // sub/superscript: not, subsript, superscript
  'не надрядкові/підрядкові', 'підрядкові', 'надрядкові',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'напрямок тексту', 'за замовчуванням', 'зліва направо', 'справа наліво',
  // bold, not bold
  'напівжирний', 'не напівжирний',
  // italic, not italic
  'курсив', 'не курсив',
  // underlined, not underlined, default underline
  'підкресленний', 'не підкреслений', 'підкреслення за замовчуванням',
  // struck out, not struck out
  'закреслений', 'не закреслений',
  // overlined, not overlined
  'риса зверху', 'без риси зверху',
  // all capitals: yes, all capitals: no
  'всі прописні', 'не всі прописні',
  // vertical shift: none, by x% up, by x% down
  'без вертикального зсуву', 'зсунутий на %d догори', 'зсунутий на %d донизу',
  // characters width [: x%]
  'ширина літер',
  // character spacing: none, expanded by x, condensed by x
  'звичайна міжсимвольна відстань', 'розріджений на %s', 'ущільнений на %s',
  // charset
  'набір символів',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'звичайний шрифт', 'звичайний абзац', 'успадковані властивості',
  // [hyperlink] highlight, default hyperlink attributes
  'підсвітка', 'звичайні властивості',
  // paragraph aligmnment: left, right, center, justify
  'по лівому краю', 'по правому краю', 'по центру', 'по ширині',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'висота рядка: %d', 'відстань між рядками: %s', 'висота рядка: не менше %s',
  'висота рядка: рівно %s',
  // no wrap, wrap
  'заборонити перенос рядків', 'дозволити перенос рядків',
  // keep lines together: yes, no
  'не розривати абзац', 'дозволити розривати абзац',
  // keep with next: yes, no
  'не відривати від наступного абзацу', 'дозволити відривати від наступного абзацу',
  // read only: yes, no
  'заборонити редагування', 'дозволити редагування',
  // body text, heading level x
  'рівень структури: основний текст', 'рівень структури: %d',
  // indents: first line, left, right
  'відступ першого рядка', 'відступ зліва', 'відступ справа',
  // space before, after
  'проміжок перед', 'проміжок після',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'табуляції', 'ні', 'вирівнювання', 'по лівому краю', 'по правому краю', 'по центру',
  // tab leader (filling character)
  'заповнювач',
  // no, yes
  'ні', 'так',
  // [padding/spacing/side:] left, top, right, bottom
  'ліворуч', 'зверху', 'праворуч', 'знизу',
  // border: none, single, double, triple, thick inside, thick outside
  'ні', 'одинарна', 'подвійна', 'потрійна', 'товста всередині', 'товста зовні',
  // background, border, padding, spacing [between text and border]
  'заливка', 'рамка', 'поля', 'відступи',
  // border: style, width, internal width, visible sides
  'стиль', 'товщина', 'проміжок', 'сторони',
  // style inspector -----------------------------------------------------------
  // title
  'Інспектор стилів',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Стиль', '(немає)', 'Абзац', 'Шрифт', 'Властивості',
  // border and background, hyperlink, standard [style]
  'Рамка і заливка', 'Гіперпосилання', '(стандартний)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Стилі', 'Стиль',
  // name, applicable to,
  '&Назва:', '&Можна застосовати до:',
  // based on, based on (no &),
  'Засн&ован на:', 'Заснован на:',
  //  next style, next style (no &)
  '&Стиль наступного абзацу:', 'Стиль наступного абзацу:',
  // quick access check-box, description and preview
  '&Швидкий доступ', 'Опис і пе&регляд:',
  // [applicable to:] paragraph and text, paragraph, text
  'абзацам і тексту', 'абзацам', 'тексту',
  // text and paragraph styles, default font
  'Стилі тексту та абзаців', 'Основний текст',
  // links: edit, reset, buttons: add, delete
  'Змінити', 'Скинути', '&Додати', '&Видалити',
  // add standard style, add custom style, default style name
  'Додати &стандартний стиль...', '&Додати стиль', 'Стиль %d',
  // choose style
  'Оберить &стиль:',
  // import, export,
  '&Імпорт...', '&Експорт...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Стилі RichView (*.rvst)|*.rvst', 'Помилка читання файлу', 'В обраному файлі відсутні стилі',
  // Title, group-box, import styles
  'Імпорт стилів з файлу', 'Імпорт', '&Імпортувати стилі:',
  // existing styles radio-group: Title, override, auto-rename
  'Вже наявні стилі', '&замінити новими', '&додати з новими назвами',
  // Select, Unselect, Invert,
  'О&брати', '&Очистити', '&Інвертувати',
  // select/unselect: all styles, new styles, existing styles
  '&Всі стилі', '&Нові стилі', '&Існуючі стилі',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Очистити формат', 'Усі стилі',
  // dialog title, prompt
  'Стилі', '&Оберіть стиль:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Синоніми', '&Пропустити все', '&Додати в словник',
  // Progress messages ---------------------------------------------------------
  'Завантажується %s', 'Йде підготовка до друку...', 'Друкується сторінка %d',
  'Перетворення з RTF...', 'Перетворення в RTF...',
  'Читання: %s...', 'Запис: %s...', 'текст',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Нема виноски', 'Виноска', 'Кінцева виноска', 'Виноска в рамці', 'Текстове поле'
  );


initialization
  RVA_RegisterLanguage(
    'Ukrainian', // english language name, do not translate
    'Українська', // native language name
    RUSSIAN_CHARSET, @Messages);

end.
