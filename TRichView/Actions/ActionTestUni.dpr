program ActionTestUni;

uses
  Forms,
  dmActionsAlpha in 'dmActionsAlpha.pas' {rvActionsResource: TDataModule},
  Unit3Unicode in 'Unit3Unicode.pas' {Form3};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TrvActionsResource, rvActionsResource);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
