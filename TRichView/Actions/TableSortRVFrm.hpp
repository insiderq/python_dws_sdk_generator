﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TableSortRVFrm.pas' rev: 27.00 (Windows)

#ifndef TablesortrvfrmHPP
#define TablesortrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tablesortrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVTableSort;
class PASCALIMPLEMENTATION TfrmRVTableSort : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Extctrls::TRadioGroup* rgOrder;
	Vcl::Extctrls::TRadioGroup* rgDataType;
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TLabel* lblCol;
	Vcl::Stdctrls::TComboBox* cmbCol;
	Vcl::Stdctrls::TCheckBox* cbCaseSensitive;
	Vcl::Stdctrls::TCheckBox* cbHeading;
	Vcl::Stdctrls::TLabel* lblRows;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cbHeadingClick(System::TObject* Sender);
	
public:
	Vcl::Controls::TControl* _cmbCol;
	Vcl::Controls::TControl* _cbHeading;
	Vcl::Controls::TControl* _rgOrder;
	Vcl::Controls::TControl* _rgDataType;
	Vcl::Controls::TControl* _cbCaseSensitive;
	Vcl::Controls::TControl* _lblRows;
	int MinRowNumber;
	int FirstRow;
	int LastRow;
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	void __fastcall UpdateRowsLabel(void);
	DYNAMIC void __fastcall Localize(void);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVTableSort(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVTableSort(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVTableSort(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVTableSort(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tablesortrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TABLESORTRVFRM)
using namespace Tablesortrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TablesortrvfrmHPP
