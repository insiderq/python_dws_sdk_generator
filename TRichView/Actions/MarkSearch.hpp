﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'MarkSearch.pas' rev: 27.00 (Windows)

#ifndef MarksearchHPP
#define MarksearchHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Marksearch
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE int __fastcall MarkSubStringA(const Rvtypes::TRVAnsiString s, System::Uitypes::TColor Color, System::Uitypes::TColor BackColor, bool IgnoreCase, bool WholeWords, Richview::TCustomRichView* rv, Crvdata::TCustomRVData* RVData = (Crvdata::TCustomRVData*)(0x0));
extern DELPHI_PACKAGE int __fastcall MarkSubStringW(const Rvtypes::TRVUnicodeString s, System::Uitypes::TColor Color, System::Uitypes::TColor BackColor, bool IgnoreCase, bool WholeWords, Richview::TCustomRichView* rv, Crvdata::TCustomRVData* RVData = (Crvdata::TCustomRVData*)(0x0));
}	/* namespace Marksearch */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_MARKSEARCH)
using namespace Marksearch;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// MarksearchHPP
