﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'SpTBXSkins.pas' rev: 27.00 (Windows)

#ifndef SptbxskinsHPP
#define SptbxskinsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <System.IniFiles.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Sptbxskins
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TSpTBXSkinType : unsigned char { sknNone, sknWindows, sknSkin, sknDelphiStyle };

enum DECLSPEC_DENUM TSpTBXLunaScheme : unsigned char { lusBlue, lusMetallic, lusGreen, lusUnknown };

enum DECLSPEC_DENUM TSpTBXSkinComponentsType : unsigned char { skncDock, skncDockablePanel, skncDockablePanelTitleBar, skncGutter, skncMenuBar, skncOpenToolbarItem, skncPanel, skncPopup, skncSeparator, skncSplitter, skncStatusBar, skncStatusBarGrip, skncTabBackground, skncTabToolbar, skncToolbar, skncToolbarGrip, skncWindow, skncWindowTitleBar, skncMenuBarItem, skncMenuItem, skncToolbarItem, skncButton, skncCheckBox, skncEditButton, skncEditFrame, skncHeader, skncLabel, skncListItem, skncProgressBar, skncRadioButton, skncTab, skncTrackBar, skncTrackBarButton };

enum DECLSPEC_DENUM TSpTBXSkinStatesType : unsigned char { sknsNormal, sknsDisabled, sknsHotTrack, sknsPushed, sknsChecked, sknsCheckedAndHotTrack };

typedef System::Set<TSpTBXSkinStatesType, TSpTBXSkinStatesType::sknsNormal, TSpTBXSkinStatesType::sknsCheckedAndHotTrack> TSpTBXSkinStatesSet;

enum DECLSPEC_DENUM TSpTBXSkinPartsType : unsigned char { sknpBody, sknpBorders, sknpText };

struct DECLSPEC_DRECORD TSpTBXSkinComponentsIdentEntry
{
public:
	System::UnicodeString Name;
	TSpTBXSkinStatesSet States;
};


typedef System::StaticArray<TSpTBXSkinComponentsIdentEntry, 33> Sptbxskins__1;

typedef System::StaticArray<System::UnicodeString, 6> Sptbxskins__2;

typedef System::StaticArray<System::UnicodeString, 6> Sptbxskins__3;

enum DECLSPEC_DENUM TSpTextRotationAngle : unsigned char { tra0, tra90, tra270 };

struct DECLSPEC_DRECORD TSpTBXTextInfo
{
public:
	System::UnicodeString Text;
	TSpTextRotationAngle TextAngle;
	unsigned TextFlags;
	System::Types::TSize TextSize;
	bool IsCaptionShown;
	bool IsTextRotated;
};


enum DECLSPEC_DENUM TSpGlyphLayout : unsigned char { ghlGlyphLeft, ghlGlyphTop };

enum DECLSPEC_DENUM TSpGlowDirection : unsigned char { gldNone, gldAll, gldTopLeft, gldBottomRight };

enum DECLSPEC_DENUM TSpTBXComboPart : unsigned char { cpNone, cpCombo, cpSplitLeft, cpSplitRight };

struct DECLSPEC_DRECORD TSpTBXMenuItemMarginsInfo
{
public:
	System::Types::TRect Margins;
	int GutterSize;
	int LeftCaptionMargin;
	int RightCaptionMargin;
	int ImageTextSpace;
};


struct DECLSPEC_DRECORD TSpTBXMenuItemInfo
{
public:
	bool Enabled;
	bool HotTrack;
	bool Pushed;
	bool Checked;
	bool HasArrow;
	bool ImageShown;
	bool ImageOrCheckShown;
	System::Types::TSize ImageSize;
	System::Types::TSize RightImageSize;
	bool IsDesigning;
	bool IsOnMenuBar;
	bool IsOnToolbox;
	bool IsOpen;
	bool IsSplit;
	bool IsSunkenCaption;
	bool IsVertical;
	TSpTBXMenuItemMarginsInfo MenuMargins;
	TSpTBXComboPart ComboPart;
	System::Types::TRect ComboRect;
	TSpTBXSkinStatesType ComboState;
	bool ToolbarStyle;
	TSpTBXSkinStatesType State;
	TSpTBXSkinType SkinType;
};


enum DECLSPEC_DENUM TSpTBXColorTextType : unsigned char { cttDefault, cttHTML, cttIdentAndHTML };

class DELPHICLASS TSpTBXSkinOptionEntry;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXSkinOptionEntry : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	int FSkinType;
	System::Uitypes::TColor FColor1;
	System::Uitypes::TColor FColor2;
	System::Uitypes::TColor FColor3;
	System::Uitypes::TColor FColor4;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TSpTBXSkinOptionEntry(void);
	void __fastcall Fill(int ASkinType, System::Uitypes::TColor AColor1, System::Uitypes::TColor AColor2, System::Uitypes::TColor AColor3, System::Uitypes::TColor AColor4);
	void __fastcall ReadFromString(System::UnicodeString S);
	System::UnicodeString __fastcall WriteToString(void);
	bool __fastcall IsEmpty(void);
	bool __fastcall IsEqual(TSpTBXSkinOptionEntry* AOptionEntry);
	void __fastcall Lighten(int Amount);
	void __fastcall Reset(void);
	
__published:
	__property int SkinType = {read=FSkinType, write=FSkinType, nodefault};
	__property System::Uitypes::TColor Color1 = {read=FColor1, write=FColor1, nodefault};
	__property System::Uitypes::TColor Color2 = {read=FColor2, write=FColor2, nodefault};
	__property System::Uitypes::TColor Color3 = {read=FColor3, write=FColor3, nodefault};
	__property System::Uitypes::TColor Color4 = {read=FColor4, write=FColor4, nodefault};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSpTBXSkinOptionEntry(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXSkinOptionCategory;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXSkinOptionCategory : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TSpTBXSkinOptionEntry* FBody;
	TSpTBXSkinOptionEntry* FBorders;
	System::Uitypes::TColor FTextColor;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TSpTBXSkinOptionCategory(void);
	__fastcall virtual ~TSpTBXSkinOptionCategory(void);
	bool __fastcall IsEmpty(void);
	void __fastcall Reset(void);
	void __fastcall LoadFromIni(System::Inifiles::TMemIniFile* MemIni, System::UnicodeString Section, System::UnicodeString Ident);
	void __fastcall SaveToIni(System::Inifiles::TMemIniFile* MemIni, System::UnicodeString Section, System::UnicodeString Ident);
	
__published:
	__property TSpTBXSkinOptionEntry* Body = {read=FBody, write=FBody};
	__property TSpTBXSkinOptionEntry* Borders = {read=FBorders, write=FBorders};
	__property System::Uitypes::TColor TextColor = {read=FTextColor, write=FTextColor, nodefault};
};

#pragma pack(pop)

class DELPHICLASS TSpTBXSkinOptions;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXSkinOptions : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Uitypes::TColor FColorBtnFace;
	int FFloatingWindowBorderSize;
	System::StaticArray<System::StaticArray<TSpTBXSkinOptionCategory*, 6>, 33> FOptions;
	bool FOfficeIcons;
	bool FOfficeMenu;
	bool FOfficeStatusBar;
	System::UnicodeString FSkinAuthor;
	System::UnicodeString FSkinName;
	bool __fastcall GetOfficeIcons(void);
	bool __fastcall GetOfficeMenu(void);
	bool __fastcall GetOfficePopup(void);
	bool __fastcall GetOfficeStatusBar(void);
	int __fastcall GetFloatingWindowBorderSize(void);
	void __fastcall SetFloatingWindowBorderSize(const int Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	void __fastcall BroadcastChanges(void);
	
public:
	__fastcall virtual TSpTBXSkinOptions(void);
	__fastcall virtual ~TSpTBXSkinOptions(void);
	void __fastcall CopyOptions(TSpTBXSkinComponentsType AComponent, TSpTBXSkinComponentsType ToComponent);
	virtual void __fastcall FillOptions(void);
	TSpTBXSkinOptionCategory* __fastcall Options(TSpTBXSkinComponentsType Component, TSpTBXSkinStatesType State)/* overload */;
	TSpTBXSkinOptionCategory* __fastcall Options(TSpTBXSkinComponentsType Component)/* overload */;
	void __fastcall LoadFromFile(System::UnicodeString Filename);
	virtual void __fastcall LoadFromStrings(System::Classes::TStrings* L);
	void __fastcall SaveToFile(System::UnicodeString Filename);
	virtual void __fastcall SaveToStrings(System::Classes::TStrings* L);
	virtual void __fastcall SaveToMemIni(System::Inifiles::TMemIniFile* MemIni);
	void __fastcall Reset(bool ForceResetSkinProperties = false);
	virtual void __fastcall GetDropDownArrowSize(/* out */ int &DropDownArrowSize, /* out */ int &DropDownArrowMargin, /* out */ int &SplitBtnArrowSize);
	virtual void __fastcall GetMenuItemMargins(Vcl::Graphics::TCanvas* ACanvas, int ImgSize, /* out */ TSpTBXMenuItemMarginsInfo &MarginsInfo);
	TSpTBXSkinStatesType __fastcall GetState(bool Enabled, bool Pushed, bool HotTrack, bool Checked)/* overload */;
	void __fastcall GetState(TSpTBXSkinStatesType State, /* out */ bool &Enabled, /* out */ bool &Pushed, /* out */ bool &HotTrack, /* out */ bool &Checked)/* overload */;
	virtual System::Uitypes::TColor __fastcall GetTextColor(TSpTBXSkinComponentsType Component, TSpTBXSkinStatesType State);
	bool __fastcall GetThemedElementDetails(TSpTBXSkinComponentsType Component, bool Enabled, bool Pushed, bool HotTrack, bool Checked, bool Focused, bool Defaulted, bool Grayed, /* out */ Vcl::Themes::TThemedElementDetails &Details)/* overload */;
	bool __fastcall GetThemedElementDetails(TSpTBXSkinComponentsType Component, TSpTBXSkinStatesType State, /* out */ Vcl::Themes::TThemedElementDetails &Details)/* overload */;
	System::Types::TSize __fastcall GetThemedElementSize(Vcl::Graphics::TCanvas* ACanvas, const Vcl::Themes::TThemedElementDetails &Details);
	void __fastcall GetThemedElementTextColor(const Vcl::Themes::TThemedElementDetails &Details, /* out */ System::Uitypes::TColor &AColor);
	System::Uitypes::TColor __fastcall GetThemedSystemColor(System::Uitypes::TColor AColor);
	virtual void __fastcall PaintBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, TSpTBXSkinComponentsType Component, TSpTBXSkinStatesType State, bool Background, bool Borders, bool Vertical = false, System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() );
	void __fastcall PaintThemedElementBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const Vcl::Themes::TThemedElementDetails &Details)/* overload */;
	void __fastcall PaintThemedElementBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, TSpTBXSkinComponentsType Component, TSpTBXSkinStatesType State)/* overload */;
	void __fastcall PaintThemedElementBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, TSpTBXSkinComponentsType Component, bool Enabled, bool Pushed, bool HotTrack, bool Checked, bool Focused, bool Defaulted, bool Grayed)/* overload */;
	virtual void __fastcall PaintMenuCheckMark(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Checked, bool Grayed, bool MenuItemStyle, TSpTBXSkinStatesType State);
	virtual void __fastcall PaintMenuRadioMark(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Checked, bool MenuItemStyle, TSpTBXSkinStatesType State);
	virtual void __fastcall PaintWindowFrame(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool IsActive, bool DrawBody, int BorderSize = 0x4);
	__property System::Uitypes::TColor ColorBtnFace = {read=FColorBtnFace, write=FColorBtnFace, nodefault};
	__property int FloatingWindowBorderSize = {read=GetFloatingWindowBorderSize, write=SetFloatingWindowBorderSize, nodefault};
	__property bool OfficeIcons = {read=GetOfficeIcons, write=FOfficeIcons, nodefault};
	__property bool OfficeMenu = {read=GetOfficeMenu, write=FOfficeMenu, nodefault};
	__property bool OfficePopup = {read=GetOfficePopup, nodefault};
	__property bool OfficeStatusBar = {read=GetOfficeStatusBar, write=FOfficeStatusBar, nodefault};
	__property System::UnicodeString SkinAuthor = {read=FSkinAuthor, write=FSkinAuthor};
	__property System::UnicodeString SkinName = {read=FSkinName, write=FSkinName};
};

#pragma pack(pop)

typedef System::TMetaClass* TSpTBXSkinOptionsClass;

class DELPHICLASS TSpTBXSkinsListEntry;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSpTBXSkinsListEntry : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	TSpTBXSkinOptionsClass SkinClass;
	System::Classes::TStringList* SkinStrings;
	__fastcall virtual ~TSpTBXSkinsListEntry(void);
public:
	/* TObject.Create */ inline __fastcall TSpTBXSkinsListEntry(void) : System::TObject() { }
	
};

#pragma pack(pop)

class DELPHICLASS TSpTBXSkinsList;
class PASCALIMPLEMENTATION TSpTBXSkinsList : public System::Classes::TStringList
{
	typedef System::Classes::TStringList inherited;
	
private:
	TSpTBXSkinsListEntry* __fastcall GetSkinOption(int Index);
	
public:
	virtual void __fastcall Delete(int Index);
	__fastcall virtual ~TSpTBXSkinsList(void);
	int __fastcall AddSkin(System::UnicodeString SkinName, TSpTBXSkinOptionsClass SkinClass)/* overload */;
	int __fastcall AddSkin(System::Classes::TStrings* SkinOptions)/* overload */;
	int __fastcall AddSkinFromFile(System::UnicodeString Filename);
	void __fastcall AddSkinsFromFolder(System::UnicodeString Folder);
	void __fastcall GetSkinNames(System::Classes::TStrings* SkinNames);
	__property TSpTBXSkinsListEntry* SkinOptions[int Index] = {read=GetSkinOption};
public:
	/* TStringList.Create */ inline __fastcall TSpTBXSkinsList(void)/* overload */ : System::Classes::TStringList() { }
	/* TStringList.Create */ inline __fastcall TSpTBXSkinsList(bool OwnsObjects)/* overload */ : System::Classes::TStringList(OwnsObjects) { }
	
};


class DELPHICLASS TSpTBXSkinManager;
class PASCALIMPLEMENTATION TSpTBXSkinManager : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TSpTBXSkinOptions* FCurrentSkin;
	System::Classes::TList* FNotifies;
	TSpTBXSkinsList* FSkinsList;
	System::Classes::TNotifyEvent FOnSkinChange;
	void __fastcall Broadcast(void);
	void __fastcall ResetDelphiStyle(void);
	System::UnicodeString __fastcall GetCurrentSkinName(void);
	
public:
	__fastcall virtual TSpTBXSkinManager(void);
	__fastcall virtual ~TSpTBXSkinManager(void);
	TSpTBXSkinType __fastcall GetSkinType(void);
	bool __fastcall IsDefaultSkin(void);
	bool __fastcall IsXPThemesEnabled(void);
	void __fastcall AddSkinNotification(System::TObject* AObject);
	void __fastcall RemoveSkinNotification(System::TObject* AObject);
	void __fastcall BroadcastSkinNotification(void);
	void __fastcall LoadFromFile(System::UnicodeString Filename);
	void __fastcall SaveToFile(System::UnicodeString Filename);
	void __fastcall SetToDefaultSkin(void);
	void __fastcall SetSkin(System::UnicodeString SkinName);
	__property TSpTBXSkinOptions* CurrentSkin = {read=FCurrentSkin};
	__property System::UnicodeString CurrentSkinName = {read=GetCurrentSkinName};
	__property TSpTBXSkinsList* SkinsList = {read=FSkinsList};
	__property System::Classes::TNotifyEvent OnSkinChange = {read=FOnSkinChange, write=FOnSkinChange};
};


class DELPHICLASS TSpTBXSkinSwitcher;
class PASCALIMPLEMENTATION TSpTBXSkinSwitcher : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	System::Classes::TNotifyEvent FOnSkinChange;
	System::UnicodeString __fastcall GetSkin(void);
	void __fastcall SetSkin(const System::UnicodeString Value);
	MESSAGE void __fastcall WMSpSkinChange(Winapi::Messages::TMessage &Message);
	
public:
	__fastcall virtual TSpTBXSkinSwitcher(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSpTBXSkinSwitcher(void);
	
__published:
	__property System::UnicodeString Skin = {read=GetSkin, write=SetSkin};
	__property System::Classes::TNotifyEvent OnSkinChange = {read=FOnSkinChange, write=FOnSkinChange};
};


typedef Vcl::Themes::TCustomStyleServices TSpTBXThemeServices;

typedef BOOL __stdcall (*TSpPrintWindow)(HWND Hnd, HDC HdcBlt, unsigned nFlags);

//-- var, const, procedure ---------------------------------------------------
static const System::Word WM_SPSKINCHANGE = System::Word(0x87d7);
extern DELPHI_PACKAGE System::Set<TSpTBXSkinComponentsType, TSpTBXSkinComponentsType::skncDock, TSpTBXSkinComponentsType::skncTrackBarButton> SpTBXSkinMultiStateComponents;
#define CSpTBXSkinAllStates (System::Set<TSpTBXSkinStatesType, TSpTBXSkinStatesType::sknsNormal, TSpTBXSkinStatesType::sknsCheckedAndHotTrack>() << TSpTBXSkinStatesType::sknsNormal << TSpTBXSkinStatesType::sknsDisabled << TSpTBXSkinStatesType::sknsHotTrack << TSpTBXSkinStatesType::sknsPushed << TSpTBXSkinStatesType::sknsChecked << TSpTBXSkinStatesType::sknsCheckedAndHotTrack )
extern DELPHI_PACKAGE Sptbxskins__1 CSpTBXSkinComponents;
extern DELPHI_PACKAGE Sptbxskins__2 SSpTBXSkinStatesString;
extern DELPHI_PACKAGE Sptbxskins__3 SSpTBXSkinDisplayStatesString;
extern DELPHI_PACKAGE Vcl::Graphics::TBitmap* StockBitmap;
extern DELPHI_PACKAGE TSpPrintWindow SpPrintWindow;
extern DELPHI_PACKAGE Vcl::Themes::TCustomStyleServices* __fastcall SpTBXThemeServices(void);
extern DELPHI_PACKAGE TSpTBXSkinManager* __fastcall SkinManager(void);
extern DELPHI_PACKAGE TSpTBXSkinOptions* __fastcall CurrentSkin(void);
extern DELPHI_PACKAGE TSpTBXLunaScheme __fastcall SpGetLunaScheme(void);
extern DELPHI_PACKAGE void __fastcall SpFillGlassRect(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
extern DELPHI_PACKAGE bool __fastcall SpIsGlassPainting(Vcl::Controls::TControl* AControl);
extern DELPHI_PACKAGE void __fastcall SpDrawParentBackground(Vcl::Controls::TControl* Control, HDC DC, const System::Types::TRect &R);
extern DELPHI_PACKAGE HFONT __fastcall SpCreateRotatedFont(HDC DC, int Orientation = 0xa8c);
extern DELPHI_PACKAGE int __fastcall SpDrawRotatedText(const HDC DC, System::UnicodeString AText, System::Types::TRect &ARect, const unsigned AFormat, TSpTextRotationAngle RotationAngle = (TSpTextRotationAngle)(0x2));
extern DELPHI_PACKAGE int __fastcall SpCalcXPText(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, System::UnicodeString Caption, System::Classes::TAlignment CaptionAlignment, unsigned Flags, const System::Types::TSize &GlyphSize, const System::Types::TSize &RightGlyphSize, TSpGlyphLayout Layout, bool PushedCaption, /* out */ System::Types::TRect &ACaptionRect, /* out */ System::Types::TRect &AGlyphRect, /* out */ System::Types::TRect &ARightGlyphRect, TSpTextRotationAngle RotationAngle = (TSpTextRotationAngle)(0x0));
extern DELPHI_PACKAGE int __fastcall SpDrawXPGlassText(Vcl::Graphics::TCanvas* ACanvas, System::UnicodeString Caption, System::Types::TRect &ARect, unsigned Flags, int CaptionGlowSize);
extern DELPHI_PACKAGE int __fastcall SpDrawXPText(Vcl::Graphics::TCanvas* ACanvas, System::UnicodeString Caption, System::Types::TRect &ARect, unsigned Flags, TSpGlowDirection CaptionGlow = (TSpGlowDirection)(0x0), System::Uitypes::TColor CaptionGlowColor = (System::Uitypes::TColor)(0xffff), TSpTextRotationAngle RotationAngle = (TSpTextRotationAngle)(0x0))/* overload */;
extern DELPHI_PACKAGE int __fastcall SpDrawXPText(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, System::UnicodeString Caption, TSpGlowDirection CaptionGlow, System::Uitypes::TColor CaptionGlowColor, System::Classes::TAlignment CaptionAlignment, unsigned Flags, const System::Types::TSize &GlyphSize, TSpGlyphLayout Layout, bool PushedCaption, /* out */ System::Types::TRect &ACaptionRect, /* out */ System::Types::TRect &AGlyphRect, TSpTextRotationAngle RotationAngle = (TSpTextRotationAngle)(0x0))/* overload */;
extern DELPHI_PACKAGE int __fastcall SpDrawXPText(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, System::UnicodeString Caption, TSpGlowDirection CaptionGlow, System::Uitypes::TColor CaptionGlowColor, System::Classes::TAlignment CaptionAlignment, unsigned Flags, Vcl::Imglist::TCustomImageList* IL, int ImageIndex, TSpGlyphLayout Layout, bool Enabled, bool PushedCaption, bool DisabledIconCorrection, /* out */ System::Types::TRect &ACaptionRect, /* out */ System::Types::TRect &AGlyphRect, TSpTextRotationAngle RotationAngle = (TSpTextRotationAngle)(0x0))/* overload */;
extern DELPHI_PACKAGE System::Types::TSize __fastcall SpGetTextSize(HDC DC, System::UnicodeString S, bool NoPrefix);
extern DELPHI_PACKAGE int __fastcall SpGetControlTextHeight(Vcl::Controls::TControl* AControl, Vcl::Graphics::TFont* AFont);
extern DELPHI_PACKAGE System::Types::TSize __fastcall SpGetControlTextSize(Vcl::Controls::TControl* AControl, Vcl::Graphics::TFont* AFont, System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SpStripAccelChars(System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SpStripShortcut(System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SpStripTrailingPunctuation(System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SpRectToString(const System::Types::TRect &R);
extern DELPHI_PACKAGE bool __fastcall SpStringToRect(System::UnicodeString S, /* out */ System::Types::TRect &R);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SpColorToHTML(const System::Uitypes::TColor Color);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SpColorToString(const System::Uitypes::TColor Color, TSpTBXColorTextType TextType = (TSpTBXColorTextType)(0x0));
extern DELPHI_PACKAGE bool __fastcall SpStringToColor(System::UnicodeString S, /* out */ System::Uitypes::TColor &Color);
extern DELPHI_PACKAGE void __fastcall SpGetRGB(System::Uitypes::TColor Color, /* out */ int &R, /* out */ int &G, /* out */ int &B);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall SpRGBToColor(int R, int G, int B);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall SpLighten(System::Uitypes::TColor Color, int Amount);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall SpBlendColors(System::Uitypes::TColor TargetColor, System::Uitypes::TColor BaseColor, int Percent);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall SpMixColors(System::Uitypes::TColor TargetColor, System::Uitypes::TColor BaseColor, System::Byte Amount);
extern DELPHI_PACKAGE System::Types::TRect __fastcall SpCenterRect(const System::Types::TRect &Parent, int ChildWidth, int ChildHeight)/* overload */;
extern DELPHI_PACKAGE System::Types::TRect __fastcall SpCenterRect(const System::Types::TRect &Parent, const System::Types::TRect &Child)/* overload */;
extern DELPHI_PACKAGE System::Types::TRect __fastcall SpCenterRectHoriz(const System::Types::TRect &Parent, int ChildWidth);
extern DELPHI_PACKAGE System::Types::TRect __fastcall SpCenterRectVert(const System::Types::TRect &Parent, int ChildHeight);
extern DELPHI_PACKAGE void __fastcall SpFillRect(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, System::Uitypes::TColor BrushColor, System::Uitypes::TColor PenColor = (System::Uitypes::TColor)(0x1fffffff));
extern DELPHI_PACKAGE void __fastcall SpDrawLine(Vcl::Graphics::TCanvas* ACanvas, int X1, int Y1, int X2, int Y2, System::Uitypes::TColor Color);
extern DELPHI_PACKAGE void __fastcall SpDrawRectangle(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int CornerSize, System::Uitypes::TColor ColorL, System::Uitypes::TColor ColorT, System::Uitypes::TColor ColorR, System::Uitypes::TColor ColorB, System::Uitypes::TColor InternalColorL, System::Uitypes::TColor InternalColorT, System::Uitypes::TColor InternalColorR, System::Uitypes::TColor InternalColorB, System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() )/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawRectangle(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int CornerSize, System::Uitypes::TColor ColorTL, System::Uitypes::TColor ColorBR, System::Uitypes::TColor ColorTLInternal = (System::Uitypes::TColor)(0x1fffffff), System::Uitypes::TColor ColorBRInternal = (System::Uitypes::TColor)(0x1fffffff), System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() )/* overload */;
extern DELPHI_PACKAGE void __fastcall SpAlphaBlend(HDC SrcDC, HDC DstDC, const System::Types::TRect &SrcR, const System::Types::TRect &DstR, System::Byte Alpha, bool SrcHasAlphaChannel = false);
extern DELPHI_PACKAGE void __fastcall SpPaintTo(Vcl::Controls::TWinControl* WinControl, Vcl::Graphics::TCanvas* ACanvas, int X, int Y);
extern DELPHI_PACKAGE void __fastcall SpDrawIconShadow(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex);
extern DELPHI_PACKAGE void __fastcall SpDrawImageList(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex, bool Enabled, bool DisabledIconCorrection);
extern DELPHI_PACKAGE void __fastcall SpGradient(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int StartPos, int EndPos, int ChunkSize, System::Uitypes::TColor C1, System::Uitypes::TColor C2, const bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpGradientFill(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const System::Uitypes::TColor C1, const System::Uitypes::TColor C2, const bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpGradientFillMirror(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const System::Uitypes::TColor C1, const System::Uitypes::TColor C2, const System::Uitypes::TColor C3, const System::Uitypes::TColor C4, const bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpGradientFillMirrorTop(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const System::Uitypes::TColor C1, const System::Uitypes::TColor C2, const System::Uitypes::TColor C3, const System::Uitypes::TColor C4, const bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpGradientFillGlass(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, const System::Uitypes::TColor C1, const System::Uitypes::TColor C2, const System::Uitypes::TColor C3, const System::Uitypes::TColor C4, const bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpDrawArrow(Vcl::Graphics::TCanvas* ACanvas, int X, int Y, System::Uitypes::TColor AColor, bool Vertical, bool Reverse, int Size);
extern DELPHI_PACKAGE void __fastcall SpDrawDropMark(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &DropMark);
extern DELPHI_PACKAGE void __fastcall SpDrawFocusRect(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect);
extern DELPHI_PACKAGE void __fastcall SpDrawGlyphPattern(HDC DC, const System::Types::TRect &R, int Width, int Height, const void *PatternBits, System::Uitypes::TColor PatternColor)/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawGlyphPattern(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, int PatternIndex, System::Uitypes::TColor PatternColor)/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawXPButton(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Enabled, bool Pushed, bool HotTrack, bool Checked, bool Focused, bool Defaulted);
extern DELPHI_PACKAGE void __fastcall SpDrawXPCheckBoxGlyph(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Enabled, Vcl::Stdctrls::TCheckBoxState State, bool HotTrack, bool Pushed);
extern DELPHI_PACKAGE void __fastcall SpDrawXPRadioButtonGlyph(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Enabled, bool Checked, bool HotTrack, bool Pushed);
extern DELPHI_PACKAGE void __fastcall SpDrawXPEditFrame(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Enabled, bool HotTrack, bool ClipContent = false, bool AutoAdjust = false)/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawXPEditFrame(Vcl::Controls::TWinControl* AWinControl, bool HotTracking, bool AutoAdjust = false, bool HideFrame = false)/* overload */;
extern DELPHI_PACKAGE void __fastcall SpDrawXPGrip(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, System::Uitypes::TColor LoC, System::Uitypes::TColor HiC);
extern DELPHI_PACKAGE void __fastcall SpDrawXPHeader(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool HotTrack, bool Pushed);
extern DELPHI_PACKAGE void __fastcall SpDrawXPListItemBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, bool Selected, bool HotTrack, bool Focused, bool ForceRectBorders = false, bool Borders = true);
extern DELPHI_PACKAGE void __fastcall SpPaintSkinBackground(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, TSpTBXSkinOptionCategory* SkinOption, bool Vertical);
extern DELPHI_PACKAGE void __fastcall SpPaintSkinBorders(Vcl::Graphics::TCanvas* ACanvas, const System::Types::TRect &ARect, TSpTBXSkinOptionCategory* SkinOption, System::Uitypes::TAnchors ForceRectBorders = System::Uitypes::TAnchors() );
extern DELPHI_PACKAGE bool __fastcall SpIsWinVistaOrUp(void);
extern DELPHI_PACKAGE bool __fastcall SpGetDirectories(System::UnicodeString Path, System::Classes::TStringList* L);
}	/* namespace Sptbxskins */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SPTBXSKINS)
using namespace Sptbxskins;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SptbxskinsHPP
