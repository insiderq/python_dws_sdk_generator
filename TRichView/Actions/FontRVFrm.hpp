﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'FontRVFrm.pas' rev: 27.00 (Windows)

#ifndef FontrvfrmHPP
#define FontrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVFontCombos.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <ColorRVFrm.hpp>	// Pascal unit
#include <RVColorGrid.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Fontrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVFont;
class PASCALIMPLEMENTATION TfrmRVFont : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Controls::TImageList* ImageList1;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* ts1;
	Rvfontcombos::TRVFontSizeComboBox* cmbSize;
	Rvfontcombos::TRVFontComboBox* cmbFont;
	Rvfontcombos::TRVFontCharsetComboBox* cmbCharset;
	Vcl::Stdctrls::TGroupBox* gbStyle;
	Vcl::Stdctrls::TCheckBox* cbB;
	Vcl::Stdctrls::TCheckBox* cbI;
	Vcl::Stdctrls::TGroupBox* gbEffects;
	Vcl::Stdctrls::TCheckBox* cbS;
	Vcl::Stdctrls::TCheckBox* cbO;
	Vcl::Stdctrls::TCheckBox* cbAC;
	Vcl::Stdctrls::TLabel* lblFont;
	Vcl::Stdctrls::TLabel* lblSize;
	Vcl::Stdctrls::TLabel* lblScript;
	Rvstyle::TRVStyle* rvs;
	Vcl::Comctrls::TTabSheet* ts2;
	Vcl::Stdctrls::TGroupBox* gbSpacing;
	Vcl::Stdctrls::TLabel* lblSpacing;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbCond;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbExp;
	Rvspinedit::TRVSpinEdit* seSpacing;
	Vcl::Stdctrls::TGroupBox* gbVShift;
	Vcl::Stdctrls::TLabel* lblVShift;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbUp;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbDown;
	Rvspinedit::TRVSpinEdit* seShift;
	Vcl::Stdctrls::TLabel* Label5;
	Vcl::Stdctrls::TGroupBox* gbCharScale;
	Vcl::Stdctrls::TLabel* lblCharScale;
	Rvspinedit::TRVSpinEdit* seCharScale;
	Vcl::Stdctrls::TLabel* Label8;
	Rvcolorcombo::TRVColorCombo* cmbColor;
	Vcl::Stdctrls::TLabel* lblColor;
	Vcl::Stdctrls::TGroupBox* gbScript;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbNormal;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbSub;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbSuper;
	Vcl::Stdctrls::TGroupBox* gbSample;
	Richview::TRichView* rv;
	Vcl::Stdctrls::TGroupBox* gbUnderline;
	Vcl::Stdctrls::TComboBox* cmbUnderline;
	Rvcolorcombo::TRVColorCombo* cmbUnderlineColor;
	Vcl::Stdctrls::TLabel* lblSpacingMU;
	Vcl::Stdctrls::TLabel* lblBackColor;
	Rvcolorcombo::TRVColorCombo* cmbBackColor;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall pcChange(System::TObject* Sender);
	void __fastcall cmbFontClick(System::TObject* Sender);
	void __fastcall cmbCharsetClick(System::TObject* Sender);
	void __fastcall rvsDrawStyleText(Rvstyle::TRVStyle* Sender, const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, int StyleNo, int SpaceBefore, int Left, int Top, int Width, int Height, Rvstyle::TRVTextDrawStates DrawState, bool &DoDefault);
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall rbExpClick(System::TObject* Sender);
	void __fastcall seSpacingChange(System::TObject* Sender);
	void __fastcall seShiftChange(System::TObject* Sender);
	void __fastcall rbUpClick(System::TObject* Sender);
	void __fastcall FormKeyDown(System::TObject* Sender, System::Word &Key, System::Classes::TShiftState Shift);
	void __fastcall cmbUnderlineDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall cmbColorColorChange(System::TObject* Sender);
	void __fastcall cmbUnderlineClick(System::TObject* Sender);
	
private:
	Rvtable::TRVTableItemInfo* table;
	bool LockUpdates;
	void __fastcall UpdatePreview(void);
	System::WideString __fastcall GetPreviewString(void);
	void __fastcall DrawUnderline(Vcl::Graphics::TCanvas* Canvas, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	
protected:
	Vcl::Controls::TControl* _gbSample;
	Vcl::Controls::TControl* _gbUnderline;
	Vcl::Controls::TControl* _gbVShift;
	Vcl::Controls::TControl* _gbCharScale;
	Vcl::Controls::TControl* _gbScript;
	Vcl::Controls::TControl* _pc;
	
public:
	Vcl::Controls::TControl* _cbB;
	Vcl::Controls::TControl* _cbI;
	Vcl::Controls::TControl* _cbS;
	Vcl::Controls::TControl* _cbO;
	Vcl::Controls::TControl* _cbAC;
	Vcl::Controls::TControl* _cmbUnderline;
	Vcl::Dialogs::TColorDialog* ColorDialog;
	System::WideString PreviewString;
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVFont(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVFont(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVFont(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVFont(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Fontrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FONTRVFRM)
using namespace Fontrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// FontrvfrmHPP
