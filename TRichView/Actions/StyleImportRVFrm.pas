unit StyleImportRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList,
  BaseRVFrm, ExtCtrls, Menus, Buttons, RVScroll, RichView, StdCtrls,
  CheckLst, RVStyle, RVALocalize, RVStyleFuncs, RVClasses;

{$IFNDEF RVDONOTUSESTYLETEMPLATES}

type
  TfrmRVStyleImport = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    gb: TGroupBox;
    lst: TCheckListBox;
    lblLst: TLabel;
    lblDescr: TLabel;
    rv: TRichView;
    btnSelect: TBitBtn;
    btnUnselect: TBitBtn;
    pm: TPopupMenu;
    mitAll: TMenuItem;
    mitNew: TMenuItem;
    mitOld: TMenuItem;
    btnInvert: TButton;
    rg: TRadioGroup;
    rvs: TRVStyle;
    procedure FormCreate(Sender: TObject);
    procedure btnInvertClick(Sender: TObject);
    procedure mitAllClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSelectClick(Sender: TObject);
    procedure btnUnselectClick(Sender: TObject);
    procedure mitNewClick(Sender: TObject);
    procedure mitOldClick(Sender: TObject);
    procedure lstClick(Sender: TObject);
  private
    { Private declarations }
    CheckValue: Boolean;
    NewStyles: TRVIntegerList;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    procedure OnCreateThemedMenu(OldMenu, NewMenu: TComponent); override;
    {$ENDIF}
  public
    { Public declarations }
    _lst, _btnSelect, _btnUnselect, _rg: TControl;
    _pm: TComponent;
    Images, Images2: TCustomImageList;
    FontImageIndex, HyperlinkImageIndex, ParaImageIndex, ParaBorderImageIndex: Integer;
    ImageIndices: TRVStyleTemplateImageIndices;
    procedure Init(arvs: TRVStyle; ExistingStyleTemplates: TRVStyleTemplateCollection;
      descrrvs: TRVStyle);
    procedure Localize; override;
    function GetStyleTemplateIndex(StyleTemplate: TRVStyleTemplate): Integer;
    function GetIndexInCheckedItems(Index: Integer): Integer;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSESTYLETEMPLATES}

{$IFDEF RICHVIEWDEF10}
uses
  Types;
{$ENDIF}

{$R *.DFM}

{ TfrmRVStyleImport }

procedure TfrmRVStyleImport.Init(arvs: TRVStyle;
  ExistingStyleTemplates: TRVStyleTemplateCollection; descrrvs: TRVStyle);
var SL: TRVALocStringList;
    i: Integer;
begin
  rvs.Units := arvs.Units;
  rvs.StyleTemplates.AssignStyleTemplates(arvs.StyleTemplates, True);
  rvs.StyleTemplates.UpdateReferences;
  rvs.TextStyles := descrrvs.TextStyles;
  rvs.ParaStyles := descrrvs.ParaStyles;
  SL := TRVALocStringList.Create;
  try
    RVStyleTemplatesToStrings(rvs.StyleTemplates, SL, True,
      [rvstkParaText, rvstkPara, rvstkText], nil, False, ControlPanel);
    SL.Sort;
    SetXBoxItems(_lst, SL);
  finally
    SL.Free;
  end;
  NewStyles := TRVIntegerList.CreateEx(rvs.StyleTemplates.Count, 0);
  for i := 0 to GetXBoxItemCount(_lst)-1 do begin
    SetListBoxChecked(_lst, i, True);
    if ExistingStyleTemplates.FindByName(TRVStyleTemplate(GetXBoxObject(_lst, i)).Name)<0 then
      NewStyles[i] := 1;
  end;
  if GetXBoxItemCount(_lst)>0 then
    SetXBoxItemIndex(_lst, 0);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.FormCreate(Sender: TObject);
begin
  _lst := lst;
  _btnSelect := btnSelect;
  _btnUnselect := btnUnselect;
  _rg := rg;
  _pm := pm;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.FormDestroy(Sender: TObject);
begin
  NewStyles.Free;
  inherited;
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVStyleImport.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _lst then
    _lst := NewControl
  else if OldControl = _btnSelect then
    _btnSelect := NewControl
  else if OldControl = _btnUnselect then
    _btnUnselect := NewControl
  else if OldControl = _rg then
    _rg := NewControl
  else
    inherited;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.OnCreateThemedMenu(OldMenu, NewMenu: TComponent);
begin
   if OldMenu = _pm then
    _pm := NewMenu
  else
    inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.btnInvertClick(Sender: TObject);
var i: Integer;
begin
  for i := 0 to GetXBoxItemCount(_lst)-1 do
    SetListBoxChecked(_lst, i, not GetListBoxChecked(_lst, i));
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.mitAllClick(Sender: TObject);
var i: Integer;
begin
  for i := 0 to GetXBoxItemCount(_lst)-1 do
    SetListBoxChecked(_lst, i, CheckValue);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.btnSelectClick(Sender: TObject);
var pt: TPoint;
begin
  CheckValue := True;
  pt := _btnSelect.ClientToScreen(Point(0, _btnSelect.Height));
  (_pm as TRVADialogPopupMenu).Popup(pt.X, pt.Y);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.btnUnselectClick(Sender: TObject);
var pt: TPoint;
begin
  CheckValue := False;
  pt := _btnUnselect.ClientToScreen(Point(0, _btnUnselect.Height));
  (_pm as TRVADialogPopupMenu).Popup(pt.X, pt.Y);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.mitNewClick(Sender: TObject);
var i: Integer;
begin
  for i := 0 to GetXBoxItemCount(_lst)-1 do
    if NewStyles[i]<>0 then
      SetListBoxChecked(_lst, i, CheckValue);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.mitOldClick(Sender: TObject);
var i: Integer;
begin
  for i := 0 to GetXBoxItemCount(_lst)-1 do
    if NewStyles[i]=0 then
      SetListBoxChecked(_lst, i, CheckValue);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.lstClick(Sender: TObject);
var StyleTemplate: TRVStyleTemplate;
begin
  if GetXBoxItemIndex(_lst)<0 then
    StyleTemplate := nil
  else
    StyleTemplate := TRVStyleTemplate(GetXBoxObject(_lst, GetXBoxItemIndex(_lst)));
  rv.Clear;
  if StyleTemplate<>nil then
    RVDisplayStyleTemplateBasic(StyleTemplate, rv, Images2, ImageIndices, ControlPanel);
    rv.AddBreak;
    RVDisplayStyleTemplate(StyleTemplate, rvs, rv, Images,
      FontImageIndex, HyperlinkImageIndex, ParaImageIndex, ParaBorderImageIndex,
      False, ControlPanel);
  rv.Format;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyleImport.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_sti_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  btnSelect.Caption := RVA_GetSAsIs(rvam_sti_Select, ControlPanel);
  btnUnselect.Caption := RVA_GetSAsIs(rvam_sti_Unselect, ControlPanel);
  btnInvert.Caption := RVA_GetSAsIs(rvam_sti_Invert, ControlPanel);
  mitAll.Caption := RVA_GetSAsIs(rvam_sti_SelectAll, ControlPanel);
  mitNew.Caption := RVA_GetSAsIs(rvam_sti_SelectNew, ControlPanel);
  mitOld.Caption := RVA_GetSAsIs(rvam_sti_SelectOld, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_sti_GB, ControlPanel);
  lblLst.Caption := RVA_GetSAsIs(rvam_sti_List, ControlPanel);
  lblDescr.Caption := RVA_GetSAsIs(rvam_st_Descr, ControlPanel);
  rg.Caption := RVA_GetSHAsIs(rvam_sti_RG, ControlPanel);
  rg.Items[0] := RVA_GetSHAsIs(rvam_sti_Override, ControlPanel);
  rg.Items[1] := RVA_GetSHAsIs(rvam_sti_Rename, ControlPanel);
  if (RVA_GetCharset(ControlPanel)=ARABIC_CHARSET) or
    (RVA_GetCharset(ControlPanel)=HEBREW_CHARSET) then 
    rv.BiDiMode := rvbdRightToLeft;  
end;

function TfrmRVStyleImport.GetStyleTemplateIndex(
  StyleTemplate: TRVStyleTemplate): Integer;
var i: Integer;
begin
  for i := 0 to GetXBoxItemCount(_lst)-1 do
    if GetXBoxObject(_lst, i)=StyleTemplate then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;

function TfrmRVStyleImport.GetIndexInCheckedItems(Index: Integer): Integer;
var i: Integer;
begin
  Result := -1;
  if not GetListBoxChecked(_lst, Index) then
    exit;
  for i := 0 to Index do
    if GetListBoxChecked(_lst, i) then
      inc(Result);
end;

function TfrmRVStyleImport.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gb.Top-gb.Height);
  Width := gb.Left*2 + gb.Width;
  Result := True;
end;


{$ENDIF}




end.
