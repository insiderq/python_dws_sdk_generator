﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'FourSidesRVFrm.pas' rev: 27.00 (Windows)

#ifndef FoursidesrvfrmHPP
#define FoursidesrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Foursidesrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVFourSides;
class PASCALIMPLEMENTATION TfrmRVFourSides : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TLabel* lblTop;
	Vcl::Stdctrls::TLabel* lblBottom;
	Rvspinedit::TRVSpinEdit* seTop;
	Rvspinedit::TRVSpinEdit* seBottom;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TLabel* lblLeft;
	Vcl::Stdctrls::TLabel* lblRight;
	Rvspinedit::TRVSpinEdit* seLeft;
	Rvspinedit::TRVSpinEdit* seRight;
	Vcl::Stdctrls::TCheckBox* cbEqual;
	void __fastcall cbEqualClick(System::TObject* Sender);
	void __fastcall seTopChange(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	bool Updating;
	
public:
	Vcl::Controls::TControl* _gb;
	Vcl::Controls::TControl* _cbEqual;
	void __fastcall SetValues(Rvstyle::TRVStyleLength Left, Rvstyle::TRVStyleLength Top, Rvstyle::TRVStyleLength Right, Rvstyle::TRVStyleLength Bottom, bool FLeft, bool FTop, bool FRight, bool FBottom, Rvstyle::TRVStyle* RVStyle, bool OnlyPositive, Rvstyle::TRVUnits Units);
	void __fastcall SetValues2(Rvstyle::TRVStyleLength Left, Rvstyle::TRVStyleLength Top, Rvstyle::TRVStyleLength Right, Rvstyle::TRVStyleLength Bottom, bool FLeft, bool FTop, bool FRight, bool FBottom, Rvstyle::TRVStyle* RVStyle, System::Extended MinValue, Rvstyle::TRVUnits Units);
	void __fastcall GetValues(Rvstyle::TRVStyleLength &Left, Rvstyle::TRVStyleLength &Top, Rvstyle::TRVStyleLength &Right, Rvstyle::TRVStyleLength &Bottom, bool &FLeft, bool &FTop, bool &FRight, bool &FBottom, Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVUnits Units);
	void __fastcall SetMinValue(int MinValue);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVFourSides(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVFourSides(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVFourSides(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVFourSides(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Foursidesrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FOURSIDESRVFRM)
using namespace Foursidesrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// FoursidesrvfrmHPP
