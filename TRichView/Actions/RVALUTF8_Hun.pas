﻿// This file is a copy of RVAL_Hun.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Hungarian translation                           }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated: Gabor Berenyi <gberenyi@csdonline.hu>     }
{ Updated: 2007-01-02 by Bógáncs Sándor                 }
{ Updated: 2007-12-31 by Gabor Berenyi                  }
{ Updated: 2011-02-11 by Bógáncs Sándor                 }
{ Updated: 2011-10-28 by Bógáncs Sándor                 }
{ Updated: 2012-08-22 by Bógáncs Sándor                 }
{ Updated: 2014-02-01 by Bógáncs Sándor                 }
{*******************************************************}

unit RVALUTF8_Hun;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, mm, cm, picas, pixels, points
  'inch', 'mm', 'cm', 'pica pont', 'képpont', 'pont',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fájl', '&Szerkesztés', 'F&ormázás', 'F&ont', 'Be&kezdés', '&Beszúrás', '&Táblázat', '&Ablak', '&Segítség',
  // exit
  '&Kilépés',
  // top-level menus: View, Tools,
  '&Nézet', '&Segédeszközök',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Méret', 'Stílus', 'Kiválasz&t', 'A cella tartalmának &igazítása', 'Ce&llaszegélyek',
  // menus: Table cell rotation
  'Cella &forgatás', 
  // menus: Text flow, Footnotes/endnotes
  '&Szöveg irány', '&Lábjegyzetek',
  // ribbon tabs: tab1, tab2, view, table
  '&Kezdőlap', '&Haladó', '&Nézet', '&Tábla',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Vágólap', 'Betűtípus', 'Bekezdés', 'Felsorolás', 'Szerkesztés',
  // ribbon groups: insert, background, page setup,
  'Beszúrás', 'Oldalháttér', 'Oldalbeállítás',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Hivatkozások', 'Lábjegyzet', 'Nézetek', 'Mutat/Elrejt', 'Nagyítás', 'Opciók',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Beszúrás', 'Törlés', 'Műveletek', 'Keretek',
  // ribbon groups: styles 
  'Stílusok',
  // ribbon screen tip footer,
  'Nyomjon F1-et a további segítségért',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Legutóbbi dokumentumok', 'Dokumentum mentése másként', 'Dokumentum előnézet és nyomtatás',
  // ribbon label: units combo
  'Egység:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'Ú&j', 'Új|Üres dokumentum létrehozása',
  // TrvActionOpen
  'Meg&nyit...', 'Megnyit|Meglévő dokumentum megnyitása',
  // TrvActionSave
  '&Mentés', 'Mentés|Dokumentum mentése',
  // TrvActionSaveAs
  'Menté&s másként...', 'Mentés másként...|Dokumentum mentése új néven',
  // TrvActionExport
  '&Export...', 'Export|Exportálás más típusú dokumentumként',
  // TrvActionPrintPreview
  'Nyomtatási ké&p', 'Nyomtatási kép|Dokumentum megjelenítése ahogy nyomtatón megjelenik',
  // TrvActionPrint
  'Nyomt&atás...', 'Nyomtatás|Nyomtatási opciók beállítása és a dokumentum nyomtatása',
  // TrvActionQuickPrint
  '&Gyorsnyomtatás', '&Gyorsnyomtatás', 'Gyorsnyomtatás|Dokuemntum küldése a nyomtatóra',
  // TrvActionPageSetup
  '&Oldalbeállítás...', 'Oldalbeállítás|Margók, papír méret, tájolás, fej- és láblécek beállítása',
  // TrvActionCut
  '&Kivág', 'Kivág|Kijelölt elemek áthelyezése a vágólapra',
  // TrvActionCopy
  '&Másol', 'Másol|Kijelölt elemek másolása a vágólapra',
  // TrvActionPaste
  '&Beilleszt', 'Beilleszt|Elemek beillesztése a vágólapról',
  // TrvActionPasteAsText
  'Beillesztés &szövegként', 'Beillesztés szövegként|A vágólap tartalmának beillesztése szövegként',
  // TrvActionPasteSpecial
  'Irá&nyított beillesztés...', 'Irányított beillesztés|Elemek beillesztése  vágólapról alternatív formában',
  // TrvActionSelectAll
  'Min&dent kijelöl', 'Mindent kijelöl|A teljes dokumentum kijelölése',
  // TrvActionUndo
  '&Visszavonás', 'Visszavonás|Utolsó művelet visszavonása',
  // TrvActionRedo
  '&Ismétel', 'Ismétel|Utolsó visszavont művelet ismétlése',
  // TrvActionFind
  '&Keresés...', 'Keresés|Megadott szöveg keresése a dokumentumban',
  // TrvActionFindNext
  'Keresés &tovább', 'Keresés tovább|Utolsó keresés folytatása',
  // TrvActionReplace
  '&Csere...', 'Csere|Megadott szöveg keresése és cseréje egy megadottal',
  // TrvActionInsertFile
  '&Fájl...', 'Fájl beszúrása|Fájl tartalmának beszúrása a dokumentumba',
  // TrvActionInsertPicture
  '&Kép...', 'Kép beszúrása|Kép beszúrása a dokumentumba',
  // TRVActionInsertHLine
  '&Vízszintes vonal', 'Vízszintes vonal beszúrása|Vízszintes vonal beszúrása',
  // TRVActionInsertHyperlink
  '&Web-link...', 'Weblink beszúrása|Weblink beszúrása a dokumentumba',
  // TRVActionRemoveHyperlinks
  '&Hivatkozások törlése', 'Hivatkozások törlése|Eltávolítja az összes hivatkozást a kijelölt szövegből',
  // TrvActionInsertSymbol
  '&Szimbólum...', 'Szimbólum beszúrása|Szimbólum beszúrása',
  // TrvActionInsertNumber
  '&Számláló...', 'Szám beszúrása|Számok beszúrása',
  // TrvActionInsertCaption
  '&Felirat beszúrása...', 'Felirat beszúrás|Beszúr egy feliratot a kiválasztott objektumhoz.',
  // TrvActionInsertTextBox
  '&Szövegdoboz', 'Szövegbox beszúrása|Szövegdobozt szúr be.',
  // TrvActionInsertSidenote
  '&Széljegyzet', 'Széljegyzet beszúrása|Beszúr egy szövegdobozban megjelenő jegyzetet.',
  // TrvActionInsertPageNumber
  '&Oldalszám', 'Oldalszám beszúrása|Oldalszámozást szúr be',
  // TrvActionParaList
  '&Felsorolás és számozás...', 'Felsorolás és számozás|A kijelölt bekezdés átalakítása, vagy szerkesztése felsorolássá vagy sorszámozássá',
  // TrvActionParaBullets
  '&Felsorolás', 'Felsorolás|Felsorolás hozzáadása vagy elvétele a bekezdésben',
  // TrvActionParaNumbering
  '&Sorszámozás', 'Sorszámozás|Sorszámozás hozzáadása vagy elvétele a bekezdésben',
  // TrvActionColor
  'Háttér &színe...', 'Háttér színe|A dokumentum háttérszínének megváltoztatása',
  // TrvActionFillColor
  '&Kitöltő szín...', 'Kitöltő szín|A szöveg, bekezdés, cella, tábla vagy dokumentum kitöltő színének a megváltoztatása',
  // TrvActionInsertPageBreak
  '&Oldaltörés beszúrása', 'Oldaltörés beszúrása|Oldaltörés beszúrása',
  // TrvActionRemovePageBreak
  'Oldaltörés &törlés', 'Oldaltörés törlés|Oldaltörés törlés',
  // TrvActionClearLeft
  'Szövegbeosztás törlése a &bal oldalon', 'Szövegbeosztás törlése a bal oldalon|A bekezdést a balra igazított képek alá helyezi',
  // TrvActionClearRight
  'Szövegbeosztás törlése a &jobb oldalon', 'Szövegbeosztás törlése a jobb oldalon|A bekezdést a jobbra igazított képek alá helyezi',
  // TrvActionClearBoth
  'Szövegbeosztás törlése a &mindkét oldalon', 'Szövegbeosztás törlése a mindkét oldalon|A bekezdést mind a balra, mind a jobbra igazított képek alá helyezi',
  // TrvActionClearNone
  '&Normál szövegbeosztás', 'Normál szövegbeosztás|A bekezdés körbefut mind a balra, mind a jobbra igazított képeken',
  // TrvActionVAlign
  '&Objektum elhelyezése...', 'Objektum elhelyezése|A kiválasztott objektum elhelyezésének beállítása',    
  // TrvActionItemProperties
  'Elemt&ulajdonság...', 'Elemtulajdonság|Aktuális elem tulajdonságainak megváltoztatása',
  // TrvActionBackground
  '&Háttér...', 'Háttér|Háttér színének és képének megváltoztatása',
  // TrvActionParagraph
  '&Bekezdés...', 'Bekezdés|Bekezdés tulajdonságainak megváltoztatása',
  // TrvActionIndentInc
  'Behúzás &növelése', 'Behúzás növelése|A kijelölt bekezdések behúzásának növelése',
  // TrvActionIndentDec
  'Behúzás &csökkentése', 'Behúzás csökkentése|A kijelölt bekezdések behúzásának csökentése',
  // TrvActionWordWrap
  '&Szóelválasztás', 'Szóelválasztás|A kijelölt bekezdésben a szóelválasztás engedélyezése',
  // TrvActionAlignLeft
  'Igazítás ba&lra', 'Igazítás balra|Kijelölt szövegek igazítása balra',
  // TrvActionAlignRight
  'Igazítás &jobbra', 'Igazítás jobbra|Kijelölt szövegek igazítása jobbra',
  // TrvActionAlignCenter
  'Igazítás &középre', 'Igazítás középre|Kijelölt szövegek igazítása középre',
  // TrvActionAlignJustify
  '&Sorkizárás', 'Sorkizárás|A kijelölt szövegek elnyújtása a teljes sorban',
  // TrvActionParaColor
  'Bekezdés &háttérszíne...', 'Bekezdés háttérszíne|A bekezdés háttérszínének megváltoztatása',
  // TrvActionLineSpacing100
  '&Szimpla sorköz', 'Szimpla sorköz|Szimpla sorköz beállítása',
  // TrvActionLineSpacing150
  '1.5 Sor&köz', '1.5 Sorköz|Másfélszeres sorköz beállítása',
  // TrvActionLineSpacing200
  '&Dupla Sorköz', 'Dupla Sorköz|Dupla sorköz beállítása',
  // TrvActionParaBorder
  'Bekezdés szélei és &háttere...', 'Bekezdés szélei és háttere|A kijelölt bekezdések széleinek és hátterének beállítása',
  // TrvActionInsertTable
  '&Tábla beszúrása...', '&Tábla beszúrása', 'Tábla beszúrása|Új tábla beszúrása',
  // TrvActionTableInsertRowsAbove
  'Sor beszúrása &elé', 'Sor beszúrása elé|A kijelölt cella elé szúrja be az új sort',
  // TrvActionTableInsertRowsBelow
  'Sor beszúrása &mögé', 'Sor beszúrása mögé|A kijelölt cella után szúrja be az új sort',
  // TrvActionTableInsertColLeft
  'Oszlop beszúrása &balról', 'Oszlop beszúrása balról|A kijelölt bal oldali cella elé szúr be egy oszlopot',
  // TrvActionTableInsertColRight
  'Oszlop beszúrása &jobbról', 'Oszlop beszúrása jobbról|A kijelölt jobb oldali cella után szúr be egy oszlopot',
  // TrvActionTableDeleteRows
  'Sor tö&rlése', 'Sor törlése|A kijelölt cellák sorainak törlése',
  // TrvActionTableDeleteCols
  'Oszlop &törlése', 'Oszlop törlése|A kijelölt cellák oszlopainak törlése',
  // TrvActionTableDeleteTable
  '&Tábla törlése', 'Tábla törlése|A kijelölt tábla törlése',
  // TrvActionTableMergeCells
  '&Cellaegyesítés', 'Cellaeggyesítés|A kijelölt cellák egyesítése',
  // TrvActionTableSplitCells
  '&Cellák felosztása...', 'Cellák felosztása|A kijelölt cellák felosztása több új cellára',
  // TrvActionTableSelectTable
  'Tábla &kiválasztása', 'Tábla kiválasztása|Tábla kiválasztása',
  // TrvActionTableSelectRows
  'So&r kiválasztása', 'Sor kiválasztása|Sor kiválasztása',
  // TrvActionTableSelectCols
  '&Oszlop kiválasztása', 'Oszlop kiválasztása|Oszlop kiválasztása',
  // TrvActionTableSelectCell
  'Ce&lla kiválasztása', 'Cella kiválasztása|Cella kiválasztása',
  // TrvActionTableCellVAlignTop
  'Cellaigazítása &felülre', 'Cellaigazítása felülre|A cellák tartalmának igazítása a cellák tetejére',
  // TrvActionTableCellVAlignMiddle
  'Cellaigazítása &középre', 'Cellaigazítás középre|A cellák tartalmának igazítása középre',
  // TrvActionTableCellVAlignBottom
  'Cellaigazítás &alulra', 'Cellaigazítás alulra|A cellák tartalmának igazítása alulra',
  // TrvActionTableCellVAlignDefault
  '&A cellák alapértelmezett függőleges igazítása', 'A cellák alapértelmezett függőleges igazítása|A kijelölt cellákra beállítja az alapértelmezett igzítást',
  // TrvActionTableCellRotationNone
  '&Nincs cella forgatás', 'Nincs cella forgatás|Cella tartalmának forgatása 0°-kal',
  // TrvActionTableCellRotation90
  '&90°-os cella forgatás', '90°-os cella forgatás|Cella tartalmának forgatása 90°-kal',
  // TrvActionTableCellRotation180
  '&180°-os cella forgatás', '180°-os cella forgatás|Cella tartalmának forgatása 180°-kal',
  // TrvActionTableCellRotation270
  '&270°-os cella forgatás', '270°-os cella forgatás|Cella tartalmának forgatása 270°-kal',
  // TrvActionTableProperties
  'Tábla &tulajdonságai...', 'Tábla tulajdonságai|A kijelölt tábla tulajdonságai módosíthatjuk',
  // TrvActionTableGrid
  'Cella&határolók megjelenítése', 'Cellahatárolók megjelenítése|Megjeleníti, vagy eltünteti a cellákat határoló vonalakat',
  // TRVActionTableSplit
  'Táblázat fe&losztása', 'Táblázat felosztása|Kettéosztja a táblázatot a kiválasztott sortól kezdve',
  // TRVActionTableToText
  'Táblázat s&zöveggé...', 'Táblázat szöveggé|A táblőázatot szöveggé koventálja',
  // TRVActionTableSort
  '&Rendezés...', 'Rendezés|A táblázat sorainak rendezése',
  // TrvActionTableCellLeftBorder
  '&Bal szegély', 'Bal szegély|Megjeleníti, vagy eltünteti a bal cellaszegélyt',
  // TrvActionTableCellRightBorder
  '&Jobb szegély', 'Jobb szegély|Megjeleníti, vagy eltünteti a jobb cellaszegélyt',
  // TrvActionTableCellTopBorder
  '&Felső szegély', 'Felső szegély|Megjeleníti, vagy eltünteti a felső cellaszegélyt',
  // TrvActionTableCellBottomBorder
  '&Alsó szegély', 'Alsó szegély|Megjeleníti, vagy eltünteti az alsó cellaszegélyt',
  // TrvActionTableCellAllBorders
  '&Minden szegély', 'Minden szegély|Megjeleníti, vagy eltünteti a cellszegélyt',
  // TrvActionTableCellNoBorders
  '&Nincs szegély', 'Nincs szegély|Eltünteti az összes cellaszegélyt',
  // TrvActionFonts & TrvActionFontEx
  '&Betűtípus...', 'Betűtípus|A kijelölt szöveg betűtípusának megváltoztatása',
  // TrvActionFontBold
  '&Félkövér', 'Félkövér|A félkövérség stílusának beállítása a kijelölt szövegen',
  // TrvActionFontItalic
  '&Dőlt', 'Dőlt|A kijelölt szöveg betűtípusának átalakítása dőlt betűvé',
  // TrvActionFontUnderline
  '&Aláhúzott', 'Aláhúzott|A kijelölt szöveg betűtípusának átalakítása aláhúzott betűvé',
  // TrvActionFontStrikeout
  'Á&thúzott', 'Áthúzott|A kijelölt szöveg áthúzása',
  // TrvActionFontGrow
  'Betű &nagyobbítása', 'Betű nagyobbítása|A kijelölt szöveg nagyobbítása 10%-al',
  // TrvActionFontShrink
  'Betű &kicsinítése', 'Betű kicsinyítése|A kijelölt szöveg kicsinyítése 10%-al',
  // TrvActionFontGrowOnePoint
  'Betűméret nö&velése 1 ponttal', 'Betűméret növelése 1 ponttal|A kiválasztott szöveg betűtípus nagyságánk növelése 1 ponttal',
  // TrvActionFontShrinkOnePoint
  'Betűméret c&sökkentése 1 ponttal', 'Betűméret csökkentése 1 ponttal|A kiválasztott szöveg betűtípus nagyságának csökkentése 1 ponttal',
  // TrvActionFontAllCaps
  'Mind &nagybetű', 'Mind nagybetű|A kiválasztott szöveg valamennyi betűjét nagybetűssé alakítja',
  // TrvActionFontOverline
  'Á&thúzás', 'Áthúzás|A kijelölt szöveget vízszintes vonallal áthúzza',
  // TrvActionFontColor
  'Szöveg &színe...', 'Szöveg színe|Megváltoztatja a kijelölt szöveg színét',
  // TrvActionFontBackColor
  'Szöveg &háttérszíne...', 'Szöveg háttérszíne|Megváltoztatja a kijelölt szöveg háttérszínét',
  // TrvActionAddictSpell3
  '&Helyesírás ellenőrzés', 'Helyesírás ellenőrzés|Ellenőrzi a kijelölt szöveg helyességét',
  // TrvActionAddictThesaurus3
  '&Szószedet', 'Szószedet|Megjeleníti a kiválasztott szó szinonimáit',
  // TrvActionParaLTR
  'Balról jobbra', 'Balról jobbra|Balról jobbra állítja át a szövegirányt a kijelölt bekezdésben',
  // TrvActionParaRTL
  'Jobbról balra', 'Jobbról balra|Jobbról balra állítj át a szövegirányt a kijelölt bekezdésben',
  // TrvActionLTR
  'Szöveg balról jobbra', 'Szöveg balról jobbra|Balról jobbra állítja át a szövegirányt a kijelölt szövegben',
  // TrvActionRTL
  'Szöveg jobbról balra', 'Szöveg jobbról balra|Jobbról balra állítja át a szövegirányt a kijelölt szövegben',
  // TrvActionCharCase
  'Betűváltás', 'Betűváltás|Megváltoztatja a betűt (kicsi, nagy) a kijelölt szövegben',
  // TrvActionShowSpecialCharacters
  '&Nem nyomtatható karakterek', 'Nem nyomtatható karakterek|Megjeleníti, vagy eltünteti a nem nyomtatható karaktereket, mint például bekezdésjelölők, táblázatok, szóközök',
  // TrvActionSubscript
  '&Alsóindex', 'Alsóindex|A kijelölt szöveg átalakítása kisméretűre a szöveg alapvonala alatt',
  // TrvActionSuperscript
  'Felsőindex', 'Felsőindex|A kijelölt szöveg átalakítása kisméretűre a szöveg alapvonala felett',
  // TrvActionInsertFootnote
  '&Lábjegyzet', 'Lábjegyzet|Lábjegyzet beszúrása',
  // TrvActionInsertEndnote
  '&Végjegyzet', 'Végjegyzet|Végjegyzet beszúrása',
  // TrvActionEditNote
  'J&egyzet szerkesztése', 'Jegyzet szerkesztése|Lábjegyzet vagy végjegyzet szerkesztése',
  // TrvActionHide
  '&Elrejt', 'Elrejt|Elrejti, vagy megmutatja a kiválasztott részt',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stílusok...', 'Stílusok|Megjeleníti a stílus beállítás ablakot',
  // TrvActionAddStyleTemplate
  'Ú&j stílus...', 'Új stílus|Új szöveg, vagy bekezdés stílus létrehozása',
  // TrvActionClearFormat,
  '&Formázás törlése', 'Formázás törlése|Törli a kijelölt rész összes szöveg és bekezdés formázását',
  // TrvActionClearTextFormat,
  '&Szöveg formázás törlése', 'Szöveg formázás törlése|Törli a kijelölt szöveg formázását',
  // TrvActionStyleInspector
  'Stílus &böngésző', 'Stílus böngésző|Megmutatja, vagy elrejti a stílus böngészőt',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Rendben', 'Mégsem', 'Bezár', 'Beszúr', '&Megnyit...', '&Mentés...', '&Töröl', 'Súgó', '&Eltávolít',
  // Others  -------------------------------------------------------------------
  // percents
  'százalék',
  // left, top, right, bottom sides
  'Bal oldal', 'Felső oldal', 'Jobb oldal', 'Alsó oldal',
  // save changes? confirm title
  'Mentsem a változásokat a: %s -ba?', 'Jóváhagyás',
  // warning: losing formatting
  'A %s számos megvalósítás nem kompatibilis azzal a formátummal melybe menteni akarja.'#13+
  'Valóban ebben a formátumban kívánja menteni a dokumentumot?',
  // RVF format name
  'RichView formátum',
  // Error messages ------------------------------------------------------------
  'Hiba',
  'Hiba az állomány betöltésekor.'#13#13'Lehetséges megoldások:'#13'- az állomány formátuma nem kompatibilis ezzel az alkalmazással;'#13+
  '- az állomány sérült;'#13'- az állományt egy másik alkalmazás nyitva tartja és zárolja.',
  'Hiba a képállomány betöltésekor.'#13#13'Lehetséges megoldások:'#13'- az állomány képformátumban van és nem támogatja ez az alkalmazás;'#13+
  '- az állomány nem tartalmazza a képet;'#13+
  '- az állomány sérült;'#13'- az állományt egy másik alkalmazás nyitva tartja és zárolja.',
  'Hiba az állomány mentésekor.'#13#13'Lehetséges megoldások:'#13'- nem állrendelkezésre a szükséges lemezterület;'#13+
  '- a lemez írásvédett;'#13'- az lemez nem található az olvasóban;'#13+
  '- az állományt egy másik alkalmazás nyitva tartja és zárolja;'#13+'- a lemez hibás.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView állományok (*.rvf)|*.rvf',  'RTF állományok (*.rtf)|*.rtf' , 'XML állományok (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Szöveges állományok (*.txt)|*.txt', 'Unicode szöveges állományok (*.txt)|*.txt', 'Szöveges állomány - Autofelismerés (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML állomány (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - egyszerű (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dokumentum (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Keresés befejezve', 'A keresett ''%s'' szövegrész nem található.',
  // 1 string replaced; Several strings replaced
  '1 szövegrész cserélve.', '%d szövegrész cserélve.',
  // continue search from the beginning/end?
  'A dokumentum végére értem, folytassam az elejétől?',
  'A dokumentum elejére értem, folytassam a végéről?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Áttetsző', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Fekete', 'Barna', 'Világoszöld', 'Sötétzöld', 'Sötét páva kék', 'Sötétkék', 'Indigókék', '80%-os szürke',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Sötétvörös', 'Narancs', 'Sötét sárga', 'Zöld', 'Pávakék', 'Kék', 'Kékesszürke', 'Fél szürke',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vörös', 'Világosnarancs', 'Lime', 'Tengerzöld', 'Tengerkék', 'Halványkék', 'Viola', '40%-os szürke',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rózsaszín', 'Arany', 'Sárga', 'Világosabb zöld', 'Türkiz', 'Égkék', 'Szilvakék', '25%-os szürke',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rózsa', 'Rozsdabarna', 'Világos sárga', 'Világoszöld', 'Világos türkisz', 'Világoskék', 'Levendulakék', 'Fehér',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Át&tetsző', '&Auto', '&Több szín...', '&Alapértelmezett',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Háttér', '&Szín:', 'Pozíció', 'Háttér', 'Példaszöveg.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nincs', '&Kifeszített', '&Rögzített méret', '&Mozaik', '&Középre',
  // Padding button
  '&Kitöltés...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Kitöltő szín', '&Alkalmaz:', '&Több szín...', '&Kitöltés...',
  'Adja meg a színt',
  // [apply to:] text, paragraph, table, cell
  'szöveg', 'bekezdés' , 'tábla', 'cella',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Betűtípus', 'Betűtípus', 'Tájolás',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Betűtípus:', '&Méret', 'Stílus', '&Félkövér', '&Dőlt',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Szín:', 'Há&ttér:', '(Alapértelmezett)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekt', 'Aláhúzott', '&Föléhúzott', 'Á&thúzott', '&Nagybetűs',
  // Sample, Sample text
  'Példa', 'Példaszöveg',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Betűköz', '&Szóköz:', '&Ritkított', '&Sűrített',
  // Offset group-box, Offset label, Down, Up,
  'Eltolás', '&Eltolás:', '&Le', '&Fel',
  // Scaling group-box, Scaling
  'Méretarány', '&Arány:',
  // Sub/super script group box, Normal, Sub, Super
  'Alsó- és felsőindex', '&Normál', 'A&lsóindex', '&Felsőindex',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Kitöltés', '&Fenn:', '&Bal:', '&Lenn:', '&Jobb:', '&Egyenlő értékek',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Hivatkozás beszúrása', 'Hivatkozás', '&Szöveg:', '&Cél', '<<kiválasztva>>',
  // cannot open URL
  'Nem tudok "%s" helyre navigálni',
  // hyperlink properties button, hyperlink style
  '&Egyedi...', '&Stílus:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) nolors
  'Hyperlink tulajdonságok', 'Normál színek', 'Aktív színek',
  // Text [color], Background [color]
  '&Szöveg:', '&Háttér',
  // Underline [color]
  '&Aláhúzás:', 
  // Text [color], Background [color] (different hotkeys)
  'S&zöveg:', 'Há&ttér',
  // Underline [color], Attributes group-box,
  'A&láhúzás:', 'Tulajdonságok',
  // Underline type: always, never, active (below the mouse)
  'Alá&húzás:', 'mindig', 'soha', 'csak ha aktív',
  // Same as normal check-box
  'Mint a &normál',
  // underline active links check-box
  'Aktív &hivatkozás aláhúzása',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Szimbólum beszúrása', '&Betűtípus:', '&Karakterbeállítás:', 'Unicode',
  // Unicode block
  '&Blokk:',
  // Character Code, Character Unicode Code, No Character
  'Karakterkód: %d', 'Karakterkód: Unicode %d', '(Nincs karakter)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Tábla beszúrása', 'Tábla mérete', '&Oszlopok száma:', '&Sorok száma:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tábla méretezése', '&Automatikus méretezés', 'A tábla méretét az &ablakhoz igazítja', 'Táblaméret &kézi megadása',
  // Remember check-box
  'E&mlékezzen az új táblák méreteire',
  // VAlign Form ---------------------------------------------------------------
  'Pozíció',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Tulajdonságok', 'Kép', 'Pozíció és méret', 'Vonal', 'Tábla', 'Sorok', 'Cellák',
  // Image Appearance, Image Misc, Number tabs
  'Megjelenés', 'Egyebek', 'Számozás',
  // Box position, Box size, Box appearance tabs
  'Pozíció', 'Méret', 'Megjelenés',
  // Preview label, Transparency group-box, checkbox, label
  'Előkép:', 'Áttetsző', 'Á&ttetsző', 'Áttetsző &szín:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Módosít...', '&Mentés...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline top, bottom, middle
  'Függőleges elrendezés', '&Igazítás:',
  'alját a szöveg alapvonalához', 'közepét a szöveg alapvonalához',
  'tetejét a sor tetejéhez', 'alját a sor aljához', 'közepét a sor közepéhez',
  // align to left side, align to right side
  'balra', 'jobbra',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Behúzás:', 'Ki&húzott', '&Szélesség:', '&Magasság:', 'Alapméret: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Rögzített méretarány', '&Alaphelyzet',
  // Inside the border, border, outside the border groupboxes
  'Kereten belül', 'Keret', 'Kereten kívül',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Kitöltés szín:', '&Belső margó:',
  // Border width, Border color labels
  'Keret &vastagság:', 'Keret &szín:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Vízszintes elosztás:', '&Fügőleges elosztás:',
  // Miscellaneous groupbox, Tooltip label
  'Egyebek', '&Elemleírás:',
  // web group-box, alt text
  'Web', '&Alternatív szöveg:',
  // Horz line group-box, color, width, style
  'Vízszintes vonal', '&Szín:', '&Szélesség:', '&Stílus:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tábla', '&Szélesség:', '&Kitöltőszín:', '&Cellabehúzás...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Vízszintes belső bargó:', '&Függőleges belső margó:', 'Vonal és kitöltés',
  // Visible table border sides button, auto width (in combobox), auto width label
  '&Látható szegélyek...', 'Automatikus', 'automatikus',
  // Keep row content together checkbox
  '&Oldaltörés soron belül nem engedélyezett',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Elforgatás', '&Nincs', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Szegély:', '&Látható szegélyek...',
  // Border, CellBorders buttons
  '&Tábla szegély...', '&Cella szegély...',
  // Table border, Cell borders dialog titles
  'Tábla szegély', 'Alapértelmezett cella szegély',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Nyomtatás', '&A táblát külön oldalon hagyja', '&Fejlécsorok száma:',
  'A táblázat fejlécsoraink ismétlése minden oldalon',
  // top, center, bottom, default
  '&Felső', '&Középső', '&Alsó', '&Alapértelmezett',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Beállítások', 'Támogatott &szélesség:', '&Minimális magasság:', '&Kitöltő szín:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Szélek', 'Látható oldalak:',  'Ár&nyékszín:', '&Világos szín', '&Szín:',
  // Background image
  '&Kép...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Vízszintes pozíció', 'Függőleges pozíció', 'Szöveg pozíció',
  // Position types: align, absolute, relative
  'Igazítás szerint', 'Abszolút pozíció', 'Relatív pozíció',
  // [Align] left side, center, right side
  'bal oldal az alább kijelölt bal oldalához',
  'középvonal az alább kijelölt középvonalához',
  'jobb oldal az alább kijelölt jobb oldalához',
  // [Align] top side, center, bottom side
  'felső oldal az alább kijelölt felső oldalához',
  'középvonal az alább kijelölt középvonalához',
  'alsó oldal az alább kijelölt alsó oldalához',
  // [Align] relative to
  'az alább kijelölthöz képest',
  // [Position] to the right of the left side of
  'az alább kijelölt bal oldalához képest',
  // [Position] below the top side of
  'az alább kijelölt felső széléhez képest',
  // Anchors: page, main text area (x2)
  '&Oldal', '&Szöveg területe', 'O&ldal', 'Szöveg &területe',
  // Anchors: left margin, right margin
  '&Bal margó', '&Jobb margó',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Felső margó', '&Alsó margó', 'B&első margó', '&Outer margin',
  // Anchors: character, line, paragraph
  '&Karakter', '&Sor', 'Beke&zdés',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&Tükrözött margók', '&Elrendezés a tábla cellájához viszonyítva.',
  // Above text, below text
  '&Szöveg felett', '&Szöveg mögött',
  // Width, Height groupboxes, Width, Height labels
  'Szélesség', 'Magasság', '&Szélesség:', '&Magasság:',
  // Auto height label, Auto height combobox item
  'Automatikus', 'automatikus',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Függőleges igazítás', '&Felül', '&Középen', '&Alul',
  // Border and background button and title
  '&Szegély és háttér...', 'Szövegdoboz szegélye és háttere',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Irányított beillesztés', '&Beillesztés, mint:', 'Rich text formátum', 'HTML formátum',
  'Szöveg', 'Unicode szöveg', 'Bitmap kép', 'WMF kép',
  'Grafikus fájl',  'hiperlink',
  // Options group-box, styles
  'Egyéb lehetőségek', '&Stílusok:',
  // style options: apply target styles, use source styles, ignore styles
  'a cél dokumentum stílusának alkalmazása', 'stílus és megjelenés megtartása',
  'megjelenés megtartása a stílusok megőrzése nélkül',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Felsorolás és számozás', 'Felsorolás', 'Számozás', 'Felsoroló lista', 'Számozó lista',
  // Customize, Reset, None
  '&Egyedi...', '&Alapbeállítás', 'Nincs',
  // Numbering: continue, reset to, create new
  'Számozás folytatása', 'Számozás nullázása', 'Új lista létrehozása, kezdése ',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Nagybetű', 'Nagybetűs római', 'Szám', 'Kisbetű', 'Kisbetűs római',
  // Bullet type
  'Kör', 'Pont', 'Négyzet',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Szint:', '&Kezdet:', '&Folytat', 'Számozás',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Egyedi lista', 'Szintek', '&Sorszám:', 'Lista tulajdonság', '&Lista típusa:',
  // List types: bullet, image
  'felsoroló', 'kép', 'Szám beszúrása|',
  // Number format, Number, Start level from, Font button
  '&Számformátum:', 'Sorszám', '&Kezdőszint sorszámának kezdése:', '&Betűtípus...',
  // Image button, bullet character, Bullet button,
  '&Kép...', 'Felsorolás &jel', '&Felsoroló...',
  // Position of list text, bullet, number, image, text
  'Listaszöveg pozíció', 'Felsorolás jel pozíciója', 'Sorszám pozíciója', 'Kép pozíciója', 'Szöveg pozíciója',
  // at, left indent, first line indent, from left indent
  '&tól:', '&Bal behúzás:', '&Első sor behúzása:', 'balról behúzás',
  // [only] one level preview, preview
  '&Egyszintű nézet', 'Nézet',
  // Preview text
  'Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'igazítás balra', 'igazítás jobbra', 'középre',
  // level #, this level (for combo-box)
  '%d szint', 'Ez a szint',
  // Marker character dialog title
  'Felsorolás jel szerkesztése',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Bekezdés szegélye és háttere', 'Szegély', 'Háttér',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Beállítások', '&Szín:', '&Szélesség:', '&Belső szélesség:', '&Eltolás...',
  // Sample, Border type group-boxes;
  'Példa', 'Szegélytípus',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nincs', '&Egyvonalas', '&Kétvonalas', '&Háromvonalas', '&Belül vastag', '&Kívül vastag',
  // Fill color group-box; More colors, padding buttons
  'Kitöltő szín', '&Több szín...', '&Kitöltés...',
  // preview text
  'Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg.',
  // title and group-box in Offsets dialog
  'Eltolás', 'Szegély eltolás',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Bekezdés', 'Igazítás', '&Bal', '&Jobb', '&Fenn', '&Lenn',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Szóköz', '&Előtt:', '&Után:', 'Sor &távolság:', 'Tó&l:',
  // Indents group-box; indents: left, right, first line
  'Behúzás', 'Ba&l:', 'J&obb:', '&Első sor:',
  // indented, hanging
  '&Behúzott', '&Kihúzott', 'Példa',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Szimpla', 'Másfeles', 'Dupla',  'Legalább', 'Pontosan', 'Változó',
  // preview text
  'Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg. Szöveg szöveg szöveg szöveg szöveg.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Behúzás és távolság', 'Tabulátorok', 'Szövegbeosztás',
  // tab stop position label; buttons: set, delete, delete all
  '&Tab pozíciók:', '&Beállít', '&Töröl', 'Töröl &mind',
  // tab align group; left, right, center aligns,
  'Igazítás', '&Bal', '&Jobb', '&Közép',
  // leader radio-group; no leader
  'Kitöltés', '(Nincs)',
  // tab stops to be deleted, delete none, delete all labels
  'Törlés megállítása a taboknál:', '(Nincs)', 'Mind.',
  // Pagination group-box, keep with next, keep lines together
  'Tördelés', '&Együtt a következővel', 'Egy &oldalra',
  // Outline level, Body text, Level #
  '&Vázlat szint:', 'Összes szint', '%d. szint',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Nyomtatási kép', 'Széles oldal', 'Teljes oldal', 'Oldalak:',
  // Invalid Scale, [page #] of #
  'Kérem adjon meg egy 10 és 500 közötti számot', 'of %d',
  // Hints on buttons: first, prior, next, last pages
  'Első oldal', 'Előző oldal', 'Következő oldal', 'Utolsó oldal',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Cellaköz', 'Szóköz', '&Cellák között', 'A táblaszélektől a cellákhoz',
  // vertical, horizontal (x2)
  '&Függőleges:', '&Vízszintes:', 'Fü&ggőleges:', 'Ví&zszintes:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Szélek', 'Beállítások', '&Szín:', '&Jelölt szín:', 'Árnyék s&zín:',
  // Width; Border type group-box;
  '&Szélesség:', 'Széltípus',
  // Border types: none, sunken, raised, flat
  '&Nincs', '&Süllyesztett', '&Kiemelt', '&Sík',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Cellák felosztása', 'Felosztás a',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Megadott oszlop és sor szerint', '&Eredeti cellák',
  // number of columns, rows, merge before
  'Oszlopok &száma:', 'Sorok s&záma:', '&Egyesítés a szétvágás előtt',
  // to original cols, rows check-boxes
  'Szétvágás eredeti &oszlopokra', 'Szétvágás eredeti so&rokra',
  // Add Rows form -------------------------------------------------------------
  'Sor hozzáadása', 'Sorok &száma:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Oldalbeállítás', 'Oldal', 'Fejéc és lábléc',
  // margins group-box, left, right, top, bottom
  'Margók (milliméter)', 'Margók (inch)', '&Bal:', '&Felső:', '&Jobb:', '&Alsó:',
  // mirror margins check-box
  '&Margók tükrözése',
  // orientation group-box, portrait, landscape
  'Tájolás', '&Álló', '&Fekvő',
  // paper group-box, paper size, default paper source
  'Papír', '&Méret:', '&Forrás:',
  // header group-box, print on the first page, font button
  'Fejléc', '&Szöveg:', '&Fejléc az első oldalon', '&Betűtípus...',
  // header alignments: left, center, right
  '&Balra', '&Középre', '&Jobbra',
  // the same for footer (different hotkeys)
  'Lábléc', '&Szöveg:', 'Lábléc az első o&ldalon', 'B&etűtípus...',
  'B&alra', 'Kö&zépre', 'J&obbra',
  // page numbers group-box, start from
  'oldalszámok', '&Kezdősorszám:',
  // hint about codes
  'Speciális karakterkombináció:'#13'&&p - oldalszám; &&P - oldalak száma; &&d - aktuális dátum; &&t - aktuális idő.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Szöveg fájl kódlap', '&Kódolás kiválasztása:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japán', 'Kínai (egyszerű)', 'Koreai',
  // chinese (trad), central european, cyrillic, west european
  'Kínai (tradícionális)', 'Közép és kelet európai', 'Cyrill', 'Nyugat európai',
  // greek, turkish, hebrew, arabic
  'Görög', 'Török', 'Héber', 'Arab',
  // baltic, vietnamese, utf-8, utf-16
  'Balti', 'Vietnámi', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vizuális megjelenés', '&Stílus:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Szöveggé konvertálás', 'Elválasztó &kijelölése:',
  // line break, tab, ';', ','
  'sortörés', 'tabulátor', 'pontosvessző', 'vessző',
  // Table sort form -----------------------------------------------------------
  // error message
  'A table containing merged rows cannot be sorted',
  // title, main options groupbox
  'Táblázat rendezése', 'Rendezés',
  // sort by column, case sensitive
  '&Rendezés oszlopa:', '&Kis- nagybetű érzékeny',
  // heading row, range or rows
  'Van &fejléc', 'Rendezett sorok: %d-%d',
  // order, ascending, descending
  'Rendezés', '&Növekvő', '&Csökkenő',
  // data type, text, number
  'Típus', '&Szöveg', 'Szá&m',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Szám beszúrása', 'Tulajdonság', 'Számláló neve:', '&Számozás &típusa:',
  // numbering groupbox, continue, start from
  'Számozás', '&Folytatólagos', 'Ú&j kezdő sorszám:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Felirat', '&Címke:', 'A &címke szöveg ne szerepeljen a feliratban',
  // position radiogroup
  'Pozíció', 'Kijelölt objektum &felett', 'Kijelölt objektum &alatt',
  // caption text
  'Felirat &szöveg:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Számozás', 'Ábra', 'Táblázat',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Látható szegélyek', 'Szegély',
  // Left, Top, Right, Bottom checkboxes
  '&Bal oldal', '&Felső', '&Jobb oldal', '&Alsó',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Táblaoszlopok átméretezése', 'Táblasorok átméretezése',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Első sor behúzása', 'Bal behúzás', 'Függő behúzás', 'Jobb behúzás',
  // Hints on lists: up one level (left), down one level (right)
  'Fel egy szinttel', 'Le egy szinttel',
  // Hints for margins: bottom, left, right and top
  'Alsó margó', 'Bal margó', 'Jobb margó', 'Felső margó',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Balra igazított tabulátor', 'Jobbra igazított tabulátor', 'Középre igazított tabulátor', 'Tizedesjelhez igazított tabulátor',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normál', 'Normál behúzással', 'Nincs térköz', 'Címsor %d', 'Listaszerű bekezdés',
  // Hyperlink, Title, Subtitle
  'Hiperhivatkozás', 'Cím', 'Aláírás',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Kiemelés', 'Finom kiemelés', 'Erős kiemelés', 'Erős hangsúlyozás',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Idézet', 'Kiemelt idézet', 'Finom hivatkozás', 'Erős hivatkozás',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blokk szöveg', 'HTML-változó', 'HTML-kód', 'HTML-mozaikszó',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-definíció', 'HTML-billentyűzet', 'HTML-példa', 'HTML-írógép',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML-ként formázott', 'HTML-idézet', 'Élőfej', 'Élőláb', 'Oldalszám',
  // Caption
  'Felirat',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Jegyzet hivatkozás', 'Lábjegyzet-hivatkozás', 'Jegyzetszöveg', 'Lábjegyzetszöveg',
  // Sidenote Reference, Sidenote Text
  'Széljegyzet hivatkozás', 'Széljegyzet szöveg',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'szín', 'háttér szín', 'átlátszó', 'alapértelmezett', 'aláhúzás szín',
  // default background color, default text color, [color] same as text
  'alapértelmezett háttér szín', 'alapértelmezett szöveg szín', 'same as text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'vékony vonal', 'vastag vonal', 'dupla vonal', 'pontozott', 'vastag pontozott', 'szaggatott',
  // underline types: thick dashed, long dashed, thick long dashed,
  'vastag szaggatott', 'hosszú szaggatott', 'vastag hosszú szaggatott',
  // underline types: dash dotted, thick dash dotted,
  'szaggatott pontozott', 'vastag szaggatott pontozott',
  // underline types: dash dot dotted, thick dash dot dotted
  'szaggatott dupla pontozott', 'vastag szaggatott dupla pontozott',
  // sub/superscript: not, subsript, superscript
  'nem alsó/felsőindexelt', 'alsóindex', 'felsőindex',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mód:', 'örökölt', 'balról jobbra', 'jobbról balra',
  // bold, not bold
  'vastag', 'nem vastag',
  // italic, not italic
  'dőlt', 'nem dőlt',
  // underlined, not underlined, default underline
  'aláhúzott', 'nem aláhúzott', 'alapértelmezett aláhúzás',
  // struck out, not struck out
  'vésett', 'nem vésett',
  // overlined, not overlined
  'domború', 'nem domború',
  // all capitals: yes, all capitals: no
  'nagybetűs', 'kisbetűs',
  // vertical shift: none, by x% up, by x% down
  'függőleges eltolás nélkül', 'felemelve %d%%-kal', 'leejtve %d%%-kal',
  // characters width [: x%]
  'méretarány',
  // character spacing: none, expanded by x, condensed by x
  'normal térköz', 'ritkított %s', 'sűrített %s',
  // charset
  'írásrendszer',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'alapértelmezett betűtípus', 'alapértelmezett bekezdés', 'örökölt',
  // [hyperlink] highlight, default hyperlink attributes
  'kiemelt', 'alapértelmezett',
  // paragraph aligmnment: left, right, center, justify
  'balra igazított', 'jobbra igazított', 'középre igazított', 'sorkizárt',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'sor magasság: %d%%', 'sorköz: %s', 'sorköz: legalább %s',
  'sorköz: pontosan %s',
  // no wrap, wrap
  'sortörés kizárva', 'sortörés',
  // keep lines together: yes, no
  'egy oldalra', 'sorokat nem kell együtt tartani',
  // keep with next: yes, no
  'együtt a következővel', 'bekezdéseket nem kell együtt tartani',
  // read only: yes, no
  'csak olvasható', 'szerkeszthető',
  // body text, heading level x
  'vázlat szint: szövegtörzs', 'vázlat szint: %d',
  // indents: first line, left, right
  'első sor behúzása', 'bal behúzás', 'jobb behúzás',
  // space before, after
  'térköz előtte', 'térköz utána',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulátorok', 'nincs', 'igazítás', 'bal', 'jobb', 'közép',
  // tab leader (filling character)
  'kitöltés',
  // no, yes
  'nem', 'igen',
  // [padding/spacing/side:] left, top, right, bottom
  'bal', 'felső', 'jobb', 'alsó',
  // border: none, single, double, triple, thick inside, thick outside
  'nincs', 'sima', 'dupla', 'tripla', 'vastag belső', 'vastag külső',
  // background, border, padding, spacing [between text and border]
  'háttér', 'szegély', 'távolság', 'térköz',
  // border: style, width, internal width, visible sides
  'stílus', 'szélesség', 'belső szélesség', 'látható oldalak',
  // style inspector -----------------------------------------------------------
  // title
  'Stílus böngésző',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stílus', '(Nincs)', 'Bekezdés', 'Betűtípus', 'Tulajdonságok',
  // border and background, hyperlink, standard [style]
  'Szegély és háttér', 'Hiperhivatkozás', '(Normál)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stílusok', 'Stílus',
  // name, applicable to,
  '&Név:', '&Hatókör:',
  // based on, based on (no &),
  'Stílus &alapja:', 'Stílus alapja:',
  //  next style, next style (no &)
  'Következő &bekezdés stílusa:', 'Következő bekezdés stílusa:',
  // quick access check-box, description and preview
  '&Gyors elérés', '&Leírás és előnézet:',
  // [applicable to:] paragraph and text, paragraph, text
  'bekezdés és szöveg', 'bekezdés', 'szöveg',
  // text and paragraph styles, default font
  'Szöveg és bekezdés stílusok', 'Alapértelmezett betűtípus',
  // links: edit, reset, buttons: add, delete
  'Módosítás', 'Alapértelmezett', 'Ú&j', '&Törlés',
  // add standard style, add custom style, default style name
  'Új &Normál stílus...', 'Új &egyedi stílus', 'Stílus %d',
  // choose style
  'Stílus &választás:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView stílusok (*.rvst)|*.rvst', 'Hiba a betöltés közben', 'Ez a fájl nem tartalmaz stílusokat',
  // Title, group-box, import styles
  'Stílus importálása fájlból', 'Import', 'Stílus i&mportálása:',
  // existing styles radio-group: Title, override, auto-rename
  'Meglévő stílusok', '&felülír', '&hozzáad átnevezve',
  // Select, Unselect, Invert,
  '&Kijelöl', '&Töröl', '&Megfordít',
  // select/unselect: all styles, new styles, existing styles
  '&Minden stílus', 'Ú&j stílusok', '&Létező stílusok',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Formázás törlése', 'Minden stílus',
  // dialog title, prompt
  'Stílusok', 'Stílus &kiválasztása:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Szinonímák', '&Kihagy mind', '&Hozzáad a szótárhoz',
  // Progress messages ---------------------------------------------------------
  'letöltés %s', 'Nyomtatás előkészítése...', '%d oldal nyomtatása',
  'Konvertálás RTF formátumba...',  'Konvertálás RTF formátumból...',
  '%s olvasása...', '%s írása...', 'szöveg',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Nincs jegyzet', 'Lábjegyzet', 'Végjegyzet', 'Széljegyzet', 'Szövegdoboz'
  );


initialization
  RVA_RegisterLanguage(
    'Hungarian',  // english language name, do not translate
    'Magyar', // native language name
    EASTEUROPE_CHARSET, @Messages);

end.