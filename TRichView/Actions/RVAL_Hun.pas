
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Hungarian translation                           }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated: Gabor Berenyi <gberenyi@csdonline.hu>     }
{ Updated: 2007-01-02 by B�g�ncs S�ndor                 }
{ Updated: 2007-12-31 by Gabor Berenyi                  }
{ Updated: 2011-02-11 by B�g�ncs S�ndor                 }
{ Updated: 2011-10-28 by B�g�ncs S�ndor                 }
{ Updated: 2012-08-22 by B�g�ncs S�ndor                 }
{ Updated: 2014-02-01 by B�g�ncs S�ndor                 }
{*******************************************************}

unit RVAL_Hun;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, mm, cm, picas, pixels, points
  'inch', 'mm', 'cm', 'pica pont', 'k�ppont', 'pont',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&F�jl', '&Szerkeszt�s', 'F&orm�z�s', 'F&ont', 'Be&kezd�s', '&Besz�r�s', '&T�bl�zat', '&Ablak', '&Seg�ts�g',
  // exit
  '&Kil�p�s',
  // top-level menus: View, Tools,
  '&N�zet', '&Seg�deszk�z�k',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'M�ret', 'St�lus', 'Kiv�lasz&t', 'A cella tartalm�nak &igaz�t�sa', 'Ce&llaszeg�lyek',
  // menus: Table cell rotation
  'Cella &forgat�s', 
  // menus: Text flow, Footnotes/endnotes
  '&Sz�veg ir�ny', '&L�bjegyzetek',
  // ribbon tabs: tab1, tab2, view, table
  '&Kezd�lap', '&Halad�', '&N�zet', '&T�bla',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'V�g�lap', 'Bet�t�pus', 'Bekezd�s', 'Felsorol�s', 'Szerkeszt�s',
  // ribbon groups: insert, background, page setup,
  'Besz�r�s', 'Oldalh�tt�r', 'Oldalbe�ll�t�s',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Hivatkoz�sok', 'L�bjegyzet', 'N�zetek', 'Mutat/Elrejt', 'Nagy�t�s', 'Opci�k',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Besz�r�s', 'T�rl�s', 'M�veletek', 'Keretek',
  // ribbon groups: styles 
  'St�lusok',
  // ribbon screen tip footer,
  'Nyomjon F1-et a tov�bbi seg�ts�g�rt',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Legut�bbi dokumentumok', 'Dokumentum ment�se m�sk�nt', 'Dokumentum el�n�zet �s nyomtat�s',
  // ribbon label: units combo
  'Egys�g:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '�&j', '�j|�res dokumentum l�trehoz�sa',
  // TrvActionOpen
  'Meg&nyit...', 'Megnyit|Megl�v� dokumentum megnyit�sa',
  // TrvActionSave
  '&Ment�s', 'Ment�s|Dokumentum ment�se',
  // TrvActionSaveAs
  'Ment�&s m�sk�nt...', 'Ment�s m�sk�nt...|Dokumentum ment�se �j n�ven',
  // TrvActionExport
  '&Export...', 'Export|Export�l�s m�s t�pus� dokumentumk�nt',
  // TrvActionPrintPreview
  'Nyomtat�si k�&p', 'Nyomtat�si k�p|Dokumentum megjelen�t�se ahogy nyomtat�n megjelenik',
  // TrvActionPrint
  'Nyomt&at�s...', 'Nyomtat�s|Nyomtat�si opci�k be�ll�t�sa �s a dokumentum nyomtat�sa',
  // TrvActionQuickPrint
  '&Gyorsnyomtat�s', '&Gyorsnyomtat�s', 'Gyorsnyomtat�s|Dokuemntum k�ld�se a nyomtat�ra',
  // TrvActionPageSetup
  '&Oldalbe�ll�t�s...', 'Oldalbe�ll�t�s|Marg�k, pap�r m�ret, t�jol�s, fej- �s l�bl�cek be�ll�t�sa',
  // TrvActionCut
  '&Kiv�g', 'Kiv�g|Kijel�lt elemek �thelyez�se a v�g�lapra',
  // TrvActionCopy
  '&M�sol', 'M�sol|Kijel�lt elemek m�sol�sa a v�g�lapra',
  // TrvActionPaste
  '&Beilleszt', 'Beilleszt|Elemek beilleszt�se a v�g�lapr�l',
  // TrvActionPasteAsText
  'Beilleszt�s &sz�vegk�nt', 'Beilleszt�s sz�vegk�nt|A v�g�lap tartalm�nak beilleszt�se sz�vegk�nt',
  // TrvActionPasteSpecial
  'Ir�&ny�tott beilleszt�s...', 'Ir�ny�tott beilleszt�s|Elemek beilleszt�se  v�g�lapr�l alternat�v form�ban',
  // TrvActionSelectAll
  'Min&dent kijel�l', 'Mindent kijel�l|A teljes dokumentum kijel�l�se',
  // TrvActionUndo
  '&Visszavon�s', 'Visszavon�s|Utols� m�velet visszavon�sa',
  // TrvActionRedo
  '&Ism�tel', 'Ism�tel|Utols� visszavont m�velet ism�tl�se',
  // TrvActionFind
  '&Keres�s...', 'Keres�s|Megadott sz�veg keres�se a dokumentumban',
  // TrvActionFindNext
  'Keres�s &tov�bb', 'Keres�s tov�bb|Utols� keres�s folytat�sa',
  // TrvActionReplace
  '&Csere...', 'Csere|Megadott sz�veg keres�se �s cser�je egy megadottal',
  // TrvActionInsertFile
  '&F�jl...', 'F�jl besz�r�sa|F�jl tartalm�nak besz�r�sa a dokumentumba',
  // TrvActionInsertPicture
  '&K�p...', 'K�p besz�r�sa|K�p besz�r�sa a dokumentumba',
  // TRVActionInsertHLine
  '&V�zszintes vonal', 'V�zszintes vonal besz�r�sa|V�zszintes vonal besz�r�sa',
  // TRVActionInsertHyperlink
  '&Web-link...', 'Weblink besz�r�sa|Weblink besz�r�sa a dokumentumba',
  // TRVActionRemoveHyperlinks
  '&Hivatkoz�sok t�rl�se', 'Hivatkoz�sok t�rl�se|Elt�vol�tja az �sszes hivatkoz�st a kijel�lt sz�vegb�l',
  // TrvActionInsertSymbol
  '&Szimb�lum...', 'Szimb�lum besz�r�sa|Szimb�lum besz�r�sa',
  // TrvActionInsertNumber
  '&Sz�ml�l�...', 'Sz�m besz�r�sa|Sz�mok besz�r�sa',
  // TrvActionInsertCaption
  '&Felirat besz�r�sa...', 'Felirat besz�r�s|Besz�r egy feliratot a kiv�lasztott objektumhoz.',
  // TrvActionInsertTextBox
  '&Sz�vegdoboz', 'Sz�vegbox besz�r�sa|Sz�vegdobozt sz�r be.',
  // TrvActionInsertSidenote
  '&Sz�ljegyzet', 'Sz�ljegyzet besz�r�sa|Besz�r egy sz�vegdobozban megjelen� jegyzetet.',
  // TrvActionInsertPageNumber
  '&Oldalsz�m', 'Oldalsz�m besz�r�sa|Oldalsz�moz�st sz�r be',
  // TrvActionParaList
  '&Felsorol�s �s sz�moz�s...', 'Felsorol�s �s sz�moz�s|A kijel�lt bekezd�s �talak�t�sa, vagy szerkeszt�se felsorol�ss� vagy sorsz�moz�ss�',
  // TrvActionParaBullets
  '&Felsorol�s', 'Felsorol�s|Felsorol�s hozz�ad�sa vagy elv�tele a bekezd�sben',
  // TrvActionParaNumbering
  '&Sorsz�moz�s', 'Sorsz�moz�s|Sorsz�moz�s hozz�ad�sa vagy elv�tele a bekezd�sben',
  // TrvActionColor
  'H�tt�r &sz�ne...', 'H�tt�r sz�ne|A dokumentum h�tt�rsz�n�nek megv�ltoztat�sa',
  // TrvActionFillColor
  '&Kit�lt� sz�n...', 'Kit�lt� sz�n|A sz�veg, bekezd�s, cella, t�bla vagy dokumentum kit�lt� sz�n�nek a megv�ltoztat�sa',
  // TrvActionInsertPageBreak
  '&Oldalt�r�s besz�r�sa', 'Oldalt�r�s besz�r�sa|Oldalt�r�s besz�r�sa',
  // TrvActionRemovePageBreak
  'Oldalt�r�s &t�rl�s', 'Oldalt�r�s t�rl�s|Oldalt�r�s t�rl�s',
  // TrvActionClearLeft
  'Sz�vegbeoszt�s t�rl�se a &bal oldalon', 'Sz�vegbeoszt�s t�rl�se a bal oldalon|A bekezd�st a balra igaz�tott k�pek al� helyezi',
  // TrvActionClearRight
  'Sz�vegbeoszt�s t�rl�se a &jobb oldalon', 'Sz�vegbeoszt�s t�rl�se a jobb oldalon|A bekezd�st a jobbra igaz�tott k�pek al� helyezi',
  // TrvActionClearBoth
  'Sz�vegbeoszt�s t�rl�se a &mindk�t oldalon', 'Sz�vegbeoszt�s t�rl�se a mindk�t oldalon|A bekezd�st mind a balra, mind a jobbra igaz�tott k�pek al� helyezi',
  // TrvActionClearNone
  '&Norm�l sz�vegbeoszt�s', 'Norm�l sz�vegbeoszt�s|A bekezd�s k�rbefut mind a balra, mind a jobbra igaz�tott k�peken',
  // TrvActionVAlign
  '&Objektum elhelyez�se...', 'Objektum elhelyez�se|A kiv�lasztott objektum elhelyez�s�nek be�ll�t�sa',    
  // TrvActionItemProperties
  'Elemt&ulajdons�g...', 'Elemtulajdons�g|Aktu�lis elem tulajdons�gainak megv�ltoztat�sa',
  // TrvActionBackground
  '&H�tt�r...', 'H�tt�r|H�tt�r sz�n�nek �s k�p�nek megv�ltoztat�sa',
  // TrvActionParagraph
  '&Bekezd�s...', 'Bekezd�s|Bekezd�s tulajdons�gainak megv�ltoztat�sa',
  // TrvActionIndentInc
  'Beh�z�s &n�vel�se', 'Beh�z�s n�vel�se|A kijel�lt bekezd�sek beh�z�s�nak n�vel�se',
  // TrvActionIndentDec
  'Beh�z�s &cs�kkent�se', 'Beh�z�s cs�kkent�se|A kijel�lt bekezd�sek beh�z�s�nak cs�kent�se',
  // TrvActionWordWrap
  '&Sz�elv�laszt�s', 'Sz�elv�laszt�s|A kijel�lt bekezd�sben a sz�elv�laszt�s enged�lyez�se',
  // TrvActionAlignLeft
  'Igaz�t�s ba&lra', 'Igaz�t�s balra|Kijel�lt sz�vegek igaz�t�sa balra',
  // TrvActionAlignRight
  'Igaz�t�s &jobbra', 'Igaz�t�s jobbra|Kijel�lt sz�vegek igaz�t�sa jobbra',
  // TrvActionAlignCenter
  'Igaz�t�s &k�z�pre', 'Igaz�t�s k�z�pre|Kijel�lt sz�vegek igaz�t�sa k�z�pre',
  // TrvActionAlignJustify
  '&Sorkiz�r�s', 'Sorkiz�r�s|A kijel�lt sz�vegek elny�jt�sa a teljes sorban',
  // TrvActionParaColor
  'Bekezd�s &h�tt�rsz�ne...', 'Bekezd�s h�tt�rsz�ne|A bekezd�s h�tt�rsz�n�nek megv�ltoztat�sa',
  // TrvActionLineSpacing100
  '&Szimpla sork�z', 'Szimpla sork�z|Szimpla sork�z be�ll�t�sa',
  // TrvActionLineSpacing150
  '1.5 Sor&k�z', '1.5 Sork�z|M�sf�lszeres sork�z be�ll�t�sa',
  // TrvActionLineSpacing200
  '&Dupla Sork�z', 'Dupla Sork�z|Dupla sork�z be�ll�t�sa',
  // TrvActionParaBorder
  'Bekezd�s sz�lei �s &h�ttere...', 'Bekezd�s sz�lei �s h�ttere|A kijel�lt bekezd�sek sz�leinek �s h�tter�nek be�ll�t�sa',
  // TrvActionInsertTable
  '&T�bla besz�r�sa...', '&T�bla besz�r�sa', 'T�bla besz�r�sa|�j t�bla besz�r�sa',
  // TrvActionTableInsertRowsAbove
  'Sor besz�r�sa &el�', 'Sor besz�r�sa el�|A kijel�lt cella el� sz�rja be az �j sort',
  // TrvActionTableInsertRowsBelow
  'Sor besz�r�sa &m�g�', 'Sor besz�r�sa m�g�|A kijel�lt cella ut�n sz�rja be az �j sort',
  // TrvActionTableInsertColLeft
  'Oszlop besz�r�sa &balr�l', 'Oszlop besz�r�sa balr�l|A kijel�lt bal oldali cella el� sz�r be egy oszlopot',
  // TrvActionTableInsertColRight
  'Oszlop besz�r�sa &jobbr�l', 'Oszlop besz�r�sa jobbr�l|A kijel�lt jobb oldali cella ut�n sz�r be egy oszlopot',
  // TrvActionTableDeleteRows
  'Sor t�&rl�se', 'Sor t�rl�se|A kijel�lt cell�k sorainak t�rl�se',
  // TrvActionTableDeleteCols
  'Oszlop &t�rl�se', 'Oszlop t�rl�se|A kijel�lt cell�k oszlopainak t�rl�se',
  // TrvActionTableDeleteTable
  '&T�bla t�rl�se', 'T�bla t�rl�se|A kijel�lt t�bla t�rl�se',
  // TrvActionTableMergeCells
  '&Cellaegyes�t�s', 'Cellaeggyes�t�s|A kijel�lt cell�k egyes�t�se',
  // TrvActionTableSplitCells
  '&Cell�k feloszt�sa...', 'Cell�k feloszt�sa|A kijel�lt cell�k feloszt�sa t�bb �j cell�ra',
  // TrvActionTableSelectTable
  'T�bla &kiv�laszt�sa', 'T�bla kiv�laszt�sa|T�bla kiv�laszt�sa',
  // TrvActionTableSelectRows
  'So&r kiv�laszt�sa', 'Sor kiv�laszt�sa|Sor kiv�laszt�sa',
  // TrvActionTableSelectCols
  '&Oszlop kiv�laszt�sa', 'Oszlop kiv�laszt�sa|Oszlop kiv�laszt�sa',
  // TrvActionTableSelectCell
  'Ce&lla kiv�laszt�sa', 'Cella kiv�laszt�sa|Cella kiv�laszt�sa',
  // TrvActionTableCellVAlignTop
  'Cellaigaz�t�sa &fel�lre', 'Cellaigaz�t�sa fel�lre|A cell�k tartalm�nak igaz�t�sa a cell�k tetej�re',
  // TrvActionTableCellVAlignMiddle
  'Cellaigaz�t�sa &k�z�pre', 'Cellaigaz�t�s k�z�pre|A cell�k tartalm�nak igaz�t�sa k�z�pre',
  // TrvActionTableCellVAlignBottom
  'Cellaigaz�t�s &alulra', 'Cellaigaz�t�s alulra|A cell�k tartalm�nak igaz�t�sa alulra',
  // TrvActionTableCellVAlignDefault
  '&A cell�k alap�rtelmezett f�gg�leges igaz�t�sa', 'A cell�k alap�rtelmezett f�gg�leges igaz�t�sa|A kijel�lt cell�kra be�ll�tja az alap�rtelmezett igz�t�st',
  // TrvActionTableCellRotationNone
  '&Nincs cella forgat�s', 'Nincs cella forgat�s|Cella tartalm�nak forgat�sa 0�-kal',
  // TrvActionTableCellRotation90
  '&90�-os cella forgat�s', '90�-os cella forgat�s|Cella tartalm�nak forgat�sa 90�-kal',
  // TrvActionTableCellRotation180
  '&180�-os cella forgat�s', '180�-os cella forgat�s|Cella tartalm�nak forgat�sa 180�-kal',
  // TrvActionTableCellRotation270
  '&270�-os cella forgat�s', '270�-os cella forgat�s|Cella tartalm�nak forgat�sa 270�-kal',
  // TrvActionTableProperties
  'T�bla &tulajdons�gai...', 'T�bla tulajdons�gai|A kijel�lt t�bla tulajdons�gai m�dos�thatjuk',
  // TrvActionTableGrid
  'Cella&hat�rol�k megjelen�t�se', 'Cellahat�rol�k megjelen�t�se|Megjelen�ti, vagy elt�nteti a cell�kat hat�rol� vonalakat',
  // TRVActionTableSplit
  'T�bl�zat fe&loszt�sa', 'T�bl�zat feloszt�sa|Kett�osztja a t�bl�zatot a kiv�lasztott sort�l kezdve',
  // TRVActionTableToText
  'T�bl�zat s&z�vegg�...', 'T�bl�zat sz�vegg�|A t�bl��zatot sz�vegg� kovent�lja',
  // TRVActionTableSort
  '&Rendez�s...', 'Rendez�s|A t�bl�zat sorainak rendez�se',
  // TrvActionTableCellLeftBorder
  '&Bal szeg�ly', 'Bal szeg�ly|Megjelen�ti, vagy elt�nteti a bal cellaszeg�lyt',
  // TrvActionTableCellRightBorder
  '&Jobb szeg�ly', 'Jobb szeg�ly|Megjelen�ti, vagy elt�nteti a jobb cellaszeg�lyt',
  // TrvActionTableCellTopBorder
  '&Fels� szeg�ly', 'Fels� szeg�ly|Megjelen�ti, vagy elt�nteti a fels� cellaszeg�lyt',
  // TrvActionTableCellBottomBorder
  '&Als� szeg�ly', 'Als� szeg�ly|Megjelen�ti, vagy elt�nteti az als� cellaszeg�lyt',
  // TrvActionTableCellAllBorders
  '&Minden szeg�ly', 'Minden szeg�ly|Megjelen�ti, vagy elt�nteti a cellszeg�lyt',
  // TrvActionTableCellNoBorders
  '&Nincs szeg�ly', 'Nincs szeg�ly|Elt�nteti az �sszes cellaszeg�lyt',
  // TrvActionFonts & TrvActionFontEx
  '&Bet�t�pus...', 'Bet�t�pus|A kijel�lt sz�veg bet�t�pus�nak megv�ltoztat�sa',
  // TrvActionFontBold
  '&F�lk�v�r', 'F�lk�v�r|A f�lk�v�rs�g st�lus�nak be�ll�t�sa a kijel�lt sz�vegen',
  // TrvActionFontItalic
  '&D�lt', 'D�lt|A kijel�lt sz�veg bet�t�pus�nak �talak�t�sa d�lt bet�v�',
  // TrvActionFontUnderline
  '&Al�h�zott', 'Al�h�zott|A kijel�lt sz�veg bet�t�pus�nak �talak�t�sa al�h�zott bet�v�',
  // TrvActionFontStrikeout
  '�&th�zott', '�th�zott|A kijel�lt sz�veg �th�z�sa',
  // TrvActionFontGrow
  'Bet� &nagyobb�t�sa', 'Bet� nagyobb�t�sa|A kijel�lt sz�veg nagyobb�t�sa 10%-al',
  // TrvActionFontShrink
  'Bet� &kicsin�t�se', 'Bet� kicsiny�t�se|A kijel�lt sz�veg kicsiny�t�se 10%-al',
  // TrvActionFontGrowOnePoint
  'Bet�m�ret n�&vel�se 1 ponttal', 'Bet�m�ret n�vel�se 1 ponttal|A kiv�lasztott sz�veg bet�t�pus nagys�g�nk n�vel�se 1 ponttal',
  // TrvActionFontShrinkOnePoint
  'Bet�m�ret c&s�kkent�se 1 ponttal', 'Bet�m�ret cs�kkent�se 1 ponttal|A kiv�lasztott sz�veg bet�t�pus nagys�g�nak cs�kkent�se 1 ponttal',
  // TrvActionFontAllCaps
  'Mind &nagybet�', 'Mind nagybet�|A kiv�lasztott sz�veg valamennyi bet�j�t nagybet�ss� alak�tja',
  // TrvActionFontOverline
  '�&th�z�s', '�th�z�s|A kijel�lt sz�veget v�zszintes vonallal �th�zza',
  // TrvActionFontColor
  'Sz�veg &sz�ne...', 'Sz�veg sz�ne|Megv�ltoztatja a kijel�lt sz�veg sz�n�t',
  // TrvActionFontBackColor
  'Sz�veg &h�tt�rsz�ne...', 'Sz�veg h�tt�rsz�ne|Megv�ltoztatja a kijel�lt sz�veg h�tt�rsz�n�t',
  // TrvActionAddictSpell3
  '&Helyes�r�s ellen�rz�s', 'Helyes�r�s ellen�rz�s|Ellen�rzi a kijel�lt sz�veg helyess�g�t',
  // TrvActionAddictThesaurus3
  '&Sz�szedet', 'Sz�szedet|Megjelen�ti a kiv�lasztott sz� szinonim�it',
  // TrvActionParaLTR
  'Balr�l jobbra', 'Balr�l jobbra|Balr�l jobbra �ll�tja �t a sz�vegir�nyt a kijel�lt bekezd�sben',
  // TrvActionParaRTL
  'Jobbr�l balra', 'Jobbr�l balra|Jobbr�l balra �ll�tj �t a sz�vegir�nyt a kijel�lt bekezd�sben',
  // TrvActionLTR
  'Sz�veg balr�l jobbra', 'Sz�veg balr�l jobbra|Balr�l jobbra �ll�tja �t a sz�vegir�nyt a kijel�lt sz�vegben',
  // TrvActionRTL
  'Sz�veg jobbr�l balra', 'Sz�veg jobbr�l balra|Jobbr�l balra �ll�tja �t a sz�vegir�nyt a kijel�lt sz�vegben',
  // TrvActionCharCase
  'Bet�v�lt�s', 'Bet�v�lt�s|Megv�ltoztatja a bet�t (kicsi, nagy) a kijel�lt sz�vegben',
  // TrvActionShowSpecialCharacters
  '&Nem nyomtathat� karakterek', 'Nem nyomtathat� karakterek|Megjelen�ti, vagy elt�nteti a nem nyomtathat� karaktereket, mint p�ld�ul bekezd�sjel�l�k, t�bl�zatok, sz�k�z�k',
  // TrvActionSubscript
  '&Als�index', 'Als�index|A kijel�lt sz�veg �talak�t�sa kism�ret�re a sz�veg alapvonala alatt',
  // TrvActionSuperscript
  'Fels�index', 'Fels�index|A kijel�lt sz�veg �talak�t�sa kism�ret�re a sz�veg alapvonala felett',
  // TrvActionInsertFootnote
  '&L�bjegyzet', 'L�bjegyzet|L�bjegyzet besz�r�sa',
  // TrvActionInsertEndnote
  '&V�gjegyzet', 'V�gjegyzet|V�gjegyzet besz�r�sa',
  // TrvActionEditNote
  'J&egyzet szerkeszt�se', 'Jegyzet szerkeszt�se|L�bjegyzet vagy v�gjegyzet szerkeszt�se',
  // TrvActionHide
  '&Elrejt', 'Elrejt|Elrejti, vagy megmutatja a kiv�lasztott r�szt',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&St�lusok...', 'St�lusok|Megjelen�ti a st�lus be�ll�t�s ablakot',
  // TrvActionAddStyleTemplate
  '�&j st�lus...', '�j st�lus|�j sz�veg, vagy bekezd�s st�lus l�trehoz�sa',
  // TrvActionClearFormat,
  '&Form�z�s t�rl�se', 'Form�z�s t�rl�se|T�rli a kijel�lt r�sz �sszes sz�veg �s bekezd�s form�z�s�t',
  // TrvActionClearTextFormat,
  '&Sz�veg form�z�s t�rl�se', 'Sz�veg form�z�s t�rl�se|T�rli a kijel�lt sz�veg form�z�s�t',
  // TrvActionStyleInspector
  'St�lus &b�ng�sz�', 'St�lus b�ng�sz�|Megmutatja, vagy elrejti a st�lus b�ng�sz�t',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Rendben', 'M�gsem', 'Bez�r', 'Besz�r', '&Megnyit...', '&Ment�s...', '&T�r�l', 'S�g�', '&Elt�vol�t',
  // Others  -------------------------------------------------------------------
  // percents
  'sz�zal�k',
  // left, top, right, bottom sides
  'Bal oldal', 'Fels� oldal', 'Jobb oldal', 'Als� oldal',
  // save changes? confirm title
  'Mentsem a v�ltoz�sokat a: %s -ba?', 'J�v�hagy�s',
  // warning: losing formatting
  'A %s sz�mos megval�s�t�s nem kompatibilis azzal a form�tummal melybe menteni akarja.'#13+
  'Val�ban ebben a form�tumban k�v�nja menteni a dokumentumot?',
  // RVF format name
  'RichView form�tum',
  // Error messages ------------------------------------------------------------
  'Hiba',
  'Hiba az �llom�ny bet�lt�sekor.'#13#13'Lehets�ges megold�sok:'#13'- az �llom�ny form�tuma nem kompatibilis ezzel az alkalmaz�ssal;'#13+
  '- az �llom�ny s�r�lt;'#13'- az �llom�nyt egy m�sik alkalmaz�s nyitva tartja �s z�rolja.',
  'Hiba a k�p�llom�ny bet�lt�sekor.'#13#13'Lehets�ges megold�sok:'#13'- az �llom�ny k�pform�tumban van �s nem t�mogatja ez az alkalmaz�s;'#13+
  '- az �llom�ny nem tartalmazza a k�pet;'#13+
  '- az �llom�ny s�r�lt;'#13'- az �llom�nyt egy m�sik alkalmaz�s nyitva tartja �s z�rolja.',
  'Hiba az �llom�ny ment�sekor.'#13#13'Lehets�ges megold�sok:'#13'- nem �llrendelkez�sre a sz�ks�ges lemezter�let;'#13+
  '- a lemez �r�sv�dett;'#13'- az lemez nem tal�lhat� az olvas�ban;'#13+
  '- az �llom�nyt egy m�sik alkalmaz�s nyitva tartja �s z�rolja;'#13+'- a lemez hib�s.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView �llom�nyok (*.rvf)|*.rvf',  'RTF �llom�nyok (*.rtf)|*.rtf' , 'XML �llom�nyok (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Sz�veges �llom�nyok (*.txt)|*.txt', 'Unicode sz�veges �llom�nyok (*.txt)|*.txt', 'Sz�veges �llom�ny - Autofelismer�s (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML �llom�ny (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - egyszer� (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dokumentum (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Keres�s befejezve', 'A keresett ''%s'' sz�vegr�sz nem tal�lhat�.',
  // 1 string replaced; Several strings replaced
  '1 sz�vegr�sz cser�lve.', '%d sz�vegr�sz cser�lve.',
  // continue search from the beginning/end?
  'A dokumentum v�g�re �rtem, folytassam az elej�t�l?',
  'A dokumentum elej�re �rtem, folytassam a v�g�r�l?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  '�ttetsz�', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Fekete', 'Barna', 'Vil�gosz�ld', 'S�t�tz�ld', 'S�t�t p�va k�k', 'S�t�tk�k', 'Indig�k�k', '80%-os sz�rke',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'S�t�tv�r�s', 'Narancs', 'S�t�t s�rga', 'Z�ld', 'P�vak�k', 'K�k', 'K�kessz�rke', 'F�l sz�rke',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'V�r�s', 'Vil�gosnarancs', 'Lime', 'Tengerz�ld', 'Tengerk�k', 'Halv�nyk�k', 'Viola', '40%-os sz�rke',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'R�zsasz�n', 'Arany', 'S�rga', 'Vil�gosabb z�ld', 'T�rkiz', '�gk�k', 'Szilvak�k', '25%-os sz�rke',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'R�zsa', 'Rozsdabarna', 'Vil�gos s�rga', 'Vil�gosz�ld', 'Vil�gos t�rkisz', 'Vil�gosk�k', 'Levendulak�k', 'Feh�r',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '�t&tetsz�', '&Auto', '&T�bb sz�n...', '&Alap�rtelmezett',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'H�tt�r', '&Sz�n:', 'Poz�ci�', 'H�tt�r', 'P�ldasz�veg.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nincs', '&Kifesz�tett', '&R�gz�tett m�ret', '&Mozaik', '&K�z�pre',
  // Padding button
  '&Kit�lt�s...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Kit�lt� sz�n', '&Alkalmaz:', '&T�bb sz�n...', '&Kit�lt�s...',
  'Adja meg a sz�nt',
  // [apply to:] text, paragraph, table, cell
  'sz�veg', 'bekezd�s' , 't�bla', 'cella',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Bet�t�pus', 'Bet�t�pus', 'T�jol�s',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Bet�t�pus:', '&M�ret', 'St�lus', '&F�lk�v�r', '&D�lt',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Sz�n:', 'H�&tt�r:', '(Alap�rtelmezett)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekt', 'Al�h�zott', '&F�l�h�zott', '�&th�zott', '&Nagybet�s',
  // Sample, Sample text
  'P�lda', 'P�ldasz�veg',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Bet�k�z', '&Sz�k�z:', '&Ritk�tott', '&S�r�tett',
  // Offset group-box, Offset label, Down, Up,
  'Eltol�s', '&Eltol�s:', '&Le', '&Fel',
  // Scaling group-box, Scaling
  'M�retar�ny', '&Ar�ny:',
  // Sub/super script group box, Normal, Sub, Super
  'Als�- �s fels�index', '&Norm�l', 'A&ls�index', '&Fels�index',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Kit�lt�s', '&Fenn:', '&Bal:', '&Lenn:', '&Jobb:', '&Egyenl� �rt�kek',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Hivatkoz�s besz�r�sa', 'Hivatkoz�s', '&Sz�veg:', '&C�l', '<<kiv�lasztva>>',
  // cannot open URL
  'Nem tudok "%s" helyre navig�lni',
  // hyperlink properties button, hyperlink style
  '&Egyedi...', '&St�lus:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) nolors
  'Hyperlink tulajdons�gok', 'Norm�l sz�nek', 'Akt�v sz�nek',
  // Text [color], Background [color]
  '&Sz�veg:', '&H�tt�r',
  // Underline [color]
  '&Al�h�z�s:', 
  // Text [color], Background [color] (different hotkeys)
  'S&z�veg:', 'H�&tt�r',
  // Underline [color], Attributes group-box,
  'A&l�h�z�s:', 'Tulajdons�gok',
  // Underline type: always, never, active (below the mouse)
  'Al�&h�z�s:', 'mindig', 'soha', 'csak ha akt�v',
  // Same as normal check-box
  'Mint a &norm�l',
  // underline active links check-box
  'Akt�v &hivatkoz�s al�h�z�sa',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Szimb�lum besz�r�sa', '&Bet�t�pus:', '&Karakterbe�ll�t�s:', 'Unicode',
  // Unicode block
  '&Blokk:',
  // Character Code, Character Unicode Code, No Character
  'Karakterk�d: %d', 'Karakterk�d: Unicode %d', '(Nincs karakter)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'T�bla besz�r�sa', 'T�bla m�rete', '&Oszlopok sz�ma:', '&Sorok sz�ma:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'T�bla m�retez�se', '&Automatikus m�retez�s', 'A t�bla m�ret�t az &ablakhoz igaz�tja', 'T�blam�ret &k�zi megad�sa',
  // Remember check-box
  'E&ml�kezzen az �j t�bl�k m�reteire',
  // VAlign Form ---------------------------------------------------------------
  'Poz�ci�',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Tulajdons�gok', 'K�p', 'Poz�ci� �s m�ret', 'Vonal', 'T�bla', 'Sorok', 'Cell�k',
  // Image Appearance, Image Misc, Number tabs
  'Megjelen�s', 'Egyebek', 'Sz�moz�s',
  // Box position, Box size, Box appearance tabs
  'Poz�ci�', 'M�ret', 'Megjelen�s',
  // Preview label, Transparency group-box, checkbox, label
  'El�k�p:', '�ttetsz�', '�&ttetsz�', '�ttetsz� &sz�n:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&M�dos�t...', '&Ment�s...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline top, bottom, middle
  'F�gg�leges elrendez�s', '&Igaz�t�s:',
  'alj�t a sz�veg alapvonal�hoz', 'k�zep�t a sz�veg alapvonal�hoz',
  'tetej�t a sor tetej�hez', 'alj�t a sor alj�hoz', 'k�zep�t a sor k�zep�hez',
  // align to left side, align to right side
  'balra', 'jobbra',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Beh�z�s:', 'Ki&h�zott', '&Sz�less�g:', '&Magass�g:', 'Alapm�ret: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&R�gz�tett m�retar�ny', '&Alaphelyzet',
  // Inside the border, border, outside the border groupboxes
  'Kereten bel�l', 'Keret', 'Kereten k�v�l',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Kit�lt�s sz�n:', '&Bels� marg�:',
  // Border width, Border color labels
  'Keret &vastags�g:', 'Keret &sz�n:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&V�zszintes eloszt�s:', '&F�g�leges eloszt�s:',
  // Miscellaneous groupbox, Tooltip label
  'Egyebek', '&Elemle�r�s:',
  // web group-box, alt text
  'Web', '&Alternat�v sz�veg:',
  // Horz line group-box, color, width, style
  'V�zszintes vonal', '&Sz�n:', '&Sz�less�g:', '&St�lus:',
  // Table group-box; Table Width, Color, CellSpacing,
  'T�bla', '&Sz�less�g:', '&Kit�lt�sz�n:', '&Cellabeh�z�s...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&V�zszintes bels� barg�:', '&F�gg�leges bels� marg�:', 'Vonal �s kit�lt�s',
  // Visible table border sides button, auto width (in combobox), auto width label
  '&L�that� szeg�lyek...', 'Automatikus', 'automatikus',
  // Keep row content together checkbox
  '&Oldalt�r�s soron bel�l nem enged�lyezett',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Elforgat�s', '&Nincs', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Szeg�ly:', '&L�that� szeg�lyek...',
  // Border, CellBorders buttons
  '&T�bla szeg�ly...', '&Cella szeg�ly...',
  // Table border, Cell borders dialog titles
  'T�bla szeg�ly', 'Alap�rtelmezett cella szeg�ly',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Nyomtat�s', '&A t�bl�t k�l�n oldalon hagyja', '&Fejl�csorok sz�ma:',
  'A t�bl�zat fejl�csoraink ism�tl�se minden oldalon',
  // top, center, bottom, default
  '&Fels�', '&K�z�ps�', '&Als�', '&Alap�rtelmezett',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Be�ll�t�sok', 'T�mogatott &sz�less�g:', '&Minim�lis magass�g:', '&Kit�lt� sz�n:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Sz�lek', 'L�that� oldalak:',  '�r&ny�ksz�n:', '&Vil�gos sz�n', '&Sz�n:',
  // Background image
  '&K�p...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'V�zszintes poz�ci�', 'F�gg�leges poz�ci�', 'Sz�veg poz�ci�',
  // Position types: align, absolute, relative
  'Igaz�t�s szerint', 'Abszol�t poz�ci�', 'Relat�v poz�ci�',
  // [Align] left side, center, right side
  'bal oldal az al�bb kijel�lt bal oldal�hoz',
  'k�z�pvonal az al�bb kijel�lt k�z�pvonal�hoz',
  'jobb oldal az al�bb kijel�lt jobb oldal�hoz',
  // [Align] top side, center, bottom side
  'fels� oldal az al�bb kijel�lt fels� oldal�hoz',
  'k�z�pvonal az al�bb kijel�lt k�z�pvonal�hoz',
  'als� oldal az al�bb kijel�lt als� oldal�hoz',
  // [Align] relative to
  'az al�bb kijel�lth�z k�pest',
  // [Position] to the right of the left side of
  'az al�bb kijel�lt bal oldal�hoz k�pest',
  // [Position] below the top side of
  'az al�bb kijel�lt fels� sz�l�hez k�pest',
  // Anchors: page, main text area (x2)
  '&Oldal', '&Sz�veg ter�lete', 'O&ldal', 'Sz�veg &ter�lete',
  // Anchors: left margin, right margin
  '&Bal marg�', '&Jobb marg�',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Fels� marg�', '&Als� marg�', 'B&els� marg�', '&Outer margin',
  // Anchors: character, line, paragraph
  '&Karakter', '&Sor', 'Beke&zd�s',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&T�kr�z�tt marg�k', '&Elrendez�s a t�bla cell�j�hoz viszony�tva.',
  // Above text, below text
  '&Sz�veg felett', '&Sz�veg m�g�tt',
  // Width, Height groupboxes, Width, Height labels
  'Sz�less�g', 'Magass�g', '&Sz�less�g:', '&Magass�g:',
  // Auto height label, Auto height combobox item
  'Automatikus', 'automatikus',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'F�gg�leges igaz�t�s', '&Fel�l', '&K�z�pen', '&Alul',
  // Border and background button and title
  '&Szeg�ly �s h�tt�r...', 'Sz�vegdoboz szeg�lye �s h�ttere',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Ir�ny�tott beilleszt�s', '&Beilleszt�s, mint:', 'Rich text form�tum', 'HTML form�tum',
  'Sz�veg', 'Unicode sz�veg', 'Bitmap k�p', 'WMF k�p',
  'Grafikus f�jl',  'hiperlink',
  // Options group-box, styles
  'Egy�b lehet�s�gek', '&St�lusok:',
  // style options: apply target styles, use source styles, ignore styles
  'a c�l dokumentum st�lus�nak alkalmaz�sa', 'st�lus �s megjelen�s megtart�sa',
  'megjelen�s megtart�sa a st�lusok meg�rz�se n�lk�l',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Felsorol�s �s sz�moz�s', 'Felsorol�s', 'Sz�moz�s', 'Felsorol� lista', 'Sz�moz� lista',
  // Customize, Reset, None
  '&Egyedi...', '&Alapbe�ll�t�s', 'Nincs',
  // Numbering: continue, reset to, create new
  'Sz�moz�s folytat�sa', 'Sz�moz�s null�z�sa', '�j lista l�trehoz�sa, kezd�se ',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Nagybet�', 'Nagybet�s r�mai', 'Sz�m', 'Kisbet�', 'Kisbet�s r�mai',
  // Bullet type
  'K�r', 'Pont', 'N�gyzet',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Szint:', '&Kezdet:', '&Folytat', 'Sz�moz�s',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Egyedi lista', 'Szintek', '&Sorsz�m:', 'Lista tulajdons�g', '&Lista t�pusa:',
  // List types: bullet, image
  'felsorol�', 'k�p', 'Sz�m besz�r�sa|',
  // Number format, Number, Start level from, Font button
  '&Sz�mform�tum:', 'Sorsz�m', '&Kezd�szint sorsz�m�nak kezd�se:', '&Bet�t�pus...',
  // Image button, bullet character, Bullet button,
  '&K�p...', 'Felsorol�s &jel', '&Felsorol�...',
  // Position of list text, bullet, number, image, text
  'Listasz�veg poz�ci�', 'Felsorol�s jel poz�ci�ja', 'Sorsz�m poz�ci�ja', 'K�p poz�ci�ja', 'Sz�veg poz�ci�ja',
  // at, left indent, first line indent, from left indent
  '&t�l:', '&Bal beh�z�s:', '&Els� sor beh�z�sa:', 'balr�l beh�z�s',
  // [only] one level preview, preview
  '&Egyszint� n�zet', 'N�zet',
  // Preview text
  'Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'igaz�t�s balra', 'igaz�t�s jobbra', 'k�z�pre',
  // level #, this level (for combo-box)
  '%d szint', 'Ez a szint',
  // Marker character dialog title
  'Felsorol�s jel szerkeszt�se',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Bekezd�s szeg�lye �s h�ttere', 'Szeg�ly', 'H�tt�r',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Be�ll�t�sok', '&Sz�n:', '&Sz�less�g:', '&Bels� sz�less�g:', '&Eltol�s...',
  // Sample, Border type group-boxes;
  'P�lda', 'Szeg�lyt�pus',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nincs', '&Egyvonalas', '&K�tvonalas', '&H�romvonalas', '&Bel�l vastag', '&K�v�l vastag',
  // Fill color group-box; More colors, padding buttons
  'Kit�lt� sz�n', '&T�bb sz�n...', '&Kit�lt�s...',
  // preview text
  'Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg.',
  // title and group-box in Offsets dialog
  'Eltol�s', 'Szeg�ly eltol�s',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Bekezd�s', 'Igaz�t�s', '&Bal', '&Jobb', '&Fenn', '&Lenn',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Sz�k�z', '&El�tt:', '&Ut�n:', 'Sor &t�vols�g:', 'T�&l:',
  // Indents group-box; indents: left, right, first line
  'Beh�z�s', 'Ba&l:', 'J&obb:', '&Els� sor:',
  // indented, hanging
  '&Beh�zott', '&Kih�zott', 'P�lda',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Szimpla', 'M�sfeles', 'Dupla',  'Legal�bb', 'Pontosan', 'V�ltoz�',
  // preview text
  'Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg. Sz�veg sz�veg sz�veg sz�veg sz�veg.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Beh�z�s �s t�vols�g', 'Tabul�torok', 'Sz�vegbeoszt�s',
  // tab stop position label; buttons: set, delete, delete all
  '&Tab poz�ci�k:', '&Be�ll�t', '&T�r�l', 'T�r�l &mind',
  // tab align group; left, right, center aligns,
  'Igaz�t�s', '&Bal', '&Jobb', '&K�z�p',
  // leader radio-group; no leader
  'Kit�lt�s', '(Nincs)',
  // tab stops to be deleted, delete none, delete all labels
  'T�rl�s meg�ll�t�sa a tabokn�l:', '(Nincs)', 'Mind.',
  // Pagination group-box, keep with next, keep lines together
  'T�rdel�s', '&Egy�tt a k�vetkez�vel', 'Egy &oldalra',
  // Outline level, Body text, Level #
  '&V�zlat szint:', '�sszes szint', '%d. szint',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Nyomtat�si k�p', 'Sz�les oldal', 'Teljes oldal', 'Oldalak:',
  // Invalid Scale, [page #] of #
  'K�rem adjon meg egy 10 �s 500 k�z�tti sz�mot', 'of %d',
  // Hints on buttons: first, prior, next, last pages
  'Els� oldal', 'El�z� oldal', 'K�vetkez� oldal', 'Utols� oldal',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Cellak�z', 'Sz�k�z', '&Cell�k k�z�tt', 'A t�blasz�lekt�l a cell�khoz',
  // vertical, horizontal (x2)
  '&F�gg�leges:', '&V�zszintes:', 'F�&gg�leges:', 'V�&zszintes:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Sz�lek', 'Be�ll�t�sok', '&Sz�n:', '&Jel�lt sz�n:', '�rny�k s&z�n:',
  // Width; Border type group-box;
  '&Sz�less�g:', 'Sz�lt�pus',
  // Border types: none, sunken, raised, flat
  '&Nincs', '&S�llyesztett', '&Kiemelt', '&S�k',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Cell�k feloszt�sa', 'Feloszt�s a',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Megadott oszlop �s sor szerint', '&Eredeti cell�k',
  // number of columns, rows, merge before
  'Oszlopok &sz�ma:', 'Sorok s&z�ma:', '&Egyes�t�s a sz�tv�g�s el�tt',
  // to original cols, rows check-boxes
  'Sz�tv�g�s eredeti &oszlopokra', 'Sz�tv�g�s eredeti so&rokra',
  // Add Rows form -------------------------------------------------------------
  'Sor hozz�ad�sa', 'Sorok &sz�ma:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Oldalbe�ll�t�s', 'Oldal', 'Fej�c �s l�bl�c',
  // margins group-box, left, right, top, bottom
  'Marg�k (millim�ter)', 'Marg�k (inch)', '&Bal:', '&Fels�:', '&Jobb:', '&Als�:',
  // mirror margins check-box
  '&Marg�k t�kr�z�se',
  // orientation group-box, portrait, landscape
  'T�jol�s', '&�ll�', '&Fekv�',
  // paper group-box, paper size, default paper source
  'Pap�r', '&M�ret:', '&Forr�s:',
  // header group-box, print on the first page, font button
  'Fejl�c', '&Sz�veg:', '&Fejl�c az els� oldalon', '&Bet�t�pus...',
  // header alignments: left, center, right
  '&Balra', '&K�z�pre', '&Jobbra',
  // the same for footer (different hotkeys)
  'L�bl�c', '&Sz�veg:', 'L�bl�c az els� o&ldalon', 'B&et�t�pus...',
  'B&alra', 'K�&z�pre', 'J&obbra',
  // page numbers group-box, start from
  'oldalsz�mok', '&Kezd�sorsz�m:',
  // hint about codes
  'Speci�lis karakterkombin�ci�:'#13'&&p - oldalsz�m; &&P - oldalak sz�ma; &&d - aktu�lis d�tum; &&t - aktu�lis id�.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Sz�veg f�jl k�dlap', '&K�dol�s kiv�laszt�sa:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Jap�n', 'K�nai (egyszer�)', 'Koreai',
  // chinese (trad), central european, cyrillic, west european
  'K�nai (trad�cion�lis)', 'K�z�p �s kelet eur�pai', 'Cyrill', 'Nyugat eur�pai',
  // greek, turkish, hebrew, arabic
  'G�r�g', 'T�r�k', 'H�ber', 'Arab',
  // baltic, vietnamese, utf-8, utf-16
  'Balti', 'Vietn�mi', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vizu�lis megjelen�s', '&St�lus:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Sz�vegg� konvert�l�s', 'Elv�laszt� &kijel�l�se:',
  // line break, tab, ';', ','
  'sort�r�s', 'tabul�tor', 'pontosvessz�', 'vessz�',
  // Table sort form -----------------------------------------------------------
  // error message
  'A table containing merged rows cannot be sorted',
  // title, main options groupbox
  'T�bl�zat rendez�se', 'Rendez�s',
  // sort by column, case sensitive
  '&Rendez�s oszlopa:', '&Kis- nagybet� �rz�keny',
  // heading row, range or rows
  'Van &fejl�c', 'Rendezett sorok: %d-%d',
  // order, ascending, descending
  'Rendez�s', '&N�vekv�', '&Cs�kken�',
  // data type, text, number
  'T�pus', '&Sz�veg', 'Sz�&m',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Sz�m besz�r�sa', 'Tulajdons�g', 'Sz�ml�l� neve:', '&Sz�moz�s &t�pusa:',
  // numbering groupbox, continue, start from
  'Sz�moz�s', '&Folytat�lagos', '�&j kezd� sorsz�m:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Felirat', '&C�mke:', 'A &c�mke sz�veg ne szerepeljen a feliratban',
  // position radiogroup
  'Poz�ci�', 'Kijel�lt objektum &felett', 'Kijel�lt objektum &alatt',
  // caption text
  'Felirat &sz�veg:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Sz�moz�s', '�bra', 'T�bl�zat',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'L�that� szeg�lyek', 'Szeg�ly',
  // Left, Top, Right, Bottom checkboxes
  '&Bal oldal', '&Fels�', '&Jobb oldal', '&Als�',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'T�blaoszlopok �tm�retez�se', 'T�blasorok �tm�retez�se',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Els� sor beh�z�sa', 'Bal beh�z�s', 'F�gg� beh�z�s', 'Jobb beh�z�s',
  // Hints on lists: up one level (left), down one level (right)
  'Fel egy szinttel', 'Le egy szinttel',
  // Hints for margins: bottom, left, right and top
  'Als� marg�', 'Bal marg�', 'Jobb marg�', 'Fels� marg�',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Balra igaz�tott tabul�tor', 'Jobbra igaz�tott tabul�tor', 'K�z�pre igaz�tott tabul�tor', 'Tizedesjelhez igaz�tott tabul�tor',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Norm�l', 'Norm�l beh�z�ssal', 'Nincs t�rk�z', 'C�msor %d', 'Listaszer� bekezd�s',
  // Hyperlink, Title, Subtitle
  'Hiperhivatkoz�s', 'C�m', 'Al��r�s',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Kiemel�s', 'Finom kiemel�s', 'Er�s kiemel�s', 'Er�s hangs�lyoz�s',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Id�zet', 'Kiemelt id�zet', 'Finom hivatkoz�s', 'Er�s hivatkoz�s',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blokk sz�veg', 'HTML-v�ltoz�', 'HTML-k�d', 'HTML-mozaiksz�',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-defin�ci�', 'HTML-billenty�zet', 'HTML-p�lda', 'HTML-�r�g�p',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML-k�nt form�zott', 'HTML-id�zet', '�l�fej', '�l�l�b', 'Oldalsz�m',
  // Caption
  'Felirat',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Jegyzet hivatkoz�s', 'L�bjegyzet-hivatkoz�s', 'Jegyzetsz�veg', 'L�bjegyzetsz�veg',
  // Sidenote Reference, Sidenote Text
  'Sz�ljegyzet hivatkoz�s', 'Sz�ljegyzet sz�veg',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'sz�n', 'h�tt�r sz�n', '�tl�tsz�', 'alap�rtelmezett', 'al�h�z�s sz�n',
  // default background color, default text color, [color] same as text
  'alap�rtelmezett h�tt�r sz�n', 'alap�rtelmezett sz�veg sz�n', 'same as text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'v�kony vonal', 'vastag vonal', 'dupla vonal', 'pontozott', 'vastag pontozott', 'szaggatott',
  // underline types: thick dashed, long dashed, thick long dashed,
  'vastag szaggatott', 'hossz� szaggatott', 'vastag hossz� szaggatott',
  // underline types: dash dotted, thick dash dotted,
  'szaggatott pontozott', 'vastag szaggatott pontozott',
  // underline types: dash dot dotted, thick dash dot dotted
  'szaggatott dupla pontozott', 'vastag szaggatott dupla pontozott',
  // sub/superscript: not, subsript, superscript
  'nem als�/fels�indexelt', 'als�index', 'fels�index',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di m�d:', '�r�k�lt', 'balr�l jobbra', 'jobbr�l balra',
  // bold, not bold
  'vastag', 'nem vastag',
  // italic, not italic
  'd�lt', 'nem d�lt',
  // underlined, not underlined, default underline
  'al�h�zott', 'nem al�h�zott', 'alap�rtelmezett al�h�z�s',
  // struck out, not struck out
  'v�sett', 'nem v�sett',
  // overlined, not overlined
  'dombor�', 'nem dombor�',
  // all capitals: yes, all capitals: no
  'nagybet�s', 'kisbet�s',
  // vertical shift: none, by x% up, by x% down
  'f�gg�leges eltol�s n�lk�l', 'felemelve %d%%-kal', 'leejtve %d%%-kal',
  // characters width [: x%]
  'm�retar�ny',
  // character spacing: none, expanded by x, condensed by x
  'normal t�rk�z', 'ritk�tott %s', 's�r�tett %s',
  // charset
  '�r�srendszer',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'alap�rtelmezett bet�t�pus', 'alap�rtelmezett bekezd�s', '�r�k�lt',
  // [hyperlink] highlight, default hyperlink attributes
  'kiemelt', 'alap�rtelmezett',
  // paragraph aligmnment: left, right, center, justify
  'balra igaz�tott', 'jobbra igaz�tott', 'k�z�pre igaz�tott', 'sorkiz�rt',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'sor magass�g: %d%%', 'sork�z: %s', 'sork�z: legal�bb %s',
  'sork�z: pontosan %s',
  // no wrap, wrap
  'sort�r�s kiz�rva', 'sort�r�s',
  // keep lines together: yes, no
  'egy oldalra', 'sorokat nem kell egy�tt tartani',
  // keep with next: yes, no
  'egy�tt a k�vetkez�vel', 'bekezd�seket nem kell egy�tt tartani',
  // read only: yes, no
  'csak olvashat�', 'szerkeszthet�',
  // body text, heading level x
  'v�zlat szint: sz�vegt�rzs', 'v�zlat szint: %d',
  // indents: first line, left, right
  'els� sor beh�z�sa', 'bal beh�z�s', 'jobb beh�z�s',
  // space before, after
  't�rk�z el�tte', 't�rk�z ut�na',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabul�torok', 'nincs', 'igaz�t�s', 'bal', 'jobb', 'k�z�p',
  // tab leader (filling character)
  'kit�lt�s',
  // no, yes
  'nem', 'igen',
  // [padding/spacing/side:] left, top, right, bottom
  'bal', 'fels�', 'jobb', 'als�',
  // border: none, single, double, triple, thick inside, thick outside
  'nincs', 'sima', 'dupla', 'tripla', 'vastag bels�', 'vastag k�ls�',
  // background, border, padding, spacing [between text and border]
  'h�tt�r', 'szeg�ly', 't�vols�g', 't�rk�z',
  // border: style, width, internal width, visible sides
  'st�lus', 'sz�less�g', 'bels� sz�less�g', 'l�that� oldalak',
  // style inspector -----------------------------------------------------------
  // title
  'St�lus b�ng�sz�',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'St�lus', '(Nincs)', 'Bekezd�s', 'Bet�t�pus', 'Tulajdons�gok',
  // border and background, hyperlink, standard [style]
  'Szeg�ly �s h�tt�r', 'Hiperhivatkoz�s', '(Norm�l)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'St�lusok', 'St�lus',
  // name, applicable to,
  '&N�v:', '&Hat�k�r:',
  // based on, based on (no &),
  'St�lus &alapja:', 'St�lus alapja:',
  //  next style, next style (no &)
  'K�vetkez� &bekezd�s st�lusa:', 'K�vetkez� bekezd�s st�lusa:',
  // quick access check-box, description and preview
  '&Gyors el�r�s', '&Le�r�s �s el�n�zet:',
  // [applicable to:] paragraph and text, paragraph, text
  'bekezd�s �s sz�veg', 'bekezd�s', 'sz�veg',
  // text and paragraph styles, default font
  'Sz�veg �s bekezd�s st�lusok', 'Alap�rtelmezett bet�t�pus',
  // links: edit, reset, buttons: add, delete
  'M�dos�t�s', 'Alap�rtelmezett', '�&j', '&T�rl�s',
  // add standard style, add custom style, default style name
  '�j &Norm�l st�lus...', '�j &egyedi st�lus', 'St�lus %d',
  // choose style
  'St�lus &v�laszt�s:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView st�lusok (*.rvst)|*.rvst', 'Hiba a bet�lt�s k�zben', 'Ez a f�jl nem tartalmaz st�lusokat',
  // Title, group-box, import styles
  'St�lus import�l�sa f�jlb�l', 'Import', 'St�lus i&mport�l�sa:',
  // existing styles radio-group: Title, override, auto-rename
  'Megl�v� st�lusok', '&fel�l�r', '&hozz�ad �tnevezve',
  // Select, Unselect, Invert,
  '&Kijel�l', '&T�r�l', '&Megford�t',
  // select/unselect: all styles, new styles, existing styles
  '&Minden st�lus', '�&j st�lusok', '&L�tez� st�lusok',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Form�z�s t�rl�se', 'Minden st�lus',
  // dialog title, prompt
  'St�lusok', 'St�lus &kiv�laszt�sa:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Szinon�m�k', '&Kihagy mind', '&Hozz�ad a sz�t�rhoz',
  // Progress messages ---------------------------------------------------------
  'let�lt�s %s', 'Nyomtat�s el�k�sz�t�se...', '%d oldal nyomtat�sa',
  'Konvert�l�s RTF form�tumba...',  'Konvert�l�s RTF form�tumb�l...',
  '%s olvas�sa...', '%s �r�sa...', 'sz�veg',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Nincs jegyzet', 'L�bjegyzet', 'V�gjegyzet', 'Sz�ljegyzet', 'Sz�vegdoboz'
  );


initialization
  RVA_RegisterLanguage(
    'Hungarian',  // english language name, do not translate
    'Magyar', // native language name
    EASTEUROPE_CHARSET, @Messages);

end.