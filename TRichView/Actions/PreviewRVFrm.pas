{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Print-preview dialog                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit PreviewRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, RVScroll, CRVPP, RVPP, ExtCtrls, BaseRVFrm, ActnList, ImgList,
  {$IFDEF USERVTNT}
  TntButtons, TntStdCtrls,
  {$ENDIF}
  {$IFDEF USERVKSDEVTE}
  te_theme,
  {$ENDIF}
  Buttons, ComCtrls, RVALocalize, PtblRV, RichViewActions;

{$IFDEF RICHVIEWDEF6}
{$IFNDEF USERVTNT}
  {$DEFINE USECOMBOBOXEX}
{$ENDIF}
{$ENDIF}

type
  TfrmRVPreview = class(TfrmRVBase)
    Panel1: TPanel;
    rvpp: TRVPrintPreview;
    cmb1: TComboBox;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    ActionList1: TActionList;
    ImageList1: TImageList;
    actFirst: TAction;
    actPrior: TAction;
    actNext: TAction;
    actLast: TAction;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    txtPageNo: TEdit;
    Label2: TLabel;
    Button1: TButton;
    sbPrint: TSpeedButton;
    actPrint: TAction;
    rvActionPageSetup1: TrvActionPageSetup;
    sbPageSetup: TSpeedButton;
    procedure cmb1Exit(Sender: TObject);
    procedure cmb1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rvppZoomChanged(Sender: TObject);
    procedure actFirstExecute(Sender: TObject);
    procedure actPriorExecute(Sender: TObject);
    procedure actNextExecute(Sender: TObject);
    procedure actLastExecute(Sender: TObject);
    procedure txtPageNoKeyPress(Sender: TObject; var Key: Char);
    procedure txtPageNoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actPrintExecute(Sender: TObject);
    procedure rvActionPageSetup1Change(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
  private
    { Private declarations }
    {$IFDEF USECOMBOBOXEX}
    cmb: TComboBoxEx;
    {$ELSE}
    cmb: TComboBox;
    {$ENDIF}
    procedure UpdateZoom;
    procedure UpdateToolbar;
    procedure GoToPage;
  protected
    _txtPageNo, _Label2, _cmb, _sb1, _sb2, _sb3, _sb4,
    _sbPrint, _sbPageSetup: TControl;
    {$IFDEF USERVKSDEVTE}
    function IsThemeAllowedFor(Component: TComponent): Boolean; override;
    {$ENDIF}
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    procedure Localize; override;
    procedure SetPageSetup(Action: TrvActionPageSetup; Images: TCustomImageList);
    procedure DoSelectNextZoomLevel;
    procedure DoSelectPreviousZoomLevel;
  end;


implementation

resourcestring

 sZoom200 = '200%';
 sZoom150 = '150%';
 sZoom100 = '100%';
 sZoom75  = '75%';
 sZoom50  = '50%';
 sZoom25  = '25%';
 sZoom10  = '10%';

{$R *.DFM}
{-----------------------------------------------------------------------}
function GetThemedColor(Color: TColor): TColor;
begin
  Result := Color;
  {$IFDEF USERVKSDEVTE}
  if GetThemeLink(nil)<>nil then
    Result := GetThemeLink(nil).SysColors[Color];
  {$ENDIF}
end;
{-----------------------------------------------------------------------}
procedure TfrmRVPreview.cmb1Exit(Sender: TObject);
begin
  UpdateZoom;
end;
{-----------------------------------------------------------------------}
procedure TfrmRVPreview.cmb1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then begin
    UpdateZoom;
    Key := 0;
  end;
end;
{-----------------------------------------------------------------------}
procedure TfrmRVPreview.UpdateZoom;
var s: String;
    zoom: Integer;
begin
  {$IFDEF USERVTNT}
  if TTntComboBox(_cmb).ItemIndex= TTntComboBox(_cmb).Items.Count-2 then begin
    rvpp.ZoomMode := rvzmPageWidth;
    exit;
  end;
  if TTntComboBox(_cmb).ItemIndex=TTntComboBox(_cmb).Items.Count-1 then begin
    rvpp.ZoomMode := rvzmFullPage;
    exit;
  end;
  s := Trim(TTntComboBox(_cmb).Text);
  if (s<>'') and (s[Length(s)]='%') then
    s := Copy(s,1,Length(s)-1);
  zoom := StrToIntDef(s,0);
  if (zoom<10) or (zoom>500) then
    RVA_MessageBox(RVA_GetS(rvam_pp_InvalidScale),RVA_GetS(rvam_pp_Title), MB_OK or MB_ICONEXCLAMATION)
  else
    rvpp.SetZoom(zoom);
  {$ELSE}
  s := Trim(cmb.Text);
  if s=RVA_GetS(rvam_pp_PageWidth, ControlPanel) then begin
    rvpp.ZoomMode := rvzmPageWidth;
    exit;
  end;
  if s=RVA_GetS(rvam_pp_FullPage, ControlPanel) then begin
    rvpp.ZoomMode := rvzmFullPage;
    exit;
  end;
  if (s<>'') and (s[Length(s)]='%') then
    s := Copy(s,1,Length(s)-1);
  zoom := StrToIntDef(s,0);
  if (zoom<10) or (zoom>500) then
    RVA_MessageBox(RVA_GetS(rvam_pp_InvalidScale, ControlPanel),RVA_GetS(rvam_pp_Title, ControlPanel), MB_OK or MB_ICONEXCLAMATION)
  else
    rvpp.SetZoom(zoom);
  {$ENDIF}
end;
{-----------------------------------------------------------------------}
procedure TfrmRVPreview.rvppZoomChanged(Sender: TObject);
begin
  {$IFDEF USERVTNT}
  TTntComboBox(_cmb).Text := IntToStr(rvpp.ZoomPercent)+'%';
  {$ELSE}
  cmb.Text := IntToStr(rvpp.ZoomPercent)+'%';
  {$ENDIF}
end;
{-----------------------------------------------------------------------}
procedure TfrmRVPreview.UpdateToolbar;
var delta: Integer;
begin
  actFirst.Enabled := rvpp.PageNo>1;
  actPrior.Enabled := rvpp.PageNo>1;
  actNext.Enabled := rvpp.PageNo<rvpp.RVPrint.PagesCount;
  actLast.Enabled := rvpp.PageNo<rvpp.RVPrint.PagesCount;
  SetControlCaption(_Label2, Format(RVA_GetS(rvam_pp_OfNo, ControlPanel), [rvpp.RVPrint.PagesCount]));
  SetControlCaption(_txtPageNo, IntToStr(rvpp.PageNo));
  delta := _Label2.Left+_Label2.Width+5-_sb3.Left;
  _sb3.Left := _sb3.Left+Delta;
  _sb4.Left := _sb4.Left+Delta;
end;
{-----------------------------------------------------------------------}
procedure TfrmRVPreview.actFirstExecute(Sender: TObject);
begin
  rvpp.First;
  UpdateToolbar;
end;

procedure TfrmRVPreview.actPriorExecute(Sender: TObject);
begin
  rvpp.Prev;
  UpdateToolbar;
end;

procedure TfrmRVPreview.actNextExecute(Sender: TObject);
begin
  rvpp.Next;
  UpdateToolbar;
end;

procedure TfrmRVPreview.actLastExecute(Sender: TObject);
begin
  rvpp.Last;
  UpdateToolbar;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVPreview.txtPageNoKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) then begin
    Key := #0;
    GoToPage;
  end;
  if not ((Key>='0') and (Key<='9')) and (Key >= #32) then begin
    Beep;
    Key := #0;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVPreview.txtPageNoExit(Sender: TObject);
begin
  GoToPage
end;
{------------------------------------------------------------------------------}
procedure TfrmRVPreview.GoToPage;
var pn: Integer;
begin
  pn := StrToIntDef(GetEditText(_txtPageNo), rvpp.PageNo);
  if pn<1 then
    pn := 1;
  if pn>rvpp.RVPrint.PagesCount then
    pn := rvpp.RVPrint.PagesCount;
  rvpp.PageNo := pn;
  UpdateToolbar;
end;
{------------------------------------------------------------------------------}

procedure TfrmRVPreview.FormCreate(Sender: TObject);
begin
  Button1.Left := Panel1.ClientWidth-Button1.Width-5;
  _txtPageNo := txtPageNo;
  _Label2 := Label2;
  _sbPrint := sbPrint;
  _sbPageSetup := sbPageSetup;
  _sb1 := SpeedButton1;
  _sb2 := SpeedButton2;
  _sb3 := SpeedButton3;
  _sb4 := SpeedButton4;
  inherited;
  Panel1.DoubleBuffered := True;
  {$IFDEF USECOMBOBOXEX}
  cmb := TComboBoxEx.Create(Self);
  cmb.Parent := Panel1;
  with cmb1 do
    cmb.SetBounds(Left,Top,Width,Height);
  cmb.TabOrder := 1;
  cmb.OnExit := cmb1.OnExit;
  cmb.OnClick := cmb1.OnClick;
  cmb.OnKeyDown := cmb1.OnKeyDown;
  cmb1.Free;
  cmb.Images := ImageList1;
  with cmb.ItemsEx do begin
   with Add do
     Caption := sZoom200;
   with Add do
     Caption := sZoom150;
   with Add do
     Caption := sZoom100;
   with Add do
     Caption := sZoom75;
   with Add do
     Caption := sZoom50;
   with Add do
     Caption := sZoom25;
   with Add do
     Caption := sZoom10;
   with Add do begin
     Caption := RVA_GetS(rvam_pp_PageWidth, ControlPanel);
     ImageIndex := 4;
   end;
   with Add do begin
     Caption := RVA_GetS(rvam_pp_FullPage, ControlPanel);
     ImageIndex := 5;
   end;
   cmb.DropDownCount := 10;
  end;
  {$ELSE}
  {$IFDEF USERVTNT}
  cmb := nil;
  with TTntComboBox(_cmb).Items do begin
   Add(sZoom200);
   Add(sZoom150);
   Add(sZoom100);
   Add(sZoom75);
   Add(sZoom50);
   Add(sZoom25);
   Add(sZoom10);
   Add(RVA_GetS(rvam_pp_PageWidth, ControlPanel));
   Add(RVA_GetS(rvam_pp_FullPage, ControlPanel));
  end;
  TTntComboBox(_cmb).DropDownCount := 10;
  {$ELSE}
  cmb := cmb1;
  with cmb.Items do begin
   Add(sZoom200);
   Add(sZoom150);
   Add(sZoom100);
   Add(sZoom75);
   Add(sZoom50);
   Add(sZoom25);
   Add(sZoom10);
   Add(RVA_GetS(rvam_pp_PageWidth, ControlPanel));
   Add(RVA_GetS(rvam_pp_FullPage, ControlPanel));
  end;
  cmb.DropDownCount := 10;
  {$ENDIF}
  {$ENDIF}
  SetPageSetup(_sbPageSetup.Action as TrvActionPageSetup, nil);
  rvpp.Color := GetThemedColor(clBtnShadow);


end;
{------------------------------------------------------------------------------}
procedure TfrmRVPreview.actPrintExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmRVPreview.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_pp_Title, ControlPanel);
  Button1.Caption := RVA_GetSAsIs(rvam_btn_Close, ControlPanel);
  Label1.Caption := RVA_GetSAsIs(rvam_pp_Pages, ControlPanel);
  SpeedButton1.Hint := RVA_GetSAsIs(rvam_pp_First, ControlPanel);
  SpeedButton2.Hint := RVA_GetSAsIs(rvam_pp_Prior, ControlPanel);
  SpeedButton3.Hint := RVA_GetSAsIs(rvam_pp_Next, ControlPanel);
  SpeedButton4.Hint := RVA_GetSAsIs(rvam_pp_Last, ControlPanel);
  actPrint.Hint := RVA_GetSAsIs(rvam_act_PrintH, ControlPanel);
  RVA_LocalizeForm(Self);
  sbPageSetup.Caption := '';
end;

procedure TfrmRVPreview.rvActionPageSetup1Change(Sender: TObject);
begin
  TRVPrint(rvpp.RVPrint).FormatPages(rvdoALL);
  rvpp.UpdateView;
end;

{$IFDEF USERVKSDEVTE}
function TfrmRVPreview.IsThemeAllowedFor(Component: TComponent): Boolean;
begin
  Result := Component<>cmb1;
end;
{$ENDIF}

{$IFDEF RVASKINNED}
procedure TfrmRVPreview.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _txtPageNo then
    _txtPageNo := NewControl
  else if OldControl = _Label2 then
    _Label2 := NewControl
  else if OldControl = cmb1 then
    _cmb := NewControl
  else if OldControl = SpeedButton1 then
    _sb1 := NewControl
  else if OldControl = SpeedButton2 then
    _sb2 := NewControl
  else if OldControl = SpeedButton3 then
    _sb3 := NewControl
  else if OldControl = SpeedButton4 then
    _sb4 := NewControl
  else if OldControl = sbPrint then
    _sbPrint := NewControl
  else if OldControl = sbPageSetup then
    _sbPageSetup := NewControl;
end;
{$ENDIF}

procedure TfrmRVPreview.SetPageSetup(Action: TrvActionPageSetup; Images: TCustomImageList);
begin
  _sbPageSetup.Action := Action;
  {$IFDEF USERVTNT}
  (_sbPageSetup as TTntSpeedButton).Caption := '';
  {$ELSE}
  (_sbPageSetup as TSpeedButton).Caption := '';
  {$ENDIF}
end;

// Select next item in zoom-level combobox 
procedure TfrmRVPreview.DoSelectNextZoomLevel;
begin
  {$IFDEF USERVTNT}
    if TTntComboBox(_cmb).ItemIndex < TTntComboBox(_cmb).Items.Count-1 then
      TTntComboBox(_cmb).ItemIndex := TTntComboBox(_cmb).ItemIndex + 1;
  {$ELSE}
    {$IFDEF USECOMBOBOXEX}
      if TComboBoxEx(cmb).ItemIndex < TComboBoxEx(cmb).Items.Count-1 then
        TComboBoxEx(cmb).ItemIndex := TComboBoxEx(cmb).ItemIndex + 1;
    {$ELSE}
      if TComboBox(cmb).ItemIndex < TComboBox(cmb).Items.Count-1 then
        TComboBox(cmb).ItemIndex := TComboBox(cmb).ItemIndex + 1;
    {$ENDIF}
  {$ENDIF}
  UpdateZoom();
end;

// Select previous item in zoom-level combobox 
procedure TfrmRVPreview.DoSelectPreviousZoomLevel;
begin
  {$IFDEF USERVTNT}
    if TTntComboBox(_cmb).ItemIndex > 0 then
      TTntComboBox(_cmb).ItemIndex := TTntComboBox(_cmb).ItemIndex - 1;
  {$ELSE}
    {$IFDEF USECOMBOBOXEX}
      if TComboBoxEx(cmb).ItemIndex > 0 then
        TComboBoxEx(cmb).ItemIndex := TComboBoxEx(cmb).ItemIndex - 1;
    {$ELSE}
      if TComboBox(cmb).ItemIndex > 0 then
        TComboBox(cmb).ItemIndex := TComboBox(cmb).ItemIndex - 1;
    {$ENDIF}
  {$ENDIF}
  UpdateZoom();
end;

procedure TfrmRVPreview.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
var NewZoom: Integer;
begin
  {$IFDEF USERVTNT}
  Handled := ActiveControl<>_cmb;
  {$ELSE}
  Handled := ActiveControl<>cmb;
  {$ENDIF}
  if Handled then
    if ssCtrl in Shift then begin
      NewZoom := rvpp.ZoomPercent-10;
      if NewZoom mod 10 > 0 then
        NewZoom := NewZoom - NewZoom mod 10 + 10;
      if NewZoom<10 then
        NewZoom := 10;
      rvpp.SetZoom(NewZoom);
      end
    else
      actNextExecute(nil);
end;

procedure TfrmRVPreview.FormMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
var NewZoom: Integer;
begin
  {$IFDEF USERVTNT}
  Handled := ActiveControl<>_cmb;
  {$ELSE}
  Handled := ActiveControl<>cmb;
  {$ENDIF}
  if Handled then
    if ssCtrl in Shift then begin  
      NewZoom := rvpp.ZoomPercent+10;
      NewZoom := NewZoom - NewZoom mod 10;
      if NewZoom>500 then
        NewZoom := 500;
      rvpp.SetZoom(NewZoom);
      end
    else
      actPriorExecute(nil);
end;

end.