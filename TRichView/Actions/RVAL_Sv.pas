
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Swedish (Svenska) translation                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Mikael Collin, 2003-01-29              }
{                mikael.collin@robmatic.se              }
{                                                       }
{ Updates: 2014-04-26: by Alconost                      }
{          2012-09-23: by Alconost                      }
{          2011-09-19: by Alconost                      }
{          2011-03-10: by Alconost                      }
{          2007-11-14:                                  }
{          2004-04-26: Updated with tabs...             }
{          2003-08-19: Minor update.                    }
{          2003-08-14: Updated "bidi"-text, charcase.   }
{          2003-04-19: Added "Page Setup".              }
{          2003-01-30: Corrected the colornames.        }
{*******************************************************}

unit RVAL_Sv;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'tum', 'cm', 'mm', 'pc', 'pixlar', 'pt',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc',  'pixlar', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Arkiv', '&Redigera', 'F&ormat', 'T&ecken', '&Stycke', '&Infoga', '&Tabell', '&F�nster', '&Hj�lp',
  // exit
  'A&vsluta',
  // top-level menus: View, Tools,
  '&Visa', 'Verk&tyg',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Storlek', 'Stil', 'V�l&j', 'Justera &cellens inneh�ll', 'C&ellkanter',
  // menus: Table cell rotation
  'Cell&rotation',   
  // menus: Text flow, Footnotes/endnotes
  '&Textfl�de', '&Fotnoter',
  // ribbon tabs: tab1, tab2, view, table
  '&Hem', '&Avancerat', '&Vy', '&Tabell',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Urklipp', 'Font', 'Paragraf', 'Lista', 'Redigering',
  // ribbon groups: insert, background, page setup,
  'Infoga', 'Bakgrund', 'Sidinst�llningar',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'L�nkar', 'Fotnoter', 'Dokumentvyer', 'Visa/G�m', 'Zooma', 'Alternativ',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Infoga', 'Radera', 'Operationer', 'Kanter',
  // ribbon groups: styles 
  'Stilar',
  // ribbon screen tip footer,
  'Tryck p� F1 f�r mer hj�lp',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Senast anv�nda dokument', 'Spara en kopia p� dokumentet', 'F�rhandsgranska och skriv ut dokumentet',
  // ribbon label: units combo
  'Enheter:',          
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nytt', 'Nytt|Skapar ett nytt dokument',
  // TrvActionOpen
  '&�ppna...', '�ppna|�ppnar ett dokument fr�n disk',
  // TrvActionSave
  '&Spara', 'Spara|Sparar dokumentet till disk',
  // TrvActionSaveAs
  'Spara so&m...', 'Spara som...|Sparar dokumentet till disk med ett nytt namn',
  // TrvActionExport
  '&Exportera...', 'Exportera|Exporterar dokumentet till ett annat format',
  // TrvActionPrintPreview
  'F�r&handsgranska', 'F�rhandsgranska|Visar dokumentet som det kommer att se ut n�r det �r utskrivet',
  // TrvActionPrint
  '&Utskrift...', 'Utskrift|L�ter dig st�lla in inst�llningar f�r skrivare och skriva ut dokumentet',
  // TrvActionQuickPrint
  '&Skriv ut', '&Snabbutskrift', 'Skriv ut|Skriver ut dokumentet',
   // TrvActionPageSetup
  'Utskriftsforma&t...', 'Utskriftsformat|S�tter marginaler, pappersstorlek, orintering, k�lla, sidhuvud och sidfot',
  // TrvActionCut
  '&Klipp ut', 'Klipp ut|Klipper ut det som �r markerat och l�gger det i urklipp',
  // TrvActionCopy
  'K&opiera', 'Kopiera|Kopierar det som �r markerat och l�gger det i urklipp',
  // TrvActionPaste
  'K&listra in', 'Klistra in|L�gger in det som finns i urklipp',
  // TrvActionPasteAsText
  'Klistra in som &text', 'Klistra in som text|Infogar text fr�n urklipp',  
  // TrvActionPasteSpecial
  'Klistra in spe&cial...', 'Klistra in special|L�gger in det som finns i urklipp med valt format',
  // TrvActionSelectAll
  '&Markera allt', 'Markera allt|Markerar allt i dokumentet',
  // TrvActionUndo
  '&�ngra', '�ngra|�ngrar den senaste f�r�ndringen',
  // TrvActionRedo
  'G&�r om', 'G�r om|G�r om den senast �ngrade f�r�ndringen',
  // TrvActionFind
  '&S�k...', 'S�k|Letar efter en specificerad text i dokumentet',
  // TrvActionFindNext
  'S�k &n�sta', 'S�k n�sta|Forts�tter den senaste s�kningen',
  // TrvActionReplace
  '&Ers�tt...', 'Ers�tt|Letar efter och ers�tter en specificerad text i dokumentet',
  // TrvActionInsertFile
  '&Fil...', 'Infoga fil|L�gger till inneh�llet fr�n en fil i dokumentet',
  // TrvActionInsertPicture
  '&Bild...', 'Infoga bild|L�gger till en bild fr�n disk',
  // TRVActionInsertHLine
  'Ho&risontell linje', 'Infoga horisontell Line|Infogar en horisontell linje',
  // TRVActionInsertHyperlink
  '&Hyperl�nk...', 'Infoga hyperl�nk|Infogar en hyperl�nk',
  // TRVActionRemoveHyperlinks
  '&Ta bort hyperl�nkar', 'Ta bort hyperl�nkar|Tar bort alla hyperl�nkar i den markerade texten',  
  // TrvActionInsertSymbol
  '&Symbol...', 'Infoga symbol|Infogar symbol',
  // TrvActionInsertNumber
  '&Nummer...', 'Infoga nummer|Infogar ett nummer',
  // TrvActionInsertCaption
  'Infoga &rubrik...', 'Infoga rubrik|Infogar en rubrik f�r markerat objekt',
  // TrvActionInsertTextBox
  '&Textf�lt', 'Infoga textf�lt|Infogar ett textf�lt',
  // TrvActionInsertSidenote
  '&Kommentar', 'Infoga kommentar|Infogar en kommentar som visas i ett textf�lt',
  // TrvActionInsertPageNumber
  '&Sidnummer', 'Infogar sidnummer|Infogar ett sidnummer',
  // TrvActionParaList
  'Punkter och &numrering...', 'Punkter och numrering|L�gger till eller �ndrar punkter och numrering f�r markerade stycken',
  // TrvActionParaBullets
  '&Punkter', 'Punkter|L�gger till eller tar bort punkter i stycket',
  // TrvActionParaNumbering
  '&Numrering', 'Numrering|L�gger till eller tar bort numrering i stycket',
  // TrvActionColor
  'Bakgrunds&f�rg...', 'Bakgrundsf�rg|V�ljer en bakgrundsf�rg f�r dokumentet',
  // TrvActionFillColor
  '&Fyllningsf�rg...', 'Fyllningsf�rg|V�ljer bakgrundsf�rg f�r text, stycke, cell, tabell eller dokument',
  // TrvActionInsertPageBreak
  '&Infoga sidbrytning', 'Infoga sidbrytning|Infogar sidbrytning',
  // TrvActionRemovePageBreak
  '&Ta bort sidbrytning', 'Ta bort sidbrytning|Tar bort sidbrytning',
  // TrvActionClearLeft
  'Rensa textfl�det p� &v�nster sida', 'Rensa textfl�det p� v�nster sida|Placerar denna paragraf under alla v�nsterst�llda bilder',
  // TrvActionClearRight
  'Rensa textfl�det p� &h�ger sida', 'Rensa textfl�det p� h�ger sida|Placerar denna paragraf under alla h�gerst�llda bilder',
  // TrvActionClearBoth
  'Rensa textfl�det p� &b�da sidor', 'Rensa textfl�det p� b�da sidor|Placerar denna paragraf under alla v�nster- eller h�gerst�llda bilder',
  // TrvActionClearNone
  '&Normalt textfl�de', 'Normalt textfl�de|Till�ter textfl�de runt b�de v�nster- och h�gerst�llda bilder',
  // TrvActionVAlign
  '&Objektposition...', 'Objektposition|�ndrar position p� det valda objektet',    
  // TrvActionItemProperties
  'Objekt&egenskaper...', 'Objektegenskaper|Definerar egenskaper f�r det aktiva objektet',
  // TrvActionBackground
  '&Bakgrund...', 'Bakgrund|V�ljer bakgrundsf�rg och bild',
  // TrvActionParagraph
  '&Stycke...', 'Stycke|�ndrar inst�llningarna f�r stycket',
  // TrvActionIndentInc
  '&�ka indrag', '�ka indrag|�kar indraget f�r markerade stycken',
  // TrvActionIndentDec
  '&Minska indrag', 'Minska indrag|Minskar indraget f�r markerade stycken',
  // TrvActionWordWrap
  '&Radbrytning', 'Radbrytning|V�xlar mellan radbrytning av ord f�r markerade stycken',
  // TrvActionAlignLeft
  '&V�nsterjustera', 'V�nsterjustera|Justerar den markerade texten till v�nster',
  // TrvActionAlignRight
  '&H�gerjustera', 'H�gerjustera|Justerar den markerade texten till h�ger',
  // TrvActionAlignCenter
  '&Centrera', 'Centrera|Centrerar den markerade texten',
  // TrvActionAlignJustify
  '&Marginaljustera', 'Marginal justera|Justerar den markerade texten mot b�de v�nster och h�ger sida',
  // TrvActionParaColor
  '&Bakgrundsf�rg f�r stycke...', 'Bakgrundsf�rg f�r stycke|Anger bakgrundsf�rgen f�r stycken',
  // TrvActionLineSpacing100
  '&Enkelt radavst�nd', 'Enkelt radavst�ng|Anger enkelt radavst�nd',
  // TrvActionLineSpacing150
  '1.5 ra&davst�nd', '1.5 radavst�nd|Anger 1,5 i radavst�nd',
  // TrvActionLineSpacing200
  '&Dubbelt radavst�nd', 'Dubbelt radavst�nd|Anger dubbelt radavst�nd',
  // TrvActionParaBorder
  '&Kantlinjer och bakgrund f�r stycke...', 'Kantlinjer och bakgrund f�r stycke|Anger kantlinjer och bakgrund f�r markerade stycken',
  // TrvActionInsertTable
  '&Infoga tabell...', '&Tabell', 'Infoga tabell|Infogar en ny tabell',
  // TrvActionTableInsertRowsAbove
  'Infoga rad &ovanf�r', 'Infoga rad ovanf�r|Infogar rad ovanf�r de markerade cellerna',
  // TrvActionTableInsertRowsBelow
  'Infoga rad &under', 'Infoga rad under|Infogar en ny rad under de markerade cellerna',
  // TrvActionTableInsertColLeft
  'Infoga kolumn till &v�nster', 'Infoga kolumn till v�nster|Infogar en ny kolumn till v�nster om de markerade cellerna',
  // TrvActionTableInsertColRight
  'Infoga kolumn till &h�ger', 'Infoga kolumn till h�ger|Infogar en ny kolumn till h�ger om de markerade cellerna',
  // TrvActionTableDeleteRows
  'Ta bort r&ader', 'Ta bort rader|Tar bort de markerade raderna',
  // TrvActionTableDeleteCols
  'Ta bort &kolumner', 'Ta bort kolumner|Tar bort kolumner med markerade celler',
  // TrvActionTableDeleteTable
  '&Ta bort tabell', 'Ta bort tabell|Tar bort tabell',
  // TrvActionTableMergeCells
  '&Sammanfoga celler', 'Sammanfoga celler|Sammanfogar markerade celler',
  // TrvActionTableSplitCells
  '&Dela celler...', 'Dela celler|Delar markerade celler',
  // TrvActionTableSelectTable
  'Markera &tabell', 'Markera tabell|Markerar tabell',
  // TrvActionTableSelectRows
  'Markera rad&er', 'Markera rader|Markerar rader',
  // TrvActionTableSelectCols
  'Markera kol&umner', 'Markera kolumner|Markerar kolumner',
  // TrvActionTableSelectCell
  'Markera c&ell', 'Markera cell|Markerar cell',
  // TrvActionTableCellVAlignTop
  'Justera cell &�verkant', 'Justera cell mot �verkant|Justerar cellens inneh�ll mot �verkanten',
  // TrvActionTableCellVAlignMiddle
  'Justera cell mot &mitten', 'Justera cell mot mitten|Justerar cellens inneh�ll mot mitten',
  // TrvActionTableCellVAlignBottom
  'Justera cell &nederkant', 'Justera cell mot nederkant|Justerar cellens inneh�ll mot nederkanten',
  // TrvActionTableCellVAlignDefault
  '&Standard celljustering vertikalt', 'Standard celljustering vertikalt|S�tter standard celljustering vertikalt f�r de markerade cellerna',
  // TrvActionTableCellRotationNone
  '&Ingen cellrotering', 'Ingen cellrotering|Roterar cellens inneh�ll 0�',
  // TrvActionTableCellRotation90
  'Rotera cellen &90�', 'Rotera cellen 90�|Roterar cellens inneh�ll 90�',
  // TrvActionTableCellRotation180
  'Rotera cellen &180�', 'Rotera cellen 180�|Roterar cellens inneh�ll 180�',
  // TrvActionTableCellRotation270
  'Rotera cellen &270�', 'Rotera cellen 270�|Roterar cellens inneh�ll 270�',
  // TrvActionTableProperties
  'Tabell &egenskaper...', 'Tabellegenskaper|�ndrar p� egenskaperna f�r den markerade tabellen',
  // TrvActionTableGrid
  'Visa &alla kantlinjer', 'Visa alla kantlinjer|Visar eller g�mmer tabellens kantlinjer',
  // TRVActionTableSplit
  'De&la tabell', 'Dela tabell|Delar tabellen i tv� tabeller med b�rjan fr�n den markerade raden',
  // TRVActionTableToText
  'Konvertera till te&xt...', 'Konvertera till text|Konverterar tabell till text',
  // TRVActionTableSort
  '&Sortera...', 'Sortera|Sorterar tabellens rader',
  // TrvActionTableCellLeftBorder
  '&V�nster kantlinje', 'V�nster kantlinje|Visar eller g�mmer cellens v�nstra kantlinje',
  // TrvActionTableCellRightBorder
  '&H�ger kantlinje', 'H�ger kantlinje|Visar eller g�mmer cellens h�gra kantlinje',
  // TrvActionTableCellTopBorder
  '&�vre kantlinje', '�vre kantlinje|Visar eller g�mmer cellens �vre kantlinje',
  // TrvActionTableCellBottomBorder
  '&Nedre kantlinje', 'Nedre kantlinje|Visar eller g�mmer cellens nedre kantlinje',
  // TrvActionTableCellAllBorders
  '&Yttre kantlinje', 'Yttre kantlinje|Visar eller g�mmer cellens alla kantlinjer',
  // TrvActionTableCellNoBorders
  '&Ingen kantlinje', 'Ingen kantlinje|G�mmer cellens alla kantlinjer',
  // TrvActionFonts & TrvActionFontEx
  '&Tecken...', 'Tecken|�ndrar teckensnitt p� den markerade texten',
  // TrvActionFontBold
  '&Fet', 'Fet|�ndrar stilen p� den markerade texten till fet',
  // TrvActionFontItalic
  '&Kursiv', 'Kursiv|�ndrar stilen p� den markerade texten till kursiv',
  // TrvActionFontUnderline
  '&Understruken', 'Understruken|�ndrar stilen p� den markerade texten till understruken',
  // TrvActionFontStrikeout
  '&�verstruken', '�verstruken|Stryker �ver markerad text',
  // TrvActionFontGrow
  '&F�rstora teckensnitt', 'F�rstora teckensnitt|F�rstorar teckensnitt p� markerad text med 10%',
  // TrvActionFontShrink
  '&Minska teckensnitt', 'Minska teckensnitt|Minskar teckensnitt p� markerad text med 10%',
  // TrvActionFontGrowOnePoint
  'F&�rstora teckensnittet med 1 punkt', 'F�rstora teckensnittet med 1 punkt|F�rstorar teckensnittet med 1 punkt',
  // TrvActionFontShrinkOnePoint
  'M&inska teckensnittet med 1 punkt', 'Minska teckensnittet med 1 punkt|Minskar teckensnittet med 1 punkt',
  // TrvActionFontAllCaps
  '&Stora tecken', 'Stora tecken|�ndrar alla markerade tecken till stora tecken',
  // TrvActionFontOverline
  '&Linje ovanf�r', 'Linje ovanf�r|Ritar en linje ovanf�r markerad text',
  // TrvActionFontColor
  'Text&f�rg...', 'Textf�rg|�ndrar f�rgen p� markerad text',
  // TrvActionFontBackColor
  'Textba&kgrundsf�rg...', 'Textbakgrundsf�rg|�ndrar bakgrundsf�rg p� markerad text',
  // TrvActionAddictSpell3
  '&Stavningskontroll', 'Stavningskontroll|Kontrollerar stavningen',
  // TrvActionAddictThesaurus3
  '&Synonymer', 'Synonymer|Visar synonymer f�r markerat ord',
  // TrvActionParaLTR
  'V�nster till h�ger', 'V�nster till h�ger|S�tter v�nster-till-h�ger textorientering f�r de markerade styckena',
  // TrvActionParaRTL
  'H�ger till v�nster', 'H�ger till v�nster|S�tter h�ger-till-v�nster textorientering f�r de markerade styckena',
  // TrvActionLTR
  'Text fr�n v�nster till h�ger', 'Text fr�n v�nster till h�ger|S�tter v�nster-till-h�ger orientering p� markerad text',
  // TrvActionRTL
  'Text fr�n h�ger till v�nster', 'Text fr�n h�ger till v�nster|S�tter h�ger-till-v�nster orientering p� markerad text',
  // TrvActionCharCase
  'Skiftl�ge', 'Skiftl�ge|�ndrar mellan stora och sm� bokst�ver p� markerad text',
  // TrvActionShowSpecialCharacters
  '&Icke utskrivbara tecken', 'Icke utskrivbara tecken|Visar eller d�ljer icke utskrivbara tecken, som paragrafmarkeringar, tabbar och blanksteg',
  // TrvActionSubscript
  'I&ndex', 'Index|Konverterar den markerade texten till index',
  // TrvActionSuperscript
  'Exponent', 'Exponent|Konverterar den markerade texten till exponent',
  // TrvActionInsertFootnote
  '&Fotnot', 'Fotnot|Infogar en fotnot',
  // TrvActionInsertEndnote
  '&Slutnot', 'Slutnot|Infogar en slutnot',
  // TrvActionEditNote
  'R&edigera not', 'Redigera not|P�b�rjar redigering av fotnoten eller slutnoten',
  '&D�lj', 'D�lj|D�ljer eller visar det markerade fragmentet',    
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stilar...', 'Stilar|�ppnar stilhanteringsdialogen',
  // TrvActionAddStyleTemplate
  '&L�gg till stil...', 'L�gg till stil|Skapar en ny text eller paragrafstil',
  // TrvActionClearFormat,
  '&Rensa format', 'Rensa format|Rensar all text och paragrafformatering fr�n det markerade fragmentet',
  // TrvActionClearTextFormat,
  'Rensa &textformat', 'Rensa textformat|Rensar all formatering fr�n den markerade texten',
  // TrvActionStyleInspector
  'Stil&inspekt�r', 'Stilinspekt�r|Visar eller g�mmer stilinspekt�ren',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'OK', 'Avbryt', 'St�ng', 'Infoga', '&�ppna...', '&Spara...', '&Rensa', 'Hj�lp', 'Ta bort',
  // Others  -------------------------------------------------------------------
  // percents
  'procent',
  // left, top, right, bottom sides
  'V�nster sida', '�ver', 'H�ger sida', 'Under',
  // save changes? confirm title
  'Spara �ndringar till %s?', 'Bekr�fta',
  // warning: losing formatting
  '%s kan inneh�lla saker som inte �r kompatibla med det valda formatet.'#13+
  'Vill du �nd� spara i det h�r formatet?',
  // RVF format name
  'RichView-format',
  // Error messages ------------------------------------------------------------
  'Fel',
  'Fel n�r fil skulle laddas.'#13#13'T�nkbara anledningar:'#13'- formatet p� den h�r filen st�ds inte av det h�r programmet;'#13+
  '- filen �r korrupt;'#13'- filen �r �ppnad och l�st av ett annat program.',
  'Fel n�r bild skulle laddas.'#13#13'T�nkbara anledningar:'#13'- bilden �r i ett format som inte st�ds av det h�r programmet;'#13+
  '- filen �r inte en bild;'#13+
  '- filen �r korrupt;'#13'- filen �r �ppnad och l�st av ett annat program.',
  'Del n�r fil skulle sparas.'#13#13'T�nkbara anledningar:'#13'- det finns inte tillr�ckligt mycke plats p� h�rddisken;'#13+
  '- disken �r skrivskyddad;'#13'- flyttbart media �r inte inserted;'#13+
  '- filen �r �ppnad och l�st av ett annat program;'#13'- disken �r korrupt.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView-filer (*.rvf)|*.rvf',  'RTF-filer (*.rtf)|*.rtf' , 'XML-filer (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Textfiler (*.txt)|*.txt', 'Textfiler - Unicode (*.txt)|*.txt', 'Textfiler - Automatisk (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML-filer (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - F�renklad (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word-dokument (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'S�kningen klar', 'S�kstr�ngen ''%s'' kunde inte hittas.',
  // 1 string replaced; Several strings replaced
  '1 str�ng ersatt.', '%d str�ngar ersatta.',
  // continue search from the beginning/end?
  'Slutet p� dokumentet har n�tts, vill du forts�tta ifr�n b�rjan?',
  'B�rjan p� dokumentet har n�tts, vill du forts�tta ifr�n slutet?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Genomskinlig', 'Automatisk',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Svart', 'Brun', 'Khakigr�n', 'M�rkgr�n', 'M�rkbl�gr�n', 'M�rkbl�', 'Indigobl�', 'Gr�-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'M�rkr�d', 'Orange', 'Olivgr�n', 'Gr�n', 'Bl�gr�n', 'Bl�', 'Bl�gr�', 'Gr�-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'R�d', 'Ljusorange', 'Gr�ngul', 'Havsgr�n', 'Turkos', 'Klarbl�', 'Lila', 'Gr�-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Guld', 'Gul', 'Ljusgr�n', 'Ljus turkos', 'Himmelsbl�', 'Plommon', 'Gr�-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosenr�d', 'Aprikos', 'Ljusgul', 'Pastellgr�n', 'Pastellbl�', 'Ljusbl�', 'Lavendel', 'Vit',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'G&enomskinlig', '&Automatisk', '&Fler f�rger...', 'F&�rvalt v�rde',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Bakgrund', 'F&�rg:', 'Position', 'Bakgrund', 'Exempel text.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ingen', '&Hela ytan', 'F&ixerad delning', '&Dela', 'C&entrera',
  // Padding button
  '&Utfyllnad...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Fyllningsf�rg', '&Anv�nd p�:', '&Fler f�rger...', '&Utfyllnad...',
  'V�lj en f�rg',
  // [apply to:] text, paragraph, table, cell
  'text', 'stycke', 'tabell', 'cell',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Font', 'Font', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Teckensnitt:', '&Storlek', 'Stil', '&Fet', '&Kursiv',
  // Script, Color, Back color labels, Default charset
  'Sk&ript:', '&F�rg:', 'Ba&kgrund:', '(F�rvalt v�rde)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekter', 'Understruken', '&�verstruken', '&Genomstruken', '&Versaler',
  // Sample, Sample text
  'Exempel', 'Exempeltext',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Teckenavst�nd', '&Avst�nd:', '&Expanderat', '&Ihoptryckt',
  // Offset group-box, Offset label, Down, Up,
  'Offset', '&Offset:', '&Ner', '&Upp',
  // Scaling group-box, Scaling
  'Skal�ndring', 'Sk&al�ndring:',
  // Sub/super script group box, Normal, Sub, Super
  'Index och exponent', '&Normal', 'In&dex', 'E&xponent',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Utfyllnad', '&Ovanf�r:', '&V�nster:', '&Under:', '&H�ger:', '&Samma v�rden',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Infoga hyperl�nk', 'Hyperl�nk', 'T&ext:', '&M�l', '<<markering>>',
  // cannot open URL
  'Kan inte g� till "%s"',
  // hyperlink properties button, hyperlink style
  '&Skr�ddarsy...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'L�nkattribut', 'Normala f�rger', 'Aktiva f�rger',
  // Text [color], Background [color]
  '&Text:', '&Bakgrund',
  // Underline [color]
  'U&nderstryk:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'B&akgrund',
  // Underline [color], Attributes group-box,
  'U&nderstryk:', 'Attribut',
  // Underline type: always, never, active (below the mouse)
  '&Understryk:', 'alltid', 'aldrig', 'aktiv',
  // Same as normal check-box
  'Som &f�rvald',
  // underline active links check-box
  '&Understryk aktiva l�nkar',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Infoga symbol', '&Teckensnitt:', '&Teckenupps�ttning:', 'Unicode',
  // Unicode block
  '&Block:',
  // Character Code, Character Unicode Code, No Character
  'Teckenkod: %d', 'Teckenkod: Unicode %d', '(inga tecken)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Infoga tabell', 'Tabellstorlek', 'Antal &kolumner:', 'Antal &rader:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabellayout', '&Automatisk storlek', 'Anpassa tabellen till &f�nstret', '�ndra tabellstorlek &manuellt',
  // Remember check-box
  'Kom ih�g &dimensionerna f�r nya tabeller',
  // VAlign Form ---------------------------------------------------------------
  'Position',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Egenskaper', 'Bild', 'Position och storlek', 'Linje', 'Tabell', 'Rader', 'Celler',
  // Image Appearance, Image Misc, Number tabs
  'Utseende', '�vrigt', 'Nummer',
  // Box position, Box size, Box appearance tabs
  'Position', 'Storlek', 'Utseende',
  // Preview label, Transparency group-box, checkbox, label
  'F�rhandsgranska:', 'Genomskinlighet', '&Genomskinlig', 'Genomskinlig &f�rg:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  '�&ndra...', '&Spara...', 'Automatiskt',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikal justering', '&Justera:',
  'undre kanten till baslinjen av texten', 'mitten till baslinjen av texten',
  '�vre kanten till topplinjen av texten', 'undre kanten till baslinjen av texten', 'mittenkanten till mittenlinjen av texten',
  // align to left side, align to right side
  'v�nster sida', 'h�ger sida',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&F�rskjut:', 'Str�ck ut', '&Bredd:', '&H�jd:', 'F�rvald storlek: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Skala om &proportionerligt', '&Nollst�ll',
  // Inside the border, border, outside the border groupboxes
  'Inom markeringen', 'Kant', 'Utanf�r markeringen',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Fyllf�rg:', '&Spaltfyllnad:',
  // Border width, Border color labels
  'Kant&bredd:', 'Kant&f�rg:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horisontalt mellanrum:', '&Vertikalt mellanrum:',
  // Miscellaneous groupbox, Tooltip label
  '�vrigt', '&Tooltip:',
  // web group-box, alt text
  'Webb', 'Alternativ &text:',
  // Horz line group-box, color, width, style
  'Horisontell linje', '&F�rg:', '&Bredd:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabell', '&Bredd:', '&Fyllnadsf�rg:', '&Cell&avst�nd...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horisontell cellutfyllnad:', '&Vertikal cellutfyllnad:', 'Kant och bakgrund',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Synliga &kantsidor...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Beh�ll inneh�ll tillsammans',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotation', '&Ingen', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Kant:', 'Synliga &kantsidor...',
  // Border, CellBorders buttons
  '&Tabellkanter...', '&Cellkanter...',
  // Table border, Cell borders dialog titles
  'Tabellkanter', 'F�rvalda cellkanter',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Skriver ut', '&Beh�ll tabellen p� en sida', 'Antalet &huvudrader:',
  'Huvudraderna visas p� varje sida med tabellen',
  // top, center, bottom, default
  'Mot &�verkant', '&Centrera', 'Mot &nederkant', '&F�rvalt v�rde',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Inst�llningar', 'F�redragen &bredd:', '&Minska h�jd:', '&Fyllningsf�rg:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kantlinje', 'Synliga sidor:', 'Skuggf&�rg:', '&Ljus f�rg', 'F&�rg:',
  // Background image
  'B&ild...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horisontell position', 'Vertikal position', 'Position i text',
  // Position types: align, absolute, relative
  'Justering', 'Absolut position', 'Relativ position',
  // [Align] left side, center, right side
  'v�nster sida av f�ltet till v�nster sida av',
  'mitten av f�ltet till mitten av',
  'h�ger sida av f�ltet till h�ger sida av',
  // [Align] top side, center, bottom side
  'ovanf�r f�ltet till ovanf�r',
  'mitten av f�ltet till mitten av',
  'nedanf�r f�ltet till nedanf�r',
  // [Align] relative to
  'relativt till',
  // [Position] to the right of the left side of
  'till h�ger om v�nstra sidan av',
  // [Position] below the top side of
  'nedanf�r ovansidan av',
  // Anchors: page, main text area (x2)
  '&Sida', '&Huvudtextf�lt', 'S&ida', 'Huvudte&xtf�lt',
  // Anchors: left margin, right margin
  '&V�nstermarginalen', '&H�germarginalen',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Toppmarginal', '&Bottenmarginal', '&Innermarginal', '&Yttermarginal',
  // Anchors: character, line, paragraph
  '&Bokstav', 'L&inje', 'Para&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Spegelv�n&da marginaler', 'La&yout i tabellcell',
  // Above text, below text
  '&Ovanf�r text', '&Nedanf�r text',
  // Width, Height groupboxes, Width, Height labels
  'Bredd', 'H�jd', '&Bredd:', '&H�jd:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikal justering', '&Topp', '&Mitten', '&Botten',
  // Border and background button and title
  'Kan&t och bakgrund...', 'F�ltkant och bakgrund',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Klistra in special', '&Klistra in som:', 'RichText-format', 'HTML-format',
  'Text', 'Unicode-text', 'Bitmappsbild', 'Metafile-bild',
  'Grafiska filer', 'URL',
  // Options group-box, styles
  'Alternativ', '&Stilar:',
  // style options: apply target styles, use source styles, ignore styles
  'till�mpa m�ldokumentets stilar', 'beh�ll stilar och utseende',
  'beh�ll utseende, ignorera stilar',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Punkter och numrering', 'Punkter', 'Numrerad', 'Punktlista', 'Numrerad lista',
  // Customize, Reset, None
  '&Anpassa...', '&Rensa', 'Ingen',
  // Numbering: continue, reset to, create new
  'forts�tt numrering', 's�tt numrering till', 'skapa en ny lista, starta p�',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Alfanumeriska gemener', 'Romerska gemener', 'Numrerade', 'Alfanumeriska versaler', 'Romerska versaler',
  // Bullet type
  'Cirkel', 'Disk', 'Fyrkant',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Niv�:', '&Starta fr�n:', '&Forts�tt', 'Numrering',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Anpassa lista', 'Niv�er', '&Antal:', 'Listegenskaper', '&Listtyp:',
  // List types: bullet, image
  'punkter', 'bild', 'Infoga nummer|',
  // Number format, Number, Start level from, Font button
  '&Nummerformat:', 'Nummer', '&Starta niv�numrering fr�n:', '&Teckensnitt...',
  // Image button, bullet character, Bullet button,
  '&Bild...', 'Punktt&ecken', '&Punkter...',
  // Position of list text, bullet, number, image, text
  'Listans textposition', 'Punktposition', 'Nummerposition', 'Bildposition', 'Textposition',
  // at, left indent, first line indent, from left indent
  '&p�:', 'V&�nster indrag:', 'Indrag p� &f�rsta raden:', 'indrag fr�n v�nster',
  // [only] one level preview, preview
  '&F�rhandsgranska en niv�', 'F�rhandsgranska',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'v�nsterjustera', 'h�gerjustera', 'centrera',
  // level #, this level (for combo-box)
  'Niv� %d', 'Denna niv�',
  // Marker character dialog title
  '�ndra tecken f�r punktlista',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Kantlinje och bakgrund f�r stycke', 'Kantlinje', 'Bakgrund',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Inst�llningar', '&F�rg:', '&Bredd:', 'Int&ern bredd:', 'O&ffset...',
  // Sample, Border type group-boxes;
  'Exempel', 'Typ av kantlinje',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ingen', '&Enkelt', '&Dubbel', '&Trippel', 'Tjock &insida', 'Tjock &utsida',
  // Fill color group-box; More colors, padding buttons
  'Fyllningsf�rg', '&Fler f�rger...', '&Utfyllnad...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Offset', 'Offset p� kantlinje',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Stycke', 'Justera', '&V�nster', '&H�ger', '&Centrera', '&Marginaljustera',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Avst�nd', '&F�re:', '&Efter:', 'Rad&avst�nd:', 'me&d:',
  // Indents group-box; indents: left, right, first line
  'Indrag', 'V&�nster:', 'H&�ger:', '&F�rsta raden:',
  // indented, hanging
  '&Indrag', '&H�ngande', 'Exempel',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Enkel', '1.5 rader', 'Dubbel', '�tminstone', 'Exakt', 'Multipel',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indrag och avst�nd', 'Tabbar', 'Textfl�de',
  // tab stop position label; buttons: set, delete, delete all
  '&Standardavst�nd:', 'A&nge', 'Rad&era', 'Radera &alla',
  // tab align group; left, right, center aligns,
  'Justering', '&V�nster', '&H�ger', '&Centrerad',
  // leader radio-group; no leader
  'Utfyllnadstecken', '(Inget)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabbstopp som ska tas bort:', '(Ingen)', 'Alla.',
  // Pagination group-box, keep with next, keep lines together
  'Numrering', '&H�ll ihop med n�sta', 'H�ll ihop &rader',
  // Outline level, Body text, Level #
  '&Konturniv�:', 'Bodytext', 'Niv� %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'F�rhandsgranska', 'Sidbredd', 'Helsida', 'Sidor:',
  // Invalid Scale, [page #] of #
  'Ange ett nummer fr�n 10 till 500', 'av %d',
  // Hints on buttons: first, prior, next, last pages
  'F�rstasidan', 'F�reg�ende sida', 'N�sta sida', 'Sista sidan',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Cellavst�nd', 'Avst�nd', '&Mellan celler', 'Fr�n tabellkanten till cellerna',
  // vertical, horizontal (x2)
  '&Vertikal:', '&Horisontell:', 'V&ertikal:', 'H&orisontell:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kantlinjer', 'Inst�llningar', '&F�rg:', '&Ljus f�rg:', '&Skuggf�rg:',
  // Width; Border type group-box;
  '&Bredd:', 'Typ av kantlinjer',
  // Border types: none, sunken, raised, flat
  '&Ingen', '&Neds�nkt', '&Upph�jd', '&Plan',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Dela', 'Dela till',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Specificera antalet rader och kolumner', '&Originalceller',
  // number of columns, rows, merge before
  'Antal &kolumner:', 'Antal &rader:', '&Sammanfoga f�r delning',
  // to original cols, rows check-boxes
  'Dela till original ko&lumner', 'Dela till original ra&der',
  // Add Rows form -------------------------------------------------------------
  'L�gg till &rader', 'Rad&antal:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Utskriftsformat', 'Sida', 'Sidhuvud och sidfot',
  // margins group-box, left, right, top, bottom
  'Marginaler (millimeter)', 'Marginaler (tum)', '&V�nster:', '&�verkant:', '&H�ger:', '&Nederkant:',
  // mirror margins check-box
  '&Speglade marginaler',
  // orientation group-box, portrait, landscape
  'Riktning', 'S&t�ende', '&Liggande',
  // paper group-box, paper size, default paper source
  'Papper', 'St&orlek:', '&K�lla:',
  // header group-box, print on the first page, font button
  'Sidhuvud', '&Text:', '&Sidhuvud p� f�rstasidan', 'Tec&kensnitt...',
  // header alignments: left, center, right
  '&V�nster', '&Centrera', '&H�ger',
  // the same for footer (different hotkeys)
  'Sidfot', 'Te&xt:', 'Sidfot p� f&�rstasidan', 'T&eckensnitt...',
  'V&�nster', 'Ce&ntrera', 'H�&ger',
  // page numbers group-box, start from
  'Sidnummer', '&B�rja p�',
  // hint about codes
  'Kombinationer f�r specialtecken:'#13'&&p - sidnummer; &&P - antal sidor; &&d - dagens datum; &&t - aktuell tid.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Kodsida f�r textfil', '&V�lj filens kodning:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japanska', 'Kinesiska (F�renklad)', 'Koreanska',
  // chinese (trad), central european, cyrillic, west european
  'Kinesiska (Traditionell)', 'Central- och �steuropeisk', 'Kyrillisk', 'V�steuropeisk',
  // greek, turkish, hebrew, arabic
  'Grekiska', 'Turkiska', 'Hebreiska', 'Arabiska',
  // baltic, vietnamese, utf-8, utf-16
  'Baltiska', 'Vietnamesiska', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Visuell stil', '&V�lj stil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Konvertera till text', 'V�lj &avgr�nsning:',
  // line break, tab, ';', ','
  'radbrytning', 'tabb', 'semikolon', 'komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'En tabell inneh�llandes sammanfogade rader kan ej sorteras',
  // title, main options groupbox
  'Tabellsortering', 'Sortering',
  // sort by column, case sensitive
  '&Sortera efter kolumn:', 'S&kiftl�gesk�nslig',
  // heading row, range or rows
  'Excludera &rubrikrad', 'Rader att sortera: fr�n %d till %d',
  // order, ascending, descending
  'Ordning', 'Sti&gande', '&Fallande',
  // data type, text, number
  'Typ', '&Text', '&Nummer',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Infoga nummer', 'Egenskaper', '&R�knarnamn:', '&Numreringstyp:',
  // numbering groupbox, continue, start from
  'Numrering', 'F&orts�tt', '&Starta fr�n:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Rubrik', '&Etikett:', '&Exkludera etikett fr�n rubrik',
  // position radiogroup
  'Position', '&Ovanf�r markerat objekt', '&Nedanf�r markerat objekt',
  // caption text
  'Rubrik&text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numrering', 'Figur', 'Tabell',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Synliga kantsidor', 'Kant',
  // Left, Top, Right, Bottom checkboxes
  '&V�nster sida', '&Topp', '&H�ger sida', '&Botten',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  '�ndra storlek p� kolumnen', '�ndra storlek p� raden',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Indrag p� f�rsta raden', 'Indrag fr�n v�nster', 'H�ngande indrag', 'Indrag fr�n h�ger',
  // Hints on lists: up one level (left), down one level (right)
  'Upp en niv�', 'Ner en niv�',
  // Hints for margins: bottom, left, right and top
  'Undre marginal', 'V�nster marginal', 'H�ger marginal', '�vre marginal',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'V�nsterjusterad tab', 'H�gerjusterad tab', 'Centrerad tab', 'Decimaljusterad tab',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normalt indrag', 'Inget mellanrum', 'Rubrik %d', 'Liststycke',
  // Hyperlink, Title, Subtitle
  'Hyperl�nk', 'Titel', 'Undertitel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Betoning', 'Diskret betoning', 'Intensiv betoning', 'Stark',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citat', 'Intensivt citat', 'Diskret referens', 'Intensiv referens',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Textblock', 'HTML-variabel', 'HTML-kod', 'HTML-akronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-definition', 'HTML-tangentbord', 'HTML-test', 'HTML-skrivmaskin',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'F�rformaterad HTML', 'HTML-citation', 'Rubrik', 'Fotnot', 'Sidnummer',
  // Caption
  'Rubrik',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Slutnotsreferens', 'Fotnotsreferens', 'Slutnotstext', 'Fotnotstext',
  // Sidenote Reference, Sidenote Text
  'Referens sidnot', 'Text sidnot',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'f�rg', 'bakgrundsf�rg', 'transparent', 'f�rvald', 'understrecksf�rg',
  // default background color, default text color, [color] same as text
  'f�rvald bakgrundsf�rg', 'f�rvald textf�rg', 'samma som text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'enkel', 'fet', 'dubbel', 'prickad', 'prickad', 'streckad',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tjockt streckad', 'l�ngt streckad', 'tjockt och l�ngt streckad',
  // underline types: dash dotted, thick dash dotted,
  'streckprickad', 'tjockt streckprickad',
  // underline types: dash dot dotted, thick dash dot dotted
  'streck och prickprickad', 'tjockt streck och prickprickad',
  // sub/superscript: not, subsript, superscript
  'ej under/superskript', 'underskript', 'superskript',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di-l�ge:', 'ned�rvt', 'v�nster till h�ger', 'h�ger till v�nster',
  // bold, not bold
  'fet', 'ej fet',
  // italic, not italic
  'kursiv', 'ej kursiv',
  // underlined, not underlined, default underline
  'understreckad', 'ej understreckad', 'f�rvald understreckad',
  // struck out, not struck out
  '�verkryssad', 'ej �verkryssad',
  // overlined, not overlined
  '�verstruken', 'ej �verstruken',
  // all capitals: yes, all capitals: no
  'endast versaler', 'versaler av',
  // vertical shift: none, by x% up, by x% down
  'utan vertikal f�rskjutning', 'f�rskjuten upp�t med %d%%', 'f�rskjuten ned�t med %d%%',
  // characters width [: x%]
  'bokstavsbredd',
  // character spacing: none, expanded by x, condensed by x
  'normalt mellanrum', '�kat mellanrum med %s', 'minskat mellanrum med %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'f�rvalt typsnitt', 'f�rvald paragraf', 'ned�rvt',
  // [hyperlink] highlight, default hyperlink attributes
  'markera', 'f�rvalda',
  // paragraph aligmnment: left, right, center, justify
  'v�nsterst�llt', 'h�gerst�llt', 'centrerat', 'justerat',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'radh�jd: %d%%', 'radmellanrum: %s', 'radh�jd: �tminstone %s',
  'radh�jd: exakt %s',
  // no wrap, wrap
  'radbrytning av', 'radbrytning',
  // keep lines together: yes, no
  'h�ll ihop raderna', 'h�ll ej ihop raderna',
  // keep with next: yes, no
  'h�ll ihop med n�sta stycke', 'h�ll inte ihop med n�sta stycke',
  // read only: yes, no
  'skrivskyddad', 'redigerbar',
  // body text, heading level x
  'konturlinjeniv�: br�dtext', 'konturlinjeniv�: %d',
  // indents: first line, left, right
  'f�rsta raden indragen', 'v�nster indrag', 'h�ger indrag',
  // space before, after
  'mellanslag f�re', 'mellanslag efter',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabbar', 'inga', 'justering', 'v�nster', 'h�ger', 'centrerad',
  // tab leader (filling character)
  'leader',
  // no, yes
  'nej', 'ja',
  // [padding/spacing/side:] left, top, right, bottom
  'v�nster', 'topp', 'h�ger', 'botten',
  // border: none, single, double, triple, thick inside, thick outside
  'ingen', 'enkel', 'dubbel', 'trippel', 'fet inv�ndigt', 'fet utv�ndigt',
  // background, border, padding, spacing [between text and border]
  'bakgrund', 'ram', 'spaltfyllnad', 'mellanrum',
  // border: style, width, internal width, visible sides
  'stil', 'bredd', 'intern bredd', 'synliga sidor',
  // style inspector -----------------------------------------------------------
  // title
  'Stilinspekt�r',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Ingen)', 'Stycke', 'Teckensnitt', 'Attribut',
  // border and background, hyperlink, standard [style]
  'Ram och bakgrund', 'Hyperl�nk', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stilar', 'Stil',
  // name, applicable to,
  '&Namn:', 'Till�mplig &p�:',
  // based on, based on (no &),
  '&Baserad p�:', 'Baserad p�:',
  //  next style, next style (no &)
  'Stil f�r &f�ljande stycke:', 'Stil f�r f�ljande paragraf:',
  // quick access check-box, description and preview
  '&Snabbtillg�ng', 'Beskrivning och &f�rhandsgranskning:',
  // [applicable to:] paragraph and text, paragraph, text
  'stycke och text', 'stycke', 'text',
  // text and paragraph styles, default font
  'Text och styckestilar', 'F�rvalt teckensnitt',
  // links: edit, reset, buttons: add, delete
  'Redigera', 'Nollst�ll', '&L�gg till', '&Radera',
  // add standard style, add custom style, default style name
  'L�gg till &standardstil...', 'L�gg till &anpassad stil', 'Stil %d',
  // choose style
  'V�lj &stil:',
  // import, export,
  '&Importera...', '&Exportera...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView-stilar (*.rvst)|*.rvst', 'Fel vid filladdning', 'Filen inneh�ller inga stilar',
  // Title, group-box, import styles
  'Importera stilar fr�n fil', 'Importera', 'I&mportera stilar:',
  // existing styles radio-group: Title, override, auto-rename
  'Existerande stilar', '&upph�v', '&l�gg till �ndrat namn',
  // Select, Unselect, Invert,
  '&Markera', '&Avmarkera', '&Invertera',
  // select/unselect: all styles, new styles, existing styles
  '&Alla stilar', '&Nya stilar', '&Existerande stilar',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Rensa format', 'Alla stilar',
  // dialog title, prompt
  'Stilar', '&V�lj stil f�r till�mpning:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonymer', '&Ignorera Alla', '&L�gg till i lexikon',
  // Progress messages ---------------------------------------------------------
  'Laddar ner %s', 'F�rbereder f�r utskrift...', 'Skriver ut sidan %d',
  'Konverterar fr�n RTF...',  'Konverterar till RTF...',
  'L�ser %s...', 'Skriver %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ingen notering', 'Fotnot', 'Slutnot', 'Sidonot', 'Textf�lt'  
  );


initialization
  RVA_RegisterLanguage(
    'Swedish', // english language name, do not translate
    'Svenska', // native language name
    ANSI_CHARSET, @Messages);

end.

