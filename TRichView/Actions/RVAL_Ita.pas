
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Italian translation                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Massimo Zago, 2003-03-27               }
{                mzago@n-aitec.com                      }
{                massimo@servicesport.it                }
{ Updated by Alconost: 2011-03-10,                      }
{                      2011-09-19,                      }
{                      2012-09-23                       }
{                      2014-04-28                       }
{*******************************************************}

unit RVAL_Ita;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'pollici', 'cm', 'mm', 'pica', 'pixel', 'punti',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'p', 'pixel', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&File', '&Modifica', 'F&ormato', 'Fo&nt', '&Paragrafo', '&Inserisci', '&Tabella', 'Fine&stra', '&?',
  // exit
  'Es&ci',
  // top-level menus: View, Tools,
  '&Visualizza', '&Strumenti',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Dimensione', 'Stile', 'Selez&iona', 'Alli&nea contenuto celle', 'Bordi c&elle',
  // menus: Table cell rotation
  'Rotazione &Cella',   
  // menus: Text flow, Footnotes/endnotes
  '&Scorrimento testo', '&Note a pi� di pagina',
  // ribbon tabs: tab1, tab2, view, table
  '&Home', '&Avanzate', '&Visualizza', '&Tabella',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Appunti', 'Carattere', 'Paragrafo', 'Elenco', 'Modifica',
  // ribbon groups: insert, background, page setup,
  'Inserisci', 'Sfondo', 'Imposta pagina',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Collegamenti', 'Note a pi� di pagina', 'Visualizzazione documento', 'Mostra/nascondi', 'Zoom', 'Opzioni',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserisci', 'Elimina', 'Operazioni', 'Bordi',
  // ribbon groups: styles 
  'Stili',
  // ribbon screen tip footer,
  'Premere F1 per la Guida',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documenti recenti', 'Salva una copia del documento', 'Anteprima e stampa del documento',
  // ribbon label: units combo
  'Unit�:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nuovo', 'Nuovo|Crea un nuovo documento vuoto',
  // TrvActionOpen
  'Ap&ri...', 'Apri|Apre un documento da disco',
  // TrvActionSave
  '&Salva', 'Salva|Salva il documento su disco',
  // TrvActionSaveAs
  'S&alva con nome...', 'Salva con nome...|Salva il documento su disco con un nuovo nome',
  // TrvActionExport
  '&Esporta...', 'Esporta|Esporta il documento in un altro formato',
  // TrvActionPrintPreview
  'Antepri&ma di stampa', 'Anteprima di stampa|Mostra come verr� stampato il documento',
  // TrvActionPrint
  'Stam&pa...', 'Stampa|Definisce le impostazioni della stampante e stampa il documento',
  // TrvActionQuickPrint
  'Stam&pa', '&Stampa veloce', 'Stampa|Stampa il documento',
   // TrvActionPageSetup
  'Imposta pa&gina...', 'Imposta pagina|Imposta margini, dimensione carta e altri parametri della pagina',  
  // TrvActionCut
  'Tagl&ia', 'Taglia|Taglia la selezione e la inserisce negli Appunti',
  // TrvActionCopy
  '&Copia', 'Copia|Copia la selezione e la inserisce negli Appunti',
  // TrvActionPaste
  'Incoll&a', 'Incolla|Inserisce il contenuto degli Appunti',
  // TrvActionPasteAsText
  'Incolla come &testo', 'Incolla come testo|Inserisce il testo dagli Appunti',  
  // TrvActionPasteSpecial
  'Incolla s&peciale...', 'Incolla speciale|Inserisce il contenuto degli appunti con un formato particolare',
  // TrvActionSelectAll
  'Sele&ziona tutto', 'Seleziona tutto|Seleziona tutto il documento',
  // TrvActionUndo
  'Ann&ulla', 'Annulla|Annulla gli effetti dell''ultima operazione',
  // TrvActionRedo
  '&Ripristina', 'Ripristina|Ripristina l''ultima operazione annullata',
  // TrvActionFind
  '&Trova...', 'Trova|Trova il testo specificato nel documento',
  // TrvActionFindNext
  'T&rova successivo', 'Trova successivo|Continua l''ultima ricerca',
  // TrvActionReplace
  '&Sostituisci...', 'Sostituisci|Trova il testo specificato nel documento e lo sostituisce come indicato',
  // TrvActionInsertFile
  '&File...', 'Inserisci File|Inserisce il contenuto di un file nel documento',
  // TrvActionInsertPicture
  'I&mmagine...', 'Inserisci Immagine|Inserisci un''immagine da disco',
  // TRVActionInsertHLine
  'Linea ori&zzontale', 'Inserisci linea orizzontale|Inserisce una linea orizzontale',
  // TRVActionInsertHyperlink
  '&Link ipertestuale...', 'Inserisci link ipertestuale|Inserisce un link (riferimento) ipertestuale',
  // TRVActionRemoveHyperlinks
  '&Rimuovi collegamenti ipertestuali', 'Rimuovi collegamenti ipertestuali|Rimuove tutti i collegamenti ipertestuali dal testo selezionato',  
  // TrvActionInsertSymbol
  '&Simbolo...', 'Inserisci simbolo|Inserisce un simbolo',
  // TrvActionInsertNumber
  '&Numero...', 'Inserisci Numero|Inserisce un numero',
  // TrvActionInsertCaption
  'Inserisci &Didascalia...', 'Inserisci Didascalia|Inserisce una didascalia per l oggetto selezionato',
  // TrvActionInsertTextBox
  '&Casella di testo', 'Inserisci Casella di testo|Inserisce una casella di testo',
  // TrvActionInsertSidenote
  '&Nota', 'Inserisci Nota|Inserisci una nota mostrata in una casella di testo',
  // TrvActionInsertPageNumber
  '&Numero di Pagina', 'Inserisci Numero di Pagina|Inserisci un numero di pagina',
  // TrvActionParaList
  'Elenchi p&untati e numerati...', 'Elenchi puntati e numerati|Applica o modifica un elenco puntato o numerato al paragrafo selezionato',
  // TrvActionParaBullets
  '&Punto elenco', 'Punto elenco|Aggiunge o rimuove un punto elenco per il paragrafo selezionato',
  // TrvActionParaNumbering
  '&Numerazione', 'Numerazione|Aggiunge o rimuove numerazione per il paragrafo selezionato',
  // TrvActionColor
  '&Colore di sfondo...', 'Sfondo|Cambia il colore di sfondo del documento',
  // TrvActionFillColor
  'Colore di rie&mpimento...', 'Colore di riempimento|Cambia il colore di sfondo di testo, paragrafo, cella, tabella o documento',
  // TrvActionInsertPageBreak
  '&Inserisci interruzione di pagina', 'Inserisci interruzione di pagina|Inserisce un''interruzione di pagina',
  // TrvActionRemovePageBreak
  '&Rimuovi interruzione di pagina', 'Rimuovi interruzione di pagina|Rimuove l''interruzione di pagina',
  // TrvActionClearLeft
  'Interrompi scorrimento a &sinistra', 'Interrompi scorrimento a sinistra|Interrompe lo scorrimento del testo intorno a un''immagine allineata a sinistra',
  // TrvActionClearRight
  'Interrompi scorrimento a &destra', 'Interrompi scorrimento a destra|Interrompe lo scorrimento del testo intorno a un''immagine allineata a destra',
  // TrvActionClearBoth
  'Interrompi scorrimento da &entrambi i lati', 'Interrompi scorrimento da entrambi i lati|Interrompe lo scorrimento del testo intorno a un''immagine allineata a sinistra o a destra',
  // TrvActionClearNone
  '&Scorrimento normale', 'Scorrimento normale|Permette lo scorrimento del testo intorno a un''immagine allineata a sinistra o a destra',
  // TrvActionVAlign
  '&Posizione oggetto...', 'Posizione oggetto|Cambia la posizione dell''oggetto selezionato',    
  // TrvActionItemProperties
  '&Propriet� oggetto...', 'Propriet� oggetto|Definisce le propriet� dell''oggetto attivo',
  // TrvActionBackground
  '&Sfondo...', 'Sfondo|Sceglie il colore o l''immagine di sfondo',
  // TrvActionParagraph
  '&Paragrafo...', 'Paragrafo|Cambia gli attributi del paragrafo',
  // TrvActionIndentInc
  'Aumenta &indentazione', 'Aumenta indentazione|Aumenta l''indentazione sinistra del paragrafo selezionato',
  // TrvActionIndentDec
  '&Diminuisci indentazione', 'Diminuisci indentazione|Diminuisce l''indentazione sinistra del paragrafo selezionato',
  // TrvActionWordWrap
  'A &capo automatico', 'A capo automatico|Imposta la modalit� di "a capo automatico"',
  // TrvActionAlignLeft
  'A&llinea a sinistra', 'Allinea a sinistra|Allinea a sinistra il testo selezionato',
  // TrvActionAlignRight
  'Allinea a dest&ra', 'Allinea a sinistra|Allinea a sinistra il testo selezionato',
  // TrvActionAlignCenter
  '&Centra', 'Centra|Centra il testo selezionato',
  // TrvActionAlignJustify
  '&Giustifica', 'Giustifica|Giustifica il testo selezionato sia a destra che a sinistra',
  // TrvActionParaColor
  'Colore s&fondo paragrafo...', 'Colore sfondo paragrafo|Imposta il colore di sfondo del paragrafo',
  // TrvActionLineSpacing100
  'Interlinea &singola', 'Interlinea singola|Imposta interlinea singola',
  // TrvActionLineSpacing150
  'I&nterlinea 1,5 righe', 'Interlinea 1,5 righe|Imposta interlinea 1,5 righe',
  // TrvActionLineSpacing200
  'Interlinea &doppia', 'Interlinea doppia|Imposta interlinea doppia',
  // TrvActionParaBorder
  '&Bordi e sfondo paragrafo...', 'Bordi e sfondo paragrafo|Imposta i bordi e lo sfondo del paragrafo selezionato',
  // TrvActionInsertTable
  'Inserisci ta&bella...', '&Tabella', 'Inserisci tabella|Inserisci nuova tabella',
  // TrvActionTableInsertRowsAbove
  'Inserisci rig&a sopra', 'Inserisci riga sopra|Inserisce una nuova riga sopra le celle selezionate',
  // TrvActionTableInsertRowsBelow
  'Inserisci riga s&otto', 'Inserisci riga sotto|Inserisce una nuova riga sotto le celle selezionate',
  // TrvActionTableInsertColLeft
  'Inserisci co&lonna a sinistra', 'Inserisci colonna a sinistra|Inserisce una nuova colonna a sinistra delle celle selezionate',
  // TrvActionTableInsertColRight
  'Inserisci colonna a &destra', 'Inserisci colonna a destra|Inserisce una nuova colonna a destra delle celle selezionate',
  // TrvActionTableDeleteRows
  'Elimina ri&ga', 'Elimina riga|Elimina la riga contenente le celle selezionate',
  // TrvActionTableDeleteCols
  'Eli&mina colonna', 'Elimina colonna|Elimina la colonna contenente le celle selezionate',
  // TrvActionTableDeleteTable
  '&Elimina tabella', 'Elimina tabella|Elimina la tabella',
  // TrvActionTableMergeCells
  'Un&isci celle', 'Unisci celle|Unisce le celle selezionate',
  // TrvActionTableSplitCells
  '&Dividi celle', 'Dividi celle|Divide le celle selezionate',
  // TrvActionTableSelectTable
  'Seleziona &tabella', 'Seleziona tabella|Seleziona la tabella',
  // TrvActionTableSelectRows
  'Seleziona rig&he', 'Seleziona righe|Seleziona righe',
  // TrvActionTableSelectCols
  'Seleziona colon&ne', 'Seleziona colonne|Seleziona colonne',
  // TrvActionTableSelectCell
  'Sele&ziona cella', 'Seleziona cella|Seleziona cella',
  // TrvActionTableCellVAlignTop
  'Allinea cella al margine s&uperiore', 'Allinea cella al margine superiore|Allinea il contenuto della cella al margine superiore',
  // TrvActionTableCellVAlignMiddle
  'Allinea cella al &centro', 'Align cella al centro|Allinea verticalmente il contenuto della cella al centro della stessa',
  // TrvActionTableCellVAlignBottom
  'Allinea cella al margine in&feriore', 'Allinea cella al margine inferiore|Allinea il contenuto della cella al margine inferiore',
  // TrvActionTableCellVAlignDefault
  'Allineamento verticale &predefinito', 'Allineamento verticale predefinito|Imposta l''allineamento verticale predefinito per le celle selezionate',
  // TrvActionTableCellRotationNone
  '&Nessuna rotazione cella', 'Nessuna rotazione cella|Ruota contenuti cella di 0�',
  // TrvActionTableCellRotation90
  'Ruota cella di &90�', 'Ruota cella di 90�|Ruota contenuti cella di 90�',
  // TrvActionTableCellRotation180
  'Ruota cella di &180�', 'Ruota cella di 180�|Ruota contenuti cella di 180�',
  // TrvActionTableCellRotation270
  'Ruota cella di &270�', 'Ruota cella di 270�|Ruota contenuti cella di 270�',
  // TrvActionTableProperties
  'P&ropriet� tabella...', 'Propriet� tabella|Cambia propriet� della tabella selezionata',
  // TrvActionTableGrid
  'Mo&stra griglia', 'Mostra griglia|Mostra o nasconde le linee della griglia',
  // TRVActionTableSplit
  'Dividere tabella', 'Dividere tabella|Divide la tabella in due tabelle partendo dalla riga selezionata',
  // TRVActionTableToText
  'Convertire in testo...', 'Convertire in testo|Converte la tabella in testo',
  // TRVActionTableSort
  '&Selezionare...', 'Selezionare|Seleziona le righe della tabella',
  // TrvActionTableCellLeftBorder
  'Bordo &sinistro', 'Bordo sinistro|Mostra o nasconde il bordo sinistro della cella',
  // TrvActionTableCellRightBorder
  'Bordo &destro', 'Bordo destro|Mostra o nasconde il bordo destro della cella',
  // TrvActionTableCellTopBorder
  'Bordo &superiore', 'Bordo superiore|Mostra o nasconde il bordo superiore della cella',
  // TrvActionTableCellBottomBorder
  'Bordo &inferiore', 'Bottom Border|Mostra o nasconde il bordo inferiore della cella',
  // TrvActionTableCellAllBorders
  '&Tutti i bordi', 'Tutti i bordi|Mostra tutti i bordi della cella',
  // TrvActionTableCellNoBorders
  '&Nessun bordo', 'Nessun bordo|Nasconde tutti i bordi della cella',
  // TrvActionFonts & TrvActionFontEx
  '&Font...', 'Font|Cambia font al testo selezionato',
  // TrvActionFontBold
  '&Grassetto', 'Grassetto|Aggiunge/toglie il grassetto allo stile del testo selezionato',
  // TrvActionFontItalic
  '&Corsivo', 'Corsivo|Aggiunge/toglie il corsivo allo stile del testo selezionato',
  // TrvActionFontUnderline
  '&Sottolinea', 'Sottolinea|Aggiunge/toglie la sottolineatura allo stile del testo selezionato',
  // TrvActionFontStrikeout
  '&Barrato', 'Barrato|Aggiunge/toglie la barratura allo stile del testo selezionato',
  // TrvActionFontGrow
  'Al&za font', 'Alza font|Aumenta l''altezza del font del testo selezionato del 10%',
  // TrvActionFontShrink
  'Ab&bassa font', 'Abbassa font|Diminuisce l''altezza del font del testo selezionato del 10%',
  // TrvActionFontGrowOnePoint
  'I&ngrandisci font di un punto', 'Ingrandisce font di un punto|Aumenta la dimensione del font del testo selezionato di un punto',
  // TrvActionFontShrinkOnePoint
  'Rim&picciolisci font di un punto', 'Rimpicciolisce font di un punto|Diminuisci la dimensione del font del testo selezionato di un punto',
  // TrvActionFontAllCaps
  'Tutto &maiuscolo', 'Tutto maiuscolo|Mette in maiuscolo tutti i caratteri del testo selezionato',
  // TrvActionFontOverline
  'Sopra&lineato', 'Sopralineato|Aggiunge una linea sopra il testo selezionato',
  // TrvActionFontColor
  '&Colore del testo...', 'Colore del testo|Cambia il colore del testo selezionato',
  // TrvActionFontBackColor
  'Colore di sf&ondo del testo...', 'Colore di sfondo del testo|Cambia il colore di sfondo del testo selezionato',
  // TrvActionAddictSpell3
  'Controllo or&tografico', 'Controllo ortografico|Esegue il controllo ortografico',
  // TrvActionAddictThesaurus3
  'T&hesaurus', 'Thesaurus|Cerca sinonimi per la parola selezionata',
  // TrvActionParaLTR
  'Da sinistra a destra', 'Da sinistra a destra|Imposta la direzione del testo da sinistra a destra per il paragrafo selezionato',
  // TrvActionParaRTL
  'Da destra a sinistra', 'Da destra a sinistra|Imposta la direzione del testo da destra a sinistra per il paragrafo selezionato',
  // TrvActionLTR
  'Testo da sinistra a destra', 'Testo da sinistra a destra|Imposta la direzione del testo da sinistra a destra per il testo selezionato',
  // TrvActionRTL
  'Testo da destra a sinistra', 'Testo da destra a sinistra|Imposta la direzione del testo da destra a sinistra per il testo selezionato',
  // TrvActionCharCase
  'Maiuscolo/Minuscolo', 'Maiuscolo/Minuscolo|Cambia impostazione maiuscolo/minuscolo del testo selezionato',
  // TrvActionShowSpecialCharacters
  'Caratteri &non stampabili', 'Caratteri non stampabili|Mostra o nascondi caratteri non stampabili, come segnaposti di paragrafo, tabulatori e spazi',
  // TrvActionSubscript
  '&Pedice', 'Pedice|Formatta il testo selezionato come pedice',
  // TrvActionSuperscript
  'Apice', 'Apice|Formatta il testo selezionato come apice',
  // TrvActionInsertFootnote
  '&Nota a pi� di pagina', 'Nota a pi� di pagina|Inserisce una nota a pi� di pagina',
  // TrvActionInsertEndnote
  '&Nota di chiusura', 'Nota di chiusura|Inserisce una nota di chiusura',
  // TrvActionEditNote
  'M&odifica nota', 'Modifica nota|Inizia la modifica della nota a pi� di pagina o di chiusura',
  '&Nascondi', 'Nascondi|Nasconde o mostra il frammento selezionato',            
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stili...', 'Stili|Apre la finestra di gestione degli stili',
  // TrvActionAddStyleTemplate
  '&Aggiungi stile...', 'Aggiungi stile|Crea un nuovo stile di testo o di paragrafo',
  // TrvActionClearFormat,
  '&Cancella formattazione', 'Cancella formattazione|Cancella la formattazione di testo o paragrafo per il frammento selezionato',
  // TrvActionClearTextFormat,
  'Cancella formattazione &testo', 'Cancella formattazione testo|Cancella la formattazione del testo per il frammento selezionato',
  // TrvActionStyleInspector
  'Controllo st&ili', 'Controllo stili|Mostra o nasconde il Controllo stili',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Annulla', 'Chiudi', 'Inserisci', '&Apri...', '&Salva...', '&Cancella', '?', 'Rimuovi',
  // Others  -------------------------------------------------------------------
  // percents
  'percento',
  // left, top, right, bottom sides
  'Parte sinistra', 'Parte superiore', 'Parte destra', 'Parte inferiore',
  // save changes? confirm title
  'Salvare i cambiamenti a %s?', 'Conferma',
  // warning: losing formatting
  '%s potrebbe contenere elementi che non sono compatibili con il formato scelto per il salvataggio.'#13+
  'Vuoi salvare il documento in questo formato?',
  // RVF format name
  'Formato RichView',
  // Error messages ------------------------------------------------------------
  'Errore',
  'Errore nel caricamento del file.'#13#13'Cause possibili:'#13'- il formato del file non � supportato da questa applicazione;'#13+
  '- il file � rovinato;'#13'- il file � aperto e bloccato da un''altra applicazione.',
  'Errore nel caricamento del file di immagine.'#13#13'Cause possibili:'#13'- il file contiene un''immagine in un formato non supportato da questa applicazione;'#13+
  '- il file non contiene un''immagine;'#13+
  '- il file � rovinato;'#13'- il file � aperto e bloccato da un''altra applicazione.',
  'Errore nel salvataggio del file.'#13#13'Cause possibili:'#13'- la dimensione del file eccede la quantit� di spazio libero del disco;'#13+
  '- il disco � protetto da scrittura;'#13'- non � stato inserito alcun disco;'#13+
  '- il file � aperto e bloccato da un''altra applicazione;'#13'- il disco � rovinato.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView (*.rvf)|*.rvf',  'Rich Text Format (*.rtf)|*.rtf' , 'XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Solo testo (*.txt)|*.txt', 'Solo testo - Unicode (*.txt)|*.txt', 'Solo testo - Ricerca automatica (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'File HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Semplificato (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documenti di Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Ricerca completata', 'Stringa cercata ''%s'' non trovata.',
  // 1 string replaced; Several strings replaced
  '1 stringa sostituita.', '%d stringhe sostituite.',
  // continue search from the beginning/end?
  'E'' stata raggiunta la fine del documento. Vuoi continuare la ricerca dall''inizio?',
  'E'' stato raggiunto l''inizio del documento. Vuoi continuare la ricerca dalla fine?',  
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Trasparente', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Nero', 'Marrone', 'Verde oliva', 'Verde scuro', 'Alzavola scuro', 'Blu scuro', 'Indaco', 'Grigio-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Rosso scuro', 'Arancione', 'Giallo scuro', 'Verde', 'Alzavola', 'Blu', 'Grigio-Blu', 'Grigio-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rosso', 'Arancione chiaro', 'Lime', 'Verde mare', 'Acqua', 'Blu chiaro', 'Violetto', 'Grigio-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Oro', 'Giallo', 'Verde luminoso', 'Turchese', 'Blu cielo', 'Prugna', 'Grigio-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosato', 'Tan�', 'Giallo chiaro', 'Verde chiaro', 'Turchese chiaro', 'Blu pallido', 'Lavanda', 'Bianco',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Traspare&nte', '&Auto', 'A&ltri colori...', '&Predefiniti',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Sfondo', 'C&olore:', 'Posizione', 'Sfondo', 'Testo esempio.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nessuna', '&Area intera', 'Riquadro f&isso', '&Riquadro', 'C&entro',
  // Padding button
  '&Motivo...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Colore di riempimento', '&Applica a:', 'A&ltri colori...', '&Motivo...',
  'Seleziona un colore',
  // [apply to:] text, paragraph, table, cell
  'testo', 'paragrafo' , 'tabella', 'cella',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Font', 'Font', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Font:', 'Dimen&sione', 'Stile', '&Grassetto', '&Corsivo',
  // Script, Color, Back color labels, Default charset
  'Co&difica:', 'Co&lore:', 'S&fondo:', '(Predefinita)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effetti', 'Sottolineatura', 'S&opralineato', '&Barrato', 'Tutto &maiuscolo',
  // Sample, Sample text
  'Esempio', 'Testo esempio',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Spaziatura caratteri', '&Spaziatura:', '&Espansa', '&Condensata',
  // Offset group-box, Offset label, Down, Up,
  'Scostamento verticale', '&Scostamento:', 'In &gi�', 'In &su',
  // Scaling group-box, Scaling
  'Ridensionamento', 'Ri&dimensionamento:',
  // Sub/super script group box, Normal, Sub, Super
  'Pedice e apice', '&Normale', 'P&edice', 'A&pice',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Margine interno', '&Sopra:', 'S&inistra:', 'So&tto:', 'Dest&ra:', 'Valori &uguali',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Inserisci ipertesto', 'Ipertesto', 'T&esto:', '&Destinazione', '<<selezione>>',
  // cannot open URL
  'Impossibile aprire "%s"',
  // hyperlink properties button, hyperlink style
  '&Personalizza...', '&Stile:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Attributi ipertesto', 'Colori normali', 'Colori attivi',
  // Text [color], Background [color]
  '&Testo:', 'S&fondo',
  // Underline [color]
  'Sottoli&neatura:', 
  // Text [color], Background [color] (different hotkeys)
  'T&esto:', 'Sfon&do',
  // Underline [color], Attributes group-box,
  'Sottoli&neatura:', 'Attributi',
  // Underline type: always, never, active (below the mouse)
  '&Sottolineatura:', 'sempre', 'mai', 'attivi',
  // Same as normal check-box
  'Come il &normale',
  // underline active links check-box
  '&Sottolinea link attivi',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Inserisci simbolo', '&Font:', 'Set di &caratteri:', 'Unicode',
  // Unicode block
  '&Blocco:',  
  // Character Code, Character Unicode Code, No Character
  'Codice carattere: %d', 'Codice carattere: Unicode %d', '(nessun carattere)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Inserisci tabella', 'Dimensione tabella', 'Numero di &colonne:', 'Numero di &righe:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Layout tabella', 'Ridimensionamento &automatico', 'Dimensione a riempire la &finestra', 'Dimensionamento &manuale',
  // Remember check-box
  'Ricorda &dimensione per nuove tabelle',
  // VAlign Form ---------------------------------------------------------------
  'Posizione',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriet�', 'Immagine', 'Posizione e Dimensione', 'Linea', 'Tabella', 'Righe', 'Celle',
  // Image Appearance, Image Misc, Number tabs
  'Aspetto', 'Varie', 'Numero',
  // Box position, Box size, Box appearance tabs
  'Posizione', 'Dimensione', 'Aspetto',
  // Preview label, Transparency group-box, checkbox, label
  'Anteprima:', 'Trasparenza', '&Trasparente', '&Colore transparenza:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'Cam&bia...', '&Salva...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Allineamento verticale', '&Allineamento:',
  'margine inferiore alla linea base del testo', 'centro alla linea base del testo',
  'margine superiore alla linea alto della riga', 'margine inferiore alla linea basso della riga', 'centro alla linea centro della riga',
  // align to left side, align to right side
  'a sinistra', 'a destra',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Sposta di:', 'Allunga', '&Ampiezza:', '&Altezza:', 'Dimensione predefinita: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Scala &proporzionalmente', '&Reset',
  // Inside the border, border, outside the border groupboxes
  'Dentro il bordo', 'Bordo', 'Fuori dal bordo',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Riempimento:', '&Padding:',
  // Border width, Border color labels
  '&Larghezza del bordo:', '&Colore del bordo:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Interlinea &orizzontale:', 'Interlinea &verticale:',
  // Miscellaneous groupbox, Tooltip label
  'Varie', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Testo alternativo:',
  // Horz line group-box, color, width, style
  'Linea orizzontale', '&Colore:', '&Ampiezza:', '&Stile:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabella', 'Ampie&zza:', 'Colore &riempimento:', '&Spaziatura tra celle...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Padding di cella &orizzontale:', 'Padding di cella &verticale:', 'Bordo e sfondo',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Lati visibili del &bordo...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Mantieni il contenuto insieme',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotazione', '&Nessuna', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Bordo:', 'Lati Visibili del B&ordo...',
  // Border, CellBorders buttons
  'Bordi &tabella...', 'Bordi &cella...',
  // Table border, Cell borders dialog titles
  'Bordi tabella', 'Bordi celle predefiniti',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Stampa', '&Mantieni la tabella in un''unica pagina', 'Numero righe di &intestazione:',
  'Le righe di intestazione vengono ripetute in ogni pagina delle tabelle',
  // top, center, bottom, default
  'Al&to', '&Centro', '&Basso', '&Predefinito',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Impostazioni', '&Ampiezza preferita:', 'A&ltezza minima:', 'Colore &riempimento:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Bordo', 'Parti visibili:',  'C&olore ombreggiatura:', 'Colore &luce', 'C&olor:',
  // Background image
  '&Immagine...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Posizione orizzontale', 'Posizione verticale', 'Posizione nel testo',
  // Position types: align, absolute, relative
  'Allinea', 'Posizione assoluta', 'Posizione relativa',
  // [Align] left side, center, right side
  'al lato sinistro della casella col lato sinistro di',
  'al centro della casella con il centro di',
  'al lato destro della casella col lato destro di',
  // [Align] top side, center, bottom side
  'al lato superiore della casella col lato superiore di',
  'al centro della casella con centro di',
  'al lato inferiore della casella col lato inferiore di',
  // [Align] relative to
  'relativamente a',
  // [Position] to the right of the left side of
  'alla destra del lato sinistro di',
  // [Position] below the top side of
  'sotto il lato superiore di',
  // Anchors: page, main text area (x2)
  '&Pagina', '&Area di testo principale', 'P&agina', 'Area di te&sto principale',
  // Anchors: left margin, right margin
  'Margine &sinistro', 'Margine &destro',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Margine &superiore', 'Margine &inferiore', 'Margine &interno', 'Margine &esterno',
  // Anchors: character, line, paragraph
  '&Carattere', 'L&inea', 'Para&grafo',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Margini specchia&ti', 'La&yout nella cella della tabella',
  // Above text, below text
  'Testo &sopra', 'Testo &sotto',
  // Width, Height groupboxes, Width, Height labels
  'Larghezza', 'Altezza', '&Larghezza:', '&Altezza:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Allineamento verticale', 'In &alto', 'Al &centro', 'In &basso',
  // Border and background button and title
  'B&ordo e sfondo...', 'Casella di Bordo e Sfondo',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Incolla speciale', '&Incolla come:', 'Rich text format (RTF)', 'Formato HTML',
  'Testo', 'Testo unicode', 'Immagine Bitmap', 'Immagine Metafile',
  'File grafici', 'URL',  
  // Options group-box, styles
  'Opzioni', '&Stili:',
  // style options: apply target styles, use source styles, ignore styles
  'applica stili del documento di destinazione', 'mantieni stili e aspetto',
  'mantieni aspetto, ignora stili',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Elenchi puntati e numerati', 'Elenchi puntati', 'Elenchi numerati', 'Liste di punti', 'Liste di numeri',
  // Customize, Reset, None
  '&Personalizza...', '&Azzera', 'Nessuno',
  // Numbering: continue, reset to, create new
  'continua numerazione', 'riparti da', 'crea nuova lista iniziando da',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Lettere maiuscole', 'Numeri romani maiuscoli', 'Numeri', 'Lettere minuscole', 'Numeri romani minuscoli',
  // Bullet type
  'Cerchietto', 'Punto', 'Quadratino',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Livello:', '&Inizia da:', '&Continua', 'Numerazione',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personalizza lista', 'Livelli', '&Conteggio:', 'Propriet� lista', 'Tipo &lista:',
  // List types: bullet, image
  'punto', 'immagine', 'Inserisci numero|',
  // Number format, Number, Start level from, Font button
  'Formato &numero:', 'Numero', '&Comincia da:', '&Font...',
  // Image button, bullet character, Bullet button,
  '&Immagine...', 'C&arattere punto', '&Punto elenco...',
  // Position of list text, bullet, number, image, text
  'Posizione testo lista', 'Posizione punto elenco', 'Posizione numero', 'Posizione immagine', 'Posizione testo',
  // at, left indent, first line indent, from left indent
  '&di:', 'Indentazione &sinistra:', 'Indentazione &prima linea:', 'dall''indentazione sinistra',
  // [only] one level preview, preview
  'Anteprima di &un livello', 'Anteprima',
  // Preview text
  'Testo testo testo testo. Testo testo testo testo. Testo testo testo testo. Testo testo testo testo. Testo testo testo testo. Testo testo testo testo.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'allinea a sinistra', 'allinea a destra', 'allinea al centro',
  // level #, this level (for combo-box)
  'Livello %d', 'Questo livello',
  // Marker character dialog title
  'Modifica carattere punto elenco',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Bordo e sfondo paragrafo', 'Bordo', 'Sfondo',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Impostazioni', '&Colore:', '&Ampiezza:', 'Ampiezza int&erna:', 'Sco&stamenti...',
  // Sample, Border type group-boxes;
  'Esempio', 'Tipo bordo',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nessuno', '&Singolo', '&Doppio', '&Triplo', 'Spesso &internamente', 'Spesso &esternamente',
  // Fill color group-box; More colors, padding buttons
  'Colore riempimento', '&Altri colori...', '&Motivo...',
  // preview text
  'Testo testo testo testo. Testo testo testo testo. Testo testo testo testo.',
  // title and group-box in Offsets dialog
  'Scostamento', 'Scostamento bordi',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragrafo', 'Allineamento', '&Sinistra', 'Dest&ra', '&Centro', '&Giustificato',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Spaziatura', '&Prima:', '&Dopo:', '&Tra le linee:', 'p&er:',
  // Indents group-box; indents: left, right, first line
  'Indentazioni', '&Sinistra:', 'Dest&ra:', '&Prima linea:',
  // indented, hanging
  '&Indentato', '&Sporgente', 'Esempio',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Singola', '1,5 linee', 'Doppia', 'Minima', 'Esatta', 'Multipla',
  // preview text
  'Testo testo testo testo. Testo testo testo testo. Testo testo testo testo. Testo testo testo testo. Testo testo testo testo. Testo testo testo testo.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indentazioni e spaziatura', 'Tabulazioni', 'Distribuzione testo',
  // tab stop position label; buttons: set, delete, delete all
  'Posizione &tabulazioni:', 'Impo&sta', 'Cance&lla', 'Cancella t&utto',
  // tab align group; left, right, center aligns,
  'Allineamento', 'A &sinistra', 'A &destra', '&Centrato',
  // leader radio-group; no leader
  'Carattere di riempimento', '(Nessuno)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulazioni da cancellare:', '(Nessuna)', 'Tutte.',
  // Pagination group-box, keep with next, keep lines together
  'Impaginazione', 'Mantieni con il &successivo', 'Mantieni assieme le rig&he',
  // Outline level, Body text, Level #
  '&Livello struttura:', 'Corpo del testo', 'Livello %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Anteprima di stampa', 'Ampiezza pagina', 'Pagina intera', 'Pagine:',
  // Invalid Scale, [page #] of #
  'Inserisci un numero compreso tra 10 e 500', 'di %d',
  // Hints on buttons: first, prior, next, last pages
  'Prima pagina', 'Pagina precedente', 'Pagina successiva', 'Ultima pagina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Spaziatura cella', 'Spaziatura', '&Tra le celle', 'Dal bordo delle tabella alle celle',
  // vertical, horizontal (x2)
  '&Verticale:', '&Orizzontale:', 'V&erticale:', 'O&rizzontale:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Bordi', 'Settaggi', '&Colori:', 'Colore &luce:', 'Colore &ombra:',
  // Width; Border type group-box;
  '&Ampiezza:', 'Tipo bordo',
  // Border types: none, sunken, raised, flat
  '&Nessuno', 'In&cassato', 'In &rilievo', '&Piatto',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Dividi', 'Dividi in',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Numero &specificato di righe e colonne', 'Celle &originali',
  // number of columns, rows, merge before
  'Numero di &colonne:', 'Numero di &righe:', '&Unisci prima di dividere',
  // to original cols, rows check-boxes
  'Dividi ripristinando le co&lonne originali', 'Dividi ripristinando le rig&he originali',
  // Add Rows form -------------------------------------------------------------
  'Aggiungi righe', '&Conteggio righe:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Imposta pagina', 'Pagina', 'Intestazione e pi� di pagina',
  // margins group-box, left, right, top, bottom
  'Margini (millimetri)', 'Margini (pollici)', 'S&inistra:', '&Superiore:', 'Dest&ra:', 'In&feriore:',
  // mirror margins check-box
  '&Inverti margini destro e sinistro per pagine pari/dispari',
  // orientation group-box, portrait, landscape
  'Orientamento', '&Verticale', '&Orizzontale',
  // paper group-box, paper size, default paper source
  'Carta', 'Di&mensione:', '&Alimentazione:',
  // header group-box, print on the first page, font button
  'Intestazione', '&Testo:', '&Intestazione nella prima pagina', '&Font...',
  // header alignments: left, center, right 
  '&Sinistra', '&Centro', '&Destra',
  // the same for footer (different hotkeys)
  'Pi� di pagina', 'T&esto', 'Pi� di pagina nella prima pa&gina', 'F&ont...',
  'Si&nistra', 'Cent&ro', 'Destr&a',
  // page numbers group-box, start from
  'Numeri di pagina', 'Ini&zia da',
  // hint about codes
  'Combinazioni speciali di caratteri:'#13'&&p - numero pagina; &&P - conteggio pagine; &&d - data corrente; &&t - orario corrente.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Pagina codice file di testo', '&Scegliere la codifica del file:',
  // thai, japanese, chinese (simpl), korean
  'Thailandese', 'Giapponese', 'Cinese (Semplificato)', 'Coreano',
  // chinese (trad), central european, cyrillic, west european
  'Cinese (Tradizionale)', 'Europeo centrale ed orientale', 'Cirillico', 'Europeo occidentale',
  // greek, turkish, hebrew, arabic
  'Greco', 'Turco', 'Ebreo', 'Arabo',
  // baltic, vietnamese, utf-8, utf-16
  'Baltico', 'Vietnamita', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Stile visuale', '&Selezionare stile:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertire in testo', 'Selezionare &delimitatore:',
  // line break, tab, ';', ','
  'separatore di linea', 'tabella', 'punto e virgola', 'virgola',
  // Table sort form -----------------------------------------------------------
  // error message
  'Non puo'' essere selezionata una tabella contenente righe incorporate',
  // title, main options groupbox
  'Selezionare tabella', 'Selezionare',
  // sort by column, case sensitive
  '&Selezionare per colonna:', '&Maiuscole e minuscole',
  // heading row, range or rows
  'Escludere &riga d''intestazione', 'Righe da selezionare: da %d a %d',
  // order, ascending, descending
  'Ordine', '&Ascendente', '&Discendente',
  // data type, text, number
  'Tipo', '&Testo', '&Numero',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserisci numero', 'Propriet�', 'Nome del &contatore:', 'Tipo di &numerazione:',
  // numbering groupbox, continue, start from
  'Numerazione', 'C&ontinua', '&Comincia da:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Didascalia', '&Etichetta:', '&Escludi l etichetta dalla didascalia',
  // position radiogroup
  'Posizione', 'Oggetti selezionati &sopra', 'Oggetti selezionati &sotto',
  // caption text
  '&Testo della didascalia:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numerazione', 'Figura', 'Tabella',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Lati del bordo visibili', 'Bordo',
  // Left, Top, Right, Bottom checkboxes
  'Lato &sinistro', 'Lato &superiore', 'Lato &destro', 'Lato &inferiore',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Ridimensiona Colonne Tabella', 'Ridimensiona Righe Tabella',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Rientro prima riga', 'Rientro sinistro', 'Rientro sporgente', 'Rientro destro',
  // Hints on lists: up one level (left), down one level (right)
  'Un livello superiore', 'Un livello inferiore',
  // Hints for margins: bottom, left, right and top
  'Margine inferiore', 'Margine sinistra', 'Margine destra', 'Margine superiore',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulazione sinistra', 'Tabulazione destra', 'Tabulazione centrata', 'Tabulazione decimale',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normale', 'Rientro normale', 'Senza spaziatura', 'Intestazione %d', 'Paragrafo elenco',
  // Hyperlink, Title, Subtitle
  'Collegamento ipertestuale', 'Titolo', 'Sottotitolo',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Enfasi (corsivo)', 'Enfasi delicata', 'Enfasi intensa', 'Enfasi (grassetto)',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citazione', 'Citazione intensa', 'Riferimento delicato', 'Riferimento intenso',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Testo del blocco', 'Variabile HTML', 'Codice HTML', 'Acronimo HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definizione HTML', 'Tastiera HTML', 'Esempio HTML', 'Macchina da scrivere HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Preformattato HTML', 'Citazione HTML', 'Intestazione', 'Pi� di pagina', 'Numero pagina',
  // Caption
  'Didascalia',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Rimando nota di chiusura', 'Rimando nota a pi� di pagina', 'Testo nota di chiusura', 'Testo nota a pi� di pagina',
  // Sidenote Reference, Sidenote Text
  'Riferimento della Nota', 'Testo della Nota',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'colore', 'colore di sfondo', 'trasparente', 'predefinito', 'colore sottolineatura',
  // default background color, default text color, [color] same as text
  'colore sfondo predefinito', 'colore testo predefinito', 'come il testo',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'singola', 'spessa', 'doppia', 'punteggiata', 'punteggiata spessa', 'tratteggiata',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tratteggiata spessa', 'tratteggiata lunga', 'tratteggiata lunga spessa',
  // underline types: dash dotted, thick dash dotted,
  'tratto punto', 'tratto punto spesso',
  // underline types: dash dot dotted, thick dash dot dotted
  'tratto punto punto', 'tratto punto punto spesso',
  // sub/superscript: not, subsript, superscript
  'no pedice/apice', 'pedice', 'apice',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'modalit� BiDi:', 'ereditata', 'da sinistra a destra', 'da destra a sinistra',
  // bold, not bold
  'grassetto', 'non grassetto',
  // italic, not italic
  'corsivo', 'non corsivo',
  // underlined, not underlined, default underline
  'sottolineato', 'non sottolineato', 'sottolineatura predefinita',
  // struck out, not struck out
  'barrato', 'non barrato',
  // overlined, not overlined
  'sopralineato', 'non sopralineato',
  // all capitals: yes, all capitals: no
  'tutte maiuscole', 'non tutte maiuscole',
  // vertical shift: none, by x% up, by x% down
  'sensa spostamento verticale', 'spostato su di %d%%', 'spostato gi� di %d%%',
  // characters width [: x%]
  'larghezza caratteri',
  // character spacing: none, expanded by x, condensed by x
  'spaziatura normale caratteri', 'spaziatura estesa di %s', 'spaziatura ridotta di %s',
  // charset
  'set di caratteri',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'carattere predefinito', 'paragrafo predefinito', 'ereditato',
  // [hyperlink] highlight, default hyperlink attributes
  'evidenziamento', 'predefinito',
  // paragraph aligmnment: left, right, center, justify
  'allineato a sinistra', 'allineato a destra', 'centrato', 'giustificato',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altezza riga: %d%%', 'spaziatura tra le righe: %s', 'altezza riga: almeno %s',
  'altezza riga: esattamente %s',
  // no wrap, wrap
  'disattiva ritorno a capo', 'ritorno a capo',
  // keep lines together: yes, no
  'mantieni assieme le righe', 'non mantenere assieme le righe',
  // keep with next: yes, no
  'mantieni con il paragrafo successivo', 'non mantenere con il paragrafo successivo',
  // read only: yes, no
  'sola lettura', 'modificabile',
  // body text, heading level x
  'livello struttura: corpo del testo', 'livello struttura: %d',
  // indents: first line, left, right
  'rientro prima riga', 'rientro sinistro', 'rientro destro',
  // space before, after
  'spazio prima', 'spazio dopo',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'schede', 'nessuna', 'allinea', 'a sinistra', 'a destra', 'centra',
  // tab leader (filling character)
  'carattere di riempimento',
  // no, yes
  'no', 's�',
  // [padding/spacing/side:] left, top, right, bottom
  'a sinistra', 'in alto', 'a destra', 'in basso',
  // border: none, single, double, triple, thick inside, thick outside
  'nessuno', 'singolo', 'doppio', 'triplo', 'spesso all''interno', 'spesso all''esterno',
  // background, border, padding, spacing [between text and border]
  'sfondo', 'bordo', 'spaziatura interna', 'spaziatura',
  // border: style, width, internal width, visible sides
  'stile', 'larghezza', 'larghezza interna', 'lati visibili',
  // style inspector -----------------------------------------------------------
  // title
  'Controllo stili',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stile', '(Nessuno)', 'Paragrafo', 'Carattere', 'Attributi',
  // border and background, hyperlink, standard [style]
  'Bordo e sfondo', 'Collegamento ipertestuale', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stili', 'Stile',
  // name, applicable to,
  '&Nome:', 'Applicabile &a:',
  // based on, based on (no &),
  '&Basato su:', 'Basato su:',
  //  next style, next style (no &)
  'Stile del &seguente paragrafo:', 'Stile del seguente paragrafo:',
  // quick access check-box, description and preview
  '&Accesso rapido', 'Descrizione e ante&prima:',
  // [applicable to:] paragraph and text, paragraph, text
  'paragrafo e testo', 'paragrafo', 'testo',
  // text and paragraph styles, default font
  'Stili testo e paragrafo', 'Carattere predefinito',
  // links: edit, reset, buttons: add, delete
  'Modifica', 'Resetta', '&Aggiungi', '&Elimina',
  // add standard style, add custom style, default style name
  'Aggiungi stile &standard...', 'Aggiungi stile &personalizzato', 'Stile %d',
  // choose style
  'Scegli &stile:',
  // import, export,
  '&Importa...', '&Esporta...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Stili RichView (*.rvst)|*.rvst', 'Errore durante il caricamento del file', 'Il file non contiene nessuno stile',
  // Title, group-box, import styles
  'Importa stili da file', 'Importa', 'I&mporta stili:',
  // existing styles radio-group: Title, override, auto-rename
  'Stili esistenti', 's&ostituisci', 'rinomina e &aggiungi',
  // Select, Unselect, Invert,
  '&Seleziona', '&Deseleziona', '&Inverti',
  // select/unselect: all styles, new styles, existing styles
  '&Tutti gli stili', '&Nuovi stili', '&Stili esistenti',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Cancella formato', 'Tutti gli stili',
  // dialog title, prompt
  'Stili', '&Scegliere gli stili da applicare:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sinonimi', '&Ignora Tutto', '&Aggiungi al Dizionario',
  // Progress messages ---------------------------------------------------------
  'Download di %s', 'Preparazione per la stampa...', 'Stampa della pagina %d',
  'Conversione da RTF...',  'Conversione in RTF...',
  'Lettura di %s...', 'Scrittura di %s...', 'testo',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Nessuna nota', 'Nota a fondo pagina', 'Nota a fine pagina', 'Nota', 'Casella di Testo'
  );


initialization
  RVA_RegisterLanguage(
    'Italian', // english language name, do not translate
    'Italiano', // native language name
    ANSI_CHARSET, @Messages);

end.

