
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for changing table and table cell        }
{       background.                                     }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

{$I RV_Defs.inc}
{$I RichViewActions.inc}

unit TableBackRVFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, BaseRVFrm, StdCtrls, RVColorCombo, RVScroll, RichView,
  RVOfficeRadioBtn, ImgList, RVStyle, ExtDlgs, RVEdit, RVTable, ExtCtrls,
  RVFuncs, RVGrHandler;

type
  TfrmRVTableBack = class(TfrmRVBase)
    il: TImageList;
    rg: TRVOfficeRadioGroup;
    gbBack: TGroupBox;
    btnOk: TButton;
    btnCancel: TButton;
    btnOpen: TButton;
    btnSave: TButton;
    btnClear: TButton;
    spd: TSavePictureDialog;
    panImg: TPanel;
    img: TImage;
    procedure btnSaveClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure rgClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    opd: TOpenPictureDialog;
    FFileName: String;
    procedure AdjustImage;
  protected
    _btnSave, _btnClear, _btnOk: TControl;  
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    Filter: String;
    procedure Init(Color: TColor; BackgroundStyle: TRVItemBackgroundStyle;
      Graphic: TGraphic);
    procedure GetResult(var BackgroundStyle: TRVItemBackgroundStyle;
      var Graphic: TGraphic; var FileName: String);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation
uses RVALocalize, RichViewActions;

{$R *.dfm}

procedure TfrmRVTableBack.btnSaveClick(Sender: TObject);
begin
  if img.Picture.Graphic=nil then begin
    Beep;
    exit;
  end;
  spd.Filter := GraphicFilter(TGraphicClass(img.Picture.Graphic.ClassType));
  if spd.Execute then
    img.Picture.SaveToFile(spd.FileName);
end;

procedure TfrmRVTableBack.btnOpenClick(Sender: TObject);
begin
  if opd=nil then begin
    opd := TOpenPictureDialog.Create(Self);
    opd.Options := [ofPathMustExist, ofFileMustExist, ofEnableSizing, ofHideReadOnly];
    if Filter<>'' then
      opd.Filter := Filter;
    if Pos('*.bmp', opd.Filter)>0 then
      opd.DefaultExt := 'bmp';
  end;
  if opd.Execute then
  try
    img.Picture.LoadFromFile(opd.FileName);
    img.Transparent := not (img.Picture.Graphic is TBitmap);
    FFileName := opd.FileName;
    if rg.ItemIndex=0 then
      rg.ItemIndex := 1;
  except
    RVA_MessageBox(RVA_GetS(rvam_err_ErrorLoadingImageFile, ControlPanel),
      RVA_GetS(rvam_err_Title, ControlPanel),
      MB_OK or MB_ICONSTOP);
  end;
  AdjustImage;  
end;

procedure TfrmRVTableBack.btnClearClick(Sender: TObject);
begin
  img.Picture.Graphic := nil;
  rg.ItemIndex := 0;
  FFileName := '';
  AdjustImage;
end;

procedure TfrmRVTableBack.AdjustImage;
begin
  if img.Picture.Graphic<>nil then
    img.Stretch := (img.Picture.Graphic.Width>img.Width) or (img.Picture.Graphic.Height>img.Height);
  _btnSave.Enabled := img.Picture.Graphic<>nil;
  _btnClear.Enabled := img.Picture.Graphic<>nil;
  _btnOk.Enabled := (rg.ItemIndex=0) or (img.Picture.Graphic<>nil);
end;

procedure TfrmRVTableBack.Init(Color: TColor;
  BackgroundStyle: TRVItemBackgroundStyle; Graphic: TGraphic);
begin
  if Color<>clNone then
    panImg.Color := Color;

  case BackgroundStyle of
    rvbsColor:
      rg.ItemIndex := 0;
    rvbsTiled:
      rg.ItemIndex := 1;
    rvbsStretched:
      rg.ItemIndex := 3;
    else
      rg.ItemIndex := 2;
  end;
  img.Picture.Graphic := Graphic;
  if Graphic<>nil then
    img.Transparent := Graphic.Transparent and not (img.Picture.Graphic is TBitmap);
  if TRVAControlPanel(ControlPanel).UserInterface=rvauiHTML then
    rg.Items[3].Enabled := False;

  AdjustImage;
end;

function TfrmRVTableBack.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gbBack.Left+gbBack.Width+rg.Left;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gbBack.Top-gbBack.Height);
  Result := True;
end;

procedure TfrmRVTableBack.GetResult(var BackgroundStyle: TRVItemBackgroundStyle;
  var Graphic: TGraphic; var FileName: String);
begin
  Graphic.Free;
  if img.Picture.Graphic=nil then
    Graphic := nil
  else begin
    Graphic := RVGraphicHandler.CreateGraphic(TGraphicClass(img.Picture.Graphic.ClassType));
    Graphic.Assign(img.Picture.Graphic);
    Graphic.Transparent := img.Picture.Graphic.Transparent;
  end;
  case rg.ItemIndex of
    0:
      BackgroundStyle := rvbsColor;
    1:
      BackgroundStyle := rvbsTiled;
    3:
      BackgroundStyle := rvbsStretched;
    else
      BackgroundStyle := rvbsCentered;
  end;
  FileName := FFileName;
end;

procedure TfrmRVTableBack.rgClick(Sender: TObject);
begin
  AdjustImage;
end;

procedure TfrmRVTableBack.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_back_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  btnOpen.Caption := RVA_GetSAsIs(rvam_btn_Open, ControlPanel);
  btnSave.Caption := RVA_GetSAsIs(rvam_btn_Save, ControlPanel);
  btnClear.Caption := RVA_GetSAsIs(rvam_btn_Clear, ControlPanel);
  rg.Caption := RVA_GetSH(rvam_back_Position, ControlPanel);
  gbBack.Caption := RVA_GetSHAsIs(rvam_back_Background, ControlPanel);
  rg.Items[0].Caption := RVA_GetS(rvam_back_None, ControlPanel);
  rg.Items[1].Caption := RVA_GetS(rvam_back_Tiles, ControlPanel);
  rg.Items[2].Caption := RVA_GetS(rvam_back_Center, ControlPanel);
  rg.Items[3].Caption := RVA_GetS(rvam_back_FullWindow, ControlPanel);
end;

procedure TfrmRVTableBack.FormCreate(Sender: TObject);
begin
  _btnSave := btnSave;
  _btnClear := btnClear;
  _btnOk := btnOk;  
  inherited;
  {$IFDEF RICHVIEWDEF6}
  img.Proportional := True;
  {$ENDIF}
  PrepareImageList(il);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVTableBack.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _btnSave then
    _btnSave := NewControl
  else if OldControl = _btnClear then
    _btnClear := NewControl
  else if OldControl = _btnOk then
    _btnOk := NewControl;
end;
{$ENDIF}

end.
