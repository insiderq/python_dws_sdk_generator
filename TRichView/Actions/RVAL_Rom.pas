
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Romanian translation                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{       Original translator: Viorel Doni                }
{                                                       }
{*******************************************************}
{ Updated by Morlova Daniel,                            }
{          by Alconost                                  }
{ Updated: 2012-10-11                                   }
{ Updated: 2014-05-01                                   }
{*******************************************************}

unit RVAL_Rom;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'inci', 'cm', 'mm', 'pica', 'pixeli', 'puncte',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'pixeli'{?}, 'pct',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  'Fi�ier', 'Editare', 'Format', 'Font', 'Paragraf', 'Inserare', 'Tabel', 'Fereastr�', 'Asisten��',
  // exit
  'Ie�ire',
  // top-level menus: View, Tools,
  'Vedere', 'Instrument',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Dimensiune', 'Stil', 'Selectare', 'Aliniere �n celule', 'Chenar celul�',
  // menus: Table cell rotation
  'Rotire &celul�', 
  // menus: Text flow, Footnotes/endnotes
  '&Curgere text', '&Note de subsol/note de final',
  // ribbon tabs: tab1, tab2, view, table
  '&Acas�', '&Avansat', '&Vizualizare', '&Tabel',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Clipboard', 'Font', 'Paragraf', 'List�', 'Editare',
  // ribbon groups: insert, background, page setup,
  'Inserare', 'Fundal', 'Setare pagin�',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Leg�turi', 'Note de subsol', 'Vizualiz�ri Document', 'Arat�/Ascunde', 'Panoramare', 'Op�iuni',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserare', '�tergere', 'Opera�ii', 'Chenare',
  // ribbon groups: styles 
  'Stiluri',
  // ribbon screen tip footer,
  'Ap�sa�i pe F1 pentru ajutor',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documente Recente', 'Salva�i o copie a documentului', 'Previzualiza�i �i tip�ri�i documentul',
  // ribbon label: units combo
  'Unit��i:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'Nou', 'Nou|Creare document nou',
  // TrvActionOpen
  'Deschide...', 'Deschide|Deschidere fi�ier',
  // TrvActionSave
  'Salvare', 'Salvare|Salvare document',
  // TrvActionSaveAs
  'Salvare ca...', 'Salvare ca...|Salvare document cu alt� denumire, format sau loca�ie',
  // TrvActionExport
  'Export...', 'Export|Exportare document �n fi�ier cu alt� denumire, format sau loca�ie',
  // TrvActionPrintPreview
  'Previzualizare', 'Previzualizare|Previzualizare document pentru tip�rire',
  // TrvActionPrint
  'Tip�rire...', 'Tip�rire|Modificare set�ri imprimant� �i tip�rire document',
  // TrvActionQuickPrint
  'Tip�rire direct�', '&Tip�rire rapid�', 'Tip�rire direct�|Tip�re�te documentul',
   // TrvActionPageSetup
  'Set�rile paginii...', 'Set�rile paginii|Setare parametri pagin�',
  // TrvActionCut
  'Decupare', 'Decupare|�tergere fragment selectat �i plasare �n clipboard',
  // TrvActionCopy
  '&Copiere', 'Copiere|Copiere fragment selectat �n clipboard',
  // TrvActionPaste
  'Lipire', 'Lipire|Lipire fragment din clipboard �n pozi�ia curent� a documentului',
  // TrvActionPasteAsText
  'Lipire ca &Text', 'Lipire ca text|Insereaz� textul din Clipboard',
  // TrvActionPasteSpecial
  'Lipire special�...', 'Lipire special�|Lipire con�inut din clipboard �n document',
  // TrvActionSelectAll
  'Selectare tot', 'Selectare tot|Selectare �ntreg document',
  // TrvActionUndo
  'Anulare', 'Anulare|Revenire asupra ultimei ac�iuni',
  // TrvActionRedo
  'Repetare', 'Repetare|Repetare ultima ac�iune',
  // TrvActionFind
  'C�utare...', 'C�utare|C�utare text indicat �n document',
  // TrvActionFindNext
  'Continuare c�utare', 'Continuare c�utare|Continuare ultima c�utare a textului',
  // TrvActionReplace
  '�nlocuire...', '�nlocuire|C�utare �i �nlocuire text indicat �n document',
  // TrvActionInsertFile
  'Fi�ier...', 'Inserare fi�ier|Inserare fi�ier �n pozi�ia curent� a documentului',
  // TrvActionInsertPicture
  'Imagine...', 'Inserare imagine|Inserare imagine �n pozi�ia curent� a documentului',
  // TRVActionInsertHLine
  'Linie orizontal�', 'Inserare linie orizontal�|Inserare linie orizontal� �n pozi�ia curent� a documentului',
  // TRVActionInsertHyperlink
  'Hiperleg�tur�...', 'Inserare hiperleg�tur�|Inserare hiperleg�tur� �n pozi�ia curent� a documentului',
  // TRVActionRemoveHyperlinks
  '&�ndep�rtare Hiperleg�turi', '�ndep�rtare Hiperleg�turi|�ndep�rteaz� toate hiperleg�turile din textul selectat',
  // TrvActionInsertSymbol
  'Simbol...', 'Inserare simbol|Inserare simbol sau caracter special',
  // TrvActionInsertNumber
  '&Numar...', 'Inserare numar|Insereaza un numar',
  // TrvActionInsertCaption
  'Inserare &Legenda...', 'Insereaza legenda|Insereaza o legenda pentru obiectul selectat',
  // TrvActionInsertTextBox
  '&Caseta Text', 'Insereaza caseta pentru text|Insereaza o caseta pentru text',
  // TrvActionInsertSidenote
  '&Nota', 'Inserare nota laterala|Insereaza o nota laterala �n caseta pentru text',
  // TrvActionInsertPageNumber
  '&Numar pagina', 'Insereaza numarul paginii|Insereaza un numar pentru pagina',
  // TrvActionParaList
  'List�...', 'List�|Ad�ugare sau modificare a formatului marcajului sau a numerot�rii �n paragrafele selectate',
  // TrvActionParaBullets
  'Marcaj de list�', 'Marcaj de list�|Ad�ugare sau modificare a marcajului de list� �n paragrafele selectate',
  // TrvActionParaNumbering
  'Numerotare', 'Numerotare|Ad�ugare sau modificare a numerot�rii �n paragrafele selectate',
  // TrvActionColor
  'Culoare de fond...', 'Culoare de fond|Modificare culoare de fond a documentului',
  // TrvActionFillColor
  'Culoare de umplere...', 'Culoare de umplere|Modificare culoare de umplere a textului, paragrafului, tabelului sau a celulei',
  // TrvActionInsertPageBreak
  'Inserare �ntrerupere pagin�', 'Inserare �ntrerupere pagin�|Inserare �ntrerupere pagin� �n pozi�ia curent� a documentului',
  // TrvActionRemovePageBreak
  '�tergere �ntrerupere pagin�', '�tergere �ntrerupere pagin�|�tergere �ntrerupere pagin�',
  // TrvActionClearLeft
  '�tergere curgere text �n &partea st�ng�', '�tergere curgere text �n partea st�ng�|Plaseaz� acest paragraf sub orice poz� aliniat� la st�nga',
  // TrvActionClearRight
  '�tergere curgere text �n &partea dreapt�', '�tergere curgere text �n partea dreapt�|Plaseaz� acest paragraf sub orice poz� aliniat� la dreapta',
  // TrvActionClearBoth
  '�tergere curgere text �n &ambele p�r�i', '�tergere curgere text �n ambele p�r�i|Plaseaz� acest paragraf sub orice poz� aliniat� la st�nga sau la dreapta',
  // TrvActionClearNone
  '&Curgere normal� a textului', 'Curgere normal� a textului|Permite curgerea textului �n jurul pozelor aliniate la st�nga sau la dreapta',
  // TrvActionVAlign
  '&Pozi�ie obiect...', 'Pozi�ie obiect|Modific� pozi�ia obiectului ales',
  // TrvActionItemProperties
  'Parametrii obiectului...', 'Parametrii obiectului|Setare parametri la imagine, linie sau tabel selectat',
  // TrvActionBackground
  'Fundal...', 'Fundal|Alegere imagine fundal',
  // TrvActionParagraph
  'Paragraf...', 'Paragraf|Modificare atribute la paragraful selectat',
  // TrvActionIndentInc
  'M�rire alineat', 'M�rire alineat|M�rire alineat din st�nga la paragrafelor selectate',
  // TrvActionIndentDec
  'Mic�orare alineat', 'Mic�orare alineat|Mic�orare alineat din st�nga la paragrafelor selectate',
  // TrvActionWordWrap
  'Desp�r�ire automat� pe linii', 'Desp�r�ire automat� pe linii|Activare/Dezactivare desp�r�ire automat� pe linii �n cadrul parafrafelor',
  // TrvActionAlignLeft
  'Aliniere la st�nga', 'Aliniere la st�nga|Aliniere text selectat dup� marginea din st�nga',
  // TrvActionAlignRight
  'Aliniere la dreapta', 'Aliniere la dreapta|Aliniere text selectat dup� marginea din dreapta',
  // TrvActionAlignCenter
  'Aliniere la centru', 'Aliniere la centru|Aliniere text selectat la centru',
  // TrvActionAlignJustify
  'Aliniere dup� l��ime', 'Aliniere dup� l��ime|Aliniere text selectat dup� marginile din dreapta �i st�nga',
  // TrvActionParaColor
  'Culoare fond �n paragraf...', 'Culoare fond �n paragraf|Alegere culoare la fond pentru paragraf',
  // TrvActionLineSpacing100
  'Interval unic', 'Interval unic|Setare interval la 1 �ntre r�ndurile paragrafului',
  // TrvActionLineSpacing150
  'Interval 1,5', 'Interval 1,5|Setare interval la 1,5 �ntre r�ndurile paragrafului',
  // TrvActionLineSpacing200
  'Interval dublu', 'Interval dublu|Setare interval dublu �ntre r�ndurile paragrafului',
  // TrvActionParaBorder
  'Chenar �i fond paragrafului...', 'Chenar �i fond paragrafului|Set�ri la chenar �i fond pentru paragrafele selectate',
  // TrvActionInsertTable
  'Inserare tabel...', '&Tabel', 'Inserare tabel|Inserare tabel �n pozi�ia curent� a documentului',
  // TrvActionTableInsertRowsAbove
  'Inserare linie sus', 'Inserare linie sus|Inserare linie nou� mai sus de celulele selectate',
  // TrvActionTableInsertRowsBelow
  'Inserare linie jos', 'Inserare linie jos|Inserare linie nou� mai jos de celulele selectate',
  // TrvActionTableInsertColLeft
  'Inserare coloan� la st�nga', 'Inserare coloan� la st�nga|Inserare coloan� nou� la st�nga de celulele selectate',
  // TrvActionTableInsertColRight
  'Inserare coloan� la dreapta', 'Inserare coloan� la dreapta|Inserare coloan� nou� la dreapta de celulele selectate',
  // TrvActionTableDeleteRows
  '�tergere linii', '�tergere linii|�tergere linii cu celule selectate',
  // TrvActionTableDeleteCols
  '�tergere coloane', '�tergere coloane|�tergere coloane cu celule selectate',
  // TrvActionTableDeleteTable
  '�tergere tabel', '�tergere tabel|�tergere tabel',
  // TrvActionTableMergeCells
  '�mbinare celule', '�mbinare celule|�mbinare celule selectate �n una singur�',
  // TrvActionTableSplitCells
  'Divizare celul�...', 'Divizare celul�|Divizare celul� selectat� �n num�rul indicat de r�nduri �i coloane',
  // TrvActionTableSelectTable
  'Selectare tabel', 'Selectare tabel|Selectare �ntregul tabel',
  // TrvActionTableSelectRows
  'Selectare linii', 'Selectare linii|Selectare linii care con�in celulele selectate',
  // TrvActionTableSelectCols
  'Selectare coloane', 'Selectare coloane|Selectare coloane care con�in celulele selectate',
  // TrvActionTableSelectCell
  'Selectare celul�', 'Selectare celul�|Selectare celul�',
  // TrvActionTableCellVAlignTop
  'Aliniere la marginea de sus', 'Aliniere la marginea de sus|Aliniere con�inut celul� la marginea de sus',
  // TrvActionTableCellVAlignMiddle
  'Centrare pe vertical�', 'Centrare pe vertical�|Aliniere con�inut celul� la centru',
  // TrvActionTableCellVAlignBottom
  'Aliniere la marginea de jos', 'Aliniere la marginea de jos|Aliniere con�inut celul� la marginea de jos',
  // TrvActionTableCellVAlignDefault
  'Aliniere prestabilit�', 'Aliniere prestabilit�|Aliniere prestabilit� pe vertical�',
  // TrvActionTableCellRotationNone
  '&F�r� rotire a celulei', 'F�r� rotire a celulei|Rote�te con�inutul celulei cu 0�',
  // TrvActionTableCellRotation90
  'Rotire celul� cu &90�', 'Rotire celul� cu 90�|Rote�te con�inutul celulei cu 90�',
  // TrvActionTableCellRotation180
  'Rotire celul� cu &180�', 'Rotire celul� cu 180�|Rote�te con�inutul celulei cu 180�',
  // TrvActionTableCellRotation270
  'Rotire celul� cu &270�', 'Rotire celul� cu 270�|Rote�te con�inutul celulei cu 270�',  
  // TrvActionTableProperties
  'Parametri tabel...', 'Parametri tabelul|Setare parametri tabel',
  // TrvActionTableGrid
  'Gril�', 'Gril�|Arat�/Ascunde linii chenar �n tabel',
  // TRVActionTableSplit
  'Divizare tabel', 'Divizare tabel|Divizeaz� tabelul �n dou� tabele �ncep�nd de la r�ndul selectat',
  // TRVActionTableToText
  'Convertire �n text...', 'Convertire �n text|Converte�te tabelul �n text',
  // TRVActionTableSort
  '&Sortare...', 'Sortare|Sorteaz� r�ndurile tabelului',
  // TrvActionTableCellLeftBorder
  'Margine din st�nga', 'Margine din st�nga|Vizualizare/ascundere margine din st�nga pentru celulele selectate',
  // TrvActionTableCellRightBorder
  'Margine din dreapta', 'Margine din dreapta|Vizualizare/ascundere margine din dreapta pentru celulele selectate',
  // TrvActionTableCellTopBorder
  'Margine de sus', 'Margine de sus|Vizualizare/ascundere margine de sus pentru celulele selectate',
  // TrvActionTableCellBottomBorder
  'Margine de jos', 'Margine de jos|Vizualizare sau ascundere margine de jos pentru celulele selectate',
  // TrvActionTableCellAllBorders
  'Chenar', 'Chenarul|Vizualizare/ascundere chenar pentru celulele selectate',
  // TrvActionTableCellNoBorders
  'F�r� chenar', 'F�r� chenar|Ascundere chenar pentru celulele selectate',
  // TrvActionFonts & TrvActionFontEx
  'Font...', 'Font|Modificare parametri la caractere �i distan�a dintre simboluri �n textul selectat',
  // TrvActionFontBold
  'Aldin', 'Aldin|Setare text selectat cu caractere aldine',
  // TrvActionFontItalic
  'Cursiv', 'Cursiv|Setare text selectat cu caractere cursive',
  // TrvActionFontUnderline
  'Subliniat', 'Subliniat|Subliniere text selectat',
  // TrvActionFontStrikeout
  'T�iat cu linie', 'T�iat cu linie|T�iere cu linie a textului selectat',
  // TrvActionFontGrow
  'M�rire dimensiune', 'M�rire dimensiune|M�rire dimensiune font �n textul selectat cu 10%',
  // TrvActionFontShrink
  'Mic�orare dimensiune', 'Mic�orare dimensiune|Mic�orare dimensiune font �n textul selectat cu 10%',
  // TrvActionFontGrowOnePoint
  'M�rire dimensiune cu 1 pct', 'M�rire dimensiune cu 1 pct|M�rire dimensiune font �n textul selectat cu 1 punct',
  // TrvActionFontShrinkOnePoint
  'Mic�orare dimensiune cu 1 pct', 'Mic�orare dimensiune cu 1 pct|Mic�orare dimensiune font �n textul selectat cu 1 punct',
  // TrvActionFontAllCaps
  'Toate majuscule', 'Toate majuscule|Modificare litere �n majuscule �n textul selectat',
  // TrvActionFontOverline
  'Supraliniat', 'Supraliniat|Ad�ugare linie orizontal� asupra textului selectat',
  // TrvActionFontColor
  'Culoare text...', 'Culoare text|Modificare culoare la textul selectat',
  // TrvActionFontBackColor
  'Culoare umplere text...', 'Culoare umplere text|Modificare culoare fundal la textul selectat',
  // TrvActionAddictSpell3
  'Ortografie', 'Ortografie|Verificare ortografic�',
  // TrvActionAddictThesaurus3
  'Tezaurus', 'Tezaurus|C�utare sinonime',
  // TrvActionParaLTR
  'De la st�nga la dreapta', 'De la st�nga la dreapta|Setare direc�ie text de la st�nga la dreapta pentru paragrafele selectate',
  // TrvActionParaRTL
  'De la dreapta la st�nga', 'De la dreapta la st�nga|Setare direc�ie text de la dreapta la st�nga pentru paragrafele selectate',
  // TrvActionLTR
  'Text de la st�nga la dreapta', 'Text de la st�nga la dreapta|Setare direc�ie text selectat de la st�nga la dreapta',
  // TrvActionRTL
  'Text de la dreapta la st�nga', 'Text de la dreapta la st�nga|Setare direc�ie text selectat de la dreapta la st�nga',
  // TrvActionCharCase
  'Tip de caracter', 'Tip de caracter|Modificare tip de caracter la litere �n textul selectat',
  // TrvActionShowSpecialCharacters
  'Simboluri non-printing', 'Simboluri non-printing|Vizualizare/ascundere simboluri non-printing',
  // TrvActionSubscript
  'Indice', 'Indice|Convertire text selectat ca indice',
  // TrvActionSuperscript
  'Exponent', 'Exponent|Convertire text selectat ca exponent',
  // TrvActionInsertFootnote
  '&Not� de subsol', 'Not� de subsol|Insereaz� o not� de subsol',
  // TrvActionInsertEndnote
  '&Not� de final', 'Not� de final|Insereaz� o not� de final',
  // TrvActionEditNote
  'Editare not�', 'Editare not�|�ncepe editarea notei de subsol sau de final',
  '&Ascundere', 'Ascundere|Ascunde sau arat� fragmentul selectat',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stiluri...', 'Stiluri|Deschide caseta de dialog de gestionare a stilurilor',
  // TrvActionAddStyleTemplate
  '&Ad�ugare stil...', 'Ad�ugare stil|Creeaz� un nou stil de text sau paragraf',
  // TrvActionClearFormat,
  '&�tergere Format', '�tergere Format|�terge formatele pentru texte �i paragrafe din fragmentul selectat',
  // TrvActionClearTextFormat,
  '�tergere &Format Text', '�tergere Format Text|�terge formatele din textul selectat',
  // TrvActionStyleInspector
  'Inspector Stil', 'Inspector stil|Arat� sau ascunde inspectorul de stil',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'OK', 'Cancel', '�nchide', 'Inserare', 'Deschide...', 'Salvare...', '�terge', 'Asisten��', '�ndepartare',
  // Others  -------------------------------------------------------------------
  // percents,
  '%',
  // left, top, right, bottom sides
  'Partea din st�nga', 'Partea de sus', 'Partea din dreapta', 'Partea de jos',
  // save changes? confirm title
  'Dori�i salvarea modific�rilor �n %s?', 'Confirmare',
  // warning: losing formatting
  '%s poate con�ine elemente de formatare care vor fi pierdute �n procesul reformat�rii fi�ierului.'#13+
  'Dori�i salvarea documentului �n acest format?',
  // RVF format name
  'Format RichView',
  // Error messages ------------------------------------------------------------
  'Eroare',
  'Eroare la citirea textului.'#13#13'Cauze posibile:'#13'- fi�ierul are format neacceptat de aplica�ie;'#13+
  '- alterare;'#13'- fi�ierul este deschis �n alt� aplica�ie care i-a blocat accesul.',
  'Eroare la citirea fi�ierului cu imagine.'#13#13'Cauze posibile:'#13'- fi�ierul con�ine imagine �n format neacceptat de aplica�ie;'#13+
  '- fi�ierul nu con�ine imagine;'#13+
  '- alterare;'#13'- fi�ierul este deschis �n alt� aplica�ie care i-a blocat accesul.',
  'Eroare la salvarea fi�ierului.'#13#13'Cauze posibile:'#13'- spa�iu insuficient;'#13+
  '- interzis accesul la salvare pe disc;'#13'- lipse�te discul �n unitatea de disc;'#13+
  '- fi�ierul este deschis �n alt� aplica�ie care i-a blocat accesul;'#13+
  '- suport de informa�ie deteriorat.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Fi�iere �n formatul RichView (*.rvf)|*.rvf',  'Fi�iere RTF (*.rtf)|*.rtf' , 'Fi�iere XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Fi�iere textuale (*.txt)|*.txt', 'Fi�iere textuale - Unicode (*.txt)|*.txt', 'Fi�iere textuale - auto (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Fi�iere HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - simplificat (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documente Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Sf�r�it c�utare', 'Textul ''%s'' nu a fost g�sit.',
  // 1 string replaced; Several strings replaced
  'S-a f�cut o �nlocuire', 'S-au f�cut %d �nlocuiri',
  // continue search from the beginning/end?
  'S-a ajuns la sf�r�itul documentului. S� re�ncep c�utarea de la �nceput?',
  'S-a ajuns la �nceputul documentului. S� re�ncep c�utarea de la sf�r�it?',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Automat',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Negru', 'Maro', 'Olive Green', 'Verde �nchis', 'Dark Teal', 'Albastru �nchis', 'Indigo', 'Gray-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Dark Red', 'Orange', 'Dark Yellow', 'Green', 'Teal', 'Blue', 'Blue-Gray', 'Gray-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Red', 'Light Orange', 'Lime', 'Sea Green', 'Aqua', 'Light Blue', 'Violet', 'Grey-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Pink', 'Gold', 'Yellow', 'Bright Green', 'Turquoise', 'Sky Blue', 'Plum', 'Gray-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Tan', 'Light Yellow', 'Light Green', 'Light Turquoise', 'Pale Blue', 'Lavender', 'White',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Transparent', '&Auto', '&Alte culori...', '&Prestabilit',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  'Fundal', '&Culoare:', 'Plasare', 'Imagine', 'Exemplu text.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Niciuna', '�ntins', 'Al�turare fix�', 'Al�turat', '�n centru',
  // Padding button
  'Completare...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  'Culoare de umplere', '&Aplicat la:', '&Alt� culoare...', 'Completare...',
  'Alege�i culoarea',
  // [apply to:] text, paragraph, table, cell
  'text', 'paragraf' , 'tabel', 'celul�',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Font', 'Font', 'Interval',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Font:', '&Dimensiune', '&Stil', 'Aldin', '&Cursiv',
  // Script, Color, Back color labels, Default charset
  'Set de simboluri:', '&Culoare:', 'Fundal:', '(prestabilit)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efecte', 'Subliniat', 'Supraliniat', 'T�iat cu linie', '&Toate majuscule',
  // Sample, Sample text
  'Exemplu', 'Exemplu de text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Interval', '&Interval:', '&Extins', '&Comprimat',
  // Offset group-bix, Offset label, Down, Up,
  'Deplasament vertical', 'Deplasament:', 'Sus', 'Jos',
  // Scaling group-box, Scaling
  'Scalare orizontal�', '&Scara:',
  // Sub/super script group box, Normal, Sub, Super
  'Indice �i exponent', '&Normal', 'Indice', 'Exponent',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  'C�mpuri', 'Sus:', 'St�nga:', 'Jos:', 'Dreapta:',
  // Equal values check-box
  '&Valori egale',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Inserare hiperleg�turii', 'Hiperleg�tur�', '&Text:', '&URL:', '<<selectare>>',
  // cannot open URL
  'Eroare la navigare c�tre "%s"',
  // hyperlink properties button, hyperlink style
  '&Atribute...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Atributele hiperleg�turii', 'Culori obi�nuite', 'Culori active',
  // Text [color], Background [color]
  '&Text:', '&Fundal',
  // Underline [color]
  'S&ubliniere:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'F&undal',
  // Underline [color], Attributes group-box,
  'S&ubliniere:', 'Atribute',
  // Underline type: always, never, active (below the mouse)
  '&Subliniere:', '�ntotdeauna', 'niciodat�', 'activ',  
  // Same as normal check-box
  'Ca obi�nuite',
  // underline active links check-box
  '&Subliniere leg�turi active',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Inserare simbol', '&Font:', '&Set de caractere:', 'Unicode',
  // Unicode block
  '&Bloc:',
  // Character Code, Character Unicode Code, No Character
  'Cod simbol: %d', 'Cod simbol: Unicode %d', '(no character)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows        
  'Inserare tabel', 'Dimensiune tabel', 'Coloane:', 'R�nduri:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'L��imea tabelului', '&Auto', 'Dup� l��imea ferestrei', '&Indicat�:',
  // Remember check-box
  'Prestabilit� pentru tabele noi',
  // VAlign Form ---------------------------------------------------------------
  'Pozi�ie',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriet��i', 'Imagine', 'Pozitie si dimensiune', 'Linia', 'Tabel', 'R�nd', 'Celul�',
  // Image Appearance, Image Misc, Number tabs
  'Aspect', 'Diverse', 'Numar',
  // Box position, Box size, Box appearance tabs
  'Pozitie', 'Dimensiune', 'Aspect',
  // Preview label, Transparency group-box, checkbox, label
  'Previzualizare:', 'Transparen��', '&Transparent', 'Culoare transparent�:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Selectare...', '&Salvare...', 'Auto',
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Aliniere vertical�', 'Aliniere:',
  'jos dup� linia de baz� a textului', 'centru dup� linia de baz� a textului',
  'sus dup� linia de sus', 'jos dup� linia de jos', 'la mijloc dup� linia de mijloc',
  // align to left side, align to right side
  'partea st�ng�', 'partea dreapt�',  
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Plasare cu:', 'Scara', 'Dup� l��ime:', 'Dup� �n�l�ime:', 'M�rimea ini�ial�: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Ajustare &proportionala', '&Resetare',
  // Inside the border, border, outside the border groupboxes
  '�n interiorul chenarului', 'Chenar', '�n exteriorul chenarului',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Culoare de umplere:', '&Completare:',
  // Border width, Border color labels
  'Latime &chenar:', 'Chenar &culoare:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Interval pe orizontala:', '&interval pe Verticala:',
  // Miscellaneous groupbox, Tooltip label
  'Diverse', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Text de �nlocuire:',
  // Horz line group-box, color, width, style
  'Linie orizontal�', '&Culoare:', '&Grosime:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing
  'Tabel', '&L��ime:', '&Culoare umplere:', '&Interval dintre celule...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Completare celula pe orizontala:', '&Completare celula pe verticala:', 'Chenar si fundal',
  // Visible table border sides button, auto width (in combobox), auto width label
  '&Laturi chenar vizibile...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Pastrare continut �mpreuna',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotire', '&Niciuna', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Chenar:', '&Laturi chenar vizibile...',
  // Border, CellBorders buttons
  '&Chenar tabel...', 'Chenar celule...',
  // Table border dialog title
  'Chenar tabel', 'Chenar celule predefinit',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tipar', 'Interzicere trecere r�nduri pe pagin� nou�', 'Nr. de r�nduri �n titlu:',
  'R�ndurile titlului repetate pe fiecare pagin� a tabelului',
  // top, center, bottom, default
  'Sus', 'La centru', 'Jos', 'Predefinit',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Parametri celule', 'L��ime aproximativ�:', '&�n�l�ime mai mult de:', 'Culoare umplere:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Chenar', 'Laturile vizibile:', 'Culoare �ntunecat�:', 'Culoare aprins�', 'Culoare:',
  // Background image
  'Imagine...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Pozitie orizontala', 'Pozitie Verticala', 'Pozitie �n text',
  // Position types: align, absolute, relative
  'Aliniere', 'Pozitie absoluta', 'Pozitie relativa',
  // [Align] left side, center, right side
  'latura st�nga a casetei spre latura st�nga a',
  'centrul casetei spre centrul',
  'latura dreapta a casetei spre latura dreapta a',
  // [Align] top side, center, bottom side
  'latura de sus a casetei spre latura de sus a',
  'centrul casetei spre centrul',
  'latura de jos a casetei spre latura de jos a',
  // [Align] relative to
  'relativa fata de',
  // [Position] to the right of the left side of
  'spre dreapta platurii din st�nga a',
  // [Position] below the top side of
  'sub latura de sus a',
  // Anchors: page, main text area (x2)
  '&Pagina', '&Zona text principal', 'P&agina', 'Zona te&xt principal',
  // Anchors: left margin, right margin
  '&Margine st�nga', '&Margine dreapta',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Margine de sus', '&Margine de jos', '&Margine interioara', '&Margine exterioara',
  // Anchors: character, line, paragraph
  '&Caracter', 'L&inie', 'Para&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Margini �n oglinda', 'Aranjarea �n celula tabelului',
  // Above text, below text
  '&Text deasupra', '&Text dedesubt',
  // Width, Height groupboxes, Width, Height labels
  'Latime', '�naltime', '&Latime:', '&�naltime:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Aliniere verticala', '&Sus', '&Centru', '&Jos',
  // Border and background button and title
  'C&henar si fundal...', 'Chenar si fundal caseta',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Inserare special�', '&Ca:', 'Format RTF', 'Format HTML',
  'Text neformatat', 'Text �n codul Unicode', 'Imagine �n puncte (BMP)', 'Metafi�ier (WMF)',
  'Fi�iere grafice', 'URL',
  // Options group-box, styles
  'Op�iuni', '&Stiluri:',
  // style options: apply target styles, use source styles, ignore styles
  'aplicare stiluri document �int�', 'p�strare stiluri �i �nf��i�are',
  'p�strare �nf��i�are, ignorare stiluri',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'List�', 'Marcat', 'Numerotat', 'Marcaj de list�', 'List� numerotat�',
  // Customize, Reset, None
  '&Modific�...', '&Anulare', 'Nu',
  // Numbering: continue, reset to, create new
  'continuare numerotare', '�nceput numerotare de la ', 'creare list� nou� de la',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Litere mari', 'Numere romane, caractere mari', 'Numere �ntregi', 'Litere mici', 'Numere romane, caractere mici',
  // Bullet type
  'Cerc', 'Disc', 'P�trat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nivel:', '&�ncepere de la:', '&Continuare', 'Numerotare',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  'Modificare list�', 'Nivele', '&Num�r:', 'Propriet��i list�', '&Tip list�:',
  // List types: bullet, image
  'marcaj', 'imagine', 'Inserare num�r|',
  // Number format, Number, Start level from, Font button
  '&Format num�r:', 'Num�r', '&Numerotare nivel de la:', '&Font...',
  // Image button, bullet character, Bullet button,
  '&Imagine...', '&Simbol marcaj', 'Simbol...',
  // Position of list text, bullet, number, image
  'Pozi�ie marcaj', 'Pozi�ie marcaj', 'Pozi�ie num�r', 'Pozi�ie imagine', 'Pozi�ie text',
  // at, left indent, first line indent, from left indent
  '', '&Alineat din st�nga:', '&Primul r�nd:', 'de la alineatul din st�nga',
  // [only] one level preview, preview
  '&Previzualizare numai un nivel', 'Model',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'la dreapta de', 'la st�nga de', 'la centru',
  // level #, this level (for combo-box)
  'Nivel %d', 'Acest nivel',
  // Marker character dialog title
  'Alegere simbol marcaj',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Chenar �i umplere paragraf', 'Chenar', 'Umplere',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Tip chenar', '&Culoare:', 'Grosime:', '&Spa�iu:', '&Deplasamente...',
  // Sample, Border type group-boxes;
  'Model', 'Tip chenar',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&F�r�', 'Unic', '&Dublu', '&Triplu', '�ngro�at intern', '�ngro�at extern',
  // Fill color group-box; More colors, padding buttons
  'Culoare umplere', '&Alt� culoare...', '&C�mpuri...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box �n Offsets dialog
  'Deplasamente', 'Deplasamente �ntre text �i chenar',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Alineat', 'Aliniere', '&St�nga', '&Dreapta', 'La centru', 'Dup� l��ime',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Intervale', 'P�n� la:', 'Dup�:', '&�ntre r�nduri:', '&Valoare:',
  // Indents group-box; indents: left, right, first line
  'Deplasamente', 'St�nga:', 'Dreapta:', 'Primul r�nd:',
  // indented, hanging
  'Indentat', 'Ag��at', 'Model',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Unic', '1,5', 'Dublu', 'Cel pu�in', 'Exact', 'Multiplu',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Alineate �i intervale', 'Tabulare', 'Plasare pe pagin�',
  // tab stop position label; buttons: set, delete, delete all
  'Pozi�ie tabulare:', '&Setare', '�terge', '�terge toate',
  // tab align group; left, right, center aligns,
  'Aliniere', 'St�nga', 'Dreapta', 'La centru',
  // leader radio-group; no leader
  'Umplere', '(F�r�)',
  // tab stops to be deleted, delete none, delete all labels
  'Vor fi �terse:', '', 'Toate.',
  // Pagination group-box, keep with next, keep lines together
  'Separare pe pagini', 'Lipire cu urm�toarea', 'Lipire linii paragraf',
  // Outline level, Body text, Level #
  '&Nivel schi��:', 'Text principal', 'Nivel %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Previzualizare', 'Dup� l��imea paginii', 'Pagina �ntreag�', 'Pagini:',
  // Invalid Scale, [page #] of #
  'Indica�i un num�r de la 10 p�n� la 500', 'din %d',
  // Hints on buttons: first, prior, next, last pages
  'Prima', 'Precedenta', 'Urm�toarea', 'Ultima',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Intervale', 'Intervale', '�ntre &celule', '�ntre chenarul tabelului �i celule',
  // vertical, horizontal (x2)
  '&Vertical�:', '&Orizontal�:', 'V&ertical�:', 'O&rizontal�:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Chenare', 'Parametri', '&Culoare:', '&Culoare �nchis�:', 'Culoare deschis�:',
  // Width; Border type group-box;
  '&Grosime:', 'Tip chenar',
  // Border types: none, sunken, raised, flat
  '&F�r�', '&�n�untru', '�n afar�', '&Plat�',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Divizare celule', 'Divizare �n',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&nr. indicat de r�nduri �i coloane', '&Celule ini�iale',
  // number of columns, rows, merge before
  'Nr. de coloane:', 'Nr. de linii:', '&Grupare p�n� la divizare',
  // to original cols, rows check-boxes
  'Divizare �n coloane ini�iale', 'Divizare �n linii ini�iale',
  // Add Rows form -------------------------------------------------------------
  'Ad�ugare linii', '&Nr. de linii:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Parametri pagin�', 'Pagin�', 'Antet&Subsol',
  // margins group-box, left, right, top, bottom
  'C�mpuri (mm)', 'C�mpuri (dm)', '&St�nga:', '&Sus:', '&Dreapta:', '&Jos:',
  // mirror margins check-box
  '&C�mpuri propor�ionale',
  // orientation group-box, portrait, landscape
  'Orientare', '&Vertical�', '&Orizontal�',
  // paper group-box, paper size, default paper source
  'H�rtie', '&Dimensiune:', 'Surs�:',
  // header group-box, print on the first page, font button
  'Antet', 'Text:', 'Tip�rire pe prima pagin�', 'Font...',
  // header alignments: left, center, right
  'St�nga', 'La centru', 'Dreapta',
  // the same for footer (different hotkeys)
  'Subsol de pagin�', 'Text:', 'Tip�rire pe prima pagin�', 'Font...',
  'St�nga', 'La centru', 'Dreapta',
  // page numbers group-box, start from
  'Numerotare pagini', '�nceput cu:',
  // hint about codes
  'Ave�i posibilitate s� folosi combina�ii speciale:'#13'&&p - nr. paginii; &&P - nr. de pagini; &&d - dat�; &&t - timpul.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Pagin� cod fi�ier text', '&Alege�i codarea fi�ierului:',
  // thai, japanese, chinese (simpl), korean
  'Thailandez�', 'Japonez�', 'Chinez� (Simplificat�d)', 'Coreean�',
  // chinese (trad), central european, cyrillic, west european
  'Chinez� (Tradi�ional�)', 'limbi din Europa Central� �i Estic�', 'Alfabetul Chirilic', 'limbi vest-europene',
  // greek, turkish, hebrew, arabic
  'Greac�', 'Turc�', 'Ebraic�', 'Arab�',
  // baltic, vietnamese, utf-8, utf-16
  'Limbi Baltice', 'Vietnamez�', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Stil vizual', '&Selectare stil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertire �n text', 'Alege�i &delimitatorul:',
  // line break, tab, ';', ','
  '�ntrerupere linie', 'tabulator', 'punct �i virgul�', 'virgul�',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabelul cu r�nduri unite nu poate fi sortat',
  // title, main options groupbox
  'Sortare tabel', 'Sortare',
  // sort by column, case sensitive
  '&Sortare dup� coloan�:', '&Sensibil la litere mari �i mici',
  // heading row, range or rows
  'Excludere &r�nd antet', 'R�nduri de sortat: de la %d la %d',
  // order, ascending, descending
  'Ordine', '&Ascendent�', '&Descendent�',
  // data type, text, number
  'Tip', '&Text', '&Num�r',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserare numar', 'Proprietati', '&Denumire contor:', '&Tip numaratoare:',
  // numbering groupbox, continue, start from
  'Numaratoare', 'C&ontinuare', '&�ncepere de la:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Legenda', '&Eticheta:', '&Excludere eticheta din legenda',
  // position radiogroup
  'Pozitie', '&Deasupra obiectului selectat', '&Sub obiectul selectat',
  // caption text
  '&Text legenda:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numaratoare', 'Figura', 'Tabel',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Laturi chenar vizibile', 'Chenar',
  // Left, Top, Right, Bottom checkboxes
  '&Latura st�nga', '&Latura de sus', '&Latura dreapta', '&Latura de jos',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Modificare l��imea coloanei tabelului', 'Modificare �n�l�imea liniei tabelului',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Alineat primul r�nd', 'Alineat stanga', 'At�rnare', 'Alineat dreapta',
  // Hints on lists: up one level (left), down one level (right)
  'Mic�orare nivel list�', 'M�rire  nivel list�',
  // Hints for margins: bottom, left, right and top
  'C�mpul de jos', 'C�mpul din st�nga', 'C�mpul din dreapta', 'C�mpul de sus',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Dup� chenarul din st�nga', 'Dup� chenarul din dreapta', 'La centru', 'Dup� separator',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Alineat normal', 'F�r� spa�iere', 'Antet %d', 'Listare paragraf',
  // Hyperlink, Title, Subtitle
  'Hiperleg�tur�', 'Titlu', 'Subtitlu',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Accentuare', 'Accentuare subtil�', 'Accentuare intens�', 'Puternic�',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Ghilimele', 'Ghilimele intense', 'Referin�� subtil�', 'Referin�� intens�',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Text bloc', 'Variabil� HTML', 'Cod HTML', 'Acronim HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Defini�ie HTML', 'Tastatur� HTML', 'Exemplu HTML', 'Ma�in� de scris HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatat', 'HTML Cite', 'Antet', 'Subsol', 'Num�r pagin�',
  // Caption
  'Legenda',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Referin�� not� de final', 'Referin�� not� de subsol', 'Text not� de final', 'Text not� de subsol',
  // Sidenote Reference, Sidenote Text
  'referinta nota laterala', 'text nota laterala',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'culoare', 'culoare fundal', 'transparent', 'prestabilit', 'culoare subliniere',
  // default background color, default text color, [color] same as text
  'culoare fundal prestabilit�', 'culoare text prestabilit�', 'la fel ca textul',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'o singur� linie', '�ngro�at', 'cu dou� linii', 'linie punctat�', 'linie punctat� �ngro�at�', 'linie �ntrerupt�',
  // underline types: thick dashed, long dashed, thick long dashed,
  'linie �ntrerupt� �ngro�at�', 'linie �ntrerupt� continu�', 'linie �ntrerupt� �ngro�at�',
  // underline types: dash dotted, thick dash dotted,
  'linie �ntrerupt� �i punctat�', 'linie �ntrerupt� �i punctat� �ngro�at�',
  // underline types: dash dot dotted, thick dash dot dotted
  'linie punctat� dublu �i �ntrerupt�', 'linie punctat� dublu �i �ntrerupt� �ngro�at�',
  // sub/superscript: not, subsript, superscript
  'f�r� indice/exponent', 'indice', 'exponent',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'mod bi-di:', 'mo�tenit', 'de la st�nga la dreapta', 'de la dreapta la st�nga',
  // bold, not bold
  'aldin', 'non aldin',
  // italic, not italic
  'cursiv', 'non cursiv',
  // underlined, not underlined, default underline
  'subliniere', 'nesubliniere', 'subliniere prestabilit�',
  // struck out, not struck out
  't�iere cu o linie', 'f�r� t�iere cu o linie',
  // overlined, not overlined
  'Cu liniu�� deasupra', 'f�r� liniu�� deasupra',
  // all capitals: yes, all capitals: no
  'toate cu majuscule', 'majusculele dezactivate',
  // vertical shift: none, by x% up, by x% down
  'f�r� deplasare vertical�', 'deplasare cu %d%% �n sus', 'deplasare cu %d%% �n jos',
  // characters width [: x%]
  'l��ime caractere',
  // character spacing: none, expanded by x, condensed by x
  'spa�iere normal� caractere', 'spa�iere extins� cu %s', 'spa�iere condensat� cu %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'font prestabilit', 'paragraf prestabilit', 'mo�tenit',
  // [hyperlink] highlight, default hyperlink attributes
  'eviden�iere', 'prestabilit',
  // paragraph aligmnment: left, right, center, justify
  'aliniere la st�nga', 'aliniere la dreapta', '�n centru', 'aliniere st�nga-dreapta',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  '�n�l�ime linie: %d%%', 'spa�iu �ntre linii: %s', '�n�l�ime linie: cel pu�in %s',
  '�n�l�ime linie: exact %s',
  // no wrap, wrap
  'Rupere r�nduri dezactivat�', 'rupere r�nduri',
  // keep lines together: yes, no
  'p�streaz� liniile grupate', 'nu p�streaz� liniile grupate',
  // keep with next: yes, no
  'p�streaz� �n paragraful urm�tor', 'nu p�streaz� �n paragraful urm�tor',
  // read only: yes, no
  'doar citire', 'editabil',
  // body text, heading level x
  'nivel schi��: text principal', 'nivel schi��: %d',
  // indents: first line, left, right
  'indentare prima linie', 'indentare la st�nga', 'indentare la dreapta',
  // space before, after
  'spa�iu �nainte', 'spa�iu dup�',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulatori', 'niciunul', 'aliniere', 'st�nga', 'dreapta', 'centru',
  // tab leader (filling character)
  'caracter de ghidare',
  // no, yes
  'nu', 'da',
  // [padding/spacing/side:] left, top, right, bottom
  'st�nga', 'sus', 'dreapta', 'jos',
  // border: none, single, double, triple, thick inside, thick outside
  'niciunul', 'singur', 'dublu', 'triplu', 'gros �n interior', 'gros �n exterior',
  // background, border, padding, spacing [between text and border]
  'fundal', 'chenar', 'completare', 'spa�iere',
  // border: style, width, internal width, visible sides
  'stil', 'l��ime', 'l��ime intern�', 'laterale vizibile',
  // style inspector -----------------------------------------------------------
  // title
  'Inspector de Stil',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Niciunul)', 'Paragraf', 'Font', 'atribute',
  // border and background, hyperlink, standard [style]
  'Chenar �i fundal', 'Hiperleg�tur�', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stiluri', 'Stil',
  // name, applicable to,
  '&Nume:', 'Aplicabil &la:',
  // based on, based on (no &),
  '&Bazat pe:', 'Bazat pe:',
  //  next style, next style (no &)
  'Stil pentru &urm�torul paragraf:', 'Stil pentru urm�torul paragraf:',
  // quick access check-box, description and preview
  '&Acces rapid', 'Descriere �i &previzualizare:',
  // [applicable to:] paragraph and text, paragraph, text
  'paragraf �i text', 'paragraf', 'text',
  // text and paragraph styles, default font
  'Stiluri Text �i paragraf', 'Font prestabilit',
  // links: edit, reset, buttons: add, delete
  'Editare', 'Resetare', '&Ad�ugare', '&�tergere',
  // add standard style, add custom style, default style name
  'Ad�ugare &stil Standard...', 'Ad�ugare &stil personalizat', 'Stil %d',
  // choose style
  'Alegere &stil:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Stiluri RichView (*.rvst)|*.rvst', 'Eroare la �nc�rcarea fi�ierului', 'Acest fi�ier nu con�ine stiluri',
  // Title, group-box, import styles
  'Importare stiluri din fi�ier', 'Importare', 'I&mportare stiluri:',
  // existing styles radio-group: Title, override, auto-rename
  'Stiluri existente', '&Suprascriere', '&ad�ugare redenumire',
  // Select, Unselect, Invert,
  '&Selectare', '&Deselectare', '&Inversare',
  // select/unselect: all styles, new styles, existing styles
  '&Toate stilurile', '&Stiluri noi', '&Stiluri existente',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(�tergere Format)', '(Toate stilurile)',
  // dialog title, prompt
  'Stiluri', '&Alege�i stilul de aplicare:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  'Sinonime', 'Ignorare toate', 'Ad�ugare �n dic?ionar',
  // Progress messages ---------------------------------------------------------
  'Se descarc� %s', 'Se preg�te�te pentru tip�rire...', 'Se tip�re�te pagina %d',
  'Se converte�te din RTF...',  'Se converte�te �n RTF...',
  'Se cite�te %s...', 'se scrie %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Fara nota', 'Nota de subsol', 'Nota de final', 'Nota laterala', 'Caseta Text'
  );


initialization
  RVA_RegisterLanguage(
    'Romanian', // english language name, do not translate
    'Rom�na', // native language name
    EASTEUROPE_CHARSET, @Messages);

end.
