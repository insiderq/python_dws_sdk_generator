﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVXMLMisc.pas' rev: 27.00 (Windows)

#ifndef RvxmlmiscHPP
#define RvxmlmiscHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVXMLBase.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvxmlmisc
{
//-- type declarations -------------------------------------------------------
struct DECLSPEC_DRECORD TBullet
{
public:
	int OldImgIndex;
	int ImgIndex;
	Vcl::Imglist::TCustomImageList* ImgList;
};


class DELPHICLASS TBulletList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TBulletList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
private:
	HIDESBASE TBullet __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, const TBullet &Value);
	
public:
	HIDESBASE void __fastcall Add(int ImgIndex, int OldImgIndex, Vcl::Imglist::TCustomImageList* ImgList);
	int __fastcall GetImgIndex(int OldImgIndex, Vcl::Imglist::TCustomImageList* ImgList);
	__property TBullet Items[int Index] = {read=Get, write=Put};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TBulletList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TBulletList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TImgListList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TImgListList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
private:
	HIDESBASE Vcl::Imglist::TCustomImageList* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, Vcl::Imglist::TCustomImageList* const Value);
	
public:
	HIDESBASE Vcl::Imglist::TCustomImageList* __fastcall Add(void);
	void __fastcall LoadFromXML(Rvxmlbase::TXMLTag* Node);
	void __fastcall SaveToXML(Rvxmlbase::TXMLTag* Node, bool SaveToFiles);
	__property Vcl::Imglist::TCustomImageList* Items[int Index] = {read=Get, write=Put};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TImgListList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TImgListList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall ScanBullets(Crvdata::TCustomRVData* RVData, TBulletList* BulletList, TImgListList* ImgList, int SelStart, int SelEnd);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetImageFileName(const System::UnicodeString Path, const System::UnicodeString Name, const System::UnicodeString Ext);
}	/* namespace Rvxmlmisc */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVXMLMISC)
using namespace Rvxmlmisc;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvxmlmiscHPP
