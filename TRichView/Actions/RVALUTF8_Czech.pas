﻿// This file is a copy of RVAL_Czech.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Czech translation                               }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Andrle Pavel 2003-04-09                }
{ Updated by: Pavel Polivka   2007-11-12                }
{             Jiří Přibil     2011-02-19                }
{             Alconost        2012-11-02                }
{             Alconost        2014-05-01                }
{*******************************************************}

unit RVALUTF8_Czech;
{$I RV_Defs.inc}    
interface
    
implementation
uses Windows, RVALocalize;
    
const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'palce', 'cm', 'mm', 'pica', 'pixely', 'body',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  'Soubor', 'Úpravy', 'Formát', 'Písmo', 'Odstavec', 'Vložit', 'Tabulka', 'Okno', 'Nápověda',
  // exit
  'Konec',
  // top-level menus: View, Tools,
  '&Zobrazit', 'Ná&stroje',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Velikost', 'Styl', 'Vybra&t', 'Zarov&nání buňky', 'Ohranič&ení buňky',
  // menus: Table cell rotation
  'Otočení &Buňky',   
  // menus: Text flow, Footnotes/endnotes
  'Obtékání textu', 'Poznámky pod čarou a vysvětlivky',
  // ribbon tabs: tab1, tab2, view, table
  'Domů', 'Pokročilé', 'Zobrazení', 'Tabulka',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Schránka', 'Písmo', 'Odstavec', 'Seznamy', 'Úpravy',
  // ribbon groups: insert, background, page setup,
  'Vložení', 'Pozadí', 'Stránka',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Odkazy', 'Poznámky pod čarou', 'Zobrazení dokumentů', 'Zobrazit/Skrýt', 'Lupa', 'Nastavení',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Vložit', 'Odstranit', 'Úpravy', 'Ohraničení',
  // ribbon groups: styles 
  'Styly',
  // ribbon screen tip footer,
  'Stiskněte F1 pro nápovědu',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Naposledy otevřené', 'Uložit jako...', 'Náhled a tisk dokumentu',
  // ribbon label: units combo
  'Jednotky:',       
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'Nový', 'Nový|Vytvoří nový dokument',
  // TrvActionOpen
  'Otevřít...', 'Otevřít|Otevře existující dokument',
  // TrvActionSave
  'Uložit', 'Uložit|Uloží dokument',
  // TrvActionSaveAs
  'Uložit jako...', 'Uložit jako...|Uloží dokument pod novým názvem',
  // TrvActionExport
  'Export...', 'Export|Export dokumentu do souboru jiného formátu',
  // TrvActionPrintPreview
  'Náhled', 'Náhled|Zobrazí dokument ve tvaru, v jakém bude vytisknut',
  // TrvActionPrint
  'Tisk', 'Tisk|Vytiskne dokument na zvolené tiskárně',
  // TrvActionQuickPrint
  'Tisk', 'Rychlý tisk', 'Rychlý tisk|Okamžitě vytiskne dokument na standardní tiskárně',
   // TrvActionPageSetup
  'Vzhled stránky...', 'Vzhled stránky|Nastaví okraje dokumentu, velikost papíru, orientaci, zdroj, záhlaví a zápatí',
  // TrvActionCut
  'Vyjmout', 'Vyjmout|Odebere vybraný text nebo objekt a vloží jej do schránky',
  // TrvActionCopy
  'Kopírovat', 'Kopírovat|Zkopíruje vybraný text nebo objekt do schránky',
  // TrvActionPaste
  'Vložit', 'Vložit|Vloží text nebo objekt ze schránky na vybrané místo',
  // TrvActionPasteAsText
  'Vložit jako text', 'Vložit jako text|Vloží obsah schránky ve formátu prostého textu',  
  // TrvActionPasteSpecial
  'Vložit speciální...', 'Vložit speciální|Vloží obsah schránky ve speciálním formátu',
  // TrvActionSelectAll
  'Vybrat vše', 'Vybrat vše|Vybere všechny položky dokumentu',
  // TrvActionUndo
  'Zpět', 'Zpět|Vrátí zpět poslední akci',
  // TrvActionRedo
  'Znovu', 'Znovu|Provede znovu poslední vrácenou akci',
  // TrvActionFind
  'Najít...', 'Najít|Vyhledá textový řetězec v dokumentu',
  // TrvActionFindNext
  'Najít další', 'Najít další|Vyhledá následující výskyt hledaného textu v dokumentu',
  // TrvActionReplace
  'Nahradit...', 'Nahradit|Nahradí hledaný text v dokumentu jiným',
  // TrvActionInsertFile
  'Soubor...', 'Vložit soubor|Vloží obsah souboru do dokumentu',
  // TrvActionInsertPicture
  'Obrázek...', 'Vložit obrázek|Vloží obrázek do dokumentu',
  // TRVActionInsertHLine
  'Oddělovač', 'Vložit oddělovač|Vloží vodorovnou oddělovací čáru',
  // TRVActionInsertHyperlink
  'Hypertextový odkaz...', 'Vložit hypertextový odkaz|Vloží hypertextový odkaz na WWW stránku nebo soubor',
  // TRVActionRemoveHyperlinks
  'Odstranit hypertextové odkazy', 'Odstranit hypertextové odkazy|Odstraní všechny hypertextové odkazy z označeného textu',  
  // TrvActionInsertSymbol
  'Symbol...', 'Vložit symbol|Vloží speciální znak výběrem z tabulky znaků',
  // TrvActionInsertNumber
  '&Číslo...', 'Vložit Číslo|Vloží číslo',
  // TrvActionInsertCaption
  'Vložit &Popisek...', 'Vložit popisek|Vloží popisek k vzbranému objektu',
  // TrvActionInsertTextBox
  '&Textové Pole', 'Vložit Textové Pole|Vloží textové pole',
  // TrvActionInsertSidenote
  '&Poznámka na okraj', 'Vložit Poznámku na Okraj|Vloží poznámku na okraj',
  // TrvActionInsertPageNumber
  '&Číslo stránky', 'Vložit Číslo stránky|Vloží Číslo stránky',
  // TrvActionParaList
  'Odrážky a číslování...', 'Odrážky a číslování|Definice stylu odrážek a číslování vybraného odstavce',
  // TrvActionParaBullets
  'Odrážky', 'Odrážky|Vloží nebo odebere odrážku pro vybraný odstavec',
  // TrvActionParaNumbering
  'Číslování', 'Číslování|Vloží nebo odebere číslování pro vybraný odstavec',
  // TrvActionColor
  'Barva pozadí dokumentu...', 'Barva pozadí dokumentu|Změní barvy pozadí dokumentu',
  // TrvActionFillColor
  'Barva pozadí...', 'Barva pozadí|Změní barvu pozadí textu, odstavce, buňky, tabulky nebo dokumentu',
  // TrvActionInsertPageBreak
  'Vložit oddělovač stránky', 'Vložit oddělovač stránky|Vloží oddělovač stránky',
  // TrvActionRemovePageBreak
  'Odstranit oddělovač stránky', 'Odstranit oddělovač stránky|Odstraní oddělovač stránky',
  // TrvActionClearLeft
  'Neobtékat text zleva', 'Neobtékat text zleva|Umístí odstavec pod jakýkoliv obrázek zarovnaný vlevo',
  // TrvActionClearRight
  'Neobtékat text zprava', 'Neobtékat text zprava|Umístí odstavec pod jakýkoliv obrázek zarovnaný pravo',
  // TrvActionClearBoth
  'Neobtékat text zleva ani zprava', 'Neobtékat text zleva ani zprava|Umístí odstavec pod jakýkoliv obrázek zarovnaný vlevo nebo vpravo',
  // TrvActionClearNone
  '&Normální obtékání textu', 'Normální obtékání textu|Umožní obtékání textu kolem obrázků zarovnaných vlevo i vpravo',
  // TrvActionVAlign
  '&Pozice objektu...', 'Pozice objektu|Změní pozici vybraného objektu',    
  // TrvActionItemProperties
  'Vlastnosti objektu...', 'Vlastnosti objektu|Nastaví vlastnosti vybraného objektu',
  // TrvActionBackground
  'Pozadí dokumentu...', 'Pozadí dokumentu|Nastaví obrázek, barvu pozadí dokumentu',
  // TrvActionParagraph
  'Odstavec...', 'Odstavec|Nastavení vlastností vybraného odstavce',
  // TrvActionIndentInc
  'Zvětšit odsazení', 'Zvětšit odsazení|Zvětší vzdálenost vybraného odstavce od levého okraje stránky',
  // TrvActionIndentDec
  'Zmenšit odsazení', 'Zmenšit odsazení|Zmenší vzdálenost vybraného odstavce od levého okraje stránky',
  // TrvActionWordWrap
  'Zalamování řádku', 'Zalamování řádku|Nastaví nebo zruší zalamování řádku vybraného odstavce',
  // TrvActionAlignLeft
  'Zarovnat vlevo', 'Zarovnat vlevo|Zarovná text vybraného odstavce k levému okraji stránky',
  // TrvActionAlignRight
  'Zarovnat vpravo', 'Zarovnat vpravo|Zarovná text vybraného odstavce k pravému okraji stránky',
  // TrvActionAlignCenter
  'Zarovnat na střed', 'Zarovnat na střed|Zarovná text vybraného odstavce na střed stránky',
  // TrvActionAlignJustify
  'Zarovnat do bloku', 'Zarovnat do bloku|Zarovná text vybraného odstavce do bloku',
  // TrvActionParaColor
  'Barva pozadí odstavce...', 'Barva pozadí odstavce|Změní barvu pozadí vybraného odstavce',
  // TrvActionLineSpacing100
  'Jednoduché řádkování', 'Jednoduché řádkování|Nastaví jednoduché řádkování odstavce',
  // TrvActionLineSpacing150
  'Řádkování 1,5 řádku', 'Řádkování 1,5 řádku|Nastaví řádkování odstavce na výšku 1,5 násobek výšky řádku',
  // TrvActionLineSpacing200
  'Dvojité řádkování', 'Dvojité řádkování|Nastaví řádkování odstavce na dvojitou výšku řádku',
  // TrvActionParaBorder
  'Ohraničení a pozadí odstavce...', 'Ohraničení a pozadí odstavce|Nastaví ohraničení a pozadí vybraného odstavce',
  // TrvActionInsertTable
  'Vložit tabulku...', 'Tabulka', 'Vložit tabulku|Vloží novou tabulku',
  // TrvActionTableInsertRowsAbove
  'Vložit řádek před', 'Vložit řádek před|Vloží nový řádek do tabulky před vybranou buňku',
  // TrvActionTableInsertRowsBelow
  'Vložit řádek za', 'Vložit řádek za|Vloží nový řádek do tabulky za vybranou buňku',
  // TrvActionTableInsertColLeft
  'Vložit sloupec vlevo', 'Vložit sloupec vlevo|Vloží sloupec vlevo od vybrané buňky',
  // TrvActionTableInsertColRight
  'Vložit sloupec vpravo', 'Vložit sloupec vpravo|Vloží sloupec vpravo od vybrané buňky',
  // TrvActionTableDeleteRows
  'Odstranit řádky', 'Odstranit řádky|Odstraní vybrané řádky z tabulky',
  // TrvActionTableDeleteCols
  'Odstranit sloupce', 'Odstranit sloupce|Odstraní vybrané sloupce z tabulky',
  // TrvActionTableDeleteTable
  'Odstranit tabulku', 'Odstranit tabulku|Odstraní tabulku',
  // TrvActionTableMergeCells
  'Sloučit buňky', 'Sloučit buňky|Sloučí vybrané buňky v jednu',
  // TrvActionTableSplitCells
  'Rozdělit buňku...', 'Rozdělit buňku|Rozdělí buňku na požadovaný počet buňek',
  // TrvActionTableSelectTable
  'Vybrat tabulku', 'Vybrat tabulku|Vybere celou tabulku',
  // TrvActionTableSelectRows
  'Vybrat řádek', 'Vybrat řádek|Provede výběr celého řádku',
  // TrvActionTableSelectCols
  'Vybrat sloupec', 'Vybrat sloupec|Provede výběr celého sloupce',
  // TrvActionTableSelectCell
  'Vybrat buňku', 'Vybrat buňku|Provede výběr celé buňky',
  // TrvActionTableCellVAlignTop

  'Zarovnat buňku nahoru', 'Zarovnat buňku nahoru|Zarovná zobrazený text v buňce k jejímu hornímu okraji',
  // TrvActionTableCellVAlignMiddle
  'Zarovnat buňku na střed', 'Zarovnat buňku na střed|Zarovná zobrazený text v buňce na střed',
  // TrvActionTableCellVAlignBottom
  'Zarovnat buňku dolů', 'Zarovnat buňku dolů|Zarovná zobrazený text v buňce k jejímu dolnímu okraji',
  // TrvActionTableCellVAlignDefault
  'Standardní zarovnání buňky', 'Standardní zarovnání buňky|Nastaví standardní svislé zarovnání buňky',
  // TrvActionTableCellRotationNone
  '&Bez otočení buňky', 'Bez otočení buňky|Otočí obsah buňky o 0°',
  // TrvActionTableCellRotation90
  'Otočit buňku o &90°', 'Otočit buňku o 90°|Otočí obsah buňky o 90°',
  // TrvActionTableCellRotation180
  'Otočit buňku o &180°', 'Otočit buňku o 180°|Otočí obsah buňky oy 180°',
  // TrvActionTableCellRotation270
  'Otočit buňku o &270°', 'Otočit buňku o 270°|Otočí obsah buňky oy 270°',
  // TrvActionTableProperties
  'Vlastnosti tabulky...', 'Vlastnosti tabulky|Nastaví vlastnosti tabulky',
  // TrvActionTableGrid
  'Zobrazit mřížku', 'Zobrazit mřížku|Zobrazí nebo skryje mřížku tabulky',
  // TRVActionTableSplit
  'Ro&zdělit tabulku', 'Rozdělit tabulku|Rozdělí tabulku na dvě samostatné od vybraného řádku',
  // TRVActionTableToText
  'Převést na te&xt...', 'Převést na text|Převede tabulku na text',
  // TRVActionTableSort
  '&Seřadit...', 'Seřadit|Seřadí řádky tabulky',
  // TrvActionTableCellLeftBorder
  'Ohraničení vlevo', 'Ohraničení vlevo|Zobrazí nebo skryje levé ohraničení buňky',
  // TrvActionTableCellRightBorder
  'Ohraničení vpravo', 'Ohraničení vpravo|Zobrazí nebo skryje pravé ohraničení buňky',
  // TrvActionTableCellTopBorder
  'Ohraničení nahoře', 'Ohraničení nahoře|Zobrazí nebo skryje horní ohraničení buňky',
  // TrvActionTableCellBottomBorder
  'Ohraničení dole', 'Ohraničení dole|Zobrazí nebo skryje spodní ohraničení buňky',
  // TrvActionTableCellAllBorders
  'Ohraničení dokola', 'Ohraničení dokola|Zobrazí nebo skryje ohraničení celé buňky',
  // TrvActionTableCellNoBorders
  'Bez ohraničení', 'Bez ohraničení|Skryje celé ohraničení vybrané buňky',
  // TrvActionFonts & TrvActionFontEx
  'Písmo...', 'Písmo|Změna nastavení písma vybraného textu',
  // TrvActionFontBold
  'Tučné', 'Tučné|Nastaví nebo zruší tučné zobrazení vybraného textu',
  // TrvActionFontItalic
  'Kurzíva', 'Kurzíva|Nastaví nebo zruší zobrazení vybraného textu kurzívou',
  // TrvActionFontUnderline
  'Podtržené', 'Podtržené|Nastaví nebo zruší podtržení vybraného textu',
  // TrvActionFontStrikeout
  'Přeškrtnuté', 'Přeškrtnuté|Nastaví nebo zruší přeškrtnutí vybraného textu',
  // TrvActionFontGrow
  'Zvětšit písmo', 'Zvětšit písmo|Zvětší výšku vybraného textu o 10%',
  // TrvActionFontShrink
  'Zmenšit písmo', 'Zmenšit písmo|Zmenší výšku vybraného textu o 10%',
  // TrvActionFontGrowOnePoint
  'Zvětšit písmo o 1 bod', 'Zvětšit písmo o 1 bod|Zvětší výšku vybraného textu o 1 bod',
  // TrvActionFontShrinkOnePoint
  'Zmenšit písmo o 1 bod', 'Zmenšit písmo o 1 bod|Zmenší výšku vybraného textu o 1 bod',
  // TrvActionFontAllCaps
  'Všechna velká', 'Všechna velká|Změní styl písma vybraného textu na všechny velké znaky',
  // TrvActionFontOverline
  'Nadtržené', 'Nadtržené|Nastaví nebo zruší vodorovnou linku nad vybraným textem',
  // TrvActionFontColor
  'Barva písma...', 'Barva písma|Změní barvu vybraného textu',
  // TrvActionFontBackColor
  'Barva pozadí písma...', 'Barva pozadí písma|Změní barvu pozadí vybraného textu',
  // TrvActionAddictSpell3
  'Kontrola pravopisu', 'Kontrola pravopisu|Provede kontrolu pravopisu vybraného textu',
  // TrvActionAddictThesaurus3
  'Synonyma', 'Synonyma|Nabídne synonyma pro vybrané slovo',
  // TrvActionParaLTR
  'Zleva doprava', 'Zleva doprava|Nastaví směr textu na směr zleva doprava',
  // TrvActionParaRTL
  'Zprava doleva', 'Zprava doleva|Nastaví směr textu na směr zprava doleva',
  // TrvActionLTR
  'Text zleva doprava', 'Text zleva doprava|Pro vybraný text nastaví směr textu na směr zleva doprava',
  // TrvActionRTL
  'Text zprava doleva', 'Text zprava doleva|Pro vybraný text nastaví směr textu na směr zprava doleva',
  // TrvActionCharCase
  'Malá / velká písmena', 'Malá / velká písmena|Změní velikost písmen vybraného textu',
  // TrvActionShowSpecialCharacters
  '&Netisknutelné znaky', 'Netisknutelné znaky|Zobrazit nebo skrýt netisknutelné znaky, např. znaky odstavce, znaky tabulátoru a mezery',
  // TrvActionSubscript
  'Do&lní index', 'Dolní index|Konvertovat vybraný text na dolní index',
  // TrvActionSuperscript
  'Horní index', 'Horní index|Konvertovat vybraný text na horní index',
  // TrvActionInsertFootnote
  'Poznámka pod čarou', 'Poznámka pod čarou|Vloží poznámku pod čarou',
  // TrvActionInsertEndnote
  'Vysvětlivka', 'Vysvětlivka|Vloží vysvětlivku na konec dokumentu',
  // TrvActionEditNote
  'Editovat poznámku/vysvětlivku', 'Editovat poznámku/vysvětlivku|Umožní editovat poznámku pod čarou nebo vysvětlivku',
  '&Skrýt', 'Skrýt|Skryje nebo zobrazí označený text',            
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Styly...', 'Styly|Otevře dialog správy stylů',
  // TrvActionAddStyleTemplate
  '&Přidat styl...', 'Přidat styl|Vytvoří nový styl textu nebo odstavce',
  // TrvActionClearFormat,
  '&Vymazat formát', 'Vymazat formát|Vymaže veškeré formátování textu a odstavce z vybraného fragmentu',
  // TrvActionClearTextFormat,
  'Vymazat formát &textu', 'Vymazat formát textu|Vymaže veškeré formátování vybraného textu',
  // TrvActionStyleInspector
  'Kontrola &stylů', 'Kontrola stylů|Zobrazí nebo skryje kontrolu stylů',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Storno', 'Zavřít', 'Vložit', 'Otevřít...', 'Uložit...', 'Vymazat', 'Nápověda', 'Odstranit',
  // Others  -------------------------------------------------------------------
  // percents
  'procenta',
  // left, top, right, bottom sides
  'Levá strana', 'Horní strana', 'Pravá strana', 'Spodní strana',
  // save changes? confirm title
  'Uložit provedené změny do %s?', 'Potvrzení',
  // warning: losing formatting
  '%s obsažený objekt nebo formátování není podporováno ve vybraném formátu souboru.'#13+
  'Opravdu chcete pro uložení použít tento formát?',
  // RVF format name
  'RichView Formát',
  // Error messages ------------------------------------------------------------
  'Chyba',
  'Chyba při načítání souboru.'#13#13'Možné příčiny:'#13'- formát otevíraného souboru není podporován v této aplikaci;'#13+
  '- soubor je poškozen;'#13'- soubor je otevřen a uzamknut jinou aplikací.',
  'Chyba při načítání obrázku.'#13#13'Možné příčiny:'#13'- formát obrázku není podporován touto aplikací;'#13+
  '- vybraný soubor neobsahuje obrázek;'#13+
  '- soubor je poškozen;'#13'- soubor je otevřen a uzamknut jinou aplikací.',
  'Chyba při ukládání souboru.'#13#13'Možné příčiny:'#13'- není místo na disku;'#13+
  '- disk je chráněn proti zápisu;'#13'- výměnné médium není vloženo;'#13+
  '- soubor je otevřen a uzamknut jinou aplikací;'#13'- výměnné médium je poškozeno.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView soubory (*.rvf)|*.rvf',  'RTF soubory (*.rtf)|*.rtf' , 'XML soubory (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Textové soubory (*.txt)|*.txt', 'Textové soubory - Unicode (*.txt)|*.txt', 'Textové soubory - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML soubory (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - zjednodušený (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Dokumenty Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Hledání dokončeno', 'Hledaný řetězec ''%s'' nenalezen.',
  // 1 string replaced; Several strings replaced
  '1 řetězec nahrazen.', '%d řetězců nahrazeno.',
  // continue from beginning? end?
  // continue search from the beginning/end?
  'Byl dosažen konec dokumentu, chcete pokračovat od začátku?',
  'Byl dosažen začátek dokumentu, chcete pokračovat od konce?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'není', 'automatická',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Černá', 'Hnědá', 'Olivově zelená', 'Tmavě zelená', 'Tmavě šedozelená', 'Tmavě modrá', 'Indigová modř', 'Šedá-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tmavě červená', 'Oranžová', 'Tmavě žlutá', 'Zelená', 'Šedozelená', 'Modrá', 'Modrošedá', 'Šedá-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Červená', 'Světle oranžová', 'Žlutozelená', 'Mořská zeleň', 'Akvamarinová', 'Světle modrá', 'Fialová', 'Šedá-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Růžová', 'Zlatá', 'Žlutá', 'Jasně zelená', 'Tyrkysová', 'Nebeská modř', 'Švestková', 'Šedá-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Světle růžová', 'Žlutooranžová', 'Světle žlutá', 'Světle zelená', 'Světle tyrkysová', 'Bledě modrá', 'Lavandulová', 'Bílá',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'není', 'automatická', 'více barev...', 'standardní',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Pozadí', 'Barva:', 'Pozice obrázku', 'Náhled', 'Ukázka textu.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  'žádný', 'celé okno', 'ukotvené dlaždice', 'dlaždice', 'na střed',
  // Padding button
  'Odsazení textu...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Barva pozadí', 'Použít na:', 'Více barev...', 'Odsazení textu...',
  'Prosím vyberte barvu',
  // [apply to:] text, paragraph, table, cell
  'text', 'odstavec' , 'tabulku', 'buňku',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Písmo', 'Písmo', 'Proložení znaků',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Písmo:', 'Velikost:', 'Řez písma', 'Tučně', 'Kurzíva',
  // Script, Color, Back color labels, Default charset
  'Skript:', 'Barva:', 'Po&zadí:', '(Standardní)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Styl', 'Podtržené', 'Nadtržené', 'Přeškrtnuté', 'Všechna velká',
  // Sample, Sample text
  'Náhled', 'Ukázkový text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Mezery mezi znaky', 'O kolik:', 'rozšířené', 'zúžené',
  // Offset group-box, Offset label, Down, Up,
  'Umístění', 'O kolik:', 'snížené', 'zvýšené',
  // Scaling group-box, Scaling
  'Proložení znaků', 'Měřítko:',
  // Sub/super script group box, Normal, Sub, Super
  'Dolní a horní indexy', '&Normání', 'D&olní index', 'Hor&ní index',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Odsazení textu', 'Nahoře:', 'Vlevo:', 'Dole:', 'Vpravo:', 'Všechny stejné',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Vložit hypertextový odkaz', 'Odkaz', 'Text:', 'Cíl', '<<výběr>>',
  // cannot open URL
  'Nelze přejít na "%s"',
  // hyperlink properties button, hyperlink style
  '&Vlastnosti...', '&Styl:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ńolors
  'Vlastnosti hypertextového odkazu', 'Barva odkazu', 'Aktivní barva',
  // Text [color], Background [color]
  'Text:', 'Pozadí',
  // Underline [color]
  'P&odtržení:', 
  // Text [color], Background [color] (different hotkeys)
  'Text:', 'Pozadí',
  // Underline [color], Attributes group-box,
  'P&odtržení:', 'Atributy',
  // Underline type: always, never, active (below the mouse)
  '&Podtržení:', 'vždy', 'nikdy', 'aktivní',
  // Same as normal check-box
  'Standardní',
  // underline active links check-box
  '&Podtrhnout aktivní odkazy',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Vložit symbol', 'Písmo:', 'Znaková sada:', 'Unicode',
  // Unicode block
  '&Blok:',  
  // Character Code, Character Unicode Code, No Character
  'Kód znaku: %d', 'Kód znaku: Unicode %d', '(není znak)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Vložit tabulku', 'Velikost tabulky', 'Počet sloupců:', 'Počet řádků:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Šířka tabulky', 'Automatická', 'Roztáhnout dle okna', 'Manuální nastavení',
  // Remember check-box
  'Použít jako vzor pro novou tabulku',
  // VAlign Form ---------------------------------------------------------------
  'Pozice',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Vlastnosti', 'Obrázek', 'Pozice a Rozměr', 'Čára', 'Tabulka', 'Řádek', 'Buňka',
  // Image Appearance, Image Misc, Number tabs
  'Vzhled', 'Různé', 'Číslo',
  // Box position, Box size, Box appearance tabs
  'Pozice', 'Rozměr', 'Vzhled',
  // Preview label, Transparency group-box, checkbox, label
  'Náhled:', 'Transparentní', 'Transparentní', 'Transparentní barva:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Změnit...', '&Uložit...', 'automatická',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Svislé zarovnání', 'Zarovnání:',
  'spodní hrana objektu na účaří textu', 'střed objektu na účaří textu',
  'horní hrana objektu na horní dotažnici textu', 'dolní hrana objektu na dolní dotažnici textu', 'střed objektu na střed řádku',
  // align to left side, align to right side
  'vlevo', 'vpravo',      
  // Shift By label, Stretch group box, Width, Height, Default size
  'Posunout o:', 'Velikost', 'Šířka:', 'Výška:', 'Standardní velikost: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Zachovat &proporce', '&Reset',
  // Inside the border, border, outside the border groupboxes
  'Uvnitř okraje', 'Okraj', 'Vně okraje',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Barva výplně:', '&Odsazení obsahu:',
  // Border width, Border color labels
  'Šířka &okraje:', 'Barva &okraje:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Vodorovné mezery:', '&Svislé mezery:',
  // Miscellaneous groupbox, Tooltip label
  'Různé', '&Tip k nástroji:',
  // web group-box, alt text
  'Web', 'Alternativní &text:',
  // Horz line group-box, color, width, style
  'Oddělovací čára', 'Barva:', 'Šířka:', 'Styl:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabulka', 'Šířka:', 'Barva výplně:', 'Odsazení buňky...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Vodorovně odsadit obsah buňky:', '&Svisle odsadit obsah buňky:', 'Okraj a pozadí',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Viditelné &strany okraje...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Zachovat obsah pohromadě',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Otočení', '&Žádné', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Okraj:', 'Viditelné &strany okraje...',
  // Border, CellBorders buttons
  'Ohraničení tabulky...', 'Ohraničení buňky...',
  // Table border, Cell borders dialog titles
  'Ohraničení tabulky', 'Ohraničení buňky',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tisk', 'Tisk tabulky na jedné stránce', 'Počet hlavičkových řádků:',
  'Hlavičkové řádky budou opakovány na každé stránce tabulky',
  // top, center, bottom, default
  'nahoru', 'na střed', 'dolů', 'standardní',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Nastavení', 'Upředňostňovaná šířka:', 'Minimální výška:', 'Barva výplně:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Ohraničení', 'Viditelné strany:',  'Barva stínu:', 'Barva světla', 'Barva:',
  // Background image
  'Obrázek...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Vodorovná pozice', 'Svislá pozice', 'Pozice v textu',
  // Position types: align, absolute, relative
  'Zarovnat', 'Absolutní pozice', 'Relativní pozice',
  // [Align] left side, center, right side
  'levá strana pole k levé straně',
  'střed pole ke středu',
  'pravá strana pole k pravé straně',
  // [Align] top side, center, bottom side
  'horní strana pole k horní straně',
  'střed pole ke středu',
  'dolní strana pole k dolní straně',
  // [Align] relative to
  'relativně k',
  // [Position] to the right of the left side of
  'vpravo od levé strany',
  // [Position] below the top side of
  'pod horní stranou',
  // Anchors: page, main text area (x2)
  '&Stránka', '&Hlavní textová oblast', 'S&tránka', 'Hlavní  te&xtová oblast',
  // Anchors: left margin, right margin
  '&Levý okraj', '&Pravý okraj',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Horní okraj', '&Dolní okraj', '&Vnitřní okraj', '&Vnější okraj',
  // Anchors: character, line, paragraph
  '&Znak', 'Ř&ádek', 'Odsta&vec',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Zrcadl&ené okraje', 'Rozlož&ení v buňce tabulky',
  // Above text, below text
  '&Nad textem', '&Pod textem',
  // Width, Height groupboxes, Width, Height labels
  'Šířka', 'Výška', '&Šířka:', '&Výška:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Svislé zarovnání', '&Nahoře', '&Střed', '&Dole',
  // Border and background button and title
  'O&kraj a pozadí...', 'Okraj a Pozadí Pole',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Vložit speciální', 'Vložit jako:', 'Rich text formát', 'HTML formát',
  'Text', 'Unicode text', 'Bitmapový obrázek', 'Obrázkový metasoubor',
  'Grafický soubor',  'URL',
  // Options group-box, styles
  'Možnosti', '&Styly:',
  // style options: apply target styles, use source styles, ignore styles
  'použít styly cílového dokumentu', 'zachovat styly a vzhled',
  'zachovat vzhled, ignorovat styly',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Odrážky a číslování', 'Odrážky', 'Číslování', 'Odrážky', 'Číslování',
  // Customize, Reset, None
  'Vlastní...', 'Obnovit', 'není',
  // Numbering: continue, reset to, create new
  'navázat na předchozí', 'pokračovat v číslování od', 'začít znovu číslovat od',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'velká písmena (A, B, …)', 'velké římské číslice (I, II, …)', 'arabské číslice (1, 2, …)', 'malá písmena (a, b, …)', 'malé římské číslice (i, ii, …)',
  // Bullet type
  'prázdné kolečko', 'plné kolečko', 'čtvereček',
  // Level, Start from, Continue numbering, Numbering group-box
  'Úroveň:', 'Začít od:', 'Pokračovat', 'Číslování',    
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Vlastní nastavení', 'Úroveň', 'Počet:', 'Vlastnosti', 'Typ:',
  // List types: bullet, image
  'odrážka', 'obrázek', 'vložit číslo|',
  // Number format, Number, Start level from, Font button
  'Formát čísla:', 'Číslo', 'Začít číslování úrovně od:', 'Písmo...',
  // Image button, bullet character, Bullet button,
  'Obrázek...', 'Znak odrážky', 'Odrážka...',
  // Position of list text, bullet, number, image, text
  'Umístění textu', 'Umístění odrážky', 'Umístění čísla', 'Umístění obrázku', 'Umístění písma',
  // at, left indent, first line indent, from left indent
  'na:', 'Odsazení zleva:', 'Odsazení prvního řádku:', 'od odsazení zleva',
  // [only] one level preview, preview
  'Náhled jedné úrovně', 'Náhled',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'zarovnat vlevo', 'zarovnat vpravo', 'zarovnat na střed',
  // level #, this level (for combo-box)
  'Úroveň %d', 'Tato úroveň',
  // Marker character dialog title
  'Změna znaku odrážky',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Ohraničení a pozadí odstavce', 'Ohraničení', 'Pozadí',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Nastavení', 'Barva:', 'Šířka:', 'Šířka rámu:', 'Odsazení textu...',
  // Sample, Border type group-boxes;
  'Náhled', 'Typ ohraničení',
  // Border types: none, single, double, tripple, thick inside, thick outside
  'žádné', 'jednoduché', 'dvojité', 'trojité', 'silné uvnitř', 'silné vně',
  // Fill color group-box; More colors, padding buttons
  'Výplň', 'Více barev...', 'Odsazení textu...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Odsazení textu', 'Odsazení textu',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Odstavec', 'Zarovnání', 'vlevo', 'vpravo', 'na střed', 'do bloku',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Mezery', 'Před:', 'Za:', 'Řádkování:', 'Násobky:',
  // Indents group-box; indents: left, right, first line
  'Odsazení', 'Zleva:', 'Zprava:', 'První řádek:',
  // indented, hanging
  'odsazený', 'předsazený', 'Náhled',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'jednoduché', '1,5 řádku', 'dvojité', 'nejméně', 'přesně', 'násobky',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Odsazení a mezery', 'Zarážky', 'Tok textu',
  // tab stop position label; buttons: set, delete, delete all
  '&Pozice zarážky:', '&Nastavit', '&Smazat', 'Smazat &vše',
  // tab align group; left, right, center aligns,
  'Zarovnání', '&Vlevo', 'Vp&ravo', '&Na střed',
  // leader radio-group; no leader
  'Vodicí znak', '(Žádný)',
  // tab stops to be deleted, delete none, delete all labels
  'Vymazat tyto zarážky:', '(Žádné)', 'Vše',
  // Pagination group-box, keep with next, keep lines together
  'Stránkování', 'Svázat s &následujícím', '&Svázat řádky',
  // Outline level, Body text, Level #
  'Úroveň osnovy:', 'Tělo textu', 'Úroveň %d',    
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Náhled před tiskem', 'Šířka stránky', 'Celá stránka', 'Stránka:',
  // Invalid Scale, [page #] of #
  'Prosím vložte číslo od 10 do 500', 'z %d',
  // Hints on buttons: first, prior, next, last pages
  'První stránka', 'Předchozí stránka', 'Následující stránka', 'Poslední stránka',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Odsazení buňky', 'Mezery', '&Před buňkou', 'Od rámu tabulky po buňku',
  // vertical, horizontal (x2)
  '&Vertikálně:', '&Horizontálně:', 'V&ertikálně:', 'H&orizontálně:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Ohraničení', 'Nastavení', 'Barva:', 'Barva světla:', 'Barva stínu:',
  // Width; Border type group-box;
  'Šířka:', 'Typ ohraničení',
  // Border types: none, sunken, raised, flat
  'žádné', 'vtlačené', 'vystouplé', 'plovoucí',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Rozdělení buňky', 'Rozdělit na',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Určení počtu sloupců a řádků', 'Originální buňky',
  // number of columns, rows, merge before
  'Počet sloupců:', 'Počet řádků:', 'Sloučit před rozdělením',
  // to original cols, rows check-boxes
  'Rozdělit na originální sloupce', 'Rozdělit na originální řádky',
  // Add Rows form -------------------------------------------------------------
  'Vložit řádky', 'Počet řádků:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Vzhled stránky', 'Stránka', 'Záhlaví a zápatí',
  // margins group-box, left, right, top, bottom
  'Okraje (v milimetrech)', 'Okraje (v palcích)', 'V&levo:', '&Nahoře:', 'V&pravo:', '&Dole:',
  // mirror margins check-box
  '&Zrcadlové okraje',
  // orientation group-box, portrait, landscape
  'Orientace', 'Na &výšku', 'Na &šířku',
  // paper group-box, paper size, default paper source
  'Papír', 'V&elikost:', 'Zd&roj:',
  // header group-box, print on the first page, font button
  'Záhlaví', '&Text:', '&Záhlaví na první stránce', '&Písmo...',
  // header alignments: left, center, right
  'v&levo', 'na &střed', 'vp&ravo',
  // the same for footer (different hotkeys)
  'Zápatí', 'T&ext:', 'Z&ápatí na první stránce', 'P&ísmo...',
  '&vlevo', '&na střed', 'vp&ravo',
  // page numbers group-box, start from
  'Číslování stránek', 'Z&ačít od:',
  // hint about codes
  'Kombinace speciálních znaků:'#13'&&p - číslo stránky; &&P - počet stránek; &&d - aktuální datum; &&t - aktuální čas.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Znaková stránka', '&Vyberte kódování souboru:',
  // thai, japanese, chinese (simpl), korean
  'Thajština', 'Japonština', 'Čínština (zjednodušená)', 'Korejština',
  // chinese (trad), central european, cyrillic, west european
  'Čínština (tradiční)', 'Střední a východní Evropa', 'Azbuka', 'Západní Evropa',
  // greek, turkish, hebrew, arabic
  'Řečtina', 'Turečtina', 'Hebrejština', 'Arabština',
  // baltic, vietnamese, utf-8, utf-16
  'Baltské jazyky', 'Vietnamština', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vizuální styl', '&Vybrat styl:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Převést na text', 'vybrat &oddělovač:',
  // line break, tab, ';', ','
  'konec řádku', 'tabulátor', 'středník', 'čárka',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabulka obsahující sloučené řádky nemůže být seřazena',
  // title, main options groupbox
  'Seřadit tabulku', 'Seřadit',
  // sort by column, case sensitive
  '&Seřadit podle sloupce:', '&Rozlišovat malá a velká',
  // heading row, range or rows
  'Kromě řádku záhlaví', 'Řádky k seřazení: od %d po %d',
  // order, ascending, descending
  'Pořadí', '&Vzestupně', '&Sestupně',
  // data type, text, number
  'Typ', '&Text', '&Číslo',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Vložit číslo', 'Vlastnosti', '&Název počítadla:', '&Typ číslování:',
  // numbering groupbox, continue, start from
  'Číslování', 'P&okračovat', '&Začít od:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Popisek', '&Štítek:', '&Vyloučit štítek z popisku',
  // position radiogroup
  'Pozice', '&Nad vybraným objektem', '&Pod vybraným objektem',
  // caption text
  'Text &popisku:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Číslování', 'Číslo', 'Tabulka',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Viditelné strany okrajů', 'Okraj',
  // Left, Top, Right, Bottom checkboxes
  '&Levá strana', '&Horní strana', '&Pravá strana', '&Dolní strana',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Změnit velikost sloupce tabulky', 'Změnit velikost řádku tabulky',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Odsazení prvního řádku', 'Odsazení zleva', 'Předsazení prvního řádku', 'Odsazení zprava',
  // Hints on lists: up one level (left), down one level (right)
  'O úroveň výš', 'O úroveň níž',
  // Hints for margins: bottom, left, right and top
  'Spodní okraj', 'Levý okraj', 'Pravý okraj', 'Horní okraj',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Levá zarážka', 'Pravá zarážka', 'Zarážka na střed', 'Desetinná zarážka',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normální', 'Normální odsazený', 'Bez mezer', 'Nadpis %d', 'Odstavec se seznamem',
  // Hyperlink, Title, Subtitle
  'Hypertextový odkaz', 'Nadpis', 'Podnadpis',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Zdůraznění', 'Zdůraznění – jemné', 'Zdůraznění – intenzivní', 'Silné',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citace', 'Citace – intenzivní', 'Odkaz – jemný', 'Odkaz – intenzivní',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Text v bloku', 'Proměnná HTML', 'Kód HTML', 'Akronym HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definice HTML', 'Klávesnice HTML', 'Vzorek HTML', 'Psací stroj HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Formátovaný v HTML', 'Citát HTML', 'Záhlaví', 'Zápatí', 'Číslo stránky',
  // Caption
  'Popisek',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Odkaz na vysvětlivky', 'Odkaz na poznámku pod čarou', 'Text vysvětlivky', 'Text poznámky pod čarou',
  // Sidenote Reference, Sidenote Text
  'Poznámka na okraj', 'Text poznámky na okraj',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'barva', 'barva pozadí', 'průhledná', 'výchozí', 'barva podtržení',
  // default background color, default text color, [color] same as text
  'výchozí barva pozadí', 'výchozí barva textu', 'stejná jako text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'jednoduchá', 'silná', 'dvojitá', 'tečkovaná', 'silná tečkovaná', 'přerušovaná',
  // underline types: thick dashed, long dashed, thick long dashed,
  'silná přerušovaná', 'dlouhá čárkovaná', 'silná dlouhá čárkovaná',
  // underline types: dash dotted, thick dash dotted,
  'tečka a čára', 'silná tečka a čára',
  // underline types: dash dot dotted, thick dash dot dotted
  'čára tečka tečka', 'silná čára tečka tečka',
  // sub/superscript: not, subsript, superscript
  'žádný dolní/horní index', 'dolní index', 'horní index',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'obousměrný režim:', 'dědičný', 'zleva doprava', 'zprava doleva',
  // bold, not bold
  'tučné', 'ne tučné',
  // italic, not italic
  'kurzíva', 'ne kurzíva',
  // underlined, not underlined, default underline
  'podtržené', 'ne podtržené', 'výchozí podtržené',
  // struck out, not struck out
  'přeškrtnuté', 'ne přeškrtnuté',
  // overlined, not overlined
  'nadtržené', 'ne nadtržené',
  // all capitals: yes, all capitals: no
  'všechna velká', 'vypnout všechna velká',
  // svislý posun: žádný, o x% nahoru, o x% dolů
  'bez svislého posunu', 'posunuto o %d%% nahoru', 'posunuto o %d%% dolů',
  // characters width [: x%]
  'šířka znaků',
  // character spacing: none, expanded by x, condensed by x
  'normální proložení znaků', 'proložení rozšířeno o %s', 'proložení zúženo o %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'výchozí písmo', 'výchozí odstavec', 'dědičný',
  // [hyperlink] highlight, default hyperlink attributes
  'zvýraznit', 'výchozí',
  // paragraph aligmnment: left, right, center, justify
  'zarovnaný doleva', 'zarovnaný doprava', 'zarovnaný na střed', 'zarovnaný do bloku',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'výška řádku: %d%%', 'mezera mezi řádky: %s', 'výška řádku: nejméně %s',
  'line height: exactly %s',
  // no wrap, wrap
  'zalomení řádků vypnuto', 'zalomení řádků',
  // keep lines together: yes, no
  'svázat řádky', 'nesvazovat řádky',
  // keep with next: yes, no
  'svázat s následujícím odstavcem', 'nesvazovat s následujícím odstavcem',
  // read only: yes, no
  'pouze pro čtení', 'editovatelný',
  // body text, heading level x
  'úroveň osnovy: základní text', 'úroveň osnovy: %d',
  // indents: first line, left, right
  'odsazení prvního řádku', 'levé odsazení', 'pravé odsazení',
  // space before, after
  'mezera před', 'mezera po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulátory', 'žádné', 'zarovnat', 'doleva', 'doprava', 'na střed',
  // tab leader (filling character)
  'vodicí znak',
  // no, yes
  'ne', 'ano',
  // [padding/spacing/side:] left, top, right, bottom
  'levé', 'horní', 'pravé', 'dolní',
  // border: none, single, double, triple, thick inside, thick outside
  'žádné', 'jednoduché', 'dvojité', 'trojité', 'silné uvnitř', 'silné vně',
  // background, border, padding, spacing [between text and border]
  'pozadí', 'ohraničení', 'odsazení obsahu', 'mezery',
  // border: style, width, internal width, visible sides
  'styl', 'šířka', 'vnitřní šířka', 'viditelné strany',
  // style inspector -----------------------------------------------------------
  // title
  'Kontrola stylů',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Styl', '(Žádný)', 'Odstavec', 'Písmo', 'Atributy',
  // border and background, hyperlink, standard [style]
  'Ohraničení a pozadí', 'Hypertextový odkaz', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Styly', 'Styl',
  // name, applicable to,
  '&Název:', 'Platí &pro:',
  // based on, based on (no &),
  '&Založeno na:', 'založeno na:',
  //  next style, next style (no &)
  'Styl pro &následující odstavec:', 'Styl pro následující odstavec:',
  // quick access check-box, description and preview
  '&Rychlý přístup', 'Popis a &náhled:',
  // [applicable to:] paragraph and text, paragraph, text
  'odstavec a text', 'odstavec', 'text',
  // text and paragraph styles, default font
  'Styly textu a odstavce', 'Výchozí písmo',
  // links: edit, reset, buttons: add, delete
  'Upravit', 'Resetovat', '&Přidat', '&Smazat',
  // add standard style, add custom style, default style name
  'Přidat &standardní styl...', 'Přidat &vlastní styl', 'Styl %d',
  // choose style
  'Vybrat &styl:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Styly RichView (*.rvst)|*.rvst', 'Chyba při otevírání souboru', 'Tento soubor neobsahuje žádné styly',
  // Title, group-box, import styles
  'Import stylů ze souboru', 'Import', 'I&mportovat styly:',
  // existing styles radio-group: Title, override, auto-rename
  'Existující styly', '&přepsat', '&přidat přejmenované',
  // Select, Unselect, Invert,
  '&Vybrat', '&Zušit výběr', '&Invertovat',
  // select/unselect: all styles, new styles, existing styles
  '&Všechny styly', '&Nové styly', '&Existující styly',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Vymazat formátování)', '(Všechny styly)',
  // dialog title, prompt
  'Styly', '&Vybrat styl k použití:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Synonyma', '&Ignorovat vše', '&Přidat do slovníku',
  // Progress messages ---------------------------------------------------------
  'Stahuji %s', 'Připravuji k tisku...', 'Tisknu stránku %d',
  'Konvertuji z RTF...',  'Konvertuji do RTF...',
  'Načítám %s...', 'Zapisuji %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Žádná poznámka', 'Poznámka pod čarou', 'Poznámka na konec', 'Poznámka na okraj', 'Textové pole'
  );
    
    
initialization
  RVA_RegisterLanguage(
    'Czech', // english language name, do not translate
    'Čeština', // native language name
    EASTEUROPE_CHARSET, @Messages);
    
end.
