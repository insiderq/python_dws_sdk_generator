unit RVALocRuler;

interface

uses RVRuler, RVALocalize, RichViewActions;

procedure RVALocalizeRuler(Ruler: TRVRuler; ControlPanel: TRVAControlPanel=nil);

implementation

procedure RVALocalizeRuler(Ruler: TRVRuler; ControlPanel: TRVAControlPanel);
begin
  if ControlPanel=nil then
    ControlPanel := MainRVAControlPanel;
  with Ruler.RulerTexts do begin
    HintColumnMove := RVA_GetS(rvam_ruler_ColumnMove, ControlPanel);
    HintRowMove := RVA_GetS(rvam_ruler_RowMove, ControlPanel);
    HintIndentFirst := RVA_GetS(rvam_ruler_FirstIndent, ControlPanel);
    HintIndentLeft := RVA_GetS(rvam_ruler_LeftIndent, ControlPanel);
    HintIndentHanging := RVA_GetS(rvam_ruler_HangingIndent, ControlPanel);
    HintIndentRight := RVA_GetS(rvam_ruler_RightIndent, ControlPanel);

    HintLevelDec := RVA_GetS(rvam_ruler_listlevel_Dec, ControlPanel); // moves left = Promote One Level
    HintLevelInc := RVA_GetS(rvam_ruler_listlevel_Inc, ControlPanel); // moves right = Demote One Level

    HintMarginBottom := RVA_GetS(rvam_ruler_MarginBottom, ControlPanel);
    HintMarginLeft := RVA_GetS(rvam_ruler_MarginLeft, ControlPanel);
    HintMarginRight := RVA_GetS(rvam_ruler_MarginRight, ControlPanel);
    HintMarginTop := RVA_GetS(rvam_ruler_MarginTop, ControlPanel);

    HintTabLeft := RVA_GetS(rvam_ruler_tab_Left, ControlPanel);
    HintTabCenter := RVA_GetS(rvam_ruler_tab_Center, ControlPanel);
    HintTabRight := RVA_GetS(rvam_ruler_tab_Right, ControlPanel);
    HintTabDecimal := '-'; // RVA_GetS(rvam_ruler_tab_Decimal);
    HintTabWordBar := '-'; // RVA_GetS(rvam_ruler_tab_WordBar);

    MenuTabLeft := RVA_GetS(rvam_ruler_tab_Left, ControlPanel);
    MenuTabCenter := RVA_GetS(rvam_ruler_tab_Center, ControlPanel);
    MenuTabRight := RVA_GetS(rvam_ruler_tab_Right, ControlPanel);
    MenuTabDecimal := '-'; // RVA_GetS(rvam_ruler_tab_Decimal);
    MenuTabWordBar := '-'; // RVA_GetS(rvam_ruler_tab_WordBar);
  end;
end;

end.
