
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for inserting hyperlink                  }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit HypRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, RVStr,
  Dialogs, StdCtrls, RVALocalize, BaseRVFrm, RVStyle, RVEdit, RVStyleFuncs;

type
  TfrmRVHyp = class(TfrmRVBase)
    gb: TGroupBox;
    btnOk: TButton;
    btnCancel: TButton;
    lblText: TLabel;
    txtText: TEdit;
    lblTarget: TLabel;
    txtTarget: TEdit;
    btnHypProp: TButton;
    lblStyle: TLabel;
    cmbStyle: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure txtTargetChange(Sender: TObject);
    procedure btnHypPropClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    _btnOk: TControl;
    procedure UpdateOKButton;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _txtText, _txtTarget, _cmbStyle, _lblStyle, _btnHypProp: TControl;
    TextColor, HoverColor, BackColor, UnderlineColor,
    HoverBackColor, HoverUnderlineColor: TColor;
    Underline: Boolean;
    Effects: TRVHoverEffects;
    procedure Localize; override;
    procedure Init(rve: TCustomRichViewEdit
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}; StyleTemplate: TRVStyleTemplate=nil{$ENDIF});
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation

uses HypPropRVFrm;

{$R *.dfm}

procedure TfrmRVHyp.UpdateOKButton;
begin
  _btnOk.Enabled := not _txtText.Enabled or
    ((Trim(GetEditText(_txtTarget))<>'')  and (GetEditText(_txtText)<>''));
  if _txtText.Enabled then
    SetControlCaption(_btnOk, RVA_GetS(rvam_btn_Insert))
  else if Trim(GetEditText(_txtTarget))<>'' then
    SetControlCaption(_btnOk, RVA_GetS(rvam_btn_OK))
  else
    SetControlCaption(_btnOk, RVA_GetS(rvam_btn_Remove))
end;

procedure TfrmRVHyp.FormActivate(Sender: TObject);
begin
  UpdateOKButton;
end;


procedure TfrmRVHyp.txtTargetChange(Sender: TObject);
begin
  UpdateOKButton;
end;

procedure TfrmRVHyp.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_hl_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_hl_GBTitle, ControlPanel);
  lblText.Caption := RVA_GetSAsIs(rvam_hl_Text, ControlPanel);
  lblTarget.Caption := RVA_GetSAsIs(rvam_hl_Target, ControlPanel);
  btnHypProp.Caption := RVA_GetSAsIs(rvam_hl_HypProperties, ControlPanel);
  lblStyle.Caption := RVA_GetSAsIs(rvam_hl_HypStyle, ControlPanel);
end;

procedure TfrmRVHyp.btnHypPropClick(Sender: TObject);
var frm: TfrmRVHypProp;
    Index: Integer;
begin
  frm := TfrmRVHypProp.Create(Application, ControlPanel);
  try
    frm.cmbText.ChosenColor          := TextColor;
    frm.cmbTextBack.ChosenColor      := BackColor;
    frm.cmbUnderlineColor.ChosenColor := UnderlineColor;
    frm.cmbHoverText.ChosenColor     := HoverColor;
    frm.cmbHoverTextBack.ChosenColor := HoverBackColor;
    frm.cmbHoverUnderlineColor.ChosenColor := HoverUnderlineColor;

    if Underline then
      Index := 0
    else if rvheUnderline in Effects then
      Index := 2
    else
      Index := 1;
    frm.SetXBoxItemIndex(frm._cmbUnderline, Index);
    if frm.ShowModal=mrOk then begin
      TextColor      := frm.cmbText.ChosenColor;
      BackColor      := frm.cmbTextBack.ChosenColor;
      UnderlineColor := frm.cmbUnderlineColor.ChosenColor;      
      HoverColor     := frm.cmbHoverText.ChosenColor;
      HoverBackColor := frm.cmbHoverTextBack.ChosenColor;
      HoverUnderlineColor := frm.cmbHoverUnderlineColor.ChosenColor;
      case frm.GetXBoxItemIndex(frm._cmbUnderline) of
        0:
          begin
            Underline := True;
            Exclude(Effects, rvheUnderline)
          end;
        1:
          begin
            Underline := False;
            Exclude(Effects, rvheUnderline)
          end;
        2:
          begin
            Underline := False;
            Include(Effects, rvheUnderline)
          end;
      end;
    end;
  finally
    frm.Free;
  end;
end;

procedure TfrmRVHyp.FormCreate(Sender: TObject);
begin
  _btnOk := btnOk;
  _btnHypProp := btnHypProp;
  _txtText := txtText;
  _txtTarget := txtTarget;
  _cmbStyle := cmbStyle;
  _lblStyle := lblStyle;
  inherited;
end;

function TfrmRVHyp.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gb.Left*2+gb.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gb.Top-gb.Height);
  Result := True;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVHyp.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = btnOk then
    _btnOk := NewControl
  else if OldControl = _txtText then
    _txtText := NewControl
  else if OldControl = _txtTarget then
    _txtTarget := NewControl
  else if OldControl = _cmbStyle then
    _cmbStyle := NewControl
  else if OldControl = _lblStyle then
    _lblStyle := NewControl
  else if OldControl = _btnHypProp then
    _btnHypProp := NewControl;
end;
{$ENDIF}

procedure TfrmRVHyp.Init(rve: TCustomRichViewEdit
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}; StyleTemplate: TRVStyleTemplate{$ENDIF});
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
var SL: TRVALocStringList;
    Index: Integer;
{$ENDIF}
begin
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if rve.UseStyleTemplates then begin
    _btnHypProp.Visible := False;
    SL := TRVALocStringList.Create;
    try
      RVStyleTemplatesToStrings(rve.Style.StyleTemplates, SL, True,
        [rvstkText], nil, False, ControlPanel);
      SL.Sort;
      Index := RVFindStyleTemplateInStrings(SL, StyleTemplate);
      SetXBoxItems(_cmbStyle, SL);
      SetXBoxItemIndex(_cmbStyle, Index);
    finally
      SL.Free;
    end;
    end
  else
  {$ENDIF}
  begin
    _lblStyle.Visible := False;
    _cmbStyle.Visible := False;
  end;

end;

end.
