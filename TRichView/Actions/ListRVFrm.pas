
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for displaying menu                      }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit ListRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  {$IFDEF USERVTNT}
  TntStdCtrls,
  {$ENDIF}
  Dialogs, BaseRVFrm, StdCtrls, Clipbrd, RVALocalize, RVEdit;

type
  TfrmRVList = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    lst: TListBox;
    lbl: TLabel;
    procedure lstDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FItemHeight: Integer;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _btnOk, _btnCancel, _lbl: TControl;
    _lst: TControl;
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation
uses RVStr, RichViewActions;

{$R *.dfm}

{ TfrmRVList }

function TfrmRVList.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := _lst.Left*2+_lst.Width;
  Height := _btnOk.Top+_btnOk.Height+(_btnOk.Top-_lst.Top-_lst.Height);
  Result := True;
end;

procedure TfrmRVList.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
end;

procedure TfrmRVList.lstDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVList.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _btnOk then
    _btnOk := NewControl
  else if OldControl = _btnCancel then
    _btnCancel := NewControl
  else if OldControl = _lst then
    _lst := NewControl
  else if OldControl = lbl then
    _lbl := NewControl;
end;
{$ENDIF}

procedure TfrmRVList.FormCreate(Sender: TObject);
begin
  _btnOk := btnOk;
  _btnCancel := btnCancel;
  _lst := lst;
  _lbl := lbl;
  FItemHeight := lst.ItemHeight;
  inherited;
end;

procedure TfrmRVList.FormShow(Sender: TObject);
var ContentHeight: Integer;
begin
  ContentHeight := GetXBoxItemCount(_lst)+1;
  if ContentHeight<5 then
    ContentHeight := 5;
  ContentHeight := FItemHeight*ContentHeight;
  if ContentHeight<_lst.ClientHeight then
    Height := Height - _lst.ClientHeight+ContentHeight;
  inherited;
end;

end.