﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVAPopupActionBar.pas' rev: 27.00 (Windows)

#ifndef RvapopupactionbarHPP
#define RvapopupactionbarHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.ActnPopup.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvapopupactionbar
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVAPopupActionBar;
typedef void __fastcall (__closure *TRVALiveSpellGetSuggestionsEvent)(TRVAPopupActionBar* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);

typedef void __fastcall (__closure *TRVALiveSpellIgnoreAllEvent)(TRVAPopupActionBar* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);

typedef void __fastcall (__closure *TRVALiveSpellAddEvent)(TRVAPopupActionBar* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);

typedef void __fastcall (__closure *TRVALiveSpellReplaceEvent)(TRVAPopupActionBar* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);

class PASCALIMPLEMENTATION TRVAPopupActionBar : public Vcl::Actnpopup::TPopupActionBar
{
	typedef Vcl::Actnpopup::TPopupActionBar inherited;
	
private:
	Richviewactions::TRVAPopupMenuHelper* FPopupMenuHelper;
	TRVALiveSpellGetSuggestionsEvent FOnLiveSpellGetSuggestions;
	TRVALiveSpellAddEvent FOnLiveSpellAdd;
	TRVALiveSpellIgnoreAllEvent FOnLiveSpellIgnoreAll;
	TRVALiveSpellReplaceEvent FOnLiveSpellWordReplace;
	Richviewactions::TRVAControlPanel* __fastcall GetControlPanel(void);
	void __fastcall SetControlPanel(Richviewactions::TRVAControlPanel* Value);
	
protected:
	System::Classes::TComponent* __fastcall GetPopupComponent(void);
	Vcl::Actnlist::TCustomActionList* __fastcall GetActionList(void);
	void __fastcall SetActionList(Vcl::Actnlist::TCustomActionList* const Value);
	int __fastcall GetMaxSuggestionsCount(void);
	void __fastcall SetMaxSuggestionsCount(int Value);
	void __fastcall ClearItems(void);
	void __fastcall AddActionItem(Vcl::Actnlist::TAction* Action, const System::UnicodeString Caption = System::UnicodeString());
	void __fastcall AddSeparatorItem(void);
	void __fastcall DeleteLastSeparatorItem(void);
	System::UnicodeString __fastcall GetItemCaption(System::TObject* Sender);
	int __fastcall GetItemIndex(System::TObject* Sender);
	void __fastcall AddClickItem(System::Classes::TNotifyEvent Click, const System::UnicodeString Caption, System::Uitypes::TFontCharset Charset, bool Enabled, bool Default);
	bool __fastcall WantLiveSpellGetSuggestions(void);
	void __fastcall LiveSpellGetSuggestions(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);
	void __fastcall LiveSpellWordReplace(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);
	bool __fastcall WantLiveSpellIgnoreAll(void);
	void __fastcall LiveSpellIgnoreAll(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	bool __fastcall WantLiveSpellAdd(void);
	void __fastcall LiveSpellAdd(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	
public:
	__fastcall virtual TRVAPopupActionBar(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVAPopupActionBar(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Popup(int X, int Y);
	
__published:
	__property Vcl::Actnlist::TCustomActionList* ActionList = {read=GetActionList, write=SetActionList};
	__property TRVALiveSpellGetSuggestionsEvent OnLiveSpellGetSuggestions = {read=FOnLiveSpellGetSuggestions, write=FOnLiveSpellGetSuggestions};
	__property TRVALiveSpellIgnoreAllEvent OnLiveSpellIgnoreAll = {read=FOnLiveSpellIgnoreAll, write=FOnLiveSpellIgnoreAll};
	__property TRVALiveSpellAddEvent OnLiveSpellAdd = {read=FOnLiveSpellAdd, write=FOnLiveSpellAdd};
	__property TRVALiveSpellReplaceEvent OnLiveSpellWordReplace = {read=FOnLiveSpellWordReplace, write=FOnLiveSpellWordReplace};
	__property int MaxSuggestionsCount = {read=GetMaxSuggestionsCount, write=SetMaxSuggestionsCount, default=0};
	__property Richviewactions::TRVAControlPanel* ControlPanel = {read=GetControlPanel, write=SetControlPanel};
private:
	void *__IRVAPopupMenu;	// Richviewactions::IRVAPopupMenu 
	
public:
	operator Richviewactions::IRVAPopupMenu*(void) { return (Richviewactions::IRVAPopupMenu*)&__IRVAPopupMenu; }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvapopupactionbar */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVAPOPUPACTIONBAR)
using namespace Rvapopupactionbar;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvapopupactionbarHPP
