﻿// This file is a copy of RVAL_Bul.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Bulgarian translation                           }
{                                                       }
{       Translation (c) AbilixSoft 2014           }
{       trichview@borsabg.com                           }
{       http://www.MarketWebBuilder.com                 }
{                                                       }
{*******************************************************}
{ Updated: 16.2.2014                                   }
{*******************************************************}

unit RVALUTF8_Bul;
{$I RV_Defs.inc}
interface

implementation

uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'инчове', 'см', 'мм', 'пики', 'пиксели', 'точки',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Файл', '&Редактиране', 'Ф&орматиране', '&Шрифт', 'Пара&граф', '&Вмъкване', '&Таблица', '&Прозорец', 'Помо&щ',
  // exit
  'Из&ход',
  // top-level menus: View, Tools,
  '&Изглед', 'И&нструменти',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Размер', 'Стил', 'Маркиране', 'Подравняване в клетка', 'Кантове на клетка',
  // menus: Table cell rotation
  'Завъртане на клетките', 
  // menus: Text flow, Footnotes/endnotes
  '&Изливане на текста', '&Бележка под черта',
  // ribbon tabs: tab1, tab2, view, table
  '&Основни', '&Допълнителни', '&Изглед', '&Таблица',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Клипборд', 'Шрифт', 'Параграф', 'Списък', 'Редактиране',
  // ribbon groups: insert, background, page setup,
  'Вмъкване', 'Фон', 'Настройка на страница',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Връзки', 'Бележка под черта', 'Изглед на документа', 'Показване/скриване', 'Мащаб', 'Настройки',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Вмъкване', 'Изтриване', 'Действия', 'Кантове',
  // ribbon groups: styles 
  'Стилове',
  // ribbon screen tip footer,
  'Натиснете F1 за повече информация',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Последно отваряни документи', 'Запазване на копие от документа', 'Преглед и печат',
  // ribbon label: units combo
  'Мерни единици:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Нов', 'Нов|Създаване на нов документ',
  // TrvActionOpen
  '&Отваряне...', 'Отваряне|Отваряне на документ',
  // TrvActionSave
  '&Запис', 'Запис|Запис на текущия документ',
  // TrvActionSaveAs
  'Запис като...', 'Запис като...|Запис на текущия документ под ново име', //'Save As...|Saves the document to disk with a new name',
  // TrvActionExport
  '&Експортиране...', 'Експортиране|Експортиране на текущия документ в друг формат', //'Export|Exports the document to another format',
  // TrvActionPrintPreview
  'Преглед преди печат', 'Преглед преди печат|Показване на документа както ще изглежда при отпечатване', //'Print Preview|Shows the document as it will be printed',
  // TrvActionPrint
  '&Печат...', 'Печат|Определяне настройките на печат и отпечатване на документа', //'Print|Changes printing settings and prints the document',
  // TrvActionQuickPrint
  '&Печат', '&Бърз печат', 'Печат|Отпечатване на текущия документ',
  // TrvActionPageSetup
  'Настройка на страницата...', 'Настройка на страницата|Задаване на полета, размер на страница, ориентация, горен и долен колонтитул',
  // TrvActionCut
  'Изрязване', 'Изрязване|Изрязване на марикраното и запазване в клипборда', //'Cut|Cuts the selection and puts it on the Clipboard',
  // TrvActionCopy
  'Копиране', 'Копиране|Копира маркираното в клипборда', //'Copy|Copies the selection and puts it on the Clipboard',
  // TrvActionPaste
  'Поставяне', 'Поставяне|Вмъкване на съдържанието от клипборда', //.'Paste|Inserts the Clipboard contents',
  // TrvActionPasteAsText
  'Поставяне като текст', 'Поставяне като текст|Вмъкване на съдържанието от клипборда като текст', //'Paste as Text|Inserts text from the Clipboard',
  // TrvActionPasteSpecial
  'Специално поставяне...', 'Специално поставяне|Вмъкване на съдържанието от клипборда като избран формат',
  // TrvActionSelectAll
  'Маркиране на всичко', 'Маркиране на всичко|Маркиране на цялото съдържание на документа', //'Select All|Selects all document',
  // TrvActionUndo
  'Отмяна', 'Отмяна|Отмяна на последното действие', //'Undo|Reverts the last action',
  // TrvActionRedo
  'Възстановяване', 'Възстановяване|Повтаряне на последното отменено действие', //'Redo|Redoes the last undone action',
  // TrvActionFind
  'Търсене...', 'Търсене|Търсене на избран текст в документа', //'Find|Finds specified text in the document',
  // TrvActionFindNext
  'Повторно търсене', 'Повторно търсене|Повторно търсене при същите критерии напред в документа',
  // TrvActionReplace
  'Замяна...', 'Замяна|Търсене и замяна на избран текст в документа', //'Replace|Finds and replaces specified text in the document',
  // TrvActionInsertFile
  '&Файл...', 'Вмъкване на файл|Вмъкване съдържанието на файл в текущия документ', //'Insert File|Inserts contents of file in the document',
  // TrvActionInsertPicture
  '&Изображение...', 'Вмъкване на изображение|Вмъкване на изображение в документа',
  // TRVActionInsertHLine
  'Хори&зонтална линия', 'Вмъкване на хоризонтална линия|Вмъкване на хоризонтална линия',
  // TRVActionInsertHyperlink
  '&Хипервръзка...', 'Вмъкване на хипервръзка|Вмъкване на хипервръзка',
  // TRVActionRemoveHyperlinks
  'Премахване на хипервръзки', 'Премахване на хипервръзки|Премахване на всички хипервръзки от маркирания текст',
  // TrvActionInsertSymbol
  '&Специален символ...', 'Вмъкване на специален символ|Вмъкване на специален символ',
  // TrvActionInsertNumber
  '&Число...', 'Вмъкни число|Вмъква число',
  // TrvActionInsertCaption
  'Вмъкни &Заглавие...', 'Вмъкни Заглавие|Вмъква заглавие за избрания обект',
  // TrvActionInsertTextBox
  '&Текстов блок', 'Вмъкане на текстов блок|Вмъква текстов блок(кутийка)',
  // TrvActionInsertSidenote
  '&Бележка встрани', 'Вмъква бележка встрани|Вмъква бележка в текстов блок(кутийка)',
  // TrvActionInsertPageNumber
  '&Номер на страница', 'Вмъкване на номер на страница|Вмъкване на номер на страница',
  // TrvActionParaList
  '&Списък и номерация...', 'Списък и номерация|Задаване или промяна на символ за списък и номерация за параграф',
  // TrvActionParaBullets
  'Списък', 'Списък|Задаване или промяна на символ за списък за параграф',
  // TrvActionParaNumbering
  '&Номерация', 'Номерация|Задаване или промяна на номерация за параграф',
  // TrvActionColor
  'Фонов цвят на документа...', 'Фонов цвят на документа|Избор на фонов цвят на документа',
  // TrvActionFillColor
  '&Фонов цвят...', 'Фонов цвят|Избор на фонов цвят за текст, абзац, клетка, таблица или документ',
  // TrvActionInsertPageBreak
  'Вмъкване на разделител на страница', 'Вмъкване на разделител на страница|Вмъкване на разделител на страница',
  // TrvActionRemovePageBreak
  'Премахване на разделител на страница', 'Премахване на разделител на страница|Премахване на разделител на страница',
  // TrvActionClearLeft
  'Без изливане на текста от &лявата страна', 'Без изливане на текста от лявата страна|Преместване на параграфа под всяко изображение подравнено в ляво',
  // TrvActionClearRight
  'Без изливане на текста от &дясната страна', 'Без изливане на текста от дясната страна|Преместване на параграфа под всяко изображение подравнено в дясно',
  // TrvActionClearBoth
  'Без изливане на текста от д&вете страна', 'Без изливане на текста от двете страни|Преместване на параграфа под всяко изображение подравнено в ляво или дясно',
  // TrvActionClearNone
  '&Нормално изливането на текста', 'Нормално изливането на текста|Позволява изливането на съдържанието на параграфа около всяко ляво или дясно подравнено изображение',
  // TrvActionVAlign
  'Позициониране на &обекта...', 'Позициониране на обекта|Позициониране на избрания обект',
  // TrvActionItemProperties
  'Параметри на обект...', 'Параметри...|Параметри на избрания обект',
  // TrvActionBackground
  '&Фон...', 'Фон|Избор на цвят или изображение за фон',
  // TrvActionParagraph
  '&Параграф...', 'Параграф|Настойки на параграфа',
  // TrvActionIndentInc
  '&Увеличаване на отстъпа', 'Увеличаване на отстъпа|Увеличаване отстъпа на маркираните параграфи отляво', //'Increase Indent|Increases left indent of the selected paragraphs',
  // TrvActionIndentDec
  '&Намаляване на отстъпа', 'Намаляване на отстъпа|Намаляване отстъпа на маркираните параграфи отляво', //'Decrease Indent|Dereases left indent of the selected paragraphs',
  // TrvActionWordWrap
  '&Пренасяне на редове', 'Пренасяне|Превключване на пренасянето на редове за избрания параграф',
  // TrvActionAlignLeft
  'Ляво подравняване', 'Ляво подравняване|Подравняване на маркирания текст отляво', //'Align Left|Aligns the selected text to the left',
  // TrvActionAlignRight
  'Дясно подравняване', 'Дясно подравняване|Подравняване на маркирания текст отдясно', //'Align Right|Aligns the selected text to the right',
  // TrvActionAlignCenter
  'Центриране', 'Центриране|Центриране на маркирания текст', //'Align Center|Centers the selected text',
  // TrvActionAlignJustify
  'Двустранно подравняване', 'Двустранно подравняване|Подравняване на маркирания текст от ляво и дясно', //'Justify|Aligns the selected text to both left and right sides',
  // TrvActionParaColor
  'Фонов цвят на параграф...', 'Фонов цвят на параграф|Задаване на фонов цвят на параграф', //'Paragraph Background Color|Sets background color of paragraphs',
  // TrvActionLineSpacing100
  'Единично междуредие', 'Единично междуредие|Задаване на единично междуредие',
  // TrvActionLineSpacing150
  '1.5 междуредие', '1.5 междуредие|Задаване на 1.5 реда междуредие',
  // TrvActionLineSpacing200
  'Двойно междуредие', 'Двойно междуредие|Задаване на двойно междуредие',
  // TrvActionParaBorder
  'Кантове и фон на параграф...', 'Кантове и фон на параграф|Задаване на кантове и фон на параграф',
  // TrvActionInsertTable
  '&Вмъкване на таблица...', '&Таблица', 'Вмъкване на таблица|Вмъкване на нова таблица',
  // TrvActionTableInsertRowsAbove
  'Вмъкване на ред отгоре', 'Вмъкване на ред отгоре|Вмъкване на нов ред на маркираните клетки', //'Insert Row Above|Inserts a new row above the selected cells',
  // TrvActionTableInsertRowsBelow
  'Вмъкване на ред отдолу', 'Вмъкване на ред отдолу|Вмъкване на нов ред под маркираните клетки', //'Insert Row Below|Inserts a new row below the selected cells',
  // TrvActionTableInsertColLeft
  'Вмъкване на колона отляво', 'Вмъкване на колона отляво|Вмъкване на нова колона отляво на маркираните клетки', //'Insert Column Left|Insert a new column to the left of selected cells',
  // TrvActionTableInsertColRight
  'Вмъкване на колона отдясно', 'Вмъкване на колона отдясно|Вмъкване на нова колона отдясно на маркираните клетки', //'Insert Column Right|Insert a new column to the right of selected cells',
  // TrvActionTableDeleteRows
  'Изтриване на редове', 'Изтриване на редове|Изтриване на редовете с маркирани клетки', //'Delete Rows|Deletes rows with the selected cells',
  // TrvActionTableDeleteCols
  'Изтриване на колони', 'Изтриване на колони|Изтриване на колоните с маркирани клетки', //'Delete Columns|Deletes columns with the selected cells',
  // TrvActionTableDeleteTable
  'Изтриване на таблица', 'Изтриване на таблица|Изтриване на таблицата',
  // TrvActionTableMergeCells
  'Сливане на клетки', 'Сливане на клетки|Сливане на маркираните клетки',
  // TrvActionTableSplitCells
  'Разделяне на клетки...', 'Разделяне на клетки|Разделяне на маркираните клетки',
  // TrvActionTableSelectTable
  'Маркиране на таблица', 'Маркиране на таблица|Маркиране на цялата таблицата',
  // TrvActionTableSelectRows
  'Маркиране на редове', 'Маркиране на редове|Маркиране на редове',
  // TrvActionTableSelectCols
  'Маркиране на колони', 'Маркиране на колони|Маркиране на колони',
  // TrvActionTableSelectCell
  'Маркиране на клетка', 'Маркиране на клетка|Маркиране на клетка',
  // TrvActionTableCellVAlignTop
  'Подравняване на клетка горе', 'Подравняване на клетка горе|Подравняване съдържанието на клетката отгоре',  //'Align Cell to the Top|Aligns cell contents to the top',
  // TrvActionTableCellVAlignMiddle
  'Подравняване на клетка в средата', 'Подравняване на клетка в средата|Подравняване съдържанието на клетката в средата', //'Align Cell to the Middle|Aligns cell contents to the middle',
  // TrvActionTableCellVAlignBottom
  'Подравняване на клетка долу', 'Подравняване на клетка долу|Подравняване съдържанието на клетката отдолу', //'Align Cell to the Bottom|Aligns cell contents to the bottom',
  // TrvActionTableCellVAlignDefault
  'Вертикално подравняване на клетка по подразбиране', 'Вертикално подравняване на клетка по подразбиране|Задаване на вертикално подравняване по подразбиране за маркираните клетки',
  // TrvActionTableCellRotationNone
  '&Без завъртане', 'Без завъртане на клетката|Завъртане на клетката с 0°',
  // TrvActionTableCellRotation90
  'Завъртане на клетката с &90°', 'Завъртане на клетката на 90°|Завърта съдържанието на 90°',
  // TrvActionTableCellRotation180
  'Завъртане на клетката с &180°', 'Завъртане на клетката на 180°|Завърта съдържанието на 180°',
  // TrvActionTableCellRotation270
  'Завъртане на клетката с &270°', 'Завъртане на клетката на 270°|Завърта съдържанието на 270°',
  // TrvActionTableProperties
  'Параметри на таблица...', 'Параметри на таблица|Промяна параметрите на маркираната таблица',
  // TrvActionTableGrid
  'Показване на &водещи линии', 'Показване на водещи линии|Показване или скриване на водещи линии',
  // TRVActionTableSplit
   'Раз&деляне на таблица', 'Разделяне на таблицата|Разделяне на таблицата на 2 от избранят ред нататък',
  // TRVActionTableToText
  'Конвертиране към текст...', 'Конвертиране към текст|Конвертиране на таблицата във текст',
  // TRVActionTableSort
  '&Сортиране...', 'Сортиране|Сортиране на редовете в таблицата',
  // TrvActionTableCellLeftBorder
  'Ляв кант', 'Ляв кант|Показване или скриване на левия кант на клетката',
  // TrvActionTableCellRightBorder
  'Десен кант', 'Десен кант|Показване или скриване на десния кант на клетката',
  // TrvActionTableCellTopBorder
  'Горен кант', 'Горен кант|Показване или скриване на горния кант на клетката',
  // TrvActionTableCellBottomBorder
  'Долен кант', 'Долен кант|Показва или скрива долния кант на клетката',
  // TrvActionTableCellAllBorders
  'Всички кантове', 'Всички кантове|Показване или скриване на всички кантове на клетката', //'All Borders|Shows or hides all cell borders',
  // TrvActionTableCellNoBorders
  'Без кантове', 'Без кантове|Скриване на всички кантове на клетката',
  // TrvActionFonts & TrvActionFontEx
  '&Шрифт...', 'Шрифт|Промяна на шрифта и атрибутите на маркирания текст',
  // TrvActionFontBold
  '&Удебелен', 'Удебелен|Промяна на атрибута "Удебелено" за избрания текст',
  // TrvActionFontItalic
  '&Курсив', 'Курсив|Промяна на атрибута "Курсив" за избрания текст',
  // TrvActionFontUnderline
  '&Подчертан', 'Подчертан|Промяна на атрибута "Подчертан" за избрания текст',
  // TrvActionFontStrikeout
  '&Засчертан', 'Зачертан|Промяна на атрибута "Зачертан" за избрания текст',
  // TrvActionFontGrow
  '&Уголемяване на шрифта', 'Уголемяване на шрифта|Уголемяване на избрания текст с 10%',
  // TrvActionFontShrink
  '&Смаляване на шрифта', 'Смаляване на шрифта|Смаляване на шрифта на избрания текст с 10%',
  // TrvActionFontGrowOnePoint
  'Уголемяване на шрифта', 'Уголемяване на шрифта|Уголемяване на шрифта на избрания текст с 1 размер',
  // TrvActionFontShrinkOnePoint
  'Смаляване на шрифта', 'Смаляване на шрифта|Смаляване на шрифта на избрания текст с 1 размер',
  // TrvActionFontAllCaps
  'Главни букви', 'Главни букви|Промяна на всички букви от маркирания текст в главни', //'All Capitals|Changes all characters of the selected text to capital letters',
  // TrvActionFontOverline
  'Линия над текст', 'Линия над текст|Добавяне на линия над маркирания текст', //'Overline|Adds line over the selected text',
  // TrvActionFontColor
  'Цвят на текста...', 'Цвят на текста|Избиране цвета на маркирания текст',
  // TrvActionFontBackColor
  'Фонов цвят на текста...', 'Фонов цвят на текста|Избиране цвета на фона на маркирания текст',
  // TrvActionAddictSpell3
  'Проверка за правопис', 'Проверка за правопис|Проверяване на текста за правопис', // 'Spell Check|Checks the spelling',
  // TrvActionAddictThesaurus3
  '&Синонимен речник', 'Синонимен речник|Предлагане на синоними за избраната дума',
  // TrvActionParaLTR
  'Отляво на&дясно', 'Отляво надясно|Задаване посока на текста "отляво надясно" за избраните параграфи',
  // TrvActionParaRTL
  'Отдясно на&ляво', 'Отдясно наляво|Задаване посока на текста "отдясно наляво" за избраните параграфи',
  // TrvActionLTR
  'Отляво надясно', 'Отляво надясно|Задаване посока на текста "отляво надясно" за избрания текст',
  // TrvActionRTL
  'Отдясно наляво', 'Отдясно наляво|Задаване посока на текста "отдясно наляво" за избрания текст',
  // TrvActionCharCase
  'Регистър на буквите', 'Регистър на буквите|Смяна на ГОРЕН/долен регистър за избрания текст',
  // TrvActionShowSpecialCharacters
  'Непечатаеми символи', 'Непечатаеми символи|Показване/скриване на непечатаемите символи като край на параграф, табулация, интервал',
                      //'Non-printing Characters|Shows or hides non-printing characters, such as paragraph marks, tabs and spaces',
  // TrvActionSubscript
  'Долен индекс', 'Долен индекс|Промяна на избрания текст в долен индекс',
  // TrvActionSuperscript
  'Горен индекс', 'Горен индекс|Промяна на избрания текст в горен индекс',
  // TrvActionInsertFootnote
  '&Бележка под черта', 'Бележка под черта|Вмъкване на бележка под черта',
  // TrvActionInsertEndnote
  '&Бележка под черта към документ', 'Бележка под черта към документ|Вмъкване на бележка под черта към документ',
  // TrvActionEditNote
  'Р&едактиране на бележка под черта', 'Редактиране на бележка под черта|Превключва в режим на редактиране на бележка под черта',
  // TrvActionHide
  '&Скриване', 'Скриване|Скриване или показване на маркирания текст',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Стилове...', 'Стилове|Отваряне на мениджъра на стилове',
  // TrvActionAddStyleTemplate
  '&Добавяне на стил...', 'Добавяне на стил|Създава нов стил за текст или абзац',
  // TrvActionClearFormat,
  'Без &форматиране', 'Премахване на форматирането|Премахване на форматирането на текста и параграфите в избраният фрагмент',
  // TrvActionClearTextFormat,
  'Без форматиране на &текст', 'Премахване на форматирането на текста|Премахване на форматирането на текста в избраният фрагмент',
  // TrvActionStyleInspector
  '&Инспектор за стилове', 'Инспектор за стилове|Показване или скриване на инспектора за стилове',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Отказ', 'Затваряне', 'Вмъкване', '&Отваряне...', '&Запис...', '&Изчистване', 'Помощ', 'Премахване',
  // Others  -------------------------------------------------------------------
  // percents
  'проценти',
  // left, top, right, bottom sides
  'Отляво', 'Отгоре', 'Отдясно', 'Отдолу',
  // save changes? confirm title
  'Запазване на промените в %s?', 'Потвърждение',
  // warning: losing formatting
  '%s може да съдържа настройки, които не могат да бъдат запазени в избрания файлов формат.'#13+
  'Желатете ли този формат да бъде използван?',
  // RVF format name
  'Формат RichView',
  // Error messages ------------------------------------------------------------
  'Грешка',
  'Грешка при зареждане на файл.'#13#13'Възможни причини:'#13'- форматът на файла не се поддържа от това приложение;'#13+
  '- файлът е повреден;'#13'- файлът се използва и е заключен от друго приложение.',
      {  'Error loading file.'#13#13'Possible reasons:'#13'- format of this file is not supported by this application;'#13+
        '- the file is corrupted;'#13'- the file is opened and locked by another application.',  }
  'Грешка при зареждане на файла с изображение.'#13#13'Възможни причини:'#13'- форматът не е поддържан от това приложение;'#13+
  '- файлът не съдържа изображение;'#13+
  '- файлът е повреден;'#13'- файлът се използва и е заключен от друго приложение.',
  'Грешка при запазване на файла.'#13#13'Възможни причини:'#13'- няма достатъчно свободно място;'#13+
  '- устройстовто за съхранение е защитено от запис;'#13'- носителят (дискета, USB устройство) не е поставен;'#13+
  '- файлът се използва и е заключен от друго приложение.;'#13'- устройстовто за съхранение е повредено.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView формати (*.rvf)|*.rvf',  'RTF файлове(*.rtf)|*.rtf' , 'XML файлове(*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Текстови формати(*.txt)|*.txt', 'Unicode текстови файлове(*.txt)|*.txt', 'Автоматично откриване на избраният текстов файл (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML файлове (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - опростен (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Документ на Microsoft Word (*.docx)|*.docx',  
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Търсенето приключи', 'Търсеният текст ''%s'' не е намерен.',
  // 1 string replaced; Several strings replaced
  'Направена е една замяна.', 'Направени са %d замени.',
  // continue search from the beginning/end?
  'Търсенето достигна края на документа. Желаете ли да се продължи от началото?',
  'Търсенето достигна началото на документа. Желаете ли да се продължи от края?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Прозрачно', 'Автоматично',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Черно', 'Кафяво', 'Маслиненио зелено', 'Тъмно зелено', 'тъмно тюркоазено', 'Тъмно синьо', 'Мастилено синьо', 'Сиво-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Тъмно червено', 'Оранжево', 'Тъмно жълто', 'Зелено', 'Тюркоазено', 'Синьо', 'Синьо-сиво', 'Сиво-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Червено', 'Светло оранжево', 'Лимонено ', 'Морско синьо', 'Водно синьо', 'Светло синьо', 'Виолетово', 'Сиво-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Розово', 'Златно', 'Жълто', 'Ярко зелено', 'Тюркоазено', 'Небесно синьо', 'Тъмно виолетово', 'Сиво-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Розово', 'Жълто-кафяво', 'Светло жълто', 'Светло зелено', 'Светло тюркоазено', 'Бледо синьо', 'Бледо лилаво', 'Бяло',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Про&зрачно', '&Автоматично', '&Още цветове...', '&По подразбиране',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Фон', 'Цвят:', 'Позиция', 'Фон', 'Примерен текст',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Неопределена', 'На цял &екран', '&Фиксирано повтаряща се', '&Повтаряща се', '&Центрирана',
  // Padding button
  '&Отстояние...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Запълващ цвят', '&Прилагане на:', '&Още цветове...', '&Отстъп...',
  'Моля, изберете цвят',
  // [apply to:] text, paragraph, table, cell
  'текст', 'параграф' , 'таблица', 'клетка',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Шрифт', 'Шрифт', 'Разположение',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Шрифт:', '&Размер', '&Стил', 'Удебелен', 'Курсив',
  // Script, Color, Back color labels, Default charset
  'Индекс:', '&Цвят:', '&Фон:', '(По подразбиране)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Ефекти', 'Подчертан', 'Линия над текст', 'Зачертан', 'Главни букви',
  // Sample, Sample text
  'Пример', 'Примерен текст',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Разстояние между символите', '&Разстояние между символите:', 'Раздалечени', 'Сгъстени',
  // Offset group-box, Offset label, Down, Up,
  'Вертикално отместване', '&Отместване:', 'Надолу', 'Нагоре',
  // Scaling group-box, Scaling
  'Мащаб', 'Мащаб:',
  // Sub/super script group box, Normal, Sub, Super
  'Горен и долен индекс', 'Нормален', 'Долен индекс', 'Горен индекс',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Отстояние', 'Отгоре:', 'Отляво:', 'Отдолу:', 'Отдясно:', '&Еднакви отстояния',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Вмъкване на хипервръзка', 'Хипервръзка', 'Текст:', 'Адрес', '<<избран текст>>',
  // cannot open URL
  'Адресът "%s" не може да бъде отворен',
  // hyperlink properties button, hyperlink style
  '&Параметри...', '&Стил:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) сolors
  'Параметри на връзката', 'Цвят на текста', 'Цвят на посочена връзка',
  // Text [color], Background [color]
  '&Текст:', '&Фон',
  // Underline [color]
  '&Подчертаване:', 
  // Text [color], Background [color] (different hotkeys)
  'Т&екст:', 'Ф&он',
  // Underline [color], Attributes group-box,
  '&Подчертаване:', 'Атрибути',
  // Underline type: always, never, active (below the mouse)
  '&Подчертаване:', 'винаги', 'никога', 'при активиране',
  // Same as normal check-box
  'Като &нормална връзка',
  // underline active links check-box
  'По&дчертаване на активните връзки',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Вмъкване на специален символ', 'Шрифт:', '&Кодова таблица:', 'Unicode',
  // Unicode block
  '&Подмножество:',
  // Character Code, Character Unicode Code, No Character
  'Код на символа: %d', 'Код на символа: Unicode %d', '(не е символ)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Вмъкване на таблица', 'Размер на таблица', 'Брой колони:', 'Брой редове:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Разположение на таблицата', 'Автоматично оразмеряване', 'По размера на &прозореца', 'По &зададен размер',
  // Remember check-box
  'Запомняне на размерите за нови таблици',
  // VAlign Form ---------------------------------------------------------------
  'Вертикално разположение',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Параметри', 'Изображение', 'Позициониране и размер', 'Линия', 'Таблица', 'Редове', 'Клетки',
  // Image Appearance, Image Misc, Number tabs
  'Изглед', 'Разни', 'Номер',
  // Box position, Box size, Box appearance tabs
  'Позиция', 'Размер', 'Изглед',
  // Preview label, Transparency group-box, checkbox, label
  'Преглед:', 'Прозрачност', 'Прозрачно', 'Прозрачно:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Промяна...', '&Запис...', 'Автоматично',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Вертикално подравняване', 'Подравняване:',
  'на долният край по основата на текста', 'на средата по основата на текста',
  'на горният край по горния край на реда', 'на долният край по долния край на реда', 'на средата по средата на реда',
  // align to left side, align to right side
  'по лявата страна', 'по дясната страна',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Отместване:', 'Разпъване', 'Ширина:', 'Височина:', 'Размер по подразбиране: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Пропорционално мащабиране', '&Нулиране',
  // Inside the border, border, outside the border groupboxes
  'В канта', 'Кант', 'Извън канта',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Запълващ цвят:',  '&Отстояние:',
  // Border width, Border color labels
  '&Ширина на канта:', '&Цвят на канта:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Хоризонтално отстояние:', '&Вертикално отстояние:',
  // Miscellaneous groupbox, Tooltip label
  'Разни','&Подсказки:',
  // web group-box, alt text
  'Web', 'Алтернативен &текст:',
  // Horz line group-box, color, width, style
  'Хоризонтална линия', 'Цвят:', 'Ширина:', 'Стил:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Таблица', 'Ширина:', '&Цвят:', 'Разстояние между клетки...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Хоризонтален отстъп в клетките:', '&Вертикален отстъп в клетките:', 'Рамка и фон',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Видими страни на &канта...', 'Автоматично', 'автоматично',
  // Keep row content together checkbox
  '&Задържане на съдържанието заедно',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Завъртане', '&0°', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Кант:', 'Видими страни на канта...',
  // Border, CellBorders buttons
  'Кант на таблица...', 'Кант на клетка...',
  // Table border, Cell borders dialog titles
  'Кант на таблица', 'Кант на клетка по подразбиране',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Отпечатване', '&Забрана за пренасяне на редове между страниците', 'Брой &заглавни редове:',
  'Заглавните редове в таблицата се повтарят на всяка страница',
  // top, center, bottom, default
  'Отгоре', 'В средата', 'Отдолу', 'По подразбиране',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Параметри', 'Оптимална ширина:', 'Оптимална височина:', 'Фонов цвят:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Кант', 'Видими кантове:',  'Цвят за сянка:', 'Светъл цвят', 'Цвят за канта:',
  // Background image
  'Изображение...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Хоризонтално позициониране', 'Вертикално позициониране', 'Позициониране в текста',
  // Position types: align, absolute, relative
  'Подравняване', 'Абсолютно позициониране', 'Относително позициониране',
  // [Align] left side, center, right side
  'Лявата страна на блока по центъра',
  'Центъра на блока по центъра',
  'Дясната страна на блока по центъра',
  // [Align] top side, center, bottom side
  'Горната страна на блока по горната страна',
  'Центъра  на блока по центъра',
  'Долната страна на блока по долната страна',
  // [Align] relative to
  'спрямо',
  // [Position] to the right of the left side of
  'вдясно от лявата страна на',
  // [Position] below the top side of
  'Под горната страна на',
  // Anchors: page, main text area (x2)
  '&Страница', '&Главен текстови регион', 'С&траница', 'Г&лавен текстови регион',
  // Anchors: left margin, right margin
  'Отстъп от&ляво', 'Отстъп от&дясно',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Горно отстояние', '&Долно отстояние', 'Въ&трешно отстояние', 'Въ&ншно отстояние',
  // Anchors: character, line, paragraph
  '&Символ', '&Ред', '&Параграф',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&Огледални отстъпи', 'Ра&зположение в клетката на таблица',
  // Above text, below text
  'Текст &над', 'Текст &под',
  // Width, Height groupboxes, Width, Height labels
  'Ширина', 'Височина', '&Ширина:', '&Височина:',
  // Auto height label, Auto height combobox item
  'Автоматично', 'Автоматично',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Подравняване по вертикала', '&Горе', '&Център', '&Долу',
  // Border and background button and title
  'Рам&ка и фон...', 'Рамка и фон на блока',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Специално поставяне', 'Поставяне като:', '"Rich text" формат (rtf)', 'HTML формат (HTML)',
  'Текст', 'Unicode текст', 'BMP изображение', 'Метафайл (WMF) изображение',
  'Графични файлове', 'URL',
  // Options group-box, styles
  'Опции', '&Стилове:',
  // style options: apply target styles, use source styles, ignore styles
  'прилагане на стиловете към целевия документ', 'запазване на стиловете и вида',
  'запазване на вида, игнориране на стиловете',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Списъци и номерация', 'Списъци', 'Номерация', 'Избор на символ за списък', 'Избор на номерация',
  // Customize, Reset, None
  'Нас&тройка..', '&Начални настр.', {?}'Премахване',
  // Numbering: continue, reset to, create new
  'продължаване на номерацията', 'задаване на номерацията от', 'създаване на нов списък, започващ с',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Главни букви', 'Главни римски цифри', 'Десетични числа', 'Малки букви', 'Малки римски цифри',
  // Bullet type
  'Кръг', 'Диск', 'Квадрат',
  // Level, Start from, Continue numbering, Numbering group-box
  'Ниво:', 'Начало от:', 'Продължаване на номерацията', 'Номерация',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Настройки...', 'Нива', 'Бр. нива:', 'Настройки на списъка', 'Вид списък:',
  // List types: bullet, image
  'списък', 'изображение', 'Вмъкване на номер|',
  // Number format, Number, Start level from, Font button
  'Формат на номерацията:', 'Номер', 'Начало на номерацията от:', 'Шрифт...',
  // Image button, bullet character, Bullet button,
  'Изображение...', 'Символ за списък', 'Списък...',
  // Position of list text, bullet, number, image, text
  'Позиция на текста', 'Позиция на символа', 'Позиция на номера', 'Позиция на изображението', 'Позиция на текста',
  // at, left indent, first line indent, from left indent
  '&от:', 'Ляв отстъп:', 'Отстъп на първия ред:', 'от левия отстъп',
  // [only] one level preview, preview
  'Преглед на едно ниво', 'Преглед',
  // Preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'ляво подравняване', 'дясно подравняване', 'в средата',
  // level #, this level (for combo-box)
  'Ниво %d', 'ниво',
  // Marker character dialog title
  'Избор на символ за списък',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Кантове и фон на параграф', 'Кант', 'Фон',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Настройки', 'Цвят:', 'Ширина:', 'Вътр. ширина:', 'Отстoяния...',
  // Sample, Border type group-boxes;
  'Пример', 'Вид кант',
  // Border types: none, single, double, tripple, thick inside, thick outside
  'няма', 'единичен', 'двоен', 'троен', 'удебелен &отвътре', 'удебелен о&твън',
  // Fill color group-box; More colors, padding buttons
  'Фонов цвят', '&Още цветове...', '&Отстояние...',
  // preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // title and group-box in Offsets dialog
  'Отстoяния за текст', 'Отстояния за кант',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Параграф', 'Подравняване', 'От&ляво', 'От&дясно', 'В &средата', 'Дв&устр.',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Разредка', '&Преди:', '&След:', 'Междуредие:', '',
  // Indents group-box; indents: left, right, first line
  'Отстъп', 'Отляво:', 'Отдясно:', 'Първи ред:',
  // indented, hanging
  '&Навътре', '&Висящ', 'Пример',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Единично', '1.5 междуредие', 'Двойно', 'Поне', 'Фиксирано', 'Множител',
  // preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Отстъпи и разредка', 'Табулатори', '&Изливане на текста',
  // tab stop position label; buttons: set, delete, delete all
  'Позиция на табулаторите:', 'Задаване', 'Изтриване', 'Изтриване всички',
  // tab align group; left, right, center aligns,
  'Подравняване', 'Отляво', 'Отдясно', 'В средата',
  // leader radio-group; no leader
  'Запълващ табулатора символ', '(Няма)',
  // tab stops to be deleted, delete none, delete all labels
  'Табулатори за изтриване:', '(Няма)', 'Всички.',
  // Pagination group-box, keep with next, keep lines together
  'Страниране', 'Заедно със следващия параграф', 'Без разделяне на параграфа',
  // Outline level, Body text, Level #
  '&Ниво на отместване:', 'Текст', 'Ниво %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Преглед преди печат', 'Ширина на страница', 'Цяла страница', 'Страници:',
  // Invalid Scale, [page #] of #
  'Моля, въведете число от 10 до 500', 'от %d',
  // Hints on buttons: first, prior, next, last pages
  'Първа страница', 'Предишна страница', 'Следваща страница', 'Последна страница',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Разстояние между клетките', 'Разстояние', 'Между &клетките', 'Между кантовете на таблицата и клетките',
  // vertical, horizontal (x2)
  '&Вертикално:', '&Хоризонтално:', 'В&ертикално:', 'Х&оризонтално:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Кантове', 'Настройки', 'Цвят:', 'Светъл цвят:', 'Сянка:',
  // Width; Border type group-box;
  'Дебелина:', 'Вид кант',
  // Border types: none, sunken, raised, flat
  'Няма', '&Вдлъбнат', '&Изпъкнал', '&Равен',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Разделяне', 'Разделяне на',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Определяне на броя на редовете и колоните', '&Оригиналният брой клетки',
  // number of columns, rows, merge before
  'Брой колони:', 'Брой редове:', 'Обединяване на клетките преди разделяне',
  // to original cols, rows check-boxes
  'Разделяне до оригиналните &колони', 'Разделяне до оригиналните &редове',
  // Add Rows form -------------------------------------------------------------
  'Добавяне на редове', 'Брой редове:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Настройка на страница', 'Страница', 'Горен и долен колонтитул',
  // margins group-box, left, right, top, bottom
  'Разстояния (милиметри)', 'Разстояния (инчове)', 'Отляво:', 'Отгоре:', 'Отдясно:', 'Отдолу:',
  // mirror margins check-box
  '&Огледални отстъпи',
  // orientation group-box, portrait, landscape
  'Ориентация на страницата', 'Портрет', 'Пейзаж',
  // paper group-box, paper size, default paper source
  'Листи', 'Размер:', 'Вх. &тава:',
  // header group-box, print on the first page, font button
  'Горен колонтитул', 'Текст:', 'Горен колонтитул на първата страница', 'Шрифт...',
  // header alignments: left, center, right
  'Отляво', 'В средата', 'Отдясно',
  // the same for footer (different hotkeys)
  'Долен колонтитул', 'Текст:', 'Долен колонтитул на първата страница', 'Шрифт...',
  'Отляво', 'В средата', 'Отдясно',
  // page numbers group-box, start from
  'Номериране на страниците', 'Начало от:',
  // hint about codes
  'Специални символи:'#13'&&p - номер на страница; &&P - брой страници; &&d - днешна дата; &&t - час.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Кодировка на файла', '&Избор на кодировка(енкодинг):',
  // thai, japanese, chinese (simpl), korean
  'Тайландски', 'Японски', 'Китайски (Опростен)', 'Корейски',
  // chinese (trad), central european, cyrillic, west european
  'Китайски (традиционен)', 'Централна Европа', 'Кирилица', 'Западна Европа',
  // greek, turkish, hebrew, arabic
   'Гръцки', 'Турски', 'Иврит', 'Арабски',
  // baltic, vietnamese, utf-8, utf-16
  'Балтийски', 'Виетнамски', 'Unicode (UTF-8)', 'Unicode (UTF-16)', //Unicode is used as-is in Bulgarian AFAIK
  // Style form ----------------------------------------------------------------
  'Стил на изгледа', '&Избор на стил:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Конвертиране към текст', 'Избор на &разделител:',
  // line break, tab, ';', ','
  'нов ред', 'табулация', 'точка и запетая', 'запетая',
  // Table sort form -----------------------------------------------------------
  // error message
  'Таблица, която съдържа сляти колони не може да бъде сортирана.',
  // title, main options groupbox
  'Сортиране ан таблица', 'Сортиране',
  // sort by column, case sensitive
  '&Сортиране по колона:', '&Сортиране, с главни/малки букви',
  // heading row, range or rows
  'Без &заглавната колона', 'Редове за сортиране %d  до %d',
  // order, ascending, descending
  'Подреждане', '&Възходящо', '&Низходящо',
  // data type, text, number
  'Тип', '&Текст', '&Число',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Вмъкване на номер', 'Свойства', 'Име на &брояча:', 'Тип &номерация:',
  // numbering groupbox, continue, start from
  'Номерация', '&Продължение', '&Започване от:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Заглавие', '&Етикет:', '&Изключване на етикета от заглавието',
  // position radiogroup
  'Позиция', '&Над избрания обект', '&Под избрания обект',
  // caption text
  '&Текст на заглавието:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Номерация', 'Фигура', 'Таблица',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Видими страни на рамката', 'Рамка',
  // Left, Top, Right, Bottom checkboxes
  '&Лява страна', '&Горна страна', '&Дясна страна', 'Д&олна страна',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Преоразмеряване на колона', 'Преоразмеряване на ред',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Отстъп на първия ред', 'Отстъп отляво', 'Висящ отстъп', 'Отстъп отдясно',
  // Hints on lists: up one level (left), down one level (right)
  'Повишаване с едно ниво', 'Понижаване с едно ниво',
  // Hints for margins: bottom, left, right and top
  'Разстояние отдолу', 'Разстояние отляво', 'Разстояние отдясно', 'Разстояние отгоре',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Ляво подравняваща табулация', 'Дясно подравняваща табулация', 'Табулация подравняваща в средата', 'Табулация спрямо десетичната запетая',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Нормален', 'Нормален отстъп', 'Без междуредие', 'Хединг %d', 'Списъчен параграф' {TODO: check again},
  // Hyperlink, Title, Subtitle
  'Хипервръзка', 'Заглавие', 'Подзаглавие',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Ударение', 'Малко ударение', 'Голямо ударение', 'Strong',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Кавички', 'Големи кавички', 'Subtle Reference', 'Intense Reference', //TODO
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Текстов блок', 'HTML Variable', 'HTML Code', 'HTML Acronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Дефиниция', 'HTML Клавиатура', 'HTML Пример', 'HTML Тайпрайтър',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML преформатирано', 'HTML цитат', 'Хедър', 'Футер', 'Номер на страница',
  // Caption
  'Заглавие',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Заглавие на бележка след текста', 'Заглавие на бележка под линия', 'Съдържание на бележка след текста', 'Съдържание на бележка под линия',
  // Sidenote Reference, Sidenote Text
  'Препратка на старнична бележка', 'Странична бележка',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'цвят', 'фонов цвят', 'прозрачен', 'по подразбиране', 'цвят на подчертаването',
  // default background color, default text color, [color] same as text
   'фонов цвят по подразбиране', 'цвят на текста по подразбиране', 'също като на текста',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'единично', 'дебело', 'двойно', 'точки', 'дебели точки', 'тирета',
  // underline types: thick dashed, long dashed, thick long dashed,
  'дебели тирета', 'дълги тирета', 'дебели, дълги тирета',
  // underline types: dash dotted, thick dash dotted,
  'тире точка', 'дебело тире точка',
  // underline types: dash dot dotted, thick dash dot dotted
  'тире точка точка', 'дебело тире точка точка',
  // sub/superscript: not, subsript, superscript
  'без индекс', 'долен индекс', 'горен индекс',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'посока на текста:', 'наследена', 'ляво на дясно', 'дясно на ляво',
  // bold, not bold
  'получер', 'без получер',
  // italic, not italic
  'курсив', 'без курсив',
  // underlined, not underlined, default underline
  'подчертаване', 'без подчертаване', 'подчертаване по премълчаване',
  // struck out, not struck out
   'зачертаване', 'без зачертаване', {TODO: to check}
  // overlined, not overlined
   'зачертаване', 'без зачертаване',
  // all capitals: yes, all capitals: no
  'главни букви', 'без главни букви',
  // vertical shift: none, by x% up, by x% down
  'без вертикално изместване', 'изместено с %d%% нагоре', 'изместено с %d%% надолу',
  // characters width [: x%]
  'широчина на буквите',
  // character spacing: none, expanded by x, condensed by x
  'нормално разстояние между символите', 'разстояние между символите + %s', 'разстояние между символите - %s',
  // charset
  'кодировка',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'шрифт по подразбиране', 'абзац по подразбиране', 'наследено',
  // [hyperlink] highlight, default hyperlink attributes
  'осветяване', 'по подразбиране',
  // paragraph aligmnment: left, right, center, justify
  'ляво подравнен', 'дясно подравнен', 'центриран', 'двустранно',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'височина на реда: %d%%', 'междуредие: %s', 'височина на реда: минимум %s',
  'line height: exactly %s',
  // no wrap, wrap
  'забранено пренасяне на редовете', 'пренасяне на редовете',
  // keep lines together: yes, no
  'запазване на редовете заедно', ' без запазване на редовете заедно',
  // keep with next: yes, no
  'запазване заедно със следващия абзац', 'без запазване заедно със следващия абзац',
  // read only: yes, no
  'само за четене', 'за редактиране',
  // body text, heading level x
  'ниво на отместване: основен текст', 'ниво на отместване: %d',
  // indents: first line, left, right
   'отстъп на първия ред', 'отстъп отляво', 'отстъп отдясно',
  // space before, after
  'интервал преди', 'интервал след',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'табове', 'без', 'пподравняване', 'ляво', 'дясно', 'центрирано',
  // tab leader (filling character)
  'запълващ символ',
  // no, yes
  'не', 'да',
  // [padding/spacing/side:] left, top, right, bottom
  'отляво', 'отгоре', 'отдясно', 'отдолу',
  // border: none, single, double, triple, thick inside, thick outside
  'без', 'единична', 'двойна', 'тройна', 'удебелена отвътре', 'удебелена отвън',
  // background, border, padding, spacing [between text and border]
  'фон', 'кант', 'отстъп', 'разстояние',
  // border: style, width, internal width, visible sides
  'стил', 'ширина', 'вътрешна ширина', 'видими страни',
  // style inspector -----------------------------------------------------------
  // title
  'Инспектор на стилове',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Стил', '(Без)', 'Параграф', 'Шрифт', 'Атрибути',
  // border and background, hyperlink, standard [style]
  'Кант и фон', 'Хипервръзка', '(Стандартно)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Стилове', 'Стил',
  // name, applicable to,
  '&Име:', 'Приложим &към:',
  // based on, based on (no &),
  '&Бзиран на:', 'Базиран на:',
  //  next style, next style (no &)
  'Стил за &следващият параграф:', 'Стил за следващият параграф:', //TODO: следващият или следният?
  // quick access check-box, description and preview
  'Б&ърз достъп', 'Описание и &преглед:',
  // [applicable to:] paragraph and text, paragraph, text
  'текст и параграф', 'параграф', 'текст',
  // text and paragraph styles, default font
  'Стилове за текст и параграф', 'Шрифт по подразбиране',
  // links: edit, reset, buttons: add, delete
  'Редактиране', 'Нулиране', '&Добавяне', 'И&зтриване',
  // add standard style, add custom style, default style name
  'Добавяне на &Стандартен стил...', 'Добавяне на &Потребителски стил', 'Стил %d',
  // choose style
  'Избор на &стил:',
  // import, export,
  'И&мпорт...', '&Експорт...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Стилове (*.rvst)|*.rvst', 'Грешка при четене на файла', 'Файлът не съдържа стилове',
  // Title, group-box, import styles
  'Импорт на стилове от файл', 'Импорт', '&Импорт на стилове:',
  // existing styles radio-group: Title, override, auto-rename
  'Налични стилове', '&презаписване', '&преименуване',
  // Select, Unselect, Invert,
  '&Избор', '&Премахване на избора', '&Обръщане на избора',
  // select/unselect: all styles, new styles, existing styles
  '&Добавяне на стилове', '&Нови стилове', '&Налични стилове',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Без форматиране', 'Всички стилове',
  // dialog title, prompt
  'Стилове', '&Избор на стил за прилагане:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  'Синоними', 'Игнориране на всички', 'Добавяне в речника',
  // Progress messages ---------------------------------------------------------
  // 'Downloading %s', 'Preparing for printing...', 'Printing the page %d',
   'Изтегляне на %s', 'Приготовления за печат...', 'Печат на страница %d',
  //'Converting from RTF...',  'Converting to RTF...',
  'Конвертиране от RTF...',  'Конвертиране към RTF...',
  //'Reading %s...', 'Writing %s...', 'text'
  'Четене на %s...', 'Запис на %s...', 'текст',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Без бележка', 'Бележка под линия', 'Бележка след текста', 'Бележка встрани', 'Текстов блок'
  );


initialization
  RVA_RegisterLanguage(
    'Bulgarian', // english language name, do not translate
    'Български', // native language name
    RUSSIAN_CHARSET, @Messages);

end.