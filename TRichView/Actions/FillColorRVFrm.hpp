﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'FillColorRVFrm.pas' rev: 27.00 (Windows)

#ifndef FillcolorrvfrmHPP
#define FillcolorrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorGrid.hpp>	// Pascal unit
#include <ColorRVFrm.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVGrids.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Fillcolorrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVFillColor;
class PASCALIMPLEMENTATION TfrmRVFillColor : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TLabel* lblColor;
	Rvcolorgrid::TRVColorGrid* rvcg;
	Vcl::Stdctrls::TButton* btnColor;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TLabel* lbl;
	Vcl::Stdctrls::TComboBox* cmb;
	Vcl::Dialogs::TColorDialog* ColorDialog1;
	Vcl::Stdctrls::TButton* btnPadding;
	void __fastcall btnColorClick(System::TObject* Sender);
	void __fastcall rvcgColorChange(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall btnOkClick(System::TObject* Sender);
	void __fastcall cmbClick(System::TObject* Sender);
	void __fastcall btnPaddingClick(System::TObject* Sender);
	void __fastcall FormActivate(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _btnPadding;
	System::Uitypes::TColor FChosenColor;
	bool Updating;
	Rvstyle::TRVStyleLength PaddingLeft;
	Rvstyle::TRVStyleLength PaddingTop;
	Rvstyle::TRVStyleLength PaddingRight;
	Rvstyle::TRVStyleLength PaddingBottom;
	bool FPaddingLeft;
	bool FPaddingTop;
	bool FPaddingRight;
	bool FPaddingBottom;
	Vcl::Dialogs::TColorDialog* __fastcall GetCD(void);
	void __fastcall SetChosenColor(System::Uitypes::TColor Value);
	
protected:
	DYNAMIC Vcl::Stdctrls::TButton* __fastcall GetOkButton(void);
	
public:
	Vcl::Controls::TControl* _cmb;
	Vcl::Controls::TControl* _lblColor;
	int LblColorRight;
	Vcl::Dialogs::TColorDialog* ColorDialog;
	Rvstyle::TRVStyle* rvs;
	bool Indeterminate;
	HIDESBASE void __fastcall SetPadding(Rvstyle::TRVRect* Padding, Rvstyle::TRVBooleanRect* UsePadding, Rvstyle::TRVStyle* rvs);
	void __fastcall GetPadding(Rvstyle::TRVRect* Padding, Rvstyle::TRVBooleanRect* UsePadding);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	__property System::Uitypes::TColor ChosenColor = {read=FChosenColor, write=SetChosenColor, nodefault};
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVFillColor(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVFillColor(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVFillColor(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVFillColor(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Fillcolorrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FILLCOLORRVFRM)
using namespace Fillcolorrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// FillcolorrvfrmHPP
