﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'dmActions.pas' rev: 27.00 (Windows)

#ifndef DmactionsHPP
#define DmactionsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdActns.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dmactions
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TrvActionsResource;
class PASCALIMPLEMENTATION TrvActionsResource : public System::Classes::TDataModule
{
	typedef System::Classes::TDataModule inherited;
	
__published:
	Vcl::Actnlist::TActionList* ActionList1;
	Richviewactions::TrvActionCut* rvActionCut1;
	Richviewactions::TrvActionCopy* rvActionCopy1;
	Richviewactions::TrvActionPaste* rvActionPaste1;
	Richviewactions::TrvActionUndo* rvActionUndo1;
	Richviewactions::TrvActionRedo* rvActionRedo1;
	Richviewactions::TrvActionFontBold* rvActionFontBold1;
	Richviewactions::TrvActionFontItalic* rvActionFontItalic1;
	Richviewactions::TrvActionInsertTable* rvActionInsertTable1;
	Richviewactions::TrvActionFontUnderline* rvActionFontUnderline1;
	Richviewactions::TrvActionFontStrikeout* rvActionFontStrikeout1;
	Richviewactions::TrvActionPrintPreview* rvActionPrintPreview1;
	Richviewactions::TrvActionPrint* rvActionPrint1;
	Richviewactions::TrvActionWordWrap* rvActionWordWrap1;
	Vcl::Controls::TImageList* ImageList1;
	Richviewactions::TrvActionFonts* rvActionFonts1;
	Richviewactions::TrvActionAlignLeft* rvActionAlignLeft1;
	Richviewactions::TrvActionAlignRight* rvActionAlignRight1;
	Richviewactions::TrvActionAlignCenter* rvActionAlignCenter;
	Richviewactions::TrvActionSelectAll* rvActionSelectAll1;
	Richviewactions::TrvActionIndentInc* rvActionIndentInc1;
	Richviewactions::TrvActionIndentDec* rvActionIndentDec1;
	Richviewactions::TrvActionTableInsertRowsAbove* rvActionTableInsertRowsAbove1;
	Richviewactions::TrvActionTableInsertRowsBelow* rvActionTableInsertRowsBelow1;
	Richviewactions::TrvActionAlignJustify* rvActionAlignJustify1;
	Richviewactions::TrvActionTableInsertColLeft* rvActionTableInsertColLeft1;
	Richviewactions::TrvActionTableInsertColRight* rvActionTableInsertColRight1;
	Richviewactions::TrvActionTableDeleteRows* rvActionTableDeleteRows1;
	Richviewactions::TrvActionTableDeleteCols* rvActionTableDeleteCols1;
	Richviewactions::TrvActionQuickPrint* rvActionQuickPrint1;
	Richviewactions::TrvActionTableMergeCells* rvActionTableMergeCells1;
	Richviewactions::TrvActionTableSplitCells* rvActionTableSplitCells1;
	Richviewactions::TrvActionTableSelectTable* rvActionTableSelectTable1;
	Richviewactions::TrvActionTableSelectRows* rvActionTableSelectRows1;
	Richviewactions::TrvActionTableSelectCols* rvActionTableSelectCols1;
	Richviewactions::TrvActionTableSelectCell* rvActionTableSelectCell1;
	Richviewactions::TrvActionFontGrow* rvActionFontGrow1;
	Richviewactions::TrvActionFontShrink* rvActionFontShrink1;
	Richviewactions::TrvActionFontGrowOnePoint* rvActionFontGrowOnePoint1;
	Richviewactions::TrvActionFontShrinkOnePoint* rvActionFontShrinkOnePoint1;
	Richviewactions::TrvActionFontAllCaps* rvActionFontAllCaps1;
	Richviewactions::TrvActionFontOverline* rvActionFontOverline1;
	Richviewactions::TrvActionFind* rvActionFind1;
	Richviewactions::TrvActionFindNext* rvActionFindNext1;
	Richviewactions::TrvActionReplace* rvActionReplace1;
	Richviewactions::TrvActionFontColor* rvActionFontColor1;
	Richviewactions::TrvActionFontBackColor* rvActionFontBackColor1;
	Richviewactions::TrvActionParaColor* rvActionParaColor1;
	Richviewactions::TrvActionColor* rvActionColor1;
	Richviewactions::TrvActionTableDeleteTable* rvActionTableDeleteTable1;
	Richviewactions::TrvActionParagraph* rvActionParagraph1;
	Richviewactions::TrvActionFontEx* rvActionFontEx1;
	Richviewactions::TrvActionFillColor* rvActionFillColor1;
	Richviewactions::TrvActionSave* rvActionSave1;
	Richviewactions::TrvActionSaveAs* rvActionSaveAs1;
	Richviewactions::TrvActionOpen* rvActionOpen1;
	Richviewactions::TrvActionExport* rvActionExport1;
	Richviewactions::TrvActionInsertFile* rvActionInsertFile1;
	Richviewactions::TrvActionInsertPicture* rvActionInsertPicture1;
	Richviewactions::TrvActionPasteSpecial* rvActionPasteSpecial1;
	Richviewactions::TrvActionLineSpacing100* rvActionLineSpacing1001;
	Richviewactions::TrvActionLineSpacing150* rvActionLineSpacing1501;
	Richviewactions::TrvActionLineSpacing200* rvActionLineSpacing2001;
	Richviewactions::TrvActionInsertPageBreak* rvActionInsertPageBreak1;
	Richviewactions::TrvActionRemovePageBreak* rvActionRemovePageBreak1;
	Richviewactions::TrvActionTableCellVAlignTop* rvActionTableCellVAlignTop1;
	Richviewactions::TrvActionTableCellVAlignMiddle* rvActionTableCellVAlignMiddle1;
	Richviewactions::TrvActionTableCellVAlignBottom* rvActionTableCellVAlignBottom1;
	Richviewactions::TrvActionTableCellVAlignDefault* rvActionTableCellVAlignDefault1;
	Richviewactions::TrvActionParaBorder* rvActionParaBorder1;
	Richviewactions::TrvActionItemProperties* rvActionItemProperties1;
	Richviewactions::TrvActionInsertHLine* rvActionInsertHLine1;
	Richviewactions::TrvActionInsertHyperlink* rvActionInsertHyperlink1;
	Richviewactions::TrvActionTableProperties* rvActionTableProperties1;
	Richviewactions::TrvActionTableGrid* rvActionTableGrid1;
	Richviewactions::TrvActionParaList* rvActionParaList1;
	Richviewactions::TrvActionInsertSymbol* rvActionInsertSymbol1;
	Richviewactions::TrvActionTableCellLeftBorder* rvActionTableCellLeftBorder1;
	Richviewactions::TrvActionTableCellRightBorder* rvActionTableCellRightBorder1;
	Richviewactions::TrvActionTableCellTopBorder* rvActionTableCellTopBorder1;
	Richviewactions::TrvActionTableCellBottomBorder* rvActionTableCellBottomBorder1;
	Richviewactions::TrvActionTableCellAllBorders* rvActionTableCellAllBorders1;
	Richviewactions::TrvActionTableCellNoBorders* rvActionTableCellNoBorders1;
	Richviewactions::TrvActionParaBullets* rvActionParaBullets1;
	Richviewactions::TrvActionParaNumbering* rvActionParaNumbering1;
	Richviewactions::TrvActionBackground* rvActionBackground1;
	Richviewactions::TrvActionPageSetup* rvActionPageSetup1;
	Richviewactions::TrvActionTextRTL* rvActionTextRTL1;
	Richviewactions::TrvActionTextLTR* rvActionTextLTR1;
	Richviewactions::TrvActionParaRTL* rvActionParaRTL1;
	Richviewactions::TrvActionParaLTR* rvActionParaLTR1;
	Richviewactions::TrvActionCharCase* rvActionCharCase1;
	Richviewactions::TrvActionShowSpecialCharacters* rvActionShowSpecialCharacters1;
	Richviewactions::TrvActionSubscript* rvActionSubscript1;
	Richviewactions::TrvActionSuperscript* rvActionSuperscript1;
	Richviewactions::TrvActionClearLeft* rvActionClearLeft1;
	Richviewactions::TrvActionClearRight* rvActionClearRight1;
	Richviewactions::TrvActionClearBoth* rvActionClearBoth1;
	Richviewactions::TrvActionClearNone* rvActionClearNone1;
	Richviewactions::TrvActionVAlign* rvActionVAlign1;
	Richviewactions::TrvActionPasteAsText* rvActionPasteAsText1;
	Richviewactions::TrvActionRemoveHyperlinks* rvActionRemoveHyperlinks1;
	Richviewactions::TrvActionHide* rvActionHide1;
	Richviewactions::TrvActionTableCellRotationNone* rvActionTableCellRotationNone1;
	Richviewactions::TrvActionTableCellRotation90* rvActionTableCellRotation901;
	Richviewactions::TrvActionTableCellRotation180* rvActionTableCellRotation1801;
	Richviewactions::TrvActionTableCellRotation270* rvActionTableCellRotation2701;
	Richviewactions::TrvActionTableSplit* rvActionTableSplit1;
	Richviewactions::TrvActionTableToText* rvActionTableToText1;
	Richviewactions::TrvActionTableSort* rvActionTableSort1;
	Richviewactions::TrvActionStyleTemplates* rvActionStyleTemplates1;
	Richviewactions::TrvActionStyleInspector* rvActionStyleInspector1;
	Richviewactions::TrvActionClearFormat* rvActionClearFormat1;
	Richviewactions::TrvActionAddStyleTemplate* rvActionAddStyleTemplate1;
	Richviewactions::TrvActionClearTextFormat* rvActionClearTextFormat1;
	Richviewactions::TrvActionInsertFootnote* rvActionInsertFootnote1;
	Richviewactions::TrvActionInsertEndnote* rvActionInsertEndnote1;
	Richviewactions::TrvActionEditNote* rvActionEditNote1;
	Richviewactions::TrvActionInsertSidenote* rvActionInsertSidenote1;
	Richviewactions::TrvActionInsertTextBox* rvActionInsertTextBox1;
	Richviewactions::TrvActionNew* rvActionNew1;
	Richviewactions::TrvActionInsertNumber* rvActionInsertNumber1;
	Richviewactions::TrvActionInsertCaption* rvActionInsertCaption1;
public:
	/* TDataModule.Create */ inline __fastcall virtual TrvActionsResource(System::Classes::TComponent* AOwner) : System::Classes::TDataModule(AOwner) { }
	/* TDataModule.CreateNew */ inline __fastcall virtual TrvActionsResource(System::Classes::TComponent* AOwner, int Dummy) : System::Classes::TDataModule(AOwner, Dummy) { }
	/* TDataModule.Destroy */ inline __fastcall virtual ~TrvActionsResource(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TrvActionsResource* rvActionsResource;
}	/* namespace Dmactions */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DMACTIONS)
using namespace Dmactions;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DmactionsHPP
