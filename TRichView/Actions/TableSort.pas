
{*******************************************************}
{                                                       }
{       RichView                                        }
{       Table rows sorting.                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit TableSort;

{$I RV_Defs.inc}

interface
uses
 SysUtils, Windows, Messages, Classes, Graphics,
 RVClasses, RVItem, RVTable, RVTypes, RVRVData,
 {$IFDEF RVUNICODESTR}
 RVGetTextW,
 {$ELSE}
 RVGetText,
 {$ENDIF}
 RVStyle, RichView, RVEdit;

type TRVSortDataType = (rvsdtString, rvsdtNumber);

procedure GetRangeOfRowsForSorting(table: TRVTableItemInfo;
  HasHeadingRow, IgnoreTableHeadingRow: Boolean;
  var FirstRow, LastRow: Integer);

// Can this table be sorted from FirstRow to LastRow?
// Yes, if it does not have cells merged across these rows
function CanSortTable(table: TRVTableItemInfo; FirstRow: Integer =0;
  LastRow: Integer = -1): Boolean;

// Returns a new table, a sorted copy of table.
// Table must be inserted in rv.
// Sorting is performed by the content of the Column-th column
// (must be in range 0..table.ColCount-1).
// Content is compared as text or as floating point number
// (non-numbers are compared as 0)

function CreateSortedTable(rv: TCustomRichView; table: TRVTableItemInfo;
  Column: Integer; Ascending, IgnoreCase: Boolean;
  DataType: TRVSortDataType;
  FirstRow: Integer =0; LastRow: Integer = -1): TRVTableItemInfo;

// Sorts the current table in rve, as an editing operation (can be undone)
// Detects a range of rows automatically. Never sorts table heading rows.
// If HeadingRow, the first [selected] row is not sorted.
function SortCurrentTable(rve: TCustomRichViewEdit;
  Column: Integer; Ascending, IgnoreCase, HasHeadingRow: Boolean;
  DataType: TRVSortDataType): Boolean;

// The same, but the table is specified in the parameters.
// table must be rve.GetItem(ItemNo)
function DoSortCurrentTable(rve: TCustomRichViewEdit; table: TRVTableItemInfo;
  ItemNo: Integer;
  Column: Integer; Ascending, IgnoreCase, HasHeadingRow: Boolean;
  DataType: TRVSortDataType): Boolean;

implementation

function CanSortTable(table: TRVTableItemInfo; FirstRow, LastRow: Integer): Boolean;
var r,c,mr,mc: Integer;
begin
  Result := False;
  if LastRow<0 then
    LastRow := table.RowCount-1;
  for r := FirstRow to LastRow do
    for c := 0 to table.ColCount-1 do
      if (table.Cells[r,c]<>nil) and
         (table.Cells[r,c].RowSpan>1) then
        exit;
  if FirstRow>0 then
    for c := 0 to table.ColCount-1 do
      if table.Cells[FirstRow,c]=nil then begin
        table.Rows.GetMainCell(FirstRow, c, mr, mc);
        if mr<FirstRow then
          exit;
      end;
  Result := True;
end;
{------------------------------------------------------------------------------}
type
  TCellSortInfo = class
    Row: Integer;
    Text: String;
  end;

function CompareCellsNA(Item1, Item2: Pointer): Integer;
var v1,v2: Extended;
begin
  try
    v1 := StrToFloat(TCellSortInfo(Item1).Text);
  except
    v1 := 0;
  end;
  try
    v2 := StrToFloat(TCellSortInfo(Item2).Text);
  except
    v2 := 0;
  end;
  if v1<v2 then
    Result := -1
  else if v2<v1 then
    Result := 1
  else
    Result := 0;
end;

function CompareCellsND(Item1, Item2: Pointer): Integer;
var v1,v2: Extended;
begin
  try
    v1 := StrToFloat(TCellSortInfo(Item1).Text);
  except
    v1 := 0;
  end;
  try
    v2 := StrToFloat(TCellSortInfo(Item2).Text);
  except
    v2 := 0;
  end;
  if v1<v2 then
    Result := 1
  else if v2<v1 then
    Result := -1
  else
    Result := 0;
end;


function CompareCellsA(Item1, Item2: Pointer): Integer;
begin
  Result := AnsiCompareText(TCellSortInfo(Item1).Text, TCellSortInfo(Item2).Text);
end;

function CompareCellsACS(Item1, Item2: Pointer): Integer;
begin
  Result := AnsiCompareStr(TCellSortInfo(Item1).Text, TCellSortInfo(Item2).Text);
end;

function CompareCellsD(Item1, Item2: Pointer): Integer;
begin
  Result := -AnsiCompareText(TCellSortInfo(Item1).Text, TCellSortInfo(Item2).Text);
end;

function CompareCellsDCS(Item1, Item2: Pointer): Integer;
begin
  Result := -AnsiCompareStr(TCellSortInfo(Item1).Text, TCellSortInfo(Item2).Text);
end;
{------------------------------------------------------------------------------}
function CreateSortedTable(rv: TCustomRichView; table: TRVTableItemInfo;
  Column: Integer; Ascending, IgnoreCase: Boolean;
  DataType: TRVSortDataType; FirstRow, LastRow: Integer): TRVTableItemInfo;
  {....................................................}
  procedure CopyRow(SrcTable, DstTable: TRVTableItemInfo;
    SrcRow, DstRow: Integer);
  var c: Integer;
      SourceCell, DestCell: TRVTableCellData;
      Stream: TMemoryStream;
      Color: TColor;
  begin
    Color := clNone;
    for c := 0 to table.ColCount-1 do begin
      SourceCell := SrcTable.Cells[SrcRow, c];
      if SourceCell<>nil then begin
        if SourceCell.ColSpan>1 then
          DstTable.MergeCells(DstRow, c, SourceCell.ColSpan, SourceCell.RowSpan, True);
        DestCell := DstTable.Cells[DstRow, c];
        DestCell.AssignAttributesFrom(SrcTable.Cells[SrcRow, c], True, 1, 1);
        Stream := TMemoryStream.Create;
        try
          SourceCell.GetRVData.SaveRVFToStream(Stream, False, clNone, nil, nil);
          Stream.Position := 0;
          DestCell.LoadRVFFromStream(Stream, Color, nil, nil
            {$IFNDEF RVDONOTUSESTYLETEMPLATES},nil{$ENDIF});
        finally
          Stream.Free;
        end;
      end;
    end;
  end;
  {....................................................}

var r: Integer;
    SortList: TRVList;
    SortItem: TCellSortInfo;
    RVFOptions: TRVFOptions;
begin
  Result := nil;
  if LastRow<0 then
    LastRow := table.RowCount-1;
  if (Column<0) or (Column>=table.ColCount) or (LastRow<=FirstRow) or
    not CanSortTable(table) then
    exit;
  SortList := TRVList.Create;
  try
    // preparing a sort list
    SortList.Capacity := LastRow-FirstRow+1;
    for r := FirstRow to LastRow do begin
      SortItem := TCellSortInfo.Create;
      SortItem.Row := r;
      if table.Cells[r, Column]<>nil then
        SortItem.Text := GetRVDataText(table.Cells[r, Column].GetRVData);
      SortList.Add(SortItem);
    end;
    // sorting
    case DataType of
      rvsdtString:
        if Ascending then
          if IgnoreCase then
            SortList.Sort(CompareCellsA)
          else
            SortList.Sort(CompareCellsACS)
        else
          if IgnoreCase then
            SortList.Sort(CompareCellsD)
          else
            SortList.Sort(CompareCellsDCS);
      rvsdtNumber:
        if Ascending then
          SortList.Sort(CompareCellsNA)
        else
          SortList.Sort(CompareCellsND)
    end;
    // creating new table
    Result := TRVTableItemInfo.CreateEx(table.RowCount, table.ColCount,
      table.Cells[0,0].GetParentData);
    Result.AssignProperties(table);
    // copying rows from the old table in the sort order
    RVFOptions := rv.RVFOptions;
    rv.RVFOptions := RVFOptions-[rvfoSaveTextStyles, rvfoSaveParaStyles];
    try
      for r := 0 to FirstRow-1 do
        CopyRow(table, Result, r, r);
      for r := FirstRow to LastRow do begin
        SortItem := TCellSortInfo(SortList.Items[r-FirstRow]);
        CopyRow(table, Result, SortItem.Row, r);
      end;
      for r := LastRow+1 to table.RowCount-1 do
        CopyRow(table, Result, r, r);
    finally
      rv.RVFOptions := RVFOptions;
    end;
  finally
    SortList.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure GetRangeOfRowsForSorting(table: TRVTableItemInfo;
  HasHeadingRow, IgnoreTableHeadingRow: Boolean; var FirstRow, LastRow: Integer);
var c, cs, rs: Integer;
begin
  if table.GetNormalizedSelectionBounds(False, FirstRow, c, cs, rs) and (rs>1) then
    LastRow := FirstRow+rs-1
  else begin
    FirstRow := 0;
    LastRow := table.RowCount-1;
  end;
  if HasHeadingRow then
   inc(FirstRow);
  if not IgnoreTableHeadingRow and (FirstRow<table.HeadingRowCount) then
    FirstRow := table.HeadingRowCount;
end;
{------------------------------------------------------------------------------}
function DoSortCurrentTable(rve: TCustomRichViewEdit; table: TRVTableItemInfo;
  ItemNo: Integer;
  Column: Integer; Ascending, IgnoreCase, HasHeadingRow: Boolean; DataType: TRVSortDataType): Boolean;
var FirstRow, LastRow, p: Integer;
begin
  Result := False;
  GetRangeOfRowsForSorting(table, HasHeadingRow, False, FirstRow, LastRow);
  table := CreateSortedTable(rve, table, Column, Ascending, IgnoreCase, DataType,
    FirstRow, LastRow);
  if table=nil then
    exit;
  rve.BeginUndoGroup(rvutModifyItem);
  rve.SetUndoGroupMode(True);
  try
    p := rve.RootEditor.VScrollPos;
    rve.BeginUpdate;
    try
      rve.SetSelectionBounds(ItemNo, 0, ItemNo, 1);
      rve.InsertItemR(rve.GetItemTextR(ItemNo), table);
      rve.RootEditor.VScrollPos := p;
    finally
      rve.EndUpdate;
    end;
  finally
    rve.SetUndoGroupMode(False);
  end;
  Result := True;
end;
{------------------------------------------------------------------------------}
function SortCurrentTable(rve: TCustomRichViewEdit;
  Column: Integer; Ascending, IgnoreCase, HasHeadingRow: Boolean;
  DataType: TRVSortDataType): Boolean;
var rvet: TCustomRichViewEdit;
    table: TRVTableItemInfo;
begin
  Result := False;
  if not rve.GetCurrentItemEx(TRVTableItemInfo, rvet, TCustomRVItemInfo(table)) then
    exit;
  Result := DoSortCurrentTable(rvet, table, table.GetMyItemNo, Column,
    Ascending, IgnoreCase, HasHeadingRow, DataType);
end;

end.
