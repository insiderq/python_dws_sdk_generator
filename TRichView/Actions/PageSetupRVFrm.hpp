﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'PageSetupRVFrm.pas' rev: 27.00 (Windows)

#ifndef PagesetuprvfrmHPP
#define PagesetuprvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.WinSpool.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Pagesetuprvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVPageSetup;
class PASCALIMPLEMENTATION TfrmRVPageSetup : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* tsPage;
	Vcl::Comctrls::TTabSheet* tsHF;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgOrientation;
	Vcl::Controls::TImageList* il;
	Vcl::Stdctrls::TGroupBox* gbMargins;
	Vcl::Stdctrls::TLabel* lblTop;
	Vcl::Stdctrls::TLabel* lblBottom;
	Vcl::Stdctrls::TLabel* lblLeft;
	Vcl::Stdctrls::TLabel* lblRight;
	Rvspinedit::TRVSpinEdit* seTop;
	Rvspinedit::TRVSpinEdit* seBottom;
	Rvspinedit::TRVSpinEdit* seLeft;
	Rvspinedit::TRVSpinEdit* seRight;
	Vcl::Stdctrls::TCheckBox* cbMirrorMargins;
	Vcl::Stdctrls::TGroupBox* gbPaper;
	Vcl::Stdctrls::TLabel* lblSize;
	Vcl::Stdctrls::TComboBox* cmbSize;
	Vcl::Stdctrls::TLabel* lblSource;
	Vcl::Stdctrls::TComboBox* cmbSource;
	Vcl::Stdctrls::TGroupBox* gbHeader;
	Vcl::Controls::TImageList* ilAlign;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbHLeft;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbHCenter;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbHRight;
	Vcl::Stdctrls::TLabel* lblHText;
	Vcl::Stdctrls::TEdit* txtHText;
	Vcl::Stdctrls::TButton* btnHFont;
	Vcl::Stdctrls::TCheckBox* cbHOnFirstPage;
	Vcl::Stdctrls::TGroupBox* gbFooter;
	Vcl::Stdctrls::TLabel* lblFText;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbFLeft;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbFCenter;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbFRight;
	Vcl::Stdctrls::TEdit* txtFText;
	Vcl::Stdctrls::TButton* btnFFont;
	Vcl::Stdctrls::TCheckBox* cbFOnFirstPage;
	Vcl::Stdctrls::TLabel* lblCodes;
	Vcl::Dialogs::TFontDialog* fd;
	Vcl::Stdctrls::TGroupBox* gbNumbers;
	Vcl::Stdctrls::TLabel* lblStartFrom;
	Rvspinedit::TRVSpinEdit* seStartFrom;
	void __fastcall btnHFontClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall btnFFontClick(System::TObject* Sender);
	
private:
	Vcl::Graphics::TFont* FHeaderFont;
	Vcl::Graphics::TFont* FFooterFont;
	Vcl::Controls::TControl* _cbMirrorMargins;
	Vcl::Controls::TControl* _cbHOnFirstPage;
	Vcl::Controls::TControl* _cbFOnFirstPage;
	Vcl::Controls::TControl* _gbMargins;
	Vcl::Controls::TControl* _cmbSize;
	Vcl::Controls::TControl* _cmbSource;
	Vcl::Controls::TControl* _txtHText;
	Vcl::Controls::TControl* _txtFText;
	bool FInches;
	
public:
	bool __fastcall Init(void);
	void __fastcall Apply(void);
	DYNAMIC void __fastcall Localize(void);
	void __fastcall UseInches(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVPageSetup(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVPageSetup(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVPageSetup(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVPageSetup(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Pagesetuprvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PAGESETUPRVFRM)
using namespace Pagesetuprvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PagesetuprvfrmHPP
