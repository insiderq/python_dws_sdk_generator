
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Intermediate form                               }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit BorderSidesRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, BaseRVFrm, RVScroll, RichView, RVStyle, Buttons, StdCtrls,
  {$IFDEF USERVKSDEVTE}
  te_controls,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntStdCtrls, TntButtons,
  {$ENDIF}
  RVColorCombo, ComCtrls;

type
  TfrmRVBorderSides = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    rvs: TRVStyle;
    PageControl1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    gbSample: TGroupBox;
    btnLeft: TSpeedButton;
    btnRight: TSpeedButton;
    btnTop: TSpeedButton;
    btnBottom: TSpeedButton;
    rv: TRichView;
    gbSettings: TGroupBox;
    lblColor: TLabel;
    lblWidth: TLabel;
    cmbColor: TRVColorCombo;
    cmbWidth: TComboBox;
    procedure cmbWidthDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnClick(Sender: TObject);
    procedure cmbColorColorChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
    {$IFDEF USERVKSDEVTE}
    procedure tecmbWidthDrawItem(Control: TWinControl; Canvas: TCanvas; Index: Integer;
    Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
  public
    { Public declarations }
    _cmbWidth: TControl;
    _btnLeft, _btnTop, _btnRight, _btnBottom: TControl;    
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation
uses RichViewActions, RVALocalize;

{$R *.dfm}

procedure TfrmRVBorderSides.cmbWidthDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  {$IFDEF USERVTNT}
  RVA_DrawCmbWidthItem(TTntComboBox(_cmbWidth).Canvas, Rect, Index, False,
    cmbColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
  {$ELSE}
  RVA_DrawCmbWidthItem(TComboBox(_cmbWidth).Canvas, Rect, Index, False,
    cmbColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
  {$ENDIF}
end;

procedure TfrmRVBorderSides.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

   procedure SwitchBtn(btn: TControl);
   var OnClick: TNotifyEvent;
   begin
     SetButtonDown(btn, not GetButtonDown(btn));
     OnClick := GetButtonClick(btn);
     if Assigned(OnClick) then
       OnClick(btn);
   end;

begin
  if ssCtrl in Shift then
    case Key of
    ord('L'):
      SwitchBtn(_btnLeft);
    ord('T'):
      SwitchBtn(_btnTop);
    ord('R'):
      SwitchBtn(_btnRight);
    ord('B'):
      SwitchBtn(_btnBottom);
    end;
end;

function TfrmRVBorderSides.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := PageControl1.Left*2+PageControl1.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-PageControl1.Top-PageControl1.Height);
  Result := True;
end;

procedure TfrmRVBorderSides.btnClick(Sender: TObject);
begin
  {$IFDEF USERVTNT}
  if Sender is TTntSpeedButton then
  {$ELSE}
  if Sender is TSpeedButton then
  {$ENDIF}
    SetButtonFlat(TControl(Sender), False);
end;

procedure TfrmRVBorderSides.cmbColorColorChange(Sender: TObject);
begin
  inherited;
  _cmbWidth.Invalidate;
end;

procedure TfrmRVBorderSides.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVBorderSides.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _cmbWidth then begin
    _cmbWidth := NewControl;
    {$IFDEF USERVKSDEVTE}
    TTeComboBox(_cmbWidth).OnDrawItem := tecmbWidthDrawItem;
    {$ENDIF}
    end
  else if OldControl = _btnLeft then
    _btnLeft := NewControl
  else if OldControl = _btnTop then
    _btnTop := NewControl
  else if OldControl = _btnRight then
    _btnRight := NewControl
  else if OldControl = _btnBottom then
    _btnBottom := NewControl;
end;
{$ENDIF}

{$IFDEF USERVKSDEVTE}
procedure TfrmRVBorderSides.tecmbWidthDrawItem(Control: TWinControl;
  Canvas: TCanvas; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  RVA_DrawCmbWidthItem(Canvas, Rect, Index, False, cmbColor.ChosenColor, State,
    ControlPanel as TRVAControlPanel);
end;
{$ENDIF}

procedure TfrmRVBorderSides.FormCreate(Sender: TObject);
begin
  _cmbWidth := cmbWidth;
  _btnLeft := btnLeft;
  _btnTop := btnTop;
  _btnRight := btnRight;
  _btnBottom := btnBottom;
  inherited;
end;

end.
