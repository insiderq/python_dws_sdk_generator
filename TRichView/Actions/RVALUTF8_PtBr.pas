﻿// This file is a copy of RVAL_PtBr.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Portuguese (Brazilian) translation              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Bruno Sonnino 2003-04-02               }
{ Updated by: Alexandre Palmeira 2014-02-12             }
{*******************************************************}

unit RVALUTF8_PtBr;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =                            
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'polegadas', 'cm', 'mm', 'picas', 'pixels', 'pontos',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pi', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Arquivo', '&Editar', 'F&ormatar', 'Fo&nte', '&Parágrafo', '&Inserir', '&Tabela', '&Janela', '&Ajuda',
  // exit
  'Sai&r',
  // top-level menus: View, Tools,
  '&Visualiza', 'Ferramen&tas',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Tamanho', 'Estilo', 'Selec&iona', 'Ali&nha Conteúdo das Células', 'Bordas das Cé&lulas',
  // menus: Table cell rotation
  '&Rotação de Célula',    
  // menus: Text flow, Footnotes/endnotes
  '&Fluxo de texto', 'Notas de &Rodapé',
  // ribbon tabs: tab1, tab2, view, table
  '&Home', '&Avançado', '&Exibir', '&Tabela',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Área de Transferência', 'Fonte', 'Parágrafo', 'Lista', 'Editando',
  // ribbon groups: insert, background, page setup,
  'Inserir', 'Fundo', 'Configuração de Página',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Links', 'Notas de Rodapé', 'Modos de Exibição de Documento', 'Exibir/Ocultar', 'Zoom', 'Opções',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserir', 'Apagar', 'Operações', 'Bordas',
  // ribbon groups: styles 
  'Estilos',
  // ribbon screen tip footer,
  'Pressione F1 para obter mais ajuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documentos recentes', 'Salvar uma cópia do documento', 'Visualizar e imprimir o documento',
  // ribbon label: units combo
  'Unidades:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Novo', 'Novo|Cria um novo documento em branco',
  // TrvActionOpen
  '&Abrir...', 'Abrir|Abre um documento do disco',
  // TrvActionSave
  '&Salvar', 'Salvar|Salva o documento para disco',
  // TrvActionSaveAs
  'Salvar &Como...', 'Salvar Como...|Salva o documento para disco com novo nome',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta o documento para outro formato',
  // TrvActionPrintPreview
  'Vi&sualizar Impressão', 'Visualizar Impressão|Mostra o documento como ele será impresso',
  // TrvActionPrint
  '&Imprimir...', 'Imprimir|Configura a impressão e imprime o documento',
  // TrvActionQuickPrint
  '&Imprimir', '&Impressão rápida', 'Imprimir|Imprime o documento',
   // TrvActionPageSetup
  'Configurar Pá&gina...', 'Configurar Página|Configura margens, tamanho de papel, orientação, fonte, cabeçalhos e rodapés',
  // TrvActionCut
  'Recor&tar', 'Recortar|Recorta a seleção e coloca na Área de Transferência',
  // TrvActionCopy
  '&Copiar', 'Copiar|Copia a seleção e coloca na Área de Transferência',
  // TrvActionPaste
  'Co&lar', 'Colar|Insere o conteúdo da Área de Transferência',
  // TrvActionPasteAsText
  'Colar como &Texto', 'Colar como Texto|Insere o conteúdo da Área de Transferência sem formatação',
  // TrvActionPasteSpecial
  'Colar E&special...', 'Colar Especial|Insere o conteúdo da Área de Transferência no formato especificado',
  // TrvActionSelectAll
  'Selecionar &Tudo', 'Selecionar Tudo|Seleciona todo o documento',
  // TrvActionUndo
  '&Desfazer', 'Desfazer|Reverte a última ação',
  // TrvActionRedo
  '&Refazer', 'Refazer|Refaz a última ação desfeita',
  // TrvActionFind
  '&Localizar...', 'Localizar|Localiza o texto especificado no documento',
  // TrvActionFindNext
  'Localizar &Próxima', 'Localizar Próxima|Continua a última localização',
  // TrvActionReplace
  '&Substituir...', 'Substituir|Localiza e substitui o texto especificado no documento',
  // TrvActionInsertFile
  '&Arquivo...', 'Inserir Arquivo|Insere o conteúdo do arquivo no documento',
  // TrvActionInsertPicture
  '&Imagem...', 'Inserir Imagem|Insere uma imagem do disco',
  // TRVActionInsertHLine
  'Linha &Horizontal', 'Inserir Linha Horizontal|Insere uma linha horizontal',
  // TRVActionInsertHyperlink
  '&Link Hipertexto...', 'Inserir Hipertexto|Insere um link hipertexto',
  // TRVActionRemoveHyperlinks
  '&Remover Hyperlinks', 'Remover Hyperlinks|Remove todos os hyperlinks no texto selecionado',
  // TrvActionInsertSymbol
  '&Símbolo...', 'Inserir Símbolo|Insere símbolo',
  // TrvActionInsertNumber
  '&Número...', 'Inserir Número|Insere um número',
  // TrvActionInsertCaption
  '&Legenda...', 'Inserir Legenda|Insere uma legenda para o objeto selecionado',
  // TrvActionInsertTextBox
  'Caixa de &Texto', 'Inserir Caixa de Texto|Insere uma caixa de texto',
  // TrvActionInsertSidenote
  'Nota em &Quadro', 'Inserir Nota em Quadro|Insere uma nota em um quadro de texto',
  // TrvActionInsertPageNumber
  'Número de &Página', 'Inserir Número de Página|Insere um número de página',
  // TrvActionParaList
  '&Marcadores e Numeração...', 'Marcadores e Numeração|Aplica ou edita marcadores ou numeração para os parágrafos selecionados',
  // TrvActionParaBullets
  '&Marcadores', 'Marcadores|Adiciona ou remove marcadores ao parágrafo',
  // TrvActionParaNumbering
  '&Numeração', 'Numeração|Adiciona ou remove numeração ao parágrafo',
  // TrvActionColor
  'Cor de &Fundo...', 'Cor de Fundo|Muda a cor de fundo do documento',
  // TrvActionFillColor
  'Cor de &Preenchimento...', 'Cor de Preenchimento|Muda cor de fundo do texto, parágrafo, célula, tabela ou documento',
  // TrvActionInsertPageBreak
  '&Inserir Quebra de Página', 'Inserir Quebra de Página|Insere uma quebra de página',
  // TrvActionRemovePageBreak
  '&Remover Quebra de Página', 'Remover Quebra de Página|Remove a quebra de página',
  // TrvActionClearLeft
  'Limpar Fluxo do Texto do Lado &Esquerdo', 'Limpar Fluxo do Texto do Lado Esquerdo|Posiciona este parágrafo abaixo de qualquer figura alinhada pela esquerda',
  // TrvActionClearRight
  'Limpar Fluxo do Texto do Lado &Direito', 'Limpar Fluxo do Texto do Lado Direito|Posiciona este parágrafo abaixo de qualquer figura alinhada pela direita',
  // TrvActionClearBoth
  'Limpar Fluxo do Texto de &Ambos os Lados', 'Limpar Fluxo do Texto de &Ambos os Lados|Posiciona este parágrafo abaixo de qualquer',
  // TrvActionClearNone
  'Fluxo de Texto &Normal', 'Fluxo do Texto Normal|Texto ao redor de figuras alinhadas pela esquerda ou pela direita',
  // TrvActionVAlign
  'Posição do &Objeto...', 'Posição do Objeto|Muda a posição do objeto selecionado',
  // TrvActionItemProperties
  '&Propriedades de Objeto...', 'Propriedades de Objeto|Define propriedades do objeto ativo',
  // TrvActionBackground
  '&Fundo...', 'Fundo|Escolhe a cor e imagem de fundo',
  // TrvActionParagraph
  '&Parágrafo...', 'Parágrafo|Muda os atributos do parágrafo',
  // TrvActionIndentInc
  '&Aumentar Recuo', 'Aumentar Recuo|Aumenta recuo à esquerda dos parágrafos selecionados',
  // TrvActionIndentDec
  '&Diminuir Recuo', 'Diminuir Recuo|Diminui recuo à esquerda dos parágrafos selecionados',
  // TrvActionWordWrap
  '&Quebra de Linha', 'Quebra de Linha|Liga/Desliga quebra de linha para os parágrafos selecionados',
  // TrvActionAlignLeft
  'Alinhar à &Esquerda', 'Alinhar à Esquerda|Alinha o texto selecionado à esquerda',
  // TrvActionAlignRight
  'Alinhar à &Direita', 'Alinhar à Direita|Alinha o texto selecionado à direita',
  // TrvActionAlignCenter
  '&Centralizar', 'Centralizar|Centraliza o texto selecionado',
  // TrvActionAlignJustify
  '&Justificar', 'Justificar|Alinha o texto selecionado à direita e à esquerda',
  // TrvActionParaColor
  'Cor de Fu&ndo do Parágrafo...', 'Cor de Fundo do Parágrafo|Configura cor de fundo de parágrafos',
  // TrvActionLineSpacing100
  'Espaço de Linha &Simples', 'Espaço de Linha Simples|Configura espaço de linha simples',
  // TrvActionLineSpacing150
  'Espaço de Lin&ha 1,5', 'Espaço de Linha 1,5|Configura espaço de linha igual a 1,5 linhas',
  // TrvActionLineSpacing200
  'Espaço de Linha &Duplo', 'Espaço de Linha Duplo|Configura espaço de linha duplo',
  // TrvActionParaBorder
  '&Bordas e Fundo de Parágrafo...', 'Bordas e Fundo de Parágrafo|Configura bordas e fundo para os parágrafos selecionados',
  // TrvActionInsertTable
  '&Inserir Tabela...', '&Tabela', 'Inserir Tabela|Insere uma nova tabela',
  // TrvActionTableInsertRowsAbove
  'Inserir Linha &Acima', 'Inserir Linha Acima|Insere nova linha acima das células selecionadas',
  // TrvActionTableInsertRowsBelow
  'Inserir Linha A&baixo', 'Inserir Linha Abaixo|Insere nova linha abaixo das células selecionadas',
  // TrvActionTableInsertColLeft
  'Inserir Coluna à &Esquerda', 'Inserir Coluna à Esquerda|Insere nova coluna à esquerda das células selecionadas',
  // TrvActionTableInsertColRight
  'Inserir Coluna à &Direita', 'Inserir Coluna à Direita|Insere nova coluna à direita das células selecionadas',
  // TrvActionTableDeleteRows
  'Excluir L&inhas', 'Excluir Linhas|Exclui linhas contendo as células selecionadas',
  // TrvActionTableDeleteCols
  'Excluir &Colunas', 'Excluir Colunas|Exclui colunas contendo as células selecionadas',
  // TrvActionTableDeleteTable
  '&Excluir Tabela', 'Excluir Tabela|Exclui a tabela',
  // TrvActionTableMergeCells
  '&Mesclar Células', 'Mesclar Células|Mescla as células selecionadas',
  // TrvActionTableSplitCells
  '&Dividir Células...', 'Dividir Células|Divide as células selecionadas',
  // TrvActionTableSelectTable
  'Selecionar &Tabela', 'Selecionar Tabela|Seleciona a tabela',
  // TrvActionTableSelectRows
  'Selecionar Lin&has', 'Selecionar Linhas|Seleciona linhas',
  // TrvActionTableSelectCols
  'Selecionar Col&unas', 'Selecionar Colunas|Seleciona colunas',
  // TrvActionTableSelectCell
  'Selecionar Cé&lula', 'Selecionar Célula|Selecionar célula',
  // TrvActionTableCellVAlignTop
  'Alinhar Célula no &Topo', 'Alinhar Célula no Topo|Alinha conteúdo da célula no topo',
  // TrvActionTableCellVAlignMiddle
  'Alinhar Célula no &Meio', 'Alinhar Célula no Meio|Alinha conteúdo da célula no meio',
  // TrvActionTableCellVAlignBottom
  'Alinhar Célula em &Baixo', 'Alinhar Célula em Baixo|Alinha conteúdo da célula em baixo',
  // TrvActionTableCellVAlignDefault
  'Alinhamento Vertical de Célula &Padrão', 'Alinhamento Vertical de Célula Padrão|Configura o alinhamento vertical padrão para as células selecionadas',
  // TrvActionTableCellRotationNone
  '&Sem Rotação de Célula', 'Sem Rotação de Célula|Gira o conteúdo da célula 0°',
  // TrvActionTableCellRotation90
  'Girar Célula &90°', 'Girar Célula 90°|Gira o conteúdo da célula 90°',
  // TrvActionTableCellRotation180
  'Girar Célula &180°', 'Girar Célula 180°|Gira o conteúdo da célula 180°',
  // TrvActionTableCellRotation270
  'Girar Célula &270°', 'Girar Célula 270°|Gira o conteúdo da célula 270°',
  // TrvActionTableProperties
  '&Propriedades da Tabela...', 'Propriedades da Tabela|Muda as propriedades da tabela selecionada',
  // TrvActionTableGrid
  'Mostrar Linhas de &Grade', 'Mostrar Linhas de Grade|Mostra ou esconde as linhas de grade',
  // TRVActionTableSplit
  'Di&vidir Tabela', 'Dividir Tabela|Divide a tabela em duas tabelas começando na linha selecionada',
  // TRVActionTableToText
  'Converter em Te&xto...', 'Converter em Texto|Converte a tabela em texto',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena as linhas da tabela',
  // TrvActionTableCellLeftBorder
  'Borda &Esquerda', 'Borda Esquerda|Mostra ou esconde a borda esquerda da célula',
  // TrvActionTableCellRightBorder
  'Borda &Direita', 'Borda Direita|Mostra ou esconde a borda direita da célula',
  // TrvActionTableCellTopBorder
  'Borda &Superior', 'Borda Superior|Mostra ou esconde a borda superior da célula',
  // TrvActionTableCellBottomBorder
  'Borda &Inferior', 'Borda Inferior|Mostra ou esconde a borda superior da célula',
  // TrvActionTableCellAllBorders
  '&Todas Bordas', 'Todas Bordas|Mostra ou esconde todas as bordas da célula',
  // TrvActionTableCellNoBorders
  '&Sem Bordas', 'Sem Bordas|Esconde todas as bordas de células',
  // TrvActionFonts & TrvActionFontEx
  '&Fonte...', 'Fonte|Altera fonte do texto selecionado',
  // TrvActionFontBold
  '&Negrito', 'Negrito|Altera o estilo do texto selecionado para negrito',
  // TrvActionFontItalic
  '&Itálico', 'Itálico|Altera o estilo do texto selecionado para itálico',
  // TrvActionFontUnderline
  '&Sublinhado', 'Sublinhado|Altera o estilo do texto selecionado para sublinhado',
  // TrvActionFontStrikeout
  '&Riscado', 'Riscado|Risca o texto selecionado',
  // TrvActionFontGrow
  '&Aumentar Fonte', 'Aumentar Fonte|Aumenta a altura do texto selecionado em 10%',
  // TrvActionFontShrink
  'D&iminuir Fonte', 'Diminuir Fonte|Diminui a altura do texto selecionado em 10%',
  // TrvActionFontGrowOnePoint
  'A&umentar Fonte em Um Ponto', 'Aumentar Fonte em Um Ponto|Aumenta a altura do texto selecionado em 1 ponto',
  // TrvActionFontShrinkOnePoint
  'D&iminuir Fonte em Um Ponto', 'Diminuir Fonte em Um Ponto|Diminui a altura do texto selecionado em 1 ponto',
  // TrvActionFontAllCaps
  '&Tudo Maiúsculas', 'Tudo Maiúsculas|Muda todos os caracteres do texto selecionado para maiúsculas',
  // TrvActionFontOverline
  '&SobreLinha', 'SobreLinha|Adiciona linha sobre o texto selecionado',
  // TrvActionFontColor
  '&Cor do Texto...', 'Cor do Texto|Muda a cor do texto selecionado',
  // TrvActionFontBackColor
  'Cor de Fu&ndo do Texto...', 'Cor de Fundo do Texto|Muda a cor de fundo do texto selecionado',
  // TrvActionAddictSpell3
  'Verificar &Ortografia', 'Verificar Ortografia|Verifica a ortografia',
  // TrvActionAddictThesaurus3
  '&Sinônimos', 'Sinônimos|Provê sinônimos para a palavra selecionada',
  // TrvActionParaLTR
  'Esquerda para Direita', 'Esquerda para Direita|Configura direção do texto da esquerda para direita para parágrafos selecionados',
  // TrvActionParaRTL
  'Direita para Esquerda', 'Direita para Esquerda|Configura direção do texto da direita para esquerda para parágrafos selecionados',
  // TrvActionLTR
  'Texto Esquerda para Direita', 'Texto Esquerda para Direita|Configura direção do texto da esquerda para direita para texto selecionado',
  // TrvActionRTL
  'Texto Direita para Esquerda', 'Texto Direita para Esquerda|Configura direção do texto da direita para esquerda para texto selecionado',
  // TrvActionCharCase
  'Caixa de Caractere', 'Caixa de Caractere|Altera caixa do texto selecionado',
  // TrvActionShowSpecialCharacters
  'Caracteres &Não Imprimíveis', 'Caracteres não Imprimíveis|Mostra ou esconde caracteres não imprimíveis, como marcas de parágrafo, tabulações e espaços',
  // TrvActionSubscript
  'Su&bscrito', 'Subscrito|Converte o texto selecionado para subscrito',
  // TrvActionSuperscript
  'Superescrito', 'Superescrito|Converte o texto selecionado para superescrito',
  'Nota de &Rodapé', 'Nota de Rodapé|Insere uma nota de rodapé',
  // TrvActionInsertEndnote
  'Nota de &Fim', 'Nota de Fim|Insere uma nota de fim',
  // TrvActionEditNote
  'E&ditar Nota', 'Editar Nota|Começa a editar uma nota de rodapé ou de fim',
  '&Ocultar', 'Ocultar|Oculta ou mostra o trecho selecionado',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Estilos...', 'Estilos|Abre o gerenciador de estilos',
  // TrvActionAddStyleTemplate
  '&Adicionar Estilo...', 'Adicionar Estilo|Cria um novo estilo de caractere ou de parágrafo',
  // TrvActionClearFormat,
  '&Limpar Formatação', 'Limpar Formatação|Limpa toda a formatação de caracteres e de parágrafos do trecho selecionado',
  // TrvActionClearTextFormat,
  'Limpar Formatação de &Caracteres', 'Limpar Formatação de Caracteres|Limpa toda a formatação de caracteres do texto selecionado',
  // TrvActionStyleInspector
  '&Inspetor de estilos', 'Inspetor de estilos|Exibe ou oculta o Inspetor de estilos',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Cancela', 'Fecha', 'Insere', '&Abre...', '&Salva...', '&Limpa', 'Ajuda', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'porcento',
  // left, top, right, bottom sides
  'Lado Esquerdo', 'Topo', 'Lado Direito', 'Baixo',
  // save changes? confirm title
  'Salva alterações em %s?', 'Confirme',
  // warning: losing formatting
  '%s pode conter recursos que não são compatíveis com o formato de salvamento escolhido.'#13+
  'Quer salvar o documento neste formato?',
  // RVF format name
  'Formato RichView',
  // Error messages ------------------------------------------------------------
  'Erro',
  'Erro carregando arquivo.'#13#13'Causas possíveis:'#13'- formato deste arquivo não é suportado por este aplicativo;'#13+
  '- o arquivo está corrompido;'#13'- o arquivo está aberto e travado por outro aplicativo.',
  'Erro carregando arquivo de imagem.'#13#13'Causas possíveis:'#13'- o arquivo tem um formato que não é suportado por este aplicativo;'#13+
  '- o arquivo não contém uma imagem;'#13+
  '- o arquivo está corrompido;'#13'- o arquivo está aberto e travado por outro aplicativo.',
  'Erro salvando arquivo.'#13#13'Causas possíveis:'#13'- sem espaço em disco;'#13+
  '- disco está protegido contra escrita;'#13'- mídia removível não está inserida;'#13+
  '- o arquivo esta aberto e travado por outro aplicativo;'#13'- mídia do disco está corrompida.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Arquivos RichView (*.rvf)|*.rvf',  'Arquivos RTF (*.rtf)|*.rtf' , 'Arquivos XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Arquivos Texto (*.txt)|*.txt', 'Arquivos Texto - Unicode (*.txt)|*.txt', 'Arquivos Texto - Autodetecta (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Arquivos HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplificado (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documentos do Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Busca Completa', 'O texto ''%s'' não foi encontrado.',
  // 1 string replaced; Several strings replaced
  '1 ocorrência substituída.', '%d ocorrências substituídas.',
  // continue search from the beginning/end?
  'O final do documento foi atingido, continua do início?',
  'O início do documento foi atingido, continua do final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparente', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Preto', 'Marrom', 'Verde Oliva', 'Verde Escuro', 'Azul Petróleo Escuro', 'Azul escuro', 'Indigo', 'Cinza-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Vermelho Escuro', 'Laranja', 'Amarelo Escuro', 'Verde', 'Azul Petróleo', 'Azul', 'Azul-Cinza', 'Cinza-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vermelho', 'Laranja Claro', 'Verde Lima', 'Verde Mar', 'Azul Água', 'Azul Claro', 'Violeta', 'Cinza-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa Choque', 'Dourado', 'Amarelo', 'Verde Brilhante', 'Turquesa', 'Azul Céu', 'Ameixa', 'Cinza-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa', 'Marrom Claro', 'Amarelo Claro', 'Verde Claro', 'Turquesa Claro', 'Azul Pálido', 'Lavanda', 'Branco',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparente', '&Auto', '&Mais Cores...', '&Padrão',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fundo', 'C&or:', 'Posição', 'Fundo', 'Texto exemplo.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nenhum', '&Expandido', 'Lado a Lado F&ixo', '&Lado a Lado', 'C&entralizado',
  // Padding button
  '&Ajuste...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Cor Preenchimento', '&Aplica em:', '&Mais Cores...', '&Ajuste...',
  'Favor selecionar uma cor',
  // [apply to:] text, paragraph, table, cell
  'texto', 'parágrafo' , 'tabela', 'célula',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Fonte', 'Fonte', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fonte:', '&Tamanho', 'Estilo', '&Negrito', '&Itálico',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Cor:', 'Plano de &Fundo:', '(Padrão)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efeitos', 'Sublinhado', '&Sobrelinha', 'R&iscado', '&Tudo maiúsculas',
  // Sample, Sample text
  'Exemplo', 'Texto exemplo',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espaçamento Caracteres', '&Espaçamento:', '&Expandido', '&Condensado',
  // Offset group-box, Offset label, Down, Up,
  'Deslocamento', '&Deslocamento:', '&Baixo', '&Cima',
  // Scaling group-box, Scaling
  'Escala', 'Esc&ala:',
  // Sub/super script group box, Normal, Sub, Super
  'Subscritos and superescritos', '&Normal', 'Su&bscrito', 'Su&perescrito',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Ajuste', '&Cima:', '&Esquerda:', '&Baixo:', '&Direita:', 'Valores &iguais',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insere Hiperlink', 'Hiperlink', 'T&exto:', '&Destino', '<<seleção>>',
  // cannot open URL
  'Impossível navegar para "%s"',
  // hyperlink properties button, hyperlink style
  '&Personalizar...', '&Estilo:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Atributos de Hiperlink', 'Cores normal', 'Cores ativo',
  // Text [color], Background [color]
  '&Texto:', '&Fundo:',
  // Underline [color]
  '&Sublinhado:',
  // Text [color], Background [color] (different hotkeys)
  'T&exto:', 'F&undo',
  // Underline [color], Attributes group-box,
  'Su&blinhado:', 'Atributos',
  // Underline type: always, never, active (below the mouse)
  'Sub&linhado:', 'sempre', 'nunca', 'ativo',
  // Same as normal check-box
  'Como &normal',
  // underline active links check-box
  'Sub&linhar links ativos',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insere Símbolo', '&Fonte:', 'Conj.&Caracteres:', 'Unicode',
  // Unicode block
  '&Bloco:',
  // Character Code, Character Unicode Code, No Character
  'Cód.Caractere: %d', 'Cód.Caractere: Unicode %d', '(sem caractere)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Insere Tabela', 'Tamanho Tabela', 'Número de &colunas:', 'Número de &linhas:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Layout Tabela', 'Tamanho &Automático', 'Dimensiona tabela para ajustar na &janela', 'Dimensiona &manualmente',
  // Remember check-box
  'Lembra &dimensões para novas tabelas',
  // VAlign Form ---------------------------------------------------------------
  'Posição',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriedades', 'Imagem', 'Posição e Tamanho', 'Linha', 'Tabela', 'Linhas', 'Células',
  // Image Appearance, Image Misc, Number tabs
  'Aparência', 'Outros', 'Número',
  // Box position, Box size, Box appearance tabs
  'Posição', 'Tamanho', 'Aparência',
  // Preview label, Transparency group-box, checkbox, label
  'Visualiza:', 'Transparência', '&Transparente', '&Cor transparente:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'Al&tera...', '&Salvar...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alinhamento vertical', '&Alinhamento:',
  'baixo da linha de base do texto', 'meio da linha de base do texto',
  'topo com o topo da linha', 'base com a base da linha', 'meio com o meio da linha',
  // align to left side, align to right side
  'lado esquerdo', 'lado direito',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Desloca de:', 'Ajusta', '&Largura:', '&Altura:', 'Tamanho padrão: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Redimensionar &proporcionalmente', '&Restaurar',
  // Inside the border, border, outside the border groupboxes
  'Dentro da borda', 'Borda', 'Fora da borda',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Cor de Preenchimento:', '&Margem interna:',
  // Border width, Border color labels
  '&Largura da borda:', '&Cor da borda:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espaçamento &horizontal:', 'Espaçamento &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Outros', '&Dica de ferramenta:',
  // web group-box, alt text
  'Web', '&Texto alternativo:',
  // Horz line group-box, color, width, style
  'Linha horizontal', '&Cor:', '&Largura:', '&Estilo:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabela', '&Largura:', 'Cor de &preenchimento:', 'Espaçamento das &células...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Margem interna &horiz. célula:', 'Margem interna &vert. célula:', 'Borda e fundo',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Laterais da &borda visíveis...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Manter conteúdo junto',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotação', '&Nenhuma', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Borda:', 'Laterais da &borda visíveis...',
  // Border, CellBorders buttons
  'Borda da &tabela...', 'Bordas das &células...',
  // Table border, Cell borders dialog titles
  'Borda da Tabela', 'Bordas Padrão das Células',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Imprimindo', '&Manter tabela em uma página', 'Número de linhas de &cabeçalho:',
  'Linhas de cabeçalho são repetidas em cada página da tabela',
  // top, center, bottom, default
  '&Topo', '&Centralizado', '&Baixo', '&Padrão',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Configurações', '&Largura Preferencial:', '&Altura mínima:', 'Cor &Preenchimento:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Borda', 'Lados visíveis:',  'C&or sombra:', 'Cor &luz', 'C&or:',
  // Background image
  '&Imagem...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Posição horizontal', 'Posição vertical', 'Posição no texto',
  // Position types: align, absolute, relative
  'Alinhar', 'Posição absoluta', 'Posição relativa',
  // [Align] left side, center, right side - left side of box to left side of
  'lado esquerdo com a esquerda de',
  'centro do quadro com o centro de',
  'lado direito com a direita de',
  // [Align] top side, center, bottom side
  'lado de cima com o lado de cima de',
  'centro do quadro com o centro de',
  'lado de baixo com o lado de baixo de',
  // [Align] relative to
  'relativo a',
  // [Position] to the right of the left side of
  'para a direita do lado esquerdo de',
  // [Position] below the top side of
  'embaixo do lado superior de',
  // Anchors: page, main text area (x2)
  '&Página', '&Área principal de texto', 'Pá&gina', 'Área principal de te&xto',
  // Anchors: left margin, right margin
  'Margem &esquerda', 'Margem &direita',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Margem &superior', 'Margem &inferior', 'Margem &interna', 'Margem &externa',
  // Anchors: character, line, paragraph
  '&Caractere', 'L&inha', 'Pará&grafo',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Margens &espelhadas', 'La&yout em célula de tabela',
  // Above text, below text
  '&Sobre o texto', '&Abaixo do texto',
  // Width, Height groupboxes, Width, Height labels
  'Largura', 'Altura', '&Largura:', '&Altura:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alinhamento vertical', 'Em c&ima', '&Centralizado', 'Em&baixo',
  // Border and background button and title
  'B&orda e fundo...', 'Borda e fundo do quadro',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Colar Especial', '&Colar como:', 'Formato Rich text', 'Formato HTML',
  'Texto', 'Texto Unicode', 'Imagem Bitmap', 'Imagem Metafile',
  'Arquivos gráficos',  'URL',
  // Options group-box, styles
  'Opções', '&Estilos:',
  // style options: apply target styles, use source styles, ignore styles
  'aplicar estilos do documento destino', 'manter estilos e aparência',
  'manter aparência, ignorar estilos',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Marcadores e Numeração', 'Marcadores', 'Numeradores', 'Listas com Marcadores', 'Listas Numeradas',
  // Customize, Reset, None
  '&Personaliza...', '&Restaura', 'Nenhum',
  // Numbering: continue, reset to, create new
  'continua numeração', 'reinicializa numeração em', 'cria uma nova lista, iniciando com',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Letras Maiúsculas', 'Números Romanos Maiúsculos', 'Decimal', 'Letras Minúsculas', 'Números Romanos Minúsculos',
  // Bullet type
  'Círculo', 'Disco', 'Quadrado',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nível:', '&Iniciar em:', '&Continuar', 'Numeração',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personaliza Lista', 'Níveis', '&Quant.:', 'Propriedades da Lista', 'Tipo de &lista:',
  // List types: bullet, image
  'marcador', 'imagem', 'Inserir Número|',
  // Number format, Number, Start level from, Font button
  'Formato de &Números:', 'Números', 'Nível &inicial a partir de :', '&Fonte...',
  // Image button, bullet character, Bullet button,
  '&Imagem...', 'C&aractere Marcador', '&Marcador...',
  // Position of list text, bullet, number, image, text
  'Posição do texto de lista', 'Posição do marcador', 'Posição da numeração', 'Posição da imagem', 'Posição do texto',
  // at, left indent, first line indent, from left indent
  '&em:', 'Recuo &esquerda:', 'Recuo &primeira linha:', 'do recuo esquerdo',
  // [only] one level preview, preview
  'Visualiza &um nível', 'Visualiza',
  // Preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'alinha à esquerda', 'alinha à direita', 'centraliza',
  // level #, this level (for combo-box)
  'Nível %d', 'Este nível',
  // Marker character dialog title
  'Edita Caractere Marcador',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Borda e Fundo do Parágrafo', 'Borda', 'Fundo',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Configurações', '&Cor:', '&Largura:', 'Largura int&erna:', 'D&eslocamento...',
  // Sample, Border type group-boxes;
  'Exemplo', 'Tipo de borda',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nenhum', '&Simples', '&Duplo', '&Triplo', 'Grosso &interno', 'Grosso &externo',
  // Fill color group-box; More colors, padding buttons
  'Cor de preenchimento', '&Mais cores...', '&Margem...',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // title and group-box in Offsets dialog
  'Deslocamento', 'Deslocamento da borda',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Parágrafo', 'Alinhamento', '&Esquerda', '&Direita', '&Centraliza', '&Justifica',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espaçamento', '&Antes:', '&Depois:', 'E&spaçamento de Linha:', 'p&or:',
  // Indents group-box; indents: left, right, first line
  'Recuo', '&Esquerda:', '&Direita:', '&Primeira linha:',
  // indented, hanging
  '&Recuado', '&Deslocado', 'Exemplo',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simples', '1,5 linhas', 'Duplo', 'Pelo menos', 'Exatamente', 'Múltiplo',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Recuos e Espaçamento', 'Tabulações', 'Fluxo de texto',
  // tab stop position label; buttons: set, delete, delete all
  'Posição de &Tabulação:', '&Marca', '&Desmarca', 'Desmarca &Todas',
  // tab align group; left, right, center aligns,
  'Alinhamento', '&Esquerda', '&Direita', '&Centraliza',
  // leader radio-group; no leader
  'Primeira Linha', '(Nenhum)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulações a desmarcar:', '(Nenhuma)', 'Todas.',
  // Pagination group-box, keep with next, keep lines together
  'Paginação', '&Manter com próximo', 'Manter &linhas juntas',
  // Outline level, Body text, Level #
  '&Nível do tópico:', 'Corpo de Texto', 'Nível %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Visualiza Impressão', 'Largura da Página', 'Página Inteira', 'Páginas:',
  // Invalid Scale, [page #] of #
  'Favor entrar com um número entre 10 e 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primeira Página', 'Página Anterior', 'Página Seguinte', 'Última Página',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espaçamento de Células', 'Espaçamento', 'E&ntre células', 'Da borda da tabela até as células',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Bordas', 'Configurações', '&Cor:', 'Cor &Luz:', '&Cor sombra:',
  // Width; Border type group-box;
  '&Largura:', 'Tipo de borda',
  // Border types: none, sunken, raised, flat
  '&Nenhum', '&Solarizado', '&Elevado', '&Liso',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Divide', 'Divide para',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Número e&specificado de linhas e colunas', 'Células &originais',
  // number of columns, rows, merge before
  'Número de &colunas:', 'Número de &linhas:', '&Mescla antes de dividir',
  // to original cols, rows check-boxes
  'Divide para co&lunas originais', 'Divide para li&nhas originais',
  // Add Rows form -------------------------------------------------------------
  'Adiciona Linhas', 'Número de &Linhas:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Configuração de Página', 'Página', 'Cabeçalho e Rodapé',
  // margins group-box, left, right, top, bottom
  'Márgens (milímetros)', 'Márgens (polegadas)', '&Esquerda:', '&Topo:', '&Direita:', '&Baixo:',
  // mirror margins check-box
  'Espelha &margens',
  // orientation group-box, portrait, landscape
  'Orientação', '&Retrato', '&Paisagem',
  // paper group-box, paper size, default paper source
  'Papel', 'T&amanho:', '&Fonte:',
  // header group-box, print on the first page, font button
  'Cabeçalho', '&Texto:', 'C&abeçalho na primeira página', '&Fonte...',
  // header alignments: left, center, right
  '&Esquerda', '&Centraliza', '&Direita',
  // the same for footer (different hotkeys)
  'Rodapé', 'Te&xto:', 'Rodapé na primeira &página', 'F&onte...',
  'E&squerda', 'Ce&ntraliza', 'D&ireita',
  // page numbers group-box, start from
  'Números de página', 'Inicia e&m:',
  // hint about codes
  'Combinações de caracteres especiais:'#13'&&p - número de página; &&P - total de páginas; &&d - data atual; &&t - hora atual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Código de Página do Arqivo de Texto', '&Escolha a codificação do arquivo:',
  // thai, japanese, chinese (simpl), korean
  'Tailandês', 'Japonês', 'Chinês (Simplificado)', 'Coreano',
  // chinese (trad), central european, cyrillic, west european
  'Chinês (Tradicional)', 'Europeu Central e Oriental', 'Cirílico', 'Europeu Ocidental',
  // greek, turkish, hebrew, arabic
  'Greco', 'Turco', 'Hebreu', 'Arábe',
  // baltic, vietnamese, utf-8, utf-16
  'Báltico', 'Vietnamês', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estilo Visual', '&Selecione o estilo:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Converter em Texto', 'Selecione o &delimitador:',
  // line break, tab, ';', ','
  'quebra de linha', 'tabulação', 'ponto-e-vírgula', 'vírgula',
  // Table sort form -----------------------------------------------------------
  // error message
  'Uma tabela com linhas mescladas não pode ser ordenada',
  // title, main options groupbox
  'Ordenar Tabela', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar por coluna:', 'Diferenciar &maiúsculas de minúsculas',
  // heading row, range or rows
  'Excluir linha de &cabeçalho', 'Linhas a serem ordenadas: de %d até %d',
  // order, ascending, descending
  'Ordem', 'C&rescente', '&Decrescente',
  // data type, text, number
  'Tipo', '&Texto', '&Número',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserir Número', 'Propriedades', 'Nome do &contador:', 'Tipo de &numeração:',
  // numbering groupbox, continue, start from
  'Numeração', 'C&ontinuar', '&Começar em:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Legenda', '&Etiqueta:', '&Excluir etiqueta da legenda',
  // position radiogroup
  'Posição', '&Sobre o objeto selecionado', '&Abaixo do objeto selecionado',
  // caption text
  '&Texto da legenda:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numeração', 'Figura', 'Tabela',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Laterais da borda visíveis', 'Borda',
  // Left, Top, Right, Bottom checkboxes
  'Lado &esquerdo', 'Lado de &cima', 'Lado &direito', 'Lado de &baixo',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Redimensiona Coluna da Tabela', 'Redimensiona Linha da Tabela',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Recuo da Primeira Linha', 'Recuo Esquerdo', 'Deslocamento', 'Recuo Direito',
  // Hints on lists: up one level (left), down one level (right)
  'Um nível acima', 'Um nível abaixo',
  // Hints for margins: bottom, left, right and top
  'Margem inferior', 'Margem esquerda', 'Margem direita', 'Margem superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulação à Esquerda', 'Tabulação à Direita', 'Tabulação Centralizada', 'Tabulação Alinhada ao Ponto Decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Recuo Normal', 'Sem espaçamento', 'Cabeçalho %d', 'Lista',
  // Hyperlink, Title, Subtitle
  'Link', 'Título', 'Subtítulo',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Ênfase', 'Ênfase Atenuada', 'Ênfase Acentuada', 'Forte',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citação', 'Citação Acentuada', 'Referência Atenuada', 'Referência Destacada',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Corpo de Texto', 'HTML Variável', 'HTML Código', 'HTML Acrônimo',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Definição', 'HTML Teclado', 'HTML Amostra', 'HTML Máquina de Escrever',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatado', 'HTML Citação', 'Cabeçalho', 'Rodapé', 'Numéro de Página',
  // Caption
  'Legenda',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Referência da Nota de Fim', 'Referência da Nota de Rodapé', 'Texto da Nota de Fim', 'Texto da Nota de Rodapé',
  // Sidenote Reference, Sidenote Text
  'Referência da Nota em Quadro', 'Texto da Nota em Quadro',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'cor', 'cor de fundo', 'transparente', 'padrão', 'cor do sublinhado',
  // default background color, default text color, [color] same as text
  'cor de fundo padrão', 'cor de texto padrão', 'mesma do texto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'simples', 'grossa', 'dupla', 'pontilhada', 'grossa pontilhada', 'com traços',
  // underline types: thick dashed, long dashed, thick long dashed,
  'grossa com traços', 'com traços longos', 'grossa com traços longos',
  // underline types: dash dotted, thick dash dotted,
  'com traços e pontos', 'grossa com traços e pontos',
  // underline types: dash dot dotted, thick dash dot dotted
  'com traços e pontos duplos', 'grossa com traços e pontos duplos',
  // sub/sobrescrito: não, subscito, sobrescrito
  'sem sub/sobrescrito', 'subscrito', 'sobrescrito',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'modo bidirecional:', 'herdado', 'da esquerda para a direita', 'da direita para a esquerda',
  // bold, not bold
  'negrito', 'sem negrito',
  // italic, not italic
  'itálico', 'sem itálico',
  // underlined, not underlined, default underline
  'sublinhado', 'sem sublinhado', 'sublinhado padrão',
  // struck out, not struck out
  'tachado', 'sem tachado',
  // overlined, not overlined
  'sobrelinha', 'sem sobrelinha',
  // all capitals: yes, all capitals: no
  'maiúsculas', 'maiúsculas desligadas',
  // vertical shift: none, by x% up, by x% down
  'sem deslocamento vertical', 'deslocado %d%% para cima', 'deslocado %d%% para baixo',
  // characters width [: x%]
  'largura dos caracteres',
  // character spacing: none, expanded by x, condensed by x
  'espaçamento de caracteres normal', 'espaçamento aumentado %s', 'espaçamento diminuído %s',
  // charset
  'cursivo',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'fonte padrão', 'parágrafo padrão', 'herdado',
  // [hyperlink] highlight, default hyperlink attributes
  'destaque', 'padrão',
  // paragraph aligmnment: left, right, center, justify
  'ainhar à esquerda', 'alinhar à direita', 'centralizado', 'justificado',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altura da linha: %d%%', 'espaço entre linhas: %s', 'altura da linha: pelo menos %s',
  'altura da linha: exatamente %s',
  // no wrap, wrap
  'quebra de linha desativada', 'quebra de linha',
  // keep lines together: yes, no
  'manter linhas juntas', 'não manter linhas juntas',
  // keep with next: yes, no
  'manter com o próximo parágrafo', 'não manter com o próximo parágrafo',
  // read only: yes, no
  'somente leitura', 'editável',
  // body text, heading level x
  'Nível da estrutura de tópicos: corpo do texto', 'Nível da estrutura de tópicos: %d',
  // indents: first line, left, right
  'recuo da primeira linha', 'afastamento pela esquerda', 'afastamento pela direita',
  // space before, after
  'espaço antes', 'espaço depois',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulações', 'nenhuma', 'alinhar', 'esquerda', 'direita', 'centro',
  // tab leader (filling character)
  'caractere de preenchimento',
  // no, yes
  'não', 'sim',
  // [padding/spacing/side:] left, top, right, bottom
  'esquerda', 'superior', 'direita', 'inferior',
  // border: none, single, double, triple, thick inside, thick outside
  'nenhuma', 'simples', 'dupla', 'tripla', 'grossa dentro', 'grossa fora',
  // background, border, padding, spacing [between text and border]
  'fundo', 'borda', 'margem interna', 'espaçamento',
  // border: style, width, internal width, visible sides
  'estilo', 'largura', 'largura interna', 'lados visíveis',
  // style inspector -----------------------------------------------------------
  // title
  'Inspetor de Estilos',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estilo', '(Nenhum)', 'Parágrafo', 'Fonte', 'Atributos',
  // border and background, hyperlink, standard [style]
  'Bordas e preenchimento', 'Link', '(Padrão)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estilos', 'Estilo',
  // name, applicable to,
  '&Nome:', 'Aplicá&vel a:',
  // based on, based on (no &),
  '&Baseado em:', 'Baseado em:',
  //  next style, next style (no &)
  'Estilo para o parágrafo &seguinte:', 'Estilo para o parágrafo seguinte:',
  // quick access check-box, description and preview
  'Acesso &rápido', 'Descrição e &visualização:',
  // [applicable to:] paragraph and text, paragraph, text
  'parágrafos e caracteres', 'parágrafos', 'caracteres',
  // text and paragraph styles, default font
  'Estilos de Parágrafo e de Caracteres', 'Fonte Padrão',
  // links: edit, reset, buttons: add, delete
  'Editar', 'Redefinir', '&Incluir', '&Apagar',
  // add standard style, add custom style, default style name
  'Incluir Estilo &Padrão...', 'Incluir Estilo &Personalizado', 'Estilo %d',
  // choose style
  'Escolher &estilo:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Estilos RichView (*.rvst)|*.rvst', 'Erro carregando arquivo', 'O arquibo não contém estilos',
  // Title, group-box, import styles
  'Importar Estilos de Arquivo', 'Importar', 'I&mportar estilos:',
  // existing styles radio-group: Title, override, auto-rename
  'Estilos existentes', '&sobrescrever', '&incluir com outro nome',
  // Select, Unselect, Invert,
  '&Selecionar', '&Remover seleção', '&Inverter',
  // select/unselect: all styles, new styles, existing styles
  '&Todos os estilos', 'Estilos &Novos', 'Estilos &Existentes',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Limpar Formação)', '(Todos os Estilos)',
  // dialog title, prompt
  'Escolher', '&Escolher estilo a ser aplicado:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sinônimos', '&Ignorar Todos', '&Adicionar ao Dicionário',
  // Progress messages ---------------------------------------------------------
  'Fazendo download de %s', 'Preparando para imprimir...', 'Imprimindo a página %d',
  'Convertendo de RTF...',  'Convertendo para RTF...',
  'Lendo %s...', 'Gravando %s...', 'texto',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Sem nota', 'Nota de rodapé', 'Nota de fim', 'Nota em quadro', 'Quadro de texto'
  );


initialization
  RVA_RegisterLanguage(
    'Portuguese (Brazilian)', // english language name, do not translate
    'Português (Brasileiro)', // native language name
    ANSI_CHARSET, @Messages);

end.
