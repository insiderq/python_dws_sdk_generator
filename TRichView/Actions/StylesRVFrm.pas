unit StylesRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseRVFrm, RVStyle, Buttons, Menus, RVScroll, RichView, StdCtrls, IniFiles,
  CRVData, RVRVData,
  RVALocalize, RichViewActions, CRVFData, RVEdit, RVStyleFuncs,
  ComCtrls, ImgList, RVStr, RVFuncs, RVTypes;

{$IFNDEF RVDONOTUSESTYLETEMPLATES}

type

  TfrmRVStyles = class(TfrmRVBase)
    tv: TTreeView;
    btnOk: TButton;
    btnCancel: TButton;
    gb: TGroupBox;
    lblName: TLabel;
    txtStyleName: TEdit;
    cmbStyleType: TComboBox;
    lblStyleType: TLabel;
    lblParent: TLabel;
    cmbParent: TComboBox;
    lblNext: TLabel;
    cmbNext: TComboBox;
    lblDescr: TLabel;
    rv: TRichView;
    btnImport: TButton;
    rvs: TRVStyle;
    ImageList1: TImageList;
    btnAdd: TBitBtn;
    pmAdd: TPopupMenu;
    mitAddCustom: TMenuItem;
    mitAddStandard: TMenuItem;
    btnDelete: TButton;
    cbQuickAccess: TCheckBox;
    btnExport: TButton;
    rvsImport: TRVStyle;
    rvImport: TRichView;
    procedure FormCreate(Sender: TObject);
    procedure tvChange(Sender: TObject; Node: TTreeNode);
    procedure btnAddClick(Sender: TObject);
    procedure mitAddCustomClick(Sender: TObject);
    procedure cmbParentClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure cmbStyleTypeClick(Sender: TObject);
    procedure mitFontClick(Sender: TObject);
    procedure rvJump(Sender: TObject; id: Integer);
    procedure mitParagraphClick(Sender: TObject);
    procedure mitBorderClick(Sender: TObject);
    procedure txtStyleNameExit(Sender: TObject);
    procedure mitAddStandardClick(Sender: TObject);
    procedure mitHyperlinkClick(Sender: TObject);
    procedure cmbNextClick(Sender: TObject);
    procedure cbQuickAccessClick(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
  private
    { Private declarations }
    _txtStyleName, _cmbStyleType, _cmbParent, _cmbNext, _btnDelete,
    _btnAdd, _cbQuickAccess, _btnOk, _tv: TControl;
    _pmAdd: TComponent;
    CurrentStyleTemplate: TRVStyleTemplate;
    CurrentNode: TRVATreeNode;
    SaveDialog: TRVASaveDialog;
    OpenDialog: TRVAOpenDialog;
    function GetStyleTemplates: TRVStyleTemplateCollection;
    procedure DisplayStyle;
    procedure UpdateParentCombo;
    procedure UpdateNextCombo;
    procedure MakePreview;
  protected
    OriginalRVStyle: TRVStyle;
    FStandardStyleTemplates: TRVStyleTemplateCollection;
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    procedure OnCreateThemedMenu(OldMenu, NewMenu: TComponent); override;
    {$ENDIF}
  public
    { Public declarations }
    UseCustomImages: Boolean;
    ImageIndices: TRVStyleTemplateImageIndices;
    Images: TCustomImageList;
    FontImageIndex, HyperlinkImageIndex, ParaImageIndex, ParaBorderImageIndex: Integer;
    RootImageIndex: Integer;
    procedure Init(rv: TCustomRichViewEdit;
      StandardStyleTemplates: TRVStyleTemplateCollection);
    procedure BuildTree(SelectedStyleTemplate: TRVStyleTemplate);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    property StyleTemplates: TRVStyleTemplateCollection
      read GetStyleTemplates;
  end;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSESTYLETEMPLATES}

uses
  {$IFDEF RICHVIEWDEF10}
  Types,
  {$ENDIF}
  FontRVFrm, ParaRVFrm, ParaBrdrRVFrm, ListRVFrm, StyleImportRVFrm,
  HypHoverPropRVFrm,
  Math;

{$R *.DFM}

{==============================================================================}
function StyleTemplatesToTreeView(StyleTemplates: TRVStyleTemplateCollection;
  tv: TRVATreeView; const ImageIndices: TRVStyleTemplateImageIndices;
  SelectedTemplate: TRVStyleTemplate; ControlPanel: TRVAControlPanel): TRVATreeNode;
var SelectedNode: TRVATreeNode;
    RootNode, TextNode: TRVATreeNode;
  {................................................................}
  procedure AddNode(StyleTemplate: TRVStyleTemplate; ParentNode: TRVATreeNode);
  var Node: TRVATreeNode;
      i: Integer;
  begin
    if (StyleTemplate.Kind = rvstkText) and (StyleTemplate.Parent=nil) then begin
      if TextNode=nil then begin
        TextNode := tv.Items.AddChildObject(ParentNode, RVA_GetS(rvam_st_DefFont, ControlPanel), nil);
        TextNode.ImageIndex := ImageIndices[rvstkText];
        TextNode.SelectedIndex := ImageIndices[rvstkText];
      end;
      ParentNode := TextNode;
    end;

    Node := tv.Items.AddChildObject(ParentNode,
      RVGetStyleName(StyleTemplate.Name, True, ControlPanel), StyleTemplate);
    if StyleTemplate=SelectedTemplate then
      SelectedNode := Node;
    Node.ImageIndex := ImageIndices[StyleTemplate.Kind];
    Node.SelectedIndex := ImageIndices[StyleTemplate.Kind];
    if StyleTemplate.Children <> nil then
      for i := 0 to StyleTemplate.Children.Count-1 do
        AddNode(TRVStyleTemplate(StyleTemplate.Children.Items[i]), Node);
  end;
  {................................................................}
var i: Integer;
begin
  SelectedNode := nil;
  tv.Items.BeginUpdate;
  try
    tv.Items.Clear;
    RootNode := tv.Items.Add(nil, RVA_GetS(rvam_st_ParaAndTextStyles, ControlPanel));
    TextNode := nil;
    for i := 0 to StyleTemplates.Count-1 do
      if StyleTemplates[i].Parent=nil then
        AddNode(StyleTemplates[i], RootNode);
    RootNode.AlphaSort{$IFDEF RICHVIEWDEF6}(True){$ENDIF};
  finally
    tv.Items.EndUpdate;
  end;
  Result := SelectedNode;
end;

function GetKindsForComboxes(Kind: TRVStyleTemplateKind): TRVStyleTemplateKinds;
begin
  case Kind of
    rvstkParaText, rvstkPara:
      Result := [rvstkParaText, rvstkPara];
    else //rvstkText:
      Result := [rvstkText];
  end;
end;

{================================= TfrmRVStyles ===============================}
procedure TfrmRVStyles.UpdateParentCombo;
var SL: TRVALocStringList;
    Index: Integer;
begin
  SL := TRVALocStringList.Create;
  try
    RVStyleTemplatesToStrings(StyleTemplates, SL, True,
      GetKindsForComboxes(CurrentStyleTemplate.Kind), CurrentStyleTemplate,
      True, ControlPanel);
    SL.Sort;
    SL.Insert(0, RVA_GetS(rvam_si_NoStyle, ControlPanel));
    SetXBoxItems(_cmbParent, SL);
    Index := RVFindStyleTemplateInStrings(SL, CurrentStyleTemplate.Parent);
    If Index<0 then
      Index := 0;
    SetXBoxItemIndex(_cmbParent, Index);
  finally
    SL.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.UpdateNextCombo;
var SL: TRVALocStringList;
    Next: TRVStyleTemplate;
begin
  if CurrentStyleTemplate.Kind<>rvstkText then begin
    SL := TRVALocStringList.Create;
    try
      RVStyleTemplatesToStrings(StyleTemplates, SL, True,
        GetKindsForComboxes(CurrentStyleTemplate.Kind), nil, False, ControlPanel);
      SL.Sort;
      Next := CurrentStyleTemplate.Next;
      if Next=nil then
        Next := CurrentStyleTemplate;
      SetXBoxItems(_cmbNext, SL);
      SetXBoxItemIndex(_cmbNext, RVFindStyleTemplateInStrings(SL, Next));
    finally
      SL.Free;
    end;
    end
  else
    ClearXBoxItems(_cmbNext);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.DisplayStyle;
var StyleType: TRVStandardStyle;
    Level: Integer;
    Standard: Boolean;
begin
  if CurrentStyleTemplate<>nil then
    Standard := RVGetStyleType(CurrentStyleTemplate.Name, StyleType, Level)
  else begin
    Standard := True;
    StyleType := rvssnNormal;
    Level := -1;
  end;
  _txtStyleName.Enabled := not Standard;
  _cmbStyleType.Enabled := (CurrentStyleTemplate<>nil) and (CurrentStyleTemplate.Children=nil);
  _cmbParent.Enabled := (CurrentStyleTemplate<>nil) and not (Standard and (StyleType=rvssnNormal));
  _cmbNext.Enabled := (CurrentStyleTemplate<>nil) and (CurrentStyleTemplate.Kind<>rvstkText);
  _btnDelete.Enabled := (CurrentStyleTemplate<>nil) and not (Standard and (StyleType=rvssnNormal));
  _cbQuickAccess.Enabled := CurrentStyleTemplate<>nil;
  rv.Clear;
  if CurrentStyleTemplate=nil then begin
    SetControlCaption(_txtStyleName, '');
    SetXBoxItemIndex(_cmbStyleType, -1);
    ClearXBoxItems(_cmbParent);
    ClearXBoxItems(_cmbNext);
    end
  else begin
    if Standard then
      case StyleType of
        rvssnHeadingN:
          begin
            CurrentStyleTemplate.ValidParaProperties :=
              CurrentStyleTemplate.ValidParaProperties+[rvpiOutlineLevel];
            CurrentStyleTemplate.ParaStyle.OutlineLevel := Level;
          end;
      end;
    if Standard then
      SetControlCaption(_txtStyleName, RVGetStandardStyleName(StyleType, Level, ControlPanel))
    else
      SetControlCaption(_txtStyleName, {$IFDEF USERVTNT}GetTNTStr{$ENDIF}(CurrentStyleTemplate.Name));
    SetCheckBoxChecked(_cbQuickAccess, CurrentStyleTemplate.QuickAccess);
    SetXBoxItemIndex(_cmbStyleType, ord(CurrentStyleTemplate.Kind));
    UpdateParentCombo;
    UpdateNextCombo;
    MakePreview;
  end;
  rv.Format;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.MakePreview;
begin
  RVDisplayStyleTemplate(CurrentStyleTemplate, rvs, rv, Images,
    FontImageIndex, HyperlinkImageIndex, ParaImageIndex, ParaBorderImageIndex,
    True, ControlPanel);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.Init(rv: TCustomRichViewEdit;
  StandardStyleTemplates: TRVStyleTemplateCollection);
begin
  FStandardStyleTemplates := StandardStyleTemplates;
  OriginalRVStyle := rv.Style;
  rvs.StyleTemplates.AssignStyleTemplates(OriginalRVStyle.StyleTemplates, True);
  rvs.Units := rv.Style.Units;  
  StyleTemplates.ResetNameCounter;
  CurrentStyleTemplate := StyleTemplates.FindItemById(rv.Style.TextStyles[rv.CurTextStyleNo].StyleTemplateId);
  if CurrentStyleTemplate=nil then
    CurrentStyleTemplate := StyleTemplates.FindItemById(rv.Style.ParaStyles[rv.CurParaStyleNo].StyleTemplateId);
  BuildTree(CurrentStyleTemplate);
end;
{------------------------------------------------------------------------------}
function TfrmRVStyles.GetStyleTemplates: TRVStyleTemplateCollection;
begin
  Result := rvs.StyleTemplates;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.BuildTree(SelectedStyleTemplate: TRVStyleTemplate);
var TreeView: TRVATreeView;
begin
  if not UseCustomImages then begin
    ImageIndices[rvstkParaText] := 1;
    ImageIndices[rvstkText] := 2;
    ImageIndices[rvstkPara] := 3;
  end;
  TreeView := _tv as TRVATreeView;
  CurrentNode := StyleTemplatesToTreeView(rvs.StyleTemplates, TreeView, ImageIndices,
    SelectedStyleTemplate, TRVAControlPanel(ControlPanel));
  TreeView.Items[0].ImageIndex := RootImageIndex;
  TreeView.Items[0].SelectedIndex := RootImageIndex;
  TreeView.Items[0].Expand(True);
  if CurrentNode<>nil then
    TreeView.Selected := CurrentNode
  else if TreeView.Items.Count>1 then
    TreeView.Selected := TreeView.Items[1]
  else
    TreeView.Selected := TreeView.Items[0];
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.FormCreate(Sender: TObject);
var i: Integer;
begin
  _txtStyleName := txtStyleName;
  _cmbStyleType := cmbStyleType;
  _cmbParent := cmbParent;
  _cmbNext := cmbNext;
  _btnDelete := btnDelete;
  _btnAdd := btnAdd;
  _cbQuickAccess := cbQuickAccess;
  _btnOk := btnOk;
  _tv := tv;
  _pmAdd := pmAdd;
  inherited;
  for i := 0 to rvs.TextStyles.Count-1 do begin
    rvs.TextStyles[i].FontName := TRVAControlPanel(ControlPanel).DialogFontName;
    rvs.TextStyles[i].Size := TRVAControlPanel(ControlPanel).DialogFontSize;
    {$IFDEF USERVTNT}
    rvs.TextStyles[i].Unicode := True;
    {$ENDIF}
  end;
  rvs.TextStyles[rvs.TextStyles.Count-1].Size := Round(TRVAControlPanel(ControlPanel).DialogFontSize*3/2);
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVStyles.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _txtStyleName then
    _txtStyleName := NewControl
  else if OldControl = _cmbStyleType then
    _cmbStyleType := NewControl
  else if OldControl = _cmbParent then
    _cmbParent := NewControl
  else if OldControl = _cmbNext then
    _cmbNext := NewControl
  else if OldControl = _btnDelete then
    _btnDelete := NewControl
  else if OldControl = _btnAdd then
    _btnAdd := NewControl
  else if OldControl = _cbQuickAccess then
    _cbQuickAccess := NewControl
  else if OldControl = _btnOk then
    _btnOk := NewControl
  else if OldControl = _tv then
    _tv := NewControl
  else
    inherited;
end;

procedure TfrmRVStyles.OnCreateThemedMenu(OldMenu, NewMenu: TComponent);
begin
  if OldMenu = _pmAdd then
    _pmAdd := NewMenu
  else
    inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.tvChange(Sender: TObject; Node: TTreeNode);
begin
  if (CurrentStyleTemplate<>nil) and TWinControl(_txtStyleName).Focused then begin
    (_tv as TRVATreeView).SetFocus;
  end;
  CurrentNode := Node as TRVATreeNode;
  if (Node=nil) then
    CurrentStyleTemplate := nil
  else
    CurrentStyleTemplate := TRVStyleTemplate(Node.Data);
  if CurrentStyleTemplate=nil then
    CurrentNode := nil;
  DisplayStyle;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.btnAddClick(Sender: TObject);
var pt: TPoint;
begin
  pt := _btnAdd.ClientToScreen(Point(0, _btnAdd.Height));
  (_pmAdd as TRVADialogPopupMenu).Popup(pt.X, pt.Y);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.mitAddCustomClick(Sender: TObject);
var ST: TRVStyleTemplate;
begin
  ST := CurrentStyleTemplate;
  CurrentStyleTemplate := TRVStyleTemplate(StyleTemplates.Add);
  if ST<>nil then begin
    if ST.Kind=rvstkText then
      CurrentStyleTemplate.Kind := rvstkText
    else
      CurrentStyleTemplate.Kind := rvstkParaText;
    CurrentStyleTemplate.ParentId := ST.Id;
    end
  else if StyleTemplates.NormalStyleTemplate<>nil then
    CurrentStyleTemplate.ParentId := StyleTemplates.NormalStyleTemplate.Id;
  BuildTree(CurrentStyleTemplate);
  if _txtStyleName is  TWinControl then
    TWinControl(_txtStyleName).SetFocus;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.cmbParentClick(Sender: TObject);
var Index: Integer;
begin
  Index := GetXBoxItemIndex(_cmbParent);
  if Index<0 then
    exit;
  if Index=0 then
    CurrentStyleTemplate.ParentId := -1
  else
    CurrentStyleTemplate.ParentId := TRVStyleTemplate(GetXBoxObject(_cmbParent, Index)).Id;
  BuildTree(CurrentStyleTemplate);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.cmbNextClick(Sender: TObject);
var Index: Integer;
    StyleTemplateId : TRVStyleTemplateId;
begin
  Index := GetXBoxItemIndex(_cmbNext);
  if Index<0 then
    StyleTemplateId := -1
  else
    StyleTemplateId := TRVStyleTemplate(GetXBoxObject(_cmbNext, Index)).Id;
  if StyleTemplateId=CurrentStyleTemplate.Id then
    StyleTemplateId := -1;
  CurrentStyleTemplate.NextId := StyleTemplateId;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.btnDeleteClick(Sender: TObject);
var ST: TRVStyleTemplate;
    i: Integer;
begin
  if CurrentStyleTemplate=nil then
    exit;
  if CurrentStyleTemplate.Children<>nil then
    for i := CurrentStyleTemplate.Children.Count-1 downto 0 do
      TRVStyleTemplate(CurrentStyleTemplate.Children[i]).ParentId := CurrentStyleTemplate.ParentId;
  StyleTemplates.ForbiddenIds.Add(CurrentStyleTemplate.Id);
  ST := CurrentStyleTemplate;
  CurrentStyleTemplate := ST.Parent;
  ST.Free;
  BuildTree(CurrentStyleTemplate);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.cbQuickAccessClick(Sender: TObject);
begin
  if CurrentStyleTemplate<>nil then
    CurrentStyleTemplate.QuickAccess := GetCheckBoxChecked(_cbQuickAccess);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.cmbStyleTypeClick(Sender: TObject);
var Index: Integer;
    NewKind, ParentKind: TRVStyleTemplateKind;
begin
  Index := GetXBoxItemIndex(_cmbStyleType);
  if (Index<0) or (CurrentStyleTemplate=nil) then
    exit;
  NewKind := TRVStyleTemplateKind(Index);
  if (NewKind=CurrentStyleTemplate.Kind) then
    exit;
  CurrentStyleTemplate.Kind := NewKind;
  if CurrentStyleTemplate.Parent<>nil then begin
    ParentKind := CurrentStyleTemplate.Parent.Kind;
    if ((NewKind in [rvstkPara, rvstkParaText]) and (ParentKind=rvstkText)) then begin
      if StyleTemplates.NormalStyleTemplate=nil then
        CurrentStyleTemplate.ParentId := -1
      else
        CurrentStyleTemplate.ParentId := StyleTemplates.NormalStyleTemplate.Id;
      end
    else if (NewKind = rvstkText) and (ParentKind<>rvstkText) then
      CurrentStyleTemplate.ParentId := -1;
  end;
  BuildTree(CurrentStyleTemplate);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.mitFontClick(Sender: TObject);
var frm: TfrmRVFont;
begin
  if CurrentStyleTemplate=nil then
    exit;
  frm := TfrmRVFont.Create(Application, ControlPanel);
  try
    StyleTemplateToFontForm(frm, CurrentStyleTemplate, rvs, ControlPanel);
    if frm.ShowModal=mrOk then begin
      StyleTemplateFromFontForm(frm, CurrentStyleTemplate, rvs, ControlPanel);
      DisplayStyle;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.mitParagraphClick(Sender: TObject);
var frm: TfrmRVPara;
    TabsWereDefined: Boolean;
begin
  if (CurrentStyleTemplate=nil) or (CurrentStyleTemplate.Kind=rvstkText) then
    exit;
  frm := TfrmRVPara.Create(Application, ControlPanel);
  try
    StyleTemplateToParaForm(frm, CurrentStyleTemplate, rvs, ControlPanel);
    frm.SimpleTabManagement := True;
    TabsWereDefined := rvpiTabs in CurrentStyleTemplate.ValidParaProperties;
    if frm.ShowModal=mrOk then begin
      StyleTemplateFromParaForm(frm, CurrentStyleTemplate, rvs, TabsWereDefined,
        ControlPanel);
      DisplayStyle;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.mitBorderClick(Sender: TObject);
var frm: TfrmRVParaBrdr;
begin
  if (CurrentStyleTemplate=nil) or (CurrentStyleTemplate.Kind=rvstkText) then
    exit;
  frm := TfrmRVParaBrdr.Create(Application, ControlPanel);
  try
    StyleTemplateToParaBorderForm(frm, CurrentStyleTemplate, rvs, ControlPanel);
    if frm.ShowModal=mrOk then begin
      StyleTemplateFromParaBorderForm(frm, CurrentStyleTemplate, rvs, ControlPanel);
      DisplayStyle;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.mitHyperlinkClick(Sender: TObject);
var frm: TfrmRVHypHoverProp;
begin
  if CurrentStyleTemplate=nil then
    exit;
  frm := TfrmRVHypHoverProp.Create(Application, ControlPanel);
  try
    StyleTemplateToHyperlinkForm(frm, CurrentStyleTemplate, rvs);
    if frm.ShowModal=mrOk then begin
      StyleTemplateFromHyperlinkForm(frm, CurrentStyleTemplate, rvs);
      DisplayStyle;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.rvJump(Sender: TObject; id: Integer);
var RVData: TCustomRVFormattedData;
    ItemNo: Integer;
begin
  if CurrentStyleTemplate=nil then
    exit;
  rv.GetJumpPointLocation(id, RVData, ItemNo);
  case StrToInt({$IFDEF RVOLDTAGS}PChar{$ENDIF}(rv.GetItemTag(ItemNo))) of
    1:
      mitFontClick(nil);
    2:
      begin
        if (CurrentStyleTemplate.Parent<>nil) or (CurrentStyleTemplate.Kind=rvstkText) then
        CurrentStyleTemplate.ValidTextProperties :=
          CurrentStyleTemplate.ValidTextProperties-RVFontDialogProps
        else begin
         CurrentStyleTemplate.ValidTextProperties :=
          CurrentStyleTemplate.ValidTextProperties+RVFontDialogProps;
          CurrentStyleTemplate.TextStyle.ResetMainProperties;
        end;
        DisplayStyle;
      end;
    3:
      mitParagraphClick(nil);
    4:
      begin
        if CurrentStyleTemplate.Parent<>nil then
          CurrentStyleTemplate.ValidParaProperties :=
            CurrentStyleTemplate.ValidParaProperties - RVParaDialogProps
        else begin
          CurrentStyleTemplate.ValidParaProperties :=
            CurrentStyleTemplate.ValidParaProperties + RVParaDialogProps;
          CurrentStyleTemplate.ParaStyle.ResetMainProperties;
        end;
        DisplayStyle;
      end;
    5:
      mitBorderClick(nil);
    6:
      begin
        if CurrentStyleTemplate.Parent<>nil then
          CurrentStyleTemplate.ValidParaProperties :=
            CurrentStyleTemplate.ValidParaProperties - RVParaBorderDialogProps
        else begin
          CurrentStyleTemplate.ValidParaProperties :=
            CurrentStyleTemplate.ValidParaProperties + RVParaBorderDialogProps;
          CurrentStyleTemplate.ParaStyle.ResetBorderAndBackProperties;
        end;
        DisplayStyle;
      end;
    7:
      mitHyperlinkClick(nil);
    8:
      begin
        if (CurrentStyleTemplate.Parent<>nil) or (CurrentStyleTemplate.Kind=rvstkText) then
        CurrentStyleTemplate.ValidTextProperties :=
          CurrentStyleTemplate.ValidTextProperties-RVFontHyperlinkProps
        else begin
         CurrentStyleTemplate.ValidTextProperties :=
          CurrentStyleTemplate.ValidTextProperties-RVFontHyperlinkProps;
          CurrentStyleTemplate.TextStyle.ResetHoverProperties;
        end;
        DisplayStyle;
      end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVStyles.txtStyleNameExit(Sender: TObject);
var StyleType: TRVStandardStyle;
  i, Level: Integer;
  Standard: Boolean;
  //Changed: Boolean;
  s: TRVALocString;
  Index: Integer;
begin
  if CurrentStyleTemplate=nil then
    exit;
  //Changed := False;
  s := Trim(GetEditText(_txtStyleName));
  for i := 1 to Length(s) do
    case s[i] of
      '\': begin s[i] := '/'; {Changed:= True;} end;
      '{': begin s[i] := '('; {Changed:= True;} end;
      '}': begin s[i] := ')'; {Changed:= True;} end;
    end;
  if s='' then begin
    s := StyleTemplates.GetUniqueName(CurrentStyleTemplate);
    //Changed := True;
  end;
  if s=RVDEFAULTPARAGRAPHFONTSTYLENAME then
    s := s+'_';
  Standard := RVGetStyleType(s, StyleType, Level);
  if Standard then begin
    s := s+'_';
    //Changed := True;
  end;
  repeat
    Index := StyleTemplates.FindByName(s);
    if (Index>=0) and (StyleTemplates[Index]<>CurrentStyleTemplate) then begin
      s := s+'_';
      //Changed := True;
    end;
  until (Index<0) or (StyleTemplates[Index]=CurrentStyleTemplate);
  if CurrentStyleTemplate.Name<>s then begin
    CurrentStyleTemplate.Name := s;
    if CurrentNode<>nil then
      CurrentNode.Text := s;
    //DisplayStyle;
  end;
  UpdateNextCombo;
end;
{------------------------------------------------------------------------------}
type
  TStyleItem = class
    public
      Style: TRVStandardStyle;
      Level: Integer;
  end;

procedure TfrmRVStyles.mitAddStandardClick(Sender: TObject);
var Info: TRVStandardStylesRec;
    frm: TfrmRVList;
    i: TRVStandardStyle;
    Level: Byte;
    j: Integer;
    Item: TStyleItem;
    {.............................................}
    procedure AddItem;
    begin
      Item := TStyleItem.Create;
      Item.Style := i;
      Item.Level := Level;
      frm.XBoxItemsAddObject(frm._lst, RVGetStandardStyleName(i, Level, ControlPanel), Item);
    end;
    {.............................................}
begin
  RVFillStandardStyleInfo(StyleTemplates, Info);
  frm := TfrmRVList.Create(Application, ControlPanel);
  try
    frm.Caption := Caption;
    frm.SetControlCaption(frm._lbl, RVA_GetS(rvam_st_ChooseStyle, ControlPanel));
    frm.SetListBoxSorted(frm._lst);
    for i := Low(TRVStandardStyle) to High(TRVStandardStyle) do
      if not (i in Info.Styles) or (i=rvssnHeadingN) then begin
        Level := 0;
        case i of
          rvssnHeadingN:
            begin
              for Level := 1 to 9 do
                if not (Level in Info.HeadingLevels) then
                  AddItem;
            end;
          else
            AddItem;
        end;
      end;
    if frm.GetXBoxItemCount(frm._lst)>0 then begin
      frm.SetXBoxItemIndex(frm._lst, 0);
      if frm.ShowModal=mrOk then begin
        Item := frm.GetXBoxObject(frm._lst, frm.GetXBoxItemIndex(frm._lst)) as TStyleItem;
        if not (Item.Style in [rvssnNormal, rvssnNoSpacing]) and (StyleTemplates.NormalStyleTemplate=nil) then
          RVSetStandardStyleDefaults(rvssnNormal, 0, StyleTemplates.Add, rvs, nil,
            FStandardStyleTemplates, ControlPanel);
        RVSetStandardStyleDefaults(Item.Style, Item.Level, StyleTemplates.Add,
          rvs, nil, FStandardStyleTemplates, ControlPanel);
        StyleTemplates.ResetNameCounter;
        BuildTree(StyleTemplates[StyleTemplates.Count-1]);
      end;
    end;
  finally
    for j := 0 to frm.GetXBoxItemCount(frm._lst)-1 do
      frm.GetXBoxObject(frm._lst, j).Free;
    frm.Free;
  end;
end;


procedure TfrmRVStyles.Localize;
var i: Integer;
begin
  inherited;
  Caption := RVA_GetS(rvam_st_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_st_GBStyle, ControlPanel);
  lblName.Caption := RVA_GetSAsIs(rvam_st_Name, ControlPanel);
  lblStyleType.Caption := RVA_GetSAsIs(rvam_st_ApplicableTo, ControlPanel);
  lblParent.Caption := RVA_GetSAsIs(rvam_st_Parent, ControlPanel);
  lblNext.Caption := RVA_GetSAsIs(rvam_st_Next, ControlPanel);
  cbQuickAccess.Caption := RVA_GetSAsIs(rvam_st_QuickAccess, ControlPanel);
  lblDescr.Caption := RVA_GetSAsIs(rvam_st_Descr, ControlPanel);
  cmbStyleType.Items[0] := RVA_GetSAsIs(rvam_st_ParaTextStyle, ControlPanel);
  cmbStyleType.Items[1] := RVA_GetSAsIs(rvam_st_ParaStyle, ControlPanel);
  cmbStyleType.Items[2] := RVA_GetSAsIs(rvam_st_TextStyle, ControlPanel);
  btnAdd.Caption := RVA_GetSAsIs(rvam_st_Add, ControlPanel);
  btnDelete.Caption := RVA_GetSAsIs(rvam_st_Delete, ControlPanel);
  mitAddStandard.Caption := RVA_GetSAsIs(rvam_st_AddStandard, ControlPanel);
  mitAddCustom.Caption := RVA_GetSAsIs(rvam_st_AddCustom, ControlPanel);
  rvs.StyleTemplates.DefStyleName := RVA_GetSAsIs(rvam_st_DefStyleName, ControlPanel);
  btnImport.Caption := RVA_GetSAsIs(rvam_st_Import, ControlPanel);
  btnExport.Caption := RVA_GetSAsIs(rvam_st_Export, ControlPanel);
  for i := 0 to rvs.TextStyles.Count-1 do
    rvs.TextStyles[i].Charset := RVA_GetCharset(ControlPanel);
  if (RVA_GetCharset(ControlPanel)=ARABIC_CHARSET) or
    (RVA_GetCharset(ControlPanel)=HEBREW_CHARSET) then begin
    rv.BiDiMode := rvbdRightToLeft;
    for i := 0 to rvs.ParaStyles.Count-1 do
      if rvs.ParaStyles[i].Alignment=rvaLeft then
        rvs.ParaStyles[i].Alignment := rvaRight;
  end;
end;

const INISECTION = 'Styles';

procedure TfrmRVStyles.btnExportClick(Sender: TObject);
var Ini: TIniFile;
begin
  if SaveDialog=nil then begin
   SaveDialog := TRVASaveDialog.Create(Self);
   if TRVAControlPanel(ControlPanel).RVFLocalizable then begin
     SaveDialog.DefaultExt := sRVStylesExtension;
     SaveDialog.Filter := RVA_GetS(rvam_sti_RVStylesFilter, ControlPanel);
     end
   else begin
     SaveDialog.DefaultExt := TRVAControlPanel(ControlPanel).RVStylesExt;
     SaveDialog.Filter := TRVAControlPanel(ControlPanel).RVStylesFilter;
   end;
   SaveDialog.Options := [ofHideReadOnly, ofPathMustExist, ofEnableSizing, ofNoReadOnlyReturn, ofOverwritePrompt];
  end;
  if SaveDialog.Execute then begin
    ini := TIniFile.Create(SaveDialog.FileName);
    try
      ini.EraseSection(INISECTION);
      StyleTemplates.SaveToINI(ini, INISECTION);
      ini.WriteInteger(INISECTION, RVINI_UNITS, ord(rvs.Units));
    finally
      ini.Free;
    end;
  end;
end;

procedure TfrmRVStyles.btnImportClick(Sender: TObject);
var frm: TfrmRVStyleImport;
    StyleKind: TRVStyleTemplateKind;
    r: Boolean;
    {........................................................}
    function LoadRVSt: Boolean;
    var Ini: TIniFile;
    begin
      try
        ini := TIniFile.Create(OpenDialog.FileName);
        try
          rvsImport.Units := TRVStyleUnits(ini.ReadInteger(INISECTION, RVINI_UNITS, ord(rvstuPixels)));
          rvsImport.StyleTemplates.LoadFromIni(ini, INISECTION);
        finally
          ini.Free;
        end;
        Result := True;
      except
        Result := False;
      end;
    end;
    {........................................................}
    procedure InitImport;
    begin
      if Screen.ActiveForm<>nil then
        Screen.ActiveForm.Update;
      rvsImport.Units := rvs.Units;
      rvsImport.TextStyles.Clear;
      rvsImport.ParaStyles.Clear;
      {$IFNDEF RVDONOTUSELISTS}
      rvsImport.ListStyles.Clear;
      {$ENDIF}
      rvsImport.TextStyles.Add;
      rvsImport.ParaStyles.Add;
      rvsImport.StyleTemplates.Clear;
      rvImport.Clear;
      rvImport.RVFOptions := rvImport.RVFOptions - [rvfoCanChangeUnits];
    end;
    {........................................................}
    procedure FinalizeImport;
    begin
      rvImport.Clear;
    end;
    {........................................................}
    function LoadRVF: Boolean;
    begin
      InitImport;
      Result := rvImport.LoadRVF(OpenDialog.FileName);
      FinalizeImport;
    end;
    {........................................................}
    function LoadRTF: Boolean;
    begin
      InitImport;
      Result := rvImport.LoadRTF(OpenDialog.FileName);
      FinalizeImport;
    end;
    {........................................................}
    procedure MergeOverride;
    var i: Integer;
      ST, ST2, ST3: TRVStyleTemplate;
    begin
      // adding & replacing
      for i := 0 to frm.GetXBoxItemCount(frm._lst)-1 do
        if frm.GetListBoxChecked(frm._lst, i) then begin
          ST := TRVStyleTemplate(GetXBoxObject(frm._lst, i));
          ST2 := StyleTemplates.FindItemByName(ST.Name);
          if ST2=nil then
            StyleTemplates.Add.Assign(ST)
          else
            ST2.Assign(ST);
        end;
      // restoring references
      for i := 0 to frm.GetXBoxItemCount(frm._lst)-1 do
        if frm.GetListBoxChecked(frm._lst, i) then begin
          ST := TRVStyleTemplate(GetXBoxObject(frm._lst, i));
          ST2 := StyleTemplates.FindItemByName(ST.Name);
          if ST.Parent<>nil then begin
            ST3 := StyleTemplates.FindItemByName(ST.Parent.Name);
            if ST3<>nil then
              ST2.ParentId := ST3.Id;
          end;
          if (ST.Next<>nil) and (ST.Next<>ST) then begin
            ST3 := StyleTemplates.FindItemByName(ST.Next.Name);
            if ST3<>nil then
              ST2.NextId := ST3.Id;
          end;
        end;
    end;
    {........................................................}
    procedure MergeRename;
    var i, idx, OldCount: Integer;
        Name: TRVStyleTemplateName;
      ST, ST2, ST3: TRVStyleTemplate;
    begin
      OldCount := StyleTemplates.Count;
      // adding
      for i := 0 to frm.GetXBoxItemCount(frm._lst)-1 do
        if frm.GetListBoxChecked(frm._lst, i) then begin
          ST := TRVStyleTemplate(GetXBoxObject(frm._lst, i));
          Name := StyleTemplates.MakeNameUnique(ST.Name);
          StyleTemplates.Add.Assign(ST);
          StyleTemplates[StyleTemplates.Count-1].Name := Name;
        end;
      // restoring references
      idx := 0;
      for i := 0 to frm.GetXBoxItemCount(frm._lst)-1 do
        if frm.GetListBoxChecked(frm._lst, i) then begin
          ST := TRVStyleTemplate(GetXBoxObject(frm._lst, i));
          ST2 := StyleTemplates[OldCount+idx];
          if ST.Parent<>nil then begin
            ST3 := StyleTemplates.FindItemByName(ST.Parent.Name);
            if ST3<>nil then
              ST2.ParentId := ST3.Id
            {
            else begin
              Index := frm.GetIndexInCheckedItems(frm.GetStyleTemplateIndex(ST.Parent));
              if Index>=0 then begin
                ST3 := StyleTemplates[OldCount+Index];
                ST2.ParentId := ST3.Id
              end;
            end;
            }
          end;
          if ST.Next<>nil then begin
            ST3 := StyleTemplates.FindItemByName(ST.Next.Name);
            if ST3<>nil then
              ST2.ParentId := ST3.Id
            {
            else begin
              Index := frm.GetIndexInCheckedItems(frm.GetStyleTemplateIndex(ST.Next));
              if Index>=0 then begin
                ST3 := StyleTemplates[OldCount+Index];
                ST2.NextId := ST3.Id
              end;
            end;
            }
          end;
          inc(idx);
        end;
    end;
    {........................................................}
begin
  if OpenDialog=nil then begin
   OpenDialog := TRVAOpenDialog.Create(Self);
   if TRVAControlPanel(ControlPanel).RVFLocalizable then begin
     OpenDialog.DefaultExt := sRVStylesExtension;
     OpenDialog.Filter := RVA_GetS(rvam_sti_RVStylesFilter, ControlPanel)+'|'+
       RVA_GetS(rvam_flt_RVF, ControlPanel)+'|'+RVA_GetS(rvam_flt_RTF, ControlPanel);
     end
   else begin
     OpenDialog.DefaultExt := TRVAControlPanel(ControlPanel).RVStylesExt;
     OpenDialog.Filter := TRVAControlPanel(ControlPanel).RVStylesFilter+'|'+
       TRVAControlPanel(ControlPanel).RVFFilter+'|'+RVA_GetS(rvam_flt_RTF, ControlPanel);
   end;
   OpenDialog.Options := [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing];
  end;
  if OpenDialog.Execute then begin
    Screen.Cursor := crHourGlass;
    case OpenDialog.FilterIndex of
      1: r := LoadRVSt;
      2: r := LoadRVF;
      3: r := LoadRTF;
      else r := False;
    end;
    Screen.Cursor := crDefault;
    if not r then begin
      RVA_MessageBox(RVA_GetS(rvam_sti_LoadError, ControlPanel), RVA_GetS(rvam_err_Title, ControlPanel),
        MB_OK or MB_ICONSTOP);
      exit;
    end;
    if rvsImport.StyleTemplates.Count=0 then begin
      RVA_MessageBox(RVA_GetS(rvam_sti_NoStyles, ControlPanel), RVA_GetS(rvam_err_Title, ControlPanel),
        MB_OK or MB_ICONEXCLAMATION);
      exit;
    end;
    rvsImport.StyleTemplates.UpdateReferences;
    if rvsImport.Units<>rvs.Units then
      rvsImport.ConvertToDifferentUnits(rvs.Units);

    frm := TfrmRVStyleImport.Create(Application, ControlPanel);
    try
      frm.Init(rvsImport, StyleTemplates, rvs);
      frm.Images := Images;
      frm.Images2 := (_tv as TRVATreeView).Images;
      frm.FontImageIndex := FontImageIndex;
      frm.HyperlinkImageIndex := HyperlinkImageIndex;
      frm.ParaImageIndex := ParaImageIndex;
      frm.ParaBorderImageIndex := ParaBorderImageIndex;
      for StyleKind := Low(TRVStyleTemplateKind) to High(TRVStyleTemplateKind) do
        frm.ImageIndices[StyleKind] := ImageIndices[StyleKind];
      frm.lstClick(nil);
      if frm.ShowModal=mrOk then begin
        if frm.GetXBoxItemIndex(frm._rg)=0 then
          MergeOverride
        else
          MergeRename;
        StyleTemplates.ResetNameCounter;
        BuildTree(CurrentStyleTemplate);
      end;
    finally
      frm.Free;
    end;


  end;
end;

function TfrmRVStyles.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Height := _btnOk.Top+_btnOk.Height+(_btnOk.Top-gb.Top-gb.Height);
  Width := _tv.Left + gb.Left + gb.Width;
  Result := True;
end;

{$ENDIF}



end.
