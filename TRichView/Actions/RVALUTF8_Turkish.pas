﻿// This file is a copy of RVAL_Turkish.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Turkish (TR) translation                        }
{                                                       }
{       Copyright (c) Necmettinozalp                    }
{       necmettinozalp@hotmail.com                      }
{       http://www.trichview.com                        }
{                                                       }
{       Copyright (c) İbrahim Kutluay                   }
{       ikutluay@ibrahimkutluay.net                     }
{                                                       }
{*******************************************************}
{ Updated: 2012-08-22 by                                }
{             Zekeriya Bozkurt <zekeriye@hotmail.com>   }
{*******************************************************}

unit RVALUTF8_Turkish;
{$I RV_Defs.inc}

interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'inç', 'cm', 'mm', 'pika', 'piksel', 'nokta',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  'inç','cm','mm', 'pika','piksel', 'nokta',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Dosya', '&Düzen', '&Biçim', '&Yazı Tipi', '&Paragraf', '&Ekle', '&Tablo', '&Pencere', '&Yardım',
  // exit
  '&Çıkış',
  // top-level menus: View, Tools,
  '&Görünüm', 'Araç&lar',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Boyut', 'Yazıtipi', '&Tabloyu Seç', 'Hücre İçeriği&ni Hizala', 'Hücr&e Kenarlığı',
  // menus: Table cell rotation
  'Hücre &Döndürme',
  // menus: Text flow, Footnotes/endnotes
  '&Yazı Akış Düzeni', 'Ü&stBilgi/AltBilgi',
  // ribbon tabs: tab1, tab2, view, table
  '&Genel', '&Gelişmiş', 'Gö&rünüm', '&Tablo',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Pano', 'Yazı Tipi', 'Paragraf', 'Liste', 'Düzenle',
  // ribbon groups: insert, background, page setup,
  'Ekle', 'Arka Plan', 'Sayfa Yapısı',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options 
  'Links', 'ÜstBilgi', 'Dosya Görünümü', 'Göster/Gizle', 'Büyült/Küçült', 'Seçenekler',
  // ribbon groups on table tab: insert, delete, operations, borders
   'Ekle', 'Sil', 'Operasyon', 'Kenarlıklar',
  // ribbon groups: styles 
  'Stil',
  // ribbon screen tip footer,
  'Yardım için F1 tuşuna basınız',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Son Kullanılanlar', 'Dosyanın bir kopyasını kaydet', 'Dosyayı Yazıcı Öngörünümü ve Yazdır',
  // ribbon label: units combo
  'Cetvel Birimi:',    
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Yeni', 'Yeni|Yeni boş bir belge oluşturur',
  // TrvActionOpen
  '&Aç...', 'Aç|Daha önceden kayıt edilmiş dosyayı açar',
  // TrvActionSave
  '&Kaydet', 'Kaydet|Mevcut belgeyi diske kaydet',
  // TrvActionSaveAs
  '&Farklı kaydet...', 'Farklı kaydet...|Mevcut belgeyi farklı bir adla kaydeder',
  // TrvActionExport
  '&Dönüştür...', 'Dönüştür|Mevcut belgeyi diğer biçimlere dönüştür',
  // TrvActionPrintPreview
  '&Baskı Önizleme', 'Baskı önizleme|Sanal kağıtta baskı önizleme yapılır',
  // TrvActionPrint
  '&Yazdır...', 'Yazdır|Yazdırma seçeneklerini ayarlar ve dosyayı yazdır',
  // TrvActionQuickPrint
  '&Yazdır','&Hızlı Yazdır', 'Yazdır|Belgeyi mevcut ayarlarla yazdır',
  // TrvActionPageSetup
  'Sayfa Yapısı...', 'Sayfa Yapısı|Kenar boşlukları, Kağıt yapısı, Kağıt Yönü, Üst ve Alt Bilgileri Ayarla',
  // TrvActionCut
  '&Kes', 'Kes|Seçili kısmı belleğe alarak belgeden çıkart',
  // TrvActionCopy
  '&Kopyala', 'Kopyala|Seçili kısmı çoğaltmak üzere belleğe al',
  // TrvActionPaste
  '&Yapıştır', 'Yapıştır|Kesilen veya Kopyalanan metni Yapıştır',
  // TrvActionPasteAsText
  '&Yazı Olarak Yapıştır', 'Yazı Olarak Yapıştır|Panodaki Bilgiyi Yazı Olarak Yapıştır',
  // TrvActionPasteSpecial
  'Ö&zel Yapıştır...', 'Özel yapıştır|Kesilen veya Kopyalanan İfadeyi Özel Yapıştır',
  // TrvActionSelectAll
  '&Tümünü Seç', 'Tümünü Seç|Tüm Belgeyi Seç',
  // TrvActionUndo
  '&Geri Al', 'Geri Al|Son yapılan işlemi geri al',
  // TrvActionRedo
  '&Yeniden Yap', 'Yeniden Yap|Geri alınan işlemi tekrar yap',
  // TrvActionFind
  '&Bul...', 'Bul|Belirtilen metni belge içersinde Ara',
  // TrvActionFindNext
  '&Sonrakini Bul', 'Sonrakini Bul|Metni aramaya devam et',
  // TrvActionReplace
  '&Değiştir...', 'Değiştir|Bulunan metni başka bir metinle değiştir',
  // TrvActionInsertFile
  '&Dosya...', 'Dosya Ekle|Dış Kaynaklı dosya ekle',
  // TrvActionInsertPicture
  '&Resim...', 'Resim Ekle|Bir resim dosyasını belgeye ekle',
  // TRVActionInsertHLine
  '&Ara Çizgi', 'Yatay Çizgi Ekle|Araya yatay çizgi ekle',
  // TRVActionInsertHyperlink
  '&Köprü...', 'Köprü Ekle|Bağlantı Köprüsü ekle',
  // TRVActionRemoveHyperlinks
  'Köprüyü Kaldır','Köprüyü Kaldır|Seçili metindeki tüm köprüleri kaldırır',
  // TrvActionInsertSymbol
  '&Sembol...', 'Sembol Ekle|Sembol tablosundan Sembol ekle',
  // TrvActionInsertNumber
  'Sayı...', 'Sayı ekle|Bir Sayı ekle',
  // TrvActionInsertCaption
  'Başlık Ekle...', 'Başlık ekle|Seçili metin/nesne için başlık ekler',
  // TrvActionInsertTextBox
  'Metin Kutusu', 'Metin Kutusu Ekle|Metin Kutusu Ekler',
  // TrvActionInsertSidenote
  'Kenar Notu', 'Kenar Notu Ekle|Metin kutusu içinde note ekler',
  // TrvActionInsertPageNumber
   'Sayfa numarası', 'Sayfa numarası|Sayfa numarası ekle',
  // TrvActionParaList
  '&Anahat ve Numaralandırma...', 'Anahat ve Numaralandırma|Seçili bölümü numaralandır',
  // TrvActionParaBullets
  '&Numaralandır', 'Numaralandır|Numaralandır veya Numaralandırmayı İptal Et',
  // TrvActionParaNumbering
  '&Numaralandırma', 'Numaralandırma|Seçili bölümü numaralandırma veya numaralandırmayı kaldırmak İçin',
  // TrvActionColor
  '&Zemin Rengi...', 'Zemin Rengi|Belgenin Arka Fonunu değiştir',
  // TrvActionFillColor
  '&Dolgu Rengi...', 'Dolgu Rengi|Paragraf, hücre, tablo, belge veya bir metin için zemin rengi belirler',
  // TrvActionInsertPageBreak
  '&Sayfasonu Ekle', 'Sayfasonu Ekle|Bu noktadan itibaren yeni bir sayfaya başlar',
  // TrvActionRemovePageBreak
  '&Sayfa Kesmesini Kaldır', 'Sayfa Kesmesini Kaldır|Sayfa kesmesini kaldır',
  // TrvActionClearLeft
   'Soldaki Yazı Akışını Temizle', 'Soldaki Yazı Akışını Temizle|Bu paragraf sola dayalı resmi takip etsin',
  // TrvActionClearRight
   'Sağdaki Yazı Akışını Temizle', 'Sağdaki Yazı Akışını Temizle|Bu paragraf sağa dayalı resmi takip etsin',
  // TrvActionClearBoth
  'Etrafındaki Yazı Akışını Temizle','Etrafındaki Yazı Akışını Temizle|Bu paragraf sola veya sağadayalı resmi takip eder',
  // TrvActionClearNone
  'Normal Yazı Akışı','Normal Yazı Akışı|Paragraf her iki yana yaslı resmin etrafında yer alır',
  // TrvActionVAlign
  'Nesne Konumu ...','Nesne Konumu|Seçili Nesnenin konumunu değiştir',
  // TrvActionItemProperties
  '&Nesne Özellikleri...', 'Nesne Özellikleri|Seçili Nesne ile ilgili özellikler',
  // TrvActionBackground
  '&Arka Plan...', 'Arka Plan|Sayfanın arka plan rengini değiştirir veya resim yerleştirir',
  // TrvActionParagraph
  '&Paragraf...', 'Paragraf|Paragrafa ait özellikleri değiştir',
  // TrvActionIndentInc
  '&Girintiyi Arttır', 'Girintiyi Arttır|Seçili paragraf girintisini artırır',
  // TrvActionIndentDec
  'Girintiyi A&zalt', 'Girintiyi Azalt|Seçili paragraf girintisini azaltır',
  // TrvActionWordWrap
  '&Metni Kaydır', 'Metni Kaydır|Satır sonuna gelince kelimeyi aşağı kaydır',
  // TrvActionAlignLeft
  '&Sola Yasla', 'Sola Yasla|Seçili metni sola yasla',
  // TrvActionAlignRight
  'Sa&ğa Yasla', 'Sağa Yasla|Seçili Metni sağa yasla',
  // TrvActionAlignCenter
  '&Ortala', 'Metni Ortala|Seçili metni ortala',
  // TrvActionAlignJustify
  '&İki Yana Yasla', 'İki Yana Yasla|Seçili metni iki yana yasla',
  // TrvActionParaColor
  'Paragraf &Dolgu Rengi...', 'Paragraf Dolgu Rengi|Paragraf dolgu rengi ayarlarını değiştir',
  // TrvActionLineSpacing100
  '&Tek Satır Boşluk', 'Tek satır boşluk|Satır aralarında tek satır boşluk bırak',
  // TrvActionLineSpacing150
  '1.5 Satır Boşluk', '1.5 Satır boşluk|Satır aralarında 1.5 satır boşluk bırak',
  // TrvActionLineSpacing200
  '&Çift satır boşluk', 'Çift satır boşluk|Satır aralarında iki satır boşluk bırak',
  // TrvActionParaBorder
  'Paragraf Kenarlık ve Dolgu Biçimi...', 'Paragraf Kenarlık ve Doldu Biçimi|Paragraf kenarlıklarını ve dolgu rengini ayarlar',
  // TrvActionInsertTable
  '&Tablo Ekle...', '&Tablo', 'Yeni Tablo|Yeni tablo ekle',
  // TrvActionTableInsertRowsAbove
  'Üste &Satır Ekle', 'Üste Satır Ekle|Seçili satırın üstüne yeni satır ekle',
  // TrvActionTableInsertRowsBelow
  'Al&ta Satır Ekle', 'Alta Satır Ekle|Seçili satırın altına yeni satır ekle',
  // TrvActionTableInsertColLeft
  'Sola Sütun Ekle', 'Sola Sütun Ekle|Sola yeni sütun ekle',
  // TrvActionTableInsertColRight
  '&Sağa Sütün Ekle', 'Sağa Sütun Ekle|Sağa yeni sütun ekle',
  // TrvActionTableDeleteRows
  'Satır Sil', 'Satır Sil|Seçili satırları sil',
  // TrvActionTableDeleteCols
  'Sütün Sil', 'Sütun Sil|Seçili sütunları sil',
  // TrvActionTableDeleteTable
  'Tablo S&il', 'Tablo Sil|Seçili tabloyu sil',
  // TrvActionTableMergeCells
  'Hücreleri &Birleştir', 'Hücreleri Birleştir|Seçili hücreleri birleştir',
  // TrvActionTableSplitCells
  'Hücreleri &Böl...', 'Hücreleri Böl|Seçili hücreleri böl',
  // TrvActionTableSelectTable
  'Tablo &Seç', 'Tablo Seç|Tabloyu seç',
  // TrvActionTableSelectRows
  '&Satır Seç', 'Satır Seç|Satır seç',
  // TrvActionTableSelectCols
  'Süt&un Seç', 'Sütun Seç|Sütun seç',
  // TrvActionTableSelectCell
  '&Hücre Seç', 'Hücre Seç|Hücre seç',
  // TrvActionTableCellVAlignTop
  'Hücreyi Üste &Hizala', 'Hücreyi Üste Hizala|Hücre içeriğini üst kenara yaklaştır',
  // TrvActionTableCellVAlignMiddle
  'Hücreyi &Ortaya Hizala', 'Hücreyi Ortaya Hizala|Hücre içeriğini ortala',
  // TrvActionTableCellVAlignBottom
  'Hücreyi &Alta Hizala', 'Hücreyi Alta Hizala|Hücre içeriğini alt kenara yaklaştır',
  // TrvActionTableCellVAlignDefault
  '&Varsayılan Hizalama', '&Varsayılan Hizalama|&Varsayılan Hizalama',
  // TrvActionTableCellRotationNone
  'Hücrede Döndürme &Yok', 'Hücrede Döndürme Yok|Hücre içeriğini normal hale getirir (0° döndürme)',
  // TrvActionTableCellRotation90
  'Hücreyi &90° Döndür', 'Hücreyi 90° Döndür|Hücre içeriğini 90° döndürür',
  // TrvActionTableCellRotation180
  'Hücreyi &180° Döndür', 'Hücreyi 180° Döndür|Hücre içeriğini 180° döndürür',
  // TrvActionTableCellRotation270
  'Hücreyi &270° Döndür', 'Hücreyi 270° Döndür|Hücre içeriğini 270° döndürür',
  // TrvActionTableProperties
  'Tablo &Özellikleri...', 'Tablo Özellikleri|Tablo özelliklerini değiştir',
  // TrvActionTableGrid
  'Kılavuz Çizgilerini &Göster', 'Kılavuz Çizgilerini Göster|Klavuz çizgilerini gizle/göster',
  // TRVActionTableSplit
  'Tabloyu İkiye &Böl', 'Tabloyu İkiye Böl|Seçili satırdan başlamak üzere tabloyu 2 ye böler',
  // TRVActionTableToText
  'Metne Çevir', 'Metne Çevir |Tabloyu metin halinde yazıya çevirir',
  // TRVActionTableSort
   '&Sırala ...', 'Sırala|Tablo satırlarını sıralar',
  // TrvActionTableCellLeftBorder
  '&Sol Kenarlık', 'Sol Kenarlık|Sol kenarlığı gizle veya göster',
  // TrvActionTableCellRightBorder
  '&Sağ Kenarlık', 'Sağ Kenarlık|Sağ kenarlığı gizle veya göster',
  // TrvActionTableCellTopBorder
  '&Üst Kenarlık', 'Üst Kenarlık|Üst kenarlığı gizle veya göster',
  // TrvActionTableCellBottomBorder
  '&Alt Kenarlık', 'Alt Kenarlık|Alt kenarlığı gizle veya göster',
  // TrvActionTableCellAllBorders
  '&Tüm Kenarlıklar', 'Tüm Kenarlıklar|Tüm hücre kenarlık çizgilerini gizle veya göster',
  // TrvActionTableCellNoBorders
  '&Kenarlık Yok', 'Kenarlık Yok|Tüm kenarlıkları gizle',
  // TrvActionFonts & TrvActionFontEx
  '&Yazı Tipi...', 'Yazı Tipi|Seçili metnin yazı tipini değiştir',
  // TrvActionFontBold
  '&Kalın', 'Kalın|Seçili metni kalınlaştır',
  // TrvActionFontItalic
  'İ&talik', 'İtalik|Seçili metni sağa yatık yazar',
  // TrvActionFontUnderline
  '&Altı Çizili', 'Altı Çizili|Seçili metnin altını çiz',
  // TrvActionFontStrikeout
  '&Üstü Çizili', 'Üstü Çizili|Seçili metnin üstünü çiz',
  // TrvActionFontGrow
  '&Yazı Boyutunu Artır', 'Yazı Boyutunu Artır|Yazı boyutunu %10 büyült',
  // TrvActionFontShrink
  'Yazı Boyutunu Küçült', 'Yazı Boyutunu Küçült|Yazı boyutunu %10 küçült',
  // TrvActionFontGrowOnePoint
  'Yazı Boyutunu 1 Nk Artır', 'Yazı Boyutunu 1 Nk Artır|Yazı boyutunu 1 Nk artır',
  // TrvActionFontShrinkOnePoint
  'Yazı Boyutunu 1 Nk Azalt', 'Yazı Boyutunu 1 Nk Azalt|Yazı boyutunu 1 Nk azaltır',
  // TrvActionFontAllCaps
  '&Tümü Büyük', 'Tümü Büyük|Tüm seçili metni BÜYÜK HARFE çevir',
  // TrvActionFontOverline
  '&Üstü Çizili', 'Üstü Çizili|Tüm seçili metni üstü çizili hale getirir',
  // TrvActionFontColor
  'Yazı &Rengi...', 'Yazı Rengi|Seçili metnin yazı rengini değiştir',
  // TrvActionFontBackColor
  '&Vurgu...', 'Vurgu|Seçili metnin Vurgulayacak rengi belirler',
  // TrvActionAddictSpell3
  '&Yazım ve &Dil Bilgisi', 'Yazım ve Dil Bilgisi|Yazım ve Dil Bilgisi kontrolü',
  // TrvActionAddictThesaurus3
  '&Eş Anlamlılar', 'Eş Anlamlılar|Seçili kelime için eş anlamlı kelimeleri belirtir',
  // TrvActionParaLTR
  'Soldan Sağa', 'Soldan Sağa|Paragraf metin yazım yönünü soldan-sağa olarak ayarlar',
  // TrvActionParaRTL
  'Sağdan Sola', 'Sağdan Sola|Paragraf metin yazım yönünü sağdan-sola olarak ayarlar',
  // TrvActionLTR
  'Soldan Sağa Metin', 'Soldan Sağa Metin|Metin yazım yönünü soldan-sağa olarak ayarlar',
  // TrvActionRTL
  'Sağdan Sola Metin', 'Sağdan Sola Metin|Metin yazım yönünü sağdan-sola olarak ayarlar',
  // TrvActionCharCase
  'Büyük/Küçük Harf Dönüştür', 'Büyük/Küçük Harf Dönüştür|Seçili metinde Büyük/Küçük Harf dönüştürme yapar',
  // TrvActionShowSpecialCharacters
  'Yazılamaya&n Karakterler', 'Yazılamayan Karakterler|Yazılamayan Karakterleri -paragraf sonu,sekme veya boşluk gbi- göster/gizle',
  // TrvActionSubscript
  'Al&t simge', 'Altsimge|Seçili metni altsimge yapar',
  // TrvActionSuperscript
  'Üst simge', 'Üst simge|Seçili metni üst simge yapar',
  // TrvActionInsertFootnote
  'Üstbilgi','Üstbilgi|Üstbilgi Ekle',
  // TrvActionInsertEndnote
  'Altbilgi','Altbilgi|Altbilgi Ekle',
  // TrvActionEditNote
  'Altbilgi/Üstbilgi Düzenle','Altbilgi/Üstbilgi Düzenle|Altbilgi / Üst Bilgiyi Düzenle, Değiştir',
  // TrvActionHide
  '&Gizle/Göster','Gizle/Göster| Seçili metni Gizler(Basılamayan metin yapar) veya Gösterir (Basılabilen metin yapar)',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stil...', 'Stil|Stil ayarlama ekranını açar',
  // TrvActionAddStyleTemplate
  'Stil &Ekle...', 'Stil Ekle|Yeni bir metin veya paragraf stili oluştur',
  // TrvActionClearFormat,
  '&Formatı Temizle', 'Formatı Temizle|Seçili metin ve paragraflara uygulanan formatları temizler',
  // TrvActionClearTextFormat,
  '&Metin Formatını Temizle', '&Metin Formatını Temizle|Seçili metindeki uygulanmış formatları iptal eder',
  // TrvActionStyleInspector
  'Stil &Listesi', 'Stil &Listesi|Stil Listesi göster / gizle',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'Tamam', 'İptal', 'Kapat', 'Ekle', '&Aç...', '&Kaydet...', '&Temizle', 'Yardım', 'Sil',
  // Others  -------------------------------------------------------------------
  // percents
  'yüzde',
  // left, top, right, bottom sides
  'Sol Kenar', 'Üst Kenar', 'Sağ Kenar', 'Alt Kenar',
  // save changes? confirm title
  '"%s" Belgede yapılan değişiklikler kaydedilsin mi?', 'Kaydet',
  // warning: losing formatting
  '%s kaydetmek istediğiniz biçim tarafından desteklenmeyen bazı özellikler içeriyor olabilir.'#13+
  'Belgeyi bu biçimde kaydetme istiyor musunuz?',
  // RVF format name
  'Zengin Görünüm Biçimi',
  // Error messages ------------------------------------------------------------
  'Hata',
  'Dosya Açılırken Bir Hata Oluştu.'#13#13'Olası nedenler:'#13'- dosya biçimi bu program tarafından desteklenmiyor olabilir;'#13+
  '- dosya bozulmuş olabilir;'#13'- dosya şu an başka bir uygulama tarafından kullanılıyor olabilir.',
  'Resim dosyası yüklenirken hata oluştu.'#13#13'Olası nedenler:'#13'- resim biçimi bu program tarafından desteklenmiyor olabilir;'#13+
  '- dosya resim içermiyor olabilir;'#13+
  '- dosya bozulmuş olabilir;'#13'- dosya şu an başka bir uygulama tarafından kullanılıyor olabilir.',
  'Dosya KAydetme Hatası.'#13#13'Olası nedenler:'#13'- yeterli boş yer olmayabilir;'#13+
  '- disk yazım-korumalı olabilir;'#13'- kaydedilecek sürücü çıkarılmış olabilir;'#13+
  '- dosya şu an başka bir uygulama tarafından kullanılıyor olabilir;'#13'- disk üzerinde bozuk alanlar olabilir.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RVF Dosyaları (*.rvf)|*.rvf',  'RTF Dosyaları (*.rtf)|*.rtf' , 'XML Dosyaları (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Metin Dosyaları (*.txt)|*.txt', 'Unicode Metin dosyaları (*.txt)|*.txt', 'Metin Dosyaları - Otomatik Tanı(*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Dosyaları (*.htm;*.html)|*.htm;*.html', 'HTML dosyaları (*.htm;*.html)|*.htm;*.html',
  'HTML dosyaları - basiteştirilmiş(*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dosyası (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Arama Tamamlandı', 'Aranan ''%s'' metni bulunamadı.',
  // 1 string replaced; Several strings replaced
  '1 metin değiştirildi.', '%d metin değiştirildi.',
  // continue search from the beginning/end?
  'Arama işlemi belgenin sonuna kadar yapıldı,belgenin başından itibaren tekrarlansınmı?',
  'Arama işlemi belgenin başına kadar yapıldı,belgenin sonundan itibaren tekrarlansınmı?',  
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Saydam', 'Otomatik',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Siyah', 'Kahverengi', 'Zeytin Yeşili', 'Koyu Yeşil', 'Koyu Deniz Mavisi', 'Koyu Mavi', 'Çivit Mavi', 'Gri-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Koyu Kırmızı', 'Turuncu', 'Kirli Sarı', 'Yeşil', 'Deniz Mavisi', 'Mavi', 'Mavi-Gri', 'Gri-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Kırmızı', 'Açık Turuncu', 'Fıstıki Yeşil', 'Deniz Yeşili', 'Okyanus Mavisi', 'Açık Mavi', 'Menekşe', 'Gri-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Pembe', 'Altın', 'Sarı', 'Parlak Yeşil', 'turkuaz', 'Gök Mavi', 'Plum', 'Gray-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Gül', 'Ten', 'Açık Sarı', 'Açık Yeşil', 'Açık Turkuaz', 'Donuk Mavi', 'Lavanta', 'Beyaz',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&Saydam', '&Otomatik', '&Tüm Renkler...', '&Varsayılan',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Dolgu', '&Renk:', 'Konum', 'Dolgu', 'Örnek Metin.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Yok', '&Yay', 'Düzgün Döşe', '&Döşe', '&Ortala',
  // Padding button
  '&Dış Kenar...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Renk Doldur', '&Uygulama Alanı:', '&Tüm Renkler...', '&Dış Kenar...',
  'Lütfen bir renk seçin',
  // [apply to:] text, paragraph, table, cell
  'metin', 'paragraf' , 'tablo', 'hücre',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Yazı Biçimi', 'Yazı Biçimi', 'Diğer',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Yazı Tipi:', '&Yazı Boyutu', 'Yazıtipi', '&Kalın', 'İ&talic',
  // Script, Color, Back color labels, Default charset
  '&Dil:', '&Renk:', 'Alt &Zemin:', '(Varsayılan)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efekt', 'Altı Çizgili', '&Üst Çizgi', '&Üstü Çizgili', '&TÜMÜ BÜYÜK HARF',
  // Sample, Sample text
  'Örnek', 'Örnek Metin',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Karakter Arası Boşluk', '&Boşluk:', '&Genişletilmiş', '&Daraltılmış',
  // Offset group-box, Offset label, Down, Up,
  'Konum', '&Konum:', '&Alçaltılmış', '&Yükseltilmiş',
  // Scaling group-box, Scaling
  'Ölçekleme', '&Ölçek:',
  // Sub/super script group box, Normal, Sub, Super
  'Alt simge ve Üst simge', '&Normal', 'Al&t simge', 'Ü&st simge',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Boşluk', '&Üst:', '&Sol:', '&Alt:', '&Sağ:', '&Eşit olarak',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Köprü Ekle', 'Köprü', '&Metin:', '&Hedef', '<<Seçim>>',
  // cannot open URL
  '"%s" web sayfası bulunamadı',
  // hyperlink properties button, hyperlink style
  '&Özelleştir...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Köprü Özellikleri', 'Normal Renkler', 'Aktif Renk',
  // Text [color], Background [color]
  '&Metin:', '&Dolgu',
  // Underline [color]
  'Altı Çizgili:',
  // Text [color], Background [color] (different hotkeys)
  '&Metin:', '&Dolgu',
  // Underline [color], Attributes group-box,
  'Altı Çizgili:', 'Özellikler',
  // Underline type: always, never, active (below the mouse)
  'Altı Çizgili::', 'Her zaman', 'hiçbir zaman', 'aktif',
  // Same as normal check-box
  'Normal renkleri kullan',
  // underline active links check-box
  '&Aktif köprünün altını çiz',

  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Simge Ekle', '&Yazı Tipi:', '&Karakter Tablosu:', 'Unicode',
  // Unicode block
  '&Blok:',    
  // Character Code, Character Unicode Code, No Character
  'Karakter Kodu: %d', 'Karakter Kodu: Unicode %d', '(Karakter Yok)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Tablo Ekle', 'Tablo Boyutu', '&Sütun Sayısı:', 'Sa&tır Sayısı:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tablo Ayarları', '&Otomatik Boyutlandır', 'Pencereye Sığdır', 'Sabit Sütun Genişliği',
  // Remember check-box
  '&Yeni Tablolar İçin Varsayılan Yap',
  // VAlign Form ---------------------------------------------------------------
  'Konum/Yerleşim',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Özellikler', 'Resim', 'Büyüklük ve Konum', 'Satır', 'Tablo', 'Satırlar', 'Hücreler',
  // Image Appearance, Image Misc, Number tabs
  'Görünüm', 'Öznitelik', 'Numara',
  // Box position, Box size, Box appearance tabs
  'Konum', 'Büyüklük', 'Görünüm',
  // Preview label, Transparency group-box, checkbox, label
  'Önizleme:', 'Saydamlık', '&Saydam', 'Saydamlık Rengi:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Değiştir...', '&Kaydet...', 'Otomatik',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Konum', '&Yön:',
  'Satır altı ile hizalar', 'Satıra göre ortala',
  'Satır üst çizgisinin üstünde', 'Satır alt çizgisinin altında', 'Satırın orta çizgisinin ortasında',
  // align to left side, align to right side
  'Sol Taraf', 'Sağ Taraf',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Kaydırma:', 'Yay', '&Genişlik:', '&Yükseklik:', 'Varsayılan Boyut: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Orantılı ölçeklendir', 'Sıfırla',
  // Inside the border, border, outside the border groupboxes
  'Kenarlık İçinde', 'Kenarlık', 'Kenarlık Dışında',
  // Fill color, Padding (i.e. margins inside border) labels
  'Dolgu Rengi:', 'Boşluk:',
  // Border width, Border color labels
  'Kenarlık kalınlığı:', 'Kenarlık rengi:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
   'Yatay Boşluk:', 'Düşey Boşluk:',
  // Miscellaneous groupbox, Tooltip label
  'Öznitelik', 'Açıklama:',
  // web group-box, alt text
  'web kutusu Ayarları', 'Alternatif &Metin:',
  // Horz line group-box, color, width, style
  'Yatay Çizgi', '&Renk:', '&Genişlik:',  '&Tip:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tablo', '&Genişlik:', '&Dolgu Rengi:', 'Hücreler Arası &Boşluk...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Yatay hücre boşluğu', 'Düşey hücre boşluğu:', 'Kenarlık ve Arkaplan',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Görünür Çerçeve Kenarları...', 'Otomatik', 'otomatik',
  // Keep row content together checkbox
  'İçerik ile birlikte koru',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Dönme Açısı', 'Yok', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Çerçeve:', 'Görünür Çerçeve Kenarları...',
  // Border, CellBorders buttons
  '&Tablo Kenarı ...', '&Hücre Kenarı...',
  // Table border, Cell borders dialog titles
  'Tablo Kenarlığı', 'Varsayılan Hücre Kenarları',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Yazdırılıyor', '&Tabloyu bir sayfaya sığdır', 'Başlık satır &sayısı:',
  'Başlık satınlarını hersayfada tekrarla',
  // top, center, bottom, default
  '&Üst', '&Orta', '&Aşağı', '&Varsayılan',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Ayarlar', 'Tercih edilen &genişlik:', '&En az yükseklik:', '&Dolgu Rengi:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kenar', 'Görünen Kenarlar:',  '&Gölge Rengi:', '&Açık Renk', '&Renk:',
  // Background image
  '&Resim...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Yatay Konum', 'Düşey konum', 'Metin İçindeki Konumu',
  // Position types: align, absolute, relative
  'Hizalama', 'Mutlak Konum', 'Göreceli Konum',
  // [Align] left side, center, right side
  '.. nın solu, kutunun solu ile aynı',
  '.. nın merkezi, kutunun merkezi ile aynı',
  '.. nın sağı, kutunun sağı ile aynı',
  // [Align] top side, center, bottom side
  '.. nın üstü, kutunun üstü ile aynı',
  '.. nın merkezi, kutunun merkezi ile aynı',
  '.. nın altı, kutunun altı ile aynı',
  // [Align] relative to
  'göreceli',
  // [Position] to the right of the left side of
  '.. nın solunun sağında',
  // [Position] below the top side of
  '.. üstünün altında ',
  // Anchors: page, main text area (x2)
  'Sayfa', 'Ana Metin Alanı', 'Sayfa', 'Ana Metin Alanı',
  // Anchors: left margin, right margin
  'Sol boşluk', 'Sağ boşluk',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Üst Boşluk', 'Alt Boşluk', 'Girinti Boşluğu', 'Çıkıntı Boşluğu',
  // Anchors: character, line, paragraph
  'Karekter', 'Satır', 'Paragraf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Karşılıklı Boşluklar', 'Tabloda hücre düzeni',
  // Above text, below text
  'Üstte metin', 'Altta metin',
  // Width, Height groupboxes, Width, Height labels
  'Genişlik', 'Yükseklik', 'Genişlik:', 'Yükseklik:',
  // Auto height label, Auto height combobox item
  'Otomatik', 'Otomatik',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Düşey Hizalama', 'Üst', 'Orta', 'Alt',
  // Border and background button and title
  'Kenarlık ve Arkaplan...', 'Kutunun Çerçevesi ve Arkaplanı',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Özel Yapıştır', '&Yapıştırma Seçenekleri:', 'Zengin Metin Biçimi', 'HTML Biçimi',
  'Metin', 'Unicode Metin', 'Bitmap Resmi', 'Windows Metafile Resmi',
  'Resim Dosyaları', 'Köprü',
  // Options group-box, styles
  'Seçenekler', '&Stil:',
  // style options: apply target styles, use source styles, ignore styles
  'Belgeye stili uygula', 'stil ve görünümleri koru',
  'görünümü koru, stilleri dikkate alma',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Numaralandırma ve Ana Hat', 'Madde İmi', 'Numaralı', 'Madde imi listesi', 'Numaralandırma Listesi',
  // Customize, Reset, None
  '&Özelleştir...', '&Sıfırla', 'Yok',
  // Numbering: continue, reset to, create new
  'Numaralandırmaya Devam Et', 'İlk numara', 'Yeni Liste Oluştur, Başlangıcı :',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Büyük Harf', 'Büyük Roma Rakamı', 'Normal Sayı', 'Küçük Harf', 'Küçük Roma Rakamı',
  // Bullet type
  'Daire', 'Disc', 'Kare',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Aşama:', '..en &Başla:', '&Devam', 'Numaralama',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Numaralandırmayı Özelleştir', 'Düzeyler', '&Toplam:', 'Numaralandırma Özellikleri', '&Biçimi:',
  // List types: bullet, image
  'Simge', 'Resim', 'Numara Ekle|',
  // Number format, Number, Start level from, Font button
  '&Sayı Biçimi:', 'Sayı', '&Bu Seviyeden Numaralandır:', '&Yazı Tipi...',
  // Image button, bullet character, Bullet button,
  '&Simge...', '&Simge Karakteri', '&Simge...',
  // Position of list text, bullet, number, image, text
  'Listelenen Metin Konumu', 'Simge Konumu', 'Sayı Konumu', 'Resim Konumu', 'Metin Konumu',
  // at, left indent, first line indent, from left indent
  '&dan:', '&Sol Girinti:', '&İlk Satır Girintisi:', 'Soldan İtibaren',
  // [only] one level preview, preview
  '&Bir Seviye Göster', 'Önizleme',
  // Preview text
  'Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Sola Yasla', 'Sağa Yasla', 'Ortala',
  // level #, this level (for combo-box)
  'Seviye %d', 'Bu Seviye',
  // Marker character dialog title
  'Simge Düzenle',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Paragraf Kenarı ve Dolgu Rengi', 'Kenar', 'Dolgu Rengi',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Ayarlar', '&Renk:', '&Genişlik:', 'İç &Genişlik:', '&Kenarlar...',
  // Sample, Border type group-boxes;
  'Örnek', 'Kenar Tipi',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Yok', '&Tek', '&Çift', '&Üçlü', '&İçi Koyu', '&Dışı Koyu',
  // Fill color group-box; More colors, padding buttons
  'Dolgu Rengi', '&Tüm Renkler...', '&Kenarlar...',
  // preview text
  'Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin',
  // title and group-box in Offsets dialog
  'Kenarlar', 'Dış Kenarlar',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragraf', 'Yönelim', '&Sol', 'Sa&ğ', '&Ortala', '&Yasla',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Aralık', '&Önce:', '&Sonra:', 'Satır Boşluğu:', '&Aralık:',
  // Indents group-box; indents: left, right, first line
  'Girintiler', '&Sol:', 'Sa&ğ:', '&İlk Satır:',
  // indented, hanging
  '&İçerden', '&Dışardan', 'Örnek',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Tek Satır', '1.5 Satır', 'Çift Satır', 'En Az (At least)', 'Tam (Exactly)', 'Birden Fazla Satır',
  // preview text
  'Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Girinti ve Boşluk', 'Sekme', 'Metin Akışı',
  // tab stop position label; buttons: set, delete, delete all
  '&Sekme Durağı Konumu:', '&Uygula', '&Sil', '&Tümünü sil',
  // tab align group; left, right, center aligns,
  'Yönelim', '&Sol', 'S&ağ', '&Ortalı',
  // leader radio-group; no leader
  'Kılavuz Çizgi', '(Yok)',
  // tab stops to be deleted, delete none, delete all labels
  'Silinecek sekme durakları:', '(Yok)', 'Tümü.',
  // Pagination group-box, keep with next, keep lines together
  'Ayarlar', '&Sonraki ile birlite tut', 'S&atırları birlikte tut',
  // Outline level, Body text, Level #
  'Çerçe&ve Aşaması:', 'Gövde Metni', 'Aşama %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Baskı Önizleme', 'Sayfa Genişliği', 'Tüm Sayfa', 'Sayfalar:',
  // Invalid Scale, [page #] of #
  '10-500 Arasında bir değer girin', '/ %d',
  // Hints on buttons: first, prior, next, last pages
  'İlk Sayfa', 'Önceki Sayfa', 'Sonraki Sayfa', 'Son Sayfa',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Hücre Boşluğu', 'Boşluklar', 'A&rasındaki Hücreler', 'Bu Tablo Kenarından İtibaren Hücreler',
  // vertical, horizontal (x2)
  '&Dikey:', '&Yatay:', 'Dik&ey:', 'Y&atay:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kenarlar', 'Ayarlar', '&Renk:', '&Açık Renk:', 'Gölge Rengi:',
  // Width; Border type group-box;
  '&Genişlik:', 'Kenarlık Tipi',
  // Border types: none, sunken, raised, flat
  '&Yok', '&Basık', '&Kabarık', '&Düz',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Hücreleri Böl', 'Bölme Yeri',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Satır ve Sütun Sayısına Göre', '&Orjinal Hücreler',
  // number of columns, rows, merge before
  'Sütun Sayısı:', 'Satır Sayısı:', '&Bölmeden Önce Hücreleri Birleştir',
  // to original cols, rows check-boxes
  'Eşit Genişlikte Sütunlara Böl', 'Eşit Yükseklikteki Satırlara Böl',
  // Add Rows form -------------------------------------------------------------
  'Satır Ekle', '&Satır Sayısı:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Sayfa Yapısı', 'Sayfa', 'Üst ve Alt Bilgi',
  // margins group-box, left, right, top, bottom
  'Kenar Boşluğu (mm)', 'Kenar Boşluğu (inç)', '&Sol:', '&Üst:', '&Sağ:', '&Alt:',
  // mirror margins check-box
  '&Kağıda Sığdır',
  // orientation group-box, portrait, landscape
  'Yönlendirme', '&Dikey', '&Yatay',
  // paper group-box, paper size, default paper source
  'Sayfa', 'Kağıt &Büyüklüğü:', '&Kağıt Kaynağı:',
  // header group-box, print on the first page, font button
  'Başlık', '&Metin:', '&Üst Bilgiyi İlk Sayfada Göster', '&Yazı Tipi...',
  // header alignments: left, center, right
  '&Sol', '&Orta', '&Sağ',
  // the same for footer (different hotkeys)
  'Alt Bilgi', '&Metin:', 'Alp Bilgiyi İlk Sayfada Göster', 'Ya&zı Tipi...',
  'So&l', '&Orta', 'Sa&ğ',
  // page numbers group-box, start from
  'Sayfa Numaraları', '&Başlangıç :',
  // hint about codes
  'Özel Karakter Bileşenleri:'#13'&&p - Sayfa Numarası; &&P - Toplam Sayfa; &&d - Günün Tarihi; &&t - Geçerli Saat.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Metin Dosyası Karakter Kodlaması', 'Dosya içeriğinin karakter kodlamasını seçiniz:',
  // thai, japanese, chinese (simpl), korean
  'Thai Dili', 'Japonca', 'Çince (Basit)', 'Korece',
  // chinese (trad), central european, cyrillic, west european
  'Çince (gelişmiş)', 'Orta ve Doğu Avrupa', 'Kril', 'Batı Avrupa',
  // greek, turkish, hebrew, arabic
  'Yunanca','Türkçe', 'İbranice', 'Arapça',
  // baltic, vietnamese, utf-8, utf-16
  'Baltık', 'Vietnamca', 'Evrensel (UTF-8)', 'Evrensel (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Görsel stil', '&Stil Seç:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Metne Çevir', 'Ayıraç Seç:',
  // line break, tab, ';', ','
  'Satır', 'Tab', 'Noktalı virgül', 'Nokta',
  // Table sort form -----------------------------------------------------------
  // error message
   'Birleştirilmiş hücre içeren tablolar sıralanamaz',
  // title, main options groupbox
   'Tabloyu Sırala', 'Sırala',
  // sort by column, case sensitive
   '&Kolona Göre Sırala:', '&Büyük küçük harf duyarlı',
  // heading row, range or rows
   'Başlık Satırını Dikkate Alma', 'Sıralanacak Satırlar: %d den %d e kadar',
  // order, ascending, descending
   'Sıralama', '&Artan', 'A&zalan',
  // data type, text, number
   'Tip', '&Metin', '&Sayı',

  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Numara Ekle', 'Özellikler', 'Sayaç adı:', 'Numaralandırma tipi:',
  // numbering groupbox, continue, start from
  'Numaralandırma', 'Devam et', '.. den başla:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Başlık', 'Etiket:', 'Başlıktan Etiket',
  // position radiogroup
  'Konum', 'Seçili nesnenin üstünde', 'Seçili nesnenin altında',
  // caption text
  'Başlık Yazısı:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numaralandırma', 'Şekil', 'Tablo',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Görünür Kenar', 'Kenarlık',
  // Left, Top, Right, Bottom checkboxes
  'Sol Kenar', 'Üst Kenar', 'Sağ Kenar', 'Alt Kenar',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Sütunu Boyutlandır', 'Satırı Boyutlandır',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Paragraf başı', 'Sol girinti', 'Asılı girinti', 'Sağ girinti',
  // Hints on lists: up one level (left), down one level (right)
  'Bir seviye yukarı', 'Bir seviye aşağı',
  // Hints for margins: bottom, left, right and top
  'Alt boşluk', 'Sol boşluk', 'Sağ boşluk', 'Üst boşluk',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Sola dayalı sekme', 'Sağa dayalı sekme', 'Ortaya dayalı sekme', 'Orana göre dayalı',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normal Girinti', 'Boşluksuz', 'Başlık %d', 'Paragraf Liste',
  // Hyperlink, Title, Subtitle
   'Köprü', 'Başlık', 'Alt Başlık',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
   'Vurgu', 'Zayıf Vurgu', 'Orta Vurgu', 'Kuvvetli',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Alıntı', 'Belirgin Alıntı', 'Zayıf Referans', 'Belirgin Referans',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blok Metin', 'Değişken HTML ', 'HTML Kod', 'HTML Kısaltma',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Tanım', 'HTML Klavye', 'HTML Örnek', 'HTML Metin',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Formatlı', 'HTML Benzer', 'Üst Bilgi', 'Alt Bilgi', 'Sayfa Numarası',
  // Caption
  'Başlık',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Note Referans', 'Sayfa Altı Referans', 'Note Metni', 'Sayfa Altı Metin',
  // Sidenote Reference, Sidenote Text
  'Yan Not Referans', 'Yan Not Metni',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
   'Renk', 'Zemin Rengi', 'Transparan', 'Varsayılan', 'Alt Çizgi Rengi',
  // default background color, default text color, [color] same as text
   'Varsayılan Zemin Rengi', 'varsayılan yazı rengi', 'yazı ile aynı',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'tek', 'ince', 'çift', 'noktalı', 'ince ve noktalı', 'tireli',
  // underline types: thick dashed, long dashed, thick long dashed,
  'kısa tire', 'uzun tire', 'kısa uzun tire',
  // underline types: dash dotted, thick dash dotted,
  'noktalı tire', 'noktalı kısa tire',
  // underline types: dash dot dotted, thick dash dot dotted
   'tire ve iki nokta', 'kısa tire ve iki nokta',
  // sub/superscript: not, subsript, superscript
  'simge /simge değil', 'alt simge', 'ust simge',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'Yazım yönü:', 'varsayılan', 'soldan sağa', 'sağdan sola',
  // bold, not bold
  'koyu', 'koyu değil',
  // italic, not italic
  'italik', 'italik değil',
  // underlined, not underlined, default underline
   'alt çizili', 'alt çizgisiz', 'varsayılan alt çizgi',
  // struck out, not struck out
  'vurgulu', 'vurgusuz',
  // overlined, not overlined
  'satır taşmalı', 'satır taşmasız',
  // all capitals: yes, all capitals: no
  'tümü büyük', 'tümü büyük kapalı',
  // vertical shift: none, by x% up, by x% down
  'ötelemesiz', '%d%% yukarı ötelendi', '%d%% aşağı ötelendi',
  // characters width [: x%]
  'karekter genişliği',
  // character spacing: none, expanded by x, condensed by x
  'normal karekter boşluğu', 'genişletilmiş boşluk %s', 'sıkıştırılmış boşluk %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'varsayılan font', 'varsayılan paragraf', 'devameden',
  // [hyperlink] highlight, default hyperlink attributes
  'vurgulnmış', 'varsayılan',
  // paragraph aligmnment: left, right, center, justify
  'sola dayalı', 'sağa dayalı', 'ortalı', 'sağa hizalı',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'satır yüksekliği: %d%%', 'satır boşluğu: %s', 'satır yüksekliği(en az): %s',
  'satır yüksekliği(tam): %s',
  // no wrap, wrap
  'satır kayma kapalı', 'satır kayma açık',
  // keep lines together: yes, no
  'satırları birlikte tut', 'satırları birlikte tutma',
  // keep with next: yes, no
  'sonraki paragraf ile birlikte', 'sonraki paragraftan ayrı',
  // read only: yes, no
  'sadece okunur', 'değiştirilebilir',
  // body text, heading level x
  'Dış Çizgi: Ana metin', 'Çizgi: %d',
  // indents: first line, left, right
  'ilk satır içerden', 'soldan girinti', 'sağdan girinti',
  // space before, after
  'boşluk önce', 'boşluk sonra',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tab', 'yok', 'dayalı', 'sola', 'sağa', 'ortaya',
  // tab leader (filling character)
  'Gösterge',
  // no, yes
  'hayır', 'evet',
  // [padding/spacing/side:] left, top, right, bottom
  'sol', 'üst', 'sağ', 'alt',
  // border: none, single, double, triple, thick inside, thick outside
  'yok', 'tek', 'çift', 'üçlü', 'içeride ince', 'dışta ince',
  // background, border, padding, spacing [between text and border]
  'Zemin', 'Dış Çizgi', 'aralık', 'boşluk',
  // border: style, width, internal width, visible sides
  'stil', 'genişlik', 'internal genişlik', 'görünür kısım',
  // style inspector -----------------------------------------------------------
  // title
  'Stil Listesi',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(yok)', 'Paragraf', 'Font', 'Özellikler',
  // border and background, hyperlink, standard [style]
  'Çizgi ve Zemin', 'Köprü', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stiller', 'Stil',
  // name, applicable to,
  '&Adı:', '&Uygulanabilir:',
  // based on, based on (no &),
  '&Temel Alınan:', 'Temel Alınan:',
  //  next style, next style (no &)
  'Stil buradan sonraki paragraflar için', 'Stil buradan sonraki paragraflar için:',
  // quick access check-box, description and preview
  '&Hızlı Ulaşım', 'Açıklama  ve ö&n görünüm:',
  // [applicable to:] paragraph and text, paragraph, text
   'paragraf ve yazı', 'paragraf', 'yazı',
  // text and paragraph styles, default font
  'Yazı ve Paragraf Stilleri', 'Varsayılan Font',
  // links: edit, reset, buttons: add, delete
  'Düzenle', 'sıfırla', '&Ekle', '&Sil',
  // add standard style, add custom style, default style name
  'Standart Stil &Ekle...', 'Özel Stil Ekle', 'Stil %d',
  // choose style
   'Stil &seç:',
  // import, export,
   'İçe Aktar...', '&Dışa Aktar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Stil (*.rvst)|*.rvst', 'Dosya Yükleme Hatası', 'Dosyada stil yok',

  // Title, group-box, import styles
   'Dosyadan Stil al', 'Al', 'Alınan Stil:',
  // existing styles radio-group: Title, override, auto-rename
  'Varolan Stiller', 'Üstüne Yaz', 'Yeniden Adlandır',
  // Select, Unselect, Invert,
  '&Seç', 'Seçimi İptal Et', 'Seçimi Çevir',
  // select/unselect: all styles, new styles, existing styles
   '&Tüm Stiller', '&Yeni Stil', '&Varolan Stiller',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Formatı Temizle', 'Tüm Stiller',
  // dialog title, prompt
  'Stiller', '&Uygulanacak Stil:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Eş Anlamlılar', '&Tümünü Yoksay', '&Sözlüğe Ekle',
  // Progress messages ---------------------------------------------------------
  'İndiriliyor %s', 'Yazdırmaya Hazırlanıyor..', 'Yazdırılan Sayfa %d',
  'RTF den çevriliyor...',  'RTF ye çevriliyor...',
  'Okunuyor %s...', 'Yazılıyor %s...', 'Metin',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Yok', 'Üstbilgi', 'Altbilgi', 'Yanbilgi', 'Metin Kutusu'
  );


initialization
  RVA_RegisterLanguage(
    'Turkish', // english language name, do not translate
    // {*} 'Turkish', // native language name
    'Türkçe', // native language name
    TURKISH_CHARSET, @Messages);
end.
