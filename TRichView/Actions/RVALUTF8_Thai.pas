﻿// This file is a copy of RVAL_Thai.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Thai translation                                }
{                                                       }
{                                                       }
{*******************************************************}

unit RVALUTF8_Thai;

interface
{$I RV_Defs.inc}
implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'นิ้ว', 'เซนติเมตร', 'มิลลิเมตร', '1/6 นิ้ว', 'พิกเซล', 'จุด',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'ซม.', 'มม.', '1/6''', 'พ.', 'จุด',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&เอกสาร', '&แก้ไข', 'รูป&แบบ', 'แบบ&อักษร', 'ย่อ&หน้า', '&แทรก', 'ตา&ราง', 'หน้า&ต่าง', 'ช่วย&เหลือ',
  // exit
  'ออก&',
  // top-level menus: View, Tools,
  'มุม&มอง', 'เครื่อง&มือ',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'ขนาด', 'ลักษณะแบบอักษร', 'เลือก', 'จัด&แนวเซลล์', 'เส้น&ขอบ',
  // menus: Table cell rotation
  'การหมุน&เซลล์',
  // menus: Text flow, Footnotes/endnotes
  '&ทิศทางของข้อความ', '&เชิงอรรถ',
  // ribbon tabs: tab1, tab2, view, table
  '&หน้าแรก', '&ขั้นสูง', '&มุมมอง', '&ตาราง',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'คลิปบอร์ด', 'แบบอักษร', 'ย่อหน้า', 'รายการ', 'แก่ไข',
  // ribbon groups: insert, background, page setup,
  'แทรก', 'พื้นหลัง', 'ตั้งค่าหน้ากระดาษ',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'การเชื่อมโยง', 'เชิงอรรถ', 'หน้าต่าง', 'แสดง/ซ่อน', 'ขยาย', 'ตัวเลือก',
  // ribbon groups on table tab: insert, delete, operations, borders
  'แทรก', 'ลบ', 'การดำเนินการ', 'ขอบ',
  // ribbon groups: styles 
  'รูปแบบ',
  // ribbon screen tip footer,
  'กด F1 เพื่อดูรายละเอียดเพิ่มเติม',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'เอกสารนี้', 'บันทึกและคัดลอกเอกสาร', 'ตัวอย่างก่อนพิมพ์และพิมพ์เอกสาร',
  // ribbon label: units combo
  'หน่วย:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&สร้าง', 'สร้าง|สร้างเอกสารใหม่',
  // TrvActionOpen
  '&เปิด...', 'เปิด|เปิดเอกสาร',
  // TrvActionSave
  '&บันทึก', 'บันทึก|บันทึกเอกสาร',
  // TrvActionSaveAs
  'บันทึก&เป็น...', 'บันทึกเป็น...|บันทึกเอกสารในชื่อใหม่',
  // TrvActionExport
  '&ส่งออก...', 'ส่งออก|ส่งออกเอกสารเป็นรูปแบบอื่น',
  // TrvActionPrintPreview
  'ตัวอย่าง&ก่อนพิมพ์', 'ตัวอย่างก่อนพิมพ์|แสดงเอกสารเหมือนที่จะพิมพ์ออกมา',
  // TrvActionPrint
  '&พิมพ์...', 'พิมพ์|ตั้งค่าการพิมพ์และพิมพ์เอกสาร',
  // TrvActionQuickPrint
  '&พิมพ์', '&พิมพ์ด่วน', 'พิมพ์|พิมพ์เอกสาร',
  // TrvActionPageSetup
  'ตั้งค่า&หน้ากระดาษ...', 'ตั้งค่าหน้ากระดาษ|ขอบกระดาษ, ขนาดกระดาษ, การวางแนว, กระดาษ, หัวและท้ายกระดาษ',
  // TrvActionCut
  'ตัด&', 'ตัด|ตัดส่วนที่เลือกและนำไว้ที่คลิปบอร์ด',
  // TrvActionCopy
  '&คัดลอก', 'คัดลอก|ตัดส่วนที่เลือกและนำไว้ที่คลิปบอร์ด',
  // TrvActionPaste
  '&วาง', 'วาง|วางเนื้อหาของคลิปบอร์ด',
  // TrvActionPasteAsText
  'วาง&เป็นข้อความ', 'วางเป็นข้อความ|วางข้อความของคลิปบอร์ด',  
  // TrvActionPasteSpecial
  'วาง&แบบพิเศษ...', 'วางแบบพิเศษ|วางเนื้อหาของคลิปบอร์ดในรูปแบบพิเศษ',
  // TrvActionSelectAll
  'เลือก&ทั้งหมด', 'เลือกทั้งหมด|เลือกทั้งเอกสาร',
  // TrvActionUndo
  '&เลิกทำ', 'เลิกทำ|กลับไปยังการกระทำล่าสุด',
  // TrvActionRedo
  '&ทำซ้ำ', 'ทำซ้ำ|ย้อนกลับการเลิกทำ',
  // TrvActionFind
  '&ค้นหา...', 'ค้นหาข้อความเจาะจงในเอกสาร',
  // TrvActionFindNext
  'ค้นหา&ถัดไป', 'ค้นหาถัดไป|ค้นหาต่อไปจากล่าสุด',
  // TrvActionReplace
  '&แทนที่...', 'แทนที่|ค้นหาและแทนที่ข้อความเจาะจงในเอกสาร',
  // TrvActionInsertFile
  '&ไฟล์...', 'แทรกไฟล์|เพิ่มเนื้อความจากไฟล์ลงในเอกสารนี้',
  // TrvActionInsertPicture
  '&รูปภาพ...', 'แทรกรูปภาพ|แทรกไฟล์รูปภาพ',
  // TRVActionInsertHLine
  '&เส้นแนวนอน', 'แทรกเส้นแนวนอน|แทรกเส้นแนวนอน',
  // TRVActionInsertHyperlink
  '&การเชื่อมโยงหลายมิติ...', 'แทรกการเชื่อมโยงหลายมิติ|แทรกการเชื่อมโยงหลายมิติ',
  // TRVActionRemoveHyperlinks
  '&การเชื่อมโยงหลายมิติ', 'ลบการเชื่อมโยงหลายมิติ|ลบการเชื่อมโยงหลายมิติที่เลือก',  
  // TrvActionInsertSymbol
  '&สัญลักษณ์...', 'แทรกสัญลักษณ์|แทรกสัญลักษณ์',
  // TrvActionInsertNumber
  {*} '&Number...', 'Insert Number|Inserts a number',
  // TrvActionInsertCaption
  {*} 'Insert &Caption...', 'Insert Caption|Inserts a caption for the selected object',
  // TrvActionInsertTextBox
  {*} '&Text Box', 'Insert Text Box|Inserts a text box',
  // TrvActionInsertSidenote
  {*} '&Sidenote', 'Insert Sidenote|Inserts a note displayed in a text box',
  // TrvActionInsertPageNumber
  {*} '&Page Number', 'Insert Page Number|Inserts a page number',
  // TrvActionParaList
  '&สัญลักษณ์แสดงหัวข้อย่อย&และลำดับเลข...', 'สัญลักษณ์แสดงหัวข้อย่อยและลำดับเลข|ใช้หรือแก้ไขหัวข้อย่อยหรือลำดับเลขสำหรับย่อหน้าที่เลือก',
  // TrvActionParaBullets
  '&สัญลักษณ์แสดงหัวข้อย่อย', 'สัญลักษณ์แสดงหัวข้อย่อย|เพิ่มหรือลบสัญลักษณ์แสดงหัวข้อย่อยสำหรับย่อหน้า',
  // TrvActionParaNumbering
  '&ลำดับเลข', 'ลำดับเลข|เพิ่มหรือลบลำดับเลขสำหรับย่อหน้า',
  // TrvActionColor
  'สี&พื้นหลัง...', 'พื้นหลัง|เปลี่ยนสีพื้นหลังของเอกสาร',
  // TrvActionFillColor
  '&เติมสี...', 'เติมสี|เปลี่ยนสีพื้นหลังของข้อความ ย่อหน้า เซลลล์ ตาราง หรือเอกสาร',
  // TrvActionInsertPageBreak
  '&แทรกตัวแบ่งหน้า', 'แทรกตัวแบ่งหน้า|แทรกตัวแบ่งหน้า',
  // TrvActionRemovePageBreak
  '&ลบตัวแบ่งหน้า', 'ลบตัวแบ่งหน้า|ลบตัวแบ่งหน้า',
  // TrvActionClearLeft
  'จัดแนวข้อความ&ชิดซ้าย', 'จัดแนวข้อความชิดซ้าย|วางย่อหน้านี้ไว้ด้านล่างของภาพที่ชิดซ้าย',
  // TrvActionClearRight
  'จัดแนวข้อความ&ชิดขวา', 'จัดแนวข้อความชิดขวา|วางย่อหน้านี้ไว้ด้านล่างของภาพที่ชิดขวา',
  // TrvActionClearBoth
  'จัด&เต็มแนว', 'จัดเต็มแนว|วางย่อหน้านี้ไว้ด้านล่างของภาพที่ชิดทั้งซ้ายและขวา',
  // TrvActionClearNone
  '&จัดแนวปกติ', 'จัดแนวปกติ|ให้ข้อความอยู่รอบๆ ภาพ',
  // TrvActionVAlign
  '&ตำแหน่งของวัตถุ...', 'ตำแหน่งของวัตถุ|เปลี่ยนตำแหน่งของวัตถุที่เลือก',  
  // TrvActionItemProperties
  'คุณสมบัติ&ของวัตถุ...', 'คุณสมบัติของวัตถุ|กำหนดคุณสมบัติของวัตถุใช้งานอยู่',
  // TrvActionBackground
  '&พื้นหลัง...', 'พื้นหลัง|เลือกภาพและสีพื้นหลัง',
  // TrvActionParagraph
  '&ย่อหน้า...', 'ย่อหน้า|เปลี่ยนรูปแบบย่อหน้า',
  // TrvActionIndentInc
  '&เพิ่มการเยื้อง', 'เพิ่มการเยื้อง|เพิ่มระดับการเยื้องของย่อหน้า',
  // TrvActionIndentDec
  '&ลดการเยื้อง', 'ลดการเยื้อง|ลดระดับการเยื้องของย่อหน้า',
  // TrvActionWordWrap
  '&การตัดคำ', 'การตัดคำ|ปรับการตัดคำสำหรับย่อหน้าที่เลือก',
  // TrvActionAlignLeft
  'ชิด&ซ้าย', 'ชิดซ้าย|จัดแนวข้อความชิดซ้าย',
  // TrvActionAlignRight
  'ชิด&ขวา', 'ชิดขวา|จัดแนวข้อความชิดขวา',
  // TrvActionAlignCenter
  'กึ่ง&กลาง', 'กึ่งกลาง|จัดแนวข้อความกึ่งกลาง',
  // TrvActionAlignJustify
  'เต็ม&แนว', 'เต็มแนว|จัดแนวชิดทั้งซ้ายและขวา',
  // TrvActionParaColor
  'สีพื้นหลัง&ของย่อหน้า...', 'สีพื้นหลังของย่อหน้า|ตั้งค่าสีพื้นหลังของย่อหน้า',
  // TrvActionLineSpacing100
  '&ระยะห่าง 1 บรรทัด', 'ระยะห่าง 1 บรรทัด|ตั้งค่าระยะห่าง 1 บรรทัด',
  // TrvActionLineSpacing150
  'ระยะห่าง &1.5 บรรทัด', 'ระยะห่าง 1.5 บรรทัด|ตั้งค่าระยะห่าง 1.5 บรรทัด',
  // TrvActionLineSpacing200
  '&ระยะห่าง 2 บรรทัด', 'ระยะห่าง 2 บรรทัด|ตั้งค่าระยะห่าง 2 บรรทัด',
  // TrvActionParaBorder
  'ขอบและพื้นหลัง&ของย่อหน้า ...', 'ขอบและพื้นหลังของย่อหน้า|ตั้งค่าขอบและพื้นหลังของย่อหน้า',
  // TrvActionInsertTable
  '&แทรกตาราง...', '&ตาราง', 'แทรกตาราง|แทรกตารางใหม่',
  // TrvActionTableInsertRowsAbove
  'แทรกแถว&ด้านบน', 'แทรกแถวด้านบน|แทรกแถวใหม่ด้านบนเซลล์ที่เลือก',
  // TrvActionTableInsertRowsBelow
  'แทรกแถว&ด้านล่าง', 'แทรกแถวด้านล่าง|แทรกแถวใหม่ด้านล่างเซลล์ที่เลือก',
  // TrvActionTableInsertColLeft
  'แทรกคอลัมน์&ด้านซ้าย', 'แทรกคอลัมน์ด้านซ้าย|แทรกคอลัมน์ใหม่ด้านซ้ายเซลล์ที่เลือก',
  // TrvActionTableInsertColRight
  'แทรกคอลัมน์&ด้านขวา', 'แทรกคอลัมน์ด้านขวา|แทรกคอลัมน์ใหม่ด้านขวาเซลล์ที่เลือก',
  // TrvActionTableDeleteRows
  'ลบแถว', 'ลบแถว|ลบแถวของเซลล์ที่เลือก',
  // TrvActionTableDeleteCols
  'ลบ&คอลัมน์', 'ลบคอลัมน์|ลบคอลัมน์ของเซลล์ที่เลือก',
  // TrvActionTableDeleteTable
  'ลบ&ตาราง', 'ลบตาราง|ลบตาราง',
  // TrvActionTableMergeCells
  'ผสาน&เซลล์', 'ผสานเซลล์|ผสานเซลล์ที่เลือก',
  // TrvActionTableSplitCells
  'แยก&เซลล์...', 'แยกเซลล์|แยกเซลล์ที่เลือก',
  // TrvActionTableSelectTable
  'เลือก&ตาราง', 'เลือกตาราง|เลือกตาราง',
  // TrvActionTableSelectRows
  'เลือก&แถว', 'เลือกแถว|เลือกแถว',
  // TrvActionTableSelectCols
  'เลือก&คอลัมน์', 'เลือกคอลัมน์|เลือกคอลัมน์',
  // TrvActionTableSelectCell
  'เลือก&เซลล์', 'เลือกเซลล์|เลือกเซลล์',
  // TrvActionTableCellVAlignTop
  'จัดชิด&ด้านบน', 'จัดชิดด้านบน|จัดเนื้อหาชิดด้านบน',
  // TrvActionTableCellVAlignMiddle
  'จัด&กึ่งกลาง', 'จัดกึ่งกลาง|จัดเนื้อหากึ่งกลางเซลล์',
  // TrvActionTableCellVAlignBottom
  'จัดชิด&ด้านล่าง', 'จัดชิดด้านล่าง|จัดเนื้อหาชิดด้านล่าง',
  // TrvActionTableCellVAlignDefault
  '&ค่าเริ่มต้นการจัดแนวแนวตั้ง', 'ค่าเริ่มต้นการจัดแนวแนวตั้ง|ตั้งเป็นค่าเริ่มต้นการจัดแนวแนวตั้งสำหรับเซลล์ที่เลือก',
  // TrvActionTableCellRotationNone
  '&ไม่มีเซลล์ที่หมุน', 'ไม่มีเซลล์ที่หมุน|หมุนเนื้อหาในเซลล์ 0 องศา',
  // TrvActionTableCellRotation90
  'เซลล์หมุน &90 องศา', 'เซลล์หมุน 90 องศา|หมุนเนื้อหาในเซลล์ 90 องศา',
  // TrvActionTableCellRotation180
  'เซลล์หมุน &180 องศา', 'เซลล์หมุน 180 องศา|หมุนเนื้อหาในเซลล์ 180 องศา',
  // TrvActionTableCellRotation270
  'เซลล์หมุน &270 องศา', 'เซลล์หมุน 270 องศา|หมุนเนื้อหาในเซลล์ 270 องศา',
  // TrvActionTableProperties
  'คุณสมบัติ&ตาราง...', 'คุณสมบัติตาราง|เปลี่ยนคุณสมบัติตารางที่เลือก',
  // TrvActionTableGrid
  'แสดง&เส้นตาราง', 'แสดงเส้นตาราง|แสดงหรือซ่อนเส้นตาราง',
  // TRVActionTableSplit
  'แยก&ตาราง', 'แยกตาราง|แยกเป็นสองตารางจากแถวที่เลือก',
  // TRVActionTableToText
  'แปลงเป็น&ข้อความ...', 'แปลงเป็นข้อความ|แปลงจากตารางเป็นข้อความ',
  // TRVActionTableSort
  '&เรียงลำดับ...', 'เรียงลำดับ|เรียงลำดับแถวของตาราง',
  // TrvActionTableCellLeftBorder
  '&ขอบซ้าย', 'ขอบซ้าย|แสดงหรือซ่อนขอบด้านซ้าย',
  // TrvActionTableCellRightBorder
  '&ขอบขวา', 'ขอบขวา|แสดงหรือซ่อนขอบด้านขวา',
  // TrvActionTableCellTopBorder
  '&ขอบบน', 'ขอบบน|แสดงหรือซ่อนขอบด้านบน',
  // TrvActionTableCellBottomBorder
  '&ขอบล่าง', 'ขอบล่าง|แสดงหรือซ่อนขอบด้านล่าง',
  // TrvActionTableCellAllBorders
  '&ขอบทุกด้าน', 'ขอบทุกด้าน|แสดงหรือซ่อนขอบทุกด้านของเซลล์',
  // TrvActionTableCellNoBorders
  '&ไม่มีขอบ', 'ไม่มีขอบ|ซ่อนขอบของเซลล์ทั้งหมด',
  // TrvActionFonts & TrvActionFontEx
  '&แบบอักษร...', 'แบบอักษร|เปลี่ยนแบบอักษรของข้อความที่เลือก',
  // TrvActionFontBold
  '&ตัวหนา', 'ตัวหนา|ทำข้อความที่เลือกเป็นตัวหนา',
  // TrvActionFontItalic
  '&ตัวเอียง', 'ตัวเอียง|ทำข้อความที่เลือกเป็นตัวเอียง',
  // TrvActionFontUnderline
  '&ขีดเส้นใต้', 'ขีดเส้นใต้|ขีดเส้นใต้ข้อความที่เลือก',
  // TrvActionFontStrikeout
  '&ขีดทับ', 'ขีดทับ|ขีดทับข้อความที่เลือก',
  // TrvActionFontGrow
  '&เพิ่มขนาดอักษร', 'เพิ่มขนาดอักษร|เพิ่มขนาดอักษรที่เลือกขึ้น 10%',
  // TrvActionFontShrink
  '&ลดขนาดอักษร', 'ลดขนาดอักษร|ลดขนาดอักษรที่เลือกลง 10%',
  // TrvActionFontGrowOnePoint
  '&เพิ่มขนาดอักษร 1 จุด', 'เพิ่มขนาดอักษร 1 จุด|เพิ่มขนาดอักษรที่เลือกขึ้น 1 จุด',
  // TrvActionFontShrinkOnePoint
  '&ลดขนาดอักษร 1 จุด', 'ลดขนาดอักษร 1 จุด|ลดขนาดอักษรที่เลือกลง 1 จุด',
  // TrvActionFontAllCaps
  '&ตัวพิมพ์ใหญ่ทั้งหมด', 'ตัวพิมพ์ใหญ่ทั้งหมด|เปลี่ยนอักษรทั้งหมดที่เลือกเป็นตัวพิมพ์ใหญ่',
  // TrvActionFontOverline
  '&ขีดเส้นบน', 'ขีดเส้นบน|ขีดเส้นเหนือข้อความที่เลือก',
  // TrvActionFontColor
  'สีของ&ข้อความ...', 'สีของข้อความ|เปลี่ยนสีของข้อความที่เลือก',
  // TrvActionFontBackColor
  'สีพื้นหลัง&ของข้อความ...', 'สีพื้นหลังของข้อความ|เปลี่ยนสีพื้นหลังของข้อความที่เลือก',
  // TrvActionAddictSpell3
  'ตรวจสอบ&ตัวสะกด', 'ตรวจสอบตัวสะกด|ตรวจสอบการสะกดคำ',
  // TrvActionAddictThesaurus3
  'อรรถา&ภิธาน', 'อรรถาภิธาน|เสนอคำอื่นที่มีความหมายใกล้เคียงกับคำที่เลือกไว้',
  // TrvActionParaLTR
  'ซ้ายไปขวา', 'ซ้ายไปขวา|ตั้งค่าทิศทางอักษรจากซ้ายไปขวาสำหรับย่อหน้าที่เลือก',
  // TrvActionParaRTL
  'ขวาไปซ้าย', 'ขวาไปซ้าย|ตั้งค่าทิศทางอักษรจากขวาไปซ้ายสำหรับย่อหน้าที่เลือก',
  // TrvActionLTR
  'ข้อความจากซ้ายไปขวา', 'ข้อความจากซ้ายไปขวา|ตั้งค่าทิศทางอักษรจากซ้ายไปขวาสำหรับข้อความที่เลือก',
  // TrvActionRTL
  'ข้อความจากขวาไปซ้าย', 'ข้อความจากขวาไปซ้าย|ตั้งค่าทิศทางอักษรจากขวาไปซ้ายสำหรับข้อความที่เลือก',
  // TrvActionCharCase
  'ลักษณะตัวพิมพ์', 'ลักษณะตัวพิมพ์|เปลี่ยนตัวพิมพ์ของข้อความที่เลือก',
  // TrvActionShowSpecialCharacters
  '&เครื่องหมายย่อหน้า', 'เครื่องหมายย่อหน้า|แสดงหรือซ่อนเครื่องหมายย่อหน้า เช่น ย่อหน้า แท็บ และเว้นวรรค',
  // TrvActionSubscript
  'ตัว&ห้อย', 'ตัวห้อย|ปรับข้อความที่เลือกไปเป็นตัวห้อย',
  // TrvActionSuperscript
  'ตัว&ยก', 'ตัวยก|ปรับข้อความที่เลือกไปเป็นตัวยก',
  // TrvActionInsertFootnote
  '&เชิงอรรถ', 'เชิงอรรถ|ใส่เชิงอรรถ',
  // TrvActionInsertEndnote
  '&อ้างอิงท้ายเรื่อง', 'อ้างอิงท้ายเรื่อง|ใส่อ้างอิงท้ายเรื่อง',
  // TrvActionEditNote
  '&แก้ไขบันทึก', 'แก้ไขบันทึก|เริ่มแก้ไขเชิงอรรถหรืออ้างอิง',
  // TrvActionHide
  '&ซ่อน', 'ซ่อน|ซ่อนหรือแสดงส่วนที่เลือก',  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&รูปแบบ...', 'รูปแบบ|เปิดตัวจัดการรูปแบบ',
  // TrvActionAddStyleTemplate
  '&เพิ่มรูปแบบ...', 'เพิ่มรูปแบบ|สร้างรูปแบบข้อความหรือรูปแบบย่อหน้าใหม่',
  // TrvActionClearFormat,
  '&ล้างแบบ', 'ล้างแบบ|ล้างแบบข้อความและแบบย่อหน้าทั้งหมดจากส่วนที่เลือก',
  // TrvActionClearTextFormat,
  'ล้าง&แบบข้อความ', 'ล้างแบบข้อความ|ล้างแบบทั้งหมดจากข้อความที่เลือก',
  // TrvActionStyleInspector
  'ตรวจสอบ&รูปแบบ', 'ตรวจสอบรูปแบบ|แสดงหรือซ่อนตัวตรวจสอบรูปแบบ',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'ตกลง', 'ยกเลิก', 'ปิด', 'แทรก', '&เปิด...', '&บันทึก...', '&ล้าง', 'ช่วยเหลือ', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'เปอร์เซนต์',
  // left, top, right, bottom sides
  'ซ้าย', 'บน', 'ขวา', 'ล่าง',
  // save changes? confirm title
  'ต้องการบันทึกการเปลี่ยนแปลงไปเป็น %s ไหม?', 'ยืนยัน',
  // warning: losing formatting
  '%s อาจจะมีคุณลักษณะที่ไม่สอดคล้องกับรูปแบบที่ต้องการบันทึก'#13+
  'ต้องการบันทึกเอกสารในรูปแบบนี้หรือไม่?',
  // RVF format name
  'รูปแบบ RichView',
  // Error messages ------------------------------------------------------------
  'ผิดพลาด',
  'การโหลดไฟล์ผิดพลาด'#13#13'เหตุผลที่น่าเป็นไปได้:'#13'- โปรแกรมไม่สนับสนุนรูปแบบไฟล์นี้;'#13+
  '- ไฟล์เสียหาย;'#13'- ไฟล์ถูกเปิดและถูกล็อคโดยโปรแกรมอื่น',
  'มีข้อผิดพลาดในการโหลดไฟล์ภาพ'#13#13'เหตุผลที่น่าเป็นไปได้:'#13'- โปรแกรมไม่สนับสนุนรูปแบบไฟล์ภาพนี้;'#13+
  '- ไม่ได้เป็นไฟล์ภาพ;'#13+
  '- ไฟล์เสียหาย;'#13'- ไฟล์ถูกเปิดและถูกล็อคโดยโปรแกรมอื่น',
  'การบันทึกไฟล์ผิดพลาด'#13#13'เหตุผลที่น่าเป็นไปได้:'#13'- ไม่มีพื้นที่ดิสก์;'#13+
  '- ดิสก์ป้องกันการเขียน;'#13'- ยังไม่ได้ใส่ดิสก์เก็บข้อมูล;'#13+
  '- ไฟล์ถูกเปิดและถูกล็อคโดยโปรแกรมอื่น;'#13'- ดิสก์เสียหาย',  
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'ไฟล์ RichView (*.rvf)|*.rvf',  'ไฟล์ RTF (*.rtf)|*.rtf' , 'ไฟล์ XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'ไฟล์ข้อความ (*.txt)|*.txt', 'ไฟล์ข้อความ - Unicode (*.txt)|*.txt', 'ไฟล์ข้อความตรวจสอบเองอัตโนมัติ (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'ไฟล์ HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - อย่างง่าย (*.htm;*.html)|*.htm;*.html',
  // DocX
  'ไมโครซอฟท์เวิร์ด (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'การค้นหาสมบูรณ์', 'การค้นหา ''%s'' ไม่พบ',
  // 1 string replaced; Several strings replaced
  'ถูกแทนที่ 1 จุด', 'ถูกแทนที่ %d จุด',
  // continue search from the beginning/end?
  'หมดเอกสาร ต้องการไปที่จุดเริ่มต้นหรือไม่?',
  'อยู่ที่จุดเริ่มต้นเอกสาร ต้องการให้ไปจนหมดเอกสารหรือไม่?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'โปร่งใส', 'อัตโนมัติ',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'ดำ', 'น้ำตาล', 'เขียวมะกอก', 'เขียวเข้ม', 'เขียวหัวเป็ดเข้ม', 'น้ำเงินเข้ม', 'คราม', 'เทา-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'แดงเข้ม', 'ส้ม', 'เหลืองเข้ม', 'เขียว', 'เขียวหัวเป็ด', 'น้ำเงิน', 'น้ำเงินเทา', 'เทา-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'แดง', 'ส้มอ่อน', 'เขียวมะนาว', 'เขียวน้ำทะเล', 'น้ำทะเล', 'ฟ้าอ่อน', 'ม่วง', 'เทา-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'ชมพู', 'ทอง', 'เหลือง', 'เขียวสว่าง', 'เขียวขุ่น', 'ท้องฟ้า', 'พลัม', 'เทา-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'กุหลาบ', 'น้ำตาล', 'เหลืองอ่อน', 'เขียวอ่อน', 'เขียวขุ่นอ่อน', 'ฟ้าจาง', 'ลาเวนเดอร์', 'ขาว',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'โปร่ง&แสง', '&อัตโนมัติ', '&สีเพิ่มเติม...', '&สีพื้นฐาน',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'พื้นหลัง', '&สี:', 'ตำแหน่ง', 'พื้นหลัง', 'ตัวอย่างข้อความ',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&ไม่มี', '&เต็มหน้าต่าง', '&ภาพเรียงเสมอ', '&ภาพเรียงต่อ', '&กึ่งกลางกลาง',
  // Padding button
  '&การขยาย...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'เติมสี', '&ใช้เป็น:', '&สีเพิ่มเติม...', '&การขยาย...',
  'กรุณาเลือกสี',
  // [apply to:] text, paragraph, table, cell
  'ข้อความ', 'ย่อหน้า' , 'ตาราง', 'เซลล์',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'แบบอักษร', 'แบบอักษร', 'เค้าโครง',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&แบบอักษร:', '&ขนาดแบบอักษร', 'ลักษณะแบบอักษร', '&ตัวหนา', '&ตัวเอียง',
  // Script, Color, Back color labels, Default charset
  'การ&เข้ารหัส:', '&สี:', 'พื้นหลัง:', '(ค่าพื้นฐาน)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'ลักษณะพิเศษ', 'ลักษณะเส้นใต้', '&ขีดเส้นบน', 'ขีด&ทับ', 'ตัวพิมพ์ใหญ่ทั้งหมด',
  // Sample, Sample text
  'ตัวอย่าง', 'ข้อความตัวอย่าง',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'ระยะห่างระหว่างตัวอักษร', '&ระยะห่าง:', '&ขยาย', '&บีบ',
  // Offset group-box, Offset label, Down, Up,
  'ตำแหน่งในแนวตั้ง', '&ขนาด:', 'ต่ำ&ลง', 'ยก&ขึ้น',
  // Scaling group-box, Scaling
  'มาตราส่วน', 'มาตรา&ส่วน:',
  // Sub/super script group box, Normal, Sub, Super
  'ตัวห้อยและตัวยก', '&ปกติ', 'ตัว&ห้อย', 'ตัว&ยก',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'ภายใน', '&บน:', '&ซ้าย:', '&ล่าง:', '&ขวา:', '&ค่าเท่ากัน',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'แทรกการเชื่อมโยงหลายมิติ', 'การเชื่อมโยงหลายมิติ', 'ข้อ&ความที่ใช้แสดง:', 'ที่อยู่&เป้าหมาย', '<<เลือก>>',
  // cannot open URL
  'ไมสามารถนำไป "%s" ได้',
  // hyperlink properties button, hyperlink style
  '&กำหนดเอง...', '&รูปแบบ:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) รฑolors
  'การเชื่อมโยงหลายมิติ', 'สีปกติ', 'สีที่กำลังทำงาน',
  // Text [color], Background [color]
  '&ข้อความ:', '&พื้นหลัง',
  // Underline [color], 
  'ขีด&เส้นใต้:',
  // Text [color], Background [color] (different hotkeys)
  'ข้อ&ความ:', 'พื้น&หลัง',
  // Underline [color], Attributes group-box,
  'ขีด&เส้นใต้:', 'ลักษณะ',
  // Underline type: always, never, active (below the mouse)
  '&ขีดเส้นใต้:', 'ตลอด', 'ไม่', 'กำลังทำงาน',
  // Same as normal check-box
  'เป็น&ปกติ',
  // underline active links check-box
  '&ขีดเส้นใต้ลิงค์ที่ทำงาน',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'แทรกสัญลักษณ์', '&แบบอักษร:', '&ชุดอักขระ:', 'Unicode',
  // Unicode block
  '&บล็อค:',  
  // Character Code, Character Unicode Code, No Character
  'โค้ดอักขระ: %d', 'โค้ดอักขระ: Unicode %d', '(ไม่มีตัวอักษร)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'แทรกตาราง', 'ขนาดตาราง', 'จำนวน&คอลัมน์:', 'จำนวน&แถว:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'เค้าโครงตาราง', '&ปรับขนาดอัตโนมัติ', 'พอดีกับ&หน้าต่าง', 'กำหนด&ขนาดเอง',
  // Remember check-box
  'จำขนาด&ตารางใหม่',
  // VAlign Form ---------------------------------------------------------------
  'ตำแหน่ง',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'คุณสมบัติ', 'ภาพ', {*}'Position and Size', 'เส้น', 'ตาราง', 'แถว', 'เซลล์',
  // Image Appearance, Image Misc, Number tabs
  {*} 'Appearance', 'Miscellaneous', 'Number',
  // Box position, Box size, Box appearance tabs
  {*} 'Position', 'Size', 'Appearance',
  // Preview label, Transparency group-box, checkbox, label
  'ตัวอย่าง:', 'ความโปร่งใส', '&โปร่งใส', 'สี&ความโปร่งใส:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&เปลี่ยน...', {*}'&Save...', 'อัตโนมัติ',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'วางตำแหน่งแนวตั้ง', '&การจัดตำแหน่ง:',
  'ล่างเส้นล่างของข้อความ', 'ตรงกลางเส้นล่างของข้อความ',
  'บนเส้นบน', 'ล่างเส้นล่าง', 'กลางเส้นกลาง',
  // align to left side, align to right side
  'ไปยังด้านซ้าย', 'ไปยังด้านขวา',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&พอดีกับ:', 'เต็มจอ', '&ความกว้าง:', '&ความสูง:', 'ขนาดพื้นฐาน: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  {*} 'Scale &proportionally', '&Reset',
  // Inside the border, border, outside the border groupboxes
  {*} 'Inside the border', 'Border', 'Outside the border',
  // Fill color, Padding (i.e. margins inside border) labels
  {*} '&Fill color:', '&Padding:',
  // Border width, Border color labels
  {*} 'Border &width:', 'Border &color:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  {*} '&Horizontal spacing:', '&Vertical spacing:',
  // Miscellaneous groupbox, Tooltip label
  {*} 'Miscellaneous', '&Tooltip:',
  // web group-box, alt text
  'เว็บ', 'ข้อความ&สำรอง:',
  // Horz line group-box, color, width, style
  'เส้นแนวนอน', '&สี:', '&ความกว้าง:', '&รูปแบบ:',
  // Table group-box; Table Width, Color, CellSpacing,
  'ตาราง', '&ความกว้างภายในเซลล์:', '&เติมสี:', {*}'Cell &Spacing...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  {*} '&Horizontal cell padding:', '&Vertical cell padding:', 'Border and background',
  // Visible table border sides button, auto width (in combobox), auto width label
  {*} 'Visible &Border Sides...', 'Auto', 'auto',
  // Keep row content together checkbox
  {*} '&Keep content together',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  {*} 'Rotation', '&None', '&90', '&180', '&270',
  // Cell border label, Visible cell border sides button,
  {*} 'Border:', 'Visible B&order Sides...',
  // Border, CellBorders buttons
  '&ขอบตาราง...', '&ขอบเซลล์...',
  // Table border, Cell borders dialog titles
  'ขอบตาราง', 'ขอบเซลล์เริ่มต้น',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'การพิมพ์', '&ไม่อนุญาตให้คอลัมน์แบ่งข้ามหน้ากระดาษ', 'จำนวน&หัวเรื่องในแถว:',
  'ขึ้นหัวเรื่องในแถวใหม่เมื่อขึ้นหน้าใหม่',
  // top, center, bottom, default
  '&บน', '&กลาง', '&ล่าง', '&ค่าเริ่มต้น',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'การตั้งค่า', '&ความกว้างที่ต้องการ:', '&สูงอย่างน้อย:', '&เติมสี:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'ขอบ', 'ด้านที่มองเห็น:',  'สี&เงา:', 'แสง&สี', '&สี:',
  // Background image
  'รูป&ภาพ...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  {*} 'Horizontal position', 'Vertical position', 'Position in text',
  // Position types: align, absolute, relative
  {*} 'Align', 'Absolute position', 'Relative position',
  // [Align] left side, center, right side
  {*} 'left side of box to left side of',
  {*} 'center of box to center of',
  {*} 'right side of box to right side of',
  // [Align] top side, center, bottom side
  {*} 'top side of box to top side of',
  {*} 'center of box to center of',
  {*} 'bottom side of box to bottom side of',
  // [Align] relative to
  {*} 'relative to',
  // [Position] to the right of the left side of
  {*} 'to the right of the left side of',
  // [Position] below the top side of
  {*} 'below the top side of',
  // Anchors: page, main text area (x2)
  {*} '&Page', '&Main text area', 'P&age', 'Main te&xt area',
  // Anchors: left margin, right margin
  {*} '&Left margin', '&Right margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  {*} '&Top margin', '&Bottom margin', '&Inner margin', '&Outer margin',
  // Anchors: character, line, paragraph
  {*} '&Character', 'L&ine', 'Para&graph',
  // Mirrored margins checkbox, layout in table cell checkbox
  {*} 'Mirror&ed margins', 'La&yout in table cell',
  // Above text, below text
  {*} '&Above text', '&Below text',
  // Width, Height groupboxes, Width, Height labels
  {*} 'Width', 'Height', '&Width:', '&Height:',
  // Auto height label, Auto height combobox item
  {*} 'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  {*} 'Vertical alignment', '&Top', '&Center', '&Bottom',
  // Border and background button and title
  {*} 'B&order and background...', 'Box Border and Background',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files, URL
  'วางแบบพิเศษ', '&วางเป็น:', 'รูปแบบ Rich text', 'รูปแบบ HTML',
  'ข้อความ', 'ข้อความ Unicode', 'รูปภาพ Bitmap', 'ภาพ Metafile',
  'ไฟล์กราฟิก', 'URL',
  // Options group-box, styles
  'ตัวเลือก', '&รูปแบบ:',
  // style options: apply target styles, use source styles, ignore styles
  'ใช้รูปแบบของเอกสารเป้าหมาย', 'รักษารูปแบบและลักษณะไว้',
  'รักษาลักษณะ, ยกเลิกรูปแบบ',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'หัวข้อย่อยและลำดับเลข', 'หัวข้อย่อย', 'ลำดับเลข', 'รายการสัญลักษณ์หัวข้อย่อย', 'รายการลำดับเลข',
  // Customize, Reset, None
  '&กำหนดเอง...', '&ตั้งค่าใหม่', 'ไม่มี',
  // Numbering: continue, reset to, create new
  'ลำดับเลขต่อไป', 'ตั้งค่าลำดับเลข', 'สร้างรายการใหม่ เริ่มต้นจาก',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'อัลฟาบน', 'โรมันบน', 'เลขฐานสิบ', 'อัลฟาล่าง', 'โรมันล่าง',
  // Bullet type
  'วงกลม', 'แผ่น', 'จัตุรัส',
  // Level, Start from, Continue numbering, Numbering group-box
  '&ระดับ:', '&เริ่มจาก:', '&ต่อไป', 'ลำดับเลข',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'กำหนดรายการเอง', 'ระดับ', '&นับจำนวน:', 'คุณสมบัติของรายการ', '&ชนิดของรายการ:',
  // List types: bullet, image
  'สัญลักษณ์', 'ภาพ', 'แทรกหมายเลข|',
  // Number format, Number, Start level from, Font button
  '&รูปแบบตัวเลข:', 'ตัวเลข', '&เริ่มจากเลข:', '&แบบอักษร...',
  // Image button, bullet character, Bullet button,
  '&ภาพ...', 'อักขระ&แสดงหัวข้อย่อย', '&หัวข้อย่อย...',
  // Position of list text, bullet, number, image, text
  'ตำแหน่งของรายการข้อความ', 'ตำแหน่งหัวข้อย่อย', 'ตำแหน่งตัวเลข', 'ตำแหน่งภาพ', 'ตำแหน่งข้อความ',
  // at, left indent, first line indent, from left indent
  '&ที่:', 'เยื้อง&ซ้าย:', '&การเยื้องบรรทัดแรก:', 'จากการเยื้องซ้าย',
  // [only] one level preview, preview
  '&ตัวอย่างระดับหนึ่ง', 'ตัวอย่าง',
  // Preview text
  'ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'จัดชิดซ้าย', 'จัดชิดขวา', 'กึ่งกลาง',
  // level #, this level (for combo-box)
  'ระดับ %d', 'ระดับนี้',
  // Marker character dialog title
  'แก้ไขอักขระแสดงหัวข้อย่อย',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'เส้นขอบย่อหน้าและพื้นหลัง', 'ขอบ', 'พื้นหลัง',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'ตั้งค่ากล่อง', '&สี:', '&ความกว้าง&ขอบนอก:', 'ความกว้าง&ขอบใน:', 'ระยะขอบ&ข้อความ..',
  // Sample, Border type group-boxes;
  'ตัวอย่าง', 'ชนิดขอบ',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&ไม่มี', '&เดี่ยว', '&คู่', '&สาม', 'หนา&ภายใน', 'หนา&ภายนอก',
  // Fill color group-box; More colors, padding buttons
  'เติมสี', 'สีเพิ่มเติม...', 'ช่องว่างระหว่าง...',
  // preview text
  'ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ',
  // title and group-box in Offsets dialog
  'ขอบอักษร', 'เส้นขอบเหลื่อม',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'ย่อหน้า', 'การจัดแนว', '&ซ้าย', '&ขวา', '&กึ่งกลาง', '&ชิดขอบ',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'ระยะห่าง', '&ก่อน:', '&หลัง:', 'ระยะห่าง&บรรทัด:', '&ขนาด:',
  // Indents group-box; indents: left, right, first line
  'การเยื้อง', '&ซ้าย:', '&ขวา:', '&บรรทัดแรก:',
  // indented, hanging
  '&เยื้อง', '&แขวน', 'ตัวอย่าง',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'หนึ่งเท่า', '1.5 บรรทัด', 'สองเท่า', 'อย่างน้อย', 'ค่าแน่นอน', 'หลายบรรทัด',
  // preview text
  'ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ ข้อความ',
  // tabs: Indents and spacing, Tabs, Text flow
  'การเยื้องและระยะห่าง', 'แท็บ', 'ทิศทางของข้อความ',
  // tab stop position label; buttons: set, delete, delete all
  '&ตำแหน่งของแท็บหยุด:', '&ตั้งค่า', '&ลบ', 'ลบ&ทั้งหมด',
  // tab align group; left, right, center aligns,
  'การจัดตำแหน่ง', 'ชิด&ซ้าย', 'ชิด&ขวา', '&กึ่งกลาง',
  // leader radio-group; no leader
  'เส้นโยง', '(ไม่มี)',
  // tab stops to be deleted, delete none, delete all labels
  'แท็บหยุดที่จะล้าง:', '(ไม่มี)', 'ล้างทั้งหมด',
  // Pagination group-box, keep with next, keep lines together
  'แบ่งหน้า', '&เก็บไว้กับถัดไป', '&เก็บบรรทัดไว้ด้วยกัน',
  // Outline level, Body text, Level #
  '&ระดับเค้าร่าง:', 'เนื้อความ', 'ระดับ %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'ตัวอย่างการพิมพ์', 'พอดีความกว้าง', 'เต็มหน้า', 'หน้า:',
  // Invalid Scale, [page #] of #
  'กรุณาพิมพ์เลขจาก 10 ถึง 500', 'ของ %d',
  // Hints on buttons: first, prior, next, last pages
  'หน้าแรก', 'หน้าก่อน', 'หน้าถัดไป', 'หน้าสุดท้าย',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'ระยะห่างระหว่างเซลล์', 'ระยะห่าง', 'ระหว่างเซลล์', 'ระหว่างเส้นขอบตารางและเซลล์',
  // vertical, horizontal (x2)
  '&แนวตั้ง:', '&แนวนอน:', 'แนว&ตั้ง:', 'แนว&นอน:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'เส้นขอบ', 'การตั้งค่า', '&สี:', 'สี&อ่อน:', 'สี&เงา:',
  // Width; Border type group-box;
  '&กว้าง:', 'ชนิดของเส้นขอบ',
  // Border types: none, sunken, raised, flat
  '&ไม่มี', '&ยุบ', '&ยก', '&ราบ',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'แยก', 'แยกไป',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&ระบุจำนวนของแถวและคอลัมน์', '&เซลล์เริ่มต้น',
  // number of columns, rows, merge before
  'จำนวน&คอลัมน์:', 'จำนวน&แถว:', '&ผสานก่อนแยก',
  // to original cols, rows check-boxes
  'จำนวน&คอลัมน์', 'แยกแถว&เริ่มต้น',
  // Add Rows form -------------------------------------------------------------
  'เพิ่มแถว', 'จำนวน&แถว:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'ตั้งค่าหน้ากระดาษ', 'หน้า', 'หัวกระดาษและท้ายกระดาษ',
  // margins group-box, left, right, top, bottom
  'ระยะขอบ (มิลลิเมตร)', 'ระยะขอบ (นิ้ว)', '&ซ้าย:', '&บน:', '&ขวา:', '&ล่าง:',
  // mirror margins check-box
  '&ระยะสะท้อน',
  // orientation group-box, portrait, landscape
  'การวางแนว', '&แนวตั้ง', '&แนวนอน',
  // paper group-box, paper size, default paper source
  'กระดาษ', '&ขนาดกระดาษ:', '&แหล่งกระดาษ:',
  // header group-box, print on the first page, font button
  'หัวกระดาษ', '&ข้อความ:', '&หัวกระดาษในหน้าแรก', '&แบบอักษร...',
  // header alignments: left, center, right
  '&ซ้าย', '&กลาง', '&ขวา',
  // the same for footer (different hotkeys)
  'ท้ายกระดาษ', '&ข้อความ:', 'ท้ายกระดาษ&ในหน้าแรก', 'แบบ&อักษร...',
  '&ซ้าย', '&กลาง', '&ขวา',
  // page numbers group-box, start from
  'หมายเลขหน้า', '&เริ่มจาก:',
  // hint about codes
  'ชุดอักขระพิเศษ:'#13'&&p - หมายเลขหน้า; &&P - จำนวนหน้า; &&d - วันที่ปัจจุบัน; &&t - เวลาปัจจุบัน',
  // Code Page form ------------------------------------------------------------
  // title, label
  'หน้ารหัสแฟ้มข้อความ', '&เลือกการเข้ารหัสแฟ้ม:',
  // thai, japanese, chinese (simpl), korean
  'ไทย', 'ญี่ปุ่น', 'จีน (ประยุกต์)', 'เกาหลี',
  // chinese (trad), central european, cyrillic, west european
  'จีน (ดั้งเดิม)', 'ยุโรปกลางและตะวันออก', 'ซีริลลิ', 'ยุโรปตะวันตก',
  // greek, turkish, hebrew, arabic
  'กรีก', 'ตุรกี', 'ฮีบรู', 'อาหรับ',
  // baltic, vietnamese, utf-8, utf-16
  'บอลติก', 'เวียดนาม', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'ลักษณะการแสดงผล', '&เลือกรูปแบบ:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'แปลงเป็นข้อความ', '&เลือกตัวคั่น:',
  // line break, tab, ';', ','
  'ตัวแบ่งบรรทัด', 'แท็บ', 'อัฒภาค', 'จุลภาค',
  // Table sort form -----------------------------------------------------------
  // error message
  'ตารางประกอบด้วยแถวที่ถูกผสานไม่สามารถเรียงลำดับได้',
  // title, main options groupbox
  'เรียงลำดับตาราง', 'เรียงลำดับ',
  // sort by column, case sensitive
  '&เรียงลำดับตามคอลัมน์:', '&แยกแยะตัวพิมพ์ใหญ่-เล็ก',
  // heading row, range or rows
  'แยกส่วนหัว&ของแถว', 'แถวในการจัดเรียง: จาก %d ถึง %d',
  // order, ascending, descending
  'ลำดับ', '&น้อยไปมาก', '&มากไปน้อย',
  // data type, text, number
  'ชนิด', '&ข้อความ', '&ตัวเลข',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  {*} 'Insert Number', 'Properties', '&Counter name:', '&Numbering type:',
  // numbering groupbox, continue, start from
  {*} 'Numbering', 'C&ontinue', '&Start from:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  {*} 'Caption', '&Label:', '&Exclude label from caption',
  // position radiogroup
  {*} 'Position', '&Above selected object', '&Below selected object',
  // caption text
  {*} 'Caption &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  {*} 'Numbering', 'Figure', 'Table',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  {*} 'Visible Border Sides', 'Border',
  // Left, Top, Right, Bottom checkboxes
  {*} '&Left side', '&Top side', '&Right side', '&Bottom side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'ปรับขนาดคอลัมน์ตาราง', 'ปรับขนาดแถวตาราง',
  // Hints on indents: first line, left (together with first), left (without first), right
  'เยื้องแรก', 'เยื้องซ้าย', 'เยื้องลอย', 'เยื่องขวา',
  // Hints on lists: up one level (left), down one level (right)
  'เพิ่มหึ่งระดับ', 'ลดหนึ่งระดับ',
  // Hints for margins: bottom, left, right and top
  'ขอบล่าง', 'ขอบซ้าย', 'ขอบขวา', 'ขอบบน',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'แท็บชิดซ้าย', 'แท็บชิดขวา', 'แท็บชิดกึ่งกลาง', 'แท็บชิดทศนิยม',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'ปกติ', 'เยื้องปกติ', 'ไม่มีช่องว่าง', 'หัวเรื่อง %d', 'รายการย่อหน้า',
  // Hyperlink, Title, Subtitle
  'ลิงค์', 'ชื่อเรื่อง', 'ชื่อเรื่องรอง',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'การเน้น', 'การเน้นบาง', 'การเน้นหนา', 'มาก',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'การอ้างอิง', 'การอ้างอิงหนา', 'การอ้างโยงบาง', 'การอ้างโยงหนา',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'กล่องข้อความ', 'HTML ผันแปร', 'รหัส HTML', 'HTML ย่อ',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'จำกัดความ HTML', 'คีย์บอร์ด HTML', 'ตัวอย่าง HTML', 'ตัวพิมพ์ HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML ดั้งเดิม', 'HTML อ้างอิง', 'ส่วนหัว', 'ส่วนท้าย', 'เลขหน้า',
  // Caption
  {*} 'Caption',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'อ้างอิงท้ายเรื่อง', 'อ้างอิงเชิงอรรถ', 'ข้อความอ้างอิงท้ายเรื่อง', 'ข้อความเชิงอรรถ',
  // Sidenote Reference, Sidenote Text
  {*} 'Sidenote Reference', 'Sidenote Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'สี', 'สีพื้นหลัง', 'โปร่งใส', 'ค่าเริ่มต้น', 'สีเส้นใต้',
  // default background color, default text color, [color] same as text
  'ค่าเริ่มต้นสีพื้นหลัง', 'ค่าเริ่มต้นสีแบบอักษร', 'เหมือนกับข้อความ',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'เดียว', 'หนา', 'คู่', 'จุด', 'จุดหนา', 'จุดประ',
  // underline types: thick dashed, long dashed, thick long dashed,
  'จุดประหนา', 'จุดประยาว', 'จุดประหนายาว',
  // underline types: dash dotted, thick dash dotted,
  'จุดประจุด', 'จุดประจุดหนา',
  // underline types: dash dot dotted, thick dash dot dotted
  'จุดประจุดจุด', 'จุดประจุดจุดหนา',
  // sub/superscript: not, subsript, superscript
  'ไม่ใช้ตัวห้อย/ตัวยก', 'ตัวห้อย', 'ตัวยก',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'โหมดสลับซ้ายขวา:', 'ต่อเนื่องกัน', 'ซ้ายไปขวา', 'ขวาไปซ้าย',
  // bold, not bold
  'ตัวหนา', 'ไม่หนา',
  // italic, not italic
  'ตัวเอียง', 'ไม่เอียง',
  // underlined, not underlined, default underline
  'ขีดเส้นใต้', 'ไม่ขีดเส้นใต้', 'ค่าเริ่มต้นกขีดเส้นใต้',
  // struck out, not struck out
  'ขีดทับ', 'ไม่ขีดทับ',
  // overlined, not overlined
  'ขีดเส้นบน', 'ไม่ขีดเส้นบน',
  // all capitals: yes, all capitals: no
  'ตัวใหญ่ทั้งหมด', 'ไม่ใช้ตัวใหญ่ทั้งหมด',
  // vertical shift: none, by x% up, by x% down
  'ไม่เปลี่ยนแปลงตามแนวตั้ง', 'เพิ่มขึ้นอีก %d%%', 'ลดลงอีก %d%%',
  // characters width [: x%]
  'ระยะห่างอักขระ',
  // character spacing: none, expanded by x, condensed by x
  'ระยะห่างอักขระปกติ', 'เพิ่มระยะห่างอีก %s', 'ลดระยะห่างอีก %s',
  // charset
  'สคริปต์',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'ค่าเริ่มต้นแบบอักษร', 'ค่าเริ่มต้นย่อหน้า', 'ค่าล่าสุด',
  // [hyperlink] highlight, default hyperlink attributes
  'เน้น', 'ค่าเริ่มต้น',
  // paragraph aligmnment: left, right, center, justify
  'ข้อความชิดซ้าย', 'ข้อความชิดขวา', 'กึ่งกลาง', 'เต็มแนว',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'ความสูงบรรทัด: %d%%', 'ระยะหะหว่างบรรทัด: %s', 'ความสูงบรรทัด: อย่างน้อย %s',
  'ความสูงบรรทัด: พอดีที่ %s',
  // no wrap, wrap
  'ยกเลิกการตัดคำขึ้นบรรทัดใหม่', 'ตัดคำขึ้นบรรทัดใหม่',
  // keep lines together: yes, no
  'รักษาบรรทัดไว้ด้วยกัน', 'ไม่ต้องรักษาบรรทัดไว้ด้วยกัน',
  // keep with next: yes, no
  'รักษาย่อหน้าถัดไป', 'ไม่ต้องรักษาย่อหน้าถัดไป',
  // read only: yes, no
  'อ่านเท่านั้น', 'สามารถแก้ไขได้',
  // body text, heading level x
  'ระดับโครงร่าง: ส่วนข้อความ', 'ระดับโครงร่าง: %d',
  // indents: first line, left, right
  'แนวเยื้องแรก', 'เยื้องซ้าย', 'เยื้องขวา',
  // space before, after
  'ช่องว่างก่อน', 'ช่องว่างหลัง',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'แท็บ', 'ไม่มี', 'แนว', 'ซ้าย', 'ขวา', 'กลาง',
  // tab leader (filling character)
  'แท็บแรก',
  // no, yes
  'ไม่', 'ใช่',
  // [padding/spacing/side:] left, top, right, bottom
  'ซ้าย', 'บน', 'ขวา', 'ล่าง',
  // border: none, single, double, triple, thick inside, thick outside
  'ไม่มี', 'เดี่ยว', 'คู่', 'สาม', 'หนาด้านใน', 'หนาด้านนอก',
  // background, border, padding, spacing [between text and border]
  'พื้นหลัง', 'ขอบ', 'ขยาย', 'ช่องไฟ',
  // border: style, width, internal width, visible sides
  'รูปแบบ', 'ความกว้าง', 'ความกว้งภายใน', 'ขนาดที่มองเห็น',
  // style inspector -----------------------------------------------------------
  // title
  'ตรวจสอบรูปแบบ',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'รูปแบบ', '(ไม่มี)', 'ย่อหน้า', 'แบบอักษร', 'คุณสมบัติ',
  // border and background, hyperlink, standard [style]
  'เส้นขอบและพื้นหลัง', 'ลิงค์', '(มาตรฐาน)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'รูปแบบ', 'รูปแบบ',
  // name, applicable to,
  '&ชื่อ:', 'ใช้ได้&กับ:',
  // based on, based on (no &),
  '&บนพื้นฐานของ:', 'บนพื้นฐานของ:',
  //  next style, next style (no &)
  'รูปแบบสำหรับ&ย่อหน้าต่อไป:', 'รูปแบบสำหรับย่อหน้าต่อไป:',
  // quick access check-box, description and preview
  '&เข้าถึงอย่างรวดเร็ว', 'รายละเอียดและ&ตัวอย่าง:',
  // [applicable to:] paragraph and text, paragraph, text
  'ย่อหน้าและข้อความ', 'ย่อหน้า', 'ข้อความ',
  // text and paragraph styles, default font
  'รูปแบบข้อความและรุปแบบย่อหน้า', 'ค่าเริ่มต้นแบบอักษร',
  // links: edit, reset, buttons: add, delete
  'แก้ไข', 'ตั้งค่าเริ่มต้น', '&เพิ่ม', '&ลบ',
  // add standard style, add custom style, default style name
  'เพิ่ม&รูปแบบมาตรฐาน...', 'เพิ่ม&รูปแบบกำหนดเอง', 'รูปแบบ%d',
  // choose style
  'เลือก&รูปแบบ:',
  // import, export,
  '&นำเข้า...', '&ส่งออก...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'รูปแบบ RichView (*.rvst)|*.rvst', 'การโหลดไล์ผิดพลาด', 'ไฟล์นี้ไม่มีรูปแบบ',
  // Title, group-box, import styles
  'นำเข้ารูปแบบจากไฟล์', 'นำเข้า', '&รูปแบบนำเข้า:',
  // existing styles radio-group: Title, override, auto-rename
  'รูปแบบที่มีอยู่', '&ทับ', '&เพิ่มการเปลี่ยนชื่อ',
  // Select, Unselect, Invert,
  '&เลือก', '&ไม่เลือก', '&ย้อนกลับ',
  // select/unselect: all styles, new styles, existing styles
  '&รูปแบบทั้งหมด', '&รูปแบบใหม่', '&รูปแบบที่มีอยู่',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(ล้างรูปแบบ)', '(รูปแบบทั้งหมด)',
  // dialog title, prompt
  'รูปแบบ', '&เลือกใช้รูปแบบ:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&คำเหมือน', '&ยกเลิกทั้งหมด', '&เพิ่มในพจนานุกรม',
  // Progress messages ---------------------------------------------------------
  'กำลังดาวน์โหลด %s', 'กำลังเตรียมการพิมพ์...', 'กำลังพิมพ์หน้า %d',
  'แปลงจาก RTF...',  'แปลงไปเป็น RTF...',
  'กำลังอ่าน %s...', 'กำลังเขียน %s...', 'ข้อความ',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  {*} 'No Note', 'Footnote', 'Endnote', 'Sidenote', 'Text Box'
  );


initialization
  RVA_RegisterLanguage(
    'Thai', // english language name, do not translate
    {*} 'Thai', // native language name
    THAI_CHARSET, @Messages);

end.
