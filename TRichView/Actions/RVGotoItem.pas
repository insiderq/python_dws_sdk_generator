
{*******************************************************}
{                                                       }
{       RichView                                        }
{       Functions for moving caret to the previous or   }
{       next item meeting the specified criteria.       }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit RVGoToItem;

interface
uses Classes,
  CRVData, CRVFData, RVItem, RVEdit, RVTable, RVUni, RVTypes, RVRVData,
  RichView;

// The functions below return True if moving is successful

// Moving the caret to the next/previous hyperlink (optionally selecting it)
function GoToNextHyperlink(rve: TCustomRichViewEdit; Select: Boolean=False): Boolean;
function GoToPrevHyperlink(rve: TCustomRichViewEdit; Select: Boolean=False): Boolean;
// Moving the caret to the next/previous item of one of the specified types
// for example GoToNextItem(rve, [rvsPicture, rvsHotPicture])
function GoToNextItem(rve: TCustomRichViewEdit; ItemTypes: array of Integer;
  IncludeCurrentItem: Boolean; Select: Boolean=False): Boolean;
function GoToPrevItem(rve: TCustomRichViewEdit; ItemTypes: array of Integer;
  IncludeCurrentItem: Boolean; Select: Boolean=False): Boolean;

// The type of function for checking item. The function must return True
// if RVData.GetItem(ItemNo) is ok (i.e. caret should be moved to it)
type
  TCheckItemFunction = function (RVData: TCustomRVData; ItemNo: Integer;
    UserData: Pointer): Boolean;
// Moving the caret to the next/previous item, using a user-defined procedure
// for checking item. UserData parameter is passed to Func.
function GoToNextItemEx(rve: TCustomRichViewEdit; UserData: Pointer;
  Func: TCheckItemFunction;  IncludeCurrentItem: Boolean; Select: Boolean=False): Boolean;
function GoToPrevItemEx(rve: TCustomRichViewEdit; UserData: Pointer;
  Func: TCheckItemFunction; IncludeCurrentItem: Boolean; Select: Boolean=False): Boolean;

implementation

function FindPreviousItem(RVData: TCustomRVData;
  ItemNo: Integer; UserData: Pointer; Func: TCheckItemFunction;
  SkipStartItem: Boolean;
  var ResRVData: TCustomRVData; var ResItemNo: Integer): Boolean;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; LastItemNo: Integer): Boolean; forward;
   {...................................................}
   function FindItemInItem(Item: TCustomRVItemInfo; StoreSub: TRVStoreSubRVData): Boolean;
   var RVData: TCustomRVData;
   begin
     Result := False;
     if StoreSub=nil then
       RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdLast))
     else
       RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdPrev));
     if RVData<>nil then begin
       repeat
         Result := FindItemInRVData(RVData, RVData.Items.Count-1);
         if Result then
           break;
         RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdPrev));
       until RVData=nil;
     end;
     StoreSub.Free;
   end;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; LastItemNo: Integer): Boolean;
   var i: Integer;
   begin
     Result := False;
     for i := LastItemNo downto 0 do begin
       if not SkipStartItem then begin
         Result := Func(RVData, i, UserData);
         SkipStartItem := False;
       end;
       if Result then begin
         ResRVData := RVData;
         ResItemNo := i;
         exit;
       end;
       Result := FindItemInItem(RVData.GetItem(i), nil);
       if Result then
         exit;
     end;
     SkipStartItem := False;
   end;
   {...................................................}
var StoreSub: TRVStoreSubRVData;
begin
  Result := False;
  StoreSub := nil;
  while RVData<>nil do begin
    Result := FindItemInRVData(RVData, ItemNo);
    if Result then
      break;
    RVData.GetParentInfo(ItemNo, StoreSub);
    if ItemNo<0 then begin
      StoreSub.Free;
      break;
    end;
    RVData := RVData.GetAbsoluteParentData;
    Result := FindItemInItem(RVData.GetItem(ItemNo), StoreSub);
    if Result then
      break;
    dec(ItemNo);
  end;
end;
{------------------------------------------------------------------------------}
function FindNextItem(RVData: TCustomRVData;
  ItemNo: Integer; UserData: Pointer; Func: TCheckItemFunction;
  var ResRVData: TCustomRVData; var ResItemNo: Integer): Boolean;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; FirstItemNo: Integer): Boolean; forward;
   {...................................................}
   function FindItemInItem(Item: TCustomRVItemInfo; StoreSub: TRVStoreSubRVData): Boolean;
   var RVData: TCustomRVData;
   begin
     Result := False;
     if StoreSub=nil then
       RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdFirst))
     else
       RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdNext));
     if RVData<>nil then begin
       repeat
         Result := FindItemInRVData(RVData, 0);
         if Result then
           break;
         RVData := TCustomRVData(item.GetSubRVData(StoreSub, rvdNext));
       until RVData=nil;
     end;
     StoreSub.Free;
   end;
   {...................................................}
   function FindItemInRVData(RVData: TCustomRVData; FirstItemNo: Integer): Boolean;
   var i: Integer;
   begin
     Result := False;
     for i := FirstItemNo to RVData.ItemCount-1 do begin
       Result := FindItemInItem(RVData.GetItem(i), nil);
       if Result then
         exit;     
       Result := Func(RVData, i, UserData);
       if Result then begin
         ResRVData := RVData;
         ResItemNo := i;
         exit;
       end;
     end;
   end;
   {...................................................}
var StoreSub: TRVStoreSubRVData;
begin
  Result := False;
  StoreSub := nil;
  while RVData<>nil do begin
    Result := FindItemInRVData(RVData, ItemNo);
    if Result then
      break;
    RVData.GetParentInfo(ItemNo, StoreSub);
    if ItemNo<0 then begin
      StoreSub.Free;
      break;
    end;
    RVData := RVData.GetAbsoluteParentData;
    Result := FindItemInItem(RVData.GetItem(ItemNo), StoreSub);
    if Result then
      break;
    inc(ItemNo);
  end;
end;
{------------------------------------------------------------------------------}
function GoToNextItemEx(rve: TCustomRichViewEdit; UserData: Pointer;
  Func: TCheckItemFunction; IncludeCurrentItem: Boolean; Select: Boolean): Boolean;
var RVData: TCustomRVData;
    ItemNo, Offs1, Offs2: Integer;
begin
  rve := rve.TopLevelEditor;
  ItemNo := rve.TopLevelEditor.CurItemNo;
  if not IncludeCurrentItem and
    ((rve.GetItemStyle(ItemNo)<>rvsTable) or (rve.OffsetInCurItem=1)) then
    inc(ItemNo);
  Result := FindNextItem(rve.RVData, ItemNo, UserData, Func, RVData, ItemNo);
  if Result then begin
    RVData := RVData.Edit;
    Offs2 := RVData.GetOffsAfterItem(ItemNo);
    if Select then
      Offs1 := RVData.GetOffsBeforeItem(ItemNo)
    else
      Offs1 := Offs2;
    TCustomRVFormattedData(RVData).SetSelectionBounds(ItemNo, Offs1, ItemNo, Offs2);
    TCustomRVFormattedData(RVData).Invalidate;
  end;
end;
{------------------------------------------------------------------------------}
function GoToPrevItemEx(rve: TCustomRichViewEdit; UserData: Pointer;
  Func: TCheckItemFunction;  IncludeCurrentItem: Boolean; Select: Boolean): Boolean;
var RVData: TCustomRVData;
    ItemNo, Offs1, Offs2: Integer;
    SkipStartItem: Boolean;
begin
  rve := rve.TopLevelEditor;
  ItemNo := rve.TopLevelEditor.CurItemNo;
  SkipStartItem := (rve.GetItemStyle(ItemNo)=rvsTable) and (rve.OffsetInCurItem=1);
  if not SkipStartItem and not IncludeCurrentItem then
    dec(ItemNo);
  Result := FindPreviousItem(rve.RVData, ItemNo, UserData, Func, SkipStartItem,
    RVData, ItemNo);
  if Result then begin
    RVData := RVData.Edit;
    Offs2 := RVData.GetOffsAfterItem(ItemNo);
    if Select then
      Offs1 := RVData.GetOffsBeforeItem(ItemNo)
    else
      Offs1 := Offs2;
    TCustomRVFormattedData(RVData).SetSelectionBounds(ItemNo, Offs1, ItemNo, Offs2);
    TCustomRVFormattedData(RVData).Invalidate;
  end;
end;
{------------------------------------------------------------------------------}
function IsHyperlink(RVData: TCustomRVData; ItemNo: Integer; UserData: Pointer):
  Boolean;
begin
  Result := RVData.GetItem(ItemNo).GetBoolValueEx(rvbpJump, RVData.GetRVStyle);
end;
{------------------------------------------------------------------------------}
function GoToNextHyperlink(rve: TCustomRichViewEdit; Select: Boolean): Boolean;
begin
  Result := GoToNextItemEx(rve, nil, IsHyperlink, Select);
end;
{------------------------------------------------------------------------------}
function GoToPrevHyperlink(rve: TCustomRichViewEdit; Select: Boolean): Boolean;
begin
  Result := GoToPrevItemEx(rve, nil, IsHyperlink, Select);
end;
{------------------------------------------------------------------------------}
{$R-}
type
  TIntArray = array[0..0] of Integer;
  PIntArray = ^TIntArray;
  TIntArrayInfo = record
    PArr: PIntArray;
    Count: Integer;
  end;
  PIntArrayInfo = ^TIntArrayInfo;
{------------------------------------------------------------------------------}
function IsItemOfType(RVData: TCustomRVData; ItemNo: Integer; UserData: Pointer):
  Boolean;
var PAI: PIntArrayInfo;
    i, StyleNo: Integer;
begin
  PAI := PIntArrayInfo(UserData);
  Result := True;
  StyleNo := RVData.GetItemStyle(ItemNo);
  for i := 0 to PAI.Count-1 do
    if StyleNo=PAI.PArr[i] then
      exit;
  Result := False;
end;
{------------------------------------------------------------------------------}
function GoToNextItem(rve: TCustomRichViewEdit; ItemTypes: array of Integer;
  IncludeCurrentItem: Boolean; Select: Boolean=False): Boolean;
var AI: TIntArrayInfo;
begin
  AI.PArr := PIntArray(@ItemTypes[0]);
  AI.Count := High(ItemTypes)+1;
  Result := GoToNextItemEx(rve, @AI, IsItemOfType, IncludeCurrentItem, Select);
end;
{------------------------------------------------------------------------------}
function GoToPrevItem(rve: TCustomRichViewEdit; ItemTypes: array of Integer;
  IncludeCurrentItem: Boolean; Select: Boolean=False): Boolean;
var AI: TIntArrayInfo;
begin

  AI.PArr := PIntArray(@ItemTypes[0]);
  AI.Count := High(ItemTypes)+1;
  Result := GoToPrevItemEx(rve, @AI, IsItemOfType, IncludeCurrentItem, Select);
end;

end.
