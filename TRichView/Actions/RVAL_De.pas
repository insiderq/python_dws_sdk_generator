
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       German (Deutsch) translation                    }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Eike Hoffmann 2003-02-04               }
{ Updated: 2009-05-19 by Harry Stahl                    }
{          2010-10-14 by Michael Damm                   }
{          2012-08-28 by Ruediger Kabbasch              }
{          2014-01-30 by Ruediger Kabbasch              } 
{*******************************************************}

unit RVAL_De;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'Inch', 'cm', 'mm', 'Pica', 'Pixel', 'Punkt',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Datei', '&Bearbeiten', 'Forma&t', '&Zeichen', '&Absatz', '&Einf�gen', '&Tabelle', '&Fenster', '&Hilfe',
  // exit
  '&Beenden',
  // top-level menus: View, Tools,
  '&Ansicht', '&Werkzeuge',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Schriftgr��e', 'Schriftstil', 'Markie&ren', 'Zelli&nhalte ausrichten', 'Z&ellrahmen',
  // menus: Table cell rotation
  'Zellend&rehung',
  // menus: Text flow, Footnotes/endnotes
  '&Textfluss', '&Fu�noten',
  // ribbon tabs: tab1, tab2, view, table
  '&Start', '&Erweitert', '&Ansicht', '&Tabelle',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Zwischenablage', 'Schrift', 'Absatz', 'Liste', 'Bearbeiten',
  // ribbon groups: insert, background, page setup,
  'Einf�gen', 'Hintergrund', 'Seiteneinstellungen',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Links', 'Fu�noten', 'Dokumentansichten', 'Anzeigen/Verbergen', 'Zoom', 'Optionen',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Einf�gen', 'L�schen', 'Aktionen', 'Rahmen',
  // ribbon groups: styles 
  'Formatvorlagen',
  // ribbon screen tip footer,
  'F�r weitere Hilfe F1 dr�cken',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Zuletzt verwendet', 'Kopie des Dokuments sichern', 'Vorschau und Ausdruck des Dokuments',
  // ribbon label: units combo
  'Einheiten:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Neu', 'Neu|Erstellt ein leeres neues Dokument',
  // TrvActionOpen
  '&�ffnen...', '�ffnen|�ffnet ein Dokument von der Festplatte',
  // TrvActionSave
  '&Speichern', 'Speichern|Speichert das Dokument auf Festplatte',
  // TrvActionSaveAs
  'Speichern &Unter...', 'Speichern Unter...|Speichert das Dokument mit einem neuen Namen auf Festplatte',
  // TrvActionExport
  '&Exportieren...', 'Exportieren|Exportiert das Dokument in einem anderen Format',
  // TrvActionPrintPreview
  'Druck&vorschau', 'Druckvorschau|Zeigt wie das Dokument gedruckt wird',
  // TrvActionPrint
  '&Drucken...', 'Drucken|Setzt die Druckeinstellungen und druckt das Dokument',
  // TrvActionQuickPrint
  '&Drucken', '&Sofortdruck', 'Drucken|Druckt das Dokument',
   // TrvActionPageSetup
  'S&eite einrichten..', 'Seite einrichten|Seitenr�nder, Papierformat, Seitenorientierung, Kopfzeilen und Fusszeilen einrichten',
  // TrvActionCut
  'Ausschnei&den', 'Ausschneiden|Schneidet den markierten Bereich aus und speichert diesen in der Zwischenablage',
  // TrvActionCopy
  '&Kopieren', 'Kopieren|Kopiert den markierten Bereich und speichert diesen in der Zwischenablage',
  // TrvActionPaste
  'E&inf�gen', 'Einf�gen|F�gt den Inhalt der Zwischenablage ein',
  // TrvActionPasteAsText
  'Einf�gen als &Text', 'Einf�gen als Text|F�gt den Inhalt der Zwischenablage als Text ein',
  // TrvActionPasteSpecial
  'Einf�gen al&s...', 'Einf�gen als|F�gt den Inhalt der Zwischenablage im angegebenen Format ein',
  // TrvActionSelectAll
  '&Alles markieren', 'Alles markieren|W�hlt das gesamte Dokument aus',
  // TrvActionUndo
  '&R�ckg�ngig', 'R�ckg�ngig|Macht die letzte Aktion r�ckg�ngig',
  // TrvActionRedo
  '&Wiederherstellen', 'Wiederherstellen|Wiederherstellen der letzten r�ckg�ngig gemachten Aktion',
  // TrvActionFind
  '&Suchen...', 'Suchen|Sucht nach dem angegebenen Text im Dokument',
  // TrvActionFindNext
  '&Weitersuchen', 'Weitersuchen|Fortsetzen der letzten Suchanfrage',
  // TrvActionReplace
  '&Ersetzen...', 'Ersetzen|Suchen und ersetzen des angegebenen Textes im Dokument',
  // TrvActionInsertFile
  '&Datei...', 'Datei einf�gen|F�gt den Inhalt einer Datei in das Dokument ein',
  // TrvActionInsertPicture
  '&Grafik...', 'Grafik einf�gen|F�gt eine Grafik von der Festplatte ein',
  // TRVActionInsertHLine
  '&Horizontale Linie', 'Horizontale Linie einf�gen|F�gt eine horizontale Linie in das Dokument ein',
  // TRVActionInsertHyperlink
  'Hyper&link...', 'Hyperlink einf�gen|F�gt einen Hyperlink in das Dokument ein',
  // TRVActionRemoveHyperlinks
  'Hyperlinks ent&fernen', 'Hyperlinks entfernen|Entfernt alle Hyperlinks aus dem markierten Text',
  // TrvActionInsertSymbol
  '&Symbol...', 'Symbol einf�gen|F�gt ein Symbol in das Dokument ein',
  // TrvActionInsertNumber
  '&Zahl...', 'Zahl einf�gen|Eine Zahl einf�gen',
  // TrvActionInsertCaption
  'Bes&chriftung einf�gen...', 'Beschriftung einf�gen|Eine Beschriftung f�r das ausgew�hlte Objekt einf�gen',
  // TrvActionInsertTextBox
  '&Textfeld', 'Textfeld einf�gen|F�gt ein Textfeld ein',
  // TrvActionInsertSidenote
  '&Randnotiz', 'Randnotiz einf�gen|F�gt eine Randnotiz in einem Textfeld ein',
  // TrvActionInsertPageNumber
  '&Seitenzahl', 'Seitenzahl einf�gen|F�gt eine Seitenzahl ein',
  // TrvActionParaList
  '&Nummerierung und Aufz�hlungszeichen...', 'Nummerierung und Aufz�hlungszeichen|Hinzuf�gen oder bearbeiten der Nummerierung bzw. des Aufz�hlungszeichens f�r die markierten Abs�tze',
  // TrvActionParaBullets
  '&Aufz�hlungszeichen', 'Aufz�hlungszeichen|Hinzuf�gen oder l�schen eines Aufz�hlungszeichens f�r diesen Absatz',
  // TrvActionParaNumbering
  '&Nummerierung', 'Nummerierung|Hinzuf�gen oder l�schen einer Nummerierung f�r diesen Absatz',
  // TrvActionColor
  'Hintergrundf&arbe...', 'Hintergrundfarbe|�ndern der Hintergrundfarbe des Dokuments',
  // TrvActionFillColor
  '&F�llfarbe...', 'F�llfarbe|�ndern der Hintergrundfarbe f�r den Text, den Absatz, die Tabellenzelle, die Tabelle oder das Dokument',
  // TrvActionInsertPageBreak
  '&Seitenumbruch einf�gen', 'Seitenumbruch einf�gen|F�gt einen Seitenumbruch ein',
  // TrvActionRemovePageBreak
  'Seitenumbruch &l�schen', 'Seitenumbruch l�schen|L�scht den Seitenumbruch',
  // TrvActionClearLeft
  'Textfluss auf &linker Seite', 'Textfluss auf linker Seite|Plaziert den Absatz unter jedes links ausgerichtete Bild',
  // TrvActionClearRight
  'Textfluss auf &rechter Seite', 'Textfluss auf rechter Seite|Plaziert den Absatz unter jedes rechts ausgerichtete Bild',
  // TrvActionClearBoth
  'Textfluss auf &beiden Seiten', 'Textfluss auf beiden Seiten|Plaziert den Absatz unter jedes links oder rechts ausgerichtete Bild',
  // TrvActionClearNone
  '&Normaler Textfluss', 'Normaler Textfluss|Erlaubt den Textfluss sowohl um links als auch rechts ausgerichtete Bilder',
  // TrvActionVAlign
  '&Objekt Position...', 'Objekt Position|�ndert die Position des selektierten Objekts',
  // TrvActionItemProperties
  'Objekt &Einstellungen...', 'Objekt Einstellungen|Einstellungen f�r das aktive Objekt festlegen',
  // TrvActionBackground
  '&Hintergrund...', 'Hintergrund|�ndern von Hintergrundfarbe und Hintergrundbild',
  // TrvActionParagraph
  '&Absatz...', 'Absatz|�ndern der Absatzattribute',
  // TrvActionIndentInc
  '&Einzug vergr��ern', 'Einzug vergr��ern|Linken Einzug des markierten Absatzes vergr��ern',
  // TrvActionIndentDec
  'Einzug &verkleinern', 'Einzug verkleinern|Linken Einzug des markierten Absatzes verkleinern',
  // TrvActionWordWrap
  '&Zeilenumbruch', 'Zeilenumbruch|Zeilenumbruch f�r den markierten Absatz ein- oder ausschalten',
  // TrvActionAlignLeft
  '&Linksb�ndig', 'Linksb�ndig|Richtet den markierten Text linksb�ndig aus',
  // TrvActionAlignRight
  '&Rechtsb�ndig', 'Rechtsb�ndig|Richtet den markierten Text rechtsb�ndig aus',
  // TrvActionAlignCenter
  '&Zentriert', 'Zentriert|Richtet den markierten Text zentriert aus',
  // TrvActionAlignJustify
  '&Blocksatz', 'Blocksatz|Richtet den markierten Text im Blocksatz aus',
  // TrvActionParaColor
  'Absatz &Hintergrundfarbe...', 'Absatz Hintergrundfarbe|�ndern der Hintergrundfarbe des Absatzes',
  // TrvActionLineSpacing100
  '&Einfacher Zeilenabstand', 'Einfacher Zeilenabstand|Den Zeilenabstand auf eine Zeile setzen ',
  // TrvActionLineSpacing150
  '1,5-facher Zeile&nabstand', '1,5-facher Zeilenabstand|Den Zeilenabstand auf 1,5 Zeilen setzen',
  // TrvActionLineSpacing200
  '&Doppelter Zeilenabstand', 'Doppelter Zeilenabstand|Den Zeilenabstand auf 2 Zeilen setzen',
  // TrvActionParaBorder
  'A&bsatz Rahmen und Hintergrundfarbe...', 'Absatz Rahmen und Hintergrundfarbe|�ndern des Rahmens und der Hintergrundfarbe f�r diesen Absatz',
  // TrvActionInsertTable
  'Tabelle &einf�gen...', '&Tabelle', 'Tabelle einf�gen|F�gt eine neue Tabelle ein',
  // TrvActionTableInsertRowsAbove
  'Zeile &oberhalb einf�gen', 'Zeile oberhalb einf�gen|F�gt eine neue Zeile oberhalb der markierten Zellen ein',
  // TrvActionTableInsertRowsBelow
  'Zeile &unterhalb einf�gen', 'Zeile unterhalb einf�gen|F�gt eine neue Zeile unterhalb der markierten Zellen ein',
  // TrvActionTableInsertColLeft
  'Spalte &links einf�gen', 'Spalte links einf�gen|F�gt eine neue Spalte links von den markierten Zellen ein',
  // TrvActionTableInsertColRight
  'Spalte &rechts einf�gen', 'Spalte rechts einf�gen|F�gt eine neue Spalte rechts von den markierten Zellen ein',
  // TrvActionTableDeleteRows
  '&Zeilen l�schen', 'Zeilen l�schen|L�scht die Zeilen mit den markierten Zellen',
  // TrvActionTableDeleteCols
  '&Spalten l�schen', 'Spalten l�schen|L�scht die Spalten mit den markierten Zellen',
  // TrvActionTableDeleteTable
  '&Tabelle l�schen', 'Tabelle l�schen|L�scht die Tabelle',
  // TrvActionTableMergeCells
  'Zellen &verbinden', 'Zellen verbinden|Verbindet die markierten Zellen',
  // TrvActionTableSplitCells
  'Zellen &teilen...', 'Zellen teilen|Teilt die markierten Zellen',
  // TrvActionTableSelectTable
  'Tabelle &markieren', 'Tabelle markieren|Markiert die gesamte Tabelle',
  // TrvActionTableSelectRows
  'Zeilen m&arkieren', 'Zeilen markieren|Markiert die Zeilen',
  // TrvActionTableSelectCols
  'S&palten markieren', 'Spalten markieren|Markiert die Spalten',
  // TrvActionTableSelectCell
  'Z&elle markieren', 'Zelle markieren|Markiert die Zelle',
  // TrvActionTableCellVAlignTop
  'Zellinhalt &oben ausrichten', 'Zellinhalt oben ausrichten|Richtet den Inhalt der Zelle am oberen Rand aus',
  // TrvActionTableCellVAlignMiddle
  'Zellinhalt &mittig ausrichten', 'Zellinhalt mittig ausrichten|Richtet den Inhalt der Zelle mittig aus',
  // TrvActionTableCellVAlignBottom
  'Zellinhalt &unten ausrichten', 'Zellinhalt unten ausrichten|Richtet den Inhalt der Zelle am unteren Rand aus',
  // TrvActionTableCellVAlignDefault
  '&Standard f�r vertikale Zellausrichtung', 'Standard f�r vertikale Zellausrichtung|Festlegend er Standardeinstellung f�r die vertikale Zellausrichtung',
  // TrvActionTableCellRotationNone
  '&Keine Zellendrehung', 'Keine Zellendrehung|Inhalt der Zelle um 0� drehen',
  // TrvActionTableCellRotation90
  'Zelle um &90� drehen', 'Zelle um 90� drehen|Inhalt der Zelle um 90� drehen',
  // TrvActionTableCellRotation180
  'Zelle um &180� drehen', 'Zelle um 180� drehen|Inhalt der Zelle um 180� drehen',
  // TrvActionTableCellRotation270
  'Zelle um &270� drehen', 'Zelle um 270� drehen|Inhalt der Zelle um 270� drehen',
  // TrvActionTableProperties
  'Tabellen&eigenschaften...', 'Tabelleneigenschaften|�ndern der Eigenschaften f�r die markierte Tabelle',
  // TrvActionTableGrid
  '&Gitternetzlinien anzeigen', 'Gitternetzlinien anzeigen|Ein- oder ausschalten der Gitternetzlinien f�r Tabellen',
  // TRVActionTableSplit
  'Tabelle tei&len', 'Tabelle teilen|Teilt die Tabelle beginnend mit der ausgew�hlten Zeile in zwei Tabellen',
  // TRVActionTableToText
  'In Te&xt umwandeln...', 'In Text umwandeln|Tabelle in Text umwandeln',
  // TRVActionTableSort
  '&Sortieren...', 'Sortieren|Sortiert die Zeilen der Tabelle',
  // TrvActionTableCellLeftBorder
  '&Linker Rahmen', 'Linker Rahmen|Linken Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellRightBorder
  '&Rechter Rahmen', 'Rechter Rahmen|Rechten Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellTopBorder
  '&Oberer Rahmen', 'Oberer Rahmen|Oberen Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellBottomBorder
  '&Unterer Rahmen', 'Unterer Rahmen|Unteren Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellAllBorders
  '&Zellrahmen', 'Zellrahmen|Gesamten Zellrahmen ein- oder ausschalten',
  // TrvActionTableCellNoBorders
  '&Keine Rahmen', 'Keine Rahmen|Alle Zellrahmen ausschalten',
  // TrvActionFonts & TrvActionFontEx
  '&Schriftart...', 'Schriftart|�ndern der Schriftart f�r den markierten Text',
  // TrvActionFontBold
  '&Fett', 'Fett|Setzen oder l�schen des Attributs Fett f�r den markierten Text',
  // TrvActionFontItalic
  '&Kursiv', 'Kursiv|Setzen oder l�schen des Attributs Kursiv f�r den markierten Text',
  // TrvActionFontUnderline
  '&Unterstrichen', 'Unterstrichen|Setzen oder l�schen des Attributs Unterstrichen f�r den markierten Text',
  // TrvActionFontStrikeout
  '&Durchgestrichen', 'Durchgestrichen|Setzen oder l�schen des Attributs Durchgestrichen f�r den markierten Text',
  // TrvActionFontGrow
  'Schriftart ver&gr��ern', 'Schriftart vergr��ern|H�he des markierten Texts um 10% vergr��ern',
  // TrvActionFontShrink
  'Sc&hriftart verkleinern', 'Schriftart verkleinern|H�he des markierten Texts um 10% verkleinern',
  // TrvActionFontGrowOnePoint
  'Schriftart um einen Punkt verg&r��ern', 'Schriftart um einen Punkt vergr��ern|Vergr��ert die H�he des markierten Texts um einen Punkt',
  // TrvActionFontShrinkOnePoint
  'Schriftart um einen Punkt verkleinern', 'Schriftart um einen Punkt verkleinern|Verkleinert die H�he des markierten Texts um einen Punkt',
  // TrvActionFontAllCaps
  '&Alles Gro�buchstaben', 'Alles Gro�buchstaben|Alle Buchstaben des markierten Texts in Gro�buchstaben umwandeln',
  // TrvActionFontOverline
  '&Linie �ber Text', 'Linie �ber Text|F�gt eine Linie �ber dem markierten Text ein',
  // TrvActionFontColor
  'Text &Farbe...', 'Text Farbe|�ndern der Farbe f�r den markierten Text',
  // TrvActionFontBackColor
  'Text Hinter&grund Farbe...', 'Text Hintergrund Farbe|�ndert die Hintergrundfarbe des markierten Texts',
  // TrvActionAddictSpell3
  'Recht&schreibpr�fung', 'Rechtschreibpr�fung|Pr�ft die Rechtschreibung',
  // TrvActionAddictThesaurus3
  '&Thesaurus', 'Thesaurus|Zeigt Synonyme f�r das markierte Wort an',
  // TrvActionParaLTR
  'Links nach Rechts', 'Links nach Rechts|Setzt Schreibrichtung Links-nach-Rechts f�r markierten Text',
  // TrvActionParaRTL
  'Rechts nach Links', 'Rechts nach Links|Setzt Schreibrichtung Rechts-nach-Links f�r markierten Text',
  // TrvActionLTR
  'Text Links nach Rechts', 'Text Links nach Rechts|Setzt Schreibrichtung Links-nach-Rechts f�r markierten Text',
  // TrvActionRTL
  'Text Rechts nach Links', 'Text Rechts nach Links|Setzt Schreibrichtung Rechts-nach-Links f�r markierten Text',
  // TrvActionCharCase
  'Gro�-/Kleinschreibung', 'Gro�-/Kleinschreibung|�ndert Gro�-/Kleinschreibung f�r markierten Text',
  // TrvActionShowSpecialCharacters
  '&Nicht-druckbare Zeichen', 'Nicht-druckbare Zeichen|Ein- oder ausblenden von nicht-druckbaren Zeichen, wie z.B. Absatzmarken, Tabulatoren und Leerzeichen.',
  // TrvActionSubscript
  '&Tiefgestellt', 'Tiefgestellt|Der markierte Text wird tiefgestellt',
  // TrvActionSuperscript
  '&Hochgestellt', 'Hochgestellt|Der markierter Text wird hochgestellt',
  // TrvActionInsertFootnote
  '&Fu�note', 'Fu�note|Eine Fu�note einf�gen',
  // TrvActionInsertEndnote
  '&Endnote', 'Endnote|Eine Endnote einf�gen',
  // TrvActionEditNote
  'Note bearbeiten', 'Note bearbeiten|Mit der Bearbeitung von Fu�- und Endnoten beginnen',
  '&Ausblenden', 'Ausblenden|Ausblenden oder anzeigen des ausgew�hlten Fragments',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Formatvorlagen...', 'Formatvorlagen|Das Fenster f�r Formatvorlagen anzeigen',
  // TrvActionAddStyleTemplate
  'Form&atvorlage hinzuf�gen...', 'Formatvorlage hinzuf�gen|Eine neue Text- oder Absatzformatvorlage erstellen',
  // TrvActionClearFormat,
  'Formatierung l�s&chen', 'Formatierung l�schen|Die Text- und Absatzformatierung des ausgew�hlten Textes l�schen',
  // TrvActionClearTextFormat,
  '&Textformat l�schen', 'Textformat l�schen|Die Textformatierung des ausgew�hlten Textes l�schen',
  // TrvActionStyleInspector
  'Format&inspektor', 'Formatinspektor|Formatinspektor anzeigen/verbergen',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Ok', 'Abbrechen', 'Schlie�en', 'Einf�gen', '&�ffnen...', '&Speichern...', '&L�schen', 'Hilfe', 'Entfernen',
  // Others  -------------------------------------------------------------------
  // percents
  'Prozent',
  // left, top, right, bottom sides
  'Linke Seite', 'Obere Seite', 'Rechte Seite', 'Untere Seite',
  // save changes? confirm title
  '�nderungen in %s speichern?', 'Bitte best�tigen',
  // warning: losing formatting
  '%s kann Funktionalit�ten enthalten die mit dem gew�hlten Speicherformat nicht kompatibel sind.'#13+
  'M�chten Sie das Dokument wirklich in diesem Format speichern?',
  // RVF format name
  'RichView Format',
  // Error messages ------------------------------------------------------------
  'Fehler',
  'Fehler beim Laden der Datei.'#13#13'M�gliche Ursachen:'#13'- das Format dieser Datei wird nicht unterst�tzt;'#13+
  '- die Datei ist besch�digt;'#13'- die Datei ist gesperrt, weil sie bereits von einer anderen Anwendung verwendet wird.',
  'Fehler beim Laden der Grafikdatei.'#13#13'M�gliche Ursachen:'#13'- die Datei liegt in einem Grafikformat vor, dass nicht unterst�tzt wird;'#13+
  '- die Datei enth�lt keine Grafik;'#13+
  '- die Datei ist besch�digt;'#13'- die Datei ist gesperrt, weil sie bereits von einer anderen Anwendung verwendet wird.',
  'Fehler beim Speichern der Datei.'#13#13'M�gliche Ursachen:'#13'- kein freier Speicherplatz;'#13+
  '- das Speichermedium ist schreibgesch�tzt;'#13'- austauschbarer Datentr�ger ist nicht eingelegt;'#13+
  '- die Datei ist gesperrt, weil sie bereits von einer anderen Anwendung verwendet wird;'#13'- das Speichermedium ist besch�digt.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Dateien (*.rvf)|*.rvf',  'RichText Dateien (*.rtf)|*.rtf' , 'XML Dateien (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Text Dateien (*.txt)|*.txt', 'Text Dateien - Unicode (*.txt)|*.txt', 'Text Dateien - Automatische Auswahl (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Dateien (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Einfach (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dokumente (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Suche beendet', 'Suchtext ''%s'' nicht gefunden.',
  // 1 string replaced; Several strings replaced
  '1 Eintrag ersetzt', '%d Eintr�ge ersetzt.',
  // continue search from the beginning/end?
  'Ende des Dokuments erreicht. Am Beginn des Dokuments fortsetzen?',
  'Anfang des Dokuments erreicht. Am Ende des Dokuments fortsetzen?',  
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Schwarz', 'Braun', 'Olivgr�n', 'Dunkelgr�n', 'Gr�nblau', 'Dunkelblau', 'Indigo', 'Grau-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Dunkelrot', 'Orange', 'Dunkelgelb', 'Gr�n', 'Blaugr�n', 'Blau', 'Blaugrau', 'Grau-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rot', 'Hellorange', 'Limone', 'Meeresgr�n', 'Aqua', 'Hellblau', 'Violett', 'Grau-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Gold', 'Gelb', 'Strahlend Gr�n', 'T�rkis', 'Himmelblau', 'Pflaume', 'Grau-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Br�unlich', 'Hellgelb', 'Hellgr�n', 'Hellt�rkis', 'Blassblau', 'Lila', 'Weiss',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparent', '&Auto', '&Mehr Farben...', '&Standard',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Hintergrund', 'F&arbe:', 'Position', 'Hintergrund', 'Beispieltext.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ohne', '&Gesamter Bereich', 'F&este Kacheln', 'Gek&achelt', '&Zentriert',
  // Padding button
  '&Abstand...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'F�llfarbe', '&Anwenden auf:', '&Mehr Farben...', '&Abstand...',
  'Bitte eine Farbe ausw�hlen.',
  // [apply to:] text, paragraph, table, cell
  'Text', 'Absatz' , 'Tabelle', 'Zelle',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Schriftart', 'Schriftart', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Schriftart:', 'Schrift&grad', 'Schriftsch&nitt', '&Fett', '&Kursiv',
  // Script, Color, Back color labels, Default charset
  'Sk&ript:', '&Farbe:', 'Hinter&grund:', '(Standard Zeichensatz)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekte', 'Unterstrichen', '&Linie �ber Text', '&Durchgestrichen', '&Alles Gro�buchstaben',
  // Sample, Sample text
  'Beispiel', 'Beispieltext',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Zeichenabstand', '&Abstand:', '&Erweitert', '&Schmal',
  // Offset group-box, Offset label, Down, Up,
  'Position', '&Position:', '&Tiefgestellt', '&Hochgestellt',
  // Scaling group-box, Scaling
  'Skalierung', 'Sk&alierung:',
  // Sub/super script group box, Normal, Sub, Super
  'Hoch- und Tiefgestellt', '&Normal', '&Tiefgestellt', '&Hochgestellt',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'F�llung', '&Oben:', '&Links:', '&Unten:', '&Rechts:', '&Gleiche Werte',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Hyperlink einf�gen', 'Hyperlink', 'T&ext:', '&Ziel:', '<<Auswahl>>',
  // cannot open URL
  'Kann Adresse "%s" nicht �ffnen.',
  // hyperlink properties button, hyperlink style
  '&Anpassen...', '&Formatvorlage:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Hyperlink Eigenschaften', 'Farbe Normal', 'Farbe Aktiv',
  // Text [color], Background [color]
  '&Text:', '&Hintergrund',
  // Underline [color]
  'U&nterstrich:',
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'H&intergrund',
  // Underline [color], Attributes group-box,
  'U&nterstrich:', 'Eigenschaften',
  // Underline type: always, never, active (below the mouse)
  '&Unterstrich:', 'immer', 'nie', 'aktiv',
  // Same as normal check-box
  'Wie &Normal',
  // underline active links check-box
  'Aktive Links &unterstreichen',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Symbol einf�gen', '&Schriftart:', '&Zeichensatz:', 'Unicode',
  // Unicode block
  '&Block:',  
  // Character Code, Character Unicode Code, No Character
  'Zeichencode: %d', 'Zeichencode: Unicode %d', '(kein Zeichen)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Tabelle einf�gen', 'Tabellengr��e', 'Anzahl der &Spalten:', 'Anzahl der &Zeilen:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabellen Layout', 'Gr��e &automatisch festlegen', 'Tabellegr��e an &Fenster anpassen', 'Tabellengr��e &manuell festlegen',
  // Remember check-box
  'Gr��enanga&ben merken f�r neue Tabellen',
  // VAlign Form ---------------------------------------------------------------
  'Position',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Eigenschaften', 'Grafik', 'Position und Gr��e', 'Linie', 'Tabelle', 'Zeile', 'Zelle',
  // Image Appearance, Image Misc, Number tabs
  'Darstellung', 'Verschiedenes', 'Zahl',
  // Box position, Box size, Box appearance tabs
  'Position', 'Gr��e', 'Darstellung',
  // Preview label, Transparency group-box, checkbox, label
  'Vorschau:', 'Transparenz', '&Transparent', 'Transparente &Farbe:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  '&�ndern...', '&Sichern...', 'Automatisch',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikale Ausrichtung', '&Ausrichtung:',
  'Unterkante an Grundlinie des Textes', 'Mitte an Grundlinie des Textes',
  'Oberkante an Oberlinie des Textes', 'Unterkante an Unterlinie des Textes', 'Mitte an Mittelinie des Textes',
  // align to left side, align to right side
  'linke Seite', 'rechte Seite',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&�ndern um:', 'Dehnen', '&Breite:', '&H�he:', 'Standardgr��e: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Proportional skalieren', 'Zu&r�cksetzen',
  // Inside the border, border, outside the border groupboxes
  'Innerhalb des Rahmens', 'Rahmen', 'Au�erhalb des Rahmens',
  // Fill color, Padding (i.e. margins inside border) labels
  '&F�llfarbe:', '&Abstand:',
  // Border width, Border color labels
  'Rahmen&breite:', 'Rahmen&farbe:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontaler Abstand:', '&Vertikaler Abstand:',
  // Miscellaneous groupbox, Tooltip label
  'Verschiedenes', '&Kurzinformation:',
  // web group-box, alt text
  'Web', 'Alterna&tiver Text:',
  // Horz line group-box, color, width, style
  'Horizontale Linie', '&Farbe:', '&Breite:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabelle', '&Breite:', '&F�llfarbe:', '&Zellraum...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizontaler Zellenabstand:', '&Vertikaler Zellenabstand:', 'Rahmen und Hintergrund',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Sichtbare &Rahmenseiten...', 'Automatisch', 'automatisch',
  // Keep row content together checkbox
  '&Inhalt zusammenhalten',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Drehung', '&Keine', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Rahmen:', 'Sichtbare R&ahmenseiten...',
  // Border, CellBorders buttons
  '&Tabellenrahmen...', 'Z&ellrahmen...',
  // Table border, Cell borders dialog titles
  'Tabellenrahmen', 'Standard Zellrahmen',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Drucken', 'Tabelle auf einer &Seite', 'Anzahl der &Kopfzeilen:',
  'Tabellenkopfzeilen werden auf jeder Seite wiederholt',
  // top, center, bottom, default
  '&Oben', '&Zentriert', '&Unten', '&Standard',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Einstellungen', 'Bevorzugte &Breite:', '&H�he mindestens:', '&F�llfarbe:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Rahmen', 'Sichtbare Seiten:',  'Sch&attenfarbe:', '&Helle Farbe', 'F&arbe:',
  // Background image
  '&Grafik...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontale Position', 'Vertikale Position', 'Position im Text',
  // Position types: align, absolute, relative
  'Ausrichtung', 'Absolute Position', 'Relative Position',
  // [Align] left side, center, right side
  'linke Seite an linker Seite von',
  'mitte an mitte von',
  'rechte Seite an rechter Seite von',
  // [Align] top side, center, bottom side
  'obere Seite an oberer Seite von',
  'mitte an mitte von',
  'untere Seite an unterer Seite von',
  // [Align] relative to
  'relativ zu',
  // [Position] to the right of the left side of
  'rechts von der linken Seite von',
  // [Position] below the top side of
  'unterhalb der oberen Seite von',
  // Anchors: page, main text area (x2)
  '&Seite', '&Haupttextbereich', 'S&eite', 'Hauptte&xtbereich',
  // Anchors: left margin, right margin
  '&Linker Rand', '&Rechter Rand',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Oberer Rand', '&Unterer Rand', '&Innerer Rand', '�&u�erer Rand',
  // Anchors: character, line, paragraph
  '&Zeichen', 'Z&eile', '&Absatz',
  // Mirrored margins checkbox, layout in table cell checkbox
  'R�nder spi&egeln', 'La&yout in der Tabellenzelle',
  // Above text, below text
  '&Oberhalb des Textes', '&Unterhalb des Textes',
  // Width, Height groupboxes, Width, Height labels
  'Breite', 'H�he', '&Breite:', '&H�he:',
  // Auto height label, Auto height combobox item
  'Automatisch', 'automatisch',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikale Ausrichtung', '&Oben', '&Mitte', '&Unten',
  // Border and background button and title
  'Rah&men und Hintergrund...', 'Feldrahmen und Hintergrund',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Einf�gen als', '&Einf�gen als:', 'Rich Text Format', 'HTML Format',
  'Text', 'Unicode Text', 'Bitmapgrafik', 'Metagrafik',
  'Grafik-Dateien', 'URL',
  // Options group-box, styles
  'Optionen', '&Formatvorlagen:',
  // style options: apply target styles, use source styles, ignore styles
  'Formatvorlagen des Zieldokuments zuweisen', 'Formatvorlagen und Erscheinung erhalten',
  'Erscheinung erhalten, Formatvorlagen ignorieren',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Nummerierung und Aufz�hlungszeichen', 'Aufz�hlungszeichen', 'Nummeriert', 'Ungeordnete Liste', 'Nummerierte Liste',
  // Customize, Reset, None
  '&Anpassen...', '&Zur�cksetzen', 'Keine',
  // Numbering: continue, reset to, create new
  'Nummerierung fortsetzen', 'Nummerierung zur�cksetzen auf', 'Erstelle eine neue Liste, beginnend mit',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  //'Upper Alpha', 'Upper Roman', 'Decimal', 'Lower Alpha', 'Lower Roman',
  'Gro� Alpha', 'Gro� Roman', 'Dezimal', 'Klein Alpha', 'Klein Roman',
  // Bullet type
  // Circle, Disc, Square,
  'Kreis', 'Scheibe', 'Viereck', 
  // Level, Start from, Continue numbering, Numbering group-box
  '&Ebene', 'Starte von:', '&Fortsetzen', 'Nummerierung',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Liste anpassen', 'Ebenen', '&Z�hlung:', 'Listeneigenschaften', '&Listentyp:',
  // List types: bullet, image
  'Gliederungspunkt', 'Grafik', 'Nummer einf�gen|',
  // Number format, Number, Start level from, Font button
  '&Nummernformat:', 'Nummer', '&Nummerierungsebene beginnt mit:', '&Schriftart...',
  // Image button, bullet character, Bullet button,
  '&Grafik...', 'Aufz�hlungszeichen', '&Gliederungspunkt...',
  // Position of list text, bullet, number, image, text
  'Listentext Position', 'Aufz�hlungszeichenposition', 'Position der Nummerierung', 'Position der Grafik', 'Textposition',
  // at, left indent, first line indent, from left indent
  '&an:', 'L&inker Einzug:', '&Einzug der ersten Zeile:', 'vom linken Einzug',
  // [only] one level preview, preview
  '&Vorschau der ersten Ebene', 'Vorschau',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'links ausgerichtet', 'rechts ausgerichtet', 'zentriert',
  // level #, this level (for combo-box)
  'Ebene %d', 'Diese Ebene',
  // Marker character dialog title
  'Aufz�hlungszeichen bearbeiten',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Absatzrahmen und Hintergrund', 'Rahmen', 'Hintergrund',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Einstellungen', '&Farbe:', '&Breite:', 'Int&erne Breite:', 'O&ffsets...',
  // Sample, Border type group-boxes;
  'Beispiel', 'Rahmentyp',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ohne', '&Einfach', '&Doppelt', 'D&reifach', '&Innen dick', '&Au�en dick',
  // Fill color group-box; More colors, padding buttons
  'F�llfarbe', '&Mehr Farben...', '&Abstand...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Abst�nde', 'Rahmenabst�nde',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Absatz', 'Ausrichtung', '&Links', '&Rechts', '&Zentriert', '&Blocksatz',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Abstand', '&Vor:', '&Nach:', 'Zeilen&abstand:', 'v&on:',
  // Indents group-box; indents: left, right, first line
  'Einz�ge', 'L&inks:', 'R&echts:', 'E&rste Zeile:',
  // indented, hanging
  '&Eingezogen', '&H�ngend', 'Beispiel',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Einfach', '1.5 Zeilen', 'Zweizeilig', 'Mindestens', 'Genau', 'Mehrfach',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Einz�ge und Abst�nde', 'Tabulatoren', 'Textfluss',
  // tab stop position label; buttons: set, delete, delete all
  '&Tabulator Position:', '&Setzen', '&L�schen', '&Alle l�schen',
  // tab align group; left, right, center aligns,
  'Ausrichtung', '&Links', '&Rechts', '&Zentriert',
  // leader radio-group; no leader
  'F�hrungslinie', '(Keine)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulatoren zu l�schen:', '(Keine)', 'Alle.',
  // Pagination group-box, keep with next, keep lines together
  'Zeilen- und Seitenumbruch', '&Abs�tze nicht trennen', '&Zeilen nicht trennen',
  // Outline level, Body text, Level #
  '&Gliederungsebene:', 'Textk�rper', 'Ebene %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Druckvorschau', 'Seitenbreite', 'Ganze Seite', 'Seiten:',
  // Invalid Scale, [page #] of #
  'Bitte geben Sie eine Nummer zwischen 10 und 500 ein', 'von %d',
  // Hints on buttons: first, prior, next, last pages
  'Erste Seite', 'Vorherige Seite', 'N�chste Seite', 'Letzte Seite',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Zellraum', 'Abstand', '&Zwischen den Zellen', 'Vom Tabellenrahmen zu den Zellen',
  // vertical, horizontal (x2)
  '&Vertikal:', '&Horizontal:', 'V&ertikal:', 'H&orizontal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Rahmen', 'Einstellungen', '&Farbe:', '&Helle Farbe:', 'Scha&ttenfarbe:',
  // Width; Border type group-box;
  '&Breite:', 'Rahmentyp',
  // Border types: none, sunken, raised, flat
  '&Kein', '&Eingesunken', '&Erhoben', '&Glatt',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Teilen', 'Teilen in',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Bitte Anzahl der Zeilen und Spalten eingeben', '&Originalzellen',
  // number of columns, rows, merge before
  'Anzahl der &Spalten:', 'Anzahl der &Zeilen:', '&Verbinden vor dem Teilen',
  // to original cols, rows check-boxes
  'Teilen wie Origina&lspalten', 'Teilen wie O&riginalzeilen',
  // Add Rows form -------------------------------------------------------------
  'Zeilen hinzuf�gen', 'Anza&hl der Zeilen:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Seite einrichten', 'Seite', 'Kopf- und Fusszeile',
  // margins group-box, left, right, top, bottom
  'R�nder (Millimeter)', 'R�nder (Zoll)', '&Links:', '&Oben:', '&Rechts:', '&Unten:',
  // mirror margins check-box
  '&Seitenr�nder spiegeln',
  // orientation group-box, portrait, landscape
  'Orientierung', '&Hochformat', 'Qu&erformat',
  // paper group-box, paper size, default paper source
  'Papier', '&Gr��e:', '&Quelle:',
  // header group-box, print on the first page, font button
  'Kopfzeile', '&Text:', '&Kopfzeile auf der ersten Seite', '&Schriftart...',
  // header alignments: left, center, right
  '&Linksb�ndig', '&Zentriert', '&Rechtsb�ndig',
  // the same for footer (different hotkeys)
  '&Fusszeile', 'Te&xt:', 'Fusszeile auf der ersten Seite', 'S&chriftart...',
  'L&inksb�ndig', 'Z&entriert', 'Rec&htsb�ndig',
  // page numbers group-box, start from
  'Seitennummerierung', '&Beginnt bei:',
  // hint about codes
  'Spezielle Zeichenkombinationen:'#13'&&p - Seitennummer; &&P - Seitenanzahl; &&d - Aktuelles Datum; &&t - Aktuelle Zeit.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Codepage f�r Textdatei', 'Zei&chenkodierung f�r Datei ausw�hlen:',
  // thai, japanese, chinese (simpl), korean
  'Thail�ndisch', 'Japanisch', 'Chinesisch (Vereinfacht)', 'Koreanisch',
  // chinese (trad), central european, cyrillic, west european
  'Chinesisch (Traditionell)', 'Zentral- und Osteuropa', 'Kyrillisch', 'Westeurop�isch',
  // greek, turkish, hebrew, arabic
  'Griechisch', 'T�rkisch', 'Hebr�isch', 'Arabisch',
  // baltic, vietnamese, utf-8, utf-16
  'Baltisch', 'Vietnamesisch', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Erscheinung', '&Stil ausw�hlen:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'In Text umwandeln', 'Trennzeichen &w�hlen:',
  // line break, tab, ';', ','
  'Zeilenumbruch', 'Tabulator', 'Semikolon', 'Komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'Eine Tabelle mit verbundenen Zeilen kann nicht sortiert werden',
  // title, main options groupbox
  'Tabelle sortieren', 'Sortieren',
  // sort by column, case sensitive
  'Nach &Spalte sortieren:', 'S&chreibweise beachten',
  // heading row, range or rows
  'Kopfzeile aussc&lie�en', 'Sortiere Zeilen: von %d bis %d',
  // order, ascending, descending
  'Reihenfolge', '&Aufsteigend', 'Absteigen&d',
  // data type, text, number
  'Typ', '&Text', '&Zahl',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Zahl einf�gen', 'Eigenschaften', '&Z�hlername:', '&Nummerierungstyp:',
  // numbering groupbox, continue, start from
  'Nummerierung', '&Fortsetzen', '&Beginnen von:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Beschriftung', '&Bezeichnung:', 'Be&zeichnung nicht in der Beschriftung verwenden',
  // position radiogroup
  'Position', '&Oberhalb des ausgew�hlten Objekts', '&Unterhalb des ausgew�hlten Objekts',
  // caption text
  'Beschriftungs&text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Nummerierung', 'Abbildung', 'Tabelle',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Sichtbare Rahmenseiten', 'Rahmen',
  // Left, Top, Right, Bottom checkboxes
  '&Linke Seite', '&Obere Seite', '&Rechte Seite', '&Untere Seite',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Gr��e der Tabellenspalte �ndern', 'Gr��e der Tabellenzeile �ndern',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Erste Zeile einr�cken', 'Einzug links', 'H�ngender Einzug', 'Einzug rechts',
  // Hints on lists: up one level (left), down one level (right)
  'Eine Ebene h�her verschieben', 'Eine Ebene niedriger verschieben',
  // Hints for margins: bottom, left, right and top
  'Unterer Seitenrand', 'Linker Seitenrand', 'Rechter Seitenrand', 'Oberer Seitenrand',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Am Tabulator links ausrichten', 'Am Tabulator rechts ausrichten', 'Am Tabulator zentriert ausrichten', 'Am Dezimalkomma ausrichten',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Standard', 'Standard einger�ckt', 'Kein Leerraum', '�berschrift %d', 'Listenabsatz',
  // Hyperlink, Title, Subtitle
  'Hyperlink', 'Titel', 'Untertitel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Hervorhebung', 'Schwache Hervorhebung', 'Intensive Hervorhebung', 'Fett',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Zitat', 'Intensives Zitat', 'Schwacher Verweis', 'Intensiver Verweis',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blocksatz', 'HTML Variable', 'HTML Code', 'HTML Akronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Definition', 'HTML Tastatur', 'HTML Beispiel', 'HTML Schreibmaschine',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Vorformatiert', 'HTML Zitat', 'Kopf', 'Fu�', 'Seitenzahl',
  // Caption
  'Beschriftung',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Verweis Endnote', 'Verweis Fu�note', 'Text Endnote', 'Text Fu�note',
  // Sidenote Reference, Sidenote Text
  'Randnotiz Referenz', 'Randnotiz Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'Farbe', 'Hintergrundfarbe', 'transparent', 'Standard', 'Farbe Unterstreichung',
  // default background color, default text color, [color] same as text
  'Standardhintergrundfarbe', 'Standardtextfarbe', 'wie Text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'einfach', 'dick', 'doppelt', 'gepunkted', 'dick gepunkted', 'gestrichelt',
  // underline types: thick dashed, long dashed, thick long dashed,
  'dick gestrichelt', 'lang gestrichelt', 'dick und lang gestrichelt',
  // underline types: dash dotted, thick dash dotted,
  'Strich-Punkt', 'Strich-Punkt dick',
  // underline types: dash dot dotted, thick dash dot dotted
  'Strich-Punkt-Punkt', 'Strich-Punkt-Punkt dick',
  // sub/superscript: not, subsript, superscript
  'nicht tief-/hochgestellt', 'tiefgestellt', 'hochgestellt',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'Bi-di Modus:', '�bernommen', 'links nach rechts', 'rechts nach links',
  // bold, not bold
  'fett', 'nicht fett',
  // italic, not italic
  'kursiv', 'nicht kursiv',
  // underlined, not underlined, default underline
  'unterstrichen', 'nicht unterstrichen', 'Standardunterstreichung',
  // struck out, not struck out
  'durchgestrichen', 'nicht durchgestrichen',
  // overlined, not overlined
  '�berstrichen', 'nicht �berstrichen',
  // all capitals: yes, all capitals: no
  'alles in Kapit�lchen', 'Kapit�lchen f�r alles aus',
  // vertical shift: none, by x% up, by x% down
  'ohne vertikalen Versatz', 'versetzt um %d%% nach oben', 'versetzt um %d%% nach unten',
  // characters width [: x%]
  'Zeichenbreite',
  // character spacing: none, expanded by x, condensed by x
  'Standardzeichenabstand', 'Abstand erweitert um %s', 'Abstand verringert um %s',
  // charset
  'Skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'Standardschrift', 'Standardabsatz', '�bernommen',
  // [hyperlink] highlight, default hyperlink attributes
  'Hervorhebung', 'Standard',
  // paragraph aligmnment: left, right, center, justify
  'Ausrichtung links', 'Ausrichtung rechts', 'Zentriert', 'Blocksatz',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'Zeilenh�he: %d%%', 'Abstand zwischen den Zeilen: %s', 'Zeilenh�he: mindestens %s',
  'Zeilenh�he: exakt %s',
  // no wrap, wrap
  'Zeilenumbruch deaktiviert', 'Zeilenumbruch',
  // keep lines together: yes, no
  'Zeilen zusammenhalten', 'Zeilen nicht zusammenhalten',
  // keep with next: yes, no
  'mit n�chsten Absatz zusammenhalten', 'nicht mit n�chsten Absatz zusammenhalten',
  // read only: yes, no
  'nur-lesen', '�nderbar',
  // body text, heading level x
  'Gliederungsebene: Text', 'Gliederungsebene: %d',
  // indents: first line, left, right
  'Einzug erste Zeile', 'Einzug links', 'Einzug rechts',
  // space before, after
  'Abstand davor', 'Abstand danach',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'Tabulatoren', 'ohne', 'Ausrichtung', 'links', 'rechts', 'zentriert',
  // tab leader (filling character)
  'F�llzeichen',
  // no, yes
  'Nein', 'Ja',
  // [padding/spacing/side:] left, top, right, bottom
  'links', 'oben', 'rechts', 'unten',
  // border: none, single, double, triple, thick inside, thick outside
  'ohne', 'einfach', 'doppelt', 'dreifach', 'innen dick', 'au�en dick',
  // background, border, padding, spacing [between text and border]
  'Hintergrund', 'Rahmen', 'Textabstand', 'Zellenabstand',
  // border: style, width, internal width, visible sides
  'Stil', 'Breite', 'Innere Breite', 'Sichtbare Seiten',
  // style inspector -----------------------------------------------------------
  // title
  'Formatinspektor',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Ohne)', 'Absatz', 'Schrift', 'Eigenschaften',
  // border and background, hyperlink, standard [style]
  'Rahmen und Hintergrund', 'Hyperlink', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Formatvorlagen', 'Formatvorlage',
  // name, applicable to,
  '&Name:', 'Anwendbar &auf:',
  // based on, based on (no &),
  '&Basiert auf:', 'Basiert auf:',
  //  next style, next style (no &)
  'Formatvorlage f�r &nachfolgenden Absatz:', 'Formatvorlage f�r nachfolgenden Absatz:',
  // quick access check-box, description and preview
  'Sch&nellzugriff', 'Beschreibung und &Vorschau:',
  // [applicable to:] paragraph and text, paragraph, text
  'Absatz und Text', 'Absatz', 'Text',
  // text and paragraph styles, default font
  'Text- und Absatzformatvorlagen', 'Standardschrift',
  // links: edit, reset, buttons: add, delete
  'Bearbeiten', 'Zur�cksetzen', '&Hinzuf�gen', '&L�schen',
  // add standard style, add custom style, default style name
  '&Standardformatvorlage hinzuf�gen...', 'Benutzerformatvorlage hin&zuf�gen', 'Formatvorlage %d',
  // choose style
  'Formatvorlage aus&w�hlen:',
  // import, export,
  '&Importieren...', '&Exportieren...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView-Formatvorlagen (*.rvst)|*.rvst', 'Fehler beim Laden der Datei', 'Diese Datei enth�lt keine Formatvorlagen',
  // Title, group-box, import styles
  'Formatvorlagen aus Datei importieren', 'Importieren', 'Formatvorlagen i&mportieren:',
  // existing styles radio-group: Title, override, auto-rename
  'Vorhandene Formatvorlagen', '�&berschreiben', 'u&mbenennen und hinzuf�gen',
  // Select, Unselect, Invert,
  '&Ausw�hlen', 'Aus&wahl aufheben', '&Umkehren',
  // select/unselect: all styles, new styles, existing styles
  '&Alle Formatvorlagen', '&Neue Formatvorlagen', '&Vorhandene Formatvorlagen',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Format l�schen', 'Alle Formatvorlagen',
  // dialog title, prompt
  'Formatvorlagen', '&Bitte w�hlen Sie die Formatvorlage:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonyme', '&Alle ignorieren', '&Zum W�rterbuch hinzuf�gen',
  // Progress messages ---------------------------------------------------------
  'Lade %s', 'Vorbereitung f�r Ausdruck...', 'Drucke Seite %d',
  'Umwandeln von RTF...',  'Umwandeln in RTF...',
  'Lese %s...', 'Schreibe %s...', 'Text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ohne Notiz', 'Fu�note', 'Endnote', 'Randnotiz', 'Textfeld'
  );


initialization
  RVA_RegisterLanguage(
    'German', // english language name, do not translate
    'Deutsch', // native language name
    ANSI_CHARSET, @Messages);

end.
