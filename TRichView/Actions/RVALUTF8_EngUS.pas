﻿// This file is a copy of RVAL_EngUS.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       English (US) translation                        }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVALUTF8_EngUS;

interface
{$I RV_Defs.inc}
implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'inches', 'cm', 'mm', 'picas', 'pixels', 'points',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&File', '&Edit', 'F&ormat', 'Fo&nt', '&Paragraph', '&Insert', '&Table', '&Window', '&Help',
  // exit
  'E&xit',
  // top-level menus: View, Tools,
  '&View', 'Too&ls',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Size', 'Style', 'Selec&t', 'Alig&n Cell Contents', 'C&ell Borders',
  // menus: Table cell rotation
  'Cell &Rotation',
  // menus: Text flow, Footnotes/endnotes
  '&Text Flow', '&Footnotes',
  // ribbon tabs: tab1, tab2, view, table
  '&Home', '&Advanced', '&View', '&Table',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Clipboard', 'Font', 'Paragraph', 'List', 'Editing',
  // ribbon groups: insert, background, page setup,
  'Insert', 'Background', 'Page Setup',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Links', 'Footnotes', 'Document Views', 'Show/Hide', 'Zoom', 'Options',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Insert', 'Delete', 'Operations', 'Borders',
  // ribbon groups: styles 
  'Styles',
  // ribbon screen tip footer,
  'Press F1 for more help',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Recent documents', 'Save a copy of the document', 'Preview and print the document',
  // ribbon label: units combo
  'Units:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&New', 'New|Creates a new blank document',
  // TrvActionOpen
  '&Open...', 'Open|Opens a document from disk',
  // TrvActionSave
  '&Save', 'Save|Saves the document to disk',
  // TrvActionSaveAs
  'Save &As...', 'Save As...|Saves the document to disk with a new name',
  // TrvActionExport
  '&Export...', 'Export|Exports the document to another format',
  // TrvActionPrintPreview
  'P&rint Preview', 'Print Preview|Shows the document as it will be printed',
  // TrvActionPrint
  '&Print...', 'Print|Changes printing settings and prints the document',
  // TrvActionQuickPrint
  '&Print', '&Quick print', 'Print|Prints the document',
  // TrvActionPageSetup
  'Pa&ge Setup...', 'Page Setup|Sets margins, paper size, orientation, source, headers and footers',
  // TrvActionCut
  'Cu&t', 'Cut|Cuts the selection and puts it on the Clipboard',
  // TrvActionCopy
  '&Copy', 'Copy|Copies the selection and puts it on the Clipboard',
  // TrvActionPaste
  '&Paste', 'Paste|Inserts the Clipboard contents',
  // TrvActionPasteAsText
  'Paste as &Text', 'Paste as Text|Inserts text from the Clipboard',  
  // TrvActionPasteSpecial
  'Paste &Special...', 'Paste Special|Inserts the Clipboard contents in the specified format',
  // TrvActionSelectAll
  'Select &All', 'Select All|Selects all document',
  // TrvActionUndo
  '&Undo', 'Undo|Reverts the last action',
  // TrvActionRedo
  '&Redo', 'Redo|Redoes the last undone action',
  // TrvActionFind
  '&Find...', 'Find|Finds specified text in the document',
  // TrvActionFindNext
  'Find &Next', 'Find Next|Continues the last search',
  // TrvActionReplace
  '&Replace...', 'Replace|Finds and replaces specified text in the document',
  // TrvActionInsertFile
  '&File...', 'Insert File|Inserts contents of file in the document',
  // TrvActionInsertPicture
  '&Picture...', 'Insert Picture|Inserts a picture from disk',
  // TRVActionInsertHLine
  '&Horizontal Line', 'Insert Horizontal Line|Inserts a horizontal line',
  // TRVActionInsertHyperlink
  'Hyper&link...', 'Insert Hyperlink|Inserts a hypertext link',
  // TRVActionRemoveHyperlinks
  '&Remove Hyperlinks', 'Remove Hyperlinks|Removes all hyperlinks in the selected text',  
  // TrvActionInsertSymbol
  '&Symbol...', 'Insert Symbol|Inserts symbol',
  // TrvActionInsertNumber
  '&Number...', 'Insert Number|Inserts a number',
  // TrvActionInsertCaption
  'Insert &Caption...', 'Insert Caption|Inserts a caption for the selected object',
  // TrvActionInsertTextBox
  '&Text Box', 'Insert Text Box|Inserts a text box',
  // TrvActionInsertSidenote
  '&Sidenote', 'Insert Sidenote|Inserts a note displayed in a text box',
  // TrvActionInsertPageNumber
  '&Page Number', 'Insert Page Number|Inserts a page number',
  // TrvActionParaList
  '&Bullets and Numbering...', 'Bullets and Numbering|Applies or edits bullets or numbering for selected paragraphs',
  // TrvActionParaBullets
  '&Bullets', 'Bullets|Adds or removes bullets for the paragraph',
  // TrvActionParaNumbering
  '&Numbering', 'Numbering|Adds or removes numberings for the paragraph',
  // TrvActionColor
  'Background &Color...', 'Background|Changes a background color of the document',
  // TrvActionFillColor
  '&Fill Color...', 'Fill Color|Changes background color of  text, paragraph, cell, table or document',
  // TrvActionInsertPageBreak
  '&Insert Page Break', 'Insert Page Break|Inserts a page break',
  // TrvActionRemovePageBreak
  '&Remove Page Break', 'Remove Page Break|Removes the page break',
  // TrvActionClearLeft
  'Clear Text Flow at &Left Side', 'Clear Text Flow at Left Side|Places this paragraph below any left-aligned picture',
  // TrvActionClearRight
  'Clear Text Flow at &Right Side', 'Clear Text Flow at Right Side|Places this paragraph below any right-aligned picture',
  // TrvActionClearBoth
  'Clear Text Flow at &Both Sides', 'Clear Text Flow at Both Sides|Places this paragraph below any left- or right-aligned picture',
  // TrvActionClearNone
  '&Normal Text Flow', 'Normal Text Flow|Allows text flow around both left- and right-aligned pictures',
  // TrvActionVAlign
  '&Object Position...', 'Object Position|Changes the position of the chosen object',  
  // TrvActionItemProperties
  'Object &Properties...', 'Object Properties|Defines properties of the active object',
  // TrvActionBackground
  '&Background...', 'Background|Chooses background color and image',
  // TrvActionParagraph
  '&Paragraph...', 'Paragraph|Changes the paragraph attributes',
  // TrvActionIndentInc
  '&Increase Indent', 'Increase Indent|Increases left indent of the selected paragraphs',
  // TrvActionIndentDec
  '&Decrease Indent', 'Decrease Indent|Dereases left indent of the selected paragraphs',
  // TrvActionWordWrap
  '&Word Wrap', 'Word Wrap|Toggles word wrapping for the selected paragraphs',
  // TrvActionAlignLeft
  'Align &Left', 'Align Left|Aligns the selected text to the left',
  // TrvActionAlignRight
  'Align &Right', 'Align Right|Aligns the selected text to the right',
  // TrvActionAlignCenter
  'Align &Center', 'Align Center|Centers the selected text',
  // TrvActionAlignJustify
  '&Justify', 'Justify|Aligns the selected text to both left and right sides',
  // TrvActionParaColor
  'Paragraph &Background Color...', 'Paragraph Background Color|Sets background color of paragraphs',
  // TrvActionLineSpacing100
  '&Single Line Spacing', 'Single Line Spacing|Sets single line spacing',
  // TrvActionLineSpacing150
  '1.5 Li&ne Spacing', '1.5 Line Spacing|Sets line spacing equal to 1.5 lines',
  // TrvActionLineSpacing200
  '&Double Line Spacing', 'Double Line Spacing|Sets double line spacing',
  // TrvActionParaBorder
  'Paragraph &Border and Background...', 'Paragraph Border and Background|Sets borders and backround for the selected paragraphs',
  // TrvActionInsertTable
  '&Insert Table...', '&Table', 'Insert Table|Inserts a new table',
  // TrvActionTableInsertRowsAbove
  'Insert Row &Above', 'Insert Row Above|Inserts a new row above the selected cells',
  // TrvActionTableInsertRowsBelow
  'Insert Row &Below', 'Insert Row Below|Inserts a new row below the selected cells',
  // TrvActionTableInsertColLeft
  'Insert Column &Left', 'Insert Column Left|Insert a new column to the left of selected cells',
  // TrvActionTableInsertColRight
  'Insert Column &Right', 'Insert Column Right|Insert a new column to the right of selected cells',
  // TrvActionTableDeleteRows
  'Delete R&ows', 'Delete Rows|Deletes rows with the selected cells',
  // TrvActionTableDeleteCols
  'Delete &Columns', 'Delete Columns|Deletes columns with the selected cells',
  // TrvActionTableDeleteTable
  '&Delete Table', 'Delete Table|Deletes the table',
  // TrvActionTableMergeCells
  '&Merge Cells', 'Merge Cells|Merges the selected cells',
  // TrvActionTableSplitCells
  '&Split Cells...', 'Split Cells|Splits the selected cells',
  // TrvActionTableSelectTable
  'Select &Table', 'Select Table|Selects the table',
  // TrvActionTableSelectRows
  'Select Ro&ws', 'Select Rows|Selects rows',
  // TrvActionTableSelectCols
  'Select Col&umns', 'Select Columns|Selects columns',
  // TrvActionTableSelectCell
  'Select C&ell', 'Select Cell|Selects cell',
  // TrvActionTableCellVAlignTop
  'Align Cell to the &Top', 'Align Cell to the Top|Aligns cell contents to the top',
  // TrvActionTableCellVAlignMiddle
  'Align Cell to the &Middle', 'Align Cell to the Middle|Aligns cell contents to the middle',
  // TrvActionTableCellVAlignBottom
  'Align Cell to the &Bottom', 'Align Cell to the Bottom|Aligns cell contents to the bottom',
  // TrvActionTableCellVAlignDefault
  '&Default Cell Vertical Alignment', 'Default Cell Vertical Alignment|Sets default vertical alignment for the selected cells',
  // TrvActionTableCellRotationNone
  '&No Cell Rotation', 'No Cell Rotation|Rotates cell contents by 0°',
  // TrvActionTableCellRotation90
  'Rotate Cell by &90°', 'Rotate Cell by 90°|Rotates cell contents by 90°',
  // TrvActionTableCellRotation180
  'Rotate Cell by &180°', 'Rotate Cell by 180°|Rotates cell contents by 180°',
  // TrvActionTableCellRotation270
  'Rotate Cell by &270°', 'Rotate Cell by 270°|Rotates cell contents by 270°',
  // TrvActionTableProperties
  'Table &Properties...', 'Table Properties|Changes properties for the selected table',
  // TrvActionTableGrid
  'Show &Grid Lines', 'Show Grid Lines|Shows or hides table grid lines',
  // TRVActionTableSplit
  'Sp&lit Table', 'Split Table|Splits the table into two tables starting from the selected row',
  // TRVActionTableToText
  'Convert to Te&xt...', 'Convert to Text|Converts the table to text',
  // TRVActionTableSort
  '&Sort...', 'Sort|Sorts the table rows',
  // TrvActionTableCellLeftBorder
  '&Left Border', 'Left Border|Shows or hides left cell border',
  // TrvActionTableCellRightBorder
  '&Right Border', 'Right Border|Shows or hides right cell border',
  // TrvActionTableCellTopBorder
  '&Top Border', 'Top Border|Shows or hides top cell border',
  // TrvActionTableCellBottomBorder
  '&Bottom Border', 'Bottom Border|Shows or hides bottom cell border',
  // TrvActionTableCellAllBorders
  '&All Borders', 'All Borders|Shows or hides all cell borders',
  // TrvActionTableCellNoBorders
  '&No Borders', 'No Borders|Hides all cell borders',
  // TrvActionFonts & TrvActionFontEx
  '&Font...', 'Font|Changes font of the selected text',
  // TrvActionFontBold
  '&Bold', 'Bold|Changes the style of selected text to bold',
  // TrvActionFontItalic
  '&Italic', 'Italic|Changes the style of selected text to italic',
  // TrvActionFontUnderline
  '&Underline', 'Underline|Changes the style of selected text to underline',
  // TrvActionFontStrikeout
  '&Strikeout', 'Strikeout|Strikes out the selected text',
  // TrvActionFontGrow
  '&Grow Font', 'Grow Font|Grows height of the selected text by 10%',
  // TrvActionFontShrink
  'S&hrink Font', 'Shrink Font|Shrinks height of the selected text by 10%',
  // TrvActionFontGrowOnePoint
  'G&row Font by One Point', 'Grow Font by One Point|Grows height of the selected text by 1 point',
  // TrvActionFontShrinkOnePoint
  'Shri&nk Font by One Point', 'Shrink Font by One Point|Shrinks height of the selected text by 1 point',
  // TrvActionFontAllCaps
  '&All Capitals', 'All Capitals|Changes all characters of the selected text to capital letters',
  // TrvActionFontOverline
  '&Overline', 'Overline|Adds line over the selected text',
  // TrvActionFontColor
  'Text &Color...', 'Text Color|Changes color of the selected text',
  // TrvActionFontBackColor
  'Text Bac&kground Color...', 'Text Background Color|Changes background color of the selected text',
  // TrvActionAddictSpell3
  '&Spell Check', 'Spell Check|Checks the spelling',
  // TrvActionAddictThesaurus3
  '&Thesaurus', 'Thesaurus|Provides synonyms for the selected word',
  // TrvActionParaLTR
  'Left to Right', 'Left to Right|Sets left-to-right text direction for the selected paragraphs',
  // TrvActionParaRTL
  'Right to Left', 'Right to Left|Sets right-to-left text direction for the selected paragraphs',
  // TrvActionLTR
  'Left to Right Text', 'Left to Right Text|Sets left-to-right direction for the selected text',
  // TrvActionRTL
  'Right to Left Text', 'Right to Left Text|Sets right-to-left direction for the selected text',
  // TrvActionCharCase
  'Character Case', 'Character Case|Changes case of the selected text',
  // TrvActionShowSpecialCharacters
  '&Non-printing Characters', 'Non-printing Characters|Shows or hides non-printing characters, such as paragraph marks, tabs and spaces',
  // TrvActionSubscript
  'Su&bscript', 'Subscript|Converts the selected text to subscript',
  // TrvActionSuperscript
  'Superscript', 'Superscript|Converts the selected text to superscript',
  // TrvActionInsertFootnote
  '&Footnote', 'Footnote|Inserts a footnote',
  // TrvActionInsertEndnote
  '&Endnote', 'Endnote|Inserts an endnote',
  // TrvActionEditNote
  'E&dit Note', 'Edit Note|Starts editing the footnote or endnote',
  // TrvActionHide
  '&Hide', 'Hide|Hides or shows the selected fragment',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Styles...', 'Styles|Opens the style management dialog',
  // TrvActionAddStyleTemplate
  '&Add Style...', 'Add style|Creates a new text or paragraph style basing on the selected fragment',
  // TrvActionClearFormat,
  '&Clear Format', 'Clear Format|Clears all text and paragraph formatting from the selected fragment',
  // TrvActionClearTextFormat,
  'Clear &Text Format', 'Clear Text Format|Clears all formatting from the selected text',
  // TrvActionStyleInspector
  'Style &Inspector', 'Style Inspector|Shows or hides the Style Inspector',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Cancel', 'Close', 'Insert', '&Open...', '&Save...', '&Clear', 'Help', 'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'percent',
  // left, top, right, bottom sides
  'Left Side', 'Top Side', 'Right Side', 'Bottom Side',
  // save changes? confirm title
  'Save changes to %s?', 'Confirm',
  // warning: losing formatting
  '%s may contain features that are not compatible with the chosen saving format.'#13+
  'Do you want to save the document in this format?',
  // RVF format name
  'RichView Format',
  // Error messages ------------------------------------------------------------
  'Error',
  'Error loading file.'#13#13'Possible reasons:'#13'- format of this file is not supported by this application;'#13+
  '- the file is corrupted;'#13'- the file is opened and locked by another application.',
  'Error loading image file.'#13#13'Possible reasons:'#13'- the file has image in format which is not supported by this application;'#13+
  '- the file does not contain an image;'#13+
  '- the file is corrupted;'#13'- the file is opened and locked by another application.',
  'Error saving file.'#13#13'Possible reasons:'#13'- out of disk space;'#13+
  '- disk is write-protected;'#13'- removable media is not inserted;'#13+
  '- the file is opened and locked by another application;'#13'- disk media is corrupted.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Files (*.rvf)|*.rvf',  'RTF Files (*.rtf)|*.rtf' , 'XML Files (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Text Files (*.txt)|*.txt', 'Text Files - Unicode (*.txt)|*.txt', 'Text Files - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Files (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplified (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Documents (*.docx)|*.docx',  
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Search Complete', 'Search string ''%s'' not found.',
  // 1 string replaced; Several strings replaced
  '1 string replaced.', '%d strings replaced.',
  // continue search from the beginning/end?
  'The end of the document is reached, continue from the beginning?',
  'The beginning of the document is reached, continue from the end?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Black', 'Brown', 'Olive Green', 'Dark Green', 'Dark Teal', 'Dark Blue', 'Indigo', 'Gray-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Dark Red', 'Orange', 'Dark Yellow', 'Green', 'Teal', 'Blue', 'Blue-Gray', 'Gray-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Red', 'Light Orange', 'Lime', 'Sea Green', 'Aqua', 'Light Blue', 'Violet', 'Grey-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Pink', 'Gold', 'Yellow', 'Bright Green', 'Turquoise', 'Sky Blue', 'Plum', 'Gray-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Tan', 'Light Yellow', 'Light Green', 'Light Turquoise', 'Pale Blue', 'Lavender', 'White',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparent', '&Auto', '&More Colors...', '&Default',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Background', 'C&olor:', 'Position', 'Background', 'Sample text.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&None', 'St&retched', 'F&ixed Tiles', '&Tiles', 'C&enter',
  // Padding button
  '&Padding...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Fill Color', '&Apply to:', '&More colors...', '&Padding...',
  'Please select a color',
  // [apply to:] text, paragraph, table, cell
  'text', 'paragraph' , 'table', 'cell',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Font', 'Font', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Font:', '&Size', 'Style', '&Bold', '&Italic',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Color:', 'Bac&kground:', '(Default)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effects', 'Underline', '&Overline', 'S&trikethrough', '&All caps',
  // Sample, Sample text
  'Sample', 'Sample text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Character Spacing', '&Spacing:', '&Expanded', '&Condensed',
  // Offset group-box, Offset label, Down, Up,
  'Vertical offset', '&Offset:', '&Down', '&Up',
  // Scaling group-box, Scaling
  'Scaling', 'Sc&aling:',
  // Sub/super script group box, Normal, Sub, Super
  'Subscripts and superscripts', '&Normal', 'Su&bscript', 'Su&perscript',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Padding', '&Top:', '&Left:', '&Bottom:', '&Right:', '&Equal values',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insert Hyperlink', 'Hyperlink', 'T&ext:', '&Target', '<<selection>>',
  // cannot open URL
  'Cannot navigate to "%s"',
  // hyperlink properties button, hyperlink style
  '&Customize...', '&Style:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Hyperlink Attributes', 'Normal colors', 'Active colors',
  // Text [color], Background [color]
  '&Text:', '&Background',
  // Underline [color]
  'U&nderline:',
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'B&ackground',
  // Underline [color], Attributes group-box,
  'Un&derline:', 'Attributes',
  // Underline type: always, never, active (below the mouse)
  '&Underline:', 'always', 'never', 'active',
  // Same as normal check-box
  'As &normal',
  // underline active links check-box
  '&Underline active links',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insert Symbol', '&Font:', '&Character set:', 'Unicode',
  // Unicode block
  '&Block:',  
  // Character Code, Character Unicode Code, No Character
  'Character code: %d', 'Character code: Unicode %d', '(no character)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Insert Table', 'Table size', 'Number of &columns:', 'Number of &rows:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Table layout', '&Autosize', 'Fit &window', 'C&ustom size',
  // Remember check-box
  'Remember &dimensions for new tables',
  // VAlign Form ---------------------------------------------------------------
  'Position',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Properties', 'Image', 'Position and size', 'Line', 'Table', 'Rows', 'Cells',
  // Image Appearance, Image Misc, Number tabs
  'Appearance', 'Miscellaneous', 'Number',
  // Box position, Box size, Box appearance tabs
  'Position', 'Size', 'Appearance',
  // Preview label, Transparency group-box, checkbox, label
  'Preview:', 'Transparency', '&Transparent', 'Transparent &color:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'C&hange...', '&Save...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertical alignment', '&Align:',
  'bottom to base line of text', 'middle to base line of text',
  'top to line top', 'bottom to line bottom', 'middle to line middle',
  // align to left side, align to right side
  'to left side', 'to right side',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Shift by:', 'Stretch', '&Width:', '&Height:', 'Default size: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Scale &proportionally', '&Reset',
  // Inside the border, border, outside the border groupboxes
  'Inside the border', 'Border', 'Outside the border',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Fill color:', '&Padding:',
  // Border width, Border color labels
  'Border &width:', 'Border &color:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontal spacing:', '&Vertical spacing:',
  // Miscellaneous groupbox, Tooltip label
  'Miscellaneous', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Alternate text:',
  // Horz line group-box, color, width, style
  'Horizontal line', '&Color:', '&Width:', '&Style:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Table', '&Width:', '&Fill color:', 'Cell &spacing...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'H&orizontal cell padding:', '&Vertical cell padding:', 'Border and background',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Visible &border sides...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Keep content together',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotation', '&None', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Border:', 'Visible b&order sides...',
  // Border, CellBorders buttons
  '&Table border...', '&Cell borders...',
  // Table border, Cell borders dialog titles
  'Table border', 'Default cells borders',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Printing', '&Don''t allow columns to break across pages', 'Number of &heading rows:',
  'Heading rows are repeated on each page of the table',
  // top, center, bottom, default
  '&Top', '&Center', '&Bottom', '&Default',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Settings', 'Preferred &width:', '&Height at least:', '&Fill color:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Border', 'Visible sides:',  'Shadow co&lor:', '&Light color', 'Co&lor:',
  // Background image
  'P&icture...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontal position', 'Vertical position', 'Position in text',
  // Position types: align, absolute, relative
  'Align', 'Absolute position', 'Relative position',
  // [Align] left side, center, right side
  'left side of box to left side of',
  'center of box to center of',
  'right side of box to right side of',
  // [Align] top side, center, bottom side
  'top side of box to top side of',
  'center of box to center of',
  'bottom side of box to bottom side of',
  // [Align] relative to
  'relative to',
  // [Position] to the right of the left side of
  'to the right of the left side of',
  // [Position] below the top side of
  'below the top side of',
  // Anchors: page, main text area (x2)
  '&Page', '&Main text area', 'P&age', 'Main te&xt area',
  // Anchors: left margin, right margin
  '&Left margin', '&Right margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Top margin', '&Bottom margin', '&Inner margin', '&Outer margin',
  // Anchors: character, line, paragraph
  '&Character', 'L&ine', 'Para&graph',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Mirror&ed margins', 'La&yout in table cell',
  // Above text, below text
  '&Above text', '&Below text',
  // Width, Height groupboxes, Width, Height labels
  'Width', 'Height', '&Width:', '&Height:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertical alignment', '&Top', '&Center', '&Bottom',
  // Border and background button and title
  'B&order and background...', 'Box Border and Background',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Paste Special', '&Paste as:', 'Rich text format', 'HTML format',
  'Text', 'Unicode text', 'Bitmap picture', 'Metafile picture',
  'Graphic files', 'URL',
  // Options group-box, styles
  'Options', '&Styles:',
  // style options: apply target styles, use source styles, ignore styles
  'apply styles of the target document', 'keep styles and appearance',
  'keep appearance, ignore styles',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Bullets and Numbering', 'Bulleted', 'Numbered', 'Bulleted lists', 'Numbered lists',
  // Customize, Reset, None
  '&Customize...', '&Reset', 'None',
  // Numbering: continue, reset to, create new
  'continue numbering', 'reset numbering to', 'create a new list, starting from',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Upper Alpha', 'Upper Roman', 'Decimal', 'Lower Alpha', 'Lower Roman',
  // Bullet type
  'Circle', 'Disc', 'Square',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Level:', '&Start from:', '&Continue', 'Numbering',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Customize List', 'Levels', '&Count:', 'List properties', '&List type:',
  // List types: bullet, image
  'bullet', 'image', 'Insert Number|',
  // Number format, Number, Start level from, Font button
  '&Number format:', 'Number', '&Start level numbering from:', '&Font...',
  // Image button, bullet character, Bullet button,
  '&Image...', 'Bullet c&haracter', '&Bullet...',
  // Position of list text, bullet, number, image, text
  'List text position', 'Bullet position', 'Number position', 'Image position', 'Text position',
  // at, left indent, first line indent, from left indent
  '&at:', 'L&eft indent:', '&First line indent:', 'from left indent',
  // [only] one level preview, preview
  '&One level preview', 'Preview',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'align left', 'align right', 'center',
  // level #, this level (for combo-box)
  'Level %d', 'This level',
  // Marker character dialog title
  'Edit Bullet Character',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Paragraph Border and Background', 'Border', 'Background',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Settings', '&Color:', '&Width:', 'Int&ernal width:', 'Text &margins...',
  // Sample, Border type group-boxes;
  'Sample', 'Border type',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&None', '&Single', '&Double', '&Triple', 'Thick &inside', 'Thick &outside',
  // Fill color group-box; More colors, padding buttons
  'Fill color', '&More colors...', '&Padding...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Text Margins', 'Border offsets',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragraph', 'Alignment', '&Left', '&Right', '&Center', '&Justify',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Spacing', '&Before:', '&After:', 'Line &spacing:', 'a&t:',
  // Indents group-box; indents: left, right, first line
  'Indentation', 'L&eft:', 'Ri&ght:', '&First line:',
  // indented, hanging
  '&Indented', '&Hanging', 'Sample',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Single', '1.5 lines', 'Double', 'At least', 'Exactly', 'Multiple',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indents and spacing', 'Tabs', 'Text flow',
  // tab stop position label; buttons: set, delete, delete all
  '&Tab stop position:', '&Set', '&Delete', 'Delete &all',
  // tab align group; left, right, center aligns,
  'Alignment', '&Left', '&Right', '&Center',
  // leader radio-group; no leader
  'Leader', '(None)',
  // tab stops to be deleted, delete none, delete all labels
  'Tab stops to be deleted:', '(None)', 'All.',
  // Pagination group-box, keep with next, keep lines together
  'Pagination', '&Keep with next', '&Keep lines together',
  // Outline level, Body text, Level #
  '&Outline level:', 'Body Text', 'Level %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Print Preview', 'Page width', 'Full page', 'Pages:',
  // Invalid Scale, [page #] of #
  'Please enter number from 10 to 500', 'of %d',
  // Hints on buttons: first, prior, next, last pages
  'First Page', 'Prior Page', 'Next Page', 'Last Page',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Cell Spacing', 'Spacing', 'Between &cells', 'Between table border and cells',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'Ve&rtical:', 'H&orizontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Borders', 'Settings', '&Color:', '&Light color:', 'Shadow &color:',
  // Width; Border type group-box;
  '&Width:', 'Border type',
  // Border types: none, sunken, raised, flat
  '&None', '&Sunken', '&Raised', '&Flat',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Split', 'Split to',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Specified number of rows and columns', '&Original cells',
  // number of columns, rows, merge before
  'Number of &columns:', 'Number of &rows:', '&Merge before splitting',
  // to original cols, rows check-boxes
  'Split to original co&lumns', 'Split to original ro&ws',
  // Add Rows form -------------------------------------------------------------
  'Add Rows', 'Row &count:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Page Setup', 'Page', 'Header and Footer',
  // margins group-box, left, right, top, bottom
  'Margins (millimeters)', 'Margins (inches)', '&Left:', '&Top:', '&Right:', '&Bottom:',
  // mirror margins check-box
  '&Mirror margins',
  // orientation group-box, portrait, landscape
  'Orientation', '&Portrait', 'L&andscape',
  // paper group-box, paper size, default paper source
  'Paper', 'Si&ze:', '&Source:',
  // header group-box, print on the first page, font button
  'Header', '&Text:', '&Header on the first page', '&Font...',
  // header alignments: left, center, right
  '&Left', '&Center', '&Right',
  // the same for footer (different hotkeys)
  'Footer', 'Te&xt:', 'Footer on the first &page', 'F&ont...',
  'L&eft', 'Ce&nter', 'R&ight',
  // page numbers group-box, start from
  'Page numbers', '&Start from:',
  // hint about codes
  'Special character combinations:'#13'&&p - page number; &&P - count of pages; &&d - current date; &&t - current time.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Text File Code Page', '&Choose the file encoding:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japanese', 'Chinese (Simplified)', 'Korean',
  // chinese (trad), central european, cyrillic, west european
  'Chinese (Traditional)', 'Central and East European', 'Cyrillic', 'West European',
  // greek, turkish, hebrew, arabic
  'Greek', 'Turkish', 'Hebrew', 'Arabic',
  // baltic, vietnamese, utf-8, utf-16
  'Baltic', 'Vietnamese', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Visual Style', '&Select style:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convert to Text', 'Choose &delimiter:',
  // line break, tab, ';', ','
  'line break', 'tab', 'semicolon', 'comma',
  // Table sort form -----------------------------------------------------------
  // error message
  'A table containing merged rows cannot be sorted',
  // title, main options groupbox
  'Table Sort', 'Sort',
  // sort by column, case sensitive
  '&Sort by column:', '&Case sensitive',
  // heading row, range or rows
  'Exclude &heading row', 'Rows to sort: from %d to %d',
  // order, ascending, descending
  'Order', '&Ascending', '&Descending',
  // data type, text, number
  'Type', '&Text', '&Number',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Insert Number', 'Properties', '&Counter name:', '&Numbering type:',
  // numbering groupbox, continue, start from
  'Numbering', 'C&ontinue', '&Start from:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Caption', '&Label:', '&Exclude label from caption',
  // position radiogroup
  'Position', '&Above selected object', '&Below selected object',
  // caption text
  'Caption &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numbering', 'Figure', 'Table',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Visible Border Sides', 'Border',
  // Left, Top, Right, Bottom checkboxes
  '&Left side', '&Top side', '&Right side', '&Bottom side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Resize Table Column', 'Resize Table Row',
  // Hints on indents: first line, left (together with first), left (without first), right
  'First Line Indent', 'Left Indent', 'Hanging Indent', 'Right Indent',
  // Hints on lists: up one level (left), down one level (right)
  'Promote One Level', 'Demote One Level',
  // Hints for margins: bottom, left, right and top
  'Bottom Margin', 'Left Margin', 'Right Margin', 'Top Margin',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Left Aligned Tab', 'Right Aligned Tab', 'Center Aligned Tab', 'Decimal Aligned Tab',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normal Indent', 'No Spacing', 'Heading %d', 'List Paragraph',
  // Hyperlink, Title, Subtitle
  'Hyperlink', 'Title', 'Subtitle',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Emphasis', 'Subtle Emphasis', 'Intense Emphasis', 'Strong',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Quote', 'Intense Quote', 'Subtle Reference', 'Intense Reference',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Block Text', 'HTML Variable', 'HTML Code', 'HTML Acronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Definition', 'HTML Keyboard', 'HTML Sample', 'HTML Typewriter',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatted', 'HTML Cite', 'Header', 'Footer', 'Page Number',
  // Caption
  'Caption',  
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Endnote Reference', 'Footnote Reference', 'Endnote Text', 'Footnote Text',
  // Sidenote Reference, Sidenote Text
  'Sidenote Reference', 'Sidenote Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'color', 'background color', 'transparent', 'default', 'underline color',
  // default background color, default text color, [color] same as text
  'default background color', 'default text color', 'same as text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'single', 'thick', 'double', 'dotted', 'thick dotted', 'dashed',
  // underline types: thick dashed, long dashed, thick long dashed,
  'thick dashed', 'long dashed', 'thick long dashed',
  // underline types: dash dotted, thick dash dotted,
  'dash dotted', 'thick dash dotted',
  // underline types: dash dot dotted, thick dash dot dotted
  'dash dot dotted', 'thick dash dot dotted',
  // sub/superscript: not, subsript, superscript
  'not sub/superscript', 'subscript', 'superscript',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mode:', 'inherited', 'left to right', 'right to left',
  // bold, not bold
  'bold', 'not bold',
  // italic, not italic
  'italic', 'not italic',
  // underlined, not underlined, default underline
  'underlined', 'not underlined', 'default underline',
  // struck out, not struck out
  'struck out', 'not struck out',
  // overlined, not overlined
  'overlined', 'not overlined',
  // all capitals: yes, all capitals: no
  'all capitals', 'all capitals off',
  // vertical shift: none, by x% up, by x% down
  'without vertical shift', 'shifted by %d%% up', 'shifted by %d%% down',
  // characters width [: x%]
  'characters width',
  // character spacing: none, expanded by x, condensed by x
  'normal character spacing', 'spacing expanded by %s', 'spacing condensed by %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'default font', 'default paragraph', 'inherited',
  // [hyperlink] highlight, default hyperlink attributes
  'highlight', 'default',
  // paragraph aligmnment: left, right, center, justify
  'left aligned', 'right aligned', 'centered', 'justified',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'line height: %d%%', 'space between lines: %s', 'line height: at least %s',
  'line height: exactly %s',
  // no wrap, wrap
   'line wrapping disabled', 'line wrapping',
  // keep lines together: yes, no
  'keep lines together', 'do not keep lines together',
  // keep with next: yes, no
  'keep with next paragraph', 'do not keep with next paragraph',
  // read only: yes, no
  'read-only', 'editable',
  // body text, heading level x
  'outline level: body text', 'outline level: %d',
  // indents: first line, left, right
  'first line indent', 'left indent', 'right indent',
  // space before, after
  'space before', 'space after',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabs', 'none', 'align', 'left', 'right', 'center',
  // tab leader (filling character)
  'leader',
  // no, yes
  'no', 'yes',
  // [padding/spacing/side:] left, top, right, bottom
  'left', 'top', 'right', 'bottom',
  // border: none, single, double, triple, thick inside, thick outside
  'none', 'single', 'double', 'triple', 'thick inside', 'thick outside',
  // background, border, padding, spacing [between text and border]
  'background', 'border', 'padding', 'spacing',
  // border: style, width, internal width, visible sides
  'style', 'width', 'internal width', 'visible sides',
  // style inspector -----------------------------------------------------------
  // title
  'Style Inspector',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Style', '(None)', 'Paragraph', 'Font', 'Attributes',
  // border and background, hyperlink, standard [style]
  'Border and background', 'Hyperlink', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Styles', 'Style',
  // name, applicable to,
  '&Name:', 'Applicable &to:',
  // based on, based on (no &),
  '&Based on:', 'Based on:',
  //  next style, next style (no &)
  'Style for &following paragraph:', 'Style for following paragraph:',
  // quick access check-box, description and preview
  '&Quick access', 'Description and &preview:',
  // [applicable to:] paragraph and text, paragraph, text
  'paragraph and text', 'paragraph', 'text',
  // text and paragraph styles, default font
  'Text and Paragraph Styles', 'Default Font',
  // links: edit, reset, buttons: add, delete
  'Edit', 'Reset', '&Add', '&Delete',
  // add standard style, add custom style, default style name
  'Add &Standard Style...', 'Add &Custom Style', 'Style %d',
  // choose style
  'Choose &style:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView Styles (*.rvst)|*.rvst', 'Error loading file', 'This file contains no styles',
  // Title, group-box, import styles
  'Import Styles from File', 'Import', 'I&mport styles:',
  // existing styles radio-group: Title, override, auto-rename
  'Existing styles', '&override', '&add renamed',
  // Select, Unselect, Invert,
  '&Select', '&Unselect', '&Invert',
  // select/unselect: all styles, new styles, existing styles
  '&All Styles', '&New Styles', '&Existing Styles',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Clear Format', 'All Styles',
  // dialog title, prompt
  'Styles', '&Choose style to apply:',    
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonyms', '&Ignore All', '&Add to Dictionary',
  // Progress messages ---------------------------------------------------------
  'Downloading %s', 'Preparing for printing...', 'Printing the page %d',
  'Converting from Rich text format...',  'Converting to Rich text format...',
  'Reading %s...', 'Writing %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'No Note', 'Footnote', 'Endnote', 'Sidenote', 'Text Box'
  );


initialization
  RVA_RegisterLanguage(
    'English (US)',
    'English (US)',
    ANSI_CHARSET, @Messages);
  RVA_SwitchLanguage('English (US)', nil); // <-- only for 'English (US)'

end.