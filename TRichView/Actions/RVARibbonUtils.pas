
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Procedures for TRibbon                          }
{                                                       }
{       TRichView (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit RVARibbonUtils;

interface

uses SysUtils, Classes, Controls, ActnList, ActnMan, Ribbon,
  RVALocalize, RVRuler;

// For all action items,  copies Action.Hint to CommandProperties.Content
procedure FillActionClientContents(Manager: TActionManager);
// Remove ending '...' for all actions on the form
procedure RemoveEllipsis(Form: TComponent);
procedure RemoveEllipsisFromRibbon(Manager: TActionManager);
// Assigns Caption and KeyTip to Page
procedure LocalizeRibbonPage(Page: TRibbonPage; CaptionID: TRVAMessageID);
// Change ruler colors
procedure SetRulerColors(Ruler: TRVRuler; ColorMap: TCustomRibbonColorMap);

implementation

{------------------------------------------------------------------------------}
procedure FillActionClientContents(Manager: TActionManager);
   {....................................................................}
   procedure ProcessItem(Item: TActionClientItem);
   var i: Integer;
   begin
     if (Item.CommandProperties is TMenuProperties) and
       (Item.Action is TAction)  then  begin
       TMenuProperties(Item.CommandProperties).Content.Text :=
         GetLongHint(TAction(Item.Action).Hint);
       TMenuProperties(Item.CommandProperties).ShowRichContent := True;
     end;
     if (Item.Action is TAction) then begin
       Item.Caption := TAction(Item.Action).Caption;
     end;
     for i := 0 to Item.Items.Count - 1 do
       ProcessItem(Item.Items[i]);
   end;
   {....................................................................}
var i,j: Integer;
begin
   for i := 0 to Manager.ActionBars.Count - 1 do
     with Manager.ActionBars[i] do
       for j := 0 to Items.Count - 1 do
         ProcessItem(Items[j]);
end;
{------------------------------------------------------------------------------}
procedure RemoveEllipsisFromRibbon(Manager: TActionManager);
   {....................................................................}
   procedure ProcessItem(Item: TActionClientItem);
   var s: String;
   begin
      s := Item.Caption;
      if Copy(s, Length(s)-2, 3)='...' then
        Item.Caption := Copy(s, 1, Length(s)-3);
   end;
   {....................................................................}
var i,j: Integer;
begin
   for i := 0 to Manager.ActionBars.Count - 1 do
     with Manager.ActionBars[i] do
       for j := 0 to Items.Count - 1 do
         ProcessItem(Items[j]);
end;
{------------------------------------------------------------------------------}
procedure RemoveEllipsis(Form: TComponent);
var i: Integer;
    s: String;
begin
  for i := 0 to Form.ComponentCount - 1 do
    if (Form.Components[i] is TAction) then begin
      s := TAction(Form.Components[i]).Caption;
      if Copy(s, Length(s)-2, 3)='...' then
        TAction(Form.Components[i]).Caption := Copy(s, 1, Length(s)-3);
    end;
end;
{------------------------------------------------------------------------------}
procedure LocalizeRibbonPage(Page: TRibbonPage; CaptionID: TRVAMessageID);
var s: String;
    i: Integer;
begin
  s := RVA_GetS(CaptionID);
  i := 1;
  while i<Length(s) do begin
    if s[i]='&' then
      if s[i+1]='&' then
        inc(i)
      else begin
        Page.KeyTip := UpperCase(s[i+1]);
        Delete(s, i, 1);
        Page.Caption := s;
        exit;
      end;
    inc(i);
  end;
  Page.Caption := s;
  Page.KeyTip := '';
end;
{------------------------------------------------------------------------------}
procedure SetRulerColors(Ruler: TRVRuler; ColorMap: TCustomRibbonColorMap);
begin
  Ruler.Color := ColorMap.BevelMinimizedOuter;
  Ruler.RulerColor := ColorMap.ControlColor;
  Ruler.MarginColor :=  ColorMap.BevelMinimizedOuter;
  Ruler.IndentColor :=  ColorMap.BevelMinimizedOuter;
  Ruler.RulerColorPageEnd := ColorMap.BevelMinimizedInner;
  Ruler.Font.Color := ColorMap.DocumentFontColor;

end;

end.
