﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'HypHoverPropRVFrm.pas' rev: 27.00 (Windows)

#ifndef HyphoverproprvfrmHPP
#define HyphoverproprvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Hyphoverproprvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVHypHoverProp;
class PASCALIMPLEMENTATION TfrmRVHypHoverProp : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gbHover;
	Vcl::Stdctrls::TLabel* lblHoverText;
	Vcl::Stdctrls::TLabel* lblHoverTextBack;
	Vcl::Stdctrls::TLabel* lblHoverUnderlineColor;
	Rvcolorcombo::TRVColorCombo* cmbHoverText;
	Rvcolorcombo::TRVColorCombo* cmbHoverTextBack;
	Vcl::Stdctrls::TCheckBox* cbAsNormal;
	Rvcolorcombo::TRVColorCombo* cmbHoverUnderlineColor;
	Vcl::Stdctrls::TGroupBox* gbEffects;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TCheckBox* cbHoverUnderline;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cbAsNormalClick(System::TObject* Sender);
	
public:
	Vcl::Controls::TControl* _cbAsNormal;
	Vcl::Controls::TControl* _cbHoverUnderline;
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVHypHoverProp(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVHypHoverProp(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVHypHoverProp(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVHypHoverProp(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Hyphoverproprvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_HYPHOVERPROPRVFRM)
using namespace Hyphoverproprvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// HyphoverproprvfrmHPP
