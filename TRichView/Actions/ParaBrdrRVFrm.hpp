﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ParaBrdrRVFrm.pas' rev: 27.00 (Windows)

#ifndef ParabrdrrvfrmHPP
#define ParabrdrrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BorderSidesRVFrm.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <RVColorGrid.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <ColorRVFrm.hpp>	// Pascal unit
#include <RVGrids.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Parabrdrrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVParaBrdr;
class PASCALIMPLEMENTATION TfrmRVParaBrdr : public Bordersidesrvfrm::TfrmRVBorderSides
{
	typedef Bordersidesrvfrm::TfrmRVBorderSides inherited;
	
__published:
	Vcl::Controls::TImageList* ImageList1;
	Vcl::Stdctrls::TLabel* lblIW;
	Rvspinedit::TRVSpinEdit* seIW;
	Vcl::Stdctrls::TButton* btnOffsets;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgBorderType;
	Vcl::Stdctrls::TGroupBox* gbFillColor;
	Vcl::Stdctrls::TLabel* lblBackColor;
	Rvcolorgrid::TRVColorGrid* rvcg;
	Vcl::Stdctrls::TButton* btnColor;
	Vcl::Stdctrls::TButton* btnPadding;
	Vcl::Dialogs::TColorDialog* ColorDialog1;
	Vcl::Stdctrls::TLabel* lblIWMU;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall rgBorderTypeClick(System::TObject* Sender);
	HIDESBASE void __fastcall cmbColorColorChange(System::TObject* Sender);
	HIDESBASE void __fastcall btnClick(System::TObject* Sender);
	void __fastcall seIWChange(System::TObject* Sender);
	void __fastcall btnOffsetsClick(System::TObject* Sender);
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall PageControl1Change(System::TObject* Sender);
	void __fastcall btnPaddingClick(System::TObject* Sender);
	void __fastcall rvcgColorChange(System::TObject* Sender);
	void __fastcall btnColorClick(System::TObject* Sender);
	void __fastcall cmbWidthClick(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _gbSample;
	Vcl::Controls::TControl* _PageControl1;
	Vcl::Controls::TControl* _lblBackColor;
	System::Uitypes::TColor FBackColor;
	int LblBackColorRight;
	Rvtable::TRVTableItemInfo* table;
	Rvstyle::TRVStyleLength BOLeft;
	Rvstyle::TRVStyleLength BOTop;
	Rvstyle::TRVStyleLength BORight;
	Rvstyle::TRVStyleLength BOBottom;
	bool UBOLeft;
	bool UBOTop;
	bool UBORight;
	bool UBOBottom;
	Rvstyle::TRVStyleLength PaddingLeft;
	Rvstyle::TRVStyleLength PaddingTop;
	Rvstyle::TRVStyleLength PaddingRight;
	Rvstyle::TRVStyleLength PaddingBottom;
	bool FPaddingLeft;
	bool FPaddingTop;
	bool FPaddingRight;
	bool FPaddingBottom;
	bool Updating;
	Rvstyle::TRVStyleLength __fastcall GetOff(Rvstyle::TRVStyleLength Offs, bool UseIt);
	void __fastcall SetBackColor(System::Uitypes::TColor Value);
	Vcl::Dialogs::TColorDialog* __fastcall GetCD(void);
	
public:
	Vcl::Dialogs::TColorDialog* ColorDialog;
	bool BCIndeterminate;
	bool OnlyPositiveOffsets;
	void __fastcall UpdateSample(void);
	void __fastcall SetBO(Rvstyle::TRVRect* Offsets, Rvstyle::TRVBooleanRect* UseOffsets);
	void __fastcall GetBO(Rvstyle::TRVRect* Offsets, Rvstyle::TRVBooleanRect* UseOffsets);
	HIDESBASE void __fastcall SetPadding(Rvstyle::TRVRect* Padding, Rvstyle::TRVBooleanRect* UsePadding);
	void __fastcall GetPadding(Rvstyle::TRVRect* Padding, Rvstyle::TRVBooleanRect* UsePadding);
	DYNAMIC void __fastcall Localize(void);
	void __fastcall SetToForm(Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVBorder* Border, Rvstyle::TRVBackgroundRect* Background, Richviewactions::TRVParaInfoBorderProperties ValidProperties);
	void __fastcall GetFromForm(Rvstyle::TRVBorder* Border, Rvstyle::TRVBackgroundRect* Background, Richviewactions::TRVParaInfoBorderProperties &ValidProperties);
	__property System::Uitypes::TColor BackColor = {read=FBackColor, write=SetBackColor, nodefault};
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVParaBrdr(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Bordersidesrvfrm::TfrmRVBorderSides(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVParaBrdr(System::Classes::TComponent* AOwner, int Dummy) : Bordersidesrvfrm::TfrmRVBorderSides(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVParaBrdr(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVParaBrdr(HWND ParentWindow) : Bordersidesrvfrm::TfrmRVBorderSides(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Parabrdrrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PARABRDRRVFRM)
using namespace Parabrdrrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ParabrdrrvfrmHPP
