﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TableSort.pas' rev: 27.00 (Windows)

#ifndef TablesortHPP
#define TablesortHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVGetTextW.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tablesort
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVSortDataType : unsigned char { rvsdtString, rvsdtNumber };

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool __fastcall CanSortTable(Rvtable::TRVTableItemInfo* table, int FirstRow = 0x0, int LastRow = 0xffffffff);
extern DELPHI_PACKAGE Rvtable::TRVTableItemInfo* __fastcall CreateSortedTable(Richview::TCustomRichView* rv, Rvtable::TRVTableItemInfo* table, int Column, bool Ascending, bool IgnoreCase, TRVSortDataType DataType, int FirstRow = 0x0, int LastRow = 0xffffffff);
extern DELPHI_PACKAGE void __fastcall GetRangeOfRowsForSorting(Rvtable::TRVTableItemInfo* table, bool HasHeadingRow, bool IgnoreTableHeadingRow, int &FirstRow, int &LastRow);
extern DELPHI_PACKAGE bool __fastcall DoSortCurrentTable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo, int Column, bool Ascending, bool IgnoreCase, bool HasHeadingRow, TRVSortDataType DataType);
extern DELPHI_PACKAGE bool __fastcall SortCurrentTable(Rvedit::TCustomRichViewEdit* rve, int Column, bool Ascending, bool IgnoreCase, bool HasHeadingRow, TRVSortDataType DataType);
}	/* namespace Tablesort */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TABLESORT)
using namespace Tablesort;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TablesortHPP
