﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ColorRVFrm.pas' rev: 27.00 (Windows)

#ifndef ColorrvfrmHPP
#define ColorrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Winapi.MMSystem.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Colorrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVColorBackPanel;
class PASCALIMPLEMENTATION TRVColorBackPanel : public Vcl::Extctrls::TPanel
{
	typedef Vcl::Extctrls::TPanel inherited;
	
protected:
	System::Classes::TComponent* ControlPanel;
	bool CustomPaint;
	virtual void __fastcall Paint(void);
public:
	/* TCustomPanel.Create */ inline __fastcall virtual TRVColorBackPanel(System::Classes::TComponent* AOwner) : Vcl::Extctrls::TPanel(AOwner) { }
	
public:
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TRVColorBackPanel(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVColorBackPanel(HWND ParentWindow) : Vcl::Extctrls::TPanel(ParentWindow) { }
	
};


typedef Vcl::Buttons::TSpeedButton TRVColorButton;

class DELPHICLASS TfrmColor;
class PASCALIMPLEMENTATION TfrmColor : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
private:
	typedef System::DynamicArray<Vcl::Buttons::TSpeedButton*> _TfrmColor__1;
	
	
__published:
	Vcl::Dialogs::TColorDialog* cd;
	void __fastcall FormKeyDown(System::TObject* Sender, System::Word &Key, System::Classes::TShiftState Shift);
	void __fastcall FormDeactivate(System::TObject* Sender);
	void __fastcall FormShow(System::TObject* Sender);
	void __fastcall FormClose(System::TObject* Sender, System::Uitypes::TCloseAction &Action);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall FormPaint(System::TObject* Sender);
	
private:
	Vcl::Dialogs::TColorDialog* FColorDialog;
	System::Uitypes::TColor FColor;
	bool FChosen;
	int Index;
	_TfrmColor__1 ColorButtons;
	int DefColorCount;
	System::UnicodeString FDefaultCaption;
	unsigned FPopupTime;
	TRVColorBackPanel* FBackPanel;
	void __fastcall CreateButtons(int Rows, int Columns, System::Uitypes::TColor DefaultColor, bool AddDefault2Color, System::Uitypes::TColor Default2Color);
	void __fastcall MoreClick(System::TObject* Sender);
	void __fastcall ColorClick(System::TObject* Sender);
	void __fastcall UpdateBtnState(void);
	
protected:
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	
public:
	bool Popup;
	bool NoSound;
	void __fastcall PopupAtMouse(void);
	void __fastcall PopupAt(const System::Types::TRect &r);
	void __fastcall PopupAtControl(Vcl::Controls::TControl* ctrl);
	void __fastcall Init(System::Uitypes::TColor DefaultColor, Vcl::Dialogs::TColorDialog* ColorDialog, System::Uitypes::TColor Color, bool AddDefault2Color = false, System::Uitypes::TColor Default2Color = (System::Uitypes::TColor)(0x1fffffff));
	__property System::Uitypes::TColor ChosenColor = {read=FColor, nodefault};
	__property bool Chosen = {read=FChosen, nodefault};
	__property System::UnicodeString DefaultCaption = {read=FDefaultCaption, write=FDefaultCaption};
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmColor(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmColor(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmColor(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmColor(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Colorrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_COLORRVFRM)
using namespace Colorrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ColorrvfrmHPP
