
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Split-cells dialog                              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit SplitRVFrm;

{$I RichViewActions.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, BaseRVFrm, RVSpinEdit, RVALocalize;

type
  TfrmRVSplit = class(TfrmRVBase)
    panUnmerge: TGroupBox;
    cbUnmergeRows: TCheckBox;
    cbUnmergeCols: TCheckBox;
    btnOk: TButton;
    btnCancel: TButton;
    GroupBox1: TGroupBox;
    rbSplit: TRadioButton;
    rbUnmerge: TRadioButton;
    panSplit: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    seColumns: TRVSpinEdit;
    seRows: TRVSpinEdit;
    cbMerge: TCheckBox;
    procedure rbClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _rbSplit: TControl;
    _rbUnmerge: TControl;
    _panUnmerge, _panSplit,
    _cbUnmergeRows, _cbUnmergeCols, _cbMerge: TControl;
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation

{$R *.dfm}

procedure TfrmRVSplit.rbClick(Sender: TObject);
begin
  _panSplit.Visible := GetRadioButtonChecked(_rbSplit);
  _panUnmerge.Visible := GetRadioButtonChecked(_rbUnmerge);
end;

function TfrmRVSplit.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := panSplit.Left*2+panSplit.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-panSplit.Top-panSplit.Height);
  Result := True;
end;

procedure TfrmRVSplit.Localize;
begin
  inherited;
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  Caption := RVA_GetS(rvam_spl_Title, ControlPanel);
  GroupBox1.Caption := RVA_GetSHAsIs(rvam_spl_SplitTo, ControlPanel);
  rbSplit.Caption := RVA_GetSAsIs(rvam_spl_Specified, ControlPanel);
  rbUnmerge.Caption := RVA_GetSAsIs(rvam_spl_Original, ControlPanel);
  cbUnmergeRows.Caption := RVA_GetSAsIs(rvam_spl_OriginalRows, ControlPanel);
  cbUnmergeCols.Caption := RVA_GetSAsIs(rvam_spl_OriginalCols, ControlPanel);
  Label1.Caption := RVA_GetSAsIs(rvam_spl_nCols, ControlPanel);
  Label2.Caption := RVA_GetSAsIs(rvam_spl_nRows, ControlPanel);
  cbMerge.Caption := RVA_GetSAsIs(rvam_spl_Merge, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVSplit.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _rbSplit then
    _rbSplit := NewControl
  else if OldControl = _rbUnmerge then
    _rbUnmerge := NewControl
  else if OldControl = _panUnmerge then
    _panUnmerge := NewControl
  else if OldControl = _panSplit then
    _panSplit := NewControl
  else if OldControl = _cbUnmergeRows then
    _cbUnmergeRows := NewControl
  else if OldControl = _cbUnmergeCols then
    _cbUnmergeCols := NewControl
  else if OldControl = _cbMerge then
    _cbMerge := NewControl;
end;
{$ENDIF}

procedure TfrmRVSplit.FormCreate(Sender: TObject);
begin
  _rbSplit   := rbSplit;
  _rbUnmerge := rbUnmerge;
  _panUnmerge := panUnmerge;
  _panSplit := panSplit;
  _cbUnmergeRows := cbUnmergeRows;
  _cbUnmergeCols := cbUnmergeCols;
  _cbMerge       := cbMerge;
  inherited;
end;

end.
