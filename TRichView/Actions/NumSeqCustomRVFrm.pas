{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       A base form for numbering sequence insertion    }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit NumSeqCustomRVFrm;

{$I RichViewActions.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  RVStyle,
  BaseRVFrm, RVALocalize, RichViewActions;

type
  TfrmRVCustomNumSeq = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    gbSeq: TGroupBox;
    lblSeqName: TLabel;
    lblSeqType: TLabel;
    cmbSeqName: TComboBox;
    cmbSeqType: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure cmbSeqNameClick(Sender: TObject);
    procedure cmbSeqNameChange(Sender: TObject);
  private
    { Private declarations }
    _cmbSeqName, _cmbSeqType, _btnOk: TControl;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    procedure Localize; override;
    procedure Init(SeqPropList: TRVALocStringList; const SeqName:
      String; SeqType: TRVSeqType);
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    procedure GetSeqProperties(var SeqName: String; var SeqType: TRVSeqType);
  end;


implementation

{$R *.dfm}

{ TfrmRVCustomNumSeq }

procedure TfrmRVCustomNumSeq.FormCreate(Sender: TObject);
begin
  _cmbSeqName := cmbSeqName;
  _cmbSeqType := cmbSeqType;
  _btnOk      := btnOk;
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVCustomNumSeq.Init(SeqPropList: TRVALocStringList;
  const SeqName: String; SeqType: TRVSeqType);
begin
  if SeqPropList<>nil then
    SetXBoxItems(_cmbSeqName, SeqPropList);
  SetControlCaption(_cmbSeqName, SeqName);
  SetXBoxItemIndex(_cmbSeqType, ord(SeqType));
end;
{------------------------------------------------------------------------------}
function TfrmRVCustomNumSeq.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := gbSeq.Left*2+gbSeq.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gbSeq.Top-gbSeq.Height);
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVCustomNumSeq.Localize;
begin
  btnOk.Caption     := RVA_GetSAsIs(rvam_btn_Insert, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  Caption := RVA_GetS(rvam_in_Title, ControlPanel);
  gbSeq.Caption := RVA_GetSHAsIs(rvam_in_PropGB, ControlPanel);
  lblSeqName.Caption := RVA_GetSAsIs(rvam_in_CounterName, ControlPanel);
  lblSeqType.Caption := RVA_GetSAsIs(rvam_in_NumType, ControlPanel);  
end;
{------------------------------------------------------------------------------}
procedure TfrmRVCustomNumSeq.GetSeqProperties(var SeqName: String;
  var SeqType: TRVSeqType);
begin
  SeqType := TRVSeqType(GetXBoxItemIndex(_cmbSeqType));
  SeqName := Trim(GetEditText(_cmbSeqName));
end;
{------------------------------------------------------------------------------}
procedure TfrmRVCustomNumSeq.cmbSeqNameClick(Sender: TObject);
var Index: Integer;
begin
  Index := GetXBoxItemIndex(_cmbSeqName);
  if Index>=0 then
    SetXBoxItemIndex(_cmbSeqType, Integer(GetXBoxObject(_cmbSeqName, Index)));
end;
{------------------------------------------------------------------------------}
procedure TfrmRVCustomNumSeq.cmbSeqNameChange(Sender: TObject);
begin
  _btnOk.Enabled := RVAIsSeqNameAllowed(Trim(GetEditText(_cmbSeqName)));
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVCustomNumSeq.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl=_cmbSeqType then
    _cmbSeqType := NewControl
  else if OldControl=_cmbSeqName then
    _cmbSeqName := NewControl
  else if OldControl=_btnOk then
    _btnOk := NewControl
end;
{$ENDIF}


end.
