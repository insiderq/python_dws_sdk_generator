﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ItemCaptionRVFrm.pas' rev: 27.00 (Windows)

#ifndef ItemcaptionrvfrmHPP
#define ItemcaptionrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <NumSeqCustomRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Itemcaptionrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVItemCaption;
class PASCALIMPLEMENTATION TfrmRVItemCaption : public Numseqcustomrvfrm::TfrmRVCustomNumSeq
{
	typedef Numseqcustomrvfrm::TfrmRVCustomNumSeq inherited;
	
__published:
	Vcl::Stdctrls::TLabel* lblCaption;
	Vcl::Stdctrls::TEdit* txtCaption;
	Vcl::Extctrls::TRadioGroup* rgPosition;
	Vcl::Stdctrls::TCheckBox* cbExcludeLabel;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
public:
	Vcl::Controls::TControl* _txtCaption;
	Vcl::Controls::TControl* _rgPosition;
	Vcl::Controls::TControl* _cbExcludeLabel;
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	DYNAMIC void __fastcall Localize(void);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVItemCaption(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Numseqcustomrvfrm::TfrmRVCustomNumSeq(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVItemCaption(System::Classes::TComponent* AOwner, int Dummy) : Numseqcustomrvfrm::TfrmRVCustomNumSeq(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVItemCaption(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVItemCaption(HWND ParentWindow) : Numseqcustomrvfrm::TfrmRVCustomNumSeq(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Itemcaptionrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ITEMCAPTIONRVFRM)
using namespace Itemcaptionrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ItemcaptionrvfrmHPP
