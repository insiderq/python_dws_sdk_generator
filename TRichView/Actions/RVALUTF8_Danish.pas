﻿// This file is a copy of RVAL_Danish.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       English (US) translation                        }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Kurt Bilde (kub@sdu.dk) 2014-04-01     }
{*******************************************************}

unit RVALUTF8_Danish;

interface
{$I RV_Defs.inc}
implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'tomme', 'cm', 'mm', 'picas', 'pixels', 'punkter',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Filer', '&Rediger', 'F&ormatter', 'Skriftt&ype', '&Afsnit', '&Indsæt', '&Tabeller', '&Vindue', '&Hjælp',
  // exit
  'Afsl&ut',
  // top-level menus: View, Tools,
  '&Vis', 'Værktø&jer',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Størrelse', 'Stil', 'Væl&g', 'Juster cellei&ndhold', 'C&ellekanter',
  // menus: Table cell rotation
  'Celle &Rotation',
  // menus: Text flow, Footnotes/endnotes
  '&Tekstomløb', '&Fodnoter',
  // ribbon tabs: tab1, tab2, view, table
  '&Hjem', '&Advanceret', '&Vis', '&Tabel',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Udklipsholder', 'Skriftype', 'Afsnit', 'Liste', 'Redigering',
  // ribbon groups: insert, background, page setup,
  'Indsæt', 'Baggrund', 'Sideopsætning',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Links', 'Fodnoter', 'Dokumentvisning', 'Vis/skjul', 'Zoom', 'Valgmuligheder',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Indsæt', 'Slet', 'Valgmuligheder', 'Kanter',
  // ribbon groups: styles 
  'Stilarter',
  // ribbon screen tip footer,
  'Tryk F1 for mere hjælp',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Sidste dokumenter', 'Gem en kopi af dokumentet', 'Preview og udskriv dokumentet',
  // ribbon label: units combo
  'Units:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Ny', 'Ny|Laver et nyt tomt dokument',
  // TrvActionOpen
  '&Åbn...', 'Åbn|Åbner et dokument fra disken',
  // TrvActionSave
  '&Gem', 'Gem|Gemmer dokumentet på disken',
  // TrvActionSaveAs
  'Gem &som...', 'Gem som...|Gemmer dokumentet på disken med et nyt filnavn',
  // TrvActionExport
  '&Eksport...', 'Eksport|Eksporter dokumentet til et andet format',
  // TrvActionPrintPreview
  'Vis udsk&rift', 'Vis udskrift|Viser udskriften som det vil blive udskrevet',
  // TrvActionPrint
  '&Udskriv...', 'Udskriv|Skift printeropsætningen og udskriv dokumentet',
  // TrvActionQuickPrint
  '&Udskriv', '&Hurtig udskrift', 'Udskriv|Udskriver dokumentet',
  // TrvActionPageSetup
  'Si&deopsætning...', 'Sideopsætning|Sætter marginer, papirstørrelse, orientering, kilde, hoved og fødder',
  // TrvActionCut
  'Kli&p', 'Klip|Klipper det udvalgte og gemmer det på udklipsholderen',
  // TrvActionCopy
  '&Kopier', 'Kopier|Kopiere det valgte og gemmer det på udklipsholderen',
  // TrvActionPaste
  '&Sæt ind', 'Sæt ind|Indsætter indholdet fra udklipsholderen',
  // TrvActionPasteAsText
  'Indsæt som &tekst', 'Indsæt som tekst|Indsætter teksten fra udklipsholderen',  
  // TrvActionPasteSpecial
  'Indsæt &speciel...', 'Indsæt speciel|Indsætter indholdet fra udklipsholderen i et specielt format',
  // TrvActionSelectAll
  'Vælg &Alt', 'Vælg alt|Vælg alle dokumenter',
  // TrvActionUndo
  'Fortr&yd', 'Fortryd|Omgør den sidste handling',
  // TrvActionRedo
  '&Omgør', 'Omgør|Omgøre den sidste fortrudte handling',
  // TrvActionFind
  '&Find...', 'Find|Finder den angivne tekst i dokumentet',
  // TrvActionFindNext
  'Find &næste', 'Find næste|Fortsætter den sidste søgning',
  // TrvActionReplace
  'E&rstat...', 'Erstat|Finder og erstatter den angivne tekst i dokumentet',
  // TrvActionInsertFile
  '&Fil...', 'Indsæt Fil|Indsætter indholdet af filen i dokumentet',
  // TrvActionInsertPicture
  '&Billede...', 'Indsæt billede|Indsætter et billede fra disken',
  // TRVActionInsertHLine
  '&Horisontal Linie', 'Indsæt en Horisontal Linie|Indsætter en horisontal linie',
  // TRVActionInsertHyperlink
  'Hyper&link...', 'Indsæt Hyperlink|Indsætter et hypertekst link',
  // TRVActionRemoveHyperlinks
  'Fje&rn Hyperlinks', 'Fjern hyperlinks|Fjern alle hyperlinks i den valgte tekst',  
  // TrvActionInsertSymbol
  '&Symbol...', 'Indsæt Symbol|Indsætter symbol',
  // TrvActionInsertNumber
  '&Nummer...', 'Indsæt nummer|Indsætter et nummer',
  // TrvActionInsertCaption
  'Indsæt &overskrift...', 'Indsæt overskrift|Indsætter en overskrift for det valgte objekt',
  // TrvActionInsertTextBox
  '&Tekstboks', 'Indsæt en tekstboks|Indsætter en tekstboks',
  // TrvActionInsertSidenote
  '&Sidenote', 'Indsæt sidenote|Indsætter en note vist i en tekstboks',
  // TrvActionInsertPageNumber
  '&Sidenummer', 'Indsæt sidenummer|Indsætter et sidenummer',
  // TrvActionParaList
  '&Punkter og nummerering...', 'Punkter og nummerering|Tilføj eller rediger punkter eller nummerering for det valgte afsnit',
  // TrvActionParaBullets
  '&Punkter', 'Punkter|Tilføj eller fjern punkter for afsnittet',
  // TrvActionParaNumbering
  '&Nummerering', 'Nummerering|Tilføj eller fjern nummerering for afsnittet',
  // TrvActionColor
  'Baggrunds&farve...', 'Baggrund|Skift baggrundsfarven i dokumentet',
  // TrvActionFillColor
  'Ud&fyldningsfarve...', 'Udfyldningsfarven|Skift baggrundsfarven på tekst, afsnit, celler, tabeller eller dokument',
  // TrvActionInsertPageBreak
  '&Indsæt sideskifte', 'Indsæt sideskifte|Indsætter et sideskifte',
  // TrvActionRemovePageBreak
  '&Fjern sideskifte', 'Fjern sideskifte|Fjerner sideskifte',
  // TrvActionClearLeft
  'Ombryd tekstforløb på den &venstre side', 'Ombryd tekstforløb på venstre side|Placerer dette afsnit under enhver venstre-justeret billede',
  // TrvActionClearRight
  'Ombryd tekstforløb på den &højre side', 'Ombryd tekstforløb på den højre side|Placerer dette afsnit under enhver højre-justeret billede',
  // TrvActionClearBoth
  'Ombryd tekstforløb på &begge sider', 'Ombryd tekstforløb på den begge sider|Placerer dette afsnit under enhver højre eller venstre-justeret billede',
  // TrvActionClearNone
  '&Normalt tekstforløb', 'Normalt tekstforløb|Tillad tekstforløb rundt om både højre og venstre justeret billeder',
  // TrvActionVAlign
  '&Objekt position...', 'Objekt position|Skifter positionen på det valgte objekt',  
  // TrvActionItemProperties
  'Objekt&egenskaber...', 'Objektegenskaber|Definere egenskaberne på det aktive objekt',
  // TrvActionBackground
  '&Baggrund...', 'Baggrund|Vælger baggrundsfarve og billede',
  // TrvActionParagraph
  '&Afsnit...', 'Afsnit|Skifter attributterne for afsnittet',
  // TrvActionIndentInc
  '&Forøg indrykning', 'Forøg indrykning|Forøger indrykningen på det valgte afsnit',
  // TrvActionIndentDec
  'F&ormindsk indrykningen', 'Formindsk indrykningen|Formindsk indrykningen på det valgte afsnit',
  // TrvActionWordWrap
  '&Teksombrydningen', 'Tekstombrydning|Skifter mellem tekstombrydningen i det valgte afsnit',
  // TrvActionAlignLeft
  '&Venstrejustering', 'Venstrejustering|Justerer den valgte tekste til venstre',
  // TrvActionAlignRight
  '&Højrejustering', 'Højrejustering|Justerer den valgte tekst til højre',
  // TrvActionAlignCenter
  '&Centreret', 'Centreret|Justerer den valgte tekst til centreret',
  // TrvActionAlignJustify
  '&Juster', 'Juster|Fordeler teksten ligeligt mellem både højre og venstre side',
  // TrvActionParaColor
  '&Baggrundsfarve for afsnit...', 'Baggrundsfarve for afsnit|Sætter baggrundsfarven for afsnittet',
  // TrvActionLineSpacing100
  '&Enkelt linjeafstand', 'Enkelt linjeafstand|Sætter enkelt linie afstand',
  // TrvActionLineSpacing150
  '1.5 li&njeafstand', '1.5 linjeafstand|Sætter linjeafstanden til 1.5 linie',
  // TrvActionLineSpacing200
  '&Dobbelt linjeafstand', 'Dobbelt linjeafstand|Sætter dobbelt linie afstand',
  // TrvActionParaBorder
  '&Kanter og baggrund for afsnit...', 'Kanter og baggrund for afsnit|Sætter kanter og baggrund for det valgte afsnit',
  // TrvActionInsertTable
  '&Indsæt tabel...', '&tabel', 'Indsæt tabel|Indsætter en ny tabel',
  // TrvActionTableInsertRowsAbove
  'Indsæt række &over', 'Indsæt række over|Indsætter en ny række over den valgte celle',
  // TrvActionTableInsertRowsBelow
  'Indsæt række &under', 'Indsæt række under|Indsætter en ny række under den valgte celle',
  // TrvActionTableInsertColLeft
  'Indsæt kolonne til &venstre', 'Indsæt kolonne til venstre for|Indsætter en ny kolonne til venstre for den valgte celle',
  // TrvActionTableInsertColRight
  'Indsæt kolonne til &højre', 'Indsæt kolonne til højre for|Indsætter en ny kolonne til højre for den valgte celle',
    // TrvActionTableDeleteRows
  'Slet ræk&ke', 'Slet rækker|Sletter rækker i de valgte celler',
  // TrvActionTableDeleteCols
  'Slet &kolonner', 'Slet kolonner|Slet kolonnerne i de valgte celler',
  // TrvActionTableDeleteTable
  'S&let tabel', 'Slet tabel|Sletter tabellen',
  // TrvActionTableMergeCells
  '&Sammensæt celler', 'Sammensæt celler|Sammensæt de valgte celler',
  // TrvActionTableSplitCells
  'Op&del celler...', 'Opdel celler|Opdel de valgte celler',
  // TrvActionTableSelectTable
  'Vælg &tabel', 'Vælg tabel|Vælg tabel',
  // TrvActionTableSelectRows
  'Vælg Ræ&kker', 'Vælg rækker|Vælg rækker',
  // TrvActionTableSelectCols
  'Vælg kol&onner', 'Vælg kolonner|Vælg kolonner',
  // TrvActionTableSelectCell
  'Vælg C&elle', 'Vælg Celle|Vælg celle',
  // TrvActionTableCellVAlignTop
  'Juster celle til &top', 'Juster celle til top|Juster celleindhold til top',
  // TrvActionTableCellVAlignMiddle
  'Juster celle til &midten', 'Juster celle til midten|Juster celleindholdet til midten',
  // TrvActionTableCellVAlignBottom
  'Juster celle til &bund', 'Juster celle til bund|Juster celleindhold til bund',
  // TrvActionTableCellVAlignDefault
  '&Standard celle vertikale justering', 'Standard celle vertikale justering|Sætter standard vertikale justering for den valgte celle',
  // TrvActionTableCellRotationNone
  '&Ingen cellerotation', 'Ingen Cellerotation|Rotér celleindhold med 0°',
  // TrvActionTableCellRotation90
  'Roter celle med &90°', 'Roter celle med 90°|Rotér celleindhold med 90°',
  // TrvActionTableCellRotation180
  'Roter celle med &180°', 'Roter celle med 180°|Rotér celleindhold med 180°',
  // TrvActionTableCellRotation270
  'Roter celle med &270°', 'Roter celle med 270°|Rotér celleindhold med 270°',
  // TrvActionTableProperties
  'Tabel&egenskaber...', 'Tabelegenskaber|Skift tabelegenskaber for den valgte tabel',
  // TrvActionTableGrid
  'Vis &gitterlinjer', 'Vis gitterlinjer|Vis eller skjul tabellens gitterlinjer',
  // TRVActionTableSplit
  'Opde&l Tabel', 'Opdel tabel|Opdeler tabellen i to tabeller startende med den valgte række',
  // TRVActionTableToText
  'Konverter til Tek&st...', 'Konverter til tekst|Konverter tabellen til tekst',
  // TRVActionTableSort
  '&Sorter...', 'Sorter|Sorter tabelrækkerne',
  // TrvActionTableCellLeftBorder
  '&Venstrekant', 'Venstrekant|Vis eller skjul venstre kant i celler',
  // TrvActionTableCellRightBorder
  '&Højrekant', 'Højrekant|Vis eller skjul højre kant i celle',
  // TrvActionTableCellTopBorder
  '&Topkant', 'Topkant|Vis eller skjul topkant i celle',
  // TrvActionTableCellBottomBorder
  '&Bundkant', 'Bundkant|Vis eller skjul bundkant i celle',
  // TrvActionTableCellAllBorders
  '&Alle kanter', 'Alle kanter|Vis eller skjul alle cellekanter',
  // TrvActionTableCellNoBorders
  '&Ingen kanter', 'Ingen kanter|Skjul alle cellekanter',
  // TrvActionFonts & TrvActionFontEx
  '&Skrifttype...', 'Skrifttype|Skift skrifttypen på den valgte tekst',
  // TrvActionFontBold
  '&Fed', 'Fed|Skift stilarten på den valgte tekst til fed',
  // TrvActionFontItalic
  '&Kursiv', 'Kursiv|Skift stilarten på den valgte tekst til kursiv',
  // TrvActionFontUnderline
  '&Understreg', 'Understreg|Skift til understregning for den valgte tekst',
  // TrvActionFontStrikeout
  '&Overstreg', 'Overstreg|Overstreg den valgte tekst',
  // TrvActionFontGrow
  '&Forøg skrifttypen', 'Forøg skrifttypen|Forøger skrifttypens højde i den valgte tekst med 10%',
  // TrvActionFontShrink
  'Formin&dsk skrifttype', 'Formindsk skrifttype|Formindsker skrifttypens højden i den valgte tekst med 10%',
  // TrvActionFontGrowOnePoint
  'Fo&røg skrifttypen med et punkt', 'Forøg skrifttypen med et punkt|Forøger skrifttypen på den valgte tekst med et punkt',
  // TrvActionFontShrinkOnePoint
  'Formi&ndsk skrifttype med et punkt', 'Formindsk skrifttype med et punkt|Formindsker skrifttypen med et punkt',
  // TrvActionFontAllCaps
  '&Alt med store bogstaver', 'Alt med store bogstaver|Skift alle tegn til store bogstaver i den valgte tekst',
  // TrvActionFontOverline
  'Linie &over', 'Linie over|Tilføj en linie over den valgte tekst',
  // TrvActionFontColor
  'Tekst&farve...', 'Tekstfarve|Skift tekstfarve på den valgte tekst',
  // TrvActionFontBackColor
  'Tekstbag&grundsfarve...', 'Tekstbaggrundsfarve|Skift baggrundsfarven for den valgte tekst',
  // TrvActionAddictSpell3
  '&Stavekontrol', 'Stavekontrol|Tjek stavningen',
  // TrvActionAddictThesaurus3
  '&Synonymordbog', 'Synonymordbog|Viser synonymer for det valgte ord',
  // TrvActionParaLTR
  'Venstre til højre', 'Venstre til højre|Sætter retningen til Venstre til højre for det valgte afsnit',
  // TrvActionParaRTL
  'Højre til venstre', 'Højre til venstre|Sætter højre til venstre tekstretning for det valgte afsnit',
  // TrvActionLTR
  'Venstre til højre tekst', 'Venstre til højre tekst|Sætter venstre til højre retningen for den valgte tekst',
  // TrvActionRTL
  'Højre til venstre tekst', 'Højre til venstre tekst|Sætter højre til venstre retning for den valgte tekst',
  // TrvActionCharCase
  'Tegntype', 'Tegntype|Sætter tegntypen (versaler/ikke vasaler) for den valgte tekst',
  // TrvActionShowSpecialCharacters
  '&Ikke -udskriftsbare tegn', 'Ikke-udskriftsbare tegn|Viser eller skjuler ikke-udskriftsbare tegn, så som afsnitsmarkører, tabulatorer og mellemrum',
  // TrvActionSubscript
  'Su&pscript', 'Supscript|Konverter den valgte tekst til supscript',
  // TrvActionSuperscript
  'Superscript', 'Superscript|Konvertere den valgte tekst til superscript',
  // TrvActionInsertFootnote
  '&Fodnote', 'Fodnote|Indsætter en fodnote',
  // TrvActionInsertEndnote
  '&Slutnote', 'Slutnote|Indsætter en slutnote',
  // TrvActionEditNote
  'Re&diger note', 'Rediger note|Påbegynder redigering af fod- eller slutnote',
  // TrvActionHide
  '&Skjul', 'Skjul|Skjuler eller viser det valgte fragment',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stilart...', 'Stilarter|Åbner dialogboksen med stilarter',
  // TrvActionAddStyleTemplate
  '&Tilføj stilart...', 'Tilføj stilart|Laver en ny tekst eller afsnitsstil baseret på det valgte fragment',
  // TrvActionClearFormat,
  '&Rens format', 'Rens format|Renser alt tekst og afsnitsformattering fra det valgte fragment',
  // TrvActionClearTextFormat,
  'Rens &tekstformat', 'Rens tekstformat|Renser alle formateringer fra den valgte tekst',
  // TrvActionStyleInspector
  'Stilarts&inspektør', 'Stilartsinspektør|Viser eller skjuler stilartsinspektøren',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Fortryd', 'Luk', 'Indsæt', 'Å&bn...', '&Gem...', '&Rens', 'Hjælp', 'Fjern',
  // Others  -------------------------------------------------------------------
  // percents
  'procent',
  // left, top, right, bottom sides
  'Venstreside', 'Topside', 'Højreside', 'Bundside',
  // save changes? confirm title
  'Gem ændringer til %s?', 'Bekræft',
  // warning: losing formatting
  '%s kan indeholde elementer der ikke er kompatible med det valgte format.'#13+
  'Ønsker du at gemme dokumentet i dette format?',
  // RVF format name
  'RichView Format',
  // Error messages ------------------------------------------------------------
  'Fejl',
  'Fejl ved indlæsningen af fil.'#13#13'Mulig årsag:'#13'- formatet af denne fil er ikke understøttet af denne apllikation;'#13+
  '- filen er beskadiget;'#13'- filen er åbnet og låst af en anden applikation.',
  'Fejl ved indlæsningen af billedefil.'#13#13'Mulig årsag:'#13'- filen indeholder et billede som ikke er understøttet af denne applikation;'#13+
  '- filen indeholder ikke t billede;'#13+
  '- filen er beskadiget;'#13'- filen er åbnet og låst af en anden applikation.',
  'Fejl ved skrivning af fil.'#13#13'Mulig årsag:'#13'- ikke mere diskplads;'#13+
  '- disk er skrivebesknyttet;'#13'- skrivebar medie er ikke tilgængeligt;'#13+
  '- filen er åbnet og låst af en anden applikation;'#13'- diskmediet er beskadiget.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Filer (*.rvf)|*.rvf',  'RTF Filer (*.rtf)|*.rtf' , 'XML Filer (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Tekstfiler (*.txt)|*.txt', 'Tekstfiler - Unicode (*.txt)|*.txt', 'Tekstfiler - Auto (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Filer (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplificeret (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word dokumenter (*.docx)|*.docx',  
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Søgning fuldført', 'Søgestreng ''%s'' blev ikke fundet.',
  // 1 string replaced; Several strings replaced
  '1 streng ombyttet.', '%d strenge ombyttet.',
  // continue search from the beginning/end?
  'Slutningen på dokumentet blev nået, fortsæt fra starten?',
  'Starten på dokumentet blev nået, fortsæt fra slutningen?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Sort', 'Brun', 'Olivengrøn', 'Mørkegrøn', 'Mørk grønblå', 'Mørkeblå', 'Indigo', 'Grå-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Mørkerød', 'Orange', 'Mørkegul', 'Grøn', 'Grønblå', 'Blå', 'Blå-Grå', 'Grå-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rød', 'Lys orange', 'Lime', 'Søgrøn', 'Vandblå', 'Lyseblå', 'Violet', 'Grå-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Lyserød', 'Guld', 'Gul', 'Klar grøn', 'Turkis', 'Himmelblå', 'Blommefarvet', 'Grå-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Solbrun', 'Lysegul', 'Lysegrøn', 'lyseturkis', 'Svag blå', 'Lavendel', 'Hvid',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparent', '&Auto', '&Flere farver...', '&Standard',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Baggrund', 'Far&ve:', 'Position', 'Baggrund', 'Tekstprøve.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Ingen', 'Udst&rukket', 'F&aste fliser', '&Fliser', 'C&entreret',
  // Padding button
  'F&yld...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Udfyldningsfarve', '&Føj til:', '&Flere farver...', 'F&yld...',
  'Vælg venligst en farve',
  // [apply to:] text, paragraph, table, cell
  'tekst', 'afsnit' , 'tabel', 'celle',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Skrifttype', 'Skrifttype', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Sk&rifttype:', '&Størrelse', 'Stilart', '&Fed', '&Kursiv',
  // Script, Color, Back color labels, Default charset
  'S&cript:', '&Farve:', 'Bag&grund:', '(Standard)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekter', 'Understreg', '&Overstreg', 'Genne&mstegning', '&Alt store bogstaver',
  // Sample, Sample text
  'Prøve', 'Tekstprøve',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Tegn afstand', '&Afstand:', '&Udvidet', '&Sammenpresset',
  // Offset group-box, Offset label, Down, Up,
  'Vertikal offset', '&Offset:', '&Ned', '&Op',
  // Scaling group-box, Scaling
  'Skallering', 'Sk&allering:',
  // Sub/super script group box, Normal, Sub, Super
  'Supscript og superscript', '&Normal', 'Su&pscript', 'Su&perscript',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Fyld', '&Top:', '&Venstre:', '&Bund:', '&Højre:', '&Identiske værdier',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Indsæt hyperlink', 'hyperlink', 'T&ekst:', '&Mål', '<<valg>>',
  // cannot open URL
  'Kan ikke gå til "%s"',
  // hyperlink properties button, hyperlink style
  '&Brugervalg...', '&Stilart:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) Colors
  'Hyperlink attributter', 'Normale farver', 'Aktive farver',
  // Text [color], Background [color]
  '&Tekst:', '&Baggrund',
  // Underline [color]
  'U&nderstregning:',
  // Text [color], Background [color] (different hotkeys)
  'T&ekst:', 'B&aggrund',
  // Underline [color], Attributes group-box,
  'Un&derstregning:', 'Attributter',
  // Underline type: always, never, active (below the mouse)
  '&Understregning:', 'altid', 'aldrig', 'aktive',
  // Same as normal check-box
  'Som &normalt',
  // underline active links check-box
  '&Understreg aktive links',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Indsæt symbol', '&Skrifttype:', '&Tegnsæt:', 'Unicode',
  // Unicode block
  '&Blok:',  
  // Character Code, Character Unicode Code, No Character
  'Tegnsætskode: %d', 'Tegnsætskode: Unicode %d', '(ingen tegn)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Indsæt tabel', 'Tabelstørrelse', 'Antal &kolonner:', 'Antal &rækker:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabellayout', '&Autostørrelse', 'Tilpas &vindue', 'Br&ugervalgt størrelse',
  // Remember check-box
  'Husk &dimensioner for nye tabeller',
  // VAlign Form ---------------------------------------------------------------
  'Position',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Egenskaber', 'Billede', 'Position og størrelse', 'Linie', 'Tabel', 'Rækker', 'Celler',
  // Image Appearance, Image Misc, Number tabs
  'Udseende', 'Diverse', 'Nummer',
  // Box position, Box size, Box appearance tabs
  'Position', 'Størrelse', 'Udseende',
  // Preview label, Transparency group-box, checkbox, label
  'Preview:', 'Transparens', '&Transparent', 'Transparent &farve:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Sk&ift...', '&Gem...', 'Automatisk',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikal justering', '&Juster:',
  'bund til basislinie af tekst', 'Midten til basislinie af teksten',
  'top til linies top', 'bund til linies bund', 'Midten til linies midte',
  // align to left side, align to right side
  'til venstre side', 'til højre side',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Flyt med:', 'Udstræk', '&Bredde:', '&Højde:', 'Standard størrelse: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Skaller &proportionelt', '&Nulstil',
  // Inside the border, border, outside the border groupboxes
  'Inden i rammen', 'Ramme', 'Udenpå rammen',
  // Fill color, Padding (i.e. margins inside border) labels
  'Ud&fyldningsfarve:', 'F&yld:',
  // Border width, Border color labels
  'Ramme&bredde:', 'Ramme&farve:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horisontal afstand:', '&Vertikal afstand:',
  // Miscellaneous groupbox, Tooltip label
  'Diverse', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Alternativ tekst:',
  // Horz line group-box, color, width, style
  'Horisontal linie', '&Farve:', '&Bredde:', '&Stilart:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabel', '&Bredde:', 'Ud&fydningsfarve:', 'Celle &afstand...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'H&orisontal cellefyld:', '&Vertikal cellefyld:', 'Ramme og baggrund',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Synlige &kantsider...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Hold indhold sammen',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotation', '&Ingen', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Kanter:', 'Synlige ka&ntsider...',
  // Border, CellBorders buttons
  '&Tabelkanter...', '&Cellekanter...',
  // Table border, Cell borders dialog titles
  'Tabelkanter', 'Standard cellekanter',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Udskriver', 'Tilla&d ikke kolonner at blive brudt mellem sider', 'Antal o&verskriftsrækker:',
  'Overskriftsrækker bliver gentaget øverst på hver side',
  // top, center, bottom, default
  '&Top', '&Center', '&Bund', '&Standard',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Opsætning', 'Foretrukket br&edde:', '&Højde mindst:', 'Ud&Fyldningsfarve:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kant', 'Synlige sider:',  'Skyggefa&rve:', '&Lys farve', 'Far&ve:',
  // Background image
  'B&illede...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horisontal position', 'Vertikal position', 'Position i teksten',
  // Position types: align, absolute, relative
  'Justering', 'Absolut position', 'Relativ position',
  // [Align] left side, center, right side
  'venstre side af boksen til venstre side af',
  'boksens center, til center af',
  'højre side af boksen til højre side af',
  // [Align] top side, center, bottom side
  'toppen af boksen til toppen af',
  'center af boksen til center af',
  'bunden af boksen til bunden af',
  // [Align] relative to
  'relativ til',
  // [Position] to the right of the left side of
  'til højre for den venstre side af',
  // [Position] below the top side of
  'under topen af',
  // Anchors: page, main text area (x2)
  '&Side', '&Hovedtekstareal', 'S&ide', 'Hovedte&kstareal',
  // Anchors: left margin, right margin
  '&venstre margin', '&Højre margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Top margin', '&Bund margin', '&Indre margin', '&Ydre margin',
  // Anchors: character, line, paragraph
  '&Tegn', 'L&inje', 'A&fsnit',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Spejl&ede marginer', 'La&yout for tabelcellerne',
  // Above text, below text
  '&Over tekst', '&Under tekst',
  // Width, Height groupboxes, Width, Height labels
  'Bredde', 'Højde', '&Bredde:', '&Højde:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikal justering', '&Top', '&Center', '&Bund',
  // Border and background button and title
  'Ka&nter og baggrund...', 'Kanter og baggrund for boks',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Indsæt speciel', '&Indsæt som:', 'Rich tekst format', 'HTML format',
  'Tekst', 'Unicode tekst', 'Bitmap billede', 'Metafil billede',
  'Grafik filer', 'URL',
  // Options group-box, styles
  'Valgmuligheder', '&Stilarter:',
  // style options: apply target styles, use source styles, ignore styles
  'tilføj stilart på det valgte dokument', 'behold stilarter og udseende',
  'behold udseende og ignorer stilarter',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Punkter og nummerering', 'Punktopstilling', 'Nummerering', 'Punktliste', 'Liste med tal',
  // Customize, Reset, None
  '&Brugervalgt...', '&Nulstil', 'Ingen',
  // Numbering: continue, reset to, create new
  'fortsæt nummerering', 'nulstil nummerering til', 'lav en ny liste, der starter med',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Store bogstaver', 'Store romertal', 'Decimaltal', 'Små bogstaver', 'Små romertal',
  // Bullet type
  'Cirkel', 'Disc', 'Firkant',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Niveau:', '&Start med:', '&Fortsæt', 'Nummerering',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Brugervalgt liste', 'Niveauer', '&Antal:', 'Egenskaber for liste', '&Listetype:',
  // List types: bullet, image
  'punkt', 'billede', 'Indsæt nummer|',
  // Number format, Number, Start level from, Font button
  '&Nummerformat:', 'Nummer', '&Start nummereringsniveauet med:', 'Skri&fttype...',
  // Image button, bullet character, Bullet button,
  '&Billede...', 'Punktte&gn', '&Punkt...',
  // Position of list text, bullet, number, image, text
  'Listens tekstposition', 'Punkttegnposition', 'Nummereringsposition', 'Billedeposition', 'Tekstposition',
  // at, left indent, first line indent, from left indent
  '&ved:', 'V&enstre indrykning:', '&Føste linjeindrykning:', 'fra venstre indrykning',
  // [only] one level preview, preview
  '&Første niveau preview', 'Preview',
  // Preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'juster venstre', 'juster højre', 'centreret',
  // level #, this level (for combo-box)
  'Niveau %d', 'Dette niveau',
  // Marker character dialog title
  'Rediger punkttegn',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Afsnitskant og baggrund', 'Kant', 'Baggrund',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Opsætning', '&Farve:', '&Bredde:', 'Inte&rn bredde:', 'Tekst&marginer...',
  // Sample, Border type group-boxes;
  'Prøve', 'kanttype',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ingen', '&Enkel', '&Dobbelt', '&Triple', 'Tyk &indeni', 'Tyk &udenpå',
  // Fill color group-box; More colors, padding buttons
  'Udfyldningsfarve', '&Flere farver...', 'F&yld...',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // title and group-box in Offsets dialog
  'Tekstmarginer', 'Udgangspunkt for kanter',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Afsnit', 'Justering', '&Venstre', '&Højre', '&Centreret', '&Justeret',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Afstand', '&Før:', '&Efter:', 'Linie&afstand:', 've&d:',
  // Indents group-box; indents: left, right, first line
  'Indrykning', 'V&enstre:', 'Hø&jre:', '&Første linie:',
  // indented, hanging
  '&Indrykket', '&Hængende', 'Prøve',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Enkel', '1.5 linje', 'Dobbelt', 'Mindst', 'Præcist', 'Flere',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Indrykninger og afstand', 'Tabulatorer', 'Tekst forløb',
  // tab stop position label; buttons: set, delete, delete all
  '&Tab stop position:', '&Sæt', 'S&let', 'Slet &alle',
  // tab align group; left, right, center aligns,
  'Justering', '&Venstre', '&Højre', '&Centreret',
  // leader radio-group; no leader
  'Leder', '(Ingen)',
  // tab stops to be deleted, delete none, delete all labels
  'Tab stop som skal slettes:', '(Ingen)', 'Alle.',
  // Pagination group-box, keep with next, keep lines together
  'Paginering', '&Hold sammen med næste', 'H&old linier sammen',
  // Outline level, Body text, Level #
  '&Omridsniveau:', 'Brødtekst', 'Niveau %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Udskrift preview', 'Sidebredde', 'Fuld side', 'Sider:',
  // Invalid Scale, [page #] of #
  'Angiv venligst et tal mellem 10 og 500', 'af %d',
  // Hints on buttons: first, prior, next, last pages
  'Første side', 'Forgående side', 'Næste side', 'Sidste side',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Celleafstand', 'Afstand', 'Mellem &celler', 'Mellem tabelkant og celler',
  // vertical, horizontal (x2)
  '&Vertikal:', '&Horisontal:', 'Ve&rtikal:', 'H&orisontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kanter', 'Opsætning', '&Farve:', '&Lyse farver:', 'Skyggefa&rve:',
  // Width; Border type group-box;
  '&Bredde:', 'Kanttype',
  // Border types: none, sunken, raised, flat
  '&Ingen', '&Sænket', '&Ophævet', '&Flad',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Del', 'Del til',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Angiv antal rækker og kolonner', '&Originale celler',
  // number of columns, rows, merge before
  'Antal &kolonner:', 'Antal &rækker:', 'Sa&mmensæt før deling',
  // to original cols, rows check-boxes
  'Del til de originale ko&lonner', 'Del til de originale ræk&ker',
  // Add Rows form -------------------------------------------------------------
  'Tilføj rækker', 'Antal &rækker :',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Sideopsætning', 'Side', 'Sidehoved og fod',
  // margins group-box, left, right, top, bottom
  'Marginer (millimeter)', 'Marginer (tommer)', 'Ven&stre:', '&Top:', '&Højre:', '&Bund:',
  // mirror margins check-box
  '&Spejl marginer',
  // orientation group-box, portrait, landscape
  'Retning', '&Portrat', 'L&andskab',
  // paper group-box, paper size, default paper source
  'Papir', 'Stør&relse:', '&Kilde:',
  // header group-box, print on the first page, font button
  'Sidehoved', '&Tekst:', 'Side&hoved på den første side', '&Skrifttype...',
  // header alignments: left, center, right
  '&Venstre', '&Centreret', '&Højre',
  // the same for footer (different hotkeys)
  'Sidefod', 'Te&kst:', 'Sidefod på den første &side', 'S&krifttype...',
  'V&enstre', 'Ce&ntreret', 'Hø&jre',
  // page numbers group-box, start from
  'Sidenummer', '&Start fra:',
  // hint about codes
  'Special tegnkombination:'#13'&&p - Sidenummer; &&P - antal sider; &&d - aktuel dato; &&t - aktuel tid.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Tekstfil Code Page', '&Vælg fil encoding:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japansk', 'Kinesisk (Simplificeret)', 'Koreansk',
  // chinese (trad), central european, cyrillic, west european
  'Kinesisk (Traditionel)', 'Central og østeuropa', 'Kyrillisk', 'Vesteuropæisk',
  // greek, turkish, hebrew, arabic
  'Græsk', 'Tyrkisk', 'Hebraisk', 'Arabisk',
  // baltic, vietnamese, utf-8, utf-16
  'Baltisk', 'Vietnamesisk', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Visuelstilart', '&Vælg stilark:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Konverter til tekst', 'Vælg a&dskiller:',
  // line break, tab, ';', ','
  'linjebrydning', 'tab', 'semikolon', 'komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'En tabel der indeholder sammensatte rækker kan ikke blive sorteret',
  // title, main options groupbox
  'Tabelsortering', 'Sortering',
  // sort by column, case sensitive
  '&Sortering efter kolonne:', '&Versalfølsom',
  // heading row, range or rows
  'Udeluk &overskriftrækken', 'Rækker at sortere: fra %d til %d',
  // order, ascending, descending
  'Orden', '&Stigende', '&Faldende',
  // data type, text, number
  'Type', '&Tekst', '&Nummer',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Indsæt nummer', 'Egenskaber', '&Tællernavn:', '&Nummereringstype:',
  // numbering groupbox, continue, start from
  'Nummerering', 'F&ortsæt', '&Start fra:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Overskrift', '&Label:', '&Udeluk label fra overskrift',
  // position radiogroup
  'Position', '&Over det valgte objekt', '&Under det valgte objekt',
  // caption text
  'Overskrifts&tekst:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Nummerering', 'Figur', 'Tabel',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Synlige kantsider', 'Kanter',
  // Left, Top, Right, Bottom checkboxes
  '&Venstre side', '&Top side', '&Højre side', '&Bund side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Tilpas tabelkolonne', 'Tilpas tabelrække',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Første linie indrykket', 'Venstre indrykning', 'Hængende indrykning', 'Højre indrykning',
  // Hints on lists: up one level (left), down one level (right)
  'Fremryk et niveau', 'Fjern et niveau',
  // Hints for margins: bottom, left, right and top
  'Bundmargin', 'Venstremargin', 'Højremargin', 'Topmargin',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Venstre justeret tab', 'Højrejusteret tab', 'Centreret justeret tab', 'Decimal justeret tab',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normal indrykning', 'Ingen mellemrum', 'Overskrift %d', 'Listeafsnit',
  // Hyperlink, Title, Subtitle
  'Hyperlink', 'Titel', 'Undertitel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Fremhæv', 'Fin fremhævelse', 'Intens fremhævelse', 'Fed',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citat', 'Intens citat', 'Fin reference', 'Intens reference',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Tekstblok', 'HTML variable', 'HTML kode', 'HTML akronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML definition', 'HTML tastatur', 'HTML prøve', 'HTML skrivemaskine',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML preformateret', 'HTML citat', 'Hoved', 'Fod', 'Sidenummer',
  // Caption
  'Overskrift',  
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Slutnotereference', 'Fodnotereference', 'Slutnotetekst', 'Fodnotetekst',
  // Sidenote Reference, Sidenote Text
  'Sidenotereference', 'Sidenotetekst',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'farve', 'baggrundsfarve', 'transparent', 'standard', 'farve for understregning',
  // default background color, default text color, [color] same as text
  'standard baggrundsfarve', 'standard tekstfarve', 'samme som tekst',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'single', 'tyk', 'dobbelt', 'prikkede', 'tyk prikkede', 'stiplede',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tyk stipled', 'lange stiplet', 'tyk langt stiplet',
  // underline types: dash dotted, thick dash dotted,
  'stiplet prikket', 'tyk stiplet prikket',
  // underline types: dash dot dotted, thick dash dot dotted
  'stiplet prik prikket', 'tyk prik prikket',
  // sub/superscript: not, subsript, superscript
  'ikke sænket/hævet skrift', 'sænket skrift', 'hævet skrift',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mode:', 'nedarvet', 'venstre til højre', 'højre til venstre',
  // bold, not bold
  'fed', 'ikke fed',
  // italic, not italic
  'kursiv', 'ikke kursiv',
  // underlined, not underlined, default underline
  'understreget', 'ikke understreget', 'standard understreget',
  // struck out, not struck out
  'overstreget', 'ikke overstreget',
  // overlined, not overlined
  'linie over', 'ikke linie over',
  // all capitals: yes, all capitals: no
  'alt store bogstaver', 'alt store bogstaver slået fra',
  // vertical shift: none, by x% up, by x% down
  'uden vertikal skub', 'skubbet med %d%% opad', 'skubbet med %d%% nedad',
  // characters width [: x%]
  'tegnbredde',
  // character spacing: none, expanded by x, condensed by x
  'normal tegnafstand', 'afstand udvidet med %s', 'afstand sammentrukket med %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'standard skrifttype', 'standard afsnit', 'nedarvet',
  // [hyperlink] highlight, default hyperlink attributes
  'fremhævet', 'standard',
  // paragraph aligmnment: left, right, center, justify
  'venstrejusteret', 'højrejusteret', 'centreret', 'justeret',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'linjehøjde: %d%%', 'afstand mellem linier: %s', 'linjehøjde: mindst %s',
  'linjehøjde: præcist %s',
  // no wrap, wrap
   'linjeombrydning slået fra', 'linjeombrydning',
  // keep lines together: yes, no
  'hold linjer sammen', 'hold ikke linjer sammen',
  // keep with next: yes, no
  'hold sammen med næste afsnit', 'hold ikke sammen med næste afsnit',
  // read only: yes, no
  'skrivebeskyttet', 'redigerbar',
  // body text, heading level x
  'niveau omrids: brødtekst', 'niveau omrids: %d',
  // indents: first line, left, right
  'indryk første linie', 'indryk venstre', 'indryk højre',
  // space before, after
  'plads før', 'plads efter',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulator', 'ingen', 'justering', 'venstre', 'højre', 'centreret',
  // tab leader (filling character)
  'leder',
  // no, yes
  'nej', 'ja',
  // [padding/spacing/side:] left, top, right, bottom
  'venstre', 'top', 'højre', 'bund',
  // border: none, single, double, triple, thick inside, thick outside
  'ingen', 'enkel', 'dobbelt', 'tripel', 'tyk indeni', 'tyk udenpå',
  // background, border, padding, spacing [between text and border]
  'baggrund', 'kanter', 'fyld', 'afstand',
  // border: style, width, internal width, visible sides
  'stil', 'bredde', 'internt bredde', 'synlige sider',
  // style inspector -----------------------------------------------------------
  // title
  'Stilartsinspektør',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Ingen)', 'Afsnit', 'Skrifttype', 'Attributter',
  // border and background, hyperlink, standard [style]
  'Kanter og baggrund', 'Hyperlink', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stilart', 'Stilart',
  // name, applicable to,
  '&Navn:', 'anvendelig &på:',
  // based on, based on (no &),
  '&Baseret på:', 'Baseret på:',
  //  next style, next style (no &)
  'Stilart for &følgende afsnit:', 'Stilart for følgende afsnit:',
  // quick access check-box, description and preview
  '&Hurtig adgang', 'Beskrivelse and &preview:',
  // [applicable to:] paragraph and text, paragraph, text
  'afsnit og tekst', 'afsnit', 'tekst',
  // text and paragraph styles, default font
  'Tekst- og afsnits-stilarter', 'Standard skrifttype',
  // links: edit, reset, buttons: add, delete
  'Editer', 'Reset', '&Tilføj', '&Slet',
  // add standard style, add custom style, default style name
  'Tilføj &standard stilart...', 'Tilføj &brugerdefineret stilart', 'Stilart %d',
  // choose style
  'Vælg &stilart:',
  // import, export,
  '&Importer...', '&Eksporter...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView stilarter (*.rvst)|*.rvst', 'Der opstod en fejl ved hentning af fil', 'Denne fil indeholder ingen stilarter',
  // Title, group-box, import styles
  'Importer stilarter fra fil', 'Importer', 'I&mporter stilarter:',
  // existing styles radio-group: Title, override, auto-rename
  'Eksisterende stilarter', '&overskriv', 't&ilføj omdøbte',
  // Select, Unselect, Invert,
  '&Vælg', '&Fravælg', 'Ve&nd om',
  // select/unselect: all styles, new styles, existing styles
  '&Alle Stilarter', '&Ny stilarter', '&Eksisterende stilarter',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Rens format', 'Alle stilarter',
  // dialog title, prompt
  'Stilart', '&Vælg en stilart som skal anvendes:',    
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonymer', '&Ignorer alle', 'Til&føj til ordbogen',
  // Progress messages ---------------------------------------------------------
  'Downloader %s', 'Forbereder udskrift...', 'Udskriver side %d',
  'Konverterer fra Rich tekst format...',  'Konverterer til Rich tekst format...',
  'Læser %s...', 'Skriver %s...', 'tekst',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ingen Note', 'Fodnote', 'Slutnote', 'Sidenote', 'Tekstboks'
  );


initialization
  RVA_RegisterLanguage(
    'Danish',
    'Dansk',
    ANSI_CHARSET, @Messages);

end.