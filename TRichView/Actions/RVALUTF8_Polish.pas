﻿// This file is a copy of RVAL_Polish.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Polish translation                              }
{                                                       }
{       Copyright (c) 2002-2003, Sergey Tkachenko       }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated: 2004-10-04 by Grzegorz Rewucki            }
{                           rewucki@rewucki.pl          }
{                           http://www.rewucki.pl       }
{ Updated: 2010-03-14 by Tomasz Trejderowski            }
{ Updated: 2010-04-06 by Bogusław Kaleta                }
{ Updated: 2011-02-09 by Grzegorz Rewucki               }
{ Updated: 2011-09-15 by Grzegorz Rewucki               }
{ Updated: 2012-08-31 by Grzegorz Rewucki               }
{ Updated: 2014-04-20 by Grzegorz Rewucki               }
{*******************************************************}

unit RVALUTF8_Polish;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'cale', 'cm', 'mm', 'pica', 'piksele', 'punkty',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pkt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Plik', '&Edycja', 'F&ormat', '&Czcionka', '&Akapit', 'W&staw', '&Tabela', '&Okno', 'Pomo&c',
  // exit
  'Za&kończ',
  // top-level menus: View, Tools,
  '&Widok', '&Narzędzia',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  '&Rozmiar', '&Styl czcionki', '&Zaznacz', 'Wyrów&nanie komórek', 'Obra&mowanie komórek',
  // menus: Table cell rotation
  'Obracanie komó&rek',
  // menus: Text flow, Footnotes/endnotes
  'Prz&epływ tekstu', '&Przypisy',
  // ribbon tabs: tab1, tab2, view, table
  'Narzędzia &główne', 'Z&aawansowane', 'Wid&ok', '&Tabela',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Schowek', 'Czcionka', 'Akapit', 'Lista', 'Edytowanie',
  // ribbon groups: insert, background, page setup,
  'Wstawianie', 'Tło strony', 'Ustawienia strony',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options,
  'Łącza', 'Przypisy', 'Widoki dokumentu', 'Pokazywanie/ukrywanie', 'Powiększenie', 'Opcje',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Wstaw', 'Usuń', 'Operacje', 'Obramowanie',
  // ribbon groups: styles
  'Style',
  // ribbon screen tip footer,
  'F1 - pomoc',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Niedawno używane dokumenty', 'Zapisz kopię dokumentu', 'Wyświetl podgląd dokumentu i wydrukuj go',
  // ribbon label: units combo
  'Miara:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nowy', 'Nowy|Tworzy nowy dokument',
  // TrvActionOpen
  '&Otwórz...', 'Otwórz|Otwiera istniejący dokument',
  // TrvActionSave
  '&Zapisz', 'Zapisz|Zapisuje aktywny dokument',
  // TrvActionSaveAs
  'Zapisz j&ako...', 'Zapisz jako...|Zapisuje aktywny dokument pod nową nazwą',
  // TrvActionExport
  '&Eksportuj...', 'Exportuj|Eksportuje aktywny dokument do innego formatu',
  // TrvActionPrintPreview
  '&Podgląd wydruku', 'Podgląd wydruku|Wyświetla pełne strony',
  // TrvActionPrint
  '&Drukuj...', 'Drukuj|Ustawia właściwości drukowania i drukuje aktywny dokument',
  // TrvActionQuickPrint
  '&Drukuj', '&Szybkie drukowanie', 'Drukuj|Drukuje aktywny dokument',
  // TrvActionPageSetup
  '&Ustawienia strony...', 'Ustawienia strony|Zmienia ustawienia układu strony',
  // TrvActionCut
  'Wyt&nij', 'Wytnij|Wycina zaznaczony fragment i umieszcza go w Schowku',
  // TrvActionCopy
  '&Kopiuj', 'Kopiuj|Kopiuje zaznaczony fragment i umieszcza go w Schowku',
  // TrvActionPaste
  'Wkl&ej', 'Wklej|Wstawia zawartość Schowka',
  // TrvActionPasteAsText
  'Wklej jako &tekst', 'Wklej jako tekst|Wstawia zawartość schowka jako tekst',
  // TrvActionPasteSpecial
  'Wklej &specjalnie...', 'Wklej specjalnie|Wstawia zawartość schowka umożliwiając wybór formatu',
  // TrvActionSelectAll
  'Z&aznacz wszystko', 'Zaznacz wszystko|Zaznacza cały dokument',
  // TrvActionUndo
  'Co&fnij', 'Cofnij|Cofa ostatnią czynność',
  // TrvActionRedo
  '&Ponów', 'Ponów|Ponawia ostatnio cofniętą czynność',
  // TrvActionFind
  '&Znajdź...', 'Znajdź|Znajduje podany tekst',
  // TrvActionFindNext
  'Znajdź &następny', 'Znajdź następny|Powtarza ostatnie wyszukiwanie',
  // TrvActionReplace
  'Za&mień...', 'Zamień|Znajduje i zamienia podany tekst',
  // TrvActionInsertFile
  '&Plik...', 'Wstaw plik|Wstawia zawartość pliku',
  // TrvActionInsertPicture
  '&Obraz...', 'Wstaw obraz|Wstawia obraz',
  // TRVActionInsertHLine
  '&Linia pozioma', 'Wstaw linię poziomą|Wstawia linię poziomą',
  // TRVActionInsertHyperlink
  '&Hiperłącze...', 'Wstaw hiperłącze|Wstawia hiperłącze',
  // TRVActionRemoveHyperlinks
  '&Usuń odnośniki', 'Usuń odnośniki|Usuwa wszystkie odnośniki w zaznaczonym tekście',
  // TrvActionInsertSymbol
  '&Symbol...', 'Wstaw symbol|Wstawia symbol',
  // TrvActionInsertNumber
  '&Licznik...', 'Wstaw licznik|Wstawia licznik',
  // TrvActionInsertCaption
  'Wstaw &podpis...', 'Wstaw podpis|Wstawia podpis wybranego obiektu',
  // TrvActionInsertTextBox
  'Przypis &sąsiadujacy', 'Wstaw przypis sąsiadujący|Wstawia przypis sąsiadujący z miejcem wstawienia',
  // TrvActionInsertSidenote
  'Przypis &boczny', 'Wstaw przypis boczny|Wstawia przypis boczny',
  // TrvActionInsertPageNumber
  '&Numer strony', 'Wstaw numer strony|Wstawia numer strony',
  // TrvActionParaList
  'Wy&punktowanie i numerowanie...', 'Wypunktowanie i numerowanie|Zmienia ustawienia wypunktowania i numerowania w zaznaczonych akapitach',
  // TrvActionParaBullets
  '&Wypunktowanie', 'Wypunktowanie|Dodaje bądź usuwa wypunktowanie akapitu',
  // TrvActionParaNumbering
  '&Numerowanie', 'Numerowanie|Dodaje bądź usuwa numerowanie akapitu',
  // TrvActionColor
  'Kolor &tła...', 'Kolor tła|Zmienia ustawienia koloru tła dokumentu',
  // TrvActionFillColor
  'Kolor &wypełnienia...', 'Kolor wypełnienia|Zmienia kolor wypełnienia zaznaczonego elementu',
  // TrvActionInsertPageBreak
  'Wstaw znak podział&u strony', 'Wstaw znak podziału strony|Wstawia znak podziału strony',
  // TrvActionRemovePageBreak
  '&Usuń znak podziału strony', 'Usuń znak podziału strony|Usuwa znak podziału strony',
  // TrvActionClearLeft
  'Usuń przepływ tekstu po &lewej stronie', 'Usuń przepływ tekstu po lewej stronie|Umieszcza ten paragraf poniżej dowolnego obrazka z wyrównaniem do lewego marginesu',
  // TrvActionClearRight
  'Usuń przepływ tekstu po &prawej stronie', 'Usuń przepływ tekstu po prawej stronie|Umieszcza ten paragraf poniżej dowolnego obrazka z wyrównaniem do prawego marginesu',
  // TrvActionClearBoth
  'Usuń przepływ tekstu po &obu stronach', 'Usuń przepływ tekstu po obu stronach|Umieszcza ten paragraf poniżej dowolnego obrazka z wyrównaniem do obu marginesów',
  // TrvActionClearNone
  '&Normalny przeływ tekstu', 'Normalny przeływ tekstu|Normalny przepływ tekstu wokół obrazków z wyrównaniem do obu marginesów',
  // TrvActionVAlign
  'Pozycja &obiektu...', 'Pozycja obiektu...|Zmienia pozycję wybranego obiektu',
  // TrvActionItemProperties
  '&Właściwości obiektu...', 'Właściwości obiektu|Definiuje właściwości wybranego obiektu',
  // TrvActionBackground
  '&Tło...', 'Tło|Zmienia efekty tła',
  // TrvActionParagraph
  '&Akapit...', 'Akapit|Zmienia ustawienia akapitu',
  // TrvActionIndentInc
  'Zwiększ wcięcie', 'Zwiększ wcięcie|Zwiększa wcięcie zaznaczonych akapitów',
  // TrvActionIndentDec
  'Zmniejsz wcięcie', 'Zmniejsz wcięcie|Zmniejsza wcięcie zaznaczonych akapitów',
  // TrvActionWordWrap
  '&Zawijaj tekst', 'Zawijaj tekst|Przełącza zawijanie tekstu w zaznaczonych akapitach',
  // TrvActionAlignLeft
  'Wyrównaj do &lewej', 'Wyrównaj do lewej|Wyrównuje zaznaczony tekst do lewej',
  // TrvActionAlignRight
  'Wyrównaj do &prawej', 'Wyrównaj do prawej|Wyrównuje zaznaczony tekst do prawej',
  // TrvActionAlignCenter
  'Wyśr&odkuj', 'Wyśrodkuj|Wyrównuje zaznaczony tekst do środka',
  // TrvActionAlignJustify
  'Wy&justuj', 'Wyjustuj|Wyrównuje zaznaczony tekst do prawej i lewej jednocześnie',
  // TrvActionParaColor
  'Kolor tła akapitu...', 'Kolor tła akapitu|Zmienia kolor tła zaznaczonych akapitów',
  // TrvActionLineSpacing100
  'Odstęp &pojedynczy', 'Odstęp pojedynczy|Ustawia pojedynczy odstęp miedzy wierszami',
  // TrvActionLineSpacing150
  'Odstęp &1.5', 'Odstęp 1.5|Ustawia odstęp 1.5 między wierszami',
  // TrvActionLineSpacing200
  '&Podwójny odstęp', 'Podwójny odstęp|Ustawia powdójny odstęp między wierszami',
  // TrvActionParaBorder
  '&Obramowanie i tło akapitu...', 'Obramowanie i tło akapitu|Ustawia obramowanie i tło zaznaczonych akapitów',
  // TrvActionInsertTable
  'Wstaw t&abelę...', '&Tabela', 'Wstaw tabelę|Wstawia nową tabelę',
  // TrvActionTableInsertRowsAbove
  'Wstaw wi&ersz powyżej', 'Wstaw wiersz powyżej|Wstawia wiersz powyżej zaznaczonych komórek',
  // TrvActionTableInsertRowsBelow
  'Wstaw &wiersz poniżej', 'Wstaw wiersz poniżej|Wstawia wiersz poniżej zaznaczonych komórek',
  // TrvActionTableInsertColLeft
  'Wstaw k&olumnę po lewej', 'Wstaw kolumnę po lewej|Wstawia kolumnę po lewej stronie zaznaczonych komórek',
  // TrvActionTableInsertColRight
  'Wstaw &kolumnę po prawej', 'Wstaw kolumnę po prawej|Wstawia kolumnę po prawej stronie zaznaczonych komórek',
  // TrvActionTableDeleteRows
  'Usuń wi&ersze', 'Usuń wiersze|Usuwa wiersze zawierające zaznaczone komórki',
  // TrvActionTableDeleteCols
  'Usuń &kolumny', 'Usuń kolumny|Usuwa kolumny zawierające zaznaczone komórki',
  // TrvActionTableDeleteTable
  'Usuń &tabelę', 'Usuń tabelę|Usuwa zaznaczoną tabelę',
  // TrvActionTableMergeCells
  'S&calaj komórki', 'Scalaj komórki|Scala zaznaczone komórki',
  // TrvActionTableSplitCells
  'Podzi&el komórki...', 'Podziel komórki|Dzieli zaznaczone komórki',
  // TrvActionTableSelectTable
  'Zaznacz &tabelę', 'Zaznacz tabelę|Zaznacza tabelę',
  // TrvActionTableSelectRows
  'Zaznacz &wiersze', 'Zaznacz wiersze|Zaznacza wiersze',
  // TrvActionTableSelectCols
  'Zaznacz k&olumny', 'Zaznacz kolumny|Zaznacza kolumny',
  // TrvActionTableSelectCell
  'Zaznacz &komórkę', 'Zaznacz komórkę|Zaznacza komórkę',
  // TrvActionTableCellVAlignTop
  'Wyrównaj komórki do &góry', 'Wyrównaj komórki do góry|Wyrównuje zawartość zaznaczonych komórek do góry',
  // TrvActionTableCellVAlignMiddle
  'Wyrównaj komórki do śr&odka', 'Wyrównaj komórki do środka|Wyrównuje zawartość zaznaczonych komórek do środka',
  // TrvActionTableCellVAlignBottom
  'Wyrównaj komórki do &dołu', 'Wyrównaj komórki do dołu|Wyrównuje zawartość zaznaczonych komórek do dołu',
  // TrvActionTableCellVAlignDefault
  '&Domyślne wyrównanie pionowe', 'Domyślne wyrównanie pionowe|Ustawia domyślne wyrównywanie zaznaczonych komórek w pionie',
  // TrvActionTableCellRotationNone
  '&Nie obracaj komórek', 'Nie obracaj komórek|Nie obraca zawartości komórek',
  // TrvActionTableCellRotation90
  'Obróć komórki o &90°', 'Obróć komórki o 90°|Obraca zawartość komórek o 90°',
  // TrvActionTableCellRotation180
  'Obróć komórki o &180°', 'Obróć komórki o 180°|Obraca zawartość komórek o 180°',
  // TrvActionTableCellRotation270
  'Obróć komórki o &270°', 'Obróć komórki o 270°|Obraca zawartość komórek o 270°',
  // TrvActionTableProperties
  '&Właściwości tabeli...', 'Właściwości tabeli|Zmienia właściwości zaznaczonej tabeli',
  // TrvActionTableGrid
  'Pokaż &linie siatki', 'Pokaż linie siatki|Pokazuje lub ukrywa linie siatki zaznaczonej tabeli',
  // TRVActionTableSplit
  'Podzie&l tabelę', 'Podziel tabelę|Dzieli tabelę na dwie osobne tabele zaczynając od wskazanego wiersza',
  // TRVActionTableToText
  'Przekształć w te&kst...', 'Przekształć w tekst|Przekształca tabelę w tekst',
  // TRVActionTableSort
  '&Sortuj...', 'Sortuj|Sortuje wiersze tabeli',
  // TrvActionTableCellLeftBorder
  'Krawędź &lewa', 'Krawędź lewa|Pokazuje lub ukrywa lewą krawędź zaznaczonych komórek',
  // TrvActionTableCellRightBorder
  'Krawędź &prawa', 'Krawędź prawa|Pokazuje lub ukrywa prawą krawędź zaznaczonych komórek',
  // TrvActionTableCellTopBorder
  'Krawędź &górna', 'Krawędź górna|Pokazuje lub ukrywa górną krawędź zaznaczonych komórek',
  // TrvActionTableCellBottomBorder
  'Krawędź &dolna', 'Krawędź dolna|Pokazuje lub ukrywa dolną krawędź zaznaczonych komórek',
  // TrvActionTableCellAllBorders
  '&Wszystkie krawędzie', 'Wszystkie krawędzie|Pokazuje lub ukrywa wszystkie krawędzie zaznaczonych komórek',
  // TrvActionTableCellNoBorders
  '&Brak krawędzi', 'Brak krawędzi|Ukrywa wszystkie krawędzie zaznaczonych komórek',
  // TrvActionFonts & TrvActionFontEx
  '&Czcionka...', 'Czcionka|Zmienia czcionkę zaznaczonego tekstu',
  // TrvActionFontBold
  '&Pogrubienie', 'Pogrubienie|Zmienia styl pogrubienia zaznaczonego tekstu',
  // TrvActionFontItalic
  '&Kursywa', 'Kursywa|Zmienia styl pochylenia zaznaczonego tekstu',
  // TrvActionFontUnderline
  'Podk&reślenie', 'Podkreślenie|Zmienia styl podkreślenia zaznaczonego tekstu',
  // TrvActionFontStrikeout
  'Prze&kreślenie', 'Przekreślenie|Zmienia styl przekreślenia zaznaczonego tekstu',
  // TrvActionFontGrow
  '&Powieksz czcionkę', 'Powieksz czcionkę|Powiększa wielkość czcionki zaznaczonego tekstu o 10%',
  // TrvActionFontShrink
  'P&omniejsz czcionkę', 'Pomniejsz czcionkę|Zmniejsza wielkość czcionki zaznaczonego tekstu o 10%',
  // TrvActionFontGrowOnePoint
  '&Powieksz czcionkę o 1 pt', 'Powieksz czcionkę o 1 pt|Powiększa wielkość czcionki zaznaczonego tekstu o 1 punkt',
  // TrvActionFontShrinkOnePoint
  'P&omniejsz czcionkę o 1 pt', 'Pomniejsz czcionkę o 1 pt|Zmniejsza wielkość czcionki zaznaczonego tekstu o 1 punkt',
  // TrvActionFontAllCaps
  '&Wielkie litery', 'Wielkie litery|Zmienia wszystkie litery zaznaczonego tekstu na wielkie',
  // TrvActionFontOverline
  '&Nadkreślenie', 'Nadkreślenie|Zmienia styl nadkreślenia zaznaczonego tekstu',
  // TrvActionFontColor
  'Kolor &tekstu...', 'Kolor tekstu|Zmienia kolor zaznaczonego tekstu',
  // TrvActionFontBackColor
  'Kolor tła te&kstu...', 'Kolor tła tekstu|Zmienia kolor tła zaznaczonego tekstu',
  // TrvActionAddictSpell3
  '&Pisownia', 'Pisownia|Sprawdza pisownię',
  // TrvActionAddictThesaurus3
  '&Tezaurus', 'Tezaurus|Wyszukuje synonimy zaznaczonego słowa',
  // TrvActionParaLTR
  'Akapit z lewej do prawej', 'Akapit z lewej do prawej|Ustawia kierunek tekstu z lewej do prawej w zaznaczonych akapitach',
  // TrvActionParaRTL
  'Akapit z prawej do lewej', 'Akapit z prawej do lewej|Ustawia kierunek tekstu z prawej do lewej w zaznaczonych akapitach',
  // TrvActionLTR
  'Tekst z lewej do prawej', 'Tekst z lewej do prawej|Ustawia kierunek z lewej do prawej w zaznaczonym tekście',
  // TrvActionRTL
  'Tekst z prawej do lewej', 'Tekst z prawej do lewej|Ustawia kierunek z prawej do lewej w zaznaczonym tekście',
  // TrvActionCharCase
  'Wielkość znaków', 'Wielkość znaków|Zmienia wielkość znaków w zaznaczonym tekście',
  // TrvActionShowSpecialCharacters
  'Znaki &formatowania', 'Znaki formatowania|Pokazuje bądź ukrywa znaki formatowania',
  // TrvActionSubscript
  'Indeks &dolny', 'Indeks dolny|Zmienia styl indeksu dolnego zaznaczonego tekstu',
  // TrvActionSuperscript
  'Indeks &górny', 'Indeks górny|Zmienia styl indeksu górnego zaznaczonego tekstu',
  // TrvActionInsertFootnote
  '&Przypis dolny', 'Przypis dolny|Wstawia przypis dolny',
  // TrvActionInsertEndnote
  'Przypis &końcowy', 'Przypis końcowy|Wstawia przypis końcowy',
  // TrvActionEditNote
  'E&dytuj treść przypisu', 'Edytuj treść przypisu|Rozpoczyna edycję treści przypisu',
  'Ukryj', 'Ukryj|Ukrywa lub pokazuje zaznaczony fragment',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Style...', 'Style|Otwiera okno zarządzania stylami',
  // TrvActionAddStyleTemplate
  'Dod&aj styl...', 'Dodaj styl|Tworzy nowy styl tekstu lub akapitu',
  // TrvActionClearFormat,
  'Wy&czyść formatowanie', 'Wyczyść formatowanie|Usuwa formatowanie tekstu i akapitu z zaznaczonego fragmentu',
  // TrvActionClearTextFormat,
  'Wyczyść formatowanie &tekstu', 'Wyczyść formatowanie tekstu|Usuwa formatowanie z zaznaczonego tekstu',
  // TrvActionStyleInspector
  '&Inspektor stylów', 'Inspektor stylów|Pokazuje lub ukrywa inspektora stylów',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Anuluj', 'Zamknij', 'Wstaw', '&Otwórz...', '&Zapisz...', '&Wyczyść', 'Pomoc', 'Usuń',
  // Others  -------------------------------------------------------------------
  // percents,
  'procent',
  // left, top, right, bottom sides
  'Lewa', 'Góra', 'Prawa', 'Dół',
  // save changes? confirm title
  'Zapisz zmiany do %s?', 'Potwierdź',
  // warning: losing formatting
  '%s może zawierać funkcje, które nie są zgodne z wybranym formatem.'#13+
  'Czy chcesz zapisać dokument w tym formacie?',
  // RVF format name
  'RVF format',
  // Error messages ------------------------------------------------------------
  'Błąd',
  'Błąd wczytywania pliku.'#13#13'Przypuszczalne powody:'#13'- format wczytywanego pliku nie jest obsługiwany;'#13+
  '- plik jest uszkodzony;'#13'- plik jest w użyciu.',
  'Błąd wczytywania obrazu.'#13#13'Przypuszczalne powody:'#13'- format wczytywanego obrazu nie jest obsługiwany;'#13+
  '- plik nie zawiera obrazu;'#13+
  '- plik jest uszkodzony;'#13'- plik jest w użyciu.',
  'Błąd zapisu pliku.'#13#13'Przypuszczalne powody:'#13'- brak miejsca na dysku;'#13+
  '- dysk jest zabezpieczony przed zapisem;'#13'- dysk wymmienny nie został włożony;'#13+
  '- plik jest w użyciu;'#13'- nośnik jest uszkodzony.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Pliki RVF (*.rvf)|*.rvf',  'Pliki RTF (*.rtf)|*.rtf' , 'Pliki XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Pliki tekstowe(*.txt)|*.txt', 'Pliki tekstowe - Unicode (*.txt)|*.txt', 'Pliki tekstowe - wykryj automatycznie (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Pliki HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - uprość (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Dokument programu Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Wyszukiwanie zakończone', 'Wyszukiwany tekst ''%s'' nie został znaleziony.',
  // 1 string replaced; Several strings replaced
  'Zastapiono 1 ciąg znaków.', 'Liczba zastąpionych ciągów znaków wynosi: %d.',
  // continue search from the beginning/end?
  'Osiągnięto koniec dokumentu, rozpocząć od początku?',
  'Osiągnięto początek dokumentu, rozpocząć od końca?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Przezroczysty', 'Automatyczny',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Czarny', 'Brązowy', 'Oliwkowy', 'Ciemnozielony', 'Szarobłękitny', 'Ciemnoniebieski', 'Indygo', 'Szary 80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Ciemnoczerwony', 'Pomarańczowy', 'Ciemnożółty', 'Zielony', 'Ciemnobłękitny', 'Niebieski', 'Niebieskoszary', 'Szary 50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Czerwony', 'Jasnopomarańczowy', 'Zielony', 'Morski', 'Błękitny', 'Jasnoniebieski', 'Fioletowy', 'Szary 40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Różowy', 'Złoty', 'Żółty', 'Jasnozielony', 'Turkusowy', 'Lazurowy', 'Śliwkowy', 'Szary 25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Różany', 'Beżowy', 'Jasnożółty', 'Bladozielony', 'Pastelowobłękitny', 'Bladoniebieski', 'Liliowy', 'Biały',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Przez&roczysty', '&Automatyczny', '&Więcej kolorów...', '&Domyślny',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Tło', 'K&olor:', 'Pozycja obrazu', 'Tło', 'Tekst tekst tekst tekst tekst.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Brak', '&Rozciągnij', '&Na zakładkę', '&Sąsiadująco', '&Centralnie',
  // Padding button
  '&Marginesy...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Kolor wypełnienia', '&Zastosuj do:', '&Więcej kolorów...', '&Marginesy...',
  'Proszę wybrać kolor',
  // [apply to:] text, paragraph, table, cell
  'tekst', 'akapit' , 'tabela', 'komórka',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Czcionka', 'C&zcionka', '&Odstępy między znakami',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Czcionka:', '&Rozmiar:', 'Styl czcionki', '&Pogrubienie', '&Kursywa',
  // Script, Color, Back color labels, Default charset
  '&Skrypt:', 'Ko&lor:', '&Tło:', '(Domyślny)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efekty', 'Podkreślenie', '&Nadkreślenie', 'Prz&ekreślenie', '&Wielkie litery',
  // Sample, Sample text
  'Podgląd', 'Przykładowy tekst',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Odsęp', 'O&dstęp:', '&Rozstrzelone', 'Z&agęszczony',
  // Offset group-box, Offset label, Down, Up,
  'Położenie', '&Położenie:', 'O&bniżone', 'Pod&niesione',
  // Scaling group-box, Scaling
  'Skalowanie', 'Ska&la:',
  // Sub/super script group box, Normal, Sub, Super
  'Indeks góny i dolny', 'Nor&malny', 'Indeks doln&y', 'Indeks &górny',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Marginesy', '&Górny:', '&Lewy:', '&Dolny:', '&Prawy:', '&Jednakowe',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Hiperłącze', 'Hiperłącze', '&Etykieta:', '&Lokalizacja:', '<<Wybór>>',
  // cannot open URL
  'Nieudane przejście do "%s"',
  // hyperlink properties button, hyperlink style
  '&Dostosuj...', '&Style:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Atrybuty hiperłącza', 'Niewaktywne', 'Aktywne',
  // Text [color], Background [color]
  'T&ekst:', '&Tło',
  // Underline [color]
  'Podk&reślenie:',
  // Text [color], Background [color] (different hotkeys)
  'Te&kst:', 'Tł&o',
  // Underline [color], Attributes group-box,
  'Podk&reślenie:', 'Atrybuty',
  // Underline type: always, never, active (below the mouse)
  'Podk&reślenie:', 'zawsze', 'nigdy', 'aktywne',
  // Same as normal check-box
  'Takie same jak &nieaktywne',
  // underline active links check-box
  '&Podkreśl aktywne łącza',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Wstaw symbol', 'C&zcionka:', '&Kodowanie:', 'Unicode',
  // Unicode block
  '&Blok:',
  // Character Code, Character Unicode Code, No Character
  'Kod znaku: %d', 'Kod znaku: Unicode %d', '(brak)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Wstaw tabelę', 'Rozmiar tabeli', 'Liczba &kolumn:', 'Liczba &wierszy:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Zachowanie autodopasowania', '&Automatyczne', 'Autodopasowanie do &okna', 'Dopasowanie &manualne',
  // Remember check-box
  '&Ustaw jako domyślne dla nowych tabel',
  // VAlign Form ---------------------------------------------------------------
  'Pozycja',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Właściwości', 'Obraz', 'Rozmiar i pozycja', '&Linie', 'T&abela', '&Wiersze', '&Komórki',
  // Image Appearance, Image Misc, Number tabs
  'Wygląd', 'Inne', 'Licznik',
  // Box position, Box size, Box appearance tabs
  'Pozycja', 'Rozmiar', 'Wygląd',
  // Preview label, Transparency group-box, checkbox, label
  'Podgląd:', 'Wypełnienie', '&Przezroczysty', '&Kolor:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Z&mień...', '&Zapisz...', 'Automatyczny',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Wyrównywanie', '&Wyrównaj:',
  'do dołu', 'do środka',
  'góra do górnej części linii', 'dół do dolnej części linii', 'środek do środkowej części linii',
  // align to left side, align to right side
  'do lewej strony', 'do prawej strony',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Przesunięcie:', 'Rozmiar', 'S&zerokość:', 'W&ysokość:', 'Domyślny rozmiar: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Skaluj proporcjonalnie', '&Resetuj',
  // Inside the border, border, outside the border groupboxes
  'Obramowanie', 'Krawędzie', 'Odstępy',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Kolor:', '&Szerokość:',
  // Border width, Border color labels
  'S&zerokość:', 'Kolo&r:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'P&oziomy:', 'P&ionowy:',
  // Miscellaneous groupbox, Tooltip label
  'Inne', '&Podpowiedź:',
  // web group-box, alt text
  'Sieć Web', '&Tekst alternatywny:',
  // Horz line group-box, color, width, style
  'Linia pozioma', '&Kolor:', '&Szerokość:', '&Styl:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabela', 'Sz&erokość:', 'K&olor wypełnienia:', '&Odstępy komórek...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Marginesy poziome komórek:', 'M&arginesy pionowe komórek:', 'Krawędzie i tło',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Wi&doczne krawędzie...', 'Automatyczna', 'automatyczna',
  // Keep row content together checkbox
  'Zachowaj wi&ersze razem',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotacja', '&Brak', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Krawędzie:', 'Widocz&ne krawędzie...',
  // Border, CellBorders buttons
  'K&rawędzie tabeli...', 'Krawędz&ie komórek...',
  // Table border, Cell borders dialog titles
  'Krawędzie tabeli', 'Krawędzie komórek',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Drukowanie', '&Zachowj na jednej stronie', 'Liczba wiersz&y nagłówka:',
  'Wiersze nagłówka będą powtarzane na każdej stronie',
  // top, center, bottom, default
  'Do &góry', 'Do śro&dka', 'Do doł&u', 'Do&myślnie',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Ustawienia', 'Preferowana &szerokość:', 'Preferowana w&ysokość:', 'Kolor wypeł&nienia:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Krawędzie', 'Widoczne krawędzie:',  'Kolor &cienia:', 'Kolor św&iatła', 'Ko&lor:',
  // Background image
  '&Tło...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Pozycja pozioma', 'Pozycja pionowa', 'Pozycja w tekście',
  // Position types: align, absolute, relative
  'Wyrównanie', 'Położenie bezwzględne', 'Położenie względne',
  // [Align] left side, center, right side
  'lewy bok ramki do lewego boku',
  'środek ramki do środka',
  'prawy bok ramki do prawego boku',
  // [Align] top side, center, bottom side
  'górny bok ramki do górnego boku',
  'środek ramki do środka',
  'dolny bok ramki do dolnego boku',
  // [Align] relative to
  'względem',
  // [Position] to the right of the left side of
  'od prawego boku ramki do lewego boku',
  // [Position] below the top side of
  'poniżej górnekgo boku',
  // Anchors: page, main text area (x2)
  '&Strony', '&Obszaru tekstu', 'St&rony', 'Obszaru &tekstu',
  // Anchors: left margin, right margin
  '&Lewego marginesu', '&Prawego marginesu',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Górnego marginesu', '&Dolnego marginesu', '&Wewnętrznego marginesu', 'Z&ewnętrznego marginesu',
  // Anchors: character, line, paragraph
  '&Znacznika', '&Linii', '&Akapitu',
  // Mirrored margins checkbox, layout in table cell checkbox
  '&Marginesy odwrócne', '&Układ w komórce tabeli',
  // Above text, below text
  '&Przed tekstem', '&Za tekstem',
  // Width, Height groupboxes, Width, Height labels
  'Szerokość', 'Wysokość', 'Szero&kość:', 'W&ysokość:',
  // Auto height label, Auto height combobox item
  'Automatyczna', 'automatycznie',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Wyrównaie pionowe', 'Do &góry', 'Do śro&dka', 'Do doł&u',
  // Border and background button and title
  '&Krawędzie i tło...', 'Krawędzie i tło',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Wklej specjalnie', '&Wstaw jako:', 'Format RTF', 'Format HTML',
  'Tekst', 'Tekst unicode', 'Obraz - bitmapa', 'Obraz - metaplik',
  'Pliki graficzne',  'Adres',
  // Options group-box, styles
  'Opcje', '&Style:',
  // style options: apply target styles, use source styles, ignore styles
  'zastosuj style dokumentu docelowego', 'zachowaj style i wygląd',
  'zachowaj wygląd, ignoruj style',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Wypunktowanie i numerowanie', '&Wypunktowane', '&Numerowane', 'Lista punktatorów', 'Lista numeratorów',
  // Customize, Reset, None
  '&Dostosuj...', '&Zresetuj', '(Brak)',
  // Numbering: continue, reset to, create new
  'kontynuuj numerowanie', 'zresetuj numerowanie, zaczynając do', 'utwórz nową listę, zaczynając od',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Duże litery', 'Duże rzymskie', 'Liczby', 'Małe litery', 'Małe rzymskie',
  // Bullet type
  'Koło', 'Dysk', 'Kwadrat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Poziom:', '&Zacznij od:', '&Kontynuuj', 'Numerowanie',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Dostosuj listę', 'Poziomy', '&Liczba:', 'Właściwości listy', '&Typ listy:',
  // List types: bullet, image
  'Wypunktowanie', 'Obraz', 'Wstaw numerację|',
  // Number format, Number, Start level from, Font button
  'Format &numeracji:', 'Numeracja', '&Rozpocznij poziom od:', '&Czcionka...',
  // Image button, bullet character, Bullet button,
  '&Obraz...', 'Zn&ak wypunktowania', '&Znak...',
  // Position of list text, bullet, number, image, text
  'Pozycje', 'Pozycja punktora', 'Pozycja numeru', 'Pozycja obrazu', 'Pozycja tekstu',
  // at, left indent, first line indent, from left indent
  '&od:', 'Lewe &wcięcie:', '&Pierwszy wiersz:', 'od lewego wcięcia',
  // [only] one level preview, preview
  'Podgląd &jednego poziomu', 'Podgląd',
  // Preview text
  'Tekst tekst tekst tekst tekst. Text tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'wyrównaj do lewej', 'wyrównaj do prawej', 'wyśrodkuj',
  // level #, this level (for combo-box)
  'Poziom %d', 'Ten poziom',
  // Marker character dialog title
  'Edytuj znak punktora',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Obramowanie i tło akapitu', '&Krawędzie', '&Tło',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Ustawienia', 'Ko&lor:', '&Szerokość:', 'O&dstęp:', '&Marginesy...',
  // Sample, Border type group-boxes;
  'Podgląd', 'Rodzaj krawędzi',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Brak', '&Pojedyncza', 'Pod&wójna', 'Pot&rójna', 'Sklej w&ewnętrznie', 'Sklej &zewnętrznie',
  // Fill color group-box; More colors, padding buttons
  'Kolor wypełnienia', '&Więcej kolorów...', '&Marginesy...',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // title and group-box in Offsets dialog
  'Marginesy wewnętrzne', 'Marginesy',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Akapit', 'Wyrównanie do', '&lewej', 'pr&awej', 'śro&dka', 'lewej &i prawej',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Odstępy', 'P&rzed:', '&Po:', 'Międ&zy wierszami:', '&Co:',
  // Indents group-box; indents: left, right, first line
  'Wcięcia', 'Od l&ewej:', '&Od prawej:', 'Pierwszy &wiersz:',
  // indented, hanging
  'Wcię&ty', 'Wy&sunięty', 'Podgląd',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Pojedyncze', '1.5 wiersza', 'Podwójne', 'Przynajmniej', 'Dokładnie', 'Wielokrotne',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Wcięcia i odstęp&y', 'Ta&bulatory', 'Podział tekst&u',
  // tab stop position label; buttons: set, delete, delete all
  'Położenie &tabulatora:', '&Ustaw', 'U&suń', 'Usuń &wszystkie',
  // tab align group; left, right, center aligns,
  'Wyrównanie', 'Do &lewej', 'Do pr&awej', 'Do śro&dka',
  // leader radio-group; no leader
  '&Znaki wiodące', 'Brak',
  // tab stops to be deleted, delete none, delete all labels
  'Tabulatory do usunięcia:', '(Brak)', 'Wszystkie.',
  // Pagination group-box, keep with next, keep lines together
  'Podział na strony', '&Razem z następnym', 'Zachowaj wi&ersze razem',
  // Outline level, Body text, Level #
  '&Poziom konspektu:', 'Tekst podstawowy', 'Poziom %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Podgląd wydruku', 'Szerokość strony', 'Cała strona', 'Strony:',
  // Invalid Scale, [page #] of #
  'Podaj liczbę pomiędzy 10 a 500', 'z %d',
  // Hints on buttons: first, prior, next, last pages
  'Piewrsza strona', 'Poprzednia strona', 'Następna strona', 'Ostatnia strona',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Odstępy komórek', 'Odstępy', '&między komórkami', 'od krawędzi tabeli',
  // vertical, horizontal (x2)
  '&Pionowy:', 'P&oziomy:', 'P&ionowy:', 'Po&ziomy:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Krawędzie', 'Ustawienia', 'K&olor:', 'Kolor św&iatła:', 'Kolor &cienia:',
  // Width; Border type group-box;
  '&Szerokość:', 'Rodzaj krawędzi',
  // Border types: none, sunken, raised, flat
  '&Brak', '&Zapadnięta', '&Wypukła', '&Płaska',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Podziel', 'Podziel na',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Podaj liczbę wierszy i kolumn', '&Rozłączne komórki',
  // number of columns, rows, merge before
  'Liczba &kolumn:', 'Liczba &wierszy:', '&Scal komórki przed podziałem',
  // to original cols, rows check-boxes
  'Rozłącz k&olumny', 'Rozłącz wi&ersze',
  // Add Rows form -------------------------------------------------------------
  'Dodaj wiersze', '&Liczba wierszy:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Ustawienia strony', 'Stron&a', 'Nagłówek &i stopka',
  // margins group-box, left, right, top, bottom
  'Marginesy (milimetry)', 'Marginesy (cale)', 'L&ewy:', '&Górny:', '&Prawy:', 'D&olny:',
  // mirror margins check-box
  'Marginesy lustr&zane',
  // orientation group-box, portrait, landscape
  'Orientacja', 'P&ionowa', 'Pozio&ma',
  // paper group-box, paper size, default paper source
  'Papier', 'Rozmia&r:', 'Źró&dło:',
  // header group-box, print on the first page, font button
  'Nagłówek', '&Tekst:', '&Nagłówek na pierwszej stronie', '&Czcionka...',
  // header alignments: left, center, right
  'Do &lewej', 'Do ś&rodka', 'Do &prawej',
  // the same for footer (different hotkeys)
  'Stopka', 'Te&kst:', '&Stopka na pierwszej stronie', 'C&zcionka...',
  'Do l&ewej', 'Do śr&odka', 'Do prawe&j',
  // page numbers group-box, start from
  'Numery stron', 'Zacznij od nu&meru:',
  // hint about codes
  'Znaki specjalne:'#13'&&p - numer strony; &&P - liczba stron; &&d - bieżąca data; &&t - bieżący czas.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Strona kodowa pliku tekstowego', '&Wybierz sposób kodowania pliku:',
  // thai, japanese, chinese (simpl), korean
  'Tajski', 'Japoński', 'Chiński (uproszczony)', 'Koreański',
  // chinese (trad), central european, cyrillic, west european
  'Chiński (tradycyjny)', 'Środkowoeuropejski', 'Cyrylica', 'Zachodnioeuropejski',
  // greek, turkish, hebrew, arabic
  'Grecki', 'Turecki', 'Hebrajski', 'Arabski',
  // baltic, vietnamese, utf-8, utf-16
  'Bałtycki', 'Wietnamski', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Styl', 'Wybierz &styl:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Przekształć w tekst', 'Wy&bierz separator:',
  // line break, tab, ';', ','
  'podział linii', 'tabulacja', 'średnik', 'przecinek',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabele zawierające scalone wiersze nie mągą być sortowane.',
  // title, main options groupbox
  'Sortowanie tabeli', 'Sortuj',
  // sort by column, case sensitive
  '&Sortuj wg kolumny:', 'Rozróżniaj &wielkość znaków',
  // heading row, range or rows
  'Pomiń wiersz &nagłówka', 'Sortuj wiersze: od %d do %d',
  // order, ascending, descending
  'Porządek', '&Rosnący', '&Malejący',
  // data type, text, number
  'Typ danych', '&Tekst', '&Liczba',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Wstaw licznik', 'Właściwości', '&Nazwa licznika:', 'Nu&merowanie:',
  // numbering groupbox, continue, start from
  'Numerowanie', 'K&ontynuuj', '&Rozpocznij od:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Podpis', '&Etykieta:', 'W&yklucz etykietę z podpisu',
  // position radiogroup
  'P&ołożenie', 'Po&wyżej wybranego obiektu', 'Po&nieżej wybranego obiektu',
  // caption text
  '&Podpis:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Licznik', 'Obraz', 'Tabela',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Widoczne krawędzie', 'Krawędzie',
  // Left, Top, Right, Bottom checkboxes
  '&Lewa', '&Górna', '&Prawa', '&Dolna',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Szerokość kolumny', 'Wysokość wiersza',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Wcięcie pierwszego wiersza', 'Wcięcie z lewej', 'Wysunięcie', 'Wcięcie z prawej',
  // Hints on lists: up one level (left), down one level (right)
  'Poziom do góry', 'Poziom w dół',
  // Hints for margins: bottom, left, right and top
  'Dolny margines', 'Lewy margines', 'Prawy margines', 'Górny margines',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulator lewy', 'Tabulator prawy', 'Tabulator środkowy', 'Tabulator dziesiętny',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normalny', 'Wcięcie normalne', 'Bez odstępów', 'Nagłówek %d', 'Akapit z listą',
  // Hyperlink, Title, Subtitle
  'Hiperłącze', 'Tytuł', 'Podtytuł',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Uwydatnienie', 'Wyróżnienie delikatne', 'Wyróżnienie intensywne', 'Wzmocnienie',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cytat', 'Cytat intnsywny', 'Odwołanie delikatne', 'Odwołanie intensywne',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Tekst blokowy', 'HTML - zmienna', 'HTML - kod', 'HTML - akronim',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML - definicja', 'HTML - klawiatura', 'HTML - przykład', 'HTML - stała szerokość',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML - wstępnie sformatowany', 'HTML - cytat', 'Nagłówek', 'Stopka', 'Numer strony',
  // Caption
  'Podpis',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Odsyłacz przypisu końcowego', 'Odsyłacz przypisu dolnego', 'Tekst przypisu końcowego', 'Tekst przypisu dolnego',
  // Sidenote Reference, Sidenote Text
  'Odsyłacz przypisu bocznego', 'Tekst przypisu bocznego',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'kolor', 'kolor tła', 'przezroczysty', 'domyślny', 'kolor podkreślenie',
  // default background color, default text color, [color] same as text
  'domyślny kolor tła', 'domyślny kolor tekstu', 'taki sam jak tekst',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'pojedyncze', 'grube', 'podwójne', 'wykropkowane', 'grube kropki', 'kreski',
  // underline types: thick dashed, long dashed, thick long dashed,
  'grube kreski', 'długie kreski', 'długie grube kreski',
  // underline types: dash dotted, thick dash dotted,
  'kropka kreska', 'gruba kropka kreska',
  // underline types: dash dot dotted, thick dash dot dotted
  'kreska kropka kropka', 'gruba kreska kropka kropka',
  // sub/superscript: not, subsript, superscript
  'bez indeksu', 'indeks dolny', 'indeks górny',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'kierunek:', 'odziedziczony', 'z lewej do prawej', 'z prawej do lewej',
  // bold, not bold
  'pogrubienie', 'bez pogrubienia',
  // italic, not italic
  'kursywa', 'bez kursywy',
  // underlined, not underlined, default underline
  'podkreślenie', 'bez podkreślenia', 'domyślne podkreślenie',
  // struck out, not struck out
  'przekreślenie', 'bez przekreślenia',
  // overlined, not overlined
  'nadkreślenie', 'bez nadkreślenia',
  // all capitals: yes, all capitals: no
  'kapitaliki', 'bez kapitalików',
  // vertical shift: none, by x% up, by x% down
  'bez przesunięcia', 'podniesiony o %d%%', 'opuszczony o %d%%',
  // characters width [: x%]
  'szerokość znaków',
  // character spacing: none, expanded by x, condensed by x
  'normalny odstęp', 'odstęp rozstrzelony o %s', 'odstęp zagęszczony o %s',
  // charset
  'skrypt',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'domyślna', 'domyślny', 'odziedziczone',
  // [hyperlink] highlight, default hyperlink attributes
  'podświetlenie', 'domyślne',
  // paragraph aligmnment: left, right, center, justify
  'do lewej', 'do prawej', 'do środka', 'wyjustowany',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'wysokość wiersza: %d%%', 'odstęp między wierszami: %s', 'wysokość wiersza: przynajmniej %s',
  'wysokość wiersza: dokłądnie %s',
  // no wrap, wrap
  'bez zawijania wierszy', 'zawijanie wierszy',
  // keep lines together: yes, no
  'zachowaj wiersze razem', 'nie zachowuj wierszy razem',
  // keep with next: yes, no
  'zachowaj razem z następnym akapitem', 'nie zachowuj razem z następnym akapitem',
  // read only: yes, no
  'tylko do odczytu', 'do edycji',
  // body text, heading level x
  'poziom konspektu: treść', 'poziom konspektu: %d',
  // indents: first line, left, right
  'wcięcie pierwszego wiersza', 'lewe wcięcie', 'prawe wcięcie',
  // space before, after
  'odstęp przed', 'odstęp po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulatory', 'brak', 'wyrównanie', 'do lewej', 'do prawej', 'do środka',
  // tab leader (filling character)
  'znak wiodący',
  // no, yes
  'nie', 'tak',
  // [padding/spacing/side:] left, top, right, bottom
  'lewy', 'górny', 'prawy', 'dolny',
  // border: none, single, double, triple, thick inside, thick outside
  'brak', 'pojedyncze', 'podwójne', 'potrójne', 'sklejone wewnętrznie', 'sklejone zewnętrznie',
  // background, border, padding, spacing [between text and border]
  'tło', 'krawędź', 'margines', 'odstęp',
  // border: style, width, internal width, visible sides
  'styl', 'szerokość', 'wewnętrzna szerokość', 'widoczne krawędzie',
  // style inspector -----------------------------------------------------------
  // title
  'Inspektor stylów',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Styl', '(brak)', 'Akapit', 'Czcionka', 'Atrybuty',
  // border and background, hyperlink, standard [style]
  'Obramowanie i tło', 'Hiperłącze', '(Standardowy)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Style', 'Styl',
  // name, applicable to,
  '&Nazwa:', 'Stosowany &do:',
  // based on, based on (no &),
  '&Oparty na:', 'Oparty na:',
  //  next style, next style (no &)
  'Styl następnego &akapitu:', 'Styl następnego akapitu:',
  // quick access check-box, description and preview
  '&Szybki dostęp', 'Opis i &podgląd:',
  // [applicable to:] paragraph and text, paragraph, text
  'akapitu i tekstu', 'akapitu', 'tekstu',
  // text and paragraph styles, default font
  'Style tekstu i akapitu', 'Domyślna czcionka',
  // links: edit, reset, buttons: add, delete
  'Zmień', 'Przywróć', '&Dodaj', '&Usuń',
  // add standard style, add custom style, default style name
  'Dodaj styl &standardowy...', 'Dodaj styl &własny', 'Styl %d',
  // choose style
  'Wybierz &styl:',
  // import, export,
  '&Import...', '&Eksport...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Style RichView (*.rvst)|*.rvst', 'Błąd wczytywania pliku', 'Plik nie zawiera żadnych stylów',
  // Title, group-box, import styles
  'Import stylów z pliku', 'Import', 'I&mport stylów:',
  // existing styles radio-group: Title, override, auto-rename
  'Jeśli styl istnieje to', '&nadpisz', '&dodaj pod zmienioną nazwą',
  // Select, Unselect, Invert,
  '&Zaznacz', '&Odznacz', 'Od&wróć',
  // select/unselect: all styles, new styles, existing styles
  '&Wszystkie style', '&Nowe style', '&Istniejące style',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Wyczyść wirmatowanie', 'Wszystkie style',
  // dialog title, prompt
  'Style', '&Wybierz style:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Synonimy', 'Pom&iń wszystkie', 'Dod&aj do Słownika',
  // Progress messages ---------------------------------------------------------
  'Pobieranie %s', 'Przygotowanie do druku...', 'Drukowanie strony %d',
  'Konwertowanie z RTF...',  'Konwertowanie do RTF...',
  'Odczytywanie %s...', 'Zapisywanie %s...', 'tekst',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Brak przypisu', 'Przypis dolny', 'Przypis końcowy', 'Przypis boczny', 'Przypis sąsiadujący'
  );
initialization
  RVA_RegisterLanguage(
    'Polish', // english language name, do not translate
    'Polski', // native language name
    EASTEUROPE_CHARSET, @Messages);
end.

