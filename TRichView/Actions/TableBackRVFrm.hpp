﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TableBackRVFrm.pas' rev: 27.00 (Windows)

#ifndef TablebackrvfrmHPP
#define TablebackrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tablebackrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVTableBack;
class PASCALIMPLEMENTATION TfrmRVTableBack : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Controls::TImageList* il;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rg;
	Vcl::Stdctrls::TGroupBox* gbBack;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TButton* btnOpen;
	Vcl::Stdctrls::TButton* btnSave;
	Vcl::Stdctrls::TButton* btnClear;
	Vcl::Extdlgs::TSavePictureDialog* spd;
	Vcl::Extctrls::TPanel* panImg;
	Vcl::Extctrls::TImage* img;
	void __fastcall btnSaveClick(System::TObject* Sender);
	void __fastcall btnOpenClick(System::TObject* Sender);
	void __fastcall btnClearClick(System::TObject* Sender);
	void __fastcall rgClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	Vcl::Extdlgs::TOpenPictureDialog* opd;
	System::UnicodeString FFileName;
	void __fastcall AdjustImage(void);
	
protected:
	Vcl::Controls::TControl* _btnSave;
	Vcl::Controls::TControl* _btnClear;
	Vcl::Controls::TControl* _btnOk;
	
public:
	System::UnicodeString Filter;
	void __fastcall Init(System::Uitypes::TColor Color, Rvstyle::TRVItemBackgroundStyle BackgroundStyle, Vcl::Graphics::TGraphic* Graphic);
	void __fastcall GetResult(Rvstyle::TRVItemBackgroundStyle &BackgroundStyle, Vcl::Graphics::TGraphic* &Graphic, System::UnicodeString &FileName);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVTableBack(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVTableBack(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVTableBack(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVTableBack(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tablebackrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TABLEBACKRVFRM)
using namespace Tablebackrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TablebackrvfrmHPP
