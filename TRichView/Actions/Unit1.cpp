/*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Demo project for C++Builder 6-2007              }
{       You can use it as a basis for your              }
{       applications.                                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "dmActions.hpp"
#include "RVALocRuler.hpp"
//---------------------------------------------------------------------------
/*
  Uncomment the defines below to enable support for Gif and Png.

  GifImage (by Anders Melander)
    http://www.torry.net/vcl/graphics/gif/gifimage.exe (original)
    http://www.trichview.com/resources/thirdparty/gifimage.zip (update)
  Note: for C++Builder 2007+, use the built in TGifImage instead

  PngObject (by Gustavo Huffenbacher Daud)
    http://www.trichview.com/resources/thirdparty/pngimage.zip
*/
//#define USE_GIFIMAGE
//#define USE_PNGOBJECT

#ifdef USE_GIFIMAGE
#if __BORLANDC__>=0x0590
#include "GifImg.hpp"  // C++Builder 2007 or newer (using standard GIF class)
#else
#include "GifImage.hpp" // C++Builder 6-2006 (using thirdparty GIF class)
#endif
#endif

#ifdef USE_PNGOBJECT
#include "PngImage.hpp"
#endif
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PtblRV"
#pragma link "RichView"
#pragma link "RichViewActions"
#pragma link "Ruler"
#pragma link "RVEdit"
#pragma link "RVFontCombos"
#pragma link "RVRuler"
#pragma link "RVScroll"
#pragma link "RVStyle"
#pragma link "RVStyleFuncs"
#pragma resource "*.dfm"


TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
        : TForm(Owner)
{
// GIF -----------
#ifdef USE_GIFIMAGE
  RegisterClass(__classid(TGIFImage));
  RVGraphicHandler->RegisterHTMLGraphicFormat(__classid(TGIFImage));
#endif
// PNG -----------
#ifdef USE_PNGOBJECT
  // C++Builder 6-2007 (using thirdparty PNG class)
  RegisterClass(__classid(TPNGObject));
  RVGraphicHandler->RegisterHTMLGraphicFormat(__classid(TPNGObject));
  RVGraphicHandler->RegisterPngGraphic(__classid(TPNGObject));
#endif
}
//---------------------------------------------------------------------------

void __fastcall TForm3::FormCreate(TObject *Sender)
{
  cmbFont->Font->Name = RVAControlPanel1->DialogFontName;
  cmbFontSize->Font->Name = RVAControlPanel1->DialogFontName;  
  cmbUnits->Font->Name = RVAControlPanel1->DialogFontName;
  cmbStyles->Font->Name = RVAControlPanel1->DialogFontName;

  // linking combo boxes and style inspector to RichViewEdit1
  UpdateLinkedControls();  

  // Almost all these assignments could be done at design time in the Object Inspector
  // But in this demo we do not want to modify rvActionsResource
  // (and we recommend to use a copy of it in your applications)
  

  // initializing TrvActionEditNote action
  rvActionsResource->rvActionEditNote1->Control = RichViewEdit1;
  rvActionsResource->rvActionEditNote1->OnShowing = rvActionEditNoteShowing;
  rvActionsResource->rvActionEditNote1->OnFormCreate = rvActionEditNoteFormCreate;  
  rvActionsResource->rvActionEditNote1->OnHide = rvActionEditNoteHide;  

  rvActionsResource->rvActionSave1->OnDocumentFileChange = rvActionSave1DocumentFileChange;

  // Code for making color-picking buttons stay pressed while a
  // color-picker window is visible.
  rvActionsResource->rvActionColor1->OnShowColorPicker = ColorPickerShow;
  rvActionsResource->rvActionColor1->OnHideColorPicker = ColorPickerHide;
  rvActionsResource->rvActionParaColor1->OnShowColorPicker = ColorPickerShow;
  rvActionsResource->rvActionParaColor1->OnHideColorPicker = ColorPickerHide;
  rvActionsResource->rvActionFontColor1->OnShowColorPicker = ColorPickerShow;
  rvActionsResource->rvActionFontColor1->OnHideColorPicker = ColorPickerHide;
  rvActionsResource->rvActionFontBackColor1->OnShowColorPicker = ColorPickerShow;
  rvActionsResource->rvActionFontBackColor1->OnHideColorPicker = ColorPickerHide;

  // C++Builder 4 and 5 do not have ActionComponent property for actions.
  // Coloring actions have a substitution - CallerControl property
  // It is ignored in C++Builder 6+
  rvActionsResource->rvActionParaColor1->CallerControl = ToolButton39;
  rvActionsResource->rvActionFontBackColor1->CallerControl = ToolButton38;
  rvActionsResource->rvActionFontColor1->CallerControl = ToolButton36;

#if __BORLANDC__>0x0550
  // AutoComplete feature causes OnClick generation when editing combo-box's text.
  // Since in OnClick we move input focus in RichViewEdit1, this effect is
  // undesirable
  cmbFont->AutoComplete = false;
  cmbFontSize->AutoComplete = false;
#endif

  cmbUnits->ItemIndex = (int) RVAControlPanel1->UnitsDisplay;

  Localize();

  // Loading initial file via ActionOpen (allowing to update user interface)
  AnsiString FileName = ExtractFilePath(Application->ExeName)+"readme.rvf";
  if (!FileExists(FileName))
    FileName = ExtractFilePath(Application->ExeName)+"..\\readme.rvf";
  rvActionsResource->rvActionOpen1->LoadFile(RichViewEdit1, FileName, ffiRVF);

  /*
  // alternative way to start
    rvActionsResource->rvActionNew1->ExecuteTarget(RichViewEdit1);
  */
}
//---------------------------------------------------------------------------
/*                       Working with document                             */
//---------------------------------------------------------------------------
// When document is created, saved, loaded...
void __fastcall TForm3::rvActionSave1DocumentFileChange(TObject* Sender,
  TCustomRichViewEdit* Editor, const AnsiString FileName,
  TrvFileSaveFilter FileFormat, bool IsNew)
{
  AnsiString s = ExtractFileName(FileName);
  rvActionsResource->rvActionPrint1->Title = s;
  rvActionsResource->rvActionQuickPrint1->Title = s;
  if (IsNew)
    s += " (*)";
  Caption = s + " - RichViewActionsTest";
}
//---------------------------------------------------------------------------
// Prompt for saving...
void __fastcall TForm3::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  CanClose = rvActionsResource->rvActionSave1->CanCloseDoc(RichViewEdit1);
}
//---------------------------------------------------------------------------
void __fastcall TForm3::mitExitClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------
/*                  Working with color-picking buttons                     */
//---------------------------------------------------------------------------
// Code for making color-picking buttons pressed while
// a color-picker window is visible.
void __fastcall TForm3::ColorPickerShow(TObject* Sender)
{
#if __BORLANDC__>0x0550
  TComponent* ActionComponent = ((TAction*)Sender)->ActionComponent;
  if (ActionComponent->InheritsFrom(__classid(TToolButton)))
    ((TToolButton*)ActionComponent)->Down = true;
#else
  TControl* CallerControl = ((TrvActionCustomColor*)Sender)->CallerControl;
  if (CallerControl)
    ((TToolButton*)CallerControl)->Down = true;
#endif
}
//---------------------------------------------------------------------------
void __fastcall TForm3::ColorPickerHide(TObject* Sender)
{
#if __BORLANDC__>0x0550
  TComponent* ActionComponent = ((TAction*)Sender)->ActionComponent;
  if (ActionComponent->InheritsFrom(__classid(TToolButton)))
    ((TToolButton*)ActionComponent)->Down = false;
#else
  TControl* CallerControl = ((TrvActionCustomColor*)Sender)->CallerControl;
  if (CallerControl)
    ((TToolButton*)CallerControl)->Down = false;
#endif
}
//---------------------------------------------------------------------------
/*             Set of events for processing hypertext jumps                */
//---------------------------------------------------------------------------
// Hyperlink click
void __fastcall TForm3::RichViewEdit1Jump(TObject *Sender, int id)
{
  rvActionsResource->rvActionInsertHyperlink1->GoToLink(RichViewEdit1, id);
}
//---------------------------------------------------------------------------
// Inserting hyperlinks from files (RTF) and drag source
void __fastcall TForm3::RichViewEdit1ReadHyperlink(TCustomRichView *Sender,
      const AnsiString Target, const AnsiString Extras,
      TRVLoadFormat DocFormat, int &StyleNo, TRVTag &ItemTag,
      TRVRawByteString &ItemName)
{
  if (DocFormat==rvlfURL)
    StyleNo = rvActionsResource->rvActionInsertHyperlink1->GetHyperlinkStyleNo(RichViewEdit1);
  ItemTag = rvActionsResource->rvActionInsertHyperlink1->EncodeTarget(Target);
}
//---------------------------------------------------------------------------
// URL detection on typing
void __fastcall TForm3::RichViewEdit1KeyPress(TObject *Sender, char &Key)
{
  /*
  // uncomment if you use Addict3
  if (Key=='\'' || Key==13 || Key==9 || RichViewEdit1->Delimiters.Pos(Key))
    RVA_Addict3AutoCorrect(RichViewEdit1);
  */
  if (Key==13 || Key==9 || Key==' ' || Key==',' || Key==';')
  {
    rvActionsResource->rvActionInsertHyperlink1->DetectURL(RichViewEdit1);
    rvActionsResource->rvActionInsertHyperlink1->TerminateHyperlink(RichViewEdit1);
  }
}
//---------------------------------------------------------------------------
/*                     Insert table popup                                  */
//---------------------------------------------------------------------------
/* We use a trick: insert-table button has style tbsDropDown and assigned
  DropDownMenu (pmFakeDropDown). This menu is empty, but shows table size
  popup instead of itself */
void __fastcall TForm3::pmFakeDropDownPopup(TObject *Sender)
{
  rvActionsResource->rvActionInsertTable1->ShowTableSizeDialog(RichViewEdit1,
    ToolButton12);
}
//---------------------------------------------------------------------------
/*                             Ruler                                       */
//---------------------------------------------------------------------------
void __fastcall TForm3::RVAControlPanel1MarginsChanged(TrvAction *Sender,
      TCustomRichViewEdit *Edit)
{
  RVRuler1->UpdateRulerMargins();
}
//---------------------------------------------------------------------------
/*                           Localization                                  */
//---------------------------------------------------------------------------
void __fastcall TForm3::btnLanguageClick(TObject *Sender)
{
  if (RVA_ChooseLanguage())
    Localize();
}
//---------------------------------------------------------------------------
void TForm3::Localize()
{
  // Fonts
  Font->Charset             = RVA_GetCharset();
  StatusBar1->Font->Charset = RVA_GetCharset();
  cmbUnits->Font->Charset = RVA_GetCharset();
#if __BORLANDC__>0x0550
  Screen->HintFont->Charset = RVA_GetCharset();
  Screen->MenuFont->Charset = RVA_GetCharset();
#endif
  cmbStyles->Font->Charset = RVA_GetCharset();
  // Localizing all actions on rvActionsResource
  RVA_LocalizeForm(rvActionsResource);
  // Localizing all actions on this form
  RVA_LocalizeForm(this);
  // Localizing ruler
  RVALocalizeRuler(RVRuler1);
  // Localizing units
  int Idx = cmbUnits->ItemIndex;
  RVA_TranslateUnits(cmbUnits->Items);
  cmbUnits->ItemIndex = Idx;
  for (int i=0; i<CoolBar1->Bands->Count; i++)
    if (CoolBar1->Bands->Items[i]->Control == tbUnits)
      CoolBar1->Bands->Items[i]->Text = RVA_GetS(rvam_lbl_Units);
  // Styles
  rvActionsResource->rvActionStyleInspector1->UpdateInfo();
  cmbStyles->Localize();
  // Localizing menus
  mitFile->Caption   = RVA_GetS(rvam_menu_File);
  mitEdit->Caption   = RVA_GetS(rvam_menu_Edit);
  mitFont->Caption   = RVA_GetS(rvam_menu_Font);
  mitPara->Caption   = RVA_GetS(rvam_menu_Para);
  mitFormat->Caption = RVA_GetS(rvam_menu_Format);
  mitInsert->Caption = RVA_GetS(rvam_menu_Insert);
  mitTable->Caption  = RVA_GetS(rvam_menu_Table);
  mitExit->Caption   = RVA_GetS(rvam_menu_Exit);
  mitTextFlow->Caption   = RVA_GetS(rvam_menu_TextFlow);
  mitHelp->Caption   = RVA_GetS(rvam_menu_Help);
  mitHelpTOC->Caption   = RVA_GetS(rvam_menu_Help);

  mitFontSize->Caption = RVA_GetS(rvam_menu_FontSize);
  mitFontStyle->Caption = RVA_GetS(rvam_menu_FontStyle);
  mitTableSelect->Caption = RVA_GetS(rvam_menu_TableSelect);
  mitTableCellBorders->Caption = RVA_GetS(rvam_menu_TableCellBorders);
  mitTableAlignCellContents->Caption = RVA_GetS(rvam_menu_TableCellAlign);
  mitTableCellRotation->Caption = RVA_GetS(rvam_menu_TableCellRotation);  
  // In your application, you can use either TrvActionFonts or TrvActionFontEx
  rvActionsResource->rvActionFonts1->Caption = rvActionsResource->rvActionFonts1->Caption+" (Standard)";
  rvActionsResource->rvActionFontEx1->Caption = rvActionsResource->rvActionFontEx1->Caption+" (Advanced)";

  /*
  // uncomment if you use Addict3. It's assumed that RVAddictSpell31
  // and RVThesaurus31 are on this form.
  RVAddictSpell31->UILanguage = GetAddictSpellLanguage(RVA_GetLanguageName());
  RVThesaurus31->UILanguage = GetAddictThesLanguage(RVA_GetLanguageName());
  */
}
//---------------------------------------------------------------------------
/*                          Progress messages                              */
//---------------------------------------------------------------------------
// Downloading
void __fastcall TForm3::RVAControlPanel1Download(TrvAction *Sender,
      const AnsiString Source)
{
  if (Source.IsEmpty())
	Application->Hint = "";
  else
	Application->Hint = RVAFormat(RVA_GetS(rvam_msg_Downloading), ARRAYOFCONST((Source.c_str())));
}
//---------------------------------------------------------------------------
// Reading/writing
void __fastcall TForm3::RichViewEdit1Progress(TCustomRichView *Sender,
      TRVLongOperation Operation, TRVProgressStage Stage, BYTE PercentDone)
{
  switch (Stage)
  {
	case rvpstgStarting:
	  ProgressBar1->Left = btnLanguage->Left - ProgressBar1->Width - 8;
	  ProgressBar1->Top  = btnLanguage->Top + (btnLanguage->Height-ProgressBar1->Height) / 2;
	  ProgressBar1->Position = 0;
	  ProgressBar1->Visible = true;
	  Application->Hint = RVA_GetProgressMessage(Operation);
	  break;
	case rvpstgRunning:
	  ProgressBar1->Position = PercentDone;
	  ProgressBar1->Update();
	  break;
	case rvpstgEnding:
	  ProgressBar1->Position = 100;
	  ProgressBar1->Update();
	  Application->Hint = "";
	  ProgressBar1->Visible = false;
	  break;
  }
}
//---------------------------------------------------------------------------
// Printing
void __fastcall TForm3::RVPrint1SendingToPrinter(TCustomRichView *Sender,
      int PageCompleted, TRVPrintingStep Step)
{
  Application->Hint = RVA_GetPrintingMessage(PageCompleted, Step);
}
//---------------------------------------------------------------------------
/*                              Notes                                      */
//---------------------------------------------------------------------------
void __fastcall TForm3::rvActionEditNoteFormCreate(TrvAction* Sender, TForm* Form)
{
  rvActionsResource->rvActionEditNote1->SubDocEditor->OnEnter = NoteEditorEnter;
  rvActionsResource->rvActionEditNote1->SubDocEditor->OnExit = NoteEditorExit;
  rvActionsResource->rvActionEditNote1->SubDocEditor->PopupMenu = RVAPopupMenu1;
  Form->Align = alBottom;
  Form->Height = 100;
  Form->Parent = this;
}
//---------------------------------------------------------------------------
void __fastcall TForm3::rvActionEditNoteShowing(TrvAction* Sender, TForm* Form)
{
  RichViewEdit1->ForceFieldHighlight = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm3::rvActionEditNoteHide(TObject* Sender)
{
  RichViewEdit1->ForceFieldHighlight = false;
  if (RichViewEdit1->CanFocus())
    RichViewEdit1->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TForm3::NoteEditorEnter(TObject* Sender)
{
  RVAControlPanel1->DefaultControl = rvActionsResource->rvActionEditNote1->SubDocEditor;
  UpdateLinkedControls();
}
//---------------------------------------------------------------------------
void __fastcall TForm3::NoteEditorExit(TObject* Sender)
{
  RVAControlPanel1->DefaultControl = RichViewEdit1;
  UpdateLinkedControls();
}
//---------------------------------------------------------------------------
void TForm3::UpdateLinkedControls()
{
  rvActionsResource->rvActionStyleInspector1->Control = RVAControlPanel1->DefaultControl;
  cmbFont->Editor = RVAControlPanel1->DefaultControl;
  cmbFontSize->Editor = RVAControlPanel1->DefaultControl;
  cmbStyles->Editor = RVAControlPanel1->DefaultControl;
}
//---------------------------------------------------------------------------
/*                    Misc                                                 */
//---------------------------------------------------------------------------

void __fastcall TForm3::mitHelpTOCClick(TObject *Sender)
{
  TRVUnicodeString HelpFileName = ExtractFilePath(Application->ExeName)+
    "Help\\RichViewActions.chm";
  RvHtmlHelp(HelpFileName);
}
//---------------------------------------------------------------------------
/*                    Live spelling with Addict 3                          */
//---------------------------------------------------------------------------
/*
// Add these events if you use Addict3
// (Assuming that you have RVAddictSpell31: TRVAddictSpell3 on the form)

// RichViewEdit1->OnSpellingCheck event
void __fastcall TForm3::RichViewEdit1SpellingCheck(TCustomRichView* Sender,
  const AnsiString AWord, int StyleNo, bool& Misspelled)
{
  Misspelled = ! (RVAddictSpell31->WordAcceptable(AWord));
}

// RVAddictSpell31->OnParserIgnoreWord: if Ignore All or Add buttons were pressed
// in spellchecker dialog, removing underlines from the ignored word
void __fastcall TForm3::RVAddictSpell31ParserIgnoreWord(TObject* Sender,
  bool Before, int State)
{
  if (!Before && (State==IgnoreState_IgnoreAll || State==IgnoreState_Add))
    RichViewEdit1->LiveSpellingValidateWord(RVAddictSpell31->CurrentWord);
}

// Besides, if you want spelling check starting when loading document, call
// RichViewEdit1.StartLiveSpelling in rvActionFileOpen.OnOpenFile event
*/

//---------------------------------------------------------------------------
/*                    Units of measurement                                 */
//---------------------------------------------------------------------------
void __fastcall TForm3::cmbUnitsClick(TObject *Sender)
{

// --- 1. Units displayed to the user
  // units for displaying in dialogs
  RVAControlPanel1->UnitsDisplay = (TRVUnits)cmbUnits->ItemIndex;
  // units for displaying in the ruler
  RVRuler1->UnitsDisplay = (TRulerUnits)cmbUnits->ItemIndex;
  // units for page setup dialog
  if (RVAControlPanel1->UnitsDisplay==rvuPixels ||
      RVAControlPanel1->UnitsDisplay==rvuCentimeters ||
      RVAControlPanel1->UnitsDisplay==rvuMillimeters) 
    rvActionsResource->rvActionPageSetup1->MarginsUnits = rvpmuMillimeters;
  else
    rvActionsResource->rvActionPageSetup1->MarginsUnits = rvpmuInches;
// --- 2. units for internal measurement
  // units of measurement for RichViewActions
  if (RVAControlPanel1->UnitsDisplay==rvuPixels)
  {
    RVA_ConvertToPixels(rvActionsResource);
    RVAControlPanel1->UnitsProgram = rvstuPixels;
  }
  else
  {
    RVA_ConvertToTwips(rvActionsResource);
    RVAControlPanel1->UnitsProgram = rvstuTwips;
  }
  // units of measurement for the current document
  if (RVStyle1->Units != RVAControlPanel1->UnitsProgram)
  {
    RichViewEdit1->ConvertDocToDifferentUnits(RVAControlPanel1->UnitsProgram);
    RVStyle1->ConvertToDifferentUnits(RVAControlPanel1->UnitsProgram);
    RichViewEdit1->Change();
  }
  RichViewEdit1->SetFocus();
}

//---------------------------------------------------------------------------
