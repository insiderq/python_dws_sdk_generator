
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Ancestor for all RichViewActions forms.         }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit BaseRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, StdCtrls,
  ExtCtrls, CheckLst, Menus,
  Dialogs, Buttons, RVScroll, RVXPTheme,
  {$IFDEF RVACTIVEX}
  AxCtrls,
  {$ENDIF}
  {$IFDEF USERVKSDEVTE}
  te_controls, te_forms, te_extctrls, CRVPP, RVPP, RichView, te_scrolladapt,
  te_adapter, te_theme,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntStdCtrls, TntButtons, TntExtCtrls, TntComCtrls, TntForms, TntCheckLst,
  TntMenus,
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  Vcl.Themes,
  {$ENDIF}
  {$IFDEF RICHVIEWDEF4}
  ImgList,
  {$ENDIF}
  RVStyle, RichView, RVOfficeRadioBtn, ComCtrls, RVSpinEdit, RVALocalize;

type
  {$IFDEF USERVTNT}
  TfrmRVBaseBase = TTntForm;
  TRVATreeView = TTntTreeView;
  TRVATreeNode = TTntTreeNode;
  TRVADialogPopupMenu = TTntPopupMenu;
  {$ELSE}
  TfrmRVBaseBase = TForm;
  TRVATreeView = TTreeView;
  TRVATreeNode = TTreeNode;
  TRVADialogPopupMenu = TPopupMenu;
  {$ENDIF}

  TfrmRVBase = class(TfrmRVBaseBase)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure btnHelpClick(Sender: TObject);
  protected
    procedure SetUseXPThemes(const Value: Boolean); virtual;
    procedure LocalizeSpecialControls;
    function FindButtonWithModalResult(ModalResult: TModalResult): TButton;
    procedure AddHelpButton;
    function GetOkButton: TButton; dynamic;
    function GetCancelButton: TButton;
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); dynamic;
    procedure OnCreateThemedMenu(OldMenu, NewMenu: TComponent); dynamic;
    function IsThemeAllowedFor(Component: TComponent): Boolean; dynamic;
    procedure MoveChildren(OldControl, NewControl: TWinControl);
    {$ENDIF}
    {$IFDEF USERVKSDEVTE}
    function IsTeFormAllowed: Boolean; dynamic;
    procedure ApplyKSDevThemeEngine;
    {$ENDIF}
    {$IFDEF USERVTNT}
    procedure ApplyTntControls;
    {$ENDIF}
  public
    { Public declarations }
    {$IFDEF USERVKSDEVTE}
    tef: TTeForm;
    {$ENDIF}
    ControlPanel: TComponent;    
    constructor Create(AOwner: TComponent; AControlPanel: TComponent); reintroduce;
    procedure Localize; dynamic;
    function GetRadioButtonChecked(rb: TControl): Boolean;
    procedure SetRadioButtonChecked(rb: TControl; Checked: Boolean);
    function GetCheckBoxChecked(cb: TControl): Boolean;
    procedure SetCheckBoxChecked(cb: TControl; Checked: Boolean);
    function GetListBoxChecked(lst: TControl; ItemIndex: Integer): Boolean;
    procedure SetListBoxChecked(lst: TControl; ItemIndex: Integer; Checked: Boolean);
    function GetCheckBoxState(cb: TControl): TCheckBoxState;
    procedure SetCheckBoxState(cb: TControl; State: TCheckBoxState);
    procedure SetFormCaption(const Caption: TRVALocString);
    procedure SetControlCaption(Control: TControl; const Caption: TRVALocString);
    function GetEditText(Edit: TControl): TRVALocString;
    procedure SetEditSelText(Edit: TControl; const s: TRVALocString);
    function GetEditFont(Edit: TControl): TFont;
    function GetEditSelStart(Edit: TControl): Integer;
    function GetEditSelLength(Edit: TControl): Integer;
    procedure SetEditSelStart(Edit: TControl; Value: Integer);
    procedure SetEditSelLength(Edit: TControl; Value: Integer);
    function GetXBoxItemCount(Box: TControl): Integer;
    procedure ClearXBoxItems(Box: TControl);
    procedure XBoxItemsAddObject(Box: TControl; const s: TRVALocString; obj: TObject;
      SysLanguage: Boolean = False);
    function GetXBoxObject(Box: TControl; Index: Integer): TObject;
    procedure SetXBoxItems(Box: TControl; Items: TRVALocStrings);
    function GetXBoxItemIndex(Box: TControl): Integer;
    function CreateXBoxItemsCopy(Box: TControl): TRVALocStrings;
    function GetPageControlActivePageIndex(pc: TControl): Integer;
    function GetPageControlActivePage(pc: TControl): TWinControl;
    procedure SetPageControlActivePage(pc: TControl; Page: TControl);
    procedure SetXBoxItemIndex(Box: TControl; ItemIndex: Integer);
    procedure SetListBoxSorted(Box: TControl);
    procedure SetButtonDown(SpeedButton: TControl; Value: Boolean);
    function GetButtonDown(SpeedButton: TControl): Boolean;
    procedure SetButtonFlat(SpeedButton: TControl; Value: Boolean);
    function GetButtonFlat(SpeedButton: TControl): Boolean;
    function GetButtonClick(SpeedButton: TControl): TNotifyEvent;
    procedure HideTabSheet(ts: TControl);
    function IsTabSheetVisible(ts: TControl): Boolean;
    procedure UpdateLengthSpinEdit(se: TRVSpinEdit;
      FineUnits, OnlyPositive: Boolean);
    procedure UpdateLengthSpinEdit2(se: TRVSpinEdit; Units: TRVUnits;
      OnlyPositive: Boolean);
    procedure UpdatePercentSpinEdit(se: TRVSpinEdit; MinValue: Integer=0);
    procedure UpdateSpinEditEx(se: TRVSpinEdit; MinValue, MaxValue, Digits: Integer);
    property UseXPThemes: Boolean write SetUseXPThemes;
    function AllowInvertImageListImage(il: TCustomImageList;
      Index: Integer): Boolean; dynamic;
    procedure PrepareImageList(il: TCustomImageList);
    function GetMyClientSize(var Width, Height: Integer): Boolean; dynamic;
    procedure AdjustFormSize;
  end;

  TfrmRVBaseClass = class of TfrmRVBase;

function _GetWideString(const s: String; ControlPanel: TComponent): WideString;
function _GetString(const s: WideString; ControlPanel: TComponent): String;

procedure RVAAddRV(rv: TCustomRichView; const s: TRVALocString;
  StyleNo: Integer = 0; ParaNo: Integer = 0; const Tag: TRVTag = RVEMPTYTAG);

implementation
uses RichViewActions, RVColorCombo, RVColorGrid, ColorRVFrm, RVUni,
  RVFuncs, RVColorTransform;

{$R *.dfm}

function _GetWideString(const s: String; ControlPanel: TComponent): WideString;
begin
  {$IFDEF RVUNICODESTR}
  Result := s;
  {$ELSE}
  if ControlPanel=nil then
    ControlPanel := MainRVAControlPanel;
  if RVA_GetCharset(ControlPanel)=DEFAULT_CHARSET then
    Result := UTF8Decode(s)
  else
    Result := RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(
      RVU_Charset2CodePage(RVA_GetCharset(ControlPanel)), s));
  {$ENDIF}
end;

function _GetString(const s: WideString; ControlPanel: TComponent): String;
begin
  {$IFDEF RVUNICODESTR}
  Result := s;
  {$ELSE}
  if ControlPanel=nil then
    ControlPanel := MainRVAControlPanel;
  if RVA_GetCharset(ControlPanel)=DEFAULT_CHARSET then
    Result := String(UTF8Decode(s))
  else
    Result := RVU_UnicodeToAnsi(RVU_Charset2CodePage(RVA_GetCharset(ControlPanel)),
      RVU_GetRawUnicode(s));
  {$ENDIF}
end;


{ TfrmRVBase }

function GetRevBiDiModeAlignment(Alignment: TAlignment): TAlignment;
begin
  case Alignment of
    taLeftJustify:  Result := taRightJustify;
    taRightJustify: Result := taLeftJustify;
    else Result := Alignment;
  end;
end;

constructor TfrmRVBase.Create(AOwner: TComponent; AControlPanel: TComponent);
begin
  ControlPanel := AControlPanel;
  inherited Create(AOwner);
end;

procedure TfrmRVBase.SetUseXPThemes(const Value: Boolean);
var i: Integer;
begin
  for i := 0 to ComponentCount -1 do
    if Components[i] is TRVScroller then
      TRVScroller(Components[i]).UseXPThemes := Value
    else if Components[i] is TRVOfficeRadioButton then
      TRVOfficeRadioButton(Components[i]).UseXPThemes := Value
    else if Components[i] is TRVOfficeRadioGroup then
      TRVOfficeRadioGroup(Components[i]).UseXPThemes := Value
    else if Components[i] is TRVColorCombo then
      TRVColorCombo(Components[i]).UseXPThemes := Value
    else if Components[i] is TRVColorGrid then
      TRVColorGrid(Components[i]).Flat := Value and
       Assigned(RV_IsAppThemed) and RV_IsAppThemed and RV_IsThemeActive;
end;

procedure TfrmRVBase.FormCreate(Sender: TObject);
var i: Integer;
    Help: String;
begin
  Font.Charset := RVA_GetCharset(ControlPanel);
  if Screen.Fonts.IndexOf(TRVAControlPanel(ControlPanel).DialogFontName)>=0 then
    Font.Name := TRVAControlPanel(ControlPanel).DialogFontName;
  Font.Size := TRVAControlPanel(ControlPanel).DialogFontSize;
  UseXPThemes := TRVAControlPanel(ControlPanel).UseXPThemes;
  for i := 0 to ComponentCount-1 do
    if Components[i] is TEdit then
       TEdit(Components[i]).Font.Charset := DEFAULT_CHARSET
    else if (Components[i] is TComboBox) and (TComboBox(Components[i]).Style=csDropDown) then
       TComboBox(Components[i]).Font.Charset := DEFAULT_CHARSET
    else if Components[i] is TRVColorCombo then
      TRVColorCombo(Components[i]).ControlPanel := ControlPanel
    else if Components[i] is TRVColorGrid then
      TRVColorGrid(Components[i]).ControlPanel := ControlPanel;
  {
  for i := 0 to ComponentCount-1 do
    if Components[i] is TRVOfficeRadioButton then
      TRVOfficeRadioButton(Components[i]).Font.Charset := RVA_GetCharset
    else if Components[i] is TRVOfficeRadioGroup then
      TRVOfficeRadioGroup(Components[i]).Font.Charset := RVA_GetCharset;
  }
  AdjustFormSize;
  if RVA_GetCharset(ControlPanel) in [ARABIC_CHARSET, HEBREW_CHARSET] then begin
    BiDiMode := bdRightToLeft;
    FlipChildren(True);
    if not SysLocale.MiddleEast then
      for i := 0 to ComponentCount-1 do
        if Components[i] is TCustomLabel then
          TLabel(Components[i]).Alignment := GetRevBiDiModeAlignment(TLabel(Components[i]).Alignment)
        else if Components[i] is TCustomCheckBox then
          TCheckBox(Components[i]).Alignment := GetRevBiDiModeAlignment(TCheckBox(Components[i]).Alignment)
        else if Components[i] is TRadioButton then
          TRadioButton(Components[i]).Alignment := GetRevBiDiModeAlignment(TRadioButton(Components[i]).Alignment)
  end;
  Localize;
  Help := RVA_GetHelpFile(ControlPanel);
  if Help<>'' then begin
    HelpFile := ExtractFilePath(Application.ExeName)+Help;
    AddHelpButton;
  end;
  {$IFDEF USERVKSDEVTE}
  ApplyKSDevThemeEngine;
  {$ENDIF}
  {$IFDEF USERVTNT}
  ApplyTntControls;
  {$ENDIF}
  {$IFDEF RVACTIVEX}
  ParentWindow := ParkingWindow;
  {$ENDIF}
end;

procedure TfrmRVBase.Localize;
begin
 LocalizeSpecialControls;
end;

procedure TfrmRVBase.LocalizeSpecialControls;
var i: Integer;
begin
  for i := 0 to ComponentCount-1 do
    if Components[i] is TRVColorCombo then begin
      TRVColorCombo(Components[i]).TransparentCaption := RVA_GetS(rvam_cl_Transparent,ControlPanel);
      TRVColorCombo(Components[i]).AutoCaption := RVA_GetS(rvam_cl_Auto, ControlPanel);
    end;
end;

function TfrmRVBase.FindButtonWithModalResult(
  ModalResult: TModalResult): TButton;
var i: Integer;
begin
  for i := 0 to ComponentCount-1 do
    if (Components[i] is TButton) and
       (TButton(Components[i]).ModalResult=ModalResult) then begin
      Result := TButton(Components[i]);
      exit;
    end;
  Result := nil;
end;

procedure TfrmRVBase.AddHelpButton;
var btnOk, btnCancel, btnHelp: TButton;
begin
  btnOk := GetOkButton;
  if btnOk=nil then
    exit;
  btnCancel := GetCancelButton;
  if btnCancel=nil then
    exit;
  btnHelp := TButton.Create(Self);
  btnHelp.Caption := RVA_GetS(rvam_btn_Help, ControlPanel);
  with btnCancel do
    btnHelp.SetBounds(Left, Top, Width, Height);
  with btnOk do
    btnCancel.SetBounds(Left, Top, Width, Height);
  btnOk.Left := btnCancel.Left - (btnHelp.Left-btnCancel.Left);
  btnOk.Top := btnCancel.Top - (btnHelp.Top-btnCancel.Top);
  btnHelp.Parent := Self;
  btnHelp.TabOrder := btnCancel.TabOrder+1;
  btnHelp.OnClick := btnHelpClick;
end;

function TfrmRVBase.GetOkButton: TButton;
begin
  Result := FindButtonWithModalResult(mrOk);
end;

function TfrmRVBase.GetCancelButton: TButton;
begin
  Result := FindButtonWithModalResult(mrCancel);
end;

procedure TfrmRVBase.btnHelpClick(Sender: TObject);
begin
  Application.HelpContext(HelpContext);
end;

function TfrmRVBase.GetRadioButtonChecked(rb: TControl): Boolean;
begin
  {$IFDEF USERVKSDEVTE}
  Result := (rb as TTeRadioButton).Checked;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := (rb as TTntRadioButton).Checked;
  {$ELSE}
  Result := (rb as TRadioButton).Checked;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetRadioButtonChecked(rb: TControl; Checked: Boolean);
begin
  {$IFDEF USERVKSDEVTE}
  (rb as TTeRadioButton).Checked := Checked;
  {$ELSE}
  {$IFDEF USERVTNT}
  (rb as TTntRadioButton).Checked := Checked;
  {$ELSE}
  (rb as TRadioButton).Checked := Checked;;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetCheckBoxChecked(cb: TControl): Boolean;
begin
  {$IFDEF USERVKSDEVTE}
  Result := (cb as TTeCheckBox).Checked;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := (cb as TTntCheckBox).Checked;
  {$ELSE}
  Result := (cb as TCheckBox).Checked;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetListBoxChecked(lst: TControl; ItemIndex: Integer): Boolean;
begin
  {$IFDEF USERVTNT}
  Result := (lst as TTntCheckListBox).Checked[ItemIndex];
  {$ELSE}
  Result := (lst as TCheckListBox).Checked[ItemIndex];
  {$ENDIF}
end;


procedure TfrmRVBase.SetListBoxChecked(lst: TControl; ItemIndex: Integer; Checked: Boolean);
begin
  {$IFDEF USERVTNT}
  (lst as TTntCheckListBox).Checked[ItemIndex] := Checked;
  {$ELSE}
  (lst as TCheckListBox).Checked[ItemIndex] := Checked;
  {$ENDIF}
end;

procedure TfrmRVBase.SetCheckBoxChecked(cb: TControl; Checked: Boolean);
begin
  {$IFDEF USERVKSDEVTE}
  (cb as TTeCheckBox).Checked := Checked;
  {$ELSE}
  {$IFDEF USERVTNT}
  (cb as TTntCheckBox).Checked := Checked;
  {$ELSE}
  (cb as TCheckBox).Checked := Checked;;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetCheckBoxState(cb: TControl): TCheckBoxState;
begin
  {$IFDEF USERVKSDEVTE}
  Result := TCheckBoxState((cb as TTeCheckBox).State);
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := (cb as TTntCheckBox).State;
  {$ELSE}
  Result := (cb as TCheckBox).State;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetCheckBoxState(cb: TControl; State: TCheckBoxState);
begin
  {$IFDEF USERVKSDEVTE}
  (cb as TTeCheckBox).State := TTeCheckBoxState(State);
  {$ELSE}
  {$IFDEF USERVTNT}
  (cb as TTntCheckBox).State := State;
  {$ELSE}
  (cb as TCheckBox).State := State;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetXBoxItemIndex(Box: TControl): Integer;
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    Result := (Box as TTeComboBox).ItemIndex
  else if Box is TTeListBox then
    Result := (Box as TTeListBox).ItemIndex
  else
    Result := (Box as TTeRadioGroup).ItemIndex
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    Result := (Box as TTntComboBox).ItemIndex
  else if Box is TTntListBox then
    Result := (Box as TTntListBox).ItemIndex
  else if Box is TTntCheckListBox then
    Result := (Box as TTntCheckListBox).ItemIndex
  else
    Result := (Box as TTntRadioGroup).ItemIndex
  {$ELSE}
  if Box is TComboBox then
    Result := (Box as TComboBox).ItemIndex
  else if Box is TCustomListBox then
    Result := (Box as TCustomListBox).ItemIndex
  else
    Result := (Box as TRadioGroup).ItemIndex
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.CreateXBoxItemsCopy(Box: TControl): TRVALocStrings;
begin
  Result := TRVALocStringList.Create;
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    Result.Assign((Box as TTeComboBox).Items)
  else if Box is TTeListBox then
    Result.Assign((Box as TTeListBox).Items)
  else
    Result.Assign((Box as TTeRadioGroup).Items)
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    Result.Assign((Box as TTntComboBox).Items)
  else if Box is TTntListBox then
    Result.Assign((Box as TTntListBox).Items)
  else if Box is TTntCheckListBox then
    Result.Assign((Box as TTntCheckListBox).Items)
  else
    Result.Assign((Box as TTntRadioGroup).Items)
  {$ELSE}
  if Box is TComboBox then
    Result.Assign((Box as TComboBox).Items)
  else if Box is TCustomListBox then
    Result.Assign((Box as TCustomListBox).Items)
  else
    Result.Assign((Box as TRadioGroup).Items)
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetXBoxObject(Box: TControl; Index: Integer): TObject;
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    Result := (Box as TTeComboBox).Items.Objects[Index]
  else if Box is TTeListBox then
    Result := (Box as TTeListBox).Items.Objects[Index]
  else
    Result := (Box as TTeRadioGroup).Items.Objects[Index]
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    Result := (Box as TTntComboBox).Items.Objects[Index]
  else if Box is TTntListBox then
    Result := (Box as TTntListBox).Items.Objects[Index]
  else if Box is TTntCheckListBox then
    Result := (Box as TTntCheckListBox).Items.Objects[Index]
  else
    Result := (Box as TTntRadioGroup).Items.Objects[Index]
  {$ELSE}
  if Box is TComboBox then
    Result := (Box as TComboBox).Items.Objects[Index]
  else if Box is TCustomListBox then
    Result := (Box as TCustomListBox).Items.Objects[Index]
  else
    Result := (Box as TRadioGroup).Items.Objects[Index]
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetXBoxItems(Box: TControl; Items: TRVALocStrings);
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    (Box as TTeComboBox).Items := Items
  else if Box is TTeListBox then
    (Box as TTeListBox).Items := Items
  else
    (Box as TTeRadioGroup).Items := Items
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    (Box as TTntComboBox).Items.Assign(Items)
  else if Box is TTntListBox then
    (Box as TTntListBox).Items.Assign(Items)
  else if Box is TTntCheckListBox then
    (Box as TTntCheckListBox).Items.Assign(Items)
  else
    (Box as TTntRadioGroup).Items.Assign(Items)
  {$ELSE}
  if Box is TComboBox then
    (Box as TComboBox).Items := Items
  else if Box is TCustomListBox then
    (Box as TCustomListBox).Items := Items
  else
    (Box as TRadioGroup).Items := Items
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetXBoxItemCount(Box: TControl): Integer;
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    Result := (Box as TTeComboBox).Items.Count
  else if Box is TTeListBox then
    Result := (Box as TTeListBox).Items.Count
  else
    Result := (Box as TTeRadioGroup).Items.Count
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    Result := (Box as TTntComboBox).Items.Count
  else if Box is TTntListBox then
    Result := (Box as TTntListBox).Items.Count
  else if Box is TTntCheckListBox then
    Result := (Box as TTntCheckListBox).Items.Count
  else
    Result := (Box as TTntRadioGroup).Items.Count
  {$ELSE}
  if Box is TComboBox then
    Result := (Box as TComboBox).Items.Count
  else if Box is TCustomListBox then
    Result := (Box as TCustomListBox).Items.Count
  else
    Result := (Box as TRadioGroup).Items.Count
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.ClearXBoxItems(Box: TControl);
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    (Box as TTeComboBox).Items.Clear
  else if Box is TTeListBox then
    (Box as TTeListBox).Items.Clear
  else
    (Box as TTeRadioGroup).Items.Clear
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    (Box as TTntComboBox).Items.Clear
  else if Box is TTntListBox then
    (Box as TTntListBox).Items.Clear
  else if Box is TTntCheckListBox then
    (Box as TTntCheckListBox).Items.Clear
  else
    (Box as TTntRadioGroup).Items.Clear
  {$ELSE}
  if Box is TComboBox then
    (Box as TComboBox).Items.Clear
  else if Box is TCustomListBox then
    (Box as TCustomListBox).Items.Clear
  else
    (Box as TRadioGroup).Items.Clear
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.XBoxItemsAddObject(Box: TControl; const s: TRVALocString;
  obj: TObject; SysLanguage: Boolean = False);
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    (Box as TTeComboBox).Items.AddObject(s, obj)
  else if Box is TTeListBox then
    (Box as TTeListBox).Items.AddObject(s, obj)
  else
    (Box as TTeRadioGroup).Items.AddObject(s, obj)
  {$ELSE}
  {$IFDEF USERVTNT}
  if SysLanguage then begin
    if Box is TTntComboBox then
      (Box as TTntComboBox).Items.AddObject(s, obj)
    else if Box is TTntListBox then
      (Box as TTntListBox).Items.AddObject(s, obj)
    else if Box is TTntCheckListBox then
      (Box as TTntCheckListBox).Items.AddObject(s, obj)
    else
      (Box as TTntRadioGroup).Items.AddObject(s, obj)
    end
  else begin
    if Box is TTntComboBox then
      (Box as TTntComboBox).Items.AddObject(s, obj)
    else if Box is TTntListBox then
      (Box as TTntListBox).Items.AddObject(s, obj)
    else if Box is TTntCheckListBox then
      (Box as TTntCheckListBox).Items.AddObject(s, obj)
    else
      (Box as TTntRadioGroup).Items.AddObject(s, obj)
  end;
  {$ELSE}
  if Box is TComboBox then
    (Box as TComboBox).Items.AddObject(s, obj)
  else if Box is TCustomListBox then
    (Box as TCustomListBox).Items.AddObject(s, obj)
  else
    (Box as TRadioGroup).Items.AddObject(s, obj)
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetXBoxItemIndex(Box: TControl;
  ItemIndex: Integer);
begin
  {$IFDEF USERVKSDEVTE}
  if Box is TTeComboBox then
    (Box as TTeComboBox).ItemIndex := ItemIndex
  else if Box is TTeListBox then
    (Box as TTeListBox).ItemIndex := ItemIndex
  else
    (Box as TTeRadioGroup).ItemIndex := ItemIndex;
  {$ELSE}
  {$IFDEF USERVTNT}
  if Box is TTntComboBox then
    (Box as TTntComboBox).ItemIndex := ItemIndex
  else if Box is TTntListBox then
    (Box as TTntListBox).ItemIndex := ItemIndex
  else if Box is TTntCheckListBox then
    (Box as TTntCheckListBox).ItemIndex := ItemIndex
  else
    (Box as TTntRadioGroup).ItemIndex := ItemIndex
  {$ELSE}
  if Box is TComboBox then
    (Box as TComboBox).ItemIndex := ItemIndex
  else if Box is TCustomListBox then
    (Box as TCustomListBox).ItemIndex := ItemIndex
  else
    (Box as TRadioGroup).ItemIndex := ItemIndex;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetListBoxSorted(Box: TControl);
begin
  {$IFDEF USERVKSDEVTE}
  (Box as TTeListBox).Sorted := True;
  {$ELSE}
  {$IFDEF USERVTNT}
  (Box as TTntListBox).Sorted := True;
  {$ELSE}
  if Box is TListBox then
    (Box as TListBox).Sorted := True;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetButtonDown(SpeedButton: TControl; Value: Boolean);
begin
  {$IFDEF USERVTNT}
  (SpeedButton as TTntSpeedButton).Down := Value;
  {$ELSE}
  (SpeedButton as TSpeedButton).Down := Value;
  {$ENDIF}
end;

function TfrmRVBase.GetButtonDown(SpeedButton: TControl): Boolean;
begin
  {$IFDEF USERVTNT}
  Result := (SpeedButton as TTntSpeedButton).Down;
  {$ELSE}
  Result := (SpeedButton as TSpeedButton).Down;
  {$ENDIF}
end;

procedure TfrmRVBase.SetButtonFlat(SpeedButton: TControl; Value: Boolean);
begin
  {$IFDEF USERVTNT}
  (SpeedButton as TTntSpeedButton).Flat := Value;
  {$ELSE}
  (SpeedButton as TSpeedButton).Flat := Value;
  {$ENDIF}
end;

function TfrmRVBase.GetButtonFlat(SpeedButton: TControl): Boolean;
begin
  {$IFDEF USERVTNT}
  Result := (SpeedButton as TTntSpeedButton).Flat;
  {$ELSE}
  Result := (SpeedButton as TSpeedButton).Flat;
  {$ENDIF}
end;

function TfrmRVBase.GetButtonClick(SpeedButton: TControl): TNotifyEvent;
begin
  {$IFDEF USERVTNT}
  Result := (SpeedButton as TTntSpeedButton).OnClick;
  {$ELSE}
  Result := (SpeedButton as TSpeedButton).OnClick;
  {$ENDIF}
end;

function TfrmRVBase.GetPageControlActivePageIndex(pc: TControl): Integer;
begin
  {$IFDEF USERVKSDEVTE}
  Result := (pc as TTePageControl).ActivePage.PageIndex;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := (pc as TTntPageControl).ActivePage.PageIndex;
  {$ELSE}
  Result := (pc as TPageControl).ActivePage.PageIndex;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetPageControlActivePage(pc: TControl): TWinControl;
begin
  {$IFDEF USERVKSDEVTE}
  Result := (pc as TTePageControl).ActivePage;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := (pc as TTntPageControl).ActivePage;
  {$ELSE}
  Result := (pc as TPageControl).ActivePage;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetPageControlActivePage(pc: TControl; Page: TControl);
begin
  {$IFDEF USERVKSDEVTE}
  (pc as TTePageControl).ActivePage := TTeTabSheet(Page);
  {$ELSE}
  {$IFDEF USERVTNT}
  (pc as TTntPageControl).ActivePage := TTntTabSheet(Page);
  {$ELSE}
  (pc as TPageControl).ActivePage := TTabSheet(Page);
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.HideTabSheet(ts: TControl);
begin
  {$IFDEF USERVKSDEVTE}
  (ts as TTeTabSheet).PageVisible := False;
  {$ELSE}
  {$IFDEF USERVTNT}
  (ts as TTntTabSheet).TabVisible := False;
  {$ELSE}
  (ts as TTabSheet).TabVisible := False;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.IsTabSheetVisible(ts: TControl): Boolean;
begin
  {$IFDEF USERVKSDEVTE}
  Result := (ts as TTeTabSheet).PageVisible;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := (ts as TTntTabSheet).TabVisible;
  {$ELSE}
  Result := (ts as TTabSheet).TabVisible;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetFormCaption(const Caption: TRVALocString);
begin
  {$IFDEF USERVKSDEVTE}
  tef.Caption := _GetWideString(Caption);
  {$ELSE}
  {$IFDEF USERVTNT}
  Self.Caption := Caption;
  {$ELSE}
  Self.Caption := Caption;
  {$ENDIF}
  {$ENDIF}
end;

type
  TControlHack = class (TControl)
    public
      property Caption;
  end;

procedure TfrmRVBase.SetControlCaption(Control: TControl;
  const Caption: TRVALocString);
begin
  {$IFDEF USERVKSDEVTE}
  if Control is TTeGroupBox then
    TTeGroupBox(Control).Caption := _GetWideString(Caption)
  else if Control is TTeButton then
    TTeButton(Control).Caption := _GetWideString(Caption)
  else  if Control is TTeRadioButton then
    TTeRadioButton(Control).Caption := _GetWideString(Caption)
  else if Control is TTeCheckbox then
    TTeCheckbox(Control).Caption := _GetWideString(Caption)
  else if Control is TTeLabel then
    TTeLabel(Control).Caption := _GetWideString(Caption)
  else if Control is TTeComboBox then
    TTeComboBox(Control).Text := _GetWideString(Caption)
  else if Control is TTeEdit then
    TTeEdit(Control).Text := _GetWideString(Caption);
  {$ELSE}
  {$IFDEF USERVTNT}
  if Control is TTntGroupBox then
    TTntGroupBox(Control).Caption := Caption
  else if Control is TTntButton then
    TTntButton(Control).Caption := Caption
  else if Control is TTntRadioButton then
    TTntRadioButton(Control).Caption := Caption
  else if Control is TTntCheckbox then
    TTntCheckbox(Control).Caption := Caption
  else if Control is TTntLabel then
    TTntLabel(Control).Caption := Caption
  else if Control is TTntComboBox then
    TTntComboBox(Control).Text := Caption
  else if Control is TTntEdit then
    TTntEdit(Control).Text := Caption;
  {$ELSE}
  if Control is TComboBox then
    TComboBox(Control).Text := Caption
  else if Control is TEdit then
    TEdit(Control).Text := Caption
  else
    TControlHack(Control).Caption := Caption;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetEditText(Edit: TControl): TRVALocString;
begin
  {$IFDEF USERVKSDEVTE}
  if Edit is TTeComboBox then
    Result := TTeComboBox(Edit).Text
  else
    Result := TTeEdit(Edit).Text;
  {$ELSE}
  {$IFDEF USERVTNT}
  if Edit is TTntComboBox then
    Result := TTntComboBox(Edit).Text
  else
    Result := TTntEdit(Edit).Text;
  {$ELSE}
  if Edit is TComboBox then
    Result := TComboBox(Edit).Text
  else
    Result := TEdit(Edit).Text;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
   Result := False;
end;

function TfrmRVBase.GetEditFont(Edit: TControl): TFont;
begin
  {$IFDEF USERVKSDEVTE}
  Result := TTeEdit(Edit).Font;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := TTntEdit(Edit).Font;
  {$ELSE}
  Result := TEdit(Edit).Font;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetEditSelLength(Edit: TControl): Integer;
begin
  {$IFDEF USERVKSDEVTE}
  Result := TTeEdit(Edit).SelLength;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := TTntEdit(Edit).SelLength;
  {$ELSE}
  Result := TEdit(Edit).SelLength;
  {$ENDIF}
  {$ENDIF}
end;

function TfrmRVBase.GetEditSelStart(Edit: TControl): Integer;
begin
  {$IFDEF USERVKSDEVTE}
  Result := TTeEdit(Edit).SelStart;
  {$ELSE}
  {$IFDEF USERVTNT}
  Result := TTntEdit(Edit).SelStart;
  {$ELSE}
  Result := TEdit(Edit).SelStart;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetEditSelLength(Edit: TControl; Value: Integer);
begin
  {$IFDEF USERVKSDEVTE}
  TTeEdit(Edit).SelLength := Value;
  {$ELSE}
  {$IFDEF USERVTNT}
  TTntEdit(Edit).SelLength := Value;
  {$ELSE}
  TEdit(Edit).SelLength := Value;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetEditSelStart(Edit: TControl; Value: Integer);
begin
  {$IFDEF USERVKSDEVTE}
  TTeEdit(Edit).SelStart := Value;
  {$ELSE}
  {$IFDEF USERVTNT}
  TTntEdit(Edit).SelStart := Value;
  {$ELSE}
  TEdit(Edit).SelStart := Value;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.SetEditSelText(Edit: TControl; const s: TRVALocString);
begin
  {$IFDEF USERVKSDEVTE}
  TTeEdit(Edit).InsertText(s);
  {$ELSE}
  {$IFDEF USERVTNT}
  TTntEdit(Edit).SelText := s;
  {$ELSE}
  TEdit(Edit).SelText := s;
  {$ENDIF}
  {$ENDIF}
end;

procedure TfrmRVBase.UpdateLengthSpinEdit2(se: TRVSpinEdit; Units: TRVUnits;
  OnlyPositive: Boolean);
begin
  se.IntegerValue := Units = rvuPixels;
  case Units of
    rvuPixels:
      begin
        se.Digits := 0;
        se.Increment := 1;
        se.MaxValue := 2100;
      end;
    rvuInches:
      begin
        se.Digits := 3;
        se.Increment := 0.1;
        se.MaxValue := 22;
      end;
    rvuCentimeters:
      begin
        se.Digits := 3;
        se.Increment := 0.1;
        se.MaxValue := 55;
      end;
    rvuMillimeters:
      begin
        se.Digits := 2;
        se.Increment := 1;
        se.MaxValue := 550;
      end;
    rvuPicas:
      begin
        se.Digits := 2;
        se.Increment := 1;
        se.MaxValue := 125;
      end;
    rvuPoints:
      begin
        se.Digits := 2;
        se.Increment := 1;
        se.MaxValue := 1500;
      end;
  end;
  if OnlyPositive then
    se.MinValue := 0
  else
    se.MinValue := -se.MaxValue;
end;

procedure TfrmRVBase.UpdateLengthSpinEdit(se: TRVSpinEdit;
  FineUnits, OnlyPositive: Boolean);
var Units: TRVUnits;
begin
  if FineUnits then
    Units := RVA_GetFineUnits(TRVAControlPanel(ControlPanel))
  else
    Units := TRVAControlPanel(ControlPanel).UnitsDisplay;
  UpdateLengthSpinEdit2(se, Units, OnlyPositive);
end;

procedure TfrmRVBase.UpdatePercentSpinEdit(se: TRVSpinEdit; MinValue: Integer);
begin
  UpdateSpinEditEx(se, MinValue, 100, 0);
end;

procedure TfrmRVBase.UpdateSpinEditEx(se: TRVSpinEdit; MinValue, MaxValue, Digits: Integer);
begin
  se.IntegerValue := Digits=0;
  se.Digits := Digits;
  se.MinValue := MinValue;
  se.MaxValue := MaxValue;
  se.Increment := 1;
end;


{$IFDEF RVASKINNED}
function CompareTabOrders(Item1, Item2: Pointer): Integer;
begin
  if TControl(Item1) is TWinControl then
    if TControl(Item2) is TWinControl then
      Result := TWinControl(Item1).TabOrder-TWinControl(Item2).TabOrder
    else
      Result := 1
  else
    if TControl(Item2) is TWinControl then
      Result := -1
    else
      Result := 0;
end;

procedure TfrmRVBase.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin

end;

procedure TfrmRVBase.OnCreateThemedMenu(OldMenu, NewMenu: TComponent);
begin

end;

function TfrmRVBase.IsThemeAllowedFor(Component: TComponent): Boolean;
begin
  Result := True;
end;

procedure TfrmRVBase.MoveChildren(OldControl, NewControl: TWinControl);
var i: Integer;
    Children: TList;
begin
  Children := TList.Create;
  try
    for i := OldControl.ControlCount-1 downto 0 do
      Children.Add(OldControl.Controls[i]);
    Children.Sort(CompareTabOrders);
    for i := 0 to Children.Count-1 do
      TControl(Children[i]).Parent := NewControl;
  finally
    Children.Free;
  end;
end;

{$ENDIF}


{$IFDEF USERVKSDEVTE}
function TfrmRVBase.IsTeFormAllowed: Boolean;
begin
  Result := True;
end;

procedure TfrmRVBase.ApplyKSDevThemeEngine;
var teb: TTeButton;
    terb: TTeRadioButton;
    tecb: TTeCheckBox;
    tegb: TTeGroupBox;
    tecmb: TTeComboBox;
    tetxt: TTeEdit;
    tepc: TTePageControl;
    tets: TTeTabSheet;
    telbl: TTeLabel;
    terg: TTeRadioGroup;
    telst: TTeListBox;
    i, j: Integer;
    DialogBorder: Boolean;
    ATabOrder: Integer;
    ScrollAdapter: TTeaScrollAdapter;
begin
   ScrollAdapter := nil;
   if IsTeFormAllowed then begin
     DialogBorder := BorderStyle=bsDialog;
     tef := TTeForm.Create(Self);
     if DialogBorder then begin
       BorderStyle := bsSizeable;
       tef.BorderStyle := kbsDialog;
       tef.Dragging.Sizeable := False;
     end;
     tef.BorderIcons := tef.BorderIcons - [kbiMaximize, kbiMinimize, kbiRollUp];
     RVA_Events.DoOnCreateTeForm(Self, tef);
     tef.Loaded;
   end;
   for i := ComponentCount-1 downto 0 do begin
     if not IsThemeAllowedFor(Components[i]) then
       continue;
     if Components[i] is TButton then
       with TButton(Components[i]) do begin
         teb := TTeButton.Create(Self);
         teb.SetBounds(Left,Top,Width,Height);
         teb.Caption := _GetWideString(Caption);
         teb.OnClick := OnClick;
         teb.ModalResult := ModalResult;
         if Self.Components[i] is TBitBtn then
          teb.Glyph := TBitBtn(Self.Components[i]).Glyph;
         teb.Hint := Hint;
         teb.ShowHint := ShowHint;
         teb.Enabled := Enabled;
         ATabOrder := TabOrder;
         teb.Default := Default;
         teb.Cancel := Cancel;
         teb.Parent := Parent;
         teb.Visible := Visible;
         OnCreateThemedControl(TControl(Self.Components[i]), teb);
         Free;
         teb.Loaded;
         teb.TabOrder := ATabOrder;
       end
     else if Components[i].ClassName= 'TRadioButton' then
       with TRadioButton(Components[i]) do begin
         terb := TTeRadioButton.Create(Self);
         terb.SetBounds(Left,Top,Width,Height);
         terb.Caption := _GetWideString(Caption);
         terb.Enabled := Enabled;
         terb.Visible := Visible;
         terb.Checked := Checked;
         terb.OnClick := OnClick;
         terb.Hint := Hint;
         terb.ShowHint := ShowHint;
         ATabOrder := TabOrder;
         terb.Parent := Parent;
         OnCreateThemedControl(TControl(Self.Components[i]), terb);
         Free;
         terb.Loaded;
         terb.TabOrder := ATabOrder;
       end
     else if Components[i] is TCheckBox then
       with TCheckBox(Components[i]) do begin
         tecb := TTeCheckBox.Create(Self);
         tecb.SetBounds(Left,Top,Width,Height);
         tecb.Caption := _GetWideString(Caption);
         tecb.Enabled := Enabled;
         tecb.Visible := Visible;
         tecb.State := TTeCheckBoxState(State);
         tecb.OnClick := OnClick;
         tecb.Hint := Hint;
         tecb.ShowHint := ShowHint;
         ATabOrder := TabOrder;
         tecb.Parent := Parent;
         OnCreateThemedControl(TControl(Self.Components[i]), tecb);
         Free;
         tecb.Loaded;
         tecb.TabOrder := ATabOrder;
       end
     else if Components[i].ClassName='TEdit' then
       with TEdit(Components[i]) do begin
         tetxt := TTeEdit.Create(Self);
         tetxt.SetBounds(Left,Top,Width,Height);
         tetxt.Text := _GetWideString(Text);
         tetxt.Enabled := Enabled;
         tetxt.Visible := Visible;
         tetxt.OnChange := OnChange;
         tetxt.OnExit := OnExit;
         tetxt.OnKeyPress := OnKeyPress;
         tetxt.Hint := Hint;
         tetxt.ShowHint := ShowHint;
         ATabOrder := TabOrder;
         tetxt.Parent := Parent;
         OnCreateThemedControl(TControl(Self.Components[i]), tetxt);
         Free;
         tetxt.Loaded;
         tetxt.TabOrder := ATabOrder;
       end
     else if (Components[i].ClassName='TComboBox') and (TComboBox(Components[i]).Style<>csSimple) then
       with TComboBox(Components[i]) do begin
         tecmb := TTeComboBox.Create(Self);
         tecmb.SetBounds(Left,Top,Width,Height);
         tecmb.Enabled := Enabled;
         tecmb.Visible := Visible;
         tecmb.Items := Items;
         tecmb.ItemIndex := ItemIndex;
         tecmb.Hint := Hint;
         tecmb.ShowHint := ShowHint;
         ATabOrder := TabOrder;
         tecmb.Parent := Parent;
         tecmb.DropDownCount := DropDownCount;
         case Style of
           csDropDown:
             tecmb.ComboStyle := kcsDropDown;
           csDropDownList:
             tecmb.ComboStyle := kcsDropDownList;
           csOwnerDrawFixed:
             begin
               tecmb.ComboStyle := kcsDropDownList;
               tecmb.ListStyle := lbOwnerDrawFixed;
               tecmb.ItemHeight := ItemHeight;
               tecmb.Height := ItemHeight+2;
             end;
         end;
         tecmb.OnChange := OnClick;
         OnCreateThemedControl(TControl(Self.Components[i]), tecmb);
         Free;
         tecmb.Loaded;
         tecmb.TabOrder := ATabOrder;
       end
     else if (Components[i] is TGroupBox) then
       with TGroupBox(Components[i]) do begin
         tegb := TTeGroupBox.Create(Self);
         tegb.SetBounds(Left,Top,Width,Height);
         tegb.Caption := _GetWideString(Caption);
         tegb.Enabled := Enabled;
         tegb.Visible := Visible;
         ATabOrder := TabOrder;
         tegb.Parent := Parent;
         MoveChildren(TWinControl(Self.Components[i]), tegb);
         OnCreateThemedControl(TControl(Self.Components[i]), tegb);
         Free;
         tegb.Loaded;
         tegb.TabOrder := ATabOrder;
       end
     else if (Components[i] is TRadioGroup) then
       with TRadioGroup(Components[i]) do begin
         terg := TTeRadioGroup.Create(Self);
         terg.SetBounds(Left,Top,Width,Height);
         terg.Caption := _GetWideString(Caption);
         terg.Enabled := Enabled;
         terg.Visible := Visible;
         ATabOrder := TabOrder;
         terg.Parent := Parent;
         terg.Columns := Columns;
         for j := 0 to Items.Count-1 do
           terg.Items.Add({_GetWideString}(Items[j]));
         terg.ItemIndex := ItemIndex;
         terg.OnClick := OnClick;
         OnCreateThemedControl(TControl(Self.Components[i]), terg);
         Free;
         terg.Loaded;
         terg.TabOrder := ATabOrder;
       end
     else if (Components[i] is TListBox) then
       with TListBox(Components[i]) do begin
         telst := TTeListBox.Create(Self);
         telst.SetBounds(Left,Top,Width,Height);
         telst.Enabled := Enabled;
         telst.Visible := Visible;
         ATabOrder := TabOrder;
         telst.Parent := Parent;
         for j := 0 to Items.Count-1 do
           telst.Items.Add({_GetWideString}(Items[j]));
         telst.ItemIndex := ItemIndex;
         telst.OnClick := OnClick;
         telst.OnDblClick := OnDblClick;
         OnCreateThemedControl(TControl(Self.Components[i]), telst);
         Free;
         telst.Loaded;
         telst.TabOrder := ATabOrder;
       end
     else if Components[i] is TLabel then
       with TLabel(Components[i]) do begin
         telbl := TTeLabel.Create(Self);
         telbl.SetBounds(Left,Top,Width,Height);
         telbl.Caption := _GetWideString(Caption);
         telbl.Enabled := Enabled;
         telbl.Visible := Visible;
         telbl.FocusControl := FocusControl;
         telbl.WordWrap := WordWrap;
         telbl.Parent := Parent;
         OnCreateThemedControl(TControl(Self.Components[i]), telbl);
         telbl.Loaded;
         Free;
       end
     else if (Components[i] is TComboBox) or
             (Components[i] is TRVScroller) then begin
       if ScrollAdapter=nil then
         ScrollAdapter := TTeaScrollAdapter.Create(Self);
       ScrollAdapter.AddControl(Components[i].Name);
       end
     else if Components[i] is TNoteBook then begin
       if GetThemeLink(nil)<>nil then
         TNoteBook(Components[i]).Color := GetThemeLink(nil).SysColors[TNoteBook(Components[i]).Color];
       end
     else if Components[i] is TPanel then begin
       if GetThemeLink(nil)<>nil then
         TPanel(Components[i]).Color := GetThemeLink(nil).SysColors[TPanel(Components[i]).Color];
       end
     else if Components[i] is TPageControl then
       with TPageControl(Components[i]) do begin
         tepc := TTePageControl.Create(Self);
         tepc.SetBounds(Left,Top,Width,Height);
         ATabOrder := TabOrder;
         tepc.Parent := Parent;
         for j := 0 to PageCount-1 do begin
           tets := TTeTabSheet.Create(tepc);
           tepc.AddPage(tets);
           tets.PageIndex := Pages[j].PageIndex;
           tets.Caption := Pages[j].Caption;
           MoveChildren(Pages[j], tets);
         end;
         tepc.OnChange := OnChange;
         OnCreateThemedControl(TControl(Self.Components[i]), tepc);
         Free;
         tepc.Loaded;
         tepc.TabOrder := ATabOrder;
       end;

   end;
end;
{$ENDIF}

{$IFDEF USERVTNT}
procedure TfrmRVBase.ApplyTntControls;
var tntb: TTntButton;
    tntbb: TTntBitBtn;
    tntsb: TTntSpeedButton;
    tntrb: TTntRadioButton;
    tntcb: TTntCheckBox;
    tntgb: TTntGroupBox;
    tntcmb: TTntComboBox;
    tnttxt: TTntEdit;
    tntpc: TTntPageControl;
    tntts: TTntTabSheet;
    tntlbl: TTntLabel;
    tntrg: TTntRadioGroup;
    tntlst: TTntListBox;
    tntclst: TTntCheckListBox;
    tnttv: TTntTreeView;
    tntpm: TTntPopupMenu;
    tntmi: TTntMenuItem;
    i, j: Integer;
    ATabOrder: Integer;
begin
 DisableAlign;
 for i := ComponentCount-1 downto 0 do begin
   if not IsThemeAllowedFor(Components[i]) then
     continue;
   if Components[i] is TBitBtn then
     with TBitBtn(Components[i]) do begin
       tntbb := TTntBitBtn.Create(Self);
       tntbb.SetBounds(Left,Top,Width,Height);
       tntbb.Anchors := Anchors;
       tntbb.Caption := _GetWideString(Caption, ControlPanel);
       tntbb.OnClick := OnClick;
       tntbb.ModalResult := ModalResult;
       tntbb.Glyph := Glyph;
       tntbb.Layout := Layout;
       tntbb.Hint := _GetWideString(Hint, ControlPanel);
       tntbb.ShowHint := ShowHint;
       tntbb.Enabled := Enabled;
       ATabOrder := TabOrder;
       tntbb.Default := Default;
       tntbb.Cancel := Cancel;
       tntbb.Parent := Parent;
       tntbb.Visible := Visible;
       OnCreateThemedControl(TControl(Self.Components[i]), tntbb);
       Free;
       tntbb.TabOrder := ATabOrder;
     end
   else if Components[i] is TSpeedButton then
     with TSpeedButton(Components[i]) do begin
       tntsb := TTntSpeedButton.Create(Self);
       tntsb.SetBounds(Left,Top,Width,Height);
       tntsb.Anchors := Anchors;
       tntsb.Caption := _GetWideString(Caption, ControlPanel);
       tntsb.OnClick := OnClick;
       tntsb.Action := Action;
       tntsb.Glyph := Glyph;
       tntsb.Hint := _GetWideString(Hint, ControlPanel);
       tntsb.ShowHint := ShowHint;
       tntsb.Enabled := Enabled;
       tntsb.Flat := Flat;
       tntsb.AllowAllUp := AllowAllUp;
       tntsb.GroupIndex := GroupIndex;
       tntsb.Parent := Parent;
       OnCreateThemedControl(TControl(Self.Components[i]), tntsb);
       Free;
     end
   else if Components[i] is TButton then
     with TButton(Components[i]) do begin
       tntb := TTntButton.Create(Self);
       tntb.SetBounds(Left,Top,Width,Height);
       tntb.Anchors := Anchors;
       tntb.Caption := _GetWideString(Caption, ControlPanel);
       tntb.OnClick := OnClick;
       tntb.ModalResult := ModalResult;
       tntb.Hint := _GetWideString(Hint, ControlPanel);
       tntb.ShowHint := ShowHint;
       tntb.Enabled := Enabled;
       ATabOrder := TabOrder;
       tntb.Default := Default;
       tntb.Cancel := Cancel;
       tntb.Parent := Parent;
       tntb.Visible := Visible;
       OnCreateThemedControl(TControl(Self.Components[i]), tntb);
       Free;
       tntb.TabOrder := ATabOrder;
     end
   else if Components[i].ClassName= 'TRadioButton' then
     with TRadioButton(Components[i]) do begin
       tntrb := TTntRadioButton.Create(Self);
       tntrb.SetBounds(Left,Top,Width,Height);
       tntrb.Anchors := Anchors;
       tntrb.Caption := _GetWideString(Caption, ControlPanel);
       tntrb.Enabled := Enabled;
       tntrb.Visible := Visible;
       tntrb.Checked := Checked;
       tntrb.OnClick := OnClick;
       tntrb.Hint := _GetWideString(Hint, ControlPanel);
       tntrb.ShowHint := ShowHint;
       ATabOrder := TabOrder;
       tntrb.Parent := Parent;
       OnCreateThemedControl(TControl(Self.Components[i]), tntrb);
       Free;
       tntrb.TabOrder := ATabOrder;
     end
   else if Components[i] is TCheckBox then
     with TCheckBox(Components[i]) do begin
       tntcb := TTntCheckBox.Create(Self);
       tntcb.SetBounds(Left,Top,Width,Height);
       tntcb.Anchors := Anchors;
       tntcb.Caption := _GetWideString(Caption, ControlPanel);
       tntcb.Enabled := Enabled;
       tntcb.Visible := Visible;
       tntcb.State := State;
       tntcb.OnClick := OnClick;
       tntcb.Hint := _GetWideString(Hint, ControlPanel);
       tntcb.ShowHint := ShowHint;
       ATabOrder := TabOrder;
       tntcb.Parent := Parent;
       OnCreateThemedControl(TControl(Self.Components[i]), tntcb);
       Free;
       tntcb.TabOrder := ATabOrder;
     end
   else if Components[i].ClassName='TEdit' then
     with TEdit(Components[i]) do begin
       tnttxt := TTntEdit.Create(Self);
       tnttxt.SetBounds(Left,Top,Width,Height);
       tnttxt.Anchors := Anchors;
       tnttxt.Text := _GetWideString(Text, ControlPanel);
       tnttxt.Enabled := Enabled;
       tnttxt.Visible := Visible;
       tnttxt.OnChange := OnChange;
       tnttxt.OnExit := OnExit;
       tnttxt.OnKeyPress := OnKeyPress;
       tnttxt.Hint := _GetWideString(Hint, ControlPanel);
       tnttxt.ShowHint := ShowHint;
       ATabOrder := TabOrder;
       tnttxt.Parent := Parent;
       OnCreateThemedControl(TControl(Self.Components[i]), tnttxt);
       Free;
       tnttxt.TabOrder := ATabOrder;
     end
   else if (Components[i].ClassName='TComboBox') then
     with TComboBox(Components[i]) do begin
       tntcmb := TTntComboBox.Create(Self);
       tntcmb.SetBounds(Left,Top,Width,Height);
       tntcmb.Anchors := Anchors;
       tntcmb.Enabled := Enabled;
       tntcmb.Visible := Visible;
       tntcmb.Parent := Parent;
       for j := 0 to Items.Count-1 do
         tntcmb.Items.Add(_GetWideString(Items[j], ControlPanel));
       tntcmb.ItemIndex := ItemIndex;
       tntcmb.Hint := _GetWideString(Hint, ControlPanel);
       tntcmb.ShowHint := ShowHint;
       ATabOrder := TabOrder;
       tntcmb.DropDownCount := DropDownCount;
       tntcmb.Style := Style;
       if Style = csOwnerDrawFixed then begin
         tntcmb.ItemHeight := ItemHeight;
         tntcmb.OnDrawItem := OnDrawItem;
       end;
       tntcmb.OnClick := OnClick;
       tntcmb.OnChange := OnChange;       
       tntcmb.OnExit := OnExit;
       tntcmb.OnKeyDown := OnKeyDown;
       OnCreateThemedControl(TControl(Self.Components[i]), tntcmb);
       Free;
       tntcmb.TabOrder := ATabOrder;
     end
   else if (Components[i] is TGroupBox) then
     with TGroupBox(Components[i]) do begin
       tntgb := TTntGroupBox.Create(Self);
       tntgb.SetBounds(Left,Top,Width,Height);
       tntgb.Anchors := Anchors;
       tntgb.Caption := _GetWideString(Caption, ControlPanel);
       tntgb.Enabled := Enabled;
       tntgb.Visible := Visible;
       ATabOrder := TabOrder;
       tntgb.Parent := Parent;
       MoveChildren(TWinControl(Self.Components[i]), tntgb);
       OnCreateThemedControl(TControl(Self.Components[i]), tntgb);
       Free;
       tntgb.TabOrder := ATabOrder;
     end
   else if (Components[i] is TRadioGroup) then
     with TRadioGroup(Components[i]) do begin
       tntrg := TTntRadioGroup.Create(Self);
       tntrg.SetBounds(Left,Top,Width,Height);
       tntrg.Anchors := Anchors;
       tntrg.Caption := _GetWideString(Caption, ControlPanel);
       tntrg.Enabled := Enabled;
       tntrg.Visible := Visible;
       ATabOrder := TabOrder;
       tntrg.Parent := Parent;
       tntrg.Columns := Columns;
       for j := 0 to Items.Count-1 do
         tntrg.Items.Add(_GetWideString(Items[j], ControlPanel));
       tntrg.ItemIndex := ItemIndex;
       tntrg.OnClick := OnClick;
       OnCreateThemedControl(TControl(Self.Components[i]), tntrg);
       Free;
       tntrg.TabOrder := ATabOrder;
     end
   else if (Components[i] is TListBox) then
     with TListBox(Components[i]) do begin
       tntlst := TTntListBox.Create(Self);
       tntlst.SetBounds(Left,Top,Width,Height);
       tntlst.Anchors := Anchors;
       tntlst.Enabled := Enabled;
       tntlst.Visible := Visible;
       ATabOrder := TabOrder;
       tntlst.Parent := Parent;
       for j := 0 to Items.Count-1 do
         tntlst.Items.Add(_GetWideString(Items[j], ControlPanel));
       tntlst.ItemIndex := ItemIndex;
       tntlst.OnClick := OnClick;
       tntlst.OnDblClick := OnDblClick;
       tntlst.OnDrawItem := OnDrawItem;
       tntlst.Style := Style;
       OnCreateThemedControl(TControl(Self.Components[i]), tntlst);
       Free;
       tntlst.TabOrder := ATabOrder;
     end
   else if (Components[i] is TCheckListBox) then
     with TCheckListBox(Components[i]) do begin
       tntclst := TTntCheckListBox.Create(Self);
       tntclst.SetBounds(Left,Top,Width,Height);
       tntclst.Anchors := Anchors;
       tntclst.Enabled := Enabled;
       tntclst.Visible := Visible;
       ATabOrder := TabOrder;
       tntclst.Parent := Parent;
       for j := 0 to Items.Count-1 do
         tntclst.Items.Add(_GetWideString(Items[j], ControlPanel));
       tntclst.ItemIndex := ItemIndex;
       tntclst.OnClick := OnClick;
       tntclst.OnDblClick := OnDblClick;
       tntclst.OnDrawItem := OnDrawItem;
       tntclst.OnClickCheck := OnClickCheck;
       tntclst.Style := Style;
       OnCreateThemedControl(TControl(Self.Components[i]), tntclst);
       Free;
       tntclst.TabOrder := ATabOrder;
     end
   else if (Components[i] is TTreeView) then
     with TTreeView(Components[i]) do begin
       // (we do not copy items, assuming that treeview is empty initially)
       tnttv := TTntTreeView.Create(Self);
       tnttv.SetBounds(Left,Top,Width,Height);
       tnttv.Anchors := Anchors;
       tnttv.Enabled := Enabled;
       tnttv.Visible := Visible;
       ATabOrder := TabOrder;
       tnttv.Parent := Parent;
       tnttv.OnClick := OnClick;
       tnttv.OnChange := OnChange;
       tnttv.OnDblClick := OnDblClick;
       tnttv.Images := Images;
       tnttv.HideSelection := HideSelection;
       OnCreateThemedControl(TControl(Self.Components[i]), tnttv);
       Free;
       tnttv.TabOrder := ATabOrder;
     end
   else if Components[i] is TLabel then
     with TLabel(Components[i]) do begin
       tntlbl := TTntLabel.Create(Self);
       tntlbl.WordWrap := WordWrap;
       if not ParentBiDiMode then
         tntlbl.BiDiMode := BiDiMode;
       tntlbl.SetBounds(Left,Top,Width,Height);
       tntlbl.Anchors := Anchors;
       tntlbl.Caption := _GetWideString(Caption, ControlPanel);
       tntlbl.Enabled := Enabled;
       tntlbl.Visible := Visible;
       tntlbl.FocusControl := FocusControl;
       tntlbl.Parent := Parent;
       OnCreateThemedControl(TControl(Self.Components[i]), tntlbl);
       Free;
     end
   else if Components[i] is TPageControl then
     with TPageControl(Components[i]) do begin
       tntpc := TTntPageControl.Create(Self);
       tntpc.SetBounds(Left,Top,Width,Height);
       tntpc.Anchors := Anchors;
       ATabOrder := TabOrder;
       tntpc.Parent := Parent;
       for j := 0 to PageCount-1 do begin
         tntts := TTntTabSheet.Create(tntpc);
         tntts.PageControl := tntpc;
         tntts.PageIndex := Pages[j].PageIndex;
         tntts.Caption := _GetWideString(Pages[j].Caption, ControlPanel);
         MoveChildren(Pages[j], tntts);
       end;
       tntpc.OnChange := OnChange;
       OnCreateThemedControl(TControl(Self.Components[i]), tntpc);
       Free;
       tntpc.TabOrder := ATabOrder;
     end;
   end;
  i := ComponentCount-1;
  while i>=0 do begin
    if IsThemeAllowedFor(Components[i]) then begin
      if (Components[i] is TPopupMenu) and not (Components[i] is TTntPopupMenu) then
        with TPopupMenu(Components[i]) do begin
          tntpm := TTntPopupMenu.Create(Self);
          tntpm.Images := Images;
          for j := 0 to Items.Count-1 do begin
            tntmi := TTntMenuItem.Create(tntpm);
            tntmi.Caption := _GetWideString(Items[j].Caption, ControlPanel);
            tntmi.OnClick := Items[j].OnClick;
            tntpm.Items.Add(tntmi);
          end;
          OnCreateThemedMenu(Self.Components[i], tntpm);
          Free;
          i := Self.ComponentCount;
        end;
    end;
    dec(i);
  end;
  EnableAlign;
end;
{$ENDIF}

procedure TfrmRVBase.PrepareImageList(il: TCustomImageList);
{$IFDEF RICHVIEWDEFXE2}
var Details: TThemedElementDetails;
{$ENDIF}
var ColorF, ColorB: TColor;
    i: Integer;
    bmp, mask: TBitmap;
    rgb: TRGBTriple;
    lst: TList;
begin
  {$IFDEF RICHVIEWDEFXE2}
  if ThemeControl(Self) then begin
    Details := StyleServices.GetElementDetails(teEditTextNormal);
    if not StyleServices.GetElementColor(Details, ecTextColor, ColorF) or
       (ColorF=clNone) then
       ColorF := clWindowText;
    {if not StyleServices.GetElementColor(Details, ecFillColor, ColorB) or
       (ColorB=clNone) then
       ColorB := clWindow; }
    ColorB := StyleServices.GetStyleColor(scListBox);
    if (ColorB=clNone) then
       ColorB := clWindow;
    end
  else
  {$ENDIF}
  begin
   ColorF := clWindowText;
   ColorB := clWindow;
  end;
  if RV_GetLuminance(ColorF)<RV_GetLuminance(ColorB) then
    exit;
  mask := TBitmap.Create;
  lst := TList.Create;
  try
    for i := 0 to il.Count-1 do
      if AllowInvertImageListImage(il, i) then begin
        bmp := TBitmap.Create;
        il.GetBitmap(i, bmp);
        bmp.PixelFormat := pf24bit;
        InvertBitmap(bmp);
        lst.Add(bmp);
      end
    else
      lst.Add(nil);
    for i := 0 to il.Count-1 do begin
      bmp := TBitmap(lst.Items[i]);
      if bmp<>nil then begin
        mask.Assign(bmp);
        rgb := PRGBTriple(bmp.ScanLine[bmp.Height-1])^;
        ColorB := rgb.rgbtBlue + (rgb.rgbtGreen shl 8) + (rgb.rgbtRed shl 16);
        mask.Mask(ColorB);
        il.Replace(i, bmp, mask);
        bmp.Free;
      end;
     end;
  finally
    lst.Free;
    mask.Free;
  end;
end;

procedure TfrmRVBase.AdjustFormSize;
var w,h: Integer;
begin
  if GetMyClientSize(w,h) then begin
    DisableAlign;
    try
      ClientWidth := w;
      ClientHeight := h;
    finally
      ControlState := ControlState-[csAlignmentNeeded];
      EnableAlign;
    end;
  end;
end;

function TfrmRVBase.AllowInvertImageListImage(il: TCustomImageList;
  Index: Integer): Boolean;
begin
   Result := True;
end;

procedure RVAAddRV(rv: TCustomRichView; const s: TRVALocString;
  StyleNo, ParaNo: Integer; const Tag: TRVTag);
begin
  rv.{$IFDEF USERVTNT}AddNLWTag{$ELSE}AddNLTag{$ENDIF}(s, StyleNo, ParaNo, Tag);
end;




end.
