﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVCharCase.pas' rev: 27.00 (Windows)

#ifndef RvcharcaseHPP
#define RvcharcaseHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvcharcase
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVCharCase : unsigned char { rvccLowerCase, rvccUpperCase, rvccTitleWord };

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall RVChangeCharCase(Rvedit::TCustomRichViewEdit* rve, TRVCharCase CharCase);
extern DELPHI_PACKAGE void __fastcall RVGetCharCase(Rvedit::TCustomRichViewEdit* rve, bool &AllUpperCase, bool &AllLowerCase);
}	/* namespace Rvcharcase */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCHARCASE)
using namespace Rvcharcase;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvcharcaseHPP
