﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'PreviewRVFrm.pas' rev: 27.00 (Windows)

#ifndef PreviewrvfrmHPP
#define PreviewrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <CRVPP.hpp>	// Pascal unit
#include <RVPP.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Previewrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVPreview;
class PASCALIMPLEMENTATION TfrmRVPreview : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Extctrls::TPanel* Panel1;
	Rvpp::TRVPrintPreview* rvpp;
	Vcl::Stdctrls::TComboBox* cmb1;
	Vcl::Stdctrls::TLabel* Label1;
	Vcl::Buttons::TSpeedButton* SpeedButton1;
	Vcl::Actnlist::TActionList* ActionList1;
	Vcl::Controls::TImageList* ImageList1;
	Vcl::Actnlist::TAction* actFirst;
	Vcl::Actnlist::TAction* actPrior;
	Vcl::Actnlist::TAction* actNext;
	Vcl::Actnlist::TAction* actLast;
	Vcl::Buttons::TSpeedButton* SpeedButton2;
	Vcl::Buttons::TSpeedButton* SpeedButton3;
	Vcl::Buttons::TSpeedButton* SpeedButton4;
	Vcl::Stdctrls::TEdit* txtPageNo;
	Vcl::Stdctrls::TLabel* Label2;
	Vcl::Stdctrls::TButton* Button1;
	Vcl::Buttons::TSpeedButton* sbPrint;
	Vcl::Actnlist::TAction* actPrint;
	Richviewactions::TrvActionPageSetup* rvActionPageSetup1;
	Vcl::Buttons::TSpeedButton* sbPageSetup;
	void __fastcall cmb1Exit(System::TObject* Sender);
	void __fastcall cmb1KeyDown(System::TObject* Sender, System::Word &Key, System::Classes::TShiftState Shift);
	void __fastcall rvppZoomChanged(System::TObject* Sender);
	void __fastcall actFirstExecute(System::TObject* Sender);
	void __fastcall actPriorExecute(System::TObject* Sender);
	void __fastcall actNextExecute(System::TObject* Sender);
	void __fastcall actLastExecute(System::TObject* Sender);
	void __fastcall txtPageNoKeyPress(System::TObject* Sender, System::WideChar &Key);
	void __fastcall txtPageNoExit(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall actPrintExecute(System::TObject* Sender);
	void __fastcall rvActionPageSetup1Change(System::TObject* Sender);
	void __fastcall FormMouseWheelDown(System::TObject* Sender, System::Classes::TShiftState Shift, const System::Types::TPoint &MousePos, bool &Handled);
	void __fastcall FormMouseWheelUp(System::TObject* Sender, System::Classes::TShiftState Shift, const System::Types::TPoint &MousePos, bool &Handled);
	
private:
	Vcl::Comctrls::TComboBoxEx* cmb;
	void __fastcall UpdateZoom(void);
	void __fastcall UpdateToolbar(void);
	void __fastcall GoToPage(void);
	
protected:
	Vcl::Controls::TControl* _txtPageNo;
	Vcl::Controls::TControl* _Label2;
	Vcl::Controls::TControl* _cmb;
	Vcl::Controls::TControl* _sb1;
	Vcl::Controls::TControl* _sb2;
	Vcl::Controls::TControl* _sb3;
	Vcl::Controls::TControl* _sb4;
	Vcl::Controls::TControl* _sbPrint;
	Vcl::Controls::TControl* _sbPageSetup;
	
public:
	DYNAMIC void __fastcall Localize(void);
	void __fastcall SetPageSetup(Richviewactions::TrvActionPageSetup* Action, Vcl::Imglist::TCustomImageList* Images);
	void __fastcall DoSelectNextZoomLevel(void);
	void __fastcall DoSelectPreviousZoomLevel(void);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVPreview(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVPreview(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVPreview(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVPreview(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Previewrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PREVIEWRVFRM)
using namespace Previewrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PreviewrvfrmHPP
