
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for changing document background         }
{       and margins.                                    }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit BackRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, ActnList,
  Dialogs, BaseRVFrm, StdCtrls, RVColorCombo, RVScroll, RichView,
  RVOfficeRadioBtn, ImgList, RVStyle, ExtDlgs, RVEdit;

type
  TfrmRVBack = class(TfrmRVBase)
    il: TImageList;
    rg: TRVOfficeRadioGroup;
    gbBack: TGroupBox;
    rv: TRichView;
    cmbColor: TRVColorCombo;
    lblColor: TLabel;
    btnOk: TButton;
    btnCancel: TButton;
    btnOpen: TButton;
    btnSave: TButton;
    btnClear: TButton;
    spd: TSavePictureDialog;
    btnMargins: TButton;
    RVStyle1: TRVStyle;
    procedure btnSaveClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure rgClick(Sender: TObject);
    procedure cmbColorColorChange(Sender: TObject);
    procedure btnMarginsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    _btnSave, _btnClear, _btnMargins: TControl;
    _rv: TRichView;
    FAction: TAction;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    ImageFileName: String;
    ImageChanged: Boolean;
    procedure Init(rve: TCustomRichViewEdit; cd: TColorDialog; Action: TAction;
      CanChangeMargins: Boolean);
    procedure GetResult(rv: TCustomRichViewEdit);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;

implementation
uses FourSidesRVFrm, RVALocalize, RichViewActions;

{$R *.dfm}

procedure TfrmRVBack.btnSaveClick(Sender: TObject);
begin
  if _rv.BackgroundBitmap.Empty then begin
    Beep;
    exit;
  end;
  if spd.Execute then
    _rv.BackgroundBitmap.SaveToFile(spd.FileName);
end;

procedure TfrmRVBack.btnOpenClick(Sender: TObject);
var opd: TOpenPictureDialog;
    pic : TPicture;
    bmp : TBitmap;
begin
  opd := TOpenPictureDialog.Create(nil);
  try
    opd.Options := [ofPathMustExist, ofFileMustExist, ofEnableSizing, ofHideReadOnly];
    opd.DefaultExt := 'bmp';
    if opd.Execute then
    try
      pic := TPicture.Create;
      bmp := TBitmap.Create;
      try
        pic.LoadFromFile(opd.FileName);
        try
          if pic.Graphic is TIcon then
            TIcon(pic.Graphic).Handle;
          bmp.Assign(pic.Graphic);
          ImageFileName := opd.FileName;
        except
          bmp.Width := pic.Graphic.Width;
          bmp.Height := pic.Graphic.Height;
          bmp.Canvas.Draw(0,0,pic.Graphic);
        end;
        _rv.BackgroundBitmap  := bmp;
        if _rv.BackgroundStyle=bsNoBitmap then
          _rv.BackgroundStyle:=bsTiled;
      finally
        pic.Free;
        bmp.Free;
      end;
      ImageChanged := True;
    except
      ImageFileName := '';
      RVA_MessageBox(RVA_GetS(rvam_err_ErrorLoadingImageFile, ControlPanel),RVA_GetS(rvam_err_Title, ControlPanel),
        MB_OK or MB_ICONSTOP);
    end;
  finally
    opd.Free;
  end;
  _btnSave.Enabled := not _rv.BackgroundBitmap.Empty;
  _btnClear.Enabled := not _rv.BackgroundBitmap.Empty;
  rg.ItemIndex := ord(_rv.BackgroundStyle);
end;

procedure TfrmRVBack.Init(rve: TCustomRichViewEdit; cd: TColorDialog;
  Action: TAction; CanChangeMargins: Boolean);
var i, StyleNo, Count: Integer;
begin
  if TRVAControlPanel(ControlPanel).UserInterface=rvauiHTML then
    rg.Items[1].Enabled := False;
  ImageFileName := '';
  FAction := Action;
  _rv.LeftMargin   := rve.LeftMargin;
  _rv.TopMargin    := rve.TopMargin;
  _rv.RightMargin  := rve.RightMargin;
  _rv.BottomMargin := rve.BottomMargin;
  _rv.BackgroundBitmap := rve.BackgroundBitmap;
  _rv.BackgroundStyle  := rve.BackgroundStyle;
  _rv.Style.TextStyles :=rve.Style.TextStyles;
  _rv.Style.ParaStyles :=rve.Style.ParaStyles;
  _rv.Style.ParaStyles[0].SpaceBefore := 3;
  _rv.Style.ParaStyles[0].SpaceAfter := 3;
  if rve.Color = clNone then
    _rv.Color := rve.Style.Color
  else
    _rv.Color := rve.Color;

  if _rv.BackgroundStyle in [bsNoBitmap..bsCentered] then
    rg.ItemIndex := ord(_rv.BackgroundStyle)
  else
    rg.ItemIndex := 4;
  _btnSave.Enabled := not rve.BackgroundBitmap.Empty;
  _btnClear.Enabled := not rve.BackgroundBitmap.Empty;
  cmbColor.ColorDialog := cd;
  cmbColor.ChosenColor := _rv.Color;
  StyleNo := rve.CurTextStyleNo;
  _rv.Style.TextStyles[StyleNo].Charset := RVA_GetCharset(ControlPanel);
  {$IFDEF USERVTNT}
  _rv.Style.TextStyles[StyleNo].Unicode := True;  
  {$ENDIF}
  {$IFDEF USELOREMIPSUM}
  Count := 2;
  {$ELSE}
  Count := 50;
  {$ENDIF}
  for i := 0 to Count do
    _rv.{$IFDEF USERVTNT}AddNLWTag{$ELSE}AddNLTag{$ENDIF}(
      {$IFDEF USELOREMIPSUM}
      LoremIpsum1+LoremIpsum2,
      {$ELSE}
      RVA_GetS(rvam_back_SampleText, ControlPanel),
      {$ENDIF}
      StyleNo, 0, RVEMPTYTAG);
  _rv.Format;
  _btnMargins.Visible := CanChangeMargins;
end;

function TfrmRVBack.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := gbBack.Left+gbBack.Width+rg.Left;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-gbBack.Top-gbBack.Height);
  Result := True;
end;

procedure TfrmRVBack.GetResult(rv: TCustomRichViewEdit);
var BackgroundChanged, MarginsChanged, EditMode: Boolean;
begin
  BackgroundChanged := (rv.Color<>_rv.Color) or
    (rv.BackgroundStyle<>_rv.BackgroundStyle) or
    ImageChanged;
  MarginsChanged := (rv.LeftMargin<>_rv.LeftMargin) or
     (rv.TopMargin<>_rv.TopMargin) or
     (rv.RightMargin<>_rv.RightMargin) or
     (rv.BottomMargin<>_rv.BottomMargin);
  if not BackgroundChanged and not MarginsChanged then
    exit;
  EditMode := (BackgroundChanged and (rvfoSaveBack in rv.RVFOptions)) or
    (MarginsChanged and (rvfoSaveLayout in rv.RVFOptions));

  if EditMode then begin
    if (BackgroundChanged and (rvfoSaveBack in rv.RVFOptions)) then
      rv.BeginUndoGroup(rvutBackground)
    else
      rv.BeginUndoGroup(rvutLayout);
    rv.SetUndoGroupMode(True);
  end;
  try
    if BackgroundChanged then
      if not (rvfoSaveBack in rv.RVFOptions) then begin
        rv.Color := _rv.Color;
        rv.BackgroundStyle := _rv.BackgroundStyle;
        if ImageChanged then
          rv.BackgroundBitmap := _rv.BackgroundBitmap;
        end
      else if rv.CanChange then begin
        rv.SetIntPropertyEd(rvipColor, _rv.Color, False);
        rv.SetIntPropertyEd(rvipBackgroundStyle, Ord(_rv.BackgroundStyle), False);
        if ImageChanged then
          rv.SetBackgroundImageEd(_rv.BackgroundBitmap, False);
      end;
    if MarginsChanged then begin
      if not (rvfoSaveLayout in rv.RVFOptions) then begin
        rv.LeftMargin   := _rv.LeftMargin;
        rv.TopMargin    := _rv.TopMargin;
        rv.RightMargin  := _rv.RightMargin;
        rv.BottomMargin := _rv.BottomMargin;
        end
      else if rv.CanChange then begin
        rv.SetIntPropertyEd(rvipLeftMargin, _rv.LeftMargin, False);
        rv.SetIntPropertyEd(rvipTopMargin, _rv.TopMargin, False);
        rv.SetIntPropertyEd(rvipRightMargin, _rv.RightMargin, False);
        rv.SetIntPropertyEd(rvipBottomMargin, _rv.BottomMargin, False);
      end;
      TRVAControlPanel(ControlPanel).DoOnMarginsChanged(TrvAction(FAction), TCustomRichViewEdit(rv));
    end;
  finally
    rv.SetUndoGroupMode(False);
  end;
  if MarginsChanged then
    rv.Reformat
  else
    rv.RefreshAll;
  if EditMode then
    rv.Change;
end;

procedure TfrmRVBack.btnClearClick(Sender: TObject);
begin
  ImageFileName := '';
  _rv.BackgroundBitmap.Handle := 0;
  _btnSave.Enabled := not _rv.BackgroundBitmap.Empty;
  _btnClear.Enabled := not _rv.BackgroundBitmap.Empty;
  ImageChanged := True;
end;

procedure TfrmRVBack.rgClick(Sender: TObject);
begin
  if rg.ItemIndex<0 then
    exit;
  _rv.BackgroundStyle := TBackgroundStyle(rg.ItemIndex);
end;

procedure TfrmRVBack.cmbColorColorChange(Sender: TObject);
begin
  _rv.Color := cmbColor.ChosenColor;
end;

procedure TfrmRVBack.btnMarginsClick(Sender: TObject);
var frm: TfrmRVFourSides;
    l,t,r,b: TRVStyleLength;
    fl,ft,fr,fb: Boolean;
begin
  frm := TfrmRVFourSides.Create(Application, ControlPanel);
  try
    frm.SetControlCaption(frm._gb, RVA_GetSHUnits(rvam_4s_DefTitle, rvuPixels, ControlPanel));
    l := _rv.LeftMargin;
    t := _rv.TopMargin;
    r := _rv.RightMargin;
    b := _rv.BottomMargin;
    fl := True;
    ft := True;
    fr := True;
    fb := True;
    frm.SetValues(l,t,r,b,fl,ft,fr,fb, RVStyle1, True, rvuPixels);
    frm.HelpContext := 90302;
    if frm.ShowModal=mrOk then begin
      frm.GetValues(l,t,r,b,fl,ft,fr,fb, RVStyle1, rvuPixels);
      if fl then
        _rv.LeftMargin := l;
      if fr then
        _rv.RightMargin := r;
      if ft then
        _rv.TopMargin := t;
      if fb then
        _rv.BottomMargin := b;
      _rv.Format;
    end;
  finally
    frm.Free;
  end;
end;

procedure TfrmRVBack.Localize;
begin
  inherited;
  Caption := RVA_GetS(rvam_back_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  btnOpen.Caption := RVA_GetSAsIs(rvam_btn_Open, ControlPanel);
  btnSave.Caption := RVA_GetSAsIs(rvam_btn_Save, ControlPanel);
  btnClear.Caption := RVA_GetSAsIs(rvam_btn_Clear, ControlPanel);
  lblColor.Caption := RVA_GetSAsIs(rvam_back_Color, ControlPanel);
  rg.Caption := RVA_GetSH(rvam_back_Position, ControlPanel);
  gbBack.Caption := RVA_GetSHAsIs(rvam_back_Background, ControlPanel);
  rg.Items[0].Caption := RVA_GetS(rvam_back_None, ControlPanel);
  rg.Items[1].Caption := RVA_GetS(rvam_back_FullWindow, ControlPanel);
  rg.Items[2].Caption := RVA_GetS(rvam_back_FixedTiles, ControlPanel);
  rg.Items[3].Caption := RVA_GetS(rvam_back_Tiles, ControlPanel);
  rg.Items[4].Caption := RVA_GetS(rvam_back_Center, ControlPanel);
  btnMargins.Caption := RVA_GetSAsIs(rvam_back_Padding, ControlPanel);
  if (RVA_GetCharset(ControlPanel)=ARABIC_CHARSET) or
    (RVA_GetCharset(ControlPanel)=HEBREW_CHARSET) then 
    _rv.BiDiMode := rvbdRightToLeft;    
end;

{$IFDEF RVASKINNED}
procedure TfrmRVBack.OnCreateThemedControl(OldControl, NewControl: TControl);
begin
  if OldControl = _btnSave then
    _btnSave := NewControl
  else if OldControl = _btnClear then
    _btnClear := NewControl
  else if OldControl = _btnMargins then
    _btnMargins := NewControl
  else if OldControl = _rv then
    _rv := TRichView(NewControl);
end;
{$ENDIF}

procedure TfrmRVBack.FormCreate(Sender: TObject);
begin
  _btnSave := btnSave;
  _btnClear := btnClear;
  _btnMargins := btnMargins;
  _rv := rv;
  inherited;
  PrepareImageList(il);
end;

end.
