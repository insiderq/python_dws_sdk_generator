
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Portuguese (Brazilian) translation              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Bruno Sonnino 2003-04-02               }
{ Updated by: Alexandre Palmeira 2014-02-12             }
{*******************************************************}

unit RVAL_PtBr;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =                            
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'polegadas', 'cm', 'mm', 'picas', 'pixels', 'pontos',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pi', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Arquivo', '&Editar', 'F&ormatar', 'Fo&nte', '&Par�grafo', '&Inserir', '&Tabela', '&Janela', '&Ajuda',
  // exit
  'Sai&r',
  // top-level menus: View, Tools,
  '&Visualiza', 'Ferramen&tas',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Tamanho', 'Estilo', 'Selec&iona', 'Ali&nha Conte�do das C�lulas', 'Bordas das C�&lulas',
  // menus: Table cell rotation
  '&Rota��o de C�lula',    
  // menus: Text flow, Footnotes/endnotes
  '&Fluxo de texto', 'Notas de &Rodap�',
  // ribbon tabs: tab1, tab2, view, table
  '&Home', '&Avan�ado', '&Exibir', '&Tabela',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '�rea de Transfer�ncia', 'Fonte', 'Par�grafo', 'Lista', 'Editando',
  // ribbon groups: insert, background, page setup,
  'Inserir', 'Fundo', 'Configura��o de P�gina',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Links', 'Notas de Rodap�', 'Modos de Exibi��o de Documento', 'Exibir/Ocultar', 'Zoom', 'Op��es',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserir', 'Apagar', 'Opera��es', 'Bordas',
  // ribbon groups: styles 
  'Estilos',
  // ribbon screen tip footer,
  'Pressione F1 para obter mais ajuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documentos recentes', 'Salvar uma c�pia do documento', 'Visualizar e imprimir o documento',
  // ribbon label: units combo
  'Unidades:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Novo', 'Novo|Cria um novo documento em branco',
  // TrvActionOpen
  '&Abrir...', 'Abrir|Abre um documento do disco',
  // TrvActionSave
  '&Salvar', 'Salvar|Salva o documento para disco',
  // TrvActionSaveAs
  'Salvar &Como...', 'Salvar Como...|Salva o documento para disco com novo nome',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta o documento para outro formato',
  // TrvActionPrintPreview
  'Vi&sualizar Impress�o', 'Visualizar Impress�o|Mostra o documento como ele ser� impresso',
  // TrvActionPrint
  '&Imprimir...', 'Imprimir|Configura a impress�o e imprime o documento',
  // TrvActionQuickPrint
  '&Imprimir', '&Impress�o r�pida', 'Imprimir|Imprime o documento',
   // TrvActionPageSetup
  'Configurar P�&gina...', 'Configurar P�gina|Configura margens, tamanho de papel, orienta��o, fonte, cabe�alhos e rodap�s',
  // TrvActionCut
  'Recor&tar', 'Recortar|Recorta a sele��o e coloca na �rea de Transfer�ncia',
  // TrvActionCopy
  '&Copiar', 'Copiar|Copia a sele��o e coloca na �rea de Transfer�ncia',
  // TrvActionPaste
  'Co&lar', 'Colar|Insere o conte�do da �rea de Transfer�ncia',
  // TrvActionPasteAsText
  'Colar como &Texto', 'Colar como Texto|Insere o conte�do da �rea de Transfer�ncia sem formata��o',
  // TrvActionPasteSpecial
  'Colar E&special...', 'Colar Especial|Insere o conte�do da �rea de Transfer�ncia no formato especificado',
  // TrvActionSelectAll
  'Selecionar &Tudo', 'Selecionar Tudo|Seleciona todo o documento',
  // TrvActionUndo
  '&Desfazer', 'Desfazer|Reverte a �ltima a��o',
  // TrvActionRedo
  '&Refazer', 'Refazer|Refaz a �ltima a��o desfeita',
  // TrvActionFind
  '&Localizar...', 'Localizar|Localiza o texto especificado no documento',
  // TrvActionFindNext
  'Localizar &Pr�xima', 'Localizar Pr�xima|Continua a �ltima localiza��o',
  // TrvActionReplace
  '&Substituir...', 'Substituir|Localiza e substitui o texto especificado no documento',
  // TrvActionInsertFile
  '&Arquivo...', 'Inserir Arquivo|Insere o conte�do do arquivo no documento',
  // TrvActionInsertPicture
  '&Imagem...', 'Inserir Imagem|Insere uma imagem do disco',
  // TRVActionInsertHLine
  'Linha &Horizontal', 'Inserir Linha Horizontal|Insere uma linha horizontal',
  // TRVActionInsertHyperlink
  '&Link Hipertexto...', 'Inserir Hipertexto|Insere um link hipertexto',
  // TRVActionRemoveHyperlinks
  '&Remover Hyperlinks', 'Remover Hyperlinks|Remove todos os hyperlinks no texto selecionado',
  // TrvActionInsertSymbol
  '&S�mbolo...', 'Inserir S�mbolo|Insere s�mbolo',
  // TrvActionInsertNumber
  '&N�mero...', 'Inserir N�mero|Insere um n�mero',
  // TrvActionInsertCaption
  '&Legenda...', 'Inserir Legenda|Insere uma legenda para o objeto selecionado',
  // TrvActionInsertTextBox
  'Caixa de &Texto', 'Inserir Caixa de Texto|Insere uma caixa de texto',
  // TrvActionInsertSidenote
  'Nota em &Quadro', 'Inserir Nota em Quadro|Insere uma nota em um quadro de texto',
  // TrvActionInsertPageNumber
  'N�mero de &P�gina', 'Inserir N�mero de P�gina|Insere um n�mero de p�gina',
  // TrvActionParaList
  '&Marcadores e Numera��o...', 'Marcadores e Numera��o|Aplica ou edita marcadores ou numera��o para os par�grafos selecionados',
  // TrvActionParaBullets
  '&Marcadores', 'Marcadores|Adiciona ou remove marcadores ao par�grafo',
  // TrvActionParaNumbering
  '&Numera��o', 'Numera��o|Adiciona ou remove numera��o ao par�grafo',
  // TrvActionColor
  'Cor de &Fundo...', 'Cor de Fundo|Muda a cor de fundo do documento',
  // TrvActionFillColor
  'Cor de &Preenchimento...', 'Cor de Preenchimento|Muda cor de fundo do texto, par�grafo, c�lula, tabela ou documento',
  // TrvActionInsertPageBreak
  '&Inserir Quebra de P�gina', 'Inserir Quebra de P�gina|Insere uma quebra de p�gina',
  // TrvActionRemovePageBreak
  '&Remover Quebra de P�gina', 'Remover Quebra de P�gina|Remove a quebra de p�gina',
  // TrvActionClearLeft
  'Limpar Fluxo do Texto do Lado &Esquerdo', 'Limpar Fluxo do Texto do Lado Esquerdo|Posiciona este par�grafo abaixo de qualquer figura alinhada pela esquerda',
  // TrvActionClearRight
  'Limpar Fluxo do Texto do Lado &Direito', 'Limpar Fluxo do Texto do Lado Direito|Posiciona este par�grafo abaixo de qualquer figura alinhada pela direita',
  // TrvActionClearBoth
  'Limpar Fluxo do Texto de &Ambos os Lados', 'Limpar Fluxo do Texto de &Ambos os Lados|Posiciona este par�grafo abaixo de qualquer',
  // TrvActionClearNone
  'Fluxo de Texto &Normal', 'Fluxo do Texto Normal|Texto ao redor de figuras alinhadas pela esquerda ou pela direita',
  // TrvActionVAlign
  'Posi��o do &Objeto...', 'Posi��o do Objeto|Muda a posi��o do objeto selecionado',
  // TrvActionItemProperties
  '&Propriedades de Objeto...', 'Propriedades de Objeto|Define propriedades do objeto ativo',
  // TrvActionBackground
  '&Fundo...', 'Fundo|Escolhe a cor e imagem de fundo',
  // TrvActionParagraph
  '&Par�grafo...', 'Par�grafo|Muda os atributos do par�grafo',
  // TrvActionIndentInc
  '&Aumentar Recuo', 'Aumentar Recuo|Aumenta recuo � esquerda dos par�grafos selecionados',
  // TrvActionIndentDec
  '&Diminuir Recuo', 'Diminuir Recuo|Diminui recuo � esquerda dos par�grafos selecionados',
  // TrvActionWordWrap
  '&Quebra de Linha', 'Quebra de Linha|Liga/Desliga quebra de linha para os par�grafos selecionados',
  // TrvActionAlignLeft
  'Alinhar � &Esquerda', 'Alinhar � Esquerda|Alinha o texto selecionado � esquerda',
  // TrvActionAlignRight
  'Alinhar � &Direita', 'Alinhar � Direita|Alinha o texto selecionado � direita',
  // TrvActionAlignCenter
  '&Centralizar', 'Centralizar|Centraliza o texto selecionado',
  // TrvActionAlignJustify
  '&Justificar', 'Justificar|Alinha o texto selecionado � direita e � esquerda',
  // TrvActionParaColor
  'Cor de Fu&ndo do Par�grafo...', 'Cor de Fundo do Par�grafo|Configura cor de fundo de par�grafos',
  // TrvActionLineSpacing100
  'Espa�o de Linha &Simples', 'Espa�o de Linha Simples|Configura espa�o de linha simples',
  // TrvActionLineSpacing150
  'Espa�o de Lin&ha 1,5', 'Espa�o de Linha 1,5|Configura espa�o de linha igual a 1,5 linhas',
  // TrvActionLineSpacing200
  'Espa�o de Linha &Duplo', 'Espa�o de Linha Duplo|Configura espa�o de linha duplo',
  // TrvActionParaBorder
  '&Bordas e Fundo de Par�grafo...', 'Bordas e Fundo de Par�grafo|Configura bordas e fundo para os par�grafos selecionados',
  // TrvActionInsertTable
  '&Inserir Tabela...', '&Tabela', 'Inserir Tabela|Insere uma nova tabela',
  // TrvActionTableInsertRowsAbove
  'Inserir Linha &Acima', 'Inserir Linha Acima|Insere nova linha acima das c�lulas selecionadas',
  // TrvActionTableInsertRowsBelow
  'Inserir Linha A&baixo', 'Inserir Linha Abaixo|Insere nova linha abaixo das c�lulas selecionadas',
  // TrvActionTableInsertColLeft
  'Inserir Coluna � &Esquerda', 'Inserir Coluna � Esquerda|Insere nova coluna � esquerda das c�lulas selecionadas',
  // TrvActionTableInsertColRight
  'Inserir Coluna � &Direita', 'Inserir Coluna � Direita|Insere nova coluna � direita das c�lulas selecionadas',
  // TrvActionTableDeleteRows
  'Excluir L&inhas', 'Excluir Linhas|Exclui linhas contendo as c�lulas selecionadas',
  // TrvActionTableDeleteCols
  'Excluir &Colunas', 'Excluir Colunas|Exclui colunas contendo as c�lulas selecionadas',
  // TrvActionTableDeleteTable
  '&Excluir Tabela', 'Excluir Tabela|Exclui a tabela',
  // TrvActionTableMergeCells
  '&Mesclar C�lulas', 'Mesclar C�lulas|Mescla as c�lulas selecionadas',
  // TrvActionTableSplitCells
  '&Dividir C�lulas...', 'Dividir C�lulas|Divide as c�lulas selecionadas',
  // TrvActionTableSelectTable
  'Selecionar &Tabela', 'Selecionar Tabela|Seleciona a tabela',
  // TrvActionTableSelectRows
  'Selecionar Lin&has', 'Selecionar Linhas|Seleciona linhas',
  // TrvActionTableSelectCols
  'Selecionar Col&unas', 'Selecionar Colunas|Seleciona colunas',
  // TrvActionTableSelectCell
  'Selecionar C�&lula', 'Selecionar C�lula|Selecionar c�lula',
  // TrvActionTableCellVAlignTop
  'Alinhar C�lula no &Topo', 'Alinhar C�lula no Topo|Alinha conte�do da c�lula no topo',
  // TrvActionTableCellVAlignMiddle
  'Alinhar C�lula no &Meio', 'Alinhar C�lula no Meio|Alinha conte�do da c�lula no meio',
  // TrvActionTableCellVAlignBottom
  'Alinhar C�lula em &Baixo', 'Alinhar C�lula em Baixo|Alinha conte�do da c�lula em baixo',
  // TrvActionTableCellVAlignDefault
  'Alinhamento Vertical de C�lula &Padr�o', 'Alinhamento Vertical de C�lula Padr�o|Configura o alinhamento vertical padr�o para as c�lulas selecionadas',
  // TrvActionTableCellRotationNone
  '&Sem Rota��o de C�lula', 'Sem Rota��o de C�lula|Gira o conte�do da c�lula 0�',
  // TrvActionTableCellRotation90
  'Girar C�lula &90�', 'Girar C�lula 90�|Gira o conte�do da c�lula 90�',
  // TrvActionTableCellRotation180
  'Girar C�lula &180�', 'Girar C�lula 180�|Gira o conte�do da c�lula 180�',
  // TrvActionTableCellRotation270
  'Girar C�lula &270�', 'Girar C�lula 270�|Gira o conte�do da c�lula 270�',
  // TrvActionTableProperties
  '&Propriedades da Tabela...', 'Propriedades da Tabela|Muda as propriedades da tabela selecionada',
  // TrvActionTableGrid
  'Mostrar Linhas de &Grade', 'Mostrar Linhas de Grade|Mostra ou esconde as linhas de grade',
  // TRVActionTableSplit
  'Di&vidir Tabela', 'Dividir Tabela|Divide a tabela em duas tabelas come�ando na linha selecionada',
  // TRVActionTableToText
  'Converter em Te&xto...', 'Converter em Texto|Converte a tabela em texto',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena as linhas da tabela',
  // TrvActionTableCellLeftBorder
  'Borda &Esquerda', 'Borda Esquerda|Mostra ou esconde a borda esquerda da c�lula',
  // TrvActionTableCellRightBorder
  'Borda &Direita', 'Borda Direita|Mostra ou esconde a borda direita da c�lula',
  // TrvActionTableCellTopBorder
  'Borda &Superior', 'Borda Superior|Mostra ou esconde a borda superior da c�lula',
  // TrvActionTableCellBottomBorder
  'Borda &Inferior', 'Borda Inferior|Mostra ou esconde a borda superior da c�lula',
  // TrvActionTableCellAllBorders
  '&Todas Bordas', 'Todas Bordas|Mostra ou esconde todas as bordas da c�lula',
  // TrvActionTableCellNoBorders
  '&Sem Bordas', 'Sem Bordas|Esconde todas as bordas de c�lulas',
  // TrvActionFonts & TrvActionFontEx
  '&Fonte...', 'Fonte|Altera fonte do texto selecionado',
  // TrvActionFontBold
  '&Negrito', 'Negrito|Altera o estilo do texto selecionado para negrito',
  // TrvActionFontItalic
  '&It�lico', 'It�lico|Altera o estilo do texto selecionado para it�lico',
  // TrvActionFontUnderline
  '&Sublinhado', 'Sublinhado|Altera o estilo do texto selecionado para sublinhado',
  // TrvActionFontStrikeout
  '&Riscado', 'Riscado|Risca o texto selecionado',
  // TrvActionFontGrow
  '&Aumentar Fonte', 'Aumentar Fonte|Aumenta a altura do texto selecionado em 10%',
  // TrvActionFontShrink
  'D&iminuir Fonte', 'Diminuir Fonte|Diminui a altura do texto selecionado em 10%',
  // TrvActionFontGrowOnePoint
  'A&umentar Fonte em Um Ponto', 'Aumentar Fonte em Um Ponto|Aumenta a altura do texto selecionado em 1 ponto',
  // TrvActionFontShrinkOnePoint
  'D&iminuir Fonte em Um Ponto', 'Diminuir Fonte em Um Ponto|Diminui a altura do texto selecionado em 1 ponto',
  // TrvActionFontAllCaps
  '&Tudo Mai�sculas', 'Tudo Mai�sculas|Muda todos os caracteres do texto selecionado para mai�sculas',
  // TrvActionFontOverline
  '&SobreLinha', 'SobreLinha|Adiciona linha sobre o texto selecionado',
  // TrvActionFontColor
  '&Cor do Texto...', 'Cor do Texto|Muda a cor do texto selecionado',
  // TrvActionFontBackColor
  'Cor de Fu&ndo do Texto...', 'Cor de Fundo do Texto|Muda a cor de fundo do texto selecionado',
  // TrvActionAddictSpell3
  'Verificar &Ortografia', 'Verificar Ortografia|Verifica a ortografia',
  // TrvActionAddictThesaurus3
  '&Sin�nimos', 'Sin�nimos|Prov� sin�nimos para a palavra selecionada',
  // TrvActionParaLTR
  'Esquerda para Direita', 'Esquerda para Direita|Configura dire��o do texto da esquerda para direita para par�grafos selecionados',
  // TrvActionParaRTL
  'Direita para Esquerda', 'Direita para Esquerda|Configura dire��o do texto da direita para esquerda para par�grafos selecionados',
  // TrvActionLTR
  'Texto Esquerda para Direita', 'Texto Esquerda para Direita|Configura dire��o do texto da esquerda para direita para texto selecionado',
  // TrvActionRTL
  'Texto Direita para Esquerda', 'Texto Direita para Esquerda|Configura dire��o do texto da direita para esquerda para texto selecionado',
  // TrvActionCharCase
  'Caixa de Caractere', 'Caixa de Caractere|Altera caixa do texto selecionado',
  // TrvActionShowSpecialCharacters
  'Caracteres &N�o Imprim�veis', 'Caracteres n�o Imprim�veis|Mostra ou esconde caracteres n�o imprim�veis, como marcas de par�grafo, tabula��es e espa�os',
  // TrvActionSubscript
  'Su&bscrito', 'Subscrito|Converte o texto selecionado para subscrito',
  // TrvActionSuperscript
  'Superescrito', 'Superescrito|Converte o texto selecionado para superescrito',
  'Nota de &Rodap�', 'Nota de Rodap�|Insere uma nota de rodap�',
  // TrvActionInsertEndnote
  'Nota de &Fim', 'Nota de Fim|Insere uma nota de fim',
  // TrvActionEditNote
  'E&ditar Nota', 'Editar Nota|Come�a a editar uma nota de rodap� ou de fim',
  '&Ocultar', 'Ocultar|Oculta ou mostra o trecho selecionado',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Estilos...', 'Estilos|Abre o gerenciador de estilos',
  // TrvActionAddStyleTemplate
  '&Adicionar Estilo...', 'Adicionar Estilo|Cria um novo estilo de caractere ou de par�grafo',
  // TrvActionClearFormat,
  '&Limpar Formata��o', 'Limpar Formata��o|Limpa toda a formata��o de caracteres e de par�grafos do trecho selecionado',
  // TrvActionClearTextFormat,
  'Limpar Formata��o de &Caracteres', 'Limpar Formata��o de Caracteres|Limpa toda a formata��o de caracteres do texto selecionado',
  // TrvActionStyleInspector
  '&Inspetor de estilos', 'Inspetor de estilos|Exibe ou oculta o Inspetor de estilos',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Cancela', 'Fecha', 'Insere', '&Abre...', '&Salva...', '&Limpa', 'Ajuda', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'porcento',
  // left, top, right, bottom sides
  'Lado Esquerdo', 'Topo', 'Lado Direito', 'Baixo',
  // save changes? confirm title
  'Salva altera��es em %s?', 'Confirme',
  // warning: losing formatting
  '%s pode conter recursos que n�o s�o compat�veis com o formato de salvamento escolhido.'#13+
  'Quer salvar o documento neste formato?',
  // RVF format name
  'Formato RichView',
  // Error messages ------------------------------------------------------------
  'Erro',
  'Erro carregando arquivo.'#13#13'Causas poss�veis:'#13'- formato deste arquivo n�o � suportado por este aplicativo;'#13+
  '- o arquivo est� corrompido;'#13'- o arquivo est� aberto e travado por outro aplicativo.',
  'Erro carregando arquivo de imagem.'#13#13'Causas poss�veis:'#13'- o arquivo tem um formato que n�o � suportado por este aplicativo;'#13+
  '- o arquivo n�o cont�m uma imagem;'#13+
  '- o arquivo est� corrompido;'#13'- o arquivo est� aberto e travado por outro aplicativo.',
  'Erro salvando arquivo.'#13#13'Causas poss�veis:'#13'- sem espa�o em disco;'#13+
  '- disco est� protegido contra escrita;'#13'- m�dia remov�vel n�o est� inserida;'#13+
  '- o arquivo esta aberto e travado por outro aplicativo;'#13'- m�dia do disco est� corrompida.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Arquivos RichView (*.rvf)|*.rvf',  'Arquivos RTF (*.rtf)|*.rtf' , 'Arquivos XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Arquivos Texto (*.txt)|*.txt', 'Arquivos Texto - Unicode (*.txt)|*.txt', 'Arquivos Texto - Autodetecta (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Arquivos HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Simplificado (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documentos do Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Busca Completa', 'O texto ''%s'' n�o foi encontrado.',
  // 1 string replaced; Several strings replaced
  '1 ocorr�ncia substitu�da.', '%d ocorr�ncias substitu�das.',
  // continue search from the beginning/end?
  'O final do documento foi atingido, continua do in�cio?',
  'O in�cio do documento foi atingido, continua do final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparente', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Preto', 'Marrom', 'Verde Oliva', 'Verde Escuro', 'Azul Petr�leo Escuro', 'Azul escuro', 'Indigo', 'Cinza-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Vermelho Escuro', 'Laranja', 'Amarelo Escuro', 'Verde', 'Azul Petr�leo', 'Azul', 'Azul-Cinza', 'Cinza-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vermelho', 'Laranja Claro', 'Verde Lima', 'Verde Mar', 'Azul �gua', 'Azul Claro', 'Violeta', 'Cinza-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa Choque', 'Dourado', 'Amarelo', 'Verde Brilhante', 'Turquesa', 'Azul C�u', 'Ameixa', 'Cinza-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa', 'Marrom Claro', 'Amarelo Claro', 'Verde Claro', 'Turquesa Claro', 'Azul P�lido', 'Lavanda', 'Branco',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparente', '&Auto', '&Mais Cores...', '&Padr�o',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fundo', 'C&or:', 'Posi��o', 'Fundo', 'Texto exemplo.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nenhum', '&Expandido', 'Lado a Lado F&ixo', '&Lado a Lado', 'C&entralizado',
  // Padding button
  '&Ajuste...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Cor Preenchimento', '&Aplica em:', '&Mais Cores...', '&Ajuste...',
  'Favor selecionar uma cor',
  // [apply to:] text, paragraph, table, cell
  'texto', 'par�grafo' , 'tabela', 'c�lula',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Fonte', 'Fonte', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fonte:', '&Tamanho', 'Estilo', '&Negrito', '&It�lico',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Cor:', 'Plano de &Fundo:', '(Padr�o)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efeitos', 'Sublinhado', '&Sobrelinha', 'R&iscado', '&Tudo mai�sculas',
  // Sample, Sample text
  'Exemplo', 'Texto exemplo',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espa�amento Caracteres', '&Espa�amento:', '&Expandido', '&Condensado',
  // Offset group-box, Offset label, Down, Up,
  'Deslocamento', '&Deslocamento:', '&Baixo', '&Cima',
  // Scaling group-box, Scaling
  'Escala', 'Esc&ala:',
  // Sub/super script group box, Normal, Sub, Super
  'Subscritos and superescritos', '&Normal', 'Su&bscrito', 'Su&perescrito',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Ajuste', '&Cima:', '&Esquerda:', '&Baixo:', '&Direita:', 'Valores &iguais',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insere Hiperlink', 'Hiperlink', 'T&exto:', '&Destino', '<<sele��o>>',
  // cannot open URL
  'Imposs�vel navegar para "%s"',
  // hyperlink properties button, hyperlink style
  '&Personalizar...', '&Estilo:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Atributos de Hiperlink', 'Cores normal', 'Cores ativo',
  // Text [color], Background [color]
  '&Texto:', '&Fundo:',
  // Underline [color]
  '&Sublinhado:',
  // Text [color], Background [color] (different hotkeys)
  'T&exto:', 'F&undo',
  // Underline [color], Attributes group-box,
  'Su&blinhado:', 'Atributos',
  // Underline type: always, never, active (below the mouse)
  'Sub&linhado:', 'sempre', 'nunca', 'ativo',
  // Same as normal check-box
  'Como &normal',
  // underline active links check-box
  'Sub&linhar links ativos',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insere S�mbolo', '&Fonte:', 'Conj.&Caracteres:', 'Unicode',
  // Unicode block
  '&Bloco:',
  // Character Code, Character Unicode Code, No Character
  'C�d.Caractere: %d', 'C�d.Caractere: Unicode %d', '(sem caractere)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Insere Tabela', 'Tamanho Tabela', 'N�mero de &colunas:', 'N�mero de &linhas:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Layout Tabela', 'Tamanho &Autom�tico', 'Dimensiona tabela para ajustar na &janela', 'Dimensiona &manualmente',
  // Remember check-box
  'Lembra &dimens�es para novas tabelas',
  // VAlign Form ---------------------------------------------------------------
  'Posi��o',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriedades', 'Imagem', 'Posi��o e Tamanho', 'Linha', 'Tabela', 'Linhas', 'C�lulas',
  // Image Appearance, Image Misc, Number tabs
  'Apar�ncia', 'Outros', 'N�mero',
  // Box position, Box size, Box appearance tabs
  'Posi��o', 'Tamanho', 'Apar�ncia',
  // Preview label, Transparency group-box, checkbox, label
  'Visualiza:', 'Transpar�ncia', '&Transparente', '&Cor transparente:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'Al&tera...', '&Salvar...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alinhamento vertical', '&Alinhamento:',
  'baixo da linha de base do texto', 'meio da linha de base do texto',
  'topo com o topo da linha', 'base com a base da linha', 'meio com o meio da linha',
  // align to left side, align to right side
  'lado esquerdo', 'lado direito',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Desloca de:', 'Ajusta', '&Largura:', '&Altura:', 'Tamanho padr�o: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Redimensionar &proporcionalmente', '&Restaurar',
  // Inside the border, border, outside the border groupboxes
  'Dentro da borda', 'Borda', 'Fora da borda',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Cor de Preenchimento:', '&Margem interna:',
  // Border width, Border color labels
  '&Largura da borda:', '&Cor da borda:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espa�amento &horizontal:', 'Espa�amento &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Outros', '&Dica de ferramenta:',
  // web group-box, alt text
  'Web', '&Texto alternativo:',
  // Horz line group-box, color, width, style
  'Linha horizontal', '&Cor:', '&Largura:', '&Estilo:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabela', '&Largura:', 'Cor de &preenchimento:', 'Espa�amento das &c�lulas...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Margem interna &horiz. c�lula:', 'Margem interna &vert. c�lula:', 'Borda e fundo',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Laterais da &borda vis�veis...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Manter conte�do junto',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rota��o', '&Nenhuma', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Borda:', 'Laterais da &borda vis�veis...',
  // Border, CellBorders buttons
  'Borda da &tabela...', 'Bordas das &c�lulas...',
  // Table border, Cell borders dialog titles
  'Borda da Tabela', 'Bordas Padr�o das C�lulas',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Imprimindo', '&Manter tabela em uma p�gina', 'N�mero de linhas de &cabe�alho:',
  'Linhas de cabe�alho s�o repetidas em cada p�gina da tabela',
  // top, center, bottom, default
  '&Topo', '&Centralizado', '&Baixo', '&Padr�o',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Configura��es', '&Largura Preferencial:', '&Altura m�nima:', 'Cor &Preenchimento:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Borda', 'Lados vis�veis:',  'C&or sombra:', 'Cor &luz', 'C&or:',
  // Background image
  '&Imagem...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Posi��o horizontal', 'Posi��o vertical', 'Posi��o no texto',
  // Position types: align, absolute, relative
  'Alinhar', 'Posi��o absoluta', 'Posi��o relativa',
  // [Align] left side, center, right side - left side of box to left side of
  'lado esquerdo com a esquerda de',
  'centro do quadro com o centro de',
  'lado direito com a direita de',
  // [Align] top side, center, bottom side
  'lado de cima com o lado de cima de',
  'centro do quadro com o centro de',
  'lado de baixo com o lado de baixo de',
  // [Align] relative to
  'relativo a',
  // [Position] to the right of the left side of
  'para a direita do lado esquerdo de',
  // [Position] below the top side of
  'embaixo do lado superior de',
  // Anchors: page, main text area (x2)
  '&P�gina', '&�rea principal de texto', 'P�&gina', '�rea principal de te&xto',
  // Anchors: left margin, right margin
  'Margem &esquerda', 'Margem &direita',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Margem &superior', 'Margem &inferior', 'Margem &interna', 'Margem &externa',
  // Anchors: character, line, paragraph
  '&Caractere', 'L&inha', 'Par�&grafo',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Margens &espelhadas', 'La&yout em c�lula de tabela',
  // Above text, below text
  '&Sobre o texto', '&Abaixo do texto',
  // Width, Height groupboxes, Width, Height labels
  'Largura', 'Altura', '&Largura:', '&Altura:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alinhamento vertical', 'Em c&ima', '&Centralizado', 'Em&baixo',
  // Border and background button and title
  'B&orda e fundo...', 'Borda e fundo do quadro',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Colar Especial', '&Colar como:', 'Formato Rich text', 'Formato HTML',
  'Texto', 'Texto Unicode', 'Imagem Bitmap', 'Imagem Metafile',
  'Arquivos gr�ficos',  'URL',
  // Options group-box, styles
  'Op��es', '&Estilos:',
  // style options: apply target styles, use source styles, ignore styles
  'aplicar estilos do documento destino', 'manter estilos e apar�ncia',
  'manter apar�ncia, ignorar estilos',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Marcadores e Numera��o', 'Marcadores', 'Numeradores', 'Listas com Marcadores', 'Listas Numeradas',
  // Customize, Reset, None
  '&Personaliza...', '&Restaura', 'Nenhum',
  // Numbering: continue, reset to, create new
  'continua numera��o', 'reinicializa numera��o em', 'cria uma nova lista, iniciando com',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Letras Mai�sculas', 'N�meros Romanos Mai�sculos', 'Decimal', 'Letras Min�sculas', 'N�meros Romanos Min�sculos',
  // Bullet type
  'C�rculo', 'Disco', 'Quadrado',
  // Level, Start from, Continue numbering, Numbering group-box
  '&N�vel:', '&Iniciar em:', '&Continuar', 'Numera��o',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personaliza Lista', 'N�veis', '&Quant.:', 'Propriedades da Lista', 'Tipo de &lista:',
  // List types: bullet, image
  'marcador', 'imagem', 'Inserir N�mero|',
  // Number format, Number, Start level from, Font button
  'Formato de &N�meros:', 'N�meros', 'N�vel &inicial a partir de :', '&Fonte...',
  // Image button, bullet character, Bullet button,
  '&Imagem...', 'C&aractere Marcador', '&Marcador...',
  // Position of list text, bullet, number, image, text
  'Posi��o do texto de lista', 'Posi��o do marcador', 'Posi��o da numera��o', 'Posi��o da imagem', 'Posi��o do texto',
  // at, left indent, first line indent, from left indent
  '&em:', 'Recuo &esquerda:', 'Recuo &primeira linha:', 'do recuo esquerdo',
  // [only] one level preview, preview
  'Visualiza &um n�vel', 'Visualiza',
  // Preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'alinha � esquerda', 'alinha � direita', 'centraliza',
  // level #, this level (for combo-box)
  'N�vel %d', 'Este n�vel',
  // Marker character dialog title
  'Edita Caractere Marcador',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Borda e Fundo do Par�grafo', 'Borda', 'Fundo',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Configura��es', '&Cor:', '&Largura:', 'Largura int&erna:', 'D&eslocamento...',
  // Sample, Border type group-boxes;
  'Exemplo', 'Tipo de borda',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nenhum', '&Simples', '&Duplo', '&Triplo', 'Grosso &interno', 'Grosso &externo',
  // Fill color group-box; More colors, padding buttons
  'Cor de preenchimento', '&Mais cores...', '&Margem...',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // title and group-box in Offsets dialog
  'Deslocamento', 'Deslocamento da borda',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Par�grafo', 'Alinhamento', '&Esquerda', '&Direita', '&Centraliza', '&Justifica',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espa�amento', '&Antes:', '&Depois:', 'E&spa�amento de Linha:', 'p&or:',
  // Indents group-box; indents: left, right, first line
  'Recuo', '&Esquerda:', '&Direita:', '&Primeira linha:',
  // indented, hanging
  '&Recuado', '&Deslocado', 'Exemplo',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simples', '1,5 linhas', 'Duplo', 'Pelo menos', 'Exatamente', 'M�ltiplo',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Recuos e Espa�amento', 'Tabula��es', 'Fluxo de texto',
  // tab stop position label; buttons: set, delete, delete all
  'Posi��o de &Tabula��o:', '&Marca', '&Desmarca', 'Desmarca &Todas',
  // tab align group; left, right, center aligns,
  'Alinhamento', '&Esquerda', '&Direita', '&Centraliza',
  // leader radio-group; no leader
  'Primeira Linha', '(Nenhum)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabula��es a desmarcar:', '(Nenhuma)', 'Todas.',
  // Pagination group-box, keep with next, keep lines together
  'Pagina��o', '&Manter com pr�ximo', 'Manter &linhas juntas',
  // Outline level, Body text, Level #
  '&N�vel do t�pico:', 'Corpo de Texto', 'N�vel %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Visualiza Impress�o', 'Largura da P�gina', 'P�gina Inteira', 'P�ginas:',
  // Invalid Scale, [page #] of #
  'Favor entrar com um n�mero entre 10 e 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primeira P�gina', 'P�gina Anterior', 'P�gina Seguinte', '�ltima P�gina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espa�amento de C�lulas', 'Espa�amento', 'E&ntre c�lulas', 'Da borda da tabela at� as c�lulas',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Bordas', 'Configura��es', '&Cor:', 'Cor &Luz:', '&Cor sombra:',
  // Width; Border type group-box;
  '&Largura:', 'Tipo de borda',
  // Border types: none, sunken, raised, flat
  '&Nenhum', '&Solarizado', '&Elevado', '&Liso',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Divide', 'Divide para',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'N�mero e&specificado de linhas e colunas', 'C�lulas &originais',
  // number of columns, rows, merge before
  'N�mero de &colunas:', 'N�mero de &linhas:', '&Mescla antes de dividir',
  // to original cols, rows check-boxes
  'Divide para co&lunas originais', 'Divide para li&nhas originais',
  // Add Rows form -------------------------------------------------------------
  'Adiciona Linhas', 'N�mero de &Linhas:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Configura��o de P�gina', 'P�gina', 'Cabe�alho e Rodap�',
  // margins group-box, left, right, top, bottom
  'M�rgens (mil�metros)', 'M�rgens (polegadas)', '&Esquerda:', '&Topo:', '&Direita:', '&Baixo:',
  // mirror margins check-box
  'Espelha &margens',
  // orientation group-box, portrait, landscape
  'Orienta��o', '&Retrato', '&Paisagem',
  // paper group-box, paper size, default paper source
  'Papel', 'T&amanho:', '&Fonte:',
  // header group-box, print on the first page, font button
  'Cabe�alho', '&Texto:', 'C&abe�alho na primeira p�gina', '&Fonte...',
  // header alignments: left, center, right
  '&Esquerda', '&Centraliza', '&Direita',
  // the same for footer (different hotkeys)
  'Rodap�', 'Te&xto:', 'Rodap� na primeira &p�gina', 'F&onte...',
  'E&squerda', 'Ce&ntraliza', 'D&ireita',
  // page numbers group-box, start from
  'N�meros de p�gina', 'Inicia e&m:',
  // hint about codes
  'Combina��es de caracteres especiais:'#13'&&p - n�mero de p�gina; &&P - total de p�ginas; &&d - data atual; &&t - hora atual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'C�digo de P�gina do Arqivo de Texto', '&Escolha a codifica��o do arquivo:',
  // thai, japanese, chinese (simpl), korean
  'Tailand�s', 'Japon�s', 'Chin�s (Simplificado)', 'Coreano',
  // chinese (trad), central european, cyrillic, west european
  'Chin�s (Tradicional)', 'Europeu Central e Oriental', 'Cir�lico', 'Europeu Ocidental',
  // greek, turkish, hebrew, arabic
  'Greco', 'Turco', 'Hebreu', 'Ar�be',
  // baltic, vietnamese, utf-8, utf-16
  'B�ltico', 'Vietnam�s', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estilo Visual', '&Selecione o estilo:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Converter em Texto', 'Selecione o &delimitador:',
  // line break, tab, ';', ','
  'quebra de linha', 'tabula��o', 'ponto-e-v�rgula', 'v�rgula',
  // Table sort form -----------------------------------------------------------
  // error message
  'Uma tabela com linhas mescladas n�o pode ser ordenada',
  // title, main options groupbox
  'Ordenar Tabela', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar por coluna:', 'Diferenciar &mai�sculas de min�sculas',
  // heading row, range or rows
  'Excluir linha de &cabe�alho', 'Linhas a serem ordenadas: de %d at� %d',
  // order, ascending, descending
  'Ordem', 'C&rescente', '&Decrescente',
  // data type, text, number
  'Tipo', '&Texto', '&N�mero',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserir N�mero', 'Propriedades', 'Nome do &contador:', 'Tipo de &numera��o:',
  // numbering groupbox, continue, start from
  'Numera��o', 'C&ontinuar', '&Come�ar em:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Legenda', '&Etiqueta:', '&Excluir etiqueta da legenda',
  // position radiogroup
  'Posi��o', '&Sobre o objeto selecionado', '&Abaixo do objeto selecionado',
  // caption text
  '&Texto da legenda:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numera��o', 'Figura', 'Tabela',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Laterais da borda vis�veis', 'Borda',
  // Left, Top, Right, Bottom checkboxes
  'Lado &esquerdo', 'Lado de &cima', 'Lado &direito', 'Lado de &baixo',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Redimensiona Coluna da Tabela', 'Redimensiona Linha da Tabela',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Recuo da Primeira Linha', 'Recuo Esquerdo', 'Deslocamento', 'Recuo Direito',
  // Hints on lists: up one level (left), down one level (right)
  'Um n�vel acima', 'Um n�vel abaixo',
  // Hints for margins: bottom, left, right and top
  'Margem inferior', 'Margem esquerda', 'Margem direita', 'Margem superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabula��o � Esquerda', 'Tabula��o � Direita', 'Tabula��o Centralizada', 'Tabula��o Alinhada ao Ponto Decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Recuo Normal', 'Sem espa�amento', 'Cabe�alho %d', 'Lista',
  // Hyperlink, Title, Subtitle
  'Link', 'T�tulo', 'Subt�tulo',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '�nfase', '�nfase Atenuada', '�nfase Acentuada', 'Forte',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cita��o', 'Cita��o Acentuada', 'Refer�ncia Atenuada', 'Refer�ncia Destacada',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Corpo de Texto', 'HTML Vari�vel', 'HTML C�digo', 'HTML Acr�nimo',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Defini��o', 'HTML Teclado', 'HTML Amostra', 'HTML M�quina de Escrever',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatado', 'HTML Cita��o', 'Cabe�alho', 'Rodap�', 'Num�ro de P�gina',
  // Caption
  'Legenda',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Refer�ncia da Nota de Fim', 'Refer�ncia da Nota de Rodap�', 'Texto da Nota de Fim', 'Texto da Nota de Rodap�',
  // Sidenote Reference, Sidenote Text
  'Refer�ncia da Nota em Quadro', 'Texto da Nota em Quadro',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'cor', 'cor de fundo', 'transparente', 'padr�o', 'cor do sublinhado',
  // default background color, default text color, [color] same as text
  'cor de fundo padr�o', 'cor de texto padr�o', 'mesma do texto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'simples', 'grossa', 'dupla', 'pontilhada', 'grossa pontilhada', 'com tra�os',
  // underline types: thick dashed, long dashed, thick long dashed,
  'grossa com tra�os', 'com tra�os longos', 'grossa com tra�os longos',
  // underline types: dash dotted, thick dash dotted,
  'com tra�os e pontos', 'grossa com tra�os e pontos',
  // underline types: dash dot dotted, thick dash dot dotted
  'com tra�os e pontos duplos', 'grossa com tra�os e pontos duplos',
  // sub/sobrescrito: n�o, subscito, sobrescrito
  'sem sub/sobrescrito', 'subscrito', 'sobrescrito',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'modo bidirecional:', 'herdado', 'da esquerda para a direita', 'da direita para a esquerda',
  // bold, not bold
  'negrito', 'sem negrito',
  // italic, not italic
  'it�lico', 'sem it�lico',
  // underlined, not underlined, default underline
  'sublinhado', 'sem sublinhado', 'sublinhado padr�o',
  // struck out, not struck out
  'tachado', 'sem tachado',
  // overlined, not overlined
  'sobrelinha', 'sem sobrelinha',
  // all capitals: yes, all capitals: no
  'mai�sculas', 'mai�sculas desligadas',
  // vertical shift: none, by x% up, by x% down
  'sem deslocamento vertical', 'deslocado %d%% para cima', 'deslocado %d%% para baixo',
  // characters width [: x%]
  'largura dos caracteres',
  // character spacing: none, expanded by x, condensed by x
  'espa�amento de caracteres normal', 'espa�amento aumentado %s', 'espa�amento diminu�do %s',
  // charset
  'cursivo',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'fonte padr�o', 'par�grafo padr�o', 'herdado',
  // [hyperlink] highlight, default hyperlink attributes
  'destaque', 'padr�o',
  // paragraph aligmnment: left, right, center, justify
  'ainhar � esquerda', 'alinhar � direita', 'centralizado', 'justificado',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altura da linha: %d%%', 'espa�o entre linhas: %s', 'altura da linha: pelo menos %s',
  'altura da linha: exatamente %s',
  // no wrap, wrap
  'quebra de linha desativada', 'quebra de linha',
  // keep lines together: yes, no
  'manter linhas juntas', 'n�o manter linhas juntas',
  // keep with next: yes, no
  'manter com o pr�ximo par�grafo', 'n�o manter com o pr�ximo par�grafo',
  // read only: yes, no
  'somente leitura', 'edit�vel',
  // body text, heading level x
  'N�vel da estrutura de t�picos: corpo do texto', 'N�vel da estrutura de t�picos: %d',
  // indents: first line, left, right
  'recuo da primeira linha', 'afastamento pela esquerda', 'afastamento pela direita',
  // space before, after
  'espa�o antes', 'espa�o depois',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabula��es', 'nenhuma', 'alinhar', 'esquerda', 'direita', 'centro',
  // tab leader (filling character)
  'caractere de preenchimento',
  // no, yes
  'n�o', 'sim',
  // [padding/spacing/side:] left, top, right, bottom
  'esquerda', 'superior', 'direita', 'inferior',
  // border: none, single, double, triple, thick inside, thick outside
  'nenhuma', 'simples', 'dupla', 'tripla', 'grossa dentro', 'grossa fora',
  // background, border, padding, spacing [between text and border]
  'fundo', 'borda', 'margem interna', 'espa�amento',
  // border: style, width, internal width, visible sides
  'estilo', 'largura', 'largura interna', 'lados vis�veis',
  // style inspector -----------------------------------------------------------
  // title
  'Inspetor de Estilos',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estilo', '(Nenhum)', 'Par�grafo', 'Fonte', 'Atributos',
  // border and background, hyperlink, standard [style]
  'Bordas e preenchimento', 'Link', '(Padr�o)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estilos', 'Estilo',
  // name, applicable to,
  '&Nome:', 'Aplic�&vel a:',
  // based on, based on (no &),
  '&Baseado em:', 'Baseado em:',
  //  next style, next style (no &)
  'Estilo para o par�grafo &seguinte:', 'Estilo para o par�grafo seguinte:',
  // quick access check-box, description and preview
  'Acesso &r�pido', 'Descri��o e &visualiza��o:',
  // [applicable to:] paragraph and text, paragraph, text
  'par�grafos e caracteres', 'par�grafos', 'caracteres',
  // text and paragraph styles, default font
  'Estilos de Par�grafo e de Caracteres', 'Fonte Padr�o',
  // links: edit, reset, buttons: add, delete
  'Editar', 'Redefinir', '&Incluir', '&Apagar',
  // add standard style, add custom style, default style name
  'Incluir Estilo &Padr�o...', 'Incluir Estilo &Personalizado', 'Estilo %d',
  // choose style
  'Escolher &estilo:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Estilos RichView (*.rvst)|*.rvst', 'Erro carregando arquivo', 'O arquibo n�o cont�m estilos',
  // Title, group-box, import styles
  'Importar Estilos de Arquivo', 'Importar', 'I&mportar estilos:',
  // existing styles radio-group: Title, override, auto-rename
  'Estilos existentes', '&sobrescrever', '&incluir com outro nome',
  // Select, Unselect, Invert,
  '&Selecionar', '&Remover sele��o', '&Inverter',
  // select/unselect: all styles, new styles, existing styles
  '&Todos os estilos', 'Estilos &Novos', 'Estilos &Existentes',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Limpar Forma��o)', '(Todos os Estilos)',
  // dialog title, prompt
  'Escolher', '&Escolher estilo a ser aplicado:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sin�nimos', '&Ignorar Todos', '&Adicionar ao Dicion�rio',
  // Progress messages ---------------------------------------------------------
  'Fazendo download de %s', 'Preparando para imprimir...', 'Imprimindo a p�gina %d',
  'Convertendo de RTF...',  'Convertendo para RTF...',
  'Lendo %s...', 'Gravando %s...', 'texto',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Sem nota', 'Nota de rodap�', 'Nota de fim', 'Nota em quadro', 'Quadro de texto'
  );


initialization
  RVA_RegisterLanguage(
    'Portuguese (Brazilian)', // english language name, do not translate
    'Portugu�s (Brasileiro)', // native language name
    ANSI_CHARSET, @Messages);

end.
