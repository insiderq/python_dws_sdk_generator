﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RichViewActions.pas' rev: 27.00 (Windows)

#ifndef RichviewactionsHPP
#define RichviewactionsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <System.DateUtils.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVTInplace.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RVERVData.hpp>	// Pascal unit
#include <RVMisc.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVFloatingBox.hpp>	// Pascal unit
#include <RVFloatingPos.hpp>	// Pascal unit
#include <RichViewXML.hpp>	// Pascal unit
#include <RVNormalize.hpp>	// Pascal unit
#include <rvhtmlimport.hpp>	// Pascal unit
#include <TB2Item.hpp>	// Pascal unit
#include <SpTBXItem.hpp>	// Pascal unit
#include <SpTBXSkins.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVOfficeCnv.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.Actions.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Richviewactions
{
//-- type declarations -------------------------------------------------------
typedef Vcl::Stdctrls::TComboBox TRVAComboBox;

enum DECLSPEC_DENUM TrvFileImportFilter : unsigned char { ffiRVF, ffiRTF, ffiXML, ffiTextANSI, ffiTextUnicode, ffiTextAuto, ffiCustom, ffiHTML, ffiOfficeConverters };

typedef TrvFileImportFilter TrvFileOpenFilter;

enum DECLSPEC_DENUM TrvFileExportFilter : unsigned char { ffeRVF, ffeRTF, ffeXML, ffeTextANSI, ffeTextUnicode, ffeCustom, ffeHTMLCSS, ffeHTML, ffeDocX, ffeOfficeConverters };

typedef TrvFileExportFilter TrvFileSaveFilter;

typedef System::Set<TrvFileImportFilter, TrvFileImportFilter::ffiRVF, TrvFileImportFilter::ffiOfficeConverters> TrvFileImportFilterSet;

typedef System::Set<TrvFileOpenFilter, TrvFileOpenFilter::ffiRVF, TrvFileOpenFilter::ffiCustom> TrvFileOpenFilterSet;

typedef System::Set<TrvFileExportFilter, TrvFileExportFilter::ffeRVF, TrvFileExportFilter::ffeOfficeConverters> TrvFileExportFilterSet;

typedef System::Set<TrvFileSaveFilter, TrvFileSaveFilter::ffeRVF, TrvFileSaveFilter::ffeCustom> TrvFileSaveFilterSet;

enum DECLSPEC_DENUM TrvPaperMarginsUnits : unsigned char { rvpmuMillimeters, rvpmuInches };

enum DECLSPEC_DENUM TrvaColorInterface : unsigned char { rvacNone, rvacColorDialog, rvacAdvanced };

typedef void __fastcall (__closure *TRVFileChangeEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, const System::UnicodeString FileName, TrvFileSaveFilter FileFormat, bool IsNew);

typedef void __fastcall (__closure *TRVOpenFileEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, const System::UnicodeString FileName, TrvFileOpenFilter FileFormat, int CustomFilterIndex);

typedef void __fastcall (__closure *TRVSaveFileEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, const System::UnicodeString FileName, TrvFileSaveFilter FileFormat, int CustomFilterIndex);

enum DECLSPEC_DENUM TRVAEditorControlCommand : unsigned char { rvaeccIsEditorControl, rvaeccHasSelection, rvaeccCanPaste, rvaeccCanPasteText, rvaeccCanUndo, rvaeccCopy, rvaeccCut, rvaeccPaste, rvaeccPasteText, rvaeccUndo };

typedef void __fastcall (__closure *TRVCustomItemPropertiesDialog)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, bool &DoDefault);

typedef void __fastcall (__closure *TRVAEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor);

typedef void __fastcall (__closure *TRVGetFormClassEvent)(System::TObject* Sender, Vcl::Forms::TFormClass &FormClass);

typedef void __fastcall (__closure *TRVGetControlEvent)(System::TObject* Sender, Vcl::Controls::TControl* &Control);

enum DECLSPEC_DENUM TRVAUserInterface : unsigned char { rvauiFull, rvauiHTML, rvauiRTF, rvauiText };

class DELPHICLASS TRVAFont;
class PASCALIMPLEMENTATION TRVAFont : public Vcl::Graphics::TFont
{
	typedef Vcl::Graphics::TFont inherited;
	
public:
	/* TFont.Create */ inline __fastcall TRVAFont(void) : Vcl::Graphics::TFont() { }
	/* TFont.Destroy */ inline __fastcall virtual ~TRVAFont(void) { }
	
};


class DELPHICLASS TRVAHFInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVAHFInfo : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TRVAFont* FFont;
	System::UnicodeString FText;
	System::Classes::TAlignment FAlignment;
	bool FPrintOnFirstPage;
	void __fastcall SetFont(TRVAFont* const Value);
	
public:
	__fastcall TRVAHFInfo(void);
	__fastcall virtual ~TRVAHFInfo(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	
__published:
	__property System::UnicodeString Text = {read=FText, write=FText};
	__property System::Classes::TAlignment Alignment = {read=FAlignment, write=FAlignment, default=2};
	__property bool PrintOnFirstPage = {read=FPrintOnFirstPage, write=FPrintOnFirstPage, default=1};
	__property TRVAFont* Font = {read=FFont, write=SetFont};
};

#pragma pack(pop)

class DELPHICLASS TrvAction;
typedef void __fastcall (__closure *TRVGetActionControlCoordsEvent)(TrvAction* Sender, System::Types::TRect &R);

typedef void __fastcall (__closure *TRVStyleNeededEvent)(TrvAction* Sender, Rvstyle::TRVStyle* RVStyle, Rvstyle::TCustomRVInfo* StyleInfo, int &StyleNo);

typedef void __fastcall (__closure *TRVAddStyleEvent)(TrvAction* Sender, Rvstyle::TCustomRVInfo* StyleInfo);

typedef void __fastcall (__closure *TRVAEditEvent)(TrvAction* Sender, Rvedit::TCustomRichViewEdit* Edit);

class DELPHICLASS TrvCustomAction;
typedef void __fastcall (__closure *TRVAEditEvent2)(TrvCustomAction* Sender, Rvedit::TCustomRichViewEdit* Edit);

enum DECLSPEC_DENUM TRVAFileOperation : unsigned char { rvafoOpen, rvafoSave, rvafoExport, rvafoInsert };

typedef void __fastcall (__closure *TRVCustomFileOperationEvent)(TrvAction* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString FileName, TRVAFileOperation Operation, TrvFileSaveFilter &SaveFormat, int &CustomFilterIndex, bool &Success);

typedef void __fastcall (__closure *TRVADownloadEvent)(TrvAction* Sender, const System::UnicodeString Source);

class DELPHICLASS TRVAPopupMenu;
typedef void __fastcall (__closure *TRVALiveSpellGetSuggestionsEvent)(TRVAPopupMenu* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);

typedef void __fastcall (__closure *TRVALiveSpellIgnoreAllEvent)(TRVAPopupMenu* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);

typedef void __fastcall (__closure *TRVALiveSpellAddEvent)(TRVAPopupMenu* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);

typedef void __fastcall (__closure *TRVALiveSpellReplaceEvent)(TRVAPopupMenu* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);

typedef void __fastcall (__closure *TRVA2LiveSpellGetSuggestionsEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);

typedef void __fastcall (__closure *TRVA2LiveSpellIgnoreAllEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);

typedef void __fastcall (__closure *TRVA2LiveSpellAddEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);

typedef void __fastcall (__closure *TRVA2LiveSpellReplaceEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);

enum DECLSPEC_DENUM TRVASearchScope : unsigned char { rvssFromCursor, rvssAskUser, rvssGlobal };

class DELPHICLASS TRVAControlPanel;
class PASCALIMPLEMENTATION TRVAControlPanel : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	System::Uitypes::TFontName FDialogFontName;
	int FDialogFontSize;
	int FDefaultMargin;
	int FFirstPageNumber;
	int FLanguageIndex;
	bool FUseXPThemes;
	bool FActionsEnabled;
	bool FUseHelpFiles;
	bool FAddColorNameToHints;
	bool FShowSoftPageBreaks;
	bool FRVFLocalizable;
	bool FPixelBorders;
	bool FUseTextCodePageDialog;
	bool FXMLLocalizable;
	System::UnicodeString FXMLFilter;
	Richviewxml::TRichViewXML* FRVXML;
	System::Uitypes::TColor FDefaultColor;
	Rvscroll::TCustomRVControl* FDefaultControl;
	Vcl::Dialogs::TColorDialog* FColorDialog;
	bool FAutoDeleteUnusedStyles;
	System::UnicodeString FRVFFilter;
	System::UnicodeString FDefaultExt;
	System::UnicodeString FRVFormatTitle;
	System::UnicodeString FDefaultFileName;
	TrvFileExportFilter FDefaultFileFormat;
	int FDefaultCustomFilterIndex;
	Ptblrv::TRVPrint* FRVPrint;
	TRVAUserInterface FUserInterface;
	Rvstyle::TRVUnits FUnitsDisplay;
	Rvstyle::TRVStyleUnits FUnitsProgram;
	TRVASearchScope FSearchScope;
	TRVAHFInfo* FHeader;
	TRVAHFInfo* FFooter;
	Rvdocparams::TRVDocParameters* FDefaultDocParameters;
	System::UnicodeString FRVStylesFilter;
	System::UnicodeString FRVStylesExt;
	Rvhtmlimport::TRvHtmlImporter* FRVHTML;
	TRVAddStyleEvent FOnAddStyle;
	TRVStyleNeededEvent FOnStyleNeeded;
	TRVAEditEvent FOnMarginsChanged;
	TRVAEditEvent2 FOnViewChanged;
	TRVCustomFileOperationEvent FOnCustomFileOperation;
	TRVADownloadEvent FOnDownload;
	TRVAEditEvent FOnBackgroundChange;
	TRVGetActionControlCoordsEvent FOnGetActionControlCoords;
	void __fastcall SetDefaultControl(Rvscroll::TCustomRVControl* const Value);
	void __fastcall SetColorDialog(Vcl::Dialogs::TColorDialog* const Value);
	void __fastcall SetRVXML(Richviewxml::TRichViewXML* const Value);
	void __fastcall SetRVHTML(Rvhtmlimport::TRvHtmlImporter* const Value);
	void __fastcall SetRVPrint(Ptblrv::TRVPrint* const Value);
	Rvalocalize::TRVALanguageName __fastcall GetLanguage(void);
	void __fastcall SetLanguage(const Rvalocalize::TRVALanguageName Value);
	void __fastcall SetDefaultDocParameters(Rvdocparams::TRVDocParameters* const Value);
	void __fastcall SetHeader(TRVAHFInfo* Value);
	void __fastcall SetFooter(TRVAHFInfo* Value);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Loaded(void);
	void __fastcall DoOnBackgroundChange(TrvAction* Sender, Rvedit::TCustomRichViewEdit* Edit);
	System::Types::TRect __fastcall GetActionControlCoords(TrvAction* Sender);
	void __fastcall DoOnDownload(TrvAction* Sender, const System::UnicodeString Source);
	void __fastcall DoOnAddStyle(TrvAction* Sender, Rvstyle::TCustomRVInfo* StyleInfo);
	int __fastcall DoStyleNeeded(TrvAction* Sender, Rvstyle::TRVStyle* RVStyle, Rvstyle::TCustomRVInfo* StyleInfo);
	bool __fastcall DoOnCustomFileOperation(TrvAction* Sender, Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString FileName, TRVAFileOperation Operation, TrvFileSaveFilter &SaveFormat, int &CustomFilterIndex);
	void __fastcall DoImportPicture(Richview::TCustomRichView* Sender, const System::UnicodeString Location, int Width, int Height, Vcl::Graphics::TGraphic* &Graphic);
	void __fastcall SetLanguageIndex(int Value);
	
public:
	System::StaticArray<Rvalocalize::TRVAColorRecord, 40> ColorNames;
	__fastcall virtual TRVAControlPanel(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVAControlPanel(void);
	void __fastcall Activate(void);
	void __fastcall DoOnMarginsChanged(TrvAction* Sender, Rvedit::TCustomRichViewEdit* Edit);
	void __fastcall DoOnViewChanged(TrvCustomAction* Sender, Rvedit::TCustomRichViewEdit* Edit);
	void __fastcall InitImportPictures(TrvAction* Sender);
	void __fastcall DoneImportPictures(void);
	__property int FirstPageNumber = {read=FFirstPageNumber, write=FFirstPageNumber, nodefault};
	__property int LanguageIndex = {read=FLanguageIndex, write=SetLanguageIndex, nodefault};
	
__published:
	__property bool UseXPThemes = {read=FUseXPThemes, write=FUseXPThemes, default=1};
	__property System::Uitypes::TFontName DialogFontName = {read=FDialogFontName, write=FDialogFontName};
	__property int DialogFontSize = {read=FDialogFontSize, write=FDialogFontSize, default=8};
	__property Rvscroll::TCustomRVControl* DefaultControl = {read=FDefaultControl, write=SetDefaultControl};
	__property Vcl::Dialogs::TColorDialog* ColorDialog = {read=FColorDialog, write=SetColorDialog};
	__property System::UnicodeString RVFFilter = {read=FRVFFilter, write=FRVFFilter};
	__property System::UnicodeString RVStylesFilter = {read=FRVStylesFilter, write=FRVStylesFilter};
	__property System::UnicodeString RVStylesExt = {read=FRVStylesExt, write=FRVStylesExt};
	__property System::UnicodeString DefaultExt = {read=FDefaultExt, write=FDefaultExt};
	__property System::UnicodeString RVFormatTitle = {read=FRVFormatTitle, write=FRVFormatTitle};
	__property System::UnicodeString DefaultFileName = {read=FDefaultFileName, write=FDefaultFileName};
	__property TrvFileSaveFilter DefaultFileFormat = {read=FDefaultFileFormat, write=FDefaultFileFormat, default=0};
	__property int DefaultCustomFilterIndex = {read=FDefaultCustomFilterIndex, write=FDefaultCustomFilterIndex, default=1};
	__property bool AutoDeleteUnusedStyles = {read=FAutoDeleteUnusedStyles, write=FAutoDeleteUnusedStyles, default=1};
	__property System::UnicodeString XMLFilter = {read=FXMLFilter, write=FXMLFilter};
	__property Richviewxml::TRichViewXML* RVXML = {read=FRVXML, write=SetRVXML};
	__property Rvhtmlimport::TRvHtmlImporter* RVHTMLImporter = {read=FRVHTML, write=SetRVHTML};
	__property bool ActionsEnabled = {read=FActionsEnabled, write=FActionsEnabled, default=1};
	__property bool UseHelpFiles = {read=FUseHelpFiles, write=FUseHelpFiles, default=1};
	__property bool AddColorNameToHints = {read=FAddColorNameToHints, write=FAddColorNameToHints, default=1};
	__property int DefaultMargin = {read=FDefaultMargin, write=FDefaultMargin, default=5};
	__property System::Uitypes::TColor DefaultColor = {read=FDefaultColor, write=FDefaultColor, default=-16777211};
	__property Rvdocparams::TRVDocParameters* DefaultDocParameters = {read=FDefaultDocParameters, write=SetDefaultDocParameters};
	__property Ptblrv::TRVPrint* RVPrint = {read=FRVPrint, write=SetRVPrint};
	__property bool ShowSoftPageBreaks = {read=FShowSoftPageBreaks, write=FShowSoftPageBreaks, default=1};
	__property Rvalocalize::TRVALanguageName Language = {read=GetLanguage, write=SetLanguage};
	__property bool RVFLocalizable = {read=FRVFLocalizable, write=FRVFLocalizable, default=1};
	__property bool XMLLocalizable = {read=FXMLLocalizable, write=FXMLLocalizable, default=1};
	__property TRVStyleNeededEvent OnStyleNeeded = {read=FOnStyleNeeded, write=FOnStyleNeeded};
	__property TRVAddStyleEvent OnAddStyle = {read=FOnAddStyle, write=FOnAddStyle};
	__property TRVAEditEvent OnMarginsChanged = {read=FOnMarginsChanged, write=FOnMarginsChanged};
	__property TRVAEditEvent2 OnViewChanged = {read=FOnViewChanged, write=FOnViewChanged};
	__property TRVCustomFileOperationEvent OnCustomFileOperation = {read=FOnCustomFileOperation, write=FOnCustomFileOperation};
	__property TRVADownloadEvent OnDownload = {read=FOnDownload, write=FOnDownload};
	__property TRVAEditEvent OnBackgroundChange = {read=FOnBackgroundChange, write=FOnBackgroundChange};
	__property TRVGetActionControlCoordsEvent OnGetActionControlCoords = {read=FOnGetActionControlCoords, write=FOnGetActionControlCoords};
	__property TRVASearchScope SearchScope = {read=FSearchScope, write=FSearchScope, default=1};
	__property TRVAUserInterface UserInterface = {read=FUserInterface, write=FUserInterface, default=0};
	__property Rvstyle::TRVUnits UnitsDisplay = {read=FUnitsDisplay, write=FUnitsDisplay, default=4};
	__property Rvstyle::TRVStyleUnits UnitsProgram = {read=FUnitsProgram, write=FUnitsProgram, default=0};
	__property bool PixelBorders = {read=FPixelBorders, write=FPixelBorders, default=0};
	__property bool UseTextCodePageDialog = {read=FUseTextCodePageDialog, write=FUseTextCodePageDialog, default=1};
	__property TRVAHFInfo* Header = {read=FHeader, write=SetHeader};
	__property TRVAHFInfo* Footer = {read=FFooter, write=SetFooter};
};


__interface IRVAPopupMenu;
typedef System::DelphiInterface<IRVAPopupMenu> _di_IRVAPopupMenu;
__interface IRVAPopupMenu  : public System::IInterface 
{
	
public:
	virtual System::Classes::TComponent* __fastcall GetPopupComponent(void) = 0 ;
	virtual Vcl::Actnlist::TCustomActionList* __fastcall GetActionList(void) = 0 ;
	virtual void __fastcall SetActionList(Vcl::Actnlist::TCustomActionList* const Value) = 0 ;
	virtual int __fastcall GetMaxSuggestionsCount(void) = 0 ;
	virtual void __fastcall SetMaxSuggestionsCount(int Value) = 0 ;
	virtual void __fastcall ClearItems(void) = 0 ;
	virtual void __fastcall AddActionItem(Vcl::Actnlist::TAction* Action, const System::UnicodeString Caption = System::UnicodeString()) = 0 ;
	virtual void __fastcall AddSeparatorItem(void) = 0 ;
	virtual void __fastcall DeleteLastSeparatorItem(void) = 0 ;
	virtual System::UnicodeString __fastcall GetItemCaption(System::TObject* Sender) = 0 ;
	virtual int __fastcall GetItemIndex(System::TObject* Sender) = 0 ;
	virtual void __fastcall AddClickItem(System::Classes::TNotifyEvent Click, const System::UnicodeString Caption, System::Uitypes::TFontCharset Charset, bool Enabled, bool Default) = 0 ;
	virtual bool __fastcall WantLiveSpellGetSuggestions(void) = 0 ;
	virtual void __fastcall LiveSpellGetSuggestions(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions) = 0 ;
	virtual void __fastcall LiveSpellWordReplace(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo) = 0 ;
	virtual bool __fastcall WantLiveSpellIgnoreAll(void) = 0 ;
	virtual void __fastcall LiveSpellIgnoreAll(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo) = 0 ;
	virtual bool __fastcall WantLiveSpellAdd(void) = 0 ;
	virtual void __fastcall LiveSpellAdd(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo) = 0 ;
};

class DELPHICLASS TRVAPopupMenuHelper;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVAPopupMenuHelper : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	_di_IRVAPopupMenu FPopupMenu;
	Vcl::Actnlist::TCustomActionList* FActionList;
	int FStyleNo;
	int FMaxSuggestionsCount;
	TRVAControlPanel* FControlPanel;
	System::Classes::TStringList* FSuggestions;
	
public:
	TrvAction* __fastcall FindAction(System::TClass ActionClass);
	void __fastcall AddAction(System::TClass ActionClass, const System::UnicodeString Caption = System::UnicodeString());
	System::Classes::TStringList* __fastcall GetSuggestions(const System::UnicodeString Word, int StyleNo);
	void __fastcall CorrectMisspelling(System::TObject* Sender);
	void __fastcall OnIgnoreAll(System::TObject* Sender);
	void __fastcall OnAddToDict(System::TObject* Sender);
	TRVAControlPanel* __fastcall GetControlPanel(void);
	void __fastcall PreparePopup(int X, int Y);
	__fastcall virtual TRVAPopupMenuHelper(_di_IRVAPopupMenu APopupMenu);
	__fastcall virtual ~TRVAPopupMenuHelper(void);
	__property Vcl::Actnlist::TCustomActionList* ActionList = {read=FActionList, write=FActionList};
	__property TRVAControlPanel* ControlPanel = {read=FControlPanel, write=FControlPanel};
	__property int MaxSuggestionsCount = {read=FMaxSuggestionsCount, write=FMaxSuggestionsCount, nodefault};
};

#pragma pack(pop)

typedef Vcl::Menus::TPopupMenu TRVAPopupMenuBase;

typedef Vcl::Menus::TMenuItem TRVAPopupMenuItem;

typedef int __fastcall (*TRVAMessageBox)(const System::UnicodeString Text, const System::UnicodeString Caption, int Flags);

class PASCALIMPLEMENTATION TRVAPopupMenu : public Vcl::Menus::TPopupMenu
{
	typedef Vcl::Menus::TPopupMenu inherited;
	
private:
	TRVAPopupMenuHelper* FPopupMenuHelper;
	TRVALiveSpellGetSuggestionsEvent FOnLiveSpellGetSuggestions;
	TRVALiveSpellAddEvent FOnLiveSpellAdd;
	TRVALiveSpellIgnoreAllEvent FOnLiveSpellIgnoreAll;
	TRVALiveSpellReplaceEvent FOnLiveSpellWordReplace;
	TRVAControlPanel* __fastcall GetControlPanel(void);
	void __fastcall SetControlPanel(TRVAControlPanel* Value);
	
protected:
	System::Classes::TComponent* __fastcall GetPopupComponent(void);
	Vcl::Actnlist::TCustomActionList* __fastcall GetActionList(void);
	void __fastcall SetActionList(Vcl::Actnlist::TCustomActionList* const Value);
	int __fastcall GetMaxSuggestionsCount(void);
	void __fastcall SetMaxSuggestionsCount(int Value);
	void __fastcall ClearItems(void);
	void __fastcall AddActionItem(Vcl::Actnlist::TAction* Action, const System::UnicodeString Caption = System::UnicodeString());
	void __fastcall AddSeparatorItem(void);
	void __fastcall DeleteLastSeparatorItem(void);
	System::UnicodeString __fastcall GetItemCaption(System::TObject* Sender);
	int __fastcall GetItemIndex(System::TObject* Sender);
	void __fastcall AddClickItem(System::Classes::TNotifyEvent Click, const System::UnicodeString Caption, System::Uitypes::TFontCharset Charset, bool Enabled, bool Default);
	bool __fastcall WantLiveSpellGetSuggestions(void);
	void __fastcall LiveSpellGetSuggestions(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);
	void __fastcall LiveSpellWordReplace(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);
	bool __fastcall WantLiveSpellIgnoreAll(void);
	void __fastcall LiveSpellIgnoreAll(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	bool __fastcall WantLiveSpellAdd(void);
	void __fastcall LiveSpellAdd(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	
public:
	__fastcall virtual TRVAPopupMenu(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVAPopupMenu(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Popup(int X, int Y);
	
__published:
	__property Vcl::Actnlist::TCustomActionList* ActionList = {read=GetActionList, write=SetActionList};
	__property TRVALiveSpellGetSuggestionsEvent OnLiveSpellGetSuggestions = {read=FOnLiveSpellGetSuggestions, write=FOnLiveSpellGetSuggestions};
	__property TRVALiveSpellIgnoreAllEvent OnLiveSpellIgnoreAll = {read=FOnLiveSpellIgnoreAll, write=FOnLiveSpellIgnoreAll};
	__property TRVALiveSpellAddEvent OnLiveSpellAdd = {read=FOnLiveSpellAdd, write=FOnLiveSpellAdd};
	__property TRVALiveSpellReplaceEvent OnLiveSpellWordReplace = {read=FOnLiveSpellWordReplace, write=FOnLiveSpellWordReplace};
	__property int MaxSuggestionsCount = {read=GetMaxSuggestionsCount, write=SetMaxSuggestionsCount, default=0};
	__property TRVAControlPanel* ControlPanel = {read=GetControlPanel, write=SetControlPanel};
private:
	void *__IRVAPopupMenu;	// IRVAPopupMenu 
	
public:
	operator IRVAPopupMenu*(void) { return (IRVAPopupMenu*)&__IRVAPopupMenu; }
	
};


class DELPHICLASS TRVATBPopupMenu;
class PASCALIMPLEMENTATION TRVATBPopupMenu : public Tb2item::TTBPopupMenu
{
	typedef Tb2item::TTBPopupMenu inherited;
	
private:
	TRVAPopupMenuHelper* FPopupMenuHelper;
	TRVA2LiveSpellGetSuggestionsEvent FOnLiveSpellGetSuggestions;
	TRVA2LiveSpellAddEvent FOnLiveSpellAdd;
	TRVA2LiveSpellIgnoreAllEvent FOnLiveSpellIgnoreAll;
	TRVA2LiveSpellReplaceEvent FOnLiveSpellWordReplace;
	TRVAControlPanel* FControlPanel;
	TRVAControlPanel* __fastcall GetControlPanel(void);
	void __fastcall SetControlPanel(TRVAControlPanel* Value);
	
protected:
	System::Classes::TComponent* __fastcall GetPopupComponent(void);
	Vcl::Actnlist::TCustomActionList* __fastcall GetActionList(void);
	void __fastcall SetActionList(Vcl::Actnlist::TCustomActionList* const Value);
	int __fastcall GetMaxSuggestionsCount(void);
	void __fastcall SetMaxSuggestionsCount(int Value);
	void __fastcall ClearItems(void);
	void __fastcall AddActionItem(Vcl::Actnlist::TAction* Action, const System::UnicodeString Caption = System::UnicodeString());
	void __fastcall AddSeparatorItem(void);
	void __fastcall DeleteLastSeparatorItem(void);
	System::UnicodeString __fastcall GetItemCaption(System::TObject* Sender);
	int __fastcall GetItemIndex(System::TObject* Sender);
	void __fastcall AddClickItem(System::Classes::TNotifyEvent Click, const System::UnicodeString Caption, System::Uitypes::TFontCharset Charset, bool Enabled, bool Default);
	bool __fastcall WantLiveSpellGetSuggestions(void);
	void __fastcall LiveSpellGetSuggestions(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);
	void __fastcall LiveSpellWordReplace(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);
	bool __fastcall WantLiveSpellIgnoreAll(void);
	void __fastcall LiveSpellIgnoreAll(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	bool __fastcall WantLiveSpellAdd(void);
	void __fastcall LiveSpellAdd(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	
public:
	__fastcall virtual TRVATBPopupMenu(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVATBPopupMenu(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Popup(int X, int Y);
	
__published:
	__property Vcl::Actnlist::TCustomActionList* ActionList = {read=GetActionList, write=SetActionList};
	__property TRVA2LiveSpellGetSuggestionsEvent OnLiveSpellGetSuggestions = {read=FOnLiveSpellGetSuggestions, write=FOnLiveSpellGetSuggestions};
	__property TRVA2LiveSpellIgnoreAllEvent OnLiveSpellIgnoreAll = {read=FOnLiveSpellIgnoreAll, write=FOnLiveSpellIgnoreAll};
	__property TRVA2LiveSpellAddEvent OnLiveSpellAdd = {read=FOnLiveSpellAdd, write=FOnLiveSpellAdd};
	__property TRVA2LiveSpellReplaceEvent OnLiveSpellWordReplace = {read=FOnLiveSpellWordReplace, write=FOnLiveSpellWordReplace};
	__property int MaxSuggestionsCount = {read=GetMaxSuggestionsCount, write=SetMaxSuggestionsCount, default=0};
	__property TRVAControlPanel* ControlPanel = {read=GetControlPanel, write=SetControlPanel};
private:
	void *__IRVAPopupMenu;	// IRVAPopupMenu 
	
public:
	operator IRVAPopupMenu*(void) { return (IRVAPopupMenu*)&__IRVAPopupMenu; }
	
};


class DELPHICLASS TRVASPTBXPopupMenu;
class PASCALIMPLEMENTATION TRVASPTBXPopupMenu : public Sptbxitem::TSpTBXPopupMenu
{
	typedef Sptbxitem::TSpTBXPopupMenu inherited;
	
private:
	TRVAPopupMenuHelper* FPopupMenuHelper;
	TRVA2LiveSpellGetSuggestionsEvent FOnLiveSpellGetSuggestions;
	TRVA2LiveSpellAddEvent FOnLiveSpellAdd;
	TRVA2LiveSpellIgnoreAllEvent FOnLiveSpellIgnoreAll;
	TRVA2LiveSpellReplaceEvent FOnLiveSpellWordReplace;
	TRVAControlPanel* FControlPanel;
	TRVAControlPanel* __fastcall GetControlPanel(void);
	void __fastcall SetControlPanel(TRVAControlPanel* Value);
	
protected:
	System::Classes::TComponent* __fastcall GetPopupComponent(void);
	Vcl::Actnlist::TCustomActionList* __fastcall GetActionList(void);
	void __fastcall SetActionList(Vcl::Actnlist::TCustomActionList* const Value);
	int __fastcall GetMaxSuggestionsCount(void);
	void __fastcall SetMaxSuggestionsCount(int Value);
	void __fastcall ClearItems(void);
	void __fastcall AddActionItem(Vcl::Actnlist::TAction* Action, const System::UnicodeString Caption = System::UnicodeString());
	void __fastcall AddSeparatorItem(void);
	void __fastcall DeleteLastSeparatorItem(void);
	System::UnicodeString __fastcall GetItemCaption(System::TObject* Sender);
	int __fastcall GetItemIndex(System::TObject* Sender);
	void __fastcall AddClickItem(System::Classes::TNotifyEvent Click, const System::UnicodeString Caption, System::Uitypes::TFontCharset Charset, bool Enabled, bool Default);
	bool __fastcall WantLiveSpellGetSuggestions(void);
	void __fastcall LiveSpellGetSuggestions(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo, System::Classes::TStrings* Suggestions);
	void __fastcall LiveSpellWordReplace(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, const System::UnicodeString Correction, int StyleNo);
	bool __fastcall WantLiveSpellIgnoreAll(void);
	void __fastcall LiveSpellIgnoreAll(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	bool __fastcall WantLiveSpellAdd(void);
	void __fastcall LiveSpellAdd(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString Word, int StyleNo);
	
public:
	__fastcall virtual TRVASPTBXPopupMenu(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVASPTBXPopupMenu(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Popup(int X, int Y);
	
__published:
	__property Vcl::Actnlist::TCustomActionList* ActionList = {read=GetActionList, write=SetActionList};
	__property TRVA2LiveSpellGetSuggestionsEvent OnLiveSpellGetSuggestions = {read=FOnLiveSpellGetSuggestions, write=FOnLiveSpellGetSuggestions};
	__property TRVA2LiveSpellIgnoreAllEvent OnLiveSpellIgnoreAll = {read=FOnLiveSpellIgnoreAll, write=FOnLiveSpellIgnoreAll};
	__property TRVA2LiveSpellAddEvent OnLiveSpellAdd = {read=FOnLiveSpellAdd, write=FOnLiveSpellAdd};
	__property TRVA2LiveSpellReplaceEvent OnLiveSpellWordReplace = {read=FOnLiveSpellWordReplace, write=FOnLiveSpellWordReplace};
	__property int MaxSuggestionsCount = {read=GetMaxSuggestionsCount, write=SetMaxSuggestionsCount, default=0};
	__property TRVAControlPanel* ControlPanel = {read=GetControlPanel, write=SetControlPanel};
private:
	void *__IRVAPopupMenu;	// IRVAPopupMenu 
	
public:
	operator IRVAPopupMenu*(void) { return (IRVAPopupMenu*)&__IRVAPopupMenu; }
	
};


class PASCALIMPLEMENTATION TrvCustomAction : public Vcl::Actnlist::TAction
{
	typedef Vcl::Actnlist::TAction inherited;
	
private:
	TRVAControlPanel* FControlPanel;
	
protected:
	Rvalocalize::TRVAMessageID FMessageID;
	bool FDisabled;
	DYNAMIC void __fastcall Localize(void);
	
public:
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	TRVAControlPanel* __fastcall GetControlPanel(void);
	
__published:
	__property bool Disabled = {read=FDisabled, write=FDisabled, default=0};
	__property TRVAControlPanel* ControlPanel = {read=FControlPanel, write=FControlPanel};
public:
	/* TAction.Create */ inline __fastcall virtual TrvCustomAction(System::Classes::TComponent* AOwner) : Vcl::Actnlist::TAction(AOwner) { }
	
public:
	/* TCustomAction.Destroy */ inline __fastcall virtual ~TrvCustomAction(void) { }
	
};


class PASCALIMPLEMENTATION TrvAction : public TrvCustomAction
{
	typedef TrvCustomAction inherited;
	
protected:
	Rvscroll::TCustomRVControl* FControl;
	virtual void __fastcall SetControl(Rvscroll::TCustomRVControl* Value);
	virtual bool __fastcall RequiresMainDoc(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	bool __fastcall IterateRVData(Crvdata::TCustomRVData* RVData, Rvedit::TCustomRichViewEdit* rve, int StartNo, int EndNo, int &CustomData);
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	bool __fastcall GetEnabledDefault(void);
	
public:
	__fastcall virtual ~TrvAction(void);
	virtual bool __fastcall HandlesTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual Rvedit::TCustomRichViewEdit* __fastcall GetControl(System::TObject* Target);
	
__published:
	__property Rvscroll::TCustomRVControl* Control = {read=FControl, write=SetControl};
public:
	/* TAction.Create */ inline __fastcall virtual TrvAction(System::Classes::TComponent* AOwner) : TrvCustomAction(AOwner) { }
	
};


class DELPHICLASS TrvActionEvent;
class PASCALIMPLEMENTATION TrvActionEvent : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TRVAEvent FOnUpdate;
	TRVAEvent FOnExecute;
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property TRVAEvent OnUpdate = {read=FOnUpdate, write=FOnUpdate};
	__property TRVAEvent OnExecute = {read=FOnExecute, write=FOnExecute};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionEvent(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionEvent(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvaDocumentInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TrvaDocumentInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Rvedit::TCustomRichViewEdit* rve;
	System::UnicodeString FileName;
	TrvFileExportFilter FileFormat;
	int CustomFilterIndex;
	bool Defined;
	bool LFWarned;
	TrvFileExportFilter LFWFileFormat;
	int LFWCustomFilterIndex;
	Rvstyle::TRVCodePage CodePage;
	__fastcall TrvaDocumentInfo(void);
	DYNAMIC void __fastcall Update(void);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TrvaDocumentInfo(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TrvaDocumentInfoClass;

class DELPHICLASS TrvActionCustomNew;
class DELPHICLASS TrvActionSave;
class PASCALIMPLEMENTATION TrvActionCustomNew : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TrvActionSave* FActionSave;
	System::Classes::TNotifyEvent FOnNew;
	void __fastcall SetActionSave(TrvActionSave* const Value);
	TrvActionSave* __fastcall GetActionSave(void);
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	void __fastcall ResetProperties(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall CompleteClear(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall CompleteFormat(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall CompleteDeleteUnusedStyles(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall UpdateHFProperties(Rvedit::TCustomRichViewEdit* rve);
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	DYNAMIC void __fastcall Reset(Rvedit::TCustomRichViewEdit* rve);
	
__published:
	__property TrvActionSave* ActionSave = {read=FActionSave, write=SetActionSave};
	__property System::Classes::TNotifyEvent OnNew = {read=FOnNew, write=FOnNew};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCustomNew(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionCustomNew(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionNew;
class PASCALIMPLEMENTATION TrvActionNew : public TrvActionCustomNew
{
	typedef TrvActionCustomNew inherited;
	
private:
	Rvstyle::TRVStyleTemplateCollection* FStyleTemplates;
	void __fastcall SetStyleTemplates(Rvstyle::TRVStyleTemplateCollection* Value);
	
protected:
	void __fastcall ResetStyleTemplates(void);
	virtual void __fastcall Loaded(void);
	
public:
	__fastcall virtual TrvActionNew(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionNew(void);
	DYNAMIC void __fastcall Reset(Rvedit::TCustomRichViewEdit* rve);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property Rvstyle::TRVStyleTemplateCollection* StyleTemplates = {read=FStyleTemplates, write=SetStyleTemplates};
};


class DELPHICLASS TrvActionOpen;
class PASCALIMPLEMENTATION TrvActionOpen : public TrvActionCustomNew
{
	typedef TrvActionCustomNew inherited;
	
private:
	System::UnicodeString FDialogTitle;
	TrvFileOpenFilterSet FFilter;
	int FLastFilterIndex;
	System::UnicodeString FInitialDir;
	System::UnicodeString FCustomFilter;
	TRVOpenFileEvent FOnOpenFile;
	TrvActionNew* FActionNew;
	TrvActionNew* __fastcall GetActionNew(void);
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	__fastcall virtual TrvActionOpen(System::Classes::TComponent* AOwner);
	void __fastcall LoadFile(Rvedit::TCustomRichViewEdit* rve, const System::UnicodeString FileName, TrvFileOpenFilter FileFormat, int CustomFilterIndex = 0x0);
	__property int LastFilterIndex = {read=FLastFilterIndex, write=FLastFilterIndex, nodefault};
	
__published:
	__property System::UnicodeString DialogTitle = {read=FDialogTitle, write=FDialogTitle};
	__property System::UnicodeString InitialDir = {read=FInitialDir, write=FInitialDir};
	__property TrvFileOpenFilterSet Filter = {read=FFilter, write=FFilter, default=127};
	__property System::UnicodeString CustomFilter = {read=FCustomFilter, write=FCustomFilter};
	__property TRVOpenFileEvent OnOpenFile = {read=FOnOpenFile, write=FOnOpenFile};
	__property TrvActionNew* ActionNew = {read=FActionNew, write=FActionNew};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionOpen(void) { }
	
};


class DELPHICLASS TrvActionSaveAs;
class PASCALIMPLEMENTATION TrvActionSave : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Rvclasses::TRVList* FDocuments;
	TrvActionSaveAs* FActionSaveAs;
	TRVFileChangeEvent FOnDocumentFileChange;
	TrvFileSaveFilterSet FLostFormatWarning;
	bool FSuppressNextErrorMessage;
	TRVSaveFileEvent FOnSaving;
	void __fastcall SetActionSaveAs(TrvActionSaveAs* const Value);
	TrvActionSaveAs* __fastcall GetActionSaveAs(void);
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	int __fastcall AddDoc(Rvedit::TCustomRichViewEdit* rve, const System::UnicodeString FileName, TrvFileSaveFilter FileFormat, int CustomFilterIndex, bool Defined, bool SaveFile, bool SuppressLFWarn, bool LFWarned, Rvstyle::TRVCodePage CodePage);
	void __fastcall DeleteDoc(Rvedit::TCustomRichViewEdit* rve);
	bool __fastcall SaveDoc(int Index, bool SuppressLFWarn);
	bool __fastcall SaveDocEx(Rvedit::TCustomRichViewEdit* rve);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	void __fastcall DoDocumentFileChange(int Index);
	
public:
	__fastcall virtual TrvActionSave(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionSave(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	bool __fastcall CanCloseDoc(Rvedit::TCustomRichViewEdit* rve);
	int __fastcall FindDoc(Rvedit::TCustomRichViewEdit* rve);
	TrvaDocumentInfo* __fastcall GetDoc(Rvedit::TCustomRichViewEdit* rve);
	__property Rvclasses::TRVList* Documents = {read=FDocuments};
	__property bool SuppressNextErrorMessage = {read=FSuppressNextErrorMessage, write=FSuppressNextErrorMessage, nodefault};
	
__published:
	__property TrvActionSaveAs* ActionSaveAs = {read=FActionSaveAs, write=SetActionSaveAs};
	__property TRVFileChangeEvent OnDocumentFileChange = {read=FOnDocumentFileChange, write=FOnDocumentFileChange};
	__property TrvFileSaveFilterSet LostFormatWarning = {read=FLostFormatWarning, write=FLostFormatWarning, default=58};
	__property TRVSaveFileEvent OnSaving = {read=FOnSaving, write=FOnSaving};
};


class DELPHICLASS TrvActionCustomIO;
class PASCALIMPLEMENTATION TrvActionCustomIO : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	System::UnicodeString FDialogTitle;
	int FLastFilterIndex;
	System::UnicodeString FFileName;
	bool FAutoUpdateFileName;
	
protected:
	System::UnicodeString FInitialDir;
	__property System::UnicodeString FileName = {read=FFileName, write=FFileName};
	__property bool AutoUpdateFileName = {read=FAutoUpdateFileName, write=FAutoUpdateFileName, default=1};
	
public:
	__fastcall virtual TrvActionCustomIO(System::Classes::TComponent* AOwner);
	
__published:
	__property System::UnicodeString DialogTitle = {read=FDialogTitle, write=FDialogTitle};
	__property System::UnicodeString InitialDir = {read=FInitialDir, write=FInitialDir};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCustomIO(void) { }
	
};


class DELPHICLASS TrvActionCustomFileIO;
class PASCALIMPLEMENTATION TrvActionCustomFileIO : public TrvActionCustomIO
{
	typedef TrvActionCustomIO inherited;
	
private:
	System::UnicodeString FCustomFilter;
	
protected:
	__property System::UnicodeString CustomFilter = {read=FCustomFilter, write=FCustomFilter};
public:
	/* TrvActionCustomIO.Create */ inline __fastcall virtual TrvActionCustomFileIO(System::Classes::TComponent* AOwner) : TrvActionCustomIO(AOwner) { }
	
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCustomFileIO(void) { }
	
};


class PASCALIMPLEMENTATION TrvActionSaveAs : public TrvActionCustomFileIO
{
	typedef TrvActionCustomFileIO inherited;
	
private:
	TrvActionSave* FActionSave;
	TrvFileSaveFilterSet FFilter;
	void __fastcall SetActionSave(TrvActionSave* const Value);
	TrvActionSave* __fastcall GetActionSave(void);
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionSaveAs(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	
__published:
	__property TrvFileSaveFilterSet Filter = {read=FFilter, write=FFilter, default=63};
	__property TrvActionSave* ActionSave = {read=FActionSave, write=SetActionSave};
	__property CustomFilter = {default=0};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionSaveAs(void) { }
	
};


class DELPHICLASS TrvActionExport;
class PASCALIMPLEMENTATION TrvActionExport : public TrvActionCustomFileIO
{
	typedef TrvActionCustomFileIO inherited;
	
private:
	TrvFileExportFilterSet FFilter;
	System::UnicodeString FFileTitle;
	System::UnicodeString FImagePrefix;
	Rvstyle::TRVSaveOptions FSaveOptions;
	bool FCreateDirectoryForHTMLImages;
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	
public:
	__fastcall virtual TrvActionExport(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	bool __fastcall ExportToFile(Rvedit::TCustomRichViewEdit* Edit, const System::UnicodeString FileName, TrvFileExportFilter ExportFormat, int ConverterOrCustomIndex, Rvofficecnv::TRVOfficeConverter* rvc);
	__property FileName = {default=0};
	
__published:
	__property TrvFileExportFilterSet Filter = {read=FFilter, write=FFilter, default=1023};
	__property System::UnicodeString FileTitle = {read=FFileTitle, write=FFileTitle};
	__property System::UnicodeString ImagePrefix = {read=FImagePrefix, write=FImagePrefix};
	__property Rvstyle::TRVSaveOptions SaveOptions = {read=FSaveOptions, write=FSaveOptions, nodefault};
	__property bool CreateDirectoryForHTMLImages = {read=FCreateDirectoryForHTMLImages, write=FCreateDirectoryForHTMLImages, default=1};
	__property AutoUpdateFileName = {default=1};
	__property CustomFilter = {default=0};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionExport(void) { }
	
};


class DELPHICLASS TrvCustomPrintAction;
class PASCALIMPLEMENTATION TrvCustomPrintAction : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Ptblrv::TRVPagePrepaintEvent FOldOnPagePrepaint;
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	Ptblrv::TRVPrint* __fastcall FindRVPrint(System::Classes::TComponent* Form);
	void __fastcall InitRVPrint(System::Classes::TComponent* Form, Ptblrv::TRVPrint* &ARVPrint, bool &ACreated);
	void __fastcall DoneRVPrint(Ptblrv::TRVPrint* ARVPrint, bool ACreated);
	void __fastcall PagePrepaint(Ptblrv::TRVPrint* Sender, int PageNo, Vcl::Graphics::TCanvas* Canvas, bool Preview, const System::Types::TRect &PageRect, const System::Types::TRect &PrintAreaRect);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvCustomPrintAction(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvCustomPrintAction(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionPrintPreview;
class DELPHICLASS TrvActionPrint;
class DELPHICLASS TrvActionPageSetup;
class PASCALIMPLEMENTATION TrvActionPrintPreview : public TrvCustomPrintAction
{
	typedef TrvCustomPrintAction inherited;
	
private:
	TrvActionPrint* FActionPrint;
	bool FMaximized;
	TrvActionPageSetup* FActionPageSetup;
	TRVGetFormClassEvent FOnGetPreviewFormClass;
	Vcl::Imglist::TCustomImageList* FButtonImages;
	void __fastcall SetActionPrint(TrvActionPrint* const Value);
	TrvActionPrint* __fastcall GetActionPrint(void);
	void __fastcall SetActionPageSetup(TrvActionPageSetup* const Value);
	Vcl::Forms::TFormClass __fastcall GetPreviewFormClass(void);
	void __fastcall SetButtonImages(Vcl::Imglist::TCustomImageList* const Value);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	__property Vcl::Imglist::TCustomImageList* ButtonImages = {read=FButtonImages, write=SetButtonImages};
	
public:
	__fastcall virtual TrvActionPrintPreview(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	
__published:
	__property TrvActionPrint* ActionPrint = {read=FActionPrint, write=SetActionPrint};
	__property TrvActionPageSetup* ActionPageSetup = {read=FActionPageSetup, write=SetActionPageSetup};
	__property bool Maximized = {read=FMaximized, write=FMaximized, default=0};
	__property TRVGetFormClassEvent OnGetPreviewFormClass = {read=FOnGetPreviewFormClass, write=FOnGetPreviewFormClass};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionPrintPreview(void) { }
	
};


class PASCALIMPLEMENTATION TrvActionPrint : public TrvCustomPrintAction
{
	typedef TrvCustomPrintAction inherited;
	
private:
	System::UnicodeString FTitle;
	
public:
	__fastcall virtual TrvActionPrint(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	
__published:
	__property System::UnicodeString Title = {read=FTitle, write=FTitle};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionPrint(void) { }
	
};


class DELPHICLASS TrvActionQuickPrint;
class PASCALIMPLEMENTATION TrvActionQuickPrint : public TrvCustomPrintAction
{
	typedef TrvCustomPrintAction inherited;
	
private:
	System::UnicodeString FTitle;
	int FCopies;
	
protected:
	DYNAMIC void __fastcall Localize(void);
	
public:
	__fastcall virtual TrvActionQuickPrint(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	__property int Copies = {read=FCopies, write=FCopies, nodefault};
	
__published:
	__property System::UnicodeString Title = {read=FTitle, write=FTitle};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionQuickPrint(void) { }
	
};


class PASCALIMPLEMENTATION TrvActionPageSetup : public TrvCustomAction
{
	typedef TrvCustomAction inherited;
	
private:
	TrvPaperMarginsUnits FMarginsUnits;
	System::Classes::TNotifyEvent FOnChange;
	
public:
	__fastcall virtual TrvActionPageSetup(System::Classes::TComponent* AOwner);
	virtual bool __fastcall HandlesTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	
__published:
	__property TrvPaperMarginsUnits MarginsUnits = {read=FMarginsUnits, write=FMarginsUnits, default=0};
	__property System::Classes::TNotifyEvent OnChange = {read=FOnChange, write=FOnChange};
public:
	/* TCustomAction.Destroy */ inline __fastcall virtual ~TrvActionPageSetup(void) { }
	
};


typedef Vcl::Dialogs::TFindDialog TRVABasicFindDialog;

typedef Vcl::Dialogs::TReplaceDialog TRVABasicReplaceDialog;

typedef Vcl::Dialogs::TOpenDialog TRVAOpenDialog;

typedef Vcl::Dialogs::TSaveDialog TRVASaveDialog;

class DELPHICLASS TrvActionFind;
class DELPHICLASS TrvActionReplace;
class PASCALIMPLEMENTATION TrvActionFind : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Rvedit::TCustomRichViewEdit* FEdit;
	Vcl::Dialogs::TFindDialog* FFindDialog;
	TrvActionReplace* FActionReplace;
	bool FFindDialogVisible;
	void __fastcall FindDialogFind(System::TObject* Sender);
	void __fastcall FindDialogClose(System::TObject* Sender);
	void __fastcall SetActionReplace(TrvActionReplace* const Value);
	TrvActionReplace* __fastcall GetActionReplace(void);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionFind(System::Classes::TComponent* AOwner);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	void __fastcall CloseDialog(void);
	__fastcall virtual ~TrvActionFind(void);
	
__published:
	__property TrvActionReplace* ActionReplace = {read=FActionReplace, write=SetActionReplace};
};


class DELPHICLASS TrvActionFindNext;
class PASCALIMPLEMENTATION TrvActionFindNext : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TrvActionFind* FActionFind;
	void __fastcall SetActionFind(TrvActionFind* const Value);
	TrvActionFind* __fastcall GetActionFind(void);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionFindNext(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	
__published:
	__property TrvActionFind* ActionFind = {read=FActionFind, write=SetActionFind};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFindNext(void) { }
	
};


typedef void __fastcall (__closure *TRVAReplacingEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, const System::UnicodeString NewText);

class PASCALIMPLEMENTATION TrvActionReplace : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Rvedit::TCustomRichViewEdit* FEdit;
	Vcl::Dialogs::TReplaceDialog* FReplaceDialog;
	bool FShowReplaceAllSummary;
	TrvActionFind* FActionFind;
	TRVAReplacingEvent FOnReplacing;
	TRVAEditEvent FOnReplaceAllEnd;
	TRVAEditEvent FOnReplaceAllStart;
	bool FReplaceDialogVisible;
	void __fastcall ReplaceDialogFind(System::TObject* Sender);
	void __fastcall ReplaceDialogClose(System::TObject* Sender);
	void __fastcall ReplaceDialogReplace(System::TObject* Sender);
	void __fastcall SetActionFind(TrvActionFind* const Value);
	TrvActionFind* __fastcall GetActionFind(void);
	void __fastcall DoReplacing(Rvedit::TCustomRichViewEdit* Editor, const System::UnicodeString NewText);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionReplace(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionReplace(void);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	void __fastcall CloseDialog(void);
	
__published:
	__property bool ShowReplaceAllSummary = {read=FShowReplaceAllSummary, write=FShowReplaceAllSummary, default=1};
	__property TrvActionFind* ActionFind = {read=FActionFind, write=SetActionFind};
	__property TRVAReplacingEvent OnReplacing = {read=FOnReplacing, write=FOnReplacing};
	__property TRVAEditEvent OnReplaceAllStart = {read=FOnReplaceAllStart, write=FOnReplaceAllStart};
	__property TRVAEditEvent OnReplaceAllEnd = {read=FOnReplaceAllEnd, write=FOnReplaceAllEnd};
};


class DELPHICLASS TrvCustomEditAction;
class PASCALIMPLEMENTATION TrvCustomEditAction : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	bool __fastcall IsDifferentEditor(System::TObject* Target);
	Rvscroll::TCustomRVControl* __fastcall GetDefaultControl(void);
	
public:
	virtual bool __fastcall HandlesTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvCustomEditAction(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvCustomEditAction(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionUndo;
class PASCALIMPLEMENTATION TrvActionUndo : public TrvCustomEditAction
{
	typedef TrvCustomEditAction inherited;
	
public:
	__fastcall virtual TrvActionUndo(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionUndo(void) { }
	
};


class DELPHICLASS TrvActionRedo;
class PASCALIMPLEMENTATION TrvActionRedo : public TrvAction
{
	typedef TrvAction inherited;
	
public:
	__fastcall virtual TrvActionRedo(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionRedo(void) { }
	
};


class DELPHICLASS TrvActionCut;
class PASCALIMPLEMENTATION TrvActionCut : public TrvCustomEditAction
{
	typedef TrvCustomEditAction inherited;
	
public:
	__fastcall virtual TrvActionCut(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCut(void) { }
	
};


class DELPHICLASS TrvActionCopy;
class PASCALIMPLEMENTATION TrvActionCopy : public TrvCustomEditAction
{
	typedef TrvCustomEditAction inherited;
	
public:
	__fastcall virtual TrvActionCopy(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCopy(void) { }
	
};


class DELPHICLASS TrvActionPaste;
class PASCALIMPLEMENTATION TrvActionPaste : public TrvCustomEditAction
{
	typedef TrvCustomEditAction inherited;
	
private:
	bool __fastcall AllowPasteHTML(void);
	
public:
	__fastcall virtual TrvActionPaste(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionPaste(void) { }
	
};


class DELPHICLASS TrvActionPasteAsText;
class PASCALIMPLEMENTATION TrvActionPasteAsText : public TrvCustomEditAction
{
	typedef TrvCustomEditAction inherited;
	
public:
	__fastcall virtual TrvActionPasteAsText(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionPasteAsText(void) { }
	
};


class DELPHICLASS TrvActionPasteSpecial;
typedef void __fastcall (__closure *TRVACustomPasteEvent)(TrvActionPasteSpecial* Sender, Rvedit::TCustomRichViewEdit* Editor, System::Word Format);

typedef void __fastcall (__closure *TRVACanPasteEvent)(TrvActionPasteSpecial* Sender, bool &CanPaste);

class PASCALIMPLEMENTATION TrvActionPasteSpecial : public TrvActionPaste
{
	typedef TrvActionPaste inherited;
	
private:
	Vcl::Forms::TForm* FListForm;
	System::Classes::TNotifyEvent FOnShowing;
	TRVACustomPasteEvent FOnCustomPaste;
	bool FStoreFileName;
	bool FStoreFileNameInItemName;
	TRVACanPasteEvent FOnCanPaste;
	
public:
	__fastcall virtual TrvActionPasteSpecial(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual bool __fastcall HandlesTarget(System::TObject* Target);
	void __fastcall AddFormat(const System::UnicodeString FormatName, System::Word Format);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property System::Classes::TNotifyEvent OnShowing = {read=FOnShowing, write=FOnShowing};
	__property TRVACanPasteEvent OnCanPaste = {read=FOnCanPaste, write=FOnCanPaste};
	__property TRVACustomPasteEvent OnCustomPaste = {read=FOnCustomPaste, write=FOnCustomPaste};
	__property bool StoreFileName = {read=FStoreFileName, write=FStoreFileName, default=0};
	__property bool StoreFileNameInItemName = {read=FStoreFileNameInItemName, write=FStoreFileNameInItemName, default=0};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionPasteSpecial(void) { }
	
};


class DELPHICLASS TrvActionSelectAll;
class PASCALIMPLEMENTATION TrvActionSelectAll : public TrvAction
{
	typedef TrvAction inherited;
	
public:
	__fastcall virtual TrvActionSelectAll(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionSelectAll(void) { }
	
};


class DELPHICLASS TrvActionCharCase;
class PASCALIMPLEMENTATION TrvActionCharCase : public TrvAction
{
	typedef TrvAction inherited;
	
public:
	__fastcall virtual TrvActionCharCase(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCharCase(void) { }
	
};


class DELPHICLASS TrvActionCustomColor;
class PASCALIMPLEMENTATION TrvActionCustomColor : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Vcl::Forms::TForm* FColorPicker;
	System::Uitypes::TColor FColor;
	System::Classes::TNotifyEvent FOnHideColorPicker;
	System::Classes::TNotifyEvent FOnShowColorPicker;
	TrvaColorInterface FUserInterface;
	Rvedit::TCustomRichViewEdit* FEdit;
	System::Uitypes::TColor FDefaultColor;
	Vcl::Controls::TControl* FCallerControl;
	void __fastcall ColorPickerDestroy(System::TObject* Sender);
	void __fastcall InitColorDialog(Vcl::Dialogs::TColorDialog* &AColorDialog, bool &ACreated);
	void __fastcall DoneColorDialog(Vcl::Dialogs::TColorDialog* AColorDialog, bool ACreated);
	void __fastcall SetCallerControl(Vcl::Controls::TControl* const Value);
	void __fastcall SetColor(const System::Uitypes::TColor Value);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, int Command) = 0 ;
	virtual System::Uitypes::TColor __fastcall GetCurrentColor(Rvedit::TCustomRichViewEdit* FEdit) = 0 ;
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionCustomColor(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionCustomColor(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	DYNAMIC void __fastcall Localize(void);
	System::UnicodeString __fastcall GetColorName(void);
	
__published:
	__property Vcl::Controls::TControl* CallerControl = {read=FCallerControl, write=SetCallerControl};
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=536870911};
	__property TrvaColorInterface UserInterface = {read=FUserInterface, write=FUserInterface, default=2};
	__property System::Classes::TNotifyEvent OnShowColorPicker = {read=FOnShowColorPicker, write=FOnShowColorPicker};
	__property System::Classes::TNotifyEvent OnHideColorPicker = {read=FOnHideColorPicker, write=FOnHideColorPicker};
};


typedef void __fastcall (__closure *TRVShowFormEvent)(TrvAction* Sender, Vcl::Forms::TForm* Form);

class DELPHICLASS TrvActionStyleTemplates;
class PASCALIMPLEMENTATION TrvActionStyleTemplates : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Vcl::Imglist::TCustomImageList* FImages;
	int FTextStyleImageIndex;
	int FParaStyleImageIndex;
	int FParaTextStyleImageIndex;
	Rvstyle::TRVStyleTemplateCollection* FStandardStyleTemplates;
	void __fastcall SetStandardStyleTemplates(Rvstyle::TRVStyleTemplateCollection* Value);
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* const Value);
	
protected:
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	Vcl::Forms::TForm* __fastcall CreateForm(void);
	virtual void __fastcall Loaded(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionStyleTemplates(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionStyleTemplates(void);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property Vcl::Imglist::TCustomImageList* Images = {read=FImages, write=SetImages};
	__property int TextStyleImageIndex = {read=FTextStyleImageIndex, write=FTextStyleImageIndex, default=-1};
	__property int ParaStyleImageIndex = {read=FParaStyleImageIndex, write=FParaStyleImageIndex, default=-1};
	__property int ParaTextStyleImageIndex = {read=FParaTextStyleImageIndex, write=FParaTextStyleImageIndex, default=-1};
	__property Rvstyle::TRVStyleTemplateCollection* StandardStyleTemplates = {read=FStandardStyleTemplates, write=SetStandardStyleTemplates};
};


class DELPHICLASS TrvActionAddStyleTemplate;
class PASCALIMPLEMENTATION TrvActionAddStyleTemplate : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TrvActionStyleTemplates* FActionStyleTemplates;
	
protected:
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	TrvActionStyleTemplates* __fastcall GetActionStyleTemplates(void);
	
public:
	__fastcall virtual TrvActionAddStyleTemplate(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property TrvActionStyleTemplates* ActionStyleTemplates = {read=FActionStyleTemplates, write=FActionStyleTemplates};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionAddStyleTemplate(void) { }
	
};


class DELPHICLASS TrvActionClearFormat;
class PASCALIMPLEMENTATION TrvActionClearFormat : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionClearFormat(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearFormat(void) { }
	
};


class DELPHICLASS TrvActionClearTextFormat;
class PASCALIMPLEMENTATION TrvActionClearTextFormat : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionClearTextFormat(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearTextFormat(void) { }
	
};


class DELPHICLASS TrvActionCustomInfoWindow;
class PASCALIMPLEMENTATION TrvActionCustomInfoWindow : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TRVShowFormEvent FOnShowing;
	
protected:
	Vcl::Forms::TForm* FInfoFrm;
	DYNAMIC System::UnicodeString __fastcall GetFormCaption(void) = 0 ;
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	DYNAMIC void __fastcall UpdateInfo(bool OnlyIfVisible = true) = 0 ;
	DYNAMIC void __fastcall Localize(void);
	
__published:
	__property TRVShowFormEvent OnShowing = {read=FOnShowing, write=FOnShowing};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionCustomInfoWindow(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionCustomInfoWindow(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionStyleInspector;
class PASCALIMPLEMENTATION TrvActionStyleInspector : public TrvActionCustomInfoWindow
{
	typedef TrvActionCustomInfoWindow inherited;
	
private:
	Vcl::Imglist::TCustomImageList* FImages;
	int FParaImageIndex;
	int FFontImageIndex;
	Rvedit::TCustomRichViewEdit* FRichViewEdit;
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* const Value);
	void __fastcall DoUpdateInfo(System::TObject* Sender);
	void __fastcall DoChangeActiveEditor(System::TObject* Sender);
	void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* Value);
	
protected:
	virtual void __fastcall SetControl(Rvscroll::TCustomRVControl* Value);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC System::UnicodeString __fastcall GetFormCaption(void);
	__property Rvedit::TCustomRichViewEdit* RichViewEdit = {read=FRichViewEdit, write=SetRichViewEdit};
	
public:
	__fastcall virtual TrvActionStyleInspector(System::Classes::TComponent* AOwner);
	DYNAMIC void __fastcall UpdateInfo(bool OnlyIfVisible = true);
	
__published:
	__property Vcl::Imglist::TCustomImageList* Images = {read=FImages, write=SetImages};
	__property int FontImageIndex = {read=FFontImageIndex, write=FFontImageIndex, default=-1};
	__property int ParaImageIndex = {read=FParaImageIndex, write=FParaImageIndex, default=-1};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionStyleInspector(void) { }
	
};


class DELPHICLASS TrvActionTextStyles;
class PASCALIMPLEMENTATION TrvActionTextStyles : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	void __fastcall NewOnStyleConversion(Rvedit::TCustomRichViewEdit* Sender, int StyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	
protected:
	DYNAMIC bool __fastcall IsRecursive(void);
	void __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, int Command);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command) = 0 ;
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTextStyles(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTextStyles(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionFontCustomColor;
class PASCALIMPLEMENTATION TrvActionFontCustomColor : public TrvActionCustomColor
{
	typedef TrvActionCustomColor inherited;
	
private:
	void __fastcall NewOnStyleConversion(Rvedit::TCustomRichViewEdit* Sender, int StyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	
protected:
	virtual void __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, int Command);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command) = 0 ;
public:
	/* TrvActionCustomColor.Create */ inline __fastcall virtual TrvActionFontCustomColor(System::Classes::TComponent* AOwner) : TrvActionCustomColor(AOwner) { }
	/* TrvActionCustomColor.Destroy */ inline __fastcall virtual ~TrvActionFontCustomColor(void) { }
	
};


class DELPHICLASS TrvActionFontColor;
class PASCALIMPLEMENTATION TrvActionFontColor : public TrvActionFontCustomColor
{
	typedef TrvActionFontCustomColor inherited;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual System::Uitypes::TColor __fastcall GetCurrentColor(Rvedit::TCustomRichViewEdit* FEdit);
	
public:
	__fastcall virtual TrvActionFontColor(System::Classes::TComponent* AOwner);
	
__published:
	__property Color = {default=-16777208};
public:
	/* TrvActionCustomColor.Destroy */ inline __fastcall virtual ~TrvActionFontColor(void) { }
	
};


class DELPHICLASS TrvActionFontBackColor;
class PASCALIMPLEMENTATION TrvActionFontBackColor : public TrvActionFontCustomColor
{
	typedef TrvActionFontCustomColor inherited;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual System::Uitypes::TColor __fastcall GetCurrentColor(Rvedit::TCustomRichViewEdit* FEdit);
	
public:
	__fastcall virtual TrvActionFontBackColor(System::Classes::TComponent* AOwner);
public:
	/* TrvActionCustomColor.Destroy */ inline __fastcall virtual ~TrvActionFontBackColor(void) { }
	
};


class DELPHICLASS TrvActionFonts;
class PASCALIMPLEMENTATION TrvActionFonts : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
private:
	TRVAFont* FFont;
	bool FUserInterface;
	void __fastcall SetFont(TRVAFont* const Value);
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionFonts(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionFonts(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	__property TRVAFont* Font = {read=FFont, write=SetFont};
	
__published:
	__property bool UserInterface = {read=FUserInterface, write=FUserInterface, default=1};
};


enum DECLSPEC_DENUM TRVFontInfoMainProperty : unsigned char { rvfimFontName, rvfimSize, rvfimCharset, rvfimBold, rvfimItalic, rvfimUnderline, rvfimStrikeout, rvfimOverline, rvfimAllCaps, rvfimVShift, rvfimColor, rvfimBackColor, rvfimCharScale, rvfimCharSpacing, rvfimSubSuperScriptType, rvfimUnderlineType, rvfimUnderlineColor };

typedef System::Set<TRVFontInfoMainProperty, TRVFontInfoMainProperty::rvfimFontName, TRVFontInfoMainProperty::rvfimUnderlineColor> TRVFontInfoMainProperties;

class DELPHICLASS TrvActionFontEx;
class PASCALIMPLEMENTATION TrvActionFontEx : public TrvActionFonts
{
	typedef TrvActionFonts inherited;
	
private:
	TRVFontInfoMainProperties FValidProperties;
	int FVShift;
	int FCharScale;
	Rvstyle::TRVFontStyles FFontStyleEx;
	Rvstyle::TRVStyleLength FCharSpacing;
	Rvstyle::TRVSubSuperScriptType FSubSuperScriptType;
	System::Uitypes::TColor FUnderlineColor;
	System::Uitypes::TColor FBackColor;
	Rvstyle::TRVUnderlineType FUnderlineType;
	bool FAutoApplySymbolCharset;
	int FFontSizeDouble;
	void __fastcall AdjustCharset(void);
	void __fastcall SetFontSizeDouble(const int Value);
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionFontEx(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	void __fastcall GetFromEditor(Rvedit::TCustomRichViewEdit* rve);
	__property Rvstyle::TRVFontStyles FontStyleEx = {read=FFontStyleEx, write=FFontStyleEx, nodefault};
	__property int VShift = {read=FVShift, write=FVShift, nodefault};
	__property int CharScale = {read=FCharScale, write=FCharScale, nodefault};
	__property Rvstyle::TRVStyleLength CharSpacing = {read=FCharSpacing, write=FCharSpacing, nodefault};
	__property Rvstyle::TRVSubSuperScriptType SubSuperScriptType = {read=FSubSuperScriptType, write=FSubSuperScriptType, nodefault};
	__property Rvstyle::TRVUnderlineType UnderlineType = {read=FUnderlineType, write=FUnderlineType, nodefault};
	__property System::Uitypes::TColor UnderlineColor = {read=FUnderlineColor, write=FUnderlineColor, nodefault};
	__property System::Uitypes::TColor BackColor = {read=FBackColor, write=FBackColor, nodefault};
	__property TRVFontInfoMainProperties ValidProperties = {read=FValidProperties, write=FValidProperties, nodefault};
	__property int FontSizeDouble = {read=FFontSizeDouble, write=SetFontSizeDouble, nodefault};
	
__published:
	__property bool AutoApplySymbolCharset = {read=FAutoApplySymbolCharset, write=FAutoApplySymbolCharset, default=1};
public:
	/* TrvActionFonts.Destroy */ inline __fastcall virtual ~TrvActionFontEx(void) { }
	
};


class DELPHICLASS TrvActionFontStyle;
class PASCALIMPLEMENTATION TrvActionFontStyle : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
protected:
	System::Uitypes::TFontStyle FFontStyle;
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	virtual bool __fastcall AdditionalCheckCondition(Rvstyle::TFontInfo* TextStyle);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontStyle(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionFontStyle(System::Classes::TComponent* AOwner) : TrvActionTextStyles(AOwner) { }
	
};


class DELPHICLASS TrvActionFontBold;
class PASCALIMPLEMENTATION TrvActionFontBold : public TrvActionFontStyle
{
	typedef TrvActionFontStyle inherited;
	
public:
	__fastcall virtual TrvActionFontBold(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontBold(void) { }
	
};


class DELPHICLASS TrvActionFontItalic;
class PASCALIMPLEMENTATION TrvActionFontItalic : public TrvActionFontStyle
{
	typedef TrvActionFontStyle inherited;
	
public:
	__fastcall virtual TrvActionFontItalic(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontItalic(void) { }
	
};


class DELPHICLASS TrvActionFontUnderline;
class PASCALIMPLEMENTATION TrvActionFontUnderline : public TrvActionFontStyle
{
	typedef TrvActionFontStyle inherited;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual bool __fastcall AdditionalCheckCondition(Rvstyle::TFontInfo* TextStyle);
	
public:
	__fastcall virtual TrvActionFontUnderline(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontUnderline(void) { }
	
};


class DELPHICLASS TrvActionFontStrikeout;
class PASCALIMPLEMENTATION TrvActionFontStrikeout : public TrvActionFontStyle
{
	typedef TrvActionFontStyle inherited;
	
public:
	__fastcall virtual TrvActionFontStrikeout(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontStrikeout(void) { }
	
};


class DELPHICLASS TrvActionFontStyleEx;
class PASCALIMPLEMENTATION TrvActionFontStyleEx : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
protected:
	Rvstyle::TRVFontStyle FFontStyleEx;
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontStyleEx(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionFontStyleEx(System::Classes::TComponent* AOwner) : TrvActionTextStyles(AOwner) { }
	
};


class DELPHICLASS TrvActionFontAllCaps;
class PASCALIMPLEMENTATION TrvActionFontAllCaps : public TrvActionFontStyleEx
{
	typedef TrvActionFontStyleEx inherited;
	
public:
	__fastcall virtual TrvActionFontAllCaps(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontAllCaps(void) { }
	
};


class DELPHICLASS TrvActionFontOverline;
class PASCALIMPLEMENTATION TrvActionFontOverline : public TrvActionFontStyleEx
{
	typedef TrvActionFontStyleEx inherited;
	
public:
	__fastcall virtual TrvActionFontOverline(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontOverline(void) { }
	
};


class DELPHICLASS TrvActionSSScript;
class PASCALIMPLEMENTATION TrvActionSSScript : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
protected:
	Rvstyle::TRVSubSuperScriptType FSubSuperSctiptType;
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionSSScript(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionSSScript(System::Classes::TComponent* AOwner) : TrvActionTextStyles(AOwner) { }
	
};


class DELPHICLASS TrvActionSubscript;
class PASCALIMPLEMENTATION TrvActionSubscript : public TrvActionSSScript
{
	typedef TrvActionSSScript inherited;
	
public:
	__fastcall virtual TrvActionSubscript(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionSubscript(void) { }
	
};


class DELPHICLASS TrvActionSuperscript;
class PASCALIMPLEMENTATION TrvActionSuperscript : public TrvActionSSScript
{
	typedef TrvActionSSScript inherited;
	
public:
	__fastcall virtual TrvActionSuperscript(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionSuperscript(void) { }
	
};


class DELPHICLASS TrvActionTextBiDi;
class PASCALIMPLEMENTATION TrvActionTextBiDi : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
private:
	Rvscroll::TRVBiDiMode FBiDiMode;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTextBiDi(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTextBiDi(System::Classes::TComponent* AOwner) : TrvActionTextStyles(AOwner) { }
	
};


class DELPHICLASS TrvActionTextLTR;
class PASCALIMPLEMENTATION TrvActionTextLTR : public TrvActionTextBiDi
{
	typedef TrvActionTextBiDi inherited;
	
public:
	__fastcall virtual TrvActionTextLTR(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTextLTR(void) { }
	
};


class DELPHICLASS TrvActionTextRTL;
class PASCALIMPLEMENTATION TrvActionTextRTL : public TrvActionTextBiDi
{
	typedef TrvActionTextBiDi inherited;
	
public:
	__fastcall virtual TrvActionTextRTL(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTextRTL(void) { }
	
};


class DELPHICLASS TrvActionFontShrinkGrow;
class PASCALIMPLEMENTATION TrvActionFontShrinkGrow : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
private:
	int FPercent;
	
public:
	__fastcall virtual TrvActionFontShrinkGrow(System::Classes::TComponent* AOwner);
	
__published:
	__property int Percent = {read=FPercent, write=FPercent, default=10};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontShrinkGrow(void) { }
	
};


class DELPHICLASS TrvActionFontShrink;
class PASCALIMPLEMENTATION TrvActionFontShrink : public TrvActionFontShrinkGrow
{
	typedef TrvActionFontShrinkGrow inherited;
	
private:
	int FMinSize;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionFontShrink(System::Classes::TComponent* AOwner);
	
__published:
	__property int MinSize = {read=FMinSize, write=FMinSize, default=1};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontShrink(void) { }
	
};


class DELPHICLASS TrvActionFontGrow;
class PASCALIMPLEMENTATION TrvActionFontGrow : public TrvActionFontShrinkGrow
{
	typedef TrvActionFontShrinkGrow inherited;
	
private:
	int FMaxSize;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionFontGrow(System::Classes::TComponent* AOwner);
	
__published:
	__property int MaxSize = {read=FMaxSize, write=FMaxSize, default=100};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontGrow(void) { }
	
};


class DELPHICLASS TrvActionFontShrinkOnePoint;
class PASCALIMPLEMENTATION TrvActionFontShrinkOnePoint : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
private:
	int FMinSize;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionFontShrinkOnePoint(System::Classes::TComponent* AOwner);
	
__published:
	__property int MinSize = {read=FMinSize, write=FMinSize, default=1};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontShrinkOnePoint(void) { }
	
};


class DELPHICLASS TrvActionFontGrowOnePoint;
class PASCALIMPLEMENTATION TrvActionFontGrowOnePoint : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
private:
	int FMaxSize;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionFontGrowOnePoint(System::Classes::TComponent* AOwner);
	
__published:
	__property int MaxSize = {read=FMaxSize, write=FMaxSize, default=100};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFontGrowOnePoint(void) { }
	
};


class DELPHICLASS TrvActionInsertFile;
class PASCALIMPLEMENTATION TrvActionInsertFile : public TrvActionCustomFileIO
{
	typedef TrvActionCustomFileIO inherited;
	
private:
	TrvFileImportFilterSet FFilter;
	Rvedit::TCustomRichViewEdit* FCurrentEditor;
	void __fastcall DoOnConvert(System::TObject* Sender, int Percent);
	
public:
	__fastcall virtual TrvActionInsertFile(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	bool __fastcall InsertFile(Rvedit::TCustomRichViewEdit* rve, const System::UnicodeString FileName, TrvFileImportFilter FileFormat, int ConverterOrCustomIndex = 0x0, Rvofficecnv::TRVOfficeConverter* rvc = (Rvofficecnv::TRVOfficeConverter*)(0x0), bool Silent = false);
	__property FileName = {default=0};
	
__published:
	__property TrvFileImportFilterSet Filter = {read=FFilter, write=FFilter, default=511};
	__property AutoUpdateFileName = {default=1};
	__property CustomFilter = {default=0};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertFile(void) { }
	
};


typedef void __fastcall (__closure *TRVAInsertTextEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, System::UnicodeString &Text);

class DELPHICLASS TrvActionInsertText;
class PASCALIMPLEMENTATION TrvActionInsertText : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TRVAInsertTextEvent FOnInsertText;
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property TRVAInsertTextEvent OnInsertText = {read=FOnInsertText, write=FOnInsertText};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertText(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionInsertText(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionInsertPicture;
class PASCALIMPLEMENTATION TrvActionInsertPicture : public TrvActionCustomIO
{
	typedef TrvActionCustomIO inherited;
	
private:
	System::UnicodeString FDefaultExt;
	System::UnicodeString FFilter;
	Rvstyle::TRVVAlign FVAlign;
	bool FStoreFileName;
	bool FStoreFileNameInItemName;
	Rvstyle::TRVStyleLength FMaxImageSize;
	Rvstyle::TRVStyleLength FSpacing;
	Rvstyle::TRVStyleLength FOuterHSpacing;
	Rvstyle::TRVStyleLength FOuterVSpacing;
	Rvstyle::TRVStyleLength FBorderWidth;
	System::Uitypes::TColor FBorderColor;
	System::Uitypes::TColor FBackgroundColor;
	bool __fastcall IsImageTooLarge(Vcl::Graphics::TGraphic* gr);
	void __fastcall GetProperImageSize(Vcl::Graphics::TGraphic* gr, Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVStyleLength &Width, Rvstyle::TRVStyleLength &Height);
	
public:
	__fastcall virtual TrvActionInsertPicture(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	__property FileName = {default=0};
	
__published:
	__property System::UnicodeString Filter = {read=FFilter, write=FFilter};
	__property Rvstyle::TRVVAlign VAlign = {read=FVAlign, write=FVAlign, nodefault};
	__property System::UnicodeString DefaultExt = {read=FDefaultExt, write=FDefaultExt};
	__property bool StoreFileName = {read=FStoreFileName, write=FStoreFileName, default=0};
	__property bool StoreFileNameInItemName = {read=FStoreFileNameInItemName, write=FStoreFileNameInItemName, default=0};
	__property Rvstyle::TRVStyleLength MaxImageSize = {read=FMaxImageSize, write=FMaxImageSize, default=0};
	__property Rvstyle::TRVStyleLength Spacing = {read=FSpacing, write=FSpacing, default=0};
	__property Rvstyle::TRVStyleLength BorderWidth = {read=FBorderWidth, write=FBorderWidth, default=0};
	__property Rvstyle::TRVStyleLength OuterHSpacing = {read=FOuterHSpacing, write=FOuterHSpacing, default=0};
	__property Rvstyle::TRVStyleLength OuterVSpacing = {read=FOuterVSpacing, write=FOuterVSpacing, default=0};
	__property System::Uitypes::TColor BorderColor = {read=FBorderColor, write=FBorderColor, default=0};
	__property System::Uitypes::TColor BackgroundColor = {read=FBackgroundColor, write=FBackgroundColor, default=536870911};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertPicture(void) { }
	
};


class DELPHICLASS TrvActionInsertHLine;
class PASCALIMPLEMENTATION TrvActionInsertHLine : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Rvstyle::TRVStyleLength FWidth;
	System::Uitypes::TColor FColor;
	Rvstyle::TRVBreakStyle FStyle;
	
public:
	__fastcall virtual TrvActionInsertHLine(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=-16777208};
	__property Rvstyle::TRVBreakStyle Style = {read=FStyle, write=FStyle, default=0};
	__property Rvstyle::TRVStyleLength Width = {read=FWidth, write=FWidth, default=1};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertHLine(void) { }
	
};


enum DECLSPEC_DENUM TRVInsertSymbolType : unsigned char { rvisBoth, rvisUnicode, rvisSingleByte };

class DELPHICLASS TrvActionInsertSymbol;
class PASCALIMPLEMENTATION TrvActionInsertSymbol : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	System::UnicodeString FFontName;
	System::Uitypes::TFontCharset FCharset;
	System::Word FCharCode;
	bool FUnicode;
	bool FInitialized;
	TRVInsertSymbolType FSymbolType;
	bool FAlwaysInsertUnicode;
	bool FDisplayUnicodeBlocks;
	bool FUseCurrentFont;
	
public:
	__fastcall virtual TrvActionInsertSymbol(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	__property System::Uitypes::TFontCharset Charset = {read=FCharset, write=FCharset, nodefault};
	__property System::Word CharCode = {read=FCharCode, write=FCharCode, nodefault};
	__property System::UnicodeString FontName = {read=FFontName, write=FFontName};
	__property bool Initialized = {read=FInitialized, write=FInitialized, nodefault};
	
__published:
	__property TRVInsertSymbolType SymbolType = {read=FSymbolType, write=FSymbolType, default=0};
	__property bool AlwaysInsertUnicode = {read=FAlwaysInsertUnicode, write=FAlwaysInsertUnicode, default=1};
	__property bool DisplayUnicodeBlocks = {read=FDisplayUnicodeBlocks, write=FDisplayUnicodeBlocks, default=1};
	__property bool UseCurrentFont = {read=FUseCurrentFont, write=FUseCurrentFont, default=0};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertSymbol(void) { }
	
};


enum DECLSPEC_DENUM TRVHyperlinkProperty : unsigned char { rvhlBold, rvhlItalic, rvhlUnderline, rvhlUnderlineColor, rvhlStrikeout, rvhlColor, rvhlBackColor, rvhlHoverColor, rvhlHoverBackColor, rvhlHoverUnderlineColor, rvhlHoverUnderline, rvhlCursor, rvhlJump };

typedef System::Set<TRVHyperlinkProperty, TRVHyperlinkProperty::rvhlBold, TRVHyperlinkProperty::rvhlJump> TRVHyperlinkProperties;

typedef void __fastcall (__closure *TRVHyperlinkFormEvent)(System::TObject* Sender, bool InsertNew, System::UnicodeString &Text, System::UnicodeString &Target, bool &Proceed);

typedef void __fastcall (__closure *TRVApplyHyperlinkToItemEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, Crvdata::TCustomRVData* RVData, int ItemNo, const System::UnicodeString Target);

typedef void __fastcall (__closure *TRVGetHyperlinkTargetFromItem)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, Crvdata::TCustomRVData* RVData, int ItemNo, System::UnicodeString &Target);

typedef void __fastcall (__closure *TRVCanApplyEvent)(System::TObject* Sender, Rvedit::TCustomRichViewEdit* Editor, Rvitem::TCustomRVItemInfo* Item, bool &CanApply);

class DELPHICLASS TrvActionInsertHyperlink;
class PASCALIMPLEMENTATION TrvActionInsertHyperlink : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
private:
	System::Uitypes::TColor FColor;
	System::Uitypes::TColor FHoverColor;
	System::Uitypes::TColor FBackColor;
	System::Uitypes::TColor FHoverBackColor;
	System::Uitypes::TColor FUnderlineColor;
	System::Uitypes::TColor FHoverUnderlineColor;
	Rvstyle::TRVHoverEffects FHoverEffects;
	System::Uitypes::TCursor FCursor;
	System::Uitypes::TFontStyles FStyle;
	TRVHyperlinkProperties FValidProperties;
	bool FSetToPictures;
	Rvstyle::TFontInfo* FDefaultFontInfo;
	System::UnicodeString FSpaceFiller;
	System::UnicodeString FStyleTemplateName;
	bool FAutoAddHyperlinkStyleTemplate;
	TrvActionStyleTemplates* FActionStyleTemplates;
	TRVHyperlinkFormEvent FOnHyperlinkForm;
	TRVApplyHyperlinkToItemEvent FOnApplyHyperlinkToItem;
	TRVGetHyperlinkTargetFromItem FOnGetHyperlinkTargetFromItem;
	bool __fastcall DoShowForm(Rvedit::TCustomRichViewEdit* Edit, bool InsertNew, System::UnicodeString &Text, System::UnicodeString &Target, Rvstyle::TRVStyleTemplateId DefStyleTemplateId);
	bool __fastcall StoreStyleTemplateName(void);
	TrvActionStyleTemplates* __fastcall GetActionStyleTemplates(void);
	Rvstyle::TRVStyleTemplate* __fastcall GetStyleTemplate(Rvedit::TCustomRichViewEdit* Edit);
	
protected:
	DYNAMIC bool __fastcall IsRecursive(void);
	bool __fastcall HasItems(Rvedit::TCustomRichViewEdit* rve, System::UnicodeString &Target, Rvstyle::TRVStyleTemplateId &StyleTemplateId);
	
public:
	__fastcall virtual TrvActionInsertHyperlink(System::Classes::TComponent* AOwner);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	void __fastcall GoToLink(Rvedit::TCustomRichViewEdit* rve, int id)/* overload */;
	void __fastcall GoToLink(Rvedit::TCustomRichViewEdit* rve, const System::UnicodeString Target)/* overload */;
	System::UnicodeString __fastcall EncodeTarget(const System::UnicodeString Target);
	void __fastcall SetTags(Rvedit::TCustomRichViewEdit* rve, const System::UnicodeString Target);
	int __fastcall GetHyperlinkStyleNo(Rvedit::TCustomRichViewEdit* rve, int StyleNo = 0xffffffff);
	int __fastcall GetNormalStyleNo(Rvedit::TCustomRichViewEdit* rve, int StyleNo = 0xffffffff);
	void __fastcall TerminateHyperlink(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall DetectURL(Rvedit::TCustomRichViewEdit* rve);
	
__published:
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=16711680};
	__property System::Uitypes::TColor HoverColor = {read=FHoverColor, write=FHoverColor, default=16711680};
	__property System::Uitypes::TColor BackColor = {read=FBackColor, write=FBackColor, default=536870911};
	__property System::Uitypes::TColor HoverBackColor = {read=FHoverBackColor, write=FHoverBackColor, default=536870911};
	__property System::Uitypes::TColor UnderlineColor = {read=FUnderlineColor, write=FUnderlineColor, default=536870911};
	__property System::Uitypes::TColor HoverUnderlineColor = {read=FHoverUnderlineColor, write=FHoverUnderlineColor, default=536870911};
	__property Rvstyle::TRVHoverEffects HoverEffects = {read=FHoverEffects, write=FHoverEffects, default=0};
	__property System::Uitypes::TFontStyles Style = {read=FStyle, write=FStyle, default=4};
	__property System::Uitypes::TCursor Cursor = {read=FCursor, write=FCursor, default=101};
	__property TRVHyperlinkProperties ValidProperties = {read=FValidProperties, write=FValidProperties, default=8172};
	__property bool SetToPictures = {read=FSetToPictures, write=FSetToPictures, default=1};
	__property System::UnicodeString SpaceFiller = {read=FSpaceFiller, write=FSpaceFiller};
	__property System::UnicodeString StyleTemplateName = {read=FStyleTemplateName, write=FStyleTemplateName, stored=StoreStyleTemplateName};
	__property bool AutoAddHyperlinkStyleTemplate = {read=FAutoAddHyperlinkStyleTemplate, write=FAutoAddHyperlinkStyleTemplate, default=1};
	__property TrvActionStyleTemplates* ActionStyleTemplates = {read=FActionStyleTemplates, write=FActionStyleTemplates};
	__property TRVHyperlinkFormEvent OnHyperlinkForm = {read=FOnHyperlinkForm, write=FOnHyperlinkForm};
	__property TRVApplyHyperlinkToItemEvent OnApplyHyperlinkToItem = {read=FOnApplyHyperlinkToItem, write=FOnApplyHyperlinkToItem};
	__property TRVGetHyperlinkTargetFromItem OnGetHyperlinkTargetFromItem = {read=FOnGetHyperlinkTargetFromItem, write=FOnGetHyperlinkTargetFromItem};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertHyperlink(void) { }
	
};


class DELPHICLASS TrvActionInsertNumSequence;
class PASCALIMPLEMENTATION TrvActionInsertNumSequence : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	System::UnicodeString FSeqName;
	Rvstyle::TRVSeqType FNumberType;
	bool FUseDefaults;
	
public:
	__fastcall virtual TrvActionInsertNumSequence(System::Classes::TComponent* AOwner);
	__property System::UnicodeString SeqName = {read=FSeqName, write=FSeqName};
	__property Rvstyle::TRVSeqType NumberType = {read=FNumberType, write=FNumberType, nodefault};
	__property bool UseDefaults = {read=FUseDefaults, write=FUseDefaults, nodefault};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertNumSequence(void) { }
	
};


class DELPHICLASS TrvActionInsertNumber;
class PASCALIMPLEMENTATION TrvActionInsertNumber : public TrvActionInsertNumSequence
{
	typedef TrvActionInsertNumSequence inherited;
	
public:
	__fastcall virtual TrvActionInsertNumber(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertNumber(void) { }
	
};


class DELPHICLASS TrvActionInsertCaption;
class PASCALIMPLEMENTATION TrvActionInsertCaption : public TrvActionInsertNumSequence
{
	typedef TrvActionInsertNumSequence inherited;
	
private:
	TRVCanApplyEvent FOnCanApply;
	bool FInsertAbove;
	bool FExcludeLabel;
	
protected:
	bool __fastcall CanApply(Rvedit::TCustomRichViewEdit* Edit);
	
public:
	__fastcall virtual TrvActionInsertCaption(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	__property bool InsertAbove = {read=FInsertAbove, write=FInsertAbove, nodefault};
	__property bool ExcludeLabel = {read=FExcludeLabel, write=FExcludeLabel, nodefault};
	
__published:
	__property TRVCanApplyEvent OnCanApply = {read=FOnCanApply, write=FOnCanApply};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertCaption(void) { }
	
};


typedef System::TMetaClass* TRVNoteItemInfoClass;

enum DECLSPEC_DENUM TRVNoteKind : unsigned char { rvnkNone, rvnkFootnote, rvnkEndnote, rvnkSidenote, rvnkTextBox };

class DELPHICLASS TRichViewNoteEdit;
class PASCALIMPLEMENTATION TRichViewNoteEdit : public Rvedit::TRichViewEdit
{
	typedef Rvedit::TRichViewEdit inherited;
	
public:
	TRVNoteKind NoteKind;
	Rvedit::TCustomRichViewEdit* FMainEditor;
	DYNAMIC Rvscroll::TCustomRVControl* __fastcall SRVGetActiveEditor(bool MainDoc);
	DYNAMIC bool __fastcall HasOwnerItemInMainDoc(void);
public:
	/* TCustomRichViewEdit.Create */ inline __fastcall virtual TRichViewNoteEdit(System::Classes::TComponent* AOwner) : Rvedit::TRichViewEdit(AOwner) { }
	/* TCustomRichViewEdit.Destroy */ inline __fastcall virtual ~TRichViewNoteEdit(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRichViewNoteEdit(HWND ParentWindow) : Rvedit::TRichViewEdit(ParentWindow) { }
	
};


class DELPHICLASS TrvCustomEditorAction;
class PASCALIMPLEMENTATION TrvCustomEditorAction : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Vcl::Forms::TForm* FForm;
	Rvedit::TRichViewEdit* FSubDocEditor;
	Rvedit::TCustomRichViewEdit* FRichViewEdit;
	TRVShowFormEvent FOnShowing;
	TRVShowFormEvent FOnFormCreate;
	System::Classes::TNotifyEvent FOnHide;
	void __fastcall DoChangeActiveEditor(System::TObject* Sender);
	void __fastcall FormClose(System::TObject* Sender, System::Uitypes::TCloseAction &Action);
	void __fastcall FormHide(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall SubDocEditorChange(System::TObject* Sender);
	
protected:
	void __fastcall CreateForm(void);
	void __fastcall CreateContent(Vcl::Controls::TWinControl* Parent, Rvedit::TCustomRichViewEdit* MainEditor);
	DYNAMIC System::UnicodeString __fastcall GetFormCaption(void) = 0 ;
	DYNAMIC void __fastcall UpdateSubDocEditorContent(bool OnlyIfVisible = true) = 0 ;
	DYNAMIC Rvedit::TRichViewEdit* __fastcall CreateSubDocEditor(Vcl::Controls::TControl* Owner);
	virtual void __fastcall SetControl(Rvscroll::TCustomRVControl* Value);
	virtual void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* Value);
	__property Rvedit::TCustomRichViewEdit* RichViewEdit = {read=FRichViewEdit, write=SetRichViewEdit};
	
public:
	__fastcall virtual ~TrvCustomEditorAction(void);
	DYNAMIC void __fastcall Localize(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	__property Vcl::Forms::TForm* Form = {read=FForm};
	__property Rvedit::TRichViewEdit* SubDocEditor = {read=FSubDocEditor};
	
__published:
	__property TRVShowFormEvent OnShowing = {read=FOnShowing, write=FOnShowing};
	__property TRVShowFormEvent OnFormCreate = {read=FOnFormCreate, write=FOnFormCreate};
	__property System::Classes::TNotifyEvent OnHide = {read=FOnHide, write=FOnHide};
public:
	/* TAction.Create */ inline __fastcall virtual TrvCustomEditorAction(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionEditNote;
class PASCALIMPLEMENTATION TrvActionEditNote : public TrvCustomEditorAction
{
	typedef TrvCustomEditorAction inherited;
	
private:
	Rvnote::TCustomRVNoteItemInfo* FNote;
	bool FJumpToNextNote;
	bool FSaving;
	void __fastcall DoCaretMove(System::TObject* Sender);
	void __fastcall DoClear(System::TObject* Sender);
	void __fastcall DoSave(System::TObject* Sender);
	void __fastcall EditNote(void);
	void __fastcall SaveToNote(void);
	TRVNoteKind __fastcall GetNoteKind(void);
	
protected:
	DYNAMIC System::UnicodeString __fastcall GetFormCaption(void);
	DYNAMIC void __fastcall UpdateSubDocEditorContent(bool OnlyIfVisible = true);
	DYNAMIC Rvedit::TRichViewEdit* __fastcall CreateSubDocEditor(Vcl::Controls::TControl* Owner);
	virtual void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* Value);
	
public:
	__fastcall virtual TrvActionEditNote(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	DYNAMIC void __fastcall Localize(void);
	__property bool JumpToNextNote = {read=FJumpToNextNote, write=FJumpToNextNote, default=1};
public:
	/* TrvCustomEditorAction.Destroy */ inline __fastcall virtual ~TrvActionEditNote(void) { }
	
};


class DELPHICLASS TrvActionNoteHelper;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TrvActionNoteHelper : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Vcl::Graphics::TFont* FFont;
	void __fastcall SetFont(Vcl::Graphics::TFont* const Value);
	
public:
	Rvstyle::TRVStyleTemplateName RefStyleTemplateName;
	Rvstyle::TRVStyleTemplateName DocStyleTemplateName;
	__fastcall TrvActionNoteHelper(void);
	__fastcall virtual ~TrvActionNoteHelper(void);
	void __fastcall GetNoteTextStyles(Rvedit::TCustomRichViewEdit* Edit, int &StyleNo, int &ParaNo);
	int __fastcall GetNoteRefStyleNo(Rvedit::TCustomRichViewEdit* Edit, int StyleNo);
	__property Vcl::Graphics::TFont* Font = {read=FFont, write=SetFont};
};

#pragma pack(pop)

class DELPHICLASS TrvActionInsertNote;
class PASCALIMPLEMENTATION TrvActionInsertNote : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TrvActionNoteHelper* FHelper;
	TrvActionEditNote* FActionEditNote;
	void __fastcall SetFont(TRVAFont* const Value);
	TRVAFont* __fastcall GetFont(void);
	TrvActionEditNote* __fastcall GetActionEditNote(void);
	
protected:
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	DYNAMIC Rvnote::TCustomRVNoteItemInfo* __fastcall CreateNote(Rvedit::TCustomRichViewEdit* Edit);
	DYNAMIC TRVNoteKind __fastcall GetNoteKind(void) = 0 ;
	DYNAMIC TRVNoteItemInfoClass __fastcall GetNoteClass(void) = 0 ;
	
public:
	__fastcall virtual TrvActionInsertNote(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionInsertNote(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property TRVAFont* Font = {read=GetFont, write=SetFont};
	__property TrvActionEditNote* ActionEditNote = {read=FActionEditNote, write=FActionEditNote};
};


class DELPHICLASS TrvActionInsertFootnote;
class PASCALIMPLEMENTATION TrvActionInsertFootnote : public TrvActionInsertNote
{
	typedef TrvActionInsertNote inherited;
	
protected:
	DYNAMIC TRVNoteKind __fastcall GetNoteKind(void);
	DYNAMIC TRVNoteItemInfoClass __fastcall GetNoteClass(void);
	
public:
	__fastcall virtual TrvActionInsertFootnote(System::Classes::TComponent* AOwner);
public:
	/* TrvActionInsertNote.Destroy */ inline __fastcall virtual ~TrvActionInsertFootnote(void) { }
	
};


class DELPHICLASS TrvActionInsertEndnote;
class PASCALIMPLEMENTATION TrvActionInsertEndnote : public TrvActionInsertNote
{
	typedef TrvActionInsertNote inherited;
	
protected:
	DYNAMIC TRVNoteKind __fastcall GetNoteKind(void);
	DYNAMIC TRVNoteItemInfoClass __fastcall GetNoteClass(void);
	
public:
	__fastcall virtual TrvActionInsertEndnote(System::Classes::TComponent* AOwner);
public:
	/* TrvActionInsertNote.Destroy */ inline __fastcall virtual ~TrvActionInsertEndnote(void) { }
	
};


class DELPHICLASS TRVABoxProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVABoxProperties : public Rvfloatingbox::TRVBoxProperties
{
	typedef Rvfloatingbox::TRVBoxProperties inherited;
	
private:
	TrvCustomAction* FOwner;
	
public:
	__fastcall TRVABoxProperties(TrvCustomAction* AOwner);
	__property TrvCustomAction* Owner = {read=FOwner};
	
__published:
	__property Border;
	__property Background;
	__property Width = {default=100};
	__property Height = {default=100};
	__property WidthType = {default=0};
	__property HeightType = {default=5};
	__property VAlign = {default=0};
public:
	/* TRVBoxProperties.Destroy */ inline __fastcall virtual ~TRVABoxProperties(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVABoxPosition;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVABoxPosition : public Rvfloatingpos::TRVBoxPosition
{
	typedef Rvfloatingpos::TRVBoxPosition inherited;
	
private:
	TrvCustomAction* FOwner;
	
public:
	__fastcall TRVABoxPosition(TrvCustomAction* AOwner);
	__property TrvCustomAction* Owner = {read=FOwner};
	
__published:
	__property HorizontalAnchor = {default=0};
	__property VerticalAnchor = {default=0};
	__property HorizontalPositionKind = {default=1};
	__property VerticalPositionKind = {default=1};
	__property HorizontalOffset = {default=0};
	__property VerticalOffset = {default=0};
	__property HorizontalAlignment = {default=0};
	__property VerticalAlignment = {default=0};
	__property PositionInText = {default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVABoxPosition(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVASidenoteBoxPosition;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVASidenoteBoxPosition : public TRVABoxPosition
{
	typedef TRVABoxPosition inherited;
	
public:
	__fastcall TRVASidenoteBoxPosition(TrvCustomAction* AOwner);
	
__published:
	__property HorizontalAnchor = {default=2};
	__property HorizontalPositionKind = {default=0};
	__property HorizontalAlignment = {default=2};
	__property VerticalAnchor = {default=1};
	__property VerticalPositionKind = {default=1};
	__property VerticalOffset = {default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVASidenoteBoxPosition(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVASidenoteBoxProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVASidenoteBoxProperties : public TRVABoxProperties
{
	typedef TRVABoxProperties inherited;
	
public:
	__fastcall TRVASidenoteBoxProperties(TrvCustomAction* AOwner);
	
__published:
	__property WidthType = {default=1};
	__property Width = {default=20000};
public:
	/* TRVBoxProperties.Destroy */ inline __fastcall virtual ~TRVASidenoteBoxProperties(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVASimpleTextBoxProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVASimpleTextBoxProperties : public TRVABoxProperties
{
	typedef TRVABoxProperties inherited;
	
public:
	__fastcall TRVASimpleTextBoxProperties(TrvCustomAction* AOwner);
	
__published:
	__property WidthType = {default=1};
	__property Width = {default=20000};
public:
	/* TRVBoxProperties.Destroy */ inline __fastcall virtual ~TRVASimpleTextBoxProperties(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVASimpleTextBoxPosition;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVASimpleTextBoxPosition : public TRVABoxPosition
{
	typedef TRVABoxPosition inherited;
	
public:
	__fastcall TRVASimpleTextBoxPosition(TrvCustomAction* AOwner);
	
__published:
	__property HorizontalAnchor = {default=2};
	__property HorizontalPositionKind = {default=0};
	__property HorizontalAlignment = {default=1};
	__property VerticalAnchor = {default=0};
	__property VerticalPositionKind = {default=1};
	__property VerticalOffset = {default=20};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVASimpleTextBoxPosition(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TrvActionCustomInsertSidenote;
class PASCALIMPLEMENTATION TrvActionCustomInsertSidenote : public TrvActionInsertNote
{
	typedef TrvActionInsertNote inherited;
	
private:
	TRVABoxProperties* FBoxProperties;
	TRVABoxPosition* FBoxPosition;
	void __fastcall SetBoxPosition(TRVABoxPosition* const Value);
	void __fastcall SetBoxProperties(TRVABoxProperties* const Value);
	System::UnicodeString __fastcall GetStyleTemplateName(void);
	void __fastcall SetStyleTemplateName(const System::UnicodeString Value);
	
protected:
	virtual bool __fastcall StoreStyleTemplateName(void);
	DYNAMIC Rvnote::TCustomRVNoteItemInfo* __fastcall CreateNote(Rvedit::TCustomRichViewEdit* Edit);
	virtual TRVABoxProperties* __fastcall CreateBoxProperties(void);
	virtual TRVABoxPosition* __fastcall CreateBoxPosition(void);
	
public:
	__fastcall virtual TrvActionCustomInsertSidenote(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionCustomInsertSidenote(void);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property TRVABoxProperties* BoxProperties = {read=FBoxProperties, write=SetBoxProperties};
	__property TRVABoxPosition* BoxPosition = {read=FBoxPosition, write=SetBoxPosition};
	__property System::UnicodeString StyleTemplateName = {read=GetStyleTemplateName, write=SetStyleTemplateName, stored=StoreStyleTemplateName};
};


class DELPHICLASS TrvActionInsertSidenote;
class PASCALIMPLEMENTATION TrvActionInsertSidenote : public TrvActionCustomInsertSidenote
{
	typedef TrvActionCustomInsertSidenote inherited;
	
private:
	System::UnicodeString __fastcall GetRefStyleTemplateName(void);
	void __fastcall SetRefStyleTemplateName(const System::UnicodeString Value);
	bool __fastcall StoreRefStyleTemplateName(void);
	
protected:
	virtual TRVABoxProperties* __fastcall CreateBoxProperties(void);
	virtual TRVABoxPosition* __fastcall CreateBoxPosition(void);
	DYNAMIC TRVNoteKind __fastcall GetNoteKind(void);
	DYNAMIC TRVNoteItemInfoClass __fastcall GetNoteClass(void);
	
public:
	__fastcall virtual TrvActionInsertSidenote(System::Classes::TComponent* AOwner);
	
__published:
	__property System::UnicodeString RefStyleTemplateName = {read=GetRefStyleTemplateName, write=SetRefStyleTemplateName, stored=StoreRefStyleTemplateName};
public:
	/* TrvActionCustomInsertSidenote.Destroy */ inline __fastcall virtual ~TrvActionInsertSidenote(void) { }
	
};


class DELPHICLASS TrvActionInsertTextBox;
class PASCALIMPLEMENTATION TrvActionInsertTextBox : public TrvActionCustomInsertSidenote
{
	typedef TrvActionCustomInsertSidenote inherited;
	
protected:
	virtual TRVABoxProperties* __fastcall CreateBoxProperties(void);
	virtual TRVABoxPosition* __fastcall CreateBoxPosition(void);
	DYNAMIC TRVNoteKind __fastcall GetNoteKind(void);
	DYNAMIC TRVNoteItemInfoClass __fastcall GetNoteClass(void);
	virtual bool __fastcall StoreStyleTemplateName(void);
	
public:
	__fastcall virtual TrvActionInsertTextBox(System::Classes::TComponent* AOwner);
public:
	/* TrvActionCustomInsertSidenote.Destroy */ inline __fastcall virtual ~TrvActionInsertTextBox(void) { }
	
};


class DELPHICLASS TrvActionHide;
class PASCALIMPLEMENTATION TrvActionHide : public TrvActionTextStyles
{
	typedef TrvActionTextStyles inherited;
	
protected:
	DYNAMIC bool __fastcall IsRecursive(void);
	
public:
	__fastcall virtual TrvActionHide(System::Classes::TComponent* AOwner);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TFontInfo* FontInfo, int StyleNo, int Command);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionHide(void) { }
	
};


class DELPHICLASS TrvActionRemoveHyperlinks;
class PASCALIMPLEMENTATION TrvActionRemoveHyperlinks : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TrvActionInsertHyperlink* FActionInsertHyperlink;
	TrvActionInsertHyperlink* __fastcall GetActionInsertHyperlink(void);
	
public:
	__fastcall virtual TrvActionRemoveHyperlinks(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	
__published:
	__property TrvActionInsertHyperlink* ActionInsertHyperlink = {read=FActionInsertHyperlink, write=FActionInsertHyperlink};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionRemoveHyperlinks(void) { }
	
};


class DELPHICLASS TrvActionParaStyles;
class PASCALIMPLEMENTATION TrvActionParaStyles : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	void __fastcall NewOnParaStyleConversion(Rvedit::TCustomRichViewEdit* Sender, int StyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	
protected:
	void __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, int Command);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command) = 0 ;
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionParaStyles(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionParaStyles(void) { }
	
};


enum DECLSPEC_DENUM TRVParaInfoMainProperty : unsigned char { rvpimFirstIndent, rvpimLeftIndent, rvpimRightIndent, rvpimSpaceBefore, rvpimSpaceAfter, rvpimAlignment, rvpimOutlineLevel, rvpimLineSpacing, rvpimKeepLinesTogether, rvpimKeepWithNext, rvpimTabs };

typedef System::Set<TRVParaInfoMainProperty, TRVParaInfoMainProperty::rvpimFirstIndent, TRVParaInfoMainProperty::rvpimTabs> TRVParaInfoMainProperties;

class DELPHICLASS TrvActionParagraph;
class PASCALIMPLEMENTATION TrvActionParagraph : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
private:
	Rvstyle::TRVAlignment FAlignment;
	Rvstyle::TRVLineSpacingValue FLineSpacing;
	Rvstyle::TRVLineSpacingType FLineSpacingType;
	TRVParaInfoMainProperties FValidProperties;
	Rvstyle::TRVStyleLength FSpaceBefore;
	Rvstyle::TRVStyleLength FLeftIndent;
	Rvstyle::TRVStyleLength FFirstIndent;
	Rvstyle::TRVStyleLength FRightIndent;
	Rvstyle::TRVStyleLength FSpaceAfter;
	bool FUserInterface;
	bool FKeepWithNext;
	bool FKeepLinesTogether;
	int FOtlineLevel;
	bool FDeleteAllTabs;
	Rvclasses::TRVIntegerList* FTabsToDelete;
	Rvstyle::TRVTabInfos* FTabs;
	void __fastcall SetTabs(Rvstyle::TRVTabInfos* const Value);
	void __fastcall SetTabsToDelete(Rvclasses::TRVIntegerList* const Value);
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	__fastcall virtual TrvActionParagraph(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionParagraph(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	void __fastcall GetFromEditor(Rvedit::TCustomRichViewEdit* rve);
	__property TRVParaInfoMainProperties ValidProperties = {read=FValidProperties, write=FValidProperties, nodefault};
	__property Rvstyle::TRVStyleLength LeftIndent = {read=FLeftIndent, write=FLeftIndent, nodefault};
	__property Rvstyle::TRVStyleLength RightIndent = {read=FRightIndent, write=FRightIndent, nodefault};
	__property Rvstyle::TRVStyleLength FirstIndent = {read=FFirstIndent, write=FFirstIndent, nodefault};
	__property Rvstyle::TRVStyleLength SpaceBefore = {read=FSpaceBefore, write=FSpaceBefore, nodefault};
	__property Rvstyle::TRVStyleLength SpaceAfter = {read=FSpaceAfter, write=FSpaceAfter, nodefault};
	__property Rvstyle::TRVAlignment Alignment = {read=FAlignment, write=FAlignment, nodefault};
	__property Rvstyle::TRVLineSpacingValue LineSpacing = {read=FLineSpacing, write=FLineSpacing, nodefault};
	__property Rvstyle::TRVLineSpacingType LineSpacingType = {read=FLineSpacingType, write=FLineSpacingType, nodefault};
	__property bool KeepLinesTogether = {read=FKeepLinesTogether, write=FKeepLinesTogether, nodefault};
	__property bool KeepWithNext = {read=FKeepWithNext, write=FKeepWithNext, nodefault};
	__property bool DeleteAllTabs = {read=FDeleteAllTabs, write=FDeleteAllTabs, nodefault};
	__property Rvclasses::TRVIntegerList* TabsToDelete = {read=FTabsToDelete, write=SetTabsToDelete};
	__property Rvstyle::TRVTabInfos* Tabs = {read=FTabs, write=SetTabs};
	__property int OutlineLevel = {read=FOtlineLevel, write=FOtlineLevel, nodefault};
	
__published:
	__property bool UserInterface = {read=FUserInterface, write=FUserInterface, default=1};
};


class DELPHICLASS TrvActionParaCustomColor;
class PASCALIMPLEMENTATION TrvActionParaCustomColor : public TrvActionCustomColor
{
	typedef TrvActionCustomColor inherited;
	
private:
	void __fastcall NewOnParaStyleConversion(Rvedit::TCustomRichViewEdit* Sender, int StyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	
protected:
	virtual void __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, int Command);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command) = 0 ;
public:
	/* TrvActionCustomColor.Create */ inline __fastcall virtual TrvActionParaCustomColor(System::Classes::TComponent* AOwner) : TrvActionCustomColor(AOwner) { }
	/* TrvActionCustomColor.Destroy */ inline __fastcall virtual ~TrvActionParaCustomColor(void) { }
	
};


class DELPHICLASS TrvActionParaColor;
class PASCALIMPLEMENTATION TrvActionParaColor : public TrvActionParaCustomColor
{
	typedef TrvActionParaCustomColor inherited;
	
protected:
	virtual System::Uitypes::TColor __fastcall GetCurrentColor(Rvedit::TCustomRichViewEdit* FEdit);
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionParaColor(System::Classes::TComponent* AOwner);
public:
	/* TrvActionCustomColor.Destroy */ inline __fastcall virtual ~TrvActionParaColor(void) { }
	
};


class DELPHICLASS TrvActionParaColorAndPadding;
class PASCALIMPLEMENTATION TrvActionParaColorAndPadding : public TrvActionParaColor
{
	typedef TrvActionParaColor inherited;
	
private:
	Rvstyle::TRVBooleanRect* FUsePadding;
	Rvstyle::TRVRect* FPadding;
	void __fastcall SetPadding(Rvstyle::TRVRect* const Value);
	void __fastcall SetUsePadding(Rvstyle::TRVBooleanRect* const Value);
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionParaColorAndPadding(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionParaColorAndPadding(void);
	__property Rvstyle::TRVRect* Padding = {read=FPadding, write=SetPadding};
	__property Rvstyle::TRVBooleanRect* UsePadding = {read=FUsePadding, write=SetUsePadding};
};


class DELPHICLASS TrvActionAlignment;
class PASCALIMPLEMENTATION TrvActionAlignment : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
protected:
	Rvstyle::TRVAlignment FAlignment;
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvActionParaStyles.Create */ inline __fastcall virtual TrvActionAlignment(System::Classes::TComponent* AOwner) : TrvActionParaStyles(AOwner) { }
	
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionAlignment(void) { }
	
};


class DELPHICLASS TrvActionAlignLeft;
class PASCALIMPLEMENTATION TrvActionAlignLeft : public TrvActionAlignment
{
	typedef TrvActionAlignment inherited;
	
public:
	__fastcall virtual TrvActionAlignLeft(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionAlignLeft(void) { }
	
};


class DELPHICLASS TrvActionAlignCenter;
class PASCALIMPLEMENTATION TrvActionAlignCenter : public TrvActionAlignment
{
	typedef TrvActionAlignment inherited;
	
public:
	__fastcall virtual TrvActionAlignCenter(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionAlignCenter(void) { }
	
};


class DELPHICLASS TrvActionAlignRight;
class PASCALIMPLEMENTATION TrvActionAlignRight : public TrvActionAlignment
{
	typedef TrvActionAlignment inherited;
	
public:
	__fastcall virtual TrvActionAlignRight(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionAlignRight(void) { }
	
};


class DELPHICLASS TrvActionAlignJustify;
class PASCALIMPLEMENTATION TrvActionAlignJustify : public TrvActionAlignment
{
	typedef TrvActionAlignment inherited;
	
public:
	__fastcall virtual TrvActionAlignJustify(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionAlignJustify(void) { }
	
};


class DELPHICLASS TrvActionParaBiDi;
class PASCALIMPLEMENTATION TrvActionParaBiDi : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
private:
	Rvscroll::TRVBiDiMode FBiDiMode;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvActionParaStyles.Create */ inline __fastcall virtual TrvActionParaBiDi(System::Classes::TComponent* AOwner) : TrvActionParaStyles(AOwner) { }
	
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionParaBiDi(void) { }
	
};


class DELPHICLASS TrvActionParaLTR;
class PASCALIMPLEMENTATION TrvActionParaLTR : public TrvActionParaBiDi
{
	typedef TrvActionParaBiDi inherited;
	
public:
	__fastcall virtual TrvActionParaLTR(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionParaLTR(void) { }
	
};


class DELPHICLASS TrvActionParaRTL;
class PASCALIMPLEMENTATION TrvActionParaRTL : public TrvActionParaBiDi
{
	typedef TrvActionParaBiDi inherited;
	
public:
	__fastcall virtual TrvActionParaRTL(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionParaRTL(void) { }
	
};


class DELPHICLASS TrvActionIndent;
class PASCALIMPLEMENTATION TrvActionIndent : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
protected:
	Rvstyle::TRVStyleLength FIndentStep;
	Rvstyle::TRVStyleLength FIndentStepTmp;
	
public:
	__fastcall virtual TrvActionIndent(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property Rvstyle::TRVStyleLength IndentStep = {read=FIndentStep, write=FIndentStep, default=24};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionIndent(void) { }
	
};


class DELPHICLASS TrvActionIndentInc;
class PASCALIMPLEMENTATION TrvActionIndentInc : public TrvActionIndent
{
	typedef TrvActionIndent inherited;
	
private:
	Rvstyle::TRVStyleLength FIndentMax;
	Rvstyle::TRVStyleLength FIndentMaxTmp;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionIndentInc(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property Rvstyle::TRVStyleLength IndentMax = {read=FIndentMax, write=FIndentMax, default=240};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionIndentInc(void) { }
	
};


class DELPHICLASS TrvActionIndentDec;
class PASCALIMPLEMENTATION TrvActionIndentDec : public TrvActionIndent
{
	typedef TrvActionIndent inherited;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	
public:
	__fastcall virtual TrvActionIndentDec(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionIndentDec(void) { }
	
};


class DELPHICLASS TrvActionWordWrap;
class PASCALIMPLEMENTATION TrvActionWordWrap : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	__fastcall virtual TrvActionWordWrap(System::Classes::TComponent* AOwner);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionWordWrap(void) { }
	
};


class DELPHICLASS TrvActionLineSpacing;
class PASCALIMPLEMENTATION TrvActionLineSpacing : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
private:
	int FLineSpacing;
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvActionParaStyles.Create */ inline __fastcall virtual TrvActionLineSpacing(System::Classes::TComponent* AOwner) : TrvActionParaStyles(AOwner) { }
	
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionLineSpacing(void) { }
	
};


class DELPHICLASS TrvActionLineSpacing100;
class PASCALIMPLEMENTATION TrvActionLineSpacing100 : public TrvActionLineSpacing
{
	typedef TrvActionLineSpacing inherited;
	
public:
	__fastcall virtual TrvActionLineSpacing100(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionLineSpacing100(void) { }
	
};


class DELPHICLASS TrvActionLineSpacing150;
class PASCALIMPLEMENTATION TrvActionLineSpacing150 : public TrvActionLineSpacing
{
	typedef TrvActionLineSpacing inherited;
	
public:
	__fastcall virtual TrvActionLineSpacing150(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionLineSpacing150(void) { }
	
};


class DELPHICLASS TrvActionLineSpacing200;
class PASCALIMPLEMENTATION TrvActionLineSpacing200 : public TrvActionLineSpacing
{
	typedef TrvActionLineSpacing inherited;
	
public:
	__fastcall virtual TrvActionLineSpacing200(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionLineSpacing200(void) { }
	
};


enum DECLSPEC_DENUM TRVParaInfoBorderProperty : unsigned char { rvpibBackground_Color, rvpibBackground_BO_Left, rvpibBackground_BO_Top, rvpibBackground_BO_Right, rvpibBackground_BO_Bottom, rvpibBorder_Color, rvpibBorder_Style, rvpibBorder_Width, rvpibBorder_InternalWidth, rvpibBorder_BO_Left, rvpibBorder_BO_Top, rvpibBorder_BO_Right, rvpibBorder_BO_Bottom, rvpibBorder_Vis_Left, rvpibBorder_Vis_Top, rvpibBorder_Vis_Right, rvpibBorder_Vis_Bottom };

typedef System::Set<TRVParaInfoBorderProperty, TRVParaInfoBorderProperty::rvpibBackground_Color, TRVParaInfoBorderProperty::rvpibBorder_Vis_Bottom> TRVParaInfoBorderProperties;

class DELPHICLASS TrvActionParaBorder;
class PASCALIMPLEMENTATION TrvActionParaBorder : public TrvActionParaStyles
{
	typedef TrvActionParaStyles inherited;
	
private:
	bool FUserInterface;
	Rvstyle::TRVBorder* FBorder;
	TRVParaInfoBorderProperties FValidProperties;
	Rvstyle::TRVBackgroundRect* FBackground;
	void __fastcall SetBorder(Rvstyle::TRVBorder* const Value);
	void __fastcall SetBackground(Rvstyle::TRVBackgroundRect* const Value);
	
protected:
	virtual void __fastcall ApplyConversion(Rvedit::TCustomRichViewEdit* Editor, Rvstyle::TParaInfo* ParaInfo, int StyleNo, int Command);
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	virtual bool __fastcall ContinueIteration(int &CustomData);
	
public:
	__fastcall virtual TrvActionParaBorder(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionParaBorder(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	void __fastcall GetFromEditor(Rvedit::TCustomRichViewEdit* rve);
	__property TRVParaInfoBorderProperties ValidProperties = {read=FValidProperties, write=FValidProperties, nodefault};
	__property Rvstyle::TRVBorder* Border = {read=FBorder, write=SetBorder};
	__property Rvstyle::TRVBackgroundRect* Background = {read=FBackground, write=SetBackground};
	
__published:
	__property bool UserInterface = {read=FUserInterface, write=FUserInterface, default=1};
};


class DELPHICLASS TrvActionShowSpecialCharacters;
class PASCALIMPLEMENTATION TrvActionShowSpecialCharacters : public TrvAction
{
	typedef TrvAction inherited;
	
public:
	__fastcall virtual TrvActionShowSpecialCharacters(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionShowSpecialCharacters(void) { }
	
};


class DELPHICLASS TrvActionColor;
class PASCALIMPLEMENTATION TrvActionColor : public TrvActionCustomColor
{
	typedef TrvActionCustomColor inherited;
	
protected:
	virtual void __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, int Command);
	virtual System::Uitypes::TColor __fastcall GetCurrentColor(Rvedit::TCustomRichViewEdit* FEdit);
	
public:
	__fastcall virtual TrvActionColor(System::Classes::TComponent* AOwner);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual bool __fastcall RequiresMainDoc(void);
public:
	/* TrvActionCustomColor.Destroy */ inline __fastcall virtual ~TrvActionColor(void) { }
	
};


enum DECLSPEC_DENUM TRVAFillColorApplyToElement : unsigned char { rvafcaText, rvafcaParagraph, rvafcaCell, rvafcaTable };

typedef System::Set<TRVAFillColorApplyToElement, TRVAFillColorApplyToElement::rvafcaText, TRVAFillColorApplyToElement::rvafcaTable> TRVAFillColorApplyToSet;

class DELPHICLASS TrvActionFillColor;
class PASCALIMPLEMENTATION TrvActionFillColor : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	System::Uitypes::TColor FTextColor;
	System::Uitypes::TColor FParaColor;
	Rvstyle::TRVRect* Padding;
	Rvstyle::TRVBooleanRect* UsePadding;
	bool FTextColorSet;
	bool FParaColorSet;
	bool FTextColorMultiple;
	bool FParaColorMultiple;
	TRVAFillColorApplyToSet FAllowApplyingTo;
	
protected:
	virtual void __fastcall IterateProc(Crvdata::TCustomRVData* RVData, int ItemNo, Rvedit::TCustomRichViewEdit* rve, int &CustomData);
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	__fastcall virtual TrvActionFillColor(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	
__published:
	__property TRVAFillColorApplyToSet AllowApplyingTo = {read=FAllowApplyingTo, write=FAllowApplyingTo, default=15};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionFillColor(void) { }
	
};


class DELPHICLASS TrvActionInsertPageBreak;
class PASCALIMPLEMENTATION TrvActionInsertPageBreak : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	
public:
	__fastcall virtual TrvActionInsertPageBreak(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionInsertPageBreak(void) { }
	
};


class DELPHICLASS TrvActionRemovePageBreak;
class PASCALIMPLEMENTATION TrvActionRemovePageBreak : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	virtual bool __fastcall RequiresMainDoc(void);
	
public:
	__fastcall virtual TrvActionRemovePageBreak(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionRemovePageBreak(void) { }
	
};


class DELPHICLASS TrvActionClearTextFlow;
class PASCALIMPLEMENTATION TrvActionClearTextFlow : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	DYNAMIC bool __fastcall ClearLeft(void);
	DYNAMIC bool __fastcall ClearRight(void);
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearTextFlow(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionClearTextFlow(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionClearLeft;
class PASCALIMPLEMENTATION TrvActionClearLeft : public TrvActionClearTextFlow
{
	typedef TrvActionClearTextFlow inherited;
	
protected:
	DYNAMIC bool __fastcall ClearLeft(void);
	
public:
	__fastcall virtual TrvActionClearLeft(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearLeft(void) { }
	
};


class DELPHICLASS TrvActionClearRight;
class PASCALIMPLEMENTATION TrvActionClearRight : public TrvActionClearTextFlow
{
	typedef TrvActionClearTextFlow inherited;
	
protected:
	DYNAMIC bool __fastcall ClearRight(void);
	
public:
	__fastcall virtual TrvActionClearRight(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearRight(void) { }
	
};


class DELPHICLASS TrvActionClearNone;
class PASCALIMPLEMENTATION TrvActionClearNone : public TrvActionClearTextFlow
{
	typedef TrvActionClearTextFlow inherited;
	
public:
	__fastcall virtual TrvActionClearNone(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearNone(void) { }
	
};


class DELPHICLASS TrvActionClearBoth;
class PASCALIMPLEMENTATION TrvActionClearBoth : public TrvActionClearTextFlow
{
	typedef TrvActionClearTextFlow inherited;
	
protected:
	DYNAMIC bool __fastcall ClearLeft(void);
	DYNAMIC bool __fastcall ClearRight(void);
	
public:
	__fastcall virtual TrvActionClearBoth(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionClearBoth(void) { }
	
};


class DELPHICLASS TrvActionItemProperties;
class DELPHICLASS TrvActionInsertTable;
class PASCALIMPLEMENTATION TrvActionItemProperties : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	System::UnicodeString FGraphicFilter;
	bool FStoreImageFileName;
	System::UnicodeString FBackgroundGraphicFilter;
	TRVCustomItemPropertiesDialog FOnCustomItemPropertiesDialog;
	TRVCanApplyEvent FOnCanApply;
	TrvActionInsertHLine* FActionInsertHLine;
	TrvActionInsertTable* FActionInsertTable;
	void __fastcall SetActionInsertHLine(TrvActionInsertHLine* const Value);
	void __fastcall SetActionInsertTable(TrvActionInsertTable* const Value);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionItemProperties(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionItemProperties(void);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	bool __fastcall CanApply(Rvedit::TCustomRichViewEdit* Edit);
	
__published:
	__property System::UnicodeString GraphicFilter = {read=FGraphicFilter, write=FGraphicFilter};
	__property System::UnicodeString BackgroundGraphicFilter = {read=FBackgroundGraphicFilter, write=FBackgroundGraphicFilter};
	__property TRVCustomItemPropertiesDialog OnCustomItemPropertiesDialog = {read=FOnCustomItemPropertiesDialog, write=FOnCustomItemPropertiesDialog};
	__property TRVCanApplyEvent OnCanApply = {read=FOnCanApply, write=FOnCanApply};
	__property bool StoreImageFileName = {read=FStoreImageFileName, write=FStoreImageFileName, default=0};
	__property TrvActionInsertHLine* ActionInsertHLine = {read=FActionInsertHLine, write=SetActionInsertHLine};
	__property TrvActionInsertTable* ActionInsertTable = {read=FActionInsertTable, write=SetActionInsertTable};
};


class DELPHICLASS TrvActionVAlign;
class PASCALIMPLEMENTATION TrvActionVAlign : public TrvAction
{
	typedef TrvAction inherited;
	
public:
	__fastcall virtual TrvActionVAlign(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionVAlign(void) { }
	
};


class DELPHICLASS TrvActionBackground;
class PASCALIMPLEMENTATION TrvActionBackground : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	TRVAEditEvent FOnChange;
	System::UnicodeString FImageFileName;
	bool FCanChangeMargins;
	
public:
	__fastcall virtual TrvActionBackground(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual bool __fastcall RequiresMainDoc(void);
	__property System::UnicodeString ImageFileName = {read=FImageFileName};
	__property TRVAEditEvent OnChange = {read=FOnChange, write=FOnChange};
	
__published:
	__property bool CanChangeMargins = {read=FCanChangeMargins, write=FCanChangeMargins, default=1};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionBackground(void) { }
	
};


typedef void __fastcall (__closure *TRVAInsertTableEvent)(TrvActionInsertTable* Sender, Rvtable::TRVTableItemInfo* table);

class PASCALIMPLEMENTATION TrvActionInsertTable : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Rvedit::TCustomRichViewEdit* FEdit;
	Vcl::Forms::TForm* FTableSizeForm;
	bool FVOutermostRule;
	bool FHOutermostRule;
	Rvstyle::TRVStyleLength FCellHSpacing;
	Rvstyle::TRVStyleLength FCellHPadding;
	Rvstyle::TRVStyleLength FCellVPadding;
	Rvstyle::TRVStyleLength FBorderWidth;
	Rvstyle::TRVStyleLength FCellVSpacing;
	Rvstyle::TRVStyleLength FHRuleWidth;
	Rvstyle::TRVStyleLength FCellBorderWidth;
	Rvstyle::TRVStyleLength FBorderHSpacing;
	Rvstyle::TRVStyleLength FBorderVSpacing;
	Rvstyle::TRVStyleLength FVRuleWidth;
	System::Uitypes::TColor FBorderLightColor;
	System::Uitypes::TColor FBorderColor;
	System::Uitypes::TColor FCellBorderColor;
	System::Uitypes::TColor FHRuleColor;
	System::Uitypes::TColor FColor;
	System::Uitypes::TColor FCellBorderLightColor;
	System::Uitypes::TColor FVRuleColor;
	Rvtable::TRVHTMLLength FBestWidth;
	Rvtable::TRVTableBorderStyle FBorderStyle;
	Rvtable::TRVTableBorderStyle FCellBorderStyle;
	Rvtable::TRVTableOptions FOptions;
	int FColCount;
	int FRowCount;
	int FHeadingRowCount;
	TRVAInsertTableEvent FOnInserting;
	System::Classes::TNotifyEvent FOnCloseTableSizeDialog;
	System::UnicodeString FItemText;
	Rvstyle::TRVBooleanRect* FVisibleBorders;
	Rvstyle::TRVItemBackgroundStyle FBackgroundStyle;
	Vcl::Graphics::TPicture* FBackgroundPicture;
	Rvtable::TRVTablePrintOptions FPrintOptions;
	void __fastcall ApplyToTable(Rvtable::TRVTableItemInfo* table, Rvtable::TRVHTMLLength ABestWidth);
	void __fastcall SetTableCellsWidth(Rvtable::TRVTableItemInfo* table, Rvstyle::TRVStyleLength Width);
	void __fastcall TableSizeFormDestroy(System::TObject* Sender);
	void __fastcall SetCellPadding(Rvstyle::TRVStyleLength Value);
	Rvstyle::TRVStyleLength __fastcall GetCellPadding(void);
	void __fastcall SetVisibleBorders(Rvstyle::TRVBooleanRect* const Value);
	void __fastcall SetBackgroundPicture(Vcl::Graphics::TPicture* const Value);
	Vcl::Graphics::TPicture* __fastcall GetBackgroundPicture(void);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	void __fastcall ShowTableSizeDialog(Rvedit::TCustomRichViewEdit* Target, Vcl::Controls::TControl* Button)/* overload */;
	void __fastcall ShowTableSizeDialog(Rvedit::TCustomRichViewEdit* Target, const System::Types::TRect &ButtonRect)/* overload */;
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	__fastcall virtual TrvActionInsertTable(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionInsertTable(void);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property int RowCount = {read=FRowCount, write=FRowCount, default=2};
	__property int ColCount = {read=FColCount, write=FColCount, default=2};
	__property Rvtable::TRVTableOptions TableOptions = {read=FOptions, write=FOptions, default=31};
	__property Rvtable::TRVTablePrintOptions TablePrintOptions = {read=FPrintOptions, write=FPrintOptions, default=3};
	__property Rvtable::TRVHTMLLength BestWidth = {read=FBestWidth, write=FBestWidth, default=0};
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=536870911};
	__property int HeadingRowCount = {read=FHeadingRowCount, write=FHeadingRowCount, default=0};
	__property Rvstyle::TRVStyleLength BorderWidth = {read=FBorderWidth, write=FBorderWidth, default=1};
	__property System::Uitypes::TColor BorderColor = {read=FBorderColor, write=FBorderColor, default=-16777208};
	__property System::Uitypes::TColor BorderLightColor = {read=FBorderLightColor, write=FBorderLightColor, default=-16777196};
	__property Rvtable::TRVTableBorderStyle BorderStyle = {read=FBorderStyle, write=FBorderStyle, default=2};
	__property Rvstyle::TRVStyleLength BorderVSpacing = {read=FBorderVSpacing, write=FBorderVSpacing, default=2};
	__property Rvstyle::TRVStyleLength BorderHSpacing = {read=FBorderHSpacing, write=FBorderHSpacing, default=2};
	__property Rvstyle::TRVBooleanRect* VisibleBorders = {read=FVisibleBorders, write=SetVisibleBorders};
	__property Rvstyle::TRVItemBackgroundStyle BackgroundStyle = {read=FBackgroundStyle, write=FBackgroundStyle, default=0};
	__property Vcl::Graphics::TPicture* BackgroundPicture = {read=GetBackgroundPicture, write=SetBackgroundPicture};
	__property Rvstyle::TRVStyleLength CellBorderWidth = {read=FCellBorderWidth, write=FCellBorderWidth, default=1};
	__property System::Uitypes::TColor CellBorderColor = {read=FCellBorderColor, write=FCellBorderColor, default=-16777208};
	__property System::Uitypes::TColor CellBorderLightColor = {read=FCellBorderLightColor, write=FCellBorderLightColor, default=-16777196};
	__property Rvstyle::TRVStyleLength CellPadding = {read=GetCellPadding, write=SetCellPadding, stored=false, nodefault};
	__property Rvstyle::TRVStyleLength CellHPadding = {read=FCellHPadding, write=FCellHPadding, default=1};
	__property Rvstyle::TRVStyleLength CellVPadding = {read=FCellVPadding, write=FCellVPadding, default=1};
	__property Rvtable::TRVTableBorderStyle CellBorderStyle = {read=FCellBorderStyle, write=FCellBorderStyle, default=2};
	__property Rvstyle::TRVStyleLength VRuleWidth = {read=FVRuleWidth, write=FVRuleWidth, default=0};
	__property System::Uitypes::TColor VRuleColor = {read=FVRuleColor, write=FVRuleColor, default=-16777208};
	__property Rvstyle::TRVStyleLength HRuleWidth = {read=FHRuleWidth, write=FHRuleWidth, default=0};
	__property System::Uitypes::TColor HRuleColor = {read=FHRuleColor, write=FHRuleColor, default=-16777208};
	__property Rvstyle::TRVStyleLength CellVSpacing = {read=FCellVSpacing, write=FCellVSpacing, default=2};
	__property Rvstyle::TRVStyleLength CellHSpacing = {read=FCellHSpacing, write=FCellHSpacing, default=2};
	__property bool VOutermostRule = {read=FVOutermostRule, write=FVOutermostRule, default=0};
	__property bool HOutermostRule = {read=FHOutermostRule, write=FHOutermostRule, default=0};
	__property System::UnicodeString ItemText = {read=FItemText, write=FItemText};
	__property TRVAInsertTableEvent OnInserting = {read=FOnInserting, write=FOnInserting};
	__property System::Classes::TNotifyEvent OnCloseTableSizeDialog = {read=FOnCloseTableSizeDialog, write=FOnCloseTableSizeDialog};
};


class DELPHICLASS TrvActionTableCell;
class PASCALIMPLEMENTATION TrvActionTableCell : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	DYNAMIC bool __fastcall CanApplyToHTML(void);
	DYNAMIC bool __fastcall CanApplyToRTF(void);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo) = 0 ;
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo) = 0 ;
	
public:
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCell(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTableCell(System::Classes::TComponent* AOwner) : TrvAction(AOwner) { }
	
};


class DELPHICLASS TrvActionTableRCBase;
class PASCALIMPLEMENTATION TrvActionTableRCBase : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableRCBase(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTableRCBase(System::Classes::TComponent* AOwner) : TrvActionTableCell(AOwner) { }
	
};


class DELPHICLASS TrvActionTableInsertRowsAbove;
class PASCALIMPLEMENTATION TrvActionTableInsertRowsAbove : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableInsertRowsAbove(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableInsertRowsAbove(void) { }
	
};


class DELPHICLASS TrvActionTableInsertRowsBelow;
class PASCALIMPLEMENTATION TrvActionTableInsertRowsBelow : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
private:
	bool FAllowMultiple;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableInsertRowsBelow(System::Classes::TComponent* AOwner);
	
__published:
	__property bool AllowMultiple = {read=FAllowMultiple, write=FAllowMultiple, nodefault};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableInsertRowsBelow(void) { }
	
};


class DELPHICLASS TrvActionTableInsertColLeft;
class PASCALIMPLEMENTATION TrvActionTableInsertColLeft : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableInsertColLeft(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableInsertColLeft(void) { }
	
};


class DELPHICLASS TrvActionTableInsertColRight;
class PASCALIMPLEMENTATION TrvActionTableInsertColRight : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableInsertColRight(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableInsertColRight(void) { }
	
};


class DELPHICLASS TrvActionTableDeleteRows;
class PASCALIMPLEMENTATION TrvActionTableDeleteRows : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableDeleteRows(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableDeleteRows(void) { }
	
};


class DELPHICLASS TrvActionTableDeleteCols;
class PASCALIMPLEMENTATION TrvActionTableDeleteCols : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableDeleteCols(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableDeleteCols(void) { }
	
};


class DELPHICLASS TrvActionTableDeleteTable;
class PASCALIMPLEMENTATION TrvActionTableDeleteTable : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableDeleteTable(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableDeleteTable(void) { }
	
};


class DELPHICLASS TrvActionTableMergeCells;
class PASCALIMPLEMENTATION TrvActionTableMergeCells : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableMergeCells(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableMergeCells(void) { }
	
};


class DELPHICLASS TrvActionTableSplitCells;
class PASCALIMPLEMENTATION TrvActionTableSplitCells : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSplitCells(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSplitCells(void) { }
	
};


class DELPHICLASS TrvActionTableSelectTable;
class PASCALIMPLEMENTATION TrvActionTableSelectTable : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSelectTable(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSelectTable(void) { }
	
};


class DELPHICLASS TrvActionTableSelectRows;
class PASCALIMPLEMENTATION TrvActionTableSelectRows : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSelectRows(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSelectRows(void) { }
	
};


class DELPHICLASS TrvActionTableSelectCols;
class PASCALIMPLEMENTATION TrvActionTableSelectCols : public TrvActionTableRCBase
{
	typedef TrvActionTableRCBase inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSelectCols(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSelectCols(void) { }
	
};


class DELPHICLASS TrvActionTableSelectCell;
class PASCALIMPLEMENTATION TrvActionTableSelectCell : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSelectCell(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSelectCell(void) { }
	
};


class DELPHICLASS TrvActionTableMultiCellAttributes;
class PASCALIMPLEMENTATION TrvActionTableMultiCellAttributes : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall IterateCellProc(Rvtable::TRVTableItemInfo* table, bool FirstCell, int Row, int Col, int CustomData);
	void __fastcall IterateCells(Rvtable::TRVTableItemInfo* table, int CustomData);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableMultiCellAttributes(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTableMultiCellAttributes(System::Classes::TComponent* AOwner) : TrvActionTableCell(AOwner) { }
	
};


class DELPHICLASS TrvActionTableCellVAlign;
class PASCALIMPLEMENTATION TrvActionTableCellVAlign : public TrvActionTableMultiCellAttributes
{
	typedef TrvActionTableMultiCellAttributes inherited;
	
private:
	Rvtable::TRVCellVAlign FVAlign;
	bool FAllEqual;
	
protected:
	virtual bool __fastcall IterateCellProc(Rvtable::TRVTableItemInfo* table, bool FirstCell, int Row, int Col, int CustomData);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellVAlign(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTableCellVAlign(System::Classes::TComponent* AOwner) : TrvActionTableMultiCellAttributes(AOwner) { }
	
};


class DELPHICLASS TrvActionTableCellVAlignTop;
class PASCALIMPLEMENTATION TrvActionTableCellVAlignTop : public TrvActionTableCellVAlign
{
	typedef TrvActionTableCellVAlign inherited;
	
public:
	__fastcall virtual TrvActionTableCellVAlignTop(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellVAlignTop(void) { }
	
};


class DELPHICLASS TrvActionTableCellVAlignMiddle;
class PASCALIMPLEMENTATION TrvActionTableCellVAlignMiddle : public TrvActionTableCellVAlign
{
	typedef TrvActionTableCellVAlign inherited;
	
public:
	__fastcall virtual TrvActionTableCellVAlignMiddle(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellVAlignMiddle(void) { }
	
};


class DELPHICLASS TrvActionTableCellVAlignBottom;
class PASCALIMPLEMENTATION TrvActionTableCellVAlignBottom : public TrvActionTableCellVAlign
{
	typedef TrvActionTableCellVAlign inherited;
	
public:
	__fastcall virtual TrvActionTableCellVAlignBottom(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellVAlignBottom(void) { }
	
};


class DELPHICLASS TrvActionTableCellVAlignDefault;
class PASCALIMPLEMENTATION TrvActionTableCellVAlignDefault : public TrvActionTableCellVAlign
{
	typedef TrvActionTableCellVAlign inherited;
	
public:
	__fastcall virtual TrvActionTableCellVAlignDefault(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellVAlignDefault(void) { }
	
};


class DELPHICLASS TrvActionTableCellRotation;
class PASCALIMPLEMENTATION TrvActionTableCellRotation : public TrvActionTableMultiCellAttributes
{
	typedef TrvActionTableMultiCellAttributes inherited;
	
private:
	Rvstyle::TRVDocRotation FRotation;
	bool FAllEqual;
	
protected:
	DYNAMIC bool __fastcall CanApplyToHTML(void);
	DYNAMIC bool __fastcall CanApplyToRTF(void);
	virtual bool __fastcall IterateCellProc(Rvtable::TRVTableItemInfo* table, bool FirstCell, int Row, int Col, int CustomData);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellRotation(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTableCellRotation(System::Classes::TComponent* AOwner) : TrvActionTableMultiCellAttributes(AOwner) { }
	
};


class DELPHICLASS TrvActionTableCellRotationNone;
class PASCALIMPLEMENTATION TrvActionTableCellRotationNone : public TrvActionTableCellRotation
{
	typedef TrvActionTableCellRotation inherited;
	
public:
	__fastcall virtual TrvActionTableCellRotationNone(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellRotationNone(void) { }
	
};


class DELPHICLASS TrvActionTableCellRotation90;
class PASCALIMPLEMENTATION TrvActionTableCellRotation90 : public TrvActionTableCellRotation
{
	typedef TrvActionTableCellRotation inherited;
	
public:
	__fastcall virtual TrvActionTableCellRotation90(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellRotation90(void) { }
	
};


class DELPHICLASS TrvActionTableCellRotation180;
class PASCALIMPLEMENTATION TrvActionTableCellRotation180 : public TrvActionTableCellRotation
{
	typedef TrvActionTableCellRotation inherited;
	
public:
	__fastcall virtual TrvActionTableCellRotation180(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellRotation180(void) { }
	
};


class DELPHICLASS TrvActionTableCellRotation270;
class PASCALIMPLEMENTATION TrvActionTableCellRotation270 : public TrvActionTableCellRotation
{
	typedef TrvActionTableCellRotation inherited;
	
public:
	__fastcall virtual TrvActionTableCellRotation270(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellRotation270(void) { }
	
};


class DELPHICLASS TrvActionTableSplit;
class PASCALIMPLEMENTATION TrvActionTableSplit : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	int __fastcall GetTopSelectedRow(Rvtable::TRVTableItemInfo* table);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSplit(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSplit(void) { }
	
};


class DELPHICLASS TrvActionTableToText;
class PASCALIMPLEMENTATION TrvActionTableToText : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableToText(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableToText(void) { }
	
};


class DELPHICLASS TrvActionTableSort;
class PASCALIMPLEMENTATION TrvActionTableSort : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	
public:
	__fastcall virtual TrvActionTableSort(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableSort(void) { }
	
};


class DELPHICLASS TrvActionTableProperties;
class PASCALIMPLEMENTATION TrvActionTableProperties : public TrvActionTableCell
{
	typedef TrvActionTableCell inherited;
	
private:
	System::UnicodeString FBackgroundGraphicFilter;
	bool FStoreImageFileName;
	TrvActionInsertTable* FActionInsertTable;
	void __fastcall SetActionInsertTable(TrvActionInsertTable* const Value);
	
protected:
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	
public:
	__fastcall virtual TrvActionTableProperties(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionTableProperties(void);
	
__published:
	__property System::UnicodeString BackgroundGraphicFilter = {read=FBackgroundGraphicFilter, write=FBackgroundGraphicFilter};
	__property bool StoreImageFileName = {read=FStoreImageFileName, write=FStoreImageFileName, default=0};
	__property TrvActionInsertTable* ActionInsertTable = {read=FActionInsertTable, write=SetActionInsertTable};
};


class DELPHICLASS TrvActionTableGrid;
class PASCALIMPLEMENTATION TrvActionTableGrid : public TrvAction
{
	typedef TrvAction inherited;
	
protected:
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	virtual bool __fastcall RequiresMainDoc(void);
	
public:
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	__fastcall virtual TrvActionTableGrid(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableGrid(void) { }
	
};


class DELPHICLASS TrvActionTableCellBorder;
class PASCALIMPLEMENTATION TrvActionTableCellBorder : public TrvActionTableMultiCellAttributes
{
	typedef TrvActionTableMultiCellAttributes inherited;
	
private:
	bool FAllEqual;
	
protected:
	virtual bool __fastcall IsBordered(Rvstyle::TRVBooleanRect* aVisibleBorders) = 0 ;
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue) = 0 ;
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall IterateCellProc(Rvtable::TRVTableItemInfo* table, bool FirstCell, int Row, int Col, int CustomData);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellBorder(void) { }
	
public:
	/* TAction.Create */ inline __fastcall virtual TrvActionTableCellBorder(System::Classes::TComponent* AOwner) : TrvActionTableMultiCellAttributes(AOwner) { }
	
};


class DELPHICLASS TrvActionTableCellLeftBorder;
class PASCALIMPLEMENTATION TrvActionTableCellLeftBorder : public TrvActionTableCellBorder
{
	typedef TrvActionTableCellBorder inherited;
	
protected:
	virtual bool __fastcall IsBordered(Rvstyle::TRVBooleanRect* aVisibleBorders);
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue);
	
public:
	__fastcall virtual TrvActionTableCellLeftBorder(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellLeftBorder(void) { }
	
};


class DELPHICLASS TrvActionTableCellRightBorder;
class PASCALIMPLEMENTATION TrvActionTableCellRightBorder : public TrvActionTableCellBorder
{
	typedef TrvActionTableCellBorder inherited;
	
protected:
	virtual bool __fastcall IsBordered(Rvstyle::TRVBooleanRect* aVisibleBorders);
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue);
	
public:
	__fastcall virtual TrvActionTableCellRightBorder(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellRightBorder(void) { }
	
};


class DELPHICLASS TrvActionTableCellTopBorder;
class PASCALIMPLEMENTATION TrvActionTableCellTopBorder : public TrvActionTableCellBorder
{
	typedef TrvActionTableCellBorder inherited;
	
protected:
	virtual bool __fastcall IsBordered(Rvstyle::TRVBooleanRect* aVisibleBorders);
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue);
	
public:
	__fastcall virtual TrvActionTableCellTopBorder(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellTopBorder(void) { }
	
};


class DELPHICLASS TrvActionTableCellBottomBorder;
class PASCALIMPLEMENTATION TrvActionTableCellBottomBorder : public TrvActionTableCellBorder
{
	typedef TrvActionTableCellBorder inherited;
	
protected:
	virtual bool __fastcall IsBordered(Rvstyle::TRVBooleanRect* aVisibleBorders);
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue);
	
public:
	__fastcall virtual TrvActionTableCellBottomBorder(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellBottomBorder(void) { }
	
};


class DELPHICLASS TrvActionTableCellAllBorders;
class PASCALIMPLEMENTATION TrvActionTableCellAllBorders : public TrvActionTableCellBorder
{
	typedef TrvActionTableCellBorder inherited;
	
protected:
	virtual bool __fastcall IsBordered(Rvstyle::TRVBooleanRect* aVisibleBorders);
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue);
	
public:
	__fastcall virtual TrvActionTableCellAllBorders(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellAllBorders(void) { }
	
};


class DELPHICLASS TrvActionTableCellNoBorders;
class PASCALIMPLEMENTATION TrvActionTableCellNoBorders : public TrvActionTableCellBorder
{
	typedef TrvActionTableCellBorder inherited;
	
protected:
	virtual bool __fastcall IsApplicable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual bool __fastcall ExecuteCommand(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table, int ItemNo);
	virtual void __fastcall SetBorder(Rvtable::TRVTableItemInfo* table, int r, int c, const bool aValue);
	
public:
	__fastcall virtual TrvActionTableCellNoBorders(System::Classes::TComponent* AOwner);
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionTableCellNoBorders(void) { }
	
};


class DELPHICLASS TrvActionParaList;
class DELPHICLASS TrvActionParaBullets;
class DELPHICLASS TrvActionParaNumbering;
class PASCALIMPLEMENTATION TrvActionParaList : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	bool FUpdateAllActionsOnForm;
	TrvActionParaBullets* FActionParaBullets;
	TrvActionParaNumbering* FActionParaNumbering;
	Rvstyle::TRVStyleLength FIndentStep;
	void __fastcall SetActionParaBullets(TrvActionParaBullets* const Value);
	void __fastcall SetActionParaNumbering(TrvActionParaNumbering* const Value);
	void __fastcall UpdateBulletsAction(Rvstyle::TRVStyle* RVStyle, int ListNo);
	void __fastcall UpdateNumberingAction(Rvstyle::TRVStyle* RVStyle, int ListNo);
	Vcl::Forms::TForm* __fastcall ShowForm(Rvedit::TCustomRichViewEdit* rve, Rvstyle::TRVListInfo* &ListStyle, int &ListNo, int &StartFrom, bool &UseStartFrom);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC bool __fastcall CanApplyToPlainText(void);
	
public:
	System::StaticArray<Rvstyle::TRVStyle*, 2> ModifiedTemplates;
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
	__fastcall virtual TrvActionParaList(System::Classes::TComponent* AOwner);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	__property TrvActionParaBullets* ActionParaBullets = {read=FActionParaBullets, write=SetActionParaBullets};
	__property TrvActionParaNumbering* ActionParaNumbering = {read=FActionParaNumbering, write=SetActionParaNumbering};
	__property bool UpdateAllActionsOnForm = {read=FUpdateAllActionsOnForm, write=FUpdateAllActionsOnForm, default=1};
	
__published:
	__property Rvstyle::TRVStyleLength IndentStep = {read=FIndentStep, write=FIndentStep, default=24};
public:
	/* TrvAction.Destroy */ inline __fastcall virtual ~TrvActionParaList(void) { }
	
};


class DELPHICLASS TrvActionCustomParaListSwitcher;
class PASCALIMPLEMENTATION TrvActionCustomParaListSwitcher : public TrvAction
{
	typedef TrvAction inherited;
	
private:
	Rvstyle::TRVListLevelCollection* FListLevels;
	Rvstyle::TRVStyleLength FIndentStep;
	void __fastcall SetListLevels(Rvstyle::TRVListLevelCollection* const Value);
	bool __fastcall StoreListLevels(void);
	void __fastcall SetIndentStep(const Rvstyle::TRVStyleLength Value);
	
protected:
	DYNAMIC void __fastcall ResetLevels(Rvstyle::TRVListLevelCollection* AListLevels) = 0 ;
	Rvstyle::TRVListLevelCollection* __fastcall GetTempLevels(Rvstyle::TRVStyle* RVStyle);
	
public:
	__fastcall virtual TrvActionCustomParaListSwitcher(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TrvActionCustomParaListSwitcher(void);
	void __fastcall Reset(void);
	virtual void __fastcall UpdateTarget(System::TObject* Target);
	DYNAMIC void __fastcall ConvertToPixels(void);
	DYNAMIC void __fastcall ConvertToTwips(void);
	
__published:
	__property Rvstyle::TRVStyleLength IndentStep = {read=FIndentStep, write=SetIndentStep, default=24};
	__property Rvstyle::TRVListLevelCollection* ListLevels = {read=FListLevels, write=SetListLevels, stored=StoreListLevels};
};


class PASCALIMPLEMENTATION TrvActionParaBullets : public TrvActionCustomParaListSwitcher
{
	typedef TrvActionCustomParaListSwitcher inherited;
	
protected:
	DYNAMIC void __fastcall ResetLevels(Rvstyle::TRVListLevelCollection* AListLevels);
	
public:
	__fastcall virtual TrvActionParaBullets(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvActionCustomParaListSwitcher.Destroy */ inline __fastcall virtual ~TrvActionParaBullets(void) { }
	
};


class PASCALIMPLEMENTATION TrvActionParaNumbering : public TrvActionCustomParaListSwitcher
{
	typedef TrvActionCustomParaListSwitcher inherited;
	
protected:
	DYNAMIC void __fastcall ResetLevels(Rvstyle::TRVListLevelCollection* AListLevels);
	
public:
	__fastcall virtual TrvActionParaNumbering(System::Classes::TComponent* AOwner);
	virtual void __fastcall ExecuteTarget(System::TObject* Target);
public:
	/* TrvActionCustomParaListSwitcher.Destroy */ inline __fastcall virtual ~TrvActionParaNumbering(void) { }
	
};


typedef bool __fastcall (*TRVAEditorControlCommandFunction)(Vcl::Controls::TControl* Control, TRVAEditorControlCommand Command);

typedef Rvedit::TCustomRichViewEdit* __fastcall (*TRVAGetRichViewEditFromPopupComponentFunction)(System::Classes::TComponent* PopupComponent);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVAEditorControlCommandFunction RVA_EditorControlFunction;
extern DELPHI_PACKAGE TRVAGetRichViewEditFromPopupComponentFunction RVA_GetRichViewEditFromPopupComponent;
extern DELPHI_PACKAGE TrvaDocumentInfoClass RVA_DocumentInfoClass;
extern DELPHI_PACKAGE bool RVA_EditForceDefControl;
extern DELPHI_PACKAGE TRVAMessageBox RVA_MessageBox;
static const System::Int8 RVALUM_THRESHOLD = System::Int8(0x1b);
extern DELPHI_PACKAGE TRVAControlPanel* MainRVAControlPanel;
extern DELPHI_PACKAGE System::ResourceString _sFileFilterRVF;
#define Richviewactions_sFileFilterRVF System::LoadResourceString(&Richviewactions::_sFileFilterRVF)
extern DELPHI_PACKAGE System::ResourceString _sFileFilterXML;
#define Richviewactions_sFileFilterXML System::LoadResourceString(&Richviewactions::_sFileFilterXML)
extern DELPHI_PACKAGE System::ResourceString _sRVFExtension;
#define Richviewactions_sRVFExtension System::LoadResourceString(&Richviewactions::_sRVFExtension)
extern DELPHI_PACKAGE System::ResourceString _sFileFilterRVStyles;
#define Richviewactions_sFileFilterRVStyles System::LoadResourceString(&Richviewactions::_sFileFilterRVStyles)
extern DELPHI_PACKAGE System::ResourceString _sRVStylesExtension;
#define Richviewactions_sRVStylesExtension System::LoadResourceString(&Richviewactions::_sRVStylesExtension)
extern DELPHI_PACKAGE System::ResourceString _errInvalidRVControl;
#define Richviewactions_errInvalidRVControl System::LoadResourceString(&Richviewactions::_errInvalidRVControl)
extern DELPHI_PACKAGE System::ResourceString _errInvalidControlPanel;
#define Richviewactions_errInvalidControlPanel System::LoadResourceString(&Richviewactions::_errInvalidControlPanel)
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVAFormat(const System::UnicodeString FormatStr, System::TVarRec const *Args, const int Args_High);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVADeleteAmp(const System::UnicodeString S);
extern DELPHI_PACKAGE Rvedit::TCustomRichViewEdit* __fastcall GetRichViewEditFromPopupComponent(System::Classes::TComponent* PopupComponent);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVA_GetColorName(System::Uitypes::TColor Color, TRVAControlPanel* ControlPanel = (TRVAControlPanel*)(0x0));
extern DELPHI_PACKAGE int __fastcall RVA_GetComboBorderWidth(int Index, bool HasZero, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE Rvstyle::TRVStyleLength __fastcall RVA_GetComboBorderWidth2(int Index, bool HasZero, Rvstyle::TRVStyle* RVStyle, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE int __fastcall RVA_GetComboBorderItemIndex(Rvstyle::TRVStyleLength Width, bool HasZero, Rvstyle::TRVStyle* RVStyle, int Count, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVA_FillWidthComboBox(Vcl::Stdctrls::TComboBox* cmb, Vcl::Graphics::TCanvas* Canvas, bool IncludeZero, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVA_DrawCmbWidthItem(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect, int Index, bool HasZero, System::Uitypes::TColor Color, Winapi::Windows::TOwnerDrawState State, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVA_DrawCmbWidthItem2(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect, int Index, bool HasZero, System::Uitypes::TColor Color1, System::Uitypes::TColor Color2, Winapi::Windows::TOwnerDrawState State, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE System::Classes::TComponent* __fastcall RVAFindComponentByClass(System::Classes::TComponent* Owner, System::Classes::TComponentClass CClass);
extern DELPHI_PACKAGE Rvstyle::TRVTag __fastcall RVAMakeTag(const System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall MakeImportFilter(TrvFileImportFilterSet FileFilter, Rvclasses::TRVIntegerList* List, const System::UnicodeString RVFFilter, const System::UnicodeString CustomFilter, Rvofficecnv::TRVOfficeConverter* rvc, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE void __fastcall GetImportFilterFormat(Rvclasses::TRVIntegerList* List, int Index, TrvFileImportFilter &FileFilter, int &ConverterOrCustomIndex);
extern DELPHI_PACKAGE System::UnicodeString __fastcall MakeExportFilter(TrvFileExportFilterSet FileFilter, Rvclasses::TRVIntegerList* List, const System::UnicodeString RVFFilter, const System::UnicodeString CustomFilter, Rvofficecnv::TRVOfficeConverter* rvc, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE void __fastcall GetExportFilterFormat(Rvclasses::TRVIntegerList* List, int Index, TrvFileExportFilter &FileFilter, int &ConverterOrCustomIndex);
extern DELPHI_PACKAGE void __fastcall ConvertToUnicode(Richview::TCustomRichView* rv);
extern DELPHI_PACKAGE void __fastcall PasteHTML(Rvedit::TCustomRichViewEdit* rve, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall GoToCheckpoint(Rvedit::TCustomRichViewEdit* rv, const System::UnicodeString CPName);
extern DELPHI_PACKAGE void __fastcall RVA_LocalizeForm(System::Classes::TComponent* Form);
extern DELPHI_PACKAGE void __fastcall RVA_ConvertToTwips(System::Classes::TComponent* Form);
extern DELPHI_PACKAGE void __fastcall RVA_ConvertToPixels(System::Classes::TComponent* Form);
extern DELPHI_PACKAGE bool __fastcall RVA_ChooseStyle(TRVAControlPanel* ControlPanel = (TRVAControlPanel*)(0x0));
extern DELPHI_PACKAGE int __fastcall RVA_ChooseValue(System::Classes::TStrings* const Strings, Rvalocalize::TRVAMessageID MsgTitle, Rvalocalize::TRVAMessageID MsgLabel, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall RVA_ChooseCodePage(Rvstyle::TRVCodePage &CodePage, TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall RVA_ChooseLanguage(TRVAControlPanel* ControlPanel = (TRVAControlPanel*)(0x0));
extern DELPHI_PACKAGE TRVAHFInfo* __fastcall RVA_HeaderInfo _DEPRECATED_ATTRIBUTE0 (TRVAControlPanel* ControlPanel = (TRVAControlPanel*)(0x0));
extern DELPHI_PACKAGE TRVAHFInfo* __fastcall RVA_FooterInfo _DEPRECATED_ATTRIBUTE0 (TRVAControlPanel* ControlPanel = (TRVAControlPanel*)(0x0));
extern DELPHI_PACKAGE bool __fastcall RVA_EditorControlFunctionDef(Vcl::Controls::TControl* Control, TRVAEditorControlCommand Command);
extern DELPHI_PACKAGE bool __fastcall RvHtmlHelp(Rvtypes::TRVUnicodeString HelpFileName);
extern DELPHI_PACKAGE Rvstyle::TRVUnits __fastcall RVA_GetFineUnits(TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE Rvstyle::TRVUnits __fastcall RVA_GetBorderUnits(TRVAControlPanel* ControlPanel);
extern DELPHI_PACKAGE System::Classes::TStringList* __fastcall RVAGetListOfSequences(Richview::TCustomRichView* rv);
extern DELPHI_PACKAGE bool __fastcall RVAIsSeqNameAllowed(const System::UnicodeString s);
}	/* namespace Richviewactions */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RICHVIEWACTIONS)
using namespace Richviewactions;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RichviewactionsHPP
