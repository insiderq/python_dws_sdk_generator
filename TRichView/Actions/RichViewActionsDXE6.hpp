﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RichViewActionsDXE6.dpk' rev: 27.00 (Windows)

#ifndef Richviewactionsdxe6HPP
#define Richviewactionsdxe6HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Ruler.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVALocRuler.hpp>	// Pascal unit
#include <RVCharCase.hpp>	// Pascal unit
#include <rvcsv.hpp>	// Pascal unit
#include <RVFontCombos.hpp>	// Pascal unit
#include <RVGrids.hpp>	// Pascal unit
#include <RVNormalize.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <RVOfficeRadioBtnReg.hpp>	// Pascal unit
#include <RVRuler.hpp>	// Pascal unit
#include <RVRulerReg.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVSpinEditReg.hpp>	// Pascal unit
#include <RVFontCombosReg.hpp>	// Pascal unit
#include <RVColorReg.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVColorGrid.hpp>	// Pascal unit
#include <TableSizeRVFrm.hpp>	// Pascal unit
#include <BackRVFrm.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <BorderSidesRVFrm.hpp>	// Pascal unit
#include <ColorRVFrm.hpp>	// Pascal unit
#include <dmActions.hpp>	// Pascal unit
#include <FillColorRVFrm.hpp>	// Pascal unit
#include <FontRVFrm.hpp>	// Pascal unit
#include <FourSidesRVFrm.hpp>	// Pascal unit
#include <HypPropRVFrm.hpp>	// Pascal unit
#include <HypRVFrm.hpp>	// Pascal unit
#include <InsSymbolRVFrm.hpp>	// Pascal unit
#include <InsTableRVFrm.hpp>	// Pascal unit
#include <IntegerRVFrm.hpp>	// Pascal unit
#include <ItemPropRVFrm.hpp>	// Pascal unit
#include <ListGallery2RVFrm.hpp>	// Pascal unit
#include <ListGalleryRVFrm.hpp>	// Pascal unit
#include <ListRVFrm.hpp>	// Pascal unit
#include <MarkSearch.hpp>	// Pascal unit
#include <PageSetupRVFrm.hpp>	// Pascal unit
#include <ParaBrdrRVFrm.hpp>	// Pascal unit
#include <ParaListRVFrm.hpp>	// Pascal unit
#include <ParaRVFrm.hpp>	// Pascal unit
#include <PreviewRVFrm.hpp>	// Pascal unit
#include <rvActionsReg.hpp>	// Pascal unit
#include <RVADsgn.hpp>	// Pascal unit
#include <SpacingRVFrm.hpp>	// Pascal unit
#include <SplitRVFrm.hpp>	// Pascal unit
#include <TableBackRVFrm.hpp>	// Pascal unit
#include <TableBrdrRVFrm.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <RVALUTF8_Ukr.hpp>	// Pascal unit
#include <RVALUTF8_BLR.hpp>	// Pascal unit
#include <RVALUTF8_ChineseBig5.hpp>	// Pascal unit
#include <RVALUTF8_ChsZh.hpp>	// Pascal unit
#include <RVALUTF8_Czech.hpp>	// Pascal unit
#include <RVALUTF8_De.hpp>	// Pascal unit
#include <RVALUTF8_Dutch.hpp>	// Pascal unit
#include <RVALUTF8_EngUS.hpp>	// Pascal unit
#include <RVALUTF8_Esp.hpp>	// Pascal unit
#include <RVALUTF8_Farsi.hpp>	// Pascal unit
#include <RVALUTF8_Fr.hpp>	// Pascal unit
#include <RVALUTF8_Hun.hpp>	// Pascal unit
#include <RVALUTF8_Ita.hpp>	// Pascal unit
#include <RVALUTF8_Lith.hpp>	// Pascal unit
#include <RVALUTF8_NorBok.hpp>	// Pascal unit
#include <RVALUTF8_Polish.hpp>	// Pascal unit
#include <RVALUTF8_PtBr.hpp>	// Pascal unit
#include <RVALUTF8_Rom.hpp>	// Pascal unit
#include <RVALUTF8_Rus.hpp>	// Pascal unit
#include <RVALUTF8_Slovak.hpp>	// Pascal unit
#include <RVALUTF8_Sv.hpp>	// Pascal unit
#include <RVALUTF8_Turkish.hpp>	// Pascal unit
#include <ObjectAlignRVFrm.hpp>	// Pascal unit
#include <RVAPopupActionBar.hpp>	// Pascal unit
#include <RVARibbonUtils.hpp>	// Pascal unit
#include <RVColorTransform.hpp>	// Pascal unit
#include <RVALUTF8_Bul.hpp>	// Pascal unit
#include <RVALUTF8_Fin.hpp>	// Pascal unit
#include <TableSortRVFrm.hpp>	// Pascal unit
#include <TableSort.hpp>	// Pascal unit
#include <RVALUTF8_Hindi.hpp>	// Pascal unit
#include <StylesRVFrm.hpp>	// Pascal unit
#include <HypHoverPropRVFrm.hpp>	// Pascal unit
#include <InfoRVFrm.hpp>	// Pascal unit
#include <PasteSpecialRVFrm.hpp>	// Pascal unit
#include <RVALUTF8_Cat.hpp>	// Pascal unit
#include <RVALUTF8_PtPt.hpp>	// Pascal unit
#include <RVALUTF8_Thai.hpp>	// Pascal unit
#include <RVStyleFuncs.hpp>	// Pascal unit
#include <StyleImportRVFrm.hpp>	// Pascal unit
#include <RVALUTF8_Arm.hpp>	// Pascal unit
#include <RVALUTF8_Malay.hpp>	// Pascal unit
#include <RVGoToItem.hpp>	// Pascal unit
#include <ItemCaptionRVFrm.hpp>	// Pascal unit
#include <NumSeqCustomRVFrm.hpp>	// Pascal unit
#include <NumSeqRVFrm.hpp>	// Pascal unit
#include <RVAResourceLanguage.hpp>	// Pascal unit
#include <VisibleSidesRVFrm.hpp>	// Pascal unit
#include <RVALUTF8_Danish.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Internal.ExcUtils.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.VarUtils.hpp>	// Pascal unit
#include <System.Variants.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <System.TimeSpan.hpp>	// Pascal unit
#include <System.SyncObjs.hpp>	// Pascal unit
#include <System.Generics.Defaults.hpp>	// Pascal unit
#include <System.Rtti.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.DateUtils.hpp>	// Pascal unit
#include <System.IOUtils.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Actions.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <System.HelpIntfs.hpp>	// Pascal unit
#include <Winapi.UxTheme.hpp>	// Pascal unit
#include <Vcl.GraphUtil.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <System.Win.ComObj.hpp>	// Pascal unit
#include <Winapi.FlatSB.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVAnimate.hpp>	// Pascal unit
#include <RVRTF.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVThumbMaker.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVXMLRoutines.hpp>	// Pascal unit
#include <rvhtmlimport.hpp>	// Pascal unit
#include <TB2Version.hpp>	// Pascal unit
#include <TB2Common.hpp>	// Pascal unit
#include <TB2Anim.hpp>	// Pascal unit
#include <TB2Acc.hpp>	// Pascal unit
#include <TB2Item.hpp>	// Pascal unit
#include <TB2Hook.hpp>	// Pascal unit
#include <SpTBXDefaultSkins.hpp>	// Pascal unit
#include <SpTBXSkins.hpp>	// Pascal unit
#include <SpTBXItem.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <Vcl.CheckLst.hpp>	// Pascal unit
#include <IDEMessages.hpp>	// Pascal unit
#include <Vcl.CaptionedDockTree.hpp>	// Pascal unit
#include <Vcl.DockTabSet.hpp>	// Pascal unit
#include <PercentageDockTree.hpp>	// Pascal unit
#include <BrandingAPI.hpp>	// Pascal unit
#include <Winapi.Mapi.hpp>	// Pascal unit
#include <Vcl.ExtActns.hpp>	// Pascal unit
#include <Vcl.ActnMenus.hpp>	// Pascal unit
#include <Vcl.ActnMan.hpp>	// Pascal unit
#include <Vcl.PlatformDefaultStyleActnCtrls.hpp>	// Pascal unit
#include <BaseDock.hpp>	// Pascal unit
#include <DeskUtil.hpp>	// Pascal unit
#include <DeskForm.hpp>	// Pascal unit
#include <DockForm.hpp>	// Pascal unit
#include <Xml.Win.msxmldom.hpp>	// Pascal unit
#include <Xml.xmldom.hpp>	// Pascal unit
#include <ToolsAPI.hpp>	// Pascal unit
#include <Proxies.hpp>	// Pascal unit
#include <DesignEditors.hpp>	// Pascal unit
#include <Vcl.AxCtrls.hpp>	// Pascal unit
#include <Vcl.AppEvnts.hpp>	// Pascal unit
#include <TreeIntf.hpp>	// Pascal unit
#include <TopLevels.hpp>	// Pascal unit
#include <StFilSys.hpp>	// Pascal unit
#include <IDEHelp.hpp>	// Pascal unit
#include <ComponentDesigner.hpp>	// Pascal unit
#include <VCLEditors.hpp>	// Pascal unit
#include <ToolWnds.hpp>	// Pascal unit
#include <ColnEdit.hpp>	// Pascal unit
#include <RVDsgn.hpp>	// Pascal unit
#include <RVSEdit.hpp>	// Pascal unit
#include <Vcl.XPStyleActnCtrls.hpp>	// Pascal unit
#include <Vcl.RibbonLunaStyleActnCtrls.hpp>	// Pascal unit
#include <Vcl.RibbonActnCtrls.hpp>	// Pascal unit
#include <Vcl.RibbonActnMenus.hpp>	// Pascal unit
#include <Vcl.CustomizeDlg.hpp>	// Pascal unit
#include <Vcl.Ribbon.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Richviewactionsdxe6
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Richviewactionsdxe6 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RICHVIEWACTIONSDXE6)
using namespace Richviewactionsdxe6;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Richviewactionsdxe6HPP
