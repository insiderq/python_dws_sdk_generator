
{*******************************************************}
{                                                       }
{       RichViewActions (not included in the main set)  }
{       Actions for applying HTML-style lists           }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVAHTMLList;

interface
uses Windows, Classes, Controls, Forms, ActnList, 
     RichView, RVEdit, RVStyle, RVClasses,
     RichViewActions;

type
  TrvActionParaListHTML = class (TrvAction)
    public
      procedure ExecuteTarget(Target: TObject); override;
  end;

implementation
uses ListGallery2RVFrm;

{ TRVActionParaListHTML }

procedure TrvActionParaListHTML.ExecuteTarget(Target: TObject);
var frm: TfrmRVListGallery2;
    rve: TCustomRichViewEdit;
    ListStyle: TRVListInfo;
    Level, ListNo, StartFrom: Integer;
    UseStartFrom: Boolean;
    UsedTextStyles, UsedParaStyles, UsedListStyles: TRVIntegerList;
    i, Index: Integer;
begin
  frm := TfrmRVListGallery2.Create(Application);
  rve := GetControl(Target);
  frm.Init(rve);
  if frm.ShowModal=mrOk then begin
    frm.GetListStyle(ListStyle, ListNo, Level, StartFrom, UseStartFrom);
    if ListStyle=nil then
      rve.RemoveLists(False)
    else begin
      if not ListStyle.HasNumbering then begin
        if ListNo>=0 then
          Index := ListNo
        else
          Index := rve.Style.ListStyles.FindSuchStyle(ListStyle, True);
        rve.ApplyListStyle(Index, -1, 1, False, False);
        // UpdateBulletsAction(rve.Style, Index);
        end
      else begin
        if ListNo<0 then begin
          UsedTextStyles := TRVIntegerList.CreateEx(rve.Style.TextStyles.Count, 1);
          UsedParaStyles := TRVIntegerList.CreateEx(rve.Style.ParaStyles.Count, 1);
          UsedListStyles := TRVIntegerList.CreateEx(rve.Style.ListStyles.Count, 0);
          rve.RVData.MarkStylesInUse(UsedTextStyles, UsedParaStyles, UsedListStyles);
          UsedTextStyles.Free;
          UsedParaStyles.Free;
          Index := -1;
          for i := 0 to rve.Style.ListStyles.Count-1 do
            if (UsedListStyles[i]=0) and rve.Style.ListStyles[i].IsSimpleEqual(ListStyle, False) then begin
              Index := i;
              break;
            end;
          UsedListStyles.Free;
          if Index<0 then begin
            rve.Style.ListStyles.Add.Assign(ListStyle);
            Index := rve.Style.ListStyles.Count-1;
            rve.Style.ListStyles[Index].Standard := False;
          end;
          end
        else begin
          Index := ListNo;
        end;
        rve.ApplyListStyle(Index, -1, StartFrom, UseStartFrom, False);
        //if rve.Style.ListStyles[Index].AllNumbered then
        //  UpdateNumberingAction(rve.Style, Index);
      end;
    end;
  end;
  frm.Free;
end;

end.
