﻿// This file is a copy of RVAL_Dutch.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dutch (NL) translation                          }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Edwin Dirkzwager, 2004-08-25           }
{                dev@cipsoft.nl                         }
{ Updated by: Henk van Bokhorst 2005-03-11              }
{ Updated by: Henk van Bokhorst 2005-06-20              }
{ Updated by: Henk van Bokhorst 2005-09-21              }
{ Updated by: Pieter Zijlstra   2006-12-16              }
{ Updated by: Henk van Bokhorst 2007-01-10              }
{ Updated by: Henk van Bokhorst 2007-07-30              }
{ Updated by: Henk van Bokhorst 2009-01-14              }
{ Updated by: Henk van Bokhorst 2009-10-22              }
{ Updated by: Henk van Bokhorst 2010-06-07              }
{ Updated by: Henk van Bokhorst 2010-06-21              }
{ Updated by: Henk van Bokhorst 2010-12-03              }
{ Updated by: Henk van Bokhorst 2011-02-09              }
{ Updated by: Henk van Bokhorst 2011-08-15              }
{ Updated by: Henk van Bokhorst 2011-09-15              }
{ Updated by: Henk van Bokhorst 2012-04-24              }
{ Updated by: Henk van Bokhorst 2012-08-14              }
{ Updated by: Henk van Bokhorst 2012-08-29              }
{ Updated by: Henk van Bokhorst 2013-04-03              }
{ Updated by: Alconost 2014-04-28                       }
{*******************************************************}

unit RVALUTF8_Dutch;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'inch', 'cm', 'mm', 'pica', 'pixels', 'punt',
  // units : [n] inches, [n] cm, [n] mm, [n] pica, [n] pixels, [n] points 
  'in', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Bestand', 'Be&werken', 'Beel&d', 'Le&ttertype', '&Alinea', '&Invoegen', '&Tabel', '&Venster', '&Help',
  // exit
  '&Afsluiten',
  // top-level menus: View, Tools,
  '&Bekijken', '&Hulpmiddelen',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Grootte', 'Stijl', 'Selec&teer', '&Uitlijnen cel inhoud', 'C&elranden ',
  // menus: Table cell rotation
  'Cel &rotatie',
  // menus: Text flow, Footnotes/endnotes
  '&Tekst omloop', '&Voetnoten',
  // ribbon tabs: tab1, tab2, view, table
  '&Start', '&Geavanceerd', '&Beeld', '&Tabel',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Klembord', 'Lettertype', 'Alinea', 'Opsomming', 'Bewerken',
  // ribbon groups: insert, background, page setup,
  'Invoegen', 'Achtergrond', 'Pagina indeling',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Koppelingen', 'Voetnoten', 'Documentweergaven', 'Weergeven/verbergen', 'In-/uitzoomen', 'Opties',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Invoegen', 'Verwijderen', 'Opdrachten', 'Randen',
  // ribbon groups: styles 
  'Stijlen',
  // ribbon screen tip footer,
  'Druk op F1 voor meer informatie',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Onlangs geopende documenten', 'Een kopie van het document opslaan', 'Een voorbeeld van het document weergeven en afdrukken',
  // ribbon label: units combo
  'Eenheden:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nieuw', 'Nieuw|Opent een nieuw blanco document',
  // TrvActionOpen
  '&Openen...', 'Openen|Opent een document van schijf',
  // TrvActionSave
  '&Opslaan', 'Opslaan|Opslaan van document op schijf',
  // TrvActionSaveAs
  'Opslaan &als...', 'Opslaan als...|Opslaan van document met nieuwe naam',
  // TrvActionExport
  '&Exporteer...', 'Exporteer|Exporteer document naar een ander formaat',
  // TrvActionPrintPreview
  'A&fdrukvoorbeeld', 'Afdrukvoorbeeld|Toont document zoals het wordt afgedrukt',
  // TrvActionPrint
  '&Afdrukken...', 'Afdrukken|Afdrukken van het document',
  // TrvActionQuickPrint
  '&Afdrukken', '&Snel afdrukken', 'Afdrukken|Afdrukken van het document',
  // TrvActionPageSetup
  'Pa&gina instellingen...', 'Pagina instellingen|Instellen marges, papier grootte, afdruk-stand, bron, koptekst en voettekst',
  // TrvActionCut
  'K&nippen', 'Knippen|Knippen van selectie en plaatst dit op het klembord',
  // TrvActionCopy
  '&Kopiëren', 'Kopiëren|Kopiëren van selectie en plaatst dit op het klembord',
  // TrvActionPaste
  '&Plakken', 'Plakken|Plakt de inhoud van het klembord',
  // TrvActionPasteAsText
  'Plakken als &tekst', 'Plakken als tekst|Plakt tekst van het klembord',  
  // TrvActionPasteSpecial
  'Plakken &speciaal...', 'Plakken speciaal|Plakken van klembord inhoud in speciaal formaat',
  // TrvActionSelectAll
  'Alles selecteren', 'Alles selecteren|Selecteer het gehele document',
  // TrvActionUndo
  '&Ongedaan maken', 'Ongedaan maken|Maakt de laatste actie ongedaan',
  // TrvActionRedo
  '&Opnieuw uitvoeren', 'Opnieuw uitvoeren|Voert de laatste actie opnieuw uit',
  // TrvActionFind
  '&Zoek...', 'Zoek|Zoekt opgegeven tekst in het document',
  // TrvActionFindNext
  'Zoek &volgende', 'Zoek volgende|Zoekt volgende van opgegeven tekst',
  // TrvActionReplace
  'Ver&vang...', 'Vervang|Zoekt en vervangt opgegeven tekst in het document',
  // TrvActionInsertFile
  '&Bestand...', 'Bestand invoegen|Voegt inhoud van bestand in',
  // TrvActionInsertPicture
  '&Afbeelding...', 'Afbeelding invoegen|Invoegen van afbeelding van schijf',
  // TRVActionInsertHLine
  '&Horizontale lijn', 'Horizontale lijn invoegen|Voegt een horizontale lijn in',
  // TRVActionInsertHyperlink
  'Hyper&link...', 'Hperlink invoegen|Invoegen van een hypertext link',
  // TRVActionRemoveHyperlinks
  '&Verwijder hyperlinks', 'Verwijder hyperlinks|Verwijder alle hyperlinks in de geselecteerde tekst',  
  // TrvActionInsertSymbol
  '&Symbool...', 'Symbool invoegen|Voegt een symbool in',
  // TrvActionInsertNumber
  '&Getal ...', 'Getal invoegen|Voegt een getal in',
  // TrvActionInsertCaption
  '&Bijschrift invoegen...', 'Bijschrift invoegen|Voegt een bijschrift in voor het geselecteerde objectn',
  // TrvActionInsertTextBox
  '&Tekstvak', 'Tekstvak invoegen|Voegt een tekstvak in',
  // TrvActionInsertSidenote
  '&Kanttekening', 'Kanttekening invoegen|Voegt een kanttekening toe die weergegeven wordt in een tekstvak',
  // TrvActionInsertPageNumber
  '&Paginanummer', 'Paginanummer invoegen|Voegt een paginanummer in',
  // TrvActionParaList
  '&Opsommingstekens en nummering...', 'Opsommingstekens en nummering|Voegt opsommingstekens en nummering in',  
  // TrvActionParaBullets
  'O&psommingstekens', 'Opsommingstekens|Voegt toe of verwijdert opsommingstekens',
  // TrvActionParaNumbering
  '&Nummering', 'Nummering|Voegt toe of verwijdert opsommingstekens',
  // TrvActionColor
  'Achtergrond&kleur ...', 'Achtergrondkleur|Wijzigt de achtergrondkleur van het document',
  // TrvActionFillColor
  '&Vulkleur...', 'Vulkleur|Wijzigt de achtergrond kleur van tekst, alinea, cel, tabel of document',
  // TrvActionInsertPageBreak
  '&Pagina einde invoegen', 'Pagina einde invoegen|Voegt pagina einde in',
  // TrvActionRemovePageBreak
  'Pagina einde &verwijderen', 'Pagina einde &verwijderen|Verwijdert pagina einde',
  // TrvActionClearLeft
  'Verwijder tekst omloop aan &linkerzijde', 'Verwijder tekst omloop aan linkerzijde|Plaats deze alinea onder links uitgelijnde afbeeldingen',
  // TrvActionClearRight
  'Verwijder tekst omloop aan &rechterzijde', 'Verwijder tekst omloop aan rechterzijde|Plaats deze alinea onder rechts uitgelijnde afbeeldingen',
  // TrvActionClearBoth
  'Verwijder tekst omloop aan &beide zijden', 'Verwijder tekst omloop aan beide zijden|Plaats deze alinea onder links of rechts uitgelijnde afbeeldingen',
  // TrvActionClearNone
  '&Normale tekst omloop', 'Normale tekst omloop|Staat tekst omloop om links- en rechts uitgelijnde afbeeldingen toe',
  // TrvActionVAlign
  '&Object positie...', 'Object positie|Wijzig de plaats van het geselecteerde object',    
  // TrvActionItemProperties
  'Object &eigenschappen...', 'Objecteigenschappen|Eigenschappen van geselecteerd object',
  // TrvActionBackground
  '&Achtergrond...', 'Achtergrond|Selecteer achtergrondkleur en illustratie',
  // TrvActionParagraph
  '&Alinea...', 'Alinea|Wijzigt eigenschappen van alinea',
  // TrvActionIndentInc
  'Inspringen ver&groten', 'Inspringen vergroten|Vergroot inspringen in alinea',
  // TrvActionIndentDec
  'Inspringen ver&kleinen', 'Inspringen verkleinen|Verkleint inspringen in alinea',
  // TrvActionWordWrap
  'Woorden &afbreken', 'Woorden afbreken|Zet het afbreken van woorden aan of uit in alinea',
  // TrvActionAlignLeft
  '&Links uitlijnen', 'Links uitlijnen|Links uitlijnen van tekst',
  // TrvActionAlignRight
  '&Rechts uitlijnen', 'Rechts uitlijnen|Rechts uitlijnen van tekst',
  // TrvActionAlignCenter
  '&Centreren', 'Centreren|Centreert de tekst',
  // TrvActionAlignJustify
  '&Uitvullen', 'Uitvullen|Uitvullen van de tekst',
  // TrvActionParaColor
  '&Achtergrond kleur alinea...', 'Achtergrond kleur alinea|Instellen van alinea achtergrond kleur',
  // TrvActionLineSpacing100
  '&Enkele regelruimte', 'Enkele regelruimte|Instellen op enkele regelruimte',
  // TrvActionLineSpacing150
  '1.5 Re&gelruimte', '1.5 Regelruimte|Instellen regelruimte 1.5 regel',
  // TrvActionLineSpacing200
  '&Dubbele regelruimte', 'Dubbele regelruimte|Instellen regelruimte op twee regels',
  // TrvActionParaBorder
  '&Randen en achtergrond alinea...', 'Randen en achtergrond alinea|Instellen van alinea randen en achtergrond ',
  // TrvActionInsertTable
  '&Tabel invoegen...', '&Tabel', 'Tabel invoegen|Invoegen nieuwe tabel',
  // TrvActionTableInsertRowsAbove
  'Invoegen rij &boven', 'Invoegen rij boven|Invoegen van rij boven geselecteerde cel',
  // TrvActionTableInsertRowsBelow
  'Invoegen rij &onder', 'Invoegen rij onder|Invoegen van rij onder geselecteerde cel',
  // TrvActionTableInsertColLeft
  'Invoegen kolom &links', 'Invoegen kolom links|Invoegen van kolom links van geselecteerde cel',
  // TrvActionTableInsertColRight
  'Invoegen kolom &rechts', 'Invoegen kolom rechts|Invoegen van kolom rechts van geselecteerde cel',
  // TrvActionTableDeleteRows
  'Verwijder r&ijen', 'Verwijder rijen|Verwijder rij met geselecteerde cellen',
  // TrvActionTableDeleteCols
  'Verwijder &kolommen', 'Verwijder kolommen|Verwijder kolommen met geselecteerde cellen',
  // TrvActionTableDeleteTable
  '&Verwijder tabel', 'Verwijder tabel|Verwijdert geselecteerde tabel',
  // TrvActionTableMergeCells
  'Cellen &samenvoegen', 'Samenvoegen van cellen|Voegt geselecteerde cellen samen',
  // TrvActionTableSplitCells
  'Cellen s&plitsen...', 'Splitsen van cellen|Splitst de geselecteerde cel',
  // TrvActionTableSelectTable
  '&Selecteer tabel', 'Selecteer tabel|Selecteert de tabel',
  // TrvActionTableSelectRows
  'Selecteer r&ijen', 'Selecteer rijen|Seleteer rijen',
  // TrvActionTableSelectCols
  'Selecteer k&olommen', 'Selecteer kolommen|Selecteer kolommen',
  // TrvActionTableSelectCell
  'Selecteer c&el', 'Selecteer cel|Selecteer cel',
  // TrvActionTableCellVAlignTop
  'Celuitlijning &boven', 'Celuitlijning boven|Uitlijning van de celinhoud boven',
  // TrvActionTableCellVAlignMiddle
  'Celuitlijning &midden', 'Celuitlijning midden|Uitlijning van de celinhoud midden',
  // TrvActionTableCellVAlignBottom
  'Celuitlijning  &onder', 'Celuitlijning onder|Uitlijning van de celinhoud onder',
  // TrvActionTableCellVAlignDefault
  'Standaard &verticale celuitlijning', 'Standaard verticale celuitlijning|Instellen van standaard verticale celuitlijning',
  // TrvActionTableCellRotationNone
  '&Geen cel rotatie', 'Geen cel rotatie|Cel inhoud niet roteren',
  // TrvActionTableCellRotation90
  'Roteer cel &90°', 'Roteer cel 90°|Roteer cel inhoud 90°',
  // TrvActionTableCellRotation180
  'Roteer cel &180°', 'Roteer cel 180°|Roteer cel inhoud 180°',
  // TrvActionTableCellRotation270
  'Roteer cel &270°', 'Roteer cel 270°|Roteer cel inhoud 270°',
  // TrvActionTableProperties
  'Tabel &eigenschappen...', 'Tabeleigenschappen|Aanpassen van eigenschappen tabel',
  // TrvActionTableGrid
  'Toon &tabel lijnen', 'Toon tabel lijnen|Toont of verbergt tabel lijnen',
  // TRVActionTableSplit
  'Sp&lits tabel', 'Splits tabel|Splits de tabel in twee tabellen vanaf de geselecteerde rij',
  // TRVActionTableToText
  'Omzetten naar te&kst...', 'Omzetten naar tekst|Zet de tabel om naar tekst',
  // TRVActionTableSort
  '&Sorteren...', 'Sorteren|Sorteer de tabel rijen',
  // TrvActionTableCellLeftBorder
  'Rand &links', 'Rand links|Toont of verbergt linkerrand van cel',
  // TrvActionTableCellRightBorder
  'Rand &rechts', 'Rand rechts|Toont of verbergt rechterrand van cel',
  // TrvActionTableCellTopBorder
  'Rand &boven', 'Rand boven|Toont of verbergt bovenrand van cel',
  // TrvActionTableCellBottomBorder
  'Rand &onder', 'Rand onder|Toont of verbergt onderrand van cel',
  // TrvActionTableCellAllBorders
  '&Alle randen', 'Alle randen|Toont of verbergt alle randen van een cel',
  // TrvActionTableCellNoBorders
  '&Geen randen', 'Geen randen|Verbergt alle celranden',
  // TrvActionFonts & TrvActionFontEx
  '&Lettertype...', 'Lettertype|Past lettertype aan van geselecteerde tekst',
  // TrvActionFontBold
  '&Vet', 'Vet|Verandert de stijl van geselecteerde tekst naar vet',
  // TrvActionFontItalic
  '&Cursief', 'Cursief|Verandert de stijl van geselecteerde tekst naar cursief',
  // TrvActionFontUnderline
  '&Onderstrepen', 'Onderstrepen|Verandert de stijl van geselecteerde tekst naar onderstreept',
  // TrvActionFontStrikeout
  '&Doorhalen', 'Doorhalen|Verandert de stijl van geselecteerde tekst naar doorhalen',
  // TrvActionFontGrow
  '&Vergroot lettertype', 'Vergroot lettertype|Vergroot lettertype van tekst met 10%',
  // TrvActionFontShrink
  'Ver&klein lettertype', 'Verklein lettertype|Verklein lettertype van tekst met 10%',
  // TrvActionFontGrowOnePoint
  'Ver&groot letterype met 1 punt', 'Vergroot letterype met 1 punt|Vergroot lettertype van tekst met 1 punt',
  // TrvActionFontShrinkOnePoint
  'Verk&lein letterype met 1 punt', 'Verklein letterype met 1 punt|Verkleint lettertype van tekst met 1 punt',
  // TrvActionFontAllCaps
  'Alles &hoofdletters', 'Alles hoofdletters|Verander tekst in hoofdletters',
  // TrvActionFontOverline
  '&Bovenstreep', 'Bovenstreep|Plaatst een lijn boven geselecteerde tekst',
  // TrvActionFontColor
  'Tekst &kleur...', 'Tekst kleur|Past kleur aan van geselecteerde tekst',
  // TrvActionFontBackColor
  'Tekst &markeren...', 'Tekst markeren|Past achtergrond kleur aan van geselecteerde tekst',
  // TrvActionAddictSpell3
  '&Spelling', 'Spelling|Controleer de spelling',
  // TrvActionAddictThesaurus3
  '&Synoniemen', 'Synoniemen|Zoekt een synoniem voor het geselecteerde woord',
  // TrvActionParaLTR
  'Links naar rechts', 'Links naar rechts|Zet de tekstrichting van links naar rechts voor de geselecteerde alinea',
  // TrvActionParaRTL
  'Rechts naar links', 'Rechts naar links|Zet de tekstrichting van rechts naar links voor de geselecterde alinea',
  // TrvActionLTR
  'Links naar rechts tekst', 'Links naar rechts tekst|Zet de tekstrichting van links naar rechts voor de geselecteerde tekst',
  // TrvActionRTL
  'Rechts naar links tekst', 'Rechts naar links tekst|Zet de tekstrichting van rechts naar links voor de geselecteerde tekst',
  // TrvActionCharCase
  'Omkering hoofd- / kleine letters', 'Omkering hoofd- / kleine letters|Past lettergrootte aan van tekst',
  // TrvActionShowSpecialCharacters
  '&Onzichtbare tekens', 'Onzichtbare tekens|Toont onzichtbare tekens zoals tabs en spaties',
  // TrvActionSubscript
  'Su&bscript', 'Subscript|Zet de geselecteerde tekst om in subscript',
  // TrvActionSuperscript
  'Superscript', 'Superscript|Zet de geselecteerde tekst om in superscript',
  // TrvActionInsertFootnote
  '&Voetnoot', 'Voetnoot|Voegt een voetnoot toe',
  // TrvActionInsertEndnote
  '&Eindnoot', 'Eindnoot|Voegt een eindnoot toe',
  // TrvActionEditNote
  'Wijzig &noot', 'Wijzig noot|Wijzig de voetnoot of eindnoot',
  'Verbergen', 'Verbergen|Verbergt of toont het geselecteerde deel',            
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stijlen...', 'Stijlen|Opent de dialoog stijlbeheer',
  // TrvActionAddStyleTemplate
  'Stijl &toevoegen...', 'Stijl toevoegen|Maak een nieuwe tekst- of alinea stijl',
  // TrvActionClearFormat,
  '&Verwijder opmaak', 'Verwijder opmaak|Verwijdert alle tekst- en alinea opmaak van het geselecteerde deel',
  // TrvActionClearTextFormat,
  'Verwijder &tekst opmaak', 'Verwijder tekst opmaak|Verwijdert alle opmaak van de geselecteerde tekst',
  // TrvActionStyleInspector
  'Stijl &beheerder', 'Stijl beheerder|Toont of verbergt de Stijl beheerder',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'OK', 'Annuleren', 'Sluiten', 'Invoegen', '&Openen...', 'Op&slaan...', '&Wissen', 'Help', 'Verwijderen',
  // Others  -------------------------------------------------------------------
  // percents
  'procenten',
  // left, top, right, bottom sides
  'Linkerkant', 'Bovenkant', 'Rechterkant', 'Onderkant',
  // save changes? confirm title
  'Wijzigingen opslaan naar %s?', 'Bevestigen',
  // warning: losing formatting
  '%s heeft eigenschappen die niet te gebruiken zijn in het nieuwe formaat.'#13+
  'Wilt u het document toch opslaan?',
  // RVF format name
  'RichView formaat',
  // Error messages ------------------------------------------------------------
  'Fout',
  'Fout bij laden van het bestand.'#13#13'Mogelijke reden:'#13'- formaat van dit bestand is niet te gebruiken met deze applicatie;'#13+
  '- bestand is beschadigd;'#13'- bestand is in gebruik door andere applicatie.',
  'Fout bij laden van afbeelding.'#13#13'Mogelijke reden:'#13'- formaat van dit bestand is niet te gebruiken met deze applicatie;'#13+
  '- bestand bevat geen afbeelding;'#13+
  '- bestand is beschadigd;'#13'- bestand is in gebruik door andere applicatie.',
  'Fout bij opslaan bestand.'#13#13'Mogelijke reden:'#13'- geen ruimte meer op schijf;'#13+
  '- opslagmedia is beschermd tegen schrijven;'#13'- station bevat geen opslagmedia;'#13+
  '- bestand is in gebruik door andere applicatie;'#13'- opslagmedia is beschadigd.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView bestanden (*.rvf)|*.rvf',  'RTF bestanden (*.rtf)|*.rtf' , 'XML bestanden (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Tekst bestanden (*.txt)|*.txt', 'Tekst bestanden - Unicode (*.txt)|*.txt', 'Tekst bestanden - Detecteer (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML bestanden (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - vereenvoudigd (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word bestanden (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Klaar met zoekopdracht', 'Tekst ''%s'' niet gevonden.',
  // 1 string replaced; Several strings replaced
  '1 tekst vervangen.', '%d teksten vervangen.',
  // continue search from the beginning/end?
  'Einde van het document is bereikt; verdergaan vanaf begin document?',
  'Begin van het document is bereikt; verdergaan vanaf einde document?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparant', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Zwart', 'Bruin', 'Olijfgroen', 'Donkergroen', 'Smaragdgroen', 'Donkerblauw', 'Indigo', 'Grijs-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Donkerrood', 'Donkeroranje', 'Donkergeel', 'Groen', 'Groenblauw', 'Blauw', 'Blauwgrijs', 'Grijs-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Rood', 'Oranje', 'Lichtgroen', 'Zeegroen', 'Zeeblauw', 'Hemelsblauw', 'Violet', 'Grijs-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Grijs-25%
  'Paars', 'Goud', 'Geel', 'Heldergroen', 'Turkoois', 'Lichtblauw', 'Roodpaars', 'Grijs-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Huidskleur', 'Lichtgeel', 'Mint', 'Lichtturkoois', 'Pastelblauw', 'Lavendel', 'Wit',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparant', '&Auto', '&Meer kleuren...', '&Standaard',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Achtergrond', 'K&leur:', 'Positie', 'Achtergrond', 'Voorbeeld tekst.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Geen', '&Volledig venster', 'V&aste tegels', '&Tegels', 'C&entreren',
  // Padding button
  '&Marges...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Vulkleur', '&Toepassen op:', '&Meer kleuren...', '&Marges...',
  'Selecteer een kleur',
  // [apply to:] text, paragraph, table, cell
  'tekst', 'alinea' , 'tabel', 'cel',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Lettertype', 'Lettertype', 'Layout',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Lettertype:', '&Grootte', 'Stijl', '&Vet', '&Cursief',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Kleur:', '&Achtergrond:', '(Standaard)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effecten', 'Onderstrepen', '&Bovenstreep', 'D&oorhalen', '&Alles hoofdletters',
  // Sample, Sample text
  'Voorbeeld', 'Voorbeeld tekst',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Tussenruimte letter', '&Ruimte:', '&Uitgespreid', '&Vernauwd',
  // Offset group-box, Offset label, Down, Up,
  'Verticale offset', '&Offset:', '&Omlaag', '&Om&hoog',
  // Scaling group-box, Scaling
  'Schalen', 'Sch&aal:',
  // Sub/super script group box, Normal, Sub, Super
  'Subscript en superscript', '&Normaal', 'Su&bscript', 'Su&perscript',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Ruimte', '&Boven:', '&Links:', '&Onder:', '&Rechts:', '&Gelijke waarden',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Invoegen hyperlink', 'Hyperlink', 'T&ekst:', '&Doel', '<<selectie>>',
  // cannot open URL
  'Kan "%s" niet openen',
  // hyperlink properties button, hyperlink style
  '&Aanpassen...', '&Stijl:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Hyperlink eigenschappen', 'Standaard kleuren', 'Actieve kleuren',
  // Text [color], Background [color]
  '&Tekst:', '&Achtergrond',
  // Underline [color]
  '&Onderstreept:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ekst:', 'A&chtergrond',
  // Underline [color], Attributes group-box,
  'O&nderstreept:', 'Attributen',
  // Underline type: always, never, active (below the mouse)
  '&Onderstrepen:', 'altijd', 'nooit', 'actief',
  // Same as normal check-box
  'Als &standaard',
  // underline active links check-box
  '&Onderstreep actieve links',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Invoegen symbool', '&Lettertype:', '&Karakterset:', 'Unicode',
  // Unicode block
  '&Blok:',
  // Character Code, Character Unicode Code, No Character
  'Karakter code: %d', 'Karakter code: Unicode %d', '(geen karakter)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Invoegen tabel', 'Tabel grootte', 'Aantal &kolommen:', 'Aantal &rijen:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabel lay-out', '&Automatisch', 'Breedte &venster', '&Handmatig',
  // Remember check-box
  'Bewaar &grootte voor nieuwe tabellen',
  // VAlign Form ---------------------------------------------------------------
  'Object positie',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Objecteigenschappen', 'Afbeelding', 'Positie en grootte', 'Lijn', 'Tabel', 'Rijen', 'Cellen',
  // Image Appearance, Image Misc, Number tabs
  'Voorkomen', 'Overige', 'Getal',
  // Box position, Box size, Box appearance tabs
  'Positie', 'Grootte', 'Voorkomen',
  // Preview label, Transparency group-box, checkbox, label
  'Voorbeeld:', 'Transparantheid', '&Transparant', 'Transparant &kleur:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Aanpassen...', '&Opslaan...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Verticale uitlijning', '&Uitlijnen:',
  'Onderkant object met grondlijn tekst', 'Midden object met grondlijn tekst',
  'Bovenkant object met bovenkant tekst', 'Onderkant object met onderkant tekst', 'Midden object met midden tekst',
  // align to left side, align to right side
  'Aan linker zijde', 'Aan rechter zijde',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Verplaatsen met:', 'Uitrekken', '&Breedte:', '&Hoogte:', 'Standaard grootte: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  '&Proportioneel aanpassen', '&Resetten',
  // Inside the border, border, outside the border groupboxes
  'Binnen de rand', 'Rand', 'Buiten de rand',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Vulkleur:', '&Opvulling:',
  // Border width, Border color labels
  '&Randbreedte:', '&Randkleur:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontale tussenruimte:', '&Verticale tussenruimte:',
  // Miscellaneous groupbox, Tooltip label
  'Overige', '&Tooltip:',
  // web group-box, alt text
  'Web', 'Alternatieve &tekst:',  
  // Horz line group-box, color, width, style
  'Horizontale lijn', '&Kleur:', '&Breedte:', '&Stijl:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabel', '&Breedte:', '&Vulkleur:', '&Cel &ruimte...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizontale celopvulling:', '&Verticale celopvulling:', 'Rand en achtergrond',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Zichtbare &randen ...', 'Automatisch', 'automatisch',
  // Keep row content together checkbox
  '&Inhoud samenhouden',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotatie', '&Geen', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Rand:', 'Zichtbare r&anden ...',
  // Border, CellBorders buttons
  '&Tabelranden...', '&Celranden...',
  // Table border, Cell borders dialog titles
  'Tabelranden', 'Standaard Celranden',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Afdrukken', '&Houd tabel op 1 pagina', 'Aantal &titel rijen:',
  'Titels worden herhaald op nieuwe pagina',
  // top, center, bottom, default
  '&Boven', '&Centreren', '&Onder', '&Standaard',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Instellingen', '&Voorkeurbreedte:', '&Hoogte tenminste:', '&Vulkleur:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Rand', 'Randen zichtbaar:',  '&Schaduwkleur:', '&Lichte kleur', 'Kl&eur:',
  // Background image
  '&Afbeelding...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontale positie', 'Verticale positie', 'Positie in tekst',
  // Position types: align, absolute, relative
  'Uitlijnen', 'Absolute positie', 'Relatieve positie',
  // [Align] left side, center, right side
  'linkerzijde van vak met linkerzijde van',
  'midden van vak met midden van',
  'rechterzijde van vak met rechterzijde van',
  // [Align] top side, center, bottom side
  'bovenzijde van vak met bovenzijde van',
  'midden van vak met midden van',
  'onderzijde van vak met onderzijde van',
  // [Align] relative to
  'met',
  // [Position] to the right of the left side of
  'rechts van de linkerzijde van',
  // [Position] below the top side of
  'onder de bovenzijde van',
  // Anchors: page, main text area (x2)
  '&Pagina', '&Zone hoofdtekst', 'P&agina', 'Zone ho&ofdtekst',
  // Anchors: left margin, right margin
  '&Linkermarge', '&Rechtermarge',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Bovenste marge', '&Onderste marge', '&Binnemarge', '&Buitenmarge',
  // Anchors: character, line, paragraph
  '&Teken', 'L&ijn', 'Para&graaf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Gespiegelde marges', 'La&y-out in tabelcel',
  // Above text, below text
  '&Boven tekst', '&Onder tekst',
  // Width, Height groupboxes, Width, Height labels
  'Breedte', 'Hoogte', '&Breedte:', '&Hoogte:',
  // Auto height label, Auto height combobox item
  'Automatisch', 'automatisch',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Verticale uitlijning', '&Bovenaan', '&Midden', '&Onderaan',
  // Border and background button and title
  'R&and en achtergrond ...', 'Rand en achtergrond van vak',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Plakken speciaal', '&Plakken als:', 'Rich Text formaat', 'HTML formaat',
  'Tekst', 'Unicode tekst', 'Bitmap afbeelding', 'Metafile afbeelding',
  'Afbeeldingen',  'URL', 
  // Options group-box, styles
  'Opties', '&Stijlen:',
  // style options: apply target styles, use source styles, ignore styles
  'opmaak van het doel document bewaren', 'stijl en opmaak behouden',
  'opmaak behouden, stijl negeren',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Opsommingstekens en nummering', 'Opsomming', 'Nummering', 'Opsommingslijsten', 'Genummerde lijsten',
  // Customize, Reset, None
  '&Aanpassen...', '&Herstel', 'Geen',
  // Numbering: continue, reset to, create new
  'doorgaan met nummering', 'herstel nummering op', 'maak nieuwe lijst, begin vanaf',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Hoofdletters', 'Romeins (hoofdl.)', 'Nummers', 'Kleine letters', 'Romeins (klein)',
  // Bullet type
  'Cirkel', 'Stip', 'Vierkant',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Diepte:', '&Start vanaf:', '&Vervolgen', 'Nummering',    
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Aanpassen lijst', 'Niveaus', '&Telling:', 'Lijst eigenschappen', '&Lijst type:',
  // List types: bullet, image
  'opsomming', 'afbeelding', 'Invoegen nummer|',
  // Number format, Number, Start level from, Font button
  '&Formaat nummer:', 'Nummer', '&Start nummering vanaf:', '&Lettertype...',
  // Image button, bullet character, Bullet button,
  '&Afbeelding...', 'Opsomming &teken', '&Opsomming...',
  // Position of list text, bullet, number, image, text
  'Positie lijsttekst', 'Positie opsomming', 'Positie nummer', 'Positie afbeelding', 'Positie tekst',
  // at, left indent, first line indent, from left indent
  '&op:', 'L&inks inspringen:', '&Eerste regel inspringen:', 'vanaf inspringen links',
  // [only] one level preview, preview
  '&Voorbeeld 1 niveau', 'Voorbeeld',
  // Preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'links uitlijnen', 'rechts uitlijnen', 'centreren',
  // level #, this level (for combo-box)
  'Niveau %d', 'Dit niveau',
  // Marker character dialog title
  'Wijzig opsommingsteken',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Alinea rand en achtergrond', 'Rand', 'Achtergrond',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Instellingen', '&Kleur:', '&Breedte:', 'Int&erne breedte:', 'O&ffsets...',
  // Sample, Border type group-boxes;
  'Voorbeeld', 'Rand type',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Geen', '&Enkel', '&Dubbel', '&Drievoudig', 'B&innenkant dik', 'B&uitenkant dik',
  // Fill color group-box; More colors, padding buttons
  'Vulkleur', '&Meer kleuren...', '&Marges...',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // title and group-box in Offsets dialog
  'Marges', 'Marges tot rand',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Alinea', 'Uitlijning', '&Links', '&Rechts', '&Centreren', '&Uitvullen',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Afstand', '&Voor alinea:', '&Achter:', 'Regel &afstand:', '&Waarde:',
  // Indents group-box; indents: left, right, first line
  'Inspringen', 'L&inks:', 'Re&chts:', '&Eerste regel:',
  // indented, hanging
  '&Inspringen', '&Hangend', 'Voorbeeld',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Enkel', '1.5 regel', '2 regels', 'Minstens', 'Precies', 'Meerdere',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Inspringen en letterruimte', 'Tabs', 'Tekst omloop',
  // tab stop position label; buttons: set, delete, delete all
  '&Tab stop positie:', '&Zet', '&Verwijder', '&Alles verwijderen',
  // tab align group; left, right, center aligns,
  'Uitlijnen', '&Links', '&Rechts', '&Centreren',
  // leader radio-group; no leader
  'Leader', '(Geen)',
  // tab stops to be deleted, delete none, delete all labels
  'Tab-stops te verwijderen:', '(Geen)', 'Alles.',
  // Pagination group-box, keep with next, keep lines together
  'Pagina indeling', '&Koppel aan volgende', '&Houd lijnen bij elkaar',
  // Outline level, Body text, Level #
  '&Opmaak stijl:', 'Normale tekst', 'Stijl %d',    
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Afdrukvoorbeeld', 'Pagina breedte', 'Volledige pagina', 'Pagina :',
  // Invalid Scale, [page #] of #
  'Voer een nummer in tussen 10 en 500', 'van %d',
  // Hints on buttons: first, prior, next, last pages
  'Eerste pagina', 'Vorige pagina', 'Volgende pagina', 'Laatste pagina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Celruimte', 'Ruimte', 'Tussen &cellen', 'Tussen tabelrand en cellen',
  // vertical, horizontal (x2)
  '&Verticaal:', '&Horizontaal:', 'V&erticaal:', 'H&orizontaal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Randen', 'Instellingen', '&Kleur:', '&Lichte kleur:', 'Schaduw &kleur:',
  // Width; Border type group-box;
  '&Breedte:', 'Rand type',
  // Border types: none, sunken, raised, flat
  '&Geen', '&Verzonken', '&Opstaand', '&Plat',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Splitsen', 'Splits op',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Opgegeven rijen en kolommen', '&Originele cellen',
  // number of columns, rows, merge before
  'Aantal &kolommen:', 'Aantal &rijen:', '&Samenvoegen voor splitsen',
  // to original cols, rows check-boxes
  'Splits naar oorspronkelijke ko&lommen', 'Splits naar oorspronkelijke r&ijen',
  // Add Rows form -------------------------------------------------------------
  'Toevoegen rijen', '&Aantal rijen:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Pagina instelling', 'Pagina', 'Koptekst en voettekst',
  // margins group-box, left, right, top, bottom
  'Marges (millimeters)', 'Marges (inches)', '&Links:', '&Boven:', '&Rechts:', '&Onder:',
  // mirror margins check-box
  '&Spiegel marges',
  // orientation group-box, portrait, landscape
  'Afdrukstand', '&Staand', '&Liggend',
  // paper group-box, paper size, default paper source
  'Papier', '&Grootte:', '&Bron:',
  // header group-box, print on the first page, font button
  'Koptekst', '&Tekst:', '&Koptekst op eerste pagina', '&Lettertype...',
  // header alignments: left, center, right
  '&Links', '&Centreren', '&Rechts',
  // the same for footer (different hotkeys)
  'Voettekst', 'Te&kst:', 'Voettekst op eerste &pagina', 'L&ettertype...',
  'L&inks', 'Ce&ntreren', 'Re&chts',
  // page numbers group-box, start from
  'Paginanummering', '&Start vanaf:',
  // hint about codes
  'Speciale lettercombinaties:'#13'&&p - paginanummer; &&P - aantal pagina''s; &&d - huidige datum; &&t - huidige tijd.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Codering tekstbestand', '&Kies de bestandscodering:',
  // thai, japanese, chinese (simpl), korean
  'Thais', 'Japans', 'Chinees (vereenvoudigd)', 'Koreaans',
  // chinese (trad), central european, cyrillic, west european
  'Chinees (traditioneel)', 'Centraal- en Oost-Europees', 'Cyrillisch', 'West-Europees',
  // greek, turkish, hebrew, arabic
  'Grieks', 'Turks', 'Hebreeuws', 'Arabisch',
  // baltic, vietnamese, utf-8, utf-16
  'Baltisch', 'Vietnamees', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Visuele stijl', '&Kies stijl:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Converteer naar tekst', 'Kies &scheidingsteken:',
  // line break, tab, ';', ','
  'regel einde', 'tab', 'puntkomma', 'komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'Een tabel met samengevoegde rijen kan niet gesorteerd worden',
  // title, main options groupbox
  'Tabel sorteren', 'Sortering',
  // sort by column, case sensitive
  '&Sorteer op kolom:', '&Hoofdletter gevoelig',
  // heading row, range or rows
  'Kop rij uitsluiten', 'Te sorteren rijen: van %d tot %d',
  // order, ascending, descending
  'Volgorde', '&Oplopend', '&Aflopend',
  // data type, text, number
  'Soort', '&Tekst', '&Nummer',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Nummer invoegen', 'Eigenschappen', '&Naam teller:', '&Type nummering:',
  // numbering groupbox, continue, start from
  'Nummering', 'D&oorgaan', '&Beginnen vanaf:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Bijschrift', '&Label:', '&Label weglaten uit bijschrift',
  // position radiogroup
  'Positie', '&Boven geselecteerd object', '&Onder geselecteerd object',
  // caption text
  'T&ekst bijschrift:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Nummering', 'Figuur', 'Tabel',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Zichtbare randen', 'Rand',
  // Left, Top, Right, Bottom checkboxes
  '&Linkerzijde', '&Bovenzijde', '&Rechterzijde', '&Onderzijde',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Aanpassen tabel kolom', 'Aanpassen tabel rij',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Inspringen eerste regel', 'Links inspringen', 'Hangend inspringen', 'Rechts inspringen',
  // Hints on lists: up one level (left), down one level (right)
  'Eén niveau hoger', 'Eén niveau lager',
  // Hints for margins: bottom, left, right and top
  'Ondermarge', 'Linkermarge', 'Rechtermarge', 'Bovenmarge',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tab links uitgelijnd', 'Tab rechts uitgelijnd', 'Tab gecentreerd', 'Tab uitgelijnd op decimaalteken',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Standaard', 'Standaard inspringen', 'Geen afstand', 'Kop %d', 'Lijst alinea',
  // Hyperlink, Title, Subtitle
  'Hyperlink', 'Titel', 'Subtitel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Benadrukt', 'Subtiele benadrukking', 'Intensieve benadrukking', 'Zwaar',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citaat', 'Duidelijk citaat', 'Subtiele verwijzing', 'Intensieve verwijzing',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blok bekst', 'HTML variabele', 'HTML code', 'HTML acroniem',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML definitie', 'HTML toetsenbord', 'HTML voorbeeld', 'HTML typemachine',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML geformatteerd', 'HTML Citaat', 'Kop', 'Voet', 'Pagina nummer',
  // Caption
  'Bijschrift',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Eindnoot verwijzing', 'Voetnoot verwijzing', 'Eindnoot tekst', 'Voetnoot tekst',
  // Sidenote Reference, Sidenote Text
  'Referentie kanttekening', 'Tekst kanttekening',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'kleur', 'achtergrond kleur', 'transparant', 'standaard', 'onderstreep kleur',
  // default background color, default text color, [color] same as text
  'standaard achtergrond kleur', 'standaard tekst kleur', 'zelfde als tekst',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'enkel', 'vet', 'dubbel', 'gestippeld', 'vet gestippeld', 'gestreept',
  // underline types: thick dashed, long dashed, thick long dashed,
  'vet gestreept', 'lang gestreept', 'vet lang gestreept',
  // underline types: dash dotted, thick dash dotted,
  'streep stip', 'vet streep stip',
  // underline types: dash dot dotted, thick dash dot dotted
  'streep stip stip', 'vet streep stip stip',
  // sub/superscript: not, subsript, superscript
  'geen sub/superscript', 'subscript', 'superscript',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'schrijfrichting:', 'geërfd', 'links naar rechts', 'rechts naar links',
  // bold, not bold
  'vet', 'niet vet',
  // italic, not italic
  'cursief', 'niet cursief',
  // underlined, not underlined, default underline
  'onderstreept', 'niet onderstreept', 'standaard onderstrepen',
  // struck out, not struck out
  'doorgehaald', 'niet doorgehaald',
  // overlined, not overlined
  'bovenstreep', 'geen bovenstreep',
  // all capitals: yes, all capitals: no
  'alles hoofdletters', 'alles hoofdletters uit',
  // vertical shift: none, by x% up, by x% down
  'geen verticale lift', 'omhoog met %d%%', 'naar beneden met %d%%',
  // characters width [: x%]
  'tekenbreedte',
  // character spacing: none, expanded by x, condensed by x
  'normale spatiëring', 'spatiëring vergroot met %s', 'spatiëring verkleind met %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'standaard lettertype', 'standaard alinea', 'geërfd',
  // [hyperlink] highlight, default hyperlink attributes
  'oplichten', 'standaard',
  // paragraph aligmnment: left, right, center, justify
  'links uitgelijnd', 'rechts uitgelijnd', 'gecentreerd', 'uitgevuld',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'regel hoogte: %d%%', 'ruimte tussen regels: %s', 'regel hoogte: tenminste %s',
  'regel hoogte: exact %s',
  // no wrap, wrap
  'regels niet afbreken', 'regels afbreken',
  // keep lines together: yes, no
  'regels bij elkaar houden', 'regels niet bij elkaar houden',
  // keep with next: yes, no
  'bij volgende alinea houden', 'niet bij volgende alinea houden',
  // read only: yes, no
  'alleen lezen', 'wijzigbaar',
  // body text, heading level x
  'niveau: normale tekst', 'kop diepte: %d',
  // indents: first line, left, right
  'eerste regel inspringen', 'links inspringen', 'rechts inspringen',
  // space before, after
  'ruimte voor', 'ruimte na',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabs', 'geen', 'uitlijnen', 'links', 'rechts', 'centreren',
  // tab leader (filling character)
  'vullen met',
  // no, yes
  'nee', 'ja',
  // [padding/spacing/side:] left, top, right, bottom
  'links', 'boven', 'rechts', 'onder',
  // border: none, single, double, triple, thick inside, thick outside
  'geen', 'enkel', 'dubbel', 'drievoudig', 'vet binnen', 'vet buiten',
  // background, border, padding, spacing [between text and border]
  'achtergrond', 'rand', 'marges', 'ruimte',
  // border: style, width, internal width, visible sides
  'stijl', 'breedte', 'breedte binnen', 'zichtbare kanten',
  // style inspector -----------------------------------------------------------
  // title
  'Stijl beheerder',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stijl', '(Geen)', 'Alinea', 'Lettertype', 'Attributen',
  // border and background, hyperlink, standard [style]
  'Rand en achtergrond', 'Hyperlink', '(Standaard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stijlen', 'Stijl',
  // name, applicable to,
  '&Naam:', 'Toepasbaar &op:',
  // based on, based on (no &),
  '&Gebaseerd op:', 'Gebaseerd op:',
  //  next style, next style (no &)
  'Stijl voor &volgende alinea:', 'Stijl voor volgende alinea:',
  // quick access check-box, description and preview
  '&Snelle toegang', 'Beschrijving en &voorbeeld:',
  // [applicable to:] paragraph and text, paragraph, text
  'alinea en tekst', 'alinea', 'tekst',
  // text and paragraph styles, default font
  'Tekst en alinea stijlen', 'Standaard lettertype',
  // links: edit, reset, buttons: add, delete
  'Bewerken', 'Resetten', '&Toevoegen', '&Verwijderen',
  // add standard style, add custom style, default style name
  '&Standaard stijl toevoegen...', '&Aangepaste stijl toevoegen', 'Stijl %d',
  // choose style
  'Kies &stijl:',
  // import, export,
  '&Importeren...', '&Exporteren...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView stijlen (*.rvst)|*.rvst', 'Fout bij laden bestand', 'Dit bestand bevat geen stijlen',
  // Title, group-box, import styles
  'Importeer stijlen uit bestand', 'Importeren', 'I&mporteer stijlen:',
  // existing styles radio-group: Title, override, auto-rename
  'Bestaande stijlen', '&vervangen', '&hernoemen',
  // Select, Unselect, Invert,
  '&Selecteren', '&Deselecteren', '&Inverse',
  // select/unselect: all styles, new styles, existing styles
  '&Alle stijlen', '&Nieuwe stijlen', '&Bestaande stijlen',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Verwijder opmaak', 'Alle stijlen',
  // dialog title, prompt
  'Stijlen', '&Kies toe te passen stijl:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Synoniemen', '&Alles negeren', '&Toevoegen aan woordenboek',
  // Progress messages ---------------------------------------------------------
  'Downloaden %s', 'Afdrukken voorbereiden...', 'Afdrukken pagina %d',
  'Converteren van RTF...',  'Converteren naar RTF...',
  'Lezen %s...', 'Schrijven %s...', 'tekst',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Geen notitie', 'Voetnoot', 'Eindnoot', 'Kanttekening', 'Tekstvak'
  );


initialization
  RVA_RegisterLanguage(
    'Dutch (NL)', // english language name, do not translate
    'Nederlands (NL)', // native language name
    ANSI_CHARSET, @Messages);

end.
