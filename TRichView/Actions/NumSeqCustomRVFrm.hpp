﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'NumSeqCustomRVFrm.pas' rev: 27.00 (Windows)

#ifndef NumseqcustomrvfrmHPP
#define NumseqcustomrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Numseqcustomrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVCustomNumSeq;
class PASCALIMPLEMENTATION TfrmRVCustomNumSeq : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TGroupBox* gbSeq;
	Vcl::Stdctrls::TLabel* lblSeqName;
	Vcl::Stdctrls::TLabel* lblSeqType;
	Vcl::Stdctrls::TComboBox* cmbSeqName;
	Vcl::Stdctrls::TComboBox* cmbSeqType;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall cmbSeqNameClick(System::TObject* Sender);
	void __fastcall cmbSeqNameChange(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _cmbSeqName;
	Vcl::Controls::TControl* _cmbSeqType;
	Vcl::Controls::TControl* _btnOk;
	
public:
	DYNAMIC void __fastcall Localize(void);
	void __fastcall Init(System::Classes::TStringList* SeqPropList, const System::UnicodeString SeqName, Rvstyle::TRVSeqType SeqType);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	void __fastcall GetSeqProperties(System::UnicodeString &SeqName, Rvstyle::TRVSeqType &SeqType);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVCustomNumSeq(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVCustomNumSeq(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVCustomNumSeq(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVCustomNumSeq(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Numseqcustomrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_NUMSEQCUSTOMRVFRM)
using namespace Numseqcustomrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// NumseqcustomrvfrmHPP
