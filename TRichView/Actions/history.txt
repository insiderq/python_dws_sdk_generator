Completed translations: Belorussian, Catalan, Chinese Simplified, Chinese Traditional, Danish, Dutch, Farsi, Finnish, French, Hindi, Italian, Lithuanian, German, Hungarian, Polish, Portuguese (Br) Romanian, Russian, Spanish, Swedish, Turkish 

v4.8.2

v4.8
packages for Delphi and C++Builder XE6

v4.7
new properties:
  TrvActionItemProperties.ActionInsertHLine, ActionInsertTable;   TrvActionTableProperties.ActionInsertTable;   TrvActionInsertTable.VisibleBorders, BackgroundPicture, BackgroundStyle. ModifiedTemplates moved to TrvActionParaList; 
new and updated translations

v4.6
new actions: 
- TrvActionInsertFootnote
- TrvActionInsertEndnote
- TrvActionInsertSidenote
- TrvActionInsertTextBox
- TrvActionEditNote
- TrvActionInsertNumber
- TrvActionInsertCaption
impr: TrvActionProperties can edit properties of sidenotes, text boxes, numbers
impr: improved pages for image and table in TrvActionProperties 
impr: new "Resource language" translation
impr: languages have English and native names specified

v4.5.5
Requires TRichView 14.11.3

v4.5.3
Requires TRichView 14.10

v4.5
packages for Delphi and C++Builder XE5

v4.4
Requires TRichView 14.8
Support of font-size in half-points in TRVFontSizeComboBox and TrvActionFontEx

v4.3.1

v4.3
Requires TRichView 14.7
Impr: changes made by TrvActionColor, TrvActionBackground can be undone

v4.2.4
Requires TRichView 14.6
TrvActionPageSetup uses TRVPrint.Margins 

v4.2
packages for Delphi and C++Builder XE4

v4.1.2
compatibility with TRichView 14.4

v4.1
new translation: Armenian
new: TrvActionExport can export DocX files
fix: applying spelling suggestions from TRVAPopupMenu and hotkeys
fix: applying hyperlinks to label items;

v4.0.2
readme.rvf for ActionTest demo uses styles; sample styles in "Styles" folder

v3.10.4
v3.10.2
requires TRichView 13.13.2
new: packages for RAD Studio XE3;
impr: TrvActionStyleInspector, TrvActionClearFormat, TrvActionClearTextFormat can work even if UseStyleTemplates=False

v3.10
requires TRichView 13.13
chg: TRVAControlPanel.TableGridStyle property is removed.
chg: TRVAControlPanel is rewritten. Previously, its properties provided access to global variables. Now, values of properties are stored inside the component; actions and components are linked to a control panel (either implicitly or explicitly).  Implicit linking: If an application does not contain TRVAControlPanel on a form, a default control panel is used (accessible as MainRVAControlPanel). If an application contains a single TRVControlPanel on a form, it becomes the main control panel automatically (assigned to MainRVAControlPanel). Another control panel can be made a main one if you call its Activate method. Explicit linking: All actions and RichViewActions components have ControlPanel property; many methods have ControlPanel as an optional parameters. So now you can have several forms with completely independent settings.
new:  TRVAControlPanel.Header and .Footer properties (RVA_HeaderInfo and RVA_FooterInfo functions still exist for a backward compatibility)
chg: the type of actions� Control and TRVAControlPanel.DefaultControl properties was changed to TCustomRVControl. You can assign not only TRichViewEdit, but also TSRichViewEdit.
new:  TRVFontComboBox, TRVFontSizeComboBox, TRVFontCharsetComboBox, TRVStyleTemplateComboBox, TRVStyleTemplateListBox components can be linked to TRichViewEdit or TSRichViewEdit using Editor property.  When linked, they work with this editor automatically, without any additional code (for TRVFontComboBox, TRVFontSizeComboBox, TRVFontCharsetComboBox, you need to assign ActionFont  property as well).
new:  the same for TrvActionStyleInspector. It is linked to TRichViewEdit or TSRichViewEdit using Control property.
impr: TRVStyleTemplateComboBox, TRVStyleTemplateListBox support Delphi XE2+ visual styles.

v3.9.3

v3.9
new: TRVStyleTemplateComboBox and TRVStyleTemplateListBox components.

v3.8
Requires TRichView 13.12+
new: style import
new: new properties of TRVAControlPanel: RVFStylesFilter and RVFStylesExt. They are used only if RVFLocalizable property = False), they allow changing name and extension of styles containing RichView styles
impr: in TrvActionStyleInspector:
  - OnShowing event allowing to define coordinates of its window before it is shown;
  - Images, FontImageIndex and ParaImageIndex property allowing to display 
icons in its window;
impr: QuickAccess property can be defined in the style dialog;
impr: all new features are localized
impr: (minimal) support of styles when inserting HTML ('Hyperlink' is assigned to read hyperlinks)
impr: in the print preview form, mouse wheel changes pages, ctrl+mouse wheel zooms in/out
fix: minor fixes related to styles


v3.7
Requires TRichView 13.11+
new translations: Catalan, Portuguese (Portuguese), Thai

new: actions and properties related to real styles.
Actions:
- TrvActionStyleTemplates
- TrvActionAddStyleTemplate
- TrvActionClearFormat
- TrvActionClearTextFormat
- TrvActionStyleInspector
Properties:
- TrvActionNew.StyleTemplates
- TrvActionInsertHyperlink.StyleTemplateName, AutoAddHyperlinkStyleTemplate
Features in UseStyleTemplates mode:
- TrvActionPasteSpecial displays option for applying styles when pasting RTF or RVF
- TrvActionInsertHyperlink does not display "Customize" button, but displays a list of text style-templates to apply; by default, TrvActionInsertHyperlink.StyleTemplateName is selected (or, if not found, 'Hyperlink' is selected). If neither StyleTemplateName nor 'Hyperlink' is found, and AutoAddHyperlinkStyleTemplate=True, 'Hyperlink' style-template is added and selected.

New features are not localized yet.

impr: TrvActionFontEx allows changing text background color (new property: BackColor: TColor)
impr: TrvActionInsertHyperlinks allows defining more properties for hyperlinks: UnderlineColor, HoverUnderlineColor, HoverEffects (note: they are used only if StyleTemplates are not used)


v3.6.1

v3.6
Requires TRichView 13.10+
new messages to display in OnProgress, OnSendingToPrinter, OnDownload events
TrvActionPasteSpecial takes TRichViewEdit.AcceptPasteFormats into account

v3.5.2
Compatibility with TRichView 13.9

v3.5.1
new: Hindi translation (beta version). It is available in non-Unicode versions of Delphi only if TNT controls are used.
impr: when TNT controls are used, RichViewActions components (office radio buttons, combo boxes, color controls) support Unicode; RichViewActions' custom drawing code supports Unicode; Unicode file dialogs are used.
Compatibility issue: if you use TNT controls, now you must not use _GetWideString when assigning the result of RVA_GetS to Unicode caption or hint, see changes in Unit3Tnt, TForm3.Localize

v3.5
new: TrvActionPasteSpecial can paste URL

v3.4.7
new: TRVAControlPanel.OnGetActionControlCoords event

Complete translations:
Belorussian, Bulgarian, Chinese Simplified and Traditional, Dutch, German, Hungarian, Farsi, Finnish, Italian, Lithuanian, Norwegian, Polish, Slovak, Spanish, Portuguese, Russian, Swedish, Turkish, Ukrainian.
Incomplete: Czech, Romanian.

v3.3.1
New packages - for Delphi XE2
Support of visual styles of XE2. Support for inverted (light text on dark background) color schemes.
New actions: TrvActionTableSort, TrvActionTableToText, TrvActionTableSplit.
File-related actions can display a dialog for choosing encoding of text files.

v3.1
New actions for rotating table cells.
New translations: Bulgarian and Finnish.

v3.0
Improved support for RVXML (loading and saving header and footer).
Using "Lorem ipsum" as a sample text (can be turned off by undefining USELOREMIPSUM in RichViewActions.inc)

v2.4
Different units of measurement in dialogs (TRVAControlPanel.UnitsDisplay, PixelBorders).
Possibility to measure properties of several actions in twips instead of pixels (TRVAControlPanel.UnitsProgram).
Possibility to exclude languages from exe-file using compiler conditional defines, see RichViewActions.inc.
New component icons.

v2.3.3
v2.3.2
New: new images for projects for Delphi 2009/C++Builder 2009 and newer.

v2.3
New TrvActionHide hides/shows the selected fragment.
TrvActionShowSpecialCharacters switches visibility of hidden text as well.

v2.2
Ability to download images using CleverComponents.
Packages for RAD Studio XE.

v2.1
New TRVAControlPanel.DialogFontSize property.

v2.0
Changes to support TRibbon control (D2009+)
New demo with ribbon (in RIBBON subfolder).
TRVAPopupMenu can support both TActionList and TActionManager.
New TRVAPopupActionBar component.

v1.77
New skin for the ruler. A new property SkinType is added in the ruler (possible values: stNoSkin, stSkin1 ��� stSkin2).
Property SkinEnabled is removed.
New property TRVAControlPanel.DefaultDocParameters: TRVDocParameters is added. It defines properties for new documents.
  Especially useful for ScaleRichView.
File name changes:
AdjustBmp.pas (added in v1.76) is renamed to RVColorTransform.pas.
RulerSkin.res (added in v1.76) is renamed to RulerSkins.res

v1.76
A "skin" mode in the ruler (implemented by Eugene Korolyov aka Rivarez <rivarez@list.ru>)
This is not a real skins, because only colors can be customized.
This mode is activated when assigning SkinEnabled=True.
Skin colors defined by same colors that used for non skinable mode.
A new color property is added: IndentColor (background color of handles for changing margins)
New MarginSaturation, IndentSaturation and RulerSaturation properties for changing color saturation.

v1.75.1

v1.74.1 (required version of TRichView: 11.3.1)

v1.74 (required version of TRichView: 11.3)
new: actions  TrvActionPasteAsText, TrvActionVAlign, TrvActionRemoveHyperlinks
impr: TrvActionParagraph supports paragraph outline levels (new OutlineLevel property)
impr: TrvActionItemProperties supports styles of "breaks"
impr: pasting HTML with rvHtmlViewImporter


v1.73 (required version of TRichView: 11.2)
new: TrvActionClearLeft, TrvActionClearRight, TrvActionClearBoth, TrvActionClearNone actions for calling RichViewEdit.ClearTextFlow
new: left and right alignment options for pictures (TrvActionItemProperties, tab "Layout", combobox "Align")


v1.72
new: packages for Delphi and C++Builder 2010

v1.71.3
impr: supporting new glyfx images (see www.glyfx.com).
 In additon to XP-style images, 3 new datamodules are available: XP-style with alpha channel (semitransparency), 
 Vista-style with and without alpha channel. Alpha channel is supported in image lists since Delphi 2009.
 Datamodules are available for customers who ordered the proper image sets from GlyFX (word processing and core 2 sets,
 xp or vista style correspondingly)

v1.71.2
impr: the New and Open actions update (clear, reset properties, format) not only the main editor, but also richviews linked via RTFReadProperties.SetHeader and SetFooter methods.

v1.71
new: TrvActionInsertPicture.Spacing property. Defines spacing around inserted pictures.
  Default value - 0 (note that the default value of the picture item property is 1, so
  all previous pictures were inserted with 1 pixel spacing)
impr: in TrvActionInsertHyperlink.DetectURL

v1.70
new: TrvActionInsertFile.InsertFile method

v1.69.2
Addict4 support

v1.69
new TrvActionInsertSymbol.UseCurrentFont property.
fix in TrvActionInsertSymbol.AlwaysInsertUnicode. This property was made True by default for Delphi 2009+.
fix in TRVAControlPanel initialization
fix in TrvActionPasteSpecial

v1.68
Help file is added.
New TrvActionInsertPicture.MaxImageSize property.
New RvHtmlHelp function for opening CHM file.
Fixes.

v1.67
Compatibility with TRichView 10.13.
New: CustomPaste action can paste graphic files (+new string to translate).
  It also has new event OnCanPaste, to provide better support of custom pasting, see the comments to v1.62

v1.66.1
RAD Studio 2009 support.
New packages:
RichViewActionsD2009.dproj (for Delphi 2009)
RichViewActionsCB2009.cbproj (for C++Builder 2009)
New demo projects:
ActionTestD2009.dproj (for Delphi 2009)
ActionTestCB2009.cbproj (for C++Builder 2009)
While the demo project for Delphi 2009 uses the same unit as the previous
versions of Delphi (Unit3.pas), the demo project for C++Builder 2009 uses
a new unit (UnitUnicode1.cpp instead of Unit1.cpp)
Requires TRichView 10.11+

v1.65
New: new line spacing types are supported.
Requires TRichView 10.4+

v1.64
New: TRVAControlPanel.UserInterface property. Possible values:
  * rvauiFull - no restrictions;
  * rvauiHTML - only features compatible with HTML are available in user
  interface. The most noticeable change - different bullets&numbering
  dialog is used.
New: support for THTMLViewer from www.pbear.com. If enabled, HTML format is added in Open and SaveAs dialogs. See install.txt how to activate it.
After enabling, place TRVHtmlViewImporter and (hidden) THtmlViewer components on the form, assign RVHtmlViewImporter1.HTMLViewer := HtmlViewer1, RVAControlPanel1.RVHtmlViewImporter := RVHtmlViewImporter1.
Properties for HTML saving are added to TrvActionSave (FileTitle, ImagePrefix, SaveOptions, CreateDirectoryForHTMLImages)


v1.62
  New: you can add custom formats in TrvActionPasteSpecial action.
  Before displaying the paste-special dialog, OnShowing event occurs.
  In this event, you can use AddFormat method to add new items in this dialog.
  If such item is chosen by the user, OnCustomPaste event occurs. See also v1.67

v1.60
  New: TRVASPTBXPopupMenu - SpTBXLib's TRVAPopupMenu (see http://club.telepolis.com/silverpointdev/sptbxlib/)
    To enable it, remove the dot from the line {.$DEFINE USESPTBX} in RichViewActions.inc
    and recompile RichViewActions packages (implemented by Denis Vygovskiy).
  Impr: When activating Find or Replace dialogs, if a text without line breaks is selected, it is assigned as FindText.

v1.59
  Fix: downloading images with Indy failed if URL contained special characters (such as '?')
  Impr: live spelling with Addic3: if custom dictionary is not selected, "Add to Dictionary" command is
    disabled in the context menu,
  new: StoreImageFileName property for TrvActionItemProperties and TrvActionTableProperties.
    If True, rvespImageFileName extra string property is assigned when changing pictures
    with these actions.

v1.58
  Support for TNT Controls (see install.txt). Better support for ThemeEngine.
  Improvements in font list box and office radiobuttons.

v1.57
  Minimal required version of TRichView: 1.9.46
  Support for custom underlines.
  ThemeEngine compatibility is restored.
  translate.txt updated.

v1.56
  Support for new Addict3 thesaurus languages (Addict 3.5 is required)
  Fix: rvActionInsertTable1.ShowTableSizeDialog
  Impr: When registering RichViewActions in Delphi, its version is written in the Registry

v1.55.1
  TrvActionNew resets DocParameters and clears DocProperties, if rvfoSaveDocProperties is included
  in RichViewEdit.RVFOptions. TrvActionOpen does the same before opening documents.
  You can override defaults in OnNew event of these actions.

v1.55
  New images for component palette in D2005-D2007
  
v1.54
  Minimal required version of TRichView: 1.9.38
  new: actions TrvActionSubscript, TrvActionSuperscript.
  impr: TrvActionFontEx supports subscript and superscript
  impr: ruler understands CellHPadding and CellVPadding.

v1.53
  new: new version of ruler; not completely localized;
  new: TrvActionPrint can print selection;
  impr: TrvActionInsertHyperlink.GoToLink understands links to checkpoints (started from '#' character)
  chg: If TrvActionInsertPicture.StoreFileName=True, file name of the inserted picture is stored
   in rvespImageFileName item property, not in the item name as before.
  new: TrvActionInsertPicture.StoreFileNameInItemName property works like StoreFileName worked before.

v1.52.1
  impr: "document may contain features that are not compatible with the chosen saving format"
    warning is not shown more than once for the same document.
  impr: custom file format can be used as default format (for new documents).
    Format for new documents is defined in two properties of TRVAControlPanel: 
    DefaultFileFormat and DefaultCustomFilterIndex (new, used if DefaultFileFormat=ffeCustom)
    See "Files: working with custom formats" in readme.rvf, and examples.txt for 
    the information how to use custom formats.
v1.52
  new: TrvActionItemProperties.OnCustomItemPropertiesDialog event allows
    displaying your own item properties dialogs.
  chg: No more runtime packages. Please uninstall and delete old runtime packages before
   installing.

v1.51.1
  impr: TRuler is updated

v1.51
  impr: InsertSymbol dialog is redesigned (fix: not enough space for labels in some localizations)
  new: packages for Borland Developer Studio 2006

v1.50
  impr: InsertSymbol dialog has a combobox allowing to filter Unicode characters
    (to show only the selected block of characters).
    This feature may be turned off by assigning rvActionInsertSymbol.DisplayUnicodeBlocks=False.
    Names of Unicode blocks are not translated and probably will not be localizable.
    One new text string is not translated (label next to unicode blocks combobox)

v1.49
  fix: critical fix in TrvActionInsertHyperlink
  impr: in English translation
  impr: font dialog preview shows Unicode characters
  fix: wrong text in cell spacing dialog (Table Properties > More...). Not translated yet.

v1.48
  fix: all the font and paragraph two-state actions (like bold, overline, alignment) did not update their state
    propertly in case of multicell selection
  fix: minor fixes in Turkish and Czech translations

v1.47.1
  new: pure C++Builder ActionTest example. The main unit is Unit1.cpp. Currently, only for CB6.
v1.47
  new: TRVActionPrintPreview.Maximized: Boolean allows showing the print preview form maximized
  upd: Czech translation is updated by Pavel Polivka.
v1.46
  fix: in ruler
  upd: Farsi (former Persian) translation. RVAL_Persian.pas was renamed to RVAL_Farsi.pas.
v1.45
  fix: in ruler
  upd: in Lithuanian translation
  upd: Turkish translation updated by Ibrahim Kutluay

v1.44
  impr: Dutch (NL) translation is fixed by Henk van Bokhorst
  impr/fix: Demo (Unit3.pas) has the following changes:
    - fix in events of comboboxes (applying the font name applied the font size as well);
    - URL detection occurs on Tab, not only on Space and Enter;
    - RV_RegisterPngGraphic(TPngObject) is called in the initialization

v1.43
  Update for compatibility with TRichView 1.9.6
  impr: RVRuler supports bullets&numbering (first step to)
  fix: TRVSpinEdit - editing floating point value >= 1000
v1.42
  new: TRVATBXPopupMenu - TBX's TRVAPopupMenu (see http://g32.org)
    To enable it, remove the dot from the line {.$DEFINE USETBX} in RichViewActions.inc
    and recompile RichViewActions packages (implemented by Grzegorz Rewucki).
  new: packages for Delphi 2005 (Win32)
  new: TrvActionItemProperties properties: GraphicFilter and BackgroundGraphicFilter.
    They allow to set file filters for open dialogs (for changing picture; for changing
    backgrounds of table and cells). If they are empty (default), a full list of supported
    graphic formats is used (as before).
v1.41
  new: TRVATBPopupMenu - Toolbar2000's TRVAPopupMenu (see http://www.jrsoftware.org).
    To enable it, remove the dot from the line {.$DEFINE USETB2K} in RichViewActions.inc
    and recompile RichViewActions packages (implemented by Grzegorz Rewucki).
  new: MaxSuggestionsCount: Integer property of TRVAPopupMenu and TRVATBPopupMenu.
    Set it to a positive value to limit a number of livespelling suggestions in
    the popup menu (implemented by Grzegorz Rewucki).
  Translations: 
    new: Chinese (Big5) by Ian Chen
    upd: Polish

v1.40.1
  some fixes and tweaks

v1.40
  new: Polish translation by Grzegorz Rewucki
  impr: TrvActionInsertTable.BestWidth is applied to tables inserted
    by TrvActionInsertTable.ShowTableSizeDialog.
    TrvActionInsertTable.BestWidth may be changed by the user in the table insertion
    dialog, if "store dimensions for new tables" is checked.

v1.39
  fix: Compatibility with Win95 is restored (RichViewActions use GetComboBoxInfo function which was added in Win98.
    Now this function is linked dynamically). Clipboard-related RichViewActions cannot work with combo boxes in
    Win95 where this function is not available.
  fix: FocusControl for several labels was missed or wrong

v1.38
New translations:
- Hungarian by Gabor Berenyi
- Chinese (Simplified) by Jiansong Liu
Updated translations:
- Spanish (by pchelp-info)
- minor changes in German and Russian

v1.37.6
  fix: TRVAPopupMenu.OnLiveSpellIgnoreAll was never called
  new: TRVAPopupMenu.OnLiveSpellWordReplace event. It occurs when misspelling is corrected
    from popup menu. Unlike other OnLiveSpell*** events, it's facultative. 
    TRVAPopupMenu corrects misspelling itself. This event can be used for
    spellcheckers which can use user's choice to adjust suggestions for this
    word in future (for example, you can call TRVASpell.StoreReplacement
    in this event).

v1.37 (requires TRichView 1.8.25)
  fix: in live spelling;
  impr: in NormalizeRichView (RVNormalize.pas)
  new: RVA_Addict3AutoCorrect function - perfoms autocorrection
   using Addict3. This function should be called when user presses
   ENTER or some word delimiter character.

v1.36
  new: smarter URL detection (adds 'mailto:' to tags, understands
    brackets and quotes)
  new: TRVAPopupMenu supports live spelling (see readme.rvf)

v1.35
  new: RVAControlPanel.SearchScope property. 
    Affects all search and replace actions.
    Possible values:
    - rvssAskUser (default): when the end/beginning of document 
      is reached,  the application asks user if he/she wants 
      to continue search from the beginning/end of the document;
    - rvssFromCursor: (as it was before) search/replacing is 
      complete when the end/beginning of document is reached;
    - rvssGlobal: search is automatically starts from the beginning/end
      when the end/beginning is reached; replace-all command starts replacing
      from the beginning of document.
  impr: minor tweaks in the table size popup window.
  new: Turkish translation by Necmettinozalp 
    (new messages are not translated yet)
v1.34
  new: TrvActionInsertTable.ShowTableSizeDialog - method displaying
  non-modal popup window for inserting table with the given size.
  impr: new look for color picker under WinXP
v1.33.1
  v1.33 (requires TRichView 1.8.20+)
  new: support of tab positions, paragraph pagination options.
  new: spacing around images, alternate text
  new: TrvShowSpecialCharacters action
  new: TrvActionInsertHyperlink methods for URL detection 
    and closing hyperlinks on typing.
  impr: TRVRuler supports tabs, bidimode, tables (great work by Pieter Zijlstra)
  impr: paper margins can be defined in inches (TrvActionPageSetup.MarginsUnits)
  impr: TRVRuler can be localized 
    (procedure RVALocalizeRuler from RVLocRuler.pas)
  impr: ThemeEngine support: TListBox is skinned

  v1.32.1
  new: TrvActionExport.ExportToFile method allows to export to the specified file
    without displaying dialog. See readme.rvf, section "Files".
  chg: it was a bad idea to assign to HTML more priority than to RTF on pasting.
    While HTML is better for pasting from MS Internet Explorer, RTF is much
    better than pasting from MS Word. Restored.
  impr: cell border actions are disabled for tables with CellBorderWidth=0.
  v1.32
  new: possibility for opening, saving, exporting and inserting files in custom formats.
    See "Files: working with custom formats" section in readme.rvf and Examples.txt
  new: downloading images referenced in HTML and RTF files, using Indy.
    See "Downloading images" section in readme.rvf.
  new: ability to use JVCL file open and save dialogs (only useful for Delphi 5)
    See RichViewActions.inc
  minor fixes

  v1.31.1
  Ukrainian translation by Gennady Ostapenko

  v1.31
  new: TRuler by Pieter Zijlstra http://home.hccnet.nl/p.zylstra/
    is included in RichViewActions (with the permission of the author).
    If you used this component before, please uninstall it before
    updating RichViewActions, since it's now included in RichViewActions
    packages.
    TRVRuler is a component (inheritef from TRuler) for using with TRichViewEdit.
    See new "Ruler" section in readme.rvf for details.
  impr: If RichViewEdit.RTFReadProperties.UnicodeMode=rvruOnlyUnicode,
    TRVHtmlImporter inserts HTML as Unicode.

  v1.30.1
  In the current version, all new messages for v1.27&28 
  are localized for:
  Czech, English, French, German, Italian, Lithuanian, Norwegian, Portuguese(Br), Russian, Spanish, Swedish.
  Waiting for:
  Dutch, Persian, Slovak.

  v1.30
  new: TRVAControlPanel.OnAddStyle event: occurs any time when actions
    add a new text, paragraph or list style.
  new: RVA_GetColorName function returns a name of color (localized).
    Color-related actions have mathod GetColorName.
    TRVAControlPanel.AddColorNameToHints: Boolean (default: True) - if set,
    color name is added to the hints of color actions having
    UserInterface = rvacNone.
  KSDev.com ThemeEngine related changes:
  - update for compatibility with TE 3.62
  - TRVAControlPanel.OnCreateTeForm event: occurs when TteForm is
    created in any of RVA forms. This event allows to set
    properties of TeForm.

  v1.29
  new: Support for ThemeEngine (http://www.ksdev.com).
    Activated when you remove dot from the line
    {.$DEFINE USERVKSDEVTE}
    in RichViewActions.inc
    Requires ThemeEngine 3.60 and "RichView for ThemeEngine" addon.
  impr: all references to standard VCL unit Grid are removed.
    A special lightweight grid (TRVGrid by Dmitry Bobrik) is used instead.
    Advantages:
    - if you application does not use TStringGrid and TDrawGrid, exe size
      becomes smaller by ~50K
    - new grid works better with scrollbar (it supports only vertical one)
      and mouse wheel.

  v1.28
  impr: insert hyperlink allows customizing hyperlink colors.

  v1.27
  new: TrvActionCharCase: changing case of characters of 
    the selected text (upper -> words -> lower -> upper) 
  new: property Disabled: Boolean for all RichViewActions.
    If this property is False (default), the action updates its
    Enabled property automatically. If this property is True,
    the action is disabled (Enabled=False).
    Note: all actions can be disabled by assigning false to
    RVAControlPanel.ActionsEnabled.
  impr: bidi-actions are made localizable;
    Note once again: do not use them if RichViewEdit.BiDiMode=rvbdUnspecified.
    BiDiMode is not completely supported by all version of Windows,
    so it's recommended to use it only if you _really_
    need this feature.
  new: ability to define help files (different files for different
    languages); several dialogs are slightly redesigned to make 
    space for the "Help" button. Help is only for built-in 
    dialogs, not for actions themselves. Help files are not
    ready yet.
  new: property RVAControlPanel.UseHelpFiles: Boolean.
    If True (default), actions will use help files (if available).      
  new: Lithuanian translation

  v1.26:
  new: actions for changing bidimode of the selected text and paragraphs;
    not localized yet; do not use if RichViewEdit.BiDiMode=rvbdUnspecified
  impr: TRVSpinEdit supports BiDiMode
  new localizations

  v1.25:
  impr: in RTL paragraphs, "Increase/Decrease indent" actions now change right indent.
  new: StoreFileName:Boolean property of TrvActionInsertPicture.
    If set to True, file name will be stored as a name of item (the first parameter
    of InsertPicture) and can be accessed as GetItemText/SetItemText. Changed in v1.53
  

  v1.24
  new: page-setup action
  impr: page-setup button in print preview dialog
  impr: "stretched" option in table background dialog

  v1.23.2
  new localizations
  fixes

  v1.23.1
  new localizations
  small tweaks

  v1.23
  new: background images for tables and cells (in table-properties dialog).
    (I decided against supporting stretching table background images RichViewActions)
  imp: object-properties action can work with selected item 
    (if one item is selected)
  imp: new unit is included: RVNormalize.pas. It is used to normalize
    documents after html import.
  new: ActionTest demo includes optional support for Gif and Png.
  new localizations

  v1.22
  new: localization; includes two languages: "English (US)" and "Russian".
    See comments in RVALocalize.pas how to add languages.
  new TRVAControlPanel.ActionsEnabled: Boolean. Set it to False to disable all
    RichViewActions.

  v1.21
  new: TrvActionInsertSymbol.AlwaysInsertUnicode: Boolean.
    If True, characters will be inserted as Unicode regardless of the
    chosen character set.
  new: two new properties for TrvActionInsertFile and TrvActionExport:
    public FileName: String - name of file which will be chosen initially
      in the save-file dialog.
    published AutoUpdateFileName: Boolean default True - if True, the last
      chosen (in the save-file dialog) file name will be stored in the 
      FileName property.
  fix: bug in TrvActionInsertSymbol

  v1.20
  new: OnNew event for TrvActionNew (main purpose - creating a default
    set of styles, see example in readme.rvf)
  chg: RVPrint property is moved from printing actions to TRVAControPanel
  new: ShowSoftPageBreaks: Boolean property of TRVAControPanel.
    If True (default), soft page breaks are shown when possible
    (after print preview or printing - until the next change in the document)
  fix: default extension in the dialog of TrvActionBackground is set to 'bmp'
  fix: if there are no images, subdirectory for HTML images is not created
  

  v1.19
  new: using RVHTML, TrvActionPasteSpecial can paste HTML format.
    TrvActionPaste can do it too (if the Clipboard does not contain
    RTF, or it is empty (for example, MS Internet Explorer copies
    empty RTF if selection is too complex)).
    Note: it's recommended to include Shift+Ins in the SecondaryShortCuts
    of TrvActionPaste, otherwise this functionality will be available only
    on Ctrl+C.
    This feature requires RichView 1.6.55
  chg: .$DEFINE USERVXML, .$DEFINE USERVHTML, .$DEFINE USERVADDICT3
    were moved to RichViewActions.inc
  impr: all dialogs fit in 640x480
  This text is moved from RichViewActions.pas to history.txt

  v1.18
  new: support for Addict3 (TrvActionAddictSpell, TrvActionAddictThesaurus3)
  (activate define USERVADDICT3 in RichViewActions.inc)
  new: TrvActionBackground
  new: DefaultMargin and DefaultColor properties of TRVAControlPanel.
    New and Open actions now reset margins and background proprties.
  new: property TrvActionExport.CreateDirectoryForHTMLImages : Boolean
    If True (default), the action creates a special subdirectory where it
    places images saved with HTML files. This subdirectory is placed in the
    same directory as the HTML file, and have the name of this HTML file
    and extension .FILES# (where # - an optional number to make this directory
    name unique)
  v.1.17
  new: TRVAPopupMenu

  v.1.16
  chg: For compatibility with C++Builder TRVAControlPanel.RVFFormatName was
    renamed to RVFFormatTitle
  new: HeadingRowCount property of TrvActionInsertTable
    Property dialog for tables now includes some options for printing.
  new: TrvActionInsertTable.OnInserting event

  v.1.15
  new: TrvActionParaBullets and TrvActionParaNumbering
  new: support for RVXML (activate define USERVXML in RichViewActions.inc)
  new: support for RVHTML (activate define USERVHTML in RichViewActions.inc)
  chg: several properties (RVFFilter, DefaultExt, RVFCaption, DefaultFileName,
    AutoDeleteUnusedStyles) were removed from actions and added
    to TRVAControlPanel.
  chg: File filters for office converters include file extensions
  imp: AllowApplyingTo property of TrvActionFillColor allows to exclude some
    elements (text, paragraph, cell, table) from the combobox in this
    action's dialog.
  v.1.14
  fix in TrvActionParaList: removing lists
  imp: in TrvActionExport, HTML export converter is not included
  new: TrvActionTableCellLeftBorder, TrvActionTableCellRightBorder,
    TrvActionTableCellTopBorder, TrvActionTableCellBottomBorder,
    TrvActionTableCellAllBorders, TrvActionTableCellNoBorders
    (thanks to Dustin Campbell)
  v.1.13
  new: TrvActionParaList
  imp: TrvActionOpen now delets unused styles just after opening new style
  v.1.12
  new: TrvActionInsertSymbol
  preview of TrvActionParaList
  v.1.11
  compatibility with RichView 1.6.42
  imp: TRVActionInsertHyperlink does not change current text style now
  imp: actions for changing indents change levels of bulleted/numbered paragraphs.

  v.1.10
  new: TRVActionInsertHyperlink.OnHyperlinkForm event allows to show your own
    form for hyperlink
  new: TrvActionItemProperties can be used for tables (if caret is before or after table)
  new: TrvActionTableProperties - editing properties for table and selected cells
  new: TrvActionTableGrid - displaying/hiding grid for all tables for all editors
  imp: TrvActionInsertTable now has the same set of properties as table.
    These properties define appearance of newly created table
  imp: TrvActionInsertTable has checkbox for storing values
  imp: TrvActionOpen stores the last chosen filter index
  chg: All ColorDialog properties are removed. TRVAControlPanel.ColorDialog should be
    used instead.
  fix: fixed bug in TRVFontComboBox

  v.1.9
  new: TrvActionItemProperties (properties for pictures and breaks)
  new: TRVActionInsertHLine
  new: TRVActionInsertHyperlink
  imp: TrvActionParaBorder allows to change background color and padding of paragraphs

  v.1.8.1
  fix: compatibility with C++Builder 6
  v.1.8
  new: TrvActionParaBorder
  impr: TrvActionPrintPreview form is redesigned.
    note: combobox has images only under Delphi6 or C++Builder6.
    TrvActionPrintPreview form has a button for calling TrvActionPrint.

  v.1.7
  new: New-Open-Save actions are completely rewritten
    Old "Save" action is separated into "Save", "SaveAs", "Export".
    Old "Load" action is renamed to "Open"
    New actions: TrvActionInsertFile
  new: TrvActionInsertPicture
  new: TrvActionPasteSpecial
  new TrvActionLineSpacing100, TrvActionLineSpacing150, TrvActionLineSpacing200
  new: TrvActionInsertPageBreak, TrvActionRemovePageBreak
  new: TrvActionTableCellVAlignTop, TrvActionTableCellVAlignMiddle,
    TrvActionTableCellVAlignBottom, TrvActionTableCellVAlignDefault
  fix: working when no installed printers
  fix: Control property was not handled properly

  v.1.6
  new: added: UserInterface: Boolean (default: True) and Font: TFont to TrvActionFonts
  new: TrvActionFontEx
  new: TrvActionFillColor
  new: new components: TRVColorGrid, TRVColorCombo, TRVFontCombobox
  impr: font combo boxes now can have transparent background in "simple" mode
  (standard simple combo boxes look wrong on page controls under WinXP)
  imp: color-picker window understands arrow keys and ENTER

  v.1.5
  new: new component: TRVAControlPanel.
       Allows to set some properties common for all actions.
       Currently they are:
       UseXPThemes: Boolean - allows to use XPThemes for RichViews,
         TRVOfficeRadioButtons, TRVOfficeRadioGroups on all action forms
       DialogFontName: TFontName - allows to set font name for all
         actions forms.
  new: bonus controls: TRVSpinEdit, TRVOfficeRadioButton, TRVOfficeRadioGroup
  new:
  imp: now all forms are inherited from one form: TfrmRVBase;
       in future it will simplify adding common form features
       (like storing position and localizing)
  imp: now TrvActionIndentDec does not allow hanging first line to go
       beyond the left margin.
  fix: compatibility with D4,D5,CB4,CB5

  v1.4
  new: color actions: TrvActionFontColor, TrvActionFontBackColor,
       TrvActionParaColor, TrvActionColor;
  new: TrvActionTableDeleteTable
  imp: actions modifying text and paragraph style now respect protection options

  v1.3
  new: TrvActionFontGrow, TrvActionFontShrink
       TrvActionFontGrowOnePoint, TrvActionFontShrinkOnePoint
       TrvActionFontAllCaps, TrvActionFontOverline
       TrvActionFind, TrvActionFindNext, TrvActionReplace
  chg: actions are separated by subcategories: RVE File, RVE Edit, etc.
  fix: minor fixes

  v1.2:
  new: a set of TrvActionTable*** actions, modified form for inserting table
  chg: some images, hints and captions are changed
  new: Old TrvActionPrint is renamed to TrvActionQuickPrint
       New TrvActionPrint displays a print dialog before printing
  fix: Increment indent fixed