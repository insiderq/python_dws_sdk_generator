{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for paragraph attributes                 }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit ParaRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RVOfficeRadioBtn, ImgList, RVScroll, RVClasses,
  {$IFDEF USERVKSDEVTE}
  te_controls, te_extctrls,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntComCtrls, TntStdCtrls,
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  Vcl.Themes,
  {$ENDIF}
  RichView, RVStyle, BaseRVFrm, RVSpinEdit, ExtCtrls, RVALocalize, ComCtrls,
  RVTypes, RVStr;

type
  TfrmRVPara = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    pc: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    ts3: TTabSheet;
    gbAlignment: TRVOfficeRadioGroup;
    gbIndents: TGroupBox;
    lblLeft: TLabel;
    lblRight: TLabel;
    lblFirstLine: TLabel;
    Bevel1: TBevel;
    seLeftIndent: TRVSpinEdit;
    seRightIndent: TRVSpinEdit;
    rbNegative: TRVOfficeRadioButton;
    rbPositive: TRVOfficeRadioButton;
    seFirstLineIndent: TRVSpinEdit;
    gbSpacing: TGroupBox;
    lblBefore: TLabel;
    lblAfter: TLabel;
    lblLineSpacing: TLabel;
    Bevel3: TBevel;
    lblBy: TLabel;
    seSpaceBefore: TRVSpinEdit;
    seSpaceAfter: TRVSpinEdit;
    seLineSpacingValue: TRVSpinEdit;
    cmbLineSpacing: TComboBox;
    gbSample: TGroupBox;
    rv: TRichView;
    ImageList1: TImageList;
    rvs: TRVStyle;
    gbPagination: TGroupBox;
    cbKeepLinesTogether: TCheckBox;
    cbKeepWithNext: TCheckBox;
    ilSmallTabs: TImageList;
    ilTabs: TImageList;
    lstTabs: TListBox;
    btnDelete: TButton;
    btnDeleteAll: TButton;
    rgTabAlign: TRVOfficeRadioGroup;
    rgLeader: TRadioGroup;
    seTabPos: TRVSpinEdit;
    lblTabPos: TLabel;
    btnSet: TButton;
    lblDelTabs: TLabel;
    lblDelTabList: TLabel;
    ilListTabs: TImageList;
    lblOutlineLevel: TLabel;
    cmbOutline: TComboBox;
    lblTabPosMU: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure seFirstLineIndentChange(Sender: TObject);
    procedure cmbLineSpacingClick(Sender: TObject);
    procedure seLineSpacingValueChange(Sender: TObject);
    procedure seChange(Sender: TObject);
    procedure gbAlignmentClick(Sender: TObject);
    procedure gbAlignmentDblClickItem(Sender: TObject);
    procedure rbFLClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure seLeftIndentChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lstTabsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure lstTabsClick(Sender: TObject);
    procedure seTabPosChange(Sender: TObject);
    procedure btnDeleteAllClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnSetClick(Sender: TObject);
  private
    { Private declarations }
    _lstTabs, _rgLeader, _lblDelTabList: TControl;
    _btnSet, _btnDelete, _btnDeleteAll: TControl;
    _ts2: TControl;    
    IgnoreChanges: Boolean;
    FDeleteAllTabs: Boolean;
    FTabsToDelete: TRVIntegerList;
    FTabs: TRVTabInfos;
    DoNotResetLeader, FHanging: Boolean;
    procedure UpdateSample;
    procedure SetTabs(Value: TRVTabInfos);
    procedure UpdateTabList(NewIndex: Integer);
    procedure DrawTabItem(Canvas: TCanvas; Index: Integer; R: TRect; Selected: Boolean);
    procedure UpdateDelTabList;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    SimpleTabManagement: Boolean;
    _cmbLineSpacing, _cmbOutlineLevel, _cbKeepWithNext, _cbKeepLinesTogether: TControl;
    TabsModified: Boolean;
    procedure Localize; override;
    procedure HideTabsPage;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    property DeleteAllTabs: Boolean read FDeleteAllTabs write FDeleteAllTabs;
    property TabsToDelete: TRVIntegerList read FTabsToDelete;
    property Tabs: TRVTabInfos read FTabs write SetTabs;
  end;

var
  frmRVPara: TfrmRVPara;

implementation
uses RichViewActions, RVUni;

{$R *.dfm}

procedure TfrmRVPara.FormCreate(Sender: TObject);
var h: Integer;
    PreviewText: TRVALocString;
begin
  Canvas.Font := lstTabs.Font;
  h := Canvas.TextHeight('Wg')+4;
  if h<20 then
    h := 20;
  lstTabs.ItemHeight := h;
  _cmbLineSpacing := cmbLineSpacing;
  _cmbOutlineLevel := cmbOutline;
  _cbKeepWithNext := cbKeepWithNext;
  _cbKeepLinesTogether := cbKeepLinesTogether;
  _lstTabs := lstTabs;
  _rgLeader := rgLeader;
  _btnSet := btnSet;
  _btnDelete := btnDelete;
  _btnDeleteAll := btnDeleteAll;
  _lblDelTabList := lblDelTabList;
  _ts2 := ts2;
  rgLeader.Items[4] := RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT+
    RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT+
    RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT+RVCHAR_MIDDLEDOT;
  UpdateLengthSpinEdit(seLeftIndent, False, True);
  UpdateLengthSpinEdit(seRightIndent, False, True);
  UpdateLengthSpinEdit(seFirstLineIndent, False, True);
  UpdateLengthSpinEdit(seSpaceBefore, True, True);
  UpdateLengthSpinEdit(seSpaceAfter, True, True);
  UpdateLengthSpinEdit(seTabPos, False, True);  
  inherited;
  {$IFDEF USERVTNT}
  rvs.TextStyles[0].Unicode := True;
  rvs.TextStyles[1].Unicode := True;  
  {$ENDIF}  
  {$IFDEF USELOREMIPSUM}
  PreviewText := LoremIpsum1;
  {$ELSE}
  PreviewText := RVA_GetS(rvam_par_Preview, ControlPanel);
  {$ENDIF}

  RVAAddRV(rv, PreviewText);
  RVAAddRV(rv, PreviewText, 1, 1);
  RVAAddRV(rv, PreviewText);
  rv.Format;
  FTabsToDelete := TRVIntegerList.Create;
  FTabs := TRVTabInfos.Create(nil);
  if TRVAControlPanel(ControlPanel).UserInterface=rvauiHTML then
    HideTabsPage;
  PrepareImageList(ImageList1);
  PrepareImageList(ilTabs);
  PrepareImageList(ilSmallTabs);
end;

procedure TfrmRVPara.FormDestroy(Sender: TObject);
begin
  FTabsToDelete.Free;
  FTabs.Free;
  inherited;
end;

procedure TfrmRVPara.seFirstLineIndentChange(Sender: TObject);
begin
  if not Visible or IgnoreChanges then
    exit;
  if seFirstLineIndent.Indeterminate or (seFirstLineIndent.AsInteger=0) then begin
    rbNegative.Checked := False;
    rbPositive.Checked := False;
    end
  else if not seFirstLineIndent.Indeterminate and not rbNegative.Checked and not rbPositive.Checked then begin
    rbPositive.Checked := not FHanging;
    rbNegative.Checked := FHanging;
    end
  else if rbNegative.Checked and
    (seLeftIndent.Value<seFirstLineIndent.Value) then begin
    IgnoreChanges := True;
    seLeftIndent.Value := seFirstLineIndent.Value;
    IgnoreChanges := False;
  end;
  UpdateSample;
end;

procedure TfrmRVPara.seLeftIndentChange(Sender: TObject);
begin
  if not Visible or IgnoreChanges then
    exit;
  if rbNegative.Checked and (seFirstLineIndent.Value>0) and
    (seLeftIndent.Value<seFirstLineIndent.Value) then begin
    IgnoreChanges := True;
    seFirstLineIndent.Value := seLeftIndent.Value;
    IgnoreChanges := False;
  end;
  UpdateSample;
end;

procedure TfrmRVPara.rbFLClick(Sender: TObject);
begin
  if not Visible then
    exit;
  if rbNegative.Checked and
    (seLeftIndent.Value<seFirstLineIndent.Value) then begin
    IgnoreChanges := True;
    seLeftIndent.Value := seFirstLineIndent.Value;
    IgnoreChanges := False;
  end;
  if seFirstLineIndent.Indeterminate then
    seFirstLineIndent.Value := 0;
  if rbNegative.Checked then
    FHanging := True
  else if rbPositive.Checked then
    FHanging := False;
  UpdateSample;
end;


procedure TfrmRVPara.cmbLineSpacingClick(Sender: TObject);
var ItemIndex: Integer;
begin
  if not Visible then
    exit;
  ItemIndex := GetXBoxItemIndex(_cmbLineSpacing);
  case ItemIndex of
    0,1,2: // predefined %
      begin
        seLineSpacingValue.Indeterminate := True;
        seLineSpacingValue.IntegerValue := False;
        seLineSpacingValue.Increment := 0.5;
        seLineSpacingValue.MinValue := 0.5;
        seLineSpacingValue.MaxValue := 100;
      end;
      3,4: // height in units
      begin
        seLineSpacingValue.Indeterminate := True;
        seLineSpacingValue.IntegerValue := True;
        UpdateLengthSpinEdit(seLineSpacingValue, True, True);
        if RVA_GetFineUnits(TRVAControlPanel(ControlPanel))=rvuPixels then
          seLineSpacingValue.Value := 16 // pixels
        else
          seLineSpacingValue.Value := 12; // points
      end;
      5:  // %
      begin
        seLineSpacingValue.IntegerValue := False;
        seLineSpacingValue.Increment := 0.5;
        seLineSpacingValue.MinValue := 0.5;
        seLineSpacingValue.MaxValue := 100;
        if seLineSpacingValue.Indeterminate then
          seLineSpacingValue.Value := 3;
      end;
  end;
  UpdateSample;
end;

procedure TfrmRVPara.seLineSpacingValueChange(Sender: TObject);
begin
  if not Visible then
    exit;
  if not seLineSpacingValue.Indeterminate and
    (GetXBoxItemIndex(_cmbLineSpacing)<3) then
    SetXBoxItemIndex(_cmbLineSpacing, 5);
  UpdateSample;
end;

procedure TfrmRVPara.gbAlignmentClick(Sender: TObject);
begin
  UpdateSample;
end;

procedure TfrmRVPara.UpdateSample;
begin
  if not Visible then
    exit;
  with rvs.ParaStyles[1] do begin
    if gbAlignment.ItemIndex<0 then
      Alignment := rvaLeft
    else
      Alignment :=  TRVAlignment(gbAlignment.ItemIndex);
    SpaceBefore := rvs.RVUnitsToUnits(seSpaceBefore.Value / 2, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
    SpaceAfter := rvs.RVUnitsToUnits(seSpaceAfter.Value / 2, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
    Background.BorderOffsets.Top := SpaceBefore;
    Background.BorderOffsets.Bottom := SpaceAfter;
    LeftIndent := rvs.RVUnitsToUnits(seLeftIndent.Value / 2, TRVAControlPanel(ControlPanel).UnitsDisplay);
    RightIndent := rvs.RVUnitsToUnits(seRightIndent.Value / 2, TRVAControlPanel(ControlPanel).UnitsDisplay);
    if rbNegative.Checked then
      FirstIndent := -rvs.RVUnitsToUnits(seFirstLineIndent.Value / 2,
        TRVAControlPanel(ControlPanel).UnitsDisplay)
    else
      FirstIndent := rvs.RVUnitsToUnits(seFirstLineIndent.Value / 2,
        TRVAControlPanel(ControlPanel).UnitsDisplay);
    case GetXBoxItemIndex(_cmbLineSpacing) of
      0:
        begin
          LineSpacing := 100;
          LineSpacingType := rvlsPercent;
        end;
      1:
        begin
          LineSpacing := 150;
          LineSpacingType := rvlsPercent;
        end;
      2:
        begin
          LineSpacing := 200;
          LineSpacingType := rvlsPercent;
        end;
      3:
        begin
          LineSpacing := 100;
          LineSpacingType := rvlsPercent;
          if not seLineSpacingValue.Indeterminate then begin
            LineSpacing := rvs.RVUnitsToUnits(seLineSpacingValue.Value,
              RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
            LineSpacingType := rvlsLineHeightAtLeast;
          end;
        end;
      4:
        begin
          LineSpacing := 100;
          LineSpacingType := rvlsPercent;
          if not seLineSpacingValue.Indeterminate then begin
            LineSpacing := rvs.RVUnitsToUnits(seLineSpacingValue.Value,
              RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
            LineSpacingType := rvlsLineHeightExact;
          end;
        end;
      5:
        begin
          LineSpacing := Round(seLineSpacingValue.Value * 100);
          if LineSpacing<50 then
            LineSpacing := 50;
          LineSpacingType := rvlsPercent;
        end;

    end;
  end;
  rv.Format;
end;

procedure TfrmRVPara.seChange(Sender: TObject);
begin
  UpdateSample;
end;


procedure TfrmRVPara.gbAlignmentDblClickItem(Sender: TObject);
begin
  ModalResult := mrOk;
end;



function TfrmRVPara.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := pc.Left*2+pc.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-pc.Top-pc.Height);
  Result := True;
end;

procedure TfrmRVPara.FormActivate(Sender: TObject);
begin
  inherited;
  UpdateSample;
end;

procedure TfrmRVPara.SetTabs(Value: TRVTabInfos);
begin
  FTabs.Assign(Value);
  UpdateTabList(-1);
  lstTabsClick(nil);  
end;

procedure TfrmRVPara.UpdateTabList(NewIndex: Integer);
var i: Integer;
    bmp: TBitmap;
begin
  ClearXBoxItems(_lstTabs);
  ilListTabs.Clear;
  ilListTabs.AllocBy := Tabs.Count;
  for i := 0 to Tabs.Count-1 do begin
    XBoxItemsAddObject(_lstTabs, IntToStr(Tabs[i].Position), nil);
    bmp := TBitmap.Create;
    ilSmallTabs.GetBitmap(ord(Tabs[i].Align), bmp);
    ilListTabs.AddMasked(bmp, bmp.Canvas.Pixels[0,0]);
    bmp.Free;
  end;
  _btnDeleteAll.Enabled := (Tabs.Count>0) or not DeleteAllTabs;
  if NewIndex>=0 then
    SetXBoxItemIndex(_lstTabs, NewIndex)
  else if seTabPos.Indeterminate and (Tabs.Count>0) then
    SetXBoxItemIndex(_lstTabs, 0)
  else
    SetXBoxItemIndex(_lstTabs, Tabs.Find(rvs.RVUnitsToUnits(seTabPos.Value,
      TRVAControlPanel(ControlPanel).UnitsDisplay)));
end;



procedure TfrmRVPara.Localize;
var i: Integer;
begin
  inherited;
  Caption := RVA_GetS(rvam_par_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  gbAlignment.Caption := RVA_GetSH(rvam_par_Alignment, ControlPanel);
  gbAlignment.Items[0].Caption := RVA_GetS(rvam_par_AlLeft, ControlPanel);
  gbAlignment.Items[1].Caption := RVA_GetS(rvam_par_AlRight, ControlPanel);
  gbAlignment.Items[2].Caption := RVA_GetS(rvam_par_AlCenter, ControlPanel);
  gbAlignment.Items[3].Caption := RVA_GetS(rvam_par_AlJustify, ControlPanel);
  lblOutlineLevel.Caption := RVA_GetSAsIs(rvam_par_OutlineLevel, ControlPanel);
  cmbOutline.Items[0] := RVA_GetSAsIs(rvam_par_OL_BodyText, ControlPanel);
  for i := 1 to cmbOutline.Items.Count-1 do
    cmbOutline.Items[i] := Format(RVA_GetSAsIs(rvam_par_OL_Level, ControlPanel), [i]);
  gbSpacing.Caption := RVA_GetSHUnitsAsIs(rvam_par_Spacing,
    RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), ControlPanel);
  lblBefore.Caption := RVA_GetSAsIs(rvam_par_Before, ControlPanel);
  lblAfter.Caption := RVA_GetSAsIs(rvam_par_After, ControlPanel);
  lblLineSpacing.Caption := RVA_GetSAsIs(rvam_par_LineSpacing, ControlPanel);
  lblBy.Caption := RVA_GetSAsIs(rvam_par_By, ControlPanel);
  gbIndents.Caption := RVA_GetSHUnitsAsIs(rvam_par_Indents,
    TRVAControlPanel(ControlPanel).UnitsDisplay, ControlPanel);
  lblLeft.Caption := RVA_GetSAsIs(rvam_par_Left, ControlPanel);
  lblRight.Caption := RVA_GetSAsIs(rvam_par_Right, ControlPanel);
  lblFirstLine.Caption := RVA_GetSAsIs(rvam_par_FirstLine, ControlPanel);
  rbNegative.Caption := RVA_GetS(rvam_par_Hanging, ControlPanel);
  rbPositive.Caption := RVA_GetS(rvam_par_Indented, ControlPanel);
  gbSample.Caption := RVA_GetSHAsIs(rvam_par_Sample, ControlPanel);
  cmbLineSpacing.Items[0] := RVA_GetSAsIs(rvam_par_LS_100, ControlPanel);
  cmbLineSpacing.Items[1] := RVA_GetSAsIs(rvam_par_150, ControlPanel);
  cmbLineSpacing.Items[2] := RVA_GetSAsIs(rvam_par_200, ControlPanel);
  cmbLineSpacing.Items[3] := RVA_GetSAsIs(rvam_par_AtLeast, ControlPanel);
  cmbLineSpacing.Items[4] := RVA_GetSAsIs(rvam_par_Exactly, ControlPanel);
  cmbLineSpacing.Items[5] := RVA_GetSAsIs(rvam_par_Multiple, ControlPanel);
  rvs.TextStyles[0].Charset := RVA_GetCharset(ControlPanel);
  rvs.TextStyles[1].Charset := RVA_GetCharset(ControlPanel);
  ts1.Caption := RVA_GetSHAsIs(rvam_par_MainTab, ControlPanel);
  ts2.Caption := RVA_GetSHAsIs(rvam_par_TabsTab, ControlPanel);
  ts3.Caption := RVA_GetSHAsIs(rvam_par_TextFlowTab, ControlPanel);
  lblTabPos.Caption := RVA_GetSAsIs(rvam_par_TabStopPos, ControlPanel);
  lblTabPosMU.Caption := RVA_GetUnitsNameAsIs(TRVAControlPanel(ControlPanel).UnitsDisplay,
    True, ControlPanel);
  rgTabAlign.Caption := RVA_GetSH(rvam_par_TabAlign, ControlPanel);
  rgTabAlign.Items[0].Caption := RVA_GetS(rvam_par_TabAlignLeft, ControlPanel);
  rgTabAlign.Items[1].Caption := RVA_GetS(rvam_par_TabAlignRight, ControlPanel);
  rgTabAlign.Items[2].Caption := RVA_GetS(rvam_par_TabAlignCenter, ControlPanel);
  rgLeader.Caption := RVA_GetSHAsIs(rvam_par_Leader, ControlPanel);
  rgLeader.Items[0] := RVA_GetSAsIs(rvam_par_LeaderNone, ControlPanel);
  {$IFDEF USERVTNT}
  if RVA_GetCharset=DEFAULT_CHARSET then
    for i := 1 to rgLeader.Items.Count-1 do
      rgLeader.Items[i] := AnsiToUtf8(rgLeader.Items[i]);
  {$ENDIF}
  lblDelTabs.Caption := RVA_GetSAsIs(rvam_par_TabsToBeDeleted, ControlPanel);
  lblDelTabList.Caption := RVA_GetSAsIs(rvam_par_TabDelNone, ControlPanel);
  btnDelete.Caption := RVA_GetSAsIs(rvam_par_Delete, ControlPanel);
  btnDeleteAll.Caption := RVA_GetSAsIs(rvam_par_DeleteAll, ControlPanel);
  btnSet.Caption := RVA_GetSAsIs(rvam_par_btnSet, ControlPanel);
  gbPagination.Caption := RVA_GetSHAsIs(rvam_par_Pagination, ControlPanel);
  cbKeepLinesTogether.Caption := RVA_GetSAsIs(rvam_par_KeepLinesTogether, ControlPanel);
  cbKeepWithNext.Caption := RVA_GetSAsIs(rvam_par_KeepWithNext, ControlPanel);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVPara.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _cmbLineSpacing then
    _cmbLineSpacing := NewControl
  else if OldControl = _cmbOutlineLevel then
    _cmbOutlineLevel := NewControl
  {$IFDEF USERVTNT}
  else if OldControl = pc then
    _ts2 := TTntPageControl(NewControl).Pages[1]
  {$ENDIF}
  {$IFDEF USERVKSDEVTE}
  else if OldControl = pc then
    _ts2 := TTePageControl(NewControl).Pages[1]
  {$ENDIF}
  else if OldControl = _cbKeepWithNext then
    _cbKeepWithNext := NewControl
  else if OldControl = _cbKeepLinesTogether then
    _cbKeepLinesTogether := NewControl
  else if OldControl = _rgLeader then
    _rgLeader := NewControl
  else if OldControl = _btnSet then
    _btnSet := NewControl
  else if OldControl = _btnDelete then
    _btnDelete := NewControl
  else if OldControl = _btnDeleteAll then
    _btnDeleteAll := NewControl
  else if OldControl = _lblDelTabList then
    _lblDelTabList := NewControl
  else if OldControl = _lstTabs then begin
    _lstTabs := NewControl;
    {$IFDEF USERVKSDEVTE}
    TteListBox(_lstTabs).Images := ilListTabs;
    {$ENDIF}
    end;
end;
{$ENDIF}


procedure TfrmRVPara.DrawTabItem(Canvas: TCanvas; Index: Integer; R: TRect;
  Selected: Boolean);
{$IFDEF RICHVIEWDEFXE2}
var Details: TThemedElementDetails;
    LColor: TColor;
{$ENDIF}
begin
  if Selected then begin
    Canvas.Font.Color := clHighlightText;
    Canvas.Brush.Color := clHighlight;
    end
  else begin
   {$IFDEF RICHVIEWDEFXE2}
    if ThemeControl(Self) then
      Canvas.Brush.Color := StyleServices.GetStyleColor(scListBox)
    else
   {$ENDIF}
     Canvas.Brush.Color := clWindow;
   Canvas.Font.Color := clWindowText;
  end;
  Canvas.FillRect(R);
  ilSmallTabs.Draw(Canvas, R.Left+2, (R.Top+R.Bottom-ilSmallTabs.Height) div 2,
    ord(FTabs[Index].Align));
  inc(R.Left, 4+ilSmallTabs.Width);
  {$IFDEF RICHVIEWDEFXE2}
  if not Selected and ThemeControl(Self) then begin
     Details := StyleServices.GetElementDetails(teEditTextNormal);
     if not StyleServices.GetElementColor(Details, ecTextColor, LColor) or (LColor = clNone) then
        LColor := Canvas.Font.Color;
      StyleServices.DrawText(Canvas.Handle, Details,
        seTabPos.GetValueString(rvs.GetAsRVUnits(FTabs[Index].Position,
          TRVAControlPanel(ControlPanel).UnitsDisplay)),
        R, TTextFormatFlags(DT_SINGLELINE or DT_VCENTER), LColor)
     end
  else
  {$ENDIF}
    DrawText(Canvas.Handle, PChar(seTabPos.GetValueString(rvs.GetAsRVUnits(FTabs[Index].Position,
      TRVAControlPanel(ControlPanel).UnitsDisplay))), -1, R,
      DT_SINGLELINE or DT_VCENTER);
end;

procedure TfrmRVPara.lstTabsDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  DrawTabItem({$IFDEF USERVTNT}TTntListBox(_lstTabs){$ELSE}TListBox(_lstTabs){$ENDIF}.Canvas,
    Index, Rect, odSelected in State);
end;

procedure TfrmRVPara.lstTabsClick(Sender: TObject);
var idx,i,ldr: Integer;
    s: String;
    ss: TRVALocStrings;
begin
  idx := GetXBoxItemIndex(_lstTabs);
  if idx<0 then
    exit;
  DoNotResetLeader := True;
  seTabPos.Value := rvs.GetAsRVUnits(FTabs[idx].Position,
    TRVAControlPanel(ControlPanel).UnitsDisplay);
  DoNotResetLeader := False;
  rgTabAlign.ItemIndex := ord(FTabs[idx].Align);
  s := Tabs[idx].Leader;
  if s='' then
    ldr := 0
  else begin
    ldr := -1;
    ss := CreateXBoxItemsCopy(_rgLeader);
    try
      for i := 1 to ss.Count-1 do
        if s=ss[i][5] then begin
          ldr := i;
          break;
        end;
    finally
      ss.Free;
    end;
  end;
  SetXBoxItemIndex(_rgLeader, ldr);
  _btnSet.Enabled := True;
  _btnDelete.Enabled := True;
end;

procedure TfrmRVPara.seTabPosChange(Sender: TObject);
begin
  _btnSet.Enabled := not seTabPos.Indeterminate;
  _btnDelete.Enabled := not seTabPos.Indeterminate;
  if rgTabAlign.ItemIndex<0 then
    rgTabAlign.ItemIndex := 0;
  if not DoNotResetLeader then
    SetXBoxItemIndex(_rgLeader, 0);
end;

procedure TfrmRVPara.btnDeleteAllClick(Sender: TObject);
begin
  TabsModified := True;
  if not SimpleTabManagement then
    DeleteAllTabs := True;
  UpdateDelTabList;
  FTabs.Clear;
  UpdateTabList(-1);
end;

procedure TfrmRVPara.UpdateDelTabList;
var i: Integer;
    s: TRVALocString;
begin
  if DeleteAllTabs then
    s := RVA_GetS(rvam_par_TabDelAll, ControlPanel)
  else if FTabsToDelete.Count=0 then
    s := RVA_GetS(rvam_par_TabDelNone, ControlPanel)
  else begin
    s := '';
    for i := 0 to FTabsToDelete.Count-2 do
      s := s+seTabPos.GetValueString(rvs.GetAsRVUnits(FTabsToDelete[i],
        TRVAControlPanel(ControlPanel).UnitsDisplay))+'; ';
    s := s+seTabPos.GetValueString(rvs.GetAsRVUnits(FTabsToDelete[FTabsToDelete.Count-1],
      TRVAControlPanel(ControlPanel).UnitsDisplay))+'.';
  end;
  SetControlCaption(_lblDelTabList, s);
end;

procedure TfrmRVPara.btnDeleteClick(Sender: TObject);
var idx: Integer;
    p: TRVStyleLength;
begin
  if seTabPos.Indeterminate then begin
    Beep;
    exit;
  end;
  TabsModified := True;
  p := rvs.RVUnitsToUnits(seTabPos.Value,
    TRVAControlPanel(ControlPanel).UnitsDisplay);
  if not DeleteAllTabs and not SimpleTabManagement then begin
    FTabsToDelete.AddUnique(p);
    FTabsToDelete.Sort;
    UpdateDelTabList;
  end;
  idx := Tabs.Find(p);
  if idx>=0 then
    Tabs[idx].Free;
  if idx>=Tabs.Count then
    dec(idx);
  UpdateTabList(idx);
  lstTabsClick(nil);
end;

procedure TfrmRVPara.btnSetClick(Sender: TObject);
var idx: Integer;
    tab: TRVTabInfo;
    SL: TRVALocStrings;
    p: TRVStyleLength;
begin
  if seTabPos.Indeterminate then begin
    Beep;
    exit;
  end;
  TabsModified := True;
  p := rvs.RVUnitsToUnits(seTabPos.Value,
    TRVAControlPanel(ControlPanel).UnitsDisplay);
  idx := Tabs.Find(p);
  if idx<0 then begin
    Tabs.Add;
    idx := Tabs.Count-1;
  end;
  tab := Tabs[idx];
  if rgTabAlign.ItemIndex>=0 then
    tab.Align := TRVTabAlign(rgTabAlign.ItemIndex);
  if GetXBoxItemIndex(_rgLeader) in [1..4] then begin
    SL := CreateXBoxItemsCopy(_rgLeader);
    tab.Leader := SL[GetXBoxItemIndex(_rgLeader)][5];
    SL.Free;
    end
  else
    tab.Leader := '';
  tab.Position := rvs.RVUnitsToUnits(seTabPos.Value,
    TRVAControlPanel(ControlPanel).UnitsDisplay);;
  UpdateTabList(tab.Index);
  lstTabsClick(nil);
end;

procedure TfrmRVPara.HideTabsPage;
begin
  HideTabSheet(_ts2);
end;

end.
