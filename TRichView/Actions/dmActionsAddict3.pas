unit dmActionsAddict3;

interface

uses
  SysUtils, Classes, ActnList, ImgList, Controls, RichViewActions, Forms;

type
  TrvActionsResourceAddict3 = class(TDataModule)
    ImageList1: TImageList;
    ActionList1: TActionList;
    rvActionAddictSpell31: TrvActionAddictSpell3;
    rvActionAddictThesaurus31: TrvActionAddictThesaurus3;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rvActionsResourceAddict3: TrvActionsResourceAddict3;

implementation

{$R *.dfm}

end.
