unit PasteSpecialRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Clipbrd, RVStr,
  RVEdit,
  RichViewActions, RVALocalize, ListRVFrm;



type
  TfrmRVPasteSpecial = class(TfrmRVList)
    gb: TGroupBox;
    lblStyles: TLabel;
    cmbStyles: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lstClick(Sender: TObject);
  private
    { Private declarations }
    LastStyleOption: Integer;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
  public
    { Public declarations }
    _gb, _cmbStyles: TControl;
    procedure InitPasteAs(const RVFCaption: TRVALocString; rve: TCustomRichViewEdit);
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
    procedure Localize; override;
  end;


implementation

uses BaseRVFrm;

{$R *.dfm}

{ TfrmRVPasteSpecial }

procedure TfrmRVPasteSpecial.FormCreate(Sender: TObject);
begin
  _gb := gb;
  _cmbStyles := cmbStyles;
  inherited;
end;

function TfrmRVPasteSpecial.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  if _gb.Visible then begin
    Width := _lst.Left*2+_lst.Width;
    Height := _btnOk.Top+_btnOk.Height+(_btnOk.Top-_gb.Top-_gb.Height);
    Result := True;
    end
  else
    Result := inherited GetMyClientSize(Width, Height);
end;

{$IFDEF RVASKINNED}
procedure TfrmRVPasteSpecial.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _gb then
    _gb := NewControl
  else if OldControl = _cmbStyles then
    _cmbStyles := NewControl
  else
    inherited;
end;
{$ENDIF}

procedure TfrmRVPasteSpecial.InitPasteAs(const RVFCaption: TRVALocString;
  rve: TCustomRichViewEdit);

  procedure AddFormat(Format: Integer; const Caption: TRVALocString; id: Integer;
    Fmt: TRVDragDropFormat);
  begin
    if (Fmt in rve.AcceptPasteFormats) and Clipboard.HasFormat(Format) then
      XBoxItemsAddObject(_lst, Caption, TObject(id));
  end;

  procedure HideOptions;
  var d: Integer;
  begin
    _gb.Visible := False;
    d := (_gb.Top + _gb.Height) - (_lst.Top + _lst.Height);
    _lst.Height := _lst.Height + d;
    ClientHeight := ClientHeight - d;
  end;

begin
  SetControlCaption(_lbl, RVA_GetS(rvam_ps_Label, ControlPanel));
  {$IFDEF USERVKSDEVTE}
  tef.Caption := RVA_GetS(rvam_ps_Title, ControlPanel);
  {$ELSE}
  Caption := RVA_GetS(rvam_ps_Title, ControlPanel);
  {$ENDIF}
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  if not rve.UseStyleTemplates then
  {$ENDIF}
    HideOptions;
  ClearXBoxItems(_lst);
  AddFormat(RegisterClipboardFormat(RVFormatName), RVFCaption, 1, rvddRVF);
  AddFormat(RegisterClipboardFormat(RTFormatName), RVA_GetS(rvam_ps_RTF, ControlPanel), 2, rvddRTF) ;
  {$IFDEF USERVHTMLVIEWER}
  if TRVAControlPanel(ControlPanel).RVHTMLViewImporter<>nil then
    AddFormat(RegisterClipboardFormat(HTMLFormatName), RVA_GetS(rvam_ps_HTML, ControlPanel), 7, rvddHTML) ;
  {$ELSE}
  {$IFDEF USERVHTML}
  if TRVAControlPanel(ControlPanel).RVHTMLImporter<>nil then
    AddFormat(RegisterClipboardFormat(HTMLFormatName), RVA_GetS(rvam_ps_HTML, ControlPanel), 7, rvddHTML) ;
  {$ENDIF}
  {$ENDIF}
  AddFormat(CF_TEXT, RVA_GetS(rvam_ps_Text, ControlPanel), 3, rvddText);
  AddFormat(CF_UNICODETEXT, RVA_GetS(rvam_ps_UnicodeText, ControlPanel), 4, rvddUnicodeText);
  AddFormat(CF_BITMAP, RVA_GetS(rvam_ps_BMP, ControlPanel), 5, rvddBitmap);
  AddFormat(CF_METAFILEPICT, RVA_GetS(rvam_ps_WMF, ControlPanel), 6, rvddMetafile);
  AddFormat(CF_HDROP, RVA_GetS(rvam_ps_GraphicFiles, ControlPanel), 8, rvddFiles);
  AddFormat(CFRV_URL, RVA_GetS(rvam_ps_URL, ControlPanel), 9, rvddURL);
  if GetXBoxItemCount(_lst)>0 then
    SetXBoxItemIndex(_lst, 0);
  HelpContext := 91300;
  lstClick(_lst);
end;


procedure TfrmRVPasteSpecial.FormShow(Sender: TObject);
var d: Integer;
begin
  d := _gb.Top - (_lst.Top+_lst.Height);
  inherited;
  _gb.Top := _lst.Top+_lst.Height+d;
end;

procedure TfrmRVPasteSpecial.lstClick(Sender: TObject);
var Index: Integer;
    AllowOptions: Boolean;
begin
  inherited;
  if not _gb.Visible then
    exit;
  Index := GetXBoxItemIndex(_lst);
  AllowOptions := False;
  if Index>=0 then
    AllowOptions := Integer(GetXBoxObject(_lst, Index)) in [1,2];
  if Visible and (AllowOptions = _cmbStyles.Enabled) then
    exit;
  if not AllowOptions then begin
    LastStyleOption :=GetXBoxItemIndex(_cmbStyles);
    SetXBoxItemIndex(_cmbStyles, -1);
    _cmbStyles.Enabled := False;
    end
  else begin
    SetXBoxItemIndex(_cmbStyles, LastStyleOption);
    _cmbStyles.Enabled := True;
  end;
end;

procedure TfrmRVPasteSpecial.Localize;
begin
  inherited;
  gb.Caption := RVA_GetSHAsIs(rvam_ps_Options, ControlPanel);
  lblStyles.Caption := RVA_GetSAsIs(rvam_ps_Styles, ControlPanel);
  cmbStyles.Items[0] := RVA_GetSAsIs(rvam_ps_Styles1, ControlPanel);
  cmbStyles.Items[1] := RVA_GetSAsIs(rvam_ps_Styles2, ControlPanel);
  cmbStyles.Items[2] := RVA_GetSAsIs(rvam_ps_Styles3, ControlPanel);
end;

end.
