﻿// This file is a copy of RVAL_Rom.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Romanian translation                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{       Original translator: Viorel Doni                }
{                                                       }
{*******************************************************}
{ Updated by Morlova Daniel,                            }
{          by Alconost                                  }
{ Updated: 2012-10-11                                   }
{ Updated: 2014-05-01                                   }
{*******************************************************}

unit RVALUTF8_Rom;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'inci', 'cm', 'mm', 'pica', 'pixeli', 'puncte',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'pixeli'{?}, 'pct',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  'Fişier', 'Editare', 'Format', 'Font', 'Paragraf', 'Inserare', 'Tabel', 'Fereastră', 'Asistenţă',
  // exit
  'Ieşire',
  // top-level menus: View, Tools,
  'Vedere', 'Instrument',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Dimensiune', 'Stil', 'Selectare', 'Aliniere în celule', 'Chenar celulă',
  // menus: Table cell rotation
  'Rotire &celulă', 
  // menus: Text flow, Footnotes/endnotes
  '&Curgere text', '&Note de subsol/note de final',
  // ribbon tabs: tab1, tab2, view, table
  '&Acasă', '&Avansat', '&Vizualizare', '&Tabel',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Clipboard', 'Font', 'Paragraf', 'Listă', 'Editare',
  // ribbon groups: insert, background, page setup,
  'Inserare', 'Fundal', 'Setare pagină',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Legături', 'Note de subsol', 'Vizualizări Document', 'Arată/Ascunde', 'Panoramare', 'Opţiuni',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserare', 'Ştergere', 'Operaţii', 'Chenare',
  // ribbon groups: styles 
  'Stiluri',
  // ribbon screen tip footer,
  'Apăsaţi pe F1 pentru ajutor',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documente Recente', 'Salvaţi o copie a documentului', 'Previzualizaţi şi tipăriţi documentul',
  // ribbon label: units combo
  'Unităţi:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'Nou', 'Nou|Creare document nou',
  // TrvActionOpen
  'Deschide...', 'Deschide|Deschidere fişier',
  // TrvActionSave
  'Salvare', 'Salvare|Salvare document',
  // TrvActionSaveAs
  'Salvare ca...', 'Salvare ca...|Salvare document cu altă denumire, format sau locaţie',
  // TrvActionExport
  'Export...', 'Export|Exportare document în fişier cu altă denumire, format sau locaţie',
  // TrvActionPrintPreview
  'Previzualizare', 'Previzualizare|Previzualizare document pentru tipărire',
  // TrvActionPrint
  'Tipărire...', 'Tipărire|Modificare setări imprimantă şi tipărire document',
  // TrvActionQuickPrint
  'Tipărire directă', '&Tipărire rapidă', 'Tipărire directă|Tipăreşte documentul',
   // TrvActionPageSetup
  'Setările paginii...', 'Setările paginii|Setare parametri pagină',
  // TrvActionCut
  'Decupare', 'Decupare|Ştergere fragment selectat şi plasare în clipboard',
  // TrvActionCopy
  '&Copiere', 'Copiere|Copiere fragment selectat în clipboard',
  // TrvActionPaste
  'Lipire', 'Lipire|Lipire fragment din clipboard în poziţia curentă a documentului',
  // TrvActionPasteAsText
  'Lipire ca &Text', 'Lipire ca text|Inserează textul din Clipboard',
  // TrvActionPasteSpecial
  'Lipire specială...', 'Lipire specială|Lipire conţinut din clipboard în document',
  // TrvActionSelectAll
  'Selectare tot', 'Selectare tot|Selectare întreg document',
  // TrvActionUndo
  'Anulare', 'Anulare|Revenire asupra ultimei acţiuni',
  // TrvActionRedo
  'Repetare', 'Repetare|Repetare ultima acţiune',
  // TrvActionFind
  'Căutare...', 'Căutare|Căutare text indicat în document',
  // TrvActionFindNext
  'Continuare căutare', 'Continuare căutare|Continuare ultima căutare a textului',
  // TrvActionReplace
  'Înlocuire...', 'Înlocuire|Căutare şi înlocuire text indicat în document',
  // TrvActionInsertFile
  'Fişier...', 'Inserare fişier|Inserare fişier în poziţia curentă a documentului',
  // TrvActionInsertPicture
  'Imagine...', 'Inserare imagine|Inserare imagine în poziţia curentă a documentului',
  // TRVActionInsertHLine
  'Linie orizontală', 'Inserare linie orizontală|Inserare linie orizontală în poziţia curentă a documentului',
  // TRVActionInsertHyperlink
  'Hiperlegătură...', 'Inserare hiperlegătură|Inserare hiperlegătură în poziţia curentă a documentului',
  // TRVActionRemoveHyperlinks
  '&Îndepărtare Hiperlegături', 'Îndepărtare Hiperlegături|Îndepărtează toate hiperlegăturile din textul selectat',
  // TrvActionInsertSymbol
  'Simbol...', 'Inserare simbol|Inserare simbol sau caracter special',
  // TrvActionInsertNumber
  '&Numar...', 'Inserare numar|Insereaza un numar',
  // TrvActionInsertCaption
  'Inserare &Legenda...', 'Insereaza legenda|Insereaza o legenda pentru obiectul selectat',
  // TrvActionInsertTextBox
  '&Caseta Text', 'Insereaza caseta pentru text|Insereaza o caseta pentru text',
  // TrvActionInsertSidenote
  '&Nota', 'Inserare nota laterala|Insereaza o nota laterala în caseta pentru text',
  // TrvActionInsertPageNumber
  '&Numar pagina', 'Insereaza numarul paginii|Insereaza un numar pentru pagina',
  // TrvActionParaList
  'Listă...', 'Listă|Adăugare sau modificare a formatului marcajului sau a numerotării în paragrafele selectate',
  // TrvActionParaBullets
  'Marcaj de listă', 'Marcaj de listă|Adăugare sau modificare a marcajului de listă în paragrafele selectate',
  // TrvActionParaNumbering
  'Numerotare', 'Numerotare|Adăugare sau modificare a numerotării în paragrafele selectate',
  // TrvActionColor
  'Culoare de fond...', 'Culoare de fond|Modificare culoare de fond a documentului',
  // TrvActionFillColor
  'Culoare de umplere...', 'Culoare de umplere|Modificare culoare de umplere a textului, paragrafului, tabelului sau a celulei',
  // TrvActionInsertPageBreak
  'Inserare întrerupere pagină', 'Inserare întrerupere pagină|Inserare întrerupere pagină în poziţia curentă a documentului',
  // TrvActionRemovePageBreak
  'Ştergere întrerupere pagină', 'Ştergere întrerupere pagină|Ştergere întrerupere pagină',
  // TrvActionClearLeft
  'Ştergere curgere text în &partea stângă', 'Ştergere curgere text în partea stângă|Plasează acest paragraf sub orice poză aliniată la stânga',
  // TrvActionClearRight
  'Ştergere curgere text în &partea dreaptă', 'Ştergere curgere text în partea dreaptă|Plasează acest paragraf sub orice poză aliniată la dreapta',
  // TrvActionClearBoth
  'Ştergere curgere text în &ambele părţi', 'Ştergere curgere text în ambele părţi|Plasează acest paragraf sub orice poză aliniată la stânga sau la dreapta',
  // TrvActionClearNone
  '&Curgere normală a textului', 'Curgere normală a textului|Permite curgerea textului în jurul pozelor aliniate la stânga sau la dreapta',
  // TrvActionVAlign
  '&Poziţie obiect...', 'Poziţie obiect|Modifică poziţia obiectului ales',
  // TrvActionItemProperties
  'Parametrii obiectului...', 'Parametrii obiectului|Setare parametri la imagine, linie sau tabel selectat',
  // TrvActionBackground
  'Fundal...', 'Fundal|Alegere imagine fundal',
  // TrvActionParagraph
  'Paragraf...', 'Paragraf|Modificare atribute la paragraful selectat',
  // TrvActionIndentInc
  'Mărire alineat', 'Mărire alineat|Mărire alineat din stânga la paragrafelor selectate',
  // TrvActionIndentDec
  'Micşorare alineat', 'Micşorare alineat|Micşorare alineat din stânga la paragrafelor selectate',
  // TrvActionWordWrap
  'Despărţire automată pe linii', 'Despărţire automată pe linii|Activare/Dezactivare despărţire automată pe linii în cadrul parafrafelor',
  // TrvActionAlignLeft
  'Aliniere la stânga', 'Aliniere la stânga|Aliniere text selectat după marginea din stânga',
  // TrvActionAlignRight
  'Aliniere la dreapta', 'Aliniere la dreapta|Aliniere text selectat după marginea din dreapta',
  // TrvActionAlignCenter
  'Aliniere la centru', 'Aliniere la centru|Aliniere text selectat la centru',
  // TrvActionAlignJustify
  'Aliniere după lăţime', 'Aliniere după lăţime|Aliniere text selectat după marginile din dreapta şi stânga',
  // TrvActionParaColor
  'Culoare fond în paragraf...', 'Culoare fond în paragraf|Alegere culoare la fond pentru paragraf',
  // TrvActionLineSpacing100
  'Interval unic', 'Interval unic|Setare interval la 1 între rândurile paragrafului',
  // TrvActionLineSpacing150
  'Interval 1,5', 'Interval 1,5|Setare interval la 1,5 între rândurile paragrafului',
  // TrvActionLineSpacing200
  'Interval dublu', 'Interval dublu|Setare interval dublu între rândurile paragrafului',
  // TrvActionParaBorder
  'Chenar şi fond paragrafului...', 'Chenar şi fond paragrafului|Setări la chenar şi fond pentru paragrafele selectate',
  // TrvActionInsertTable
  'Inserare tabel...', '&Tabel', 'Inserare tabel|Inserare tabel în poziţia curentă a documentului',
  // TrvActionTableInsertRowsAbove
  'Inserare linie sus', 'Inserare linie sus|Inserare linie nouă mai sus de celulele selectate',
  // TrvActionTableInsertRowsBelow
  'Inserare linie jos', 'Inserare linie jos|Inserare linie nouă mai jos de celulele selectate',
  // TrvActionTableInsertColLeft
  'Inserare coloană la stânga', 'Inserare coloană la stânga|Inserare coloană nouă la stânga de celulele selectate',
  // TrvActionTableInsertColRight
  'Inserare coloană la dreapta', 'Inserare coloană la dreapta|Inserare coloană nouă la dreapta de celulele selectate',
  // TrvActionTableDeleteRows
  'Ştergere linii', 'Ştergere linii|Ştergere linii cu celule selectate',
  // TrvActionTableDeleteCols
  'Ştergere coloane', 'Ştergere coloane|Ştergere coloane cu celule selectate',
  // TrvActionTableDeleteTable
  'Ştergere tabel', 'Ştergere tabel|Ştergere tabel',
  // TrvActionTableMergeCells
  'Îmbinare celule', 'Îmbinare celule|Îmbinare celule selectate în una singură',
  // TrvActionTableSplitCells
  'Divizare celulă...', 'Divizare celulă|Divizare celulă selectată în numărul indicat de rânduri şi coloane',
  // TrvActionTableSelectTable
  'Selectare tabel', 'Selectare tabel|Selectare întregul tabel',
  // TrvActionTableSelectRows
  'Selectare linii', 'Selectare linii|Selectare linii care conţin celulele selectate',
  // TrvActionTableSelectCols
  'Selectare coloane', 'Selectare coloane|Selectare coloane care conţin celulele selectate',
  // TrvActionTableSelectCell
  'Selectare celulă', 'Selectare celulă|Selectare celulă',
  // TrvActionTableCellVAlignTop
  'Aliniere la marginea de sus', 'Aliniere la marginea de sus|Aliniere conţinut celulă la marginea de sus',
  // TrvActionTableCellVAlignMiddle
  'Centrare pe verticală', 'Centrare pe verticală|Aliniere conţinut celulă la centru',
  // TrvActionTableCellVAlignBottom
  'Aliniere la marginea de jos', 'Aliniere la marginea de jos|Aliniere conţinut celulă la marginea de jos',
  // TrvActionTableCellVAlignDefault
  'Aliniere prestabilită', 'Aliniere prestabilită|Aliniere prestabilită pe verticală',
  // TrvActionTableCellRotationNone
  '&Fără rotire a celulei', 'Fără rotire a celulei|Roteşte conţinutul celulei cu 0°',
  // TrvActionTableCellRotation90
  'Rotire celulă cu &90°', 'Rotire celulă cu 90°|Roteşte conţinutul celulei cu 90°',
  // TrvActionTableCellRotation180
  'Rotire celulă cu &180°', 'Rotire celulă cu 180°|Roteşte conţinutul celulei cu 180°',
  // TrvActionTableCellRotation270
  'Rotire celulă cu &270°', 'Rotire celulă cu 270°|Roteşte conţinutul celulei cu 270°',  
  // TrvActionTableProperties
  'Parametri tabel...', 'Parametri tabelul|Setare parametri tabel',
  // TrvActionTableGrid
  'Grilă', 'Grilă|Arată/Ascunde linii chenar în tabel',
  // TRVActionTableSplit
  'Divizare tabel', 'Divizare tabel|Divizează tabelul în două tabele începând de la rândul selectat',
  // TRVActionTableToText
  'Convertire în text...', 'Convertire în text|Converteşte tabelul în text',
  // TRVActionTableSort
  '&Sortare...', 'Sortare|Sortează rândurile tabelului',
  // TrvActionTableCellLeftBorder
  'Margine din stânga', 'Margine din stânga|Vizualizare/ascundere margine din stânga pentru celulele selectate',
  // TrvActionTableCellRightBorder
  'Margine din dreapta', 'Margine din dreapta|Vizualizare/ascundere margine din dreapta pentru celulele selectate',
  // TrvActionTableCellTopBorder
  'Margine de sus', 'Margine de sus|Vizualizare/ascundere margine de sus pentru celulele selectate',
  // TrvActionTableCellBottomBorder
  'Margine de jos', 'Margine de jos|Vizualizare sau ascundere margine de jos pentru celulele selectate',
  // TrvActionTableCellAllBorders
  'Chenar', 'Chenarul|Vizualizare/ascundere chenar pentru celulele selectate',
  // TrvActionTableCellNoBorders
  'Fără chenar', 'Fără chenar|Ascundere chenar pentru celulele selectate',
  // TrvActionFonts & TrvActionFontEx
  'Font...', 'Font|Modificare parametri la caractere şi distanţa dintre simboluri în textul selectat',
  // TrvActionFontBold
  'Aldin', 'Aldin|Setare text selectat cu caractere aldine',
  // TrvActionFontItalic
  'Cursiv', 'Cursiv|Setare text selectat cu caractere cursive',
  // TrvActionFontUnderline
  'Subliniat', 'Subliniat|Subliniere text selectat',
  // TrvActionFontStrikeout
  'Tăiat cu linie', 'Tăiat cu linie|Tăiere cu linie a textului selectat',
  // TrvActionFontGrow
  'Mărire dimensiune', 'Mărire dimensiune|Mărire dimensiune font în textul selectat cu 10%',
  // TrvActionFontShrink
  'Micşorare dimensiune', 'Micşorare dimensiune|Micşorare dimensiune font în textul selectat cu 10%',
  // TrvActionFontGrowOnePoint
  'Mărire dimensiune cu 1 pct', 'Mărire dimensiune cu 1 pct|Mărire dimensiune font în textul selectat cu 1 punct',
  // TrvActionFontShrinkOnePoint
  'Micşorare dimensiune cu 1 pct', 'Micşorare dimensiune cu 1 pct|Micşorare dimensiune font în textul selectat cu 1 punct',
  // TrvActionFontAllCaps
  'Toate majuscule', 'Toate majuscule|Modificare litere în majuscule în textul selectat',
  // TrvActionFontOverline
  'Supraliniat', 'Supraliniat|Adăugare linie orizontală asupra textului selectat',
  // TrvActionFontColor
  'Culoare text...', 'Culoare text|Modificare culoare la textul selectat',
  // TrvActionFontBackColor
  'Culoare umplere text...', 'Culoare umplere text|Modificare culoare fundal la textul selectat',
  // TrvActionAddictSpell3
  'Ortografie', 'Ortografie|Verificare ortografică',
  // TrvActionAddictThesaurus3
  'Tezaurus', 'Tezaurus|Căutare sinonime',
  // TrvActionParaLTR
  'De la stânga la dreapta', 'De la stânga la dreapta|Setare direcţie text de la stânga la dreapta pentru paragrafele selectate',
  // TrvActionParaRTL
  'De la dreapta la stânga', 'De la dreapta la stânga|Setare direcţie text de la dreapta la stânga pentru paragrafele selectate',
  // TrvActionLTR
  'Text de la stânga la dreapta', 'Text de la stânga la dreapta|Setare direcţie text selectat de la stânga la dreapta',
  // TrvActionRTL
  'Text de la dreapta la stânga', 'Text de la dreapta la stânga|Setare direcţie text selectat de la dreapta la stânga',
  // TrvActionCharCase
  'Tip de caracter', 'Tip de caracter|Modificare tip de caracter la litere în textul selectat',
  // TrvActionShowSpecialCharacters
  'Simboluri non-printing', 'Simboluri non-printing|Vizualizare/ascundere simboluri non-printing',
  // TrvActionSubscript
  'Indice', 'Indice|Convertire text selectat ca indice',
  // TrvActionSuperscript
  'Exponent', 'Exponent|Convertire text selectat ca exponent',
  // TrvActionInsertFootnote
  '&Notă de subsol', 'Notă de subsol|Inserează o notă de subsol',
  // TrvActionInsertEndnote
  '&Notă de final', 'Notă de final|Inserează o notă de final',
  // TrvActionEditNote
  'Editare notă', 'Editare notă|Începe editarea notei de subsol sau de final',
  '&Ascundere', 'Ascundere|Ascunde sau arată fragmentul selectat',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stiluri...', 'Stiluri|Deschide caseta de dialog de gestionare a stilurilor',
  // TrvActionAddStyleTemplate
  '&Adăugare stil...', 'Adăugare stil|Creează un nou stil de text sau paragraf',
  // TrvActionClearFormat,
  '&Ştergere Format', 'Ştergere Format|Şterge formatele pentru texte şi paragrafe din fragmentul selectat',
  // TrvActionClearTextFormat,
  'Ştergere &Format Text', 'Ştergere Format Text|Şterge formatele din textul selectat',
  // TrvActionStyleInspector
  'Inspector Stil', 'Inspector stil|Arată sau ascunde inspectorul de stil',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'OK', 'Cancel', 'Închide', 'Inserare', 'Deschide...', 'Salvare...', 'Şterge', 'Asistenţă', 'Îndepartare',
  // Others  -------------------------------------------------------------------
  // percents,
  '%',
  // left, top, right, bottom sides
  'Partea din stânga', 'Partea de sus', 'Partea din dreapta', 'Partea de jos',
  // save changes? confirm title
  'Doriţi salvarea modificărilor în %s?', 'Confirmare',
  // warning: losing formatting
  '%s poate conţine elemente de formatare care vor fi pierdute în procesul reformatării fişierului.'#13+
  'Doriţi salvarea documentului în acest format?',
  // RVF format name
  'Format RichView',
  // Error messages ------------------------------------------------------------
  'Eroare',
  'Eroare la citirea textului.'#13#13'Cauze posibile:'#13'- fişierul are format neacceptat de aplicaţie;'#13+
  '- alterare;'#13'- fişierul este deschis în altă aplicaţie care i-a blocat accesul.',
  'Eroare la citirea fişierului cu imagine.'#13#13'Cauze posibile:'#13'- fişierul conţine imagine în format neacceptat de aplicaţie;'#13+
  '- fişierul nu conţine imagine;'#13+
  '- alterare;'#13'- fişierul este deschis în altă aplicaţie care i-a blocat accesul.',
  'Eroare la salvarea fişierului.'#13#13'Cauze posibile:'#13'- spaţiu insuficient;'#13+
  '- interzis accesul la salvare pe disc;'#13'- lipseşte discul în unitatea de disc;'#13+
  '- fişierul este deschis în altă aplicaţie care i-a blocat accesul;'#13+
  '- suport de informaţie deteriorat.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Fişiere în formatul RichView (*.rvf)|*.rvf',  'Fişiere RTF (*.rtf)|*.rtf' , 'Fişiere XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Fişiere textuale (*.txt)|*.txt', 'Fişiere textuale - Unicode (*.txt)|*.txt', 'Fişiere textuale - auto (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Fişiere HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - simplificat (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documente Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Sfârşit căutare', 'Textul ''%s'' nu a fost găsit.',
  // 1 string replaced; Several strings replaced
  'S-a făcut o înlocuire', 'S-au făcut %d înlocuiri',
  // continue search from the beginning/end?
  'S-a ajuns la sfârşitul documentului. Să reîncep căutarea de la început?',
  'S-a ajuns la începutul documentului. Să reîncep căutarea de la sfârşit?',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Automat',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Negru', 'Maro', 'Olive Green', 'Verde închis', 'Dark Teal', 'Albastru închis', 'Indigo', 'Gray-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Dark Red', 'Orange', 'Dark Yellow', 'Green', 'Teal', 'Blue', 'Blue-Gray', 'Gray-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Red', 'Light Orange', 'Lime', 'Sea Green', 'Aqua', 'Light Blue', 'Violet', 'Grey-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Pink', 'Gold', 'Yellow', 'Bright Green', 'Turquoise', 'Sky Blue', 'Plum', 'Gray-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Tan', 'Light Yellow', 'Light Green', 'Light Turquoise', 'Pale Blue', 'Lavender', 'White',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Transparent', '&Auto', '&Alte culori...', '&Prestabilit',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  'Fundal', '&Culoare:', 'Plasare', 'Imagine', 'Exemplu text.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Niciuna', 'Întins', 'Alăturare fixă', 'Alăturat', 'În centru',
  // Padding button
  'Completare...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  'Culoare de umplere', '&Aplicat la:', '&Altă culoare...', 'Completare...',
  'Alegeţi culoarea',
  // [apply to:] text, paragraph, table, cell
  'text', 'paragraf' , 'tabel', 'celulă',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Font', 'Font', 'Interval',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Font:', '&Dimensiune', '&Stil', 'Aldin', '&Cursiv',
  // Script, Color, Back color labels, Default charset
  'Set de simboluri:', '&Culoare:', 'Fundal:', '(prestabilit)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efecte', 'Subliniat', 'Supraliniat', 'Tăiat cu linie', '&Toate majuscule',
  // Sample, Sample text
  'Exemplu', 'Exemplu de text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Interval', '&Interval:', '&Extins', '&Comprimat',
  // Offset group-bix, Offset label, Down, Up,
  'Deplasament vertical', 'Deplasament:', 'Sus', 'Jos',
  // Scaling group-box, Scaling
  'Scalare orizontală', '&Scara:',
  // Sub/super script group box, Normal, Sub, Super
  'Indice şi exponent', '&Normal', 'Indice', 'Exponent',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  'Câmpuri', 'Sus:', 'Stânga:', 'Jos:', 'Dreapta:',
  // Equal values check-box
  '&Valori egale',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Inserare hiperlegăturii', 'Hiperlegătură', '&Text:', '&URL:', '<<selectare>>',
  // cannot open URL
  'Eroare la navigare către "%s"',
  // hyperlink properties button, hyperlink style
  '&Atribute...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Atributele hiperlegăturii', 'Culori obişnuite', 'Culori active',
  // Text [color], Background [color]
  '&Text:', '&Fundal',
  // Underline [color]
  'S&ubliniere:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'F&undal',
  // Underline [color], Attributes group-box,
  'S&ubliniere:', 'Atribute',
  // Underline type: always, never, active (below the mouse)
  '&Subliniere:', 'întotdeauna', 'niciodată', 'activ',  
  // Same as normal check-box
  'Ca obişnuite',
  // underline active links check-box
  '&Subliniere legături active',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Inserare simbol', '&Font:', '&Set de caractere:', 'Unicode',
  // Unicode block
  '&Bloc:',
  // Character Code, Character Unicode Code, No Character
  'Cod simbol: %d', 'Cod simbol: Unicode %d', '(no character)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows        
  'Inserare tabel', 'Dimensiune tabel', 'Coloane:', 'Rânduri:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Lăţimea tabelului', '&Auto', 'După lăţimea ferestrei', '&Indicată:',
  // Remember check-box
  'Prestabilită pentru tabele noi',
  // VAlign Form ---------------------------------------------------------------
  'Poziţie',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Proprietăţi', 'Imagine', 'Pozitie si dimensiune', 'Linia', 'Tabel', 'Rând', 'Celulă',
  // Image Appearance, Image Misc, Number tabs
  'Aspect', 'Diverse', 'Numar',
  // Box position, Box size, Box appearance tabs
  'Pozitie', 'Dimensiune', 'Aspect',
  // Preview label, Transparency group-box, checkbox, label
  'Previzualizare:', 'Transparenţă', '&Transparent', 'Culoare transparentă:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Selectare...', '&Salvare...', 'Auto',
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Aliniere verticală', 'Aliniere:',
  'jos după linia de bază a textului', 'centru după linia de bază a textului',
  'sus după linia de sus', 'jos după linia de jos', 'la mijloc după linia de mijloc',
  // align to left side, align to right side
  'partea stângă', 'partea dreaptă',  
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Plasare cu:', 'Scara', 'După lăţime:', 'După înălţime:', 'Mărimea iniţială: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Ajustare &proportionala', '&Resetare',
  // Inside the border, border, outside the border groupboxes
  'În interiorul chenarului', 'Chenar', 'În exteriorul chenarului',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Culoare de umplere:', '&Completare:',
  // Border width, Border color labels
  'Latime &chenar:', 'Chenar &culoare:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Interval pe orizontala:', '&interval pe Verticala:',
  // Miscellaneous groupbox, Tooltip label
  'Diverse', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Text de înlocuire:',
  // Horz line group-box, color, width, style
  'Linie orizontală', '&Culoare:', '&Grosime:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing
  'Tabel', '&Lăţime:', '&Culoare umplere:', '&Interval dintre celule...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Completare celula pe orizontala:', '&Completare celula pe verticala:', 'Chenar si fundal',
  // Visible table border sides button, auto width (in combobox), auto width label
  '&Laturi chenar vizibile...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Pastrare continut împreuna',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotire', '&Niciuna', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Chenar:', '&Laturi chenar vizibile...',
  // Border, CellBorders buttons
  '&Chenar tabel...', 'Chenar celule...',
  // Table border dialog title
  'Chenar tabel', 'Chenar celule predefinit',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tipar', 'Interzicere trecere rânduri pe pagină nouă', 'Nr. de rânduri în titlu:',
  'Rândurile titlului repetate pe fiecare pagină a tabelului',
  // top, center, bottom, default
  'Sus', 'La centru', 'Jos', 'Predefinit',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Parametri celule', 'Lăţime aproximativă:', '&Înălţime mai mult de:', 'Culoare umplere:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Chenar', 'Laturile vizibile:', 'Culoare întunecată:', 'Culoare aprinsă', 'Culoare:',
  // Background image
  'Imagine...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Pozitie orizontala', 'Pozitie Verticala', 'Pozitie în text',
  // Position types: align, absolute, relative
  'Aliniere', 'Pozitie absoluta', 'Pozitie relativa',
  // [Align] left side, center, right side
  'latura stânga a casetei spre latura stânga a',
  'centrul casetei spre centrul',
  'latura dreapta a casetei spre latura dreapta a',
  // [Align] top side, center, bottom side
  'latura de sus a casetei spre latura de sus a',
  'centrul casetei spre centrul',
  'latura de jos a casetei spre latura de jos a',
  // [Align] relative to
  'relativa fata de',
  // [Position] to the right of the left side of
  'spre dreapta platurii din stânga a',
  // [Position] below the top side of
  'sub latura de sus a',
  // Anchors: page, main text area (x2)
  '&Pagina', '&Zona text principal', 'P&agina', 'Zona te&xt principal',
  // Anchors: left margin, right margin
  '&Margine stânga', '&Margine dreapta',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Margine de sus', '&Margine de jos', '&Margine interioara', '&Margine exterioara',
  // Anchors: character, line, paragraph
  '&Caracter', 'L&inie', 'Para&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Margini în oglinda', 'Aranjarea în celula tabelului',
  // Above text, below text
  '&Text deasupra', '&Text dedesubt',
  // Width, Height groupboxes, Width, Height labels
  'Latime', 'Înaltime', '&Latime:', '&Înaltime:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Aliniere verticala', '&Sus', '&Centru', '&Jos',
  // Border and background button and title
  'C&henar si fundal...', 'Chenar si fundal caseta',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Inserare specială', '&Ca:', 'Format RTF', 'Format HTML',
  'Text neformatat', 'Text în codul Unicode', 'Imagine în puncte (BMP)', 'Metafişier (WMF)',
  'Fişiere grafice', 'URL',
  // Options group-box, styles
  'Opţiuni', '&Stiluri:',
  // style options: apply target styles, use source styles, ignore styles
  'aplicare stiluri document ţintă', 'păstrare stiluri şi înfăţişare',
  'păstrare înfăţişare, ignorare stiluri',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Listă', 'Marcat', 'Numerotat', 'Marcaj de listă', 'Listă numerotată',
  // Customize, Reset, None
  '&Modifică...', '&Anulare', 'Nu',
  // Numbering: continue, reset to, create new
  'continuare numerotare', 'început numerotare de la ', 'creare listă nouă de la',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Litere mari', 'Numere romane, caractere mari', 'Numere întregi', 'Litere mici', 'Numere romane, caractere mici',
  // Bullet type
  'Cerc', 'Disc', 'Pătrat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nivel:', '&Începere de la:', '&Continuare', 'Numerotare',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  'Modificare listă', 'Nivele', '&Număr:', 'Proprietăţi listă', '&Tip listă:',
  // List types: bullet, image
  'marcaj', 'imagine', 'Inserare număr|',
  // Number format, Number, Start level from, Font button
  '&Format număr:', 'Număr', '&Numerotare nivel de la:', '&Font...',
  // Image button, bullet character, Bullet button,
  '&Imagine...', '&Simbol marcaj', 'Simbol...',
  // Position of list text, bullet, number, image
  'Poziţie marcaj', 'Poziţie marcaj', 'Poziţie număr', 'Poziţie imagine', 'Poziţie text',
  // at, left indent, first line indent, from left indent
  '', '&Alineat din stânga:', '&Primul rând:', 'de la alineatul din stânga',
  // [only] one level preview, preview
  '&Previzualizare numai un nivel', 'Model',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'la dreapta de', 'la stânga de', 'la centru',
  // level #, this level (for combo-box)
  'Nivel %d', 'Acest nivel',
  // Marker character dialog title
  'Alegere simbol marcaj',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Chenar şi umplere paragraf', 'Chenar', 'Umplere',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Tip chenar', '&Culoare:', 'Grosime:', '&Spaţiu:', '&Deplasamente...',
  // Sample, Border type group-boxes;
  'Model', 'Tip chenar',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Fără', 'Unic', '&Dublu', '&Triplu', 'Îngroşat intern', 'Îngroşat extern',
  // Fill color group-box; More colors, padding buttons
  'Culoare umplere', '&Altă culoare...', '&Câmpuri...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box în Offsets dialog
  'Deplasamente', 'Deplasamente între text şi chenar',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Alineat', 'Aliniere', '&Stânga', '&Dreapta', 'La centru', 'După lăţime',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Intervale', 'Până la:', 'După:', '&Între rânduri:', '&Valoare:',
  // Indents group-box; indents: left, right, first line
  'Deplasamente', 'Stânga:', 'Dreapta:', 'Primul rând:',
  // indented, hanging
  'Indentat', 'Agăţat', 'Model',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Unic', '1,5', 'Dublu', 'Cel puţin', 'Exact', 'Multiplu',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Alineate şi intervale', 'Tabulare', 'Plasare pe pagină',
  // tab stop position label; buttons: set, delete, delete all
  'Poziţie tabulare:', '&Setare', 'Şterge', 'Şterge toate',
  // tab align group; left, right, center aligns,
  'Aliniere', 'Stânga', 'Dreapta', 'La centru',
  // leader radio-group; no leader
  'Umplere', '(Fără)',
  // tab stops to be deleted, delete none, delete all labels
  'Vor fi şterse:', '', 'Toate.',
  // Pagination group-box, keep with next, keep lines together
  'Separare pe pagini', 'Lipire cu următoarea', 'Lipire linii paragraf',
  // Outline level, Body text, Level #
  '&Nivel schiţă:', 'Text principal', 'Nivel %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Previzualizare', 'După lăţimea paginii', 'Pagina întreagă', 'Pagini:',
  // Invalid Scale, [page #] of #
  'Indicaţi un număr de la 10 până la 500', 'din %d',
  // Hints on buttons: first, prior, next, last pages
  'Prima', 'Precedenta', 'Următoarea', 'Ultima',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Intervale', 'Intervale', 'Între &celule', 'Între chenarul tabelului şi celule',
  // vertical, horizontal (x2)
  '&Verticală:', '&Orizontală:', 'V&erticală:', 'O&rizontală:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Chenare', 'Parametri', '&Culoare:', '&Culoare închisă:', 'Culoare deschisă:',
  // Width; Border type group-box;
  '&Grosime:', 'Tip chenar',
  // Border types: none, sunken, raised, flat
  '&Fără', '&înăuntru', 'În afară', '&Plată',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Divizare celule', 'Divizare în',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&nr. indicat de rânduri şi coloane', '&Celule iniţiale',
  // number of columns, rows, merge before
  'Nr. de coloane:', 'Nr. de linii:', '&Grupare până la divizare',
  // to original cols, rows check-boxes
  'Divizare în coloane iniţiale', 'Divizare în linii iniţiale',
  // Add Rows form -------------------------------------------------------------
  'Adăugare linii', '&Nr. de linii:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Parametri pagină', 'Pagină', 'Antet&Subsol',
  // margins group-box, left, right, top, bottom
  'Câmpuri (mm)', 'Câmpuri (dm)', '&Stânga:', '&Sus:', '&Dreapta:', '&Jos:',
  // mirror margins check-box
  '&Câmpuri proporţionale',
  // orientation group-box, portrait, landscape
  'Orientare', '&Verticală', '&Orizontală',
  // paper group-box, paper size, default paper source
  'Hârtie', '&Dimensiune:', 'Sursă:',
  // header group-box, print on the first page, font button
  'Antet', 'Text:', 'Tipărire pe prima pagină', 'Font...',
  // header alignments: left, center, right
  'Stânga', 'La centru', 'Dreapta',
  // the same for footer (different hotkeys)
  'Subsol de pagină', 'Text:', 'Tipărire pe prima pagină', 'Font...',
  'Stânga', 'La centru', 'Dreapta',
  // page numbers group-box, start from
  'Numerotare pagini', 'Început cu:',
  // hint about codes
  'Aveţi posibilitate să folosi combinaţii speciale:'#13'&&p - nr. paginii; &&P - nr. de pagini; &&d - dată; &&t - timpul.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Pagină cod fişier text', '&Alegeţi codarea fişierului:',
  // thai, japanese, chinese (simpl), korean
  'Thailandeză', 'Japoneză', 'Chineză (Simplificatăd)', 'Coreeană',
  // chinese (trad), central european, cyrillic, west european
  'Chineză (Tradiţională)', 'limbi din Europa Centrală şi Estică', 'Alfabetul Chirilic', 'limbi vest-europene',
  // greek, turkish, hebrew, arabic
  'Greacă', 'Turcă', 'Ebraică', 'Arabă',
  // baltic, vietnamese, utf-8, utf-16
  'Limbi Baltice', 'Vietnameză', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Stil vizual', '&Selectare stil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertire în text', 'Alegeţi &delimitatorul:',
  // line break, tab, ';', ','
  'Întrerupere linie', 'tabulator', 'punct şi virgulă', 'virgulă',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabelul cu rânduri unite nu poate fi sortat',
  // title, main options groupbox
  'Sortare tabel', 'Sortare',
  // sort by column, case sensitive
  '&Sortare după coloană:', '&Sensibil la litere mari şi mici',
  // heading row, range or rows
  'Excludere &rând antet', 'Rânduri de sortat: de la %d la %d',
  // order, ascending, descending
  'Ordine', '&Ascendentă', '&Descendentă',
  // data type, text, number
  'Tip', '&Text', '&Număr',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserare numar', 'Proprietati', '&Denumire contor:', '&Tip numaratoare:',
  // numbering groupbox, continue, start from
  'Numaratoare', 'C&ontinuare', '&Începere de la:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Legenda', '&Eticheta:', '&Excludere eticheta din legenda',
  // position radiogroup
  'Pozitie', '&Deasupra obiectului selectat', '&Sub obiectul selectat',
  // caption text
  '&Text legenda:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numaratoare', 'Figura', 'Tabel',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Laturi chenar vizibile', 'Chenar',
  // Left, Top, Right, Bottom checkboxes
  '&Latura stânga', '&Latura de sus', '&Latura dreapta', '&Latura de jos',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Modificare lăţimea coloanei tabelului', 'Modificare înălţimea liniei tabelului',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Alineat primul rând', 'Alineat stanga', 'Atârnare', 'Alineat dreapta',
  // Hints on lists: up one level (left), down one level (right)
  'Micşorare nivel listă', 'Mărire  nivel listă',
  // Hints for margins: bottom, left, right and top
  'Câmpul de jos', 'Câmpul din stânga', 'Câmpul din dreapta', 'Câmpul de sus',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'După chenarul din stânga', 'După chenarul din dreapta', 'La centru', 'După separator',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Alineat normal', 'Fără spaţiere', 'Antet %d', 'Listare paragraf',
  // Hyperlink, Title, Subtitle
  'Hiperlegătură', 'Titlu', 'Subtitlu',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Accentuare', 'Accentuare subtilă', 'Accentuare intensă', 'Puternică',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Ghilimele', 'Ghilimele intense', 'Referinţă subtilă', 'Referinţă intensă',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Text bloc', 'Variabilă HTML', 'Cod HTML', 'Acronim HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definiţie HTML', 'Tastatură HTML', 'Exemplu HTML', 'Maşină de scris HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatat', 'HTML Cite', 'Antet', 'Subsol', 'Număr pagină',
  // Caption
  'Legenda',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Referinţă notă de final', 'Referinţă notă de subsol', 'Text notă de final', 'Text notă de subsol',
  // Sidenote Reference, Sidenote Text
  'referinta nota laterala', 'text nota laterala',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'culoare', 'culoare fundal', 'transparent', 'prestabilit', 'culoare subliniere',
  // default background color, default text color, [color] same as text
  'culoare fundal prestabilită', 'culoare text prestabilită', 'la fel ca textul',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'o singură linie', 'îngroşat', 'cu două linii', 'linie punctată', 'linie punctată îngroşată', 'linie întreruptă',
  // underline types: thick dashed, long dashed, thick long dashed,
  'linie întreruptă îngroşată', 'linie întreruptă continuă', 'linie întreruptă îngroşată',
  // underline types: dash dotted, thick dash dotted,
  'linie întreruptă şi punctată', 'linie întreruptă şi punctată îngroşată',
  // underline types: dash dot dotted, thick dash dot dotted
  'linie punctată dublu şi întreruptă', 'linie punctată dublu şi întreruptă îngroşată',
  // sub/superscript: not, subsript, superscript
  'fără indice/exponent', 'indice', 'exponent',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'mod bi-di:', 'moştenit', 'de la stânga la dreapta', 'de la dreapta la stânga',
  // bold, not bold
  'aldin', 'non aldin',
  // italic, not italic
  'cursiv', 'non cursiv',
  // underlined, not underlined, default underline
  'subliniere', 'nesubliniere', 'subliniere prestabilită',
  // struck out, not struck out
  'tăiere cu o linie', 'fără tăiere cu o linie',
  // overlined, not overlined
  'Cu liniuţă deasupra', 'fără liniuţă deasupra',
  // all capitals: yes, all capitals: no
  'toate cu majuscule', 'majusculele dezactivate',
  // vertical shift: none, by x% up, by x% down
  'fără deplasare verticală', 'deplasare cu %d%% în sus', 'deplasare cu %d%% în jos',
  // characters width [: x%]
  'lăţime caractere',
  // character spacing: none, expanded by x, condensed by x
  'spaţiere normală caractere', 'spaţiere extinsă cu %s', 'spaţiere condensată cu %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'font prestabilit', 'paragraf prestabilit', 'moştenit',
  // [hyperlink] highlight, default hyperlink attributes
  'evidenţiere', 'prestabilit',
  // paragraph aligmnment: left, right, center, justify
  'aliniere la stânga', 'aliniere la dreapta', 'în centru', 'aliniere stânga-dreapta',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'înălţime linie: %d%%', 'spaţiu între linii: %s', 'înălţime linie: cel puţin %s',
  'înălţime linie: exact %s',
  // no wrap, wrap
  'Rupere rânduri dezactivată', 'rupere rânduri',
  // keep lines together: yes, no
  'păstrează liniile grupate', 'nu păstrează liniile grupate',
  // keep with next: yes, no
  'păstrează în paragraful următor', 'nu păstrează în paragraful următor',
  // read only: yes, no
  'doar citire', 'editabil',
  // body text, heading level x
  'nivel schiţă: text principal', 'nivel schiţă: %d',
  // indents: first line, left, right
  'indentare prima linie', 'indentare la stânga', 'indentare la dreapta',
  // space before, after
  'spaţiu înainte', 'spaţiu după',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulatori', 'niciunul', 'aliniere', 'stânga', 'dreapta', 'centru',
  // tab leader (filling character)
  'caracter de ghidare',
  // no, yes
  'nu', 'da',
  // [padding/spacing/side:] left, top, right, bottom
  'stânga', 'sus', 'dreapta', 'jos',
  // border: none, single, double, triple, thick inside, thick outside
  'niciunul', 'singur', 'dublu', 'triplu', 'gros în interior', 'gros în exterior',
  // background, border, padding, spacing [between text and border]
  'fundal', 'chenar', 'completare', 'spaţiere',
  // border: style, width, internal width, visible sides
  'stil', 'lăţime', 'lăţime internă', 'laterale vizibile',
  // style inspector -----------------------------------------------------------
  // title
  'Inspector de Stil',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Niciunul)', 'Paragraf', 'Font', 'atribute',
  // border and background, hyperlink, standard [style]
  'Chenar şi fundal', 'Hiperlegătură', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stiluri', 'Stil',
  // name, applicable to,
  '&Nume:', 'Aplicabil &la:',
  // based on, based on (no &),
  '&Bazat pe:', 'Bazat pe:',
  //  next style, next style (no &)
  'Stil pentru &următorul paragraf:', 'Stil pentru următorul paragraf:',
  // quick access check-box, description and preview
  '&Acces rapid', 'Descriere şi &previzualizare:',
  // [applicable to:] paragraph and text, paragraph, text
  'paragraf şi text', 'paragraf', 'text',
  // text and paragraph styles, default font
  'Stiluri Text şi paragraf', 'Font prestabilit',
  // links: edit, reset, buttons: add, delete
  'Editare', 'Resetare', '&Adăugare', '&Ştergere',
  // add standard style, add custom style, default style name
  'Adăugare &stil Standard...', 'Adăugare &stil personalizat', 'Stil %d',
  // choose style
  'Alegere &stil:',
  // import, export,
  '&Import...', '&Export...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Stiluri RichView (*.rvst)|*.rvst', 'Eroare la încărcarea fişierului', 'Acest fişier nu conţine stiluri',
  // Title, group-box, import styles
  'Importare stiluri din fişier', 'Importare', 'I&mportare stiluri:',
  // existing styles radio-group: Title, override, auto-rename
  'Stiluri existente', '&Suprascriere', '&adăugare redenumire',
  // Select, Unselect, Invert,
  '&Selectare', '&Deselectare', '&Inversare',
  // select/unselect: all styles, new styles, existing styles
  '&Toate stilurile', '&Stiluri noi', '&Stiluri existente',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Ştergere Format)', '(Toate stilurile)',
  // dialog title, prompt
  'Stiluri', '&Alegeţi stilul de aplicare:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  'Sinonime', 'Ignorare toate', 'Adăugare în dic?ionar',
  // Progress messages ---------------------------------------------------------
  'Se descarcă %s', 'Se pregăteşte pentru tipărire...', 'Se tipăreşte pagina %d',
  'Se converteşte din RTF...',  'Se converteşte în RTF...',
  'Se citeşte %s...', 'se scrie %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Fara nota', 'Nota de subsol', 'Nota de final', 'Nota laterala', 'Caseta Text'
  );


initialization
  RVA_RegisterLanguage(
    'Romanian', // english language name, do not translate
    'Româna', // native language name
    EASTEUROPE_CHARSET, @Messages);

end.
