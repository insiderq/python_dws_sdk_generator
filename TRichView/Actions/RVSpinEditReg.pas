
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Design-time support for TRVSpinEdit             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVSpinEditReg;

interface

uses
  Classes, RVSpinEdit;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('RichView Misc', [TRVSpinEdit]);
end;

end.
