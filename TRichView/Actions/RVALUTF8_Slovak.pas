﻿// This file is a copy of RVAL_Slovak.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Slovak translation                              }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Miroslav Štipkala 2003-05-20           }
{                znalec@znalec.sk                       }
{                www.znalec.sk                          }
{                                                       }
{ Updated: 2006-12-16                                   }
{ Updated: 2011-10-17                                   }
{*******************************************************}
{ Translated by: RoTurSoft, s.r.o. 2013-10-09           }
{                info@rotursoft.sk                      }
{                www.rotursoft.sk                       }
{ Updated: 2014-03-20                                   }
{*******************************************************}

unit RVALUTF8_Slovak;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'palce(Inch)', 'cm', 'mm', 'Pica', 'Pixely', 'Body',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Súbor', 'Úpr&avy', '&Formát', '&Písmo', '&Odstavec', '&Vložiť', '&Tabuľka', '&Okno', '&Nápoveda',
  // exit
  '&Koniec',
  // top-level menus: View, Tools,
  '&Zobraziť', '&Nástroje',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Veľkosť', 'Štýl', 'Vybr&ať', 'Zarovnana&nie obsahu bunky', 'Ohranič&enie bunky',
  // menus: Table cell rotation
  '&Rotácia bunky',   
  // menus: Text flow, Footnotes/endnotes
  'Obtekať &Text', '&Poznámky pod čarou',
  // ribbon tabs: tab1, tab2, view, table
  '&Domov', '&Pokročilé', '&Zobrazenie', '&Tabuľka',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Schránka', 'Písmo', 'Odstavec', 'Zoznamy', 'Úpravy',
  // ribbon groups: insert, background, page setup,
  'Vložiť', 'Pozadie', 'Stránka nastavenie',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Odkazy', 'Poznámky pod čiarou', 'Zobrazenie dokumentov', 'Zobraziť/Skryť', 'Lupa', 'Nastavenie',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Vložiť', 'Odstrániť', 'Úpravy', 'Ohraničenie',
  // ribbon groups: styles 
  'Štýly',
  // ribbon screen tip footer,
  'Stlačte F1 pre nápovedu',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Naposledy použité', 'Uložiť ako', 'Náhľad a tlač dokumentu',
  // ribbon label: units combo
  'Jednotky:',          
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nový', 'Nový|Vytvorí nový dokument',
  // TrvActionOpen
  '&Otvoriť...', ' Otvoriť |Otvorí existujúci dokument',
  // TrvActionSave
  '&Uložiť', 'Uložiť|Uloží dokument',
  // TrvActionSaveAs
  'Ul&ožiť ako...', 'Uložiť ako...|Uloží dokument pod novým názvom',
  // TrvActionExport
  '&Export...', 'Export|Export dokumentu do súboru iného formátu',
  // TrvActionPrintPreview
  'Ná&hľad', 'Náhľad|Zobrazí dokument v tvare, v akom bude vytlačený',
  // TrvActionPrint
  '&Tlač...', 'Tlač|Vytlačí dokument na tlačiareň',
  // TrvActionQuickPrint
  '&Rýchla tlač', 'Rýchla tlač', 'Tlač|Vytlačí dokument na tlačiareň',
   // TrvActionPageSetup
  '&Vzhľad stránky', 'Vzhľad stránky|Nastaví okraje dokumentu, veľkosť papiera, orientáciu, zdroj, záhlavie a päta',
  // TrvActionCut
  'V&ystrihnúť', 'Vystrihnúť|Vystrihne vybraný text, alebo objekt a vloží ho do schránky',
  // TrvActionCopy
  '&Kopírovať', 'Kopírovať|Skopíruje vybraný text alebo objekt do schránky',
  // TrvActionPaste
  '&Prilepiť', 'Prilepiť|Vloží text alebo objekt zo schránky na vybrané miesto',
  // TrvActionPasteAsText
  'Prilepiť ako &Text', 'Prilepiť ako Text|Vloží obsah schránky ve formáte jednoduchého textu',  
  // TrvActionPasteSpecial
  'Prilepiť š&peciálne...', 'Prilepiť špeciálne|Vloží obsah schránky v špeciálnom formáte',
  // TrvActionSelectAll
  '&Vybrať všetko', 'Vybrať všetko|Vyberie všetky položky dokumentu',
  // TrvActionUndo
  '&Späť', 'Späť|Vráti späť poslednú akciu',
  // TrvActionRedo
  '&Znovu', 'Znovu|Prevedie znovu poslednú vrátenú akciu',
  // TrvActionFind
  '&Hľadať...', 'Hľadať|Vyhľadá textový reťazec v dokumente',
  // TrvActionFindNext
  '&Hľadať ďalej', 'Hľadať ďalej|Vyhľadá nasledujúci výskyt hľadaného textu v dokumente',
  // TrvActionReplace
  '&Nahradiť...', 'Nahradiť|Nahradí hľadaný text v dokumente iným textom',
  // TrvActionInsertFile
  '&Súbor...', 'Vložiť Súbor|Vloží obsah Súboru do dokumentu',
  // TrvActionInsertPicture
  '&Obrázok...', 'Vložiť obrázok|Vloží obrázok do dokumentu',
  // TRVActionInsertHLine
  'Od&deľovač', 'Vložiť oddeľovač|Vloží vodorovnú oddeľovaciu čiaru',
  // TRVActionInsertHyperlink
  'H&ypertextový odkaz...', 'Vložiť hypertextový odkaz|Vloží hypertextový odkaz na WWW stránku alebo Súbor',
  // TRVActionRemoveHyperlinks
  'O&dstrániť hypertextové odkazy', 'Odstrániť hypertextové odkazy|Odstráni všetky hypertextové odkazy z označeného textu',  
  // TrvActionInsertSymbol
  '&Symbol...', 'Vložiť symbol|Vloží špeciálny znak výberom z tabuľky znakov',
  // TrvActionInsertNumber
  '&Číslo...', 'Vložiť číslo|Vložiť číslo',
  // TrvActionInsertCaption
  'Vložiť &popis...', 'Vložiť popis|Vložiť popis pre zvolený objekt',
  // TrvActionInsertTextBox
  '&Textová oblasť', 'Vložiť textovú oblasť|Vložiť textovú oblasť',
  // TrvActionInsertSidenote
  '&Bočná poznámka', 'Vložiť bočnú poznámku|Vloží poznámku do textového poľa zobrazeného na bočnej strane',
  // TrvActionInsertPageNumber
  'Číslo stran&y', 'Vložiť číslo strany|Vloží číslo strany',
  // TrvActionParaList
  'Od&rážky a číslovánie...', 'Odrážky a číslovánie|Definície štýlu odrážok a číslovania vybraného odstavca',
  // TrvActionParaBullets
  'Od&rážky', 'Odrážky|Vloží alebo odoberie odrážku pre vybraný odstavec',
  // TrvActionParaNumbering
  'Čís&lovanie', 'Číslovanie|Vloží alebo odoberie číslovanie pre vybraný odstavec',
  // TrvActionColor
  '&Farba pozadia dokumentu...', 'Farba pozadia dokumentu|Zmení farbu pozadia dokumentu',
  // TrvActionFillColor
  'Farba &pozadia...', 'Farba pozadia|Zmení farbu pozadia textu, odstavca, bunky, tabuľky, alebo dokumentu',
  // TrvActionInsertPageBreak
  '&Vložiť oddeľovač stránky', 'Vložiť oddeľovač stránky|Vloží oddeľovač stránky',
  // TrvActionRemovePageBreak
  '&Odstrániť oddeľovač stránky', 'Odstrániť oddeľovač stránky|Odstráni oddeľovač stránky',
  // TrvActionClearLeft
  'Neobtekať text z &ľava', 'Neobtekať text zľava|Umiestni odstavec pod akýkoľvek obrázok zarovnaný vľavo',
  // TrvActionClearRight
  'Neobtekať text z &prava', 'Neobtekať text zprava|Umiestni odstavec pod akýkoľvek obrázok zarovnaný vpravo',
  // TrvActionClearBoth
  'Neo&btekať text zľava ani zprava', 'Neobtekať text zľava ani zprava|Umiestni odstavec pod akýkoľvek obrázok zarovnaný vľavo alebo vpravo',
  // TrvActionClearNone
  '&Normálne obtekanie textu', 'Normálne obtekanie textu|Umožní obtekanie textu okolo obrázkov zarovnaných vľavo aj vpravo',
  // TrvActionVAlign
  'Po&zícia objektu...', 'Pozícia objektu|Zmení pozíciu vybraného objektu',    
  // TrvActionItemProperties
  '&Vlastnosti objektu...', 'Vlastnosti objektu|Nastaví vlastnosti vybraného objektu',
  // TrvActionBackground
  '&Pozadie dokumentu...', 'Pozadie dokumentu|Nastaví obrázok, farbu pozadia dokumentu',
  // TrvActionParagraph
  '&Odstavec...', 'Odstavec|Nastavenie vlastností vybraného odstavca',
  // TrvActionIndentInc
  'Z&väčšiť odsadenie', ' Zväčšiť odsadenie| Zväčší vzdialenosť vybraného odstavca od ľavého okraja stránky',
  // TrvActionIndentDec
  'Z&menšiť odsadenie', 'Zmenšiť odsadenie|Zmenší vzdialenosť vybraného odstavca od ľavého okraja stránky',
  // TrvActionWordWrap
  'Za&lamovanie riadku', 'Zalamovanie riadku|Nastaví alebo zruší zalamovanie riadku vybraného odstavca',
  // TrvActionAlignLeft
  'Zarovnať v&ľavo', 'Zarovnať vľavo|Zarovná text vybraného odstavca k ľavému okraju stránky',
  // TrvActionAlignRight
  'Zarovnať v&pravo', 'Zarovnať vpravo|Zarovná text vybraného odstavca k pravému okraju stránky',
  // TrvActionAlignCenter
  'Zarovnať na &stred', 'Zarovnať na stred|Zarovná text vybraného odstavca na stred stránky',
  // TrvActionAlignJustify
  'Zarovnať do &bloku', 'Zarovnať do bloku|Zarovná text vybraného odstavca do bloku',
  // TrvActionParaColor
  '&Farba pozadia odstavca...', 'Farba pozadia odstavca|Zmení farbu pozadia vybraného odstavca',
  // TrvActionLineSpacing100
  '&Jednoduché riadkovanie', 'Jednoduché riadkovanie |Nastaví jednoduché riadkovanie odstavca',
  // TrvActionLineSpacing150
  '&Riadkovanie 1,5 riadku ', ' Riadkovanie 1.5 riadku|Nastaví riadkovanie odstavca na výšku 1.5 násobku výšky riadku',
  // TrvActionLineSpacing200
  '&Dvojité riadkovanie', 'Dvojité riadkovanie|Nastaví riadkovanie odstavca na dvojitú výšku riadku',
  // TrvActionParaBorder
  '&Ohraničenie a pozadie odstavca...', 'Ohraničenie a pozadie odstavca|Nastaví ohraničenie a pozadie vybraného odstavca',
  // TrvActionInsertTable
  '&Vložiť tabuľku...', '&Tabuľka', 'Vložiť tabuľku|Vloží novú tabuľku',
  // TrvActionTableInsertRowsAbove
  'Vložiť &riadok pred', 'Vložiť riadok pred|Vloží nový riadok do tabuľky pred vybranú bunku',
  // TrvActionTableInsertRowsBelow
  'Vložiť riadok &za', 'Vložiť riadok za|Vloží nový riadok do tabuľky za vybranú bunku',
  // TrvActionTableInsertColLeft
  'Vložiť stĺpec v&ľavo', 'Vložiť stĺpec vľavo|Vloží stĺpec vľavo od vybranej bunky',
  // TrvActionTableInsertColRight
  'Vložiť stĺpec v&pravo', 'Vložiť stĺpec vpravo|Vloží stĺpec vpravo od vybranej bunky',
  // TrvActionTableDeleteRows
  'Odstrániť r&iadky', 'Odstrániť riadky|Odstráni vybrané riadky z tabuľky',
  // TrvActionTableDeleteCols
  'Odstrániť &stĺpce', 'Odstrániť stĺpce|Odstráni vybrané stĺpce z tabuľky',
  // TrvActionTableDeleteTable
  '&Odstrániť tabuľku', 'Odstrániť tabuľku|Odstráni tabuľku',
  // TrvActionTableMergeCells
  '&Zlúčit bunky', 'Zlúčiť bunky|Zlúči vybrané bunky do jednej',
  // TrvActionTableSplitCells
  '&Rozdeliť bunku...', 'Rozdeliť bunku|Rozdelí bunku na požadovaný počet buniek',
  // TrvActionTableSelectTable
  '&Vybrať tabuľku', 'Vybrať tabuľku|Vyberie celú tabuľku',
  // TrvActionTableSelectRows
  'Vybrať &riadok', 'Vybrať riadok|Urobí výber celého riadku',
  // TrvActionTableSelectCols
  'Vybrať &stĺpec', 'Vybrať stĺpec| Urobí výber celého stĺpca',
  // TrvActionTableSelectCell
  'Vybrať &bunku', 'Vybrať bunku| Urobí výber celej bunky',
  // TrvActionTableCellVAlignTop
  'Zarovnať bunku &hore', 'Zarovnať bunku hore|Zarovná zobrazovaný text v bunke k jej hornému okraju',
  // TrvActionTableCellVAlignMiddle
  'Zarovnať bunku na &stred', 'Zarovnať bunku na stred|Zarovná zobrazovaný text v bunke na stred',
  // TrvActionTableCellVAlignBottom
  'Zarovnať bunku &dolu', 'Zarovnať bunku dolu|Zarovná zobrazovaný text v bunke k jej dolnému okraju',
  // TrvActionTableCellVAlignDefault
  'Štandartné &zarovnanie bunky', ' Štandartné zarovnanie bunky|Nastaví štandartné zvislé zarovnanie bunky',
  // TrvActionTableCellRotationNone
  '&Bez rotácie bunky', 'Bez rotácie bunky|Rotácia obsahu bunky o 0°',
  // TrvActionTableCellRotation90
  'Rotácia bunky o &90°', 'Rotácia bunky o 90°|Rotácia obsahu bunky o 90°',
  // TrvActionTableCellRotation180
  'Rotácia bunky o &180°', 'Rotácia bunky o 180°|Rotácia obsahu bunky o 180°',
  // TrvActionTableCellRotation270
  'Rotácia bunky o &270°', 'Rotácia bunky o 270°|Rotácia obsahu bunky o 270°',
  // TrvActionTableProperties
  '&Vlastnosti tabuľky...', 'Vlastnosti tabuľky|Nastaví vlastnosti tabuľky',
  // TrvActionTableGrid
  'Zobraziť &mriežku', 'Zobrazit mriežku|Zobrazí alebo skryje mriežku tabuľky',
  // TRVActionTableSplit
  'Roz&deliť tabuľku', 'Rozdeliť tabuľku|Rozdelí tabuľku na dve tabuľky od vybraného riadku',
  // TRVActionTableToText
  'Prevod do te&xtu...', 'Prevod do textu |Prevedie tabuľku na text',
  // TRVActionTableSort
  '&Zoradiť...', 'Zoradiť|Zoradí riadky v tabuľke',
  // TrvActionTableCellLeftBorder
  'Ohraničenie v&ľavo', 'Ohraničenie vľavo|Zobrazí, alebo skryje ľavé ohraničenie bunky',
  // TrvActionTableCellRightBorder
  'Ohraničenie v&pravo', 'Ohraničenie vpravo|Zobrazí, alebo skryje pravé ohraničenie bunky',
  // TrvActionTableCellTopBorder
  'Ohraničenie &hore', 'Ohraničenie hore|Zobrazí, alebo skryje horné ohraničenie bunky',
  // TrvActionTableCellBottomBorder
  'Ohraničenie &dole', 'Ohraničenie dole|Zobrazí, alebo skryje spodné ohraničenie bunky',
  // TrvActionTableCellAllBorders
  'Ohraničenie d&okola', 'Ohraničenie dokola|Zobrazí, alebo skryje ohraničenie celej bunky',
  // TrvActionTableCellNoBorders
  '&Bez ohraničenia', 'Bez ohraničenia|Skryje celé ohraničenie vybranej bunky',
  // TrvActionFonts & TrvActionFontEx
  '&Písmo...', 'Písmo|Zmena nastavenia písma vybraného textu',
  // TrvActionFontBold
  '&Tučné', 'Tučné|Nastaví, alebo zruší tučné zobrazovanie vybraného textu',
  // TrvActionFontItalic
  '&Kurzíva', 'Kurzíva|Nastaví, alebo zruší zobrazovanie vybraného textu kurzívou',
  // TrvActionFontUnderline
  'Po&dčiarknuté', 'Podčiarknuté|Nastaví, alebo zruší podčiarknutie vybraného textu',
  // TrvActionFontStrikeout
  'P&reškrtnuté', 'Preškrtnuté|Nastaví, alebo zruší preškrtnutie vybraného textu',
  // TrvActionFontGrow
  'Z&väčšiť písmo', 'Zväčšiť písmo|Zväčší výšku vybraného textu o 10%',
  // TrvActionFontShrink
  'Z&menšiť písmo', 'Zmenšiť písmo|Zmenší výšku vybraného textu o 10%',
  // TrvActionFontGrowOnePoint
  'Zv&äčšiť písmo o 1 bod', 'Zväčšit písmo o 1 bod|Zväčší výšku vybraného textu o 1 bod',
  // TrvActionFontShrinkOnePoint
  'Z&menšiť písmo o 1 bod', 'Zmenšiť písmo o 1 bod|Zmenší výšku vybraného textu o 1 bod',
  // TrvActionFontAllCaps
  '&Všetky veľké', 'Všetky veľké|Zmení štýl písma vybraného textu na všetky veľké znaky',
  // TrvActionFontOverline
  'Nad&čiarknuté', ' Nadčiarknuté|Nastaví, alebo zruší vodorovnú linku nad vybraným textom',
  // TrvActionFontColor
  '&Farba písma...', 'Farba písma|Zmení farbu vybraného textu',
  // TrvActionFontBackColor
  'Farba &pozadia písma...', 'Farba pozadia písma|Zmení farbu pozadia vybraného textu',
  // TrvActionAddictSpell3
  '&Kontrola pravopisu', 'Kontrola pravopisu|Prevedie kontrolu pravopisu vybraného textu',
  // TrvActionAddictThesaurus3
  '&Synonymá', 'Synonyma|Ponúkne synonyma pre vybrané slovo',
  // TrvActionParaLTR
  'Zľava do prava', 'Zľava do Prava|Nastaví smer textu zľava -do- prava',
  // TrvActionParaRTL
  'Zprava do ľava', 'Zprava do Ľava|Nastaví smer textu zprava -do- ľava',
  // TrvActionLTR
  'Text zľava do prava', 'Text zľava do prava|Nastaví smer textu zľava -do- prava pre označený text',
  // TrvActionRTL
  'Text zprava do ľava', 'Text zprava do ľava|Nastaví smer textu zprava -do- ľava pre označený text',
  // TrvActionCharCase
  'Veľké / malé písmená', 'Veľké / malé písmená|Zmení veľkosť písmen vybraného textu',
  // TrvActionShowSpecialCharacters
  '&Netlačiteľné znaky', 'Netlačiteľné znaky|Zobrazit alebo skryť netlačiteľné znaky, napr. znaky odstavcov, znaky tabulátorov a medzery',
  // TrvActionSubscript
  '&Horný index', 'Horný index|Prevedie vybraný text na horný index - subscript',
  // TrvActionSuperscript
  '&Dolný index', 'Dolný index|Prevedie vybraný text na dolný index - superscript',
  // TrvActionInsertFootnote
  '&Poznámka pod čiarou', 'Poznámka pod čiarou|Vloží poznámku pod čiarou',
  // TrvActionInsertEndnote
  '&Vysvetlivka', 'Vysvetlivka|Vloží vysvetlivku',
  // TrvActionEditNote
  'E&ditovať poznámku/vysvetlivku', 'E&ditovať poznámku/vysvetlivku|Umožňuje editovať poznámku/vysvetlivku',
  '&Skryť', 'Skryť|Skryje alebo zobrazí označený fragment',            
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Štýly...', 'Štýly|Otvoriť správcu štýlov',
  // TrvActionAddStyleTemplate
  'Prid&ať štýly...', 'Pridať štýly|Vytvoriť nový text alebo odstavcový štýl',
  // TrvActionClearFormat,
  'Vymazať &formát', 'Vymazať formát|Vymazať celý text a formátovanie odseku z vybraného fragmentu',
  // TrvActionClearTextFormat,
  'Vymazať formát &textu', 'Vymazať formátovanie označeného textu',
  // TrvActionStyleInspector
  '&Inšpektor štýlov', 'Inšpektor štýlov|Zobrazí alebo skryje inšpektora štýlov',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Storno', 'Zavrieť', 'Vložiť', '&Otvoriť...', '&Uložiť...', '&Vymazať', 'Nápoveda', 'Odobrať',
  // Others  -------------------------------------------------------------------
  // percents
  'percentá',
  // left, top, right, bottom sides
  'Ľavá strana', 'Horná strana', 'Pravá strana', 'Spodná strana',
  // save changes? confirm title
  'Uložiť urobené zmeny do %s?', 'Potvrdiť',
  // warning: losing formatting
  '%s obsiahnutý objekt, alebo formátovanie nie je podporované vo vybranom formáte súboru.'#13+
  'Naozaj chcete pre uloženie použiť tento formát?',
  // RVF format name
  'RichView Formát',
  // Error messages ------------------------------------------------------------
  'Chyba',
  'Chyba pri načítavaní súboru.'#13#13'Možné príčiny:'#13'- formát otváraného súboru nie je podporovaný v tejto aplikácii;'#13+
  '- Súbor je poškodený;'#13'- Súbor je otvorený a uzamknutý inou aplikáciou.',
  'Chyba pri načítavaní obrázku.'#13#13'Možné príčiny:'#13'- formát obrázku nie je podporovaný touto aplikáciou;'#13+
  '- vybraný súbor neobsahuje obrázok;'#13+
  '- Súbor je poškodený;'#13'- Súbor je otvorený a uzamknutý inou aplikáciou.',
  'Chyba pri ukladaní Súboru.'#13#13'Možné príčiny:'#13'- nie je miesto na disku;'#13+
  '- disk je chránený proti zápisu;'#13'- výmenné médium nie je vložené;'#13+
  '- Súbor je otvorený a uzamknutý inou aplikáciou;'#13'- výmenné médium je poškodené.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Súbory (*.rvf)|*.rvf',  'RTF Súbory (*.rtf)|*.rtf' , 'XML Súbory (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Textové Súbory (*.txt)|*.txt', 'Textové Súbory - Unicode (*.txt)|*.txt', 'Textové Súbory - Autodetect (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Súbory (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - zjednodušený (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Dokumenty Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Hľadanie dokončené', 'Hľadaný reťazec ''%s'' nenájdený.',
  // 1 string replaced; Several strings replaced
  '1 reťazec nahradený.', '%d reťazcov nahradených.',
  // continue search from the beginning/end?
  'Byl dosiahnutý koniec dokumentu, chcete pokračovať od začiatku?',
  'Byl dosiahnutý začiatok dokumentu, chcete pokračovat od konca?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'nie je', 'automatická',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Čierna', 'Hnedá', 'Olivovo zelená', 'Tmavo zelená', 'Tmavo šedo zelená', 'Tmavo modrá', 'Indigovo modrá', 'Šedá-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tmavo červená', 'Oranžová', 'Tmavo žltá', 'Zelená', 'Šedo zelená', 'Modrá', 'Modro šedá', 'Šedá-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Červená', 'Svetlo oranžová', 'Žlto zelená', 'Morská zeleň', 'Akvamarínová', 'Svetlo modrá', 'Fialová', 'Šedá-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Ružová', 'Zlatá', 'Zlatá', 'Jasno zelená', 'Tyrkysová', 'Nebesky modrá', 'Slivková', 'Šedá-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Svetlo ružová', 'Žlto oranžová', 'Svetlo žltá', 'Svetlo zelená', 'Svetlo tyrkysová', 'Bledo modrá', 'Levandulová', 'Biela',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparentná', '&Automatická', '&Viac farieb...', 'Š&tandardná',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Pozadie', 'F&arba:', 'Pozícia obrázku', 'Náhľad', 'Ukážka textu.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  'Žiadny', 'Celé okno', 'Ukotvené dlažd&ice', '&Dlaždice', 'Na &stred',
  // Padding button
  'Odsadenie text&u...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Farba pozadia', 'Použiť n&a:', 'Viac farie&b...', 'Odsadenie text&u...',
  'Prosím, vyberte farbu',
  // [apply to:] text, paragraph, table, cell
  'text', 'odstavec' , 'tabuľku', 'bunku',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Písmo', 'Písmo', 'Preloženie znakov',
  // Font Name, Font Sizo labels, Font Style group-box, Bold, Italic check-boxes
  '&Písmo:', '&Veľkosť:', '&Rez písma', '&Tučné', '&Kurzíva',
  // Script, Color, Back color labels, Default charset
  'Sk&ript:', '&Farba:', 'Poza&die:', '(Štandardné)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Štýl', '&Podčiarknuté', '&Nadčiarknuté', 'P&reškrtnuté', 'Všet&ko veľké',
  // Sample, Sample text
  'Náhľad', 'Ukážkový text',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Medzery medzi znakmi', 'O &koľko:', '&Rozšírené', '&Zúžené',
  // Offset group-box, Offset label, Down, Up,
  'Umiestnenie', 'O koľko:', '&Znížené', 'Z&výšené',
  // Scaling group-box, Scaling
  'Preloženie znakov', 'Me&rítko:',
  // Sub/super script group box, Normal, Sub, Super
  'Horný a dolný index', '&Normálne', 'Su&bscript(Horný index)', 'Su&perscript (Dolný index)',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Odsadenie textu', '&Hore:', 'V&ľavo:', '&Dole:', 'V&pravo:', '&Všetko rovnako',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Vložiť hypertextový odkaz', 'Odkaz', 'T&ext:', 'C&ieľ', '<<výber>>',
  // cannot open URL
  'Nedá sa prejsť na "%s"',
  // hyperlink properties button, hyperlink style
  '&Vlastnosti...', 'Š&týl:', 
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ńolors
  'Vlastnosti hypertextového odkazu', 'Farba odkazu', 'Aktívna farba',
  // Text [color], Background [color]
  '&Text:', '&Pozadie',
  // Underline [color]
  'Podčiak&nuté:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'Po&zadie',
  // Underline [color], Attributes group-box,
  'Podčiark&nuté:', 'Atribúty',
  // Underline type: always, never, active (below the mouse)
  'Podčiarkn&utie:', 'vždy', 'nikdy', 'aktívny',
  // Same as normal check-box
  'Ako &Normálne',
  // underline active links check-box
  'Podčiarkn&úť aktívne odkazy',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Vložiť symbol', '&Písmo:', '&Znaková sada:', 'Unicode',
  // Unicode block
  '&Blok:',  
  // Character Code, Character Unicode Code, No Character
  'Kód znaku: %d', 'Kód znaku: Unicode %d', '(nie je znak)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table sizo group-box, nCols, nRows, 
  'Vložiť tabuľku', 'Veľkosť tabuľky', 'Počet &stĺpcov:', 'Počet &riadkov:',
  // Table layout group-box, Autosizo, Fit, Manual sizo,
  'Šírka tabuľky', '&Automatická', '&Rozťiahnuť podľa okna', '&Manuálne nastavenie',
  // Remember check-box
  'Použiť ako &vzor pre novú tabuľku',
  // VAlign Form ---------------------------------------------------------------
  'Pozícia',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Vlastnosti', 'Obrázok', 'Umiestnenie a veľkosť', 'Čiara', 'Tabuľka', 'Riadok', 'Bunka',
  // Image Appearance, Image Misc, Number tabs
  'Vzhľad', 'Rôzne', 'Čísla',
  // Box position, Box size, Box appearance tabs
  'Pozícia', 'Veľkosť', 'Vzhľad',
  // Preview label, Transparency group-box, checkbox, label
  'Náhľad:', 'Transparentný', '&Transparentný', 'Transparentná &farba:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Zmeniť...', '&Uložiť...', 'Automatická',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Zvislé zarovnávanie', 'Z&arovnávanie:',
  'zarovnať k spodnému okraju textu', 'zarovnať na stred s textom',
  'horná hrana objektu na hornej línii textu', 'dolná hrana objektu na dolnej línii textu', 'stred objektu na stred riadku',
  // align to left side, align to right side
  'ľavá strana', 'pravá strana',      
  // Shift By label, Stretch group box, Width, Height, Default sizo
  '&Posunúť o:', 'Veľkosť', 'Š&írka:', '&Výška:', 'Štandardná veľkosť: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Mierka &proporcionálna', '&Obnoviť',
  // Inside the border, border, outside the border groupboxes
  'Vo vnútri okraju', 'Okraj', 'Zvonkajšej strany okraju',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Farba výplne:', '&Výplň:',
  // Border width, Border color labels
  'Šírka okraja:', 'Farba &okraja:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontálny odstup:', '&Vertikálny odstup:',
  // Miscellaneous groupbox, Tooltip label
  'Rôzne', '&Tooltip:',
  // web group-box, alt text
  'Web', 'Alternatívny &text:',  
  // Horz line group-box, color, width, style
  'Oddeľovacia čiara', '&Farba:', 'Š&írka:', 'Štý&l:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabuľka', 'Š&írka:', '&Farba výplne:', '&Odsadenie bunky...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizontálny odstup buniek:', '&Vertikálny odstup buniek:', 'Okraj a pozadie',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Viditeľné strany &okrajov...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Zachovať obsah spolu',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotácia', 'Ž&iadna', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Okraj:', 'Viditeľné strany &okrajov...',
  // Border, CellBorders buttons
  'Ohraničenie &tabuľky...', 'Ohraničenie &bunky...',
  // Table border, Cell borders dialog titles
  'Ohraničenie tabuľky', 'Ohraničenie bunky',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Tlač', 'Tlač tab&uľky na jednej stránke', 'Počet &hlavičkových riadkov:',
  'Hlavičkové riadky budú opakované na každej stránke tabuľky',
  // top, center, bottom, default
  '&Hore', 'Na &stred', '&Dolu', 'Štanda&rd',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Nastavenie', 'Uprednostňovaná &šírka:', 'Minimálna &výška:', 'Farba v&ýplne:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Ohraničenie', 'Viditeľné strany:',  'Fa&rba tieňa:', 'Farba &svetla', 'F&arba:',
  // Background image
  'Ob&rázok...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontálna pozícia', 'Vertikálna pozícia', 'Pozícia v texte',
  // Position types: align, absolute, relative
  'Zarovnanie', 'Absolútna pozícia', 'Relatívna pozícia',
  // [Align] left side, center, right side
  'ľavá strana oblasti k ľavej strane',
  'stred oblasti k stredu',
  'pravá strana oblasti k pravej strane',
  // [Align] top side, center, bottom side
  'horná strana oblasti k hornej strane',
  'stred oblasti k stredu',
  'spodná strana oblasti k spodnej strane',
  // [Align] relative to
  'relatívne k',
  // [Position] to the right of the left side of
  'napravo k ľavej strane',
  // [Position] below the top side of
  'dole hornú stranu',
  // Anchors: page, main text area (x2)
  '&Strana', '&Hlavná oblasť textu', 'Str&ana', 'Hlavná oblasť te&xtu',
  // Anchors: left margin, right margin
  'Ľavý okraj', 'P&ravý okraj',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Horný okraj', '&Dolný okraj', '&Vnútorný okraj', 'V&onkajší okraj',
  // Anchors: character, line, paragraph
  '&Znak', 'R&iadok', 'Odsek',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Zrkadlovité okraje', 'Vrstva v bunke tabuľky',
  // Above text, below text
  'N&ad textom', 'Po&d textom',
  // Width, Height groupboxes, Width, Height labels
  'Šírka', 'Výška', '&Šírka:', '&Výška:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikálne zarovnanie', '&Hore', '&Stred', '&Dole',
  // Border and background button and title
  '&Okraj a pozadie...', 'Okraj a pozadie oblasti',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Vložiť špeciálne', '&Vložiť ako:', 'Rich text formát', 'HTML formát',
  'Text', 'Unicode text', 'Bitmapový obrázok', 'Obrázkový meta Súbor',
  'Grafický súbor',  'URL',
  // Options group-box, styles
  'Možnosti', '&Štýly:',
  // style options: apply target styles, use source styles, ignore styles
  'aplikovať štýly na cieľový dokument', 'zachovať štýly a vzhľad',
  'zachovať vzhľad, ignorovať štýly',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Odrážky a číslovanie', 'Odrážky', 'Číslovanie', 'Odrážky', 'Číslovanie',
  // Customizo, Reset, None
  '&Vlastné...', '&Obnoviť', 'nie je',
  // Numbering: continue, reset to, create new
  'naviazať na predchádzajúce', 'pokračovať v číslovaní od', 'začať znovu číslovať od',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Veľké písmená (A, B, …)', 'Veľké rímske číslice (I, II, …)', 'Arabské číslice (1, 2, …)', 'Malé písmená (a, b, …)', 'Malé rímske číslice (i, ii, …)',
  // Bullet type
  'Kruh', 'Disk', 'Štvorec',
  // Level, Start from, Continue numbering, Numbering group-box
  'Úr&oveň:', 'Za&čať od:', '&Pokračovať', 'Číslovanie',
  // Customizo List Form -------------------------------------------------------
  // Title, Levels, Level count, List preperties, List type,
  'Vlastné nastavenie', 'Úroveň', 'Počet:', 'Vlastnosti', 'Typ:',
  // List types: bullet, image
  'Odrážka', 'Obrázok', 'Vložiť číslo|',
  // Number format, Number, Start level from, Font button
  'For&mát čísla:', 'Číslo', 'Začať čí&slovanie úrovne od:', '&Písmo...',
  // Image button, bullet character, Bullet button,
  '&Obrázok...', '&Znak odrážky', 'O&drážka...',
  // Position of list text, bullet, number, image, text
  'Umiestnenie textu', 'Umiestnenie odrážky', ' Umiestnenie čísla', ' Umiestnenie obrázku', ' Umiestnenie písma',
  // at, left indent, first line indent, from left indent
  '&na:', 'Odsadenie z&ľava:', 'Odsadenie pr&vého riadku:', 'od odsadenie zľava',
  // [only] one level preview, preview
  '&Náhľad jednej úrovne', 'Náhľad',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'zarovnať vľavo', 'zarovnať vpravo', 'zarovnať na stred',
  // level #, this level (for combo-box)
  'Úroveň %d', 'Táto úroveň',
  // Marker character dialog title
  'Zmena znaku odrážky',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Ohraničenie a pozadie odstavca', 'Ohraničenie', 'Pozadie',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Nastavenie', '&Farba:', 'Š&írka:', 'Šírka &rámu:', 'Odsadenie &textu...',
  // Sample, Border type group-boxes;
  'Náhľad', 'Typ ohraničenia',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Žiadne', '&Jednoduché', '&Dvojité', '&Trojité', 'Silné vo &vnútri', 'Silné &okolo',
  // Fill color group-box; More colors, padding buttons
  'Výplň', '&Viac farieb...', '&Odsadenie textu...',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text.',
  // title and group-box in Offsets dialog
  'Odsadenie textu', 'Odsadenie textu',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Odstavec', 'Zarovnanie', 'v&ľavo', 'v&pravo', 'na &stred', 'do &bloku',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Medzery', '&Pred:', '&Za:', '&Riadkovanie:', '&Násobky:',
  // Indents group-box; indents: left, right, first line
  'Odsadenie', 'Z&ľava:', 'S&prava:', 'Pr&vý riadok:',
  // indented, hanging
  '&Odsadený', '&Predsadený', 'Náhľad',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Jednoduché', '1.5 riadku', 'Dvojité', 'Minimálne', 'Presne', 'Násobky',
  // preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Odsadenie a medzery', 'Zarážky', 'Tok textu',
  // tab stop position label; buttons: set, delete, delete all
  '&Pozícia zarážky:', '&Nastaviť', '&Zmazať', 'Zmazať &všetko',
  // tab align group; left, right, center aligns,
  'Zarovnanie', 'V&ľavo', 'V&pravo', 'Na &stred',
  // leader radio-group; no leader
  'Vodiaci znak', '(Žiadny)',
  // tab stops to be deleted, delete none, delete all labels
  'Vymazať tieto zarážky:', '(Žiadne)', 'Všetky.',
  // Pagination group-box, keep with next, keep lines together
  'Stránkovanie', '&Zviazať s nasledujúcim', 'Zviazať &riadky',
  // Outline level, Body text, Level #
  'Ú&roveň osnovy:', 'Telo textu', 'Úroveň %d',    
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Náhľad pred tlačou', 'Šírka stránky', 'Celá stránka', 'Stránka:',
  // Invalid Scale, [page #] of #
  'Prosím vložte číslo od 10 do 500', 'z %d',
  // Hints on buttons: first, prior, next, last pages
  'Prvá stránka', 'Predchádzajúca stránka', 'Nasledujúca stránka', 'Posledná stránka',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Odsadenie bunky', 'Medzery', '&Pred bunkou', 'Od rámu tabuľky po bunku',
  // vertical, horizontal (x2)
  '&Vertikálne:', '&Horizontálne:', 'V&ertikálne:', 'H&orizontálne:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Ohraničenie', 'Nastavenie', '&Farba:', 'Farba &svetlá:', 'Farba &tieňa:',
  // Width; Border type group-box;
  'Šírka:', 'Typ ohraničenia',
  // Border types: none, sunken, raised, flat
  'Ž&iadne', '&Vtlačené', 'V&ystúpené', '&Plávajúce',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Rozdelenie bunky', 'Rozdeliť na',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Určenie počtu stĺpcov a riadkov', '&Originálne bunky',
  // number of columns, rows, merge before
  'Počet &stĺpcov:', 'Počet &riadkov:', '&Zlúčiť pred rozdelením',
  // to original cols, rows check-boxes
  'Rozdeliť na originá&lne stĺpce', 'Rozdeliť na originálne riad&ky',
  // Add Rows form -------------------------------------------------------------
  'Vložiť riadky', 'Po&čet riadkov:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Vzhľad stránky', 'Stránka', 'Záhlavie a zápätie',
  // margins group-box, left, right, top, bottom
  'Okraje (v milimetroch)',  'Okraje (inches)', 'V&ľavo:', '&Hore:', 'V&pravo:', '&Dole:',
  // mirror margins check-box
  '&Zrkadlové okraje',
  // orientation group-box, portrait, landscape
  'Orientácia', 'Na &výšku', 'Na &šírku',
  // paper group-box, paper sizo, default paper source
  'Papier', 'V&eľkosť:', 'Zd&roj:',
  // header group-box, print on the first page, font button
  'Záhlavie', '&Text:', '&Záhlavie na prvej stránke', '&Písmo...',
  // header alignments: left, center, right
  'v&ľavo', 'na &stred', 'v&pravo',
  // the same for footer (different hotkeys)
  'Päta', 'T&ext:', 'Z&ápätie na prvej stránke', 'P&ísmo...',
  '&vľavo', '&na stred', 'vp&ravo',
  // page numbers group-box, start from
  'Číslovanie stránok', 'Za&čať od:',
  // hint about codes
  'Kombinácia špeciálnych znakov:'#13'&&p - číslo stránky; &&P - počet stránok; &&d - aktuálny dátum; &&t - aktuálny čas.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Kódová stránka pre textový súbor', 'Vyberte súbor dekódovanie:',
  // thai, japanese, chinese (simpl), korean
  'Thajsko', 'Japonsko', 'Čína (zjednodušená)', 'Korea',
  // chinese (trad), central european, cyrillic, west european
  'Čínština (tradičná)', 'Stredná a východná Európa', 'Cyrilika', 'Západná Europa',
  // greek, turkish, hebrew, arabic
  'Gréčtina', 'Turečtina', 'Hebrejčina', 'Arabčina',
  // baltic, vietnamese, utf-8, utf-16
  'Balticko', 'Vietnamčina', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vizuálny štýl', '&Vyber štýl:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Prevod do textu', 'Vybrať &oddelovač:',
  // line break, tab, ';', ','
  'koniec riadku', 'tab', 'bodko čiarka', 'čiarka',
  // Table sort form -----------------------------------------------------------
  // error message
  'Tabuľku obsahujúcu zlúčené riadky nie je možné zoradiť',
  // title, main options groupbox
  'Tabuľku zoradiť', 'Zoradiť',
  // sort by column, case sensitive
  '&Zoradiť podľa stĺpca:', '&Veľké a malé písmená',
  // heading row, range or rows
  'Vylúčiť &hlavičku riadka', 'Riadky na zoradenie: od %d do %d',
  // order, ascending, descending
  'Objednávky', '&Vzostupne', '&Zostupne',
  // data type, text, number
  'Typ', '&Text', 'Č&íslo',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Vložiť číslo', 'Vlastnosti', 'Meno číslovania:', '&Typ číslovania:',
  // numbering groupbox, continue, start from
  'Číslovanie', 'P&okračovať', '&Začať od:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Popis', '&Označenie:', '&Vylúčiť označenie z popisu',
  // position radiogroup
  'Pozícia', '&Nad zvoleným objektom', '&Pod zvoleným objektom',
  // caption text
  'Popisný &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Číslovanie', 'Figure', 'Tabuľka',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Viditeľné strany okrajov', 'Okraj',
  // Left, Top, Right, Bottom checkboxes
  '&Ľavá strana', '&Vrchná strana', 'P&ravá strana', '&Spodná strana',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Zmenit veľkosť stĺpca tabuľky', 'Zmenit veľkosť riadku tabuľky',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Odsadenie prvého riadku', 'Odsadenie zľava', 'Predsadenie prvého riadku', 'Odsadenie vpravo',
  // Hints on lists: up one level (left), down one level (right)
  'O úroveň vyššie', 'O úroveň nižšie',
  // Hints for margins: bottom, left, right and top
  'Spodný okraj', 'Ľavý okraj', 'Pravý okraj', 'Horný okraj',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Ľavá zarážka', 'Pravá zarážka', 'Zarážka na stred', 'Desatinná zarážka',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normálny', 'Normálne odsadenie', 'Žiadny priestor', 'Záhlavie %d', 'Zoznam odsekov',
  // Hyperlink, Title, Subtitle
  'Hypertextový odkaz', 'Nadpis', 'Podnadpis',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Zdôraznenie', 'Zdôraznenie - jemné', 'Zdôraznenie - intenzívne', 'Silné',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citácia', 'Citácia - intenzívne', 'Odkaz - jemný', 'Odkaz - intenzívny',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Text v bloku', 'Premenná HTML', 'Kód HTML', 'Akronym HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definícia HTML', 'Klávesnica HTML', 'Vzorka HTML', 'Písací stroj HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Formátovaný v HTML', 'Citát HTML', 'Hlavička', 'Päta', 'Číslo strany',
  // Caption
  'Popis',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Odkaz na vysvetlivky', 'Odkaz na poznámku pod čiarou', 'Text vysvetlivky', 'Text poznámky pod čiarou',
  // Sidenote Reference, Sidenote Text
  'Odkazy bočnej poznámky', 'Text bočnej poznámky',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'farba', 'farba pozadia', 'priehľadný', 'východzí', 'farba podčiarknutia',
  // default background color, default text color, [color] same as text
  'východzia farba pozadia', 'východzia farba textu', 'rovnaký ako text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'jednoduché', 'silné', 'dvojité', 'bodkovaný', 'silne bodkovaný', 'prerušovaná',
  // underline types: thick dashed, long dashed, thick long dashed,
  'silná prerušovaná', 'dlhá prerušovaná', 'silná dlhá prerušovaná',
  // underline types: dash dotted, thick dash dotted,
  'bodka a čiara', 'silná bodka a čiara',
  // underline types: dash dot dotted, thick dash dot dotted
  'čiara bodka bodka', 'silná čiara bodka bodka',
  // sub/superscript: not, subsript, superscript
  'žiadny dolný/horný index', 'dolný index', 'horný index',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'obojsmerný režim:', 'zdedený', 'zľava do prava', 'zprava do ľava',
  // bold, not bold
  'tučné', 'netučné',
  // italic, not italic
  'kurzíva', 'nekurzíva',
  // underlined, not underlined, default underline
  'podčiarknúť', 'nepodčiarknúť', 'východzie podčiarknutie',
  // struck out, not struck out
  'preškrtnuté', 'nepreškrtnuté',
  // overlined, not overlined
  'nadčiarknuté', 'nenadčiarknuté',
  // all capitals: yes, all capitals: no
  'všetky veľké', 'všetký veľké vypnuté',
  // vertical shift: none, by x% up, by x% down
  'bez zvislého posunu', 'posunuté hore o %d%%', 'posunuté dole o %d%%',
  // characters width [: x%]
  'šírka znakov',
  // character spacing: none, expanded by x, condensed by x
  'normálne rozloženie znakov', 'rozloženie rozšírené o %s', 'rozloženie zúžené o %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'východzie písmo', 'východzí odstavec', 'zdedené atribúty',
  // [hyperlink] highlight, default hyperlink attributes
  'zvýrazniť', 'východzie',
  // paragraph aligmnment: left, right, center, justify
  'zarovnať doľava', 'zarovnať doprava', 'zarovnať na stred', 'zarovnať na šírku',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'výška riadku: %d%%', 'medzera medzi riadkami: %s', 'výška riadku: aspoň %s',
  'výška riadku: presne %s',
  // no wrap, wrap
  'zarovnanie riadku vypnuté', 'zarovnanie riadku',
  // keep lines together: yes, no
  'udržat riadky spolu', 'neudržať riadky spolu',
  // keep with next: yes, no
  'zachovať s nasledujúcim odstavcom', 'nezachovať s nasledujúcim odstavcom',
  // read only: yes, no
  'len pre čítanie', 'upraviteľné',
  // body text, heading level x
  'úroveň osnovy: telo textu', 'úroveň osnovy: %d',
  // indents: first line, left, right
  'odsadenie prvého riadku', 'ľavé odsadenie', 'pravé odsadenie',
  // space before, after
  'medzera pred', 'medzera po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulátory', 'žiadne', 'zarovnanie', 'vľavo', 'vpravo', 'na stred',
  // tab leader (filling character)
  'vodiaci znak',
  // no, yes
  'nie', 'áno',
  // [padding/spacing/side:] left, top, right, bottom
  'vľavo', 'hore', 'vpravo', 'dole',
  // border: none, single, double, triple, thick inside, thick outside
  'žiadne', 'jednoduché', 'dvojité', 'trojité', 'hrubo vnútri', 'hrubo zvonka',
  // background, border, padding, spacing [between text and border]
  'pozadie', 'okraj', 'výplň', 'rozostup',
  // border: style, width, internal width, visible sides
  'štýl', 'šírka', 'vnútorná šírka', 'viditeľné strany',
  // style inspector -----------------------------------------------------------
  // title
  'Inšpektor štýlov',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Štýl', '(Žiadny)', 'Odstavec', 'Písmo', 'Atribúty',
  // border and background, hyperlink, standard [style]
  'Okraj a pozadie', 'Hyperlink', '(Štandard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Štýly', 'Štýl',
  // name, applicable to,
  '&Názov:', 'Aplikovať na:',
  // based on, based on (no &),
  'Štýl založený n&a:', 'Štýl založený na:',
  //  next style, next style (no &)
  'Štýl pre nasledujúce odstavec:', 'Štýl pre nasledujúce odstavec:',
  // quick access check-box, description and preview
  'Rý&chly prístup', '&Popis a náhľad:',
  // [applicable to:] paragraph and text, paragraph, text
  'odstavec a text', 'odstavec', 'text',
  // text and paragraph styles, default font
  'Text a štýly odstavcov', 'Východzie písmo',
  // links: edit, reset, buttons: add, delete
  'Úpravy', 'Východzie', 'Prid&ať', '&Vymazať',
  // add standard style, add custom style, default style name
  'Pridať &štandardný štýl...', 'Pridať &uživateľský štýl', 'Štýl %d',
  // choose style
  'Zvoľte &štýl:',
  // import, export,
  '&Importovať...', '&Exportovať...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView štýly (*.rvst)|*.rvst', 'Chyba otvorenia súboru', 'Tento súbor neobsahuje štýly',
  // Title, group-box, import styles
  'Importovať štýly zo súboru', 'Import', 'I&mportovať štýly:',
  // existing styles radio-group: Title, override, auto-rename
  'Existujúce štýly', '&prepísať', '&pridať premenované',
  // Select, Unselect, Invert,
  '&Označiť', 'Od&značiť', 'Zameniť označenie',
  // select/unselect: all styles, new styles, existing styles
  '&Všetky štýly', '&Nové štýly', '&Existujúce štýly',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  '(Vymazať formát)', '(Všetky štýly)',
  // dialog title, prompt
  'Štýly', 'Zvoľte štýl na použitie:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Synonymá', '&Ignorovať všetko', '&Pridať do slovníka',
  // Progress messages ---------------------------------------------------------
  'Preberanie %s', 'Príprava pre tlač...', 'Tlač strany %d',
  'Konverzia z RTF...',  'Konverzia do RTF...',
  'Čítanie %s...', 'Zápis %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Bez poznámky', 'Poznámka v päte', 'Záverečná poznámka', 'Bočná poznámka', 'Text oblasť'
  );


initialization
  RVA_RegisterLanguage(
    'Slovak', // english language name, do not translate
    'Slovenčina', // native language name
    EASTEUROPE_CHARSET, @Messages);

end.
