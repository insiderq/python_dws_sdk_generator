
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Turkish (TR) translation                        }
{                                                       }
{       Copyright (c) Necmettinozalp                    }
{       necmettinozalp@hotmail.com                      }
{       http://www.trichview.com                        }
{                                                       }
{       Copyright (c) �brahim Kutluay                   }
{       ikutluay@ibrahimkutluay.net                     }
{                                                       }
{*******************************************************}
{ Updated: 2012-08-22 by                                }
{             Zekeriya Bozkurt <zekeriye@hotmail.com>   }
{*******************************************************}

unit RVAL_Turkish;
{$I RV_Defs.inc}

interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'in�', 'cm', 'mm', 'pika', 'piksel', 'nokta',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  'in�','cm','mm', 'pika','piksel', 'nokta',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Dosya', '&D�zen', '&Bi�im', '&Yaz� Tipi', '&Paragraf', '&Ekle', '&Tablo', '&Pencere', '&Yard�m',
  // exit
  '&��k��',
  // top-level menus: View, Tools,
  '&G�r�n�m', 'Ara�&lar',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Boyut', 'Yaz�tipi', '&Tabloyu Se�', 'H�cre ��eri�i&ni Hizala', 'H�cr&e Kenarl���',
  // menus: Table cell rotation
  'H�cre &D�nd�rme',
  // menus: Text flow, Footnotes/endnotes
  '&Yaz� Ak�� D�zeni', '�&stBilgi/AltBilgi',
  // ribbon tabs: tab1, tab2, view, table
  '&Genel', '&Geli�mi�', 'G�&r�n�m', '&Tablo',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Pano', 'Yaz� Tipi', 'Paragraf', 'Liste', 'D�zenle',
  // ribbon groups: insert, background, page setup,
  'Ekle', 'Arka Plan', 'Sayfa Yap�s�',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options 
  'Links', '�stBilgi', 'Dosya G�r�n�m�', 'G�ster/Gizle', 'B�y�lt/K���lt', 'Se�enekler',
  // ribbon groups on table tab: insert, delete, operations, borders
   'Ekle', 'Sil', 'Operasyon', 'Kenarl�klar',
  // ribbon groups: styles 
  'Stil',
  // ribbon screen tip footer,
  'Yard�m i�in F1 tu�una bas�n�z',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Son Kullan�lanlar', 'Dosyan�n bir kopyas�n� kaydet', 'Dosyay� Yaz�c� �ng�r�n�m� ve Yazd�r',
  // ribbon label: units combo
  'Cetvel Birimi:',    
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Yeni', 'Yeni|Yeni bo� bir belge olu�turur',
  // TrvActionOpen
  '&A�...', 'A�|Daha �nceden kay�t edilmi� dosyay� a�ar',
  // TrvActionSave
  '&Kaydet', 'Kaydet|Mevcut belgeyi diske kaydet',
  // TrvActionSaveAs
  '&Farkl� kaydet...', 'Farkl� kaydet...|Mevcut belgeyi farkl� bir adla kaydeder',
  // TrvActionExport
  '&D�n��t�r...', 'D�n��t�r|Mevcut belgeyi di�er bi�imlere d�n��t�r',
  // TrvActionPrintPreview
  '&Bask� �nizleme', 'Bask� �nizleme|Sanal ka��tta bask� �nizleme yap�l�r',
  // TrvActionPrint
  '&Yazd�r...', 'Yazd�r|Yazd�rma se�eneklerini ayarlar ve dosyay� yazd�r',
  // TrvActionQuickPrint
  '&Yazd�r','&H�zl� Yazd�r', 'Yazd�r|Belgeyi mevcut ayarlarla yazd�r',
  // TrvActionPageSetup
  'Sayfa Yap�s�...', 'Sayfa Yap�s�|Kenar bo�luklar�, Ka��t yap�s�, Ka��t Y�n�, �st ve Alt Bilgileri Ayarla',
  // TrvActionCut
  '&Kes', 'Kes|Se�ili k�sm� belle�e alarak belgeden ��kart',
  // TrvActionCopy
  '&Kopyala', 'Kopyala|Se�ili k�sm� �o�altmak �zere belle�e al',
  // TrvActionPaste
  '&Yap��t�r', 'Yap��t�r|Kesilen veya Kopyalanan metni Yap��t�r',
  // TrvActionPasteAsText
  '&Yaz� Olarak Yap��t�r', 'Yaz� Olarak Yap��t�r|Panodaki Bilgiyi Yaz� Olarak Yap��t�r',
  // TrvActionPasteSpecial
  '�&zel Yap��t�r...', '�zel yap��t�r|Kesilen veya Kopyalanan �fadeyi �zel Yap��t�r',
  // TrvActionSelectAll
  '&T�m�n� Se�', 'T�m�n� Se�|T�m Belgeyi Se�',
  // TrvActionUndo
  '&Geri Al', 'Geri Al|Son yap�lan i�lemi geri al',
  // TrvActionRedo
  '&Yeniden Yap', 'Yeniden Yap|Geri al�nan i�lemi tekrar yap',
  // TrvActionFind
  '&Bul...', 'Bul|Belirtilen metni belge i�ersinde Ara',
  // TrvActionFindNext
  '&Sonrakini Bul', 'Sonrakini Bul|Metni aramaya devam et',
  // TrvActionReplace
  '&De�i�tir...', 'De�i�tir|Bulunan metni ba�ka bir metinle de�i�tir',
  // TrvActionInsertFile
  '&Dosya...', 'Dosya Ekle|D�� Kaynakl� dosya ekle',
  // TrvActionInsertPicture
  '&Resim...', 'Resim Ekle|Bir resim dosyas�n� belgeye ekle',
  // TRVActionInsertHLine
  '&Ara �izgi', 'Yatay �izgi Ekle|Araya yatay �izgi ekle',
  // TRVActionInsertHyperlink
  '&K�pr�...', 'K�pr� Ekle|Ba�lant� K�pr�s� ekle',
  // TRVActionRemoveHyperlinks
  'K�pr�y� Kald�r','K�pr�y� Kald�r|Se�ili metindeki t�m k�pr�leri kald�r�r',
  // TrvActionInsertSymbol
  '&Sembol...', 'Sembol Ekle|Sembol tablosundan Sembol ekle',
  // TrvActionInsertNumber
  'Say�...', 'Say� ekle|Bir Say� ekle',
  // TrvActionInsertCaption
  'Ba�l�k Ekle...', 'Ba�l�k ekle|Se�ili metin/nesne i�in ba�l�k ekler',
  // TrvActionInsertTextBox
  'Metin Kutusu', 'Metin Kutusu Ekle|Metin Kutusu Ekler',
  // TrvActionInsertSidenote
  'Kenar Notu', 'Kenar Notu Ekle|Metin kutusu i�inde note ekler',
  // TrvActionInsertPageNumber
   'Sayfa numaras�', 'Sayfa numaras�|Sayfa numaras� ekle',
  // TrvActionParaList
  '&Anahat ve Numaraland�rma...', 'Anahat ve Numaraland�rma|Se�ili b�l�m� numaraland�r',
  // TrvActionParaBullets
  '&Numaraland�r', 'Numaraland�r|Numaraland�r veya Numaraland�rmay� �ptal Et',
  // TrvActionParaNumbering
  '&Numaraland�rma', 'Numaraland�rma|Se�ili b�l�m� numaraland�rma veya numaraland�rmay� kald�rmak ��in',
  // TrvActionColor
  '&Zemin Rengi...', 'Zemin Rengi|Belgenin Arka Fonunu de�i�tir',
  // TrvActionFillColor
  '&Dolgu Rengi...', 'Dolgu Rengi|Paragraf, h�cre, tablo, belge veya bir metin i�in zemin rengi belirler',
  // TrvActionInsertPageBreak
  '&Sayfasonu Ekle', 'Sayfasonu Ekle|Bu noktadan itibaren yeni bir sayfaya ba�lar',
  // TrvActionRemovePageBreak
  '&Sayfa Kesmesini Kald�r', 'Sayfa Kesmesini Kald�r|Sayfa kesmesini kald�r',
  // TrvActionClearLeft
   'Soldaki Yaz� Ak���n� Temizle', 'Soldaki Yaz� Ak���n� Temizle|Bu paragraf sola dayal� resmi takip etsin',
  // TrvActionClearRight
   'Sa�daki Yaz� Ak���n� Temizle', 'Sa�daki Yaz� Ak���n� Temizle|Bu paragraf sa�a dayal� resmi takip etsin',
  // TrvActionClearBoth
  'Etraf�ndaki Yaz� Ak���n� Temizle','Etraf�ndaki Yaz� Ak���n� Temizle|Bu paragraf sola veya sa�adayal� resmi takip eder',
  // TrvActionClearNone
  'Normal Yaz� Ak���','Normal Yaz� Ak���|Paragraf her iki yana yasl� resmin etraf�nda yer al�r',
  // TrvActionVAlign
  'Nesne Konumu ...','Nesne Konumu|Se�ili Nesnenin konumunu de�i�tir',
  // TrvActionItemProperties
  '&Nesne �zellikleri...', 'Nesne �zellikleri|Se�ili Nesne ile ilgili �zellikler',
  // TrvActionBackground
  '&Arka Plan...', 'Arka Plan|Sayfan�n arka plan rengini de�i�tirir veya resim yerle�tirir',
  // TrvActionParagraph
  '&Paragraf...', 'Paragraf|Paragrafa ait �zellikleri de�i�tir',
  // TrvActionIndentInc
  '&Girintiyi Artt�r', 'Girintiyi Artt�r|Se�ili paragraf girintisini art�r�r',
  // TrvActionIndentDec
  'Girintiyi A&zalt', 'Girintiyi Azalt|Se�ili paragraf girintisini azalt�r',
  // TrvActionWordWrap
  '&Metni Kayd�r', 'Metni Kayd�r|Sat�r sonuna gelince kelimeyi a�a�� kayd�r',
  // TrvActionAlignLeft
  '&Sola Yasla', 'Sola Yasla|Se�ili metni sola yasla',
  // TrvActionAlignRight
  'Sa&�a Yasla', 'Sa�a Yasla|Se�ili Metni sa�a yasla',
  // TrvActionAlignCenter
  '&Ortala', 'Metni Ortala|Se�ili metni ortala',
  // TrvActionAlignJustify
  '&�ki Yana Yasla', '�ki Yana Yasla|Se�ili metni iki yana yasla',
  // TrvActionParaColor
  'Paragraf &Dolgu Rengi...', 'Paragraf Dolgu Rengi|Paragraf dolgu rengi ayarlar�n� de�i�tir',
  // TrvActionLineSpacing100
  '&Tek Sat�r Bo�luk', 'Tek sat�r bo�luk|Sat�r aralar�nda tek sat�r bo�luk b�rak',
  // TrvActionLineSpacing150
  '1.5 Sat�r Bo�luk', '1.5 Sat�r bo�luk|Sat�r aralar�nda 1.5 sat�r bo�luk b�rak',
  // TrvActionLineSpacing200
  '&�ift sat�r bo�luk', '�ift sat�r bo�luk|Sat�r aralar�nda iki sat�r bo�luk b�rak',
  // TrvActionParaBorder
  'Paragraf Kenarl�k ve Dolgu Bi�imi...', 'Paragraf Kenarl�k ve Doldu Bi�imi|Paragraf kenarl�klar�n� ve dolgu rengini ayarlar',
  // TrvActionInsertTable
  '&Tablo Ekle...', '&Tablo', 'Yeni Tablo|Yeni tablo ekle',
  // TrvActionTableInsertRowsAbove
  '�ste &Sat�r Ekle', '�ste Sat�r Ekle|Se�ili sat�r�n �st�ne yeni sat�r ekle',
  // TrvActionTableInsertRowsBelow
  'Al&ta Sat�r Ekle', 'Alta Sat�r Ekle|Se�ili sat�r�n alt�na yeni sat�r ekle',
  // TrvActionTableInsertColLeft
  'Sola S�tun Ekle', 'Sola S�tun Ekle|Sola yeni s�tun ekle',
  // TrvActionTableInsertColRight
  '&Sa�a S�t�n Ekle', 'Sa�a S�tun Ekle|Sa�a yeni s�tun ekle',
  // TrvActionTableDeleteRows
  'Sat�r Sil', 'Sat�r Sil|Se�ili sat�rlar� sil',
  // TrvActionTableDeleteCols
  'S�t�n Sil', 'S�tun Sil|Se�ili s�tunlar� sil',
  // TrvActionTableDeleteTable
  'Tablo S&il', 'Tablo Sil|Se�ili tabloyu sil',
  // TrvActionTableMergeCells
  'H�creleri &Birle�tir', 'H�creleri Birle�tir|Se�ili h�creleri birle�tir',
  // TrvActionTableSplitCells
  'H�creleri &B�l...', 'H�creleri B�l|Se�ili h�creleri b�l',
  // TrvActionTableSelectTable
  'Tablo &Se�', 'Tablo Se�|Tabloyu se�',
  // TrvActionTableSelectRows
  '&Sat�r Se�', 'Sat�r Se�|Sat�r se�',
  // TrvActionTableSelectCols
  'S�t&un Se�', 'S�tun Se�|S�tun se�',
  // TrvActionTableSelectCell
  '&H�cre Se�', 'H�cre Se�|H�cre se�',
  // TrvActionTableCellVAlignTop
  'H�creyi �ste &Hizala', 'H�creyi �ste Hizala|H�cre i�eri�ini �st kenara yakla�t�r',
  // TrvActionTableCellVAlignMiddle
  'H�creyi &Ortaya Hizala', 'H�creyi Ortaya Hizala|H�cre i�eri�ini ortala',
  // TrvActionTableCellVAlignBottom
  'H�creyi &Alta Hizala', 'H�creyi Alta Hizala|H�cre i�eri�ini alt kenara yakla�t�r',
  // TrvActionTableCellVAlignDefault
  '&Varsay�lan Hizalama', '&Varsay�lan Hizalama|&Varsay�lan Hizalama',
  // TrvActionTableCellRotationNone
  'H�crede D�nd�rme &Yok', 'H�crede D�nd�rme Yok|H�cre i�eri�ini normal hale getirir (0� d�nd�rme)',
  // TrvActionTableCellRotation90
  'H�creyi &90� D�nd�r', 'H�creyi 90� D�nd�r|H�cre i�eri�ini 90� d�nd�r�r',
  // TrvActionTableCellRotation180
  'H�creyi &180� D�nd�r', 'H�creyi 180� D�nd�r|H�cre i�eri�ini 180� d�nd�r�r',
  // TrvActionTableCellRotation270
  'H�creyi &270� D�nd�r', 'H�creyi 270� D�nd�r|H�cre i�eri�ini 270� d�nd�r�r',
  // TrvActionTableProperties
  'Tablo &�zellikleri...', 'Tablo �zellikleri|Tablo �zelliklerini de�i�tir',
  // TrvActionTableGrid
  'K�lavuz �izgilerini &G�ster', 'K�lavuz �izgilerini G�ster|Klavuz �izgilerini gizle/g�ster',
  // TRVActionTableSplit
  'Tabloyu �kiye &B�l', 'Tabloyu �kiye B�l|Se�ili sat�rdan ba�lamak �zere tabloyu 2 ye b�ler',
  // TRVActionTableToText
  'Metne �evir', 'Metne �evir |Tabloyu metin halinde yaz�ya �evirir',
  // TRVActionTableSort
   '&S�rala ...', 'S�rala|Tablo sat�rlar�n� s�ralar',
  // TrvActionTableCellLeftBorder
  '&Sol Kenarl�k', 'Sol Kenarl�k|Sol kenarl��� gizle veya g�ster',
  // TrvActionTableCellRightBorder
  '&Sa� Kenarl�k', 'Sa� Kenarl�k|Sa� kenarl��� gizle veya g�ster',
  // TrvActionTableCellTopBorder
  '&�st Kenarl�k', '�st Kenarl�k|�st kenarl��� gizle veya g�ster',
  // TrvActionTableCellBottomBorder
  '&Alt Kenarl�k', 'Alt Kenarl�k|Alt kenarl��� gizle veya g�ster',
  // TrvActionTableCellAllBorders
  '&T�m Kenarl�klar', 'T�m Kenarl�klar|T�m h�cre kenarl�k �izgilerini gizle veya g�ster',
  // TrvActionTableCellNoBorders
  '&Kenarl�k Yok', 'Kenarl�k Yok|T�m kenarl�klar� gizle',
  // TrvActionFonts & TrvActionFontEx
  '&Yaz� Tipi...', 'Yaz� Tipi|Se�ili metnin yaz� tipini de�i�tir',
  // TrvActionFontBold
  '&Kal�n', 'Kal�n|Se�ili metni kal�nla�t�r',
  // TrvActionFontItalic
  '�&talik', '�talik|Se�ili metni sa�a yat�k yazar',
  // TrvActionFontUnderline
  '&Alt� �izili', 'Alt� �izili|Se�ili metnin alt�n� �iz',
  // TrvActionFontStrikeout
  '&�st� �izili', '�st� �izili|Se�ili metnin �st�n� �iz',
  // TrvActionFontGrow
  '&Yaz� Boyutunu Art�r', 'Yaz� Boyutunu Art�r|Yaz� boyutunu %10 b�y�lt',
  // TrvActionFontShrink
  'Yaz� Boyutunu K���lt', 'Yaz� Boyutunu K���lt|Yaz� boyutunu %10 k���lt',
  // TrvActionFontGrowOnePoint
  'Yaz� Boyutunu 1 Nk Art�r', 'Yaz� Boyutunu 1 Nk Art�r|Yaz� boyutunu 1 Nk art�r',
  // TrvActionFontShrinkOnePoint
  'Yaz� Boyutunu 1 Nk Azalt', 'Yaz� Boyutunu 1 Nk Azalt|Yaz� boyutunu 1 Nk azalt�r',
  // TrvActionFontAllCaps
  '&T�m� B�y�k', 'T�m� B�y�k|T�m se�ili metni B�Y�K HARFE �evir',
  // TrvActionFontOverline
  '&�st� �izili', '�st� �izili|T�m se�ili metni �st� �izili hale getirir',
  // TrvActionFontColor
  'Yaz� &Rengi...', 'Yaz� Rengi|Se�ili metnin yaz� rengini de�i�tir',
  // TrvActionFontBackColor
  '&Vurgu...', 'Vurgu|Se�ili metnin Vurgulayacak rengi belirler',
  // TrvActionAddictSpell3
  '&Yaz�m ve &Dil Bilgisi', 'Yaz�m ve Dil Bilgisi|Yaz�m ve Dil Bilgisi kontrol�',
  // TrvActionAddictThesaurus3
  '&E� Anlaml�lar', 'E� Anlaml�lar|Se�ili kelime i�in e� anlaml� kelimeleri belirtir',
  // TrvActionParaLTR
  'Soldan Sa�a', 'Soldan Sa�a|Paragraf metin yaz�m y�n�n� soldan-sa�a olarak ayarlar',
  // TrvActionParaRTL
  'Sa�dan Sola', 'Sa�dan Sola|Paragraf metin yaz�m y�n�n� sa�dan-sola olarak ayarlar',
  // TrvActionLTR
  'Soldan Sa�a Metin', 'Soldan Sa�a Metin|Metin yaz�m y�n�n� soldan-sa�a olarak ayarlar',
  // TrvActionRTL
  'Sa�dan Sola Metin', 'Sa�dan Sola Metin|Metin yaz�m y�n�n� sa�dan-sola olarak ayarlar',
  // TrvActionCharCase
  'B�y�k/K���k Harf D�n��t�r', 'B�y�k/K���k Harf D�n��t�r|Se�ili metinde B�y�k/K���k Harf d�n��t�rme yapar',
  // TrvActionShowSpecialCharacters
  'Yaz�lamaya&n Karakterler', 'Yaz�lamayan Karakterler|Yaz�lamayan Karakterleri -paragraf sonu,sekme veya bo�luk gbi- g�ster/gizle',
  // TrvActionSubscript
  'Al&t simge', 'Altsimge|Se�ili metni altsimge yapar',
  // TrvActionSuperscript
  '�st simge', '�st simge|Se�ili metni �st simge yapar',
  // TrvActionInsertFootnote
  '�stbilgi','�stbilgi|�stbilgi Ekle',
  // TrvActionInsertEndnote
  'Altbilgi','Altbilgi|Altbilgi Ekle',
  // TrvActionEditNote
  'Altbilgi/�stbilgi D�zenle','Altbilgi/�stbilgi D�zenle|Altbilgi / �st Bilgiyi D�zenle, De�i�tir',
  // TrvActionHide
  '&Gizle/G�ster','Gizle/G�ster| Se�ili metni Gizler(Bas�lamayan metin yapar) veya G�sterir (Bas�labilen metin yapar)',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stil...', 'Stil|Stil ayarlama ekran�n� a�ar',
  // TrvActionAddStyleTemplate
  'Stil &Ekle...', 'Stil Ekle|Yeni bir metin veya paragraf stili olu�tur',
  // TrvActionClearFormat,
  '&Format� Temizle', 'Format� Temizle|Se�ili metin ve paragraflara uygulanan formatlar� temizler',
  // TrvActionClearTextFormat,
  '&Metin Format�n� Temizle', '&Metin Format�n� Temizle|Se�ili metindeki uygulanm�� formatlar� iptal eder',
  // TrvActionStyleInspector
  'Stil &Listesi', 'Stil &Listesi|Stil Listesi g�ster / gizle',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'Tamam', '�ptal', 'Kapat', 'Ekle', '&A�...', '&Kaydet...', '&Temizle', 'Yard�m', 'Sil',
  // Others  -------------------------------------------------------------------
  // percents
  'y�zde',
  // left, top, right, bottom sides
  'Sol Kenar', '�st Kenar', 'Sa� Kenar', 'Alt Kenar',
  // save changes? confirm title
  '"%s" Belgede yap�lan de�i�iklikler kaydedilsin mi?', 'Kaydet',
  // warning: losing formatting
  '%s kaydetmek istedi�iniz bi�im taraf�ndan desteklenmeyen baz� �zellikler i�eriyor olabilir.'#13+
  'Belgeyi bu bi�imde kaydetme istiyor musunuz?',
  // RVF format name
  'Zengin G�r�n�m Bi�imi',
  // Error messages ------------------------------------------------------------
  'Hata',
  'Dosya A��l�rken Bir Hata Olu�tu.'#13#13'Olas� nedenler:'#13'- dosya bi�imi bu program taraf�ndan desteklenmiyor olabilir;'#13+
  '- dosya bozulmu� olabilir;'#13'- dosya �u an ba�ka bir uygulama taraf�ndan kullan�l�yor olabilir.',
  'Resim dosyas� y�klenirken hata olu�tu.'#13#13'Olas� nedenler:'#13'- resim bi�imi bu program taraf�ndan desteklenmiyor olabilir;'#13+
  '- dosya resim i�ermiyor olabilir;'#13+
  '- dosya bozulmu� olabilir;'#13'- dosya �u an ba�ka bir uygulama taraf�ndan kullan�l�yor olabilir.',
  'Dosya KAydetme Hatas�.'#13#13'Olas� nedenler:'#13'- yeterli bo� yer olmayabilir;'#13+
  '- disk yaz�m-korumal� olabilir;'#13'- kaydedilecek s�r�c� ��kar�lm�� olabilir;'#13+
  '- dosya �u an ba�ka bir uygulama taraf�ndan kullan�l�yor olabilir;'#13'- disk �zerinde bozuk alanlar olabilir.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RVF Dosyalar� (*.rvf)|*.rvf',  'RTF Dosyalar� (*.rtf)|*.rtf' , 'XML Dosyalar� (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Metin Dosyalar� (*.txt)|*.txt', 'Unicode Metin dosyalar� (*.txt)|*.txt', 'Metin Dosyalar� - Otomatik Tan�(*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Dosyalar� (*.htm;*.html)|*.htm;*.html', 'HTML dosyalar� (*.htm;*.html)|*.htm;*.html',
  'HTML dosyalar� - basite�tirilmi�(*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dosyas� (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Arama Tamamland�', 'Aranan ''%s'' metni bulunamad�.',
  // 1 string replaced; Several strings replaced
  '1 metin de�i�tirildi.', '%d metin de�i�tirildi.',
  // continue search from the beginning/end?
  'Arama i�lemi belgenin sonuna kadar yap�ld�,belgenin ba��ndan itibaren tekrarlans�nm�?',
  'Arama i�lemi belgenin ba��na kadar yap�ld�,belgenin sonundan itibaren tekrarlans�nm�?',  
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Saydam', 'Otomatik',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Siyah', 'Kahverengi', 'Zeytin Ye�ili', 'Koyu Ye�il', 'Koyu Deniz Mavisi', 'Koyu Mavi', '�ivit Mavi', 'Gri-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Koyu K�rm�z�', 'Turuncu', 'Kirli Sar�', 'Ye�il', 'Deniz Mavisi', 'Mavi', 'Mavi-Gri', 'Gri-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'K�rm�z�', 'A��k Turuncu', 'F�st�ki Ye�il', 'Deniz Ye�ili', 'Okyanus Mavisi', 'A��k Mavi', 'Menek�e', 'Gri-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Pembe', 'Alt�n', 'Sar�', 'Parlak Ye�il', 'turkuaz', 'G�k Mavi', 'Plum', 'Gray-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'G�l', 'Ten', 'A��k Sar�', 'A��k Ye�il', 'A��k Turkuaz', 'Donuk Mavi', 'Lavanta', 'Beyaz',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&Saydam', '&Otomatik', '&T�m Renkler...', '&Varsay�lan',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Dolgu', '&Renk:', 'Konum', 'Dolgu', '�rnek Metin.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Yok', '&Yay', 'D�zg�n D��e', '&D��e', '&Ortala',
  // Padding button
  '&D�� Kenar...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Renk Doldur', '&Uygulama Alan�:', '&T�m Renkler...', '&D�� Kenar...',
  'L�tfen bir renk se�in',
  // [apply to:] text, paragraph, table, cell
  'metin', 'paragraf' , 'tablo', 'h�cre',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Yaz� Bi�imi', 'Yaz� Bi�imi', 'Di�er',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Yaz� Tipi:', '&Yaz� Boyutu', 'Yaz�tipi', '&Kal�n', '�&talic',
  // Script, Color, Back color labels, Default charset
  '&Dil:', '&Renk:', 'Alt &Zemin:', '(Varsay�lan)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efekt', 'Alt� �izgili', '&�st �izgi', '&�st� �izgili', '&T�M� B�Y�K HARF',
  // Sample, Sample text
  '�rnek', '�rnek Metin',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Karakter Aras� Bo�luk', '&Bo�luk:', '&Geni�letilmi�', '&Daralt�lm��',
  // Offset group-box, Offset label, Down, Up,
  'Konum', '&Konum:', '&Al�alt�lm��', '&Y�kseltilmi�',
  // Scaling group-box, Scaling
  '�l�ekleme', '&�l�ek:',
  // Sub/super script group box, Normal, Sub, Super
  'Alt simge ve �st simge', '&Normal', 'Al&t simge', '�&st simge',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Bo�luk', '&�st:', '&Sol:', '&Alt:', '&Sa�:', '&E�it olarak',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'K�pr� Ekle', 'K�pr�', '&Metin:', '&Hedef', '<<Se�im>>',
  // cannot open URL
  '"%s" web sayfas� bulunamad�',
  // hyperlink properties button, hyperlink style
  '&�zelle�tir...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'K�pr� �zellikleri', 'Normal Renkler', 'Aktif Renk',
  // Text [color], Background [color]
  '&Metin:', '&Dolgu',
  // Underline [color]
  'Alt� �izgili:',
  // Text [color], Background [color] (different hotkeys)
  '&Metin:', '&Dolgu',
  // Underline [color], Attributes group-box,
  'Alt� �izgili:', '�zellikler',
  // Underline type: always, never, active (below the mouse)
  'Alt� �izgili::', 'Her zaman', 'hi�bir zaman', 'aktif',
  // Same as normal check-box
  'Normal renkleri kullan',
  // underline active links check-box
  '&Aktif k�pr�n�n alt�n� �iz',

  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Simge Ekle', '&Yaz� Tipi:', '&Karakter Tablosu:', 'Unicode',
  // Unicode block
  '&Blok:',    
  // Character Code, Character Unicode Code, No Character
  'Karakter Kodu: %d', 'Karakter Kodu: Unicode %d', '(Karakter Yok)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Tablo Ekle', 'Tablo Boyutu', '&S�tun Say�s�:', 'Sa&t�r Say�s�:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tablo Ayarlar�', '&Otomatik Boyutland�r', 'Pencereye S��d�r', 'Sabit S�tun Geni�li�i',
  // Remember check-box
  '&Yeni Tablolar ��in Varsay�lan Yap',
  // VAlign Form ---------------------------------------------------------------
  'Konum/Yerle�im',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  '�zellikler', 'Resim', 'B�y�kl�k ve Konum', 'Sat�r', 'Tablo', 'Sat�rlar', 'H�creler',
  // Image Appearance, Image Misc, Number tabs
  'G�r�n�m', '�znitelik', 'Numara',
  // Box position, Box size, Box appearance tabs
  'Konum', 'B�y�kl�k', 'G�r�n�m',
  // Preview label, Transparency group-box, checkbox, label
  '�nizleme:', 'Saydaml�k', '&Saydam', 'Saydaml�k Rengi:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&De�i�tir...', '&Kaydet...', 'Otomatik',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Konum', '&Y�n:',
  'Sat�r alt� ile hizalar', 'Sat�ra g�re ortala',
  'Sat�r �st �izgisinin �st�nde', 'Sat�r alt �izgisinin alt�nda', 'Sat�r�n orta �izgisinin ortas�nda',
  // align to left side, align to right side
  'Sol Taraf', 'Sa� Taraf',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Kayd�rma:', 'Yay', '&Geni�lik:', '&Y�kseklik:', 'Varsay�lan Boyut: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Orant�l� �l�eklendir', 'S�f�rla',
  // Inside the border, border, outside the border groupboxes
  'Kenarl�k ��inde', 'Kenarl�k', 'Kenarl�k D���nda',
  // Fill color, Padding (i.e. margins inside border) labels
  'Dolgu Rengi:', 'Bo�luk:',
  // Border width, Border color labels
  'Kenarl�k kal�nl���:', 'Kenarl�k rengi:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
   'Yatay Bo�luk:', 'D��ey Bo�luk:',
  // Miscellaneous groupbox, Tooltip label
  '�znitelik', 'A��klama:',
  // web group-box, alt text
  'web kutusu Ayarlar�', 'Alternatif &Metin:',
  // Horz line group-box, color, width, style
  'Yatay �izgi', '&Renk:', '&Geni�lik:',  '&Tip:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tablo', '&Geni�lik:', '&Dolgu Rengi:', 'H�creler Aras� &Bo�luk...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Yatay h�cre bo�lu�u', 'D��ey h�cre bo�lu�u:', 'Kenarl�k ve Arkaplan',
  // Visible table border sides button, auto width (in combobox), auto width label
  'G�r�n�r �er�eve Kenarlar�...', 'Otomatik', 'otomatik',
  // Keep row content together checkbox
  '��erik ile birlikte koru',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'D�nme A��s�', 'Yok', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  '�er�eve:', 'G�r�n�r �er�eve Kenarlar�...',
  // Border, CellBorders buttons
  '&Tablo Kenar� ...', '&H�cre Kenar�...',
  // Table border, Cell borders dialog titles
  'Tablo Kenarl���', 'Varsay�lan H�cre Kenarlar�',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Yazd�r�l�yor', '&Tabloyu bir sayfaya s��d�r', 'Ba�l�k sat�r &say�s�:',
  'Ba�l�k sat�nlar�n� hersayfada tekrarla',
  // top, center, bottom, default
  '&�st', '&Orta', '&A�a��', '&Varsay�lan',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Ayarlar', 'Tercih edilen &geni�lik:', '&En az y�kseklik:', '&Dolgu Rengi:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kenar', 'G�r�nen Kenarlar:',  '&G�lge Rengi:', '&A��k Renk', '&Renk:',
  // Background image
  '&Resim...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Yatay Konum', 'D��ey konum', 'Metin ��indeki Konumu',
  // Position types: align, absolute, relative
  'Hizalama', 'Mutlak Konum', 'G�receli Konum',
  // [Align] left side, center, right side
  '.. n�n solu, kutunun solu ile ayn�',
  '.. n�n merkezi, kutunun merkezi ile ayn�',
  '.. n�n sa��, kutunun sa�� ile ayn�',
  // [Align] top side, center, bottom side
  '.. n�n �st�, kutunun �st� ile ayn�',
  '.. n�n merkezi, kutunun merkezi ile ayn�',
  '.. n�n alt�, kutunun alt� ile ayn�',
  // [Align] relative to
  'g�receli',
  // [Position] to the right of the left side of
  '.. n�n solunun sa��nda',
  // [Position] below the top side of
  '.. �st�n�n alt�nda ',
  // Anchors: page, main text area (x2)
  'Sayfa', 'Ana Metin Alan�', 'Sayfa', 'Ana Metin Alan�',
  // Anchors: left margin, right margin
  'Sol bo�luk', 'Sa� bo�luk',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '�st Bo�luk', 'Alt Bo�luk', 'Girinti Bo�lu�u', '��k�nt� Bo�lu�u',
  // Anchors: character, line, paragraph
  'Karekter', 'Sat�r', 'Paragraf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Kar��l�kl� Bo�luklar', 'Tabloda h�cre d�zeni',
  // Above text, below text
  '�stte metin', 'Altta metin',
  // Width, Height groupboxes, Width, Height labels
  'Geni�lik', 'Y�kseklik', 'Geni�lik:', 'Y�kseklik:',
  // Auto height label, Auto height combobox item
  'Otomatik', 'Otomatik',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'D��ey Hizalama', '�st', 'Orta', 'Alt',
  // Border and background button and title
  'Kenarl�k ve Arkaplan...', 'Kutunun �er�evesi ve Arkaplan�',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  '�zel Yap��t�r', '&Yap��t�rma Se�enekleri:', 'Zengin Metin Bi�imi', 'HTML Bi�imi',
  'Metin', 'Unicode Metin', 'Bitmap Resmi', 'Windows Metafile Resmi',
  'Resim Dosyalar�', 'K�pr�',
  // Options group-box, styles
  'Se�enekler', '&Stil:',
  // style options: apply target styles, use source styles, ignore styles
  'Belgeye stili uygula', 'stil ve g�r�n�mleri koru',
  'g�r�n�m� koru, stilleri dikkate alma',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Numaraland�rma ve Ana Hat', 'Madde �mi', 'Numaral�', 'Madde imi listesi', 'Numaraland�rma Listesi',
  // Customize, Reset, None
  '&�zelle�tir...', '&S�f�rla', 'Yok',
  // Numbering: continue, reset to, create new
  'Numaraland�rmaya Devam Et', '�lk numara', 'Yeni Liste Olu�tur, Ba�lang�c� :',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'B�y�k Harf', 'B�y�k Roma Rakam�', 'Normal Say�', 'K���k Harf', 'K���k Roma Rakam�',
  // Bullet type
  'Daire', 'Disc', 'Kare',
  // Level, Start from, Continue numbering, Numbering group-box
  '&A�ama:', '..en &Ba�la:', '&Devam', 'Numaralama',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Numaraland�rmay� �zelle�tir', 'D�zeyler', '&Toplam:', 'Numaraland�rma �zellikleri', '&Bi�imi:',
  // List types: bullet, image
  'Simge', 'Resim', 'Numara Ekle|',
  // Number format, Number, Start level from, Font button
  '&Say� Bi�imi:', 'Say�', '&Bu Seviyeden Numaraland�r:', '&Yaz� Tipi...',
  // Image button, bullet character, Bullet button,
  '&Simge...', '&Simge Karakteri', '&Simge...',
  // Position of list text, bullet, number, image, text
  'Listelenen Metin Konumu', 'Simge Konumu', 'Say� Konumu', 'Resim Konumu', 'Metin Konumu',
  // at, left indent, first line indent, from left indent
  '&dan:', '&Sol Girinti:', '&�lk Sat�r Girintisi:', 'Soldan �tibaren',
  // [only] one level preview, preview
  '&Bir Seviye G�ster', '�nizleme',
  // Preview text
  'Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Sola Yasla', 'Sa�a Yasla', 'Ortala',
  // level #, this level (for combo-box)
  'Seviye %d', 'Bu Seviye',
  // Marker character dialog title
  'Simge D�zenle',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Paragraf Kenar� ve Dolgu Rengi', 'Kenar', 'Dolgu Rengi',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Ayarlar', '&Renk:', '&Geni�lik:', '�� &Geni�lik:', '&Kenarlar...',
  // Sample, Border type group-boxes;
  '�rnek', 'Kenar Tipi',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Yok', '&Tek', '&�ift', '&��l�', '&��i Koyu', '&D��� Koyu',
  // Fill color group-box; More colors, padding buttons
  'Dolgu Rengi', '&T�m Renkler...', '&Kenarlar...',
  // preview text
  'Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin Metin',
  // title and group-box in Offsets dialog
  'Kenarlar', 'D�� Kenarlar',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragraf', 'Y�nelim', '&Sol', 'Sa&�', '&Ortala', '&Yasla',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Aral�k', '&�nce:', '&Sonra:', 'Sat�r Bo�lu�u:', '&Aral�k:',
  // Indents group-box; indents: left, right, first line
  'Girintiler', '&Sol:', 'Sa&�:', '&�lk Sat�r:',
  // indented, hanging
  '&��erden', '&D��ardan', '�rnek',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Tek Sat�r', '1.5 Sat�r', '�ift Sat�r', 'En Az (At least)', 'Tam (Exactly)', 'Birden Fazla Sat�r',
  // preview text
  'Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin. Metin Metin Metin Metin.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Girinti ve Bo�luk', 'Sekme', 'Metin Ak���',
  // tab stop position label; buttons: set, delete, delete all
  '&Sekme Dura�� Konumu:', '&Uygula', '&Sil', '&T�m�n� sil',
  // tab align group; left, right, center aligns,
  'Y�nelim', '&Sol', 'S&a�', '&Ortal�',
  // leader radio-group; no leader
  'K�lavuz �izgi', '(Yok)',
  // tab stops to be deleted, delete none, delete all labels
  'Silinecek sekme duraklar�:', '(Yok)', 'T�m�.',
  // Pagination group-box, keep with next, keep lines together
  'Ayarlar', '&Sonraki ile birlite tut', 'S&at�rlar� birlikte tut',
  // Outline level, Body text, Level #
  '�er�e&ve A�amas�:', 'G�vde Metni', 'A�ama %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Bask� �nizleme', 'Sayfa Geni�li�i', 'T�m Sayfa', 'Sayfalar:',
  // Invalid Scale, [page #] of #
  '10-500 Aras�nda bir de�er girin', '/ %d',
  // Hints on buttons: first, prior, next, last pages
  '�lk Sayfa', '�nceki Sayfa', 'Sonraki Sayfa', 'Son Sayfa',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'H�cre Bo�lu�u', 'Bo�luklar', 'A&ras�ndaki H�creler', 'Bu Tablo Kenar�ndan �tibaren H�creler',
  // vertical, horizontal (x2)
  '&Dikey:', '&Yatay:', 'Dik&ey:', 'Y&atay:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kenarlar', 'Ayarlar', '&Renk:', '&A��k Renk:', 'G�lge Rengi:',
  // Width; Border type group-box;
  '&Geni�lik:', 'Kenarl�k Tipi',
  // Border types: none, sunken, raised, flat
  '&Yok', '&Bas�k', '&Kabar�k', '&D�z',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'H�creleri B�l', 'B�lme Yeri',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Sat�r ve S�tun Say�s�na G�re', '&Orjinal H�creler',
  // number of columns, rows, merge before
  'S�tun Say�s�:', 'Sat�r Say�s�:', '&B�lmeden �nce H�creleri Birle�tir',
  // to original cols, rows check-boxes
  'E�it Geni�likte S�tunlara B�l', 'E�it Y�kseklikteki Sat�rlara B�l',
  // Add Rows form -------------------------------------------------------------
  'Sat�r Ekle', '&Sat�r Say�s�:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Sayfa Yap�s�', 'Sayfa', '�st ve Alt Bilgi',
  // margins group-box, left, right, top, bottom
  'Kenar Bo�lu�u (mm)', 'Kenar Bo�lu�u (in�)', '&Sol:', '&�st:', '&Sa�:', '&Alt:',
  // mirror margins check-box
  '&Ka��da S��d�r',
  // orientation group-box, portrait, landscape
  'Y�nlendirme', '&Dikey', '&Yatay',
  // paper group-box, paper size, default paper source
  'Sayfa', 'Ka��t &B�y�kl���:', '&Ka��t Kayna��:',
  // header group-box, print on the first page, font button
  'Ba�l�k', '&Metin:', '&�st Bilgiyi �lk Sayfada G�ster', '&Yaz� Tipi...',
  // header alignments: left, center, right
  '&Sol', '&Orta', '&Sa�',
  // the same for footer (different hotkeys)
  'Alt Bilgi', '&Metin:', 'Alp Bilgiyi �lk Sayfada G�ster', 'Ya&z� Tipi...',
  'So&l', '&Orta', 'Sa&�',
  // page numbers group-box, start from
  'Sayfa Numaralar�', '&Ba�lang�� :',
  // hint about codes
  '�zel Karakter Bile�enleri:'#13'&&p - Sayfa Numaras�; &&P - Toplam Sayfa; &&d - G�n�n Tarihi; &&t - Ge�erli Saat.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Metin Dosyas� Karakter Kodlamas�', 'Dosya i�eri�inin karakter kodlamas�n� se�iniz:',
  // thai, japanese, chinese (simpl), korean
  'Thai Dili', 'Japonca', '�ince (Basit)', 'Korece',
  // chinese (trad), central european, cyrillic, west european
  '�ince (geli�mi�)', 'Orta ve Do�u Avrupa', 'Kril', 'Bat� Avrupa',
  // greek, turkish, hebrew, arabic
  'Yunanca','T�rk�e', '�branice', 'Arap�a',
  // baltic, vietnamese, utf-8, utf-16
  'Balt�k', 'Vietnamca', 'Evrensel (UTF-8)', 'Evrensel (UTF-16)',
  // Style form ----------------------------------------------------------------
  'G�rsel stil', '&Stil Se�:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Metne �evir', 'Ay�ra� Se�:',
  // line break, tab, ';', ','
  'Sat�r', 'Tab', 'Noktal� virg�l', 'Nokta',
  // Table sort form -----------------------------------------------------------
  // error message
   'Birle�tirilmi� h�cre i�eren tablolar s�ralanamaz',
  // title, main options groupbox
   'Tabloyu S�rala', 'S�rala',
  // sort by column, case sensitive
   '&Kolona G�re S�rala:', '&B�y�k k���k harf duyarl�',
  // heading row, range or rows
   'Ba�l�k Sat�r�n� Dikkate Alma', 'S�ralanacak Sat�rlar: %d den %d e kadar',
  // order, ascending, descending
   'S�ralama', '&Artan', 'A&zalan',
  // data type, text, number
   'Tip', '&Metin', '&Say�',

  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Numara Ekle', '�zellikler', 'Saya� ad�:', 'Numaraland�rma tipi:',
  // numbering groupbox, continue, start from
  'Numaraland�rma', 'Devam et', '.. den ba�la:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Ba�l�k', 'Etiket:', 'Ba�l�ktan Etiket',
  // position radiogroup
  'Konum', 'Se�ili nesnenin �st�nde', 'Se�ili nesnenin alt�nda',
  // caption text
  'Ba�l�k Yaz�s�:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numaraland�rma', '�ekil', 'Tablo',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'G�r�n�r Kenar', 'Kenarl�k',
  // Left, Top, Right, Bottom checkboxes
  'Sol Kenar', '�st Kenar', 'Sa� Kenar', 'Alt Kenar',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'S�tunu Boyutland�r', 'Sat�r� Boyutland�r',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Paragraf ba��', 'Sol girinti', 'As�l� girinti', 'Sa� girinti',
  // Hints on lists: up one level (left), down one level (right)
  'Bir seviye yukar�', 'Bir seviye a�a��',
  // Hints for margins: bottom, left, right and top
  'Alt bo�luk', 'Sol bo�luk', 'Sa� bo�luk', '�st bo�luk',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Sola dayal� sekme', 'Sa�a dayal� sekme', 'Ortaya dayal� sekme', 'Orana g�re dayal�',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normal Girinti', 'Bo�luksuz', 'Ba�l�k %d', 'Paragraf Liste',
  // Hyperlink, Title, Subtitle
   'K�pr�', 'Ba�l�k', 'Alt Ba�l�k',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
   'Vurgu', 'Zay�f Vurgu', 'Orta Vurgu', 'Kuvvetli',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Al�nt�', 'Belirgin Al�nt�', 'Zay�f Referans', 'Belirgin Referans',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blok Metin', 'De�i�ken HTML ', 'HTML Kod', 'HTML K�saltma',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Tan�m', 'HTML Klavye', 'HTML �rnek', 'HTML Metin',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Formatl�', 'HTML Benzer', '�st Bilgi', 'Alt Bilgi', 'Sayfa Numaras�',
  // Caption
  'Ba�l�k',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Note Referans', 'Sayfa Alt� Referans', 'Note Metni', 'Sayfa Alt� Metin',
  // Sidenote Reference, Sidenote Text
  'Yan Not Referans', 'Yan Not Metni',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
   'Renk', 'Zemin Rengi', 'Transparan', 'Varsay�lan', 'Alt �izgi Rengi',
  // default background color, default text color, [color] same as text
   'Varsay�lan Zemin Rengi', 'varsay�lan yaz� rengi', 'yaz� ile ayn�',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'tek', 'ince', '�ift', 'noktal�', 'ince ve noktal�', 'tireli',
  // underline types: thick dashed, long dashed, thick long dashed,
  'k�sa tire', 'uzun tire', 'k�sa uzun tire',
  // underline types: dash dotted, thick dash dotted,
  'noktal� tire', 'noktal� k�sa tire',
  // underline types: dash dot dotted, thick dash dot dotted
   'tire ve iki nokta', 'k�sa tire ve iki nokta',
  // sub/superscript: not, subsript, superscript
  'simge /simge de�il', 'alt simge', 'ust simge',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'Yaz�m y�n�:', 'varsay�lan', 'soldan sa�a', 'sa�dan sola',
  // bold, not bold
  'koyu', 'koyu de�il',
  // italic, not italic
  'italik', 'italik de�il',
  // underlined, not underlined, default underline
   'alt �izili', 'alt �izgisiz', 'varsay�lan alt �izgi',
  // struck out, not struck out
  'vurgulu', 'vurgusuz',
  // overlined, not overlined
  'sat�r ta�mal�', 'sat�r ta�mas�z',
  // all capitals: yes, all capitals: no
  't�m� b�y�k', 't�m� b�y�k kapal�',
  // vertical shift: none, by x% up, by x% down
  '�telemesiz', '%d%% yukar� �telendi', '%d%% a�a�� �telendi',
  // characters width [: x%]
  'karekter geni�li�i',
  // character spacing: none, expanded by x, condensed by x
  'normal karekter bo�lu�u', 'geni�letilmi� bo�luk %s', 's�k��t�r�lm�� bo�luk %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'varsay�lan font', 'varsay�lan paragraf', 'devameden',
  // [hyperlink] highlight, default hyperlink attributes
  'vurgulnm��', 'varsay�lan',
  // paragraph aligmnment: left, right, center, justify
  'sola dayal�', 'sa�a dayal�', 'ortal�', 'sa�a hizal�',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'sat�r y�ksekli�i: %d%%', 'sat�r bo�lu�u: %s', 'sat�r y�ksekli�i(en az): %s',
  'sat�r y�ksekli�i(tam): %s',
  // no wrap, wrap
  'sat�r kayma kapal�', 'sat�r kayma a��k',
  // keep lines together: yes, no
  'sat�rlar� birlikte tut', 'sat�rlar� birlikte tutma',
  // keep with next: yes, no
  'sonraki paragraf ile birlikte', 'sonraki paragraftan ayr�',
  // read only: yes, no
  'sadece okunur', 'de�i�tirilebilir',
  // body text, heading level x
  'D�� �izgi: Ana metin', '�izgi: %d',
  // indents: first line, left, right
  'ilk sat�r i�erden', 'soldan girinti', 'sa�dan girinti',
  // space before, after
  'bo�luk �nce', 'bo�luk sonra',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tab', 'yok', 'dayal�', 'sola', 'sa�a', 'ortaya',
  // tab leader (filling character)
  'G�sterge',
  // no, yes
  'hay�r', 'evet',
  // [padding/spacing/side:] left, top, right, bottom
  'sol', '�st', 'sa�', 'alt',
  // border: none, single, double, triple, thick inside, thick outside
  'yok', 'tek', '�ift', '��l�', 'i�eride ince', 'd��ta ince',
  // background, border, padding, spacing [between text and border]
  'Zemin', 'D�� �izgi', 'aral�k', 'bo�luk',
  // border: style, width, internal width, visible sides
  'stil', 'geni�lik', 'internal geni�lik', 'g�r�n�r k�s�m',
  // style inspector -----------------------------------------------------------
  // title
  'Stil Listesi',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(yok)', 'Paragraf', 'Font', '�zellikler',
  // border and background, hyperlink, standard [style]
  '�izgi ve Zemin', 'K�pr�', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stiller', 'Stil',
  // name, applicable to,
  '&Ad�:', '&Uygulanabilir:',
  // based on, based on (no &),
  '&Temel Al�nan:', 'Temel Al�nan:',
  //  next style, next style (no &)
  'Stil buradan sonraki paragraflar i�in', 'Stil buradan sonraki paragraflar i�in:',
  // quick access check-box, description and preview
  '&H�zl� Ula��m', 'A��klama  ve �&n g�r�n�m:',
  // [applicable to:] paragraph and text, paragraph, text
   'paragraf ve yaz�', 'paragraf', 'yaz�',
  // text and paragraph styles, default font
  'Yaz� ve Paragraf Stilleri', 'Varsay�lan Font',
  // links: edit, reset, buttons: add, delete
  'D�zenle', 's�f�rla', '&Ekle', '&Sil',
  // add standard style, add custom style, default style name
  'Standart Stil &Ekle...', '�zel Stil Ekle', 'Stil %d',
  // choose style
   'Stil &se�:',
  // import, export,
   '��e Aktar...', '&D��a Aktar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Stil (*.rvst)|*.rvst', 'Dosya Y�kleme Hatas�', 'Dosyada stil yok',

  // Title, group-box, import styles
   'Dosyadan Stil al', 'Al', 'Al�nan Stil:',
  // existing styles radio-group: Title, override, auto-rename
  'Varolan Stiller', '�st�ne Yaz', 'Yeniden Adland�r',
  // Select, Unselect, Invert,
  '&Se�', 'Se�imi �ptal Et', 'Se�imi �evir',
  // select/unselect: all styles, new styles, existing styles
   '&T�m Stiller', '&Yeni Stil', '&Varolan Stiller',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Format� Temizle', 'T�m Stiller',
  // dialog title, prompt
  'Stiller', '&Uygulanacak Stil:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&E� Anlaml�lar', '&T�m�n� Yoksay', '&S�zl��e Ekle',
  // Progress messages ---------------------------------------------------------
  '�ndiriliyor %s', 'Yazd�rmaya Haz�rlan�yor..', 'Yazd�r�lan Sayfa %d',
  'RTF den �evriliyor...',  'RTF ye �evriliyor...',
  'Okunuyor %s...', 'Yaz�l�yor %s...', 'Metin',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Yok', '�stbilgi', 'Altbilgi', 'Yanbilgi', 'Metin Kutusu'
  );


initialization
  RVA_RegisterLanguage(
    'Turkish', // english language name, do not translate
    // {*} 'Turkish', // native language name
    'T�rk�e', // native language name
    TURKISH_CHARSET, @Messages);
end.
