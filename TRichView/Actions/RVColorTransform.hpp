﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVColorTransform.pas' rev: 27.00 (Windows)

#ifndef RvcolortransformHPP
#define RvcolortransformHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvcolortransform
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall SaturationBitmap(Vcl::Graphics::TBitmap* const Bmp, const int Amount);
extern DELPHI_PACKAGE void __fastcall InvertBitmap(Vcl::Graphics::TBitmap* bmp);
extern DELPHI_PACKAGE void __fastcall SaturationColors(System::Uitypes::TColor *Colors, const int Colors_High, const int Amount);
extern DELPHI_PACKAGE void __fastcall ColorizeBitmapAlter(Vcl::Graphics::TBitmap* const Bmp, const System::Uitypes::TColor CColor);
extern DELPHI_PACKAGE void __fastcall ColorizeColorsAlter(System::Uitypes::TColor *Colors, const int Colors_High, const System::Uitypes::TColor CColor);
extern DELPHI_PACKAGE void __fastcall DrawAlphaBitmap(Vcl::Graphics::TBitmap* bmp, Vcl::Graphics::TBitmap* target, int X, int Y);
}	/* namespace Rvcolortransform */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVCOLORTRANSFORM)
using namespace Rvcolortransform;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvcolortransformHPP
