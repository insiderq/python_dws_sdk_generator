
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Cell-spacing dialog                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit SpacingRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, RVStyle,
  Dialogs, BaseRVFrm, FourSidesRVFrm, ExtCtrls, StdCtrls, RVSpinEdit, RVALocalize;

type
  TfrmRVSpacing = class(TfrmRVFourSides)
    Label6: TLabel;
    Bevel1: TBevel;
    cbCellSpacing: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure cbCellSpacingClick(Sender: TObject);
    procedure seTopChange(Sender: TObject);
    procedure seBottomChange(Sender: TObject);
    procedure seLeftChange(Sender: TObject);
    procedure seRightChange(Sender: TObject);
  private
    { Private declarations }
    _cbCellSpacing: TControl;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}    
  public
    { Public declarations }
    procedure Localize; override;
    procedure SetSpacing(BorderHSpacing, CellHSpacing,
      BorderVSpacing, CellVSpacing: TRVStyleLength; RVStyle: TRVStyle;
      Units: TRVUnits);
    procedure GetSpacing(var BorderHSpacing, CellHSpacing,
      BorderVSpacing, CellVSpacing: TRVStyleLength;
      var FBorderHSpacing, FCellHSpacing,
      FBorderVSpacing, FCellVSpacing: Boolean;
      RVStyle: TRVStyle; Units: TRVUnits);
  end;

implementation                                                           

uses RichViewactions;

{$R *.dfm}

{ TfrmRVSpacing }

procedure TfrmRVSpacing.Localize;
begin
  inherited;
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiRTF:
      seLeft.Enabled := False;
  end;

  Caption := RVA_GetS(rvam_cs_Title, ControlPanel);
  gb.Caption := RVA_GetSHAsIs(rvam_cs_GB, ControlPanel);
  cbCellSpacing.Caption := RVA_GetSAsIs(rvam_cs_BetweenCells, ControlPanel);
  Label6.Caption := RVA_GetSAsIs(rvam_cs_FromTableToCell, ControlPanel);
  lblTop.Caption := RVA_GetSAsIs(rvam_cs_Horz1, ControlPanel);
  lblBottom.Caption := RVA_GetSAsIs(rvam_cs_Vert1, ControlPanel);
  lblLeft.Caption := RVA_GetSAsIs(rvam_cs_Horz2, ControlPanel);
  lblRight.Caption := RVA_GetSAsIs(rvam_cs_Vert2, ControlPanel);
end;

procedure TfrmRVSpacing.FormCreate(Sender: TObject);
begin
  _cbCellSpacing := cbCellSpacing;
  inherited;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVSpacing.OnCreateThemedControl(OldControl, NewControl: TControl);
begin
  if OldControl =_cbCellSpacing then
    _cbCellSpacing := NewControl
  else
    inherited OnCreateThemedControl(OldControl, NewControl);
end;
{$ENDIF}

procedure TfrmRVSpacing.cbCellSpacingClick(Sender: TObject);
begin
  if not GetCheckBoxChecked(_cbCellSpacing) then begin
    SetCheckBoxChecked(_cbEqual, False);
    seTop.Indeterminate := True;
    seBottom.Indeterminate := True;
    end
  else begin
    seTop.Value := seTop.Value;
    seBottom.Value := seBottom.Value;
    seLeft.Value := seLeft.Value;
    seRight.Value := seRight.Value;
  end;
end;

procedure TfrmRVSpacing.SetSpacing(BorderHSpacing, CellHSpacing,
      BorderVSpacing, CellVSpacing: TRVStyleLength; RVStyle: TRVStyle; Units: TRVUnits);
begin
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiRTF:
      begin
        BorderHSpacing := CellHSPacing;
      end;
    rvauiHTML:
      begin
        if (BorderHSpacing < CellHSPacing) or (CellHSpacing<0) then
          BorderHSpacing := CellHSPacing;
        if (BorderVSpacing < CellVSpacing) or (CellVSpacing<0) then
          BorderVSpacing := CellVSpacing;
      end;
  end;
  SetValues2(BorderHSpacing, CellHSpacing, BorderVSpacing, CellVSpacing,
    True, True, True, True, RVStyle, 0, Units);
  if (CellHSpacing<0) or (CellVSpacing<0) then begin
    SetCheckBoxChecked(_cbCellSpacing, False);
  end;
end;


procedure TfrmRVSpacing.GetSpacing(var BorderHSpacing, CellHSpacing,
  BorderVSpacing, CellVSpacing: TRVStyleLength; var FBorderHSpacing,
  FCellHSpacing, FBorderVSpacing, FCellVSpacing: Boolean;
  RVStyle: TRVStyle; Units: TRVUnits);
begin
  GetValues(BorderHSpacing, CellHSpacing, BorderVSpacing, CellVSpacing,
    FBorderHSpacing, FCellHSpacing, FBorderVSpacing, FCellVSpacing,
    RVStyle, Units);
  if not GetCheckBoxChecked(_cbCellSpacing) then begin
    CellHSpacing := -1;
    CellVSpacing := -1;
    FCellHSpacing := True;
    FCellVSpacing := True;
    case TRVAControlPanel(ControlPanel).UserInterface of
      rvauiRTF:
        begin
          BorderHSpacing := 0;
          FBorderHSpacing := True;
        end;
      rvauiHTML:
        begin
          BorderHSpacing := CellHSPacing;
          BorderVSpacing := CellVSpacing;
          FBorderHSpacing := True;
          FBorderVSpacing := True;
        end;
    end;
  end;
end;

procedure TfrmRVSpacing.seTopChange(Sender: TObject);
begin
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiRTF:
      begin
        seLeft.Value := seTop.Value;
        seLeft.Indeterminate := seTop.Indeterminate;
      end;
    rvauiHTML:
      begin
        if seTop.Indeterminate then
          seLeft.Indeterminate := True
        else if (seLeft.Value<seTop.Value) or seLeft.Indeterminate then
          seLeft.Value := seTop.Value;
      end;
  end;
  inherited seTopChange(Sender);
  if not seTop.Indeterminate then
    SetCheckBoxChecked(_cbCellSpacing, True);
end;

procedure TfrmRVSpacing.seBottomChange(Sender: TObject);
begin
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiHTML:
      begin
        if seBottom.Indeterminate then
          seRight.Indeterminate := True
        else if (seRight.Value<seBottom.Value) or seRight.Indeterminate then
          seRight.Value := seBottom.Value;
      end;
  end;
  inherited seTopChange(Sender);
  if not seBottom.Indeterminate then
    SetCheckBoxChecked(_cbCellSpacing, True);
end;

procedure TfrmRVSpacing.seLeftChange(Sender: TObject);
begin
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiHTML:
      begin
        if seLeft.Indeterminate then
          seTop.Indeterminate := True
        else if (seLeft.Value<seTop.Value) or seTop.Indeterminate then
          seTop.Value := seLeft.Value;
      end;
  end;
  inherited seTopChange(Sender);
  if not seTop.Indeterminate then
    SetCheckBoxChecked(_cbCellSpacing, True);
end;

procedure TfrmRVSpacing.seRightChange(Sender: TObject);
begin
  case TRVAControlPanel(ControlPanel).UserInterface of
    rvauiHTML:
      begin
        if seRight.Indeterminate then
          seBottom.Indeterminate := True
        else if (seRight.Value<seBottom.Value) or seBottom.Indeterminate then
          seBottom.Value := seRight.Value;
      end;
  end;
  inherited seTopChange(Sender);
  if not seBottom.Indeterminate then
    SetCheckBoxChecked(_cbCellSpacing, True);
end;



end.
