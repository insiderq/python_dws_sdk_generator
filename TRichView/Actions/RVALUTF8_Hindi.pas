﻿// This file is a copy of RVAL_Hindi.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Hindi translation                               }
{                                                       }
{                                                       }
{*******************************************************}

unit RVALUTF8_Hindi;

interface
{$I RV_Defs.inc}
implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'इंच', 'सेंमी', 'एमएम', 'पाइका', 'पिक्सेल', 'बिंदु',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'सेंमी', 'एमएम', 'पाइका', 'पिक्सेल', 'बिंदु',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  'फाइल', 'संपादन', 'स्वरूपण', 'फौंट', 'अनुच्छेद', 'भरें', 'तालिका', 'विंडो', 'मदद',
  // exit
  'बाहर निकलें',
  // top-level menus: View, Tools,
  'दृश्य', 'उपकरण',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'फौन्ट आकार', 'फौन्ट शैली', 'तालिका चयन', 'कक्ष सामग्री संरेखण', 'कक्ष किनारे',
  // menus: Table cell rotation
  'तालिका कक्ष घुमाव',
  // menus: Text flow, Footnotes/endnotes
  'पाठ प्रवाह', 'पादनोट/अन्तनोट',
  // ribbon tabs: tab1, tab2, view, table
  'होम', 'उन्नत', 'दृश्य', 'तालिका',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'क्लिपबोर्ड', 'फौन्ट', 'अनुच्छेद', 'सूची', 'संपादन',
  // ribbon groups: insert, background, page setup,
  'भरें', 'पृष्ठभूमि', 'पृष्ठ सेटअप',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'लिंक्स', 'पादनोट्स', 'दस्तावेज़ दृश्य', 'दिखाएं/छुपाएं', 'ज़ूम', 'विकल्प',
  // ribbon groups on table tab: insert, delete, operations, borders
  'भरें', 'हटाएं', 'कार्य', 'किनारे',
  // ribbon groups: styles 
  'शैलियाँ',
  // ribbon screen tip footer,
  'और मदद के लिए F1 दबाएं',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'हाल ही के दस्तावेज़', 'दस्तावेज़ की प्रतिलिपी सहेजें', 'पूर्वालोकन तथा दस्तावेज़ मुद्रण करें',
  // ribbon label: units combo
  'इकाईयाँ:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'नया', 'नया|कोरा नया दस्तावेज़ बनाया जाएगा',
  // TrvActionOpen
  'खोलें...', 'खोलें|डिस्क में स्थित दस्तवेज़ खोलें',
  // TrvActionSave
  'सहेजें', 'सहेजें|दस्तावेज़ को डिस्क में सहेजें',
  // TrvActionSaveAs
  'नए नाम से सहेजें...', 'नए नाम से सहेजें...|दस्तावेज़ को डिस्क में नए नाम से सहेजें',
  // TrvActionExport
  'निर्यात करें...', 'निर्यात करें|दस्तावेज़ को अन्य स्वरूप में निर्यात करें',
  // TrvActionPrintPreview
  'मुद्रण पूर्वावलोकन', 'मुद्रण पूर्वावलोकन|देखिए दस्तावेज़ कैसा मुद्रित होगा',
  // TrvActionPrint
  'मुद्रण...', 'मुद्रण|मुद्रण निर्धारण बदलें और दस्तावेज़ मुद्रित करें',
  // TrvActionQuickPrint
  'मुद्रण', 'त्वरित मुद्रण', 'मुद्रण|दस्तावेज़ मुद्रित करें',
  // TrvActionPageSetup
  'पृष्ठ निर्धारण...', 'पृष्ठ निर्धारण|हाशिए निर्धारण, पृष्ठ आकार, रुख़, स्त्रोत, शीर्षलेख तथा पादलेख',
  // TrvActionCut
  'काटें', 'काटें|चयनित को काट के क्लिपबोर्ड में रखें',
  // TrvActionCopy
  'प्रतिलिपि बनाएं', 'प्रतिलिपि बनाएं|चयनित की प्रतिलिपि बना के क्लिपबोर्ड में रखें',
  // TrvActionPaste
  'चिपकाएं', 'चिपकाएं|क्लिपबोर्ड में रखी सामग्री को चिपकाएं',
  // TrvActionPasteAsText
  'लेख रूप में चिपकाएं', 'लेख रूप में चिपकाएं|क्लिपबोर्ड से लेख सम्मिलित करें',  
  // TrvActionPasteSpecial
  'विशेष रूप में चिपकाएं...', 'विशेष रूप में चिपकाएं|क्लिपबोर्ड में रखी सामग्री को निर्दिष्ट स्वरूप में चिपकाएं',
  // TrvActionSelectAll
  'सभी चुनें', 'सभी चुनें|संपूर्ण दस्तावेज़ सामग्री चुनें',
  // TrvActionUndo
  'पूर्ववत करें', 'पूर्ववत करें|अंतिम क्रिया को पलटें',
  // TrvActionRedo
  'फिर से करें', 'फिर से करें|अंतिम पलटी गई क्रिया को पुनः करें',
  // TrvActionFind
  'ढूँढें...', 'ढूँढें|निर्दिष्ट लेख को दस्तावेज़ में ढूँढें',
  // TrvActionFindNext
  'अगला ढूँढें', 'अगला ढूँढें|पिछली खोज को ज़ारी रखें',
  // TrvActionReplace
  'बदलें...', 'बदलें|निर्दिष्ट लेख को दस्तावेज़ में ढूँढकर बदलें',
  // TrvActionInsertFile
  'फाइल...', 'फाइल जोड़ें|फाइल की सामग्री को लेख में जोड़ें',
  // TrvActionInsertPicture
  'चित्र...', 'चित्र जोड़ें|डिस्क से लेकर चित्र जोड़ें',
  // TRVActionInsertHLine
  'क्षैतिज रेखा', 'क्षैतिज रेखा जोड़ें|क्षैतिज रेखा जोड़ें',
  // TRVActionInsertHyperlink
  'हाइपरलिंक...', 'हाइपरलिंक जोड़ें|हाइपर्टेक्स्ट लिंक जोड़ें',
  // TRVActionRemoveHyperlinks
  'हाइपरलिंक्स हटाएँ', 'हाइपरलिंक्स हटाएँ|चयनित लेख में से सभी हाइपरलिंक्स हटाएँ',  
  // TrvActionInsertSymbol
  'चिन्ह...', 'चिन्ह जोड़ें|चिन्ह जोड़ें',
  // TrvActionInsertNumber
  'संख्या...', 'संख्या भरें|संख्या भरी जाएगी',
  // TrvActionInsertCaption
  'शीर्षक भरें...', 'शीर्षक भरें| चुने हुए विषय के लिए शीर्षक भरा जाएगा',
  // TrvActionInsertTextBox
  'टेक्स्ट बौक्स', 'टेक्स्ट बौक्स लगाएं|टेक्स्ट बौक्स लगाया जाएगा',
  // TrvActionInsertSidenote
  'साइडनोट', 'साइडनोट भरें|टेक्स्ट बौक्स में दिखाया जाने वाला नोट भरा जाएगा',
  // TrvActionInsertPageNumber
  'पृष्ठ संख्या', 'पृष्ठ संख्या भरें| पृष्ठ संख्या भरी जाएगी',
  // TrvActionParaList
  'बुलेट्स तथा संख्याएं...', 'बुलेट्स तथा संख्याएं|चयनित अनुच्छेदों में बुलेट या संख्या लगाएं अथवा संपादित करें',
  // TrvActionParaBullets
  'बुलेट्स', 'बुलेट्स|अनुच्छेद में बुलेट लगाएं या हटाएँ',
  // TrvActionParaNumbering
  'संख्याएं', 'संख्याएं|अनुच्छेद की संख्या लगाएं या हटाएँ',
  // TrvActionColor
  'पृष्ठभूमि का रंग...', 'पृष्ठभूमि|दस्तावेज़ की पृष्ठभूमि का रंग बदलें',
  // TrvActionFillColor
  'रंग भरें...', 'रंग भरें|लेख, अनुच्छेद, कक्ष, तालिका या दस्तावेज़ की पृष्ठभूमि का रंग बदलें',
  // TrvActionInsertPageBreak
  'पृष्ठ ब्रेक करें', 'पृष्ठ ब्रेक करें|निर्धारित स्थान से नया पृष्ठ आरंभ करें',
  // TrvActionRemovePageBreak
  'पृष्ठ ब्रेक हटाएँ', 'पृष्ठ ब्रेक हटाएँ|किए गए पृष्ठ ब्रेक को हटाएँ',
  // TrvActionClearLeft
  'बाईं ओर का लेख प्रवाह साफ करें', 'बाईं ओर का लेख प्रवाह साफ करें|अनुच्छेद को किसी भी बाएं संरेखित चित्र के नीचे लगाएं',
  // TrvActionClearRight
  'दाहिनी ओर का लेख प्रवाह साफ करें', 'दाहिनी ओर का लेख प्रवाह साफ करें|अनुच्छेद को किसी भी दाहिने संरेखित चित्र के नीचे लगाएं',
  // TrvActionClearBoth
  'दोनो ओर का लेख प्रवाह साफ करें', 'दोनो ओर का लेख प्रवाह साफ करें|अनुच्छेद को किसी भी दाहिने या बाएं संरेखित चित्र के नीचे लगाएं',
  // TrvActionClearNone
  'सामान्य लेख प्रवाह', 'सामान्य लेख प्रवाह|दाहिने अथवा बाएं संरेखित चित्रों के दोनों ओर लेख प्रवाह होने देता है',
  // TrvActionVAlign
  'ऑब्जेक्ट स्थिति...', 'ऑब्जेक्ट स्थिति|चुने हुए ऑब्जेक्ट की स्थिति बदलें',  
  // TrvActionItemProperties
  'ऑब्जेक्ट गुण...', 'ऑब्जेक्ट गुण|सक्रीय ऑब्जेक्ट के गुण निरधारित करें',
  // TrvActionBackground
  'पृष्ठभूमि...', 'पृष्ठभूमि|पृष्ठभूमि के रंग और चित्र चुनें',
  // TrvActionParagraph
  'अनुच्छेद...', 'अनुच्छेद|अनुच्छेद के गुण बदलें',
  // TrvActionIndentInc
  'इंडेंट बढ़ाएं', 'इंडेंट बढ़ाएं|चुने हुए अन्च्छेद का बायां इंडेंट बढ़ाएं',
  // TrvActionIndentDec
  'इंडेंट घटाएं', 'इंडेंट घटाएं|चुने हुए अन्च्छेद का बायां इंडेंट घटाएं',
  // TrvActionWordWrap
  'वर्ड रैप', 'वर्ड रैप|चुने हुए अनुच्छेद के लिए वर्ड रैप आरंभ अथवा समाप्त करें',
  // TrvActionAlignLeft
  'बाएं संरेखित', 'बाएं संरेखित|चुने हुए लेख को बाएं संरेखित करें',
  // TrvActionAlignRight
  'दाहिने संरेखित', 'दाहिने संरेखित|चुने हुए लेख को दाहिने संरेखित करें',
  // TrvActionAlignCenter
  'मध्य संरेखित', 'मध्य संरेखित|चुने हुए लेख को मध्य से संरेखित करें',
  // TrvActionAlignJustify
  'समायोजित', 'समायोजित|चुने हुए लेख को दाहिने बाएं दोनो छोर से संरेखित करें',
  // TrvActionParaColor
  'अनुच्छेद पृष्ठभूमि रंग...', 'अनुच्छेद पृष्ठभूमि रंग|अनुच्छेद की पृष्ठभूमि के रंग को निर्धारित करें',
  // TrvActionLineSpacing100
  'एकल लाइन अंतर', 'एकल लाइन अंतर|एकल लाइन अंतर निर्धारित करें',
  // TrvActionLineSpacing150
  '1.5 लाइन अंतर', '1.5 लाइन अंतर|1.5 लाइन अंतर निर्धारित करें',
  // TrvActionLineSpacing200
  'दो लाइन अंतर', 'दो लाइन अंतर|दो लाइन अंतर निर्धारित करें',
  // TrvActionParaBorder
  'अनुच्छेद के किनारे एवं पृष्ठभूमि...', 'अनुच्छेद के किनारे एवं पृष्ठभूमि|चुने हुए अनुच्छेद के लिए किनारे और पृष्ठभूमि निर्धारित करें',
  // TrvActionInsertTable
  'तालिका जोड़ें...', 'तालिका', 'तालिका जोड़ें|नई तालिका जोड़ें',
  // TrvActionTableInsertRowsAbove
  'ऊपर पंक्ति जोड़ें', 'ऊपर पंक्ति जोड़ें|चयनित कक्षों के ऊपर नई पंक्ति जोड़ें',
  // TrvActionTableInsertRowsBelow
  'नीचे पंक्ति जोड़ें', 'नीचे पंक्ति जोड़ें|चयनित कक्षों के नीचे नई पंक्ति जोड़ें',
  // TrvActionTableInsertColLeft
  'बाईं ओर कॉलम जोड़ें', 'बाईं ओर कॉलम जोड़ें|चयनित कक्षों के बाईं ओर नया कॉलम जोड़ें',
  // TrvActionTableInsertColRight
  'दाहिनी ओर कॉलम जोड़ें', 'दाहिनी ओर कॉलम जोड़ें|चयनित कक्षों के दाहिनी ओर नया कॉलम जोड़ें',
  // TrvActionTableDeleteRows
  'पंक्तियां हटाएं', 'पंक्तियां हटाएं|चयनित कक्षों वाली पंक्तियां हटाएं',
  // TrvActionTableDeleteCols
  'कॉलम हटाएं', 'कॉलम हटाएं|चयनित कक्षों वाले कॉलम हटाएं',
  // TrvActionTableDeleteTable
  'तालिका हटाएं', 'तालिका हटाएं|चयनित तालिका हटाएं',
  // TrvActionTableMergeCells
  'कक्ष विलय करें', 'कक्ष विलय करें|चयनित कक्षों को आपस में विलय करें',
  // TrvActionTableSplitCells
  'कक्ष विभाजित करें...', 'कक्ष विभाजित करें|चयनित कक्षों को विभाजित करें',
  // TrvActionTableSelectTable
  'तालिका चुनें', 'तालिका चुनें|तालिका को चयनित करें',
  // TrvActionTableSelectRows
  'पंक्तियां चुनें', 'पंक्तियां चुनें|पंक्तियां चयनित करें',
  // TrvActionTableSelectCols
  'कॉलम चुनें', 'कॉलम चुनें|कॉलम चयनित करें',
  // TrvActionTableSelectCell
  'कक्ष चुनें', 'कक्ष चुनें|कक्ष चयनित करें',
  // TrvActionTableCellVAlignTop
  'कक्ष संरेखण ऊपर से करें', 'कक्ष संरेखण ऊपर से करें|कक्ष की सामग्री को कक्ष के ऊपरी किनारे से संरेखित करें',
  // TrvActionTableCellVAlignMiddle
  'कक्ष संरेखण मध्य से करें', 'कक्ष संरेखण मध्य से करें|कक्ष की सामग्री को कक्ष के मध्य से संरेखित करें',
  // TrvActionTableCellVAlignBottom
  'कक्ष संरेखण नीचे से करें', 'कक्ष संरेखण नीचे से करें|कक्ष की सामग्री को कक्ष के निचले किनारे से संरेखित करें',
  // TrvActionTableCellVAlignDefault
  'कक्ष का डिफॉल्ट खड़ा संरेखन', 'कक्ष का डिफॉल्ट खड़ा संरेखन|चयनित कक्षों के खड़ा संरेखण को निर्धारित करें',
  // TrvActionTableCellRotationNone
  'कक्ष में कोई घुमाव नहीं', 'कक्ष में कोई घुमाव नहीं|कक्ष की सामग्री को 0° घुमाएं',
  // TrvActionTableCellRotation90
  'कक्ष में 90° घुमाव', 'कक्ष में 90° घुमाव|कक्ष की सामग्री को 90° घुमाएं',
  // TrvActionTableCellRotation180
  'कक्ष में 180° घुमाव', 'कक्ष में 180° घुमाव|कक्ष की सामग्री को 180° घुमाएं',
  // TrvActionTableCellRotation270
  'कक्ष में 270° घुमाव', 'कक्ष में 270° घुमाव|कक्ष की सामग्री को 270° घुमाएं',
  // TrvActionTableProperties
  'तालिका के गुण...', 'तालिका के गुण|चुनी हुई तालिका के गुणों में बदलाव करें',
  // TrvActionTableGrid
  'ग्रिड रेखाएं दिखाएं', 'ग्रिड रेखाएं दिखाएं|तालिका ग्रिड की रेखाओं को दिखाएं या छुपाएं',
  // TRVActionTableSplit
  'तालिका विभाजित करें', 'तालिका विभाजित करें|चुनी हुई पंक्ति से तालिका को दो तालिकाओं में विभाजित करें',
  // TRVActionTableToText
  'लेख में परिवर्तित करें...', 'लेख में परिवर्तित करें|तालिका को लेख में परिवर्तित करें',
  // TRVActionTableSort
  'क्रमवार...', 'क्रमवार|तालिका पंक्तियों को क्रमवार करें',
  // TrvActionTableCellLeftBorder
  'बायां किनारा', 'बायां किनारा|कक्ष का बायां किनारा दिखाएं या छुपाएं',
  // TrvActionTableCellRightBorder
  'दाहिना किनारा', 'दाहिना किनारा|कक्ष का दाहिना किनारा दिखाएं या छुपाएं',
  // TrvActionTableCellTopBorder
  'ऊपरी किनारा', 'ऊपरी किनारा|कक्ष का ऊपरी किनारा दिखाएं या छुपाएं',
  // TrvActionTableCellBottomBorder
  'निचला किनारा', 'निचला किनारा|कक्ष का निचला किनारा दिखाएं या छुपाएं',
  // TrvActionTableCellAllBorders
  'सभी किनारे', 'सभी किनारे|कक्ष के सभी किनारे दिखाएं या छुपाएं',
  // TrvActionTableCellNoBorders
  'कोई किनारा नहीं', 'कोई किनारा नहीं|कक्ष के सभी किनारे छुपाएं',
  // TrvActionFonts & TrvActionFontEx
  'फौन्ट...', 'फौन्ट|चुने हुए लेख के फौन्ट को बदलें',
  // TrvActionFontBold
  'बोल्ड', 'बोल्ड|चुने हुए लेख की शैली को बोल्ड में परिवर्तित करें',
  // TrvActionFontItalic
  'तिरछे', 'तिरछे|चुने हुए लेख की शैली को तिरछे अक्षरों में परिवर्तित करें',
  // TrvActionFontUnderline
  'रेखांकित', 'रेखांकित|चुने हुए लेख को रेखांकित करें',
  // TrvActionFontStrikeout
  'काटा हुआ', 'काटा हुआ|चुने हुए लेख को काटा हुआ करें',
  // TrvActionFontGrow
  'फौन्ट बढ़ाएं', 'फौन्ट बढ़ाएं|चुने हुए लेख के फौन्ट को 10% बढ़ाएं',
  // TrvActionFontShrink
  'फौन्ट घटाएं', 'फौन्ट घटाएं|चुने हुए लेख के फौन्ट को 10% घटाएं',
  // TrvActionFontGrowOnePoint
  'फौन्ट एक बिंदु बढ़ाएं', 'फौन्ट एक बिंदु बढ़ाएं|चुने हुए लेख के आकार को 1 बिंदु बढ़ाएं',
  // TrvActionFontShrinkOnePoint
  'फौन्ट एक बिंदु घटाएं', 'फौन्ट एक बिंदु घटाएं|चुने हुए लेख के आकार को 1 बिंदु घटाएं',
  // TrvActionFontAllCaps
  'सभी बड़े अक्षरों में', 'सभी बड़े अक्षरों में|चुने हुए लेख के सभी अक्षरों को बड़े अक्षरों में परिवर्तित करें',
  // TrvActionFontOverline
  'ऊपर रेखांकित', 'ऊपर रेखांकित|चुने हुए लेख के ऊपर रेखा लगाएं',
  // TrvActionFontColor
  'लेख का रंग...', 'लेख का रंग|चुने हुए लेख के रंग को बदलें',
  // TrvActionFontBackColor
  'लेख पृष्ठभूमि का रंग...', 'लेख पृष्ठभूमि का रंग|चुने हुए लेख की पृष्ठभूमि के रंग को बदलें',
  // TrvActionAddictSpell3
  'वर्तनी जांचें', 'वर्तनी जांचें|वर्तनी की जांच करें',
  // TrvActionAddictThesaurus3
  'थिसॉरस', 'थिसॉरस|चुने हुए शब्द के पर्यायवाची देखें',
  // TrvActionParaLTR
  'बाएं से दाहिने', 'बाएं से दाहिने|चुने हुए अनुच्छेदों के लेख की दिशा बाएं से दाहिने करें',
  // TrvActionParaRTL
  'दाहिने से बाएं', 'दाहिने से बाएं|चुने हुए अनुच्छेदों के लेख की दिशा दाहिने से बाएं करें',
  // TrvActionLTR
  'बाएं से दाहिने लेख', 'बाएं से दाहिने लेख|चुने हुए लेख की दिशा बाएं से दाहिने करें',
  // TrvActionRTL
  'दाहिने से बाएं लेख', 'दाहिने से बाएं लेख|चुने हुए लेख की दिशा दाहिने से बाएं करें',
  // TrvActionCharCase
  'वर्ण केस', 'वर्ण केस|चुने हुए लेख के वर्ण केस को बदलें',
  // TrvActionShowSpecialCharacters
  'मुद्रित ना होने वाले वर्ण', 'मुद्रित ना होने वाले वर्ण|मुद्रित नहीं किए जाने वाले वर्ण जैसे अनुच्छेद चिन्ह, टैब तथा रिक्त स्थान इत्यादि दिखाएं या छुपाएं',
  // TrvActionSubscript
  'सबस्क्रिप्ट', 'सबस्क्रिप्ट|चुने हुए लेख को सबस्क्रिप्ट बनाएं',
  // TrvActionSuperscript
  'सुपरस्क्रिप्ट', 'सुपरस्क्रिप्ट|चुने हुए लेख को सुपरस्क्रिप्ट बनाएं',
  // TrvActionInsertFootnote
  'पादनोट', 'पादनोट|पादनोट जोड़ें',
  // TrvActionInsertEndnote
  'अंतनोट', 'अंतनोट|अंतनोट जोड़ें',
  // TrvActionEditNote
  'नोट संपादन', 'नोट संपादन|पादनोट या अंतनोट का संपादन',
  // TrvActionHide
  'छुपाएं', 'छुपाएं|चुने हुए भाग को छुपाएं या दिखाएं',  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  'शैलियाँ...', 'शैलियाँ|शैली प्रबंधन डायलौग बॉक्स खोलें',
  // TrvActionAddStyleTemplate
  'शैली जोड़ें...', 'शैली जोड़ें।चयनित खण्ड के आधार पर लेख या अनुच्छेद की नई शैली बनाएं',
  // TrvActionClearFormat
  'स्वरूप मिटाएं', 'स्वरूप मिटाएं।चयनित खण्ड से समस्त लेख एवं अनुच्छेद स्वरूपिकरण मिटाता है',
  // TrvActionClearTextFormat,
  'लेख स्वरूपिकरण मिटाएं', 'लेख स्वरूपिकरण मिटाएं।चयनित लेख से सारा स्वरूपिकरण मिटाएं',
  // TrvActionStyleInspector
  'शैली निरीक्षक', 'शैली निरीक्षक।शैली निरीक्षक को दिखाता या छुपाएं',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'ठीक है', 'रद्द करें', 'बन्द करें', 'जोड़ें', 'खोलें...', 'सहेजें...', 'साफ करें', 'मदद', 'हटाएँ',
  // Others  -------------------------------------------------------------------
  // percents
  'प्रतिशत',
  // left, top, right, bottom sides
  'बाएं', 'ऊपर', 'दाहिने', 'नीचे',
  // save changes? confirm title
  '%s में हुए बदलाव सहेजें?', 'पुष्टि करें',
  // warning: losing formatting
  '%s में ऐसे गुण हो सकते हैं जो सहेजे जाने के लिए चुने हुए स्वरूप से मेल ना खाएं।'#13+
  'क्या आप दस्तावेज़ को इसी स्वरूप में सहेजना चाहते हैं?',
  // RVF format name
  'रिचव्यू स्वरूप',
  // Error messages ------------------------------------------------------------
  'त्रुटि',
  'फाईल लोड करने में त्रुटि हुई है।'#13#13'संभावित कारण:'+#13+'- यह ऐप्लिकेशन इस फाइल स्वरूप को समर्थित नहीं करती;'+#13+
  '- फाइल बिगड़ गई है;'+#13+'- किसी अन्य ऐप्लिकेशन द्वारा फाइल खोली तथा लौक कर दी गई है।',
  'चित्र फाइल लोड करने में त्रुटि हुई है।'+#13#13+'संभावित कारण:'+#13+'- फाइल में ऐसा चित्र स्वरूप है जो इस ऐप्लिकेशन द्वारा समर्थित नहीं है;'+#13+
  '- फाइल में कोई चित्र नहीं है;'+#13+
  '- फाइल बिगड़ गई है;'+#13+'- किसी अन्य ऐप्लिकेशन द्वारा फाइल खोली जाकर लौक कर दी गई है।',
  'फाइल सहेजने में त्रुटि हुई है।'+#13#13+'संभावित कारण:'+#13+'- डिस्क में स्थान कम है;'+#13+
  '- डिस्क पर लिखे जाने की अनुमति नहीं है;'+#13+'- हटाए जा सकने वाला मीडीया कंप्यूटर में लगाया नहीं गया;'+#13+
  '- किसी अन्य ऐप्लिकेशन द्वारा फाइल खोली तथा लौक कर दी गई है;'+#13+'- डिस्क मीडीया बिगड़ गया है।',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'रिच्व्यू फाइल्स (*.rvf)|*.rvf',  'RTF फाइल्स (*.rtf)|*.rtf' , 'XML फाइल्स (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'टेक्स्ट फाइल्स (*.txt)|*.txt', 'टेक्स्ट फाइल्स - यूनीकोड (*.txt)|*.txt', 'टेक्स्ट फाइल्स - ऑटोडिटेक्ट (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML फाइल्स (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - सरल (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word दस्तावेज़ (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'खोज पूरी हुई', 'खोज वाक्यांश ''%s'' नहीं मिला।',
  // 1 string replaced; Several strings replaced
  '1 वाक्यांश बदला।', '%d वाक्यांश बदले।',
  // continue search from the beginning/end?
  'दस्तावेज़ का अन्त आ गया है, आरंभ से खोजें?',
  'दस्तावेज़ का आरंभ आ गया है, अन्त से खोजें?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'पारदर्शी', 'स्वचलित',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'काला', 'भूरा', 'ओलिव हरा', 'गहरा हरा', 'गहरा टील', 'गहरा नीला', 'इंडिगो', 'ग्रे-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'गहरा लाल', 'नारंगी', 'गहरा पीला', 'हरा', 'टील', 'नीला', 'नीला-ग्रे', 'ग्रे-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'लाल', 'हल्का नारंगी', 'लाईम', 'सी ग्रीन', 'एक्वा', 'हल्का नीला', 'वायलेट', 'ग्रे-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'गुलाबी', 'सुनहरी', 'पीला', 'चटक हरा', 'टर्क्युऑज़', 'आसमानी', 'प्लम', 'ग्रे-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'रोज़', 'टैन', 'हल्का पीला', 'हल्का हरा', 'हल्का टर्क्युऑज़', 'हल्का नीला', 'लैवेंडर', 'सफेद',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'पारदर्शी', 'स्वचलित', 'और रंग...', 'डिफौल्ट',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'पृष्ठभूमि', 'रंग:', 'स्थिति', 'पृष्ठभूमि', 'लेख का नमूना।',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  'कोई नहीं', 'फैलावदार', 'स्थायी टाइलनुमा', 'टाइल', 'मध्य केंद्रित',
  // Padding button
  'पैडिंग...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'भरने का रंग', 'लागू करें:', 'और रंग...', 'पैडिंग...',
  'कृप्या कोई एक रंग चुनें',
  // [apply to:] text, paragraph, table, cell
  'लेख', 'अनुच्छेद' , 'तालिका', 'कक्ष',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'फौन्ट', 'फौन्ट', 'लेआउट',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'फौन्ट:', 'आकार', 'शैली', 'बोल्ड', 'तिरछे',
  // Script, Color, Back color labels, Default charset
  'स्क्रिप्ट:', 'रंग:', 'पृष्ठभूमि:', '(डिफौल्ट)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'प्रभाव', 'रेखांकित', 'ऊपर रेखा', 'काटा हुआ', 'सभी बड़े अक्षर',
  // Sample, Sample text
  'नमूना', 'नमूना लेख',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'वर्ण अंतर', 'अंतर:', 'विस्तृत', 'सघनिकृत',
  // Offset group-box, Offset label, Down, Up,
  'खड़ा अंतर्लम्ब', 'अंतर्लम्ब:', 'नीचे', 'ऊपर',
  // Scaling group-box, Scaling
  'अनुपातिकरण', 'अनुपातिकरण:',
  // Sub/super script group box, Normal, Sub, Super
  'सबस्क्रिप्ट्स तथा सुपरस्क्रिप्ट्स', 'सामान्य', 'सबस्क्रिप्ट', 'सुपरस्क्रिप्ट',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'पैडिंग', 'ऊपर:', 'बाएं:', 'नीचे:', 'दाहिने:', 'समान मान',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'हाइपरलिंक जोड़ें', 'हाइपरलिंक', 'लेख:', 'लक्षय', '<<चुनाव>>',
  // cannot open URL
  '"%s" तक नहीं पहुँच पा रहे',
  // hyperlink properties button, hyperlink style
  'अनुकूलित करें...', 'शैली:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'हाइपरलिंक गुण', 'सामान्य रंग', 'सक्रीय रंग',
  // Text [color], Background [color]
  'लेख:', 'पृष्ठभूमि',
  // Underline [color]
  'रेखांकित:',
  // Text [color], Background [color] (different hotkeys)
  'लेख:', 'पृष्ठभूमि',
  // Underline [color], Attributes group-box,
  'रेखांकित:', 'गुण',
  // Underline type: always, never, active (below the mouse)
  'रेखांकित:', 'सदैव', 'कभी नहीं', 'सक्रीय',
  // Same as normal check-box
  'सामन्य जैसे',
  // underline active links check-box
  'सक्रीय लिंक रेखांकित करें',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'चिन्ह जोड़ें', 'फौन्ट:', 'वर्ण निर्धारित:', 'यूनिकोड',
  // Unicode block
  'ब्लॉक:',  
  // Character Code, Character Unicode Code, No Character
  'वर्ण कोड: %d', 'वर्ण कोड: यूनिकोड %d', '(कोई वर्ण नहीं)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'तालिका जोड़ें', 'तालिका आकार', 'कॉलम संख्या:', 'पंक्ति संख्या:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'तालिका का लेआउट', 'स्वचलित आकार', 'विंडो में फिट करें', 'कस्टम आकार',
  // Remember check-box
  'नई तालिका के लिए आयाम स्मरण रखें',
  // VAlign Form ---------------------------------------------------------------
  'स्थिति',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'गुण', 'चित्र', 'स्थिति एवं आकार', 'रेखा', 'तालिका', 'पंक्ति', 'कक्ष',
  // Image Appearance, Image Misc, Number tabs
  'रुप', 'विविध', 'संख्या',
  // Box position, Box size, Box appearance tabs
  'स्थिति', 'आकार', 'रुप',
  // Preview label, Transparency group-box, checkbox, label
  'पूर्वावलोकन:', 'पारदर्शिता', 'पारदर्शी', 'पारदर्शी रंग:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'बदलें...', 'सहेजें...', 'स्वचलित',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'खड़ा संरेखण', 'संरेखण:',
  'नीचे से लेख की निचली सीमारेखा तक', 'मध्य से लेख की निचली सीमारेखा तक',
  'ऊपर से पंक्ति की ऊपरी सीमा तक', 'नीचे से पंक्ति की निचली सीमा तक', 'मध्य से पंक्ति के मध्य तक',
  // align to left side, align to right side
  'बाईं ओर', 'दाहिनी ओर',
  // Shift By label, Stretch group box, Width, Height, Default size
  'खिस्काएं:', 'विस्तृत करें', 'चौड़ाई:', 'ऊँचाई:', 'डिफौल्ट आकार: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'अनुपातानुसार आकार दें', 'पुनः भरें',
  // Inside the border, border, outside the border groupboxes
  'किनारे के अन्दर', 'किनारा', 'किनारे के बाहर',
  // Fill color, Padding (i.e. margins inside border) labels
  'भरने का रंग:', 'पैडिंग:',
  // Border width, Border color labels
  'किनारे की चौड़ाई:', 'किनारे का रंग:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'क्षैतिज अन्तराल:', 'खड़ा अन्तराल:',
  // Miscellaneous groupbox, Tooltip label
  'विविध', 'टूलटिप:',
  // web group-box, alt text
  'वेब', 'वैकल्पिक लेख:',
  // Horz line group-box, color, width, style
  'क्षैतिज रेखा', 'रंग:', 'चौड़ाई:', 'शैली:',
  // Table group-box; Table Width, Color, CellSpacing,
  'तालिका', 'चौड़ाई:', 'भरने का रंग:', 'कक्ष अन्तराल...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'कक्ष की क्षैतिजी पैडिंग:', 'कक्ष की खड़ी पैडिंग:', 'किनारे और पृष्ठभूमि',
  // Visible table border sides button, auto width (in combobox), auto width label
  'प्रत्यक्ष किनारे...', 'स्वचलित', 'स्वचलितः',
  // Keep row content together checkbox
  'सामग्री इकठी रखें',
  // Rotation groupbox, rotations: 0, 90, 180, 270
   'घुमाव', 'कोई नहीं', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'किनारे:', 'प्रकट किनारे...',
  // Border, CellBorders buttons
  'तालिका किनारे...', 'कक्ष किनारे...',
  // Table border, Cell borders dialog titles
  'तालिका किनारे', 'डिफौल्ट कक्ष किनारे',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'मुद्रण', 'पृष्ठ बदलने पर कॉलम्स को विच्छेदित ना होने दें', 'शीर्षक पंक्तियों की संख्या:', 'तालिका के प्रत्येक पृष्ठ पर शीर्षक पंक्तियां दोहराई जाएंगी',
  // top, center, bottom, default
  'ऊपर', 'मध्य', 'नीचे', 'डिफौल्ट',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'सेटिंग्स', 'निर्धारित चौड़ाई:', 'न्यूनतम ऊँचाई:', 'भरने का रंग:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'किनारे', 'प्रकट किनारे:',  'परछाईं रंग:', 'हल्का रंग', 'रंग:',
  // Background image
  'चित्र...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'क्षैतिज स्थिति', 'खड़ी स्थिति', 'लेख में स्थिति',
  // Position types: align, absolute, relative
  'संरेखित करें', 'पक्की स्थिति', 'सापेक्ष स्थिति',
  //[Align] left side, center, right side
  'बॉक्स के बाएं से, के बांई ओर तक',
  'बॉक्स के मध्य से, के मध्य तक',
  'बॉक्स की दाहिने से, के दाहिनी ओर तक',
  // [Align] top side, center, bottom side
  'बॉक्स के उपरी छोर से, के ऊपर तक',
  'बॉक्स के मध्य से, के मध्य तक',
  'बॉक्स के निकाले छोर से नीचे तक',
  // [Align] relative to
  'के सापेक्ष',
  // [Position] to the right of the left side of
  'के बाँए छोर के दाहिने',
  // [Position] below the top side of
  'के उपरी छोर के निचली ओर',
  // Anchors: page, main text area (x2)
  'पृष्ठ', 'मुख्य लेख क्षेत्र', 'पृष्ठ', 'मुख्य लेख क्षेत्र',
  // Anchors: left margin, right margin
  'बांया किनारा', 'दाहिना किनारा',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'ऊपरी किनारा', 'निचला किनारा', 'भीतरी सीमा', 'बाहरी सीमा',
  // Anchors: character, line, paragraph
  'वर्ण', 'पंक्ति', 'अनुच्छेद',
  // Mirrored margins checkbox, layout in table cell checkbox
  'प्रतिबिंबित किनारे', 'तालिका कक्ष में ख़ाका',
  // Above text, below text
  'लेख के ऊपर', 'लेख के नीचे',
  // Width, Height groupboxes, Width, Height labels
  'चौड़ाई', 'ऊँचाई', 'चौड़ाई:', 'ऊँचाई:',
  // Auto height label, Auto height combobox item
  'स्वचलित', 'स्वचलित',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'खड़ा संरेखण', 'ऊपर', 'मध्य', 'नीचे',
  // Border and background button and title
  'किनारे तथा पृष्ठभूमि...', 'बॉक्स के किनारे तथा पृष्ठभूमि',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files, URL
  'विशेष चिपकाएं', 'इस रुप में चिपकाएं:', 'रिच टेक्स्ट स्वरूप', 'HTML स्वरूप',
  'टेक्स्ट', 'यूनिकोड टेक्स्ट', 'बिटमैप चित्र', 'मेटाफाइल चित्र',
  'ग्राफिक फाइल्स', 'URL',
  // Options group-box, styles
  'विकल्प', 'शैलियाँ:',
  // style options: apply target styles, use source styles, ignore styles
  'लक्ष्य दस्तावेज़ की शैलियाँ लागू करें', 'शैलियाँ तथा प्रकट रूप बनाए रखें',
  'प्रकट रूप बनाए रखें, शैली की अवहेलना करें',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'बुलेट और संख्याएं', 'बुलेटेड', 'संख्यावार', 'बुलेटेड सूची', 'संख्यावार सूची',
  // Customize, Reset, None
  'अनुकूलित करें...', 'पुनः निर्धारित करें', 'कोई नहीं',
  // Numbering: continue, reset to, create new
  'संख्या ज़ारी रखें', 'पर संख्या पुनः निर्धारित करें', 'यहां से आरंभ करके, नई सूची बनाएं',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'ऊपरी अल्फा', 'ऊपरी रोमन', 'दशमलव', 'नीचले अल्फा', 'निचले रोमन',
  // Bullet type
  'गोल', 'डिस्क', 'चौकोर',
  // Level, Start from, Continue numbering, Numbering group-box
  'स्तर:', 'आरंभ करें:', 'ज़ारी रखें', 'संख्यां',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'अनुकूलित सुची', 'स्तर ', 'गिनती:', 'सुची के गुण', 'सूची प्रकार:',
  // List types: bullet, image
  'बुलेट', 'चित्र', 'संख्या जोड़ें|',
  // Number format, Number, Start level from, Font button
  'संख्या स्वरूप:', 'संख्या', 'स्तर की संख्या यहां से आरंभ करें:', 'फौन्ट...',
  // Image button, bullet character, Bullet button,
  'चित्र...', 'बुलेट वर्ण', 'बुलेट...',
  // Position of list text, bullet, number, image, text
  'सूची लेख की स्थिति', 'बुलेट की स्थिति', 'संख्या की स्थिति', 'चित्र की स्थिति', 'लेख की स्थिति',
  // at, left indent, first line indent, from left indent
  'यहां से:', 'बायां इंडेंट:', 'प्रथम पंक्ति इंडेंट:', 'बाएं इंडेंट से',
  // [only] one level preview, preview
  'एक स्तर का पूर्वावलोकन', 'पूर्वावलोकन',
  // Preview text
  'लेख लेख लेख पाठ। लेख लेख लेख पाठ।'+' लेख लेख लेख पाठ। लेख लेख लेख पाठ। लेख'+' लेख लेख पाठ। लेख लेख लेख पाठ।',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'बाएं संरेखण करें', 'दाहिने संरेखण करें', 'मध्य',
  // level #, this level (for combo-box)
  'स्तर %d', 'यह स्तर',
  // Marker character dialog title
  'बुलेट वर्ण सम्पादित करें',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'अनुच्छेद किनारे और पृष्ठभूमि', 'किनारा', 'पृष्ठभूमि',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'सेटिंग्स', 'रंग:', 'चौड़ाई:', 'भीतरी चौड़ाई:', 'लेख किनारे...',
  // Sample, Border type group-boxes;
  'नमूना', 'किनारे का प्रकार',
  // Border types: none, single, double, tripple, thick inside, thick outside
  'कोई नहीं', 'एकल', 'दोगुना', 'तिगुना', 'भीतरी मोटा', 'बाहरी मोटा',
  // Fill color group-box; More colors, padding buttons
  'भरने का रंग', 'और रंग...', 'पैडिंग...',
  // preview text
  'लेख लेख लेख पाठ। लेख लेख लेख पाठ।'+' लेख लेख लेख पाठ। लेख लेख लेख पाठ।'+' लेख लेख लेख पाठ। लेख लेख लेख पाठ।',
  // title and group-box in Offsets dialog
  'लेख किनारे', 'किनारों के ऑफसेट',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'अनुच्छेद', 'संरेखण', 'बांएं', 'दाहिने', 'मध्य', 'समायोजित',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'अंतराल', 'पहले:', 'बाद में:', 'पंक्ति अंतराल:', 'से:',
  // Indents group-box; indents: left, right, first line
  'इंडेंटेशन', 'बाएं:', 'दाहिने:', 'प्रथम पंक्ति:',
  // indented, hanging
  'इंडेंटिड', 'हैंगिंग', 'नमूना',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'एकल', '1.5 पंक्ति', 'दोगुना', 'कम से कम', 'सटीक', 'एकाधिक',
  // preview text
  'लेख लेख लेख पाठ। लेख लेख लेख पाठ।'+' लेख लेख लेख पाठ। लेख लेख लेख पाठ। लेख'+' लेख लेख पाठ। लेख लेख लेख पाठ। लेख'+' लेख लेख पाठ। लेख लेख लेख पाठ। लेख'+' लेख लेख पाठ। लेख लेख लेख पाठ।'+' लेख लेख लेख पाठ। लेख लेख लेख पाठ।',
  // tabs: Indents and spacing, Tabs, Text flow
  'इंडेंट और अंतराल', 'टैब्स', 'लेख प्रवाह',
  // tab stop position label; buttons: set, delete, delete all
  'टैब रुकने का स्थान:', 'निर्धारण', 'मिटाएं', 'सभी मिटाएं',
  // tab align group; left, right, center aligns,
  'संरेखण', 'बाएं', 'दाहिने', 'मध्य',
  // leader radio-group; no leader
  'अग्रिम', '(कोई नहीं)',
  // tab stops to be deleted, delete none, delete all labels
  'टैब स्टॉप मिटाएं:', '(कोई नहीं)', 'सभी।',
  // Pagination group-box, keep with next, keep lines together
  'पृष्ठनिर्धारण', 'अगले के साथ रखें', 'पंक्तियों को साथ रखें',
  // Outline level, Body text, Level #
  'ख़ाका स्तर:', 'मुख्य लेख', 'स्तर %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'मुद्रर्ण पूर्वावलोकन', 'पृष्ठ चौड़ाई', 'पूरा पृष्ठ', 'पृष्ठ संख्या:',
  // Invalid Scale, [page #] of #
  '10 से 500 के बीच की कोई संख्या भरें', '%d का',
  // Hints on buttons: first, prior, next, last pages
  'प्रथम पृष्ठ', 'पूर्व पृष्ठ', 'अगला पृष्ठ', 'अंतिम पृष्ठ',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'कक्ष अंतराल', 'अंतराल', 'कक्षों के बीच', 'तालिका के किनारों और कक्षों के बीच',
  // vertical, horizontal (x2)
  'खड़ा:', 'क्षैतिज:', 'खड़ा:', 'क्षैतिज:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'किनारे', 'निर्धारण', 'रंग:', 'हल्का रंग:', 'परछाईं का रंग:',
  // Width; Border type group-box;
  'चौड़ाई:', 'किनारे का प्रकार',
  // Border types: none, sunken, raised, flat
  'कोई नहीं', 'धंसे हुए', 'उभरे हुए', 'चपटे',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'विभाजित', 'विभाजित करें',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'पंक्तियों और कॉलम की निर्दिष्ट संख्या', 'मूल कक्ष',
  // number of columns, rows, merge before
  'कॉलम की संख्या:', 'पंक्तियों की संख्या:', 'विभाजन से पहले विलय करें',
  // to original cols, rows check-boxes
  'मूल कॉलमों में विभाजित करें', 'मूल पंक्तियों में विभाजित करें',
  // Add Rows form -------------------------------------------------------------
  'पंक्ति जोड़ें', 'पंक्तियों की संख्या:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'पृष्ठ व्यवस्था', 'पृष्ठ', 'शीर्षलेख एवं पादलेख',
  // margins group-box, left, right, top, bottom
  'हाशिए (मिलिमीटर्स)', 'हाशिए (इंच)', 'बांएं:', 'ऊपरी:', 'दाहिने:', 'निचले:',
  // mirror margins check-box
  'प्रतिबिंबित हाशिए',
  // orientation group-box, portrait, landscape
  'रुख़', 'पोर्टरेट', 'लैंडस्केप',
  // paper group-box, paper size, default paper source
  'पृष्ठ', 'आकार:', 'स्त्रोत:',
  // header group-box, print on the first page, font button
  'शीर्षलेख', 'लेख:', 'प्रथम पृष्ठ पर शीर्षलेख', 'फौन्ट...',
  // header alignments: left, center, right
  'बाएं', 'मध्य', 'दाहिने',
  // the same for footer (different hotkeys)
  'पादलेख', 'लेख:', 'प्रथम पृष्ठ पर पादलेख', 'फौन्ट...',
  'बाएं', 'मध्य', 'दाहिने',
  // page numbers group-box, start from
  'पृष्ठ संख्या', 'से आरंभ करें:',
  // hint about codes
  'विशेष वर्ण संयोजन:'#13'&&p - पृष्ठ संख्या; &&P - पृष्ठों की गिनती; &&d - वर्तमान तिथि; &&t - वर्तमान समय.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'लेख फाइल का कोड पृष्ठ', 'फाइल की एन्कोडिंग चुनें:',
  // thai, japanese, chinese (simpl), korean
  'थाई', 'जापानी', 'चीनी (सरल)', 'कोरियन',
  // chinese (trad), central european, cyrillic, west european
  'चीनी (पारंपरिक)', 'मध्य एवं पूर्व योरपियन', 'साइरिलिक', 'पश्चिमी योरपियन',
  // greek, turkish, hebrew, arabic
  'ग्रीक', 'तुर्की', 'हीब्रू', 'अरबी',
  // baltic, vietnamese, utf-8, utf-16
  'बॉलटिक', 'वियतनामी', 'यूनीकोड (UTF-8)', 'यूनिकोड (UTF-16)',
  // Style form ----------------------------------------------------------------
  'दृश्यावलोकन शैली', 'शैली चुनें:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'लेख में परिवर्तित करें', 'सीमांकक चुनें:',
  // line break, tab, ';', ','
  'पंक्ति ब्रेक', 'टैब', 'अर्धविराम', 'अल्पविराम',
  // Table sort form -----------------------------------------------------------
  // error message
  'संयोजित करी गई पंक्तियों वाली तालिका क्रमवार नहीं करी जा सकती',
  // title, main options groupbox
  'तालिका क्रमवार करें', 'क्रमवार',
  // sort by column, case sensitive
  'कॉलम से क्रमवार करें:', 'केस संवेदी',
  // heading row, range or rows
  'शीर्षक पंक्ति को छोड़ दें', 'क्रमवार करने के लिए पंक्तियाँ: %d से %d तक',
  // order, ascending, descending
  'क्रम', 'बढ़ते हुए', 'घटते हुए',
  // data type, text, number
  'प्रकार', 'लेख', 'संख्या',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'संख्या भरें', 'गुण', 'गणक का नाम:', 'संख्या प्रकार',
  // numbering groupbox, continue, start from
  'संख्या', 'ज़ारी रखें', 'से आरंभ करें:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'शीर्षक', 'लेबल:', 'लेबल को शीर्षक से पृथक रखें',
  // position radiogroup
  'स्थिति', 'चुने हुए के ऊपर', 'चुने हुए के नीचे',
  // caption text
  'शीर्षक एवं लेख:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'संख्या', 'चित्र', 'तालिका',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'प्रकट किनारे', 'किनारे',
  // Left, Top, Right, Bottom checkboxes
  'बाएं ओर', 'ऊपरी ओर', 'दाहिनी ओर', 'निचली ओर',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'तालिका में कॉलम का आकार बदलें', 'तालिका में पंक्ति का आकार बदलें',
  // Hints on indents: first line, left (together with first), left (without first), right
  'प्रथम पंक्ति इंडेंट', 'बायां इंडेंट', 'हैंगिंग इंडेंट', 'दाहिना इंडेंट',
  // Hints on lists: up one level (left), down one level (right)
  'एक स्तर बढ़ाएं', 'एक स्तर घटाएं',
  // Hints for margins: bottom, left, right and top
  'निचला हाशिया', 'बायां हाशिया', 'दाहिना हाशिया', 'ऊपरी हाशिया',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'बाएं संरेखित टैब', 'दाहिने संरेखित टैब', 'मध्य संरेखित टैब', 'दश्मलव संरेखित टैब',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'सामान्य', 'सामान्य इंडेंट', 'कोई अंतराल नहीं', 'शीर्षक %d', 'अनुच्छेद सूची',
  // Hyperlink, Title, Subtitle
  'हाइपरलिंक', 'शीर्षक', 'उपशीर्षक',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'प्रमुख', 'सूक्ष्म रीति से प्रमुख', 'तीव्र रीति से प्रमुख', 'बलवन्त',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'उद्धर्ण', 'तीव्र उद्धर्ण', 'सूक्ष्म हवाला', 'तीव्र हवाला',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'लेख खण्ड', 'HTML वैरिएबल', 'HTML कोड', 'HTML एक्रोनिम',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML परिभाषा', 'HTML कुंजीपटल', 'HTML नमूना', 'HTML टाइपराइटर', 
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML पूर्वस्वरूपित', 'HTML उद्धरित', 'शीर्ष लेख', 'पाद लेख', 'पृष्ठ संख्या',
  // Caption
  'शीर्षक',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'अन्त टिप्पणी हवाला', 'पाद टिप्पणी हवाला', 'अन्त टिप्पणी लेख', 'पाद टिप्पणी लेख', 
  // Sidenote Reference, Sidenote Text
  'साइडनोट हवाला', 'साइडनोट लेख',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'रंग', 'पृष्ठभूमि रंग', 'पारदर्शक', 'डिफॉल्ट', 'रेखांकन रंग',
  // default background color, default text color, [color] same as text
  'डिफॉल्ट पृष्ठभूमि रंग', 'डिफॉल्ट लेख रंग', 'लेख के समान',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'एकल', 'मोटा', 'दोगुना', 'बिन्दुवार', 'मोटे बिन्दुवार', 'डैश',
  // underline types: thick dashed, long dashed, thick long dashed,
  'मोटे डैश', 'लंबे डैश', 'मोटे एवं लंबे डैश',
  // underline types: dash dotted, thick dash dotted,
  'डैश बिन्दुवार', 'मोटे डैश बिन्दुवार',
  // underline types: dash dot dotted, thick dash dot dotted
  'डैश बिन्दु बिन्दुवार', 'मोटे डैश बिन्दु बिन्दुवार',
  // sub/superscript: not, subsript, superscript
  'सब/सुपरस्क्रिप्ट नहीं', 'सबस्क्रिप्ट', 'सुपरस्क्रिप्ट', 
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'दाएं-बाएं प्रकार:', 'विरासती', 'बाएं से दाएं', 'दाएं से बाएं',
  // bold, not bold
  'बोल्ड', 'बोल्ड नहीं',
  // italic, not italic
  'तिरछे', 'तिरछे नहीं',
  // underlined, not underlined, default underline
  'रेखांकित', 'रेखांकित नहीं', 'डिफॉल्ट रेखांकित',
  // struck out, not struck out
  'काटा हुआ', 'काटा हुआ नहीं',
  // overlined, not overlined
  'ऊपर से रेखांकित', 'ऊपर से रेखांकित नहीं',
  // all capitals: yes, all capitals: no
  'सभी अक्षर बड़े', 'कोई अक्षर बड़ा नहीं',
  // vertical shift: none, by x% up, by x% down
  'बिना लंबाकार खिसकाए', 'ऊपर की ओर %d%% खिसकाएं', 'नीचे की ओर %d%% खिसकाएं', 
  // characters width [: x%]
  'वर्ण की चौड़ाई',
  // character spacing: none, expanded by x, condensed by x
  'सामान्य वर्ण अंतर', 'अंतर %s से बढ़ाएं', 'अंतर %s से घटाएं', 
  // charset
  'लिपि',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'डिफॉल्ट फौन्ट', 'डिफॉल्ट अनुच्छेद', 'विरासती',
  // [hyperlink] highlight, default hyperlink attributes
  'प्रमुख', 'डिफॉल्ट',
  // paragraph aligmnment: left, right, center, justify
  'बाएं संरेखित', 'दाएं संरेखित', 'मध्य संरेखित', 'समायोजित', 
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'पंक्ति की ऊँचाई: %d%%', 'पंक्तियों के बीच का अंतर: %s', 'पंक्ति की ऊँचाई: कम से कम %s', 'पंक्ति की ऊँचाई: ठीक %s',
  // no wrap, wrap
  'नई पंक्ति आरंभ अक्षम', 'नई पंक्ति आरंभ सक्षम',
  // keep lines together: yes, no
  'पंक्तियां साथ रखें', 'पंक्तियां साथ नहीं रखें', 
  // keep with next: yes, no
  'अगले अनुच्छेद के साथ रखें', 'अगले अनुच्छेद के साथ नहीं रखें', 
  // read only: yes, no
  'केवल पढ़ने के लिए', 'संपादन संभव', 
  // body text, heading level x
  'ख़ाका स्तर: मुख्य लेख', 'ख़ाका स्तर: %d',
  // indents: first line, left, right
  'प्रथम पंक्ति इंडेंट', 'बाएं इंडेंट', 'दाएं इंडेंट',
  // space before, after
  'पहले अंतराल', 'बाद में अंतराल',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'टैब्स', 'कोई नहीं', 'संरेखण', 'बाएं', 'दाएं', 'मध्य',
  // tab leader (filling character)
  'अग्रिम',
  // no, yes
  'नहीं', 'हां', 
  // [padding/spacing/side:] left, top, right, bottom
  'बाएं', 'ऊपर', 'दाएं', 'नीचे',
  // border: none, single, double, triple, thick inside, thick outside
  'कोई नहीं', 'एकल', 'दोगुना', 'तिगुना', 'अन्दर से मोटा', 'बाहर से मोटा', 
  // background, border, padding, spacing [between text and border]
  'पृष्ठभूमि', 'किनारे', 'पैडिंग', 'अंतराल', 
  // border: style, width, internal width, visible sides
  'शैली', 'चौड़ाई', 'भीतरी चौड़ाई', 'प्रकट किनारे', 
  // style inspector -----------------------------------------------------------
  // title
  'शैली निरीक्षक',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'शैली', '(कोई नहीं)', 'अनुच्छेद', 'फौंट', 'गुण',
  // border and background, hyperlink, standard [style]
  'किनारे एवं पृष्ठभूमि', 'हाइपरलिंक', '(मानक)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'शैलियाँ', 'शैली',
  // name, applicable to,
  'नाम', 'पर लागू होता है:',
  // based on, based on (no &),
  'आधारित है:', 'आधारित है:',
  //  next style, next style (no &)
  'अगले अनुच्छेद की शैली', 'अगले अनुच्छेद की शैली',
  // quick access check-box, description and preview
  'त्वरित पहुँच', 'विवरण एवं पूर्वावलोकन',
  // [applicable to:] paragraph and text, paragraph, text
  'अनुच्छेद एवं लेख', 'अनुच्छेद', 'लेख',
  // text and paragraph styles, default font
  'लेख एवं अनुच्छेद शैलियाँ', 'डिफॉल्ट फौंट',
  // links: edit, reset, buttons: add, delete
  'संपादन', 'पुनः भरें', 'जोड़ें', 'मिटाएं',
  // add standard style, add custom style, default style name
  'मानक शैली जोड़ें...', 'एच्छित शैली जोड़ें', 'शैली  %d',
  // choose style
  'शैली चुनें:',
  // import, export,
  'आयात...', 'निर्यात...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'रिचव्यू शैलियाँ (*.rvst)|*.rvst', 'फाइल लोड करने में त्रुटि हुई', 'इस फाइल में कोई शैली नहीं है', 
  // Title, group-box, import styles
  'फाइल से शैली आयात करें', 'आयात', 'शैलियाँ आयात करें:',
  // existing styles radio-group: Title, override, auto-rename
  'वर्तमान शैलियाँ', 'ओवर्राइड', 'पुनःनामित को जोड़ें',
  // Select, Unselect, Invert,
  'चुनें', 'चुनाव रद्द करें', 'विपरीत करें',
  // select/unselect: all styles, new styles, existing styles
  'सभी शैलियाँ', 'नई शैलियाँ', 'वर्तमान शैलियाँ',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'स्वरूप मिटाएं', 'सभी शैलियाँ',
  // dialog title, prompt
  'शैलियाँ', 'लागू करने के लिए शैली चुनें',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  'पर्यायवाची', 'सभी की अवहेलना करें', 'शब्दकोष में जोड़ें',
  // Progress messages ---------------------------------------------------------
  '%s डाउनलोड हो रहा है', 'मुद्रण के लिए तैयार किया जा रहा है...', 'पृष्ठ %d मुद्रित किया जा रहा है',  
  'रिच टेक्सट स्वरुप से परिवर्तित किया जा रहा है...',  'रिच टेक्सट स्वरुप में परिवर्तित किया जा रहा है...',
  '%s पढ़ा जा रहा है...', '%s लिखा जा रहा है...', 'लेख',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'कोई नोट नहीं', 'पादनोट', 'अंतनोट', 'साइडनोट', 'टेक्सट बॉक्स'
  );


initialization
  RVA_RegisterLanguage(
    'Hindi', // english language name, do not translate
    'हिन्दी', // native language name
    DEFAULT_CHARSET, @Messages);

end.
