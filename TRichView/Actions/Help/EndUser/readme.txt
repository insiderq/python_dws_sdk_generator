How to make a help file for your language

1) There must be a translation of RichViewActions to this
  language (RVAL_***.pas file, see RVALocalize.pas)

2) Create a help file with the same structure as demo rvat.hlp.
  27 topics, topics' IDs and captions are not important, but
  topics' help context numbers must be the same as in rvat.hlp
  Included demo files:
  rvat.hlp - demo help file;
  rvat.cnt - contents for this help file (not required,
    but allows to see a list of topics);
  ~rvat.hpj, ~rvat.rtf, ~rvat.hm - files required to
    build this help file (~rvat.hm contains context numbers
    and it is included in ~rvat.hpj)
  rvat.hm3 - for users of Help and Manual 3
    (http://www.helpandmanual.com)

3) In RVAL_***.pas file for this language, add a file name
  for the help file as a last parameter of call of 
  RVA_RegisterLanguage, for example:
  RVA_RegisterLanguage('English (US)', ANSI_CHARSET, 
    @Messages, 'rvat.hlp');
  (this parameter is optional)

4) Place the help file (*.HLP) to the same directory as the
  resulting EXE-file.
  
I recommend to give to the help file the same file name as
the corresponding PAS file (RVAL_EngUs.pas -> RVAL_EngUS.hlp)