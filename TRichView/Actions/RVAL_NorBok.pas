
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Norwegian (Bokm�l) translation                  }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Roy Magne Klever                       }
{                                                       }
{ Update: 2003-03-29: Did some corrections.             }
{ Update: 2003-04-23: Page setup translated             }
{ Update: 2003-08-23: Hyperlink  translated             }
{ Update: 2004-05-30                                    }
{ Update: 2004-08-14                                    }
{ Update: 2006-12-16                                    }
{ Update: 2011-10-16 by Alconost                        }
{ Update: 2012-09-08 by Alconost                        }
{ Update: 2014-04-28 by Alconost                        }
{*******************************************************}

// Advanced translated to Norwegian = Avansert

unit RVAL_NorBok;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'tommer', 'cm', 'mm', 'pica', 'piksler', 'punkter',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'piksler'{?}, 'punkter',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fil', '&Rediger', 'F&ormat', '&Skrift', '&Paragraf', 'S&ett inn', '&Tabell', '&Vindu', '&Hjelp',
  // exit
  '&Avslutt',
  // top-level menus: View, Tools,
  '&Vis', 'V&erkt�y',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'St�rrelse', 'Stil', 'V&elg', '&Cellejustering', 'C&elle-kantlinje',
  // menus: Table cell rotation
  'Cellerotasjon',
  // menus: Text flow, Footnotes/endnotes
  '&Tekstflyt', '&Fotnoter',
  // ribbon tabs: tab1, tab2, view, table
  '&Hjem', '&Avansert', '&Se', '&Tabell',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Utklippstavle', 'Font', 'Paragraf', 'Liste', 'Redigering',
  // ribbon groups: insert, background, page setup,
  'Sett inn', 'Bakgrunn', 'Sideoppsett',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Lenker', 'Fotnoter', 'Dokumentvisning', 'Se/Gjem', 'Zoom', 'Valg',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Sett inn', 'Slett', 'Valg', 'Kanter',
  // ribbon groups: styles 
  'Stiler',
  // ribbon screen tip footer,
  'Trykk F1 for mer hjelp',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Nylig �pnet dokument', 'Lagre en kopi av dette dokumentet', 'Forh�ndsvis og print dokumentet',
  // ribbon label: units combo
  'Enheter:',        
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Ny', 'Ny|Nytt tomt dokument',
  // TrvActionOpen
  '�&pne...', '�pne|�pner et tidligere lagret dokument',
  // TrvActionSave
  'La&gre', 'Lagre|Lagrer dokumetet til harddisken',
  // TrvActionSaveAs
  'Lagre &som...', 'Lagre som...|Lagrer dokumentet med nytt navn',
  // TrvActionExport
  '&Eksporter...', 'Eksporter|Eksporter dokumentet til et annet format',
  // TrvActionPrintPreview
  'Forh�nds&visning', 'Forh�ndsvisning|Viser dokumentet slik det vil bli ved en utskrift',
  // TrvActionPrint
  'Skriv &ut...', 'Skriv ut|Endrer utskriftsinnstillinger og skriver ut dokumentet',
  // TrvActionQuickPrint
  'Skriv &ut', '&Hurtigutskrift', 'Skriv ut|Skriver ut dokumentet',
   // TrvActionPageSetup
  'Si&de oppsett...', 'Page Setup|Sets margins, paper size, orientation, source, headers and footers',  
  // TrvActionCut
  'Klipp &ut', 'Klipp ut|Klipper ut merket omr�det og plasserer det p� utklippstavlen',
  // TrvActionCopy
  '&Kopier', 'Kopier|Kopierer merket omr�det og plasserer det p� utklippstavlen',
  // TrvActionPaste
  '&Lim inn', 'Lim inn|Setter inn innholdet fra utklippstavlen',
  // TrvActionPasteAsText
  'Lim inn som &Tekst', 'Lim inn som Tekst|Sett inn tekst fra utklippstavlen',
  // TrvActionPasteSpecial
  'Lim inn ut&valg...', 'Lim inn spesifisert|Setter inn innholdet fra utklippstavlen i spesifisert format',
  // TrvActionSelectAll
  '&Merk alt', 'Merk alt|Merker hele dokumentet',
  // TrvActionUndo
  '&Angre', 'Angre|Omgj�r siste handling',
  // TrvActionRedo
  '&Gj�r om', 'Gj�r om|Gj�r om den siste omgjorte handlingen',
  // TrvActionFind
  '&S�k...', 'S�k|Finner speifisert tekst i dokumentet',
  // TrvActionFindNext
  'S�k &neste', 'S�k neste|Finn neste forekomst av forrige s�k',
  // TrvActionReplace
  '&Erstatt...', 'Erstatt|Finner og erstatter spesifisert text i dokumentet',
  // TrvActionInsertFile
  '&Fil...', 'Sett inn fil|Setter inn innholdet fra en fil i dokumentet',
  // TrvActionInsertPicture
  '&Bilde...', 'Sett inn bilde|Setter inn et bilde fra harddisken',
  // TRVActionInsertHLine
  '&Horisontal linje', 'Sett inn horisontal linje|Setter inn en horisontal linje',
  // TRVActionInsertHyperlink
  '&Hyperkobling...', 'Sett inn hyperkobling|Setter inn en hyperkobling',
  // TRVActionRemoveHyperlinks
  '&Fjern hyperlinker', 'Fjern hyperlinker|Fjern alle hyperlinker i den utvalgte teksten',  
  // TrvActionInsertSymbol
  'S&ymbol...', 'Sett inn symbol|Setter inn et symbol',
  // TrvActionInsertNumber
  '&Nummer...', 'Sett inn nummer|Setter inn et nummer',
  // TrvActionInsertCaption
  'Sett inn &bildetekst...', 'Sett inn bildetekst|Setter inn bildetekst for valgte objekt',
  // TrvActionInsertTextBox
  '&Tekstboks', 'Sett inn tekstboks|Setter inn en tekstboks',
  // TrvActionInsertSidenote
  '&Sidenote', 'Sett inn sidenote|Setter inn en note som vises i en tekstboks',
  // TrvActionInsertPageNumber
  '&Sidenummer', 'Sett inn sidenummer|Setter inn et sidenummer',
  // TrvActionParaList
  '&Punktmerking og nummerering...', 'Punktmerking og nummerering|Legger p� eller redigerer punktmerking eller nummerering for merkede paragrafer',
  // TrvActionParaBullets
  'P&unkt', 'Punkt|Legger til eller fjerner punktmerker fra paragrafen',
  // TrvActionParaNumbering
  '&Nummerering', 'Nummerering|Legger til eller fjerner nummerering fra paragrafen',
  // TrvActionColor
  '&Bakgrunnsfarge...', 'Bakgrunn|Endrer bakgrunnsfargen til dokumentet',
  // TrvActionFillColor
  '&Fyll farge...', 'Fyll farge|Endrer bakgrunnsfarge til tekst, paragraf, celle, tabell eller dokument',
  // TrvActionInsertPageBreak
  '&Sett inn sideskift', 'Sett inn sideskift|Setter inn et sideskift',
  // TrvActionRemovePageBreak
  '&Fjern sideskift', 'Fjern sideskift|Fjerner sideskiftet',
  // TrvActionClearLeft
  'Fjern tekstflyt p� &Venstre side', 'Fjern tekstflyt p� Venstre side|Plasserer denne paragrafen under et venstre-justert bilde',
  // TrvActionClearRight
  'Fjern tekstflyt p� &H�yre side', 'Fjern tekstflyt p� H�yre side|Plasserer denne paragrafen under et h�yre-justert bilde',
  // TrvActionClearBoth
  'Fjern tekstflyt p� &Begge sider', 'Fjern tekstflyt p� Begge siderC|Plasserer denne paragrafen under et venstre- eller h�yre-justert bilde',
  // TrvActionClearNone
  '&Normal tekstflyt', 'Normal tekstflyt|Tillater tekstflyt rundt b�de venstre- og h�yre-justerte bilder',
  // TrvActionVAlign
  '&Objektposisjon...', 'Objektposisjon|Endre posisjonen til det valgte objektet',    
  // TrvActionItemProperties
  'Ob&jektegenskaper...', 'Objektegenskaper|Definerer egenskaper for det aktive objektet',
  // TrvActionBackground
  '&Bakgrunn...', 'Bakgrund|Velger bakgrunnsfarge og bilde',
  // TrvActionParagraph
  '&Paragraf...', 'Paragraf|Endrer paragraf innstillingene',
  // TrvActionIndentInc
  '�&k innrykk', '�k innrykk|�ker venstre innrykk i merkede paragrafer',
  // TrvActionIndentDec
  '&Reduser innrykk', 'Reduserer innrykk|Reduser venstre innrykk i merkede paragrafer',
  // TrvActionWordWrap
  '&Tekstbryting', 'Tekstbryting|Skrur p� eller av tekstbryting',
  // TrvActionAlignLeft
  '&Venstrejuster', 'Venstrejuster|Venstre justerer merket tekst',
  // TrvActionAlignRight
  '&H�yrejuster', 'H�yrejuster|H�yre justerer merket tekst',
  // TrvActionAlignCenter
  '&Midtstill', 'Midtstill|Midtstiller merket tekst',
  // TrvActionAlignJustify
  '&Blokkjuster', 'Blokkjuster|Justerer merket tekst til b�de h�yre og venstre side',
  // TrvActionParaColor
  'Paragraf &bakgrunnsfarge...', 'Paragraf bakgrunnsfarge|Setter bakgrunnsfargen til paragrafen',
  // TrvActionLineSpacing100
  '&Enkel linjeavstand', 'Enkel linjeavstand|Setter enkel linjeavstand',
  // TrvActionLineSpacing150
  '1.5 &linjeavstand', '1.5 linjeavstand|Sette linjeavstanden til 1.5 linjer',
  // TrvActionLineSpacing200
  '&Dobbel linjeavstand', 'Dobbel linjeavstand|Setter dobbel linjeavstand',
  // TrvActionParaBorder
  'Paragraf &ramme og bakgrunn...', 'Paragraf ramme og bakgrunn|Setter ramme og bakgrunnsfarge for merket paragraf',
  // TrvActionInsertTable
  '&Sett inn tabell...', '&Tabell', 'Sett inn tabell|Setter inn en ny tabell',
  // TrvActionTableInsertRowsAbove
  'Sett inn rekke &over', 'Sett inn rekke over|Setter inn en ny rekke over valgte celler',
  // TrvActionTableInsertRowsBelow
  'Sett inn &rekke under', 'Sett inn rekke under|Setter inn en ny rekke under valgte celler',
  // TrvActionTableInsertColLeft
  'Sett inn kolonne til &venstre', 'Sett inn kolonne til venstre|Setter inn en ny kolonne til venstre for merkede celler',
  // TrvActionTableInsertColRight
  'Sett inn kolonne til &h�yre', 'Sett inn kolonne til h�yre|Setter inn en ny kolonne til h�yre for merkede celler',
  // TrvActionTableDeleteRows
  '&Slett rekker', 'Slett rekker|Sletter rekkene med merkede celler',
  // TrvActionTableDeleteCols
  'Slett k&olonner', 'Slett kolonner|Sletter kolonnene med merkede celler',
  // TrvActionTableDeleteTable
  'Slett ta&bell', 'Slett tabell|Sletter tabellen',
  // TrvActionTableMergeCells
  'Sl� i s&ammen celler', 'Sl� i sammen celler|Sl�r i sammen de merkede cellene',
  // TrvActionTableSplitCells
  '&Del &celler...', 'Del celler|Deler opp de merkede cellene',
  // TrvActionTableSelectTable
  '&Tabell', 'Velg tabell|Velger tabellen',
  // TrvActionTableSelectRows
  'Re&kke', 'Velg rekke|Velger rekken',
  // TrvActionTableSelectCols
  'Kolon&ne', 'Velg kolonne|Velger kolonnen',
  // TrvActionTableSelectCell
  '&Celle', 'Velg celle|velger cellen',
  // TrvActionTableCellVAlignTop
  'Juster celle topp', 'Juster celle toppen|Justerer celle innholdet til toppen',
  // TrvActionTableCellVAlignMiddle
  'Juster celle midten', 'Juster celle midten|Justerer celle innholdet til midten',
  // TrvActionTableCellVAlignBottom
  'Juster celle bunn', 'Juster celle bunn|Justerer celle innholdet til bunnen',
  // TrvActionTableCellVAlignDefault
  '&Standard celle vertikal justering', 'Standard celle vertikal justering|Setter standard vertikal justering for merkede celler',
  // TrvActionTableCellRotationNone
  '&Ingen celle rotasjon', 'Ingen celle rotasjon|Roterer cellens innhold med 0�',
  // TrvActionTableCellRotation90
  'Roter cellen med &90�', 'Roter cellen med 90�|Roterer cellens innhold med 90�',
  // TrvActionTableCellRotation180
  'Roterer cellen med &180�', 'Roterer cellen med 180�|Roterer cellens innhold med 180�',
  // TrvActionTableCellRotation270
  'Roterer cellen med &270�', 'Roterer cellen med 270�|Roterer cellens innhold med 270�',
  // TrvActionTableProperties
  'Tabell &egenskaper...', 'Tabellegenskaper|Endrer egenskapene for tabellen',
  // TrvActionTableGrid
  '&Vis tabell kantlinjer', 'Vis tabell kantlinjer|Skrur p� eller av vising av tabell kantlinjer',
  // TRVActionTableSplit
  'D&el tabell', 'Del tabell|Deler tabellen til to tabeller med start fra den valgte raden',
  // TRVActionTableToText
  'Konverter til Te&kst...', 'Konverter til tekst|Konverterer tabellen til tekst',
  // TRVActionTableSort
  '&Sorter...', 'Sorter|Sorterer radene i tabellen',
  // TrvActionTableCellLeftBorder
  '&Venstre kantlinje', 'Venstre kantlinje|Viser eller skjuler venstre celle kantlinje',
  // TrvActionTableCellRightBorder
  '&H�yre kantlinje', 'H�yre kantlinje|Viser eller skjuler h�yre celle kantlinje',
  // TrvActionTableCellTopBorder
  '&�vre kantlinje', '�vre kantlinje|Viser eller skjuler �vre celle kantlinje',
  // TrvActionTableCellBottomBorder
  '&Nedre kantlinje', 'Nedre kantlinje|Viser elle skjuler nedre celle kantlinje',
  // TrvActionTableCellAllBorders
  '&Alle kantlinje', 'Alle kantlinje|Viser eller skjuler alle celle kantlinjene',
  // TrvActionTableCellNoBorders
  '&Ingen kantlinje', 'Ingen rammer|Skjuler alle celle kantlinjene',
  // TrvActionFonts & TrvActionFontEx
  '&Skrift...', 'Skrift|Endrer skriften i merket tekst',
  // TrvActionFontBold
  '&Fet', 'Fet|Endrer stilen i merket tekst til fet',
  // TrvActionFontItalic
  '&Kursiv', 'Kursiv|Endrer stilen i merket tekst til kursiv',
  // TrvActionFontUnderline
  '&Understreket', 'Understreket|Endrer stilen i merket tekst til understreket',
  // TrvActionFontStrikeout
  '&Gjennomstr�ket', 'Gjennomstr�ket|Endrer stilen i merket tekst til gjennomstr�ket',
  // TrvActionFontGrow
  '&�k skrift', '�k skrift|�ker skrift st�rrelsen med 10%',
  // TrvActionFontShrink
  '&Reduser skrift', 'Reduser skrift|Reduserer skriftst�rrelsen med 10%',
  // TrvActionFontGrowOnePoint
  '�&k skrift et punkt', '�k skrift et punkt|�ker skriftst�rrelsen i merket tekst med 1 punkt ',
  // TrvActionFontShrinkOnePoint
  'Re&duser skrift et punkt', 'Reduser skrift et punkt|Reduserer skriftst�rrelsen i merket tekst med 1 punkt',
  // TrvActionFontAllCaps
  'Store bokst&aver', 'Alle store bokstaver|Endrer alle bokstaver i merket tekst til store bokstaver',
  // TrvActionFontOverline
  '&Overstreket', 'Overstreket|Legger til linje over teksten i merket tekst',
  // TrvActionFontColor
  'Skrift&farge...', 'Skriftfarge|Endrer skriftfargen i merket tekst',
  // TrvActionFontBackColor
  'Tekst bakgrunnsfarge...', 'Tekst bakgrunnsfarge|Endrer bakgrunnsfargen i merket tekst',
  // TrvActionAddictSpell3
  '&Stavekontroll', 'Stavekontroll|Kontrolerer stavingen',
  // TrvActionAddictThesaurus3
  '&Synonym ordbok', 'Synonym ordbok|Gir deg synonymer for det ordet som er merket',
  // TrvActionParaLTR
  'Venstre mot h�yre', 'Venstre mot h�yre|Setter leseretning til venstre mot h�yre for valgte paragrafer',
  // TrvActionParaRTL
  'H�yre mot venstre', 'H�yre mot venstre|Setter leseretning til h�yre mot venstre for valgte paragrafer',
  // TrvActionLTR
  'Venstre mot h�yre tekst', 'Venstre mot h�yre tekst|Setter leseretning til venstre mot h�yre for merket tekst',
  // TrvActionRTL
  'H�yre mot venstre tekst', 'H�yre mot venstre tekst|Setter leseretning til h�yre mot venstre for merket tekst',
  // TrvActionCharCase
  'Bokstavtype', 'Bokstavtype|Endrer bokstavtypen for merket tekst',
  // TrvActionShowSpecialCharacters
  '&Kontrol tegn', 'Kontroll tegn|Viser eller skjuler kontroll tegn som f.eks. paragraf, tabulator og mellomroms merker',
  // TrvActionSubscript
  'Se&nket tekst', 'Senket tekst|Konverterer den valgte teksten til senket tekst',
  // TrvActionSuperscript
  'Hevet tekst', 'Hevet tekst|Konverterer den valgte teksten til hevet tekst',
  // TrvActionInsertFootnote
  '&Fotnote', 'Fotnote|Setter inn en fotnote',
  // TrvActionInsertEndnote
  '&Sluttnote', 'Sluttnote|Setter inn en sluttnote',
  // TrvActionEditNote
  'E&diter note', 'Editer note|Starter editering av fotnote eller sluttnote',
  '&Gjem', 'Gjem|Gjem eller vis det utvalgte fragmentet',      
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
   '&Stiler...', 'Stiler|�pner stil dialogruten',
  // TrvActionAddStyleTemplate
  '&Legg til stil...', 'Legg til stil|Lager en ny tekst eller paragrafstil',
  // TrvActionClearFormat,
  '&T�m format', 'T�m format|T�mmer all tekst og paragraf-formattering fra valgte deler',
  // TrvActionClearTextFormat,
  'T�m &tekstformat', 'T�m tekstformat|T�mmer all formattering fra den valgte teksten',
  // TrvActionStyleInspector
  'Stil &inspekt�r', 'Stil inspekt�r|Viser eller gjemmer stilinspekt�ren',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Avbryt', 'Lukk', 'Sett inn', '&�pne...', '&Lagre...', '&Fjern', 'Hjelp', 'Fjern',
  // Others  -------------------------------------------------------------------
  // percents
  'Prosent',
  // left, top, right, bottom sides
  'Venstre side', '�vre side', 'H�yre side', 'Nedre side',
  // save changes? confirm title
  'Lagre endringene til %s?', 'Bekreft',
  // warning: losing formatting
  '%s kan inneholde formatering som ikke er kompatibelt med valgt lagrings format.'#13+
  '�nsker du � lagre dokumentet i dette formatet?',
  // RVF format name
  'RichView Format',
  // Error messages ------------------------------------------------------------
  'Feil',
  'Feil lasting av fil.'#13#13'Mulige �rsaker:'#13'- formatet i denne file st�ttes ikke ikke av dette programmet;'#13+
  '- filen er korrupt;'#13'- filen er �pnet og l�st av et annet program.',
  'Feil lasting av bilde file.'#13#13'Mulige �rsaker:'#13'- filen er i et format som ikke st�ttes av dette programmet;'#13+
  '- filen inneholder ikke et bilde;'#13+
  '- filen er korrupt;'#13'- filen er �pnet og l�st av et annet program.',
  'Feil lagring av fil.'#13#13'Mulige �rsaker:'#13'- ikke nok lagringsplass;'#13+
  '- lagringsmediet er skrivebeskyttet;'#13'- flyttbart lagringsmediet er ikke tilstede;'#13+
  '- filen er �pnet og l�st av et annet program;'#13'- lagringsmediet er korrupt.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView filer (*.rvf)|*.rvf',  'RTF filer (*.rtf)|*.rtf' , 'XML filer (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Tekst filer (*.txt)|*.txt', 'Tekst filer - Unicode (*.txt)|*.txt', 'Tekst filer - Automatisk valg (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML filer (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Forenklet (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word-dokumenter (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'S�k ferdig', 'S�ke streng ''%s'' ikke funnet.',
  // 1 string replaced; Several strings replaced
  '1 streng erstattet.', '%d strenger erstattet.',
  // continue search from the beginning/end?
  //'The end of the document is reached, continue from the beginning?',
  //'The beginning of the document is reached, continue from the end?',
  'Du er ved slutten av dokumentet, starte forfra?',
  'Du er ved begynnelsen av dokumentet, fortsette fra slutten?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Gjennomsiktig', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Svart', 'Brun', 'Olivengr�nn', 'M�rk gr�nn', 'M�rk bl�gr�nn', 'M�rk bl�', 'Indigo', '80% gr�',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'M�rk r�d', 'Oransje', 'Mosegr�nn', 'Gr�nn', 'Bl�gr�nn', 'Bl�', 'Bl�gr�', '50% gr�',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'R�d', 'Lys oransje', 'Sitrusgr�nn', 'Sj�gr�nn', 'Akvamarine', 'Lys bl�', 'Fiolett', '40% gr�',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Gull', 'Gul', 'Skarp gr�nn', 'Turkis', 'Himmel bl�', 'Plomme', '25% gr�',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Lys rosa', 'Lys brun', 'Lys gul', 'Lys gr�nn', 'Lys turkis', 'Svak lys bl�', 'Lavendel', 'Hvit',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  '&Gjennomsiktig', '&Auto', '&Flere farger...', '&Standard',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Bakgrunn', '&Farger:', 'Posisjon', 'Bakgrunn', 'Eksempel tekst.',
  //positions: None, Tiles, Full Window, Center, Fixed Tiles
  '&Ingen', '&Hele vinduet', 'L�s&te fliser', 'Flis&er', '&Sentrert',
  // Padding button
  '&Utfylling...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Fyll farge', '&Gj�r gjellende for:', '&Flere farger...', '&Utfylling...',
  'Vennligs velge en farge',
  // [apply to:] text, paragraph, table, cell
  'tekst', 'paragraf' , 'tabell', 'cell',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Skrift', 'Sk&rift', 'Tegna&vstand',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Skrif&t:', 'St&�rrelse', 'Sk&riftstil', '&Fet', '&Kursiv',
  // Script, Color, Back color labels, Default charset
  'Skrifttyp&e:', 'F&arge:', 'Bak&grunn:', '(Standard)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Effekter', 'Understreket', '&Overstreket', '&Gjennomstreket', '&Store bokstaver',
  // Sample, Sample text
  'Eksempel', 'Eksempeltekst',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Tegnavstand', '&Mellomrom:', 'Sp&erret', '&Knepet',
  // Offset group-box, Offset label, Down, Up,
  'Avstand', '&Avstand:', '&Senket', '&Hevet',
  // Scaling group-box, Scaling
  'Skala', 'S&kala:',
  // Sub/super script group box, Normal, Sub, Super
  'Senket tekst og hevet tekst', '&Normal', 'Se&nket tekst', 'He&vet tekst',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Utfylling', '&Topp:', '&Venstre:', '&Bunn:', '&H�yre:', '&Gi alle samme verdi',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Sett inn hyperkobling', 'Hyperkobling', 'T&ekst:', '&M�l', '<<merket>>',
  // cannot open URL
  'Kan ikke navigere til "%s"',
  // hyperlink properties button, hyperlink style
  '&Tilpasse...', '&Stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Hyperkobling egenskaper', 'Normal farger', 'Aktiv farger',
  // Text [color], Background [color]
  '&Tekst:', '&Bakgrunn',
  // Underline [color]
  'U&nderstrek:', 
  // Text [color], Background [color] (different hotkeys)
  'T&ekst:', 'B&akgrunn',
  // Underline [color], Attributes group-box,
  'U&nderstrek:', 'Attributter',
  // Underline type: always, never, active (below the mouse)
  '&Understrek:', 'alltid', 'aldri', 'aktiv',
  // Same as normal check-box
  'Som &normal',
  // underline active links check-box
  '&Understrek aktive lenker',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Sett inn symbol', '&Skrift:', '&Tegn sett:', 'Unicode',
  // Unicode block
  '&Blokk:',  
  // Character Code, Character Unicode Code, No Character
  'Tegn kode: %d', 'Tegn kode: Unicode %d', '(ingen tegn)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Sett inn tabell', 'Tabell st�rrelse', 'Antall &kolonner:', 'Antall &rekker:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tabell utforming', '&Auto st�rrelse', 'Tilpass tabellen til &vinduet', 'Tilpass tabellen &manuelt',
  // Remember check-box
  'Husk &dimensjonen for nye tabeller',
  // VAlign Form ---------------------------------------------------------------
  'Possisjon',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Egenskaper', 'Bilde', 'Posisjon og st�rrelse', 'Linje', 'Tabell', 'Rekke', 'Celler',
  // Image Appearance, Image Misc, Number tabs
  'Utseende', 'Annet', 'Nummer',
  // Box position, Box size, Box appearance tabs
  'Posisjon', 'St�rrelse', 'Utseende',
  // Preview label, Transparency group-box, checkbox, label
  'Forh�ndsvisning:', '&Gjennomsiktighet', '&Gjennomsiktig', 'Gjennomsiktig &farge:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'E&ndre...', '&Lagre...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikal justering', '&Juster:',
  'under kanten til basislinjen av teksten', 'midten av kanten til basislinjen av teksten',
  'topp til linjetopp', 'bunn til linjebunn', 'midt til linjemidt',
  // align to left side, align to right side
  'venstre side', 'h�yre side',      
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Avstand:', 'Strekk', '&Bredde:', '&H�yde:', 'Orginal st�rrelse: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Skaler &proporsjonelt', '&Nullstill',
  // Inside the border, border, outside the border groupboxes
  'Innenfor kanten', 'Kanten', 'Utenfor kanten',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Fyll farge:', '&Padding:',
  // Border width, Border color labels
  'Kant &bredde:', 'Kant &farge:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horisontal fordeling:', '&Vertikal fordeling:',
  // Miscellaneous groupbox, Tooltip label
  'Annet', '&Verkt�ytips:',
  // web group-box, alt text
  'Web', 'Alternativ &tekst:',
  // Horz line group-box, color, width, style
  'Horisontal linje', '&Farge:', '&Bredde:', '&Stil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabell', '&Bredde:', '&Fyll farge:', '&Celle &avstand...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horisontal celle-padding:', '&Vertikal celle-padding:', 'Kant og bakgrunn',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Synlige &kantsider...', 'Auto', 'auto',
  // Keep row content together checkbox
  '&Hold innhold sammen',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Rotasjon', '&Ingen', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Kant:', 'Synlige k&antsider...',
  // Border, CellBorders buttons
  '&Tabell kantlinje...', '&Celle kantlinje...',
  // Table border, Cell borders dialog titles
  'Tabell kantlinje', 'Standard celle kantlinje',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Utskrift', '&Hold tabellen p� en side', 'Trekk ifra &overskrifts rekker:',
  'Overskrifts rekker gjentas p� hver side med tabellen',
  // top, center, bottom, default
  '&Topp', 'Midt&still', '&Bunn', 'Sta&ndard',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Innstillinger', 'Foretrukket &bredde:', '&H�yde minimum:', '&Fyll farge:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Kantlinje', 'Synlige sider:',  'Skyggef&arge:', '&Lys farge', 'F&arge:',
  // Background image
  'B&ilde...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horisontal posisjon', 'Vertikal posisjon', 'Posisjon i tekst',
  // Position types: align, absolute, relative
  'Juster', 'Absolutt posisjon', 'Relativ posisjon',
  // [Align] left side, center, right side
  'venstre side av boks til venstre side av',
  'midten av boks til midten av',
  'h�yre side av boks til h�yre side av',
  // [Align] top side, center, bottom side
  'toppside av boks til toppside av',
  'midten av boks til midten av',
  'sidebunn av boks til sidebunn av',
  // [Align] relative to
  'relativ til',
  // [Position] to the right of the left side of
  'til h�yre for den venstre siden av',
  // [Position] below the top side of
  'under toppsiden av',
  // Anchors: page, main text area (x2)
  '&Side', '&Hovedtekstomr�de', 'S&ide', 'Hovedte&kstomr�de',
  // Anchors: left margin, right margin
  '&Venstremargin', '&H�yremargin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Toppmargin', '&Bunnmargin', '&Indremargin', '&Yttermargin',
  // Anchors: character, line, paragraph
  '&Tegn', 'L&inje', 'Para&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Speil&ede marginer', 'Opp&sett i tabellcelle',
  // Above text, below text
  '&Over tekst', '&Under tekst',
  // Width, Height groupboxes, Width, Height labels
  'Bredde', 'H�yde', '&Bredde:', '&H�yde:',
  // Auto height label, Auto height combobox item
  'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikal justering', '&Topp', '&Midten', '&Bunn',
  // Border and background button and title
  'K&ant og bakgrunn...', 'Bokskant og bakgrunn',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Lim inn spesifisert', '&Lim inn som:', 'Rik tekst format', 'HTML format',
  'Tekst', 'Unicode tekst', 'Bitmap bilde', 'Metafile bilde',
  'Grafikk filer', 'URL',
  // Options group-box, styles
  'Valg', '&Stiler:',
  // style options: apply target styles, use source styles, ignore styles
  'bruk stiler fra m�ldokument', 'behold stiler og utseende',
  'behold utseende, ignorer stiler',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Punkter og nummerering', 'Punkter', 'Numerering', 'Punkt lister', 'Nummererte lister',
  // Customize, Reset, None
  '&Tilpass...', '&Tilbakestill', 'Ingen',
  // Numbering: continue, reset to, create new
  'fortsett nummerering', 'tilbakestill nummerering til', 'lag en ny liste, start fra',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Stor bokstav Alfa', 'Stor bokstav Roman', 'Desimal', 'Lav bokstav Alfa', 'Lav bokstav Roman',
  // Bullet type
  'Sirkel', 'Skive', 'Firkant',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Niv�:', '&Start fra:', '&Fortsett', 'Nummerering',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Tilpasse liste', 'Niv�er', '&Antall:', 'Liste egenskaper', '&Liste type:',
  // List types: bullet, image
  'punkt', 'bilde', 'Sett inn nummer|',
  // Number format, Number, Start level from, Font button
  '&Nummer format:', 'Nummer', '&Start niv� numerering fra:', '&Skrift...',
  // Image button, bullet character, Bullet button,
  '&Bilde...', 'Punkt te&gn', '&Punkter...',
  // Position of list text, bullet, number, image, text
  'Liste tekst posisjon', 'Punkt posisjon', 'Nummer posisjon', 'Bilde posisjon', 'Tekst posisjon',
  // at, left indent, first line indent, from left indent
  '&p�:', '&Venstre innrykk:', '&f�rste linje innrykkt:', 'fra venstre innrykk',
  // [only] one level preview, preview
  '&Forh�ndsvis et niv�', 'Forh�ndsvis',
  // Preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Venstre juster', 'H�yre juster', 'Midtstill juster',
  // level #, this level (for combo-box)
  'Niv� %d', 'Dette niv�et',
  // Marker character dialog title
  'Rediger Punkt tegn',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Paragraf ramme og bakgrunn', 'Ramme', 'Bakgrunn',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Innstilinger', '&Farge:', '&Bredde:', 'Inte&rn bredde:', '&Utfylling...',
  // Sample, Border type group-boxes;
  'Eksempel', 'Ramme type',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Ingen', '&Enkel', '&Dobbel', '&Tripple', 'Tykk i&nside', 'Ty&kk utside',
  // Fill color group-box; More colors, padding buttons
  'Fyll farge', '&Flere farger...', '&Utfylling...',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // title and group-box in Offsets dialog
  'Utfylling', 'Utfylling ramme',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paragraf', 'Justering', '&Venstre', '&H�yre', '&Midtstill', '&Blokk',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Utfylling', 'F�&r:', '&Etter:', 'Linje &avstand:', '&Posisjon:',
  // Indents group-box; indents: left, right, first line
  'Innrykk', 'Ve&nstre:', 'H�&yre:', '&F�rste linje:',
  // indented, hanging
  '&Innrykket', 'Hen&gende', 'Eksempel',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Enkel', '1.5 linjer', 'Dobbel', 'Minst', 'Eksakt', 'Flere',
  // preview text
  'Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst. Tekst tekst tekst tekst tekst.',
  // tabs: Indents and spacing, Tabs, Text flow
  '&Innrykk og avstand', '&Tabulatorer', 'Tekst&flyt',
  // tab stop position label; buttons: set, delete, delete all
  '&Tabulatorposisjon:', '&Angi', '&Fjern', 'Fj&ern alle',
  // tab align group; left, right, center aligns,
  'Justering', '&Venstre', '&H�yre', '&Midtstilt',
  // leader radio-group; no leader
  'Fylltegn', '(Ingen)',
  // tab stops to be deleted, delete none, delete all labels
  'Fjern f�lgende tabulatorer:', '(Ingen)', 'Alle.',
  // Pagination group-box, keep with next, keep lines together
  'Paginering', 'Hold sammen med &neste', '&Hold linjene samlet',
  // Outline level, Body text, Level #
  '&Skisse niv�:', 'Hoved tekst', 'Niv� %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Forh�ndsvisning utskrift', 'Side bredde', 'Hele siden', 'Side:',
  // Invalid Scale, [page #] of #
  'Vennligst bruk kun nummber mellom 10 og 500', 'av %d',
  // Hints on buttons: first, prior, next, last pages
  'F�rste side', 'Forrige side', 'Neste side', 'Siste side',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Celle utfylling', 'Utfylling', '&Mellom cellene', 'Fra tabell kanten til cellene',
  // vertical, horizontal (x2)
  '&Vertikal:', '&Horisontal:', 'V&ertikal:', 'H&orisontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Kantlinjer', 'Innstillinger', '&Farge:', '&Lys farge:', '&M�rk farge:',
  // Width; Border type group-box;
  '&Bredde:', 'Ramme type',
  // Border types: none, sunken, raised, flat
  '&Ingen', '&Senket', '&Hevet', '&Flat',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Del opp', 'Del opp til',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Spesifisert nummer med rekker og kolonner', '&Orginal celler',
  // number of columns, rows, merge before
  'Antall &kolonner:', 'Antall &rekker:', '&Sl� i sammen f�r oppdeling',
  // to original cols, rows check-boxes
  'Del opp &kolonner til orginal', 'Del opp &rekker til orginal',
  // Add Rows form -------------------------------------------------------------
  'Legg til &rekker', '&Antall rekker:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Side oppsett', 'Side', 'Topptekst og bunntekst',
  // margins group-box, left, right, top, bottom
  'Marger (millimeter)', 'Marger (tommer)', '&Venstre:', '&Topp:', '&H�yre:', '&Bunn:',
  // mirror margins check-box
  '&Speill &margene',
  // orientation group-box, portrait, landscape
  'Papirretning', 'St&�ende', '&Liggende',
  // paper group-box, paper size, default paper source
  'Papir', 'Papirst&�rrelse:', '&Kilde:',
  // header group-box, print on the first page, font button
  'Topptekst', '&Tekst:', '&Topptekst p� f�rste sid&en', '&Skrift...',
  // header alignments: left, center, right
  '&Venstre', '&Midtstilt', '&H�yre',
  // the same for footer (different hotkeys)
  'Bunntekst', 'Tek&st:', '&Bunntekst p� f�rste siden', 'Skri&ft...',
  'Ve&nstre', 'Mi&dtstilt', 'H�&yre',
  // page numbers group-box, start from
  'Side t&all', 'Sta&rt med',
  // hint about codes
  'Spesielle tegnkombinasjoner:'#13'&&p - side tall; &&P - antall sider; &&d - n�v�rende dato; &&t - n�v�rende tid.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Tekst Fil Kode Side', '&Velg filens koding:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japansk', 'Kinesisk (Forenklet)', 'Koreansk',
  // chinese (trad), central european, cyrillic, west european
  'Kinesisk (Tradisjonell)', 'Sentral og �st-Europeisk', 'Kyrrilisk', 'Vest-Europeisk',
  // greek, turkish, hebrew, arabic
  'Gresk', 'Tyrkisk', 'Hebraisk', 'Arabisk',
  // baltic, vietnamese, utf-8, utf-16
  'Baltisk', 'Vietnamesisk', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Visuell stil', '&Velg stil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Konverter til tekst', 'Velg &skikketegn:',
  // line break, tab, ';', ','
  'Linjeskift', 'tab', 'semikolon', 'komma',
  // Table sort form -----------------------------------------------------------
  // error message
  'En tabell som inneholder sammensl�tte rader kan ikke sortert',
  // title, main options groupbox
  'Tabell type', 'Type',
  // sort by column, case sensitive
  '&Sorter etter kolonne:', '&Skill mellom store og sm� bokstaver',
  // heading row, range or rows
  'Ekskluder &overskrift p� radenh', 'Rader som sorteres: fra %d til %d',
  // order, ascending, descending
  'Rekkef�lge', '&Stigende', '&Synkende',
  // data type, text, number
  'Type', '&Tekt', '&Nummer',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Sett inn nummer', 'Egenskaper', '&Tellernavn:', '&Nummereringstype:',
  // numbering groupbox, continue, start from
  'Nummerering', 'F&ortsett', '&Start fra:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Bildetekst', '&Etikett:', '&Ekskluder etikett fra bildetekst',
  // position radiogroup
  'Posisjon', '&Over valgte objekt', '&Under valgte objekt',
  // caption text
  'Bilde&tekst:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Nummerering', 'Figur', 'Tabell',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Synlige kantsider', 'Kant',
  // Left, Top, Right, Bottom checkboxes
  '&Venstre side', '&Toppside', '&H�yre side', '&Bunnside',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Flytt tabellkolonne', 'Flytt tabellrad',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Innrykk i f�rste linje', 'Venstreinnrykk', 'Hengende innrykk', 'H�yreinnrykk',
  // Hints on lists: up one level (left), down one level (right)
  'Opp et niv�', 'Ned et niv�o',
  // Hints for margins: bottom, left, right and top
  'Bunnmarg', 'Venstre marg', 'H�yre marg', 'Toppmarg',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Venstretabulator', 'H�yretabulator', 'Midtstillingstabulator', 'Desimaltabulator',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Normalt innrykk', 'Ingen mellomrom', 'Overskrift %d', 'Liste avsnitt',
  // Hyperlink, Title, Subtitle
  'Hyperlenke', 'Tittel', 'Undertittel',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Vektlegging', 'Subtil vektlegging', 'Intens vektlegging', 'Sterk',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Sitering', 'Intens sitering', 'Subtil referanse', 'Intens referanse',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blokktekst', 'HTML-variabel', 'HTML-kode', 'HTML-akronym',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML-definisjon', 'HTML-tastatur', 'HTML-pr�ve', 'HTML-skrivemaskin',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML-preformatert', 'HTML-sitering', 'Overskrift', 'Bunn', 'Sidenummer',
  // Caption
  'Bildetekst',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Sluttekst referanse', 'Bunnotat referanse', 'Bunntekst', 'Bunnotat tekst',
  // Sidenote Reference, Sidenote Text
  'Sidenotereferanse', 'Sidenotetekst',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'farge', 'bakgrunnsfarge', 'transparent', 'standard', 'understrekingsfarge',
  // default background color, default text color, [color] same as text
  'standard bakgrunnsfarge', 'standard tekstfarge', 'samme som tekst',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'singel', 'tykk', 'dobbel', 'stiplet', 'tykk stiplet', 'streket',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tykk streket', 'langstreket', 'tykk langstreket',
  // underline types: dash dotted, thick dash dotted,
  'strekprikket', 'tykk strekprikket',
  // underline types: dash dot dotted, thick dash dot dotted
  'strek prikk prikket', 'tykk strek prikk prikket',
  // sub/superscript: not, subsript, superscript
  'ikke under/superskript', 'underskript', 'superskript',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di modus:', 'nedarvet', 'venstre til h�yre', 'h�yre til venstre',
  // bold, not bold
  'fet', 'ikke fet',
  // italic, not italic
  'kursiv', 'ikke kursiv',
  // underlined, not underlined, default underline
  'understreket', 'ikke understreket', 'standard understreking',
  // struck out, not struck out
  'utsl�tt', 'ikke utsl�tt',
  // overlined, not overlined
  'overlinjert', 'ikke overlinjert',
  // all capitals: yes, all capitals: no
  'alle store bokstaver', 'all store bokstaver av',
  // vertical shift: none, by x% up, by x% down
  'uten vertikal forskyvning', 'forskj�vet av %d%% opp', 'forskj�vet av %d%% ned',
  // characters width [: x%]
  'tegn bredde',
  // character spacing: none, expanded by x, condensed by x
  'normal tegnavstand', 'tegnavstand uthevet av %s', 'tegnavstand sammendratt av %s',
  // charset
  'skript',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'standard skrifttype', 'standard avsnitt', 'nedarvet',
  // [hyperlink] highlight, default hyperlink attributes
  'uthev', 'standard',
  // paragraph aligmnment: left, right, center, justify
  'venstrejuster', 'h�yrejustert', 'sentrert', 'justert',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'linjeh�yde: %d%%', 'avstand mellom linjer: %s', 'linjeh�yde: minst %s',
  'linjeh�yde: n�yaktig %s',
  // no wrap, wrap
  'linjebryting deaktivert', 'linjebryting',
  // keep lines together: yes, no
  'hold linjene sammen', 'ikke hold linjene sammen',
  // keep with next: yes, no
  'hold sammen med neste avsnitt', 'ikke hold sammen med neste avsnitt',
  // read only: yes, no
  'kun lesing', 'redigerbar',
  // body text, heading level x
  'skisseniv�: hovedtekst', 'skissenv�: %d',
  // indents: first line, left, right
  'f�rste linjeinnrykk', 'venstre innrykk', 'h�yre innrykk',
  // space before, after
  'mellomrom f�r', 'mellomrom etter',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulatorer', 'ingen', 'juster', 'venstre', 'h�yre', 'sentrert',
  // tab leader (filling character)
  'punktering',
  // no, yes
  'nei', 'ja',
  // [padding/spacing/side:] left, top, right, bottom
  'venstre', 'topp', 'h�yre', 'bunn',
  // border: none, single, double, triple, thick inside, thick outside
  'ingen', 'singel', 'dobbel', 'trippel', 'tykk innside', 'tykk utside',
  // background, border, padding, spacing [between text and border]
  'bakgrunn', 'kant', 'utfylling', 'avstand',
  // border: style, width, internal width, visible sides
  'stil', 'bredde', 'intern bredde', 'synlige sider',
  // style inspector -----------------------------------------------------------
  // title
  'Stil inspekt�r',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stil', '(Ingen)', 'Avsnitt', 'Skrifttype', 'Attributter',
  // border and background, hyperlink, standard [style]
  'Kant og bakgrunn', 'Hyperlenke', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stileres', 'Stil',
  // name, applicable to,
  '&Navn:', 'Aktuelt &for:',
  // based on, based on (no &),
  '&Basert p�:', 'Basert p�:',
  //  next style, next style (no &)
  'Stil for &f�lgende avsnitt:', 'Stil for f�lgende avsnitt:',
  // quick access check-box, description and preview
  '&Hurtigtilgang', 'Beskrivelse og &forh�ndsvisning:',
  // [applicable to:] paragraph and text, paragraph, text
  'avsnitt og tekst', 'avsnitt', 'tekst',
  // text and paragraph styles, default font
  'Tekst og avsnittstiler', 'Standard skrifttype',
  // links: edit, reset, buttons: add, delete
  'Rediger', 'Tilbakestill', '&Legg til', '&Slett',
  // add standard style, add custom style, default style name
  'Legg til &standard stil...', 'Legg til &tilpasset stil', 'Stil %d',
  // choose style
  'Velg &stil:',
  // import, export,
  '&Importer...', '&Eksporter...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'RichView Stiler (*.rvst)|*.rvst', 'Feil ved lasting av fil', 'Denne filen inneholder ingen stiler',
  // Title, group-box, import styles
  'Importer styler fra fil', 'Importer', 'I&mporter stiler:',
  // existing styles radio-group: Title, override, auto-rename
  'Eksisterende stiler', '&overstyr', '&legg til omd�pt',
  // Select, Unselect, Invert,
  '&Marker', '&Avmarker', '&Snu',
  // select/unselect: all styles, new styles, existing styles
  '&Alle stiler', '&Nye stiler', '&Eksisterende stiler',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'T�m  format', 'Alle stiler',
  // dialog title, prompt
  'Stiler', '&Velg stiler som skal brukes:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  //'&Synonyms', '&Ignore All', '&Add to Dictionary'
  '&Synonymer', '&Ignorer alle', '&Legg til i ordboken',
  // Progress messages ---------------------------------------------------------
  'Laster ned %s', 'Klargj�r for skriving...', 'Skriver ut side %d',
  'Konverterer fra RTF...',  'Konverterer til RTF...',
  'Leser %s...', 'Skriver %s...', 'tekst',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ingen note', 'Fotnote', 'Endenote', 'Sidenote', 'Tekstboks'
  );


initialization
  RVA_RegisterLanguage(
    'Norwegian', // english language name, do not translate
    'Norsk (Bokm�l)', // native language name
    ANSI_CHARSET, @Messages);
end.


