unit dmActionsEx;

interface

uses
  SysUtils, Classes, ActnList, ImgList, Controls, RichViewActions,
  RVAHTMLList;

type
  TrvActionsResourceEx = class(TDataModule)
    ImageList1: TImageList;
    ActionList1: TActionList;
    RVActionParaListHTML1: TRVActionParaListHTML;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rvActionsResourceEx: TrvActionsResourceEx;

implementation

{$R *.dfm}

end.
