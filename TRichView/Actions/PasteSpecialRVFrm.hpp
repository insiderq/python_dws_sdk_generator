﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'PasteSpecialRVFrm.pas' rev: 27.00 (Windows)

#ifndef PastespecialrvfrmHPP
#define PastespecialrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RichViewActions.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <ListRVFrm.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Pastespecialrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVPasteSpecial;
class PASCALIMPLEMENTATION TfrmRVPasteSpecial : public Listrvfrm::TfrmRVList
{
	typedef Listrvfrm::TfrmRVList inherited;
	
__published:
	Vcl::Stdctrls::TGroupBox* gb;
	Vcl::Stdctrls::TLabel* lblStyles;
	Vcl::Stdctrls::TComboBox* cmbStyles;
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	HIDESBASE void __fastcall FormShow(System::TObject* Sender);
	void __fastcall lstClick(System::TObject* Sender);
	
private:
	int LastStyleOption;
	
public:
	Vcl::Controls::TControl* _gb;
	Vcl::Controls::TControl* _cmbStyles;
	void __fastcall InitPasteAs(const System::UnicodeString RVFCaption, Rvedit::TCustomRichViewEdit* rve);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
	DYNAMIC void __fastcall Localize(void);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVPasteSpecial(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Listrvfrm::TfrmRVList(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVPasteSpecial(System::Classes::TComponent* AOwner, int Dummy) : Listrvfrm::TfrmRVList(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVPasteSpecial(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVPasteSpecial(HWND ParentWindow) : Listrvfrm::TfrmRVList(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Pastespecialrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PASTESPECIALRVFRM)
using namespace Pastespecialrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PastespecialrvfrmHPP
