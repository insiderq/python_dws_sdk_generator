﻿// This file is a copy of RVAL_Rus.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file

{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Russian translation                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVALUTF8_Rus;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'дюймы', 'см', 'мм', 'пики', 'пиксели', 'пункты',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'см', 'мм', 'пик', 'пикс.', 'пт',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  '&Файл', '&Правка', 'Фо&рмат', '&Шрифт', '&Абзац', '&Вставка', '&Таблица', '&Окно', '&Справка',
  // exit
  'Вы&ход',
  // top-level menus: View, Tools,
  'В&ид', 'С&ервис',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Размер', 'Начертание', 'В&ыделить', 'Выравнивание в &ячейке', 'Ра&мка ячейки',
  // menus: Table cell rotation
  'Вра&щение ячейки',
  // menus: Text flow, Footnotes/endnotes
  '&Обтекание', '&Сноски',
  // ribbon tabs: tab1, tab2, view, table
  '&Главная', 'Вставка и &фон', '&Вид', '&Таблица',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Буфер обмена', 'Шрифт', 'Абзац', 'Список', 'Редактирование',
  // ribbon groups: insert, background, page setup,
  'Вставка', 'Фон', 'Параметры страницы',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Связи', 'Сноски', 'Режимы просмотра документа', 'Показать или скрыть', 'Масштаб', 'Параметры',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Вставка', 'Удаление', 'Операции', 'Рамки',
  // ribbon groups: styles 
  'Стили',
  // ribbon screen tip footer,
  'Нажмите F1 для справки',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Последние документы', 'Сохранить копию документа', 'Предварительный просмотр и печать',
  // ribbon label: units combo
  'Единицы:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Создать', 'Создать|Создание нового документа',
  // TrvActionOpen
  '&Открыть...', 'Открыть|Открытие файла с документом',
  // TrvActionSave
  'Со&хранить', 'Сохранить|Сохранение документа',
  // TrvActionSaveAs
  'Сохранить &как...', 'Сохранить как...|Сохранение документа под другим именем, в другом месте или формате',
  // TrvActionExport
  '&Экспорт...', 'Экспорт|Экспорт документа в файл с другим именем, в другое место или формат',
  // TrvActionPrintPreview
  'Пред&варительный просмотр', 'Предварительный просмотр|Просмотр документа в том виде, в котором он будет напечатан',
  // TrvActionPrint
  '&Печать...', 'Печать|Задание параметров печати и печать документа',
  // TrvActionQuickPrint
  '&Печать', '&Быстрая печать', 'Печать|Печать документа',
   // TrvActionPageSetup
  'П&араметры страницы...', 'Параметры страницы|Установка полей, размера и источника подачи бумаги, колонтитулов',  
  // TrvActionCut
  '&Вырезать', 'Вырезать|Удаление выделенного фрагмента и помещение его в буфер обмена',
  // TrvActionCopy
  '&Копировать', 'Копировать|Копирование выделенного фрагмента в буфер обмена',
  // TrvActionPaste
  'Вст&авить', 'Вставить|Вставка фрагмента из буфера обмена в текущую позицию документа',
  // TrvActionPasteAsText
  'Вставить как &текст', 'Вставить как текст|Вставка текста из буфера обмена',  
  // TrvActionPasteSpecial
  'Сп&ециальная вставка...', 'Специальная вставка|Вставка содержимого буфера обмена в документ в выбранном формате',
  // TrvActionSelectAll
  'В&ыделить всё', 'Выделить всё|Выделение всего документа',
  // TrvActionUndo
  '&Отменить', 'Отменить|Отмена последней операции',
  // TrvActionRedo
  'Вернут&ь', 'Вернуть|Отмена действия команды "Отменить"',
  // TrvActionFind
  '&Найти...', 'Найти|Поиск указанного текста в документе',
  // TrvActionFindNext
  'Найти &далее', 'Найти далее|Продолжение последнего поиска текста',
  // TrvActionReplace
  '&Заменить...', 'Заменить|Поиск и замена указанного текста в документе',
  // TrvActionInsertFile
  '&Файл...', 'Вставить файл|Вставка файла в текущую позицию документа',
  // TrvActionInsertPicture
  '&Рисунок...', 'Вставить рисунок|Вставка рисунка в текущую позицию документа',
  // TRVActionInsertHLine
  '&Горизонтальная линия', 'Вставить горизонтальную линию|Вставка горизонтальной линии в текущую позицию документа',
  // TRVActionInsertHyperlink
  'Гип&ерссылка...', 'Добавить гиперссылку|Добавление новой или редактирование выделенной гиперссылки',
  // TRVActionRemoveHyperlinks
  '&Удалить гиперссылки', 'Удалить гиперссылки|Удаление всех гиперссылок из выделенного текста',  
  // TrvActionInsertSymbol
  '&Символ...', 'Вставить символ|Вставка символа или специального знака в текущую позицию документа',
  // TrvActionInsertNumber
  'Но&мер...', 'Вставить номер|Вставка номера',
  // TrvActionInsertCaption
  '&Вставить название...', 'Вставить название|Добавление названия выделенного объекта',
  // TrvActionInsertTextBox
  '&Текстовое поле', 'Вставить текстовое поле|Вставка текстового поля в текущую позицию документа',
  // TrvActionInsertSidenote
  'Сноска в р&амке', 'Вставить сноску в рамке|Вставка сноски, отображаемой в текстовом поле, в текущую позицию документа',
  // TrvActionInsertPageNumber
  'Номер стр&аницы', 'Вставить номер страницы|Вставка номера страницы в текущую позицию документа',
  // TrvActionParaList
  '&Список...', 'Список|Добавление или изменение формата маркеров или нумерации выделенных абзацев',
  // TrvActionParaBullets
  '&Маркеры', 'Маркеры|Добавление или удаление маркировки выделенных абзацев',
  // TrvActionParaNumbering
  '&Нумерация', 'Нумерация|Добавление или удаление нумерации выделенных абзацев',
  // TrvActionColor
  '&Цвет фона...', 'Цвет фона|Изменение цвета фона документа',
  // TrvActionFillColor
  '&Заливка...', 'Заливка|Изменение цвета фона текста, абзаца, таблицы или ячейки',
  // TrvActionInsertPageBreak
  '&Разрыв страницы', 'Разрыв страницы|Добавление разрыва страницы в текущую позицию документа',
  // TrvActionRemovePageBreak
  '&Удалить разрыв страницы', 'Удалить разрыв страницы|Удаление разрыва страницы',
  // TrvActionClearLeft
  'Не обтекать &левую сторону', 'Не обтекать левую сторону|Размещение абзаца под объектами, выровненными по левому краю',
  // TrvActionClearRight
  'Не обтекать &правую сторону', 'Не обтекать правую сторону|Размещение абзаца под объектами, выровненными по правому краю',
  // TrvActionClearBoth
  'Не обтекать &обе стороны', 'Не обтекать обе стороны|Размещение абзаца под объектами, выровненными по краям',
  // TrvActionClearNone
  'О&бтекать обе стороны', 'Обтекать обе стороны|Разрешение абзацу обтекать объекты, выровненные по краям',
  // TrvActionVAlign
  '&Положение объекта...', 'Положение объекта|Задание положения выбранного объекта в тексте',
  // TrvActionItemProperties
  'Свойства &объекта...', 'Свойства объекта|Задание свойств выделенного рисунка, линии или таблицы',
  // TrvActionBackground
  '&Фон...', 'Фон|Выбор фонового рисунка и заливки',
  // TrvActionParagraph
  '&Абзац...', 'Абзац|Изменение атрибутов выделенных абзацев',
  // TrvActionIndentInc
  'У&величить отступ', 'Увеличить отступ|Увеличение левого отступа выделенных абзацев',
  // TrvActionIndentDec
  'У&меньшить отступ', 'Уменьшить отступ|Уменьшение левого отступа выделенных абзацев',
  // TrvActionWordWrap
  'П&еренос по словам', 'Перенос по словам|Разрешение или запрещение автоматического перевода строк внутри выделенных абзацев',
  // TrvActionAlignLeft
  'По &левому краю', 'По левому краю|Выравнивание выделенного текста по левому краю',
  // TrvActionAlignRight
  'По &правому краю', 'По правому краю|Выравнивание выделенного текста по правому краю',
  // TrvActionAlignCenter
  'По &центру', 'По центру|Выравнивание выделенного текста по центру',
  // TrvActionAlignJustify
  'По &ширине', 'По ширине|Выравнивание выделенного текста по левому и правому краям',
  // TrvActionParaColor
  'Цвет заливки а&бзаца...', 'Цвет заливки абзаца|Выбор цвета заливки абзаца',
  // TrvActionLineSpacing100
  '&Одинарный интервал', 'Одинарный интервал|Установка одинарного интервала между строками абзаца',
  // TrvActionLineSpacing150
  'Пол&уторный интервал', 'Полуторный интервал|Установка полуторного интервала между строками абзаца',
  // TrvActionLineSpacing200
  '&Двойной интервал', 'Двойной интервал|Установка двойного интервала между строками абзаца',
  // TrvActionParaBorder
  '&Рамка и заливка абзаца...', 'Рамка и заливка абзаца|Задание рамки, заливки и полей выделенных абзацев',
  // TrvActionInsertTable
  'Вставить &таблицу...', '&Таблица', 'Вставить таблицу|Вставка таблицы в текущую позицию документа',
  // TrvActionTableInsertRowsAbove
  'Добавить строку &выше', 'Добавить строку выше|Добавление новой строки над выделенными ячейками',
  // TrvActionTableInsertRowsBelow
  'Добавить строку &ниже', 'Добавить строку ниже|Добавление новой строки под выделенными ячейками',
  // TrvActionTableInsertColLeft
  'Добавить столбец с&лева', 'Добавить столбец слева|Добавление нового столбца слева от выделенных ячеек',
  // TrvActionTableInsertColRight
  'Добавить столбец с&права', 'Добавить столбец справа|Добавление нового столбца справа от выделенных ячеек',
  // TrvActionTableDeleteRows
  'Удалить ст&роки', 'Удалить строки|Удаление строк с выделенными ячейками',
  // TrvActionTableDeleteCols
  'Удалить ст&олбцы', 'Удалить столбцы|Удаление столбцов с выделенными ячейками',
  // TrvActionTableDeleteTable
  '&Удалить таблицу', 'Удалить таблицу|Удаление таблицы',
  // TrvActionTableMergeCells
  'О&бъединить ячейки', 'Объединить ячейки|Объединение выделенных ячеек в одну',
  // TrvActionTableSplitCells
  'Р&азбить ячейки...', 'Разбить ячейки|Разбиение выделенных ячеек на заданной число строк и столбцов',
  // TrvActionTableSelectTable
  'В&ыделить таблицу', 'Выделить таблицу|Выделение всей таблицы',
  // TrvActionTableSelectRows
  'Выделить стро&ки', 'Выделить строки|Выделение строк, содержащих выделенные ячейки',
  // TrvActionTableSelectCols
  'Выделить столб&цы', 'Выделить столбцы|Выделение столбцов, содержащих выделенные ячейки',
  // TrvActionTableSelectCell
  'Выделить &ячейку', 'Выделить ячейку|Выделение ячейки',
  // TrvActionTableCellVAlignTop
  'Выровнять по &верхнему краю', 'Выровнять по верхнему краю|Выравнивание содержимого ячейки по верхнему краю',
  // TrvActionTableCellVAlignMiddle
  '&Центрировать по вертикали', 'Центрировать по вертикали|Выравнивание содержимого ячейки по центру',
  // TrvActionTableCellVAlignBottom
  'Выровнять по &нижнему краю', 'Выровнять по нижнему краю|Выравнивание содержимого ячейки по нижнему краю',
  // TrvActionTableCellVAlignDefault
  'Выравнивание по &умолчанию', 'Выравнивание по умолчанию|Установить вертикальное выравнивание по умолчанию',
  // TrvActionTableCellRotationNone
  '&Без вращения', 'Без вращения|Не поворачивать содержимое ячейки',
  // TrvActionTableCellRotation90
  'Повернуть ячейку на &90°', 'Повернуть ячейку на 90°|Поворот содержимого ячейки на 90°',
  // TrvActionTableCellRotation180
  'Повернуть ячейку на &180°', 'Повернуть ячейку на 180°|Поворот содержимого ячейки на 180°',
  // TrvActionTableCellRotation270
  'Повернуть ячейку на &270°', 'Повернуть ячейку на 270°|Поворот содержимого ячейки на 270°',
  // TrvActionTableProperties
  'Сво&йства таблицы...', 'Свойства таблицы|Задание свойств таблицы',
  // TrvActionTableGrid
  'С&етка', 'Сетка|Показ или скрытие сетки на месте невидимых рамок во всех таблицах',
  // TRVActionTableSplit
  '&Разбить таблицу', 'Разбить таблицу|Разбиение таблицы на две части, начиная с выделенной строки',
  // TRVActionTableToText
  'Преобразовать в &текст...', 'Преобразовать в текст|Преобразование таблицы в обычный текст',
  // TRVActionTableSort
  '&Сортировка...', 'Сортировка|Сортировка строк таблицы',
  // TrvActionTableCellLeftBorder
  '&Левая граница', 'Левая граница|Показ или скрытие левой границы выделенных ячеек',
  // TrvActionTableCellRightBorder
  '&Правая граница', 'Правая граница|Показ или скрытие правой границы выделенных ячеек',
  // TrvActionTableCellTopBorder
  '&Верхняя граница', 'Верхняя граница|Показ или скрытие верхней границы выделенных ячеек',
  // TrvActionTableCellBottomBorder
  '&Нижняя граница', 'Нижняя граница|Показ или скрытие нижней границы выделенных ячеек',
  // TrvActionTableCellAllBorders
  'В&се границы', 'Все границы|Показ или скрытие всех границ выделенных ячеек',
  // TrvActionTableCellNoBorders
  '&Нет границы', 'Нет границы|Скрытие всех границ выделенных ячеек',
  // TrvActionFonts & TrvActionFontEx
  '&Шрифт...', 'Шрифт|Изменения параметров шрифта и расстояния между символами в выделенном тексте',
  // TrvActionFontBold
  'Полу&жирный', 'Полужирный|Оформление выделенного текста полужирным шрифтом',
  // TrvActionFontItalic
  '&Курсив', 'Курсив|Оформление выделенного текста курсивом',
  // TrvActionFontUnderline
  'Под&чёркнутый', 'Подчёркнутый|Подчёркивание выделенного текста',
  // TrvActionFontStrikeout
  '&Зачёркнутый', 'Зачёркнутый|Зачёркивание выделенного текста',
  // TrvActionFontGrow
  'У&величить размер', 'Увеличить размер|Увеличение размера шрифта выделенного текста на 10%',
  // TrvActionFontShrink
  'У&меньшить размер', 'Уменьшить размер|Уменьшение размера шрифта выделенного текста на 10%',
  // TrvActionFontGrowOnePoint
  'Уве&личить размер на 1 пт', 'Увеличить размер на 1 пт|Увеличение размера шрифта выделенного текста на 1 пункт',
  // TrvActionFontShrinkOnePoint
  'Уме&ньшить размер на 1 пт', 'Уменьшить размер на 1 пт|Уменьшение размера шрифта выделенного текста на 1 пункт',
  // TrvActionFontAllCaps
  'Все &прописные', 'Все прописные|Показ всех букв выделенного текста как прописных',
  // TrvActionFontOverline
  'Вер&хняя черта', 'Верхняя черта|Добавление горизонтальной черты над выделенным текстом',
  // TrvActionFontColor
  '&Цвет текста...', 'Цвет текста|Изменение цвета выделенного текста',
  // TrvActionFontBackColor
  'Цвет &фона текста...', 'Цвет фона текста|Изменение цвета фона выделенного текста',
  // TrvActionAddictSpell3
  '&Правописание', 'Правописание|Проверка орфографии',
  // TrvActionAddictThesaurus3
  '&Тезаурус', 'Тезаурус|Поиск синонимов',
  // TrvActionParaLTR
  'Слева направо', 'Слева направо|Установка направления текста слева направо для выделенных абзацев',
  // TrvActionParaRTL
  'Справа налево', 'Справа налево|Установка направления текста справа налево для выделенных абзацев',
  // TrvActionLTR
  'Текст слева направо', 'Текст слева направо|Установка направления выделенного текста слева направо',
  // TrvActionRTL
  'Текст справа налево', 'Текст справа налево|Установка направление выделенного текста справа налево',
  // TrvActionCharCase
  '&Регистр', 'Регистр|Изменение регистра букв выделенного текста',
  // TrvActionShowSpecialCharacters
  '&Непечатаемые знаки', 'Непечатаемые знаки|Показать или скрыть служебные знаки, такие как символ конца абзаца, пробелы и табуляции',
  // TrvActionSubscript
  'П&одстрочный знак', 'Подстрочный|Преобразование выделенных символов в нижние индексы',
  // TrvActionSuperscript
  'Н&адстрочный знак', 'Надстрочный|Преобразование выделенных символов в верхние индексы',
  // TrvActionInsertFootnote
  'С&носка', 'Сноска|Вставка сноски в текущую позицию документа',
  // TrvActionInsertEndnote
  '&Концевая сноска', 'Концевая сноска|Вставка концевой сноски  в текущую позицию документа',
  // TrvActionEditNote
  'Ре&дактировать сноску', 'Редактировать сноску|Редактирование текста текущей сноски',
  // TrvActionHide
  '&Скрыть', 'Скрыть|Скрытие или показ выделенного фрагмента документа',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Стили...', 'Стили|Открытие окна редактирования стилей',
  // TrvActionAddStyleTemplate
  'С&оздать стиль...', 'Создать стиль|Создание нового стиля на основе выделенного текста',
  // TrvActionClearFormat,
  '&Очистить формат', 'Очистить формат|Удаление любого форматирования из выделенного фрагмента',
  // TrvActionClearTextFormat,
  'Очистить формат &текста', 'Очистить формат текста|Удаление форматирования текста из выделенного фрагмента',
  // TrvActionStyleInspector
  '&Инспектор стилей', 'Инспектор стилей|Показ или скрытие окна «инспектора стилей»',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'ОК', 'Отмена', 'Закрыть', 'Вставить', '&Выбрать...', '&Сохранить...', '&Очистить', 'Справка', 'Удалить',
  // Others  -------------------------------------------------------------------
  '%',
  // left, top, right, bottom sides
  'Левая сторона', 'Верхняя сторона', 'Правая сторона', 'Нижняя сторона',
  // save changes? confirm title
  'Сохранить изменения в %s?', 'Подтверждение',
  // warning: losing formatting
  '%s может содержать форматирование, которое будет потерено при преобразовании в выбранный формат файла.'#13+
  'Сохранить документ в этом формате?',
  // RVF format name
  'Формат RichView',  
  // Error messages ------------------------------------------------------------
  'Ошибка',
  'Ошибка чтения файла.'#13#13'Возможные причины:'#13'- файл имеет формат, который не поддерживается приложением;'#13+
  '- файл повреждён;'#13'- файл открыт другим приложением, которое блокировало доступ к нему.',
  'Ошибка чтения файла с изображением.'#13#13'Возможные причины:'#13'- файл содержит изображение в формате, который не поддерживается приложением;'#13+
  '- файл не содержит изображения;'#13+
  '- файл повреждён;'#13'- файл открыт другим приложением, которое блокировало доступ к нему.',
  'Ошибка сохранения файла.'#13#13'Возможные причины:'#13'- нет места на диске;'#13+
  '- диск защищён от записи;'#13'- диск не вставлен в дисковод;'#13+
  '- файл открыт другим приложением, которое блокировало доступ к нему;'#13+
  '- носитель информации повреждён.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Файлы формата RichView (*.rvf)|*.rvf',  'Файлы RTF (*.rtf)|*.rtf' , 'Файлы XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Текстовые файлы (*.txt)|*.txt', 'Текстовые файлы - Юникод (*.txt)|*.txt', 'Текстовые файлы - авто (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Файлы HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - упрощённый (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Документы Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Поиск окончен', 'Строка ''%s'' не найдена.',
  // 1 string replaced; Several strings replaced
  'Произведено замен: 1.', 'Произведено замен: %d',
  // continue search from the beginning/end?
  'Достигнут конец документа. Продолжить поиск с начала?',
  'Достигнуто начало документа. Продолжить поиск с конца?',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  'Нет заливки', 'Авто',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Чёрный', 'Коричневый', 'Оливковый', 'Тёмно-зелёный',
  'Тёмно-сизый', 'Тёмно-синий', 'Индиго','Серый-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Тёмно-красный', 'Оранжевый', 'Тёмно-жёлтый', 'Зелёный',
  'Сине-зелёный', 'Синий', 'Сизый', 'Серый-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Красный', 'Светло-оранжевый', 'Травяной', 'Изумрудный',
  'Тёмно-бирюзовый', 'Тёмно-голубой', 'Фиолетовый', 'Серый-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Лиловый', 'Золотистый', 'Жёлтый', 'Ярко-зелёный',
  'Бирюзовый', 'Голубой', 'Вишнёвый', 'Серый-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Розовый', 'Светло-коричневый', 'Светло-жёлтый', 'Бледно-зелёный',
  'Светло-бирюзовый', 'Бледно-голубой', 'Сиреневый', 'Белый',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Нет з&аливки', '&Авто', '&Другие цвета...', '&Стандартный',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  'Фон', '&Цвет:', 'Размещение', 'Изображение', 'Образец текста.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center, 
  '&Нет', '&Растянуть', '&Жёсткая мозаика', '&Мозаика', 'В ц&ентре',
  // Padding button
  '&Поля...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  'Заливка', '&Применить к:', '&Другой цвет...', 'П&оля...',
  'Пожалуйста, выберите цвет',
  // [apply to:] text, paragraph, table, cell
  'тексту', 'абзацу' , 'таблице', 'ячейке',  
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Шрифт', 'Шрифт', 'Интервал',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Шрифт:', '&Размер', '&Начертание', 'Полу&жирный', '&Курсив',
  // Script, Color, Back color labels, Default charset
  'Набор &символов:', '&Цвет:', '&Фон:', '(По умолчанию)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Видоизменение', 'Подчёркивание', '&Черта сверху', '&Зачёркнутый', '&Все прописные',
  // Sample, Sample text
  'Образец', 'Образец текста',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Интервал', '&Интервал:', '&Разрежённый', '&Уплотнённый',
  // Offset group-bix, Offset label, Down, Up,
  'Смещение', '&Смещение:', 'В&низ', 'В&верх',
  // Scaling group-box, Scaling
  'Растяжение', '&Масштаб:',
  // Sub/super script group box, Normal, Sub, Super
  'Верхние и нижние индексы', '&Обычный', 'П&одстрочный', 'Н&адстрочный',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  'Поля', 'C&верху:', 'С&лева:', 'С&низу:', 'С&права:',
  // Equal values check-box
  '&Одинаковые значения',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Добавление гиперссылки', 'Гиперссылка', '&Текст:', '&URL:', '<<выделенный фрагмент>>',
  // cannot open URL
  'Ошибка открытия "%s"',
  // hyperlink properties button, hyperlink style
  '&Атрибуты...', '&Стиль:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) сolors
  'Атрибуты ссылки', 'Обычные цвета', 'Цвета подсветки',
  // Text [color], Background [color]
  '&Текст:', '&Фон:',
  // Underline [color]
  'По&дчёркивание:',
  // Text [color], Background [color] (different hotkeys)
  'Т&екст:', 'Ф&он:',
  // Underline [color], Attributes group-box,
  'Под&чёркивание:', 'Атрибуты',
  // Underline type: always, never, active (below the mouse)
  '&Подчёркивание:', 'всегда', 'никогда', 'при наведении',
  // Same as normal check-box
  'Т&акие же, как обычные',
  // underline active links check-box
  '&Подчёркивать при наведении',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Символ', '&Шрифт:', '&Набор символов:', 'Юникод',
  // Unicode block
  '&Диапазон:',  
  // Character Code, Character Unicode Code, No Character
  'Код символа: %d', 'Код символа: Юникод %d', '(символ не выбран)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows 
  'Вставка таблицы', 'Размер таблицы', '&Число столбцов:', 'Ч&исло строк:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Ширина таблицы', '&Авто', 'По ширине &окна', '&Указанная:',
  // Remember check-box
  'По умолчанию для новых &таблиц',
  // VAlign Form ---------------------------------------------------------------
  'Положение',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Size and position, Line, Table, Rows, Cells tabs
  'Свойства', 'Рисунок', 'Размер и положение', 'Линия', 'Таблица', 'Строки', 'Ячейки',
  // Image Appearance, Image Misc, Number tabs
  'Вид', 'Разное', 'Номер',
  // Box position, Box size, Box appearance tabs
  'Положение', 'Размер', 'Вид',
  // Preview label, Transparency group-box, checkbox, label
  'Просмотр:', 'Прозрачность', '&Прозрачный', 'Прозрачный &цвет:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  '&Выбрать...', '&Сохранить...', 'Авто',
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Выравнивание', 'Вы&ровнять:',
  'низ по базовой линии текста', 'центр по базовой линии текста',
  'по верху строки', 'по низу строки', 'по центру строки',
  // align to left side, align to right side
  'по левому краю', 'по правому краю',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Сместить на:', 'Размер', '&Ширина:', '&Высота:', 'Исходный размер: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Со&хранять соотношение сторон', 'С&брос',
  // Inside the border, border, outside the border groupboxes
  'Внутри рамки', 'Рамка', 'Вокруг рамки',
  // Fill color, Padding (i.e. margins inside border) labels
  '&Цвет фона:', '&Поля:',
  // Border width, Border color labels
  '&Ширина рамки:', 'Цвет &рамки:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Отступы по &горизонтали:', 'Отступы по &вертикали:',
  // Miscellaneous groupbox, Tooltip label
  'Разное', '&Подсказка:',
  // web group-box, alt text
  'Веб', '&Замещающий текст:',
  // Horz line group-box, color, width, style
  'Горизонтальная линия', '&Цвет:', '&Толщина:', '&Стиль:',
  // Table group-box; Table Width, Color, CellSpacing
  'Таблица', '&Ширина:', '&Цвет заливки:', '&Интервалы между ячейками...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Горизонтальные поля ячеек:', '&Вертикальные поля ячеек:', 'Рамка и заливка',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Видимые с&тороны рамки...', 'Авто', 'авто',
  // Keep row content together checkbox
  'Н&е разрывать содержимое строк',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Вращение', 'Н&ет', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Рамки:', 'Видимые стороны &рамки...',
  // Border, CellBorders buttons
  '&Рамка таблицы...', 'Рамки &ячеек...',
  // Table border dialog title
  'Рамка таблицы', 'Рамки ячеек по умолчанию',  
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Печать', 'Запретить перенос &строк на следующую страницу', 'Число строк в &заголовке:',
  'Строки заголовка будут напечатаны на каждой странице с таблицей',
  // top, center, bottom, default
  'С&верху', 'По &центру', 'С&низу', 'По &умолчанию',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Параметры ячеек', 'Примерная &ширина:', 'В&ысота не менее:', 'Цвет &заливки:',
  // Cell Border group-box, VisibleSides; shadow, light, border colors
  'Рамка', 'Видимые стороны:', 'Т&ёмный цвет:', '&Светлый цвет', 'Цве&т:',
  // Background image
  'Рисун&ок...',  
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'По горизонтали', 'По вертикали', 'Положение в тексте',
  // Position types: align, absolute, relative
  'Выровнять', 'Положение', 'Относительное положение',
  // [Align] left side, center, right side
  'левую сторону по левой стороне',
  'середину по середине',
  'правую сторону по правой стороне',
  // [Align] top side, center, bottom side
  'верх по верху',
  'середину по середине',
  'низ по низу',
  // [Align] relative to
  'относительно:',
  // [Position] to the right of the left side of
  'вправо от левого края:',
  // [Position] below the top side of
  'ниже верхнего края:',
  // Anchors: page, main text area (x2)
  '&Страницы', '&Области текста', 'Страни&цы', 'О&бласти текста',
  // Anchors: left margin, right margin
  '&Левого поля', '&Правого поля',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Вер&хнего поля', '&Нижнего поля', 'Вн&утреннего поля', 'Вн&ешнего поля',
  // Anchors: character, line, paragraph
  '&Знака', 'С&троки', '&Абзаца',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Зер&кальные поля', '&Относительно ячейки таблицы',
  // Above text, below text
  '&Перед текстом', '&За текстом',
  // Width, Height groupboxes, Width, Height labels
  'Ширина', 'Высота', '&Ширина:', '&Высота:',
  // Auto height label, Auto height combobox item
  'Авто', 'авто',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Вертикальное выравнивание', 'С&верху', 'По &центру', 'С&низу',
  // Border and background button and title
  '&Рамка и заливка..', 'Рамка и заливка текстового поля',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Специальная вставка', '&Как:', 'Формат RTF', 'Формат HTML',
  'Неформатированный текст', 'Текст в кодировке Юникод', 'Точечный рисунок (BMP)', 'Метафайл (WMF)',
  'Файлы с рисунками', 'URL',
  // Options group-box, styles
  'Параметры', '&Стили:',
  // style options: apply target styles, use source styles, ignore styles
  'применить стили из редактируемого документа', 'сохранить стили и внешний вид текста',
  'сохранить внешний вид, игнорировать стили',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Список', 'Маркированный', 'Нумерованный', 'Маркированный список', 'Нумерованный список',
  // Customize, Reset, None
  '&Изменить...', '&Сброс', 'Нет',
  // Numbering: continue, reset to, create new
  'продолжить нумерацию', 'начать нумерацию в списке с ', 'создать новый список, начиная с',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Прописные буквы', 'Прописные римские', 'Десятичные', 'Строчные буквы', 'Строчные римские',
  // Bullet type
  'Окружность', 'Круг', 'Квадрат',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Уровень:', '&Начать с:', '&Продолжить', 'Нумерация',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  'Изменение списка', 'Уровни', '&Число:', 'Свойства списка', '&Тип списка:',
  // List types: bullet, image
  'маркер', 'рисунок', 'Вставить номер|', 
  // Number format, Number, Start level from, Font button
  '&Формат номера:', 'Номер', '&Нумерация уровня с:', '&Шрифт...',
  // Image button, bullet character, Bullet button,
  '&Рисунок...', '&Знак маркера', 'З&нак...',
  // Position of list text, bullet, number, image
  'Положение маркера', 'Положение маркера', 'Положение номера', 'Положение рисунка', 'Положение текста',
  // at, left indent, first line indent, from left indent
  '', '&Отступ слева:', '&Первая строка:', 'от левого отступа',
  // [only] one level preview, preview
  '&Предварительный просмотр только одного уровня', 'Образец',
  // Preview text  
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]  
  'справа от', 'слева от', 'в центре',
  // level #, this level (for combo-box)
  'Уровень %d', 'Этот уровень',
  // Marker character dialog title
  'Выбор знака маркера',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Рамка и заливка абзаца', 'Рамка', 'Заливка',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Вид рамки', '&Цвет:', 'Тол&щина:', '&Зазор:', '&Отступы...',
  // Sample, Border type group-boxes;
  'Образец', 'Тип рамки',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Нет', 'Од&инарная', '&Двойная', '&Тройная', 'Толстая &внутри', 'Толстая &снаружи',
  // Fill color group-box; More colors, padding buttons
  'Цвет заливки', '&Другой цвет...', '&Поля...',
  // preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // title and group-box in Offsets dialog
  'Отступы', 'Отступы от текста до рамки',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Абзац', 'Выравнивание', '&Левое', '&Правое', 'По &центру', 'По &ширине',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Интервалы', 'П&еред:', 'П&осле:', '&Междустрочный:', '&Значение:',
  // Indents group-box; indents: left, right, first line
  'Отступы', 'Сле&ва:', 'Сп&рава:', 'Перв&ая строка:',
  // indented, hanging
  'О&тступ', 'В&ыступ', 'Образец',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Одинарный', 'Полуторный', 'Двойной', 'Минимум', 'Точно', 'Множитель',
  // preview text
  'Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст. Текст текст текст текст текст.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Отступы и интервалы', 'Табуляция', 'Положение на странице',
  // tab stop position label; buttons: set, delete, delete all
  'По&зиция табуляции:', '&Установить', 'Уд&алить', 'Удалит&ь все',
  // tab align group; left, right, center aligns,
  'Выравнивание', '&Левое', '&Правое', 'По &центру',
  // leader radio-group; no leader
  'Заполнитель', '(Нет)',
  // tab stops to be deleted, delete none, delete all labels
  'Будут удалены:', '', 'Все.',
  // Pagination group-box, keep with next, keep lines together
  'Разбивка на страницы', 'Не &отрывать от следующего', 'Не &разрывать абзац',
  // Outline level, Body text, Level #
  '&Уровень:', 'Основной текст', 'Уровень %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Предварительный просмотр', 'По ширине страницы', 'Страница целиком', 'Страницы:',
  // Invalid Scale, [page #] of #
  'Пожалуйста, введите число от 10 до 500', 'из %d',
  // Hints on buttons: first, prior, next, last pages
  'К первой', 'К предыдущей', 'К следующей', 'К последней',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Интервалы', 'Интервалы', '&Между ячейками', 'Между рамкой таблицы и ячейками',
  // vertical, horizontal (x2)
  '&Вертикальный:', '&Горизонтальный:', 'В&ертикальный:', 'Го&ризонтальный:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Рамки', 'Параметры', '&Цвет:', '&Светлый цвет:', 'Тёмный &цвет:',
  // Width; Border type group-box;
  '&Толщина:', 'Тип рамки',
  // Border types: none, sunken, raised, flat
  '&Нет', '&Вдавленная', 'В&ыпуклая', '&Плоская',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Разбиение ячеек', 'Разбить на',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Указанное число строк и столбцов', '&Исходные ячейки',
  // number of columns, rows, merge before
  'Число ст&олбцов:', 'Число ст&рок:', '&Объединить перед разбиением',
  // to original cols, rows check-boxes
  'Разбить на исходные сто&лбцы', 'Разбить на исходные стро&ки',
  // Add Rows form -------------------------------------------------------------
  'Добавление строк', '&Число строк:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Параметры страницы', 'Страница', 'Колонтитулы',
  // margins group-box, left, right, top, bottom
  'Поля (миллиметры)', 'Поля (дюймы)', '&Левое:', '&Верхнее:', '&Правое:', '&Нижнее:',
  // mirror margins check-box
  '&Зеркальные поля',
  // orientation group-box, portrait, landscape
  'Ориентация', '&Книжная', '&Альбомная',
  // paper group-box, paper size, default paper source
  'Бумага', '&Размер:', 'П&одача:',
  // header group-box, print on the first page, font button
  'Верхний колонтитул', '&Текст:', '&Печатать на первой странице', '&Шрифт...',
  // header alignments: left, center, right
  'С&лева', 'В &центре', 'Сп&рава',
  // the same for footer (different hotkeys)
  'Нижний колонтитул', 'Т&екст:', 'Пе&чатать на первой странице', 'Шр&ифт...',
  '&Слева', '&В центре', 'Спр&ава',
  // page numbers group-box, start from
  'Номера страниц', '&Начать с:',
  // hint about codes
  'Вы можете использовать специальные сочетания:'#13'&&p - номер страницы; &&P - число страниц; &&d - дата; &&t - время.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Кодировка текстового файла', '&Выберите кодировку текста:',
  // thai, japanese, chinese (simpl), korean
  'Тайская', 'Японская', 'Китайская (упрощённая)', 'Корейская',
  // chinese (trad), central european, cyrillic, west european
  'Китайская (традиционная)', 'Центральноевропейская', 'Кириллица', 'Западноевропейская',
  // greek, turkish, hebrew, arabic
  'Греческая', 'Турецкая', 'Иврит', 'Арабская',
  // baltic, vietnamese, utf-8, utf-16
  'Балтийская', 'Вьетнамская', 'Юникод (UTF-8)', 'Юникод (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Оформление', '&Выберите стиль оформления:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Преобразование в текст', 'Выберите &разделитель:',
  // line break, tab, ';', ','
  'разрыв строки', 'знак табуляции', 'точка с запятой', 'запятая',
  // Table sort form -----------------------------------------------------------
  // error message
  'Невозможно выполнить сортировку таблицы, содержащей объединённые ячейки',
  // title, main options groupbox
  'Сортировка', 'Сортировка',
  // sort by column, case sensitive
  'Упорядочить по &столбцу:', 'учитывать &регистр букв',
  // heading row, range or rows
  'Исключить строку &заголовка', 'Строки для сортировки: с %d по %d',
  // order, ascending, descending
  'Упорядочить', 'по &возрастанию', 'по &убыванию',
  // data type, text, number
  'Тип', '&текст', '&числа',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Номер', 'Свойства', 'Название &счётчика:', '&Тип нумерации:',
  // numbering groupbox, continue, start from
  'Нумерация', '&Продолжить', '&Начать с:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Название', '&Подпись:', '&Исключить подпись из названия',
  // position radiogroup
  'Положение', '&Над выделенным объектом', 'П&од выделенным объектом',
  // caption text
  'Т&екст названия:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Нумерация', 'Рисунок', 'Таблица',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Видимые стороны рамки', 'Рамка',
  // Left, Top, Right, Bottom checkboxes
  '&Левая сторона', '&Верхняя сторона', '&Правая сторона', '&Нижнаяя сторона',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Изменение ширины столбца таблицы', 'Изменение высоты строки таблицы',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Отступ первой строки', 'Отступ слева', 'Выступ', 'Отступ справа',
  // Hints on lists: up one level (left), down one level (right)
  'Уменьшить уровень списка', 'Увеличить уровень списка',
  // Hints for margins: bottom, left, right and top
  'Нижнее поле', 'Левое поле', 'Правое поле', 'Верхнее поле',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'По левому краю', 'По правому краю', 'По центру', 'По разделителю',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Обычный', 'Обычный отступ', 'Без интервала', 'Заголовок %d', 'Абзац списка',
  // Hyperlink, Title, Subtitle
  'Гиперссылка', 'Название', 'Подзаголовок',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Выделение', 'Слабое выделение', 'Сильное выделение', 'Строгий',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Цитата 2', 'Выделенная цитата', 'Слабая ссылка', 'Сильная ссылка',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Цитата', 'Переменный HTML', 'Код HTML', 'Акроним HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Определение HTML', 'Клавиатура HTML', 'Образец HTML', 'Пишущая машинка HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Стандартный HTML', 'Цитата HTML', 'Верхний колонтитул', 'Нижний колонтитул',
  'Номер страницы',
  // Caption
  'Название объекта',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Знак концевой сноски', 'Знак сноски', 'Текст концевой сноски', 'Текст сноски',
  // Sidenote Reference, Sidenote Text
  'Знак сноски в рамке', 'Текст сноски в рамке',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'цвет', 'цвет фона', 'прозрачный', 'обычный', 'цвет подчёркивания',
  // default background color, default text color, [color] same as text
  'обычный цвет фона', 'обычный цвет текста', 'как у текста',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'обычное', 'жирное', 'двойное', 'пунктирное', 'жирное пунктирное', 'штриховое',
  // underline types: thick dashed, long dashed, thick long dashed,
  'жирное штриховое', 'длинное штриховое', 'жирное длинное штриховое',
  // underline types: dash dotted, thick dash dotted,
  'штрихпунктирное', 'жирное штрихпунктирное',
  // underline types: dash dot dotted, thick dash dot dotted
  'штрихпунктирное с 2 точками', 'жирное штрихпунктирное с 2 точками',
  // sub/superscript: not, subsript, superscript
  'не надстрочные/подстрочные', 'подстрочные', 'надстрочные',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'направление текста', 'по умолчанию', 'слева направо', 'справа налево',
  // bold, not bold
  'полужирный', 'не полужирный',
  // italic, not italic
  'курсив', 'не курсив',
  // underlined, not underlined, default underline
  'подчёркивание', 'не подчёркнутый', 'подчёркивание по умолчанию',
  // struck out, not struck out
  'зачёркнутый', 'не зачёркнутый',
  // overlined, not overlined
  'черта сверху', 'без черты сверху',
  // all capitals: yes, all capitals: no
  'все прописные', 'не все прописные',
  // vertical shift: none, by x% up, by x% down
  'без вертикального смещения', 'сдвинут на %d%% вверх', 'сдвинут на %d%% вниз',
  // characters width [: n%]
  'ширина букв',
  // character spacing: none, expanded by x, condensed by x
  'обычное межсимвольное расстояние', 'разрежённый на %s', 'уплотнённый на %s',
  // charset
  'набор символов',
  // default font, default paragraph, inherited attibutes [of font, paragraph attributes, etc.]
  'обычный шрифт', 'обычный абзац', 'унаследованные свойства',
  // [hyperlink] highlight, default hyperlink attributes
  'подсветка', 'обычные свойства',
  // paragraph aligmnment: left, right, center, justify
  'по левому краю', 'по правому краю', 'по центру', 'по ширине',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'высота строки: %d%%', 'расстояние между строками: %s', 'высота строки: не менее %s',
  'высота строки: ровно %s',
  // no wrap, wrap
  'запретить перенос строк', 'разрешить перенос строк',
  // keep lines together: yes, no
  'не разрывать абзац', 'разрешить разрывать абзац',
  // keep with next: yes, no
  'не отрывать от следующего абзаца', 'разрешить отрывать от следующего абзаца',
  // read only: yes, no
  'запретить редактирование', 'разрешить редактирование',
  // body text, heading level x
  'уровень структуры: основной текст', 'уровень структуры: %d',
  // indents: first line, left, right
  'отступ первой строки', 'отступ слева', 'отступ справа',
  // space before, after
  'интервал перед', 'интервал после',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'табуляции', 'нет', 'выравнивание', 'по левому краю', 'по правому краю', 'по центру',
  // tab leader (filling character)
  'заполнитель',
  // no, yes,
  'нет', 'да',
  // [padding/spacing/side:] left, top, right, bottom
  'слева', 'сверху', 'справа', 'снизу',
  // border: none, single, double, triple, thick inside, thick outside
  'нет', 'одинарная', 'двойная', 'тройная', 'толстая внутри', 'толстая снаружи',
  // background, border, padding, spacing [between text and border]
  'заливка', 'рамка', 'поля', 'отступы',
  // border: style, width, internal width, visible sides
  'стиль', 'толщина', 'зазор', 'стороны',
  // style inspector -----------------------------------------------------------
  // title
  'Инспектор стилей',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Стиль', '(нет)', 'Абзац', 'Шрифт', 'Свойства',
  // border and background, hyperlink, standard [style]
  'Рамка и заливка', 'Гиперссылка', '(стандартный)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Стили', 'Стиль',
  // name, applicable to,
  '&Название:', '&Применим к:',
  // based on, based on (no &),
  '&Основан на стиле:', 'Основан на стиле:',
  //  next style, next style (no &)
  'Стиль &следующего абзаца:', 'Стиль следующего абзаца:',
  // quick access check-box, description and preview
  '&Быстрый доступ', 'Описание и п&росмотр:',
  // [applicable to:] paragraph and text, paragraph, text
  'абзацам и тексту', 'абзацам', 'тексту',
  // text and paragraph styles, default font
  'Стили текста и абзацев', 'Основной шрифт',
  // links: edit, reset, buttons: add, delete
  'Изменить', 'Сбросить', '&Добавить', '&Удалить',
  // add standard style, add custom style, default style name
  'Добавить &стандартный стиль...', '&Добавить стиль', 'Стиль %d',
  // choose style
  'Выберите &стиль:',
  // import, export,
  '&Импорт...', '&Экспорт...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'Стили RichView (*.rvst)|*.rvst', 'Ошибка чтения файла', 'В выбранном файле отсутствуют стили',
  // Title, group-box, import styles
  'Импорт стилей из файла', 'Импорт', '&Импортировать стили:',
  // existing styles radio-group: Title, override, auto-rename
  'Уже имеющиеся стили', '&заменить новыми', '&добавить с новыми названиями',
  // Select, Unselect, Invert,
  '&Выбрать', '&Очистить', 'О&братить',
  // select/unselect: all styles, new styles, existing styles
  '&Все стили', '&Новые стили', '&Уже имеющиеся стили',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Очистить формат', 'Все стили',
  // dialog title, prompt
  'Стили', '&Выберите стиль:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Синонимы', '&Пропустить все', '&Добавить в словарь',
  // Progress messages ---------------------------------------------------------
  'Скачивается %s', 'Идёт подготовка к печати...',
  'Печатается страница %d',
  'Преобразование из RTF...',  'Преобразование в RTF...',
  'Чтение: %s...', 'Запись: %s...', 'текст',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Нет сноски', 'Сноска', 'Концевая сноска', 'Сноска в рамке', 'Текстовое поле'
  );


initialization
  RVA_RegisterLanguage(
    'Russian',
    'Русский',
    RUSSIAN_CHARSET, @Messages);

end.
