//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop
USEFORMNS("TableBackRVFrm.pas", Tablebackrvfrm, frmRVTableBack);
USEFORMNS("TableBrdrRVFrm.pas", Tablebrdrrvfrm, frmRVTableBrdr);
USEFORMNS("TableSizeRVFrm.pas", Tablesizervfrm, frmRVTableSize);
USEFORMNS("StylesRVFrm.pas", Stylesrvfrm, frmRVStyles);
USEFORMNS("SpacingRVFrm.pas", Spacingrvfrm, frmRVSpacing);
USEFORMNS("SplitRVFrm.pas", Splitrvfrm, frmRVSplit);
USEFORMNS("StyleImportRVFrm.pas", Styleimportrvfrm, frmRVStyleImport);
USEFORMNS("VisibleSidesRVFrm.pas", Visiblesidesrvfrm, frmRVVisibleSides);
USEFORMNS("TableSortRVFrm.pas", Tablesortrvfrm, frmRVTableSort);
USEFORMNS("ListGallery2RVFrm.pas", Listgallery2rvfrm, frmRVListGallery2);
USEFORMNS("ListGalleryRVFrm.pas", Listgalleryrvfrm, frmRVListGallery);
USEFORMNS("ListRVFrm.pas", Listrvfrm, frmRVList);
USEFORMNS("ItemPropRVFrm.pas", Itemproprvfrm, frmRVItemProp);
USEFORMNS("InsTableRVFrm.pas", Instablervfrm, frmRVInsTable);
USEFORMNS("IntegerRVFrm.pas", Integerrvfrm, frmRVInteger);
USEFORMNS("PageSetupRVFrm.pas", Pagesetuprvfrm, frmRVPageSetup);
USEFORMNS("ParaBrdrRVFrm.pas", Parabrdrrvfrm, frmRVParaBrdr);
USEFORMNS("ParaListRVFrm.pas", Paralistrvfrm, frmRVParaList);
USEFORMNS("ObjectAlignRVFrm.pas", Objectalignrvfrm, frmRVObjectAlign);
USEFORMNS("NumSeqCustomRVFrm.pas", Numseqcustomrvfrm, frmRVCustomNumSeq);
USEFORMNS("NumSeqRVFrm.pas", Numseqrvfrm, frmRVNumSeq);
USEFORMNS("dmActions.pas", Dmactions, rvActionsResource); /* TDataModule: File Type */
USEFORMNS("FillColorRVFrm.pas", Fillcolorrvfrm, frmRVFillColor);
USEFORMNS("ColorRVFrm.pas", Colorrvfrm, frmColor);
USEFORMNS("BaseRVFrm.pas", Baservfrm, frmRVBase);
USEFORMNS("BorderSidesRVFrm.pas", Bordersidesrvfrm, frmRVBorderSides);
USEFORMNS("HypRVFrm.pas", Hyprvfrm, frmRVHyp);
USEFORMNS("InfoRVFrm.pas", Inforvfrm, frmRVInfo);
USEFORMNS("InsSymbolRVFrm.pas", Inssymbolrvfrm, frmRVInsertSymbol);
USEFORMNS("HypPropRVFrm.pas", Hypproprvfrm, frmRVHypProp);
USEFORMNS("FontRVFrm.pas", Fontrvfrm, frmRVFont);
USEFORMNS("FourSidesRVFrm.pas", Foursidesrvfrm, frmRVFourSides);
USEFORMNS("HypHoverPropRVFrm.pas", Hyphoverproprvfrm, frmRVHypHoverProp);
USEFORMNS("ParaRVFrm.pas", Pararvfrm, frmRVPara);
USEFORMNS("PasteSpecialRVFrm.pas", Pastespecialrvfrm, frmRVPasteSpecial);
USEFORMNS("PreviewRVFrm.pas", Previewrvfrm, frmRVPreview);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------


#pragma argsused
extern "C" int _libmain(unsigned long reason)
{
	return 1;
}
//---------------------------------------------------------------------------
