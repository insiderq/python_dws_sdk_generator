
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Design-time support for TRVColorGrid and        }
{       TRVColorCombo                                   }
{                                                       }
{       Copyright (c) 2002-2003, Sergey Tkachenko       }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit RVColorReg;

interface
uses Classes, RVGrids, RVColorGrid, RVColorCombo;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('RichView Misc', [TRVGrid, TRVColorGrid, TRVColorCombo]);
end;

end.
