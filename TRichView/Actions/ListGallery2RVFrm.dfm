�
 TFRMRVLISTGALLERY2 0  TPF0�TfrmRVListGallery2frmRVListGallery2Left[Top� HelpContext@d ActiveControlrg1BorderStylebsDialogCaptionBullets and NumberingClientHeight�ClientWidth�OldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TLabellblLevelLeftTopyWidthHeightAnchorsakLeftakBottom Caption&Level:FocusControlseLevel  TPageControlpcLeft	TopWidth�Heighte
ActivePage	tsBulletsTabOrder  	TTabSheet	tsBulletsCaptionBullets TRVOfficeRadioGrouprg1LeftTopWidthGHeight7	ItemIndex ColumnsItems
ImageIndex Captionnone 
ImageIndexCaptiondisc 
ImageIndexCaptioncircle 
ImageIndexCaptionsquare  Imagesil1OnDblClickItemrg2DblClickItemCaption	List TypeTabOrder    	TTabSheettsNumberingCaption	Numbering
ImageIndex 	TGroupBoxgbNumberingLeftRTopWidthrHeight7Caption Numbering TabOrder TRVSpinEditseStartFromLeft'TopFWidth9Height	Increment       ��?MaxValue       �@Value       ��?TabOrderOnChangeseStartFromChange  TRadioButton
rbContinueLeft
TopWidthWHeightCaption	&ContinueChecked	TabOrder TabStop	  TRadioButtonrbStartFromLeft
Top1WidthWHeightCaption&Start from:TabOrder   TRVOfficeRadioGrouprg2LeftTopWidthGHeight7	ItemIndex ColumnsItems
ImageIndex Captionnone 
ImageIndexCaptiondecimal 
ImageIndexCaptionupper alpha 
ImageIndexCaptionlower alpha 
ImageIndexCaptionupper roman 
ImageIndexCaptionlower roman  Imagesil1OnDblClickItemrg2DblClickItemCaption	List TypeTabOrder     TRVSpinEditseLevelLeftsToptWidth9Height	Increment       ��?MaxValue       �@Value       ��?AnchorsakLeftakBottom TabOrder  TButtonbtnOkLeftTopvWidthdHeightAnchorsakRightakBottom CaptionOKDefault	ModalResultTabOrder  TButton	btnCancelLeftTopvWidthdHeightAnchorsakRightakBottom Cancel	CaptionCancelModalResultTabOrder  
TImageListil1HeightbWidthZLeft� Topm   