﻿// This file is a copy of RVAL_Cat.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Catala (SP) translation                         }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by: Josep Mas i Dedeu, 2008-03-13          }
{                bep@xci.cat                            }
{ Updated, 2014-02-11                                   }
{*******************************************************}

unit RVALUTF8_Cat;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'polzades', 'cm', 'mm', 'piques', 'píxels', 'punts',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fitxer', '&Edició', 'F&ormat', 'Fo&nt', '&Paràgraf', '&Insereix', '&Taula', 'Fine&stra', '&Ajuda',
  // exit
  '&Surt',
  // top-level menus: View, Tools,
  '&Veure', 'Eines',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Mida', 'Estil', 'Selecciona', 'Ali&neació Contingut Cel·la', 'Contorns C&el·la',
  // menus: Table cell rotation
  '&Rotació de cel·les',
  // menus: Text flow, Footnotes/endnotes
  '&Flux de &text', '&Notes al peu',
  // ribbon tabs: tab1, tab2, view, table
  '&Inici', '&Avançat', '&Vista', '&Taula',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'porta-retalls', 'Font', 'Paràgraf', 'Llista', 'Edició',
  // ribbon groups: insert, background, page setup,
  'Insertar', 'Fons', 'Configurar pàgina',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Enllaços', 'Notes al peu', 'Vistes de document', 'Mostrar/Ocultar', 'Zoom', 'Opcions',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Insertar', 'Eliminar', 'Operacions', 'Contorns',
  // ribbon groups: styles 
  'Estils',
  // ribbon screen tip footer,
  'Prem F1 per a més ajuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Documents recents', 'Guardar una còpia del document', 'Vista prèvia i impresió del document',
  // ribbon label: units combo
  'Unitats:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Nou', 'Nou|Crea un nou document en blanc',
  // TrvActionOpen
  '&Obrir...', 'Obrir|Obra un document de disc',
  // TrvActionSave
  '&Guardar', 'Guardar|Guarda el document a disc',
  // TrvActionSaveAs
  'Guardar &Com...', 'Guardar Com...|Guarda el document a disc amb un nom nou',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta el document a un altre format',
  // TrvActionPrintPreview
  '&Vista Preliminar', 'Visualitza la impressió|Mostra com s''imprimirà el document',
  // TrvActionPrint
  'Im&primeix...', 'Imprimeix|Estableix els paràmetres d''impressió i imprimeix el document',
  // TrvActionQuickPrint
  '&Imprimir', '&Impressió ràpida', 'Imprimir|Imprimeix el document',
  // TrvActionPageSetup
  'Configu&ració de la pàgina...', 'Configuració de la pàgina|Definir marges, Mida del paper, Orientació, Font, capçaleres i peus de pàgina',
  // TrvActionCut
  'Re&talla', 'Retalla|Retalla la selecció i posa-la al porta-retalls',
  // TrvActionCopy
  '&Copia', 'Copia|Copia la selecció i posa-la al porta-retalls',
  // TrvActionPaste
  '&Enganxa', 'Enganxa|Enganxa els continguts del porta-retalls',
  // TrvActionPasteAsText
  'Enganxa com a &text', 'Enganxa com a text|Inserta el text del porta-retalls',
  // TrvActionPasteSpecial
  'Enganxament &Especial...', 'Enganxament especial|Enganxa els continguts del porta-retalls en el format especificat',
  // TrvActionSelectAll
  'Seleccionar &Tot', 'Seleccionar tot|Seleccionar tot el document',
  // TrvActionUndo
  '&Desfés', 'Desfés|Torna a la darrera acció',
  // TrvActionRedo
  '&Refés', 'Refés|Refés la darrera acció desfeta',
  // TrvActionFind
  '&Cerca...', 'Cerca|Cerca el text en el document',
  // TrvActionFindNext
  'Cerca el &següe&nt', 'Cerca el &següent|Continua l''últim cerca',
  // TrvActionReplace
  '&Reemplaça...', 'Reemplaça|Cerca i ree&mplaça el text al document',
  // TrvActionInsertFile
  '&Fitxer...', 'Insereix un fitxer|Insereix el contingut del fitxer al document',
  // TrvActionInsertPicture
  '&Imatge...', 'Insereix una imatge|Insereix una imatge del disc',
  // TRVActionInsertHLine
  '&Línia horitzontal', 'Insereix una línia horitzontal|Insereix una línia horitzontal',
  // TRVActionInsertHyperlink
  'Hiperen&llaç...', 'Insereix un Hiperenllaç|Insereix un enllaç d''hypertext',
  // TRVActionRemoveHyperlinks
  '&Eliminar hiperenllaç', 'Eliminar hiperenllaç|Elimina tots els hiperenllaços al text seleccionat',
  // TrvActionInsertSymbol
  '&Símbol...', 'Insereix un símbol|Insereix un símbol',
  // TrvActionInsertNumber
  '&Número...', 'Insereix un Número|Insereix un número',
  // TrvActionInsertCaption
  'Inserí Títol...', 'Insereix un Títol|Insereix un títol per a l''objecte seleccionat',
  // TrvActionInsertTextBox
  'Quadre &Text', 'Insereix un Quadre de Text|Insereix un quadre de text',
  // TrvActionInsertSidenote
  'Nota dins el marc', 'Insereix Nota dins el marc|Insereix una nota que apareix en un marc de text',
  // TrvActionInsertPageNumber
  'Número &Pàgina', 'Insereix el Número de Pàgina|Insereix el número de pàgina',
  // TrvActionParaList
  '&Pics i numeració...', 'Pics i numeració|Aplica o edita els pics o numeració al paràgraf seleccionat',
  // TrvActionParaBullets
  '&Vinyetes', 'Vinyetes|Afegeix o elimina vinyetes del paràgraf',
  // TrvActionParaNumbering
  '&Numeració', 'Numeració|Afegeix o elimina numeració del paràgraf',
  // TrvActionColor
  '&Color de Fons...', 'Fons|Canvia el color de fons del document',
  // TrvActionFillColor
  'Colo&r per emplenar...', 'Color per emplenar|Canvia el color de fons de  text, paràgrafs, cel·la, taula o document',
  // TrvActionInsertPageBreak
  '&Insereix salt de pàgina', 'Insereix salt de pàgina|Insereix un salt de pàgina',
  // TrvActionRemovePageBreak
  '&Elimina salt de pàgina', 'Elimina salt de pàgina|Elimina el salt de pàgina',
  // TrvActionClearLeft
  'Neteja el flux de text a l''esquerra', 'Neteja el flux de text a l''esquerra|Posiciona aquest paràgraf sota de qualsevol imatge alineada a l''esquerra',
  // TrvActionClearRight
  'Neteja el flux de text a la dreta', 'Neteja el flux de text a la  dreta|Posiciona aquest paràgraf sota de qualsevol imatge alineada a la dreta',
  // TrvActionClearBoth
  'Neteja el flux de text a ambdós costats', 'Neteja el flux de text ambdós costats|Posiciona aquest paràgraf sota de qualsevol imatge alineada a la dreta o a l''esquerra',
  // TrvActionClearNone
  'Flux de text &normal', 'Flux de text normal|Permet insertar text al voltant de les imatges alineades a la dreta i esquerra',
  // TrvActionVAlign
  'Posició de l''&Objecte...', 'Posició de l''Objecte|Canvia la posició de l''objecte seleccionat',
  // TrvActionItemProperties
  '&Propietats Objecte...', 'Propietats Objecte|Defineix les propietats de l''objecte actiu',
  // TrvActionBackground
  '&Fons...', 'Fons|Escollir color i imatge de fons',
  // TrvActionParagraph
  '&Paràgraf...', 'Paràgraf|Canvia els atributs del paràgraf',
  // TrvActionIndentInc
  '&Augmenta el sagnat', 'Augmenta el sagnat|Incrementa el sagnat esquerra del paràgraf seleccionat',
  // TrvActionIndentDec
  '&Redueix el sagnat', 'Redueix el sagnat|Redueix el sagnat esquerra del paràgraf seleccionat',
  // TrvActionWordWrap
  '&Ajust de paraula', 'Ajust de paraula|Commuta l''ajustament de paraula pel paràgraf seleccionat',
  // TrvActionAlignLeft
  'A&lineació esquerra', 'Alineació esquerra|Alineació del text selecionat a l''esquerra',
  // TrvActionAlignRight
  'Alineació D&reta', 'Alineació dreta|Alineació del text selecionat a la dreta',
  // TrvActionAlignCenter
  'Alineació &Centrada', 'Alineació centrada|Centra el text seleccionat',
  // TrvActionAlignJustify
  '&Justifica', 'Justifica|Alinea el text seleccionat als dos costats',
  // TrvActionParaColor
  'Color &fons del paràgraf...', 'Color fons del paràgraf|Defineix el color de fons del paràgraf',
  // TrvActionLineSpacing100
  'E&spai Interlineat', 'Espai Interlineat|Definexi l''espai Interlineat',
  // TrvActionLineSpacing150
  '1.5 I&nterlineat', '1.5 Interlineat|Definexi l''espai Interlineat igual a 1.5 línies',
  // TrvActionLineSpacing200
  'Espai Interlineat &Doble', 'Espai Interlineat Doble|Defineix l''espai interlineat al doble',
  // TrvActionParaBorder
  'Paràgraf Contorns i Fons...', 'Paràgraf Contorns i Fons|Defineix contorns i fons pel text seleccionat',
  // TrvActionInsertTable
  '&Insereix Taula...', '&Taula', 'Insereix taula|Insereix una taula nova',
  // TrvActionTableInsertRowsAbove
  'Insereix Fila &a Sobre', 'Insereix fila a sobre|Insereix una fila nova a sobre de la cel·la seleccionada',
  // TrvActionTableInsertRowsBelow
  'Insereix Fila a Sota', 'Insereix fila a sota|Insereix una fila nova a sota de la cel·la seleccionada',
  // TrvActionTableInsertColLeft
  'Insereix Columna a l''Esquerra', 'Insereix columna a l''esquerra|Insereix una columna nova a l''esquerra de la cel·la seleccionada',
  // TrvActionTableInsertColRight
  'Insereix Columna a la Dreta', 'Insereix columna a la dreta|Insereix una columna nova a la dreta de la cel·la seleccionada',
  // TrvActionTableDeleteRows
  'Suprimeix Files', 'Suprimeix files|Suprimeix les files de les cel·les seleccionades',
  // TrvActionTableDeleteCols
  'Suprimeix &Columnes', 'Suprimeix Columnes|Suprimeix les columnes de les cel·les seleccionades',
  // TrvActionTableDeleteTable
  'Suprimeix Taula', 'Suprimeix taula|Suprimeix la taula',
  // TrvActionTableMergeCells
  'Fusiona Cel·les', 'Fusiona ce&l·les|Fusiona les ce&l·les seleccionades',
  // TrvActionTableSplitCells
  'Divideix Cel·les...', 'Divideix cel·les|Divideix les cel·les seleccionades',
  // TrvActionTableSelectTable
  'Selecciona &Taula', 'Selecciona taula|Selecciona la taula',
  // TrvActionTableSelectRows
  'Selecciona Files', 'Selecciona files|Selecciona files',
  // TrvActionTableSelectCols
  'Selecciona Col&umnes', 'Selecciona Columnes|Selecciona columnes',
  // TrvActionTableSelectCell
  'Selecciona C&el·la', 'Selecciona Cel·la|Selecciona cel·les',
  // TrvActionTableCellVAlignTop
  'Alinea Cel·la a Part Superior', 'Alinea cel·la a la part superior|Alinea el contingut de la cel·la a la part superior',
  // TrvActionTableCellVAlignMiddle
  'Alinea Cel·la al &Mig', 'Alinea cel·la al mig|Alinea el contingut de la cel·la al mig',
  // TrvActionTableCellVAlignBottom
  'Alinea Cel·la a Part Inferior', 'Alinea cel·la a la part inferior|Alinea el contingut de la cel·la a la part inferior',
  // TrvActionTableCellVAlignDefault
  'Cel·la amb Alineament Vertical per &Defecte', 'Cel·la amb alineament vertical per defecte|Assignat alineament vertical per defecte a les cel·les seleccionades',
  // TrvActionTableCellRotationNone
  '&Sense rotació de cel·la', 'Sense rotació de cel·la|Girar el contingut de la cel·la 0°',
  // TrvActionTableCellRotation90
  'Girar cel·la &90°', 'Girar cel·la 90°|Girar el contingut de la cel·la 90°',
  // TrvActionTableCellRotation180
  'Girar cel·la &180°', 'Girar cel·la 180°|Girar el contingut de la cel·la 180°',
  // TrvActionTableCellRotation270
  'Girar cel·la &270°', 'Girar cel·la 270°|Girar el contingut de la cel·la 270°',
  // TrvActionTableProperties
  '&Propietats Taula...', 'Propietats taula|Canvia les propietats de la taula seleccionada',
  // TrvActionTableGrid
  'Mostra Línies &Graella', 'Mostra línies graella|Mostra o oculta línies de la graella',
  // TRVActionTableSplit
  '&Dividir taula', 'Dividir taula|Divideix la taula en dos començant des de la fila seleccionada',
  // TRVActionTableToText
  '&Converteix en text...', 'Convertir en text|Converteix la taula a text',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena les files de la taula',
  // TrvActionTableCellLeftBorder
  'Vora Esquerra', 'Vora Esquerra|Mostra o oculta la vora esquerra de la cel·la',
  // TrvActionTableCellRightBorder
  'Vora Dreta', 'Vora Dreta|Mostra o oculta la vora dreta de la cel·la',
  // TrvActionTableCellTopBorder
  'Vora Superior', 'Vora Superior|Mostra o oculta la vora superior de la cel·la',
  // TrvActionTableCellBottomBorder
  'Vora Inferior', 'Vora Inferior|Mostra o oculta la vora inferior de la cel·la',
  // TrvActionTableCellAllBorders
  'Totes les Vores', 'Totes les vores|Mostra o oculta totes les vores de la cel·la',
  // TrvActionTableCellNoBorders
  'Sense Vores', 'Sense Vores|Oculta totes les vores de la cel·la',
  // TrvActionFonts & TrvActionFontEx
  'Tipus Lletra...', 'Tipus de lletra|Canvia el tipus de lletra del text seleccionat',
  // TrvActionFontBold
  'Negreta', 'Negreta|Canvia l''estil del text seleccionat a negreta',
  // TrvActionFontItalic
  'Cursiva', 'Cursiva|Canvia l''estil del text seleccionat a cursiva',
  // TrvActionFontUnderline
  'S&ubratllat', 'Subratllat|Canvia l''estil del text seleccionat a subratllat',
  // TrvActionFontStrikeout
  'Ratllat', 'Ratllat|Ratlla el text seleccionat',
  // TrvActionFontGrow
  'Augmenta Tipus Lletra', 'Augmenta Tipus Lletra|Augmenta el 10% l''alçada del text seleccionat',
  // TrvActionFontShrink
  'Redueix Tipus Lletra', 'Redueix Tipus Lletra|Redueix el 10% l''alçada del text seleccionat',
  // TrvActionFontGrowOnePoint
  'Augmenta Tipus Lletra un Punt', 'Augmenta Tipus Lletra un Punt|Augmenta un punt l''alçada del text seleccionat',
  // TrvActionFontShrinkOnePoint
  'Redueix Tipus Lletra un Punt', 'Redueix Tipus Lletra un Punt|Redueix un punt l''alçada del text seleccionat',
  // TrvActionFontAllCaps
  'Tot Majúscules', 'Tot Majúscules|Canvia tots els caràcters del text seleccionat a majúscules',
  // TrvActionFontOverline
  'Sobreratllat', 'Sobreratllat|Posar sobreratllat el text seleccionat',
  // TrvActionFontColor
  '&Color text...', 'Color del text|Canvia el color del text seleccionat',
  // TrvActionFontBackColor
  'Color Fons del Text...', 'Color Fons del Text|Canvia el color del fons del text seleccionat',
  // TrvActionAddictSpell3
  'Verificació Ortografia', 'Verificació Ortografia|Verifica l''ortografia',
  // TrvActionAddictThesaurus3
  '&Tesaurus', 'Tesaurus|Proporciona sinònims de la paraula seleccionada',
  // TrvActionParaLTR
  'D''Esquerra a Dreta', 'D''esquerra a dreta|Estableix la direcció del text d''esquerra a dreta pel paràgraf seleccionat',
  // TrvActionParaRTL
  'De Dreta a Esquerra', 'De dreta a esquerra|Estableix la direcció del text de dreta a esquerra pel paràgraf seleccionat',
  // TrvActionLTR
  'Text d''Esquerra a Dreta', 'Text d''Esquerra a Dreta|Estableix la direcció d''esquerra a dreta pel text seleccionat',
  // TrvActionRTL
  'Text de Dreta a Esquerra', 'Text de Dreta a Esquerra|Estableix la direcció de dreta a esquerra pel text seleccionat',
  // TrvActionCharCase
  'Majúscules/minúscules', 'Majúscules/minúscules|Canvia de majúscules/minúscules el text seleccionat',
  // TrvActionShowSpecialCharacters
  'Visualitza caràcters &no imprimibles', 'Visualitza els caràcters no imprimibles|Mostra o oculta els caràcters no imprimibles, com marques de paràgraf, tabuladors i espais',
  // TrvActionSubscript
  'Su&bratllat','Subratllat|Converteix el text seleccionat a subratllat',
  // TrvActionSuperscript
  'Sobreratllat','Sobreratllat|Converteix el text seleccionat a sobreratllat',
  // TrvActionInsertFootnote
  '&Nota al peu', 'Nota al peu|Inserta una nota al peu',
  // TrvActionInsertEndnote
  '&Nota final', 'Nota final|Inserta una nota final',
  // TrvActionEditNote
  'E&ditar nota', 'Editar nota|Permet editar les notes al peu o notes finals',
  // TrvActionHide
  '&Ocultar', 'Ocultar|Oculta o mostra el fragment seleccionat',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Estils...', 'Estils|Obrir el diàleg de gestió d''estils',
  // TrvActionAddStyleTemplate
  '&Afegir Estil...', 'Afegir estil|Crear un nou estil de text o paràgraf',
  // TrvActionClearFormat,
  'Treure Format', 'Treure Format|Treure tots els formats dels text o paràgrafs del fragment seleccionat',
  // TrvActionClearTextFormat,
  'Treure Format &Text', 'Treure Format Text|Treure tots els formats del text seleccionat',
  // TrvActionStyleInspector
  '&Inspector Estils', 'Inspector Estils|Mostra o oculta l''Inspector d''Estils',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------------
   'D''acord', 'Cancel·la', 'Tanca', 'Insereix', '&Obre...', '&Desa...', '&Elimina', 'Ajuda', 'Eliminar',
  // Others  -------------------------------------------------------------------
  // percents
  'per cent',
  // left, top, right, bottom sides
  'Costat Esquerre', 'Costat Superior', 'Costat Dret', 'Costat Inferior',
  // save changes? confirm title
  'Voleu desar els canvis a %s?', 'Confirma',
  // warning: losing formatting
  '%s podria contenir característiques que no són compatibles amb el format seleccionat.'#13+
  'Voleu desar el document en aquest format?',
  // RVF format name
  'Format RichView',
  // Error messages ------------------------------------------------------------
  'Error',
  'Error en carregar el fitxer.'#13#13'Possibles causes:'#13'- el format d''aquest fitxer no és suportat per aquesta aplicació;'#13+
  '- el fitxer és corrupte;'#13'- el fitxer està sent obert i bloquejat per una altra aplicació.',
  'Error carregant fitxer d''imatge.'#13#13'Possibles causes:'#13'- el format d''aquesta imatge no és suportat per aquesta aplicació;'#13+
  '- el fitxer no conté una imatge;'#13+
  '- el fitxer és corrupte;'#13'- el fitxer està sent obert i bloquejat per una altra aplicació.',
  'Error en desar el fitxer.'#13#13'Possibles causes:'#13'- Sense espai en disc;'#13+
  '- el disc està protegit contra escriptura;'#13'- dispositiu extraïble no introduït;'#13+
  '- el fitxer està sent obert i bloquejat per una altra aplicació;'#13'- el disk està corromput.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Fitxers RichView (*.rvf)|*.rvf',  'Fitxers RTF (*.rtf)|*.rtf' , 'Fitxers XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Fitxers Text (*.txt)|*.txt', 'Fitxers Text - Unicode (*.txt)|*.txt', 'Fitxers Text - Autodetecció (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Fitxers HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - simplificat (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Documents de Microsoft Word (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'S''ha completat la cerca', 'La cadena a cercar ''%s'' no s''ha trobat.',
  // 1 string replaced; Several strings replaced
  '1 cadena reemplaçada.', '%d cadenes reemplaçades.',
  // continue search from the beginning/end?
  'S''ha arribat al final del document, continuar al començament?',
  'S''ha arribat al començament del document, continuar al final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparent', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Negre', 'Marró', 'Verd Oliva', 'Verd Fosc', 'Verd Marbrenc', 'Blau Marí', 'Anyil', 'Gris-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Vermell Fosc', 'Taronja', 'Groc Fosc', 'Verd', 'Verd Blavós', 'Blau', 'Blau-Gris', 'Gris-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vermell', 'Taronja Clar', 'Llimona', 'Verd Mar', 'Blau Cel', 'Blau Clar', 'Violeta', 'Gris-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa', 'Or', 'Groc', 'Verd Viu', 'Turquesa', 'Blau Cel', 'Pruna', 'Gris-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa Clar', 'Canyella', 'Groc Clar', 'Verd Clar', 'Turquesa Clar', 'Blau Pàl·lid', 'Lavanda', 'Blanc',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparent', '&Auto', '&Més Colors...', 'Per &Defecte',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fons', 'C&olor:', 'Posició', 'Fons', 'Texts de Mostra.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  'Cap', 'Ampliat', 'F&itxes Fixes', 'Fitxes', 'C&entrat',
  // Padding button
  '&Encoixinament...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Color per emplenar', '&Aplica a:', '&Més Colors...', '&Encoixinament...',
  'Si us plau selecciona un color',
  // [apply to:] text, paragraph, table, cell
  'text', 'paràgraf' , 'taula', 'cel·la',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Tipus de lletra', 'Tipus de lletra', 'Disposició',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Tipus de lletra:', 'Mida', 'Estil', 'Negreta', 'Curs&iva',
  // Script, Color, Back color labels, Default charset
  'Seqüència:', '&Color:',  'Fons:', '(Per Defecte)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efectes', 'S&ubratllat', 'S&obreratllat', 'Barra&t', '&Tot majúscules',
  // Sample, Sample text
  'Mostra', 'Text de mostra',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espaiat entre caràcters', 'E&spaiat:', '&Expandit', '&Condensat',
  // Offset group-box, Offset label, Down, Up,
  'Desplaçament vertical', 'Desplaçament:', 'Baixa', 'P&uja',
  // Scaling group-box, Scaling
  'Escalat horitzontal', 'Esc&alat:',
  // Sub/super script group box, Normal, Sub, Super
  'Subratllats i sobreratllats','&Normal','Su&bratllat','&Sobreratllat',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Encoixinament', 'Superior:', 'Esquerra:', 'Inferior:', 'Dreta:', 'Valors iguals',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Insereix hiperenllaç', 'Hiperenllaç', 'T&ext:', 'Objec&tiu', '<<selecció>>',
  // cannot open URL
  'No es pot navegar a "%s"',
  // hyperlink properties button, hyperlink style
  'Personalitza...', 'E&stil:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) ñolors
  'Atributs Hiperenllaç', 'Colors normals', 'Colors actius',
  // Text [color], Background [color]
  '&Text:', '&Fons:',
  // Underline [color]
  'Subratllat:',
  // Text [color], Background [color] (different hotkeys)
  'T&ext:', 'F&ons:',
  // Underline [color], Attributes group-box,
  'Subratllat:', 'Atributs',
  // Underline type: always, never, active (below the mouse)
  'S&ubratllat:', 'sempre', 'mai', 'actiu',
  // Same as normal check-box
  '&Normal',
  // underline active links check-box
  'S&ubratllar enllaços actius',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Insereix un símbol', 'Tipus de lletra:', 'Joc de &caràcters:', 'Unicode',
  // Unicode block
  '&Bloca:',
  // Character Code, Character Unicode Code, No Character
  'Caràcter: %d', 'Caràcter: Unicode %d', '(sense caràcter)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Insereix una Taula', 'Mida de la taula', 'Nombre de &columnes:', 'Nombre de files:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Format de la taula', 'Mida &automàtica', 'Ajusta la finestra', 'Mida personalitzada',
  // Remember check-box
  'Recorda &dimensions per a noves taules',
  // VAlign Form ---------------------------------------------------------------
  'Posició',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size,, Line, Table, Rows, Cells tabs
  'Propietats', 'Imatge', 'Posició i Mida', 'Línia', 'Taula', 'Files', 'Cel·les',
  // Image Appearance, Image Misc, Number tabs
  'Aparença', 'Miscel·lania', 'Número',
  // Box position, Box size, Box appearance tabs
  'Posició', 'Mida', 'Aparença',
  // Preview label, Transparency group-box, checkbox, label
  'Previsualització:', 'Transparència', '&Transparent', '&Color transparent:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Canvia...', 'De&sar...', 'Automàtic',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alineament vertical', '&Alineació:',
  'inferior, a la línia base del text', 'centre, a la línia de base del text',
  'superior, a la part superior de la línia', 'inferior, a la part inferior de la línia', 'central, al centre de la línia',
  // align to left side, align to right side
  'a l''esquerra', 'a la dreta',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Canviar per:', 'Expandit', '&Amplada:', '&Alçada:', 'Mida per defecte: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Escalar &proporcionalment', '&Reiniciar',
  // Inside the border, border, outside the border groupboxes
  'Dins el contorn', 'Contorn', 'Fora del contorn',
  // Fill color, Padding (i.e. margins inside border) labels
  'Color per emplenar:', 'Se&paració:',
  // Border width, Border color labels
  'Amplada contorn:', '&Color contorn:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Espaiat &horitzontal:', 'Espaiat &vertical:',
  // Miscellaneous groupbox, Tooltip label
  'Miscel·lani', 'Indicador de funció:',
  // spacing group-box, spacing, web group-box, alt text
  'Web', '&Text alternatiu:',
  // Horz line group-box, color, width, style
  'Línia horitzontal', '&Color:', 'Amplada:', '&Estil:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Taula', 'Amplada:', 'Color per emplenar:', 'Espaiat de &Cel·la...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Separació &horitzontal de cel·les:', 'Separació &vertical de cel·les:', 'Contorn i fons',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Visible c&ostats contorns...', 'Automàtic', 'automàtic',
  // Keep row content together checkbox
  'Mantenir contingut junt',
  // Rotation groupbox, rotacions: 0, 90, 180, 270
  'Rotació', 'Cap', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  'Contorn:', 'Visible c&ostats contorns...',
  // Border, CellBorders buttons
  'Contorns de la &taula...', 'Contorns de la &cel·la...',
  // Table border, Cell borders dialog titles
  'Contorns de la taula', 'Contorns de la &cel·la per defecte',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Impressió', 'No permetre tallar les columnes en diferents pàgines', 'Número d''encapçalament de files:',
  'Repeteix l''encapçalament de les files de la taula a cada pàgina',
  // top, center, bottom, default
  'Superior', '&Centre', 'Inferior', 'Per &Defecte',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Paràmetres', 'Amplada preferida:', 'Alçada mínima:', 'Color per emplenar:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Contorn', 'Costats visibles:',  'C&olor ombra:', 'Color clar', 'C&olor:',
  // Background image
  '&Imatge...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Posició horitzontal', 'Posició vertical', 'Posició en el text',
  // Position types: align, absolute, relative
  'Alinear', 'Posició absoluta', 'Posició relativa',
  // [Align] left side, center, right side
  'costat esquerra del quadre al costat esquerra de',
  'centre del quadre al centre de',
  'costat dret del quadre al costat dret de',
  // [Align] top side, center, bottom side
  'costat superior del quadre al costat superior de',
  'centre del quadre al centre de',
  'costat inferior del quadre al costat inferior de',
  // [Align] relative to
  'relatiu a',
  // [Position] to the right of the left side of
  'a la dreta del costat esquerra',
  // [Position] below the top side of
  'per sota del costat superior',
  // Anchors: page, main text area (x2)
  '&Pàgina', 'Àrea de text principal', 'Pàgin&a', 'Àrea principal de te&xt',
  // Anchors: left margin, right margin
  'Marge esquerra', 'Marge d&ret',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Marge superior', 'Marge inferior', 'Marge &interior', 'Marge exteri&or',
  // Anchors: character, line, paragraph
  '&Caràcter', 'L&ínia', 'Parà&graf',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Marg&es simètrics', 'Format de la cel·la',
  // Above text, below text
  'Text superior', 'Text inferior',
  // Width, Height groupboxes, Width, Height labels
  'Amplada', 'Alçada', 'Amplada:', 'Alçada:',
  // Auto height label, Auto height combobox item
  'Automàtic', 'automàtic',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Alineament vertical', 'Superior', '&Centre', 'Inferior',
  // Border and background button and title
  'C&ontorn i fons...', 'Contorn i fons quadre',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Enganxament especial', 'Enganxa com:', 'Format de text enriquit', 'format HTML',
  'Text', 'Text Unicode', 'Imatge Bitmap', 'Imatge Metafitxer',
  'Arxius d''imatges', 'URL',
  // Options group-box, styles
  'Opcions', '&Estils:',
  // style options: apply target styles, use source styles, ignore styles
 'Aplica estils al document destí', 'mantenir estils i aparença',
 'mantenir aparença, ignorar estils',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Pics i Numeracions', 'Amb pics', 'Numerada', 'Llistes de pics', 'Llistes numerades',
  // Customize, Reset, None
  'Personalitza...', '&Restaura', 'Cap',
  // Numbering: continue, reset to, create new
  'Segueix la numeració', 'restaura la numeració a', 'crea una nova llista, iniciant des de',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Aràbic gran', 'Romà gran', 'Decimal', 'Aràbia petit', 'Romà gran',
  // Bullet type
  'Cercle', 'Disc', 'Quadrat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Nivell:', '&Iniciar des de:', '&Continuar', 'Numeració',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personalitzar Llistes', 'Nivells', '&Comptador:', 'Propietats de la llista', 'Tipus de &llista:',
  // List types: bullet, image
  'vinyeta', 'imatge', 'Insereix un Número|',
  // Number format, Number, Start level from, Font button
  'Format &numèric:', 'Número', 'Inicia el nivell de numeració des de:', 'Tipus de lletra...',
  // Image button, bullet character, Bullet button,
  '&Imatge...', 'Caràcter de Vinyeta', 'Vinyeta...',
  // Position of list text, bullet, number, image, text
  'Posició text de llista', 'Posició vinyeta', 'Posició número', 'Posició Imatge', 'Posició text',
  // at, left indent, first line indent, from left indent
  'de:', 'Sagnat &esquerra:', 'Sagnat de la primera línia:', 'des de sagnat esquerra',
  // [only] one level preview, preview
  'Previsualització d''un nivell', 'Previsualització',
  // Preview text
  'Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text. Text text text text text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'Alineació esquerra', 'Alineació dreta', 'centre',
  // level #, this level (for combo-box)
  'Nivell %d', 'Aquest nivell',
  // Marker character dialog title
  'Edita el caràcter de vinyeta',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Contorn i Fons del paràgraf', 'Contorn', 'Fons',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Paràmetres', '&Color:', 'Amplada:', 'Amplada int&erna:', '&Marges de Text...',
  // Sample, Border type group-boxes;
  'Mostra', 'Tipus contorn',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Cap', '&Senzill', '&Doble', '&Triple', 'Gruix &interior', 'Gruix exteri&or',
  // Fill color group-box; More colors, padding buttons
  'Color per emplenar', '&Més Colors...', 'Encoixinament...',
  // preview text
  'Text text text text. Text text text text. Text text text text.',
  // title and group-box in Offsets dialog
  'Marges del Text', 'Marges del Contorn',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Paràgraf', 'Alineació', 'Esquerra', 'Dreta', '&Centre', '&Justifica',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espaiat', 'Anterior:', 'Posterior:', 'E&spaiat de línia:', 'de:',
  // Indents group-box; indents: left, right, first line
  'Sagnat', '&Esquerra:', 'Dreta:', 'Primera línia:',
  // indented, hanging
  'Sagnats', 'Suspesa', 'Mostra',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Senzill', '1.5 línies', 'Doble', 'Al mínim','Exacte', 'Múltiple',
  // preview text
  'Text text text text. Text text text text. Text text text text. Text text text text. Text text text text. Text text text text.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Sagnat i Espaiat', 'Tabulacions', 'Flux del text',
  // tab stop position label; buttons: set, delete, delete all
  'Posició de la &tabulació:', 'Aplica', 'Suprimeix', 'Suprimeix-ho Tot',
  // tab align group; left, right, center aligns,
  'Alineació', '&Esquerra', 'Dreta', '&Centre',
  // leader radio-group; no leader
  'Líder', '(Cap)',
  // tab stops to be deleted, delete none, delete all labels
  'Les tabulacions seran suprimides:', '(Cap)', 'Totes.',
  // Pagination group-box, keep with next, keep lines together
  'Paginació', 'Conserva amb el següent', 'Mantingues les línies juntes',
  // Outline level, Body text, Level #
  '&Nivell d''esquema:', 'Text de cos', 'Nivel %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Previsualitza la impressió', 'Amplada de la pàgina', 'Pàgina completa', 'Pàgines:',
  // Invalid Scale, [page #] of #
  'Si us plau introdueix un número del 10 al 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primera Pàgina', 'Pàgina Anterior', 'Pàgina Següent', 'Última Pàgina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espaiat de Cel·la', 'Espaiat', 'Entre &cel·les', 'Entre contorn de taula i cel·les',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horitzontal:', 'Ve&rtical:', 'H&oritzontal:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Contorns', 'Paràmetres', '&Color:', 'Color de la &Llum:', 'Color de l''ombra:',
  // Width; Border type group-box;
  'Amplada:', 'Tipus contorn',
  // Border types: none, sunken, raised, flat
  '&Cap', 'Enfonsat', '&Elevat', 'Pla',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Divideix', 'Divideix a',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'E&specifica número de files i columnes', 'Cel·la &Original',
  // number of columns, rows, merge before
  'Número de &columnes:', 'Número de files:', 'Combina abans de dividir',
  // to original cols, rows check-boxes
  'Divideix a les columnes originals', 'Divideix a les files originals',
  // Add Rows form -------------------------------------------------------------
  'Afegeix Files', 'Nombre de Files:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Configuració de la pàgina', 'Pàgina', 'Capçalera i peu de pàgina',
  // margins group-box, left, right, top, bottom
  'Marges (mil·límetres)', 'Marges (polzades)', '&Esquerra:', 'Superior:', 'Dreta:', 'Inferior:',
  // mirror margins check-box
  'Rèplica &Marges',
  // orientation group-box, portrait, landscape
  'Orientació', 'Vertical', 'Horitzontal',
  // paper group-box, paper size, default paper source
  'Paper', 'Mida:', 'Font:',
  // header group-box, print on the first page, font button
  'Capçalera', '&Text:', '&Capçalera a la primera pàgina', 'Tipus de lletra...',
  // header alignments: left, center, right
  '&Esquerra', '&Centre', 'Dreta',
  // the same for footer (different hotkeys)
  'Peu', 'Te&xt:', 'Peu a la primera &pàgina', 'Tipus de lletra...',
  '&Esquerra', 'Ce&ntre', 'Dreta',
  // page numbers group-box, start from
  'Números de pàgina', 'Comença per:',
  // hint about codes
  'Combinacions de caràcters especials:'#13'&&p - número de pàgina; &&P - nombre de pàgines; &&d - data actual; &&t - hora actual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Pàina de códis de l''arxiu de text', '&Selecciona la codificació d''arxius:',
  // thai, japanese, chinese (simpl), korean
  'Tailandesa', 'Japonesa', 'Xina (Simplificada)', 'Coreana',
  // chinese (trad), central european, cyrillic, west european
  'Xina (Tradicional)', 'D''Europa Central i Oriental', 'ciríl·lica', 'D''Europa Occidental',
  // greek, turkish, hebrew, arabic
  'Grega', 'turca', 'Hebrea', 'Aràbic',
  // baltic, vietnamese, utf-8, utf-16
  'Bàltica', 'Vietnamita', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estil visual', '&Selecciona estil:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Convertir en text', 'Selecciona un &delimitador:',
  // line break, tab, ';', ','
  'salt de línia', 'tabulació', 'punt i coma', 'coma',
  // Table sort form -----------------------------------------------------------
  // error message
  'No és possible ordenar una taula amb files combinades',
  // title, main options groupbox
  'Criteri d''ordenació de taula', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar per la columna:', 'Distingir &majúscules de minúscules',
  // heading row, range or rows
  '&Excloure fila de capçalera', 'Files a ordenar: des de %d fins a %d',
  // order, ascending, descending
  'Ordre', '&Ascendent', '&Descendent',
  // data type, text, number
  'Tipus', '&Text', '&Número',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Inserir Número', 'Propietats', 'Nom &Comptador:', 'Tipus &Numeració:',
  // numbering groupbox, continue, start from
  'Numeració', 'C&ontinuar', 'Iniciar de&s de:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Títol', 'Etiqueta:', '&Excloure etiqueta de títol',
  // position radiogroup
  'Posició', 'Objecte seleccion&at superior', 'O&bjecte seleccionat inferior',
  // caption text
  'Títol &text:',  
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numeració', 'Figura', 'Taula',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Visible els costats del contorn', 'Contorn',
  // Left, Top, Right, Bottom checkboxes
  'Costat esquerra', 'Cos&tat superior', 'Costat d&ret', 'Costat inferior',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Canviar la mida de la columna', 'Canviar la mida de la fila',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Sagnat de la primera línia', 'Sagnat Esquerra', 'Sagnat Negatiu', 'Sagnat Dreta',
  // Hints on lists: up one level (left), down one level (right)
  'Un nivell cap amunt', 'Un nivell cap avall',
  // Hints for margins: bottom, left, right and top
  'Marge Inferior', 'Marge Esquerra', 'Marge Dreta', 'Marge Superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabulació Alineat Esquerra', 'Tabulació Alineat Dreta', 'Tabulació Alineat Centre', 'Tabulació Alineat Decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Sagnat Normal', 'Sense espai', 'Encapçalament %d', 'Llista Paràgrafs',
  // Hyperlink, Title, Subtitle
  'Hiperenllaç', 'Títol', 'Subtítol',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Èmfasi', 'Subtítol Èmfasi', 'Èmfasi Intens', 'Fort',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cita', 'Cita Intensa', 'Subtítol Referència', 'Referència Intensa',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Bloc Text', 'Variable HTML', 'Codi HTML', 'Sigles HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definició HTML', 'Teclat HTML', 'Exemple HTML', 'Màquina d''escriure HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Preformatejat HTML', 'Citar HTML', 'Encapçalament', 'Peu', 'Número Pàgina',
  // Caption
  'Títol',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Referència Nota al Final', 'Referència Nota al Peu', 'Text Nota al Final', 'Text Nota al Peu',
  // Sidenote Reference, Sidenote Text
  'Referència de nota al marge', 'Text de nota al marge',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'color', 'color fons', 'transparent', 'predeterminat', 'color subratllat',
  // default background color, default text color, [color] same as text
  'color predeterminat de fons', 'color predeterminat de text', 'mateix text',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'senzill', 'gruix', 'doble', 'puntejat', 'puntejat gruixut', 'discontinua',
  // underline types: thick dashed, long dashed, thick long dashed,
  'discontinua gruixuda', 'traços llargs', 'traços llargs gruixuts',
  // underline types: dash dotted, thick dash dotted,
  'punts discontinus', 'punts discontinus gruixuts',
  // underline types: dash dot dotted, thick dash dot dotted
  'punt i ratlla discontinuu', 'punt i ratlla discontinuu gruixut',
  // sub/superscript: not, subsript, superscript
  'no sub/superíndex', 'subíndex', 'superíndex',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'mode bi-di:', 'heretat', 'd''Esquerra a dreta', 'de dreta a esquerra',
  // bold, not bold
  'Negreta', 'sense Negreta',
  // italic, not italic
  'Cursiva', 'sense Cursiva',
  // underlined, not underlined, default underline
  'subratllat', 'sense subratllar', 'subratllat per defecte',
  // struck out, not struck out
  'tatxat', 'sense tatxar',
  // overlined, not overlined
  'sobreratllat', 'sense sobreratllar',
  // all capitals: yes, all capitals: no
  'Tot majúscules', 'cap majúscula',
  // vertical shift: none, by x% up, by x% down
  'sense desplaçament vertical', 'desplaçada %d%% amunt', 'desplaçada %d%% avall',
  // characters width [: x%]
  'amplada caràcters',
  // character spacing: none, expanded by x, condensed by x
  'espaiat caràcters normal', 'espaiat ampliada per %s', 'espaiat condensat per %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'font per defecte', 'paràgraf per defecte', 'heretat',
  // [hyperlink] highlight, default hyperlink attributes
  'realçar', 'per defecte',
  // paragraph aligmnment: left, right, center, justify
  'alineat a l''esquerra', 'alineat a la dreta', 'centrat', 'justificat',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altura línia: %d%%', 'espaiat entre línies: %s', 'altura línia: almenys %s',
  'altura línia: exactament %s',
  // no wrap, wrap
  'ajust de línia desactivada', 'ajust de línia',
  // keep lines together: yes, no
  'ajuntar línies', 'no ajuntar les línies',
  // keep with next: yes, no
  'mantenir amb el paràgraf següent', 'no mantenir amb el paràgraf següent',
  // read only: yes, no
  'només lectura', 'editable',
  // body text, heading level x
  'nivell d''esquema: text de cos', 'nivell d''esquema: %d',
  // indents: first line, left, right
  'sagnia primera línia', 'sagnia esquerra', 'sagnia dreta',
  // space before, after
  'espai abans', 'espai després',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabulacions', 'cap', 'alinear', 'esquerra', 'dreta', 'centre',
  // tab leader (filling character)
  'líder',
  // no, yes
  'no', 'sí',
  // [padding/spacing/side:] left, top, right, bottom
  'esquerra', 'superior', 'dreta', 'inferior',
  // border: none, single, double, triple, thick inside, thick outside
  'cap', 'simple', 'doble', 'triple', 'gruix interior', 'gruix exterior',
  // background, border, padding, spacing [between text and border]
  'fons', 'contorn', 'separació', 'espaiat',
  // border: style, width, internal width, visible sides
  'estil', 'amplada', 'amplada interna', 'costats visibles',
  // style inspector -----------------------------------------------------------
  // title
  'Inspector estils',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estil', '(Cap)', 'Paràgraf', 'Font', 'Atributs',
  // border and background, hyperlink, standard [style]
  'Contorn i fons', 'Hiperenllaç', '(estàndard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estils', 'Estil',
  // name, applicable to,
  '&Nom:', 'Aplicable &a:',
  // based on, based on (no &),
  '&Basat en:', 'Basat en:',
  //  next style, next style (no &)
  'Estil del paràgra&f següent:', 'Estil del paràgraf següent:',
  // quick access check-box, description and preview
  '&Accés ràpid', 'Descripció i &previsualització:',
  // [applicable to:] paragraph and text, paragraph, text
  'paràgraf i text', 'paràgraf', 'text',
  // text and paragraph styles, default font
  'Estil de Text i Paràgraf', 'Font per Defecte',
  // links: edit, reset, buttons: add, delete
  'Edició', 'Reiniciar', '&Afegir', '&Esborrar',
  // add standard style, add custom style, default style name
  'Afegir Estil E&stàndard...', 'Afegir Estil Personalitzat', 'Estil %d',
  // choose style
  'Triar e&stil:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Estils RichView (*.rvst)|*.rvst', 'Error al carregar fitxer', 'Aquest fitxer no conté estils',
  // Title, group-box, import styles
  'Importar Estils des del Fitxer', 'Importar', 'I&mportar estils:',
  // existing styles radio-group: Title, override, auto-rename
  'Estils existents', 's&obreescriure', '&afegir amb un altre nom',
  // Select, Unselect, Invert,
  '&Seleccionar', 'Desseleccionar', '&Invertir',
  // select/unselect: all styles, new styles, existing styles
  'Tots els Estils', 'Estils &Nous', 'Estils &Existents',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Esborrar Format', 'Tots els Estils',
  // dialog title, prompt
  'Estils', 'Triar estil per apli&car:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sinònims', '&Ignora''ls tots', '&Afegeix al Diccionari',
  // Progress messages ---------------------------------------------------------
  'Descarregar %s', 'Preparant per imprimir...', 'Imprimint la pàgina %d',
  'Conversió des de RTF...',  'Conversió a RTF...',
  'Llegit %s...', 'Escrit %s...', 'text',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Cap Nota', 'Nota al peu', 'Nota al final', 'Nota dins el marc', 'Quadre de Text'
  );


initialization
  RVA_RegisterLanguage(
    'Catalan', // english language name, do not translate
    'Català', // native language name
    ANSI_CHARSET, @Messages);

end.

