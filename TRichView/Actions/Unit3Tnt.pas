
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Demo project.                                   }
{       You can use it as a basis for your              }
{       applications.                                   }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit Unit3Tnt;

interface

{
  Remove dots from the defines below to enable support for Gif and Png.

  GifImage (by Anders Melander)
    http://www.torry.net/vcl/graphics/gif/gifimage.exe (original)
    http://www.trichview.com/resources/thirdparty/gifimage.zip (update)
  Note: for Delphi 2007, use the built in TGifImage instead)
  PngObject (by Gustavo Huffenbacher Daud)
    http://pngdelphi.sourceforge.net/
}

{$I RV_Defs.inc}          // contains defines about Delphi compiler versions

{.$DEFINE USE_GIFIMAGE}
{.$DEFINE USE_PNGOBJECT}


uses
  Windows, Messages, SysUtils,
  Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls,
  ActnList, StdActns, ImgList,
  Menus, ShellApi, Printers, ToolWin, RVUni,

  {$IFDEF USE_GIFIMAGE}
    {$IFDEF RICHVIEWDEF2007}
    GifImg, RVGifAnimate2007,
    {$ELSE}
    GifImage, RVGifAnimate,
    {$ENDIF}
  {$ENDIF}
  {$IFDEF USE_PNGOBJECT}
  PngImage,
  {$ENDIF}

  RVScroll, RichView, RVEdit,
  RVStyle, PtblRV, CRVFData, RVTable, RVItem, CRVData, RVFuncs, RVTypes,

  // RVAAddictLanguages,

  RichViewActions, RVALocRuler, RVFontCombos, RVALocalize, Ruler, RVRuler,
  BaseRVFrm,

  TntComCtrls, TntMenus, TntForms, TntStdCtrls, RVStyleFuncs;

type
  TForm3 = class(TTntForm)
    RichViewEdit1: TRichViewEdit;
    MainMenu1: TTntMainMenu;
    mitFile: TTntMenuItem;
    mitEdit: TTntMenuItem;
    New1: TTntMenuItem;
    Load1: TTntMenuItem;
    PrintPreview1: TTntMenuItem;
    N1: TTntMenuItem;
    Print1: TTntMenuItem;
    mitExit: TTntMenuItem;
    Cut1: TTntMenuItem;
    Copy1: TTntMenuItem;
    n2: TTntMenuItem;
    mitFont: TTntMenuItem;
    Bold1: TTntMenuItem;
    Italic1: TTntMenuItem;
    Underline1: TTntMenuItem;
    N3: TTntMenuItem;
    mitPara: TTntMenuItem;
    Leftjustify1: TTntMenuItem;
    N4: TTntMenuItem;
    Undo1: TTntMenuItem;
    Redo1: TTntMenuItem;
    Changefont1: TTntMenuItem;
    Save1: TTntMenuItem;
    Strikeout1: TTntMenuItem;
    mitTable: TTntMenuItem;
    InsertTable1: TTntMenuItem;
    N5: TTntMenuItem;
    SelectAll1: TTntMenuItem;
    AlignLeft1: TTntMenuItem;
    AlignCenter1: TTntMenuItem;
    AlignRight1: TTntMenuItem;
    N6: TTntMenuItem;
    DecreaseIndent1: TTntMenuItem;
    IncreaseIndent1: TTntMenuItem;
    RVStyle1: TRVStyle;
    N7: TTntMenuItem;
    StatusBar1: TTntStatusBar;
    Justify1: TTntMenuItem;
    InsertRowAbove1: TTntMenuItem;
    InsertRowBelow1: TTntMenuItem;
    N8: TTntMenuItem;
    InsertColumnLeft1: TTntMenuItem;
    InsertColumnRight1: TTntMenuItem;
    N9: TTntMenuItem;
    N10: TTntMenuItem;
    DeleteRows1: TTntMenuItem;
    rvActionTableDeleteCols11: TTntMenuItem;
    N11: TTntMenuItem;
    MergeCells1: TTntMenuItem;
    SplitCells1: TTntMenuItem;
    N12: TTntMenuItem;
    mitTableSelect: TTntMenuItem;
    SelectTable1: TTntMenuItem;
    SelectColumns1: TTntMenuItem;
    SelectRows1: TTntMenuItem;
    SelectCell1: TTntMenuItem;
    mitFontStyle: TTntMenuItem;
    mitFontSize: TTntMenuItem;
    ShrinkFont1: TTntMenuItem;
    GrowFont1: TTntMenuItem;
    N13: TTntMenuItem;
    ShrinkFontByOnePoint1: TTntMenuItem;
    GrowFontByOnePoint1: TTntMenuItem;
    N14: TTntMenuItem;
    AllCapitals1: TTntMenuItem;
    Overline1: TTntMenuItem;
    N15: TTntMenuItem;
    Find1: TTntMenuItem;
    FindNext1: TTntMenuItem;
    Replace1: TTntMenuItem;
    CoolBar1: TCoolBar;
    ToolBar1: TTntToolBar;
    ToolButton11: TTntToolButton;
    ToolButton1: TTntToolButton;
    ToolButton2: TTntToolButton;
    ToolButton13: TTntToolButton;
    ToolButton22: TTntToolButton;
    ToolButton17: TTntToolButton;
    ToolButton18: TTntToolButton;
    ToolButton19: TTntToolButton;
    ToolButton20: TTntToolButton;
    ToolButton21: TTntToolButton;
    ToolBar2: TTntToolBar;
    ToolButton41: TTntToolButton;
    ToolButton42: TTntToolButton;
    ToolButton43: TTntToolButton;
    ToolButton44: TTntToolButton;
    ToolButton49: TTntToolButton;
    ToolButton50: TTntToolButton;
    ToolButton51: TTntToolButton;
    ToolButton53: TTntToolButton;
    ToolButton54: TTntToolButton;
    ToolButton3: TTntToolButton;
    ToolButton4: TTntToolButton;
    ToolButton5: TTntToolButton;
    ToolButton6: TTntToolButton;
    ToolButton7: TTntToolButton;
    ToolBar3: TTntToolBar;
    ToolButton9: TTntToolButton;
    ToolButton10: TTntToolButton;
    ToolButton12: TTntToolButton;
    ToolButton14: TTntToolButton;
    ToolButton15: TTntToolButton;
    ToolButton16: TTntToolButton;
    ToolButton23: TTntToolButton;
    ToolButton24: TTntToolButton;
    ToolButton25: TTntToolButton;
    ToolButton26: TTntToolButton;
    ToolButton28: TTntToolButton;
    ToolButton8: TTntToolButton;
    ToolButton27: TTntToolButton;
    ToolButton29: TTntToolButton;
    ToolButton30: TTntToolButton;
    ToolButton31: TTntToolButton;
    ToolButton32: TTntToolButton;
    ToolButton33: TTntToolButton;
    ToolButton34: TTntToolButton;
    ToolButton35: TTntToolButton;
    N16: TTntMenuItem;
    ToolButton36: TTntToolButton;
    ToolButton37: TTntToolButton;
    extColor1: TTntMenuItem;
    ToolButton38: TTntToolButton;
    extBackgroundColor1: TTntMenuItem;
    N17: TTntMenuItem;
    ParagraphBackgroundColor1: TTntMenuItem;
    ToolButton39: TTntToolButton;
    ToolButton40: TTntToolButton;
    mitFormat: TTntMenuItem;
    BackgroundColor1: TTntMenuItem;
    DeleteTable1: TTntMenuItem;
    ToolButton45: TTntToolButton;
    N18: TTntMenuItem;
    rvActionParagraph11: TTntMenuItem;
    RVAControlPanel1: TRVAControlPanel;
    Font1: TTntMenuItem;
    FillColor1: TTntMenuItem;
    SaveAs1: TTntMenuItem;
    Export1: TTntMenuItem;
    mitInsert: TTntMenuItem;
    File1: TTntMenuItem;
    Picture1: TTntMenuItem;
    ToolButton46: TTntToolButton;
    PasteSpecial1: TTntMenuItem;
    N19: TTntMenuItem;
    SingleLineSpacing1: TTntMenuItem;
    N15LineSpacing1: TTntMenuItem;
    DoubleLineSpacing1: TTntMenuItem;
    N20: TTntMenuItem;
    InsertPageBreak1: TTntMenuItem;
    RemovePageBreak1: TTntMenuItem;
    N21: TTntMenuItem;
    mitTableAlignCellContents: TTntMenuItem;
    AlignCellToTheTop1: TTntMenuItem;
    AlignCellToTheMiddle1: TTntMenuItem;
    AlignCellToTheBottom1: TTntMenuItem;
    DefaultCellVerticalAlignment1: TTntMenuItem;
    ParagraphBorders1: TTntMenuItem;
    N22: TTntMenuItem;
    Properties1: TTntMenuItem;
    HorizontalLine1: TTntMenuItem;
    HypertextLink1: TTntMenuItem;
    ToolButton47: TTntToolButton;
    N23: TTntMenuItem;
    ableProperties1: TTntMenuItem;
    N24: TTntMenuItem;
    ShowGridLines1: TTntMenuItem;
    N25: TTntMenuItem;
    BulletsandNumbering1: TTntMenuItem;
    InsertSymbol1: TTntMenuItem;
    mitTableCellBorders: TTntMenuItem;
    LeftBorder1: TTntMenuItem;
    rvActionTableCellTopBorder11: TTntMenuItem;
    rvActionTableCellRightBorder11: TTntMenuItem;
    rvActionTableCellBottomBorder11: TTntMenuItem;
    rvActionTableCellAllBorders11: TTntMenuItem;
    rvActionTableCellNoBorders11: TTntMenuItem;
    ToolBar4: TTntToolBar;
    ToolButton48: TTntToolButton;
    ToolButton52: TTntToolButton;
    ToolButton55: TTntToolButton;
    ToolButton56: TTntToolButton;
    ToolButton58: TTntToolButton;
    ToolButton59: TTntToolButton;
    Bullets1: TTntMenuItem;
    Numbering1: TTntMenuItem;
    ToolButton57: TTntToolButton;
    ToolButton60: TTntToolButton;
    ToolButton61: TTntToolButton;
    ToolBar5: TTntToolBar;
    cmbFont: TRVFontComboBox;
    RVAPopupMenu1: TRVAPopupMenu;
    Background1: TTntMenuItem;
    RVPrint1: TRVPrint;
    ColorDialog1: TColorDialog;
    Button1: TButton;
    cmbFontSize: TRVFontSizeComboBox;
    PageSetup1: TTntMenuItem;
    N26: TTntMenuItem;
    CharacterCase1: TTntMenuItem;
    RVRuler1: TRVRuler;
    ToolButton62: TTntToolButton;
    ToolButton63: TTntToolButton;
    pmFakeDropDown: TPopupMenu;
    N27: TTntMenuItem;
    rvActionSubscript11: TTntMenuItem;
    rvActionSuperscript11: TTntMenuItem;
    ToolButton64: TTntToolButton;
    ToolButton65: TTntToolButton;
    PasteSpecial2: TTntMenuItem;
    N28: TTntMenuItem;
    ObjectPosition1: TTntMenuItem;
    N29: TTntMenuItem;
    RemoveHyperlinks1: TTntMenuItem;
    TntToolButton1: TTntToolButton;
    TntToolButton2: TTntToolButton;
    N30: TTntMenuItem;
    Hide1: TTntMenuItem;
    tbUnits: TTntToolBar;
    cmbUnits: TTntComboBox;
    mitTableCellRotation: TTntMenuItem;
    RotateCellby270Degrees1: TTntMenuItem;
    RotateCellby180Degrees1: TTntMenuItem;
    RotateCellby90Degrees1: TTntMenuItem;
    NoCellRotation1: TTntMenuItem;
    ConverttoText1: TTntMenuItem;
    Sort1: TTntMenuItem;
    ProgressBar1: TProgressBar;
    Styles1: TTntMenuItem;
    AddStyle1: TTntMenuItem;
    ClearTextFormat1: TTntMenuItem;
    ClearFormat1: TTntMenuItem;
    InspectStyles1: TTntMenuItem;
    N31: TTntMenuItem;
    cmbStyles: TRVStyleTemplateComboBox;
    Number1: TTntMenuItem;
    extBox1: TTntMenuItem;
    N32: TTntMenuItem;
    InsertFootnote1: TTntMenuItem;
    InsertEndnote1: TTntMenuItem;
    Sidenote1: TTntMenuItem;
    EditNote1: TTntMenuItem;
    InsertCaption1: TTntMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure mitExitClick(Sender: TObject);
    procedure RichViewEdit1Jump(Sender: TObject; id: Integer);
    procedure RichViewEdit1ReadHyperlink(Sender: TCustomRichView;
      const Target, Extras: String; DocFormat: TRVLoadFormat; var StyleNo: Integer;
      var ItemTag: TRVTag; var ItemName: TRVRawByteString);
    procedure Button1Click(Sender: TObject);
    procedure RichViewEdit1WriteHyperlink(Sender: TCustomRichView;
      id: Integer; RVData: TCustomRVData; ItemNo: Integer;
      SaveFormat: TRVSaveFormat; var Target, Extras: String);
    procedure RVAControlPanel1MarginsChanged(Sender: TrvAction;
      Edit: TCustomRichViewEdit);
    procedure RVAControlPanel1Download(Sender: TrvAction;
      const Source: String);
    procedure pmFakeDropDownPopup(Sender: TObject);
    procedure RichViewEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cmbUnitsChange(Sender: TObject);
    procedure RVPrint1SendingToPrinter(Sender: TCustomRichView;
      PageCompleted: Integer; Step: TRVPrintingStep);
    procedure RichViewEdit1Progress(Sender: TCustomRichView;
      Operation: TRVLongOperation; Stage: TRVProgressStage;
      PercentDone: Byte);
  private
    { Private declarations }
    procedure ColorPickerShow(Sender: TObject);
    procedure ColorPickerHide(Sender: TObject);
    procedure rvActionSave1DocumentFileChange(Sender: TObject;
      Editor: TCustomRichViewEdit; const FileName: String;
      FileFormat: TrvFileSaveFilter; IsNew: Boolean);
    procedure Localize;
    procedure UpdateLinkedControls;
    procedure NoteEditorEnter(Sender: TObject);
    procedure NoteEditorExit(Sender: TObject);
    procedure rvActionEditNoteFormCreate(Sender: TrvAction; Form: TForm);
    procedure rvActionEditNoteShowing(Sender: TrvAction; Form: TForm);
    procedure rvActionEditNoteHide(Sender: TObject);    
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses dmActions;
{$R *.dfm}

procedure TForm3.FormCreate(Sender: TObject);
var FileName: String;
begin
  // Almost all these assignments could be done at design time in the Object Inspector
  // But in this demo we do not want to modify rvActionsResource
  // (and we recommend to use a copy of it in your applications)

  // initializing TrvActionEditNote action
  rvActionsResource.rvActionEditNote1.Control := RichViewEdit1;
  rvActionsResource.rvActionEditNote1.OnFormCreate := rvActionEditNoteFormCreate;
  rvActionsResource.rvActionEditNote1.OnShowing := rvActionEditNoteShowing;
  rvActionsResource.rvActionEditNote1.OnHide := rvActionEditNoteHide;

  // linking combo boxes and style inspector to RichViewEdit1
  UpdateLinkedControls;

  rvActionsResource.rvActionSave1.OnDocumentFileChange := rvActionSave1DocumentFileChange;

  // Code for making color-picking buttons stay pressed while a
  // color-picker window is visible.
  rvActionsResource.rvActionColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionParaColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionParaColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionFontColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionFontColor1.OnHideColorPicker := ColorPickerHide;
  rvActionsResource.rvActionFontBackColor1.OnShowColorPicker := ColorPickerShow;
  rvActionsResource.rvActionFontBackColor1.OnHideColorPicker := ColorPickerHide;


  // AutoComplete feature causes OnClick generation when editing combo-box's text.
  // Since in OnClick we move input focus in RichViewEdit1, this effect is
  // undesirable
  cmbFont.AutoComplete := False;
  cmbFontSize.AutoComplete := False;

  cmbFont.Font.Name := RVAControlPanel1.DialogFontName;
  cmbFontSize.Font.Name := RVAControlPanel1.DialogFontName;
  cmbUnits.Font.Name := RVAControlPanel1.DialogFontName;
  cmbStyles.Font.Name := RVAControlPanel1.DialogFontName;
  cmbUnits.ItemIndex := ord(RVAControlPanel1.UnitsDisplay);

  Localize;

  // Loading initial file via ActionOpen (allowing to update user interface)  
  FileName := ExtractFilePath(Application.ExeName)+'readme.rvf';
  if FileExists(FileName) then
    rvActionsResource.rvActionOpen1.LoadFile(RichViewEdit1, FileName, ffiRVF)
  else begin
    RichViewEdit1.Modified := False;
    rvActionsResource.rvActionNew1.ExecuteTarget(RichViewEdit1);
  end;
end;


{------------------- Working with document ------------------------------------}

// When document is created, saved, loaded...
procedure TForm3.rvActionSave1DocumentFileChange(Sender: TObject;
  Editor: TCustomRichViewEdit; const FileName: String;
  FileFormat: TrvFileSaveFilter; IsNew: Boolean);
var s: String;
begin
  s := ExtractFileName(FileName);
  rvActionsResource.rvActionPrint1.Title := s;
  rvActionsResource.rvActionQuickPrint1.Title := s;
  if IsNew then
    s := s+' (*)';
  Caption := s + ' - RichViewActionsTest';
end;

// Prompt for saving...
procedure TForm3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := rvActionsResource.rvActionSave1.CanCloseDoc(RichViewEdit1);
end;

procedure TForm3.mitExitClick(Sender: TObject);
begin
  Close;
end;

{--------------- Working with color-picking buttons ---------------------------}

// Code for making color-picking buttons pressed while
// a color-picker window is visible.
procedure TForm3.ColorPickerShow(Sender: TObject);
begin
  if (Sender as TAction).ActionComponent is TToolButton then
    TToolButton(TAction(Sender).ActionComponent).Down := True;
end;

procedure TForm3.ColorPickerHide(Sender: TObject);
begin
  if (Sender as TAction).ActionComponent is TToolButton then
    TToolButton(TAction(Sender).ActionComponent).Down := False;
end;

{-------------- Set of events for processing hypertext jumps ------------------}

// Hyperlink click
procedure TForm3.RichViewEdit1Jump(Sender: TObject; id: Integer);
begin
  rvActionsResource.rvActionInsertHyperlink1.GoToLink(RichViewEdit1, id);
end;

// Importing hyperlink from RTF
procedure TForm3.RichViewEdit1ReadHyperlink(Sender: TCustomRichView;
  const Target, Extras: String; DocFormat: TRVLoadFormat; var StyleNo: Integer;
  var ItemTag: TRVTag; var ItemName: TRVRawByteString);
begin
  if DocFormat=rvlfURL then
    StyleNo :=
      rvActionsResource.rvActionInsertHyperlink1.GetHyperlinkStyleNo(RichViewEdit1);
  ItemTag := rvActionsResource.rvActionInsertHyperlink1.EncodeTarget(Target);
end;

// Exporting hyperlink to RTF and HTML
procedure TForm3.RichViewEdit1WriteHyperlink(Sender: TCustomRichView;
  id: Integer; RVData: TCustomRVData; ItemNo: Integer;
  SaveFormat: TRVSaveFormat; var Target, Extras: String);
begin
  Target := RVData.GetItemTag(ItemNo);
end;

// URL detection on typing
procedure TForm3.RichViewEdit1KeyPress(Sender: TObject; var Key: Char);
begin
  {
  // uncomment if you use Addict3
  if (Key in ['''', #13, #9]) or (Pos(Key, RichViewEdit1.Delimiters)<>0) then
    RVA_Addict3AutoCorrect(RichViewEdit1);
  }
  if Key in [#13, #9, ' ', ',', ';'] then begin
    rvActionsResource.rvActionInsertHyperlink1.DetectURL(RichViewEdit1);
    rvActionsResource.rvActionInsertHyperlink1.TerminateHyperlink(RichViewEdit1);
  end;
end;

{----------------------- Insert table popup      -----------------------------}
{ We use a trick: insert-table button has style tbsDropDown and assigned
  DropDownMenu (pmFakeDropDown). This menu is empty, but shows table size
  popup instead of itself }
procedure TForm3.pmFakeDropDownPopup(Sender: TObject);
begin
  rvActionsResource.rvActionInsertTable1.ShowTableSizeDialog(RichViewEdit1,
    ToolButton12);
end;

{---------------------------- Ruler ------------------------------------}

procedure TForm3.RVAControlPanel1MarginsChanged(Sender: TrvAction;
  Edit: TCustomRichViewEdit);
begin
  RVRuler1.UpdateRulerMargins;
end;

{---------------------------- Localization ------------------------------------}

procedure TForm3.Button1Click(Sender: TObject);
begin
  if RVA_ChooseLanguage then
    Localize;
end;

procedure TForm3.Localize;
begin
  // Fonts
  Font.Charset            := RVA_GetCharset;
  StatusBar1.Font.Charset := RVA_GetCharset;
  Screen.HintFont.Charset := RVA_GetCharset;
  Screen.MenuFont.Charset := RVA_GetCharset;
  // Localizing all actions on rvActionsResource
  RVA_LocalizeForm(rvActionsResource);
  // Localizing all actions on this form
  RVA_LocalizeForm(Self);
  // Localizing ruler
  RVALocalizeRuler(RVRuler1);
  // Localizing units
  {
  // TCoolBar is not Unicode (it displays ??? for Hindi), so we commented this code.
  Index := cmbUnits.ItemIndex;
  RVA_TranslateUnits(cmbUnits.Items);
  cmbUnits.ItemIndex := Index;
  for i := 0 to CoolBar1.Bands.Count-1 do
    if CoolBar1.Bands[i].Control = tbUnits then
      CoolBar1.Bands[i].Text := RVA_GetS(rvam_lbl_Units);
  }
  cmbUnits.Hint := RVA_GetS(rvam_lbl_Units);
  // Styles
  rvActionsResource.rvActionStyleInspector1.UpdateInfo;
  cmbStyles.Font.Charset := RVA_GetCharset;
  cmbStyles.Localize;
  // Localizing menus
  mitFile.Caption := RVA_GetS(rvam_menu_File);
  mitEdit.Caption := RVA_GetS(rvam_menu_Edit);
  mitFont.Caption := RVA_GetS(rvam_menu_Font);
  mitPara.Caption := RVA_GetS(rvam_menu_Para);
  mitFormat.Caption := RVA_GetS(rvam_menu_Format);
  mitInsert.Caption := RVA_GetS(rvam_menu_Insert);
  mitTable.Caption := RVA_GetS(rvam_menu_Table);
  mitExit.Caption := RVA_GetS(rvam_menu_Exit);

  mitFontSize.Caption := RVA_GetS(rvam_menu_FontSize);
  mitFontStyle.Caption := RVA_GetS(rvam_menu_FontStyle);
  mitTableSelect.Caption := RVA_GetS(rvam_menu_TableSelect);
  mitTableCellBorders.Caption := RVA_GetS(rvam_menu_TableCellBorders);
  mitTableAlignCellContents.Caption := RVA_GetS(rvam_menu_TableCellAlign);
  mitTableCellRotation.Caption := RVA_GetS(rvam_menu_TableCellRotation);
  // In your application, you can use either TrvActionFonts or TrvActionFontEx
  rvActionsResource.rvActionFonts1.Caption := rvActionsResource.rvActionFonts1.Caption+' (Standard)';
  rvActionsResource.rvActionFontEx1.Caption := rvActionsResource.rvActionFontEx1.Caption+' (Advanced)';

  {
  // uncomment if you use Addict3. It's assumed that RVAddictSpell31
  // and RVThesaurus31 are on this form.
  RVAddictSpell31.UILanguage := GetAddictSpellLanguage(RVA_GetLanguageName);
  RVThesaurus31.UILanguage := GetAddictThesLanguage(RVA_GetLanguageName);
  }
end;

{-------------------- Progress messages ---------------------------------------}
// Downloading images referred in RTF or HTML (when Indy or CleverComponents are used)
procedure TForm3.RVAControlPanel1Download(Sender: TrvAction;
  const Source: String);
begin
  if Source='' then
    Application.Hint := ''
  else
    Application.Hint := RVAFormat(RVA_GetS(rvam_msg_Downloading), [Source]);
end;
{------------------------------------------------------------------------------}
// Printing
procedure TForm3.RVPrint1SendingToPrinter(Sender: TCustomRichView;
  PageCompleted: Integer; Step: TRVPrintingStep);
begin
  Application.Hint := RVA_GetPrintingMessage(PageCompleted, Step);
end;
{------------------------------------------------------------------------------}
// Reading or writing
procedure TForm3.RichViewEdit1Progress(Sender: TCustomRichView;
  Operation: TRVLongOperation; Stage: TRVProgressStage; PercentDone: Byte);
begin
  case Stage of
    rvpstgStarting:
      begin
        ProgressBar1.Left := Button1.Left - ProgressBar1.Width - 10;
        ProgressBar1.Top :=  Button1.Top + (Button1.Height-ProgressBar1.Height) div 2;
        ProgressBar1.Position := 0;
        ProgressBar1.Visible := True;
        Application.Hint := RVA_GetProgressMessage(Operation);
      end;
    rvpstgRunning:
      begin
        ProgressBar1.Position := PercentDone;
        ProgressBar1.Update;
      end;
    rvpstgEnding:
      begin
        ProgressBar1.Position := 100;
        ProgressBar1.Update;
        Application.Hint := '';
        ProgressBar1.Visible := False;
      end;
  end;
end;
{---------------------- Live spelling with Addict 3 ---------------------------}
(*
// Add these events if you use Addict3
// (Assuming that you have RVAddictSpell31: TRVAddictSpell3 on the form)

// RichViewEdit1.OnSpellingCheck event
procedure TForm3.RichViewEdit1SpellingCheck(Sender: TCustomRichView;
  const AWord: String; StyleNo: Integer; var Misspelled: Boolean);
begin
  Misspelled := not RVAddictSpell31.WordAcceptable(AWord);
end;

// RVAddictSpell31.OnParserIgnoreWord: if Ignore All or Add buttons were pressed
// in spellchecker dialog, removing underlines from the ignored word
procedure TForm3.RVAddictSpell31ParserIgnoreWord(Sender: TObject;
  Before: Boolean; State: Integer);
begin
  if not Before and (State in [IgnoreState_IgnoreAll, IgnoreState_Add]) then
    RichViewEdit1.LiveSpellingValidateWord(RVAddictSpell31.CurrentWord);
end;

// Besides, if you want spelling check starting when loading document, call
// RichViewEdit1.StartLiveSpelling in rvActionFileOpen.OnOpenFile event
*)

procedure TForm3.cmbUnitsChange(Sender: TObject);
begin
// --- 1. Units displayed to the user
  // units for displaying in dialogs
  RVAControlPanel1.UnitsDisplay := TRVUnits(cmbUnits.ItemIndex);
  // units for displaying in the ruler
  RVRuler1.UnitsDisplay := TRulerUnits(cmbUnits.ItemIndex);
  // units for page setup dialog
  if RVAControlPanel1.UnitsDisplay in [rvuPixels, rvuCentimeters, rvuMillimeters] then
    rvActionsResource.rvActionPageSetup1.MarginsUnits := rvpmuMillimeters
  else
    rvActionsResource.rvActionPageSetup1.MarginsUnits := rvpmuInches;
// --- 2. units for internal measurement
  // units of measurement for RichViewActions
  if RVAControlPanel1.UnitsDisplay=rvuPixels then begin
    RVA_ConvertToPixels(rvActionsResource);
    RVAControlPanel1.UnitsProgram := rvstuPixels;
    end
  else begin
    RVA_ConvertToTwips(rvActionsResource);
    RVAControlPanel1.UnitsProgram := rvstuTwips;
  end;
  // units of measurement for the current document
  if RVStyle1.Units<>RVAControlPanel1.UnitsProgram then begin
    RichViewEdit1.ConvertDocToDifferentUnits(RVAControlPanel1.UnitsProgram);
    RVStyle1.ConvertToDifferentUnits(RVAControlPanel1.UnitsProgram);
    RichViewEdit1.Change;
  end;
  RichViewEdit1.SetFocus;
end;
{------------------------------- Notes ----------------------------------------}
procedure TForm3.rvActionEditNoteFormCreate(Sender: TrvAction;
  Form: TForm);
begin
  rvActionsResource.rvActionEditNote1.SubDocEditor.OnEnter := NoteEditorEnter;
  rvActionsResource.rvActionEditNote1.SubDocEditor.OnExit := NoteEditorExit;
  rvActionsResource.rvActionEditNote1.SubDocEditor.PopupMenu := RVAPopupMenu1;
  Form.Align := alBottom;
  Form.Height := 100;
  Form.Parent := Self;
end;

procedure TForm3.rvActionEditNoteShowing(Sender: TrvAction; Form: TForm);
begin
  RichViewEdit1.ForceFieldHighlight := True;
end;

procedure TForm3.rvActionEditNoteHide(Sender: TObject);
begin
  RichViewEdit1.ForceFieldHighlight := False;
  if RichViewEdit1.CanFocus then
    RichViewEdit1.SetFocus;
end;

procedure TForm3.NoteEditorEnter(Sender: TObject);
begin
  RVAControlPanel1.DefaultControl := rvActionsResource.rvActionEditNote1.SubDocEditor;
  UpdateLinkedControls;
end;

procedure TForm3.NoteEditorExit(Sender: TObject);
begin
  RVAControlPanel1.DefaultControl := RichViewEdit1;
  UpdateLinkedControls;
end;

procedure TForm3.UpdateLinkedControls;
begin
  rvActionsResource.rvActionStyleInspector1.Control := RVAControlPanel1.DefaultControl;
  cmbFont.Editor := RVAControlPanel1.DefaultControl;
  cmbFontSize.Editor := RVAControlPanel1.DefaultControl;
  cmbStyles.Editor := RVAControlPanel1.DefaultControl;
end;

initialization

{$IFDEF USE_PNGOBJECT}
  RegisterClass(TPngObject);
  RVGraphicHandler.RegisterHTMLGraphicFormat(TPngObject);
  RVGraphicHandler.RegisterPngGraphic(TPngObject);
{$ENDIF}

end.