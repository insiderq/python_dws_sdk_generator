{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Insert-symbol dialog                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Thanks to Wim van der Vegt for implementing Unicode blocks }

unit InsSymbolRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  {$IFDEF RICHVIEWDEFXE2}
  Vcl.Themes,
  {$ENDIF}
  Dialogs, BaseRVFrm, RVFontCombos, StdCtrls, ExtCtrls, RVALocalize,
  RVGrids, RVTypes, RVUni;

const 
  CM_DENYSUBCLASSING = CM_BASE + 2000; 

type 

  TZoomPanel = class (TPanel)
  private
    procedure WMNCHitTest(var Message: TWMNCHitTest); message WM_NCHITTEST;
    procedure CMDenySubclassing(var Msg: TMessage); message CM_DENYSUBCLASSING;
  protected
    procedure Paint; override;
  public
    DefWidth: Integer;
    FontName: String;
    FontCharset: TFontCharset;
    AlwaysUnicode: Boolean;
    TextA: TRVAnsiString;
    TextW: TRVUnicodeString;
  end;

  TfrmRVInsertSymbol = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    lblCharset: TLabel;
    lblFont: TLabel;
    cmbFont: TRVFontComboBox;
    cmbCharset: TRVFontCharsetComboBox;
    dg: TRVGrid;
    Label1: TLabel;
    cmbBlock: TComboBox;
    lblBlock: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure dgDrawCell(Sender: TObject; ACol, ARow: Integer; ARect: TRect; 
      Selected: Boolean); 
    procedure cmbFontClick(Sender: TObject); 
    procedure cmbCharsetClick(Sender: TObject);
    procedure dgSelectCell(Sender: TObject);
    procedure dgTopLeftChanged(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dgEnter(Sender: TObject);
    procedure cmbBlockClick(Sender: TObject);
    procedure dgDblClick(Sender: TObject);
  private
    { Private declarations }
    FUseBlocks: Boolean;
    FontHeight: Integer;
    FontName: String;
    FontCharset: TFontCharset;
    Offset: Integer;
    Endpos: Integer;
    procedure AdjustControlsCoords;
    procedure CalcFontHeight;
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    function IsThemeAllowedFor(Component: TComponent): Boolean; override;
    {$ENDIF}
    procedure CreateParams(var Params: TCreateParams);override;
  public
    { Public declarations }
    Panel: TZoomPanel;
    FAlwaysUnicode: Boolean;
    _btnOk, _lblCharset, _lblBlock, _lblFont, _Label1: TControl;
    procedure Init(Char: Word; const AFontName: String; ACharset: TFontCharset;
      AlwaysUnicode: Boolean);
    procedure GetInfo(var Char: Word; var AFontName: String; var ACharset: TFontCharset);
    procedure SetOptions(AllowUnicode, AllowANSI, UseBlocks: Boolean);
    procedure Localize; override;
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;


implementation

uses math, RichViewActions; // for Min()

{$R *.dfm}

//See - ftp://ftp.unicode.org/Public/UNIDATA/blocks.txt

type
  uniblock = record 
               s,e : Integer; 
               n   : string; 
  end; 

const 
  blocks     = 121; 
  uniblocks  : array[0..blocks] OF uniblock = ( 
                                              (s:$0020;e:$FFFF;n:'All'), 
                                              (s:$0020;e:$007F;n:'Basic Latin'),
                                              (s:$0080;e:$00FF;n:'Latin-1 Supplement'), 
                                              (s:$0100;e:$017F;n:'Latin Extended-A'), 
                                              (s:$0180;e:$024F;n:'Latin Extended-B'), 
                                              (s:$0250;e:$02AF;n:'IPA Extensions'), 
                                              (s:$02B0;e:$02FF;n:'Spacing Modifier Letters'), 
                                              (s:$0300;e:$036F;n:'Combining Diacritical Marks'), 
                                              (s:$0370;e:$03FF;n:'Greek and Coptic'), 
                                              (s:$0400;e:$04FF;n:'Cyrillic'), 
                                              (s:$0500;e:$052F;n:'Cyrillic Supplement'), 
                                              (s:$0530;e:$058F;n:'Armenian'), 
                                              (s:$0590;e:$05FF;n:'Hebrew'), 
                                              (s:$0600;e:$06FF;n:'Arabic'),
                                              (s:$0700;e:$074F;n:'Syriac'), 
                                              (s:$0750;e:$077F;n:'Arabic Supplement'), 
                                              (s:$0780;e:$07BF;n:'Thaana'), 
                                              (s:$0900;e:$097F;n:'Devanagari'), 
                                              (s:$0980;e:$09FF;n:'Bengali'), 
                                              (s:$0A00;e:$0A7F;n:'Gurmukhi'), 
                                              (s:$0A80;e:$0AFF;n:'Gujarati'), 
                                              (s:$0B00;e:$0B7F;n:'Oriya'), 
                                              (s:$0B80;e:$0BFF;n:'Tamil'), 
                                              (s:$0C00;e:$0C7F;n:'Telugu'), 
                                              (s:$0C80;e:$0CFF;n:'Kannada'), 
                                              (s:$0D00;e:$0D7F;n:'Malayalam'),
                                              (s:$0D80;e:$0DFF;n:'Sinhala'),
                                              (s:$0E00;e:$0E7F;n:'Thai'),
                                              (s:$0E80;e:$0EFF;n:'Lao'), 
                                              (s:$0F00;e:$0FFF;n:'Tibetan'), 
                                              (s:$1000;e:$109F;n:'Myanmar'), 
                                              (s:$10A0;e:$10FF;n:'Georgian'), 
                                              (s:$1100;e:$11FF;n:'Hangul Jamo'), 
                                              (s:$1200;e:$137F;n:'Ethiopic'), 
                                              (s:$1380;e:$139F;n:'Ethiopic Supplement'), 
                                              (s:$13A0;e:$13FF;n:'Cherokee'), 
                                              (s:$1400;e:$167F;n:'Unified Canadian Aboriginal Syllabics'), 
                                              (s:$1680;e:$169F;n:'Ogham'), 
                                              (s:$16A0;e:$16FF;n:'Runic'), 
                                              (s:$1700;e:$171F;n:'Tagalog'), 
                                              (s:$1720;e:$173F;n:'Hanunoo'), 
                                              (s:$1740;e:$175F;n:'Buhid'), 
                                              (s:$1760;e:$177F;n:'Tagbanwa'), 
                                              (s:$1780;e:$17FF;n:'Khmer'), 
                                              (s:$1800;e:$18AF;n:'Mongolian'), 
                                              (s:$1900;e:$194F;n:'Limbu'), 
                                              (s:$1950;e:$197F;n:'Tai Le'), 
                                              (s:$1980;e:$19DF;n:'New Tai Lue'), 
                                              (s:$19E0;e:$19FF;n:'Khmer Symbols'), 
                                              (s:$1A00;e:$1A1F;n:'Buginese'), 
                                              (s:$1D00;e:$1D7F;n:'Phonetic Extensions'), 
                                              (s:$1D80;e:$1DBF;n:'Phonetic Extensions Supplement'), 
                                              (s:$1DC0;e:$1DFF;n:'Combining Diacritical Marks Supplement'), 
                                              (s:$1E00;e:$1EFF;n:'Latin Extended Additional'), 
                                              (s:$1F00;e:$1FFF;n:'Greek Extended'), 
                                              (s:$2000;e:$206F;n:'General Punctuation'), 
                                              (s:$2070;e:$209F;n:'Superscripts and Subscripts'), 
                                              (s:$20A0;e:$20CF;n:'Currency Symbols'), 
                                              (s:$20D0;e:$20FF;n:'Combining Diacritical Marks for Symbols'), 
                                              (s:$2100;e:$214F;n:'Letterlike Symbols'),
                                              (s:$2150;e:$218F;n:'Number Forms'), 
                                              (s:$2190;e:$21FF;n:'Arrows'), 
                                              (s:$2200;e:$22FF;n:'Mathematical Operators'), 
                                              (s:$2300;e:$23FF;n:'Miscellaneous Technical'), 
                                              (s:$2400;e:$243F;n:'Control Pictures'), 
                                              (s:$2440;e:$245F;n:'Optical Character Recognition'),
                                              (s:$2460;e:$24FF;n:'Enclosed Alphanumerics'), 
                                              (s:$2500;e:$257F;n:'Box Drawing'),
                                              (s:$2580;e:$259F;n:'Block Elements'), 
                                              (s:$25A0;e:$25FF;n:'Geometric Shapes'), 
                                              (s:$2600;e:$26FF;n:'Miscellaneous Symbols'), 
                                              (s:$2700;e:$27BF;n:'Dingbats'), 
                                              (s:$27C0;e:$27EF;n:'Miscellaneous Mathematical Symbols-A'), 
                                              (s:$27F0;e:$27FF;n:'Supplemental Arrows-A'), 
                                              (s:$2800;e:$28FF;n:'Braille Patterns'), 
                                              (s:$2900;e:$297F;n:'Supplemental Arrows-B'), 
                                              (s:$2980;e:$29FF;n:'Miscellaneous Mathematical Symbols-B'),
                                              (s:$2A00;e:$2AFF;n:'Supplemental Mathematical Operators'), 
                                              (s:$2B00;e:$2BFF;n:'Miscellaneous Symbols and Arrows'),
                                              (s:$2C00;e:$2C5F;n:'Glagolitic'), 
                                              (s:$2C80;e:$2CFF;n:'Coptic'), 
                                              (s:$2D00;e:$2D2F;n:'Georgian Supplement'), 
                                              (s:$2D30;e:$2D7F;n:'Tifinagh'), 
                                              (s:$2D80;e:$2DDF;n:'Ethiopic Extended'), 
                                              (s:$2E00;e:$2E7F;n:'Supplemental Punctuation'), 
                                              (s:$2E80;e:$2EFF;n:'CJK Radicals Supplement'), 
                                              (s:$2F00;e:$2FDF;n:'Kangxi Radicals'), 
                                              (s:$2FF0;e:$2FFF;n:'Ideographic Description Characters'), 
                                              (s:$3000;e:$303F;n:'CJK Symbols and Punctuation'), 
                                              (s:$3040;e:$309F;n:'Hiragana'), 
                                              (s:$30A0;e:$30FF;n:'Katakana'), 
                                              (s:$3100;e:$312F;n:'Bopomofo'), 
                                              (s:$3130;e:$318F;n:'Hangul Compatibility Jamo'), 
                                              (s:$3190;e:$319F;n:'Kanbun'),
                                              (s:$31A0;e:$31BF;n:'Bopomofo Extended'), 
                                              (s:$31C0;e:$31EF;n:'CJK Strokes'), 
                                              (s:$31F0;e:$31FF;n:'Katakana Phonetic Extensions'), 
                                              (s:$3200;e:$32FF;n:'Enclosed CJK Letters and Months'), 
                                              (s:$3300;e:$33FF;n:'CJK Compatibility'), 
                                              (s:$3400;e:$4DBF;n:'CJK Unified Ideographs Extension A'), 
                                              (s:$4DC0;e:$4DFF;n:'Yijing Hexagram Symbols'), 
                                              (s:$4E00;e:$9FFF;n:'CJK Unified Ideographs'), 
                                              (s:$A000;e:$A48F;n:'Yi Syllables'), 
                                              (s:$A490;e:$A4CF;n:'Yi Radicals'), 
                                              (s:$A700;e:$A71F;n:'Modifier Tone Letters'), 
                                              (s:$A800;e:$A82F;n:'Syloti Nagri'), 
                                              (s:$AC00;e:$D7AF;n:'Hangul Syllables'), 
                                              (s:$D800;e:$DB7F;n:'High Surrogates'), 
                                              (s:$DB80;e:$DBFF;n:'High Private Use Surrogates'),
                                              (s:$DC00;e:$DFFF;n:'Low Surrogates'), 
                                              (s:$E000;e:$F8FF;n:'Private Use Area'), 
                                              (s:$F900;e:$FAFF;n:'CJK Compatibility Ideographs'), 
                                              (s:$FB00;e:$FB4F;n:'Alphabetic Presentation Forms'), 
                                              (s:$FB50;e:$FDFF;n:'Arabic Presentation Forms-A'), 
                                              (s:$FE00;e:$FE0F;n:'Variation Selectors'), 
                                              (s:$FE10;e:$FE1F;n:'Vertical Forms'), 
                                              (s:$FE20;e:$FE2F;n:'Combining Half Marks'),
                                              (s:$FE30;e:$FE4F;n:'CJK Compatibility Forms'),
                                              (s:$FE50;e:$FE6F;n:'Small Form Variants'), 
                                              (s:$FE70;e:$FEFF;n:'Arabic Presentation Forms-B'), 
                                              (s:$FF00;e:$FFEF;n:'Halfwidth and Fullwidth Forms'), 
                                              (s:$FFF0;e:$FFFF;n:'Specials')); 

procedure TfrmRVInsertSymbol.FormCreate(Sender: TObject); 
var 
  i: Integer;
begin
  AdjustControlsCoords;
  _btnOk := btnOk;
  _lblCharset := lblCharset;
  _lblBlock := lblBlock;
  _lblFont := lblFont;
  _Label1 := Label1;

  Offset:=$0020;
  Endpos:=$FFFF;

//unicode blocks
  cmbBlock.Items.BeginUpdate;
  cmbBlock.Clear;
  for i:=0 to blocks do
    cmbBlock.Items.Add(uniblocks[i].n);
  cmbBlock.ItemIndex:=0;
  cmbBlock.Items.EndUpdate;
  //Label1.Font.Style := [fsBold];

  inherited;
  // defaults are not needed
  //dg.DefaultColWidth := (dg.Width-4-GetSystemMetrics(SM_CXVSCROLL)) div (dg.ColCount+1);
  //dg.DefaultRowHeight := (dg.ClientHeight) div (dg.RowCount+1);
  //dg.Height := dg.RowCount*(dg.DefaultRowHeight+1)+(dg.Height-dg.ClientHeight);

  Panel := TZoomPanel.Create(Self);
  Panel.Parent := Self;
  Panel.Visible := False;

  Init(183, 'Symbol', SYMBOL_CHARSET, False);
  CalcFontHeight;
end;

procedure TfrmRVInsertSymbol.Init(Char: Word; const AFontName: String;
  ACharset: TFontCharset; AlwaysUnicode: Boolean);
var idx: Integer;
begin
  FAlwaysUnicode := AlwaysUnicode;
  Panel.AlwaysUnicode := AlwaysUnicode;
  cmbFont.ItemIndex := cmbFont.Items.IndexOf(AFontName);
  if cmbFont.ItemIndex<0 then 
    cmbFont.ItemIndex := 0; 
  cmbFontClick(nil);
  idx := cmbCharset.IndexOfCharset(ACharset); 
  if idx>=0 then 
    cmbCharset.ItemIndex := idx;
  cmbCharsetClick(nil); 
  if Char=0 then
    Char := offset;
  dec(Char, offset); 
  dg.SelectCell(Char mod 28, Char div 28);
end;

procedure TfrmRVInsertSymbol.SetOptions(AllowUnicode, AllowANSI,
  UseBlocks: Boolean); 
var idx: Integer; 
begin
  FUseBlocks := UseBlocks;
  if not AllowANSI then begin 
    idx := cmbCharset.IndexOfCharset(DEFAULT_CHARSET);
    if idx>=0 then 
      cmbCharset.ItemIndex := idx; 
    cmbCharset.Visible := False; 
    _lblCharset.Visible := False;
    _lblBlock.Visible := UseBlocks;
    cmbBlock.Visible := UseBlocks;
    _lblBlock.Left := _lblFont.Left;
    cmbBlock.Left := cmbFont.Left;
  end;
  if not AllowUnicode then begin
    cmbCharset.AddDefaultCharset := False;
    _lblBlock.Visible := False;
    cmbBlock.Visible := False;
  end;
  cmbCharsetClick(nil);
end;

procedure TfrmRVInsertSymbol.GetInfo(var Char: Word;
  var AFontName: String; var ACharset: TFontCharset);
begin
  Char := offset+{32+}dg.Row*dg.ColCount+dg.Col;
  AFontName := FontName;
  ACharset  := FontCharset;
end;

function TfrmRVInsertSymbol.GetMyClientSize(var Width,
  Height: Integer): Boolean;
begin
  Width := dg.Left*2+dg.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-dg.Top-dg.Height);
  Result := True;
end;

procedure TfrmRVInsertSymbol.dgDrawCell(Sender: TObject; ACol,
  ARow: Integer; ARect: TRect; Selected: Boolean);
var sz: TSize;
    ansis: TRVAnsiString;
    ws: TRVUnicodeString;
begin
  dg.Canvas.FillRect(ARect);
  dg.Canvas.Font.Name := FontName;
  dg.Canvas.Font.Charset := FontCharset;
  dg.Canvas.Font.Style := [];
  dg.Canvas.Font.Height := FontHeight;
  offset:=$0020;
  endpos:=$FFFF;
  if FontCharset<>DEFAULT_CHARSET then
    if FAlwaysUnicode then begin
      // displaying converted to Unicode... a difference is possible for SYMBOL_CHARSET
      ws := RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(
        RVU_Charset2CodePage(FontCharset), TRVAnsiChar(offset+ARow*dg.ColCount+ACol)));
      GetTextExtentPoint32W(dg.Canvas.Handle, PRVUnicodeChar(ws), 1, sz);
      sz.cx := (ARect.Right-ARect.Left-sz.cx) div 2 + ARect.Left;
      sz.cy := (ARect.Bottom-ARect.Top-sz.cy) div 2 + ARect.Top;
      TextOutW(dg.Canvas.Handle, sz.cx, sz.cy, PRVUnicodeChar(ws), 1);
      end
    else begin
      // displaying ANSI
      ansis := TRVAnsiChar(offset+ARow*dg.ColCount+ACol);
      GetTextExtentPoint32A(dg.Canvas.Handle, PRVAnsiChar(ansis), 1, sz);
      sz.cx := (ARect.Right-ARect.Left-sz.cx) div 2 + ARect.Left;
      sz.cy := (ARect.Bottom-ARect.Top-sz.cy) div 2 + ARect.Top;
      TextOutA(dg.Canvas.Handle, sz.cx, sz.cy, PRVAnsiChar(ansis), 1);
    end
  else begin
    //unicode blocks
    Offset:=uniBlocks[cmbBlock.ItemIndex].s;
    EndPos:=uniBlocks[cmbBlock.ItemIndex].e;
    if offset+{32+}ARow*dg.ColCount+ACol>endpos then
      exit;
    ws := WideChar(offset+{32+}ARow*dg.ColCount+ACol);
    GetTextExtentPoint32W(dg.Canvas.Handle, PRVUnicodeChar(ws), 1, sz);
    sz.cx := (ARect.Right-ARect.Left-sz.cx) div 2 + ARect.Left;
    sz.cy := (ARect.Bottom-ARect.Top-sz.cy) div 2 + ARect.Top;
    TextOutW(dg.Canvas.Handle, sz.cx, sz.cy, PRVUnicodeChar(ws), 1);
  end;
end;

procedure TfrmRVInsertSymbol.cmbFontClick(Sender: TObject);
begin
  if cmbFont.ItemIndex<0 then
    exit;
  dg.repaint;
  FontName := cmbFont.Items[cmbFont.ItemIndex];
  if Panel<>nil then begin
    Panel.FontName := FontName;
    Panel.Refresh;
  end;
  cmbCharset.FontName := FontName;
end;

procedure TfrmRVInsertSymbol.CreateParams(var Params: TCreateParams);
begin
  inherited;
  {$IFDEF RVACTIVEX}
  Params.Style := Params.Style or WS_POPUP;
  {$ENDIF}
end;

procedure TfrmRVInsertSymbol.cmbCharsetClick(Sender: TObject);
var rowcount: Integer;
begin
  FontCharset := cmbCharset.Charsets[cmbCharset.ItemIndex];
  if Panel<>nil then
    Panel.FontCharset := FontCharset;
  if FontCharset=DEFAULT_CHARSET then
    begin
      Offset:=uniBlocks[cmbBlock.ItemIndex].s;
      EndPos:=uniBlocks[cmbBlock.ItemIndex].e;
      rowcount := (EndPos-Offset+1) div dg.ColCount;
      if (EndPos-Offset+1) mod dg.ColCount>0 then
        inc(rowcount);
      dg.RowCount := rowcount;
    end
  else
    dg.RowCount := 8;
  cmbBlock.Visible:=(FontCharset=DEFAULT_CHARSET) and FUseBlocks;
  _lblBlock.Visible:=(FontCharset=DEFAULT_CHARSET) and FUseBlocks;
  dgSelectCell(dg);
end; 

procedure TfrmRVInsertSymbol.dgSelectCell(Sender: TObject); 
var 
  ARow, ACol: Integer; 
  ARect: TRect; 
begin 
  _btnOk.Enabled := True;
  ARow := Max(dg.Row, 0); 
  ACol := Max(dg.Col, 0); 
  if FontCharset<>DEFAULT_CHARSET then
    SetControlCaption(_Label1, RVAFormat(RVA_GetS(rvam_is_CharCode, ControlPanel), [offset+{32+}ARow*dg.ColCount+ACol]))
  else if offset+{32+}ARow*dg.ColCount+ACol>endpos then begin
    SetControlCaption(_Label1, RVA_GetS(rvam_is_NoChar, ControlPanel));
    _btnOk.Enabled := False;
    end
  else
    SetControlCaption(_Label1, RVAFormat(RVA_GetS(rvam_is_UCharCode, ControlPanel), [{32+}Offset+ARow*dg.ColCount+ACol]));
  if Panel=nil then 
    exit; 
  if FontCharset<>DEFAULT_CHARSET then begin
    if FAlwaysUnicode then
      Panel.TextW := RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(
        RVU_Charset2CodePage(FontCharset), TRVAnsiChar(offset+ARow*dg.ColCount+ACol)))
    else
      Panel.TextA := TRVAnsiChar(offset+{32+}ARow*dg.ColCount+ACol)
    end
  else begin
    if {32+}Offset+ARow*dg.ColCount+ACol>endpos then
      Panel.TextW := ''
    else
      Panel.TextW := TRVUnicodeChar({32+}Offset+ARow*dg.ColCount+ACol);
  end; 
  // The code below replaces the huge .SetBounds that was here before
  ARect := dg.GetCellRect(ACol, ARow);
  InflateRect(ARect, 10, 10); 
  OffsetRect(ARect, dg.Left, dg.Top);
  Panel.DefWidth := ARect.Right - ARect.Left;
  with ARect do Panel.SetBounds(Left, Top, Right - Left, Bottom - Top);
  // --- 
  Panel.Refresh; 
end;

{ TZoomPanel } 

procedure TZoomPanel.CMDenySubclassing(var Msg: TMessage); 
begin 
  Msg.Result := 1; 
end; 

procedure TZoomPanel.Paint; 
var r: TRect; 
    sz: TSize; 
    w: Integer;
    {$IFDEF RICHVIEWDEFXE2}
    Color: TColor;
    {$ENDIF}
begin
  r := ClientRect;
  {$IFDEF RICHVIEWDEFXE2}
  if ThemeControl(Self) and
    StyleServices.DrawElement(Canvas.Handle,
     StyleServices.GetElementDetails(tgCellSelected), r) then begin
    Canvas.Brush.Color := clNone;
    Canvas.Brush.Style := bsClear;
    Canvas.Pen.Color := StyleServices.GetSystemColor(clActiveBorder);
    end
  else
  {$ENDIF}
  begin
    Canvas.Brush.Color := clHighlight;
    Canvas.Pen.Color := clActiveBorder;
  end;
  Canvas.Rectangle(0,0,r.Right,r.Bottom);

  Canvas.Brush.Color := clNone;
  Canvas.Brush.Style := bsClear;

  Canvas.Font.Height := Height-10;
  Canvas.Font.Name := FontName;
  Canvas.Font.Charset := FontCharset;
  Canvas.Font.Color := clHighlightText;

   {$IFDEF RICHVIEWDEFXE2}
  if ThemeControl(Self) and
    StyleServices.GetElementColor(
     StyleServices.GetElementDetails(tgCellSelected), ecTextColor, Color) and
     (Color<>clNone) then
    Canvas.Font.Color := Color;
  {$ENDIF}
  if (FontCharset<>DEFAULT_CHARSET) and not AlwaysUnicode then begin
    GetTextExtentPoint32A(Canvas.Handle, PRVAnsiChar(TextA), Length(TextA), sz);
    w := sz.cx;
    sz.cx := (ClientWidth-sz.cx) div 2;
    sz.cy := (ClientHeight-sz.cy) div 2;
    TextOutA(Canvas.Handle, sz.cx, sz.cy, PRVAnsiChar(TextA), Length(TextA));
    end
  else begin
    GetTextExtentPoint32W(Canvas.Handle, PRVUnicodeChar(TextW), Length(TextW), sz);
    w := sz.cx;
    sz.cx := (ClientWidth-sz.cx) div 2;
    sz.cy := (ClientHeight-sz.cy) div 2;
    TextOutW(Canvas.Handle, sz.cx, sz.cy, PRVUnicodeChar(TextW), Length(TextW));
  end;
  if w > DefWidth then 
    Width := w; 
{  else 
    Width := DefWidth;} 
end; 

procedure TZoomPanel.WMNCHitTest(var Message: TWMNCHitTest); 
begin 
  Message.Result := HTTRANSPARENT; 
end; 

procedure TfrmRVInsertSymbol.Localize;
var r: Integer;
begin
  inherited;
  Caption := RVA_GetS(rvam_is_Title, ControlPanel);
  btnOk.Caption := RVA_GetSAsIs(rvam_btn_Insert, ControlPanel);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, ControlPanel);
  lblFont.Caption := RVA_GetSAsIs(rvam_is_Font, ControlPanel);
  lblCharset.Caption := RVA_GetSAsIs(rvam_is_Charset, ControlPanel);
  lblBlock.Caption := RVA_GetSAsIs(rvam_is_Block, ControlPanel);
  cmbCharset.DefaultCharsetCaption := RVA_GetS(rvam_is_Unicode, ControlPanel);
  r := cmbBlock.Left+cmbBlock.Width;
  cmbBlock.Left := lblBlock.Left+lblBlock.Width+10;
  cmbBlock.Width := r-cmbBlock.Left;
end;

procedure TfrmRVInsertSymbol.dgTopLeftChanged(Sender: TObject);
begin
  dgSelectCell(dg); // actually, onSelectCell is called twice... 
                    // see DoTopLeftChanged first two "if"s
                    // but it doesn't matter IMHO 
end; 

procedure TfrmRVInsertSymbol.FormResize(Sender: TObject); 
begin 
  dgSelectCell(dg); // to adjust Zoompanel position
  CalcFontHeight;
end; 

procedure TfrmRVInsertSymbol.dgEnter(Sender: TObject); 
begin 
  Panel.Visible := True; // also to adjust Zoompanel position 
end; 

{$IFDEF RVASKINNED}
procedure TfrmRVInsertSymbol.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl = _btnOk then
    _btnOk := NewControl
  else if OldControl = _lblCharset then
    _lblCharset := NewControl
  else if OldControl = _lblBlock then
    _lblBlock := NewControl
  else if OldControl = _lblFont then
    _lblFont := NewControl
  else if OldControl = _Label1 then
    _Label1 := NewControl;
end;

function TfrmRVInsertSymbol.IsThemeAllowedFor(Component: TComponent): Boolean;
begin
  if Component=cmbBlock then
    Result := False
  else
    Result := inherited IsThemeAllowedFor(Component);
end;
{$ENDIF}

procedure TfrmRVInsertSymbol.cmbBlockClick(Sender: TObject);
begin
  //unicode blocks
  if FontCharset=DEFAULT_CHARSET then begin
    dg.SelectCell(0, 0);
    cmbCharsetClick(nil);
  end;
end;

procedure TfrmRVInsertSymbol.CalcFontHeight;
begin
  Canvas.Font := dg.Font;
  Canvas.Font.Height := (dg.ClientHeight div dg.RowsVisible - 4);
  while (Canvas.Font.Height>3) and (Canvas.TextWidth('W')>dg.ClientWidth div dg.ColCount) do
    Canvas.Font.Height := Canvas.Font.Height-1;
  FontHeight := Canvas.Font.Height;
end;

procedure TfrmRVInsertSymbol.AdjustControlsCoords;
begin
  OnResize := nil;
  dg.Width := ClientWidth-dg.Left*2;
  cmbBlock.Left := dg.Left+dg.Width-cmbBlock.Width;
  btnCancel.Left := dg.Left+dg.Width-btnCancel.Width;
  btnOk.Left := btnCancel.Left-btnOk.Width-5;
  OnResize := FormResize;
  btnCancel.Top := ClientHeight - btnCancel.Height - 10;
  btnOk.Top := btnCancel.Top;
  Label1.Top := btnCancel.Top+(btnCancel.Height-Label1.Height) div 2;
  dg.Height :=  btnCancel.Top - dg.Top - 10;
end;

procedure TfrmRVInsertSymbol.dgDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.