﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'BackRVFrm.pas' rev: 27.00 (Windows)

#ifndef BackrvfrmHPP
#define BackrvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Backrvfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVBack;
class PASCALIMPLEMENTATION TfrmRVBack : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Controls::TImageList* il;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rg;
	Vcl::Stdctrls::TGroupBox* gbBack;
	Richview::TRichView* rv;
	Rvcolorcombo::TRVColorCombo* cmbColor;
	Vcl::Stdctrls::TLabel* lblColor;
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Stdctrls::TButton* btnOpen;
	Vcl::Stdctrls::TButton* btnSave;
	Vcl::Stdctrls::TButton* btnClear;
	Vcl::Extdlgs::TSavePictureDialog* spd;
	Vcl::Stdctrls::TButton* btnMargins;
	Rvstyle::TRVStyle* RVStyle1;
	void __fastcall btnSaveClick(System::TObject* Sender);
	void __fastcall btnOpenClick(System::TObject* Sender);
	void __fastcall btnClearClick(System::TObject* Sender);
	void __fastcall rgClick(System::TObject* Sender);
	void __fastcall cmbColorColorChange(System::TObject* Sender);
	void __fastcall btnMarginsClick(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	
private:
	Vcl::Controls::TControl* _btnSave;
	Vcl::Controls::TControl* _btnClear;
	Vcl::Controls::TControl* _btnMargins;
	Richview::TRichView* _rv;
	Vcl::Actnlist::TAction* FAction;
	
public:
	System::UnicodeString ImageFileName;
	bool ImageChanged;
	void __fastcall Init(Rvedit::TCustomRichViewEdit* rve, Vcl::Dialogs::TColorDialog* cd, Vcl::Actnlist::TAction* Action, bool CanChangeMargins);
	void __fastcall GetResult(Rvedit::TCustomRichViewEdit* rv);
	DYNAMIC void __fastcall Localize(void);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVBack(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVBack(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVBack(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVBack(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Backrvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BACKRVFRM)
using namespace Backrvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BackrvfrmHPP
