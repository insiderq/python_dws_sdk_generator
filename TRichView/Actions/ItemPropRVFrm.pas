
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Object properties dialog                        }
{                                                       }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit ItemPropRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

{$R rvabox.res}
{$R rvacell.res}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, ActnList,
  Dialogs, BaseRVFrm, ComCtrls, StdCtrls, ImgList, ExtCtrls, RVSpinEdit,
  {$IFNDEF RVDONOTUSESEQ}
  RVSeqItem, RVNote, RVSidenote, RVFloatingPos, RVFloatingBox,
  {$ENDIF}
  {$IFDEF USERVKSDEVTE}
  te_controls, te_extctrls,
  {$ENDIF}
  {$IFDEF USERVTNT}
  TntComCtrls, TntStdCtrls, TntButtons,
  {$ENDIF}
  {$IFDEF RICHVIEWDEFXE2}
  Vcl.Themes,
  {$ENDIF}
  RVColorCombo, CRVData, RVItem, RVEdit, RVFuncs, RVStyle, ExtDlgs,
  RVOfficeRadioBtn, Buttons, RVTable, RVALocalize, RVTypes, RVWinGrIn,
  RVGrHandler;

type
  TRVImagePropRecord = record
    cmbVShiftItemIndex: Integer;
    LastChangedIsHeight, SizeInPixels, MakingProportional: Boolean;
    OriginalImage: TGraphic;
    ImageWidth, ImageHeight: Integer;
    ImageFileName: String;
    ImageFileNameChanged: Boolean;
    BorderWidth: TRVStyleLength;
  end;

  TRVTablePropRecord = record
    ImageFileName: String;
    ImageFileNameChanged: Boolean;
    TableWidth, CellWidth: TRVLength;
    TableWidthPrc, CellWidthPrc: Integer;
    CellHSpacing, CellVSpacing, BorderHSpacing, BorderVSpacing: TRVStyleLength;
    BorderStyle: TRVTableBorderStyle;
    BorderColorD, BorderColorL: TColor;
    CellBorderStyle: TRVTableBorderStyle;
    CellBorderColorD, CellBorderColorL: TColor;
    BorderWidth, CellBorderWidth: TRVStyleLength;
    FDefCellColorD, FDefCellColorL: Boolean;
    TableBackgroundImage: TGraphic;
    TableBackgroundStyle: TRVItemBackgroundStyle;
    TableBackgroundChanged: Boolean;
    CellBackgroundImage: TGraphic;
    CellBackgroundStyle: TRVItemBackgroundStyle;
    CellBackgroundChanged: Boolean;
    CellImageFileName: String;
    TableVBLeft, TableVBRight, TableVBTop, TableVBBottom,
    CellVBLeft, CellVBRight, CellVBTop, CellVBBottom,
    CellDefVBLeft, CellDefVBRight, CellDefVBTop, CellDefVBBottom: Boolean;
  end;

  TRVBreakPropRecord = record
    Width: TRVStyleLength;
  end;

  {$IFNDEF RVDONOTUSESEQ}
  TRVBoxPropRecord = record
    Height, Width, HOffs, VOffs: TRVLength;
    HeightPct, WidthPct, HOffsPct, VOffsPct: Extended;
    Border: TRVBorder;
    Background: TRVBackgroundRect;
  end;

  TRVSeqPropRecord = record
    SeqPropList: TRVALocStringList;
  end;
  {$ENDIF}

  TfrmRVItemProp = class(TfrmRVBase)
    btnOk: TButton;
    btnCancel: TButton;
    pc: TPageControl;
    tsImage: TTabSheet;
    Panel1: TPanel;
    tsImgLayout: TTabSheet;
    gbVAlign: TGroupBox;
    lblAlignTo: TLabel;
    cmbAlignTo: TComboBox;
    lblVShift: TLabel;
    seVShift: TRVSpinEdit;
    cmbVShiftType: TComboBox;
    gbSize: TGroupBox;
    seWidth: TRVSpinEdit;
    seHeight: TRVSpinEdit;
    img: TImage;
    btnChangeImage: TButton;
    gbTransp: TGroupBox;
    lblTrColor: TLabel;
    cmbColor: TRVColorCombo;
    cbTransp: TCheckBox;
    lblPreview: TLabel;
    tsBreak: TTabSheet;
    lblWidth: TLabel;
    lblHeight: TLabel;
    lblDefSize: TLabel;
    gbBreak: TGroupBox;
    lblBreakColor: TLabel;
    lblBreakWidth: TLabel;
    cmbBreakColor: TRVColorCombo;
    cmbWidth: TComboBox;
    tsTable: TTabSheet;
    gbTableSize: TGroupBox;
    seTableWidth: TRVSpinEdit;
    cmbTWType: TComboBox;
    ilTable: TImageList;
    lblCellHPadding: TLabel;
    seCellHPadding: TRVSpinEdit;
    seCellVPadding: TRVSpinEdit;
    lblCellVPadding: TLabel;
    btnSpacing: TButton;
    tsCells: TTabSheet;
    gbCellBorder: TGroupBox;
    gbCells: TGroupBox;
    seCellBestWidth: TRVSpinEdit;
    cmbCellBWType: TComboBox;
    seCellBestHeight: TRVSpinEdit;
    rgCellVAlign: TRVOfficeRadioGroup;
    lblTableWidth: TLabel;
    tsRows: TTabSheet;
    rgRowVAlign: TRVOfficeRadioGroup;
    lblCellBestHeightMU: TLabel;
    gbTablePrint: TGroupBox;
    cbNoSplit: TCheckBox;
    lblHeadingRows: TLabel;
    seHRC: TRVSpinEdit;
    cmbBreakStyle: TComboBox;
    lblBreakStyle: TLabel;
    lblCellHPaddingMU: TLabel;
    lblCellVPaddingMU: TLabel;
    lblDefSize2: TLabel;
    tsBoxPosition: TTabSheet;
    gbBoxHPos: TGroupBox;
    cmbBoxHPosition: TComboBox;
    ilBoxAnchor: TImageList;
    rbBoxHPage: TRVOfficeRadioButton;
    seBoxHOffs: TRVSpinEdit;
    rbBoxHLeftMargin: TRVOfficeRadioButton;
    rbBoxHMainTextArea: TRVOfficeRadioButton;
    rbBoxHRightMargin: TRVOfficeRadioButton;
    rbBoxHCharacter: TRVOfficeRadioButton;
    lblBoxHTxt: TLabel;
    lblBoxHMU: TLabel;
    lstBoxPosition: TListBox;
    cmbBoxHAlign: TComboBox;
    gbBoxVPos: TGroupBox;
    lblBoxVTxt: TLabel;
    lblBoxVMU: TLabel;
    cmbBoxVPosition: TComboBox;
    rbBoxVPage: TRVOfficeRadioButton;
    seBoxVOffs: TRVSpinEdit;
    rbBoxVTopMargin: TRVOfficeRadioButton;
    rbBoxVMainTextArea: TRVOfficeRadioButton;
    rbBoxVBottomMargin: TRVOfficeRadioButton;
    rbBoxVLine: TRVOfficeRadioButton;
    cmbBoxVAlign: TComboBox;
    rbBoxVPara: TRVOfficeRadioButton;
    ilBoxZPos: TImageList;
    tsBoxSize: TTabSheet;
    gbBoxHeight: TGroupBox;
    rbBoxHeightPage: TRVOfficeRadioButton;
    rbBoxHeightTopMargin: TRVOfficeRadioButton;
    rbBoxHeightMainTextArea: TRVOfficeRadioButton;
    rbBoxHeightBottomMargin: TRVOfficeRadioButton;
    rgBoxZPos: TRVOfficeRadioGroup;
    cmbBoxHeight: TComboBox;
    seBoxHeight: TRVSpinEdit;
    lblBoxHeightTxt: TLabel;
    gbBoxWidth: TGroupBox;
    lblBoxWidthTxt: TLabel;
    rbBoxWidthPage: TRVOfficeRadioButton;
    rbBoxWidthLeftMargin: TRVOfficeRadioButton;
    rbBoxWidthMainTextArea: TRVOfficeRadioButton;
    rbBoxWidthRightMargin: TRVOfficeRadioButton;
    cmbBoxWidth: TComboBox;
    seBoxWidth: TRVSpinEdit;
    tsBoxContent: TTabSheet;
    rgBoxVAlign: TRVOfficeRadioGroup;
    btnBoxBorder: TButton;
    lblBoxHeight: TLabel;
    lblBoxWidth: TLabel;
    lblBoxAutoHeight: TLabel;
    cbRelativeToCell: TCheckBox;
    cbBoxWidthMirror: TCheckBox;
    cbBoxPosMirror: TCheckBox;
    btnSaveImage: TButton;
    spd: TSavePictureDialog;
    tsImgAppearance: TTabSheet;
    gbImgInsideBorder: TGroupBox;
    lblImgColor: TLabel;
    cmbImgFillColor: TRVColorCombo;
    lblImgSpacing: TLabel;
    seImgSpacing: TRVSpinEdit;
    lblImgSpacingMU: TLabel;
    gbImgBorder: TGroupBox;
    lblImgBorderColor: TLabel;
    lblImgBorderWidth: TLabel;
    cmbImgBorderColor: TRVColorCombo;
    gbImgOutsideBorder: TGroupBox;
    lblImgOuterVSpacing: TLabel;
    lblImgOuterVSpacingMU: TLabel;
    seImgOuterVSpacing: TRVSpinEdit;
    lblImgOuterHSpacing: TLabel;
    seImgOuterHSpacing: TRVSpinEdit;
    lblImgOuterHSpacingMU: TLabel;
    tsImgMisc: TTabSheet;
    seWidthPrc: TRVSpinEdit;
    seHeightPrc: TRVSpinEdit;
    Label13: TLabel;
    Label14: TLabel;
    cmbImgSizeUnits: TComboBox;
    cbImgScaleProportionally: TCheckBox;
    btnImgSizeReset: TButton;
    gbWeb: TGroupBox;
    lblAlt: TLabel;
    txtAlt: TEdit;
    gbImgMisc: TGroupBox;
    lblImgHint: TLabel;
    txtImgHint: TEdit;
    cmbImgBorderWidth: TComboBox;
    gbTableBackAndBorder: TGroupBox;
    lblTableColor: TLabel;
    cmbTableColor: TRVColorCombo;
    btnTableBack: TButton;
    btnTableBorder: TButton;
    btnCellBorder: TButton;
    btnTableVisibleBorders: TButton;
    lblTableDefWidth: TLabel;
    btnCellBack: TButton;
    rgCellRotation: TRVOfficeRadioGroup;
    cmbCellColor: TRVColorCombo;
    lblCellColor: TLabel;
    lblCellBorderColor: TLabel;
    cmbCellBorderColor: TRVColorCombo;
    lblCellBorderLightColor: TLabel;
    cmbCellBorderLightColor: TRVColorCombo;
    btnCellVisibleBorders: TButton;
    lblCellHeight: TLabel;
    lblCellDefWidth: TLabel;
    lblCellWidth: TLabel;
    gbRowPrint: TGroupBox;
    cbRowKeepTogether: TCheckBox;
    tsSeq: TTabSheet;
    gbSeq: TGroupBox;
    lblSeqName: TLabel;
    lblSeqType: TLabel;
    cmbSeqName: TComboBox;
    cmbSeqType: TComboBox;
    gbSeqNumbering: TGroupBox;
    rbSeqContinue: TRadioButton;
    rbSeqReset: TRadioButton;
    seSeqStartFrom: TRVSpinEdit;
    lblCellBorder: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure cmbColorColorChange(Sender: TObject);
    procedure cmbVShiftTypeClick(Sender: TObject);
    procedure btnChangeImageClick(Sender: TObject);
    procedure cmbWidthDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cmbBreakColorColorChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure seTableWidthChange(Sender: TObject);
    procedure btnSpacingClick(Sender: TObject);
    procedure btnTableBorderClick(Sender: TObject);
    procedure btnCellBordersClick(Sender: TObject);
    procedure cmbCellBorderColorColorChange(Sender: TObject);
    procedure cmbCellBorderLightColorColorChange(Sender: TObject);
    procedure seCellBestWidthChange(Sender: TObject);
    procedure btnTopClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnTableBackClick(Sender: TObject);
    procedure btnCellBackClick(Sender: TObject);
    procedure cmbBreakStyleDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cmbWidthClick(Sender: TObject);
    procedure cmbTWTypeClick(Sender: TObject);
    procedure cmbCellBWTypeClick(Sender: TObject);
    procedure cmbAlignToChange(Sender: TObject);
    procedure lstBoxPositionClick(Sender: TObject);
    procedure cmbBoxHeightClick(Sender: TObject);
    procedure cmbBoxWidthClick(Sender: TObject);
    procedure seBoxHeightChange(Sender: TObject);
    procedure seBoxWidthChange(Sender: TObject);
    procedure cmbBoxHPositionClick(Sender: TObject);
    procedure cmbBoxVPositionClick(Sender: TObject);
    procedure seBoxHOffsChange(Sender: TObject);
    procedure seBoxVOffsChange(Sender: TObject);
    procedure btnBoxBorderClick(Sender: TObject);
    procedure cbBoxWidthMirrorClick(Sender: TObject);
    procedure cbBoxPosMirrorClick(Sender: TObject);
    procedure btnSaveImageClick(Sender: TObject);
    procedure cmbImgSizeUnitsClick(Sender: TObject);
    procedure seWidthChange(Sender: TObject);
    procedure seHeightChange(Sender: TObject);
    procedure seWidthPrcChange(Sender: TObject);
    procedure seHeightPrcChange(Sender: TObject);
    procedure btnImgSizeResetClick(Sender: TObject);
    procedure cbImgScaleProportionallyClick(Sender: TObject);
    procedure cmbImgBorderWidthDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure cmbImgBorderColorColorChange(Sender: TObject);
    procedure btnTableVisibleBordersClick(Sender: TObject);
    procedure btnCellVisibleBordersClick(Sender: TObject);
    procedure seSeqStartFromChange(Sender: TObject);
    procedure cmbSeqNameClick(Sender: TObject);
    procedure cmbSeqNameChange(Sender: TObject);
  private
    { Private declarations }
    FUpdatingCombos: Boolean;
    ImgProps: TRVImagePropRecord;
    TableProps: TRVTablePropRecord;
    BreakProps: TRVBreakPropRecord;
    {$IFNDEF RVDONOTUSESEQ}
    BoxProps: TRVBoxPropRecord;
    SeqProps: TRVSeqPropRecord;
    {$ENDIF}
    procedure UpdateCellBorders;
    procedure UpdateTableBorders;
    procedure UpdateLblDefSize;
    procedure UpdateImgPrcSize(se, sePrc: TRVSpinEdit; OriginalSize: Integer);
    procedure UpdateImgSizeFromPrc(se, sePrc: TRVSpinEdit; OriginalSize: Integer);
    procedure UpdateImgSizeProportionally;
    procedure StoreImgSize;
  protected
    _btnOk,
    _rbSeqContinue, _rbSeqReset, _cmbSeqName, _cmbSeqType,
    _gbTransp, _gbSize, _gbCellBorder,
    _cbTransp, _cbNoSplit, _cbRowKeepTogether, _cbImgScaleProportionally,
    _cmbAlignTo, _cmbVShiftType, _cmbWidth, _cmbBreakStyle, _cmbImgBorderWidth,
    _cmbTWType, _cmbCellBWType, _cmbImgSizeUnits,
    _tsImage, _tsImgLayout, _tsImgAppearance, _tsImgMisc, _tsBreak, _tsTable,
     _tsCells, _tsRows, _tsBoxPosition,
    _tsBoxSize, _tsBoxContent, _tsSeq,
    _lblTableDefWidth, _lblCellDefWidth,
    _lblDefSize, _lblDefSize2, _lblCellBorderColor, _lblCellBorderLightColor,
    _txtAlt, _txtImgHint: TControl;
    _lstBoxPosition, _gbBoxHPos, _gbBoxVPos, _cmbBoxWidth, _cmbBoxHeight,
    _lblBoxHeightTxt, _lblBoxWidthTxt, _lblBoxAutoHeight: TControl;
    _cmbBoxHPosition, _cmbBoxVPosition, _cmbBoxHAlign, _cmbBoxVAlign,
    _lblBoxHMU, _lblBoxVMU, _lblBoxHTxt, _lblBoxVTxt, _cbRelativeToCell,
    _cbBoxWidthMirror, _cbBoxPosMirror,
    _btnCellVisibleBorders, _lblCellBorder, 
    _btnTableVisibleBorders : TControl;
    _pc : TWinControl;
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}
    {$IFDEF USERVKSDEVTE}
    procedure tecmbWidthDrawItem(Control: TWinControl; Canvas: TCanvas; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure tecmbImgBorderWidthDrawItem(Control: TWinControl; Canvas: TCanvas; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure tecmbBreakStyleDrawItem(Control: TWinControl; Canvas: TCanvas; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
    procedure HideImgTabSheets;
    procedure HideTableTabSheets;
    procedure HideBoxTabSheets;
    function GetTableBestWidth(Units: TRVStyleUnits; DefValue: TRVStyleLength): TRVStyleLength;
    procedure AdjustTableProps;
  public
    { Public declarations }
    FRVStyle: TRVStyle;
    ColorDialog: TColorDialog;
    Filter, BackgroundFilter: String;
    StoreImageFileName: Boolean;
    ActionInsertTable, ActionInsertHLine: TAction;
    function SetItem(rve: TCustomRichViewEdit): Boolean;
    procedure SetTable(table: TRVTableItemInfo; TableOnly: Boolean);
    procedure SetGraphic(Item: TRVGraphicItemInfo);
    procedure SetBreak(Item: TRVBreakItemInfo);
    {$IFNDEF RVDONOTUSESEQ}
    procedure SetSidenote(Item: TRVSidenoteItemInfo);
    procedure GetSideNote(rve: TCustomRichViewEdit);
    procedure SetSequence(rve: TCustomRichViewEdit; Item: TRVSeqItemInfo);
    procedure GetSequence(rve: TCustomRichViewEdit);
    {$ENDIF}
    procedure GetItem(rve: TCustomRichViewEdit);
    procedure GetGraphic(rve: TCustomRichViewEdit);
    procedure GetBreak(rve: TCustomRichViewEdit);
    procedure FillBreakAction(Action: TAction);
    procedure GetTable(rve: TCustomRichViewEdit; table: TRVTableItemInfo);
    procedure FillTableAction(Action: TAction);
    procedure Localize; override;
    procedure DrawBreak(Canvas: TCanvas; Index: Integer; Rect: TRect;
      State: TOwnerDrawState);
    function GetMyClientSize(var Width, Height: Integer): Boolean; override;
  end;


implementation
uses RichViewActions, SpacingRVFrm, TableBrdrRVFrm, TableBackRVFrm,
  ParaBrdrRVFrm, VisibleSidesRVFrm, Math;
{$R *.dfm}

{============================ TfrmRVItemProp ==================================}
procedure TfrmRVItemProp.FormCreate(Sender: TObject);
begin
  _btnOk        := btnOk;
  _gbTransp     := gbTransp;
  _gbSize       := gbSize;
  _gbCellBorder := gbCellBorder;
  _cbTransp     := cbTransp;
  _cbNoSplit    := cbNoSplit;
  _cbRowKeepTogether := cbRowKeepTogether;
  _cbImgScaleProportionally := cbImgScaleProportionally;  
  _cmbAlignTo   := cmbAlignTo;
  _cmbVShiftType := cmbVShiftType;
  _cmbImgSizeUnits := cmbImgSizeUnits;
  _cmbWidth     := cmbWidth;
  _cmbImgBorderWidth := cmbImgBorderWidth;
  _cmbBreakStyle:= cmbBreakStyle;
  _cmbTWType    := cmbTWType;
  _cmbCellBWType := cmbCellBWType;
  _pc           := pc;
  _tsImage      := tsImage;
  _tsImgLayout  := tsImgLayout;
  _tsImgAppearance  := tsImgAppearance;
  _tsImgMisc    := tsImgMisc;  
  _tsBreak      := tsBreak;
  _tsTable      := tsTable;
  _tsCells      := tsCells;
  _tsRows       := tsRows;
  _tsBoxPosition:= tsBoxPosition;
  _tsBoxSize    := tsBoxSize;
  _tsBoxContent := tsBoxContent;
  _tsSeq        := tsSeq;
  _lblDefSize   := lblDefSize;
  _lblDefSize2  := lblDefSize2;
  _lblTableDefWidth := lblTableDefWidth;
  _lblCellDefWidth := lblCellDefWidth;  
  _lblCellBorderColor := lblCellBorderColor;
  _lblCellBorderLightColor := lblCellBorderLightColor;
  _btnCellVisibleBorders := btnCellVisibleBorders;
  _lblCellBorder := lblCellBorder;
  _btnTableVisibleBorders := btnTableVisibleBorders;  
  _txtAlt := txtAlt;
  _txtImgHint := txtImgHint;
  _lstBoxPosition := lstBoxPosition;
  _cbRelativeToCell := cbRelativeToCell;
  _cbBoxWidthMirror := cbBoxWidthMirror;
  _cbBoxPosMirror   := cbBoxPosMirror;
  _gbBoxHPos := gbBoxHPos;
  _gbBoxVPos := gbBoxVPos;
  _cmbBoxWidth := cmbBoxWidth;
  _cmbBoxHeight := cmbBoxHeight;
  _lblBoxHeightTxt := lblBoxHeightTxt;
  _lblBoxWidthTxt := lblBoxWidthTxt;
  _lblBoxAutoHeight := lblBoxAutoHeight;
  _cmbBoxHPosition := cmbBoxHPosition;
  _cmbBoxVPosition := cmbBoxVPosition;
  _cmbBoxHAlign := cmbBoxHAlign;
  _cmbBoxVAlign := cmbBoxVAlign;
  _lblBoxHMU := lblBoxHMU;
  _lblBoxVMU := lblBoxVMU;
  _lblBoxHTxt := lblBoxHTxt;
  _lblBoxVTxt := lblBoxVTxt;
  _rbSeqContinue := rbSeqContinue;
  _rbSeqReset := rbSeqReset;
  _cmbSeqName := cmbSeqName;
  _cmbSeqType := cmbSeqType;

  UpdateLengthSpinEdit(seCellHPadding, True, True);
  UpdateLengthSpinEdit(seCellVPadding, True, True);
  UpdateLengthSpinEdit(seCellBestHeight, False, True);
  UpdateLengthSpinEdit(seTableWidth, False, True);
  seTableWidth.MinValue := seTableWidth.Increment;
  UpdateLengthSpinEdit(seCellBestWidth, False, True);
  seCellBestWidth.MinValue := seCellBestWidth.Increment;  
  inherited;
  {$IFDEF RICHVIEWDEF6}
  img.Proportional := True;
  {$ENDIF}
  TableProps.TableBackgroundImage := nil;
  {$IFNDEF RVDONOTUSESEQ}
  BoxProps.Border := nil;
  BoxProps.Background := nil;
  SeqProps.SeqPropList := nil;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.FormDestroy(Sender: TObject);
begin
  inherited;
  RVFreeAndNil(TableProps.TableBackgroundImage);
  {$IFNDEF RVDONOTUSESEQ}
  RVFreeAndNil(SeqProps.SeqPropList);
  RVFreeAndNil(BoxProps.Border);
  RVFreeAndNil(BoxProps.Background);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function TfrmRVItemProp.GetMyClientSize(var Width, Height: Integer): Boolean;
begin
  Width := pc.Left*2+pc.Width;
  Height := btnOk.Top+btnOk.Height+(btnOk.Top-pc.Top-pc.Height);
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.FormActivate(Sender: TObject);
begin
  inherited;
  cmbColor.ColorDialog := ColorDialog;
  cmbBreakColor.ColorDialog := ColorDialog;
  cmbTableColor.ColorDialog := ColorDialog;
  cmbCellColor.ColorDialog := ColorDialog;
  cmbCellBorderColor.ColorDialog := ColorDialog;
  cmbCellBorderLightColor.ColorDialog := ColorDialog;
end;
{------------------------------------------------------------------------------}
// Assigning an item to the form
{------------------------------------------------------------------------------}
function TfrmRVItemProp.SetItem(rve: TCustomRichViewEdit): Boolean;
var item: TCustomRVItemInfo;
begin
  Result := False;
  FRVStyle := rve.Style;
  item := rve.GetCurrentItem;
  {........................................}
  if item is TRVGraphicItemInfo then begin
    SetGraphic(TRVGraphicItemInfo(Item));
    Result := True;
    end
  else if item is TRVBreakItemInfo then begin
    SetBreak(TRVBreakItemInfo(Item));
    Result := True;
    end
  else if item is TRVBreakItemInfo then begin
    SetBreak(TRVBreakItemInfo(Item));
    Result := True;
    end
  {$IFNDEF RVDONOTUSESEQ}
  else if item is TRVSidenoteItemInfo then begin
    SetSidenote(TRVSidenoteItemInfo(Item));
    Result := True;
    end
  else if item.StyleNo=rvsSequence then begin
    SetSequence(rve, TRVSeqItemInfo(Item));
    Result := True;
    end
  {$ENDIF}
  else if item is TRVTableItemInfo then begin
    SetTable(TRVTableItemInfo(item), False);
    Result := True;    
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.HideImgTabSheets;
begin
  HideTabSheet(_tsImage);
  HideTabSheet(_tsImgLayout);
  HideTabSheet(_tsImgAppearance);
  HideTabSheet(_tsImgMisc);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.HideTableTabSheets;
begin
  HideTabSheet(_tsTable);
  HideTabSheet(_tsCells);
  HideTabSheet(_tsRows);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.HideBoxTabSheets;
begin
  HideTabSheet(_tsBoxPosition);
  HideTabSheet(_tsBoxSize);
  HideTabSheet(_tsBoxContent);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.StoreImgSize;
begin
  if img.Picture.Graphic<>nil then begin
    ImgProps.ImageWidth := img.Picture.Graphic.Width;
    ImgProps.ImageHeight := img.Picture.Graphic.Height;
    end
  else begin
    ImgProps.ImageWidth := 0;
    ImgProps.ImageHeight := 0;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.SetGraphic(Item: TRVGraphicItemInfo);
var CP: TRVAControlPanel;
begin
  CP := TRVAControlPanel(ControlPanel);
  HelpContext := 90800;
  _pc.HandleNeeded;
  HideTabSheet(_tsBreak);
  HideTabSheet(_tsSeq);
  HideTableTabSheets;
  HideBoxTabSheets;
  SetPageControlActivePage(_pc, _tsImage);
  RVA_FillWidthComboBox(TRVAComboBox(_cmbImgBorderWidth), Canvas, True, CP);
  UpdateLengthSpinEdit(seImgSpacing, True, True);
  UpdateLengthSpinEdit(seImgOuterHSpacing, True, True);
  UpdateLengthSpinEdit(seImgOuterVSpacing, True, True);
  ImgProps.OriginalImage := Item.Image;
  img.Picture.Graphic := ImgProps.OriginalImage;
  StoreImgSize;  

  img.Picture.Graphic.Transparent := ImgProps.OriginalImage.Transparent and not (img.Picture.Graphic is TBitmap);
  img.Transparent := ImgProps.OriginalImage.Transparent and not (img.Picture.Graphic is TBitmap);

  if Item.Image is TBitmap then begin
    SetCheckBoxChecked(_cbTransp, TBitmap(Item.Image).Transparent);
    if TBitmap(Item.Image).TransparentMode = tmAuto then
      cmbColor.ChosenColor := cmbColor.AutoColor
    else
      cmbColor.ChosenColor := TBitmap(Item.Image).TransparentColor;
    end
  else
    _gbTransp.Visible := False;
  img.Stretch := (ImgProps.ImageWidth>img.Width) or (ImgProps.ImageHeight>img.Height);
  if ord(Item.VAlign)<GetXBoxItemCount(_cmbAlignTo) then
    SetXBoxItemIndex(_cmbAlignTo, ord(Item.VAlign));

  if Item.VShiftAbs then begin
    // vshift in units
    ImgProps.cmbVShiftItemIndex := 1;
    SetXBoxItemIndex(_cmbVShiftType, 1);
    UpdateLengthSpinEdit(seVShift, True, False);
    seVShift.Value := FRVStyle.GetAsRVUnits(Item.VShift, RVA_GetFineUnits(CP));
    end
  else begin
    // vshift in %
    ImgProps.cmbVShiftItemIndex := 0;
    SetXBoxItemIndex(_cmbVShiftType, 0);
    seVShift.IntegerValue := True;
    seVShift.MinValue := -500;
    seVShift.MaxValue := +500;
    seVShift.Increment := 1;
    seVShift.Value := Item.VShift;
  end;
  UpdateLengthSpinEdit(seWidth, False, True);
  UpdateLengthSpinEdit(seHeight, False, True);
  seWidth.MinValue := TRVStyle.PixelsToRVUnits(1, CP.UnitsDisplay);
  seHeight.MinValue := TRVStyle.PixelsToRVUnits(1, CP.UnitsDisplay);
  UpdateSpinEditEx(seWidthPrc, 1, 3000, 0);
  UpdateSpinEditEx(seHeightPrc, 1, 3000, 0);
  if Item.ImageWidth<>0 then
    seWidth.Value := FRVStyle.GetAsRVUnits(Item.ImageWidth, CP.UnitsDisplay);
  if Item.ImageHeight<>0 then
    seHeight.Value := FRVStyle.GetAsRVUnits(Item.ImageHeight, CP.UnitsDisplay);
  if (seHeight.Indeterminate and seWidth.Indeterminate) or
    (Round(seWidthPrc.Value)=Round(seHeightPrc.Value)) then
    SetCheckBoxChecked(_cbImgScaleProportionally, True);
  UpdateImgPrcSize(seWidth, seWidthPrc, ImgProps.ImageWidth);
  UpdateImgPrcSize(seHeight, seHeightPrc, ImgProps.ImageHeight);  
  UpdateLblDefSize;
  _gbSize.Visible := not (img.Picture.Graphic is TIcon);
  seImgSpacing.Value := FRVStyle.GetAsRVUnits(Item.Spacing, RVA_GetFineUnits(CP));
  seImgOuterHSpacing.Value := FRVStyle.GetAsRVUnits(Item.OuterHSpacing, RVA_GetFineUnits(CP));
  seImgOuterVSpacing.Value := FRVStyle.GetAsRVUnits(Item.OuterVSpacing, RVA_GetFineUnits(CP));
  SetControlCaption(_txtAlt, Item.Alt);
  {$IFNDEF RVDONOTUSEITEMHINTS}
  SetControlCaption(_txtImgHint, Item.Hint);    
  {$ENDIF}  
  cmbAlignToChange(nil);
  cmbImgFillColor.ChosenColor := Item.BackgroundColor;
  cmbImgBorderColor.ChosenColor := Item.BorderColor;
  ImgProps.BorderWidth := Item.BorderWidth;
  SetXBoxItemIndex(_cmbImgBorderWidth,
    RVA_GetComboBorderItemIndex(Item.BorderWidth, True, FRVStyle,
      GetXBoxItemCount(_cmbImgBorderWidth), CP));
  ImgProps.LastChangedIsHeight := False;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.SetBreak(Item: TRVBreakItemInfo);
var v: Integer;
begin
  HelpContext := 90900;
  _pc.HandleNeeded;
  HideImgTabSheets;
  HideTableTabSheets;
  HideBoxTabSheets;
  HideTabSheet(_tsSeq);  
  SetPageControlActivePage(_pc, _tsBreak);
  RVA_FillWidthComboBox(TRVAComboBox(_cmbWidth), Canvas, False, TRVAControlPanel(ControlPanel));
  cmbBreakColor.ChosenColor := Item.Color;
  BreakProps.Width := Item.LineWidth;
  SetXBoxItemIndex(_cmbWidth,
    RVA_GetComboBorderItemIndex(Item.LineWidth, False, FRVStyle,
      GetXBoxItemCount(_cmbWidth), TRVAControlPanel(ControlPanel)));
  v := ord(Item.Style);
  if v>GetXBoxItemCount(_cmbBreakStyle)-1 then
    v := -1;
  SetXBoxItemIndex(_cmbBreakStyle, v);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
procedure TfrmRVItemProp.SetSidenote(Item: TRVSidenoteItemInfo);

var CP: TRVAControlPanel;

  procedure PrepareILBoxAnchor;
  var bmp: TBitmap;
  begin
    bmp := TBitmap.Create;
    try
      bmp.LoadFromResourceName(hInstance, 'RVA_BOX_AREA');
      ilBoxAnchor.AddMasked(bmp, bmp.Canvas.Pixels[0,bmp.Height-1]);
    finally
      bmp.Free;
    end;
    PrepareImageList(ilBoxAnchor);
  end;

  procedure PrepareILBoxZPos;
  var bmp: TBitmap;
  begin
    bmp := TBitmap.Create;
    try
      bmp.LoadFromResourceName(hInstance, 'RVA_BOX_INTEXT');
      ilBoxZPos.AddMasked(bmp, bmp.Canvas.Pixels[0,bmp.Height-1]);
    finally
      bmp.Free;
    end;
    PrepareImageList(ilBoxZPos);
  end;

  procedure PrepareILTable;
  var bmp: TBitmap;
  begin
    bmp := TBitmap.Create;
    try
      bmp.LoadFromResourceName(hInstance, 'RVA_CELL');
      ilTable.AddMasked(bmp, bmp.Canvas.Pixels[0,bmp.Height-1]);
    finally
      bmp.Free;
    end;
    PrepareImageList(ilTable);
  end;

begin
  CP := TRVAControlPanel(ControlPanel);
  PrepareILBoxAnchor;
  PrepareILBoxZPos;
  PrepareILTable;
  _pc.HandleNeeded;
  HideTabSheet(_tsBreak);
  HideTabSheet(_tsSeq);
  HideImgTabSheets;
  HideTableTabSheets;
  SetPageControlActivePage(_pc, _tsBoxPosition);
  SetXBoxItemIndex(_lstBoxPosition, 0);
  rgBoxZPos.ItemIndex := ord(Item.BoxPosition.PositionInText);
  case Item.BoxProperties.VAlign of
    tlTop: rgBoxVAlign.ItemIndex := 0;
    tlCenter: rgBoxVAlign.ItemIndex := 1;
    tlBottom: rgBoxVAlign.ItemIndex := 2;
  end;
  // box horizontal position ---------
  BoxProps.HOffs := 0;
  BoxProps.HOffsPct := 0.0;
  SetXBoxItemIndex(_cmbBoxHAlign, 0);
  case Item.BoxPosition.HorizontalPositionKind of
    rvfpkAlignment:
      SetXBoxItemIndex(_cmbBoxHAlign, ord(Item.BoxPosition.HorizontalAlignment));
    rvfpkAbsPosition:
      BoxProps.HOffs := FRVStyle.GetAsRVUnits(Item.BoxPosition.HorizontalOffset, CP.UnitsDisplay);
    rvfpkPercentPosition:
      BoxProps.HOffsPct := Item.BoxPosition.HorizontalOffset/1000;
  end;
  case Item.BoxPosition.HorizontalAnchor of
    rvhanCharacter: rbBoxHCharacter.Checked := True;
    rvhanPage: rbBoxHPage.Checked := True;
    rvhanMainTextArea: rbBoxHMainTextArea.Checked := True;
    rvhanLeftMargin, rvhanInnerMargin: rbBoxHLeftMargin.Checked := True;
    rvhanRightMargin,rvhanOuterMargin: rbBoxHRightMargin.Checked := True;
  end;
  SetCheckBoxChecked(_cbBoxPosMirror, Item.BoxPosition.HorizontalAnchor in [rvhanInnerMargin, rvhanOuterMargin]);
  cbBoxPosMirrorClick(nil);
  SetXBoxItemIndex(_cmbBoxHPosition, ord(Item.BoxPosition.HorizontalPositionKind));
  cmbBoxHPositionClick(nil);
  // box vertical position ---------
  BoxProps.VOffs := 0;
  BoxProps.VOffsPct := 0.0;
  SetXBoxItemIndex(_cmbBoxVAlign, 0);
  case Item.BoxPosition.VerticalPositionKind of
    rvfpkAlignment:
      SetXBoxItemIndex(_cmbBoxVAlign, ord(Item.BoxPosition.VerticalAlignment));
    rvfpkAbsPosition:
      BoxProps.VOffs := FRVStyle.GetAsRVUnits(Item.BoxPosition.VerticalOffset, CP.UnitsDisplay);
    rvfpkPercentPosition:
      BoxProps.VOffsPct := Item.BoxPosition.VerticalOffset/1000;
  end;
  case Item.BoxPosition.VerticalAnchor of
    rvvanLine: rbBoxVLine.Checked := True;
    rvvanParagraph: rbBoxVPara.Checked := True;
    rvvanPage: rbBoxVPage.Checked := True;
    rvvanMainTextArea: rbBoxVMainTextArea.Checked := True;
    rvvanTopMargin: rbBoxVTopMargin.Checked := True;
    rvvanBottomMargin: rbBoxVBottomMargin.Checked := True;
  end;
  SetXBoxItemIndex(_cmbBoxVPosition, ord(Item.BoxPosition.VerticalPositionKind));
  cmbBoxVPositionClick(nil);
  // cell ----------------------------
  SetCheckBoxChecked(_cbRelativeToCell, Item.BoxPosition.RelativeToCell);
  // box size ------------------------
  BoxProps.Height := FRVStyle.GetAsRVUnits(FRVStyle.PixelsToUnits(100), CP.UnitsDisplay);
  BoxProps.HeightPct := 20.0;
  BoxProps.Width := BoxProps.Height;
  BoxProps.WidthPct := 40.0;
  case Item.BoxProperties.HeightType of
    rvbhtAbsolute:
      begin
        SetXBoxItemIndex(_cmbBoxHeight, 0);
        BoxProps.Height := FRVStyle.GetAsRVUnits(Item.BoxProperties.Height, CP.UnitsDisplay);
      end;
    rvbhtAuto:
      SetXBoxItemIndex(_cmbBoxHeight, 2);
    else begin
      SetXBoxItemIndex(_cmbBoxHeight, 1);
      BoxProps.HeightPct := Item.BoxProperties.Height / 1000;
      case Item.BoxProperties.HeightType of
        rvbhtRelPage: rbBoxHeightPage.Checked := True;
        rvbhtRelMainTextArea: rbBoxHeightMainTextArea.Checked := True;
        rvbhtRelTopMargin: rbBoxHeightTopMargin.Checked := True;
        rvbhtRelBottomMargin: rbBoxHeightBottomMargin.Checked := True;
      end;
    end;
  end;
  cmbBoxHeightClick(nil);
  case Item.BoxProperties.WidthType of
    rvbwtAbsolute:
      begin
        SetXBoxItemIndex(_cmbBoxWidth, 0);
        BoxProps.Width := FRVStyle.GetAsRVUnits(Item.BoxProperties.Width, CP.UnitsDisplay);
      end;
    else begin
      SetXBoxItemIndex(_cmbBoxWidth, 1);
      BoxProps.WidthPct := Item.BoxProperties.Width / 1000;
      case Item.BoxProperties.WidthType of
        rvbwtRelPage: rbBoxWidthPage.Checked := True;
        rvbwtRelMainTextArea: rbBoxWidthMainTextArea.Checked := True;
        rvbwtRelLeftMargin, rvbwtRelInnerMargin: rbBoxWidthLeftMargin.Checked := True;
        rvbwtRelRightMargin, rvbwtRelOuterMargin: rbBoxWidthRightMargin.Checked := True;
      end;
      SetCheckBoxChecked(_cbBoxWidthMirror, Item.BoxProperties.WidthType in [rvbwtRelInnerMargin, rvbwtRelOuterMargin]);
     cbBoxWidthMirrorClick(nil);
    end;
  end;
  cmbBoxWidthClick(nil);
  // box background ------------------------
  BoxProps.Border := TRVBorder.Create(nil);
  BoxProps.Background := TRVBackgroundRect.Create(nil);
  BoxProps.Border.Assign(Item.BoxProperties.Border);
  BoxProps.Background.Assign(Item.BoxProperties.Background);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.SetSequence(rve: TCustomRichViewEdit; Item: TRVSeqItemInfo);
begin
  _pc.HandleNeeded;
  HideTabSheet(_tsBreak);
  HideImgTabSheets;
  HideTableTabSheets;
  HideBoxTabSheets;
  SetPageControlActivePage(_pc, _tsSeq);
  SeqProps.SeqPropList := RVAGetListOfSequences(rve);
  if SeqProps.SeqPropList<>nil then
    SetXBoxItems(_cmbSeqName, SeqProps.SeqPropList);
  SetControlCaption(_cmbSeqName, Item.SeqName);
  SetXBoxItemIndex(_cmbSeqType, ord(Item.NumberType));
  if Item.Reset then begin
    seSeqStartFrom.Value := Item.StartFrom;
    SetRadioButtonChecked(_rbSeqReset, True)
    end
  else
    SetRadioButtonChecked(_rbSeqContinue, True)  
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.SetTable(table: TRVTableItemInfo;
  TableOnly: Boolean);
var r,c,sr,sc,rs,cs: Integer;
    rowselected: Boolean;
    FirstRow: TRVTableRow;
    EditedCell, FirstCell, CurCell: TRVTableCellData;
    CP: TRVAControlPanel;

  procedure PrepareILTable;
  var bmp: TBitmap;
  begin
    bmp := TBitmap.Create;
    try
      bmp.LoadFromResourceName(hInstance, 'RVA_CELL');
      ilTable.AddMasked(bmp, bmp.Canvas.Pixels[0,bmp.Height-1]);
    finally
      bmp.Free;
    end;
    PrepareImageList(ilTable);
  end;

  function GetRotationIndex(Rotation: TRVDocRotation): Integer;
  begin
    case CP.UserInterface of
      rvauiRTF: // no 180 rotation in RTF
        case Rotation of
          rvrotNone: Result := 0;
          rvrot90:   Result := 1;
          rvrot270:  Result := 2;
          else       Result := -1;
        end;
      rvauiHTML:
        Result := -1;
      else
        Result := ord(Rotation);
    end;
  end;

  function GetRowVAlignIndex(VAlign: TRVCellVAlign): Integer;
  begin
    case VAlign of
      rvcMiddle: Result := 1;
      rvcBottom: Result := 2;
      else       Result := 0;
    end
  end;

begin
  PrepareILTable;
  CP := TRVAControlPanel(ControlPanel);
  HelpContext := 91000;
  _pc.HandleNeeded;
  HideTabSheet(_tsBreak);
  HideTabSheet(_tsSeq);  
  HideImgTabSheets;
  HideBoxTabSheets;
  if TableOnly then begin
    HideTabSheet(_tsCells);
    HideTabSheet(_tsRows);
  end;
  SetPageControlActivePage(_pc, _tsTable);
  FRVStyle := table.GetRVStyle;

  case CP.UserInterface of
    rvauiHTML:
      rgCellRotation.Visible := False;
    rvauiRTF:
      rgCellRotation.Items[2].Free; // no 180 rotation in RTF
  end;

  TableProps.TableWidth := FRVStyle.GetAsRVUnits(
    FRVStyle.PixelsToUnits(table.GetWidth(FRVStyle)), CP.UnitsDisplay);
  TableProps.TableWidthPrc := 100;

  if table.BestWidth=0 then begin // default width
    SetXBoxItemIndex(_cmbTWType, 0);
    end
  else if table.BestWidth>0 then begin // absolute width
    TableProps.TableWidth := FRVStyle.GetAsRVUnits(table.BestWidth, CP.UnitsDisplay);
    SetXBoxItemIndex(_cmbTWType, 2);
    end
  else if table.BestWidth<0 then begin // %
    TableProps.TableWidthPrc := -table.BestWidth;
    SetXBoxItemIndex(_cmbTWType, 1);
  end;
  cmbTWTypeClick(_cmbTWType);
  SetCheckBoxChecked(_cbNoSplit, not (rvtoRowsSplit in table.PrintOptions));
  seHRC.Value := table.HeadingRowCount;
  cmbTableColor.ChosenColor := table.Color;
  seCellHPadding.Value := FRVStyle.GetAsRVUnits(table.CellHPadding, RVA_GetFineUnits(CP));
  seCellVPadding.Value := FRVStyle.GetAsRVUnits(table.CellVPadding, RVA_GetFineUnits(CP));
  TableProps.CellHSpacing := table.CellHSpacing;
  TableProps.CellVSpacing := table.CellVSpacing;
  TableProps.BorderHSpacing := table.BorderHSpacing;
  TableProps.BorderVSpacing := table.BorderVSpacing;
  TableProps.BorderWidth := table.BorderWidth;
  TableProps.BorderStyle := table.BorderStyle;
  TableProps.BorderColorD := table.BorderColor;
  TableProps.BorderColorL := table.BorderLightColor;
  TableProps.CellBorderWidth := table.CellBorderWidth;
  TableProps.CellBorderColorD := table.CellBorderColor;
  TableProps.CellBorderColorL := table.CellBorderLightColor;
  TableProps.CellBorderStyle := table.CellBorderStyle;
  table.VisibleBorders.GetValues(
    TableProps.TableVBLeft, TableProps.TableVBTop,
    TableProps.TableVBRight, TableProps.TableVBBottom);
  if table.BackgroundImage=nil then
    TableProps.TableBackgroundImage := nil
  else begin
    try
      TableProps.TableBackgroundImage := RVGraphicHandler.CreateGraphic(TGraphicClass(table.BackgroundImage.ClassType));
      TableProps.TableBackgroundImage.Assign(table.BackgroundImage);
    except;
    end;
  end;
  TableProps.TableBackgroundStyle := table.BackgroundStyle;

  if TableOnly then
    exit;
  if not table.GetNormalizedSelectionBounds(True, sr,sc,cs,rs) then begin
    HideTabSheet(_tsCells);
    HideTabSheet(_tsRows);
    exit;
  end;
  if table.GetEditedCell(r,c)<>nil then
    EditedCell := table.Cells[r,c]
  else
    EditedCell := nil;
  if table.ColCount>0 then begin
    TableProps.CellWidthPrc := Round(TableProps.TableWidthPrc / table.ColCount);
    TableProps.CellWidth := TableProps.TableWidth / table.ColCount;
    end
  else begin
    TableProps.CellWidthPrc := TableProps.TableWidthPrc;
    TableProps.CellWidth := TableProps.TableWidth;
  end;
  if EditedCell<>nil then
    TableProps.CellWidth := FRVStyle.GetAsRVUnits(FRVStyle.PixelsToUnits(EditedCell.GetWidth), CP.UnitsDisplay);

  FirstRow := nil;
  FirstCell := nil;
  TableProps.FDefCellColorD := True;
  TableProps.FDefCellColorL := True;
  if EditedCell<>nil then begin
    TableProps.CellBackgroundStyle := EditedCell.BackgroundStyle;
    if EditedCell.BackgroundImage=nil then
      TableProps.CellBackgroundImage := nil
    else begin
      try
        TableProps.CellBackgroundImage := RVGraphicHandler.CreateGraphic(TGraphicClass(EditedCell.BackgroundImage.ClassType));
        TableProps.CellBackgroundImage.Assign(EditedCell.BackgroundImage);
      except;
      end;
    end;
  end;
  for r := 0 to table.Rows.Count-1 do begin
    rowselected := False;
    for c := 0 to table.Rows[r].Count-1 do
      if (table.Cells[r,c]<>nil) and
         (table.IsCellSelected(r,c) or (EditedCell=table.Cells[r,c])) then begin
        rowselected := True;
        if TableProps.CellBackgroundImage=nil then begin
          TableProps.CellBackgroundStyle := table.Cells[r,c].BackgroundStyle;
          if table.Cells[r,c].BackgroundImage=nil then
            TableProps.CellBackgroundImage := nil
          else begin
            try
              TableProps.CellBackgroundImage := RVGraphicHandler.CreateGraphic(TGraphicClass(table.Cells[r,c].BackgroundImage.ClassType));
              TableProps.CellBackgroundImage.Assign(table.Cells[r,c].BackgroundImage);
            except;
            end;
          end;
        end;
        if FirstCell=nil then begin
          FirstCell := table.Cells[r,c];
          if FirstCell.BestWidth=0 then begin // default
            SetXBoxItemIndex(_cmbCellBWType, 0);
            end
          else if FirstCell.BestWidth>0 then begin // CP.Units
            TableProps.CellWidth := FRVStyle.GetAsRVUnits(FirstCell.BestWidth, CP.UnitsDisplay);
            SetXBoxItemIndex(_cmbCellBWType, 2);
            end
          else if FirstCell.BestWidth<0 then begin // %
            TableProps.CellWidthPrc := -FirstCell.BestWidth;
            SetXBoxItemIndex(_cmbCellBWType, 1);
          end;
          seCellBestHeight.Value := FRVStyle.GetAsRVUnits(FirstCell.BestHeight, CP.UnitsDisplay);
          cmbCellColor.ChosenColor := FirstCell.Color;
          rgCellVAlign.ItemIndex := ord(FirstCell.VAlign);
          rgCellRotation.ItemIndex := GetRotationIndex(FirstCell.Rotation);
          cmbCellBorderColor.ChosenColor := FirstCell.BorderColor;
          cmbCellBorderLightColor.ChosenColor := FirstCell.BorderLightColor;
          TableProps.FDefCellColorD := FirstCell.BorderColor=clNone;
          TableProps.FDefCellColorL := FirstCell.BorderLightColor=clNone;
          FirstCell.VisibleBorders.GetValues(
            TableProps.CellVBLeft, TableProps.CellVBTop,
            TableProps.CellVBRight, TableProps.CellVBBottom);
          TableProps.CellDefVBLeft := True;
          TableProps.CellDefVBTop := True;
          TableProps.CellDefVBRight := True;
          TableProps.CellDefVBBottom := True;
         end
        else begin
          CurCell := table.Cells[r,c];
          if CurCell.BestWidth<>FirstCell.BestWidth then
            SetXBoxItemIndex(_cmbCellBWType, -1);
          if CurCell.BestHeight<>FirstCell.BestHeight then
            seCellBestHeight.Indeterminate := True;
          if CurCell.Color<>FirstCell.Color then
            cmbCellColor.Indeterminate := True;
          if CurCell.VAlign<>FirstCell.VAlign then
            rgCellVAlign.ItemIndex := -1;
          if CurCell.Rotation<>FirstCell.Rotation then
            rgCellRotation.ItemIndex := -1;
          if CurCell.VisibleBorders.Left<>FirstCell.VisibleBorders.Left then
            TableProps.CellDefVBLeft := False;
          if CurCell.VisibleBorders.Top<>FirstCell.VisibleBorders.Top then
            TableProps.CellDefVBTop := False;
          if CurCell.VisibleBorders.Right<>FirstCell.VisibleBorders.Right then
            TableProps.CellDefVBRight := False;
          if CurCell.VisibleBorders.Bottom<>FirstCell.VisibleBorders.Bottom then
            TableProps.CellDefVBBottom := False;
          if CurCell.BorderColor<>FirstCell.BorderColor then begin
            cmbCellBorderColor.Indeterminate := True;
            TableProps.FDefCellColorD := False;
          end;
          if CurCell.BorderLightColor<>FirstCell.BorderLightColor then begin
            cmbCellBorderLightColor.Indeterminate := True;
            TableProps.FDefCellColorL := False;
          end;
        end;
      end;
    cmbCellBWTypeClick(_cmbCellBWType);
    if rowselected then
      if FirstRow=nil then begin
        FirstRow := table.Rows[r];
        rgRowVAlign.ItemIndex := GetRowVAlignIndex(FirstRow.VAlign);
        SetCheckBoxChecked(_cbRowKeepTogether, FirstRow.KeepTogether);
        end
      else begin
        if table.Rows[r].VAlign<>FirstRow.VAlign then
          rgRowVAlign.ItemIndex := -1;
        if table.Rows[r].KeepTogether<>FirstRow.KeepTogether then
          SetCheckBoxState(_cbRowKeepTogether, cbGrayed);
      end;
  end;
  UpdateCellBorders;
  UpdateTableBorders;
end;
{------------------------------------------------------------------------------}
// Applying changes to the item
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.GetItem(rve: TCustomRichViewEdit);
begin
  rve := rve.TopLevelEditor;
  if rve.GetCurrentItem is TRVTableItemInfo then
    rve.BeginUndoGroup(rvutTable)
  else
    rve.BeginUndoGroup(rvutModifyItem);
  rve.SetUndoGroupMode(True);
  try
    {........................................}
    if rve.GetCurrentItem is TRVGraphicItemInfo then
      GetGraphic(rve)
    {........................................}
    else if rve.GetCurrentItem is TRVBreakItemInfo then begin
      GetBreak(rve);
      if ActionInsertHLine<>nil then
        FillBreakAction(ActionInsertHLine);
      end
    {$IFNDEF RVDONOTUSESEQ}
    else if rve.GetCurrentItem is TRVSidenoteItemInfo then
      GetSidenote(rve)
    else if rve.GetCurrentItem.StyleNo=rvsSequence then
      GetSequence(rve)
    {$ENDIF}
    else if rve.GetCurrentItem is TRVTableItemInfo then begin
      GetTable(rve, TRVTableItemInfo(rve.GetCurrentItem));
      if ActionInsertTable<>nil then
        FillTableAction(ActionInsertTable);
    end;
  finally
    rve.SetUndoGroupMode(False);
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.GetGraphic(rve: TCustomRichViewEdit);
var gr: TGraphic;
    Data: Integer;
    VAlign: TRVVAlign;
    CP: TRVAControlPanel;
begin
  CP := TRVAControlPanel(ControlPanel);
  if ImgProps.OriginalImage<>nil then
    gr := ImgProps.OriginalImage
  else begin
    gr := RVGraphicHandler.CreateGraphic(TGraphicClass(img.Picture.Graphic.ClassType));
    gr.Assign(img.Picture.Graphic);
    gr.Transparent := img.Picture.Graphic.Transparent;
  end;
  if gr is TBitmap then begin
     TBitmap(gr).Transparent := GetCheckBoxChecked(_cbTransp);
     if cmbColor.ChosenColor=cmbColor.AutoColor then
       TBitmap(gr).TransparentMode := tmAuto
     else begin
       TBitmap(gr).TransparentMode := tmFixed;
       TBitmap(gr).TransparentColor := cmbColor.ChosenColor;
     end;
  end;
  VAlign := rve.GetCurrentItemVAlign;
  if GetXBoxItemIndex(_cmbAlignTo)>=0 then
    VAlign := TRVVAlign(GetXBoxItemIndex(_cmbAlignTo));
  rve.SetCurrentPictureInfo(rve.GetCurrentItemTextA, gr,
    VAlign, rve.GetCurrentTag);
  rve.BeginCurrentItemModify(Data);
  if not seWidth.Indeterminate then
    case GetXBoxItemIndex(_cmbImgSizeUnits) of
      0: // pixels
        rve.SetCurrentItemExtraIntProperty(rvepImageWidth,
          FRVStyle.PixelsToUnits(seWidth.AsInteger), False);
      1: // units
        rve.SetCurrentItemExtraIntProperty(rvepImageWidth,
          FRVStyle.RVUnitsToUnits(seWidth.Value, CP.UnitsDisplay), False);
    end
  else
    rve.SetCurrentItemExtraIntProperty(rvepImageWidth, 0 , False);
  if not seHeight.Indeterminate then
    case GetXBoxItemIndex(_cmbImgSizeUnits) of
      0: // pixels
        rve.SetCurrentItemExtraIntProperty(rvepImageHeight,
          FRVStyle.PixelsToUnits(seHeight.AsInteger), False);
      1: // units
        rve.SetCurrentItemExtraIntProperty(rvepImageHeight,
          FRVStyle.RVUnitsToUnits(seHeight.Value, CP.UnitsDisplay), False);
    end
  else
    rve.SetCurrentItemExtraIntProperty(rvepImageHeight, 0 , False);
  if not seVShift.Indeterminate then
    if GetXBoxItemIndex(_cmbVShiftType)=0 then begin // %
      rve.SetCurrentItemExtraIntProperty(rvepVShiftAbs, 0, False);
      rve.SetCurrentItemExtraIntProperty(rvepVShift, seVShift.AsInteger, False);
      end
    else begin  // units
      rve.SetCurrentItemExtraIntProperty(rvepVShiftAbs, 1, False);
      rve.SetCurrentItemExtraIntProperty(rvepVShift,
        FRVStyle.RVUnitsToUnits(seVShift.Value, RVA_GetFineUnits(CP)), False);
    end;
  if not seImgSpacing.Indeterminate then
    rve.SetCurrentItemExtraIntProperty(rvepSpacing,
      FRVStyle.RVUnitsToUnits(seImgSpacing.Value, RVA_GetFineUnits(CP)), False);
  if not seImgOuterHSpacing.Indeterminate then
    rve.SetCurrentItemExtraIntProperty(rvepOuterHSpacing,
      FRVStyle.RVUnitsToUnits(seImgOuterHSpacing.Value, RVA_GetFineUnits(CP)), False);
  if not seImgOuterVSpacing.Indeterminate then
    rve.SetCurrentItemExtraIntProperty(rvepOuterVSpacing,
      FRVStyle.RVUnitsToUnits(seImgOuterVSpacing.Value, RVA_GetFineUnits(CP)), False);
  rve.SetCurrentItemExtraStrProperty(rvespAlt, GetEditText(_txtAlt), False);
  {$IFNDEF RVDONOTUSEITEMHINTS}
  rve.SetCurrentItemExtraStrProperty(rvespHint, GetEditText(_txtImgHint), False);    
  {$ENDIF}
  if StoreImageFileName and ImgProps.ImageFileNameChanged then
    rve.SetCurrentItemExtraStrProperty(rvespImageFileName, ImgProps.ImageFileName, False);
  rve.SetCurrentItemExtraIntProperty(rvepColor, cmbImgFillColor.ChosenColor, False);
  rve.SetCurrentItemExtraIntProperty(rvepBorderColor, cmbImgBorderColor.ChosenColor, False);

  if GetXBoxItemIndex(_cmbImgBorderWidth)>=0 then
    ImgProps.BorderWidth := RVA_GetComboBorderWidth2(GetXBoxItemIndex(_cmbImgBorderWidth),
      True, FRVStyle, CP);
  rve.SetCurrentItemExtraIntProperty(rvepBorderWidth, ImgProps.BorderWidth, False);
  rve.EndCurrentItemModify(Data);
  rve.Change;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.GetBreak(rve: TCustomRichViewEdit);
var v: Integer;
begin
  v := GetXBoxItemIndex(_cmbBreakStyle);
  if v<0 then
    v := 0;
  rve.SetCurrentBreakInfo(BreakProps.Width,
      TRVBreakStyle(v), cmbBreakColor.ChosenColor, rve.GetCurrentTag);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.FillBreakAction(Action: TAction);
var Act: TrvActionInsertHLine;
var v: Integer;
begin
  Act := Action as TrvActionInsertHLine;
  v := GetXBoxItemIndex(_cmbBreakStyle);
  if v<0 then
    v := 0;
  Act.Style := TRVBreakStyle(v);
  Act.Color := cmbBreakColor.ChosenColor;
  Act.Width := FRVStyle.GetAsDifferentUnits(BreakProps.Width, TRVAControlPanel(ControlPanel).UnitsProgram);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
procedure TfrmRVItemProp.GetSideNote(rve: TCustomRichViewEdit);
var CP: TRVAControlPanel;
  {.....................................................................}
  function GetHorizontalAnchor: TRVHorizontalAnchor;
  begin
    if rbBoxHCharacter.Checked then
      Result := rvhanCharacter
    else if rbBoxHPage.Checked then
      Result := rvhanPage
    else if rbBoxHMainTextArea.Checked then
      Result := rvhanMainTextArea
    else if rbBoxHLeftMargin.Checked then
      if GetCheckBoxChecked(_cbBoxPosMirror) then
        Result := rvhanInnerMargin
      else
        Result := rvhanLeftMargin
    else // if rbBoxHRightMargin.Checked then
      if GetCheckBoxChecked(_cbBoxPosMirror) then
        Result := rvhanOuterMargin
      else
        Result := rvhanRightMargin;
  end;
  {.....................................................................}
  function GetVerticalAnchor: TRVVerticalAnchor;
  begin
    if rbBoxVLine.Checked then
      Result := rvvanLine
    else if rbBoxVPara.Checked then
      Result := rvvanParagraph
    else if rbBoxVPage.Checked then
      Result := rvvanPage
    else if rbBoxVMainTextArea.Checked then
      Result := rvvanMainTextArea
    else if rbBoxVTopMargin.Checked then
      Result := rvvanTopMargin
    else // if rbBoxVBottomMargin.Checked then
      Result := rvvanBottomMargin;
  end;
  {.....................................................................}
  function GetHeightType: TRVBoxHeightType;
  begin
    if rbBoxHeightPage.Checked then
      Result := rvbhtRelPage
    else if rbBoxHeightMainTextArea.Checked then
      Result := rvbhtRelMainTextArea
    else if rbBoxHeightTopMargin.Checked then
      Result := rvbhtRelTopMargin
    else // if rbBoxHeightBottomMargin.Checked then
      Result := rvbhtRelBottomMargin;
  end;
  {.....................................................................}
  function GetWidthType: TRVBoxWidthType;
  begin
    if rbBoxWidthPage.Checked then
      Result := rvbwtRelPage
    else if rbBoxWidthMainTextArea.Checked then
      Result := rvbwtRelMainTextArea
    else if rbBoxWidthLeftMargin.Checked then
      if GetCheckBoxChecked(_cbBoxWidthMirror) then
        Result := rvbwtRelInnerMargin
      else
        Result := rvbwtRelLeftMargin
    else // if rbBoxWidthRightMargin.Checked then
      if GetCheckBoxChecked(_cbBoxWidthMirror) then
        Result := rvbwtRelOuterMargin      
      else
        Result := rvbwtRelRightMargin;
  end;
  {.....................................................................}
  function GetContentVAlign: TTextLayout;
  begin
    case rgBoxVAlign.ItemIndex of
      0: Result := tlTop;
      1: Result := tlCenter;
      else Result := tlBottom;
    end;
  end;
begin
  CP := TRVAControlPanel(ControlPanel);
  // box horizontal position ---------
  rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatHorizontalAnchor, ord(GetHorizontalAnchor), True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatHorizontalPositionKind, GetXBoxItemIndex(_cmbBoxHPosition), True);
  case TRVFloatPositionKind(GetXBoxItemIndex(_cmbBoxHPosition)) of
    rvfpkAlignment:
      rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatHorizontalAlign, GetXBoxItemIndex(_cmbBoxHAlign), True);
    rvfpkAbsPosition:
      rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatHorizontalOffset,
        FRVStyle.RVUnitsToUnits(BoxProps.HOffs, CP.UnitsDisplay), True);
    rvfpkPercentPosition:
      rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatHorizontalOffset, Round(BoxProps.HOffsPct*1000), True);
  end;
  // box vertical position ---------
  rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatVerticalAnchor, ord(GetVerticalAnchor), True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatVerticalPositionKind, GetXBoxItemIndex(_cmbBoxVPosition), True);
  case TRVFloatPositionKind(GetXBoxItemIndex(_cmbBoxVPosition)) of
    rvfpkAlignment:
      rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatVerticalAlign, GetXBoxItemIndex(_cmbBoxVAlign), True);
    rvfpkAbsPosition:
      rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatVerticalOffset,
        FRVStyle.RVUnitsToUnits(BoxProps.VOffs, CP.UnitsDisplay), True);
    rvfpkPercentPosition:
      rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatVerticalOffset, Round(BoxProps.VOffsPct*1000), True);
  end;
  // cell ----------------------------
  rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatRelativeToCell, ord(GetCheckBoxChecked(_cbRelativeToCell)), True);
  // box position in text ------------
  rve.SetCurrentItemExtraIntPropertyEx(rveipcFloatPositionInText, rgBoxZPos.ItemIndex, True);
  // vertical align
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxVAlign, ord(GetContentVAlign), True);
  // box size ------------------------
  case GetXBoxItemIndex(_cmbBoxHeight) of
    0:
      begin
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxHeightType, ord(rvbhtAbsolute), True);
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxHeight,
          FRVStyle.RVUnitsToUnits(BoxProps.Height, CP.UnitsDisplay), True);
      end;
    1:
      begin
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxHeightType, ord(GetHeightType), True);
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxHeight, Round(BoxProps.HeightPct*1000), True);
      end;
    2:
      begin
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxHeightType, ord(rvbhtAuto), True);
      end;
  end;
  case GetXBoxItemIndex(_cmbBoxWidth) of
    0:
      begin
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxWidthType, ord(rvbwtAbsolute), True);
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxWidth,
          FRVStyle.RVUnitsToUnits(BoxProps.Width, CP.UnitsDisplay), True);
      end;
    1:
      begin
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxWidthType, ord(GetWidthType), True);
        rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxWidth, Round(BoxProps.WidthPct*1000), True);
      end;
  end;
  // box background ------------------------
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderColor, BoxProps.Border.Color, True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderWidth, BoxProps.Border.Width, True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderInternalWidth, BoxProps.Border.InternalWidth, True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderStyle, ord(BoxProps.Border.Style), True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderVisibleBorders_Left,   ord(BoxProps.Border.VisibleBorders.Left),   True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderVisibleBorders_Top,    ord(BoxProps.Border.VisibleBorders.Top),    True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderVisibleBorders_Right,  ord(BoxProps.Border.VisibleBorders.Right),  True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderVisibleBorders_Bottom, ord(BoxProps.Border.VisibleBorders.Bottom), True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderBorderOffsets_Left,   BoxProps.Border.BorderOffsets.Left,   True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderBorderOffsets_Top,    BoxProps.Border.BorderOffsets.Top,    True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderBorderOffsets_Right,  BoxProps.Border.BorderOffsets.Right,  True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxBorderBorderOffsets_Bottom, BoxProps.Border.BorderOffsets.Bottom, True);

  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxColor, BoxProps.Background.Color, True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxPadding_Left,   BoxProps.Background.BorderOffsets.Left,   True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxPadding_Top,    BoxProps.Background.BorderOffsets.Top,    True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxPadding_Right,  BoxProps.Background.BorderOffsets.Right,  True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcBoxPadding_Bottom, BoxProps.Background.BorderOffsets.Bottom, True);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.GetSequence(rve: TCustomRichViewEdit);
begin
  rve.SetCurrentItemExtraStrPropertyEx(rvespcSeqName, Trim(GetEditText(_cmbSeqName)), True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcSeqNumberType, GetXBoxItemIndex(_cmbSeqType), True);
  rve.SetCurrentItemExtraIntPropertyEx(rveipcSeqReset, ord(GetRadioButtonChecked(_rbSeqReset)), True);
  if not seSeqStartFrom.Indeterminate then
    rve.SetCurrentItemExtraIntPropertyEx(rveipcSeqStartFrom, seSeqStartFrom.AsInteger, True);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TfrmRVItemProp.GetTableBestWidth(Units: TRVStyleUnits; DefValue: TRVStyleLength): TRVStyleLength;
begin
  Result := DefValue;
  case GetXBoxItemIndex(_cmbTWType) of
    0: // default width
      Result := 0;
    1: // %
      if not seTableWidth.Indeterminate then
        if TableProps.TableWidthPrc>100 then
          Result := -100
        else
          Result := -TableProps.TableWidthPrc;
    2: // CP.UnitsDisplay
      if not seTableWidth.Indeterminate then
        Result := FRVStyle.RVUnitsToUnitsEx(TableProps.TableWidth,
          TRVAControlPanel(ControlPanel).UnitsDisplay, Units);
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.AdjustTableProps;
begin
  if TableProps.CellHSpacing<0 then
    TableProps.CellHSpacing := - TableProps.CellBorderWidth;
  if TableProps.CellVSpacing<0 then
    TableProps.CellVSpacing := - TableProps.CellBorderWidth;
  if TableProps.BorderHSpacing<0 then
    TableProps.BorderHSpacing := -Min(TableProps.CellBorderWidth, TableProps.BorderWidth);
  if TableProps.BorderVSpacing<0 then
    TableProps.BorderVSpacing := -Min(TableProps.CellBorderWidth, TableProps.BorderWidth);

  case TRVAControlPanel(ControlPanel).UserInterface of
  rvauiRTF:
    begin
      TableProps.TableBackgroundChanged := True;
      TableProps.TableBackgroundStyle   := rvbsColor;
      TableProps.TableBackgroundImage.Free;
      TableProps.TableBackgroundImage := nil;
      TableProps.CellBackgroundChanged := True;
      TableProps.CellBackgroundStyle   := rvbsColor;
      TableProps.CellBackgroundImage.Free;
      TableProps.CellBackgroundImage := nil;
      TableProps.ImageFileName := '';
      TableProps.CellImageFileName := '';
    end;
  rvauiHTML:
    begin
      rgCellRotation.ItemIndex := -1;
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.GetTable(rve: TCustomRichViewEdit; table: TRVTableItemInfo);
var Data, ItemNo: Integer;
var r,c,sr,sc,rs,cs,v: Integer;
    rowselected: Boolean;
    EditedCell: TRVTableCellData;
    CP: TRVAControlPanel;
    vl, vt, vr, vb: Boolean;


  // this function assumes rgCellRotation.ItemIndex>=0
  function GetRotation: TRVDocRotation;
  begin
    Result := rvrotNone;
    case CP.UserInterface of
      rvauiRTF:
        case rgCellRotation.ItemIndex of
          1: Result := rvrot90;
          2: Result := rvrot270;
        end;
      else
        Result := TRVDocRotation(rgCellRotation.ItemIndex);
    end;
  end;

  // this function assumes rgRowVAlign.ItemIndex>=0
  function GetRowVAlign: TRVCellVAlign;
  begin
    case rgRowVAlign.ItemIndex of
      1:   Result := rvcMiddle;
      2:   Result := rvcBottom;
      else Result := rvcTop;
    end;
  end;

begin
  CP := TRVAControlPanel(ControlPanel);
  ItemNo := table.GetMyItemNo;
  rve.BeginItemModify(ItemNo, Data);
  table.BestWidth := GetTableBestWidth(FRVStyle.Units, table.BestWidth);
  if not seCellHPadding.Indeterminate then
    table.CellHPadding := FRVStyle.RVUnitsToUnits(seCellHPadding.Value, RVA_GetFineUnits(CP));
  if not seCellVPadding.Indeterminate then
    table.CellVPadding := FRVStyle.RVUnitsToUnits(seCellVPadding.Value, RVA_GetFineUnits(CP));
  AdjustTableProps;
  table.SetTableVisibleBorders(TableProps.TableVBLeft, TableProps.TableVBTop,
    TableProps.TableVBRight, TableProps.TableVBBottom);
  table.CellHSpacing := TableProps.CellHSpacing;
  table.CellVSpacing := TableProps.CellVSpacing;
  case CP.UserInterface of
    rvauiRTF:
      begin
        if TableProps.CellHSpacing<0 then
          table.BorderHSpacing := 0
        else
          table.BorderHSpacing := TableProps.CellHSpacing;
        table.BorderVSpacing := TableProps.BorderVSpacing;
        table.Color := clNone;
      end;
    rvauiHTML:
      begin
        if (TableProps.BorderHSpacing<TableProps.CellHSpacing) or (TableProps.CellHSpacing<0) then
          table.BorderHSpacing := TableProps.CellHSpacing
        else
          table.BorderHSpacing := TableProps.BorderHSpacing;
        if (TableProps.BorderVSpacing<TableProps.CellVSpacing) or (TableProps.CellVSpacing<0) then
          table.BorderVSpacing := TableProps.CellVSpacing
        else
          table.BorderVSpacing := TableProps.BorderVSpacing;        
        if not cmbTableColor.Indeterminate then
          table.Color := cmbTableColor.ChosenColor;
      end;
    else
      begin
        table.BorderHSpacing := TableProps.BorderHSpacing;
        table.BorderVSpacing := TableProps.BorderVSpacing;
        if not cmbTableColor.Indeterminate then
          table.Color := cmbTableColor.ChosenColor;
      end;
  end;
  table.BorderWidth      := TableProps.BorderWidth;
  table.BorderStyle      := TableProps.BorderStyle;
  table.BorderColor      := TableProps.BorderColorD;
  table.BorderLightColor := TableProps.BorderColorL;
  table.CellBorderWidth  := TableProps.CellBorderWidth;
  table.CellBorderColor  := TableProps.CellBorderColorD;
  table.CellBorderLightColor := TableProps.CellBorderColorL;
  table.CellBorderStyle  := TableProps.CellBorderStyle;

  if TableProps.TableBackgroundChanged then begin
    table.BackgroundStyle := TableProps.TableBackgroundStyle;
    table.BackgroundImage := TableProps.TableBackgroundImage;
    if StoreImageFileName then
      rve.SetItemExtraStrPropertyEd(ItemNo, rvespImageFileName, TableProps.ImageFileName, False);
  end;
  if GetCheckBoxChecked(_cbNoSplit) then
    table.PrintOptions := table.PrintOptions - [rvtoRowsSplit]
  else
    table.PrintOptions := table.PrintOptions + [rvtoRowsSplit];
  table.HeadingRowCount := seHRC.AsInteger;
  if IsTabSheetVisible(_tsRows) and (rgRowVAlign.ItemIndex>=0) then begin
    if not table.GetNormalizedSelectionBounds(True, sr,sc,cs,rs) then
      exit;
    if table.GetEditedCell(r,c)<>nil then
      EditedCell := table.Cells[r,c]
    else
      EditedCell := nil;
    for r := 0 to table.Rows.Count-1 do begin
      rowselected := False;
      for c := 0 to table.Rows[r].Count-1 do
        if (table.Cells[r,c]<>nil) and
         (table.IsCellSelected(r,c) or (EditedCell=table.Cells[r,c])) then begin
          rowselected := True;
          if TableProps.CellBackgroundChanged then begin
            table.SetCellBackgroundStyle(TableProps.CellBackgroundStyle, r,c);
            table.SetCellBackgroundImage(TableProps.CellBackgroundImage, r,c);
            if StoreImageFileName then
              table.SetCellBackgroundImageFileName(TableProps.CellImageFileName, r, c);
          end;
          case GetXBoxItemIndex(_cmbCellBWType) of
            0: // default
              table.SetCellBestWidth(0, r, c);
            1: // %
              if not seCellBestWidth.Indeterminate then begin
                if TableProps.CellWidthPrc>100 then
                  v := -100
                else
                  v := TableProps.CellWidthPrc;
                table.SetCellBestWidth(v, r, c);
              end;
             2: // CP.UnitsDisplay
               if not seCellBestWidth.Indeterminate then
                 table.SetCellBestWidth(
                   FRVStyle.RVUnitsToUnits(TableProps.CellWidth, CP.UnitsDisplay), r, c);
          end;
          if not seCellBestHeight.Indeterminate then
            table.SetCellBestHeight(FRVStyle.RVUnitsToUnits(seCellBestHeight.Value, CP.UnitsDisplay), r, c);
          if not cmbCellColor.Indeterminate then
            table.SetCellColor(cmbCellColor.ChosenColor, r,c);
          if rgCellVAlign.ItemIndex>=0 then
            table.SetCellVAlign(TRVCellVAlign(rgCellVAlign.ItemIndex), r, c);
          if rgCellRotation.ItemIndex>=0 then
            table.SetCellRotation(GetRotation, r, c);
          table.Cells[r,c].VisibleBorders.GetValues(vl, vt, vr, vb);
          if TableProps.CellDefVBLeft then
            vl := TableProps.CellVBLeft;
          if TableProps.CellDefVBRight then
            vr := TableProps.CellVBRight;
          if TableProps.CellDefVBTop then
            vt := TableProps.CellVBTop;
          if TableProps.CellDefVBBottom then
            vb := TableProps.CellVBBottom;
          table.SetCellVisibleBorders(vl,vt,vr,vb, r, c);
          if cmbCellBorderColor.Visible and not cmbCellBorderColor.Indeterminate then
            if cmbCellBorderColor.ChosenColor=cmbCellBorderColor.AutoColor then
              table.SetCellBorderColor(clNone, r,c)
            else
              table.SetCellBorderColor(cmbCellBorderColor.ChosenColor, r,c);
          if cmbCellBorderLightColor.Visible and not cmbCellBorderLightColor.Indeterminate then
            if cmbCellBorderLightColor.ChosenColor=cmbCellBorderLightColor.AutoColor then
              table.SetCellBorderLightColor(clNone, r,c)
            else
              table.SetCellBorderLightColor(cmbCellBorderLightColor.ChosenColor, r,c);
        end;
      if rowselected then begin
        if rgRowVAlign.ItemIndex>=0 then
          table.SetRowVAlign(GetRowVAlign, r);
        if GetCheckBoxState(_cbRowKeepTogether)<>cbGrayed then
          table.SetRowKeepTogether(GetCheckBoxChecked(_cbRowKeepTogether), r);
      end;
    end;
  end;
  rve.EndItemModify(ItemNo, Data);
  rve.Change;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.FillTableAction(Action: TAction);
var Act: TrvActionInsertTable;
    CP: TRVAControlPanel;
begin
  Act := Action as TrvActionInsertTable;
  CP := TRVAControlPanel(ControlPanel);
  Act.BestWidth := GetTableBestWidth(CP.UnitsProgram, Act.BestWidth);
  if not seCellHPadding.Indeterminate then
    Act.CellHPadding := FRVStyle.RVUnitsToUnitsEx(seCellHPadding.Value, RVA_GetFineUnits(CP), CP.UnitsProgram);
  if not seCellVPadding.Indeterminate then
    Act.CellVPadding := FRVStyle.RVUnitsToUnitsEx(seCellVPadding.Value, RVA_GetFineUnits(CP), CP.UnitsProgram);
  AdjustTableProps;
  Act.VisibleBorders.Left := TableProps.TableVBLeft;
  Act.VisibleBorders.Top := TableProps.TableVBTop;
  Act.VisibleBorders.Right := TableProps.TableVBRight;
  Act.VisibleBorders.Bottom := TableProps.TableVBBottom;
  Act.CellHSpacing := TableProps.CellHSpacing;
  Act.CellVSpacing := TableProps.CellVSpacing;
  case CP.UserInterface of
    rvauiRTF:
      begin
        if TableProps.CellHSpacing<0 then
          Act.BorderHSpacing := 0
        else
          Act.BorderHSpacing := FRVStyle.GetAsDifferentUnits(TableProps.CellHSpacing, CP.UnitsProgram);
        Act.BorderVSpacing := FRVStyle.GetAsDifferentUnits(TableProps.BorderVSpacing, CP.UnitsProgram);
        Act.Color := clNone;
      end;
    rvauiHTML:
      begin
        if (TableProps.BorderHSpacing<TableProps.CellHSpacing) or (TableProps.CellHSpacing<0) then
          Act.BorderHSpacing := FRVStyle.GetAsDifferentUnits(TableProps.CellHSpacing, CP.UnitsProgram)
        else
          Act.BorderHSpacing := FRVStyle.GetAsDifferentUnits(TableProps.BorderHSpacing, CP.UnitsProgram);
        if (TableProps.BorderVSpacing<TableProps.CellVSpacing) or (TableProps.CellVSpacing<0) then
          Act.BorderVSpacing := FRVStyle.GetAsDifferentUnits(TableProps.CellVSpacing, CP.UnitsProgram)
        else
          Act.BorderVSpacing := FRVStyle.GetAsDifferentUnits(TableProps.BorderVSpacing, CP.UnitsProgram);
        if not cmbTableColor.Indeterminate then
          Act.Color := cmbTableColor.ChosenColor;
      end;
    else
      begin
        Act.BorderHSpacing := FRVStyle.GetAsDifferentUnits(TableProps.BorderHSpacing, CP.UnitsProgram);
        Act.BorderVSpacing := FRVStyle.GetAsDifferentUnits(TableProps.BorderVSpacing, CP.UnitsProgram);
        if not cmbTableColor.Indeterminate then
          Act.Color := cmbTableColor.ChosenColor;
      end;
  end;
  Act.BorderWidth      := FRVStyle.GetAsDifferentUnits(TableProps.BorderWidth, CP.UnitsProgram);
  Act.BorderStyle      := TableProps.BorderStyle;
  Act.BorderColor      := TableProps.BorderColorD;
  Act.BorderLightColor := TableProps.BorderColorL;
  Act.CellBorderWidth  := TableProps.CellBorderWidth;
  Act.CellBorderColor  := TableProps.CellBorderColorD;
  Act.CellBorderLightColor := TableProps.CellBorderColorL;
  Act.CellBorderStyle  := TableProps.CellBorderStyle;

  if TableProps.TableBackgroundChanged then begin
    Act.BackgroundStyle := TableProps.TableBackgroundStyle;
    Act.BackgroundPicture.Graphic := TableProps.TableBackgroundImage;
    //if StoreImageFileName then
    //  rve.SetItemExtraStrPropertyEd(ItemNo, rvespImageFileName, TableProps.ImageFileName, False);
  end;
  if GetCheckBoxChecked(_cbNoSplit) then
    Act.TablePrintOptions := Act.TablePrintOptions - [rvtoRowsSplit]
  else
    Act.TablePrintOptions := Act.TablePrintOptions + [rvtoRowsSplit];
  Act.HeadingRowCount := seHRC.AsInteger;
end;
{------------------------------------------------------------------------------}
// Graphic item UI
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbColorColorChange(Sender: TObject);
begin
  if Visible then
    SetCheckBoxChecked(_cbTransp, True);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbVShiftTypeClick(Sender: TObject);
var v: Extended;
begin
  if not Visible or (ImgProps.ImageHeight=0) then
    exit;
  if ImgProps.cmbVShiftItemIndex = GetXBoxItemIndex(_cmbVShiftType) then
    exit;
  v := seVShift.Value;
  if GetXBoxItemIndex(_cmbVShiftType)=0 then begin
    // convert to percents
    seVShift.IntegerValue := True;
    seVShift.MinValue := -500;
    seVShift.MaxValue := +500;
    seVShift.Increment := 1;
    if ImgProps.ImageHeight>0 then
      seVShift.Value :=
        Round (v * 100 / TRVStyle.PixelsToRVUnits(ImgProps.ImageHeight,
          RVA_GetFineUnits(TRVAControlPanel(ControlPanel))))
    else
      seVShift.Value := 0;
    end
  else begin
    // convert to units
    UpdateLengthSpinEdit(seVShift, True, False);
    seVShift.Value := TRVStyle.PixelsToRVUnits(
      Round(seVShift.Value * ImgProps.ImageHeight / 100),
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
  end;
  ImgProps.cmbVShiftItemIndex := GetXBoxItemIndex(_cmbVShiftType);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbImgSizeUnitsClick(Sender: TObject);
var w, h: TRVLength;
    CP: TRVAControlPanel;
    {..........................................................}
    procedure SetSEPixels(se: TRVSpinEdit);
    begin
      se.IntegerValue := True;
      se.Digits := 0;
      se.MinValue := 1;
      se.MaxValue := 2100;
      se.Increment := 1;
    end;
    {..........................................................}
begin
  if not Visible or (GetXBoxItemCount(_cmbImgSizeUnits)<2) then
    exit;
  w := seWidth.Value;
  h := seHeight.Value;
  CP := TRVAControlPanel(ControlPanel);
  case GetXBoxItemIndex(_cmbImgSizeUnits) of
    0: // pixels
      if not ImgProps.SizeInPixels then begin
        // from pixels to CP.UnitsDisplay
        SetSEPixels(seWidth);
        SetSEPixels(seHeight);
        if not seWidth.Indeterminate then
          seWidth.Value := TRVStyle.RVUnitsToPixels(w, CP.UnitsDisplay);
        if not seHeight.Indeterminate then
          seHeight.Value := TRVStyle.RVUnitsToPixels(h, CP.UnitsDisplay);
        ImgProps.SizeInPixels := True;
      end;
    1: // CP.UnitsDisplay
      if ImgProps.SizeInPixels then begin
        // from CP.UnitsDisplay to pixels
        UpdateLengthSpinEdit(seWidth, False, True);
        UpdateLengthSpinEdit(seHeight, False, True);
        seWidth.MinValue := TRVStyle.PixelsToRVUnits(1, CP.UnitsDisplay);
        seHeight.MinValue := TRVStyle.PixelsToRVUnits(1, CP.UnitsDisplay);
        if not seWidth.Indeterminate and (ImgProps.ImageWidth<>0) then
          seWidth.Value := TRVStyle.PixelsToRVUnits(Round(w), CP.UnitsDisplay);
        if not seHeight.Indeterminate and (ImgProps.ImageHeight<>0) then
          seHeight.Value := TRVStyle.PixelsToRVUnits(Round(h), CP.UnitsDisplay);
        ImgProps.SizeInPixels := False;
      end
  end;
  UpdateImgPrcSize(seWidth,  seWidthPrc,  ImgProps.ImageWidth);
  UpdateImgPrcSize(seHeight, seHeightPrc, ImgProps.ImageHeight);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.UpdateImgPrcSize(se, sePrc: TRVSpinEdit; OriginalSize: Integer);
begin
  if OriginalSize=0 then begin
    FUpdatingCombos := True;
    try
      sePrc.Indeterminate := True;
    finally
      FUpdatingCombos := False;
    end;
    exit;
  end;
  if se.Indeterminate then begin
    FUpdatingCombos := True;
    try
      sePrc.Value := 100;
    finally
      FUpdatingCombos := False;
    end;
    exit;
  end;
  FUpdatingCombos := True;
  try
    case GetXBoxItemIndex(_cmbImgSizeUnits) of
      0: // from pixels to %
        sePrc.Value := se.Value * 100 / OriginalSize;
      1: // from CP.UnitsDisplay to %
        sePrc.Value := TRVStyle.RVUnitsToPixels(se.Value, TRVAControlPanel(ControlPanel).UnitsDisplay) * 100 / OriginalSize;
    end;
  finally
    FUpdatingCombos := False;  
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.UpdateImgSizeFromPrc(se, sePrc: TRVSpinEdit; OriginalSize: Integer);
var Prc: Extended;
begin
  if OriginalSize=0 then begin
    exit;
  end;
  if sePrc.Indeterminate then
    Prc := 100
  else
    Prc := sePrc.Value;
  FUpdatingCombos := True;
  try
    case GetXBoxItemIndex(_cmbImgSizeUnits) of
      0: // % to pixels
        se.Value := Round(Prc * OriginalSize / 100);
      1: // from % to CP.UnitsDisplay
        se.Value := TRVStyle.PixelsToRVUnits(
          Round(Prc * OriginalSize / 100), TRVAControlPanel(ControlPanel).UnitsDisplay);
    end;
  finally
    FUpdatingCombos := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnChangeImageClick(Sender: TObject);
var opd: TOpenPictureDialog;
begin
  opd := TOpenPictureDialog.Create(Self);
  try
    if Filter<>'' then
      opd.Filter := Filter;
    if opd.Execute then begin
      ImgProps.OriginalImage := nil;
      img.Picture.LoadFromFile(opd.FileName);
      img.Picture.Graphic.Transparent := not (img.Picture.Graphic is TBitmap);
      ImgProps.ImageFileName := opd.FileName;
      ImgProps.ImageFileNameChanged := True;
      StoreImgSize;
      img.Stretch := (ImgProps.ImageWidth>img.Width) or (ImgProps.ImageHeight>img.Height);
    end;
  finally
    StoreImgSize;  
    UpdateLblDefSize;
    _gbTransp.Visible := img.Picture.Graphic is TBitmap;
    opd.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnSaveImageClick(Sender: TObject);
begin
  if img.Picture.Graphic=nil then
    exit;
  spd.Filter := GraphicFilter(TGraphicClass(img.Picture.Graphic.ClassType));
  spd.DefaultExt := GraphicExtension(TGraphicClass(img.Picture.Graphic.ClassType));
  if spd.Execute then
    img.Picture.SaveToFile(spd.FileName);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.UpdateLblDefSize;
var s: TRVALocString;
begin
  if (RVA_GetCharSet(ControlPanel)=ARABIC_CHARSET) or (RVA_GetCharSet(ControlPanel)=HEBREW_CHARSET) then begin
    SetControlCaption(_lblDefSize, RVA_GetS(rvam_ip_DefaultSize, ControlPanel));
    s := RVAFormat('(%d x %d %s)', [ImgProps.ImageWidth, ImgProps.ImageHeight,
      RVA_GetUnitsName(rvuPixels, True, ControlPanel)]);
    if TRVAControlPanel(ControlPanel).UnitsDisplay<>rvuPixels then
      s := s + RVAFormat('  (%s x %s %s)',
        [seWidth.GetValueStringNoCheck(TRVStyle.PixelsToRVUnits(ImgProps.ImageWidth, TRVAControlPanel(ControlPanel).UnitsDisplay)),
         seHeight.GetValueStringNoCheck(TRVStyle.PixelsToRVUnits(ImgProps.ImageHeight, TRVAControlPanel(ControlPanel).UnitsDisplay)),
         RVA_GetUnitsName(TRVAControlPanel(ControlPanel).UnitsDisplay, True, ControlPanel)]);
      SetControlCaption(_lblDefSize2, s);
      _lblDefSize2.Left := _lblDefSize.Left+_lblDefSize.Width-_lblDefSize2.Width;
    end
  else begin
    s := RVAFormat(RVA_GetS(rvam_ip_DefaultSize, ControlPanel),
      [ImgProps.ImageWidth, ImgProps.ImageHeight])+
       ' '+RVA_GetUnitsName(rvuPixels, True, ControlPanel);
    if TRVAControlPanel(ControlPanel).UnitsDisplay<>rvuPixels then
      s := s + RVAFormat('  (%s x %s %s)',
        [seWidth.GetValueStringNoCheck(TRVStyle.PixelsToRVUnits(ImgProps.ImageWidth, TRVAControlPanel(ControlPanel).UnitsDisplay)),
         seHeight.GetValueStringNoCheck(TRVStyle.PixelsToRVUnits(ImgProps.ImageHeight, TRVAControlPanel(ControlPanel).UnitsDisplay)),
         RVA_GetUnitsName(TRVAControlPanel(ControlPanel).UnitsDisplay, True, ControlPanel)]);
    SetControlCaption(_lblDefSize, s);
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbAlignToChange(Sender: TObject);
begin
  seVShift.Enabled := GetXBoxItemIndex(_cmbAlignTo)<5;
  _cmbVShiftType.Enabled := seVShift.Enabled;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seWidthChange(Sender: TObject);
begin
  if FUpdatingCombos then
    exit;
  FUpdatingCombos := True;
  try
    UpdateImgPrcSize(seWidth, seWidthPrc, ImgProps.ImageWidth);
    ImgProps.LastChangedIsHeight := False;
  finally
    FUpdatingCombos := False;
  end;
  UpdateImgSizeProportionally;
end;

procedure TfrmRVItemProp.seHeightChange(Sender: TObject);
begin
  if FUpdatingCombos then
    exit;
  FUpdatingCombos := True;
  try
    UpdateImgPrcSize(seHeight, seHeightPrc, ImgProps.ImageHeight);
    ImgProps.LastChangedIsHeight := True;
  finally
    FUpdatingCombos := False;
  end;
  UpdateImgSizeProportionally;
end;

procedure TfrmRVItemProp.seWidthPrcChange(Sender: TObject);
begin
  if FUpdatingCombos then
    exit;
  FUpdatingCombos := True;
  try
    UpdateImgSizeFromPrc(seWidth, seWidthPrc, ImgProps.ImageWidth);
    ImgProps.LastChangedIsHeight := False;
  finally
    FUpdatingCombos := False;
  end;
  if not ImgProps.MakingProportional and GetCheckBoxChecked(_cbImgScaleProportionally) then begin
    ImgProps.MakingProportional := True;
    try
      if seWidthPrc.Indeterminate then
        seHeightPrc.Indeterminate := True
      else
        seHeightPrc.Value := seWidthPrc.Value;
    finally
      ImgProps.MakingProportional := False;
    end;
  end;
end;

procedure TfrmRVItemProp.seHeightPrcChange(Sender: TObject);
begin
  if FUpdatingCombos then
    exit;
  FUpdatingCombos := True;
  try
    UpdateImgSizeFromPrc(seHeight, seHeightPrc, ImgProps.ImageHeight);
    ImgProps.LastChangedIsHeight := True;
  finally
    FUpdatingCombos := False;
  end;
  if not ImgProps.MakingProportional and GetCheckBoxChecked(_cbImgScaleProportionally) then begin
    ImgProps.MakingProportional := True;
    try
      if seHeightPrc.Indeterminate then
        seWidthPrc.Indeterminate := True
      else
        seWidthPrc.Value := seHeightPrc.Value;
    finally
      ImgProps.MakingProportional := False;
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cbImgScaleProportionallyClick(Sender: TObject);
begin
  if not Visible then
    exit;
  UpdateImgSizeProportionally;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.UpdateImgSizeProportionally;
begin
  if ImgProps.MakingProportional or not GetCheckBoxChecked(_cbImgScaleProportionally) or
   (seWidth.Indeterminate and seHeight.Indeterminate) then
    exit;
  if seWidth.Indeterminate and not seHeight.Indeterminate then
    ImgProps.LastChangedIsHeight := True
  else if seHeight.Indeterminate and not seWidth.Indeterminate then
    ImgProps.LastChangedIsHeight := False;
  ImgProps.MakingProportional := True;
  try
    if ImgProps.LastChangedIsHeight then begin
      if ImgProps.ImageHeight>0 then begin
        seWidth.Value := seHeight.Value * ImgProps.ImageWidth / ImgProps.ImageHeight;
        {
        FUpdatingCombos := True;
        try
          seHeightPrc.Value := seWidthPrc.Value;
        finally
          FUpdatingCombos := False;
        end;
        }
      end;
      end
    else begin
      if ImgProps.ImageWidth>0 then begin
        seHeight.Value := seWidth.Value * ImgProps.ImageHeight / ImgProps.ImageWidth;
        {
        FUpdatingCombos := True;
        try
          seWidthPrc.Value := seHeightPrc.Value;
        finally
          FUpdatingCombos := False;
        end;
        }
      end;
    end;
  finally
    ImgProps.MakingProportional := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnImgSizeResetClick(Sender: TObject);
begin
  ImgProps.MakingProportional := True;
  try
    seWidth.Indeterminate := True;
    seHeight.Indeterminate := True;
  finally
    ImgProps.MakingProportional := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbImgBorderColorColorChange(Sender: TObject);
begin
  _cmbImgBorderWidth.Invalidate;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbImgBorderWidthDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  {$IFDEF USERVTNT}
  RVA_DrawCmbWidthItem(TTntComboBox(_cmbImgBorderWidth).Canvas, Rect, Index, True,
    cmbImgBorderColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
  {$ELSE}
  RVA_DrawCmbWidthItem(TComboBox(_cmbImgBorderWidth).Canvas, Rect, Index, True,
    cmbImgBorderColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFDEF USERVKSDEVTE}
procedure TfrmRVItemProp.tecmbImgBorderWidthDrawItem(Control: TWinControl;
  Canvas: TCanvas; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  RVA_DrawCmbWidthItem(Canvas, Rect, Index, True,
    cmbImgBorderColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
// Break item UI
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbBreakColorColorChange(Sender: TObject);
begin
  _cmbWidth.Invalidate;
  _cmbBreakStyle.Invalidate;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbWidthClick(Sender: TObject);
begin
  if GetXBoxItemIndex(_cmbWidth)>=0 then
    BreakProps.Width := RVA_GetComboBorderWidth2(
      GetXBoxItemIndex(_cmbWidth), False, FRVStyle, TRVAControlPanel(ControlPanel));
  _cmbBreakStyle.Invalidate;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.DrawBreak(Canvas: TCanvas; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
var Color, BackColor: TColor;
    LineWidth, Y: Integer;
    GraphicInterface: TRVWinGraphicInterface;
begin
  GraphicInterface := TRVWinGraphicInterface.Create;
  if odSelected in State then
    BackColor := clHighlight
  else
  {$IFDEF RICHVIEWDEFXE2}
  if StyleServices.Enabled then
    BackColor := StyleServices.GetStyleColor(scListBox)
  else
   {$ENDIF}
    BackColor := clWindow;
  Color := cmbBreakColor.ChosenColor;
  if not (odSelected in State) and
    (Abs(RV_GetLuminance(Color)-RV_GetLuminance(BackColor))<RVALUM_THRESHOLD) then
    BackColor := (not ColorToRGB(BackColor)) and $FFFFFF;
  Canvas.Brush.Color := BackColor;
  Canvas.FillRect(Rect);
  LineWidth := FRVStyle.GetAsPixels(BreakProps.Width);
  Canvas.Pen.Style := psInsideFrame;
  Canvas.Pen.Color := Color;
  Y := (Rect.Top+Rect.Bottom) div 2;
  case TRVBreakStyle(Index) of
    rvbsLine:
      RVDrawCustomHLine(Canvas, Color, rvlsNormal, LineWidth,
        Rect.Left, Rect.Right, Y, 0, GraphicInterface);
    rvbsRectangle:
      begin
        Canvas.Pen.Width := 1;
        GraphicInterface.Rectangle(Canvas, Rect.Left, Y-LineWidth div 2,
          Rect.Right, Y-LineWidth div 2+LineWidth);
      end;
    rvbs3d:
      begin
        Canvas.Pen.Width := 1;
        RVDrawEdge(Canvas,
          Classes.Rect(Rect.Left, Y-LineWidth div 2, Rect.Right-1,
            Y-LineWidth div 2+LineWidth-1),
          clBtnShadow, clBtnFace, 1, GraphicInterface);
      end;
    rvbsDotted:
      RVDrawCustomHLine(Canvas, Color, rvlsRoundDotted, LineWidth,
        Rect.Left, Rect.Right, Y, 0, GraphicInterface);
    rvbsDashed:
      RVDrawCustomHLine(Canvas, Color, rvlsDashed, LineWidth,
        Rect.Left, Rect.Right, Y, 0, GraphicInterface);
  end;
  GraphicInterface.Free;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbBreakStyleDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  {$IFDEF USERVTNT}
  DrawBreak(TTntComboBox(_cmbBreakStyle).Canvas, Index, Rect, State);
  {$ELSE}
  DrawBreak(TComboBox(_cmbBreakStyle).Canvas, Index, Rect, State);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbWidthDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  {$IFDEF USERVTNT}
  RVA_DrawCmbWidthItem(TTntComboBox(_cmbWidth).Canvas, Rect, Index, False,
    cmbBreakColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
  {$ELSE}
  RVA_DrawCmbWidthItem(TComboBox(_cmbWidth).Canvas, Rect, Index, False,
    cmbBreakColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{$IFDEF USERVKSDEVTE}
procedure TfrmRVItemProp.tecmbWidthDrawItem(Control: TWinControl;
  Canvas: TCanvas; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  RVA_DrawCmbWidthItem(Canvas, Rect, Index, False,
    cmbBreakColor.ChosenColor, State, ControlPanel as TRVAControlPanel);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.tecmbBreakStyleDrawItem(Control: TWinControl;
  Canvas: TCanvas; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  DrawBreak(Canvas, Index, Rect, State);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
// Table item UI
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seTableWidthChange(Sender: TObject);
begin
  if not Visible or FUpdatingCombos or seTableWidth.Indeterminate then
    exit;
  case GetXBoxItemIndex(_cmbTWType) of
    1: // %
      TableProps.TableWidthPrc := seTableWidth.AsInteger;
    2: // CP.UnitsDisplay
      TableProps.TableWidth := seTableWidth.Value;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbTWTypeClick(Sender: TObject);
var ItemIndex: Integer;
begin
  ItemIndex := GetXBoxItemIndex(_cmbTWType);
  FUpdatingCombos := True;
  try
    case ItemIndex of
      1: // %
        begin
          UpdatePercentSpinEdit(seTableWidth, 1);
          seTableWidth.Value := TableProps.TableWidthPrc;
        end;
      2: // CP.UnitsDisplay
        begin
          UpdateLengthSpinEdit(seTableWidth, False, True);
          seTableWidth.MinValue := seTableWidth.Increment;
          seTableWidth.Value := TableProps.TableWidth;
        end
    end;
    _lblTableDefWidth.Visible := ItemIndex=0;
    seTableWidth.Visible := ItemIndex<>0;
  finally
    FUpdatingCombos := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seCellBestWidthChange(Sender: TObject);
begin
  if not Visible or FUpdatingCombos or seCellBestWidth.Indeterminate then
    exit;
  case GetXBoxItemIndex(_cmbCellBWType) of
   -1:
      begin
        TableProps.CellWidth := seCellBestWidth.Value;
        SetXBoxItemIndex(_cmbCellBWType, 2);
        cmbCellBWTypeClick(_cmbCellBWType);
      end;
    1: // %
      TableProps.CellWidthPrc := seCellBestWidth.AsInteger;
    2: // CP.UnitsDisplay
      TableProps.CellWidth := seCellBestWidth.Value;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbCellBWTypeClick(Sender: TObject);
var ItemIndex: Integer;
begin
  ItemIndex := GetXBoxItemIndex(_cmbCellBWType);
  FUpdatingCombos := True;
  try
    case ItemIndex of
     -1: // indeterminate
       begin
         UpdateLengthSpinEdit(seCellBestWidth, False, True);
         seCellBestWidth.MinValue := seCellBestWidth.Increment;
         seCellBestWidth.Indeterminate := True;
       end;
      1: // %
        begin
          UpdatePercentSpinEdit(seCellBestWidth, 1);
          seCellBestWidth.Value := TableProps.CellWidthPrc;
        end;
      2: // CP.UnitsDisplay
        begin
          UpdateLengthSpinEdit(seCellBestWidth, False, True);
          seCellBestWidth.MinValue := seCellBestWidth.Increment;
          seCellBestWidth.Value := TableProps.CellWidth;
        end
    end;
    _lblCellDefWidth.Visible := ItemIndex=0;
    seCellBestWidth.Visible := ItemIndex<>0;
  finally
    FUpdatingCombos := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnSpacingClick(Sender: TObject);
var frm:TfrmRVSpacing;
    BHS, CHS, BVS, CVS: TRVStyleLength;
    UBHS, UCHS, UBVS, UCVS: Boolean;
begin

  frm := TfrmRVSpacing.Create(Application, ControlPanel);
  try
    frm.SetSpacing(TableProps.BorderHSpacing, TableProps.CellHSpacing,
      TableProps.BorderVSpacing, TableProps.CellVSpacing,
      FRVStyle, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
    frm.SetControlCaption(frm._gb,
      RVA_GetSHUnits(rvam_cs_GB, RVA_GetFineUnits(TRVAControlPanel(ControlPanel)), ControlPanel));
    if frm.ShowModal=mrOk then begin
      frm.GetSpacing(BHS, CHS, BVS, CVS, UBHS, UCHS, UBVS, UCVS, FRVStyle,
        RVA_GetFineUnits(TRVAControlPanel(ControlPanel)));
      if UBHS then
        TableProps.BorderHSpacing := BHS;
      if UBVS then
        TableProps.BorderVSpacing := BVS;
      if UCHS then
        TableProps.CellHSpacing := CHS;
      if UCVS then
        TableProps.CellVSpacing := CVS;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnTableBorderClick(Sender: TObject);
var frm: TfrmRVTableBrdr;
begin
  frm := TfrmRVTableBrdr.Create(Application, ControlPanel);
  try
    frm.ColorDialog := ColorDialog;
    frm.SetFormCaption(RVA_GetS(rvam_ip_TableBorderTitle, ControlPanel));
    frm.SetData(TableProps.BorderStyle, TableProps.BorderColorL,
      TableProps.BorderColorD, TableProps.BorderWidth, FRVStyle);
    if frm.ShowModal=mrOk then begin
      frm.GetData(TableProps.BorderStyle, TableProps.BorderColorL,
        TableProps.BorderColorD, TableProps.BorderWidth);
      UpdateTableBorders;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnCellBordersClick(Sender: TObject);
var frm: TfrmRVTableBrdr;
begin
  frm := TfrmRVTableBrdr.Create(Application, ControlPanel);
  try
    frm.ColorDialog := ColorDialog;
    frm.SetFormCaption(RVA_GetS(rvam_ip_CellBorderTitle, ControlPanel));
    frm.SetData(TableProps.CellBorderStyle, TableProps.CellBorderColorL,
      TableProps.CellBorderColorD, TableProps.CellBorderWidth, FRVStyle);
    if frm.ShowModal=mrOk then begin
      frm.GetData(TableProps.CellBorderStyle, TableProps.CellBorderColorL,
        TableProps.CellBorderColorD, TableProps.CellBorderWidth);
      UpdateCellBorders;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.UpdateCellBorders;

  procedure ShowHideBorderUI(Visible: Boolean);
  begin
    _btnCellVisibleBorders.Visible := Visible;
    _lblCellBorderColor.Visible := Visible;
    cmbCellBorderColor.Visible := Visible;
    _lblCellBorderLightColor.Visible := Visible;
    cmbCellBorderLightColor.Visible := Visible;
    _lblCellBorder.Visible := Visible;
  end;


begin
  if TableProps.CellBorderWidth=0 then begin
    ShowHideBorderUI(False);
    exit;
  end;
  ShowHideBorderUI(True);
  case TableProps.CellBorderStyle of
   rvtbRaised, rvtbLowered:
     begin
       _lblCellBorderColor.Visible := False;
       cmbCellBorderColor.Visible := False;
       _lblCellBorderLightColor.Visible := False;
       cmbCellBorderLightColor.Visible := False;
       _lblCellBorder.Visible := False;
     end;
   rvtbRaisedColor, rvtbLoweredColor:
     begin
       _lblCellBorderColor.Visible := True;
       cmbCellBorderColor.Visible := True;
       _lblCellBorderLightColor.Visible := True;
       cmbCellBorderLightColor.Visible := True;
       cmbCellBorderColor.AutoColor := TableProps.CellBorderColorD;
       cmbCellBorderLightColor.AutoColor := TableProps.CellBorderColorL;
       SetControlCaption(_lblCellBorderColor, RVA_GetS(rvam_ip_CellShadowColor, ControlPanel));
       if TableProps.FDefCellColorD and not cmbCellBorderColor.Indeterminate then
         cmbCellBorderColor.ChosenColor := cmbCellBorderColor.AutoColor;
       if TableProps.FDefCellColorL and not cmbCellBorderLightColor.Indeterminate then
         cmbCellBorderLightColor.ChosenColor := cmbCellBorderLightColor.AutoColor;
     end;
   rvtbColor:
     begin
       _lblCellBorderColor.Visible := True;
       cmbCellBorderColor.Visible := True;
       _lblCellBorderLightColor.Visible := False;
       cmbCellBorderLightColor.Visible := False;
       cmbCellBorderColor.AutoColor := TableProps.CellBorderColorD;
       SetControlCaption(_lblCellBorderColor,  RVA_GetS(rvam_ip_CellBorderColor, ControlPanel));
       if TableProps.FDefCellColorD and not cmbCellBorderColor.Indeterminate then
         cmbCellBorderColor.ChosenColor := cmbCellBorderColor.AutoColor;
     end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.UpdateTableBorders;
begin
  _btnTableVisibleBorders.Visible := TableProps.BorderWidth<>0;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnTableVisibleBordersClick(Sender: TObject);
var frm: TfrmRVVisibleSides;
    dl,dt,dr,db: Boolean;
begin
  frm := TfrmRVVisibleSides.Create(Application, ControlPanel);
  try
    dl := True;
    dt := True;
    dr := True;
    db := True;
    //frm.SetFormCaption(RVA_GetS(rvam_ip_TableBorderTitle, ControlPanel));
    frm.SetSides(TableProps.TableVBLeft, TableProps.TableVBTop,
      TableProps.TableVBRight, TableProps.TableVBBottom, dl, dt, dr, db);
    if frm.ShowModal=mrOk then begin
      frm.GetSides(TableProps.TableVBLeft, TableProps.TableVBTop,
        TableProps.TableVBRight, TableProps.TableVBBottom, dl, dt, dr, db);
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnCellVisibleBordersClick(Sender: TObject);
var frm: TfrmRVVisibleSides;
begin
  frm := TfrmRVVisibleSides.Create(Application, ControlPanel);
  try
    //frm.SetFormCaption(RVA_GetS(rvam_ip_CellBorderTitle, ControlPanel));
    frm.SetSides(TableProps.CellVBLeft, TableProps.CellVBTop,
      TableProps.CellVBRight, TableProps.CellVBBottom,
      TableProps.CellDefVBLeft, TableProps.CellDefVBTop,
      TableProps.CellDefVBRight, TableProps.CellDefVBBottom);
    if frm.ShowModal=mrOk then begin
      frm.GetSides(TableProps.CellVBLeft, TableProps.CellVBTop,
      TableProps.CellVBRight, TableProps.CellVBBottom,
      TableProps.CellDefVBLeft, TableProps.CellDefVBTop,
      TableProps.CellDefVBRight, TableProps.CellDefVBBottom);
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbCellBorderColorColorChange(Sender: TObject);
begin
  if not cmbCellBorderColor.Indeterminate then
    TableProps.FDefCellColorD := cmbCellBorderColor.ChosenColor=cmbCellBorderColor.AutoColor;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbCellBorderLightColorColorChange(
  Sender: TObject);
begin
  if not cmbCellBorderLightColor.Indeterminate then
    TableProps.FDefCellColorL := cmbCellBorderLightColor.ChosenColor=cmbCellBorderLightColor.AutoColor;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnTopClick(Sender: TObject);
begin
  {$IFDEF USERVTNT}
  if Sender is TTntSpeedButton then
  {$ELSE}
  if Sender is TSpeedButton then
  {$ENDIF}
    SetButtonFlat(TControl(Sender), False);
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnTableBackClick(Sender: TObject);
var frm: TfrmRVTableBack;
    Color: TColor;
begin
  frm := TfrmRVTableBack.Create(Application, ControlPanel);
  try
    Color := clNone;
    if not cmbTableColor.Indeterminate then
      Color := cmbTableColor.ChosenColor;
    frm.Init(Color, TableProps.TableBackgroundStyle, TableProps.TableBackgroundImage);
    frm.Filter := BackgroundFilter;
    if frm.ShowModal=mrOk then begin
      frm.GetResult(TableProps.TableBackgroundStyle, TableProps.TableBackgroundImage, TableProps.ImageFileName);
      TableProps.TableBackgroundChanged := True;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnCellBackClick(Sender: TObject);
var frm: TfrmRVTableBack;
    Color: TColor;
begin
  frm := TfrmRVTableBack.Create(Application, ControlPanel);
  try
    Color := clNone;
    frm.Init(Color, TableProps.CellBackgroundStyle, TableProps.CellBackgroundImage);
    frm.Filter := BackgroundFilter;
    if frm.ShowModal=mrOk then begin
      frm.GetResult(TableProps.CellBackgroundStyle, TableProps.CellBackgroundImage, TableProps.CellImageFileName);
      TableProps.CellBackgroundChanged := True;
    end;
  finally
    frm.Free;
  end;
end;
{------------------------------------------------------------------------------}
// Sidenote item UI
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.lstBoxPositionClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var Index: Integer;    
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  Index := GetXBoxItemIndex(_lstBoxPosition);
  _gbBoxHPos.Visible := Index=0;
  _gbBoxVPos.Visible := Index=1;
  rgBoxZPos.Visible := Index=2;
  _cbRelativeToCell.Visible := Index<>2;
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbBoxHeightClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var ItemIndex: Integer;
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  FUpdatingCombos := True;
  try
    ItemIndex := GetXBoxItemIndex(_cmbBoxHeight);
    seBoxHeight.Visible := ItemIndex<2;
    _lblBoxAutoHeight.Visible := not seBoxHeight.Visible;
    _lblBoxHeightTxt.Visible := ItemIndex=1;
    rbBoxHeightPage.Visible := ItemIndex=1;
    rbBoxHeightMainTextArea.Visible := ItemIndex=1;
    rbBoxHeightTopMargin.Visible := ItemIndex=1;
    rbBoxHeightBottomMargin.Visible := ItemIndex=1;
    case ItemIndex of
      0:
        begin
          UpdateLengthSpinEdit(seBoxHeight, False, True);
          seBoxHeight.Value := BoxProps.Height;
        end;
      1:
        begin
          UpdateSpinEditEx(seBoxHeight, 1, 1000, 2);
          seBoxHeight.Value := BoxProps.HeightPct;
        end;
    end;
  finally
    FUpdatingCombos := False;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbBoxWidthClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var ItemIndex: Integer;
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  FUpdatingCombos := True;
  try
    ItemIndex := GetXBoxItemIndex(_cmbBoxWidth);
    _lblBoxWidthTxt.Visible := ItemIndex=1;
    rbBoxWidthPage.Visible := ItemIndex=1;
    rbBoxWidthMainTextArea.Visible := ItemIndex=1;
    rbBoxWidthLeftMargin.Visible := ItemIndex=1;
    rbBoxWidthRightMargin.Visible := ItemIndex=1;
    _cbBoxWidthMirror.Visible := ItemIndex=1;
    if ItemIndex=0 then begin
      UpdateLengthSpinEdit(seBoxWidth, False, True);
      seBoxWidth.Value := BoxProps.Width;
      end
    else begin
      UpdateSpinEditEx(seBoxWidth, 1, 1000, 2);
      seBoxWidth.Value := BoxProps.WidthPct;
    end;
  finally
    FUpdatingCombos := False;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seBoxHeightChange(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if FUpdatingCombos or seBoxHeight.Indeterminate then
    exit;
  case GetXBoxItemIndex(_cmbBoxHeight) of
    0: BoxProps.Height := seBoxHeight.Value;
    1: BoxProps.HeightPct := seBoxHeight.Value;
  end;                   
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seBoxWidthChange(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if FUpdatingCombos or seBoxWidth.Indeterminate then
    exit;
  case GetXBoxItemIndex(_cmbBoxWidth) of
    0: BoxProps.Width := seBoxWidth.Value;
    1: BoxProps.WidthPct := seBoxWidth.Value;
  end;
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbBoxHPositionClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var ItemIndex: Integer;

    procedure UpdateForRTF(ItemIndex: Integer);
    begin
      if TRVAControlPanel(ControlPanel).UserInterface<>rvauiRTF then
        exit;
      if (ItemIndex=2) and rbBoxHCharacter.Checked then
        rbBoxHMainTextArea.Checked := True;
      rbBoxHCharacter.Visible := ItemIndex<>2;
    end;
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  FUpdatingCombos := True;
  try
    ItemIndex := GetXBoxItemIndex(_cmbBoxHPosition);
    seBoxHOffs.Visible := ItemIndex<>0;
    _cmbBoxHAlign.Visible := ItemIndex=0;
    case ItemIndex of
      0:
        begin
          _lblBoxHTxt.Visible := False;
        end;
      1:
        begin
          _lblBoxHTxt.Visible := True;
          UpdateLengthSpinEdit(seBoxHOffs, False, False);
          seBoxHOffs.Value := BoxProps.HOffs;
          SetControlCaption(_lblBoxHMU, RVA_GetUnitsName(TRVAControlPanel(ControlPanel).UnitsDisplay, True, ControlPanel));
          SetControlCaption(_lblBoxHTxt, RVA_GetS(rvam_ip_BoxPosToTheRightOfLeftSideOf));
        end;
      2:
        begin
          _lblBoxHTxt.Visible := True;
          UpdateSpinEditEx(seBoxHOffs, -1000, 1000, 2);
          seBoxHOffs.Value := BoxProps.HOffsPct;
          SetControlCaption(_lblBoxHMU, RVA_GetS(rvam_Percents, ControlPanel));
          SetControlCaption(_lblBoxHTxt, RVA_GetS(rvam_ip_BoxPosRelativeTo));
        end;
    end;
    UpdateForRTF(ItemIndex);
  finally
    FUpdatingCombos := False;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbBoxVPositionClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var ItemIndex: Integer;

    procedure UpdateForRTF(ItemIndex: Integer);
    begin
      if TRVAControlPanel(ControlPanel).UserInterface<>rvauiRTF then
        exit;
      if (ItemIndex=2) and (rbBoxVLine.Checked or rbBoxVPara.Checked) then
        rbBoxVMainTextArea.Checked := True
      else if (ItemIndex=0) and rbBoxVPara.Checked then
        rbBoxVMainTextArea.Checked := True;      
      rbBoxVLine.Visible := ItemIndex<>2;
      rbBoxVPara.Visible := ItemIndex=1;
    end;  
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  FUpdatingCombos := True;
  try
    ItemIndex := GetXBoxItemIndex(_cmbBoxVPosition);
    seBoxVOffs.Visible := ItemIndex<>0;
    _cmbBoxVAlign.Visible := ItemIndex=0;
    case ItemIndex of
      0:
        begin
          _lblBoxVTxt.Visible := False;
        end;
      1:
        begin
          _lblBoxVTxt.Visible := True;
          UpdateLengthSpinEdit(seBoxVOffs, False, False);
          seBoxVOffs.Value := BoxProps.VOffs;
          SetControlCaption(_lblBoxVMU, RVA_GetUnitsName(TRVAControlPanel(ControlPanel).UnitsDisplay, True, ControlPanel));
          SetControlCaption(_lblBoxVTxt, RVA_GetS(rvam_ip_BoxPosBelowTheTopSideOf));
        end;
      2:
        begin
          _lblBoxVTxt.Visible := True;
          UpdateSpinEditEx(seBoxVOffs, -1000, 1000, 2);
          seBoxVOffs.Value := BoxProps.VOffsPct;
          SetControlCaption(_lblBoxVMU, RVA_GetS(rvam_Percents, ControlPanel));
          SetControlCaption(_lblBoxVTxt, RVA_GetS(rvam_ip_BoxPosRelativeTo));
        end;
    end;
    UpdateForRTF(ItemIndex);
  finally
    FUpdatingCombos := False;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seBoxHOffsChange(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if FUpdatingCombos or seBoxHOffs.Indeterminate then
    exit;
  case GetXBoxItemIndex(_cmbBoxHPosition) of
    1: BoxProps.HOffs := seBoxHOffs.Value;
    2: BoxProps.HOffsPct := seBoxHOffs.Value;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seBoxVOffsChange(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if FUpdatingCombos or seBoxVOffs.Indeterminate then
    exit;
  case GetXBoxItemIndex(_cmbBoxVPosition) of
    1: BoxProps.VOffs := seBoxVOffs.Value;
    2: BoxProps.VOffsPct := seBoxVOffs.Value;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.btnBoxBorderClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var frm: TfrmRVParaBrdr;
    ValidProperties: TRVParaInfoBorderProperties;  
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  frm := TfrmRVParaBrdr.Create(Application, ControlPanel);
  try
    ValidProperties := [Low(TRVParaInfoBorderProperty)..High(TRVParaInfoBorderProperty)];
    frm.OnlyPositiveOffsets := True;
    frm.Caption := RVA_GetS(rvam_ip_BoxBorderAndBackTitle);
    frm.SetToForm(FRVStyle, BoxProps.Border, BoxProps.Background, ValidProperties);
    if frm.ShowModal=mrOk then
      frm.GetFromForm(BoxProps.Border, BoxProps.Background, ValidProperties);
  finally
    frm.Free;
  end;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cbBoxWidthMirrorClick(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if GetCheckBoxChecked(_cbBoxWidthMirror) then begin
    rbBoxWidthLeftMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorInnerMargin);
    rbBoxWidthRightMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorOuterMargin);
    end
  else begin
    rbBoxWidthLeftMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorLeftMargin);
    rbBoxWidthRightMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorRightMargin);
  end;
  rbBoxWidthLeftMargin.Repaint;
  rbBoxWidthRightMargin.Repaint;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cbBoxPosMirrorClick(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if GetCheckBoxChecked(_cbBoxPosMirror) then begin
    rbBoxHLeftMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorInnerMargin);
    rbBoxHRightMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorOuterMargin);
    end
  else begin
    rbBoxHLeftMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorLeftMargin);
    rbBoxHRightMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorRightMargin);
  end;
  rbBoxHLeftMargin.Repaint;
  rbBoxHRightMargin.Repaint;  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
// Sequence item UI
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.seSeqStartFromChange(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  if seSeqStartFrom.Indeterminate then
    SetRadioButtonChecked(_rbSeqContinue, True)
  else
    SetRadioButtonChecked(_rbSeqReset, True);  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbSeqNameClick(Sender: TObject);
{$IFNDEF RVDONOTUSESEQ}
var Index: Integer;
{$ENDIF}
begin
{$IFNDEF RVDONOTUSESEQ}
  Index := GetXBoxItemIndex(_cmbSeqName);
  if Index>=0 then
    SetXBoxItemIndex(_cmbSeqType, Integer(GetXBoxObject(_cmbSeqName, Index)));  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.cmbSeqNameChange(Sender: TObject);
begin
{$IFNDEF RVDONOTUSESEQ}
  _btnOk.Enabled := RVAIsSeqNameAllowed(Trim(GetEditText(_cmbSeqName)));  
{$ENDIF}
end;
{------------------------------------------------------------------------------}
// Localization and third-party control support
{------------------------------------------------------------------------------}
procedure TfrmRVItemProp.Localize;
var i,dx: Integer;
    CP: TRVAControlPanel;
begin
  inherited;
  CP := TRVAControlPanel(ControlPanel);
  gbTransp.Visible := CP.UserInterface=rvauiFull;
  gbWeb.Visible := CP.UserInterface in [rvauiFull, rvauiHTML];
  cbNoSplit.Visible := CP.UserInterface=rvauiFull;
  if CP.UserInterface=rvauiRTF then begin
    //seSpacing.MaxValue := 0.1;
    btnTableBack.Visible := False;
    lblTableColor.Visible := False;
    cmbTableColor.Visible := False;
    btnCellBack.Visible := False;
    btnTableVisibleBorders.Visible := False;
    btnCellVisibleBorders.Top := btnCellBack.Top;
    dx := cmbTableColor.Top-btnTableBorder.Top;
    btnTableBorder.Top := btnTableBorder.Top+dx;
    btnCellBorder.Top := btnCellBorder.Top+dx;
    btnTableVisibleBorders.Top := btnTableVisibleBorders.Top+dx;
  end;

  btnOk.Caption := RVA_GetSAsIs(rvam_btn_OK, CP);
  btnCancel.Caption := RVA_GetSAsIs(rvam_btn_Cancel, CP);
  Caption := RVA_GetS(rvam_ip_Title, CP);
  // pages
  tsImage.Caption := RVA_GetSHAsIs(rvam_ip_ImageTab, CP);
  tsImgLayout.Caption := RVA_GetSHAsIs(rvam_ip_LayoutTab, CP);
  tsImgAppearance.Caption := RVA_GetSHAsIs(rvam_ip_ImgAppTab, CP);
  tsImgMisc.Caption := RVA_GetSHAsIs(rvam_ip_ImgMiscTab, CP);  
  tsBreak.Caption := RVA_GetSHAsIs(rvam_ip_LineTab, CP);
  tsTable.Caption := RVA_GetSHAsIs(rvam_ip_TableTab, CP);
  tsRows.Caption := RVA_GetSHAsIs(rvam_ip_RowsTab, CP);
  tsCells.Caption := RVA_GetSHAsIs(rvam_ip_CellsTab, CP);
  tsBoxPosition.Caption := RVA_GetSHAsIs(rvam_ip_BoxPosTab, CP);
  tsBoxSize.Caption := RVA_GetSHAsIs(rvam_ip_BoxSizeTab, CP);
  tsBoxContent.Caption := RVA_GetSHAsIs(rvam_ip_BoxAppTab, CP);
  tsSeq.Caption := RVA_GetSHAsIs(rvam_ip_SeqTab, CP);
  // image - image
  lblPreview.Caption := RVA_GetSAsIs(rvam_ip_Preview, CP);
  gbTransp.Caption := RVA_GetSHAsIs(rvam_ip_Transparency, CP);
  cbTransp.Caption := RVA_GetSAsIs(rvam_ip_Transparent, CP);
  lblTrColor.Caption := RVA_GetSAsIs(rvam_ip_TrColor, CP);
  btnChangeImage.Caption := RVA_GetSAsIs(rvam_ip_Change, CP);
  btnSaveImage.Caption := RVA_GetSAsIs(rvam_ip_ImgSave, CP);
  // image - position and size
  gbVAlign.Caption := RVA_GetSHAsIs(rvam_ip_VAlign, CP);
  lblAlignTo.Caption := RVA_GetSAsIs(rvam_ip_VAlignValue, CP);
  cmbAlignTo.Items[0] := RVA_GetSAsIs(rvam_ip_VAlign1, CP);
  if CP.UserInterface=rvauiRTF then
    for i := 6 downto 1 do
      cmbAlignTo.Items.Delete(i)
  else begin
    cmbAlignTo.Items[1] := RVA_GetSAsIs(rvam_ip_VAlign2, CP);
    cmbAlignTo.Items[2] := RVA_GetSAsIs(rvam_ip_VAlign3, CP);
    cmbAlignTo.Items[3] := RVA_GetSAsIs(rvam_ip_VAlign4, CP);
    cmbAlignTo.Items[4] := RVA_GetSAsIs(rvam_ip_VAlign5, CP);
    cmbAlignTo.Items[5] := RVA_GetSAsIs(rvam_ip_VAlign6, CP);
    cmbAlignTo.Items[6] := RVA_GetSAsIs(rvam_ip_VAlign7, CP);
  end;
  lblVShift.Caption := RVA_GetSAsIs(rvam_ip_ShiftBy, CP);
  cmbVShiftType.Items[0] := RVA_GetSAsIs(rvam_Percents, CP);
  cmbVShiftType.Items[1] := RVA_GetUnitsNameAsIs(RVA_GetFineUnits(CP), True, CP);
  gbSize.Caption := RVA_GetSHAsIs(rvam_ip_Stretch, CP);
  lblWidth.Caption := RVA_GetSAsIs(rvam_ip_Width, CP);
  lblHeight.Caption := RVA_GetSAsIs(rvam_ip_Height, CP);
  cmbImgSizeUnits.Items.Add(RVA_GetUnitsNameAsIs(rvuPixels, True, CP));
  if CP.UnitsDisplay<>rvuPixels then
    cmbImgSizeUnits.Items.Add(RVA_GetUnitsNameAsIs(CP.UnitsDisplay, True, CP));
  cmbImgSizeUnits.ItemIndex := cmbImgSizeUnits.Items.Count-1;
  cbImgScaleProportionally.Caption := RVA_GetSAsIs(rvam_ip_ImgScaleProportionally, CP);
  btnImgSizeReset.Caption := RVA_GetSAsIs(rvam_ip_ImgSizeReset, CP);
  if (RVA_GetCharset(CP) = ARABIC_CHARSET) or (RVA_GetCharset(CP) = HEBREW_CHARSET) then begin
    lblDefSize2.Visible := True;
    lblDefSize2.Caption := '';
    lblDefSize.Top := lblDefSize2.Top-lblDefSize.Height;
  end;
  // image - appearance
  gbImgInsideBorder.Caption := RVA_GetSHAsIs(rvam_ip_ImgInsideBorderGB, CP);;
  gbImgBorder.Caption := RVA_GetSHAsIs(rvam_ip_ImgBorderGB, CP);
  gbImgOutsideBorder.Caption := RVA_GetSHAsIs(rvam_ip_ImgOutsideBorderGB, CP);;
  lblImgSpacingMU.Caption := RVA_GetUnitsNameAsIs(RVA_GetFineUnits(CP), True, CP);
  lblImgOuterHSpacingMU.Caption := lblImgSpacingMU.Caption;
  lblImgOuterVSpacingMU.Caption := lblImgSpacingMU.Caption;
  lblImgColor.Caption := RVA_GetSAsIs(rvam_ip_ImgFillColor, CP);
  lblImgSpacing.Caption := RVA_GetSAsIs(rvam_ip_ImgPadding, CP);
  lblImgBorderColor.Caption := RVA_GetSAsIs(rvam_ip_ImgBorderColor, CP);
  lblImgBorderWidth.Caption := RVA_GetSAsIs(rvam_ip_ImgBorderWidth, CP);
  lblImgOuterHSpacing.Caption := RVA_GetSAsIs(rvam_ip_ImgHSpacing, CP);
  lblImgOuterVSpacing.Caption := RVA_GetSAsIs(rvam_ip_ImgVSpacing, CP);;
  // image - misc
  gbWeb.Caption := RVA_GetSHAsIs(rvam_ip_Web, CP);
  lblAlt.Caption := RVA_GetSAsIs(rvam_ip_Alt, CP);
  gbImgMisc.Caption := RVA_GetSHAsIs(rvam_ip_ImgMisc, CP);
  lblImgHint.Caption := RVA_GetSAsIs(rvam_ip_ImgHint, CP);
  // break
  gbBreak.Caption :=  RVA_GetSHAsIs(rvam_ip_HorzLine, CP);
  lblBreakColor.Caption := RVA_GetSAsIs(rvam_ip_HLColor, CP);
  lblBreakWidth.Caption := RVA_GetSAsIs(rvam_ip_HLWidth, CP);
  lblBreakStyle.Caption := RVA_GetSAsIs(rvam_ip_HLStyle, CP);
  // table - table
  gbTableSize.Caption := RVA_GetSHAsIs(rvam_ip_TableGB, CP);
  lblTableWidth.Caption := RVA_GetSAsIs(rvam_ip_TableWidth, CP);
  lblTableColor.Caption := RVA_GetSAsIs(rvam_ip_TableColor, CP);
  lblCellHPadding.Caption := RVA_GetSAsIs(rvam_ip_CellHPadding, CP);
  lblCellVPadding.Caption := RVA_GetSAsIs(rvam_ip_CellVPadding, CP);
  lblCellHPaddingMU.Caption := RVA_GetUnitsNameAsIs(RVA_GetFineUnits(CP), True, CP);
  lblCellVPaddingMU.Caption := RVA_GetUnitsNameAsIs(RVA_GetFineUnits(CP), True, CP);
  lblTableDefWidth.Caption := RVA_GetSAsIs(rvam_ip_TableAutoSizeLbl, CP);
  cmbTWType.Items[0] := RVA_GetSAsIs(rvam_ip_TableAutoSize, CP);
  cmbTWType.Items[1] := RVA_GetSAsIs(rvam_Percents, CP);
  cmbTWType.Items[2] := RVA_GetUnitsNameAsIs(CP.UnitsDisplay, True, CP);
  btnSpacing.Caption := RVA_GetSAsIs(rvam_ip_CellSpacing, CP);
  gbTableBackAndBorder.Caption := RVA_GetSHAsIs(rvam_ip_TableBorderGB, CP);
  btnTableBorder.Caption := RVA_GetSAsIs(rvam_ip_TableBorder, CP);
  btnCellBorder.Caption := RVA_GetSAsIs(rvam_ip_CellBorder, CP);
  btnTableVisibleBorders.Caption := RVA_GetSAsIs(rvam_ip_TableVisibleBorders, CP);
  gbTablePrint.Caption := RVA_GetSHAsIs(rvam_ip_TablePrinting, CP);
  cbNoSplit.Caption := RVA_GetSAsIs(rvam_ip_KeepOnPage, CP);
  lblHeadingRows.Caption := RVA_GetSAsIs(rvam_ip_HeadingRows, CP);
  //lblHeadingRowsTip.Caption := RVA_GetSAsIs(rvam_ip_HeadingRowsTip, CP);
  // table - row
  rgRowVAlign.Caption := RVA_GetSH(rvam_ip_VAlign, CP);
  rgRowVAlign.Items[0].Caption := RVA_GetS(rvam_ip_VATop, CP);
  rgRowVAlign.Items[1].Caption := RVA_GetS(rvam_ip_VACenter, CP);
  rgRowVAlign.Items[2].Caption := RVA_GetS(rvam_ip_VABottom, CP);
  gbRowPrint.Caption := RVA_GetSHAsIs(rvam_ip_TablePrinting, CP);
  cbRowKeepTogether.Caption := RVA_GetS(rvam_ip_RowKeepTogether, CP);
  // table - cell
  gbCells.Caption := RVA_GetSHAsIs(rvam_ip_CellSettings, CP);
  lblCellWidth.Caption := RVA_GetSAsIs(rvam_ip_CellBestWidth, CP);
  lblCellHeight.Caption := RVA_GetSAsIs(rvam_ip_CellBestHeight, CP);
  lblCellColor.Caption := RVA_GetSAsIs(rvam_ip_CellFillColor, CP);
  lblCellDefWidth.Caption := RVA_GetSAsIs(rvam_ip_TableAutoSizeLbl, CP);
  cmbCellBWType.Items[0] := RVA_GetSAsIs(rvam_ip_TableAutoSize, CP);
  cmbCellBWType.Items[1] := RVA_GetSAsIs(rvam_Percents, CP);
  cmbCellBWType.Items[2] := RVA_GetUnitsNameAsIs(CP.UnitsDisplay, True, CP);
  lblCellBestHeightMU.Caption := RVA_GetUnitsNameAsIs(CP.UnitsDisplay, True, CP);
  rgCellVAlign.Caption := RVA_GetSH(rvam_ip_VAlign, CP);
  rgCellVAlign.Items[0].Caption := RVA_GetS(rvam_ip_VATop, CP);
  rgCellVAlign.Items[1].Caption := RVA_GetS(rvam_ip_VACenter, CP);
  rgCellVAlign.Items[2].Caption := RVA_GetS(rvam_ip_VABottom, CP);
  rgCellVAlign.Items[3].Caption := RVA_GetS(rvam_ip_VADefault, CP);
  rgCellRotation.Caption := RVA_GetSH(rvam_ip_CellRotationGB, CP);
  rgCellRotation.Items[0].Caption := RVA_GetS(rvam_ip_CellRotation0, CP);
  rgCellRotation.Items[1].Caption := RVA_GetS(rvam_ip_CellRotation90, CP);
  rgCellRotation.Items[2].Caption := RVA_GetS(rvam_ip_CellRotation180, CP);
  rgCellRotation.Items[3].Caption := RVA_GetS(rvam_ip_CellRotation270, CP);
  gbCellBorder.Caption := RVA_GetSHAsIs(rvam_ip_TableBorderGB, CP);
  lblCellBorder.Caption := RVA_GetSAsIs(rvam_ip_CellBorderLbl, CP);
  btnCellVisibleBorders.Caption := RVA_GetSAsIs(rvam_ip_CellVisibleBorders, CP);
  lblCellBorderColor.Caption := RVA_GetSAsIs(rvam_ip_CellShadowColor, CP);
  lblCellBorderLightColor.Caption := RVA_GetSAsIs(rvam_ip_CellLightColor, CP);
  cmbColor.DefaultCaption := RVA_GetS(rvam_ip_TrAutoColor, CP);
  btnTableBack.Caption := RVA_GetSAsIs(rvam_ip_BackgroundImage, CP);
  btnCellBack.Caption := RVA_GetSAsIs(rvam_ip_BackgroundImage, CP);
  // box - position
  lstBoxPosition.Items[0] := RVA_GetSAsIs(rvam_ip_BoxHPosGB, CP);
  lstBoxPosition.Items[1] := RVA_GetSAsIs(rvam_ip_BoxVPosGB, CP);
  lstBoxPosition.Items[2] := RVA_GetSAsIs(rvam_ip_BoxPosInTextGB, CP);
  gbBoxHPos.Caption := RVA_GetSHAsIs(rvam_ip_BoxHPosGB, CP);
  gbBoxVPos.Caption := RVA_GetSHAsIs(rvam_ip_BoxVPosGB, CP);
  rgBoxZPos.Caption := RVA_GetSHAsIs(rvam_ip_BoxPosInTextGB, CP);
  cmbBoxHPosition.Items[0] := RVA_GetSAsIs(rvam_ip_BoxPosAlign, CP);
  cmbBoxHPosition.Items[1] := RVA_GetSAsIs(rvam_ip_BoxPosAbsolute, CP);
  cmbBoxHPosition.Items[2] := RVA_GetSAsIs(rvam_ip_BoxPosRelative, CP);
  cmbBoxVPosition.Items[0] := RVA_GetSAsIs(rvam_ip_BoxPosAlign, CP);
  cmbBoxVPosition.Items[1] := RVA_GetSAsIs(rvam_ip_BoxPosAbsolute, CP);
  cmbBoxVPosition.Items[2] := RVA_GetSAsIs(rvam_ip_BoxPosRelative, CP);
  cmbBoxHAlign.Items[0] := RVA_GetSAsIs(rvam_ip_BoxHPosLeft, CP);
  cmbBoxHAlign.Items[1] := RVA_GetSAsIs(rvam_ip_BoxHPosCenter, CP);
  cmbBoxHAlign.Items[2] := RVA_GetSAsIs(rvam_ip_BoxHPosRight, CP);
  cmbBoxVAlign.Items[0] := RVA_GetSAsIs(rvam_ip_BoxVPosTop, CP);
  cmbBoxVAlign.Items[1] := RVA_GetSAsIs(rvam_ip_BoxVPosCenter, CP);
  cmbBoxVAlign.Items[2] := RVA_GetSAsIs(rvam_ip_BoxVPosBottom, CP);
  rgBoxZPos.Items[0].Caption := RVA_GetS(rvam_ip_BoxAboveText, CP);
  rgBoxZPos.Items[1].Caption := RVA_GetS(rvam_ip_BoxBelowText, CP);
  rbBoxVPage.Caption := RVA_GetS(rvam_ip_BoxAnchorPage, CP);
  rbBoxVMainTextArea.Caption := RVA_GetS(rvam_ip_BoxAnchorMainTextArea, CP);
  rbBoxHLeftMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorLeftMargin, CP);
  rbBoxHRightMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorRightMargin, CP);
  rbBoxHCharacter.Caption := RVA_GetS(rvam_ip_BoxAnchorCharacter, CP);
  rbBoxHPage.Caption := RVA_GetS(rvam_ip_BoxAnchorPage, CP);
  rbBoxHMainTextArea.Caption := RVA_GetS(rvam_ip_BoxAnchorMainTextArea, CP);
  rbBoxVTopMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorTopMargin, CP);
  rbBoxVBottomMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorBottomMargin, CP);
  rbBoxVLine.Caption := RVA_GetS(rvam_ip_BoxAnchorLine, CP);
  rbBoxVPara.Caption := RVA_GetS(rvam_ip_BoxAnchorPara, CP);
  cbBoxPosMirror.Caption := RVA_GetSAsIs(rvam_ip_BoxMirroredMargins, CP);
  cbRelativeToCell .Caption := RVA_GetSAsIs(rvam_ip_BoxLayoutInTableCell, CP);
  // box - size
  gbBoxWidth.Caption := RVA_GetSHAsIs(rvam_ip_BoxWidthGB, CP);
  gbBoxHeight.Caption := RVA_GetSHAsIs(rvam_ip_BoxHeightGB, CP);
  lblBoxWidth.Caption := RVA_GetSHAsIs(rvam_ip_BoxWidth, CP);
  lblBoxHeight.Caption := RVA_GetSHAsIs(rvam_ip_BoxHeight, CP);
  lblBoxHeightTxt.Caption := RVA_GetSAsIs(rvam_ip_BoxPosRelativeTo, CP);
  lblBoxWidthTxt.Caption := RVA_GetSAsIs(rvam_ip_BoxPosRelativeTo, CP);
  lblBoxAutoHeight.Caption := RVA_GetSAsIs(rvam_ip_BoxHeightAutoLbl, CP);
  cmbBoxHeight.Items[0] := RVA_GetUnitsNameAsIs(CP.UnitsDisplay, True, CP);
  cmbBoxHeight.Items[1] := RVA_GetSAsIs(rvam_Percents, CP);
  cmbBoxHeight.Items[2] := RVA_GetSAsIs(rvam_ip_BoxHeightAuto, CP);
  cmbBoxWidth.Items[0] := RVA_GetUnitsNameAsIs(CP.UnitsDisplay, True, CP);
  cmbBoxWidth.Items[1] := RVA_GetSAsIs(rvam_Percents, CP);
  rbBoxHeightPage.Caption := RVA_GetS(rvam_ip_BoxAnchorPage, CP);
  rbBoxHeightMainTextArea.Caption := RVA_GetS(rvam_ip_BoxAnchorMainTextArea, CP);
  rbBoxHeightTopMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorTopMargin, CP);
  rbBoxHeightBottomMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorBottomMargin, CP);
  rbBoxWidthPage.Caption := RVA_GetS(rvam_ip_BoxAnchorPage2, CP);
  rbBoxWidthMainTextArea.Caption := RVA_GetS(rvam_ip_BoxAnchorMainTextArea2, CP);
  rbBoxWidthLeftMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorLeftMargin, CP);
  rbBoxWidthRightMargin.Caption := RVA_GetS(rvam_ip_BoxAnchorRightMargin, CP);
  cbBoxWidthMirror.Caption := RVA_GetSAsIs(rvam_ip_BoxMirroredMargins, CP);
  // box - appearance
  rgBoxVAlign.Caption := RVA_GetSH(rvam_ip_BoxVAlign, CP);
  rgBoxVAlign.Items[0].Caption := RVA_GetS(rvam_ip_BoxVAlignTop, CP);
  rgBoxVAlign.Items[1].Caption := RVA_GetS(rvam_ip_BoxVAlignCenter, CP);
  rgBoxVAlign.Items[2].Caption := RVA_GetS(rvam_ip_BoxVAlignBottom, CP);
  btnBoxBorder.Caption := RVA_GetSAsIs(rvam_ip_BoxBorderAndBack, CP);
  // number
  gbSeq.Caption := RVA_GetSHAsIs(rvam_in_PropGB, ControlPanel);
  lblSeqName.Caption := RVA_GetSAsIs(rvam_in_CounterName, ControlPanel);
  lblSeqType.Caption := RVA_GetSAsIs(rvam_in_NumType, ControlPanel);
  gbSeqNumbering.Caption := RVA_GetSHAsIs(rvam_in_NumberingRG, ControlPanel);
  rbSeqContinue.Caption := RVA_GetSHAsIs(rvam_in_Continue, ControlPanel);
  rbSeqReset.Caption := RVA_GetSHAsIs(rvam_in_StartFrom, ControlPanel);  
end;
{------------------------------------------------------------------------------}
{$IFDEF RVASKINNED}
procedure TfrmRVItemProp.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  if OldControl =_btnOk then
    _btnOk := NewControl
  else if OldControl =_gbTransp then
    _gbTransp := NewControl
  else if OldControl =_gbSize then
    _gbSize := NewControl
  else if OldControl =_txtAlt then
    _txtAlt := NewControl
  else if OldControl =_txtImgHint then
    _txtImgHint := NewControl
  else if OldControl =_gbCellBorder then
    _gbCellBorder := NewControl
  else if OldControl =_cbTransp then
    _cbTransp := NewControl
  else if OldControl =_cbNoSplit then
    _cbNoSplit := NewControl
  else if OldControl =_cbRowKeepTogether then
    _cbRowKeepTogether := NewControl
  else if OldControl =_cbImgScaleProportionally then
    _cbImgScaleProportionally := NewControl
  else if OldControl =_cmbAlignTo then
    _cmbAlignTo := NewControl
  else if OldControl =_cmbVShiftType then
    _cmbVShiftType := NewControl
  else if OldControl =_cmbImgSizeUnits then
    _cmbImgSizeUnits := NewControl
  else if OldControl =_cmbWidth then begin
    _cmbWidth := NewControl;
    {$IFDEF USERVKSDEVTE}
    TTeComboBox(_cmbWidth).OnDrawItem := tecmbWidthDrawItem;
    {$ENDIF}
    end
  else if OldControl =_cmbBreakStyle then begin
    _cmbBreakStyle := NewControl;
    {$IFDEF USERVKSDEVTE}
    TTeComboBox(_cmbBreakStyle).OnDrawItem := tecmbWidthDrawItem;
    {$ENDIF}
    end
  else if OldControl =_cmbImgBorderWidth then begin
    _cmbImgBorderWidth := NewControl;
    {$IFDEF USERVKSDEVTE}
    TTeComboBox(_cmbWidth).OnDrawItem := tecmbImgBorderWidthDrawItem;
    {$ENDIF}
    end
  else if OldControl =_cmbTWType then
    _cmbTWType := NewControl
  else if OldControl =_cmbCellBWType then
    _cmbCellBWType := NewControl
  else if OldControl =_lblDefSize then
    _lblDefSize := NewControl
  else if OldControl =_lblDefSize2 then
    _lblDefSize2 := NewControl
  else if OldControl =_lblTableDefWidth then
    _lblTableDefWidth := NewControl
  else if OldControl =_lblCellDefWidth then
    _lblCellDefWidth := NewControl
  else if OldControl =_lblCellBorderColor then
    _lblCellBorderColor := NewControl
  else if OldControl =_lblCellBorderLightColor then
    _lblCellBorderLightColor := NewControl
  else if OldControl =_lstBoxPosition then
    _lstBoxPosition := NewControl
  else if OldControl =_cbRelativeToCell then
    _cbRelativeToCell := NewControl
  else if OldControl =_cbBoxWidthMirror then
    _cbBoxWidthMirror := NewControl
  else if OldControl =_cbBoxPosMirror then
    _cbBoxPosMirror := NewControl
  else if OldControl =_gbBoxHPos then
    _gbBoxHPos := NewControl
  else if OldControl =_gbBoxVPos then
    _gbBoxVPos := NewControl
  else if OldControl =_cmbBoxWidth then
    _cmbBoxWidth := NewControl
  else if OldControl = _cmbBoxHeight then
    _cmbBoxHeight := NewControl
  else if OldControl = _lblBoxHeightTxt then
    _lblBoxHeightTxt := NewControl
  else if OldControl = _lblBoxWidthTxt then
    _lblBoxWidthTxt := NewControl
  else if OldControl = _cmbBoxHPosition then
    _cmbBoxHPosition := NewControl
  else if OldControl = _cmbBoxVPosition then
    _cmbBoxVPosition := NewControl
  else if OldControl = _cmbBoxHAlign then
    _cmbBoxHAlign := NewControl
  else if OldControl = _cmbBoxVAlign then
    _cmbBoxVAlign := NewControl
  else if OldControl = _lblBoxHMU then
    _lblBoxHMU := NewControl
  else if OldControl = _lblBoxVMU then
    _lblBoxVMU := NewControl
  else if OldControl = _lblBoxHTxt then
    _lblBoxHTxt := NewControl
  else if OldControl = _lblBoxVTxt then
    _lblBoxVTxt := NewControl
  else if OldControl = _lblBoxAutoHeight then
    _lblBoxAutoHeight := NewControl
  else if OldControl = _btnCellVisibleBorders then
    _btnCellVisibleBorders := NewControl
  else if OldControl = _lblCellBorder then
    _lblCellBorder := NewControl
  else if OldControl = _btnTableVisibleBorders then
    _btnTableVisibleBorders := NewControl
  else if OldControl = _rbSeqContinue then
    _rbSeqContinue := NewControl
  else if OldControl = _rbSeqReset then
    _rbSeqReset := NewControl
  else if OldControl = _cmbSeqName then
    _cmbSeqName := NewControl
  else if OldControl = _cmbSeqType then
    _cmbSeqType := NewControl
  else if OldControl =_pc then begin
    _pc := TWinControl(NewControl);
    {$IFDEF USERVKSDEVTE}
    _tsImage      := TTePageControl(_pc).Pages[0];
    _tsImgLayout  := TTePageControl(_pc).Pages[1];
    _tsImgAppearance := TTePageControl(_pc).Pages[2];
    _tsImgMisc    := TTePageControl(_pc).Pages[3];
    _tsBreak      := TTePageControl(_pc).Pages[4];
    _tsTable      := TTePageControl(_pc).Pages[5];
    _tsRows       := TTePageControl(_pc).Pages[6];
    _tsCells      := TTePageControl(_pc).Pages[7];
    _tsBoxPosition:= TTePageControl(_pc).Pages[8];
    _tsBoxSize    := TTePageControl(_pc).Pages[9];
    _tsBoxContent := TTePageControl(_pc).Pages[10];
    _tsSeq        := TTePageControl(_pc).Pages[11];
    {$ENDIF}
    {$IFDEF USERVTNT}
    _tsImage      := TTntPageControl(_pc).Pages[0];
    _tsImgLayout  := TTntPageControl(_pc).Pages[1];
    _tsImgAppearance := TTntPageControl(_pc).Pages[2];
    _tsImgMisc    := TTntPageControl(_pc).Pages[3];
    _tsBreak      := TTntPageControl(_pc).Pages[4];
    _tsTable      := TTntPageControl(_pc).Pages[5];
    _tsRows       := TTntPageControl(_pc).Pages[6];
    _tsCells      := TTntPageControl(_pc).Pages[7];
    _tsBoxPosition:= TTntPageControl(_pc).Pages[8];
    _tsBoxSize    := TTntPageControl(_pc).Pages[9];
    _tsBoxContent := TTntPageControl(_pc).Pages[10];
    _tsSeq        := TTntPageControl(_pc).Pages[11];
    {$ENDIF}
  end;
end;
{$ENDIF}


end.
