unit InfoRVFrm;

interface

{$I RV_Defs.inc}
{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, RVStyle, RVScroll, RichView, BaseRVFrm;

type
  TfrmRVInfo = class(TfrmRVBaseBase)
    RichView1: TRichView;
    RVStyle1: TRVStyle;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TfrmRVInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

end.
