
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Dialog for hyperlink properties                 }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit HypPropRVFrm;

interface

{$I RichViewActions.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, BaseRVFrm, HypHoverPropRVFrm, StdCtrls, RVColorCombo, RVALocalize;

type
  TfrmRVHypProp = class(TfrmRVHypHoverProp)
    gbNormal: TGroupBox;
    lblText: TLabel;
    cmbText: TRVColorCombo;
    cmbTextBack: TRVColorCombo;
    lblTextBack: TLabel;
    lblUnderlineColor: TLabel;
    cmbUnderlineColor: TRVColorCombo;
    lblUnderline: TLabel;
    cmbUnderline: TComboBox;
    procedure cbAsNormalClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbTextBackColorChange(Sender: TObject);
  private
    { Private declarations }
  protected
    {$IFDEF RVASKINNED}
    procedure OnCreateThemedControl(OldControl, NewControl: TControl); override;
    {$ENDIF}    
  public
    { Public declarations }
    _cmbUnderline: TControl;
    procedure Localize; override;
  end;

implementation
uses RichViewActions;

{$R *.dfm}

procedure TfrmRVHypProp.cbAsNormalClick(Sender: TObject);
begin
  if GetCheckBoxChecked(_cbAsNormal) then begin
    cmbHoverText.ChosenColor     := clNone;
    cmbHoverTextBack.ChosenColor := cmbTextBack.ChosenColor;
    cmbHoverUnderlineColor.ChosenColor := clNone;
  end;
  cmbHoverText.Enabled := not GetCheckBoxChecked(_cbAsNormal);
  cmbHoverTextBack.Enabled := not GetCheckBoxChecked(_cbAsNormal);
  cmbHoverUnderlineColor.Enabled := not GetCheckBoxChecked(_cbAsNormal);
end;

procedure TfrmRVHypProp.Localize;
begin
  inherited;
  gbNormal.Caption := RVA_GetSHAsIs(rvam_hp_GBNormal, ControlPanel);
  lblText.Caption := RVA_GetSAsIs(rvam_hp_Text, ControlPanel);
  lblTextBack.Caption := RVA_GetSAsIs(rvam_hp_TextBack, ControlPanel);
  lblUnderlineColor.Caption := RVA_GetSAsIs(rvam_hp_UnderlineColor, ControlPanel);
  cmbUnderlineColor.DefaultCaption := RVA_GetS(rvam_ip_TrAutoColor, ControlPanel);
  lblUnderline.Caption := RVA_GetSAsIs(rvam_hp_UnderlineType, ControlPanel);
  cmbUnderline.Items[0] := RVA_GetSAsIs(rvam_hp_UAlways, ControlPanel);
  cmbUnderline.Items[1] := RVA_GetSAsIs(rvam_hp_UNever, ControlPanel);
  cmbUnderline.Items[2] := RVA_GetSAsIs(rvam_hp_UActive, ControlPanel);
end;

procedure TfrmRVHypProp.FormCreate(Sender: TObject);
begin
  _cmbUnderline := cmbUnderline;
  inherited;
  cmbText.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
  cmbTextBack.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
  cmbUnderlineColor.ColorDialog := TRVAControlPanel(ControlPanel).ColorDialog;
end;

procedure TfrmRVHypProp.cmbTextBackColorChange(Sender: TObject);
begin
  if GetCheckBoxChecked(_cbAsNormal) then
    cmbHoverTextBack.ChosenColor := cmbTextBack.ChosenColor;
end;

{$IFDEF RVASKINNED}
procedure TfrmRVHypProp.OnCreateThemedControl(OldControl,
  NewControl: TControl);
begin
  inherited;
  if OldControl=_cmbUnderline then
   _cmbUnderline := NewControl;
end;
{$ENDIF}

end.
