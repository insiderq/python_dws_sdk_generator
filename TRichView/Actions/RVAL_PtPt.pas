
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Portuguese (Portuguese) translation             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{ Translated by:	Gilberto Fonte 2012-07-26			        }
{ Updated by: 		Gilberto Fonte 2012-08-31			        }
{*******************************************************}

unit RVAL_PtPt;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =                            
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'polegadas', 'cm', 'mm', 'picas', 'pixeis', 'pontos',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pi', 'px', 'pt',  
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Ficheiro', '&Editar', 'F&ormatar', 'Tipo de &letra', '&Par�grafo', '&Inserir', '&Tabela', '&Janela', '&Ajuda',
  // exit
  'Sai&r',
  // top-level menus: View, Tools,
  '&Ver', 'Ferramen&tas',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Tamanho', 'Estilo', 'Selec&ionar', 'Ali&nhamento de C�lulas', 'Limites de C�&lulas',
  // menus: Table cell rotation
  '&Orienta��o de C�lula',    
  // menus: Text flow, Footnotes/endnotes
  '&Fluxo de texto', 'Notas de &Rodap�',
  // ribbon tabs: tab1, tab2, view, table
  '&Base', '&Avan�ado', '&Ver', '&Tabela',
  // ribbon groups: clipboard, font, paragraph, list, editing
  '�rea de Transfer�ncia', 'Tipo de Letra', 'Par�grafo', 'Lista', 'Editar',
  // ribbon groups: insert, background, page setup,
  'Inserir', 'Fundo', 'Configurar P�gina',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Liga��es', 'Notas de Rodap�', 'Vistas de Documento', 'Mostrar/Ocultar', 'Zoom', 'Op��es',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Inserir', 'Eliminar', 'Opera��es', 'Limites',
  // ribbon groups: styles 
  'Estilos',
  // ribbon screen tip footer,
  'Prima F1 para obter mais ajuda',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Recentes', 'Guardar documento', 'Pr�-visualizar e imprimir',
  // ribbon label: units combo
  'Unidades:',  
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Novo', 'Novo|Cria um novo documento em branco',
  // TrvActionOpen
  '&Abrir...', 'Abrir|Abre um documento',
  // TrvActionSave
  '&Guardar', 'Guardar|Guarda o documento',
  // TrvActionSaveAs
  'Guardar &Como...', 'Guardar Como...|Guarda o documento com um novo nome',
  // TrvActionExport
  '&Exportar...', 'Exportar|Exporta o documento para outro formato',
  // TrvActionPrintPreview
  'Pr�-vi&sualizar Impress�o', 'Pr�-visualizar Impress�o|Mostrar o documento como ser� impresso',
  // TrvActionPrint
  '&Imprimir...', 'Imprimir|Configurar a impress�o e imprime o documento',
  // TrvActionQuickPrint
  '&Imprimir', '&Impress�o r�pida', 'Imprimir|Imprime o documento',
   // TrvActionPageSetup
  'Configurar P�&gina...', 'Configurar P�gina|Configurar margens, tamanho de papel, orienta��o, fonte, cabe�alho e rodap�',
  // TrvActionCut
  'Cor&tar', 'Cortar|Corta a sele��o para a �rea de Transfer�ncia',
  // TrvActionCopy
  '&Copiar', 'Copiar|Copia a sele��o para a �rea de Transfer�ncia',
  // TrvActionPaste
  'Co&lar', 'Colar|Colar o conte�do da �rea de Transfer�ncia',
  // TrvActionPasteAsText
  'Colar como &Texto', 'Colar como Texto|Colar apenas o texto do conte�do da �rea de Transfer�ncia',
  // TrvActionPasteSpecial
  'Colar E&special...', 'Colar Especial|Colar o conte�do da �rea de Transfer�ncia no formato especificado',
  // TrvActionSelectAll
  'Selecionar &Tudo', 'Selecionar Tudo|Seleciona todos os itens',
  // TrvActionUndo
  '&Anular', 'Anular|Anula a �ltima a��o',
  // TrvActionRedo
  '&Refazer', 'Refazer|Refaz a �ltima a��o anulada',
  // TrvActionFind
  '&Localizar...', 'Localizar|Localiza texto no documento',
  // TrvActionFindNext
  'Localizar &Seguinte', 'Localizar Seguinte|Continua a �ltima localiza��o',
  // TrvActionReplace
  '&Substituir...', 'Substituir|Substitui o texto especificado no documento',
  // TrvActionInsertFile
  '&Ficheiro...', 'Inserir Ficheiro|Inserir o conte�do do ficheiro no documento',
  // TrvActionInsertPicture
  '&Imagem...', 'Inserir Imagem|Inserir uma imagem',
  // TRVActionInsertHLine
  'Linha &Horizontal', 'Inserir Linha Horizontal|Inserir uma linha horizontal',
  // TRVActionInsertHyperlink
  '&Hiperliga��o...', 'Inserir Hiperliga��o|Inserir uma hiperliga��o',
  // TRVActionRemoveHyperlinks
  '&Remover Hiperliga��o', 'Remove Hiperliga��o|Remove todas as hiperliga��es no texto selecionado',
  // TrvActionInsertSymbol
  '&S�mbolo...', 'Inserir S�mbolo|Inserir s�mbolo',
  // TrvActionInsertNumber
  {*} '&Number...', 'Insert Number|Inserts a number',
  // TrvActionInsertCaption
  {*} 'Insert &Caption...', 'Insert Caption|Inserts a caption for the selected object',
  // TrvActionInsertTextBox
  {*} '&Text Box', 'Insert Text Box|Inserts a text box',
  // TrvActionInsertSidenote
  {*} '&Sidenote', 'Insert Sidenote|Inserts a note displayed in a text box',
  // TrvActionInsertPageNumber
  {*} '&Page Number', 'Insert Page Number|Inserts a page number',
  // TrvActionParaList
  '&Marcas e Numera��o...', 'Marcas e Numera��o|Aplica ou edita marcas ou numera��es para os par�grafos selecionados',
  // TrvActionParaBullets
  '&Marcas', 'Marcas|Adicionar ou remove marcas ao par�grafo',
  // TrvActionParaNumbering
  '&Numera��o', 'Numera��o|Adicionar ou remove numera��o ao par�grafo',
  // TrvActionColor
  'Cor da &Pagina...', 'Cor da P�gina|Aplica a cor para fundo do documento',
  // TrvActionFillColor
  '&Sombreado...', 'Sombreado|Aplica a cor de fundo do texto, par�grafo, c�lula, tabela ou documento',
  // TrvActionInsertPageBreak
  '&Inserir Quebra de P�gina', 'Inserir Quebra de P�gina|Inserir uma quebra de p�gina',
  // TrvActionRemovePageBreak
  '&Remover Quebra de P�gina', 'Remover Quebra de P�gina|Remove a quebra de p�gina',
  // TrvActionClearLeft
  'Limpar Fluxo do Texto do Lado &Esquerdo', 'Limpar Fluxo do Texto do Lado Esquerdo|Posiciona este par�grafo abaixo de qualquer figura alinhada pela esquerda',
  // TrvActionClearRight
  'Limpar Fluxo do Texto do Lado &Direito', 'Limpar Fluxo do Texto do Lado Direito|Posiciona este par�grafo abaixo de qualquer figura alinhada pela direita',
  // TrvActionClearBoth
  'Limpar Fluxo do Texto de &Ambos os Lados', 'Limpar Fluxo do Texto de &Ambos os Lados|Posiciona este par�grafo abaixo de qualquer',
  // TrvActionClearNone
  'Fluxo de Texto &Normal', 'Fluxo do Texto Normal|Texto ao redor de figuras alinhadas pela esquerda ou pela direita',
  // TrvActionVAlign
  'Posi��o do &Objeto...', 'Posi��o do Objeto|Muda a posi��o do objeto selecionado',
  // TrvActionItemProperties
  '&Propriedades de Objeto...', 'Propriedades de Objeto|Define propriedades do objeto ativo',
  // TrvActionBackground
  '&Fundo...', 'Fundo|Escolhe a cor e imagem de fundo',
  // TrvActionParagraph
  '&Par�grafo...', 'Par�grafo|Muda os atributos do par�grafo',
  // TrvActionIndentInc
  '&Aumentar Avan�o', 'Aumentar Avan�o|Aumentar avan�o � esquerda dos par�grafos selecionados',
  // TrvActionIndentDec
  '&Diminuir Avan�o', 'Diminuir Avan�o|Diminuir avan�o � esquerda dos par�grafos selecionados',
  // TrvActionWordWrap
  '&Quebra de Linha', 'Quebra de Linha|Liga/Desliga quebra de linha para os par�grafos selecionados',
  // TrvActionAlignLeft
  'Alinhar � &Esquerda', 'Alinhar � Esquerda|Alinha o texto selecionado � esquerda',
  // TrvActionAlignRight
  'Alinhar � &Direita', 'Alinhar � Direita|Alinha o texto selecionado � direita',
  // TrvActionAlignCenter
  'Alinhar ao &Centro', 'Alinhar ao &Centro|Centrar o texto selecionado',
  // TrvActionAlignJustify
  '&Justificar', 'Justificar|Alinha o texto selecionado � direita e � esquerda',
  // TrvActionParaColor
  'Cor de Fu&ndo do Par�grafo...', 'Cor de Fundo do Par�grafo|Configurar cor de fundo de par�grafos',
  // TrvActionLineSpacing100
  'Espa�amento de Linha &Simples', 'Espa�amento de Linha Simples|Configurar espa�amento de linha simples',
  // TrvActionLineSpacing150
  'Espa�amento de 1,5 Lin&has', 'Espa�amento de 1,5 Lin&has|Configurar espa�amento de 1,5 linhas',
  // TrvActionLineSpacing200
  'Espa�amento de Linha &Duplo', 'Espa�amento de Linha Duplo|Configurar espa�amento de linha duplo',
  // TrvActionParaBorder
  '&Limites e Sombreado...', 'Limites e Sombreado|Configurar limites e cor de fundo para os par�grafos selecionados',
  // TrvActionInsertTable
  '&Inserir Tabela...', '&Tabela', 'Inserir Tabela|Inserir uma nova tabela',
  // TrvActionTableInsertRowsAbove
  'Inserir Linha &Acima', 'Inserir Linha Acima|Inserir nova linha acima das c�lulas selecionadas',
  // TrvActionTableInsertRowsBelow
  'Inserir Linha A&baixo', 'Inserir Linha Abaixo|Inserir nova linha abaixo das c�lulas selecionadas',
  // TrvActionTableInsertColLeft
  'Inserir Coluna � &Esquerda', 'Inserir Coluna � Esquerda|Inserir nova coluna � esquerda das c�lulas selecionadas',
  // TrvActionTableInsertColRight
  'Inserir Coluna � &Direita', 'Inserir Coluna � Direita|Inserir nova coluna � direita das c�lulas selecionadas',
  // TrvActionTableDeleteRows
  'Eliminar L&inhas', 'Eliminar Linhas|Elimina linhas contendo as c�lulas selecionadas',
  // TrvActionTableDeleteCols
  'Eliminar &Colunas', 'Eliminar Colunas|Elimina colunas contendo as c�lulas selecionadas',
  // TrvActionTableDeleteTable
  '&Eliminar Tabela', 'Eliminar Tabela|Elimina a tabela',
  // TrvActionTableMergeCells
  '&Unir C�lulas', 'Unir C�lulas|Intercala as c�lulas selecionadas',
  // TrvActionTableSplitCells
  '&Dividir C�lulas...', 'Dividir C�lulas|Divide as c�lulas selecionadas',
  // TrvActionTableSelectTable
  'Selecionar &Tabela', 'Selecionar Tabela|Seleciona a tabela',
  // TrvActionTableSelectRows
  'Selecionar Lin&has', 'Selecionar Linhas|Seleciona linhas',
  // TrvActionTableSelectCols
  'Selecionar Col&unas', 'Selecionar Colunas|Seleciona colunas',
  // TrvActionTableSelectCell
  'Selecionar C�&lula', 'Selecionar C�lula|Selecionar c�lula',
  // TrvActionTableCellVAlignTop
  'Alinhar C�lula em &Cima', 'Alinhar C�lula em Cima|Alinha conte�do na parte superior da c�lula',
  // TrvActionTableCellVAlignMiddle
  'Alinhar C�lula ao C&entro', 'Alinhar C�lula no Centro|Alinha conte�do na parte central da c�lula',
  // TrvActionTableCellVAlignBottom
  'Alinhar C�lula em &Baixo', 'Alinhar C�lula em Baixo|Alinha conte�do na parte inferior da c�lula',
  // TrvActionTableCellVAlignDefault
  'Alinhamento Vertical de C�lula &Normal', 'Alinhamento Vertical de C�lula Normal|Configurar o alinhamento vertical normal para as c�lulas selecionadas',
  // TrvActionTableCellRotationNone
  '&Sem Orienta��o de C�lula', 'Sem Orienta��o de C�lula|Orienta o conte�do da c�lula para 0�',
  // TrvActionTableCellRotation90
  'Orienta��o de C�lula a &90�', 'Orienta��o C�lula para 90�|Orienta o conte�do da c�lula para 90�',
  // TrvActionTableCellRotation180
  'Orienta��o de C�lula a &180�', 'Orienta��o C�lula para 180�|Orienta o conte�do da c�lula para 180�',
  // TrvActionTableCellRotation270
  'Orienta��o de C�lula a &270�', 'Orienta��o C�lula para 270�|Orienta o conte�do da c�lula para 270�',
  // TrvActionTableProperties
  '&Propriedades da Tabela...', 'Propriedades da Tabela|Alterar as propriedades da tabela selecionada',
  // TrvActionTableGrid
  'Mostrar Linhas de &Grelha', 'Mostrar Linhas de Grelha|Mostrar ou ocultar as linhas de grelha na tabela',
  // TRVActionTableSplit
  'Di&vidir Tabela', 'Dividir Tabela|Divide a tabela em duas tabelas come�ando na linha selecionada',
  // TRVActionTableToText
  'Converter em Te&xto...', 'Converter em Texto|Converte a tabela em texto normal',
  // TRVActionTableSort
  '&Ordenar...', 'Ordenar|Ordena as linhas da tabela',
  // TrvActionTableCellLeftBorder
  'Limite &Esquerdo', 'Limite Esquerda|Mostrar ou Ocultar o limite esquerdo da c�lula',
  // TrvActionTableCellRightBorder
  'Limite &Direita', 'Limite Direita|Mostrar ou Ocultar o limite direito da c�lula',
  // TrvActionTableCellTopBorder
  'Limite &Superior', 'Limite Superior|Mostrar ou Ocultar o limite superior da c�lula',
  // TrvActionTableCellBottomBorder
  'Limite &Inferior', 'Limite Inferior|Mostrar ou Ocultar o limite inferior da c�lula',
  // TrvActionTableCellAllBorders
  '&Todos os Limites', 'Todos os Limites|Mostrar ou Ocultar todos os limites da c�lula',
  // TrvActionTableCellNoBorders
  '&Sem Limites', 'Sem Limites|Ocultar todos os limites da c�lulas',
  // TrvActionFonts & TrvActionFontEx
  'Tipo de &Letra...', 'Tipo de Letra|Alterar o tipo de letra do texto selecionado',
  // TrvActionFontBold
  '&Negrito', 'Negrito|Alterar o estilo do texto selecionado para negrito',
  // TrvActionFontItalic
  '&It�lico', 'It�lico|Alterar o estilo do texto selecionado para it�lico',
  // TrvActionFontUnderline
  '&Sublinhado', 'Sublinhado|Alterar o estilo do texto selecionado para sublinhado',
  // TrvActionFontStrikeout
  '&Rasurado', 'Rasurado|Desenha um linha no meio do texto selecionado',
  // TrvActionFontGrow
  '&Aumentar Tipo de Letra', 'Aumentar Tipo de Letra|Aumentar o tamanho do tipo de letra selecionado em 10%',
  // TrvActionFontShrink
  'D&iminuir Tipo de Letra', 'Diminuir Tipo de Letra|Diminuir o tamanho do tipo de letra selecionado em 10%',
  // TrvActionFontGrowOnePoint
  'A&umentar Tipo de Letra em Um Ponto', 'Aumentar Tipo de Letra em Um Ponto|Aumentar o tamanho do tipo de letra selecionado em 1 ponto',
  // TrvActionFontShrinkOnePoint
  'D&iminuir Tipo de Letra em Um Ponto', 'Diminuir Tipo de Letra em Um Ponto|Diminuir o tamanho do tipo de letra selecionado em 1 ponto',
  // TrvActionFontAllCaps
  '&Tudo Mai�sculas', 'Tudo Mai�sculas|Alterar todos os caracteres do texto selecionado para mai�sculas',
  // TrvActionFontOverline
  '&SobreLinha', 'SobreLinha|Adicionar linha sobre o texto selecionado',
  // TrvActionFontColor
  '&Cor do Tipo de Letra...', 'Cor do Tipo de Letra|Alterar a cor do texto selecionado',
  // TrvActionFontBackColor
  'Cor de &Realce do Texto...', 'Cor de Realce do Texto|Alterar a cor de fundo do texto selecionado',
  // TrvActionAddictSpell3
  'Verificar &Ortografia', 'Verificar Ortografia|Verifica a ortografia',
  // TrvActionAddictThesaurus3
  '&Sin�nimos', 'Sin�nimos|Lista de sin�nimos para a palavra selecionada',
  // TrvActionParaLTR
  'Esquerda para Direita', 'Esquerda para Direita|Configurar dire��o do texto da esquerda para direita para par�grafos selecionados',
  // TrvActionParaRTL
  'Direita para Esquerda', 'Direita para Esquerda|Configurar dire��o do texto da direita para esquerda para par�grafos selecionados',
  // TrvActionLTR
  'Texto Esquerda para Direita', 'Texto Esquerda para Direita|Configurar dire��o do texto da esquerda para direita para texto selecionado',
  // TrvActionRTL
  'Texto Direita para Esquerda', 'Texto Direita para Esquerda|Configurar dire��o do texto da direita para esquerda para texto selecionado',
  // TrvActionCharCase
  'Mai�sculas/Min�sculas', 'Mai�sculas/Min�sculas|Alterar para mai�sculas ou min�sculas do texto selecionado',
  // TrvActionShowSpecialCharacters
  'Mostrar &Tudo', 'Mostrar Tudo|Mostrar ou ocultar s�mbolos ocultos',
  // TrvActionSubscript
  '&Inferior � linha', 'Inferior � linha|Cria letras pequenas por baixo da linha de texto',
  // TrvActionSuperscript
  '&Superior � linha', 'Superior � linha|Cria letras pequenas por cima da linha de texto',
  'Nota de &Rodap�', 'Nota de Rodap�|Inserir uma nota de rodap�',
  // TrvActionInsertEndnote
  'Nota de &Fim', 'Nota de Fim|Inserir uma nota de fim',
  // TrvActionEditNote
  'E&ditar Nota', 'Editar Nota|Editar uma nota de rodap� ou de fim',
  '&Ocultar', 'Ocultar|Ocultar ou Mostrar o texto selecionado',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Estilos...', 'Estilos|Abrir a janela de estilos',
  // TrvActionAddStyleTemplate
  '&Adicionar Estilo...', 'Adicionar Estilo|Criar um novo estilo de texto ou par�grafo',
  // TrvActionClearFormat,
  '&Limpar formata��o', 'Limpar Formata��o|Limpar todas as formata��es no texto ou par�grafo selecionado',
  // TrvActionClearTextFormat,
  'Limpar &Formato de texto', 'Limpar Formato de Texto|Limpar todas as formata��es no texto selecionado',
  // TrvActionStyleInspector
  '&Inspetor de Estilos', 'Inspetor de Estilos|Abre ou fecha o inspetor de estilos',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Cancelar', 'Fechar', 'Inserir', '&Abrir...', '&Guardar...', '&Limpar', 'Ajuda', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'percentagem',
  // left, top, right, bottom sides
  'Esquerdo', 'Superior', 'Direito', 'Inferior',
  // save changes? confirm title
  'Deseja guardar as altera��es no documento %s?', 'Confirma��o',
  // warning: losing formatting
  '%s pode conter recursos n�o compat�veis com o tipo de ficheiro escolhido.'#13+
  'Deseja guardar o documento neste tipo de ficheiro?',
  // RVF format name
  'Tipo RichView',
  // Error messages ------------------------------------------------------------
  'Erro',
  'Erro ao abrir o ficheiro.'#13#13'Causas poss�veis:'#13'- tipo de ficheiro n�o compat�vel;'#13+
  '- o ficheiro est� corrompido;'#13'- o ficheiro est� aberto noutra aplica��o.',
  'Erro ao abrir a imagem.'#13#13'Causas poss�veis:'#13'- tipo de ficheiro n�o compat�vel;'#13+
  '- o ficheiro n�o � uma imagem;'#13+
  '- o ficheiro est� corrompido;'#13'- o ficheiro est� aberto noutra aplica��o.',
  'Erro ao guardar o ficheiro.'#13#13'Causas poss�veis:'#13'- sem espa�o em disco;'#13+
  '- disco est� protegido contra escrita;'#13'- disco amov�vel n�o inserido;'#13+
  '- o ficheiro esta aberto noutra aplica��o;'#13'- disco amov�vel est� corrompido.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Ficheiros RichView (*.rvf)|*.rvf',  'Ficheiros RTF (*.rtf)|*.rtf' , 'Ficheiros XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Ficheiros Texto (*.txt)|*.txt', 'Ficheiros Texto - Unicode (*.txt)|*.txt', 'Ficheiros Texto - Dete��o autom�tica (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Ficheiros HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'Ficheiros HTML - Simplificado (*.htm;*.html)|*.htm;*.html',
  // DocX
  {*} 'Microsoft Word Documents (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Pesquisa Completa', 'O texto ''%s'' n�o foi encontrado.',
  // 1 string replaced; Several strings replaced
  '1 item substitu�do.', '%d itens substitu�dos.',
  // continue search from the beginning/end?
  'O final do documento foi atingido, continuar do in�cio?',
  'O in�cio do documento foi atingido, continuar do final?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Transparente', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Preto', 'Castanho', 'Verde Azeitona', 'Verde Escuro', 'Azul Petr�leo Escuro', 'Azul escuro', 'Indigo', 'Cinza-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Vermelho Escuro', 'Laranja', 'Amarelo Escuro', 'Verde', 'Azul Petr�leo', 'Azul', 'Azul-Cinza', 'Cinza-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Vermelho', 'Laranja Claro', 'Verde Lima', 'Verde Mar', 'Azul �gua', 'Azul Claro', 'Violeta', 'Cinza-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Rosa Choque', 'Dourado', 'Amarelo', 'Verde Brilhante', 'Turquesa', 'Azul C�u', 'Ameixa', 'Cinza-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rosa', 'Castanho Claro', 'Amarelo Claro', 'Verde Claro', 'Turquesa Claro', 'Azul P�lido', 'Lil�s', 'Branco',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Tr&ansparente', '&Auto', '&Mais Cores...', '&Cores Padr�o',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Fundo', 'C&or:', 'Posi��o', 'Fundo', 'Texto de exemplo.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Nenhum', '&Expandido', 'Lado a Lado F&ixo', '&Lado a Lado', 'C&entrado',
  // Padding button
  '&Ajustar...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'Cor Preenchimento', '&Aplicar em:', '&Mais Cores...', '&Ajustar...',
  'Favor selecionar uma cor',
  // [apply to:] text, paragraph, table, cell
  'texto', 'par�grafo' , 'tabela', 'c�lula',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Tipo de Letra', 'Tipo de Letra', 'Apresenta��o',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Tipo de Letra:', '&Tamanho', 'Estilo do Tipo de Letra', '&Negrito', '&It�lico',
  // Script, Color, Back color labels, Default charset
  'Sc&ript:', '&Cor do Tipo de Letra:', 'Cor do &realce:', '(Padr�o)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efeitos', 'Sublinhado', '&Linha superior', 'R&asurado', '&Mai�sculas',
  // Sample, Sample text
  'Pr�-visualizar', 'Texto de exemplo',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Espa�amento entre Caracteres', '&Por:', '&Expandido', '&Comprimido',
  // Offset group-box, Offset label, Down, Up,
  'Posicionamento', '&Por:', '&Rebaixado', '&Elevado',
  // Scaling group-box, Scaling
  'Escala', '&De:',
  // Sub/super script group box, Normal, Sub, Super
  'Efeitos', '&Normal', '&Inferior � linha', '&Superior � linha',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Ajuste', '&Superior:', '&Esquerda:', '&Inferior:', '&Direita:', 'Valores &iguais',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Inserir Hiperliga��o', 'Hiperliga��o', 'T&exto:', '&Destino', '<<sele��o>>',
  // cannot open URL
  'Imposs�vel abrir o endere�o "%s"',
  // hyperlink properties button, hyperlink style
  '&Personalizar...', '&Estilo:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Atributos de Hiperlink', 'Cores normal', 'Cores ativo',
  // Text [color], Background [color]
  '&Texto:', '&Fundo',
  // Underline [color]
  'S&ublinhado:',   
  // Text [color], Background [color] (different hotkeys)
  'T&exto:', 'F&undo',
  // Underline [color], Attributes group-box,
  'S&ublinhado:', 'Atributos',
  // Underline type: always, never, active (below the mouse)
  'S&ublinhado:', 'sempre', 'nunca', 'ativo',
  // Same as normal check-box
  'Como &normal',
  // underline active links check-box
  '&Sublinhar os links ativos',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Inserir S�mbolo', '&Tipo de Letra:', 'Subconjunto:', 'Unicode',
  // Unicode block
  '&Bloco:',
  // Character Code, Character Unicode Code, No Character
  'C�d. Caractere: %d', 'C�d. Caractere: Unicode %d', '(sem caractere)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  'Inserir Tabela', 'Tamanho Tabela', 'N�mero de &colunas:', 'N�mero de &linhas:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Comportamento do ajuste autom�tico', 'Ajuste &Autom�tico', 'Ajuste autom�tico � &janela', 'Largura &manual',
  // Remember check-box
  'Lembrar &dimens�es de novas tabelas',
  // VAlign Form ---------------------------------------------------------------
  'Posi��o',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Propriedades', 'Imagem', {*}'Position and Size', 'Linha', 'Tabela', 'Linhas', 'C�lulas',
  // Image Appearance, Image Misc, Number tabs
  {*} 'Appearance', 'Miscellaneous', 'Number',
  // Box position, Box size, Box appearance tabs
  {*} 'Position', 'Size', 'Appearance',
  // Preview label, Transparency group-box, checkbox, label
  'Pr�-visualiza��o:', 'Transpar�ncia', '&Transparente', '&Cor transparente:',
  // change button, save button, no transparent color (use color of right-bottom pixel)
  'Al&terar...', {*}'&Save...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Alinhamento vertical', '&Alinhamento:',
  'inferior � linha de texto', 'meio da linha de texto',
  'superior � linha de texto', 'em linha com o texto', 'meio com o centro da linha de texto',
  // align to left side, align to right side
  'lado esquerdo', 'lado direito',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Desloca��o de:', 'Ajustar', '&Largura:', '&Altura:', 'Tamanho padr�o: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  {*} 'Scale &proportionally', '&Reset',
  // Inside the border, border, outside the border groupboxes
  {*} 'Inside the border', 'Border', 'Outside the border',
  // Fill color, Padding (i.e. margins inside border) labels
  {*} '&Fill color:', '&Padding:',
  // Border width, Border color labels
  {*} 'Border &width:', 'Border &color:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  {*} '&Horizontal spacing:', '&Vertical spacing:',
  // Miscellaneous groupbox, Tooltip label
  {*} 'Miscellaneous', '&Tooltip:',
  // web group-box, alt text
  'Web', '&Texto alternativo:',
  // Horz line group-box, color, width, style
  'Linha horizontal', '&Cor:', '&Largura:', '&Estilo:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Tabela', '&Largura:', 'Cor &preenchimento:', 'Espa�amento entre &c�lulas...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  {*} '&Horizontal cell padding:', '&Vertical cell padding:', 'Border and background',
  // Visible table border sides button, auto width (in combobox), auto width label
  {*} 'Visible &Border Sides...', 'Auto', 'auto',
  // Keep row content together checkbox
  {*} '&Keep content together',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  {*} 'Rotation', '&None', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  {*} 'Border:', 'Visible B&order Sides...',
  // Border, CellBorders buttons
  'Limite de &Tabela...', 'Limites das &C�lulas...',
  // Table border, Cell borders dialog titles
  'Limite da Tabela', 'Limites das C�lula',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Impress�o', '&Manter tabela numa p�gina', 'N�mero de linhas de &cabe�alho:',
  'Linhas de cabe�alho s�o repetidas no in�co de cada p�gina',
  // top, center, bottom, default
  'Em &Cima', '&Centrado', 'Em &Baixo', '&Normal',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Configura��es', '&Largura Preferida:', '&Altura m�nima:', 'Cor &Preenchimento:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Limites', 'Limites vis�veis:',  'C&or sombra:', 'Cor &luz', 'C&or:',
  // Background image
  '&Imagem...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  {*} 'Horizontal position', 'Vertical position', 'Position in text',
  // Position types: align, absolute, relative
  {*} 'Align', 'Absolute position', 'Relative position',
  // [Align] left side, center, right side
  {*} 'left side of box to left side of',
  {*} 'center of box to center of',
  {*} 'right side of box to right side of',
  // [Align] top side, center, bottom side
  {*} 'top side of box to top side of',
  {*} 'center of box to center of',
  {*} 'bottom side of box to bottom side of',
  // [Align] relative to
  {*} 'relative to',
  // [Position] to the right of the left side of
  {*} 'to the right of the left side of',
  // [Position] below the top side of
  {*} 'below the top side of',
  // Anchors: page, main text area (x2)
  {*} '&Page', '&Main text area', 'P&age', 'Main te&xt area',
  // Anchors: left margin, right margin
  {*} '&Left margin', '&Right margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  {*} '&Top margin', '&Bottom margin', '&Inner margin', '&Outer margin',
  // Anchors: character, line, paragraph
  {*} '&Character', 'L&ine', 'Para&graph',
  // Mirrored margins checkbox, layout in table cell checkbox
  {*} 'Mirror&ed margins', 'La&yout in table cell',
  // Above text, below text
  {*} '&Above text', '&Below text',
  // Width, Height groupboxes, Width, Height labels
  {*} 'Width', 'Height', '&Width:', '&Height:',
  // Auto height label, Auto height combobox item
  {*} 'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  {*} 'Vertical alignment', '&Top', '&Center', '&Bottom',
  // Border and background button and title
  {*} 'B&order and background...', 'Box Border and Background',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Colar Especial', '&Colar como:', 'Texto formatado (RTF)', 'Formato HTML',
  'Texto n�o formatado', 'Texto unicode n�o formatado', 'Imagem bitmap', 'Imagem metafile',
  'Ficheiros gr�ficos', 'Endere�o',
  // Options group-box, styles
  'Op��es', '&Estilos:',
  // style options: apply target styles, use source styles, ignore styles
  'aplicar estilos ao documento', 'manter estilos e apar�ncia',
  'manter a apar�ncia, ignorar os estilos',  
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Marcas e Numera��o', 'Marcas', 'Numera��o', 'Listas com Marcas', 'Listas numeradas',
  // Customize, Reset, None
  '&Personalizar...', '&Predefinir', 'Nenhum',
  // Numbering: continue, reset to, create new
  'continuar numera��o', 'reiniciar numera��o em', 'cria uma nova lista',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Letras Mai�sculas', 'N�meros Romanos Mai�sculos', 'Decimal', 'Letras Min�sculas', 'N�meros Romanos Min�sculos',
  // Bullet type
  'C�rculo', 'Disco', 'Quadrado',
  // Level, Start from, Continue numbering, Numbering group-box
  '&N�vel:', '&Iniciar em:', '&Continuar', 'Numera��o',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Personaliza Lista', 'N�veis', '&Quant.:', 'Propriedades da Lista', 'Tipo de &lista:',
  // List types: bullet, image
  'marcas', 'imagem', 'Inserir N�mero|',
  // Number format, Number, Start level from, Font button
  'Formato dos &N�meros:', 'N�meros', 'N�vel &inicial a partir de :', '&Tipo de Letra...',
  // Image button, bullet character, Bullet button,
  '&Imagem...', 'C&aractere da Marca', '&Marca...',
  // Position of list text, bullet, number, image, text
  'Posi��o do texto de lista', 'Posi��o da marca', 'Posi��o da numera��o', 'Posi��o da imagem', 'Posi��o do texto',
  // at, left indent, first line indent, from left indent
  '&em:', 'Avan�o &esquerda:', 'Avan�o &primeira linha:', 'do avan�o esquerdo',
  // [only] one level preview, preview
  'Pr�-visualizar &um n�vel', 'Pr�-visualizar',
  // Preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'alinha � esquerda', 'alinha � direita', 'centrado',
  // level #, this level (for combo-box)
  'N�vel %d', 'Este n�vel',
  // Marker character dialog title
  'Alterar Marca',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Limites e Sombreado do Par�grafo', 'Limites', 'Sombreado',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Configura��es', '&Cor:', '&Largura:', 'Largura int&erna:', 'D&eslocamento...',
  // Sample, Border type group-boxes;
  'Exemplo', 'Tipo de limite',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Nenhum', '&Simples', '&Duplo', '&Triplo', 'Espesso &interno', 'Espesso &externo',
  // Fill color group-box; More colors, padding buttons
  'Cor de preenchimento', '&Mais cores...', '&Margem...',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // title and group-box in Offsets dialog
  'Deslocamento', 'Deslocado do limite',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Par�grafo', 'Alinhamento', '&Esquerda', '&Direita', '&Centrado', '&Justificado',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Espa�amento', '&Antes:', '&Depois:', '&Entre Linhas:', 'E&m:',
  // Indents group-box; indents: left, right, first line
  'Avan�o', '&Esquerda:', '&Direita:', '&Primeira linha:',
  // indented, hanging
  '&Avan�ado', '&Pendente', 'Pr�-visualizar',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Simples', '1,5 linhas', 'Duplo', 'Pelo menos', 'Exatamente', 'M�ltiplo',
  // preview text
  'Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto. Texto texto texto texto texto.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Avan�os e Espa�amento', 'Tabula��es', 'Fluxo de texto',
  // tab stop position label; buttons: set, delete, delete all
  'Posi��o de &tabula��o:', '&Definir', '&Limpar', 'Limpar &Tudo',
  // tab align group; left, right, center aligns,
  'Alinhamento', '�& esquerda', '� &direita', 'Ao &centro',
  // leader radio-group; no leader
  'Car�cter de Preenchimento', '(Nenhum)',
  // tab stops to be deleted, delete none, delete all labels
  'Tabula��es a desmarcar:', '(Nenhuma)', 'Todas.',
  // Pagination group-box, keep with next, keep lines together
  'Pagina��o', '&Manter com o seguinte', 'Manter &linhas juntas',
  // Outline level, Body text, Level #
  '&N�vel do contorno:', 'Corpo de Texto', 'N�vel %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Pr�-visualizar Impress�o', 'Largura da P�gina', 'P�gina Inteira', 'P�ginas:',
  // Invalid Scale, [page #] of #
  'Por favor introduza um n�mero entre 10 e 500', 'de %d',
  // Hints on buttons: first, prior, next, last pages
  'Primeira P�gina', 'P�gina Anterior', 'P�gina Seguinte', '�ltima P�gina',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Espa�amento de C�lulas', 'Espa�amento', 'E&ntre c�lulas', 'Do limite da tabela at� as c�lulas',
  // vertical, horizontal (x2)
  '&Vertical:', '&Horizontal:', 'V&ertical:', 'H&orizontal:',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Limites', 'Configura��es', '&Cor:', 'Cor &Luz:', '&Cor sombra:',
  // Width; Border type group-box;
  '&Largura:', 'Tipo de limite',
  // Border types: none, sunken, raised, flat
  '&Nenhum', '&Baixo relevo', '&Alto relevo', '&Liso',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Dividir c�lulas', 'Dividir para',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'N�mero e&specificado', 'C�lulas &originais',
  // number of columns, rows, merge before
  'N�mero de &colunas:', 'N�mero de &linhas:', '&Unir c�lulas antes de as dividir',
  // to original cols, rows check-boxes
  'Dividir para co&lunas originais', 'Dividir para li&nhas originais',
  // Add Rows form -------------------------------------------------------------
  'Adicionar Linhas', 'N�mero de &Linhas:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Configurar de P�gina', 'P�gina', 'Cabe�alho e Rodap�',
  // margins group-box, left, right, top, bottom
  'Margens (mil�metros)', 'Margens (polegadas)', '&Esquerda:', '&Superior:', '&Direita:', '&Inferior:',
  // mirror margins check-box
  'Espelhar &margens',
  // orientation group-box, portrait, landscape
  'Orienta��o', '&Vertical', '&Horizontal',
  // paper group-box, paper size, default paper source
  'Papel', 'T&amanho:', '&Origem:',
  // header group-box, print on the first page, font button
  'Cabe�alho', '&Texto:', 'C&abe�alho na primeira p�gina', '&Fonte...',
  // header alignments: left, center, right
  '&Esquerda', '&Centrado', '&Direita',
  // the same for footer (different hotkeys)
  'Rodap�', 'Te&xto:', 'Rodap� na primeira &p�gina', 'F&onte...',
  'E&squerda', 'Ce&ntraliza', 'D&ireita',
  // page numbers group-box, start from
  'N�meros de p�gina', 'Inicia e&m:',
  // hint about codes
  'Combina��es de caracteres especiais:'#13'&&p - n�mero de p�gina; &&P - total de p�ginas; &&d - data atual; &&t - hora atual.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'C�digo de P�gina do Ficheiro de Texto', '&Escolha a codifica��o do ficheiro:',
  // thai, japanese, chinese (simpl), korean
  'Tailand�s', 'Japon�s', 'Chin�s (Simplificado)', 'Coreano',
  // chinese (trad), central european, cyrillic, west european
  'Chin�s (Tradicional)', 'Europeu Central e Oriental', 'Cir�lico', 'Europeu Ocidental',
  // greek, turkish, hebrew, arabic
  'Greco', 'Turco', 'Hebreu', 'Ar�be',
  // baltic, vietnamese, utf-8, utf-16
  'B�ltico', 'Vietnam�s', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Estilo Visual', '&Selecione o estilo:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Converter em Texto', 'Selecione o &delimitador:',
  // line break, tab, ';', ','
  'quebra de linha', 'tabula��o', 'ponto e v�rgula', 'v�rgula',
  // Table sort form -----------------------------------------------------------
  // error message
  'N�o � poss�vel ordenar uma tabela com linhas unidas',
  // title, main options groupbox
  'Ordenar Tabela', 'Ordenar',
  // sort by column, case sensitive
  '&Ordenar por coluna:', 'Diferenciar &mai�sculas de min�sculas',
  // heading row, range or rows
  'Eliminar linha de &cabe�alho', 'Linhas a serem ordenadas: de %d at� %d',
  // order, ascending, descending
  'Ordem', '&Ascendente', '&Descendente',
  // data type, text, number
  'Tipo', '&Texto', '&N�mero',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  {*} 'Insert Number', 'Properties', '&Counter name:', '&Numbering type:',
  // numbering groupbox, continue, start from
  {*} 'Numbering', 'C&ontinue', '&Start from:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  {*} 'Caption', '&Label:', '&Exclude label from caption',
  // position radiogroup
  {*} 'Position', '&Above selected object', '&Below selected object',
  // caption text
  {*} 'Caption &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  {*} 'Numbering', 'Figure', 'Table',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  {*} 'Visible Border Sides', 'Border',
  // Left, Top, Right, Bottom checkboxes
  {*} '&Left side', '&Top side', '&Right side', '&Bottom side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Mover a Coluna da Tabela', 'Mover a Linha da Tabela',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Avan�o da Primeira Linha', 'Avan�o Esquerdo', 'Avan�o', 'Avan�o Direito',
  // Hints on lists: up one level (left), down one level (right)
  'Um n�vel acima', 'Um n�vel abaixo',
  // Hints for margins: bottom, left, right and top
  'Margem inferior', 'Margem esquerda', 'Margem direita', 'Margem superior',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tabula��o � Esquerda', 'Tabula��o � Direita', 'Tabula��o Centrada', 'Tabula��o Alinhada ao Ponto Decimal',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Avan�o Normal', 'Sem Espa�amento', 'Cabe�alho %d', 'Par�grafo da lista',
  // Hyperlink, Title, Subtitle
  'Hiperliga��o', 'T�tulo', 'Subt�tulo',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  '�nfase', '�nfase Discreto', '�nfase Intenso', 'Forte',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Cita��o', 'Cita��o Intensa', 'Refer�ncia Discreta', 'Refer�ncia Intensa',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Texto de Bloco', 'Vari�vel HTML', 'C�digo HTML', 'Acr�nimo HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Defini��o HTML', 'Teclado HTML', 'Exemplo de HTML', 'M�quina de Escrever HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Pr�-formatado', 'Cita��o HTML', 'Cabe�alho', 'Rodap�', 'Numero de P�gina',
  // Caption
  {*} 'Caption',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Ref. de Nota de Fim', 'Ref. de Nota de Rodap�', 'Texto de Nota de Fim', 'Texto de Nota de Rodap�',
  // Sidenote Reference, Sidenote Text
  {*} 'Sidenote Reference', 'Sidenote Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'cor', 'cor de fundo', 'transparente', 'por defeito', 'cor do sublinhado',
  // default background color, default text color, [color] same as text
  'cor de fundo por defeito', 'cor de texto por defeito', 'como texto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'simples', 'grosso', 'duplo', 'pontilhado', 'pontilhado grosso', 'tracejado',
  // underline types: thick dashed, long dashed, thick long dashed,
  'tracejado grosso', 'tracejado longo', 'tracejado longo grosso',
  // underline types: dash dotted, thick dash dotted,
  'tra�o pontilhado', 'tra�o pontilhado grosso ',
  // underline types: dash dot dotted, thick dash dot dotted
  'ponto-tra�o-traco', 'ponto-tra�o-traco grosso',
  // sub/superscript: not, subsript, superscript
  'sem inferior/superior � linha', 'inferior � linha', 'superior � linha',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di mode:', 'inherited', 'esquerda para a direita', 'direita para a esquerda',
  // bold, not bold
  'negrito', 'sem negrito',
  // italic, not italic
  'it�lico', 'sem it�lico',
  // underlined, not underlined, default underline
  'sublinhado', 'sem sublinhado', 'sublinhado por defeito',
  // struck out, not struck out
  'rasurado', 'sem rasurado',
  // overlined, not overlined
  'linha superior', 'sem linha superior',
  // all capitals: yes, all capitals: no
  'Em mai�sculas', 'Sem mai�sculas',
  // vertical shift: none, by x% up, by x% down
  'Sem deslocamento vertical', 'Elevado por %d%%', 'Rebaixado por %d%%',  
  // characters width [: x%]
  'largura dos caracteres',
  // character spacing: none, expanded by x, condensed by x
  'espa�amento caracteres normal', 'espa�amento caracteres expandido por %s', 'espa�amento caracteres comprimido por %s',
  // charset
  'script',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'tipo de letra por defeito', 'paragrafo por defeito', 'atributos base',
  // [hyperlink] highlight, default hyperlink attributes
  'realce', 'por defeito',
  // paragraph aligmnment: left, right, center, justify
  'alinhar � esquerda', 'alinhar � direita', 'ao centro', 'justificar',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'altura da linha: %d%%', 'espa�amento entre linhas: %s', 'altura da linha: pelo menos %s',
  'altura da linha: exatamente %s',
  // no wrap, wrap
  'sem quebra de linhas', 'com quebra de linhas',
  // keep lines together: yes, no
  'manter linhas juntas', 'n�o manter as linhas juntas',
  // keep with next: yes, no
  'manter com pr�ximo par�grafo', 'n�o manter com pr�ximo par�grafo',
  // read only: yes, no
  's� de leitura', 'edit�vel',
  // body text, heading level x
  'n�vel de contorno: corpo de texto', 'n�vel: %d',
  // indents: first line, left, right
  'avan�o de primeira linha', 'avan�o � esquerda', 'avan�o � direita',
  // space before, after
  'espa�amento antes', 'espa�amento depois',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabula��es', 'nenhum', 'alinhamento', 'esquerda', 'direita', 'centro',
  // tab leader (filling character)
  'leader',
  // no, yes
  'n�o', 'sim',
  // [padding/spacing/side:] left, top, right, bottom
  'esquerda', 'cima', 'direita', 'baixo',
  // border: none, single, double, triple, thick inside, thick outside
  'nenhum', 'simples', 'duplo', 'triplo', 'grosso por dentro', 'grosso por fora',
  // background, border, padding, spacing [between text and border]
  'fundo', 'limite', 'espa�amento', 'espa�amento entre o texto e o limite',
  // border: style, width, internal width, visible sides
  'estilo', 'largura', 'largura interna', 'lados visiveis',
  // style inspector -----------------------------------------------------------
  // title
  'Inspetor de Estilos',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Estilo', '(Nenhum)', 'Par�grafo', 'Tipo de letra', 'propriedades',
  // border and background, hyperlink, standard [style]
  'Limites e sombreados', 'Hiperliga��o', '(Padr�o)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Estilos', 'Estilo',
  // name, applicable to,
  '&Nome:', 'Aplicar &a:',
  // based on, based on (no &),
  '&Baseado em:', 'Baseado em:',
  //  next style, next style (no &)
  'Estilo para o &par�grafo seguinte:', 'Estilo para o par�grafo seguinte:',
  // quick access check-box, description and preview
  '&Adicionar aos estilos r�pidos', 'Descri��o e &pr�-visualiza��o:',
  // [applicable to:] paragraph and text, paragraph, text
  'par�grafo e texto', 'par�grafo', 'texto',
  // text and paragraph styles, default font
  'Estilo de Texto e Paragrafo', 'Tipo de letra por defeito',
  // links: edit, reset, buttons: add, delete
  'Editar', 'Reiniciar', '&Adicionar', '&Eliminar',
  // add standard style, add custom style, default style name
  'Adicionar um estilo &modelo...', 'Adicionar um estilo &personalizado', 'Estilo %d',
  // choose style
  'Escolher &estilo:',
  // import, export,
  '&Importar...', '&Exportar...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Styles (*.rvst)|*.rvst', 'Erro ao carregar o ficheiro', 'Este ficheiro n�o cont�m estilos',
  // Title, group-box, import styles
  'Importar Estilos de Ficheiro', 'Importar', 'Importar estilos:',
  // existing styles radio-group: Title, override, auto-rename
  'Estilos existentes' , '&substituir', '&auto renomear',
  // Select, Unselect, Invert,
  '&Selecionar', '&Desselecionar', '&Inverter',
  // select/unselect: all styles, new styles, existing styles
  '&Todos os estilos', '&Novos estilos', '&Estilos Existentes',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Limpar tudo', 'Todos os Estilos',
  // dialog title, prompt
  'Estilos', '&Escolher o estilo a aplicar:',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sin�nimos', '&Ignorar Todos', '&Adicionar ao Dicion�rio',
  // Progress messages ---------------------------------------------------------
  'A descarregar %s', 'A preparar a impress�o...', 'A imprimir a p�gina %d',
  'A converter de RTF...',  'A converter para RTF...',
  'A ler %s...', 'A escrever %s...', 'texto',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  {*} 'No Note', 'Footnote', 'Endnote', 'Sidenote', 'Text Box'  
  );


initialization
  RVA_RegisterLanguage(
    'Portuguese (Portuguese)', // english language name, do not translate
    {*} 'Portuguese (Portuguese)', // native language name
    ANSI_CHARSET, @Messages);

end.