﻿// This file is a copy of RVAL_Malay.pas automatically converted to UTF-8
// If you want to make changes, please modify the original file
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Malay translation                               }
{                                                       }
{                                                       }
{*******************************************************}

unit RVALUTF8_Malay;

interface
{$I RV_Defs.inc}
implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'inci', 'cm', 'mm', 'pika', 'pixel', 'poin',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'cm', 'mm', 'pc', 'px', 'pt',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Fail', 'E&dit', 'F&ormat', 'Fo&n', '&Perenggan', '&Selit', '&Jadual', '&Tetinggkap', '&Bantu',
  // exit
  'K&eluar',
  // top-level menus: View, Tools,
  'Papa&ran', 'A&lat-alat',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Saiz', 'Gaya', 'P&ilih', '&Jajarkan Kandungan Sel', 'S&empadan Sel',
  // menus: Table cell rotation
  'Puta&r Sel',
  // menus: Text flow, Footnotes/endnotes
  'Aliran &Teks', '&Nota Kaki',
  // ribbon tabs: tab1, tab2, view, table
  '&Rumah', '&Lebih Maju', '&Paparan', 'J&adual',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Papan Klip', 'Fon', 'Perenggan', 'Senarai', 'Mengedit',
  // ribbon groups: insert, background, page setup,
  'Selit', 'Latar Belakang', 'Persediaan Halaman',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Pautan', 'Nota Kaki', 'Pandangan Dokumen', 'Tayangkan/Sembunyikan', 'Zum', 'Opsyen',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Selit', 'Hapuskan', 'Operasi', 'Sempadan',
  // ribbon groups: styles 
  'Gaya',
  // ribbon screen tip footer,
  'Tekan F1 untuk batuan lanjut',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Dokumen Terbaru', 'Simpan dokumen', 'Pratonton dan cetak dokumen',
  // ribbon label: units combo
  'Unit:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Baru', 'Baru|Cipta dokumen kosong yang baru',
  // TrvActionOpen
  'B&uka...', 'Buka|Buka dokumen dari disk',
  // TrvActionSave
  '&Simpan', 'Simpan|Menyimpan dokumen ke disk',
  // TrvActionSaveAs
  'Simp&an Sebagai...', 'Simpan Sebagai...|Menyimpan dokumen ke disk dengan nama yang baru',
  // TrvActionExport
  '&Eksport...', 'Eksport|Eksport dokumen kepada format lain',
  // TrvActionPrintPreview
  'P&ratonton Cetak', 'Pratonton Cetak|Tunjukkan dokumen seperti ia akan dicetakkan',
  // TrvActionPrint
  '&Cetak...', 'Cetak|Tukar seting percetakan dan mencetak dokumen',
  // TrvActionQuickPrint
  '&Cetak', 'Cetak &segera', 'Cetak|Mencetak dokumen',
  // TrvActionPageSetup
  '&Persediaan Halaman...', 'Persediaan Halaman|Set jidar, saiz kertas, orientasi, sumber, pengepala dan pengaki',
  // TrvActionCut
  'Po&tong', 'Potong|Potongkan terpilih dan letakkan di Papan Klip',
  // TrvActionCopy
  '&Salin', 'Salin|Salin terpilih dan letakkan di Papan Klip',
  // TrvActionPaste
  'Ta&mpal', 'Tampal|Selitkan kandungan dari Papan Klip',
  // TrvActionPasteAsText
  'Tampal se&bagai Teks', 'Tampal sebagai Teks|Selitkan teks dari Papan Klip',  
  // TrvActionPasteSpecial
  'Tampal &Istimewa...', 'Tampal Istimewa|Selitkan kandungan Papan Klip sebagai format yang dispesifikasi',
  // TrvActionSelectAll
  'Pilih Semu&a', 'Pilih Semua|Pilih semua dokumen',
  // TrvActionUndo
  'Bua&t Asal', 'Buat Asal|Kembali daripada tindakan terakhir',
  // TrvActionRedo
  '&Ulang', 'Ulang|Ulang tindakan terakhir',
  // TrvActionFind
  '&Cari...', 'Cari|Cari teks terpilih dalam dokumen',
  // TrvActionFindNext
  'Cari &Berikutnya', 'Cari Berikutnya|Teruskan carian teks terpilih yang berikut',
  // TrvActionReplace
  '&Ganti...', 'Ganti|Cari dan ganti teks terpilih dalam dokumen',
  // TrvActionInsertFile
  '&Fail...', 'Selit Fail|Selitkan kandungan fail dalam dokumen',
  // TrvActionInsertPicture
  'Gamba&r...', 'Insert Gambar|Selitkan satu gambar dari disk',
  // TRVActionInsertHLine
  'Garis &Mendatar', 'Selit Garis Mendatar|Selitkan satu garis mendatar',
  // TRVActionInsertHyperlink
  '&Hiperpautan...', 'Selit Hiperpautan|Selitkan satu pautan hiperteks',
  // TRVActionRemoveHyperlinks
  'H&apus Hiperpautan', 'Hapus Hiperpautan|Hapus semua hiperpautan dalam teks terpilih',  
  // TrvActionInsertSymbol
  '&Simbol...', 'Insert Simbol|Selitkan simbol',
  // TrvActionInsertNumber
  {*} '&Number...', 'Insert Number|Inserts a number',
  // TrvActionInsertCaption
  {*} 'Insert &Caption...', 'Insert Caption|Inserts a caption for the selected object',
  // TrvActionInsertTextBox
  {*} '&Text Box', 'Insert Text Box|Inserts a text box',
  // TrvActionInsertSidenote
  {*} '&Sidenote', 'Insert Sidenote|Inserts a note displayed in a text box',
  // TrvActionInsertPageNumber
  {*} '&Page Number', 'Insert Page Number|Inserts a page number',
  // TrvActionParaList
  '&Bulet dan Penomboran...', 'Bulet dan Penomboran|Laksanakan atau edit bulet atau penomboran untuk perenggan terpilih',
  // TrvActionParaBullets
  '&Bulet', 'Bulet|Tambah atau hapus bulet daripada perenggan',
  // TrvActionParaNumbering
  'Per&nomboran', 'Pernomboran|Tambah atau hapus pernomboran daripada perenggan',
  // TrvActionColor
  '&Warna Latar Belakang...', 'Warna Latar Belakang|Tukar warna latar belakang dokumen',
  // TrvActionFillColor
  'I&sian Warna...', 'Isian Warna |Tukar warna di latar sebalik teks, perenggan, sel, jadual atau dokumen',
  // TrvActionInsertPageBreak
  'Selit &Pemisah Halaman', 'Selit Pemisah Halaman|Selitkan satu pemisah halaman',
  // TrvActionRemovePageBreak
  '&Hapus Pemisah Halaman', 'Hapus Pemisah Halaman|Buang Pemisah Halaman',
  // TrvActionClearLeft
  'Pandangan Bersih Teks di tepi &Kiri', 'Pandangan Bersih Teks di belah Kiri|Letakkan perenggan ini di bawah sebarang gambar berjajar kiri',
  // TrvActionClearRight
  'Pandangan Bersih Teks di tepi Kana&n', 'Pandangan Bersih Teks di belah Kanan|Letakkan perenggan ini di bawah sebarang gambar berjajar kanan',
  // TrvActionClearBoth
  'Pandangan Teks Bersih Pada Kedua-dua &Tepi', 'Pandangan Teks Bersih Pada Kedua-dua Belah|Letakkan perenggan ini di bawah sebarang gambar berjajar kiri atau kanan',
  // TrvActionClearNone
  '&Pandangan Teks Normal', 'Pandangan Teks Normal|Izin aliran teks mengelilingi gambar berjajar kiri atau kanan',
  // TrvActionVAlign
  'Kedudukan &Objek...', 'Kedudukan Objek|Menukar kedudukan objek terpilih',  
  // TrvActionItemProperties
  '&Sifat Objek...', 'Sifat Objek|Mentakrifkan sifat objek aktif',
  // TrvActionBackground
  'Latar &Belakang...', 'Latar &Belakang|Pilih warna dan imej latar belakang',
  // TrvActionParagraph
  'P&erenggan...', 'Perenggan|Tukar ciri-ciri perenggan',
  // TrvActionIndentInc
  'Tambah &Inden', 'Tambah Inden|Tambah inden kiri pada perenggan terpilih',
  // TrvActionIndentDec
  '&Kurangkan Inden', 'Kurangkan Inden|Kurang inden kiri pada perenggan terpilih',
  // TrvActionWordWrap
  'Balut &Perkataan', 'Balut Perkataan|Toggle pembalutan perkataan untuk perenggan terpilih',
  // TrvActionAlignLeft
  'Jajarkan &Kiri', 'Jajarkan Kiri|Jajarkan teks terpilih ke kiri',
  // TrvActionAlignRight
  'Jajarkan Ka&nan', 'Jajarkan Kanan|Jajarkan teks terpilih ke kanan',
  // TrvActionAlignCenter
  'Jajarkan ke &Tengah', 'Jajarkan ke Tengah|Pusatkan teks terpilih',
  // TrvActionAlignJustify
  '&Laraskan', 'Laraskan|Jajarkan teks terpilih ke kedua-dua jidar kiri dan kanan',
  // TrvActionParaColor
  'Warna Latar &Belakang Perenggan...', 'Warna Latar Belakang Perenggan|Tetapkan warna latar belakang perenggan',
  // TrvActionLineSpacing100
  'Jarak Baris &Tunggal', 'Jarak Baris Tunggal|Tetapkan jarak ruang baris kepada 1 baris',
  // TrvActionLineSpacing150
  '&Jarak Baris 1.5', 'Jarak Baris 1.5|Tetapkan jarak ruang baris kepada 1.5 baris',
  // TrvActionLineSpacing200
  'Jarak Baris &Kembar', 'Jarak Baris Kembar|Tetapkan jarak ruang baris kepada 2 baris',
  // TrvActionParaBorder
  '&Sempadan dan Latar Belakang...', 'Sempadan dan Latar Belakang|Tetapkan sempadan dan latar belakang untuk perenggan terpilih',
  // TrvActionInsertTable
  'Sel&it Jadual...', '&Jadual', 'Sekit Jadual|Selitkan satu jadual baru',
  // TrvActionTableInsertRowsAbove
  'Selit Baris di &Atas', 'Selit Baris di Atas|Selitkan satu baris baru di atas sel-sel terpilih',
  // TrvActionTableInsertRowsBelow
  'Selit Baris di &Bawah', 'Selit Baris di Bawah|Selitkan satu baris baru di bawah sel-sel terpilih',
  // TrvActionTableInsertColLeft
  'Selit Lajur &Kiri', 'Selit Lajur Kiri|Selitkan satu lajur baru ke kiri sel-sel terpilih',
  // TrvActionTableInsertColRight
  'Selit Lajur Kana&n', 'Selit Lajur kanan|Selitkan satu lajur baru ke kanan sel-sel terpilih',
  // TrvActionTableDeleteRows
  'Hapuskan &Baris', 'Hapuskan Baris|Hapuskan baris-baris bersama dengan sel-sel terpilih',
  // TrvActionTableDeleteCols
  'Hapuskan &Lajur', 'Hapuskan Lajur|Hapuskan lajur-lajur bersama dengan sel-sel terpilih',
  // TrvActionTableDeleteTable
  '&Hapuskan Jadual', 'Hapuskan Jadual|Hapuskan jadual',
  // TrvActionTableMergeCells
  '&Cantumkan Sel', 'Cantumkan Sel|Cantumkan sel-sel terpilih',
  // TrvActionTableSplitCells
  '&Asingkan Sel...', 'Asingkan Sel|Asingkan sel-sel terpilih',
  // TrvActionTableSelectTable
  'Pilih &Jadual', 'Pilih Jadual|Pilih jadual',
  // TrvActionTableSelectRows
  'Pilih &Baris', 'Pilih Baris|Pilih baris-baris',
  // TrvActionTableSelectCols
  'Pilih &Lajur', 'Pilih Lajur|Pilih lajur-lajur',
  // TrvActionTableSelectCell
  'Pilih &Sel', 'Pilih Sel|Pilih sel',
  // TrvActionTableCellVAlignTop
  'Jajarkan Sel Menurut di &Atas', 'Jajarkan Sel Menurut di Atas|Jajarkan kandungan-kandungan sel mengikut aturan di atas',
  // TrvActionTableCellVAlignMiddle
  'Jajarkan Sel ke &Tengah', 'Jajarkan Sel ke Tengah|Jajarkan kandungan-kandungan sel ke tengah',
  // TrvActionTableCellVAlignBottom
  'Jajar sel ke &Bawah', 'Jajar sel ke Bawah|Jajarkan kandungan-kandungan sel ke bawah',
  // TrvActionTableCellVAlignDefault
  'Jadar Menegak Sel &Lalai', 'Jadar Menegak Sel Lalai|Tetapkan tetapan biasa jadar menegak untuk sel-sel terpilih',
  // TrvActionTableCellRotationNone
  '&Tiada Putaran Sel', 'Tiada Putaran Sel|Putarkan kandungan sel sebanyak 0°',
  // TrvActionTableCellRotation90
  'Putar Sel sebanyak &90°', 'Putar Sel sebanyak 90°|Memutarkan kandungan sel sebanyak 90°',
  // TrvActionTableCellRotation180
  'Putar Sel sebanyak &180°', 'Putar Sel sebanyak 180°|Memutarkan kandungan sel sebanyak 180°',
  // TrvActionTableCellRotation270
  'Putar Sel sebanyak &270°', 'Putar Sel sebanyak 270°|Memutarkan kandungan sel sebanyak 270°',
  // TrvActionTableProperties
  '&Sifat Jadual...', 'Sifat Jadual|Tukar sifat-sifat untuk jadual terpilih',
  // TrvActionTableGrid
  'Tunjukkan Garisan &Grid', 'Tunjukkan Garisan Grid|Paparkan atau sembunyikan garisan grid untuk jadual',
  // TRVActionTableSplit
  '&Pisah Jadual', 'Pisah Jadual|Pisahkan jadual menjadi dua jadual bermula dengan baris terpilih',
  // TRVActionTableToText
  'Tuka&rkan kepada Teks...', 'Tukarkan kepada Teks|Tukarkan jadual kepada teks',
  // TRVActionTableSort
  '&Isih...', 'Isih|Isis baris-baris jadual',
  // TrvActionTableCellLeftBorder
  'Sempadan &Kiri', 'Sempadan Kiri|Tunjukkan atau sembunyikan sempadan kiri sel',
  // TrvActionTableCellRightBorder
  'Sempadan Ka&nan', 'Sempadan Kanan|Tunjukkan atau sembunyikan sempadan kanan sel',
  // TrvActionTableCellTopBorder
  'Sempadan &Atas', 'Sempadan Atas|Tunjukkan atau sembunyikan sempadan atas sel',
  // TrvActionTableCellBottomBorder
  'Sempadan &Bawah', 'Sempadan Bawah|Tunjukkan atau sembunyikan sempadan bawah sel',
  // TrvActionTableCellAllBorders
  '&Semua Sempadan', 'Semua Sempadan|Tunjukkan atau sembunyikan semua sempadan sel',
  // TrvActionTableCellNoBorders
  'Tiada Se&mpadan', 'Tiada Sempadan|Sembunyikan semua sempadan sel',
  // TrvActionFonts & TrvActionFontEx
  '&Fon...', 'Fon|Tukarkan fon untuk teks terpilih',
  // TrvActionFontBold
  '&Tebal', 'Tebal|Tukarkan gaya teks terpilih ke tebal',
  // TrvActionFontItalic
  '&Italik', 'Italik|Tukarkan gaya teks terpilih ke italik',
  // TrvActionFontUnderline
  '&Garis Bawah', 'Garis Bawah|Tukarkan gaya teks terpilih ke garis bawah',
  // TrvActionFontStrikeout
  'Garis &Lorek', 'Garis Lorek|Lukis garisan melalui tengah teks terpilih',
  // TrvActionFontGrow
  'B&esarkan Fon', 'Besarkan Fon|Kembangkan ketinggian teks terpilih sebanyak 10%',
  // TrvActionFontShrink
  '&Kecilkan Fon', 'Kecilkan Fon|Kurangkan ketinggian teks terpilih sebanyak 10%',
  // TrvActionFontGrowOnePoint
  '&Besarkan Fon sebanyak Satu Poin', 'Besarkan Fon sebanyak Satu Poin|Kembangkan ketinggian teks terpilih sebanyak 1 poin',
  // TrvActionFontShrinkOnePoint
  '&Kecilkan Fon sebanyak Satu Poin', 'Kecilkan Fon sebanyak Satu Poin|Kurangkan ketinggian teks terpilih sebanyak 1 poin',
  // TrvActionFontAllCaps
  'Semua &Huruf Besar', 'Semua Huruf Besar|Tukarkan semua aksara teks terpilih kepada huruf besar',
  // TrvActionFontOverline
  'Garis &Atas', 'Garis Atas|Tambah garis atas teks terpilih',
  // TrvActionFontColor
  '&Warna Teks...', 'Warna Teks|Tukarkan warna teks terpilih',
  // TrvActionFontBackColor
  'Warna &Latar Belakang Teks...', 'Warna Latar Belakang Teks|Tukarkan warna latar belakang teks terpilih',
  // TrvActionAddictSpell3
  'Periksa &Ejaan', 'Periksa Ejaan|Periksa ejaan teks',
  // TrvActionAddictThesaurus3
  '&Tesaurus', 'Tesaurus|Cadangkan perkataan lain untuk perkataan yang terpilih',
  // TrvActionParaLTR
  'Kiri ke Kanan', 'Kiri ke Kanan|Tukar arah teks kiri-ke-kanan untuk perenggan terpilih',
  // TrvActionParaRTL
  'Kanan ke Kiri', 'Kanan ke Kiri|Tukar arah teks kanan-ke-kiri untuk perenggan terpilih',
  // TrvActionLTR
  'Kiri ke Kanan Teks', 'Kiri ke Kanan Teks|Tetapkan arah kiri-ke-kanan untuk teks terpilih',
  // TrvActionRTL
  'Kanan ke Kiri Teks', 'Kanan ke Kiri Teks|Tetapkan arah kanan-ke-kiri untuk teks terpilih',
  // TrvActionCharCase
  'Kes Aksara', 'Kes Aksara|Ubah kes untuk teks terpilih',
  // TrvActionShowSpecialCharacters
  'Aksara Tidak-&Cetak', 'Aksara Tidak-Cetak|Tunjukkan atau sembunyikan aksara tidak-cetak, seperti tanda-tanda perenggan, tab dan ruang putih',
  // TrvActionSubscript
  'Su&bskrip', 'Subskrip|Tukar teks terpilih kepada subskrip',
  // TrvActionSuperscript
  'Su&perskrip', 'Superskrip|Tukar teks terpilih kepada superskrip',
  // TrvActionInsertFootnote
  'Nota &Kaki', 'Nota Kaki|Selitkan nota kaki',
  // TrvActionInsertEndnote
  'Nota &Hujung', 'Nota Hujung|Selitkan nota hujung',
  // TrvActionEditNote
  'E&dit Nota', 'Edit Nota|Mula mengedit nota kaki atau nota hujung',
  // TrvActionHide
  '&Sembunyi', 'Sembunyi|Sembunyikan atau tunjukkan serpihan terpilih',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Gaya...', 'Gaya|Buka dialog pengurusan gaya',
  // TrvActionAddStyleTemplate
  '&Tambah Gaya...', 'Tambah Gaya|Ciptakan gaya baru teks atau perenggan berdasarkan serpihan terpilih',
  // TrvActionClearFormat,
  'Tiada &Pemformatan', 'Tiada Pemformatan|Kosongkan semua pemformatan teks dan perenggan daripada serpihan terpilih',
  // TrvActionClearTextFormat,
  'Tiada Pemformatan &Teks', 'Kosongkan Pemformatan Teks|Kosongkan semua pemformatan daripada teks terpilih',
  // TrvActionStyleInspector
  '&Inspektor Gaya', 'Inspektor Gaya|Tunjukkan atau sembunyikan Inspektor Gaya',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'OK', 'Batal', 'Tutup', 'Selit', '&Buka...', '&Simpan...', '&Kosongkan', 'Bantu', {*}'Remove',
  // Others  -------------------------------------------------------------------
  // percents
  'peratus',
  // left, top, right, bottom sides
  'Belah Kiri', 'Belas Atas', 'Belah Kanan', 'Belah Bawah',
  // save changes? confirm title
  'Simpan perubahan ke %s?', 'Pasti',
  // warning: losing formatting
  '%s mungkin mengandungi sifat-sifat yang tidak serasi dengan format yang dipilih untuk menyimpan.'#13+
  'Adakah anda ingin simpan dokumen dalam format ini?',
  // RVF format name
  'Format RichView',
  // Error messages ------------------------------------------------------------
  'Ralat',
  'Ralat memuatkan fail.'#13#13'Mungkin sebab:'#13'- format fail ini tidak disokong oleh program ini;'#13+
  '- fail rosak;'#13'- fail telah dibuka dan dikunci oleh program lain.',
  'Ralat memuatkan fail imej.'#13#13'Mungkin sebab:'#13'- fail ini mempunyai imej dalam format yang tidak disokong oleh program ini;'#13+
  '- fail tidak mengandungi imej;'#13+
  '- fail rosak;'#13'- fail telah dibuka dan dikunci oleh program lain.',
  'Ralat menyimpankan fail.'#13#13'Mungkin sebab:'#13'- kehabisan ruang dalam disk;'#13+
  '- disk dilindungi-menulis;'#13'- removable media tidak dimasukan;'#13+
  '- fail telah dibuka dan dikunci oleh program lain;'#13'- disk media rosak.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'Fail RichView (*.rvf)|*.rvf',  'Fail RTF (*.rtf)|*.rtf' , 'Fail XML (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Fail Teks (*.txt)|*.txt', 'Fail Teks - Unikod (*.txt)|*.txt', 'Fail Teks - AutoKesan (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'Fail HTML (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Dipermudahkan (*.htm;*.html)|*.htm;*.html',
  // DocX
  {*} 'Dokumen Microsoft Word (*.docx)|*.docx',  
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Selesai menggelintar', 'String carian ''%s'' tidak dijumpai.',
  // 1 string replaced; Several strings replaced
  '1 string digantikan.', '%d penggantian string.',
  // continue search from the beginning/end?
  'Sudah menggelintar ke penghujung dokumen, terus menggelintar dari permulaan?',
  'Sudah menggelintar ke permulaan dokumen, terus menggelintar dari penghujung?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Lut sinar', 'Auto',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Hitam', 'Coklat', 'Hijau buah Zaitun', 'Hijau Tua', 'Teal Tua', 'Biru Tua', 'Indigo', 'Kelabu-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Merah Tua', 'Jingga', 'Kuning Tua', 'Hijau', 'Teal', 'Biru', 'Biru-Kelabu', 'Kelabu-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Merah', 'Jingga Muda', 'Limau', 'Hijau Laut', 'Aqua', 'Biru Muda', 'Ungu', 'Kelabu-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Merah Muda', 'Emas', 'Kuning', 'Hijau Cerah', 'Biru Hijau', 'Biru Langit', 'Ungu Muda', 'Kelabu-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Rose', 'Coklat Perang', 'Kuning Muda', 'Hijau Muda', 'Biru Hijau Muda', 'Biru Pucat', 'Ungu Cerah', 'Putih',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Lut &sinar', '&Auto', 'Lebih &banyak Warna...', '&Lalai',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Latar Belakang', '&Warna:', 'Kedudukan', 'Latar Belakang', 'Contoh teks.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Tiada', '&Memanjang', '&Berjubin Tetap', 'Ber&jubin', '&Pusat',
  // Padding button
  'Pa&dding...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select warna
  'Warna Isian', '&Gunakan kepada:', 'Warna &Lain...', '&Padding...',
  'Sila pilih warna',
  // [apply to:] text, paragraph, table, cell
  'teks', 'perenggan' , 'jadual', 'sel',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Fon', 'Fon', 'Tataletak',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '&Fon:', 'Sai&z', 'Gaya', '&Tebal', '&Italik',
  // Script, Color, Back color labels, Default charset
  'S&krip:', '&Warna:', '&Latar Belakang:', '(Lalai)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Kesan', 'Garis Bawah', '&Garis Atas', 'Garis Lo&rek', 'Semua &Huruf besar',
  // Sample, Sample text
  'Contoh', 'Contoh teks',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Jarak Aksara', '&Jarak:', '&Dikembangkan', 'Dip&adatkan',
  // Offset group-box, Offset label, Down, Up,
  'Ofset menegak', '&Ofset:', 'T&urun', '&Naik',
  // Scaling group-box, Scaling
  'Skala', '&Skala:',
  // Sub/super script group box, Normal, Sub, Super
  'Subskrips and superskrips', 'Nor&mal', 'Su&bskrip', 'Su&perskrip',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Padding', '&Atas:', '&Kiri:', '&Bawah:', 'Ka&nan:', 'Nilai &sama',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Selitkan Hiperpautan', 'Hiperpautan', 'T&eks:', '&Sasaran', '<<selection>>',
  // cannot open URL
  'Tidak boleh memasuki ke "%s"',
  // hyperlink properties button, hyperlink style
  'S&uaikan...', '&Gaya:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) colors
  'Ciri-ciri Hiperpautan', 'Warna normal', 'Warna aktif',
  // Text [color], Background [color]
  '&Teks:', '&Latar Belakang',
  // Underline [color]
  'Garis &Bawah:',
  // Text [color], Background [color] (different hotkeys)
  'Te&ks:', 'L&atar Belakang',
  // Underline [color], Attributes group-box,
  'Garis Ba&wah:', 'Ciri-ciri',
  // Underline type: always, never, active (below the mouse)
  '&Garis Bawah:', 'selalu', 'tidak', 'aktif',
  // Same as normal check-box
  '&Seperti Normal',
  // underline active links check-box
  'Garis Bawah &pautan aktif',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Selitkan Simbol', '&Fon:', '&Set Aksara:', 'Unikod',
  // Unicode block
  '&Blok:',  
  // Character Code, Character Unicode Code, No Character
  'Kod Aksara: %d', 'Kod Aksara: Unikod %d', '(tiada aksara)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows, 
  'Selitkan Jadual', 'Saiz Jadual', 'Bilangan &Lajur:', 'Bilangan &Baris:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Tataletak Jadual', '&Automuat saiz', 'Automuat &tetingkap', '&Suaikan saiz',
  // Remember check-box
  'Ingat &dimensi untuk jadual-jadual baru',
  // VAlign Form ---------------------------------------------------------------
  'Kedudukan',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Sifat', 'Imej', {*}'Position and Size', 'Garis', 'Jadual', 'Baris', 'Sel',
  // Image Appearance, Image Misc, Number tabs
  {*} 'Appearance', 'Miscellaneous', 'Number',
   // Box position, Box size, Box appearance tabs
  {*} 'Position', 'Size', 'Appearance',
  // Preview label, Transparency group-box, checkbox, label
  'Pratonton:', 'Kelutsinaran', '&Lut sinar', '&Warna lut sinar:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  '&Tukar...', {*}'&Save...', 'Auto',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Penjajaran menegak', '&Jajarkan:',
  'bawah ke baris asas teks', 'tengah ke baris asas teks',
  'atas ke baris atas', 'bawah ke garis bawah', 'tengah ke garis tengah',
  // align to left side, align to right side
  'ke sebelah kiri', 'ke sebelah kanan',
  // Shift By label, Stretch group box, Width, Height, Default size
  '&Anjakkan:', 'Memanjang', '&Lebar:', '&Tinggi :', 'Saiz lalai: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  {*} 'Scale &proportionally', '&Reset',
  // Inside the border, border, outside the border groupboxes
  {*} 'Inside the border', 'Border', 'Outside the border',
  // Fill color, Padding (i.e. margins inside border) labels
  {*} '&Fill color:', '&Padding:',
  // Border width, Border color labels
  {*} 'Border &width:', 'Border &color:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  {*} '&Horizontal spacing:', '&Vertical spacing:',
  // Miscellaneous groupbox, Tooltip label
  {*} 'Miscellaneous', '&Tooltip:',
  // Web group-box, alt text
  'Web', 'Teks &gantian:',
  // Horz line group-box, color, width, style
  'Garis mendatar', '&Warna:', '&Lebar:', 'Ga&ya:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Jadual', 'Leb&ar:', '&Isian warna:', '&Ruang Sel...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  {*} '&Horizontal cell padding:', '&Vertical cell padding:', 'Border and background',
  // Visible table border sides button, auto width (in combobox), auto width label
  {*} 'Visible &Border Sides...', 'Auto', 'auto',
  // Keep row content together checkbox
  {*} '&Keep content together',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  {*} 'Rotation', '&None', '&90°', '&180°', '&270°',
  // Cell border label, Visible cell border sides button,
  {*} 'Border:', 'Visible B&order Sides...',
  // Border, CellBorders buttons
  'Sempa&dan Jadual...', '&Sempadan Sel ...',
  // Table border, Cell borders dialog titles
  'Jadual Sempadan', 'Sempadan Lalai Sel',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Pencetakan', '&Jangan membenarkan lajur dipisah merentasi halaman', 'Bilangan &pengepala baris:',
  'Pengepala Baris akan diulangi untuk setiap halaman jadual',
  // top, center, bottom, default
  '&Atas', '&Tengah', '&Bawah', '&Lalai',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Tetapan', '&Kesukaan lebar:', 'Tin&ggi sekurang-kurangnya:', '&Isi warna:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Sempadan', 'Tepi-tepi terlihat:',  'Warna ba&yangan:', 'Warna &muda', '&Warna:',
  // Background image
  'Gamba&r...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  {*} 'Horizontal position', 'Vertical position', 'Position in text',
  // Position types: align, absolute, relative
  {*} 'Align', 'Absolute position', 'Relative position',
  // [Align] left side, center, right side
  {*} 'left side of box to left side of',
  {*} 'center of box to center of',
  {*} 'right side of box to right side of',
  // [Align] top side, center, bottom side
  {*} 'top side of box to top side of',
  {*} 'center of box to center of',
  {*} 'bottom side of box to bottom side of',
  // [Align] relative to
  {*} 'relative to',
  // [Position] to the right of the left side of
  {*} 'to the right of the left side of',
  // [Position] below the top side of
  {*} 'below the top side of',
  // Anchors: page, main text area (x2)
  {*} '&Page', '&Main text area', 'P&age', 'Main te&xt area',
  // Anchors: left margin, right margin
  {*} '&Left margin', '&Right margin',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  {*} '&Top margin', '&Bottom margin', '&Inner margin', '&Outer margin',
  // Anchors: character, line, paragraph
  {*} '&Character', 'L&ine', 'Para&graph',
  // Mirrored margins checkbox, layout in table cell checkbox
  {*} 'Mirror&ed margins', 'La&yout in table cell',
  // Above text, below text
  {*} '&Above text', '&Below text',
  // Width, Height groupboxes, Width, Height labels
  {*} 'Width', 'Height', '&Width:', '&Height:',
  // Auto height label, Auto height combobox item
  {*} 'Auto', 'auto',
  // Vertical alignment groupbox; alignments: top, center, bottom
  {*} 'Vertical alignment', '&Top', '&Center', '&Bottom',
  // Border and background button and title
  {*} 'B&order and background...', 'Box Border and Background',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Tampal Istimewa', 'Tampal &sebagai:', 'format Rich text', 'format HTML',
  'Teks', 'Teks Unikod', 'Gambar Bitmap', 'Gambar Metafile',
  'Fail grafik', 'URL',
  // Options group-box, styles
  'Opsyen', '&Gaya:',
  // style options: apply target styles, use source styles, ignore styles
  'guna gaya dari dokumen sasaran', 'simpan gaya dan rupa',
  'simpan rupa, mengabaikan gaya',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Bulet dan Penomboran', 'Berbulet', 'Penomboran', 'Senarai berbulet', 'Senarai penomboran',
  // Customize, Reset, None
  '&Sesuaikan...', 'Se&t semula', 'Tiada',
  // Numbering: continue, reset to, create new
  'teruskan penomboran', 'set penomboran kepada', 'cipta senarai baru, bermula daripada',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Alpha besar', 'Roman besar', 'Decimal', 'Alpha kecil', 'Roman kecil',
  // Bullet type
  'Bulatan', 'Disk', 'Segi empat',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Aras:', '&Bermula dari:', 'Terus&kan', 'Penomboran',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Suaikan Senarai', 'Aras', 'Bi&langan:', 'Sifat Senarai', '&Jenis Senarai:',
  // List types: bullet, image
  'bulet', 'imej', 'Selit Nombor|',
  // Number format, Number, Start level from, Font button
  'Format &Nombor:', 'Nombor', '&Mula penomboran aras daripada:', '&Fon...',
  // Image button, bullet character, Bullet button,
  '&Imej...', 'Bulet a&ksara', 'B&ulet...',
  // Position of list text, bullet, number, image, text
  'Kedudukan senarai teks', 'Kedudukan bulet', 'Kedudukan nombor', 'Kedudukan imej ', 'Kedudukan Teks',
  // at, left indent, first line indent, from left indent
  '&di:', 'Ind&en kiri L&eft indent:', 'Inden &baris pertama &First line indent:', 'dari inden kiri',
  // [only] one level preview, preview
  'Prat&onton satu aras', 'Pratonton',
  // Preview text
  'Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'jajarkan kiri', 'jajarkan kanan', 'tengah',
  // level #, this level (for combo-box)
  'Aras %d', 'Aras ini',
  // Marker character dialog title
  'Edit Aksara Bulet',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Latar Belakang tabs
  'Sempadan perenggan dan Latar Belakang', 'Sempadan', 'Latar Belakang',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Settings', '&Warna:', '&Lebar:', 'Lebar &dalaman Int&ernal width:', '&Jidar Teks...',
  // Sample, Border type group-boxes;
  'Contoh', 'Jenis sempadan',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Tiada', '&Satu', '&Kembar', 'Ti&ga', 'Tebal &dalam', 'Tebal &luar',
  // Fill color group-box; More colors, padding buttons
  'Isi warna', '&Warna Lagi...', '&Padding...',
  // preview text
  'Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks.',
  // title and group-box in Offsets dialog
  'Jidar teks', 'Ofset Sempadan',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Perenggan', 'Penjajaran', '&Kiri', 'Kana&n', '&Tengah', '&Laraskan',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Ruang', '&Sebelum:', 'S&elepas:', 'Ruang &Baris:', '&di:',
  // Indents group-box; indents: left, right, first line
  'Indentasi', 'K&iri L&eft:', 'K&anan Ri&ght:', 'Ba&ris pertama:',
  // indented, hanging
  'dindentasi', 'Bergant&ung', 'Contoh',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Satu', '1.5 lines', 'Kembar', 'Sekurang-kurang', 'Tepat', 'Berganda',
  // preview text
  'Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks. Teks teks teks teks teks.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Inden dan Ruang', 'Tab', 'Aliran Teks',
  // tab stop position label; buttons: set, delete, delete all
  'Kedudukan berhenti &Tab :', '&Set', '&Hapus', 'Hapus Semu&a',
  // tab align group; left, right, center aligns,
  'Penjajaran', '&Kiri', 'Kana&n', 'Ten&gah',
  // leader radio-group; no leader
  'Ketua', '(Tiada)',
  // tab stops to be deleted, delete none, delete all labels
  'Tab berhenti untuk dihapuskan:', '(Tiada)', 'Semua.',
  // Pagination group-box, keep with next, keep lines together
  'Penomboran halaman', '&Simpan dengan seterusnya', 'Kekalkan &barisan bersama-sama',
  // Outline level, Body text, Level #
  '&Aras garis luar:', 'Badan Teks', 'Aras %d',  
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Pratonton cetakan', 'Lebar halaman', 'Halaman Penuh', 'Halaman-halaman:',
  // Invalid Scale, [page #] of #
  'Sila masukkan nombor dari 10 hingga 500', 'daripada %d',
  // Hints on buttons: first, prior, next, last pages
  'Halaman Pertama', 'Halaman Sebelum', 'Halaman Berikut', 'Halaman Terakhir',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Ruang Sel', 'Ruang', '&Antara Sel', 'Antara sempadan jadual dan sel-sel',
  // vertical, horizontal (x2)
  '&Menegak:', 'Men&datar:', 'Mene&gak:', 'Mendata&r:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Sempadan', 'Tetapan', '&Warna:', 'Warna &muda:', 'Warna &bayang:',
  // Width; Border type group-box;
  '&Lebar:', 'Jenis Sempadan',
  // Border types: none, sunken, raised, flat
  '&Tiada', 'Ten&ggelam', '&Dibangkitkan', '&Rata',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Pisah', 'Pisah kepada',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&Ditentukan bilangan baris dan lajur', '&Sel asal',
  // number of columns, rows, merge before
  'Bilangan &lajur:', 'Bilangan &Baris:', '&Cantum sebelum berpisah',
  // to original cols, rows check-boxes
  'Pisah kepada la&jur asal', 'Pisah kepada ba&ris asal',
  // Add Rows form -------------------------------------------------------------
  'Tambah Baris', '&Kiraan Baris:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Persediaan Halaman', 'Halaman', 'Pengepala dan Pengaki',
  // margins group-box, left, right, top, bottom
  'Jidar (milimeter)', 'Jidar (inci)', '&Kiri:', '&Atas:', 'Kana&n:', '&Bawah:',
  // mirror margins check-box
  '&Cerminkan jidar',
  // orientation group-box, portrait, landscape
  'Orientasi', '&Potret', '&Lanskap',
  // paper group-box, paper size, default paper source
  'Kertas', 'Sai&z:', 'Su&mber:',
  // header group-box, print on the first page, font button
  'Pengepala', 'T&eks:', 'Pengepala pada &halaman pertama', '&Fon...',
  // header alignments: left, center, right
  '&Kiri', 'Ten&gah', 'Ka&nan',
  // the same for footer (different hotkeys)
  'Pengaki', 'Tek&s:', 'Pengaki pa&da halaman pertama', 'F&on...',
  'K&iri', '&Tengah', 'K&anan',
  // page numbers group-box, start from
  'Nombor Halaman', '&Mula daripada:',
  // hint about codes
  'Kombinasi istimewa aksara:'#13'&&p - nombor halaman; &&P - kiraan halaman; &&d - tarikh kini; &&t - masa kini.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Fail Teks Halaman Kod', '&Pilih pengekodan fail:',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Jepun', 'Cina (Dipermudahkan)', 'Korea',
  // chinese (trad), central european, cyrillic, west european
  'Cina (Tradisional)', 'Eropah Tengah dan Timur', 'Cyrillic', 'Eropah Barat',
  // greek, turkish, hebrew, arabic
  'Yunani', 'Turki', 'Ibrani', 'Arab',
  // baltic, vietnamese, utf-8, utf-16
  'Baltik', 'Vietnam', 'Unikod (UTF-8)', 'Unikod (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Gaya Visual', 'Pilih &Gaya:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Tukarkan kepada Teks', 'Pi&lih pemisah:',
  // line break, tab, ';', ','
  'pemisah baris', 'tab', 'koma bertitik', 'koma',
  // Table sort form -----------------------------------------------------------
  // error message
  'Satu jadual mengandungi baris-baris dicantumkan tidak boleh diisih',
  // title, main options groupbox
  'Jadual Isih', 'Isih',
  // sort by column, case sensitive
  '&Isih mengikut lajur:', 'Peka &huruf',
  // heading row, range or rows
  '&Kecualikan pengepala baris', 'Barisan untuk isih: daripada %d to %d',
  // order, ascending, descending
  'Susunan', '&Menaiki', 'Me&nurun',
  // data type, text, number
  'Jenis', '&Teks', 'N&ombor',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  {*} 'Insert Number', 'Properties', '&Counter name:', '&Numbering type:',
  // numbering groupbox, continue, start from
  {*} 'Numbering', 'C&ontinue', '&Start from:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  {*} 'Caption', '&Label:', '&Exclude label from caption',
  // position radiogroup
  {*} 'Position', '&Above selected object', '&Below selected object',
  // caption text
  {*} 'Caption &text:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  {*} 'Numbering', 'Figure', 'Table',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  {*} 'Visible Border Sides', 'Border',
  // Left, Top, Right, Bottom checkboxes
  {*} '&Left side', '&Top side', '&Right side', '&Bottom side',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Ubah Saiz Lajur Jadual', 'Ubah Saiz Baris Jadual',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Inden Baris Pertama', 'Inden Kiri', 'Inden Tergantung', 'Inden Kanan',
  // Hints on lists: up one level (left), down one level (right)
  'Naikkan Satu Aras', 'Turunkan Satu Aras',
  // Hints for margins: bottom, left, right and top
  'Jidar Bawah', 'Jidar Kiri', 'Jidar Kanan', 'Jidar Atas',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Tab Dijajarkan Kiri', 'Tab Dijajarkan Kanan', 'Tab Dijajarkan Tengah', 'Tab Dijajarkan',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normal', 'Inden Normal', 'Tiada Ruang', 'Kepala %d', 'Senarai Perenggan',
  // Hyperlink, Title, Subtitle
  'Hyperpautan', 'Tajuk', 'Tajauk kecil',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Penekanan', 'Penekanan Halus', 'Penekanan Sengit', 'Kuat',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Petikkan', 'Petikkan Sengit', 'Rujukkan Halus', 'Rujukkan Sengit',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blok Teks', 'Ubahan HTML', 'Kod HTML', 'Akronim HTML',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'Definasi HTML', 'Keyboard HTML ', 'Contoh HTML ', 'Mesin taip HTML',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Diformatkan HTML', 'Memetik HTML', 'Pengepala', 'Pengaki', 'Nombor Halaman',
  // Caption
  {*} 'Caption',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Rujukkan Nota Hujung', 'Rujukkan Nota Kaki', 'Teks Nota Hujung', 'Teks Nota Kaki',
  // Sidenote Reference, Sidenote Text
  {*} 'Sidenote Reference', 'Sidenote Text',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'warna', 'warna latar belakang', 'lut sinar', 'lalai', 'warna garis bawah',
  // default background color, default text color, [color] same as text
  'warna latar belakang lalai', 'warna teks lalai', 'sama seperti teks',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'satu', 'tebal', 'kembar', 'bertitik', 'bertitik tebal', 'berputus-putus',
  // underline types: thick dashed, long dashed, thick long dashed,
  'berputus-putus tebal', 'berputus-putus panjang', 'berputus-putus panjang tebal',
  // underline types: dash dotted, thick dash dotted,
  'sempang bertitik', 'sempang tebal bertitik',
  // underline types: dash dot dotted, thick dash dot dotted
  'sempang titik bertitik', 'sempang titik bertitik tebal',
  // sub/superscript: not, subsript, superscript
  'tidak sub/superskrip', 'subskrip', 'superskrip',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'mod bi-di:', 'diwarisi', 'kiri ke kanan', 'kanan ke kiri',
  // bold, not bold
  'tebal', 'tidak tebal',
  // italic, not italic
  'italik', 'tidak italik',
  // underlined, not underlined, default underline
  'digaris bawah', 'tidak digaris bawah', 'digaris bawah lalai',
  // struck out, not struck out
  'digaris lorek', 'tidak digaris lorek',
  // overlined, not overlined
  'digarisatas', 'tidak digarisatas',
  // all capitals: yes, all capitals: no
  'semua huruf besar', 'semua huruf besar off',
  // vertical shift: none, by x% up, by x% down
  'tanpa anjakkan menegak', 'dianjakkan sebanyak %d%% ke atas', 'dianjakkan sebanyak %d%% ke bawah',
  // characters width [: x%]
  'lebar aksara',
  // character spacing: none, expanded by x, condensed by x
  'jarak biasa aksara', 'jarak dikembangkan sebanyak %s', 'jarak dipadatkan sebanyak %s',
  // charset
  'skrip',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'fon lalai', 'perenggan lalai', 'diwarisi',
  // [hyperlink] highlight, default hyperlink attributes
  'menyerlah', 'lalai',
  // paragraph aligmnment: left, right, center, justify
  'dijajarkan kiri', 'dijajarkan kanan', 'ditengahkan', 'dijustifikasikan',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'ketinggian baris: %d%%', 'jarak antara baris: %s', 'ketinggian baris: sekurang-kurangnya %s',
  'ketinggian baris: tepat %s',
  // no wrap, wrap
   'pembalut baris dimatikan', 'pembalut baris',
  // keep lines together: yes, no
  'simpan baris-baris bersama-sama', 'jangan simpan baris-baris bersama-sama',
  // keep with next: yes, no
  'simpan dengan perenggan berikut', 'jangan simpan dengan perenggan berikut',
  // read only: yes, no
  'membaca-sahaja', 'boleh diedit',
  // body text, heading level x
  'aras rangka: badan teks', 'aras rangka: %d',
  // indents: first line, left, right
  'inden baris pertama', 'inden kiri', 'inden kanan',
  // space before, after
  'ruang sebelum', 'ruang selepas',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tab', 'tiada', 'jajar', 'kiri', 'kanan', 'tengah',
  // tab leader (filling character)
  'kepala',
  // no, yes
  'tidak', 'ya',
  // [padding/spacing/side:] left, top, right, bottom
  'kiri', 'atas', 'kanan', 'bawah',
  // border: none, single, double, triple, thick inside, thick outside
  'tiada', 'satu', 'kembar', 'tiga', 'didalam tebal', 'diluar tebal',
  // background, border, padding, spacing [between text and border]
  'latar belakang', 'sempadan', 'padding', 'jarak',
  // border: style, width, internal width, visible sides
  'gaya', 'lebar', 'lebar dalaman', 'tepi dilihat',
  // style inspector -----------------------------------------------------------
  // title
  'Inspektor Gaya',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Gaya', '(Tiada)', 'Perenggan', 'Fon', 'Ciri-ciri',
  // border and background, hyperlink, standard [style]
  'Sempadan dan latar belakang', 'Hyperpautan', '(Standard)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Gaya-gaya', 'Gaya',
  // name, applicable to,
  '&Nama:', 'Laksanakan &kepada:',
  // based on, based on (no &),
  '&Didasarkan ke atas:', 'Didasarkan ke atas:',
  //  next style, next style (no &)
  'Gaya untuk &perenggan berikut:', 'Gaya untuk perenggan berikut:',
  // quick access check-box, description and preview
  '&Akses cepat', '&Penerangan dan pratonton:',
  // [applicable to:] paragraph and text, paragraph, text
  'perenggan dan teks', 'perenggan', 'teks',
  // text and paragraph styles, default font
  'Gaya Teks dan Perenggan', 'Fon Lalai',
  // links: edit, reset, buttons: add, delete
  'Edit', 'Set Semula', '&Tambah', '&Hapus',
  // add standard style, add custom style, default style name
  'Tambah Gaya &Standard...', 'Tambah Gaya Tersuai', 'Gaya %d',
  // choose style
  'Pilih ga&ya:',
  // import, export,
  '&Import...', '&Eksport...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
 'Gaya RichView (*.rvst)|*.rvst', 'Ralat memuatkan fail', 'Fail ini tidak mengandungi  gaya',
  // Title, group-box, import styles
  'Import Gaya daripada Fail', 'Import', 'I&mport gaya:',
  // existing styles radio-group: Title, override, auto-rename
  'Gaya sedia ada', 'mengetepikan &override', '&tambah nama semula',
  // Select, Unselect, Invert,
  '&Pilih', '&Nyahpilih', 'Menter&balik',
  // select/unselect: all styles, new styles, existing styles
  'Semu&a Gaya', 'Gaya bar&u', 'Gaya se&dia ada',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Kosongkan Format', 'Semua Gaya',
  // dialog title, prompt
  'Gaya', '&Pilih gaya untuk digunakan:',    
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  '&Sinonim', '&Abai semua', '&Tambah ke Kamus',
  // Progress messages ---------------------------------------------------------
  'Muatturun %s', 'Menyediakan untuk cetakkan...', 'Mencetak halaman %d',
  'Menukar daripada format Rich text...',  'Menukar kepada format Rich text...',
  'Membaca %s...', 'Menulis %s...', 'teks',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  {*} 'No Note', 'Footnote', 'Endnote', 'Sidenote', 'Text Box'
  );


initialization
  RVA_RegisterLanguage(
    'Malay', // english language name, do not translate
    {*} 'Malay', // native language name
    ANSI_CHARSET, @Messages);

end.
