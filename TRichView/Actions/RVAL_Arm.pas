{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}
{       Armenian translation                            }
{                                                       }
{       Copyright (c) Gagik Aghekyan                    }
{       gagik.aghekyan@gmail.com                        }
{                                                       }
{*******************************************************}

unit RVAL_Arm;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'դույմ', 'սմ', 'մմ', 'պիկեր', 'պիքսելներ', 'կետեր',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  '''''', 'սմ', 'մմ', 'պիկ', 'պիքսել', 'կետ',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Table
  'Ֆայլ', 'Խմբագրեմ', 'Ֆորմատ', 'Տառատեսակ', 'Պարբերություն', 'Տեղադրեմ', 'Աղյուսակ', 'Պատուհան', 'Տեղեկանք',
  // exit
  'Ելք',
  // top-level menus: View, Tools,
  'Տեսք', 'Գործիքներ',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Չափ', 'Ոճ', 'Ընտրեմ', 'Հավասարեցում վանդակում', 'Վանդակի շրջանակը',
  // menus: Table cell rotation
  'Պտտեմ վանդակը',
  // menus: Text flow, Footnotes/endnotes
  'Շրջանցում', 'Ծանոթագրություններ',
  // ribbon tabs: tab1, tab2, view, table
  'Գլխավոր', 'Տեղադրում և ֆոն', 'Տեսք', 'Աղյուսակ',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'Փոխանակման բուֆեր', 'Տառատեսակ', 'Պարբերություն', 'Ցուցակ', 'Խմբագրում',
  // ribbon groups: insert, background, page setup,
  'Տեղադրեմ', 'Ֆոն', 'Էջի պարամետրերը',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Հղումներ', 'Ծանոթագրություններ', 'Փաստաթուղթը նայելու ռեժիմները', 'Ցույց տամ կամ թաքցնեմ', 'Մասշտաբ', 'Պարամետրեր',
  // ribbon groups on table tab: insert, delete, operations, borders
  'Տեղադրեմ', 'Ջնջեմ', 'Գործողություններ', 'Շրջանակներ',
  // ribbon groups: styles 
  'Ոճեր',
  // ribbon screen tip footer,
  'ՏԵղեկանքի համար սեղմեք F1',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Վերջին փաստաթղթերը', 'Պահեմ փաստաթղթի կրկնօրինակը', 'Նախնական դիտումը և տպելը',
  // ribbon label: units combo
  'Միավորներ.',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  'Ստեղծեմ', 'Ստեղծեմ|Նոր փաստաթուղթ ստեղծելը',
  // TrvActionOpen
  'Բացեմ...', 'Բացեմ|Փաստաթղթով ֆայլը բացելը',
  // TrvActionSave
  'Պահեմ', 'Պահեմ|Փաստաթուղթը պահելը',
  // TrvActionSaveAs
  'Պահեմ որպես...', 'Պահեմ որպես...|Փաստաթուղթը պահելը այլ անունով, այլ տեղում կամ այլ ֆորմատով',
  // TrvActionExport
  'Արտահանեմ...', 'Արտահանեմ|Փաստաթուղթը արտահանելը այլ անունով ֆայլում, այլ տեղում կամ այլ ֆորմատով',
  // TrvActionPrintPreview
  'Նախնական դիտում', 'Նախնական դիտում|Փաստաթուղթը դիտելը այն տեսքով, որով տպվելու է',
  // TrvActionPrint
  'Տպեմ...', 'Տպեմ|Տպելու պարամետրերը տալն ու փաստաթուղթը տպելը',
  // TrvActionQuickPrint
  'Տպեմ', 'Արագ տպելը', 'Տպեմ|Փաստաթուղթը տպելը',
   // TrvActionPageSetup
  'Էջի պարամետրերը...', 'Էջի պարամետրերը|Դաշտերը, թղթի չափը, ուղղությունն ու աղբյուրը, էջագլուխներն ու էջատակերը տալը',  
  // TrvActionCut
  'Կտրեմ', 'Կտրեմ|Ընտրված հատվածը ջնջելը և նրա ուղարկելը փոխանակման բուֆեր',
  // TrvActionCopy
  'Կրկնօրինակեմ', 'Կրկնօրինակեմ|Ընտրված հատվածը կրկնօրինակելը փոխանակման բուֆերում',
  // TrvActionPaste
  'Տեղադրեմ', 'Տեղադրեմ|Փոխանակման բուֆերից հատված տեղադրելը փաստաթղթի ընթացիկ դիրքում',
  // TrvActionPasteAsText
  'Տեղադրեմ որպես տեքստ', 'Տեղադրեմ որպես տեքստ|Փոխանակման բուֆերից տեքստ տեղադրելը',  
  // TrvActionPasteSpecial
  'Հատուկ տեղադրում...', 'Հատուկ տեղադրում|Փոխանակման բուֆերի պարունակության տեղադրելը փաստաթղթի մեջ` ընտրված ֆորմատով',
  // TrvActionSelectAll
  'Ընտրեմ բոլորը', 'Ընտրեմ բոլորը|Ամբողջ փաստաթուղթը ընտրելը',
  // TrvActionUndo
  'Չեղյալ համարեմ', 'Չեղյալ համարեմ|Վերջին գործողությունը չեղյալ համարելը',
  // TrvActionRedo
  'Ետ բերեմ', 'Ետ բերեմ|"Չեղյալ համարեմ" հրամանը վերացելը',
  // TrvActionFind
  'Գտնեմ...', 'Գտնեմ|Նշված տեքստը որոնելը փաստաթղթում',
  // TrvActionFindNext
  'Գտնեմ շարունակությունում', 'Գտնեմ շարունակությունում|Տեքստի վերջին որոնման շարունակելը',
  // TrvActionReplace
  'Փոխարինեմ...', 'Փոխարինեմ|Նշված տեքստը որոնելը և փոխարինելը փաստաթղթում',
  // TrvActionInsertFile
  'Ֆայլ...', 'Տեղադրեմ ֆայլ|Ֆայլը տեղադրելը փաստաթղթի ընթացիկ դիրքում',
  // TrvActionInsertPicture
  'Նկար...', 'Տեղադրեմ նկար|Նկարը տեղադրելը փաստաթղթի ընթացիկ դիրքում',
  // TRVActionInsertHLine
  'Հորիզոնական գիծ', 'Տեղադրեմ հորիզոնական գիծ|Հորիզոնական գիծ տեղադրելը փաստաթղթի ընթացիկ դիրքում',
  // TRVActionInsertHyperlink
  'Հիպերհղում...', 'Ավելացնեմ հիպերհղում|Նոր հիպերհղում ավելացնելը կամ ընտրվածը խմբագրելը',
  // TRVActionRemoveHyperlinks
  'Ջնջեմ հիպերհղումները', 'Ջնջեմ հիպերհղումները|Ընտրված տեքստի բոլոր հիպերհղումները ջնջելը',  
  // TrvActionInsertSymbol
  'Սիմվոլ...', 'Տեղադրեմ սիմվոլ|Սիմվոլ կամ հատուկ նշան տեղադրելը',
  // TrvActionInsertNumber
  'Համար...', 'Ներմուծեմ համար|Համարի ներմուծումը',
  // TrvActionInsertCaption
  'Ներմուծեմ անվանում...', 'Ներմուծեմ անվանում|Ընտրված օբյեկտի համար անվանման ներմուծումը',
  // TrvActionInsertTextBox
  'Տեքստային դաշտ', 'Ներմուծեմ տեքստային դաշտ|Տեքստային դաշտի մերմուծումը',
  // TrvActionInsertSidenote
  'Ծանոթագրություն շրջանակում', 'Ներմուծեմ ծանոթագրություն շրջանակում|Թաստաթղթի ընթացիկ դիրքում ծանոթագրության ներմուծում, որն արտացոլվում է տեքստային դաշտում',
  // TrvActionInsertPageNumber
  'Էջի համարը', 'Ներմուծեմ էջի համար|Էջի համարի ներմուծումը',
  // TrvActionParaList
  'Ցուցակ...', 'Ցուցակ|Ընտրված պարբերությունների համար նշիչներ ավելացնելը կամ դրանց ֆորմատը փոխելը կամ համարակալելը',
  // TrvActionParaBullets
  'Նշիչներ', 'Նշիչներ|Ընտրված պարբերությունների համար նշիչներ ավելացնելը կամ ջնջելը',
  // TrvActionParaNumbering
  'Համարակալում', 'Համարակալում|Ընտրված պարբերությունների համարակալումները ավելացելը կամ ջնջելը',
  // TrvActionColor
  'Ֆոնի գույնը...', 'Ֆոնի գույնը|Փաստաթղթի ֆոնի գույնի փոփոխություն',
  // TrvActionFillColor
  'Ներկում...', 'Ներկում|Տեքստի, պարբերության, աղյուսակի կամ վանդակի ֆոնի գույնը փոխելը',
  // TrvActionInsertPageBreak
  'Էջի ընդհատում', 'Էջի ընդհատում|Փաստաթղթի ընթացիկ դիրքում էջի ընդհատում ավելացնելը',
  // TrvActionRemovePageBreak
  'Հանեմ էջի ընդհատումը', 'Հանեմ էջի ընդհատումը|Հանեմ էջի ընդհատումը',
  // TrvActionClearLeft
  'Չշրջանցեմ ձախ կողմից', 'Չշրջանցեմ ձախ կողմից|Պարբերություն տեղադրելը օբյեկտների տակ` հավասարեցնելով ըստ ձախ եզրի',
  // TrvActionClearRight
  'Չշրջանցեմ աջ կողմից', 'Չշրջանցեմ աջ կողմից|Պարբերություն տեղադրելը օբյեկտների տակ` հավասարեցնելով ըստ աջ եզրի',
  // TrvActionClearBoth
  'Չշրջանցեմ ոչ մի կողմից', 'Չշրջանցեմ ոչ մի կողմից|Պարբերություն տեղադրելը օբյեկտների տակ` հավասարեցնելով ըստ եզրերի',
  // TrvActionClearNone
  'Շրջանցեմ երկու կողմից', 'Շրջանցեմ երկու կողմից|Պարբերությանը օբյեկտները շրջանցել թույլ տալը` հավասարեցնելով ըստ եզրերի',
  // TrvActionVAlign
  'Օբյեկտի դիրքը...', 'Օբյեկտի դիրքը|Ընտրված օբյեկտի դիրքը տալը տեքստում',
  // TrvActionItemProperties
  'Օբյեկտի հատկությունները...', 'Օբյեկտի հատկությունները|Ընտրված նկարի, գծի կամ աղյուսակի հատկությունները տալը',
  // TrvActionBackground
  'Ֆոն...', 'Ֆոն|Ֆոնային նկարի և ներկման ընտրություն',
  // TrvActionParagraph
  'Պարբերություն...', 'Պարբերություն|Ընտրված պարբերությունների ատրիբուտները փոխելը',
  // TrvActionIndentInc
  'Մեծացնեմ նահանջը', 'Մեծացնեմ նահանջը|Ընտրված պարբերությունների ձախ նահանջը մեծացելը',
  // TrvActionIndentDec
  'Փոքրացնեմ նահանջը', 'Փոքրացնեմ նահանջը|Ընտրված պարբերությունների ձախ նահանջը փոքրացելը',
  // TrvActionWordWrap
  'Դարձնեմ մի տող', 'Դարձնեմ մի տող|Թույլատրեմ կամ արգելեմ պարբերությունը մի տող դարձնելը',
  // TrvActionAlignLeft
  'Ըստ ձախ եզրի', 'Ըստ ձախ եզրի|Ընտրված տեքստը հավասարեցնելը ըստ ձախ եզրի',
  // TrvActionAlignRight
  'Ըստ աջ եզրի', 'Ըստ աջ եզրի|Ընտրված տեքստը հավասարեցնելը ըստ աջ եզրի',
  // TrvActionAlignCenter
  'Ըստ կենտրոնի', 'Ըստ կենտրոնի|Ընտրված տեքստը հավասարեցնելը ըստ կենտրոնի',
  // TrvActionAlignJustify
  'Ըստ լայնության', 'Ըստ լայնության|Ընտրված տեքստը հավասարեցնելը ըստ ձախ և աջ եզրերի',
  // TrvActionParaColor
  'Պարբերության ներկման գույնը...', 'Պարբերության ներկման գույնը|Պարբերության ներկման գույնը ընտրելը',
  // TrvActionLineSpacing100
  'Եզակի ինտերվալ', 'Եզակի ինտերվալ|Պարբերության տողերի միջև եզակի ինտերվալ տալը',
  // TrvActionLineSpacing150
  'Մեկ ու կես ինտերվալ', 'Մեկ ու կես ինտերվալ|Պարբերության տողերի միջև մեկ ու կես ինտերվալ տալը',
  // TrvActionLineSpacing200
  'Կրկնակի ինտերվալ', 'Կրկնակի ինտերվալ|Պարբերության տողերի միջև կրկնակի ինտերվալ տալը',
  // TrvActionParaBorder
  'Պարբերության շրջանակն ու ներկումը...', 'Պարբերության շրջանակն ու ներկումը|Ընտրված պարբերությունների շրջանակները, ներկումներն ու դաշտերը տալը',
  // TrvActionInsertTable
  'Տեղադրեմ աղյուսակ...', 'Աղյուսակ', 'Տեղադրեմ աղյուսակ|Տեղադրեմ աղյուսակ փաստաթղթի ընթացիկ դիրքում',
  // TrvActionTableInsertRowsAbove
  'Վերևում տող ավելացնեմ', 'Վերևում տող ավելացնեմ|Ընտրված վանդակների վերևում նոր տող ավելացնեմ',
  // TrvActionTableInsertRowsBelow
  'Ներքևում տող ավելացնեմ', 'Ներքևում տող ավելացնեմ|Ընտրված վանդակների ներքևում նոր տող ավելացնեմ',
  // TrvActionTableInsertColLeft
  'Ձախից սյուն ավելացնեմ', 'Ձախից սյուն ավելացնեմ|Ընտրված վանդակներից ձախ նոր սյուն ավելացնեմ',
  // TrvActionTableInsertColRight
  'Աջից սյուն ավելացնեմ', 'Աջից սյուն ավելացնեմ|Ընտրված վանդակներից աջ նոր սյուն ավելացնեմ',
  // TrvActionTableDeleteRows
  'Ջնջեմ տողերը', 'Ջնջեմ տողերը|Ջնջեմ ընտրված վանդակներով տողերը',
  // TrvActionTableDeleteCols
  'Ջնջեմ սյուները', 'Ջնջեմ սյուները|Ջնջեմ ընտրված վանդակներով սյուները',
  // TrvActionTableDeleteTable
  'Ջնջեմ աղյուսակը', 'Ջնջեմ աղյուսակը|Ջնջեմ աղյուսակները',
  // TrvActionTableMergeCells
  'Միավորեմ վանդակները', 'Միավորեմ վանդակները|Ընտրված վանդակները միավորեմ մեկում',
  // TrvActionTableSplitCells
  'Տրոհեմ վանդակները...', 'Տրոհեմ վանդակները|Ընտրված վանդակները տրված թվով տողերի ու սյուների տրոհելը',
  // TrvActionTableSelectTable
  'Ընտրեմ աղյուսակ', 'Ընտրեմ աղյուսակ|Ամբողջ աղյուսակը ընտրելը',
  // TrvActionTableSelectRows
  'Ընտրեմ տողեր', 'Ընտրեմ տողեր|Ընտրված վանդակները պարունակող տողերը ընտրելը',
  // TrvActionTableSelectCols
  'Ընտրեմ սյուներ', 'Ընտրեմ սյուներ|Ընտրված վանդակները պարունակող սյուները ընտրելը',
  // TrvActionTableSelectCell
  'Ընտրեմ վանդակ', 'Ընտրեմ վանդակ|Վանդակ ընտրելը',
  // TrvActionTableCellVAlignTop
  'Հավասարեցնեմ ըստ վերևի եզրի', 'Հավասարեցնեմ ըստ վերևի եզրի|Վանդակի պարունակության հավասարեցումը ըստ վերևի եզրի',
  // TrvActionTableCellVAlignMiddle
  'Հավասարեցնեմ ըստ կենտրոնի', 'Հավասարեցնեմ ըստ կենտրոնի|Վանդակի պարունակության հավասարեցումը ըստ կենտրոնի',
  // TrvActionTableCellVAlignBottom
  'Հավասարեցնեմ ըստ ներքևի եզրի', 'Հավասարեցնեմ ըստ ներքևի եզրի|Վանդակի պարունակության հավասարեցումը ըստ ներքևի եզրի',
  // TrvActionTableCellVAlignDefault
  'Հիմնական հավասարեցում', 'Հիմնական հավասարեցում|Որպես հիմնական հավասարեցում ընդունեմ հավասարեցումը ըստ կենտրոնի',
  // TrvActionTableCellRotationNone
  'Չպտտեմ', 'Չպտտեմ|Վանդակի պարունակությունը չպտտեմ',
  // TrvActionTableCellRotation90
  'Պտտեմ վանդակը 90°-ով', 'Պտտեմ վանդակը 90°-ով|Վանդակի պարունակությունը 90°-ով պտտելը',
  // TrvActionTableCellRotation180
  'Պտտեմ վանդակը 180°-ով', 'Պտտեմ վանդակը 180°-ով|Վանդակի պարունակությունը 180°-ով պտտելը',
  // TrvActionTableCellRotation270
  'Պտտեմ վանդակը 270°-ով', 'Պտտեմ վանդակը 270°-ով|Վանդակի պարունակությունը 270°-ով պտտելը',
  // TrvActionTableProperties
  'Աղյուսակի հատկությունները...', 'Աղյուսակի հատկությունները|Աղյուսակի հատկությունները տալը',
  // TrvActionTableGrid
  'Ցանց', 'Ցանց|Չերևացող շրջանակների տեղը, բոլոր աղյուսակներում, ցանցերի ցույց տալը կամ թաքցնելը',
  // TRVActionTableSplit
  'Տրոհեմ աղյուսակը', 'Տրոհեմ աղյուսակը|Աղյուսակի տրոհումը երկու մասի` սկսած ընտրված տողից',
  // TRVActionTableToText
  'Փոխակերպեմ տեքստի...', 'Փոխակերպեմ տեքստի|Աղյուսակը փոխակերպելը սովորական տեքստի',
  // TRVActionTableSort
  'Տեսակավորում...', 'Տեսակավորում|Աղյուսակի տողերը տեսակավորելը',
  // TrvActionTableCellLeftBorder
  'Ձախ սահմանը', 'Ձախ սահմանը|Ընտրված վանդակների ձախ սահմանը ցույց տալը կամ թաքցնելը',
  // TrvActionTableCellRightBorder
  'Աջ սահմանը', 'Աջ սահմանը|Ընտրված վանդակների աջ սահմանը ցույց տալը կամ թաքցնելը',
  // TrvActionTableCellTopBorder
  'Վերևի սահմանը', 'Վերևի սահմանը|Ընտրված վանդակների վերևի սահմանը ցույց տալը կամ թաքցնելը',
  // TrvActionTableCellBottomBorder
  'Ներքևի սահմանը', 'Ներքևի սահմանը|Ընտրված վանդակների ներքևի սահմանը ցույց տալը կամ թաքցնելը',
  // TrvActionTableCellAllBorders
  'Բոլոր սահմանները', 'Բոլոր սահմանները|Ընտրված վանդակների բոլոր սահմանները ցույց տալը կամ թաքցնելը',
  // TrvActionTableCellNoBorders
  'Առանց սահմանների', 'Առանց սահմանների|Ընտրված վանդակների բոլոր սահմանները թաքցնելը',
  // TrvActionFonts & TrvActionFontEx
  'Տառատեսակ...', 'Տառատեսակ|Ընտրված տեքստի տառատեսակի պարամետրերը և սիմվոլների միջև հեռավորությունը փոխելը',
  // TrvActionFontBold
  'Թավ', 'Թավ|Ընտրված տեքստի տառատեսակը թավով փոխարինելը',
  // TrvActionFontItalic
  'Շեղ', 'Շեղ|Ընտրված տեքստի տառատեսակը շեղով փոխարինելը',
  // TrvActionFontUnderline
  'Ընդգծված', 'Ընդգծված|Ընտրված տեքստը ընդգծելը',
  // TrvActionFontStrikeout
  'Վրան գիծ քաշած', 'Վրան գիծ քաշած|Ընտրված տեքստի տառերի վրա գիծ քաշելը',
  // TrvActionFontGrow
  'Մեծացնեմ չափը', 'Մեծացնեմ չափը|Ընտրված տեքստի տառատեսակի չափը 10%-ով մեծացնելը',
  // TrvActionFontShrink
  'Փոքրացնեմ չափը', 'Փոքրացնեմ չափը|Ընտրված տեքստի տառատեսակի չափը 10%-ով փոքրացնելը',
  // TrvActionFontGrowOnePoint
  'Մեծացնեմ չափը 1 կետով', 'Մեծացնեմ չափը 1 կետով|Ընտրված տեքստի տառատեսակի չափը 1 կետով մեծացնեմ',
  // TrvActionFontShrinkOnePoint
  'Փոքրացնեմ չափը 1 կետով', 'Փոքրացնեմ չափը 1 կետով|Ընտրված տեքստի տառատեսակի չափը 1 կետով փոքրացնեմ',
  // TrvActionFontAllCaps
  'Բոլորը մեծատառ', 'Բոլորը մեծատառ|Ընտրված տեքստի բոլոր տառերը մեծատառերով ցույց տալը',
  // TrvActionFontOverline
  'Վերևից գծիկով', 'Վերևից գծիկով|Ընտրված տեքստի տառերից վերև հորիզոնական գիծ ավելացնելը',
  // TrvActionFontColor
  'Տեքստի գույնը...', 'Տեքստի գույնը|Ընտրված տեքստի գույնը փոխելը',
  // TrvActionFontBackColor
  'Տեքստի ֆոնի գույնը...', 'Տեքստի ֆոնի գույնը|Ընտրված տեքստի ֆոնի գույնը փոխելը',
  // TrvActionAddictSpell3
  'Ուղղագրություն', 'Ուղղագրություն|Ուղղագրությունը ստուգելը',
  // TrvActionAddictThesaurus3
  'Հոմանիշարան', 'Հոմանիշարան|Հոմանիշների որոնում',
  // TrvActionParaLTR
  'Ձախից աջ', 'Ձախից աջ|Ընտրված պարբերությունների տեքստի ուղղությունը տալ ձախից աջ',
  // TrvActionParaRTL
  'Աջից փախ', 'Աջից փախ|Ընտրված պարբերությունների տեքստի ուղղությունը տալ աջից ձախ',
  // TrvActionLTR
  'Ձախից աջ տեքստ', 'Ձախից աջ տեքստ|Ընտրված տեքստի ուղղությունը տալ ձախից աջ',
  // TrvActionRTL
  'Աջից ձախ տեքստ', 'Աջից ձախ տեքստ|Ընտրված տեքստի ուղղությունը տալ աջից ձախ',
  // TrvActionCharCase
  'Ռեգիստր', 'Ռեգիստր|Ընտրված տեքստի տառերի ռեգիստրը փոխելը',
  // TrvActionShowSpecialCharacters
  'Չտպվող նշաններ', 'Չտպվող նշաններ|Ծառայողական նշանները (բացարկ, տաբուլացիա) և պարբերությունը վերջանալու նշանները ցույց տալը կամ թաքցնելը',
  // TrvActionSubscript
  'Նշանը տողից ներքև', 'Տեղից ներքև|Ընտրված սիմվոլները ձևափոխելը ստորին ինդեքսների',
  // TrvActionSuperscript
  'Նշանը տողից վերև', 'Տողից վերև|Ընտրված սիմվոլները ձևափոխելը վերին ինդեքսների',
  // TrvActionInsertFootnote
  'Տողատակ', 'Տողատակ|Փաստաթղթում տողատակ ավելացնելը',
  // TrvActionInsertEndnote
  'Վերջնանշում', 'Վերջնանշում|Փաստաթղթում վերջնանշում ավելացնելը',
  // TrvActionEditNote
  'Խմբագրեմ տողատակը', 'Խմբագրեմ տողատակը|Ընթացիկ տողատակի տեքստը խմբագրելը',
  // TrvActionHide
  'Թաքցնեմ', 'Թաքցնեմ|Փաստաթղթի ընտրված հատվածը թաքցնելը կամ ցույց տալը',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  'Ոճեր...', 'Ոճեր|Ոճերի խմբագրման պատուհանը բացելը',
  // TrvActionAddStyleTemplate
  'Ստեղծեմ ոճ...', 'Ստեղծեմ ոճ|Ընտրված տեքստի հիման վրա ոճ ստեղծելը',
  // TrvActionClearFormat,
  'Հանեմ ֆորմատավորումը', 'Հանեմ ֆորմատավորումը|Հանեմ ընտրված հատվածի բոլոր ֆորմատավորումները',
  // TrvActionClearTextFormat,
  'Հանեմ տեքստի ֆորմատավորումը', 'Հանեմ տեքստի ֆորմատավորումը|Հանեմ ընտրված հատվածի տեքստի բոլոր ֆորմատավորումները',
  // TrvActionStyleInspector
  'Ոճերի տեսուչ', 'Ոճերի տեսուչ|"Ոճերի տեսուչ" պատուհանը ցույց տալը կամ թաքցնելը',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
  'ОК', 'Բան չանեմ', 'Փակեմ', 'Տեղադրեմ', 'Ընտրեմ...', 'Պահեմ...', 'Հանեմ', 'Տեղեկանք', 'Հեռացնեմ',
  // Others  -------------------------------------------------------------------
  '%',
  // left, top, right, bottom sides
  'Ձախ կողմ', 'Վերևի կողմ', 'Աջ կողմ', 'Ներքևի կողմ',
  // save changes? confirm title
  'Փոփոխությունները պահե՞մ %s-ում', 'Հաստատում',
  // warning: losing formatting
  '%s-ը կարող է ֆորմատավորում ունենալ, որը կկորի ընտրված ֆորմատի ֆայլի ձևափոխելիս:'#13+
  'Փաստաթուղթը այդ ֆորմատով պահե՞մ:',
  // RVF format name
  'RichView ֆորմատ',  
  // Error messages ------------------------------------------------------------
  'Սխալ',
  'Ֆայլը կարդալու սխալ:'#13#13'Հնարավոր պատճառները.'#13'- Ֆայի ֆորմատը հավելվածին հասկանալի չէ;'#13+
  '- ֆայլը վնասված է;'#13'- ֆայլը բացված է այլ հավելվածով, որն արգելափակում է նրա հասանելիությունը:',
  'Նկարով ֆայլը կարդալու սխալ:'#13#13'Հնարավոր պատճառները.'#13'- ֆայլում պարունակվող նկարի ֆորմատը հավելվածին հասկանալի չէ;'#13+
  '- ֆայլը նկար չի պարունակում;'#13+
  '- ֆայլը վնասված է;'#13'- ֆայլը բացված է այլ հավելվածով, որն արգելափակում է նրա հասանելիությունը:',
  'Ֆայլը պահելու սխալ:'#13#13'Հնարավոր պատճառները.'#13'- սկավառակի վրա տեղ չկա;'#13+
  '- սկավառակը փակ է գրելու համար;'#13'- սկավառակ չի դրված;'#13+
  '- ֆայլը բացված է այլ հավելվածով, որն արգելափակում է նրա հասանելիությունը;'#13+
  '- ինֆորմացիայի կրիչը վնասված է:',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView ֆորմատի ֆայլեր  (*.rvf)|*.rvf',  'RTF ֆայլեր (*.rtf)|*.rtf' , 'XML ֆայլեր (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Տեքստային ֆայլեր (*.txt)|*.txt', 'Տեքստային ֆայլեր` յունիկոդ (*.txt)|*.txt', 'Տեքստային ֆայլեր` ավտո (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML ֆայլեր (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'Պարզեցված HTML (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Documents (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Որոնումն ավարտվել է', 'Տող ''%s-ը'' չի գտնվել:',
  // 1 string replaced; Several strings replaced
  'Արված փոփոխությունները. 1.', 'Արված փոփոխությունները. %d',
  // continue search from the beginning/end?
  'Հասել եք փաստաթղթի վերջին: Շարունակե՞մ որոնումը սկզբից:',
  'Հասել եք փաստաթղթի սկզբին: Շարունակե՞մ որոնումը վերջից:',
  // Colors --------------------------------------------------------------------
  // Transparent, Auto
  'Առանց ներկման', 'Ավտո',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Սև', 'Շագանակագույն', 'Դեղնականաչ', 'Մուգ կանաչ',
  'Մուգ կապտամանուշակագույն', 'Մուգ կապույտ', 'Ինդիգո','Գորշ-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Մուգ կարմիր', 'Նարնջագույն', 'Մուգ դեղին', 'Կանաչ',
  'Կապտականաչ', 'Կապույտ', 'Կապտամանուշակագույն', 'Գորշ-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Կարմիր', 'Բաց նարնջագույն', 'Խոտի գույն', 'Զմրուխտ',
  'Մուգ փիրուզագույն', 'Մուգ երկնագույն', 'Մանուշակագույն', 'Գորշ-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Բաց մանուշակագույն', 'Ոսկեգույն', 'Դեղին', 'Վառ կանաչ',
  'Փիրուզագույն', 'Երկնագույն', 'Բալագույն', 'Գորշ-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Վարդագույն', 'Բաց շագանակագույն', 'Բաց դեղին', 'Թույլ կանաչ',
  'Բաց փիրուզագույն', 'Թույլ երկնագույն', 'Յասամանագույն', 'Սպիտակ',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Առանց ներկման', 'Ավտո', 'Այլ գույներ...', 'Սովորական',
  // Background Form -----------------------------------------------------------
  // Color label, Position group-box, Background group-box, Sample text
  'Ֆոն', 'Գույն.', 'Զետեղում', 'Պատկեր', 'Տեքստի  նմուշ:',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center, 
  'Ոչ', 'Ձգեմ', 'Կոշտ մոզայկա', 'Մոզայկա', 'Կենտրոնում',
  // Padding button
  'Դաշտեր...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button
  'Ներկում', 'Կիրառեմ.', 'Այլ գույն...', 'Դաշտեր...',
  'Ընտրեք գույն',
  // [apply to:] text, paragraph, table, cell
  'տեքստի համար', 'պարբերության համար' , 'աղյուսակի համար', 'վանդակի համար',  
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  'Տառատեսակ', 'Տառատեսակ', 'Ինտերվալ',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  'Տառատեսակ.', 'Չափ', 'Ոճ', 'Թավ', 'Շեղ',
  // Script, Color, Back color labels, Default charset
  'Սիմվոլների հավաքածու.', 'Գույն.', 'Ֆոն.', '(Հիմնական)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Ձևափոխություն', 'Ընդգծված', 'Վերևից գծիկով', 'Գիծ քաշված', 'Բոլոր մեծատառերը',
  // Sample, Sample text
  'Նմուշ', 'Տեքստի նմուշ',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Ինտերվալ', 'Ինտերվալ.', 'Նոսրացված', 'Խտացված',
  // Offset group-bix, Offset label, Down, Up,
  'Տեղաշարժ', 'Տեղաշարժ.', 'Ներքև', 'Վերև',
  // Scaling group-box, Scaling
  'Ձգում', 'Մասշտաբ.',
  // Sub/super script group box, Normal, Sub, Super
  'Վերին և ստորին ինդեքսներ', 'Սովորական', 'Տողից ներքև', 'Տողից վերև',  
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right
  'Դաշտեր', 'Վերևից.', 'Ձախից.', 'Ներքևից.', 'Աջից.',
  // Equal values check-box
  'Միևնույն արժեքը',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  'Հիպերհղում ավելացնելը', 'Հիպերհղում', 'Տեքստ:', 'URL:', '<<ընտրված հատված>>',
  // cannot open URL
  '"%s"-ը բացելու սխալ',
  // hyperlink properties button, hyperlink style
  'Ատրիբուտներ...', 'Ոճ.',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) сolors
  'Հղման ատրիբուտները', 'Սովորական գույներ', 'Տակից լուսավորման գույնը',
  // Text [color], Background [color]
  'Տեքստ.', 'Ֆոն.',
  // Underline [color]
  'Ընդգծում.',
  // Text [color], Background [color] (different hotkeys)
  'Տեքստ.', 'Ֆոն.',
  // Underline [color], Attributes group-box,
  'Ընդգծում.', 'Ատրիբուտներ',
  // Underline type: always, never, active (below the mouse)
  'Ընդգծում.', 'միշտ', 'երբեք', 'վրան պահելիս',
  // Same as normal check-box
  'Սովորականի պես',
  // underline active links check-box
  'Վրան պահելիս ընդգծեմ',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  'Սիմվոլ', 'Տառատեսակ.', 'Սիմվոլների հավաքածու.', 'Յունիկոդ',
  // Unicode block
  'Միջակայք.',  
  // Character Code, Character Unicode Code, No Character
  'Սիմվոլի կոդը. %d', 'Սիմվոլի կոդը. Յունիկոդ %d', '(սիմվոլ չի ընտրված)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows 
  'Աղյուսակի տեղադրում', 'Աղյուսակի չափերը', 'Սյուների թիվը.', 'Տողերի թիվը.',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Աղյուսակի լայնությունը', 'Ավտո', 'Պատուհանի լայնությամբ', 'Նշված.',
  // Remember check-box
  'Նոր աղյուսակների համար ընդունեմ որպես հիմնական',
  // VAlign Form ---------------------------------------------------------------
  'Դիրք',  
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Հատկություններ', 'Նկար', 'Դիրքը և չափերը', 'Գիծ', 'Աղյուսակ', 'Տողեր', 'Վանդակներ',
  // Image Appearance, Image Misc, Number tabs
  'Տեսք', 'Խառը', 'Համար',
   // Box position, Box size, Box appearance tabs
  'Դիրք', 'Չափ', 'Տեսք',
  // Preview label, Transparency group-box, checkbox, label
  'Դիտում.', 'Թափանցիկություն', 'Թափանցիկ', 'Թափանցիկ գույն.',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Ընտրեմ...', 'Պահեմ...', 'Ավտո',  
  // VAlign group-box and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Հավասարեցում', 'Հավասարեցնեմ.',
  'ներքևը ըստ տեքստի բազային գծի', 'կենտրոնը ըստ տեքստի բազային գծի',
  'ըստ տողի վերևի', 'ըստ տողի ներքևի', 'ըստ տողի կենտրոնի',
  // align to left side, align to right side
  'ըստ ձախ եզրի', 'ըստ աջ եզրի',
  // Shift By label, Stretch group box, Width, Height, Default size
  'Տեղաշարժեմ.', 'Չափ', 'Լայնություն.', 'Բարձրություն.', 'Սկզբնական չափը. %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'Պահպանեմ կողմերի հարաբերակցությունը', 'Սկզբնական վիճակ',
  // Inside the border, border, outside the border groupboxes
  'Շրջանակի ներսում', 'Շրջանակ', 'Շրջանակի շուրջը',
  // Fill color, Padding (i.e. margins inside border) labels
  'Ֆոնի գույնը.', 'Դաշտեր.',
  // Border width, Border color labels
  'Շրջանակի լայնությունը.', 'Շրջանակի գույնը.',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  'Հորիզոնական նահանջ.', 'Ուղղաձիգ նահանջ.',
  // Miscellaneous groupbox, Tooltip label
  'Խառը', 'Հուշում.',
  // web group-box, alt text
  'Վեբ', 'Տեղաշարժվող տեքստ.',
  // Horz line group-box, color, width, style
  'Հորիզոնական գիծ', 'Գույն.', 'Հաստություն.', 'Ոճ.',
  // Table group-box; Table Width, Color, CellSpacing
  'Աղյուսակ', 'Լայնություն.', 'Ներկման գույնը.', 'Վանդակների միջև ինտերվալները...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  'Վանդակների հորիզ. դաշտեր.', 'Վանդակների ուղղաձ. դաշտեր.', 'Շրջանակ և ֆոն',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Շրջանակի տեսանելի կողմեր', 'Ավտո', 'ավտո',
  // Keep row content together checkbox
  'Չընդհատեմ տողերի պարունակությունը',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Պտտեմ', 'Ոչ', '90°-ով', '180°-ով', '270°-ով',
  // Cell border label, Visible cell border sides button,
  'Շրջանակ.', 'Շրջանակի տեսանելի կողմեր',
  // Border, CellBorders buttons
  'Աղյուսակի շրջանակ...', 'Վանդակների շրջանակները',
  // Table border dialog title
  'Աղյուսակի շրջանակը', 'Վանդակների հիմնական շրջանակները',  
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Տպեմ', 'Արգելեմ տողեր փոխադրելը հաջորդ էջ', 'Վերնագրի տողերի թիվը.',
  'Վերնագրի տողերը կտպվեն աղյուսակի բոլոր էջերում',
  // top, center, bottom, default
  'Վերևից', 'Ըստ կենտրոնի', 'Ներքևից', 'Հիմնական',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Վանդակների պարամետրերը', 'Նախընտրելի լայնությունը.', 'Բարձրությունը ոչ պակաս.', 'Ներկման գույնը.',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Շրջանակ', 'Տեսանելի կողմերը.', 'Մուգ գույն.', 'Բաց գույն', 'Գույն.',
  // Background image
  'Նկար...',  
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Հորիզոնական դիրք', 'Ուղղաձիգ դիրք', 'Դիրքը տեքստում',
  // Position types: align, absolute, relative
  'Հավասարեցում', 'Բացարձակ դիրք', 'Հարաբերական դիրք',
  // [Align] left side, center, right side
  'ձախ կողմն ըստ ձախ կողմի',
  'կենտրոնն ըստ կենտրոնի',
  'աջ կողմն ըստ աջ կողմի',
  // [Align] top side, center, bottom side
  'վերևն ըստ վերևի գծի',
  'կենտրոնն ըստ կենտրոնի',
  'ներքևն ըստ ներքևի գծի',
  // [Align] relative to
  'նկատմամբ.',
  // [Position] to the right of the left side of
  'ձախ եզրից աջ.',
  // [Position] below the top side of
  'վերին եզրից ներքև.',
  // Anchors: page, main text area (x2)
  'Էջեր', 'Տեքստի տիրույթ', 'Էջեր', 'Տեքստի տիրույթ',
  // Anchors: left margin, right margin
  'Ձախ լուսանցք', 'Աջ լուսանցք',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  'Վերևի դաշտ', 'Ներքևի դաշտ', 'Ներքին դաշտի', 'Արտաքին դաշտի',
  // Anchors: character, line, paragraph
  'Նշան', 'Գիծ', 'Պարբերություն',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Հայելային դաշտեր', 'Աղյուսակի վանդակի նկատմամաբ',
  // Above text, below text
  'Տեքստից առաջ', 'Տեքստից հետո',
  // Width, Height groupboxes, Width, Height labels
  'Լայնություն', 'Բարձրություն', 'Լայնություն.', 'Բարձրություն.',
  // Auto height label, Auto height combobox item
  'Ավտո', 'ավտո',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Ուղղաձիգ հավասարցում', 'Վերևից', 'Ըստ կենտրոնի', 'Ներքևից',
  // Border and background button and title
  'Շրջանակ և ֆոն...', 'Տեքստային դաշտի շրջանակ և ֆոն',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Հատուկ տեղադրում', 'Որպես.', 'RTF ֆորմատ', 'HTML ֆորմատ',
  'Չֆորմատավորված տեքստ', 'Յունիկոդով տեքստ', 'Կետային նկար (BMP)', 'Մետաֆայլ (WMF)',
  'Նկարներով ֆայլեր', 'URL',
  // Options group-box, styles
  'Պարամետրեր', 'Ոճեր.',
  // style options: apply target styles, use source styles, ignore styles
  'կիրառեմ խմբագրվող փաստաթղթի ոճերը', 'պահեմ ոճերը և տեքստի արտաքին տեսքը',
  'պահեմ արտաքին տեսքը, անտեսեմ ոճերը',
  // List Gallery Form ---------------------------------------------------------
  // Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  'Ցուցակ', 'Նշիչով', 'Համարակալումով', 'Նշիչով ցուցակ', 'Համարակալումով ցուցակ',
  // Customize, Reset, None
  'Փոխեմ...', 'Reset', 'Ոչ',
  // Numbering: continue, reset to, create new
  'շարունակեմ համարակալումը', 'ցուցակում համարակալումը սկսեմ', 'ստեղծեմ նոր ցուցակ, սկսելով',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Մեծատառեր', 'Հռոմեական մեծատառեր', 'Տասնորդական', 'Փոքրատառեր', 'Հռոմեական փոքրատառեր',
  // Bullet type
  'Շրջանագիծ', 'Շրջան', 'Քառակուսի',
  // Level, Start from, Continue numbering, Numbering group-box
  'Մակարդակ.', 'Սկիզբ.', 'Շարունակեմ', 'Համարակալում',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, List properties, List types,
  'Ցուցակի փոփոխում', 'Մակարդակներ', 'թիվ.', 'Ցուցակի հատկությունները', 'Ցուցակի տեսակը.',
  // List types: bullet, image
  'նշիչ', 'նկար', 'Համար դնեմ|', 
  // Number format, Number, Start level from, Font button
  'Համարի ֆորմատը.', 'Համարը', 'Մակարդակի համարակալումը սկսեմ.', 'Տառատեսակ...',
  // Image button, bullet character, Bullet button,
  'Նկար...', 'Նշիչի ձևը', 'Ձև...',
  // Position of list text, bullet, number, image
  'Նշիչի դիրքը', 'Նշիչի դիրքը', 'Համարի դիրքը', 'Նկարի դիրքը', 'Տեքստի դիրքը',
  // at, left indent, first line indent, from left indent
  '', 'Նահանջ ձախից.', 'Նահանջ աջից.', 'ձախ նահանջից',
  // [only] one level preview, preview
  'Նախնական դիտում միայն մեկ մակարդակի համար', 'Նմուշ',
  // Preview text  
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]  
  'աջ կողմում', 'ձախ կողմում', 'կենտրոնում',
  // level #, this level (for combo-box)
  'Մակարդակ %d', 'Այդ մակարդակը',
  // Marker character dialog title
  'Նշիչի ընտրում',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Պարբերության շրջանակն ու ներկումը', 'Շրջանակ', 'Ներկում',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Շրջանակի տեսքը', 'Գույն.', 'Հաստություն.', 'Արանք.', 'Նահանջներ...',
  // Sample, Border type group-boxes;
  'Նմուշ', 'Շրջանակի տեսակը',
  // Border types: none, single, double, tripple, thick inside, thick outside
  'Ոչ', 'Եզակի', 'Կրկնակի', 'Եռակի', 'Հաստ, ներսից', 'Հաստ, դրսից',
  // Fill color group-box; More colors, padding buttons
  'Ներկման գույնը', 'Այլ գույն...', 'Դաշտեր...',
  // preview text
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. Տեքստ տեքստ տեքստ տեքստ տեքստ. Տեքստ տեքստ տեքստ տեքստ տեքստ.',
  // title and group-box in Offsets dialog
  'Նահանջներ', 'Նահանջ տեքստից մինչև շրջանակ',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Պարբերություն', 'Հավասարեցում', 'Ձախից', 'Աջից', 'Ըստ կենտրոնի', 'Ըստ լայնության',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Ինտերվալներ', 'Դեմից.', 'Վերջից.', 'Միջտողային.', 'Արժեքը.',
  // Indents group-box; indents: left, right, first line
  'Նահանջներ', 'Ձախից.', 'Աջից.', 'Առաջին տող.',
  // indented, hanging
  'Նահանջ', 'Ելուստ', 'Նմուշ',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Եզակի', 'Մեկ ու կես', 'Կրկնակի', 'Մինիմում', 'Ճշգրիտ', 'Գործակից',
  // preview text
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ. '+
  'Տեքստ տեքստ տեքստ տեքստ տեքստ.',
  // tabs: Indents and spacing, Tabs, Text flow
  'Նահանջներ և ինտերվալներ', 'Տաբուլացիա', 'Դիրքը էջի վրա',
  // tab stop position label; buttons: set, delete, delete all
  'Տաբուլացիայի դիրքը.', 'Տամ', 'Ջնջեմ', 'Ջնջեմ բոլորը',
  // tab align group; left, right, center aligns,
  'Հավասարեցում', 'Ձախից', 'Աջից', 'Ըստ կենտրոնի',
  // leader radio-group; no leader
  'Առաջնորդ', '(Ոչ)',
  // tab stops to be deleted, delete none, delete all labels
  'Կջնջվեն.', '', 'Բոլորը.',
  // Pagination group-box, keep with next, keep lines together
  'Բաժանում էջերի', 'Չկտրեմ հաջորդից', 'Չընդհատեմ պարբերությունը',
  // Outline level, Body text, Level #
  'Մակարդակ.', 'Հիմնական տեքստ', 'Մակարդակ %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Նախնական դիտում', 'Էջի լայնությամբ', 'Ամբողջ էջը', 'Էջերը.',
  // Invalid Scale, [page #] of #
  'Մուտքագրեք, խնդրեմ, 10-ից մինչև 500-ը որևէ թիվ', '%d-ից',
  // Hints on buttons: first, prior, next, last pages
  'Առաջին էջ', 'Նախորդ էջ', 'Հաջորդ էջ', 'Վերջին էջ',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Ինտերվալներ', 'Ինտերվալներ', 'Վանդակների միջև', 'Աղյուսակի շրջանակի և վանդակների միջև',
  // vertical, horizontal (x2)
  'Ուղղաձիգ.', 'Հորիզոնական.', 'Ուղղաձիգ.', 'Հորիզոնական.',  
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'Շրջանակներ', 'Պարամետրեր', 'Գույն.', 'Բաց գույն.', 'Մուգ գույն.',
  // Width; Border type group-box;
  'Հաստությունը.', 'Շրջանակի տեսակը',
  // Border types: none, sunken, raised, flat
  'Ոչ', 'Ներընկած', 'Ուռուցիկ', 'Հարթ',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Վանդակների տրոհում', 'Տրոհեմ',
  // split to specified number of rows and cols, split to original cells (unmerge)
  'Նշված թվով տողերի ու սյուների', 'Սկզբնական վանդակները',
  // number of columns, rows, merge before
  'Սյուների թիվը.', 'Տողերի թիվը.', 'Տրոհելուց առաջ միավորեմ',
  // to original cols, rows check-boxes
  'Տրոհեմ սկզբնական սյուների', 'Տրոհեմ սկզբնական տողերի',
  // Add Rows form -------------------------------------------------------------
  'Տողերի ավելացում', 'Տողերի թիվը.',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Էջի պարամետրերը', 'Էջ', 'Էջագլուխները և էջատակերը',
  // margins group-box, left, right, top, bottom
  'Դաշտեր (միլիմետրեր)', 'Դաշտեր (դույմեր)', 'Ձախը.', 'Վերևինը.', 'Աջը.', 'Ներքևինը.',
  // mirror margins check-box
  'Հայելապատճենեմ դաշտերը',
  // orientation group-box, portrait, landscape
  ' Ուղղվածություն', 'Ուղղաձիգ', 'Հորիզոնական',
  // paper group-box, paper size, default paper source
  'Թուղթ', 'Չափ.', 'Աղբյուր.',
  // header group-box, print on the first page, font button
  'Էջագլուխը', 'Տեքստ.', 'Տպեմ առաջին էջին', 'Տառատեսակ...',
  // header alignments: left, center, right
  'Ձախից', 'Կենտրոնում', 'Աջից',
  // the same for footer (different hotkeys)
  'Էջատակը', 'Տեքստ.', 'Տպեմ առաջին էջին', 'Տառատեսակ...',
  'Ձախից', 'Կենտրոնում', 'Աջից',
  // page numbers group-box, start from
  'Էջերի համարները', 'Առաջին համարը.',
  // hint about codes
  'Կարող եք օգտագործել հատուկ համադրություններ.'#13'&&p - էջի համարը; &&P - էջերի քանակը; &&d - ամսաթիվ; &&t - ընթացիկ ժամ:',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Տեքստային ֆայլի կոդավորումը', 'Ընտրեք տեքստի կոդավորումը.',
  // thai, japanese, chinese (simpl), korean
  'Thai', 'Japanese', 'Chinese (Simplified)', 'Korean',
  // chinese (trad), central european, cyrillic, west european
  'Chinese (Traditional)', 'Central and East European', 'Cyrillic', 'West European',
  // greek, turkish, hebrew, arabic
 'Greek', 'Turkish', 'Hebrew', 'Arabic',
  // baltic, vietnamese, utf-8, utf-16
 'Baltic', 'Vietnamese', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Ձևավորում', 'Ընտրեք ձևավորման ոճը.',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Ձևափոխեմ տեքստի', 'Ընտրեք բաժանիչ.',
  // line break, tab, ';', ','
  'տողի ընդհատում', 'տաբուլացիայի նշան', 'կետ, ստորակետ', 'ստորակետ',
  // Table sort form -----------------------------------------------------------
  // error message
  'Հնարավոր չէ տեսակավորել միավորված վանդակներ պարունակող աղյուսակը',
  // title, main options groupbox
  'Տեսակավորում', 'Տեսակավորում',
  // sort by column, case sensitive
  'Տեսակավորեմ ըստ սյան.', 'հաշվի առնեմ տառերի ռեգիստրը',
  // heading row, range or rows
  'Բացառեմ վերնագիր տողը', 'Տողեր տեսակավորման համար. %d-ից մինչև %d',
  // order, ascending, descending
  'Տեսակավորեմ', 'ըստ աճման', 'ըստ նվազման',
  // data type, text, number
  'Տեսակ', 'տեքստ', 'թվեր',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  'Համար', 'Հատկություններ', 'Հաշվիչի անունը.', 'Համարակալման տեսակը.',
  // numbering groupbox, continue, start from
  'Համարակալում', 'Շարունակեմ', 'Սկզբնհամար.',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Վերնագիր', 'Ստորագրություն.', 'Բացառեմ ստորագրությունը վերնագրում',
  // position radiogroup
  'Դիրք', 'Ընտրված օբյեկտից վերև', 'Ընտրված օբյեկտից ներքև',
  // caption text
  'Անվանման տեքստը.',  
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Համարակալում', 'Նկար', 'Աղյուսակ',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Շրջանակի տեսանելի կողմերը', 'Շրջանակ',
  // Left, Top, Right, Bottom checkboxes
  'Ձախ կողմ', 'Վերևի կողմ', 'Աջ կողմ', 'Ներքևի կողմ',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Աղյուսակի սյան լայնության փոփոխում', 'Աղյուսակի տողի բարձրությունը փոխելը',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Նահանջ առաջին տողի համար', 'Նահանջ աջից', 'Ելուստ', 'Նահանջ ձախից',
  // Hints on lists: up one level (left), down one level (right)
  'Փոքրացնեմ ցուցակի մակարդակը', 'Մեծացնեմ ցուցակի մակարդակը',
  // Hints for margins: bottom, left, right and top
  'Ներքևի դաշտը', 'Ձախ դաշտը', 'Աջ դաշտը', 'Վերևի դաշտը',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Ըստ ձախ եզրի', 'Ըստ աջ եզրի', 'Ըստ կենտրոնի', 'Ըստ բաժանիչի',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Սովորական', 'Սովորական նահանջ', 'Առանց ինտերվալի', 'Վերնագիր %d', 'Ցուցակի պարբերություն',
  // Hyperlink, Title, Subtitle
  'Հիպերհղում', 'Անվանում', 'Ենթավերնագիր',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'Ընդգծում', 'Նրբագեղ ընդգծում', 'Գործուն ընդգծում', 'Ուժեղ',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Մեջբերում 2', 'Գործուն մեջբերում', 'Նրբագեղ հղում', 'Գործուն հղում',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Մեջբերում', 'HTML փոփոխական', 'HTML կոդ', 'HTML սեղմանուն',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML սահմանում', 'HTML ստեղնաշար', 'HTML նմուշ', 'HTML մեքենագրող',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'Ստանդարտ HTML', 'HTML մեջբերում', 'Էջագլուխը', 'Էջատակը',
  'Էջի համարը',
  // Caption
  'Վերնագիր',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'Վերջնանշման հղում', 'Տողատակի հղում', 'Վերջնանշման տեքստ', 'Տողատակի տեքստ',
  // Sidenote Reference, Sidenote Text
  'Ծանոթագրության նշան շրջանակում', 'Ծանոթագրության տեքստ շրջանակում',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'գույն', 'ֆոնի գույնը', 'թափանցիկ', 'սովորական', 'ընդգծման գույնը',
  // default background color, default text color, [color] same as text
  'ֆոնի սովորական գույնը', 'տեքստի սովորական գույնը', 'ինչպես տեքստինն է',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'սովորական', 'թավ', 'կրկնակի', 'կետագծով', 'թավ կետագծեր', 'շտրիխավորված',
  // underline types: thick dashed, long dashed, thick long dashed,
  'թավ շտրիխավորված', 'երկար շտրիխավորված', 'թավ երկար շտրիխավորված',
  // underline types: dash dotted, thick dash dotted,
  'կետագծով շտրիխավորված', 'թավ կետագծով  շտրիխավորված',
  // underline types: dash dot dotted, thick dash dot dotted
  'կետագծով շտրիխավորված 2 կետով', 'թավ կետագծով  շտրիխավորված 2 կետով',
  // sub/superscript: not, subsript, superscript
  'ոչ տողից վերև/տողից ներքև', 'տողից ներքև', 'տողից վերև',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'տեքստի ուղղությունը', 'հիմնական', 'ձախից աջ', 'աջից ձախ',
  // bold, not bold
  'թավ', 'ոչ թավ',
  // italic, not italic
  'շեղ', 'ոչ շեղ',
  // underlined, not underlined, default underline
  'ընդգծում', 'չընդգծված', 'ընդգծվածը որպես հիմնական',
  // struck out, not struck out
  'գիծ քաշված', 'առանց գիծ քաշելու',
  // overlined, not overlined
  'վերևից գծիկով', 'առանց վերևից գծիկի',
  // all capitals: yes, all capitals: no
  'բոլոր մեծատառերը', ' ոչ բոլոր մեծատառերը',
  // vertical shift: none, by x% up, by x% down
  'առանց ուղղաձիգ տեղաշարժի', 'տեղաշարժված է %d%%-ով  վերև', 'տեղաշարժված է %d%%-ով ներքև',
  // characters width [: n%]
  'տառերի լայնությունը',
  // character spacing: none, expanded by x, condensed by x
  'միջսիմվոլային սովորական հեռավորություն', '%s-ով նոսրացված', '%s-ով խտացված',
  // charset
  'սիմվոլների հավաքածու',
  // default font, default paragraph, inherited attibutes [of font, paragraph attributes, etc.]
  'հիմնական տառատեսակ', 'հիմնական պարբերություն', 'ժառանգված հատկություններ',
  // [hyperlink] highlight, default hyperlink attributes
  'տակից լույս', 'հիմնական հատկություներ',
  // paragraph aligmnment: left, right, center, justify
  'ըստ ձախ եզրի', 'ըստ աջ եզրի', 'ըստ կենտրոնի', 'ըստ լայնության',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'տողի բարձրությունը. %d%%', 'տողերի միջև հեռավորությունը. %s', 'տողի բարձրությունը. ոչ պակաս %s',
  'տողի բարձրությունը. հավասար է %s',
  // no wrap, wrap
  'արգելեմ տողերի փոխադրումը', 'թույլատրեմ տողերի փոխադրումը',
  // keep lines together: yes, no
  'չտրոհեմ պարբերությունը', 'թույլ տամ տրոհել պարբերությունը',
  // keep with next: yes, no
  'չկտրեմ հաջորդ պարբերությունից', 'թույլ տամ կտրել հաջորդ պարբերությունից',
  // read only: yes, no
  'արգելեմ խմբագրել', 'թույլ տամ խմբագրել',
  // body text, heading level x
  'կառուցվածքի մակարդակը. հիմնական տեքստ', 'կառուցվածքի մակարդակը. %d',
  // indents: first line, left, right
  'առաջին տողի նահանջը', 'ձախ նահանջ', 'աջ նահանջ',
  // space before, after
  'նախաինտերվալ', 'ետինտերվալ',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'տաբուլացիաներ', 'ոչ', 'հավասարեցում', 'ըստ ձախ եզրի', 'ըստ աջ եզրի', 'ըստ կենտրոնի',
  // tab leader (filling character)
  'առաջնորդ',
  // no, yes,
  'ոչ', 'այո',
  // [padding/spacing/side:] left, top, right, bottom
  'ձախից', 'վերևից', 'աջից', 'ներքևից',
  // border: none, single, double, triple, thick inside, thick outside
  'ոչ', 'եզակի', 'կրկնակի', 'եռակի', 'հաստ, ներսից', 'հաստ, դրսից',
  // background, border, padding, spacing [between text and border]
  'ներկում', 'շրջանակ', 'դաշտեր', 'նահանջ',
  // border: style, width, internal width, visible sides
  'ոճ', 'հաստություն', 'արանք', 'կողմեր',
  // style inspector -----------------------------------------------------------
  // title
  'Ոճերի տեսուչ',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Ոճ', '(ոչ)', 'Պարբերություն', 'Տառատեսակ', 'Հատկություններ',
  // border and background, hyperlink, standard [style]
  'Շրջանակ և ներկում', 'Հիպերհղում', '(սովորական)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Ոճեր', 'Ոճ',
  // name, applicable to,
  'Անվանում.', 'Կիրառեմ.',
  // based on, based on (no &),
  'Հիմնված է ոճի վրա.', 'Հիմնված է ոճի վրա.',
  //  next style, next style (no &)
  'Հաջորդ պարբերության ոճը.', 'Հաջորդ պարբերության ոճը.',
  // quick access check-box, description and preview
  'Արագ հասանելիություն', 'Նկարագրություն և դիտում.',
  // [applicable to:] paragraph and text, paragraph, text
  'պարբերություններին և տեքստին', 'պարբերություններին', 'տեքստին',
  // text and paragraph styles, default font
  'Տեքստի և պարբերությունների ոճը', 'Հիմնական տառատեսակ',
  // links: edit, reset, buttons: add, delete
  'Փոխեմ', 'Reset', 'Ավելացնեմ', 'Ջնջեմ',
  // add standard style, add custom style, default style name
  'Ավելացնեմ ստանդարտ ոճ...', 'Ավելացնեմ ոճ', 'Ոճ %d',
  // choose style
  'Ընտրեք ոճը.',
  // import, export,
  'Ներմուծեմ...', 'Արտահանեմ...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView (*.rvst)|*.rvst ոճեր', 'Ֆայլը կարդալու սխալ', 'Ընտրված ֆայլում ոճեր չկան',
  // Title, group-box, import styles
  'Ներմուծեմ ոճեր ֆայլից', 'Ներմուծեմ', 'Ներմուծեմ ոճեր.',
  // existing styles radio-group: Title, override, auto-rename
  'Առկա ոճերը', 'փոխարինեմ նորերով', 'ավելացնեմ նոր անուններով',
  // Select, Unselect, Invert,
  'Ընտրեմ', 'Հանեմ ընտրվածությունը', 'Շրջեմ',
  // select/unselect: all styles, new styles, existing styles
  'Բոլոր ոճերը', 'Նոր ոճերը', 'Առկա ոճերը',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Հանեմ ֆորմատավորումը', 'Բոլոր ոճերը',
  // dialog title, prompt
  'Ոճերը', 'Ընտրեք ոճը.',
  {$ENDIF}
  // Spelling check and thesaurus ----------------------------------------------
  'Հոմանիշները', 'Բաց թողնել բոլորը', 'Ավելացնեմ բառարանում',
  // Progress messages ---------------------------------------------------------
  'Ներբեռնվում է %s', 'Նախապատրաստվում է տպելու համար...',
  'Տպվում է %d էջը',
  'Ձևափոխեմ RTF-ից...',  'Ձևափոխեմ RTF-ի...',
  'Կարդալը. %s...', 'Գրելը. %s...', 'տեքստ',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Ծանոթագրություն չկա', 'Ծանոթագրություն', 'Վերջնանշում', 'Ծանոթագրություն շրջանակում', 'Տեքստային դաշտ'
  );


initialization
  RVA_RegisterLanguage(
    'Armenian', // english language name, do not translate
    'Հայերեն', // native language name
    DEFAULT_CHARSET, @Messages);

end.
