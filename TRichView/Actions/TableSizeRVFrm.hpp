﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TableSizeRVFrm.pas' rev: 27.00 (Windows)

#ifndef TablesizervfrmHPP
#define TablesizervfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <RVGrids.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tablesizervfrm
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVTableSize;
class PASCALIMPLEMENTATION TfrmRVTableSize : public Vcl::Forms::TForm
{
	typedef Vcl::Forms::TForm inherited;
	
__published:
	Rvgrids::TRVGrid* grid;
	Vcl::Extctrls::TPanel* Panel1;
	Vcl::Extctrls::TShape* Shape1;
	void __fastcall gridDrawCell(System::TObject* Sender, int ACol, int ARow, const System::Types::TRect &ARect, bool Selected);
	void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall gridSelectCell(System::TObject* Sender);
	void __fastcall gridMouseMove(System::TObject* Sender, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall gridKeyDown(System::TObject* Sender, System::Word &Key, System::Classes::TShiftState Shift);
	void __fastcall gridMouseUp(System::TObject* Sender, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall FormDeactivate(System::TObject* Sender);
	void __fastcall Panel1MouseMove(System::TObject* Sender, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall Panel1Click(System::TObject* Sender);
	void __fastcall Shape1MouseDown(System::TObject* Sender, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall FormClose(System::TObject* Sender, System::Uitypes::TCloseAction &Action);
	void __fastcall FormActivate(System::TObject* Sender);
	
private:
	unsigned FPopupTime;
	bool NoSelection;
	bool JustShown;
	bool FSelected;
	System::UnicodeString FCancelCaption;
	
public:
	bool Cancelled;
	void __fastcall Init(const System::UnicodeString CancelCaption, const System::UnicodeString FontName, System::Uitypes::TFontCharset Charset);
	void __fastcall PopupAtMouse(void);
	void __fastcall PopupAt(const System::Types::TRect &r);
	void __fastcall PopupAtControl(Vcl::Controls::TControl* ctrl);
public:
	/* TCustomForm.Create */ inline __fastcall virtual TfrmRVTableSize(System::Classes::TComponent* AOwner) : Vcl::Forms::TForm(AOwner) { }
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVTableSize(System::Classes::TComponent* AOwner, int Dummy) : Vcl::Forms::TForm(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVTableSize(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVTableSize(HWND ParentWindow) : Vcl::Forms::TForm(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tablesizervfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TABLESIZERVFRM)
using namespace Tablesizervfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TablesizervfrmHPP
