{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Lithuanian translation                          }
{                                                       }
{       Copyright (c) Marius �emaitis                   }
{       ze.markos@gmail.com; marius@lvat.lt;            }
{       http://ze.markos.googlepages.com                }
{                                                       }
{*******************************************************}
{ Updated: 2014-02-04                                   }
{*******************************************************}

unit RVAL_Lith;
{$I RV_Defs.inc}
interface

implementation
uses Windows, RVALocalize;

const
  Messages: TRVAMessages =
  (
  '',
  // units: inches, cm, mm, picas, pixels, points
  'coliai','cm', 'mm', 'cicerai', 'pikseliai', 'ta�kai',
  // units : [n] inches, [n] cm, [n] mm, [n] picas, [n] pixels, [n] points
  'col', 'cm', 'mm', 'cc', 'pks', 't',
  // not used in actions, but can be useful: common menus:
  // top-level menus: File, Format, Text, Paragraph, Insert, Table, Window, Help
  '&Failas', '&Redaguoti', 'F&ormatas', '�r&iftas', '&Pastraipa', '�&terpti', '&Lentel�', 'L&angas', 'Pa&galba',
  // exit
  'I�&eiti',
  // top-level menus: View, Tools,
  '&Rodyti', '�ra&nkiai',
  // menus: Font size, Font style, Table select, Table cell align, table cell borders
  'Dydis', 'Stilius', 'Pa�y&m�ti', '&Lygiuoti L�stel�s Turin�', 'L�stel�s R�m�liai',
  // menus: Table cell rotation
  'L�stel�s &Sukimas',
  // menus: Text flow, Footnotes/endnotes
  '&Teksto Srautas', '&Pastabos/Galin�s I�na�os',
  // ribbon tabs: tab1, tab2, view, table
  '&Prad�ia', '&Papildomi', '&Vaizdas', '&Lentel�',
  // ribbon groups: clipboard, font, paragraph, list, editing
  'I�karpin�', '�riftas', 'Pastraipa', 'S�ra�ai', 'Redagavimas',
  // ribbon groups: insert, background, page setup,
  '�terpimas', 'Pagrindas', 'Lapo nuotatos',
  // ribbon groups: links, footnotes, document views, show/hide, zoom, options
  'Saitai', 'I�na�os', 'Dokumento vaizdas', 'Rodyti/Sl�pti', 'M�stelis',  'Nustatymai',
  // ribbon groups on table tab: insert, delete, operations, borders
  '�terpimas', 'Trynimas', 'Operacijos', 'Kra�tin�s',
  // ribbon groups: styles 
  'Stiliai',
  // ribbon screen tip footer,
  'Spauskite F1 i�kviesti pagalbos lang�',
  // ribbon app menu: recent files, save-as menu title, print menu title
  'Paskutiniai dokumentai', 'I�saugoti dokumento kopij�', 'Per�i�r�ti ir spausdinti dokument�',
  // ribbon label: units combo
  'Vienetai:',
  // actions -------------------------------------------------------------------
  // TrvActionNew
  '&Naujas', 'Naujas|Sukuriamas naujas tu��ias dokumentas',
  // TrvActionOpen
  '&Atverti...', 'Atverti|Atveriamas dokumentas i� disko',
  // TrvActionSave
  '&Saugoti', 'Saugoti|Saugojamas dokumentas � disk�',
  // TrvActionSaveAs
  'Saugoti &Kaip...', 'Saugoti Kaip...|Saugojamas dokumentas � disk� nauju vardu',
  // TrvActionExport
  '&Eksportuoti...', 'Eksportuoti|Eksportuojamas dokumentas kitame formate',
  // TrvActionPrintPreview
  'Spausdinimo &Per�i�ra', 'Spausdinimo Per�i�ra|Parodoma kaip bus i�spausdintas dokumentas',
  // TrvActionPrint
  '&Spausdinti...', 'Spausdinti|Derinamas spausdintuvas ir spausdinamas dokumentas',
  // TrvActionQuickPrint
  '&Spausdinti', 'Spausdinti i�&kart', 'Spausdinti|Spausdinamas dokumentas',
  // TrvActionPageSetup
  '&Lapo Nustatymai...', 'Lapo Nustatymai|Nustatomos ribos, lapo dydis, orientacija, �altinis, antra�t� ir i�na�os',
  // TrvActionCut
  'I�&kirpti', 'I�kirpti|I�kerpama pa�ym�ta dalis ir �simenama i�karp� atmintin�je',
  // TrvActionCopy
  '�&siminti', '�siminti|�simenama pa�ym�ta dalis ir padedama � i�karp� atmintin�',
  // TrvActionPaste
  '&Terpti', 'Terpti|� dokument� �terpiamas i�karp� atmintin�s turinys',
  // TrvActionPasteAsText
  'Terpti kaip T&ekst�', 'Terpti kaip tekst�|�terpiamas tekstas i� i�karpin�s',
  // TrvActionPasteSpecial
  'Te&rpti Ypatingai...', 'Terpti Ypatingai|� dokument� �terpiamas i�karp� atmintin�s turinys nustatytame formate',
  // TrvActionSelectAll
  'Pa�ym�ti &Vis�', 'Pa�ym�ti Vis�|Pa�ymimas visas dokumentas',
  // TrvActionUndo
  '&At�aukti', 'At�aukti|Ignoruojamas, at�aukiamas paskutinis veiksmas',
  // TrvActionRedo
  '&Pakartoti', 'Pakartoti|Kartojamas paskutinis at�auktas veiksmas',
  // TrvActionFind
  '&Rasti...', 'Rasti|Dokumente ie�koma konkretaus teksto',
  // TrvActionFindNext
  'Rasti &Toliau', 'Rasti Toliau|T�siama paskutin� paie�ka',
  // TrvActionReplace
  'S&ukeisti...', 'Sukeisti|Randamas ir pakei�iamas nurodytas dokumento tekstas',
  // TrvActionInsertFile
  '&Failas...', '�terpti fail�|� dokument� �terpiamas failo turinys',
  // TrvActionInsertPicture
  '&Paveikslas...', '�terpti paveiksl�|�terpiamas paveikslas i� disko',
  // TRVActionInsertHLine
  '&Horizontali Linija', '�terpti Horizontali� Linij�|�terpiama horizontali linija',
  // TRVActionInsertHyperlink
  '�&terpti Sait�...', '�terpti Sait�|�terpiamas saitas (hipertekstin� nuoroda)',
  // TRVActionRemoveHyperlinks
  '&Naikinti Saitus', 'Naikinti Saitus|Panaikinami visi saitai pa�ym�tame tekste',
  // TrvActionInsertSymbol
  '�terpti �&enkl�...', '�terpti �enkl�|�terpiamas �enklas',
  // TrvActionInsertNumber
  '�terpti &Skai�i�...', '�terpti Skai�i�|�terpiamas skai�ius (skai�iavimas)',
  // TrvActionInsertCaption
  '�terpti &Antra�t�...', '�terpti Antra�t�|�terpiama antra�t� pasirinktam objektui',
  // TrvActionInsertTextBox
  '&Teksto Laukelis', 'Teksto Laukelis|�terpiama tekstin� �ym� laukelyje',
  // TrvActionInsertSidenote
  '�terpti �&inut�', '�terpti �inut�|�terpti �inut� teksto laukelyje',
  // TrvActionInsertPageNumber
  '&Puslapio numeris', 'Puslapio numeris|�terpiamas puslapio numeris',
  // TrvActionParaList
  '�ym�jimas ir N&umeravimas...', '�ym�jimas ir Numeravimas|Nustatomi arba redaguojami �ym�jimas ir numeravimas pa�ym�toms pastraipoms',
  // TrvActionParaBullets
  '�y&m�jimas', '�ym�jimas|�dedama arba naikinama pastraipos �ym�jimas',
  // TrvActionParaNumbering
  '&Numeravimas', 'Numeravimas|�dedamas arba naikinamas pastraipos numeravimas',
  // TrvActionColor
  'Pagrindo &Spalva...', 'Pagrindo Spalva|Kei�iama dokumento pagrindo spalva',
  // TrvActionFillColor
  'U�pil&do spalva...', 'U�pildo Spalva|Kei�iama teksto, pastraipos, l�steli�, lentel�s ar pastraipos pagrindo spalva',
  // TrvActionInsertPageBreak
  '�&terpti Lapo Perskyrim�', '�terpti Lapo Perskyrim�|Perskiriamas lapas � du atskirus lapus',
  // TrvActionRemovePageBreak
  'Pa&naikinti Lapo Perskyrim�', 'Panaikitni Lapo Perskyrim�|Panaikinamas dviej� lap� i�skyrimas',
  // TrvActionClearLeft
  'Panaikinti Teksto Sraut� &Kair�je Pus�je', 'Panaikinti Teksto Sraut� Kair�je Pus�je|�ios pastraipos pozicija bus �emiau bet kurio paveiksliuko, esan�io kair�je pus�je',
  // TrvActionClearRight
  'Panaikinti Teksto Sraut� &De�in�je Pus�je', 'Panaikinti Teksto Sraut� De�in�je Pus�je|�ios pastraipos pozicija bus �emiau bet kurio paveiksliuko, esan�io de�in�je pus�je',
  // TrvActionClearBoth
  'Panaikinti Teksto Sraut� &Abiejose Pus�se', 'Panaikinti Teksto Sraut� Abiejose Pus�se|�ios pastraipos pozicija bus �emiau bet kurio paveiksliuko, esan�io de�in�je ar kair�je pus�je',
  // TrvActionClearNone
  '&Normalus Teksto Srautas', 'Normalus Teksto Srautas|Leid�iama ra�yti tekst� aplink de�in�je ir kair�je pus�je esan�ius paveiksliukus',
  // TrvActionVAlign
  '&Objekto Pozicija...', 'Objekto Pozicija|Kei�iama pasirinkto objekto pozicija',
  // TrvActionItemProperties
  'Objekto Sa&vyb�s...', 'Objekto Savyb�s|Nustatomos aktyvaus objekto savyb�s',
  // TrvActionBackground
  'Pa&grindo Fonas...', 'Pagrindo fonas|Parenkama pagrindo spalva ir fono paveikslas',
  // TrvActionParagraph
  '&Pastraipa...', 'Pastraipa|Kei�iami pastraipos parametrai',
  // TrvActionIndentInc
  'Pa&didinti �trauk�', 'Padidinti �trauk�|Pastraipa traukiama toliau nuo kairio kra�to',
  // TrvActionIndentDec
  'Su&ma�inti �trauk�', 'Suma�inti �trauk�|Pastraipa traukiama ar�iau kairio kra�to',
  // TrvActionWordWrap
  '�&od�i� K�limas', '�od�i� K�limas|Perjungiama pa�ym�t� pastraip� �od�i� k�limas',
  // TrvActionAlignLeft
  'Lygiuoti Pagal &Kair�', 'Lygiuoti Pagal Kair�|Lygiuoja pa�ym�t� tekst� pagal kair� kra�t�',
  // TrvActionAlignRight
  'Lygiuoti Pagal &De�in�', 'Lygiuoti Pagal De�in�|Lygiuoja pa�ym�t� tekst� pagal de�in� kra�t�',
  // TrvActionAlignCenter
  'Lygiuoti &Centre', 'Lygiuoti Centre|Centruojamas pa�ym�tas tekstas',
  // TrvActionAlignJustify
  'Lygiuoti &Abiem Kra�tais', 'Lygiuoti Abiem Kra�tais|Lygiuoja pa�ym�t� tekst� pagal kair� ir de�in� kra�t� kartu',
  // TrvActionParaColor
  'Pastraipos &Pagrindo Spalva...', 'Pastraipos Pagrindo Spalva|Nustatoma pastraipos pagrindo spalva',
  // TrvActionLineSpacing100
  '&Vienos Eilut�s Tarpai', 'Vienos Eilut�s Tarpai|Tarpai tarp eilu�i� lyg�s vienai eilutei',
  // TrvActionLineSpacing150
  '1.5 Ei&lut�s Tarpai', '1.5 Eilut�s Tarpai|Tarpai tarp eilu�i� lyg�s 1.5 eilut�s',
  // TrvActionLineSpacing200
  'Dvigu&bi Eilu�i� Tarpai', 'Dvigubi Eilu�i� Tarpai|Padvigubinami tarpai tarp eilu�i�',
  // TrvActionParaBorder
  'Pastraip� &R�melis ir Pagrindas...', 'Pastraip� R�melis ir Pagrindas|Nustatomi r�meliai ir pagrindas pa�ym�toms pastraipoms',
  // TrvActionInsertTable
  '�terpti &Lentel�...', '&Lentel�', '�terpti Lentel�|�terpiama nauja lentel�',
  // TrvActionTableInsertRowsAbove
  '�terpti Eilut� &Vir�uje', '�terpti Eilut� Vir�uje|�terpiama nauja eilut� pa�ym�tos l�stel�s vir�uje',
  // TrvActionTableInsertRowsBelow
  '�terpti Eilut� &Apa�ioje', '�terpti Eilut� Apa�ioje|�terpiama nauja eilut� pa�ym�tos l�stel�s apa�ioje',
  // TrvActionTableInsertColLeft
  '�terpti Stulpel� &Kair�je', '�terpti Stulpel� Kair�je|�terpiamas naujas stulpelis pa�ym�t� lasteli� kair�je',
  // TrvActionTableInsertColRight
  '�terpti Stulpel� &De�in�je', '�terpti Stulpel� De�in�je|�terpiamas naujas stulpelis pa�ym�t� l�steli� de�in�je',
  // TrvActionTableDeleteRows
  'Pa�alinti &Eilutes', 'Pa�alinti Eilutes|Pa�alinti i� lentel�s eilutes su pa�ym�tomis l�stel�mis',
  // TrvActionTableDeleteCols
  'Pa�alinti St&ulpelius', 'Pa�alinti Stulpelius|Pa�alinti i� lentel�s stulpelius su pa�ym�tomis l�stel�mis',
  // TrvActionTableDeleteTable
  '&Trinti Lentel�', 'Trinti Lentel�|Pa�alinti vis� lentel�',
  // TrvActionTableMergeCells
  'Su&jungti L�steles', 'Sujungti L�steles|Sujungti pa�ym�tas l�steles � vien�',
  // TrvActionTableSplitCells
  'Per&skelti L�steles...', 'Perskelti L�steles|Perskelti pa�ym�tas l�steles',
  // TrvActionTableSelectTable
  'Pa�ym�ti &Lentel�', 'Pa�ym�ti Lentel�|Pa�ymima visa lentel�',
  // TrvActionTableSelectRows
  'Pa�ym�ti &Eilutes', 'Pa�ym�ti Eilutes|Pa�ymimos lentel�s eilut�s',
  // TrvActionTableSelectCols
  'Pa�ym�ti S&tulpelius', 'Pa�ym�ti Stulpelius|Pa�ymimi lentel�s stulpeliai',
  // TrvActionTableSelectCell
  'Pa�ym�ti L�&stel�', 'Pa�ym�ti L�stel�|Pa�ymimas l�stel�s turinys',
  // TrvActionTableCellVAlignTop
  'Lygiuoti L�stel� Pagal &Vir��', 'Lygiuoti L�stel� Pagal Vir��|Lygiuoti l�stel�s turin� jos vir�uje',
  // TrvActionTableCellVAlignMiddle
  'Lygiuoti L�stel� Pagal Vi&dur�', 'Lygiuoti L�stel� Pagal Vidur�|Lygiuoti l�stel�s turin� jos viduryje',
  // TrvActionTableCellVAlignBottom
  'Lygiuoti L�stel� Pagal &Apa�i�', 'Lygiuoti L�stel� Pagal Apa�i�|Lygiuoti l�stel�s turin� jos apa�ioje',
  // TrvActionTableCellVAlignDefault
  '&Nustatytas Vertikalus L�steli� Lygiavimas', 'Nustatytas Vertikalus L�steli� Lygiavimas|Nustatomas pastovus vertikalus l�steli� lygiavimas',
  // TrvActionTableCellRotationNone
  '&Jokio Sukimo', 'Jokio sukimo|Pasukti l�stel�s turin� per 0�',
  // TrvActionTableCellRotation90
  'Pasukti L�stel� per &90�', 'Pasukti L�stel� per 90�|Pasukti l�stel�s turin� per 90�',
  // TrvActionTableCellRotation180
  'Pasukti L�stel� per &180�', 'Pasukti L�stel� per 180�|Pasukti l�stel�s turin� per 180�',
  // TrvActionTableCellRotation270
  'Pasukti L�stel� per &270�', 'Pasukti L�stel� per 270�|Pasukti l�stel�s turin� per 270�',
  // TrvActionTableProperties
  'Lentel�s &Savyb�s...', 'Lentel�s Savyb�s|Kei�iamos pa�ym�tos lentel�s savyb�s ir nustatymai',
  // TrvActionTableGrid
  'Rodyti &Tinklel�', 'Rodyti Tinklel�|Rodomas arba paslepiamas lentel�s tinklelis',
  // TRVActionTableSplit
  'Pers&kelti Lentel�', 'Perskelti Lentel�|Lentel� perskeliama � dvi lenteles pradedant nuo pa�ym�tos eilut�s',
  // TRVActionTableToText
  'K&onvertuoti � Tekst�...', 'Konvertuoti � Tekst�|Lentel� konvertuojama � tekst�',
  // TRVActionTableSort
  '&Rikiuoti...', 'Rikiuoti|Rikiuojamos lentel�s eilut�s',
  // TrvActionTableCellLeftBorder
  'Briauna &Kair�je', 'Briauna Kair�je|Rodoma arba slepiama kairioji l�stel�s briauna',
  // TrvActionTableCellRightBorder
  'Briauna &De�in�je', 'Briauna De�in�je|Rodoma arba slepiam de�inioji l�stel�s briauna',
  // TrvActionTableCellTopBorder
  '&Vir�utin� Briauna', 'Vir�utin� Briauna|Rodoma arba slepiama l�stel�s briauna vir�uje',
  // TrvActionTableCellBottomBorder
  '&Apatin� Briauna', 'Apatin� Briauna|Rodoma arba slepiama l�stel�s briauna apa�ioje',
  // TrvActionTableCellAllBorders
  'V&isos Briaunos', 'Visos Briaunas|Rodomos arba slepiamos l�stel�s briaunos',
  // TrvActionTableCellNoBorders
  '&Be Briaun�', 'Be Briaun�|Slepiamos visos l�stel�s briaunos',
  // TrvActionFonts & TrvActionFontEx
  '�ri&ftas...', '�riftas|Kei�iamas pa�ym�to teksto �riftas',
  // TrvActionFontBold
  'Pa&ry�kintas', 'Pary�kintas|Pa�ym�to teksto stilius kei�iamas � pary�kint� (pastorint�)',
  // TrvActionFontItalic
  '&Kursyvas', 'Kursyvas|Pa�ym�to teksto stilius kei�iamas � pakreipt�',
  // TrvActionFontUnderline
  'Pa&brauktas', 'Pabrauktas|Pa�ym�to teksto stilius kei�iamas � pabraukt�',
  // TrvActionFontStrikeout
  '&Perbrauktas', 'Perbrauktas|Perbraukiamas pa�ym�tas tekstas',
  // TrvActionFontGrow
  '&Didinti �rift�', 'Didinti �rift�|Pa�ym�to teksto raid�i� auk�tis didinamas 10%',
  // TrvActionFontShrink
  '&Ma�inti �rift�', 'Ma�inti �rift�|Pa�ym�to teksto raid�i� auk�tis ma�inamas 10%',
  // TrvActionFontGrowOnePoint
  'D&idinti �rift� vienu punktu', 'Didinti �rift� vienu punktu|Pa�ym�to teksto raid�i� auk�tis didinamas vienu punktu',
  // TrvActionFontShrinkOnePoint
  'M&a�inti �rift� vienu punktu', 'Ma�inti �rift� vienu punktu|Pa�ym�to teksto raid�i� auk�tis ma�inamas vienu punktu',
  // TrvActionFontAllCaps
  '&Visos Did�iosios', 'Visos Did�iosios|Pa�ym�to teksto raid�s kei�iamos � did�i�sias',
  // TrvActionFontOverline
  '&U�braukimas', 'U�braukimas|Pa�ym�to teksto vir�uje pie�iama linija',
  // TrvActionFontColor
  '&Teksto Spalva...', 'Teksto Spalva|Kei�iama pa�ym�to teksto spalva',
  // TrvActionFontBackColor
  'Teksto Pa&grindo Spalva...', 'Teksto Pagrindo Spalva|Kei�iama pa�ym�to teksto pagrindo spalva',
  // TrvActionAddictSpell3
  'Patikrinti &Ra�yb�', 'Patikrinti Ra�yb�|Tikrinama ra�yba tekste',
  // TrvActionAddictThesaurus3
  '&Sinonim� �odynas', 'Sinonim� �odynas|Pasi�lomi sinonimai pa�ym�tame tekste',
  // TrvActionParaLTR
  'I� Kair�s � De�in�', 'I� Kair�s � De�in�|Pa�ym�t� pastraip� tekstui nustatoma Kair�s-De�in�s kryptis',
  // TrvActionParaRTL
  'I� De�in�s � Kair�', 'I� De�in�s � Kair�|Pa�ym�t� pastraip� tekstui nustatoma De�in�s-Kair�s kryptis',
  // TrvActionLTR
  'I� Kair�s � De�in� Tekste', 'Tekstas i� Kair�s � De�in�|Pa�ym�tam tekstui nustatoma Kair�s-De�in�s kryptis',
  // TrvActionRTL
  'I� De�in�s � Kair� Tekste', 'Tekstas i� De�in�s � Kair�|Pa�ym�tam tekstui nustatoma De�in�s-Kair�s kryptis',
  // TrvActionCharCase
  'Ra�yba ABC/abc/Abc', 'Keisti Ra�yb� ABC/abc/Abc|Kei�iama pa�ym�to teksto simboli� ra�yba ABC/abc/Abc',
  // TrvActionShowSpecialCharacters
  '&Nespausdintini Simboliai', 'Nespausdintini Simboliai|Rodomi arba slepiami nespausdintini simboliai, tokie kaip pastraip� �ym�s, tabuliatoriai ir tarpai',
  // TrvActionSubscript
  'Ap&atinis indeksas', 'Indeksas|Pa�ym�tas tekstas kei�iamas � apatin� indeks�',
  // TrvActionSuperscript
  '&Vir�utinis indeksas', 'Indeksas|Pa�ym�tas tekstas kei�iamas � vir�utin� indeks�',
  // TrvActionInsertFootnote
  '&Pastabos', 'Pastabos|�ra�yti pastab�',
  // TrvActionInsertEndnote
  'Galin�s &I�na�os', 'Galin�s I�na�os|�ra�yti galines i�na�as',
  // TrvActionEditNote
  'Re&daguoti Pastabas', 'Redaguoti Pastabas|Kei�iamos pastabos ir i�na�os',
  '&Sl�pti', 'Sl�pti|Slepiamas ar rodomas pasirinktas fragmentas',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // TrvActionStyleTemplates
  '&Stiliai...', 'Stiliai|Atveriamas stili� valdymo langas',
  // TrvActionAddStyleTemplate
  'Pri&d�ti Stili�...', 'Prid�ti Stili�|Sukuriamas naujas pastraipos ir teksto stilius',
  // TrvActionClearFormat,
  'I�&valyti Formatavim�', 'I�valyti Formatavim�|I�valomi visi teksto ir pastraipos formatavimai pa�ym�tame fragmente',
  // TrvActionClearTextFormat,
  'I�valyti &Teksto Formatavim�', 'I�valyti Teksto Formatavim�|I�valomi visi formatavimai i� pa�ym�to teksto',
  // TrvActionStyleInspector
  'Stiliaus T&ikrintojas', 'Stiliaus Tikrintojas|Rodomas ar slepiamas Stiliaus Tikrintojas',
  {$ENDIF}
  // button captions: OK, Cancel, Close, Insert, Open, Save, Clear, Help, Remove -------
   'Gerai', 'At�aukti', 'U�daryti', '�terpti', '&Atverti...', '&Saugoti...', '&Valyti', '&Pagalba', 'Pa&naikinti',
  // Others  -------------------------------------------------------------------
  // percents
  'procentai',
  // left, top, right, bottom sides
  'Kair� Pus�', 'Vir�utin� Pus�', 'De�in� Pus�', 'Apatin� Pus�',
  // save changes? confirm title
  'I�saugoti keitimus � %s?', 'Patvirtinti',
  // warning: losing formatting
  '%s gali tur�ti savybi� kurios yra nesuderinamos su pasirinktu formatu.'#13+
  'Ar j�s norite i�saugoti dokument� �iame formate?',
  // RVF format name
  'RichView Formatas',
  // Error messages ------------------------------------------------------------
  'Klaida',
  'Klaida atveriant fail�.'#13#13'Galimos prie�astys:'#13'- failo formatas yra nesuprantamas �iai programai;'#13+
  '- failas yra sugadintas;'#13'- failas jau yra atvertas ir u�rakintas kitoje programoje.',
  'Klaida atveriant paveikslo fail�.'#13#13'Galimos prie�astys:'#13'- paveikslo failo formatas yra nesuprantamas �iai programai;'#13+
  '- faile n�ra paveikslo;'#13+
  '- failas yra sugadintas;'#13'- failas jau yra atvertas ir u�rakintas kitoje programoje.',
  'Klaida saugant fail�.'#13#13'Galimos prie�astys:'#13'- n�ra vietos diske;'#13+
  '- diskas yra tik skaitymui;'#13'- n�ra �d�tos informacijos laikmenos;'#13+
  '- failas jau yra atvertas ir u�rakintas kitoje programoje;'#13'- informacijos laikmena yra sugadinta.',
  // File filters --------------------------------------------------------------
  // RVF, RTF, XML (for RichViewXML)
  'RichView Failai (*.rvf)|*.rvf',  'RTF Failai (*.rtf)|*.rtf' , 'XML Failai (*.xml)|*.xml',
  // Text - ansi, unicode, auto
  'Teksto Failai (*.txt)|*.txt', 'Teksto Failai - Unicode (*.txt)|*.txt', 'Teksto Failai - Automatinis nustatymas (*.txt)|*.txt',
  // open html (RVHtmlImporter), save HTML with CSS, save HTML without CSS
  'HTML Failai (*.htm;*.html)|*.htm;*.html', 'HTML (*.htm;*.html)|*.htm;*.html',
  'HTML - Supaprastintas (*.htm;*.html)|*.htm;*.html',
  // DocX
  'Microsoft Word Dokumentai (*.docx)|*.docx',
  // Search and replace --------------------------------------------------------
  // Search complete; String not found
  'Paie�ka Baigta', 'Ie�koma eilut� ''%s'' nerasta.',
  // 1 string replaced; Several strings replaced
  '1 eilut� sukeista.', '%d eilu�i� sukeista.',
  // continue search from the beginning/end?
  'Dokumento pabaiga pasiekta, t�sti nuo dokumento prad�ios?',
  'Dokumento prad�ia pasiekta, t�sti nuo dokumento pabaigos?',
  // Colors -------------------------------------------------------------------
  // Transparent, Auto
  'Permatoma', 'Nustatytoji',
  // Black, Brown, Olive Green, Dark Green, Dark Teal, Dark blue, Indigo, Gray-80%
  'Juoda', 'Ruda', 'Rusvai �alia', 'Tamsiai �alia', 'Tamsiai �alsva-M�lyna', 'Tamsiai M�lyna', 'Indigo', 'Pilka-80%',
  // Dark Red, Orange, Dark Yellow, Green, Teal, Blue, Blue-Gray, Gray-50%
  'Tamsiai Raudona', 'Oran�in�', 'Tamsiai Geltona', '�alia', '�alsva-M�lyna', 'M�lyna', 'Pilkai-M�lyna', 'Pilka-50%',
  // Red, Light Orange, Lime, Sea Green, Aqua, Light Blue, Violet, Grey-40%
  'Raudona', 'Lengvai Oran�in�', 'Gelsva', 'J�rin� �alia', 'Akva', 'Lengvai M�lyna', 'Violetin�', 'Pilka-40%',
  // Pink, Gold, Yellow, Bright Green, Turquoise, Sky Blue, Plum, Gray-25%
  'Ro�in�', 'Auksin�', 'Geltona', '�viesiai �alia', 'Turkio', 'Dangaus', 'Tamsiai Violetin�', 'Pilka-25%',
  // Rose, Tan, Light Yellow, Light Green, Light Turquoise, Pale Blue, Lavender, White
  'Ro�i�', 'Gelsvai Ruda', '�viesiai Geltona', 'Lengvai �alia', 'Lengvai Melsva', 'Bly�kiai M�lyna', '�viesiai Violetin�', 'Balta',
  // color picker buttons: Transparent, Auto, More Colors, Default
  // (correspond to color names, but have hotkeys)
  'Per&matoma', '&Automatin�', '&Daugiau Spalv�...', '&Nustatyta',
  // Background Form -----------------------------------------------------------
  // Title, Color label, Position group-box, Background group-box, Sample text
  'Pagrindo fonas', '&Spalva:', 'Pozicija', 'Pagrindo vaizdas', 'Pavyzdys.',
  //positions: None, Full Window, Fixed Tiles, Tiles, Center,
  '&Jokia', 'I�&pl�sta', '&Fiksuota vieta', '&I�klotine', '&Centre',
  // Padding button
  '&Poslinkis...',
  // Fill Color Form -----------------------------------------------------------
  // Title, Apply to, More Colors button, Padding button, Please select color
  'U�pildo Spalva', 'Kam &aktyvuoti:', '&Daugiau Spalv�...', '&Poslinkis...',
  'Pasirinkite spalv�',
  // [apply to:] text, paragraph, table, cell
  'tekstui', 'pastraipai' , 'lentelei', 'l�stelei',
  // Font Form -----------------------------------------------------------------
  // Title, Font tab, Layout tab
  '�riftas', '�riftas', 'I�d�stymas',
  // Font Name, Font Size labels, Font Style group-box, Bold, Italic check-boxes
  '�&riftas:', '&Dydis', '&Stilius', 'St&orintas', '&Kursyvas',
  // Script, Color, Back color labels, Default charset
  '&Indeksas:', '&Spalva:', '&Fonas:', '(Nustatytas)',
  // Effects group-box, Underline, Overline, Strikethrough, All Caps
  'Efektai', '&Pabraukimas', '&U�brauktas', 'Per&brauktas', '&Visos Did�iosios',
  // Sample, Sample text
  'Pavyzdys', 'Pavyzdinis tekstas',
  // Spacing group-box, Spacing label, Expanded, Condensed,
  'Tarpai tarp raid�i�', '&Tarpai:', '&I�pl�stas', '&Sutrauktas',
  // Offset group-box, Offset label, Down, Up,
  'Poslinkis', '&Poslinkis:', '�&emyn', '&Vir�un',
  // Scaling group-box, Scaling
  'M�stelis', 'P&roporcijos:',
  // Sub/super script group box, Normal, Sub, Super
  'Apatinis ir vir�utinis indeksai', '&Normalus', 'Ap&atinis', 'V&ir�utinis',
  // 4 Sides Form --------------------------------------------------------------
  // Default Title; Top, Left, Bottom, Right, Equal values check-box
  'Poslinkis', '&Vir�us:', '&Kair�:', '&Apa�ia:', '&De�in�:', 'V&ienodi Dyd�iai',
  // Hyperlink Form ------------------------------------------------------------
  // Title, Group-box Title, Text, Target, <<Selected Fragment>>
  '�terpti Hipernuorod�', 'Hipernuoroda', 'Tekstas:', 'Nuoroda:', '<<pasirinkimas>>',
  // cannot open URL
  'Negalime susisiekti su "%s"',
  // hyperlink properties button, hyperlink style
  '&Derinti...', '&Stilius:',
  // Hyperlink Properties Form -------------------------------------------------
  // Title, Group-boxes: Normal colors, Active (highlight) �olors
  'Hipernuorodos Savyb�s', 'Normalios spalvos', 'Aktyvios spalvos',
  // Text [color], Background [color]
  '&Teksto:', '&Fono',
  // Underline [color]
  'Pa&braukimas:',
  // Text [color], Background [color] (different hotkeys)
  'T&eksto:', 'F&ono',
  // Underline [color], Attributes group-box,
  'Pa&braukimas:', 'Po�ymiai',
  // Underline type: always, never, active (below the mouse)
  '&Pabraukimas:', 'visada', 'niekada', 'aktyvintas',
  // Same as normal check-box
  'Tokia kaip &normali',
  // underline active links check-box
  'Pa&braukti aktyvias nuorodas',
  // Insert Symbol Form --------------------------------------------------------
  // Title, Font name, Character set, Unicode
  '�terpti �enkl�', '�&riftas:', '�&enkl� s�ra�as:', 'Unicode',
  // Unicode block
  '&Blokas:',
  // Character Code, Character Unicode Code, No Character
  '�enklo kodas: %d', '�enklo kodas: Unicode %d', '(jokio �enklo)',
  // Insert Table Form ---------------------------------------------------------
  // Title, Table size group-box, nCols, nRows,
  '�terpti Lentel�', 'Lentel�s dydis', '&Stulpeli� skai�ius:', '&Eilu�i� skai�ius:',
  // Table layout group-box, Autosize, Fit, Manual size,
  'Lentel�s planas', '&Autopl�timasis', 'Sutapdinti su &lango dyd�iu', 'Nustatyti matmenis',
  // Remember check-box
  '�siminti &matmenis naujoms lentel�ms',
  // VAlign Form ---------------------------------------------------------------
  'Pozicija',
  // Item Properties Form ------------------------------------------------------
  // Title; Image, Position and size, Line, Table, Rows, Cells tabs
  'Savyb�s', 'Paveikslas', 'Vieta ir Dydis', 'Linija', 'Lentel�', 'Eilut�s', 'L�stel�s',
  // Image Appearance, Image Misc, Number tabs
  'I�vaizda', '�vair�s', 'Skai�ius',
  // Box position, Box size, Box appearance tabs
  'Vieta', 'Dydis', 'I�vaizda',
  // Preview label, Transparency group-box, checkbox, label
  'Per�i�ra:', 'Permatomumas', '&Permatoma', 'P&ermatoma spalva:',
  // change button, save button, no transparent color (use color right-bottom pixel)
  'Pa&keisti...', 'I�&saugoti...', 'Autom.',
  // VAlign group-box (used 3 times) and label, bottom-baseline, middle-baseline, top, bottom, middle
  'Vertikalus lygiavimas', 'L&ygiuoti:',
  'apa�ioje pagal teksto linij�', 'viduryje pagal teksto linij�',
  'vir�uje pagal eilut�s vir��', 'apa�ioje pagal eilut�s apa�i�', 'viduryje pagal eilut�s vidur�',
  // align to left side, align to right side
  'kair�je pus�je', 'de�in�je pus�je',
  // Shift By label, Stretch group box, Width, Height, Default size
  'P&astumti per:', 'I�tempti', 'Pl&otis:', '&Auk�tis:', 'Nustatytas dydis: %d x %d',
  // Scale proportionally checkbox, Reset ("no stretch") button
  'I�didinti &proporcingai', 'P&radin� b�sena',
  // Inside the border, border, outside the border groupboxes
  'Viduje kra�t�', 'R�melis', 'I�or�je kra�t�',
  // Fill color, Padding (i.e. margins inside border) labels
  '&U�pildo spalva:', '&Poslinkis:',
  // Border width, Border color labels
  'R�melio &storis:', 'R�melio s&palva:',
  // Horizontal and vertical spacing (i.e. margins outside border) labels
  '&Horizontalus tarpelis:', '&Vertikalus tarpelis:',
  // Miscellaneous groupbox, Tooltip label
  '�vair�s', '&Patarimas:',
  // web group-box, alt text
  'Tinklas', 'Pagalbinis &tekstas:',
  // Horz line group-box, color, width, style
  'Horizontali linija', '&Spalva:', '&Plotis:', 'S&tilius:',
  // Table group-box; Table Width, Color, CellSpacing,
  'Lentel�', '&Plotis:', 'U�pil&do spalva:', '&Tarpai tarp l�steli�...',
  // Horizontal cellpadding, Vertical cellpadding, Border&Background groupbox
  '&Horizontalus l�stel�s poslinkis:', '&Vertikalus l�stel�s poslinkis:', 'Kra�tin�s ir fonas',
  // Visible table border sides button, auto width (in combobox), auto width label
  'Matomos &kra�tin�s...', 'Autom.', 'autom.',
  // Keep row content together checkbox
  'I�&laikyti turin� vientis�',
  // Rotation groupbox, rotations: 0, 90, 180, 270
  'Pasukimas', '&Jokio', '&90�', '&180�', '&270�',
  // Cell border label, Visible cell border sides button,
  'Kra�tin�:', 'Matomos &kra�tin�s...',
  // Border, CellBorders buttons
  '&Lentel�s R�melis...', 'L�steli� &Briaunos...',
  // Table border, Cell borders dialog titles
  'Lentel�s R�melis', 'Nustatytos L�steli� Briaunos',
  // Table Printing group-box, KeepOnPage checkbox, HeadingRows, HeadingRowsTip labels
  'Spausdinimas', 'Talpinti Lentel� Viename La&pe', '&Antra�tini� Eilu�i� skai�ius',
  'Antra�tin�s eilut�s kartojamos kiekviename lentel�s lape',
  // top, center, bottom, default
  '&Vir�us', '&Centras', '&Apa�ia', '&Nustatyta',
  // Cell Settings group-box, BestWidth, BestHeight, FillColor
  'Nustatymai', 'Pageidaujamas &plotis:', '&Auk�tis ma�iausiai:', 'U�pil&do spalva:',
  // Cell Border group-box,, VisibleSides; shadow, light, border colors
  'Briauna', 'Matomos briaunos:',  '�e��lio &spalva:', '&Lengva spalva', '&Spalva:',
  // Background image
  '&Fono Paveikslas...',
  // Horizontal position, Vertical position, Position in text groupboxes and listitems
  'Horizontali pad�tis', 'Vertikali pad�tis', 'Vieta tekste',
  // Position types: align, absolute, relative
  'Lygiuota', 'Absoliuti pad�tis', 'Santykin� pad�tis',
  // [Align] left side, center, right side
  'kair�je lauko pus�je kair�je',
  'centruota lauke centre',
  'de�in�je lauko pus�je de�in�je',
  // [Align] top side, center, bottom side
  'vir�uje lauko vir�uje',
  'centruota lauke centre',
  'apa�ioje lauko apa�ioje',
  // [Align] relative to
  'santykinai su',
  // [Position] to the right of the left side of
  'i� de�in�s kair�je kra�to pus�je',
  // [Position] below the top side of
  '�emiau vir�utinio kra�to',
  // Anchors: page, main text area (x2)
  '&Lapas', '&Pagrindin� teksto sritis', 'L&apas', 'Pagrindin� te&ksto sritis',
  // Anchors: left margin, right margin
  '&Kairin� riba', '&De�inioji riba',
  // Anchors: top margin, bottom margin, inner margin, outer margin
  '&Vir�utin� riba', '&Apatin� riba', 'V&idin� riba', 'I�&orin� riba',
  // Anchors: character, line, paragraph
  '�&enklas', 'L&inija', 'Pa&straipa',
  // Mirrored margins checkbox, layout in table cell checkbox
  'Veid&rodin�s ribos', 'I�&d�stymas lentel�s l�stel�je',
  // Above text, below text
  '&Vir�uje teksto', '&Apa�ioje teksto',
  // Width, Height groupboxes, Width, Height labels
  'Plotis', 'Auk�itis', '&Plotis:', '&Auk�tis:',
  // Auto height label, Auto height combobox item
  'Autom.', 'autom.',
  // Vertical alignment groupbox; alignments: top, center, bottom
  'Vertikalus lygiavimas', '&Vir�us', '&Centras', '&Apa�ia',
  // Border and background button and title
  '&R�melis ir splava...', 'Lauko R�melis ir Spalva',
  // Paste Special Form --------------------------------------------------------
  //Title, Label; Formats: RTF, HTML, Text, UnicodeText, BMP, WMF, graphic files
  'Terpti ypatingai', '&�terpti kaip:', '"Rich text" formatas', 'HTML formatas',
  'Tekstas', 'Unicode tekstas', '"Bitmap" paveikslas', '"Metafile" paveikslas',
  'Grafiniai failai', 'Nuoroda',
  // Options group-box, styles
  'Nustatymai', '&Stiliai:',
  // style options: apply target styles, use source styles, ignore styles
  'pritaikyti nurodyto dokumento stilius', 'i�laikyti stilius ir i�vaizd�',
  'i�laikyti i�vaizd�, ignoruoti stilius',
  // List Gallery Form ---------------------------------------------------------
  // Title; Bulleted, Numbered tabs; Bulleted, Numbered group-boxes
  '�ym�jimas ir Numeravimas', '�ym�jimas', 'Numeravimas', '�ym�jim� s�ra�ai', 'Numeruoti s�ra�ai',
  // Customize, Reset, None
  '&Prisitaikyti...', '&Naujai', 'Joks',
  // Numbering: continue, reset to, create new
  't�sti numeravim�', 'nustatyti nauj� numeravim� �', 'sukurti nauj� s�ra��, pradedant nuo',
  // List Gallery 2 Form (HTML) -----------------------------------------------
  // Numbering type
  'Did�iosios Raid�s', 'Did�iosios Rom�ni�kos', 'De�imtainis', 'Ma�osios Raid�s', 'Ma�osios Rom�ni�kos',
  // Bullet type
  'Aspkritimai', 'Ratukai', 'Kvadratukai',
  // Level, Start from, Continue numbering, Numbering group-box
  '&Lygis:', '&Prad�ti nuo:', '&T�sti', 'Numeravimas',
  // Customize List Form -------------------------------------------------------
  // Title, Levels, Level count, List properties, List type,
  'Keisti s�ra�o �ymes', 'Lygiai', '&Skai�ius:', 'S�ra�o savyb�s', '&S�ra�o tipas:',
  // List types: bullet, image
  '�ym�jimas', 'paveikslas', '�terpti Skai�i�|',
  // Number format, Number, Start level from, Font button
  '&Skai�i� formatas:', 'Skai�ius', '&Prad�ti lygio numeravim� nuo:', '&�riftas...',
  // Image button, bullet character, Bullet button,
  '&Paveikslas...', '�ym�s �&enklas', '�&ym�jimas...',
  // Position of list text, bullet, number, image, text
  'Teksto s�ra�e pozicija', '�ym�jimo pozicija', 'Skai�iaus pozicija', 'Paveikslo pozicija', 'Teksto pozicija',
  // at, left indent, first line indent, from left indent
  '&nuo:', '&Kairioji �trauka:', '&Pirmos eilut�s �trauka:', 'nuo kairiosios �traukos',
  // [only] one level preview, preview
  '&Vieno lygio per�i�ra', 'Per�i�ra',
  // Preview text
  'Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text.',
  // marker align: left side of marker is aligned [at ...],
  // right side of marker is aligned [at ...], center of marker is aligned [at ...]
  'lygiuoti kair�je', 'lygiuoti de�in�je', 'centre',
  // level #, this level (for combo-box)
  'Lygis %d', '�is lygis',
  // Marker character dialog title
  'Keisti �ym�jimo �enkl�',
  // Paragraph Border and Backgound Form ---------------------------------------
  // Title; Border, Background tabs
  'Pastraipos R�melis ir Pagrindas', 'R�melis', 'Pagrindas',
  // Settings group-box; Color, Width, Internal width labels; Offsets button
  'Nustatymai', '&Spalva:', '&Plotis:', 'Vidinis p&lotis:', 'P&oslinkis...',
  // Sample, Border type group-boxes;
  'Pavyzdys', 'R�melio tipas',
  // Border types: none, single, double, tripple, thick inside, thick outside
  '&Jokio', '&Viengubas', '&Dvigubas', '&Trigubas', 'Storas vid&uje', 'Storas &i�or�je',
  // Fill color group-box; More colors, padding buttons
  'U�pildo spalva', '&Daugiau spalv�...', '&Poslinkis...',
  // preview text
  'Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text.',
  // title and group-box in Offsets dialog
  'Poslinkis', 'R�melio poslinkis',
  // Paragraph Form ------------------------------------------------------------
  // Form title; Alignment group-box; alignments: left, right, center, justify
  'Pastraipa', 'Lygiavimas', '&Kair�je', '&De�in�je', '&Centre', '&Kra�tuose',
  // Spacing group-box; spacing before, after, between lines, [spacing] by
  'Tarpai', 'P&rie�:', 'P&o:', 'Eilu�i� &tarpai:', 'per:',
  // Indents group-box; indents: left, right, first line
  '�traukos', 'K&airioji:', 'D&e�inioji:', '&Pirmos eilut�s:',
  // indented, hanging
  '�tra&ukta', 'Ka&banti', 'Pavyzdys',
  // line spacing: single, 1.5 lines, double, at least, exactly, multiple
  'Vienos eilut�s', '1.5 eilut�s', 'Dviej� eilu�i�', 'Bent', 'Tiksliai', 'Didesnis',
  // preview text
  'Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text. Text tekstas text tekstas text.',
  // tabs: Indents and spacing, Tabs, Text flow
  '�traukos ir i�d�stymas', 'Tabuliatoriai', 'Teksto srautas',
  // tab stop position label; buttons: set, delete, delete all
  '&Tabuliatoriaus sustojimai:', '&Nustatyti', '&Naikinti', 'Panaikinti &Visus',
  // tab align group; left, right, center aligns,
  'Lygiavimas', '&Kair�je', '&De�in�je', '&Centre',
  // leader radio-group; no leader
  'Punktyras', '(jokio)',
  // tab stops to be deleted, delete none, delete all labels
  'Naikinami tabuliatoriai:', '(jokio)', 'Visi.',
  // Pagination group-box, keep with next, keep lines together
  'Puslapi� numeracija', '&Kartu su tekstu', '&I�laikyti eilutes kartu',
  // Outline level, Body text, Level #
  '&Bendras lygis:', 'Pirminis Tekstas', 'Lygis %d',
  // Preview Form --------------------------------------------------------------
  // Title; page width, full pages; pages label
  'Spausdinimo Per�i�ra', 'Lapo plotis', 'Visas lapas', 'Lapai:',
  // Invalid Scale, [page #] of #
  '�veskite ski�i� nuo 10 iki 500', 'i� %d',
  // Hints on buttons: first, prior, next, last pages
  'Pirmas Lapas', 'Ankstesnis Lapas', 'Sekantis Lapas', 'Paskutinis Lapas',
  // Cell Spacing Form ---------------------------------------------------------
  // Title, groupbox, between cells, from table border to cells
  'Tarpai tarp l�steli�', 'Tarpai', '&Tarp l�steli�', 'Nuo lentel�s r�melio iki l�steli�',
  // vertical, horizontal (x2)
  '&Vertikaliai:', '&Horizontaliai:', 'V&ertikaliai:', 'H&orizontaliai:',
  // Table Borders Form --------------------------------------------------------
  // Title; Settings group-box; Color, light color, shadow color
  'R�meliai', 'Nustatymai', '&Spalva:', '&�viesi spalva:', '�e��lio &spalva:',
  // Width; Border type group-box;
  '&Plotis:', 'R�melio tipas',
  // Border types: none, sunken, raised, flat
  '&Joks', '�d&ub�s', '&I�kil�s', '&Plok��ias',
  // Split Cells Form ----------------------------------------------------------
  // title, group-box
  'Perskelti', 'Perskelti �',
  // split to specified number of rows and cols, split to original cells (unmerge)
  '&nurodyt� kiek� eilu�i� ir stulpeli�', '&Originalias l�steles',
  // number of columns, rows, merge before
  '&Stulpeli� skai�ius:', '&Eilu�i� skai�ius:', '&Sujungti prie� perskeliant',
  // to original cols, rows check-boxes
  'Perskelti � originalius s&tulpelius', 'Perskelti � originalias ei&lutes',
  // Add Rows form -------------------------------------------------------------
  '�terpti Eilutes', 'Eilu�i� &skai�ius:',
  // Page Setup form -----------------------------------------------------------
  // title, Page tab, Header&Footer tab
  'Lapo nustatymai', 'Lapas', 'Antra�t� ir I�na�os',
  // margins group-box, left, right, top, bottom
  'Ribos (millimetrais)', 'Ribos (coliais)', '&Kair�:', '&Vir�us:', '&De�in�:', '&Apa�ia:',
  // mirror margins check-box
  '&Veidrodin�s ribos',
  // orientation group-box, portrait, landscape
  'Orientacija', '&Portretas', 'Pei&za�as',
  // paper group-box, paper size, default paper source
  'Popierius', '&Dydis:', '�alt&inis:',
  // header group-box, print on the first page, font button
  'Antra�t�', '&Tekstas:', '&Antra�t� pirmame lape', '�&riftas...',
  // header alignments: left, center, right
  '&Kair�je', '&Centre', '&De�in�je',
  // the same for footer (different hotkeys)
  'I�na�os', 'Te&kstas:', 'I�na�os pirmame &lape', '�ri&ftas...',
  'K&air�je', 'Ce&ntre', 'D&e�in�je',
  // page numbers group-box, start from
  'Lap� numeravimas', '&Prad�ti nuo:',
  // hint about codes
  'Ypatingos �enkl� kombinacijos:'#13'&&p - lapo numeris; &&P - lap� skai�ius; &&d - dabartin� data; &&t - dabartinis laikas.',
  // Code Page form ------------------------------------------------------------
  // title, label
  'Tekstinio Failo Koduot�', '&Pasirinkite failo kodavim�:',
  // thai, japanese, chinese (simpl), korean
  'Tai', 'Japon�', 'Kin� (Supaprastintas)', 'Kor�jie�i�',
  // chinese (trad), central european, cyrillic, west european
  'Kin� (Tradicinis)', 'Centrin�s ir Ryt� Europos', 'Kirilica', 'Vakar� Europos',
  // greek, turkish, hebrew, arabic
  'Graik�', 'Turk�', 'Hebraj�', 'Arab�',
  // baltic, vietnamese, utf-8, utf-16
  'Balt�', 'Vietnamie�i�', 'Unicode (UTF-8)', 'Unicode (UTF-16)',
  // Style form ----------------------------------------------------------------
  'Vaizdinis Stilius', '&Pasirinkite stili�:',
  // Delimiter form ------------------------------------------------------------
  // title, label
  'Konvertuoti � Tekst�', 'Pasirinkite &skyrikl�:',
  // line break, tab, ';', ','
  'nauja eilut�', 'tabuliacija', 'kabliata�kis', 'kablelis',
  // Table sort form -----------------------------------------------------------
  // error message
  'Lentel� turinti sujungtas eilutes negali b�ti surikiuota',
  // title, main options groupbox
  'Lentel�s Rikiavimas', 'Rikiavimas',
  // sort by column, case sensitive
  '&Rikiavimas pagal stulpel�:', '&Did�i�sias ir ma��sias raides skiriantis',
  // heading row, range or rows
  'Nerikiuoti &vir�utin�s eilut�s', 'Rikiuoti eilutes: nuo %d iki %d',
  // order, ascending, descending
  'Tvarka', '&Did�janti', '&Ma��janti',
  // data type, text, number
  'Tipas', '&Tekstas', '&Skai�ius',
  // Insert number form --------------------------------------------------------
  // title, properties groupbox, counter name, numbering type
  '�terpti Skai�i�', 'Savyb�s', '&Skaitliuko vardas:', '&Numeravimo tipas:',
  // numbering groupbox, continue, start from
  'Numeravimas', '&T�sti', '&Prad�ti nuo:',
  // Insert caption form -------------------------------------------------------
  // title, label, exclude label checkbox
  'Antra�t�', '&Etiket�:', '&Ne�traukti etiket�s � antra�t�',
  // position radiogroup
  'Vieta', '&Vir� pasirinkto objekto', '�&emiau pasirinkto objekto',
  // caption text
  'Antra�t�s &tekstas:',
  // Default sequences ---------------------------------------------------------
  // numbering, figure, table
  'Numeravimas', 'Diagrama', 'Lentel�',
  // visible sides form --------------------------------------------------------
  // Title, Groupbox
  'Matomos r�melio kra�tin�s', 'R�melis',
  // Left, Top, Right, Bottom checkboxes
  '&Kair� kra�tin�', '&Vir�utin� kra�tin�', '&D��in� kra�tin�', '&Apatin� kra�tin�',
  // Ruler ---------------------------------------------------------------------
  // Hints: table column resize, row resize
  'Keisti Lentel�s Stulpelio Dyd�(ilg�)', 'Keisti Lentel�s Eilut�s Dyd�(auk�t�)',
  // Hints on indents: first line, left (together with first), left (without first), right
  'Pirmos Eilut�s �trauka', 'Kairioji �trauka', 'Kabanti �trauka', 'De�inioji �trauka',
  // Hints on lists: up one level (left), down one level (right)
  'Vienu lygiu auk��iau', 'Vienu lygiu �emiau',
  // Hints for margins: bottom, left, right and top
  'Apatin� riba', 'Kairioji riba', 'De�inioji riba', 'Vir�utin� riba',
  // Hints for tabs: left, right, center, decimal (reserved) aligned
  'Kairiojo Lygiavimo Tabuliatorius', 'De�inio Lygiavimo Tabuliatorius', 'Vidurio (Centre) Lygiavimo Tabuliatorius', '(De)�imt�j� Lygiavimo Tabuliatorius',
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  // Standard styles -----------------------------------------------------------
  // Normal, Normal Indent, No Spacing, Heading %d, List Paragraph
  'Normalus', 'Normalus �trauktas', 'Joki� tarp�', 'Antra�t� %d', 'S�ra�o Pastraipa',
  // Hyperlink, Title, Subtitle
  'Nuoroda', 'Pavadinimas', 'Paantra�t�',
  // Emphasis, Subtle Emphasis, Intense Emphasis, Strong
  'I�ry�kinimas', 'Subtilus I�ry�kinimas', 'Intensyvus I�ry�kinimas', 'Stiprus',
  // Quote, Intense Quote, Subtle Reference, Intense Reference
  'Citata', 'Ry�ki Citata', 'Subtilus Pamin�jimas', 'Ry�kus Pamin�jimas',
  // Block Text, HTML Variable, HTML Code, HTML Acronym,
  'Blokinis Tekstas', 'HTML Kintamasis', 'HTML Kodas', 'HTML Akronimas',
  // HTML Definition, HTML Keyboard, HTML Sample, HTML Typewriter,
  'HTML Apibr��imas', 'HTML Klaviat�ra', 'HTML Pavyzdys', 'HTML Ra�omoji-ma�in�l�',
  // HTML Preformatted, HTML Cite, Header, Footer, Page Number
  'HTML Preformatuotas', 'HTML Citavimas', 'Antra�t�', 'Apa�ia', 'Puslapio Numeris',
  // Caption
  'Antra�t�',
  // Endnote Reference, Footnote Reference, Endnote Text, Footnote Text
  'I�na�� Nuorodos', 'Pastab� Nuorodos', 'I�na�� Tekstas', 'Pastab� Tekstas',
  // Sidenote Reference, Sidenote Text
  '�inut�s Nuoroda', '�inut�s Tekstas',
  // Style description ---------------------------------------------------------
  // color, background color, transparent, default [color], underline color,
  'spalva', 'fono spalva', 'permatoma', 'numatytoji', 'pabraukimo spalva',
  // default background color, default text color, [color] same as text
  'numatytoji fono spalva', 'numatytoji teksto spalva', 'tokia pati kaip teksto',
  // underline types: single, thick, double, dotted, thick dotted, dashed
  'viengubas', 'storas', 'dvigubas', 'ta�kuotas', 'storas ta�kuotas', 'br�k�niuotas',
  // underline types: thick dashed, long dashed, thick long dashed,
  'storas br�k�niuotas', 'ilgi br�k�niai', 'stori ilgi br�k�niai',
  // underline types: dash dotted, thick dash dotted,
  'br�k�nys-ta�kas', 'storas br�k�nys-ta�kas',
  // underline types: dash dot dotted, thick dash dot dotted
  'br�k�nys-ta�kas ta�kuotas', 'storas br�k�nys-ta�kas ta�kuotas',
  // sub/superscript: not, subsript, superscript
  'be apat./vir�. indekso', 'apatinis indeksas', 'vir�utinis indeksas',
  // bi-di mode, inherited [bi-di mode], LTR, RTL
  'bi-di b�das:', 'paveld�tas', 'i� kair�s � de�in�', 'i� de�in�s � kair�',
  // bold, not bold
  'storintas', 'nestorintas',
  // italic, not italic
  'kursyvas', 'nebe kursyvas',
  // underlined, not underlined, default underline
  'pabrauktas', 'nepabrauktas', 'numatytasis pabraukimas',
  // struck out, not struck out
  'perbrauktas', 'neperbrauktas',
  // overlined, not overlined
  'u�brauktas', 'neu�brauktas',
  // all capitals: yes, all capitals: no
  'visos did�iosios', 'nebe visos did�iosios',
  // vertical shift: none, by x% up, by x% down
  'be vertikalaus poslinkio', 'paslinkta per %d%% vir�un', 'paslinkta per %d%% apa�ion',
  // characters width [: x%]
  'simboli� plotis',
  // character spacing: none, expanded by x, condensed by x
  'normal�s simboli� tarpai', 'tarpai prapl�sti per %s', 'tarpai sutraukti per %s',
  // charset
  'skriptas',
  // default font, default paragraph, inherited attributes [of font, paragraph attributes, etc.]
  'numatytasis �riftas', 'numatytoji pastraipa', 'paveld�ta',
  // [hyperlink] highlight, default hyperlink attributes
  'pa�ym�ta', 'numatyta',
  // paragraph aligmnment: left, right, center, justify
  'kairysis lygiavimas', 'de�inysis lygiavimas', 'centruota', 'abipus� lygiuot�',
  // line height: x%, space between lines: x, line height: at least x, line height: exactly x
  'eilut�s auk�tis: %d%%', 'tarpas tarp eilu�i�: %s', 'eilut�s auk�tis: bent %s',
  'eilut�s auk�t�s: tiksliai %s',
  // no wrap, wrap
  'eilu�i� k�limas u�drastas', 'eilu�i� k�limas',
  // keep lines together: yes, no
  'i�laikyti eilutes kartu', 'nelaikyti eilu�i� kartu',
  // keep with next: yes, no
  'i�laikyti su sekan�ia pastraipa', 'nelaikyti su sekan�ia pastraipa',
  // read only: yes, no
  'tik skaitymas', 'redaguojamas',
  // body text, heading level x
  'bendras lygis: pirminis tekstas', 'lygis: %d',
  // indents: first line, left, right
  'pirmos eilut�s �trauka', 'kairin� �trauka', 'de�inin� �trauka',
  // space before, after
  'tarpas prie�', 'tarpas po',
  // tabs, no tabs, tab alignment, tab alignment: left, right, center
  'tabuliatoriai', 'joki�', 'vieta', 'kair�', 'de�in�', 'centras',
  // tab leader (filling character)
  'skirtukas',
  // no, yes
  'ne', 'taip',
  // [padding/spacing/side:] left, top, right, bottom
  'kair�', 'vir�us', 'de�in�', 'apa�ia',
  // border: none, single, double, triple, thick inside, thick outside
  'jokio', 'viengubas', 'dvigubas', 'trigubas', 'storas viduje', 'storas i�or�je',
  // background, border, padding, spacing [between text and border]
  'fonas', 'kra�tas', 'atitraukimas', 'tarpas',
  // border: style, width, internal width, visible sides
  'stilius', 'plotis', 'vidinis plotis', 'matomi kra�tai',
  // style inspector -----------------------------------------------------------
  // title
  'Styliaus Tikrintojas',
  // style, no style, paragraph, font, attributes (of font/paragraph)
  'Stilius', '(Jokio)', 'Pastraipa', '�riftas', 'Po�ymiai',
  // border and background, hyperlink, standard [style]
  'Kra�tai ir fonas', 'Nuoroda', '(Standartinis)',
  // Styles Form ---------------------------------------------------------------
  // title, style group-box
  'Stiliai', 'Stilius',
  // name, applicable to,
  '&Vardas:', 'Pri&taikomas:',
  // based on, based on (no &),
  '&Paremtas:', 'Paremtas:',
  //  next style, next style (no &)
  'Se&kan�ios pastraipos stilius:', 'Sekan�ios pastraipos stilius:',
  // quick access check-box, description and preview
  '&Greitoji prieiga', 'Apra�y&mas ir per�i�ra:',
  // [applicable to:] paragraph and text, paragraph, text
  'pastraipa ir tekstas', 'pastraipa', 'tekstas',
  // text and paragraph styles, default font
  'Teksto ir Pastraipos Stiliai', 'Numatytasis �riftas',
  // links: edit, reset, buttons: add, delete
  'Redaguoti', 'Naujai nustatyti', 'Pri&d�ti', 'I�&trinti',
  // add standard style, add custom style, default style name
  'Prid�ti &Standartin� Stili�...', 'Prid�ti &Individul� Stili�', 'Stilius %d',
  // choose style
  'Pasirinkti &stili�:',
  // import, export,
  '&Importuoti...', '&Eksportuoti...',
  // Style Import Form ---------------------------------------------------------
  // RichViewStyles filter, load error, no styles
  'RichView Stiliai (*.rvst)|*.rvst', 'Klaida u�kraunant fail�', '�iame faile n�ra joki� stili�',
  // Title, group-box, import styles
  'Importuoti Stilius i� Failo', 'Importuoti', 'I&mportuoti stilius:',
  // existing styles radio-group: Title, override, auto-rename
  'Esantys stiliai', '&perra�yti', 'pri&d�ti pervardint�',
  // Select, Unselect, Invert,
  'Pa�&ym�ti', 'A&t�ym�ti', '&Invertuoti',
  // select/unselect: all styles, new styles, existing styles
  '&Visi Stiliai', '&Naujas Stilius', '&Esantys Stiliai',
  // Style Combo-box -----------------------------------------------------------
  // clear format, more styles
  'Valyti Formatavim�', 'Visi Stiliai',
  // dialog title, prompt
  'Stiliai', '&Pasirinktas stilius taikymui:',
  {$ENDIF}
  // spelling check and thesaurus ----------------------------------------------
  '&Sinonimai', '&Ignoruoti Visus', '�&ra�yti � �odyn�',
  // Progress messages ---------------------------------------------------------
  'Siun�iama %s', 'Ruo�iamasi spausdinti...', 'Spausdinamas lapas %d',
  'Konvertuojama i� RTF...',  'Konvertuojama � RTF...',
  'Skaitoma %s...', 'Ra�oma %s...', 'tekstas',
  // Edit note -----------------------------------------------------------------
  // Titles: No note, Footnote, Endnote, Sidenote, Text box
  'Joki� pastab�', 'Pastaba', 'I�na�a', '�inut�', 'Teksto laukelis'
  );


initialization
  RVA_RegisterLanguage(
    'Lithuanian', // english language name, do not translate
    'Lietuvi�',   // native language name
    BALTIC_CHARSET, @Messages);

end.
