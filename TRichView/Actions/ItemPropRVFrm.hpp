﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ItemPropRVFrm.pas' rev: 27.00 (Windows)

#ifndef ItemproprvfrmHPP
#define ItemproprvfrmHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <BaseRVFrm.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVSpinEdit.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVFloatingPos.hpp>	// Pascal unit
#include <RVFloatingBox.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <RVColorCombo.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.ExtDlgs.hpp>	// Pascal unit
#include <RVOfficeRadioBtn.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVWinGrIn.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Itemproprvfrm
{
//-- type declarations -------------------------------------------------------
struct DECLSPEC_DRECORD TRVImagePropRecord
{
public:
	int cmbVShiftItemIndex;
	bool LastChangedIsHeight;
	bool SizeInPixels;
	bool MakingProportional;
	Vcl::Graphics::TGraphic* OriginalImage;
	int ImageWidth;
	int ImageHeight;
	System::UnicodeString ImageFileName;
	bool ImageFileNameChanged;
	Rvstyle::TRVStyleLength BorderWidth;
};


struct DECLSPEC_DRECORD TRVTablePropRecord
{
public:
	System::UnicodeString ImageFileName;
	bool ImageFileNameChanged;
	Rvstyle::TRVLength TableWidth;
	Rvstyle::TRVLength CellWidth;
	int TableWidthPrc;
	int CellWidthPrc;
	Rvstyle::TRVStyleLength CellHSpacing;
	Rvstyle::TRVStyleLength CellVSpacing;
	Rvstyle::TRVStyleLength BorderHSpacing;
	Rvstyle::TRVStyleLength BorderVSpacing;
	Rvtable::TRVTableBorderStyle BorderStyle;
	System::Uitypes::TColor BorderColorD;
	System::Uitypes::TColor BorderColorL;
	Rvtable::TRVTableBorderStyle CellBorderStyle;
	System::Uitypes::TColor CellBorderColorD;
	System::Uitypes::TColor CellBorderColorL;
	Rvstyle::TRVStyleLength BorderWidth;
	Rvstyle::TRVStyleLength CellBorderWidth;
	bool FDefCellColorD;
	bool FDefCellColorL;
	Vcl::Graphics::TGraphic* TableBackgroundImage;
	Rvstyle::TRVItemBackgroundStyle TableBackgroundStyle;
	bool TableBackgroundChanged;
	Vcl::Graphics::TGraphic* CellBackgroundImage;
	Rvstyle::TRVItemBackgroundStyle CellBackgroundStyle;
	bool CellBackgroundChanged;
	System::UnicodeString CellImageFileName;
	bool TableVBLeft;
	bool TableVBRight;
	bool TableVBTop;
	bool TableVBBottom;
	bool CellVBLeft;
	bool CellVBRight;
	bool CellVBTop;
	bool CellVBBottom;
	bool CellDefVBLeft;
	bool CellDefVBRight;
	bool CellDefVBTop;
	bool CellDefVBBottom;
};


struct DECLSPEC_DRECORD TRVBreakPropRecord
{
public:
	Rvstyle::TRVStyleLength Width;
};


struct DECLSPEC_DRECORD TRVBoxPropRecord
{
public:
	Rvstyle::TRVLength Height;
	Rvstyle::TRVLength Width;
	Rvstyle::TRVLength HOffs;
	Rvstyle::TRVLength VOffs;
	System::Extended HeightPct;
	System::Extended WidthPct;
	System::Extended HOffsPct;
	System::Extended VOffsPct;
	Rvstyle::TRVBorder* Border;
	Rvstyle::TRVBackgroundRect* Background;
};


struct DECLSPEC_DRECORD TRVSeqPropRecord
{
public:
	System::Classes::TStringList* SeqPropList;
};


class DELPHICLASS TfrmRVItemProp;
class PASCALIMPLEMENTATION TfrmRVItemProp : public Baservfrm::TfrmRVBase
{
	typedef Baservfrm::TfrmRVBase inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* tsImage;
	Vcl::Extctrls::TPanel* Panel1;
	Vcl::Comctrls::TTabSheet* tsImgLayout;
	Vcl::Stdctrls::TGroupBox* gbVAlign;
	Vcl::Stdctrls::TLabel* lblAlignTo;
	Vcl::Stdctrls::TComboBox* cmbAlignTo;
	Vcl::Stdctrls::TLabel* lblVShift;
	Rvspinedit::TRVSpinEdit* seVShift;
	Vcl::Stdctrls::TComboBox* cmbVShiftType;
	Vcl::Stdctrls::TGroupBox* gbSize;
	Rvspinedit::TRVSpinEdit* seWidth;
	Rvspinedit::TRVSpinEdit* seHeight;
	Vcl::Extctrls::TImage* img;
	Vcl::Stdctrls::TButton* btnChangeImage;
	Vcl::Stdctrls::TGroupBox* gbTransp;
	Vcl::Stdctrls::TLabel* lblTrColor;
	Rvcolorcombo::TRVColorCombo* cmbColor;
	Vcl::Stdctrls::TCheckBox* cbTransp;
	Vcl::Stdctrls::TLabel* lblPreview;
	Vcl::Comctrls::TTabSheet* tsBreak;
	Vcl::Stdctrls::TLabel* lblWidth;
	Vcl::Stdctrls::TLabel* lblHeight;
	Vcl::Stdctrls::TLabel* lblDefSize;
	Vcl::Stdctrls::TGroupBox* gbBreak;
	Vcl::Stdctrls::TLabel* lblBreakColor;
	Vcl::Stdctrls::TLabel* lblBreakWidth;
	Rvcolorcombo::TRVColorCombo* cmbBreakColor;
	Vcl::Stdctrls::TComboBox* cmbWidth;
	Vcl::Comctrls::TTabSheet* tsTable;
	Vcl::Stdctrls::TGroupBox* gbTableSize;
	Rvspinedit::TRVSpinEdit* seTableWidth;
	Vcl::Stdctrls::TComboBox* cmbTWType;
	Vcl::Controls::TImageList* ilTable;
	Vcl::Stdctrls::TLabel* lblCellHPadding;
	Rvspinedit::TRVSpinEdit* seCellHPadding;
	Rvspinedit::TRVSpinEdit* seCellVPadding;
	Vcl::Stdctrls::TLabel* lblCellVPadding;
	Vcl::Stdctrls::TButton* btnSpacing;
	Vcl::Comctrls::TTabSheet* tsCells;
	Vcl::Stdctrls::TGroupBox* gbCellBorder;
	Vcl::Stdctrls::TGroupBox* gbCells;
	Rvspinedit::TRVSpinEdit* seCellBestWidth;
	Vcl::Stdctrls::TComboBox* cmbCellBWType;
	Rvspinedit::TRVSpinEdit* seCellBestHeight;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgCellVAlign;
	Vcl::Stdctrls::TLabel* lblTableWidth;
	Vcl::Comctrls::TTabSheet* tsRows;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgRowVAlign;
	Vcl::Stdctrls::TLabel* lblCellBestHeightMU;
	Vcl::Stdctrls::TGroupBox* gbTablePrint;
	Vcl::Stdctrls::TCheckBox* cbNoSplit;
	Vcl::Stdctrls::TLabel* lblHeadingRows;
	Rvspinedit::TRVSpinEdit* seHRC;
	Vcl::Stdctrls::TComboBox* cmbBreakStyle;
	Vcl::Stdctrls::TLabel* lblBreakStyle;
	Vcl::Stdctrls::TLabel* lblCellHPaddingMU;
	Vcl::Stdctrls::TLabel* lblCellVPaddingMU;
	Vcl::Stdctrls::TLabel* lblDefSize2;
	Vcl::Comctrls::TTabSheet* tsBoxPosition;
	Vcl::Stdctrls::TGroupBox* gbBoxHPos;
	Vcl::Stdctrls::TComboBox* cmbBoxHPosition;
	Vcl::Controls::TImageList* ilBoxAnchor;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHPage;
	Rvspinedit::TRVSpinEdit* seBoxHOffs;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHLeftMargin;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHMainTextArea;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHRightMargin;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHCharacter;
	Vcl::Stdctrls::TLabel* lblBoxHTxt;
	Vcl::Stdctrls::TLabel* lblBoxHMU;
	Vcl::Stdctrls::TListBox* lstBoxPosition;
	Vcl::Stdctrls::TComboBox* cmbBoxHAlign;
	Vcl::Stdctrls::TGroupBox* gbBoxVPos;
	Vcl::Stdctrls::TLabel* lblBoxVTxt;
	Vcl::Stdctrls::TLabel* lblBoxVMU;
	Vcl::Stdctrls::TComboBox* cmbBoxVPosition;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxVPage;
	Rvspinedit::TRVSpinEdit* seBoxVOffs;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxVTopMargin;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxVMainTextArea;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxVBottomMargin;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxVLine;
	Vcl::Stdctrls::TComboBox* cmbBoxVAlign;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxVPara;
	Vcl::Controls::TImageList* ilBoxZPos;
	Vcl::Comctrls::TTabSheet* tsBoxSize;
	Vcl::Stdctrls::TGroupBox* gbBoxHeight;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHeightPage;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHeightTopMargin;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHeightMainTextArea;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxHeightBottomMargin;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgBoxZPos;
	Vcl::Stdctrls::TComboBox* cmbBoxHeight;
	Rvspinedit::TRVSpinEdit* seBoxHeight;
	Vcl::Stdctrls::TLabel* lblBoxHeightTxt;
	Vcl::Stdctrls::TGroupBox* gbBoxWidth;
	Vcl::Stdctrls::TLabel* lblBoxWidthTxt;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxWidthPage;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxWidthLeftMargin;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxWidthMainTextArea;
	Rvofficeradiobtn::TRVOfficeRadioButton* rbBoxWidthRightMargin;
	Vcl::Stdctrls::TComboBox* cmbBoxWidth;
	Rvspinedit::TRVSpinEdit* seBoxWidth;
	Vcl::Comctrls::TTabSheet* tsBoxContent;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgBoxVAlign;
	Vcl::Stdctrls::TButton* btnBoxBorder;
	Vcl::Stdctrls::TLabel* lblBoxHeight;
	Vcl::Stdctrls::TLabel* lblBoxWidth;
	Vcl::Stdctrls::TLabel* lblBoxAutoHeight;
	Vcl::Stdctrls::TCheckBox* cbRelativeToCell;
	Vcl::Stdctrls::TCheckBox* cbBoxWidthMirror;
	Vcl::Stdctrls::TCheckBox* cbBoxPosMirror;
	Vcl::Stdctrls::TButton* btnSaveImage;
	Vcl::Extdlgs::TSavePictureDialog* spd;
	Vcl::Comctrls::TTabSheet* tsImgAppearance;
	Vcl::Stdctrls::TGroupBox* gbImgInsideBorder;
	Vcl::Stdctrls::TLabel* lblImgColor;
	Rvcolorcombo::TRVColorCombo* cmbImgFillColor;
	Vcl::Stdctrls::TLabel* lblImgSpacing;
	Rvspinedit::TRVSpinEdit* seImgSpacing;
	Vcl::Stdctrls::TLabel* lblImgSpacingMU;
	Vcl::Stdctrls::TGroupBox* gbImgBorder;
	Vcl::Stdctrls::TLabel* lblImgBorderColor;
	Vcl::Stdctrls::TLabel* lblImgBorderWidth;
	Rvcolorcombo::TRVColorCombo* cmbImgBorderColor;
	Vcl::Stdctrls::TGroupBox* gbImgOutsideBorder;
	Vcl::Stdctrls::TLabel* lblImgOuterVSpacing;
	Vcl::Stdctrls::TLabel* lblImgOuterVSpacingMU;
	Rvspinedit::TRVSpinEdit* seImgOuterVSpacing;
	Vcl::Stdctrls::TLabel* lblImgOuterHSpacing;
	Rvspinedit::TRVSpinEdit* seImgOuterHSpacing;
	Vcl::Stdctrls::TLabel* lblImgOuterHSpacingMU;
	Vcl::Comctrls::TTabSheet* tsImgMisc;
	Rvspinedit::TRVSpinEdit* seWidthPrc;
	Rvspinedit::TRVSpinEdit* seHeightPrc;
	Vcl::Stdctrls::TLabel* Label13;
	Vcl::Stdctrls::TLabel* Label14;
	Vcl::Stdctrls::TComboBox* cmbImgSizeUnits;
	Vcl::Stdctrls::TCheckBox* cbImgScaleProportionally;
	Vcl::Stdctrls::TButton* btnImgSizeReset;
	Vcl::Stdctrls::TGroupBox* gbWeb;
	Vcl::Stdctrls::TLabel* lblAlt;
	Vcl::Stdctrls::TEdit* txtAlt;
	Vcl::Stdctrls::TGroupBox* gbImgMisc;
	Vcl::Stdctrls::TLabel* lblImgHint;
	Vcl::Stdctrls::TEdit* txtImgHint;
	Vcl::Stdctrls::TComboBox* cmbImgBorderWidth;
	Vcl::Stdctrls::TGroupBox* gbTableBackAndBorder;
	Vcl::Stdctrls::TLabel* lblTableColor;
	Rvcolorcombo::TRVColorCombo* cmbTableColor;
	Vcl::Stdctrls::TButton* btnTableBack;
	Vcl::Stdctrls::TButton* btnTableBorder;
	Vcl::Stdctrls::TButton* btnCellBorder;
	Vcl::Stdctrls::TButton* btnTableVisibleBorders;
	Vcl::Stdctrls::TLabel* lblTableDefWidth;
	Vcl::Stdctrls::TButton* btnCellBack;
	Rvofficeradiobtn::TRVOfficeRadioGroup* rgCellRotation;
	Rvcolorcombo::TRVColorCombo* cmbCellColor;
	Vcl::Stdctrls::TLabel* lblCellColor;
	Vcl::Stdctrls::TLabel* lblCellBorderColor;
	Rvcolorcombo::TRVColorCombo* cmbCellBorderColor;
	Vcl::Stdctrls::TLabel* lblCellBorderLightColor;
	Rvcolorcombo::TRVColorCombo* cmbCellBorderLightColor;
	Vcl::Stdctrls::TButton* btnCellVisibleBorders;
	Vcl::Stdctrls::TLabel* lblCellHeight;
	Vcl::Stdctrls::TLabel* lblCellDefWidth;
	Vcl::Stdctrls::TLabel* lblCellWidth;
	Vcl::Stdctrls::TGroupBox* gbRowPrint;
	Vcl::Stdctrls::TCheckBox* cbRowKeepTogether;
	Vcl::Comctrls::TTabSheet* tsSeq;
	Vcl::Stdctrls::TGroupBox* gbSeq;
	Vcl::Stdctrls::TLabel* lblSeqName;
	Vcl::Stdctrls::TLabel* lblSeqType;
	Vcl::Stdctrls::TComboBox* cmbSeqName;
	Vcl::Stdctrls::TComboBox* cmbSeqType;
	Vcl::Stdctrls::TGroupBox* gbSeqNumbering;
	Vcl::Stdctrls::TRadioButton* rbSeqContinue;
	Vcl::Stdctrls::TRadioButton* rbSeqReset;
	Rvspinedit::TRVSpinEdit* seSeqStartFrom;
	Vcl::Stdctrls::TLabel* lblCellBorder;
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall cmbColorColorChange(System::TObject* Sender);
	void __fastcall cmbVShiftTypeClick(System::TObject* Sender);
	void __fastcall btnChangeImageClick(System::TObject* Sender);
	void __fastcall cmbWidthDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall cmbBreakColorColorChange(System::TObject* Sender);
	HIDESBASE void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall seTableWidthChange(System::TObject* Sender);
	void __fastcall btnSpacingClick(System::TObject* Sender);
	void __fastcall btnTableBorderClick(System::TObject* Sender);
	void __fastcall btnCellBordersClick(System::TObject* Sender);
	void __fastcall cmbCellBorderColorColorChange(System::TObject* Sender);
	void __fastcall cmbCellBorderLightColorColorChange(System::TObject* Sender);
	void __fastcall seCellBestWidthChange(System::TObject* Sender);
	void __fastcall btnTopClick(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall btnTableBackClick(System::TObject* Sender);
	void __fastcall btnCellBackClick(System::TObject* Sender);
	void __fastcall cmbBreakStyleDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall cmbWidthClick(System::TObject* Sender);
	void __fastcall cmbTWTypeClick(System::TObject* Sender);
	void __fastcall cmbCellBWTypeClick(System::TObject* Sender);
	void __fastcall cmbAlignToChange(System::TObject* Sender);
	void __fastcall lstBoxPositionClick(System::TObject* Sender);
	void __fastcall cmbBoxHeightClick(System::TObject* Sender);
	void __fastcall cmbBoxWidthClick(System::TObject* Sender);
	void __fastcall seBoxHeightChange(System::TObject* Sender);
	void __fastcall seBoxWidthChange(System::TObject* Sender);
	void __fastcall cmbBoxHPositionClick(System::TObject* Sender);
	void __fastcall cmbBoxVPositionClick(System::TObject* Sender);
	void __fastcall seBoxHOffsChange(System::TObject* Sender);
	void __fastcall seBoxVOffsChange(System::TObject* Sender);
	void __fastcall btnBoxBorderClick(System::TObject* Sender);
	void __fastcall cbBoxWidthMirrorClick(System::TObject* Sender);
	void __fastcall cbBoxPosMirrorClick(System::TObject* Sender);
	void __fastcall btnSaveImageClick(System::TObject* Sender);
	void __fastcall cmbImgSizeUnitsClick(System::TObject* Sender);
	void __fastcall seWidthChange(System::TObject* Sender);
	void __fastcall seHeightChange(System::TObject* Sender);
	void __fastcall seWidthPrcChange(System::TObject* Sender);
	void __fastcall seHeightPrcChange(System::TObject* Sender);
	void __fastcall btnImgSizeResetClick(System::TObject* Sender);
	void __fastcall cbImgScaleProportionallyClick(System::TObject* Sender);
	void __fastcall cmbImgBorderWidthDrawItem(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	void __fastcall cmbImgBorderColorColorChange(System::TObject* Sender);
	void __fastcall btnTableVisibleBordersClick(System::TObject* Sender);
	void __fastcall btnCellVisibleBordersClick(System::TObject* Sender);
	void __fastcall seSeqStartFromChange(System::TObject* Sender);
	void __fastcall cmbSeqNameClick(System::TObject* Sender);
	void __fastcall cmbSeqNameChange(System::TObject* Sender);
	
private:
	bool FUpdatingCombos;
	TRVImagePropRecord ImgProps;
	TRVTablePropRecord TableProps;
	TRVBreakPropRecord BreakProps;
	TRVBoxPropRecord BoxProps;
	TRVSeqPropRecord SeqProps;
	void __fastcall UpdateCellBorders(void);
	void __fastcall UpdateTableBorders(void);
	void __fastcall UpdateLblDefSize(void);
	void __fastcall UpdateImgPrcSize(Rvspinedit::TRVSpinEdit* se, Rvspinedit::TRVSpinEdit* sePrc, int OriginalSize);
	void __fastcall UpdateImgSizeFromPrc(Rvspinedit::TRVSpinEdit* se, Rvspinedit::TRVSpinEdit* sePrc, int OriginalSize);
	void __fastcall UpdateImgSizeProportionally(void);
	void __fastcall StoreImgSize(void);
	
protected:
	Vcl::Controls::TControl* _btnOk;
	Vcl::Controls::TControl* _rbSeqContinue;
	Vcl::Controls::TControl* _rbSeqReset;
	Vcl::Controls::TControl* _cmbSeqName;
	Vcl::Controls::TControl* _cmbSeqType;
	Vcl::Controls::TControl* _gbTransp;
	Vcl::Controls::TControl* _gbSize;
	Vcl::Controls::TControl* _gbCellBorder;
	Vcl::Controls::TControl* _cbTransp;
	Vcl::Controls::TControl* _cbNoSplit;
	Vcl::Controls::TControl* _cbRowKeepTogether;
	Vcl::Controls::TControl* _cbImgScaleProportionally;
	Vcl::Controls::TControl* _cmbAlignTo;
	Vcl::Controls::TControl* _cmbVShiftType;
	Vcl::Controls::TControl* _cmbWidth;
	Vcl::Controls::TControl* _cmbBreakStyle;
	Vcl::Controls::TControl* _cmbImgBorderWidth;
	Vcl::Controls::TControl* _cmbTWType;
	Vcl::Controls::TControl* _cmbCellBWType;
	Vcl::Controls::TControl* _cmbImgSizeUnits;
	Vcl::Controls::TControl* _tsImage;
	Vcl::Controls::TControl* _tsImgLayout;
	Vcl::Controls::TControl* _tsImgAppearance;
	Vcl::Controls::TControl* _tsImgMisc;
	Vcl::Controls::TControl* _tsBreak;
	Vcl::Controls::TControl* _tsTable;
	Vcl::Controls::TControl* _tsCells;
	Vcl::Controls::TControl* _tsRows;
	Vcl::Controls::TControl* _tsBoxPosition;
	Vcl::Controls::TControl* _tsBoxSize;
	Vcl::Controls::TControl* _tsBoxContent;
	Vcl::Controls::TControl* _tsSeq;
	Vcl::Controls::TControl* _lblTableDefWidth;
	Vcl::Controls::TControl* _lblCellDefWidth;
	Vcl::Controls::TControl* _lblDefSize;
	Vcl::Controls::TControl* _lblDefSize2;
	Vcl::Controls::TControl* _lblCellBorderColor;
	Vcl::Controls::TControl* _lblCellBorderLightColor;
	Vcl::Controls::TControl* _txtAlt;
	Vcl::Controls::TControl* _txtImgHint;
	Vcl::Controls::TControl* _lstBoxPosition;
	Vcl::Controls::TControl* _gbBoxHPos;
	Vcl::Controls::TControl* _gbBoxVPos;
	Vcl::Controls::TControl* _cmbBoxWidth;
	Vcl::Controls::TControl* _cmbBoxHeight;
	Vcl::Controls::TControl* _lblBoxHeightTxt;
	Vcl::Controls::TControl* _lblBoxWidthTxt;
	Vcl::Controls::TControl* _lblBoxAutoHeight;
	Vcl::Controls::TControl* _cmbBoxHPosition;
	Vcl::Controls::TControl* _cmbBoxVPosition;
	Vcl::Controls::TControl* _cmbBoxHAlign;
	Vcl::Controls::TControl* _cmbBoxVAlign;
	Vcl::Controls::TControl* _lblBoxHMU;
	Vcl::Controls::TControl* _lblBoxVMU;
	Vcl::Controls::TControl* _lblBoxHTxt;
	Vcl::Controls::TControl* _lblBoxVTxt;
	Vcl::Controls::TControl* _cbRelativeToCell;
	Vcl::Controls::TControl* _cbBoxWidthMirror;
	Vcl::Controls::TControl* _cbBoxPosMirror;
	Vcl::Controls::TControl* _btnCellVisibleBorders;
	Vcl::Controls::TControl* _lblCellBorder;
	Vcl::Controls::TControl* _btnTableVisibleBorders;
	Vcl::Controls::TWinControl* _pc;
	void __fastcall HideImgTabSheets(void);
	void __fastcall HideTableTabSheets(void);
	void __fastcall HideBoxTabSheets(void);
	Rvstyle::TRVStyleLength __fastcall GetTableBestWidth(Rvstyle::TRVStyleUnits Units, Rvstyle::TRVStyleLength DefValue);
	void __fastcall AdjustTableProps(void);
	
public:
	Rvstyle::TRVStyle* FRVStyle;
	Vcl::Dialogs::TColorDialog* ColorDialog;
	System::UnicodeString Filter;
	System::UnicodeString BackgroundFilter;
	bool StoreImageFileName;
	Vcl::Actnlist::TAction* ActionInsertTable;
	Vcl::Actnlist::TAction* ActionInsertHLine;
	bool __fastcall SetItem(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall SetTable(Rvtable::TRVTableItemInfo* table, bool TableOnly);
	void __fastcall SetGraphic(Rvitem::TRVGraphicItemInfo* Item);
	void __fastcall SetBreak(Rvitem::TRVBreakItemInfo* Item);
	void __fastcall SetSidenote(Rvsidenote::TRVSidenoteItemInfo* Item);
	void __fastcall GetSideNote(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall SetSequence(Rvedit::TCustomRichViewEdit* rve, Rvseqitem::TRVSeqItemInfo* Item);
	void __fastcall GetSequence(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall GetItem(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall GetGraphic(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall GetBreak(Rvedit::TCustomRichViewEdit* rve);
	void __fastcall FillBreakAction(Vcl::Actnlist::TAction* Action);
	void __fastcall GetTable(Rvedit::TCustomRichViewEdit* rve, Rvtable::TRVTableItemInfo* table);
	void __fastcall FillTableAction(Vcl::Actnlist::TAction* Action);
	DYNAMIC void __fastcall Localize(void);
	void __fastcall DrawBreak(Vcl::Graphics::TCanvas* Canvas, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	DYNAMIC bool __fastcall GetMyClientSize(int &Width, int &Height);
public:
	/* TfrmRVBase.Create */ inline __fastcall TfrmRVItemProp(System::Classes::TComponent* AOwner, System::Classes::TComponent* AControlPanel) : Baservfrm::TfrmRVBase(AOwner, AControlPanel) { }
	
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVItemProp(System::Classes::TComponent* AOwner, int Dummy) : Baservfrm::TfrmRVBase(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVItemProp(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVItemProp(HWND ParentWindow) : Baservfrm::TfrmRVBase(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Itemproprvfrm */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ITEMPROPRVFRM)
using namespace Itemproprvfrm;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ItemproprvfrmHPP
