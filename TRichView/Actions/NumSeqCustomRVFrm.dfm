�
 TFRMRVCUSTOMNUMSEQ 0C  TPF0�TfrmRVCustomNumSeqfrmRVCustomNumSeqLeft� Top;BorderStylebsDialogCaptionfrmRVCustomNumSeqClientHeight� ClientWidthOldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TButtonbtnOkLeft)Top� WidthdHeightAnchorsakRightakBottom CaptionOKDefault	ModalResultTabOrder  TButton	btnCancelLeft� Top� WidthdHeightAnchorsakRightakBottom Cancel	CaptionCancelModalResultTabOrder  	TGroupBoxgbSeqLeftTopWidth� Height� Caption
PropertiesTabOrder  TLabel
lblSeqNameLeftTopWidthEHeightCaptionCounter name:FocusControl
cmbSeqName  TLabel
lblSeqTypeLeftTopFWidthMHeightCaption&Numbering type:FocusControl
cmbSeqType  	TComboBox
cmbSeqNameLeftTop(Width� HeightDropDownCount
ItemHeightTabOrder OnChangecmbSeqNameChangeOnClickcmbSeqNameClick  	TComboBox
cmbSeqTypeLeftTopXWidth� HeightStylecsDropDownListDropDownCount
ItemHeightTabOrderItems.Strings1, 2, 3, ...a, b, c, ...A, B, C, ...i, ii, iii, ...I, II, III, ...     