
{*******************************************************}
{                                                       }
{       RichViewActions                                 }
{       Design-time support for                         }
{       TRVFontSizeComboBox, TRVFontCharsetComboBox,    }
{       TRVFontComboBox v1.2                            }
{                                                       }
{       Copyright (c) 2002-2003, Sergey Tkachenko       }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

{$I RV_Defs.inc}

unit RVFontCombosReg;
interface

uses
  Classes, RVFontCombos, RVStyleFuncs;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('RichView Misc', [
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}TRVStyleTemplateComboBox,
    TRVStyleTemplateListBox,{$ENDIF}
    TRVFontSizeComboBox, TRVFontCharsetComboBox, TRVFontComboBox]);
end;

end.
