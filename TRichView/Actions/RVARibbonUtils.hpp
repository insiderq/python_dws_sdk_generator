﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVARibbonUtils.pas' rev: 27.00 (Windows)

#ifndef RvaribbonutilsHPP
#define RvaribbonutilsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <Vcl.ActnMan.hpp>	// Pascal unit
#include <Vcl.Ribbon.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVRuler.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvaribbonutils
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall FillActionClientContents(Vcl::Actnman::TActionManager* Manager);
extern DELPHI_PACKAGE void __fastcall RemoveEllipsisFromRibbon(Vcl::Actnman::TActionManager* Manager);
extern DELPHI_PACKAGE void __fastcall RemoveEllipsis(System::Classes::TComponent* Form);
extern DELPHI_PACKAGE void __fastcall LocalizeRibbonPage(Vcl::Ribbon::TRibbonPage* Page, Rvalocalize::TRVAMessageID CaptionID);
extern DELPHI_PACKAGE void __fastcall SetRulerColors(Rvruler::TRVRuler* Ruler, Vcl::Ribbon::TCustomRibbonColorMap* ColorMap);
}	/* namespace Rvaribbonutils */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVARIBBONUTILS)
using namespace Rvaribbonutils;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvaribbonutilsHPP
