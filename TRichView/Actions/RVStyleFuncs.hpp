﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVStyleFuncs.pas' rev: 27.00 (Windows)

#ifndef RvstylefuncsHPP
#define RvstylefuncsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVALocalize.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <FontRVFrm.hpp>	// Pascal unit
#include <ParaRVFrm.hpp>	// Pascal unit
#include <ParaBrdrRVFrm.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <HypHoverPropRVFrm.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvstylefuncs
{
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<int, 3> TRVStyleTemplateImageIndices;

class DELPHICLASS TRVStyleTemplateComboBox;
class PASCALIMPLEMENTATION TRVStyleTemplateComboBox : public Vcl::Stdctrls::TCustomComboBox
{
	typedef Vcl::Stdctrls::TCustomComboBox inherited;
	
private:
	bool FShowClearFormat;
	bool FShowAllStyles;
	bool FUpdating;
	bool FSmartHeadings;
	Rvedit::TCustomRichViewEdit* FRichViewEdit;
	Rvscroll::TCustomRVControl* FEditor;
	Vcl::Imglist::TCustomImageList* FImages;
	int FTextStyleImageIndex;
	int FParaStyleImageIndex;
	int FParaTextStyleImageIndex;
	int FClearFormatImageIndex;
	int FAllStylesImageIndex;
	System::Classes::TNotifyEvent FOnClickAllStyles;
	int FTemporaryShowAllState;
	System::Classes::TComponent* FControlPanel;
	void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* Value);
	void __fastcall SetEditor(Rvscroll::TCustomRVControl* Value);
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* Value);
	void __fastcall SetControlPanel(System::Classes::TComponent* Value);
	HIDESBASE MESSAGE void __fastcall CNCommand(Winapi::Messages::TWMCommand &Message);
	void __fastcall DoUpdateSelection(System::TObject* Sender);
	void __fastcall DoFill(System::TObject* Sender);
	void __fastcall DoSRVChangeActiveEditor(System::TObject* Sender);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall MeasureItem(int Index, int &Height);
	virtual void __fastcall DrawItem(int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	__property Rvedit::TCustomRichViewEdit* RichViewEdit = {read=FRichViewEdit, write=SetRichViewEdit};
	
public:
	__fastcall virtual TRVStyleTemplateComboBox(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVStyleTemplateComboBox(void);
	void __fastcall Fill(void);
	void __fastcall UpdateSelection(void);
	DYNAMIC void __fastcall Click(void);
	void __fastcall Localize(void);
	
__published:
	__property Rvscroll::TCustomRVControl* Editor = {read=FEditor, write=SetEditor};
	__property System::Classes::TComponent* ControlPanel = {read=FControlPanel, write=SetControlPanel};
	__property bool ShowAllStyles = {read=FShowAllStyles, write=FShowAllStyles, default=0};
	__property bool ShowClearFormat = {read=FShowClearFormat, write=FShowClearFormat, default=1};
	__property bool SmartHeadings = {read=FSmartHeadings, write=FSmartHeadings, default=1};
	__property Vcl::Imglist::TCustomImageList* Images = {read=FImages, write=SetImages};
	__property int TextStyleImageIndex = {read=FTextStyleImageIndex, write=FTextStyleImageIndex, default=-1};
	__property int ParaStyleImageIndex = {read=FParaStyleImageIndex, write=FParaStyleImageIndex, default=-1};
	__property int ParaTextStyleImageIndex = {read=FParaTextStyleImageIndex, write=FParaTextStyleImageIndex, default=-1};
	__property int ClearFormatImageIndex = {read=FClearFormatImageIndex, write=FClearFormatImageIndex, default=-1};
	__property int AllStylesImageIndex = {read=FAllStylesImageIndex, write=FAllStylesImageIndex, default=-1};
	__property System::Classes::TNotifyEvent OnClickAllStyles = {read=FOnClickAllStyles, write=FOnClickAllStyles};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property Color = {default=-16777211};
	__property Constraints;
	__property Ctl3D;
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property DropDownCount = {default=10};
	__property Enabled = {default=1};
	__property Font;
	__property ImeMode = {default=3};
	__property ImeName = {default=0};
	__property ItemHeight = {default=40};
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Visible = {default=1};
	__property OnChange;
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnDropDown;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnStartDock;
	__property OnStartDrag;
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVStyleTemplateComboBox(HWND ParentWindow) : Vcl::Stdctrls::TCustomComboBox(ParentWindow) { }
	
};


class DELPHICLASS TRVStyleTemplateListBox;
class PASCALIMPLEMENTATION TRVStyleTemplateListBox : public Vcl::Stdctrls::TCustomListBox
{
	typedef Vcl::Stdctrls::TCustomListBox inherited;
	
private:
	bool FShowClearFormat;
	bool FShowAllStyles;
	bool FUpdating;
	bool FSmartHeadings;
	Rvedit::TCustomRichViewEdit* FRichViewEdit;
	Rvscroll::TCustomRVControl* FEditor;
	Vcl::Imglist::TCustomImageList* FImages;
	int FTextStyleImageIndex;
	int FParaStyleImageIndex;
	int FParaTextStyleImageIndex;
	int FClearFormatImageIndex;
	int FAllStylesImageIndex;
	System::Classes::TNotifyEvent FOnClickAllStyles;
	System::Classes::TComponent* FControlPanel;
	void __fastcall SetRichViewEdit(Rvedit::TCustomRichViewEdit* Value);
	void __fastcall SetEditor(Rvscroll::TCustomRVControl* Value);
	void __fastcall SetImages(Vcl::Imglist::TCustomImageList* Value);
	void __fastcall SetControlPanel(System::Classes::TComponent* Value);
	void __fastcall DoUpdateSelection(System::TObject* Sender);
	void __fastcall DoFill(System::TObject* Sender);
	void __fastcall DoSRVChangeActiveEditor(System::TObject* Sender);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall DrawItem(int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);
	__property Rvedit::TCustomRichViewEdit* RichViewEdit = {read=FRichViewEdit, write=SetRichViewEdit};
	
public:
	__fastcall virtual TRVStyleTemplateListBox(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVStyleTemplateListBox(void);
	void __fastcall Fill(void);
	void __fastcall UpdateSelection(void);
	DYNAMIC void __fastcall Click(void);
	void __fastcall Localize(void);
	
__published:
	__property Rvscroll::TCustomRVControl* Editor = {read=FEditor, write=SetEditor};
	__property System::Classes::TComponent* ControlPanel = {read=FControlPanel, write=SetControlPanel};
	__property bool ShowAllStyles = {read=FShowAllStyles, write=FShowAllStyles, default=0};
	__property bool ShowClearFormat = {read=FShowClearFormat, write=FShowClearFormat, default=1};
	__property bool SmartHeadings = {read=FSmartHeadings, write=FSmartHeadings, default=1};
	__property Vcl::Imglist::TCustomImageList* Images = {read=FImages, write=SetImages};
	__property int TextStyleImageIndex = {read=FTextStyleImageIndex, write=FTextStyleImageIndex, default=-1};
	__property int ParaStyleImageIndex = {read=FParaStyleImageIndex, write=FParaStyleImageIndex, default=-1};
	__property int ParaTextStyleImageIndex = {read=FParaTextStyleImageIndex, write=FParaTextStyleImageIndex, default=-1};
	__property int ClearFormatImageIndex = {read=FClearFormatImageIndex, write=FClearFormatImageIndex, default=-1};
	__property int AllStylesImageIndex = {read=FAllStylesImageIndex, write=FAllStylesImageIndex, default=-1};
	__property System::Classes::TNotifyEvent OnClickAllStyles = {read=FOnClickAllStyles, write=FOnClickAllStyles};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property Color = {default=-16777211};
	__property Constraints;
	__property Ctl3D;
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property Font;
	__property ImeMode = {default=3};
	__property ImeName = {default=0};
	__property ItemHeight = {default=40};
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnStartDock;
	__property OnStartDrag;
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVStyleTemplateListBox(HWND ParentWindow) : Vcl::Stdctrls::TCustomListBox(ParentWindow) { }
	
};


enum DECLSPEC_DENUM TRVStandardStyle : unsigned char { rvssnNormal, rvssnNormalIndent, rvssnNoSpacing, rvssnHeadingN, rvssnListParagraph, rvssnHyperlink, rvssnTitle, rvssnSubtitle, rvssnEmphasis, rvssnSubtleEmphasis, rvssnIntenseEmphasis, rvssnStrong, rvssnQuote, rvssnIntenseQuote, rvssnSubtleReference, rvssnIntenseReference, rvssnBlockText, rvssnHTMLVariable, rvssnHTMLCode, rvssnHTMLAcronym, rvssnHTMLDefinition, rvssnHTMLKeyboard, rvssnHTMLSample, rvssnHTMLTypewriter, rvssnHTMLPreformatted, rvssnHTMLCite, rvssnHeader, rvssnFooter, rvssnPageNumber, rvssnCaption, rvssnEndnoteReference, rvssnFootnoteReference, rvssnEndnoteText, rvssnFootnoteText, rvssnSidenoteReference, rvssnSidenoteText };

typedef System::Set<TRVStandardStyle, TRVStandardStyle::rvssnNormal, TRVStandardStyle::rvssnSidenoteText> TRVStandardStyles;

typedef System::StaticArray<Rvtypes::TRVAnsiString, 36> Rvstylefuncs__3;

struct DECLSPEC_DRECORD TRVStandardStylesRec
{
public:
	TRVStandardStyles Styles;
	System::Set<System::Byte, 0, 255> HeadingLevels;
};


//-- var, const, procedure ---------------------------------------------------
static const System::Int8 RVMINSTYLETEMPLATECOMBOCOUNT = System::Int8(0xa);
extern DELPHI_PACKAGE Rvstyle::TRVFontInfoProperties RVFontDialogProps;
extern DELPHI_PACKAGE Rvstyle::TRVFontInfoProperties RVFontHyperlinkProps;
extern DELPHI_PACKAGE Rvstyle::TRVParaInfoProperties RVParaDialogProps;
extern DELPHI_PACKAGE Rvstyle::TRVParaInfoProperties RVParaBorderDialogProps;
extern DELPHI_PACKAGE Rvstylefuncs__3 RVStandardStyleNames;
extern DELPHI_PACKAGE void __fastcall StyleTemplateToFontForm(Fontrvfrm::TfrmRVFont* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall StyleTemplateFromFontForm(Fontrvfrm::TfrmRVFont* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall StyleTemplateToHyperlinkForm(Hyphoverproprvfrm::TfrmRVHypHoverProp* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle);
extern DELPHI_PACKAGE void __fastcall StyleTemplateFromHyperlinkForm(Hyphoverproprvfrm::TfrmRVHypHoverProp* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle);
extern DELPHI_PACKAGE void __fastcall StyleTemplateToParaForm(Pararvfrm::TfrmRVPara* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall StyleTemplateFromParaForm(Pararvfrm::TfrmRVPara* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, bool TabsWereDefined, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall StyleTemplateToParaBorderForm(Parabrdrrvfrm::TfrmRVParaBrdr* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall StyleTemplateFromParaBorderForm(Parabrdrrvfrm::TfrmRVParaBrdr* frm, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall DescribeStyleFont(Rvstyle::TCustomRVFontInfo* TextStyle, const Rvstyle::TRVFontInfoProperties &Properties, Rvstyle::TRVStyle* RVStyle, bool Root, System::UnicodeString &Descr, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall DescribeStyleHyperlink(Rvstyle::TCustomRVFontInfo* TextStyle, const Rvstyle::TRVFontInfoProperties &Properties, Rvstyle::TRVStyle* RVStyle, bool Root, System::UnicodeString &Descr, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall DescribeStylePara(Rvstyle::TCustomRVParaInfo* ParaStyle, const Rvstyle::TRVParaInfoProperties &Properties, Rvstyle::TRVStyle* RVStyle, bool Root, System::UnicodeString &Descr, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall DescribeStyleParaBorder(Rvstyle::TCustomRVParaInfo* ParaStyle, const Rvstyle::TRVParaInfoProperties &Properties, Rvstyle::TRVStyle* RVStyle, bool Root, System::UnicodeString &Descr, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVDisplayCurrentStyles(Rvedit::TCustomRichViewEdit* rve, Richview::TCustomRichView* rv, Vcl::Imglist::TCustomImageList* Images, int FontImageIndex, int ParaImageIndex, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE bool __fastcall RVGetStyleType(Rvstyle::TRVStyleTemplateName Name, TRVStandardStyle &StyleType, int &Level);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVGetStandardStyleName(TRVStandardStyle StyleType, int Level, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVGetStyleName(const Rvstyle::TRVStyleTemplateName Name, bool MarkStandard, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVSetStandardStyleDefaults(TRVStandardStyle StyleType, int Level, Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* RVStyle, Rvstyle::TRVStyleTemplateCollection* StyleTemplates, Rvstyle::TRVStyleTemplateCollection* StandardStyleTemplates, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVFillStandardStyleInfo(Rvstyle::TRVStyleTemplateCollection* StyleTemplates, TRVStandardStylesRec &Info);
extern DELPHI_PACKAGE void __fastcall RVStyleTemplatesToStrings(Rvstyle::TRVStyleTemplateCollection* StyleTemplates, System::Classes::TStrings* Strings, bool AssignObjects, Rvstyle::TRVStyleTemplateKinds Kinds, Rvstyle::TRVStyleTemplate* ExcludeThisStyle, bool OnlyPotintialParents, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE int __fastcall RVFindStyleTemplateInStrings(System::Classes::TStrings* Strings, Rvstyle::TRVStyleTemplate* StyleTemplate);
extern DELPHI_PACKAGE int __fastcall RVFindStyleTemplateNameInStrings(System::Classes::TStrings* Strings, const Rvstyle::TRVStyleTemplateName Name);
extern DELPHI_PACKAGE void __fastcall RVDisplayStyleTemplate(Rvstyle::TRVStyleTemplate* StyleTemplate, Rvstyle::TRVStyle* rvs, Richview::TCustomRichView* rv, Vcl::Imglist::TCustomImageList* Images, int FontImageIndex, int HyperlinkImageIndex, int ParaImageIndex, int ParaBorderImageIndex, bool Editable, System::Classes::TComponent* ControlPanel);
extern DELPHI_PACKAGE void __fastcall RVDisplayStyleTemplateBasic(Rvstyle::TRVStyleTemplate* StyleTemplate, Richview::TCustomRichView* rv, Vcl::Imglist::TCustomImageList* Images, const TRVStyleTemplateImageIndices &ImageIndices, System::Classes::TComponent* ControlPanel);
}	/* namespace Rvstylefuncs */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSTYLEFUNCS)
using namespace Rvstylefuncs;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvstylefuncsHPP
