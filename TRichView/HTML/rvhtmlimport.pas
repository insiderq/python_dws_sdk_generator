unit rvhtmlimport;

{.$DEFINE RVHTML_ALLCOLORS}

{

  Copyright (c) 2002-2005 by Carlo Kok <rvhtml@carlo-kok.com>
  Additions by
    - Harry Mulder <hpmulder@wanadoo.nl> (hm)
    - Rudy Ramsey <rudy@ramsisle.com> (rr)
    - Sergey Tkachenko (st)
    - Miha Vrhovnik (mv)
  You are free to use, modify and redistribute it with the following restrictions:
	 - This header and copyright notice must stay intact
	 - You must have a TRichView license
              
Version 0.0050
        - packages for Delphi and C++Builder XE6
Version 0.0049   
        - compatibility with TRichView 14.13
Version 0.0048
        - fix: sub/superscript font size
        - fix: reading table and cell background color from CSS
Version 0.0047
        - packages for Delphi and C++Builder XE5
Version 0.0046
        - packages for Delphi and C++Builder XE4
Version 0.0045
        - compatibility with TRichView 14.3
Version 0.0044
        - compatibility with TRichView 14.1
Version 0.0043
        - 64-bit compatibility
Version 0.0042
        - all named entities can be read (in Delphi 2009+)
Version 0.0041
        - assigning 'Hyperlink' style-template to hyperlinks, if possible/necessary
Version 0.0040
        - Delphi XE2 support
Version 0.0039
        - support RichViews measured in twips
        - fixes
Version 0.0038
        - packages for Delphi and C++Builder XE
Version 0.0037
        - fix: pasting HTML with images
Version 0.0036
        - reading color values in form rgb(RR,GG,BB), used in CSS (thanks to Thore Sittly)
Version 0.0035
        - fixes (memory leaks; StyleValue) (thanks to sigbert)
Version 0.0034 (st, here and later)
        - new: support of image alignment
        - new: support of headers
Version 0.0033 (st)
Version 0.0032 (st)
        - fix: reverted some incorrect changes
Version 0.0031 (st)
        - fix: in Clipboard pasting
Version 0.0030 (st)
        - fix: processing uppercase tag attributes (thanks to Kai Fischer)
Version 0.0029 (st)
        - New Option: rvhtmloIgnorePictures. If set, pictures (including
          backgrounds) are not loaded;
        - If Encoding=rvhtmleANSI, but the input string has UTF-8 byte order mark
          at the beginning, HTML is loaded as UTF-8
Version 0.0028 (st)
        - Compatibility with Delphi 2009
        - Fixes in reading UTF-8 files
        - Reading 3 digit color values (#NNN) (ts - Thore Sittly)
        - Fix in StyleValue (st + ts)
Version 0.0027 (st)
        - Fix in pasting multicell selection
        - Fix in link processing (links containing ':', link containing codes
          like '&amp;', links started from '/')
        - If you compile with RVHTML_ALLCOLORS compiler define, RvHtmlImporter
          will understand more color codes (used in IE) (implemented by krho)
Version 0.0026 (st)
        - Support of <blockquote> (thanks to Chia Chen Yap)
        - Support for attribute values in single quotes
Version 0.0025 (st)
        - impr: loading <td style='border-width : ...'> (thanks to simon-c)
Version 0.0024 (mv)
        - fix: don't truncate rvfonts, rvparas if more than one style tag is found in message
        - fix: Replaced all StrToInt with StrToIntDef, to prevent exceptions
Version 0.0023.3 (st)
Version 0.0023 (st)
        - fix: &XXX; in UTF-8 HTMLs
        - skipping byte order characters in UTF-8 html
Version 0.0022 (martindk)
        - fix in processing <strike>
        - <s> and <del> are supported
Version 0.0021 (st)
        - If TextStyles[0].Unicode was True, all new styles will be Unicode too,
          even if ClearDocument is True
        - Support of <sup> and <sub>
Version 0.0020
        - Packages for BDS 2006 (Delphi Win32 and C++Builder)
        - Reading files saved by SaveHTMLEx (with embedded style sheet) (Henk van Bokhorst)
        - Reading border and background properties of table, saved in CSS (Henk van Bokhorst)
        - If TextStyles[0].Unicode was True, all new styles will be Unicode too (st)
        - All new styles have Standard property = False. (st)
        - Compatibility with D4 and D5 is restored
Version 0.0019 (st)
        - Encoding property, allows reading UTF-8 encoded HTMLs
          (note: encoding is not autodetected)
Version 0.0018 (st)
        - packages for Delphi 2005 (Win32)

Version 0.0017.1 (st)
        - fix: some fixes in line breaking 

Version 0.0017

What's new in 0.0017 (st)
        - fix: processing &#number; codes. Number is an Unicode character code.
            No, TrvHtmlImporter does not support Unicode yet, it converts these
            characters to ANSI using RVStyle.DefCodePage for conversion.
        - fix: correct scaling of images when only width (without height) or
            only height (without width) is defined.
What's new in 0.0016 (st)
        - del: OnBackgrounRequired, OnImageRequired. Use OnImageRequired2
          (or, better, RichView.OnImportPicture), or do not use at all -
          local files are loaded automatically
        - chg: Picture reading order:
          1) OnImageRequired2 is called. If image is obtained, end.
          2) RichView.OnImportPicture is called. Unlike OnImageRequired2, the
             full file name to image is provided in this event (together with
             BasePath). If image is obtained, end.
          3) The component tries to load picture, if it is a local file.
             If image is read, end.
          4) If this is a background image, end.
          5) Return RichView.Style.InvalidPicture
        - impr: <table background> and <td background> are supported
        - impr: default values of properties are not saved with form
        - chg: default colors are changed from clBlack/clWhite to
          clWindowText/clWindow
        - new: Options property. A set of values:
          * rvhtmloBasePathLinks - if set (default), BasePath is automatically
            added to hyperlinks and stored image paths
          * rvhtmloAutoHyperlinks - if set (default) and (rvoTagsArePChars in
            RichView.Options), hyperlinks' targets are automatically assigned to
            tags (as it was before). If not, RichView.OnReadHyperlink is called.
          * rvhtmloImgSrcToImageName - if set, <IMG src> value is assigned to
            images items names (it's not necessary because they are also stored
            in rvespImageFileName extra string property).
        - if you remove dot from the $DEFINE RICHVIEW_REG at the beginning // upd: this define is removed
          of rvhtmlimport.pas, the following items' string properties will be
          loaded (note: since 0.0018 this define is removed, and these features
          are always enabled):
          * rvespImageFileName for images and tables, and BackgroundImageFileName
            property for table cells;
          * rvespImageAlt for images.
        - fix: BasePath when pasting from the Clipboard
        - fix: table width in percents (thanks to Jan Dekkers)
        - fix: autoadjusting RichView property when the linked component is deleted
        - fix: fixes in line breaking
What's new in 0.0015 (st)

        - Possible range check error is fixed. (st)
        - Correctly adds checkpoints in table cells. (st)
        - IMPORTANT: LoadHtml method does not call Format any more!
          Please check your existing code and add Format if necessary.
          This change is made to allow TRVHtmlImporter working with
          TRVReportHelper.RichView
          (Format must not be called for it). (st)
        - Does not resize graphic according to Width and Height attributes of
          <img>. Applying rvepImageWidth/Height is enough. (st)
        - Compatibility with Delphi 4 is restored.

What's new in 0.0014

        -  Range check error if line if finished with #13.  (st)
        -  Possible line breaks in checkpoint names and hyperlinks. (st)

What's new in 0.0013

        - <td>s without <tr> in tables are now supported. (hm)
        - Correctly handles text outside <td>-tags in tables.  (hm)

        - Revised UL and OL; fixed various issues, and added support for params.(hm)

        - New property: EnforceListLevels. Exotic usage of HTML's UL/OL tags    (hm)
          can be emulated within RichView, but only by applying ListLevels in
          an undocumented fashion (simply put, the index of a ListLevel no
          longer specifies its depth). This property determines if this
          technique should be used. Settings:

            True  : ListLevels are used in the documented manner, but you will
                    have less HTML compatibility. (Default)
            False : You will have a higher degree of HTML compatibility, but
                    the resulting ListLevels do not behave in the documented
                    manner. Advisable for experts only.

What's new in 0.0012
        - Fix: "Empty" tags made <div>-tags function incorrectly (hm)
        - Support for stretched images (hm)
        - Support for ISO Latin-1 code table (hm)

What's new in 0.0011
        - Added an option if the document (and it's styles) should be cleared (ck)

What's new in 0.0010
        - Fix in <li> when exporting and reimporting it (ck)
        - Specific (wrongly written) HTML could generate an "AV at 00000000" (hm)
        - Support for hypertext pictures (hm)

What's new in 0.0009
Changes by Sergey Tkachenko:
        - procedure LoadFromClipboard loads HTML from the Clipboard.
          After loading, you can use  function GetSelectionBounds to get a range
          of items that was selected in browser
        - BasePath: String property specifies a base path for images and links.
          When loading from file, you should assign it yourself.
          When loading from the Clipboard, this property is assigned automatically
          (probably, to www location)
          Suggestion: processing <BASE> should modify this property.
        - bulleted lists are represented by rvlstBullet, not by rvlstImageList.
        - list indents are changed

What's new in 0.0008
        - CharCount property provides running character count for use in events (rr)
        - LineCount property provides running line count for use in events (rr)
        - SeparateLines property causes separate text lines to be loaded into
                RichView as separate items, rather than being accumulated (rr)
        - OnComment event makes comment text available for external use (rr)
        - OnNewLine event notifies user at start of each new line of HTML
                In my simple implementation, ignores newlines within tags and in <pre>...</pre> (rr)


What's new in 0.0007
	- Added support for TH (ck)
	- Small changes (ck)

What's new in 0.0006
	- Fixed bug in Table colspan (ck)
	- Fixed bug in Table border colors (ck)
	- Added support for Table/TR/TD valign (ck)

What's new in 0.0005
        - Alignment support (table align; center; div) (hm)
	- A lot of fixes (font styles don't leak to other cells anymore) (hm)
	- <title></title> fix (hm)
	- Support for <body background>; new event "OnBackgroundRequired" for supplying the bitmap. (hm)
	- The name-property of an image now contains the URL (hm)
	- <tr bgcolor> (hm)
	- Support for named colors (hm)
	- <script>; <noembed> tags (contents will be ignored) (hm)


What's new in 0.0004
        - CR, LF and Tab now also supported in tags (hm)
        - New event: TOnImageRequired2, this event has width and height passed from the html. (hm)


What's new in 0.0003
        - Updated to support <div></div> (hm)

What's new in 0.0002
        - <table cellspacing,cellpadding,bordercolor,bordercolorlight> (hm)
        - <ul style="disc,circle,square"> and <ol style="a,i,A,I" start="1"> (hm)

}

{$I RV_Defs.inc}


interface

uses
  {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  SysUtils, Windows, Classes, Graphics, Controls,
  {$IFDEF RICHVIEWDEFXE2}
  UITypes,
  {$ENDIF}
  RichView, RVStyle, CRVData,
  RVTable, RVFuncs, RVClasses, RVTypes, RVGrHandler;

type
  TOnComment = procedure(Sender: TObject; const sComment: string) of
  object;  //hrr OnComment

  TOnImageRequired2 = procedure(Sender: TObject; const src: string;
    Width, Height: integer; var Img: TGraphic) of object;

  TListType = (ltNone, ltDisc, ltCircle, ltSquare, ltNumber, ltLowerAlpha,
    ltUpperAlpha, ltLowerRoman, ltUpperRoman);

  TRVHTMLOption = (rvhtmloAutoHyperlinks, rvhtmloBasePathLinks,
    rvhtmloImgSrcToImageName, rvhtmloIgnorePictures);
  TRVHTMLOptions = set of TRVHTMLOption;

  TRVHTMLEncoding = (rvhtmleANSI, rvhtmleUTF8);

  TRVRawStringArray = array[0..$7FFFFFF{MaxListSize}] of TRVAnsiString;
  PRVRawStringArray = ^TRVRawStringArray;

  TRVRawByteStringList = class
  private
    FCount, FCapacity: Integer;
    FItems: PRVRawStringArray;
  public
    procedure Clear;
    procedure Add(const s: TRVRawByteString);
    function IndexOfName(const Name: TRVRawByteString): Integer;
    function GetValue(const Name: TRVRawByteString): TRVRawByteString;
    destructor Destroy; override;
  end;

  TRvHtmlParaStackItem = class
    public
      ParaNo: Integer;
      Alignment: TRVAlignment;
      OutlineLevel: Integer;
  end;

  {$IFDEF RICHVIEWDEFXE2}
  [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}

  TRvHtmlImporter = class(TComponent)
  private
    FCharCount: integer;            //hrr	CharCount
    FLineCount: integer;            //hrr LineCount
    FSeparateLines: boolean;          //hrr SeparateLines
    FViewer: TCustomRichView;
    FDefaultFontSize: integer;
    FDefaultCFontSize: integer;
    FDefaultFontName: TFontName;
    FDefaultCFontName: TFontName;
    FDefaultColor: TColor;
    FDefaultLinkCol: TColor;
    FDefaultLinkStyle: TFontStyles;
    FDefaultBGColor: TColor;
    FTitle: string;
    FOnComment: TOnComment;          //hrr OnComment
    FOnNewLine: TNotifyEvent;          //hrr OnNewLine
    FOnImageRequired2: TOnImageRequired2;
    FStartSelection, FEndSelection, FStartSelNo, FEndSelNo: integer;
    FNextTimeCloseSelection: boolean;
    FSelRVData: TCustomRVData;
    FBasePath: string;
    FClearDocument: boolean;
    FOptions: TRVHTMLOptions;
    FBackgroundRead: Boolean;

    FClrListStyle : integer;
    FEnforceListLevels: boolean;
    FFixedCSSStyles: Boolean;
    FEncoding, FActualEncoding: TRVHTMLEncoding;

    procedure LoadHtmlSelection(const Data: TRVRawByteString;
      StartSelection, EndSelection: integer);
    procedure CheckSelection(Parser: TObject; RVData: TCustomRVData;
      RVT: TRVTableItemInfo);
    procedure LoadClipboardHtml(const Data: TRVRawByteString);

    procedure InitListLevel(ALevel: TRVListLevel; AType: TListType; ADepth: integer);
    function ReadImage(const src: String; Width, Height: integer;
      NoFail: Boolean): TGraphic;
    function GetBitmap(gr: TGraphic): TBitmap;
    function GetFullFileName(const FileName: String): String;
    procedure SetFixedCSSStyles(const Value: Boolean);
    procedure AddRVStyles(TagList: String; FViewer: TCustomRichView);
    function GetCodePage: TRVCodePage;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property FixedCSSStyles: Boolean read FFixedCSSStyles write SetFixedCSSStyles default False;
  public
    procedure LoadFromClipboard;
    procedure LoadHtml(const Data: TRVRawByteString);
    constructor Create(AOwner: TComponent); override;
    function GetSelectionBounds(var RVData: TCustomRVData;
      var StartNo, EndNo: integer): boolean;
    property CharCount: integer read FCharCount;    //hrr CharCount
    property LineCount: integer read FLineCount;    //hrr LineCount
    property Title: string read FTitle;
    property BasePath: string read FBasePath write FBasePath;
  published
    property ClearDocument: boolean read FClearDocument write FClearDocument default True;
    property SeparateLines: boolean read FSeparateLines write FSeparateLines default False;
    //hrr SeparateLines
    property RichView: TCustomRichView read FViewer write FViewer;
    property DefaultFontName: TFontName read FDefaultFontName write FDefaultFontName;
    property DefaultCFontName: TFontName read FDefaultCFontName write FDefaultCFontName;
    property DefaultFontSize: integer read FDefaultFontSize write FDefaultFontSize default 12;
    property DefaultCFontSize: integer read FDefaultCFontSize write FDefaultCFontSize default 10;
    property DefaultColor: TColor read FDefaultColor write FDefaultColor default clWindowText;
    property DefaultLinkCol: TColor read FDefaultLinkCol write FDefaultLinkCol default clBlue;
    property DefaultLinkStyle: TFontStyles read FDefaultLinkStyle write FDefaultLinkStyle default [fsUnderline];
    property DefaultBGColor: TColor read FDefaultBGColor write FDefaultBGColor default clWindow;
    property EnforceListLevels : boolean read FEnforceListLevels write FEnforceListLevels default True;
    property Options: TRVHTMLOptions read FOptions write FOptions
      default [rvhtmloAutoHyperlinks, rvhtmloBasePathLinks];
    property OnComment: TOnComment read FOnComment write FOnComment;    //hrr OnComment
    property OnNewLine: TNotifyEvent read FOnNewLine write FOnNewLine;  //hrr OnNewLine
    property OnImageRequired2: TOnImageRequired2
      read FOnImageRequired2 write FOnImageRequired2;
    property Encoding: TRVHTMLEncoding read FEncoding write FEncoding default rvhtmleANSI;
  end;

procedure Register;

implementation

uses
  RVItem, CRVFData, RVRVData, RVScroll, RVUni, RVStr;

type
{$IFDEF RICHVIEWDEFXE2}
  TRVNativeUInt = NativeUInt;
{$ELSE}
  TRVNativeUInt = Cardinal;
{$ENDIF}  
{========================== Constants and types ===============================}
const
  DefMarkerIndentPix = 24;
  DefLeftIndentPix = 24;
  FontSize: array[1..7] of integer = (8,10,12, 14, 18, 24, 36);
  Spaces = [' ', #9, #13, #10];
type
  TCurrBlock = (cbComment, cbText, cbTag);
  THtmlParser = class
  private
    OrgText: TRVRawByteString;
    FLastWasSpace: boolean;
    FEncoding: TRVHTMLEncoding;
    P: PRVAnsiChar;

    FTagNameX: String;
    FTagParamsX: TStringList;
    FCurrBlock: TCurrBlock;
    FRemoveSpaces: boolean;
    FCurrLine: integer;
    FCodePage: TRVCodePage;
    function GetBytesParsed: integer;
    function ConvertAllCData(const s: String): String;
    function ConvertCData(CData: String): String;
    function IsUTF8: Boolean;
  public
    property LastWasSpace: boolean read FLastWasSpace write FLastWasSpace;
    property RemoveSpaces: boolean read FRemoveSpaces write FRemoveSpaces;

    constructor Create(const Text: TRVRawByteString);
    destructor Destroy; override;

    procedure Next;
    function Done: boolean;

    property TagNameX: String read FTagNameX;
    property TagParamsX: TStringList read FTagParamsX;
    property CurrBlock: TCurrBlock read FCurrBlock;
    property CurrLine: integer read FCurrLine;      //hrr LineCount
    property BytesParsed: integer read GetBytesParsed;
  end;
  TRVTableRowsH = class(TRVTableRows)
  end;
  TRVTableItemInfoH = class(TRVTableItemInfo)
  public
    CurrRow, CurrCol: longint;
  end;

  TContainerLevel = (clMain, clTable, clRow, clCol);
  TIndentType = (llUL, llOL, llBQ);
  TContainerIndentType = class
  public
    IndentType  : TIndentType;
    ListType    : TListType;
    ListValue   : integer;
  end;

  TContainerDataType = class(TCollectionItem)
    (* contains settings and local stacks for a container *)
    (* container is: main doc, table, tr or td.           *)
  private
    ParaStack: TRVList;
    PrevFont: TList;
    IndentStack : TRVList;
    function FindAlignment(AAlignment: TRVAlignment; AOutlineLevel: Integer): integer;
    function GetCurAlignment: TRVAlignment;
    //function GetCurOutlineLevel: Integer;
  public
    Importer : TRvHtmlImporter;
    Level: TContainerLevel;
    TextStyle: integer;
    ParaStyle: integer;
    IndentListStyle : integer;
    VAlign: TRVCellVAlign;
    BgColor: TColor;
    CurLIType  : TListType; // current Listype, applied by LI
    CurLIValue : integer;   // current value, applied by LI
    CurHadLI   : boolean;
    RVT : TRVTableItemInfoH; // contains current RV Table-instance
    RVData : TCustomRVData;  // contains default RVData (for unbalanced tables)
    property CurAlignment: TRVAlignment read GetCurAlignment;
    //property CurOutlineLevel: Integer read GetCurOutlineLevel;
    procedure EnterAlignment(AAlignment: TRVAlignment; AOutlineLevel: Integer;
      ParaStyleNo: Integer = -1);
    //procedure EnterAlignmentFake;
    procedure LeaveAlignment;
    procedure EnterIndent( AIndentType : TIndentType; AType: TListType; AStartAt: longint );
    procedure LeaveIndent( AIndentType : TIndentType );
    function ListCount : integer; // returns no of Ul/OL's in IndentStack
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;

  TRVParas = record
    TAlign: String;
    ParaStyleNo: integer;
  end;

  TRVFonts = record
    TextStyleNo: integer;
  end;

var
  RVParas: array of TRVParas;
  RVFonts: array of TRVFonts;


{============================ Procedures ======================================}
procedure Register;
begin
  RegisterComponents('RichView', [TRVHtmlImporter]);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVUNICODESTR}
type
  TSysCharSet = set of Char;
function CharInSet(C: Char; CharSet: TSysCharSet): Boolean;
begin
  Result := C in CharSet;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function HTMLStringToString(const s: TRVRawByteString; UTF8: Boolean;
  CodePage: TRVCodePage): String;
begin
{$IFDEF RVUNICODESTR}
  if UTF8 then
    Result := UTF8ToString(s)
  else
    Result := RVU_RawUnicodeToWideString(RVU_AnsiToUnicode(CodePage, s));
{$ELSE}
  if UTF8 then
    Result := RVU_UnicodeToAnsi(CodePage, RVU_GetRawUnicode(UTF8Decode(s)))
  else
    Result := s;
{$ENDIF}
end;
{------------------------------------------------------------------------------}
function ConvertUTF8ToString(const s: TRVRawByteString): String;
begin
  {$IFDEF RVUNICODESTR}
  Result := UTF8ToString(s)
  {$ELSE}
  Result := UTF8Decode(s)
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
function gv(c: TStringList; const Name: String): String;
begin
  Result := c.Values[Name];
end;
{------------------------------------------------------------------------------}
function HFtoF(FSize: integer): integer;
var
  I: integer;
begin
  for i := 1 to 6 do
  begin
    if FSize <= FontSize[i] then
    begin
      Result := i;
      exit;
    end;
  end;
  Result := 7;
end;
{------------------------------------------------------------------------------}
function FtoHF(HSize: integer): integer;
begin
  if HSize < Low(FontSize) then HSize := Low(FontSize)
  else if HSize > High(FontSize) then HSize := High(FontSize);
  Result := FontSize[HSize];
end;

{------------------------------------------------------------------------------}
// returns color value specified as 'rgb(XX,XX,XX)'
function GetRGBColor(const s: String): TColor;
var nPos     : Integer;
  nLoop      : Integer;
  R, G, B    : String;
  CurColor   : Integer;
  nR, nG, nB : Integer;
begin
  CurColor := 0;
  nPos := 1;
  while nPos<Length(s) do begin
    if (s[nPos] = '(') then
      Break;
    Inc(nPos);
  end;
  for nLoop:=nPos+1 to Length(s) do begin
    if (s[nLoop] = ',') then
      Inc(CurColor)
    else if (s[nLoop] = ')') then
      Break
    else begin
      case CurColor of
        0: R := R + s[nLoop];
        1: G := G + s[nLoop];
        2: B := B + s[nLoop];
      end;
    end;
  end;
  R := Trim(R);
  G := Trim(G);
  B := Trim(B);
  nR := StrToIntDef(R,0);
  nG := StrToIntDef(G,0);
  nB := StrToIntDef(B,0);
  Result := ((nR and $FF)) or ((nG and $FF) shl 8) or ((nB and $FF) shl 16)
end;
{------------------------------------------------------------------------------}
function GetHtmlColor(s: String): TColor;
var
  Col: TColor;
  sCol: String;
begin
  {$IFDEF RVHTML_ALLCOLORS}
  if (CompareText(s, 'aliceblue') = 0) then Result := $FFF8F0
  else if (CompareText(s, 'antiquewhite') = 0) then Result := $D7EBFA
  else if (CompareText(s, 'aqua') = 0) then Result := $FFFF00
  else if (CompareText(s, 'aquamarine') = 0) then Result := $D4FF7F
  else if (CompareText(s, 'azure') = 0) then Result := $FFFFF0
  else if (CompareText(s, 'beige') = 0) then Result := $DCF5F5
  else if (CompareText(s, 'bisque') = 0) then Result := $C4E4FF
  else if (CompareText(s, 'black') = 0) then Result := $000000
  else if (CompareText(s, 'blanchedalmond') = 0) then Result := $CDEBFF
  else if (CompareText(s, 'blue') = 0) then Result := $FF0000
  else if (CompareText(s, 'blueviolet') = 0) then Result := $E22B8A
  else if (CompareText(s, 'brown') = 0) then Result := $2A2AA5
  else if (CompareText(s, 'burlywood') = 0) then Result := $87B8DE
  else if (CompareText(s, 'cadetblue') = 0) then Result := $A09E5F
  else if (CompareText(s, 'chartreuse') = 0) then Result := $00FF7F
  else if (CompareText(s, 'chocolate') = 0) then Result := $1E69D2
  else if (CompareText(s, 'coral') = 0) then Result := $507FFF
  else if (CompareText(s, 'cornflowerblue') = 0) then Result := $ED9564
  else if (CompareText(s, 'cornsilk') = 0) then Result := $DCF8FF
  else if (CompareText(s, 'crimson') = 0) then Result := $3C14DC
  else if (CompareText(s, 'cyan') = 0) then Result := $FFFF00
  else if (CompareText(s, 'darkblue') = 0) then Result := $8B0000
  else if (CompareText(s, 'darkcyan') = 0) then Result := $8B8B00
  else if (CompareText(s, 'darkgoldenrod') = 0) then Result := $0B86B8
  else if (CompareText(s, 'darkgray') = 0) then Result := $A9A9A9
  else if (CompareText(s, 'darkgrey') = 0) then Result := $A9A9A9
  else if (CompareText(s, 'darkgreen') = 0) then Result := $006400
  else if (CompareText(s, 'darkkhaki') = 0) then Result := $6BB7BD
  else if (CompareText(s, 'darkmagenta') = 0) then Result := $8B008B
  else if (CompareText(s, 'darkolivegreen') = 0) then Result := $2F6B55
  else if (CompareText(s, 'darkorange') = 0) then Result := $008CFF
  else if (CompareText(s, 'darkorchid') = 0) then Result := $CC3299
  else if (CompareText(s, 'darkred') = 0) then Result := $00008B
  else if (CompareText(s, 'darksalmon') = 0) then Result := $7A96E9
  else if (CompareText(s, 'darkseagreen') = 0) then Result := $8FBC8F
  else if (CompareText(s, 'darkslateblue') = 0) then Result := $8B3D48
  else if (CompareText(s, 'darkslategray') = 0) then Result := $4F4F2F
  else if (CompareText(s, 'darkslategrey') = 0) then Result := $4F4F2F
  else if (CompareText(s, 'darkturquoise') = 0) then Result := $D1CE00
  else if (CompareText(s, 'darkviolet') = 0) then Result := $D30094
  else if (CompareText(s, 'deeppink') = 0) then Result := $9314FF
  else if (CompareText(s, 'deepskyblue') = 0) then Result := $FFBF00
  else if (CompareText(s, 'dimgray') = 0) then Result := $696969
  else if (CompareText(s, 'dimgrey') = 0) then Result := $696969
  else if (CompareText(s, 'dodgerblue') = 0) then Result := $FF901E
  else if (CompareText(s, 'firebrick') = 0) then Result := $2222B2
  else if (CompareText(s, 'floralwhite') = 0) then Result := $F0FAFF
  else if (CompareText(s, 'forestgreen') = 0) then Result := $228B22
  else if (CompareText(s, 'fuchsia') = 0) then Result := $FF00FF
  else if (CompareText(s, 'gainsboro') = 0) then Result := $DCDCDC
  else if (CompareText(s, 'ghostwhite') = 0) then Result := $FFF8F8
  else if (CompareText(s, 'gold') = 0) then Result := $00D7FF
  else if (CompareText(s, 'goldenrod') = 0) then Result := $20A5DA
  else if (CompareText(s, 'gray') = 0) then Result := $808080
  else if (CompareText(s, 'grey') = 0) then Result := $808080
  else if (CompareText(s, 'green') = 0) then Result := $008000
  else if (CompareText(s, 'greenyellow') = 0) then Result := $2FFFAD
  else if (CompareText(s, 'honeydew') = 0) then Result := $F0FFF0
  else if (CompareText(s, 'hotpink') = 0) then Result := $B469FF
  else if (CompareText(s, 'indianred') = 0) then Result := $5C5CCD
  else if (CompareText(s, 'indigo') = 0) then Result := $82004B
  else if (CompareText(s, 'ivory') = 0) then Result := $F0FFFF
  else if (CompareText(s, 'khaki') = 0) then Result := $8CE6F0
  else if (CompareText(s, 'lavender') = 0) then Result := $FAE6E6
  else if (CompareText(s, 'lavenderblush') = 0) then Result := $F5F0FF
  else if (CompareText(s, 'lawngreen') = 0) then Result := $00FC7C
  else if (CompareText(s, 'lemonchiffon') = 0) then Result := $CDFAFF
  else if (CompareText(s, 'lightblue') = 0) then Result := $E6D8AD
  else if (CompareText(s, 'lightcoral') = 0) then Result := $8080F0
  else if (CompareText(s, 'lightcyan') = 0) then Result := $FFFFE0
  else if (CompareText(s, 'lightgoldenrodyellow') = 0) then Result := $D2FAFA
  else if (CompareText(s, 'lightgray') = 0) then Result := $D3D3D3
  else if (CompareText(s, 'lightgrey') = 0) then Result := $D3D3D3
  else if (CompareText(s, 'lightgreen') = 0) then Result := $90EE90
  else if (CompareText(s, 'lightpink') = 0) then Result := $C1B6FF
  else if (CompareText(s, 'lightsalmon') = 0) then Result := $7AA0FF
  else if (CompareText(s, 'lightseagreen') = 0) then Result := $AAB220
  else if (CompareText(s, 'lightskyblue') = 0) then Result := $FACE87
  else if (CompareText(s, 'lightslategray') = 0) then Result := $998877
  else if (CompareText(s, 'lightslategrey') = 0) then Result := $998877
  else if (CompareText(s, 'lightsteelblue') = 0) then Result := $DEC4B0
  else if (CompareText(s, 'lightyellow') = 0) then Result := $E0FFFF
  else if (CompareText(s, 'lime') = 0) then Result := $00FF00
  else if (CompareText(s, 'limegreen') = 0) then Result := $32CD32
  else if (CompareText(s, 'linen') = 0) then Result := $E6F0FA
  else if (CompareText(s, 'magenta') = 0) then Result := $FF00FF
  else if (CompareText(s, 'maroon') = 0) then Result := $000080
  else if (CompareText(s, 'mediumaquamarine') = 0) then Result := $AACD66
  else if (CompareText(s, 'mediumblue') = 0) then Result := $CD0000
  else if (CompareText(s, 'mediumorchid') = 0) then Result := $D355BA
  else if (CompareText(s, 'mediumpurple') = 0) then Result := $D87093
  else if (CompareText(s, 'mediumseagreen') = 0) then Result := $71B33C
  else if (CompareText(s, 'mediumslateblue') = 0) then Result := $EE687B
  else if (CompareText(s, 'mediumspringgreen') = 0) then Result := $9AFA00
  else if (CompareText(s, 'mediumturquoise') = 0) then Result := $CCD148
  else if (CompareText(s, 'mediumvioletred') = 0) then Result := $8515C7
  else if (CompareText(s, 'midnightblue') = 0) then Result := $701919
  else if (CompareText(s, 'mintcream') = 0) then Result := $FAFFF5
  else if (CompareText(s, 'mistyrose') = 0) then Result := $E1E4FF
  else if (CompareText(s, 'moccasin') = 0) then Result := $B5E4FF
  else if (CompareText(s, 'navajowhite') = 0) then Result := $ADDEFF
  else if (CompareText(s, 'navy') = 0) then Result := $800000
  else if (CompareText(s, 'oldlace') = 0) then Result := $E6F5FD
  else if (CompareText(s, 'olive') = 0) then Result := $008080
  else if (CompareText(s, 'olivedrab') = 0) then Result := $238E6B
  else if (CompareText(s, 'orange') = 0) then Result := $00A5FF
  else if (CompareText(s, 'orangered') = 0) then Result := $0045FF
  else if (CompareText(s, 'orchid') = 0) then Result := $D670DA
  else if (CompareText(s, 'palegoldenrod') = 0) then Result := $AAE8EE
  else if (CompareText(s, 'palegreen') = 0) then Result := $98FB98
  else if (CompareText(s, 'paleturquoise') = 0) then Result := $EEEEAF
  else if (CompareText(s, 'palevioletred') = 0) then Result := $9370D8
  else if (CompareText(s, 'papayawhip') = 0) then Result := $D5EFFF
  else if (CompareText(s, 'peachpuff') = 0) then Result := $B9DAFF
  else if (CompareText(s, 'peru') = 0) then Result := $3F85CD
  else if (CompareText(s, 'pink') = 0) then Result := $CBC0FF
  else if (CompareText(s, 'plum') = 0) then Result := $DDA0DD
  else if (CompareText(s, 'powderblue') = 0) then Result := $E6E0B0
  else if (CompareText(s, 'purple') = 0) then Result := $800080
  else if (CompareText(s, 'red') = 0) then Result := $0000FF
  else if (CompareText(s, 'rosybrown') = 0) then Result := $8F8FBC
  else if (CompareText(s, 'royalblue') = 0) then Result := $E16941
  else if (CompareText(s, 'saddlebrown') = 0) then Result := $13458B
  else if (CompareText(s, 'salmon') = 0) then Result := $7280FA
  else if (CompareText(s, 'sandybrown') = 0) then Result := $60A4F4
  else if (CompareText(s, 'seagreen') = 0) then Result := $578B2E
  else if (CompareText(s, 'seashell') = 0) then Result := $EEF5FF
  else if (CompareText(s, 'sienna') = 0) then Result := $2D52A0
  else if (CompareText(s, 'silver') = 0) then Result := $C0C0C0
  else if (CompareText(s, 'skyblue') = 0) then Result := $EBCE87
  else if (CompareText(s, 'slateblue') = 0) then Result := $CD5A6A
  else if (CompareText(s, 'slategray') = 0) then Result := $908070
  else if (CompareText(s, 'slategrey') = 0) then Result := $908070
  else if (CompareText(s, 'snow') = 0) then Result := $FAFAFF
  else if (CompareText(s, 'springgreen') = 0) then Result := $7FFF00
  else if (CompareText(s, 'steelblue') = 0) then Result := $B48246
  else if (CompareText(s, 'tan') = 0) then Result := $8CB4D2
  else if (CompareText(s, 'teal') = 0) then Result := $808000
  else if (CompareText(s, 'thistle') = 0) then Result := $D8BFD8
  else if (CompareText(s, 'tomato') = 0) then Result := $4763FF
  else if (CompareText(s, 'turquoise') = 0) then Result := $D0E040
  else if (CompareText(s, 'violet') = 0) then Result := $EE82EE
  else if (CompareText(s, 'wheat') = 0) then Result := $B3DEF5
  else if (CompareText(s, 'white') = 0) then Result := $FFFFFF
  else if (CompareText(s, 'whitesmoke') = 0) then Result := $F5F5F5
  else if (CompareText(s, 'yellow') = 0) then Result := $00FFFF
  else if (CompareText(s, 'yellowgreen') = 0) then Result := $32CD9A
  else if (CompareText(s, 'background') = 0) then Result := clBackground
  else if (CompareText(s, 'activecaption') = 0) then Result := clActiveCaption
  else if (CompareText(s, 'inactivecaption') = 0) then Result := clInactiveCaption
  else if (CompareText(s, 'menu') = 0) then Result := clMenu
  else if (CompareText(s, 'window') = 0) then Result := clWindow
  else if (CompareText(s, 'windowframe') = 0) then Result := clWindowFrame
  else if (CompareText(s, 'menutext') = 0) then Result := clMenuText
  else if (CompareText(s, 'windowtext') = 0) then Result := clWindowText
  else if (CompareText(s, 'captiontext') = 0) then Result := clCaptionText
  else if (CompareText(s, 'activeborder') = 0) then Result := clActiveBorder
  else if (CompareText(s, 'inactiveborder') = 0) then Result := clInactiveBorder
  else if (CompareText(s, 'appworkspace') = 0) then Result := clAppWorkSpace
  else if (CompareText(s, 'highlight') = 0) then Result := clHighlight
  else if (CompareText(s, 'hightlighttext') = 0) then Result := clHighlightText
  else if (CompareText(s, 'buttonface') = 0) then Result := clBtnFace
  else if (CompareText(s, 'buttonshadow') = 0) then Result := clBtnShadow
  else if (CompareText(s, 'graytext') = 0) then Result := clGrayText
  else if (CompareText(s, 'buttontext') = 0) then Result := clBtnText
  else if (CompareText(s, 'inactivecaptiontext') = 0) then Result := clInactiveCaptionText
  else if (CompareText(s, 'buttonhighlight') = 0) then Result := clBtnHighlight
  else if (CompareText(s, 'threeddarkshadow') = 0) then Result := cl3DDkShadow
  else if (CompareText(s, 'threedlightshadow') = 0) then Result := clBtnHighlight
  else if (CompareText(s, 'infotext') = 0) then Result := clInfoText
  else if (CompareText(s, 'infobackground') = 0) then Result := clInfoBk
  else if (CompareText(s, 'scrollbar') = 0) then Result := clScrollBar
  else if (CompareText(s, 'threedface') = 0) then Result := clBtnFace
  else if (CompareText(s, 'threedhighlight') = 0) then Result := cl3DLight
  else if (CompareText(s, 'threedshadow') = 0) then Result := clBtnShadow
  else if (CompareText(s, 'transparent') = 0) then Result := clNone
  {$ELSE}
  if (CompareText(s, 'white') = 0) then Result := clWhite
  else if (CompareText(s, 'black') = 0) then Result := clBlack
  else if (CompareText(s, 'red') = 0) then Result := clRed
  else if (CompareText(s, 'green') = 0) then Result := clGreen
  else if (CompareText(s, 'blue') = 0) then Result := clBlue
  else if (CompareText(s, 'aqua') = 0) then Result := clAqua
  else if (CompareText(s, 'fuchsia') = 0) then Result := clFuchsia
  else if (CompareText(s, 'gray') = 0) then Result := clDkGray
  else if (CompareText(s, 'lime') = 0) then Result := clLime
  else if (CompareText(s, 'maroon') = 0) then Result := clMaroon
  else if (CompareText(s, 'navy') = 0) then Result := clNavy
  else if (CompareText(s, 'olive') = 0) then Result := clOlive
  else if (CompareText(s, 'purple') = 0) then Result := clPurple
  else if (CompareText(s, 'silver') = 0) then Result := clGray
  else if (CompareText(s, 'teal') = 0) then Result := clTeal
  else if (CompareText(s, 'yellow') = 0) then Result := clYellow
  else if (CompareText(s, 'brown') = 0) then Result := $2820A0
  {$ENDIF}
  else if (Pos('rgb', s)=1) then
    Result := GetRGBColor(s)
  else begin
    if s[1] = '#' then
      Delete(s, 1, 1);
    if Length(s) = 3 then
      sCol := s[1]+s[1]+s[2]+s[2]+s[3]+s[3]
    else
      sCol := s;
    Col := StrToIntDef('$' + sCol, - 1);
    if Col <> -1 then
      Result := ((Col and $FF) shl 16) or (Col and $FF00) or ((Col and $FF0000) shr 16)
    else
      Result := 0;
  end;
end;
{------------------------------------------------------------------------------}
function GetListType( AData : String; ADef : TListType ): TListType;
begin
  if CompareText( AData, 'disc') = 0         then Result := ltdisc
  else if CompareText( AData, 'circle') = 0  then Result := ltcircle
  else if CompareText(  AData, 'square') = 0 then Result := ltSquare
  else if (AData = 'a')              then Result := ltLowerAlpha
  else if (AData = 'A')              then Result := ltUpperAlpha
  else if (AData = 'i')              then Result := ltLowerRoman
  else if (AData = 'I')              then Result := ltUpperRoman
  else if (AData = '1')              then Result := ltNumber
  else Result := ADef;
end;
{------------------------------------------------------------------------------}
function GetAlignParam(c: TStringList; ADefault: TRVAlignment): TRVAlignment;
  (* converts text alignments to TRVAligment; returns default if align-param not found/incorrect *)
var
  s: String;
begin
  s := LowerCase(gv(c, 'align'));

  if (s = 'left') then Result := rvaLeft
  else if (s = 'right') then Result := rvaRight
  else if (s = 'center') then Result := rvaCenter
  else if (s = 'justify') then Result := rvaJustify
  else
    Result := ADefault;
end;

{------------------------------------------------------------------------------}
function StyleValue(const AStyleParam, AStyleType: String): String;
var
  StyleParam: String;
  Position, nPos: integer;

begin
  Result := '';
  StyleParam := AStyleParam;
  nPos := Pos(AStyleType, StyleParam);
  while nPos>0 do
    if (nPos>1) and not CharInSet(StyleParam[nPos-1], [' ', ';', '{']) then begin
      StyleParam := Copy(StyleParam, nPos + Length(AStyleType), Length(StyleParam));
      nPos := Pos(AStyleType, StyleParam);
      end
    else
      break;
  if nPos>0 then begin
    StyleParam := Copy(StyleParam, nPos + Length(AStyleType), 100);
    while CharInSet(StyleParam[1], [' ', ':']) do
      Delete(StyleParam, 1, 1);
    Position := 1;
    if CharInSet(StyleParam[1], ['''','"']) then begin
      Position := 2;
      while not CharInSet(StyleParam[Position], ['''', '"']) do begin
        Result := Result + StyleParam[Position];
        Inc(Position);
        if Position > Length(StyleParam) then
          break;
      end;
      end
    else
      while not CharInSet(StyleParam[Position], [';', ')', '>', ']', '}']) do begin
        Result := Result + StyleParam[Position];
        Inc(Position);
        if Position > Length(StyleParam) then
          break;        
      end;
  end;
end;
{------------------------------------------------------------------------------}
function TRvHTMLImporter.GetCodePage: TRVCodePage;
begin
  Result := FViewer.Style.DefCodePage;
end;
{------------------------------------------------------------------------------}
// Returning a value from css in RVStyle.Units (or, if RVStyle=nil, in pixels)
// Supports integer pixels or floating points
function GetCSSValue(const Value: String; RVStyle: TRVStyle;
  DefPixValue: Integer): TRVStyleLength;
var p: Integer;
    s: String;
    v: TRVLength;
begin
  p := Pos('px', Value);
  if p>0 then begin
    Result := StrToIntDef(Copy(Value, 1, p-1), DefPixValue);
    if RVStyle<>nil then
      Result := RVStyle.PixelsToUnits(Result);
    exit;
  end;
  p := Pos('pt', Value);
  if p>0 then begin
    s := Copy(Value, 1, p-1);
    p := Pos('.', s);
    if p>0 then
      s[p] := {$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator;
    try
      v := StrToFloat(s);
      if RVStyle<>nil then
        Result := RVStyle.RVUnitsToUnits(v, rvuPoints)
      else
        Result := TRVStyle.RVUnitsToUnitsEx(v, rvuPoints, rvstuPixels);
    except
      if RVStyle<>nil then
        Result := RVStyle.PixelsToUnits(DefPixValue)
      else
        Result := DefPixValue;
    end;
    exit;
  end;
  Result := StrToIntDef(Value, DefPixValue);
  if RVStyle<>nil then
    Result := RVStyle.PixelsToUnits(Result);
end;
{------------------------------------------------------------------------------}
// Read line height to ParaStyle
procedure GetLineHeight(const Value: String; ParaStyle: TParaInfo);
var p: Integer;
    s: String;
    v: TRVLength;
begin
  p := Pos('px', Value);
  if p>0 then begin
    try
      ParaStyle.LineSpacing := StrToInt(Copy(Value, 1, p-1));
      ParaStyle.LineSpacing := ParaStyle.GetRVStyle.PixelsToUnits(ParaStyle.LineSpacing);
      ParaStyle.LineSpacingType := rvlsLineHeightAtLeast;
    except
    end;
    exit;
  end;
  p := Pos('pt', Value);
  if p>0 then begin
    s := Copy(Value, 1, p-1);
    p := Pos('.', s);
    if p>0 then
      s[p] := {$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator;
    try
      v := StrToFloat(s);
      ParaStyle.LineSpacing := ParaStyle.GetRVStyle.RVUnitsToUnits(v, rvuPoints);
      ParaStyle.LineSpacingType := rvlsLineHeightAtLeast;
    except
    end;
    exit;
  end;
  s := Value;
  p := Pos('.', s);
  if p>0 then
    s[p] := {$IFDEF RICHVIEWDEFXE}FormatSettings.{$ENDIF}DecimalSeparator;
  try
    v := StrToFloat(s);
    ParaStyle.LineSpacing := Round(v*100);
    ParaStyle.LineSpacingType := rvlsPercent;
  except
  end;
end;
{------------------------------------------------------------------------------}
function GetBorderWidth(Border: TRVBorder): TRVStyleLength;
begin
  Result := 0;
  if Border.Color=clNone then
    exit;
  case Border.Style of
    rvbSingle:
      Result := Border.Width;
    rvbDouble:
      Result := Border.Width*2+Border.InternalWidth;
    rvbThickInside, rvbThickOutside:
      Result := Border.Width*3+Border.InternalWidth;
    rvbTriple:
      Result := Border.Width*3+Border.InternalWidth*2;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRvHTMLImporter.AddRVStyles(TagList: String; FViewer: TCustomRichView);

var
  ArrayPos, TextStyleNo, ParaStyleNo, xPos: integer;
  StyleDesc, Value: String;
  BackGr: TGraphic;
  BackBMP: TBitmap;
  AJump: Boolean;
  TextStyle: TFontInfo;
  ParaStyle: TParaInfo;

begin
  if Length(RVFonts) = 0 then begin
    SetLength(RVFonts, 1);
    RVFonts[0].TextStyleNo := 0;
  end;

  if Length(RVParas) = 0 then begin
    SetLength(RVParas, 1);
    RVParas[0].TAlign      := 'left';
    RVParas[0].ParaStyleNo := 0;
  end;

  while TagList <> '' do begin
    if (Copy(TagList, 1, 4) = 'body') or
       (Copy(TagList, 1, 9) = 'span.rvts') or
       ((Copy(TagList, 1, 6) = 'a.rvts') and ((Pos(':',TagList)=0) or (Pos(':',TagList)>Pos('{',TagList)))) then begin
      if Copy(TagList, 1, 4) = 'body' then begin
        Delete(TagList, 1, Pos('{', TagList));
        StyleDesc   := Copy(TagList, 1, Pos('}', TagList) - 1);
        TextStyle := FViewer.Style.TextStyles[0];
        ArrayPos := 0;
        end
      else begin
        AJump := Copy(TagList, 1, 6) = 'a.rvts';
        Delete(TagList, 1, Pos('{', TagList));
        StyleDesc   := Copy(TagList, 1, Pos('}', TagList) - 1);
        TextStyle := FViewer.Style.TextStyles.Add;
        TextStyle.Assign(FViewer.Style.TextStyles[0]);
        TextStyle.Standard := False;
        TextStyle.Jump := AJump;
        ArrayPos    := Length(RVFonts);
        SetLength(RVFonts, ArrayPos + 1);
      end;
      TextStyleNo := TextStyle.Index;
      Value := StyleValue(StyleDesc, 'font-family');
        if Value <> '' then
          TextStyle.FontName := Value;
      Value := StyleValue(StyleDesc, 'font-size');
        if Value <> '' then begin
          xPos := Pos('pt', Value);
          if xPos > 0 then
            TextStyle.Size := StrToIntDef(Copy(Value, 1, xPos - 1), 10)
          else
            TextStyle.Size := StrToIntDef(Value, 10);
        end;
      Value := StyleValue(StyleDesc, 'color');
        if Value <> '' then
          TextStyle.Color := GetHtmlColor(Value);
      Value := StyleValue(StyleDesc, 'background-color');
        if Value <> '' then
          if TextStyleNo=0 then
            FViewer.Color := GetHtmlColor(Value)
          else
            TextStyle.BackColor := GetHtmlColor(Value);
      if not (rvhtmloIgnorePictures in Options) and (TextStyleNo=0) then begin
        Value := StyleValue(StyleDesc, 'background-image');
        if Value <> '' then begin
          Delete(Value, 1, 5);
          Delete(Value, Length(Value), 1);
          BackGr := ReadImage(Value, 0, 0, False);
          if BackGr <> nil then begin
            BackBMP := GetBitmap(BackGr);
            FViewer.BackgroundBitmap := BackBMP;
            BackBMP.Free;
            FBackgroundRead := True;
          end;
          BackGr.Free;
        end;
        Value := StyleValue(StyleDesc, 'background-position');
        if Value = '' then begin
          Value := StyleValue(StyleDesc, 'background-attachment');
          if Value = 'scroll' then
            FViewer.BackgroundStyle := bsTiledAndScrolled
          else if Value = 'fixed' then
            FViewer.BackgroundStyle := bsTiled;
          end
        else begin
          if Value = 'top left' then
            FViewer.BackgroundStyle := bsTopLeft
          else if Value = 'top right' then
            FViewer.BackgroundStyle := bsTopRight
          else if Value = 'bottom left' then
            FViewer.BackgroundStyle := bsBottomLeft
          else if Value = 'bottom right' then
            FViewer.BackgroundStyle := bsBottomRight
          else
            FViewer.BackgroundStyle := bsCentered;
        end;
      end;
      Value := StyleValue(StyleDesc, 'font-style');
        if Value = 'italic' then
          TextStyle.Style := TextStyle.Style + [fsItalic]
        else if Value = 'normal' then
          TextStyle.Style := TextStyle.Style - [fsItalic];
      Value := StyleValue(StyleDesc, 'font-weight');
        if Value = 'bold' then
          TextStyle.Style := TextStyle.Style + [fsBold]
        else if Value = 'normal' then
          TextStyle.Style := TextStyle.Style - [fsBold];
      Value := StyleValue(StyleDesc, 'text-decoration');
        if Pos('underline', Value) > 0 then
          TextStyle.Style   := TextStyle.Style + [fsUnderline];
        if Pos('line-through', Value) > 0 then
          TextStyle.Style   := TextStyle.Style + [fsStrikeOut];
        if Pos('overline', Value) > 0 then
          TextStyle.StyleEx := TextStyle.StyleEx + [rvfsOverline];
      Value := StyleValue(StyleDesc, 'text-transform');
        if Value = 'uppercase' then
          TextStyle.StyleEx := TextStyle.StyleEx + [rvfsAllCaps];
      Value := StyleValue(StyleDesc, 'display');
        if Value = 'none' then
          TextStyle.Options := TextStyle.Options + [rvteoHidden];
      Value := StyleValue(StyleDesc, 'letter-spacing');
        if Value<>'' then
          TextStyle.CharSpacing := GetCSSValue(Value, FViewer.Style, 0);
      Value := StyleValue(StyleDesc, 'vertical-align');
        if Value = 'sub' then
          TextStyle.SubSuperScriptType := rvsssSubscript
        else if Value = 'super' then
          TextStyle.SubSuperScriptType := rvsssSuperScript;
      Value := StyleValue(StyleDesc, 'direction');
        if Value = 'ltr' then
          TextStyle.BiDiMode := rvbdLeftToRight
        else if Value = 'rtl' then
          TextStyle.BiDiMode := rvbdRightToLeft;
      if TextStyleNo=0 then begin
        Value := StyleValue(StyleDesc, 'margin');
        if Value <> '' then begin
          FViewer.TopMargin := GetCSSValue(Value, nil, 0);
          Delete(Value, 1, Pos(' ', Value));
          FViewer.RightMargin := GetCSSValue(Value, nil, 0);
          Delete(Value, 1, Pos(' ', Value));
          FViewer.BottomMargin := GetCSSValue(Value, nil, 0);
          Delete(Value, 1, Pos(' ', Value));
          FViewer.LeftMargin := GetCSSValue(Value, nil, 0);
        end;
      end;
      RVFonts[ArrayPos].TextStyleNo := TextStyleNo;
      Delete(TagList, 1, Pos('}', TagList));
    end;

    if (Copy(TagList, 1, 7) = 'p,ul,ol') or (Copy(TagList, 1, 5) = '.rvps') then begin
      if Copy(TagList, 1, 7) = 'p,ul,ol' then begin
        Delete(TagList, 1, Pos('{', TagList));
        StyleDesc   := Copy(TagList, 1, Pos('}', TagList) - 1);
        ParaStyle := FViewer.Style.ParaStyles[0];
        ArrayPos := 0;
        end
      else begin
        Delete(TagList, 1, Pos('{', TagList));
        StyleDesc   := Copy(TagList, 1, Pos('}', TagList) - 1);
        ParaStyle := FViewer.Style.ParaStyles.Add;
        ParaStyle.Assign(FViewer.Style.ParaStyles[0]);
        ParaStyle.Standard := False;
        ArrayPos    := Length(RVParas);
        SetLength(RVParas, ArrayPos + 1);
        RVParas[ArrayPos].TAlign := StyleValue(StyleDesc, 'text-align');
      end;
      ParaStyleNo := ParaStyle.Index;
      with ParaStyle do
      begin
        Value := StyleValue(StyleDesc, 'text-align');
          if Value = 'left' then
            Alignment := rvaLeft
          else if Value = 'right' then
            Alignment := rvaRight
          else if Value = 'center' then
            Alignment := rvaCenter
          else if Value = 'justify' then
            Alignment := rvaJustify;
        Value := StyleValue(StyleDesc, 'direction');
          if Value = 'ltr' then
            BiDiMode := rvbdLeftToRight
          else if Value = 'rtl' then
            BiDiMode := rvbdRightToLeft;
        Value := StyleValue(StyleDesc, 'text-indent');
          if Value<>'' then
            FirstIndent := GetCSSValue(Value, FViewer.Style, 0);
        Value := StyleValue(StyleDesc, 'line-height');
          if Value<>'' then
            GetLineHeight(Value, ParaStyle);
        Value := StyleValue(StyleDesc, 'white-space');
          if Value='nowrap' then
            Options := Options+[rvpaoNoWrap]
          else if Value='normal' then
            Options := Options-[rvpaoNoWrap];
        Value := StyleValue(StyleDesc, 'page-break-inside');
          if Value='avoid' then
            Options := Options+[rvpaoKeepLinesTogether]
          else if Value='auto' then
            Options := Options-[rvpaoKeepLinesTogether];
        Value := StyleValue(StyleDesc, 'page-break-after');
          if Value='avoid' then
            Options := Options+[rvpaoKeepWithNext]
          else if Value='auto' then
            Options := Options-[rvpaoKeepWithNext];        
        Value := StyleValue(StyleDesc, 'background');
          if Value <> '' then
            Background.Color := GetHtmlColor(Value);
        Value := StyleValue(StyleDesc, 'padding');
          if Value <> '' then begin
            Background.BorderOffsets.Top := GetCSSValue(Value, FViewer.Style, 0);
            Delete(Value, 1, Pos(' ', Value));
            Background.BorderOffsets.Right := GetCSSValue(Value, FViewer.Style, 0);
            Delete(Value, 1, Pos(' ', Value));
            Background.BorderOffsets.Bottom := GetCSSValue(Value, FViewer.Style, 0);
            Delete(Value, 1, Pos(' ', Value));
            Background.BorderOffsets.Left := GetCSSValue(Value, FViewer.Style, 0);
          end;
        Value := StyleValue(StyleDesc, 'border-color');
          if Value <> '' then
            Border.Color := GetHtmlColor(Value);
        Value := StyleValue(StyleDesc, 'border-style');
          if Value = 'none' then
            Border.Style := rvbNone
          else if Value = 'solid' then
            Border.Style := rvbSingle
          else if Value = 'double' then
            Border.Style := rvbDouble;
          if Border.Style<>rvbNone then
            Border.BorderOffsets.Assign(Background.BorderOffsets);
        Value := StyleValue(StyleDesc, 'border-width');
          if Value <> '' then begin
            Border.Width := GetCSSValue(Value, FViewer.Style, 0);
            Border.InternalWidth := Border.Width;
          end;
        Border.VisibleBorders.Bottom := (StyleValue(StyleDesc, 'border-bottom') <> 'none');
        Border.VisibleBorders.Left   := (StyleValue(StyleDesc, 'border-left')   <> 'none');
        Border.VisibleBorders.Right  := (StyleValue(StyleDesc, 'border-right')  <> 'none');
        Border.VisibleBorders.Top    := (StyleValue(StyleDesc, 'border-top')    <> 'none');
        Value := StyleValue(StyleDesc, 'margin');
          if Value <> '' then begin
            SpaceBefore := GetCSSValue(Value, FViewer.Style, 0)+Border.BorderOffsets.Top+GetBorderWidth(Border);
            Delete(Value, 1, Pos(' ', Value));
            RightIndent := GetCSSValue(Value, FViewer.Style, 0)+Border.BorderOffsets.Right+GetBorderWidth(Border);
            Delete(Value, 1, Pos(' ', Value));
            SpaceAfter := GetCSSValue(Value, FViewer.Style, 0)+Border.BorderOffsets.Bottom+GetBorderWidth(Border);
            Delete(Value, 1, Pos(' ', Value));
            LeftIndent := GetCSSValue(Value, FViewer.Style, 0)+Border.BorderOffsets.Left+GetBorderWidth(Border);
          end;
      end;
      RVParas[ArrayPos].ParaStyleNo := ParaStyleNo;
      Delete(TagList, 1, Pos('}', TagList));
    end;
    while (TagList <> '') and (not CharInSet(TagList[1], [#10, #13, ' '])) do Delete(TagList, 1, 1);
    while (TagList <> '') and CharInSet(TagList[1], [#10, #13, ' ']) do Delete(TagList, 1, 1);
  end;
end;

{============================== THtmlParser ===================================}
constructor THtmlParser.Create(const Text: TRVRawByteString);
begin
  inherited Create;
  FTagParamsX := TStringList.Create;
  FRemoveSpaces := True;
  OrgText := Text;
  P := PRVAnsiChar(OrgText);
  FCurrLine := 1;                  //hrr LineCount
end;
{------------------------------------------------------------------------------}
destructor THtmlParser.Destroy;
begin
  FTagParamsX.Free;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
function THtmlParser.Done: boolean;
begin
  Result := p^ = #0;
end;
{------------------------------------------------------------------------------}
function THtmlParser.GetBytesParsed: integer;
begin
  Result := p - PRVAnsiChar(OrgText);
end;
{------------------------------------------------------------------------------}
function THtmlParser.IsUTF8: Boolean;
begin
  Result := FEncoding=rvhtmleUTF8;
end;
{------------------------------------------------------------------------------}
function THtmlParser.ConvertCData( CData : String): String;
type
  TCTableEntry = record
    CData : String;
    CChar : byte;
  end;

  TCTableEntryW = record
    CData : String;
    CChar : Word;
  end;
var
  CharCode: Integer;
  ch: TRVAnsiChar;
const
  CTableCnt = 6 + 98;
  CTable : array[0..CTableCnt-1] of TCTableEntry =
  (
    (* Markup Controls (cnt=6) *)
    (CData:'nbsp'; CChar: 32),(CData:'quot'; CChar: 34),(CData:'amp'; CChar: 38),
    (CData:'lt'; CChar:   60),(CData:'gt'; CChar:   62), (CData:'apos'; CChar: 39),

    (* ISO Latin-1 (cnt=98) *)
    (CData:'iexcl'; CChar:  161),(CData:'cent'; CChar:   162),
    (CData:'pound'; CChar:  163),(CData:'curren'; CChar: 164),(CData:'yen'; CChar:    165),
    (CData:'brvbar'; CChar: 166),(CData:'sect'; CChar:   167),(CData:'uml'; CChar:    168),
    (CData:'copy'; CChar:   169),(CData:'ordf'; CChar:   170),(CData:'laquo'; CChar:  171),
    (CData:'not'; CChar:    172),(CData:'shy'; CChar:    173),(CData:'reg'; CChar:    174),
    (CData:'macr'; CChar:   175),(CData:'deg'; CChar:    176),(CData:'plusmn'; CChar: 177),
    (CData:'sup2'; CChar:   178),(CData:'sup3'; CChar:   179),(CData:'acute'; CChar:  180),
    (CData:'micro'; CChar:  181),(CData:'para'; CChar:   182),(CData:'middot'; CChar: 183),
    (CData:'cedil'; CChar:  184),(CData:'sup1'; CChar:   185),(CData:'ordm'; CChar:   186),
    (CData:'raquo'; CChar:  187),(CData:'frac14'; CChar: 188),(CData:'frac12'; CChar: 189),
    (CData:'frac34'; CChar: 190),(CData:'iquest'; CChar: 191),(CData:'Agrave'; CChar: 192),
    (CData:'Aacute'; CChar: 193),(CData:'Acirc'; CChar:  194),(CData:'Atilde'; CChar: 195),
    (CData:'Auml'; CChar:   196),(CData:'Aring'; CChar:  197),(CData:'AElig'; CChar:  198),
    (CData:'Ccedil'; CChar: 199),(CData:'Egrave'; CChar: 200),(CData:'Eacute'; CChar: 201),
    (CData:'Ecirc'; CChar:  202),(CData:'Euml'; CChar:   203),(CData:'Igrave'; CChar: 204),
    (CData:'Iacute'; CChar: 205),(CData:'Icirc'; CChar:  206),(CData:'Iuml'; CChar:   207),
    (CData:'ETH'; CChar:    208),(CData:'Ntilde'; CChar: 209),(CData:'Ograve'; CChar: 210),
    (CData:'Oacute'; CChar: 211),(CData:'Ocirc'; CChar:  212),(CData:'Otilde'; CChar: 213),
    (CData:'Ouml'; CChar:   214),(CData:'times'; CChar:  215),(CData:'Oslash'; CChar: 216),
    (CData:'Ugrave'; CChar: 217),(CData:'Uacute'; CChar: 218),(CData:'Ucircv'; CChar: 219),
    (CData:'Uuml'; CChar:   220),(CData:'Yacute'; CChar: 221),(CData:'THORN'; CChar:  222),
    (CData:'szlig'; CChar:  223),(CData:'agrave'; CChar: 224),(CData:'aacute'; CChar: 225),
    (CData:'acirc'; CChar:  226),(CData:'atilde'; CChar: 227),(CData:'auml'; CChar:   228),
    (CData:'aring'; CChar:  229),(CData:'aelig'; CChar:  230),(CData:'ccedil'; CChar: 231),
    (CData:'egrave'; CChar: 232),(CData:'eacute'; CChar: 233),(CData:'ecirc'; CChar:  234),
    (CData:'euml'; CChar:   235),(CData:'igrave'; CChar: 236),(CData:'iacute'; CChar: 237),
    (CData:'icirc'; CChar:  238),(CData:'iuml'; CChar:   239),(CData:'eth'; CChar:    240),
    (CData:'ntilde'; CChar: 241),(CData:'ograve'; CChar: 242),(CData:'oacute'; CChar: 243),
    (CData:'ocirc'; CChar:  244),(CData:'otilde'; CChar: 245),(CData:'ouml'; CChar:   246),
    (CData:'divide'; CChar: 247),(CData:'oslash'; CChar: 248),(CData:'ugrave'; CChar: 249),
    (CData:'uacute'; CChar: 250),(CData:'ucirc'; CChar:  251),(CData:'uuml'; CChar:   252),
    (CData:'yacute'; CChar: 253),(CData:'thorn'; CChar:  254),(CData:'yuml'; CChar:   255),
    (CData:'trade'; CChar:  153),(CData:'dagger'; CChar: 134),(CData:'Dagger'; CChar: 135)
  );
{$IFDEF RVUNICODESTR}
const CTableCntW = 148;
  CTableW : array[0..CTableCntW-1] of TCTableEntryW =
  (
    (CData:'OElig'; CChar:   $0152), (CData:'oelig'; CChar:  $0153), (CData:'Scaron'; CChar:   $0160),
    (CData:'Yuml'; CChar:    $0178), (CData:'fnof'; CChar:   $0192), (CData:'circ'; CChar:     $02C6),
    (CData:'tilde'; CChar:   $02DC), (CData:'Alpha'; CChar:  $0391), (CData:'Beta'; CChar:     $0392),
    (CData:'Gamma'; CChar:   $0393), (CData:'Delta'; CChar:  $0394), (CData:'Epsilon'; CChar:  $0395),
    (CData:'Zeta'; CChar:    $0396), (CData:'Eta'; CChar:    $0397), (CData:'Theta'; CChar:    $0398),
    (CData:'Iota'; CChar:    $0399), (CData:'Kappa'; CChar:  $039A), (CData:'Lambda'; CChar:   $039B),
    (CData:'Mu'; CChar:      $039C), (CData:'Nu'; CChar:     $039D), (CData:'Xi'; CChar:       $039E),
    (CData:'Omicron'; CChar: $039F), (CData:'Pi'; CChar:     $03A0), (CData:'Rho'; CChar:      $03A1),
    (CData:'Sigma'; CChar:   $03A3), (CData:'Tau'; CChar:    $03A4), (CData:'Upsilon'; CChar:  $03A5),
    (CData:'Phi'; CChar:     $03A6), (CData:'Chi'; CChar:    $03A7), (CData:'Psi'; CChar:      $03A8),
    (CData:'Omega'; CChar:   $03A9), (CData:'alpha'; CChar:  $03B1), (CData:'beta'; CChar:     $03B2),
    (CData:'gamma'; CChar:   $03B3), (CData:'delta'; CChar:  $03B4), (CData:'epsilon'; CChar:  $03B5),
    (CData:'zeta'; CChar:    $03B6), (CData:'eta'; CChar:    $03B7), (CData:'theta'; CChar:    $03B8),
    (CData:'iota'; CChar:    $03B9), (CData:'kappa'; CChar:  $03BA), (CData:'lambda'; CChar:   $03BB),
    (CData:'mu'; CChar:      $03BC), (CData:'nu'; CChar:     $03BD), (CData:'xi'; CChar:       $03BE),
    (CData:'omicron'; CChar: $03BF), (CData:'pi'; CChar:     $03C0), (CData:'rho'; CChar:      $03C1),
    (CData:'sigmaf'; CChar:  $03C2), (CData:'sigma'; CChar:  $03C3), (CData:'tau'; CChar:      $03C4),
    (CData:'upsilon'; CChar: $03C5), (CData:'phi'; CChar:    $03C6), (CData:'chi'; CChar:      $03C7),
    (CData:'psi'; CChar:     $03C8), (CData:'omega'; CChar:  $03C9), (CData:'thetasym'; CChar: $03D1),
    (CData:'upsih'; CChar:   $03D2), (CData:'piv'; CChar:    $03D6), (CData:'ensp'; CChar:     $2002),
    (CData:'emsp'; CChar:    $2003), (CData:'thinsp'; CChar: $2009), (CData:'ensp'; CChar:     $2002),
    (CData:'zwnj'; CChar:    $200C), (CData:'zwj'; CChar:    $200D), (CData:'lrm'; CChar:      $200E),
    (CData:'rlm'; CChar:     $200F), (CData:'ndash'; CChar:  $2013), (CData:'mdash'; CChar:    $2014),
    (CData:'lsquo'; CChar:   $2018), (CData:'rsquo'; CChar:  $2019), (CData:'sbquo'; CChar:    $201A),
    (CData:'ldquo'; CChar:   $201C), (CData:'rdquo'; CChar:  $201D), (CData:'bull'; CChar:     $2022),
    (CData:'hellip'; CChar:  $2026), (CData:'permil'; CChar: $2030), (CData:'prime'; CChar:    $2032),
    (CData:'Prime'; CChar:   $2033), (CData:'lsaquo'; CChar: $2039), (CData:'rsaquo'; CChar:   $203A),
    (CData:'oline'; CChar:   $203E), (CData:'frasl'; CChar:  $2044), (CData:'euro'; CChar:     $20AC),
    (CData:'image'; CChar:   $2111), (CData:'weierp'; CChar: $2118), (CData:'real'; CChar:     $211C),
    (CData:'alefsym'; CChar: $2135), (CData:'larr'; CChar:   $2190), (CData:'uarr'; CChar:     $2191),
    (CData:'rarr'; CChar:    $2192), (CData:'darr'; CChar:   $2193), (CData:'harr'; CChar:     $2194),
    (CData:'crarr'; CChar:   $21B5), (CData:'lArr'; CChar:   $21D0), (CData:'uArr'; CChar:     $21D1),
    (CData:'rArr'; CChar:    $21D2), (CData:'dArr'; CChar:   $21D3), (CData:'hArr'; CChar:     $21D4),
    (CData:'forall'; CChar:  $2200), (CData:'part'; CChar:   $2202), (CData:'exist'; CChar:    $2203),
    (CData:'empty'; CChar:   $2205), (CData:'nabla'; CChar:  $2207), (CData:'isin'; CChar:     $2208),
    (CData:'notin'; CChar:   $2209), (CData:'ni'; CChar:     $220B), (CData:'prod'; CChar:     $220F),
    (CData:'sum'; CChar:     $2211), (CData:'minus'; CChar:  $2212), (CData:'lowast'; CChar:   $2217),
    (CData:'radic'; CChar:   $221A), (CData:'prop'; CChar:   $221D), (CData:'infin'; CChar:    $221E),
    (CData:'ang'; CChar:     $2220), (CData:'and'; CChar:    $2227), (CData:'or'; CChar:       $2228),
    (CData:'cap'; CChar:     $2229), (CData:'cup'; CChar:    $222A), (CData:'int'; CChar:      $222B),
    (CData:'there4'; CChar:  $2234), (CData:'sim'; CChar:    $223C), (CData:'cong'; CChar:     $2245),
    (CData:'asymp'; CChar:   $224B), (CData:'ne'; CChar:     $2260), (CData:'equiv'; CChar:    $2261),
    (CData:'le'; CChar:      $2264), (CData:'ge'; CChar:     $2265), (CData:'sub'; CChar:      $2282),
    (CData:'sup'; CChar:     $2283), (CData:'nsub'; CChar:   $2284), (CData:'sube'; CChar:     $2286),
    (CData:'supe'; CChar:    $2287), (CData:'oplus'; CChar:  $2295), (CData:'otimes'; CChar:   $2297),
    (CData:'perp'; CChar:    $22A5), (CData:'sdot'; CChar:   $22C5), (CData:'lceil'; CChar:    $2308),
    (CData:'rceil'; CChar:   $2309), (CData:'lfloor'; CChar: $230A), (CData:'rfloor'; CChar:   $230B),
    (CData:'lang'; CChar:    $2329), (CData:'rang'; CChar:   $232A), (CData:'loz'; CChar:      $25CA),
    (CData:'spades'; CChar:  $2660), (CData:'clubs'; CChar:  $2663), (CData:'hearts'; CChar:   $2665),
    (CData:'diams'; CChar:  $2666));
{$ENDIF}

var i : integer;
begin
  (* try character entities *)
  for i :=0 to CTableCnt-1 do
    if (CTable[i].CData = CData) then
    begin
      ch := TRVAnsiChar(CTable[i].CChar);
      Result := HTMLStringToString(ch, False, 1252);
      Exit;
    end;
  {$IFDEF RVUNICODESTR}
  for i :=0 to CTableCntW-1 do
    if (CTableW[i].CData = CData) then
    begin
      Result := WideChar(CTableW[i].CChar);
      Exit;
    end;
  {$ENDIF}
  (* still here ? try number *)
  //Result := chr(StrToIntDef( CData, ord(' ') ));
  Result := '';
  if (CData<>'') and (CData[1]='#') then begin
    CharCode := StrToIntDef(Copy(CData, 2, Length(CData)-1), 0);
    if (CharCode<=0) or (CharCode>$FFFF) then
      exit;
    {$IFDEF RVUNICODESTR}
    Result := WideChar(CharCode);
    {$ELSE}
    Result := RVU_UnicodeToAnsi(FCodePage, RVU_GetRawUnicode(WideChar(CharCode)));
    {$ENDIF}
    exit;
  end;
  Result := '&'+CData+';';
end;
{------------------------------------------------------------------------------}
function THtmlParser.ConvertAllCData(const s: String): String;
var i,j: Integer;
    CData: String;
begin
  Result := s;
  i := 1;
  while i<Length(Result) do begin
    if Result[i]='&' then begin
      j := i+1;
      while (j<=Length(Result)) and CharInSet(Result[j], ['a'..'z','A'..'Z','0'..'9']) do
        inc(j);
      if (j<>i+1) and (j<=Length(Result)) and (Result[j]=';') then begin
        CData := Copy(Result, i+1, j-i-1);
        CData := ConvertCData(CData);
        Delete(Result, i, j-i+1);
        Insert(CData, Result, i);
        i := i+Length(CData);
        continue;
      end;
    end;
    inc(i);
  end;
end;
{------------------------------------------------------------------------------}
procedure THtmlParser.Next;
var
  s2: TRVRawByteString;
  c, s: String;
  PCurr, PEnd: PRVAnsiChar;
  I, Pos: longint;
begin
  PEnd := P;
  if PEnd^ = '<' then
  begin
    Inc(PEnd);
    if (PEnd[0] = '!') and (PEnd[1] = '-') and (PEnd[2] = '-') then
    begin
      Inc(PEnd, 3);
//      while PEnd <> #0 do
      while (PEnd[0] <> #0) and (PEnd[1] <> #0) and (PEnd[2] <> #0) do
      begin
        if (PEnd[0] = '-') and (PEnd[1] = '-') and (PEnd[2] = '>') then
        begin
          Inc(PEnd, 3);
          SetLength(s2, TRVNativeUInt(PEnd) - TRVNativeUInt(P));
          Move(P^, s2[1], Length(s2));
          FTagNameX := HTMLStringToString(s2, IsUTF8, FCodePage);
          FTagParamsX.Clear;
          Break;
        end;
        Inc(PEnd);
      end; // while
      FCurrBlock := cbComment;
    end {if}
    else
    begin
      FTagParamsX.Clear;
      PCurr := PEnd;
      Inc(PEnd);
      while not (PEnd^ in (['/', #0, '>'] + Spaces)) do Inc(PEnd);
      SetString(s2, PCurr, TRVNativeUInt(PEnd) - TRVNativeUInt(PCurr));
      FTagNameX := HTMLStringToString(s2, IsUTF8, FCodePage);
      if PEnd^ in Spaces then
      begin
        Inc(PEnd);
        while (PEnd^ <> '>') and (pend^ <> #0) do
        begin
          while PEnd^ in Spaces do Inc(PEnd);

          PCurr := PEnd;

          while not (PEnd^ in ([#0, '>', '='] + Spaces)) do Inc(PEnd);

          SetString(c, PCurr, TRVNativeUInt(PEnd) - TRVNativeUInt(PCurr));
          c := lowercase(c);


          (* filter optional spaces before = *)
          while (PEnd^ in Spaces) do Inc(PEnd);

          if PEnd^ = '=' then
          begin
            Inc(PEnd);

            (* filter optional spaces after = *)
            while (PEnd^ in Spaces) do Inc(PEnd);

            if PEnd^ = '"' then
            begin
              Inc(PEnd);
              PCurr := PEnd;
              while not (PEnd^ in [#0, '"']) do Inc(PEnd);
              SetString(s2, PCurr, TRVNativeUInt(PEnd) - TRVNativeUInt(PCurr));
              s := HTMLStringToString(s2, IsUTF8, FCodePage);
              if PEnd^ = '"' then Inc(PEnd);
              if PEnd^ = ' ' then Inc(PEnd);
              FTagParamsX.Add(c + '=' + s);
            end
            else
            if PEnd^ = '''' then
            begin
              Inc(PEnd);
              PCurr := PEnd;
              while not (PEnd^ in [#0, '''']) do Inc(PEnd);
              SetString(s2, PCurr, TRVNativeUInt(PEnd) - TRVNativeUInt(PCurr));
              s := HTMLStringToString(s2, IsUTF8, FCodePage);
              if PEnd^ = '''' then Inc(PEnd);
              if PEnd^ = ' ' then Inc(PEnd);
              FTagParamsX.Add(c + '=' + s);
            end
            else
            begin
              PCurr := PEnd;

              while not (PEnd^ in ([#0, '>'] + Spaces)) do Inc(PEnd);

              SetString(s2, PCurr, TRVNativeUInt(PEnd) - TRVNativeUInt(PCurr));
              s := HTMLStringToString(s2, IsUTF8, FCodePage);
              if PEnd^ = ' ' then Inc(PEnd);
              FTagParamsX.Add(c + '=' + s);
            end;
          end
          else
          begin
            FTagParamsX.Add(c);
          end;
        end;
      end; //if
      if PEnd^ = '/' then Inc(PEnd);
      if PEnd^ = '>' then Inc(PEnd);
      FCurrBlock := cbTag;
    end; // else if
  end
  else
  begin
    //	 while not (PEnd^ in [#0, '<']) do Inc(PEnd);			//hrr out
    while not (PEnd^ in [#0, '<', #10]) do Inc(PEnd);    //hrr LineCount...
    if PEnd^ = #10 then
    begin
      Inc(FCurrLine);
      Inc(PEnd);
    end;                                  //...hrr LineCount

    SetLength(s2, TRVNativeUInt(PEnd) - TRVNativeUInt(P));
    Move(P^, s2[1], Length(s2));
    s := HTMLStringToString(s2, IsUTF8, FCodePage);
    SetLength(FTagNameX, Length(s));
    Pos := 0;
    I := 1;
    while I <= Length(S) do
    begin
      Inc(Pos);
      if CharInSet(s[i], Spaces) and (FRemoveSpaces) then
      begin
        Inc(I);
        while (I <= Length(s)) and CharInSet(s[i], Spaces) do
          Inc(I);
        FTagNameX[Pos] := ' ';
      end
      else if s[i] = '&' then
      begin
        Inc(i);
        c := '';
        while (I<= Length(s)) and (s[i] <> ';') do
        begin
          c := c + s[i];
          Inc(I);
        end;
        if (i<=Length(s)) and (s[i] = ';') then inc(i);

        c := ConvertCData( c );
        if c='' then
        begin
          Delete(FTagNameX, Pos, 1);
          dec(Pos);
        end
        else
        begin
          FTagNameX[Pos] := c[1];
          if Length(c)>1 then
          begin
            Insert(Copy(c,2,Length(c)), FTagNameX, Pos+1);
            inc(Pos, Length(c)-1);
          end;
        end;
      end else
      begin
        FTagNameX[Pos] :=s[i];
        Inc(I);
      end;
    end;

    SetLength(FTagNameX, Pos);
    if (Pos > 0) and (CharInSet(s[1],Spaces) and FLastWasSpace) and (FRemoveSpaces) then
      Delete(FTagNameX, 1, 1);
    if FTagNameX <> '' then
      FLastWasSpace := FTagNameX[Length(FTagNameX)] = ' ';
    if not FRemoveSpaces then
      FTagNameX := StringReplace(FTagNameX, #13#10, #13, [rfReplaceAll]);
    FTagParamsX.Clear;
    FCurrBlock := cbText;
  end;
  P := PEnd;
end;
{=============================== TContainerDataType ===========================}
constructor TContainerDataType.Create(Collection: TCollection);
begin
  inherited;
  ParaStack := TRVList.Create;
  PrevFont := TList.Create;
  IndentStack := TRVList.Create;
  IndentListStyle := -1;
end;
{------------------------------------------------------------------------------}
destructor TContainerDataType.Destroy;
begin
  IndentStack.Free;
  PrevFont.Free;
  ParaStack.Free;
  inherited;
end;
{------------------------------------------------------------------------------}
function TContainerDataType.FindAlignment(AAlignment: TRVAlignment;
  AOutlineLevel: Integer): integer;
var ParaInfo: TParaInfo;
begin
  (* make sure RichView has an applicable style *)
  ParaInfo := TParaInfo.Create(nil);
  try
    ParaInfo.Assign(Importer.FViewer.Style.ParaStyles[0]);
    ParaInfo.Alignment := AAlignment;
    ParaInfo.OutlineLevel := AOutlineLevel;
    Result := Importer.FViewer.Style.ParaStyles.FindSuchStyle(0, ParaInfo,
      RVAllParaInfoProperties);
    if (Result < 0) then
      with Importer.FViewer.Style.ParaStyles.Add do begin
        Result := Index;
        Assign(ParaInfo);
        Standard := False;
      end;
  finally
    ParaInfo.Free;
  end;
end;
{------------------------------------------------------------------------------}
procedure TContainerDataType.EnterAlignment(AAlignment: TRVAlignment;
  AOutlineLevel: Integer; ParaStyleNo: integer = -1);
var Item: TRvHtmlParaStackItem;
begin
  (* add alignment to stack *)
  Item := TRvHtmlParaStackItem.Create;
  Item.Alignment := AAlignment;
  Item.OutlineLevel := AOutlineLevel;
  Item.ParaNo := ParaStyleNo;  
  ParaStack.Add(Item);
  (* get richview style *)
  if ParaStyleNo<0 then
    ParaStyle := FindAlignment(AAlignment, AOutlineLevel)
  else
    ParaStyle := ParaStyleNo;
end;
{------------------------------------------------------------------------------}
{
procedure TContainerDataType.EnterAlignmentFake;
var Item: TRvHtmlParaStackItem;
begin
  Item := TRvHtmlParaStackItem.Create;
  Item.ParaNo := -1;
  if ParaStack.Count=0 then
    Item.Alignment := rvaLeft
  else
    Item.Alignment := TRvHtmlParaStackItem(ParaStack.Items[ParaStack.Count-1]).Alignment;
  ParaStack.Add(Item);
end;
}
{------------------------------------------------------------------------------}
procedure TContainerDataType.LeaveAlignment;
var Item: TRvHtmlParaStackItem;
begin
  if ParaStack.Count > 1 then
    ParaStack.Delete(ParaStack.Count-1);
  Item := TRvHtmlParaStackItem(ParaStack.Items[ParaStack.Count-1]);
  if Item.ParaNo<0 then
    ParaStyle := FindAlignment(Item.Alignment, Item.OutlineLevel)
  else
    ParaStyle := Item.ParaNo;
end;
{------------------------------------------------------------------------------}
function TContainerDataType.GetCurAlignment: TRVAlignment;
begin
  Result := TRvHtmlParaStackItem(ParaStack.Items[ParaStack.Count-1]).Alignment;
end;
{
function TContainerDataType.GetCurOutlineLevel: Integer;
begin
  Result := TRvHtmlParaStackItem(ParaStack.Items[ParaStack.Count-1]).OutlineLevel;
end;
}
{------------------------------------------------------------------------------}

procedure TContainerDataType.EnterIndent(AIndentType: TIndentType;
  AType: TListType; AStartAt: Integer);
var
  Indent : TContainerIndentType;
  Level  : TRVListLevel;
begin
  (* add new list to stack *)
  Indent := TContainerIndentType.Create;
  IndentStack.Add( Indent );
  (* init list *)
  Indent.IndentType := AIndentType;
  Indent.ListType   := AType;
  Indent.ListValue  := AStartAt;
  (* always create a ListLevel when enforcing them *)
  if Importer.EnforceListLevels then
    with Importer.FViewer.Style do
    begin
      if IndentListStyle = -1 then
        IndentListStyle := ListStyles.Add.Index;
      Level := ListStyles[IndentListStyle].Levels.Add;
      Importer.InitListLevel( Level, AType, Level.Index+1 );
    end
  else
    with Importer.FViewer.Style do
    begin
      if ListStyles.Count>0 then
        IndentListStyle:=0;
    end;
end;
{------------------------------------------------------------------------------}
procedure TContainerDataType.LeaveIndent(AIndentType: TIndentType);
var
  i,j : integer;
begin
  (* search for parent tag, and remove all others if found *)
  for i := IndentStack.Count-1 downto 0 do
    with TContainerIndentType(IndentStack[i]) do
      if (IndentType = AIndentType) then
      begin
        for j := IndentStack.Count-1 downto i do
          IndentStack.Delete(j);

        if IndentStack.Count = 0 then
          IndentListStyle := -1;

        CurLIType := ltNone;
        Exit;
      end;
end;
{------------------------------------------------------------------------------}
function TContainerDataType.ListCount: integer;
var i : integer;
begin
  Result := 0;
  for i := 0 to IndentStack.Count-1 do
    if (TContainerIndentType(IndentStack[i]).IndentType in [llUL, llOL]) then
      inc(Result);
end;
{============================ THtmlImporter ===================================}
constructor TRvHtmlImporter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDefaultFontSize := 12;
  FDefaultCFontSize := 10;
  FDefaultFontName := 'Times New Roman';
  FDefaultCFontName := 'Courier New';
  FDefaultColor := clWindowText;
  FDefaultLinkCol := clBlue;
  FDefaultLinkStyle := [fsUnderline];
  FDefaultBGColor := clWindow;
  FStartSelection := -1;
  FEndSelection := -1;
  FClearDocument := True;
  FEnforceListLevels := True;
  FOptions := [rvhtmloAutoHyperlinks, rvhtmloBasePathLinks];
end;
{------------------------------------------------------------------------------}
function ExtractDomain(const URL: String): String;
var p,p2: Integer;
    Prefix: String;
begin
  Result := URL;
  p := Pos('://', Result);
  if p>0 then begin
    Prefix := Copy(Result, 1, p+2);
    Result := Copy(Result, p+3, Length(Result));
    end
  else
    Prefix := '';
  p := Pos('/', Result);
  p2 := Pos('\', Result);
  if (p2>0) and (p2<p) then
    p := p2;
  p2 := Pos('?', Result);
  if (p2>0) and (p2<p) then
    p := p2;
  if p>0 then
    Result := Copy(Result, 1, p-1);
  Result := Prefix+Result;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.LoadClipboardHtml(const Data: TRVRawByteString);
const
  StrSignature = 'Version:';
  StrStartHTML = 'StartHTML:';
  StrEndHTML = 'EndHTML:';
  StrStartFragment = 'StartFragment:';
  StrEndFragment = 'EndFragment:';
  StrSourceURL = 'SourceURL:';
  //StrStartSelection = 'StartSelection:';
  //StrEndSelection = 'EndSelection';
var
  p1, p2: integer;
  s1, s2, s3: TRVRawByteString;
  OldEncoding: TRVHTMLEncoding;
  {..........................................................}
  function ReadString(const Key: TRVRawByteString): TRVRawByteString;
  var
    p: integer;
  begin
    Result := '';
    p := RVPos(Key, Data);
    if p = 0 then
      exit;
    inc(p, Length(Key));
    Result := '';
    while not (Data[p] in [#10, #13]) do
    begin
      Result := Result + Data[p];
      inc(p);
    end;
  end;
  {..........................................................}
  function ReadInteger(const Key: TRVRawByteString): integer;
  var
    p: integer;
    val: TRVRawByteString;
  begin
    p := RVPos(Key, Data);
    if p = 0 then
      abort;
    inc(p, Length(Key));
    val := '';
    while Data[p] in ['0'..'9'] do
    begin
      val := val + Data[p];
      inc(p);
    end;
    Result := RVStrToInt(Val);
  end;
  {..........................................................}
  function ExtractPath(const S: string): String;
  var l,i: Integer;
      NotFinish: Boolean;
  begin
    l := Length(S);
    while l > 0 do begin
      if ((S[l]='\') or (S[l]='/') or (S[l]=':')) and (ByteType(S, l)<>mbTrailByte) then begin
        NotFinish := False;
        if S[l]=':' then begin
          for i := 1 to l-1 do
            if ((S[l]='\') or (S[l]='/') or (S[l]=':')) and (ByteType(S, l)<>mbTrailByte) then begin
              NotFinish := True;
              break;
            end;
        end;
        if not NotFinish then
          break;
      end;
      dec(l);
    end;
    if l<0 then
      l := 0;
    Result := Copy(S, 1, l);
  end;
  {..........................................................}
begin
  if Copy(Data, 1, Length(StrSignature)) <> StrSignature then
    abort;
  p1 := ReadInteger(StrStartHTML);
  if p1 < 0 then
    p1 := ReadInteger(StrStartFragment);
  p2 := ReadInteger(StrEndHTML);
  if p2 < 0 then
    p2 := ReadInteger(StrEndFragment);
  BasePath := ConvertUTF8ToString(ReadString(StrSourceURL));
  if Pos('file://', BasePath) = 1 then
    BasePath := Copy(BasePath, 8,Length(BasePath));
  if (Pos(':', BasePath)<>0) and (BasePath[1]='/') then
    Delete(FBasePath, 1, 1);
  BasePath := ExtractPath(BasePath);
  FStartSelection := ReadInteger(StrStartFragment) - p1;
  FEndSelection := ReadInteger(StrEndFragment) - p1;
  s3 := Copy(Data, p1, p2 - p1);
  s1 := Copy(s3, 1, FStartSelection + 1);
  s2 := Copy(s3, FStartSelection + 2, FEndSelection - FStartSelection);
  s3 := Copy(s3, FEndSelection + 2, Length(s3));
  FStartSelection := Length(s1) - 1;
  FEndSelection := Length(s1) + Length(s2) - 1;
  OldEncoding := Encoding;
  try
    Encoding := rvhtmleUTF8;
    FActualEncoding := Encoding;
    LoadHtmlSelection(s1 + s2 + s3, FStartSelection, FEndSelection);
  finally
    Encoding := OldEncoding;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.LoadFromClipboard;
var
  cf: cardinal;
  Data: TRVAnsiString;
  mem: cardinal;
  ptr: Pointer;
begin
  cf := RegisterClipboardFormat('HTML Format');
  if not IsClipboardFormatAvailable(cf) then
    exit;
  OpenClipboard(0);
  try
    mem := GetClipboardData(cf);
    SetLength(Data, GlobalSize(mem));
    ptr := GlobalLock(mem);
    Move(ptr^, PRVAnsiChar(Data)^, Length(Data));
    GlobalUnlock(mem);
  finally
    CloseClipboard;
  end;
  LoadClipboardHtml(Data);
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.LoadHtmlSelection(const Data: TRVRawByteString;
  StartSelection, EndSelection: integer);
begin
  FStartSelection := StartSelection;
  FEndSelection := EndSelection;
  try
    LoadHtml(Data);
  finally
    FStartSelection := -1;
    FEndSelection := -1;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.CheckSelection(Parser: TObject; RVData: TCustomRVData;
  RVT: TRVTableItemInfo);
var ARVData, APrevRVData: TCustomRVData;
    f: Boolean;
begin
  if (FStartSelection >= 0) then
  begin
    if (FStartSelNo < 0) and (THtmlParser(Parser).BytesParsed >= FStartSelection) then
    begin
      FNextTimeCloseSelection := False;
      FSelRVData := RVData;
      if FSelRVData = nil then
      begin
        FSelRVData := TRVTableRowsH(RVT.Rows).FMainRVData;
        FStartSelNo := FSelRVData.ItemCount - 1;
        FEndSelNo := FSelRVData.ItemCount - 1;
      end
      else
        FStartSelNo := RVData.Items.Count;
    end;
    if (FEndSelNo < 0) then
    begin
      if FNextTimeCloseSelection and (RVData <> nil) then
      begin
        if FSelRVData=RVData then
          FEndSelNo := FSelRVData.ItemCount - 1
        else begin
          f := False;
          APrevRVData := FSelRVData;
          ARVData := APrevRVData.GetAbsoluteParentData;
          while ARVData<>nil do begin
            if ARVData = RVData then begin
              FSelRVData := RVData;
              if APrevRVData is TRVTableCellData then begin
                FStartSelNo := TRVTableCellData(APrevRVData).GetTable.GetMyItemNo;
                FEndSelNo := FStartSelNo;
                end
              else begin
                FStartSelNo := 0;
                FEndSelNo := FSelRVData.ItemCount-1;
              end;
              f := True;
              break;
            end;
            APrevRVData := ARVData;
            ARVData := ARVData.GetAbsoluteParentData;
          end;
          if not f then begin
            APrevRVData := RVData;
            ARVData := APrevRVData.GetAbsoluteParentData;
            while ARVData<>nil do begin
              if ARVData = FSelRVData then begin
                if APrevRVData is TRVTableCellData then begin
                  FStartSelNo := TRVTableCellData(APrevRVData).GetTable.GetMyItemNo;
                  FEndSelNo := FStartSelNo;
                  end
                else begin
                  FStartSelNo := 0;
                  FEndSelNo := FSelRVData.ItemCount-1;
                end;
                break;
              end;
              APrevRVData := ARVData;
              ARVData := ARVData.GetAbsoluteParentData;
            end;
          end;
        end;
        FNextTimeCloseSelection := False;
      end
      else if (THtmlParser(Parser).BytesParsed > FEndSelection) then
        FNextTimeCloseSelection := True;
    end;
  end;
end;
{------------------------------------------------------------------------------}
function TRvHtmlImporter.GetSelectionBounds(var RVData: TCustomRVData;
  var StartNo, EndNo: integer): boolean;
begin
  RVData := FSelRVData;
  Result := RVData <> nil;
  if RVData = nil then
    exit;
  StartNo := FStartSelNo;
  if (FEndSelNo < 0) or (FEndSelNo >= RVData.Items.Count) then
    FEndSelNo := RVData.Items.Count - 1;
  EndNo := FEndSelNo;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.InitListLevel(ALevel: TRVListLevel; AType: TListType;
  ADepth: integer);
begin

  with ALevel do
  begin
    FirstIndent := 0;
    MarkerIndent := FViewer.Style.PixelsToUnits(DefMarkerIndentPix) * ADepth;
    LeftIndent := MarkerIndent + FViewer.Style.PixelsToUnits(DefLeftIndentPix);
    FormatString := '%' + IntToStr(Index) + ':s.';
    Font.Size := FDefaultFontSize;
    case AType of
      ltNumber:
        begin
          ListType := rvlstDecimal;
        end;
      ltDisc:
        begin
          ListType := rvlstBullet;
          Font.Name := 'Symbol';
          Font.Charset := SYMBOL_CHARSET;
          FormatString := #$B7;
        end;
      ltCircle:
        begin
          ListType := rvlstBullet;
          Font.Name := 'Courier New';
          Font.Charset := ANSI_CHARSET;
          FormatString := 'o';
        end;
      ltSquare:
        begin
          ListType := rvlstBullet;
          Font.Name := 'Wingdings';
          Font.Charset := SYMBOL_CHARSET;
          FormatString := #$A7;
        end;
      ltLowerAlpha:
        begin
          ListType := rvlstLowerAlpha;
        end;
      ltUpperAlpha:
        begin
          ListType := rvlstUpperAlpha;
        end;
      ltLowerRoman:
        begin
          ListType := rvlstLowerRoman;
        end;
      ltUpperRoman:
        begin
          ListType := rvlstUpperRoman;
        end;
      ltNone:
        begin
          FormatString := '';
        end;
    end;
  end;
end;
{------------------------------------------------------------------------------}
function TRvHtmlImporter.ReadImage(const src: String; Width, Height: integer;
  NoFail: Boolean): TGraphic;
var FileName: String;
begin
  Result := nil;
  if src='' then
    exit;
  FileName := src;
  if Assigned(OnImageRequired2) then
    OnImageRequired2(Self, FileName, Width, Height, Result);
  if Result<>nil then
    exit;
  FileName := GetFullFileName(Trim(FileName));
  if Assigned(RichView.OnImportPicture) then
    RichView.OnImportPicture(RichView, FileName, Width, Height, Result);
  if Result<>nil then
    exit;
  Result := RVGraphicHandler.LoadFromFile(FileName);
  if Result = nil then begin
    FileName := RV_DecodeURL(FileName, False);
    Result := RVGraphicHandler.LoadFromFile(FileName);
  end;
  if Result<>nil then
    exit;  
  if NoFail then begin
    Result := RVGraphicHandler.CreateGraphic(TGraphicClass(RichView.Style.InvalidPicture.Graphic.ClassType));
    Result.Assign(RichView.Style.InvalidPicture.Graphic);
  end;
end;
{------------------------------------------------------------------------------}
function TRvHtmlImporter.GetBitmap(gr: TGraphic): TBitmap;
begin
  Result := nil;
  if gr=nil then
    exit;
  Result := TBitmap.Create;
  try
    Result.Assign(gr);
  except
    Result.Width := gr.Width;
    Result.Height := gr.Height;
    Result.Canvas.Draw(0,0,gr);
  end;
end;
{------------------------------------------------------------------------------}
function TRvHtmlImporter.GetFullFileName(const FileName: String): String;
begin
  if ((Pos(':', FileName)>0) and
     (RVIsURL(FileName) or ((Length(FileName)>1) and (FileName[2]=':')))) or
     ((FileName<>'') and (FileName[1]='#')) then
    Result := FileName
  else if (FileName<>'') and ((FileName[1]='\') or (FileName[1]='/')) then
    Result := ExtractDomain(BasePath)+FileName
  else
    Result := BasePath+FileName;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if (Operation=opRemove) and (AComponent=FViewer) then
    FViewer := nil;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.SetFixedCSSStyles(const Value: Boolean);
begin
  FFixedCSSStyles := Value;
  if Value then
    FEnforceListLevels:=false;
end;
{------------------------------------------------------------------------------}
procedure TRvHtmlImporter.LoadHtml(const Data: TRVRawByteString);
var
  RVData: TCustomRVData;
  FLinkCol: TColor;
  Parser: THtmlParser;
  CurrLink, ImageFileName: string;
  ItemName: TRVRawByteString;
  BeforeLinkFont: longint;
  CurrListStyle, ListLevel: longint;
  CurrStyle, DefStyle, TmpStyle: longint;
  GraphicItem: TRVGraphicItemInfo;
  Link : TRVTag;
  lt: TListType;
  ts: String;
  s: String;
  OutlineLevel: Integer;
  CF: TFont;
  SSType: TRVSubSuperScriptType;
  Gr: TGraphic;
  bmp: TBitmap;
  I: longint;
  RVT: TRVTableItemInfoH;

  ContainerStack: TCollection;
  CurContainer: TContainerDataType;

  CurParaStyle: integer;  // keeps track of last used ParaStyle
  {..........................................................}
  procedure RemoveLineBreaks(var s: String);
  var i: Integer;
  begin
    for i := Length(s) downto 1 do
      if (s[i]=#10) or (s[i]=#13) then
        Delete(s, i, 1);
  end;
  {..........................................................}
  function GetList(AType: TListType; ADepth: integer): integer;
  var
    Level: TRVListLevel;
    i : integer;
    dpth : TRVStyleLength;

    function IsSame( lvl : TRVListLevel ): boolean;
    begin
      Result := False;
      if lvl.MarkerIndent = dpth then
        case AType of
          ltNumber     : Result := (lvl.ListType = rvlstDecimal);
          ltLowerAlpha : Result := (lvl.ListType = rvlstLowerAlpha);
          ltUpperAlpha : Result := (lvl.ListType = rvlstUpperAlpha);
          ltLowerRoman : Result := (lvl.ListType = rvlstLowerRoman);
          ltUpperRoman : Result := (lvl.ListType = rvlstUpperRoman);
          ltDisc       : Result := (lvl.ListType = rvlstBullet) and (lvl.FormatString=#$B7);
          ltCircle     : Result := (lvl.ListType = rvlstBullet) and (lvl.FormatString='o');
          ltSquare     : Result := (lvl.ListType = rvlstBullet) and (lvl.FormatString=#$A7);
          ltNone       : Result := (lvl.FormatString = '');
        end
    end;
    begin
      dpth := FViewer.Style.PixelsToUnits(DefMarkerIndentPix) * ADepth;

      if EnforceListLevels then
      begin
        (* Level already created; just retrieve it here *)
        Result := ADepth-1;
      end
      else
      begin
        (* find applicable level *)
        with FViewer.Style.ListStyles[CurContainer.IndentListStyle] do
         for i := 0 to Levels.Count-1 do
           if IsSame( Levels[i] ) then
           begin
             Result := Levels[i].Index;
             Exit; //LEAVE NOW
           end;
        (* still here ? then create new *)
       if FixedCSSStyles then begin
         with FViewer.Style.ListStyles[CurContainer.IndentListStyle] do begin
           if Levels.Count > 0 then
             Result:=levels[0].index
           else begin
             Level := FViewer.Style.ListStyles[CurContainer.IndentListStyle].Levels.Add;
             InitListLevel( Level, AType, ADepth );
             Result := Level.Index;
           end;
         end;
         end
        else begin
          Level := FViewer.Style.ListStyles[CurContainer.IndentListStyle].Levels.Add;
          InitListLevel( Level, AType, ADepth );
          Result := Level.Index;
        end;
      end;
    end;
  {..........................................................}
  procedure SetListMarker;
  var i : integer;
  begin
    if CurContainer.IndentListStyle = -1 then
      with FViewer.Style.ListStyles.Add do begin
        CurContainer.IndentListStyle := Index;
        Standard := False;
      end;

    i := GetList( CurContainer.CurLIType, CurContainer.IndentStack.Count );
    if i >= 0 then
      CurContainer.RVData.SetListMarkerInfo( -1, CurContainer.IndentListStyle,
        i,  CurContainer.CurLIValue, CurContainer.ParaStyle, True);
  end;
  {..........................................................}
  function GetCurParaNo: Integer;
  begin
    (* do we need to switch parastyles ? *)
    if (CurParaStyle <> CurContainer.ParaStyle) then
    begin
      CurParaStyle := CurContainer.ParaStyle;
      if (CurContainer.IndentStack.Count > 0) then
      begin
        (* Apply indentation *)
        SetListMarker;
        Result := -1;   // ListMarker already causes para-change
      end
      else
      begin
        (* no indentation *)
        Result := CurContainer.ParaStyle;
      end
    end
    else
      Result := -1;
  end;
  {..........................................................}
  procedure writets;
  var
    Para : integer;
    ts2: TRVRawByteString;
    ts3: String;
  begin
    (* Make sure even empty <LI> tags generate an indentation *)
    if CurContainer.CurHadLI then
    begin
      SetListMarker;
      CurContainer.CurLIType := ltNone;
      CurParaStyle := CurContainer.ParaStyle;
      CurContainer.CurHadLI := False;
    end;
    (* is there any text ? *)
    if Trim(ts) <> '' then
//    if ts <> '' then
    begin
//      ParagraphWritten := True;
      (* do we need to switch parastyles ? *)
      Para := GetCurParaNo;
      ts3 := ts;
      if Para>=0 then
        ts3 := TrimLeft(ts3);
      {$IFDEF RVUNICODESTR}
      ts2 := RVU_GetRawUnicode(ts3);
      {$ELSE}
      ts2 := ts3;
      {$ENDIF}
      (* setup hyperlink *)
      if CurrLink <> '' then
      begin
        RemoveLineBreaks(CurrLink);
        CurrLink := Parser.ConvertAllCData(CurrLink);
        if rvhtmloBasePathLinks in Options then
          CurrLink := GetFullFileName(CurrLink);
        if (rvhtmloAutoHyperlinks in Options)
           {$IFDEF RVOLDTAGS}and (rvoTagsArePChars in RichView.Options){$ENDIF}  then
          Link := {$IFDEF RVOLDTAGS}integer(StrNew(PChar(CurrLink))){$ELSE}CurrLink{$ENDIF}
        else begin
          Link := RVEMPTYTAG;
          CurContainer.RVData.ReadHyperlink(CurrLink, '',rvlfHTML,
            CurrStyle,Link,ts2);
        end;
      end
      else
        Link := RVEMPTYTAG;
      (* write data *)
      CurContainer.RVData.AddNLTag(ts, CurrStyle, Para, Link);
      Inc(FCharCount, Length(ts));      //hrr CharCount
      ts := '';
    end;
  end;
  {..........................................................}
  procedure writeCR;
  begin
    (* write a forced break *)
    if Assigned(RVData) then
      RVData.AddNLTag('', CurrStyle, CurContainer.ParaStyle, RVEMPTYTAG);
  end;
  {..........................................................}
  procedure WriteTSCR;
    { Added for PRE }
  var
    s: String;
    i1: integer;
  begin
    //if (ts = ' ') and (GetCurParaNo>=0) then
    //  s := ''
    //else
      s := ts;
    i1 := 1;
    while i1 <= Length(s) do
    begin
      if (s[i1] = #13) then
      begin
        ts := copy(s, 1, i1 - 1);
        if (i1+1<=Length(s)) and (s[i1 + 1] = #10) then
          Delete(s, 1, i1 + 1)
        else
          Delete(s, 1, i1);
        WriteTS;
        WriteCR;
        i1 := 0;
      end
      else if s[i1] = #10 then
      begin
        ts := copy(s, 1, i1 + 1);
        Delete(s, 1, i1);
        WriteTS;
        WriteCR;
        i1 := 0;
      end;
      inc(i1);
    end;
    ts := s;
    WriteTS;
  end;
  {..........................................................}
  procedure FindFont(b, alink: boolean);
  var
    I: longint;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    LStyleTemplateId: TRVStyleTemplateId;
    {$ENDIF}
  begin
    if RVData = nil then Exit;
    if b then CurContainer.PrevFont.Add(Pointer(CurrStyle))
    else
    begin
      if CurContainer.PrevFont.Count > 0 then
        CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
    end;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    if FViewer.UseStyleTemplates then begin
      LStyleTemplateId := FViewer.Style.StyleTemplates.FindByName(RVHYPERLINKSTYLETEMPLATENAME);
      if LStyleTemplateId>=0 then
        LStyleTemplateId := FViewer.Style.StyleTemplates[LStyleTemplateId].Id;
      end
    else
      LStyleTemplateId := -1;
    {$ENDIF}
    with FViewer.Style.TextStyles do
    begin
      for i := 0 to Count - 1 do
      begin
        with Items[i] do
        begin
          if (Size = cf.size) and
            (Color = cf.Color) and
            (jump = alink) and
            (FontName = CF.Name) and
            (Style = CF.Style) and
            (SubSuperScriptType=SSType)
            {$IFNDEF RVDONOTUSESTYLETEMPLATES}
            and (not FViewer.UseStyleTemplates or (StyleTemplateId=LStyleTemplateId))
            {$ENDIF}
            then
          begin
            CurrStyle := I;
            Exit;
          end;
        end;
      end; // for
      with Add do
      begin
        FontName := CF.Name;
        Size := CF.Size;
        Color := CF.Color;
        Style := CF.Style;
        Jump := alink;
        {$IFNDEF RVDONOTUSESTYLETEMPLATES}
        if FViewer.UseStyleTemplates then
          StyleTemplateId := LStyleTemplateId;
        {$ENDIF}
        CurrStyle := Index;
        SubSuperScriptType := SSType;
        Unicode := Items[0].Unicode;
        Standard := False;
      end;
    end;
  end;
  {..........................................................}
  procedure EnterContainer(ADefTextStyle: integer; ALevel: TContainerLevel;
    ADefAlignment: TRVAlignment);
  var
    BgClr: TColor;
    VAlign: TRVCellVAlign;
    RVData: TCustomRVData;
  begin
    VAlign := rvcMiddle;
    RVData := nil;
    if Assigned(CurContainer) then
    begin
      (* Sync current container with CurrStyle *)
      CurContainer.TextStyle := CurrStyle;
      (* new container wil get current BgColor as default *)
      BgClr := CurContainer.BgColor;

      if CurContainer.Level in [clTable, clRow] then
        VAlign := CurContainer.VALign;

      RVData := CurContainer.RVData;
    end
    else
      BgClr := DefaultBGColor;
    (* place a new container on the stack, and make this one the current *)
    CurContainer := TContainerDataType(ContainerStack.Add);
    CurContainer.Importer := Self;
    CurContainer.Level := ALevel;
    CurContainer.TextStyle := ADefTextStyle;
    CurContainer.BgColor := BgClr;
    CurContainer.VAlign := VAlign;
    CurContainer.RVData := RVData;
    (* reset current parastyle *)
    CurParaStyle := -1;
    CurrListStyle := -1;
    ListLevel := 0;
    (* sync CurrStyle with new container settings *)
    CurrStyle := CurContainer.TextStyle;
    FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
    SSType := FViewer.Style.TextStyles[CurrStyle].SubSuperScriptType;
    (* make sure we have a default alignment *)
    CurContainer.EnterAlignment(ADefAlignment, 0);
  end;
  {..........................................................}
  procedure LeaveContainer(ALevel: TContainerLevel);
  begin
    (* remove un-balanced levels *)
    while (ContainerStack.Count > 1) and (ALevel < CurContainer.Level) do
    begin
      ContainerStack.Items[ContainerStack.Count - 1].Free;
      CurContainer := TContainerDataType(ContainerStack.Items[ContainerStack.Count - 1]);
    end;
    (* remove container from stack *)
    if (ContainerStack.Count > 1) and (ALevel = CurContainer.Level) then
    begin
      ContainerStack.Items[ContainerStack.Count - 1].Free;
      CurContainer := TContainerDataType(ContainerStack.Items[ContainerStack.Count - 1]);
    end;
    (* reset current parastyle *)
    CurParaStyle := -1;
    (* sync CurrStyle with new container settings *)
    CurrStyle := CurContainer.TextStyle;
    FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
    SSType := FViewer.Style.TextStyles[CurrStyle].SubSuperScriptType;
  end;
  {..........................................................}
  procedure DoTable;
  var
    i: integer;
    s: String;
    FileName: String;
    RVStyle: TRVStyle;
  begin
    if RVData = nil then Exit;
    RVStyle := RVData.GetRVStyle;
    RVT := TRVTableItemInfoH.Create(RVData);
    CurContainer.RVT := RVT;
    RVT.BorderWidth :=  RVStyle.PixelsToUnits(StrToIntDef(gv(Parser.TagParamsX, 'border'), 0));
    if RVT.BorderWidth > 0 then
      RVT.CellBorderWidth := RVStyle.PixelsToUnits(1);
    i := RVStyle.PixelsToUnits(StrToIntDef(gv(Parser.TagParamsX, 'cellspacing'), 2));
    rvt.BorderVSpacing := i;
    rvt.BorderHSpacing := i;
    rvt.CellVSpacing := i;
    rvt.CellHSpacing := i;
    rvt.BorderStyle := rvtbRaisedColor;
    rvt.CellBorderStyle := rvtbLoweredColor;
    RVT.CellPadding := RVStyle.PixelsToUnits(StrToIntDef(gv(Parser.TagParamsX, 'cellpadding'), 1));
    s := gv(Parser.TagParamsX, 'bordercolor');
    if s <> '' then
    begin
      RVT.BorderColor := GetHtmlColor(s);
      rvt.BorderStyle := rvtbColor;
      rvt.CellBorderStyle := rvtbColor;
    end
    else
    begin      
      RVT.BorderColor := clBtnShadow;
      rvt.BorderStyle := rvtbRaisedColor;
      rvt.CellBorderStyle := rvtbLoweredColor;
      s := gv(Parser.TagParamsX, 'style');
      if s <> '' then
      begin
        s := StyleValue(s, 'border-color');
        if s <> '' then
        begin
          RVT.BorderColor := GetHtmlColor(s);
          rvt.BorderStyle := rvtbColor;
          rvt.CellBorderStyle := rvtbColor;
        end;
      end;
    end;
    RVT.CellBorderColor := RVT.BorderColor;
    s := gv(Parser.TagParamsX, 'bordercolorlight');
    if s <> '' then
      RVT.BorderLightColor := GetHtmlColor(s)
    else
    begin
      if ColorToRGB(rvt.Color) = ColorToRGB(clBtnHighlight) then
        RVT.BorderLightColor := clBtnFace
      else
        RVT.BorderLightColor := clBtnHighlight
    end;
    RVT.CellBorderLightColor := RVT.BorderLightColor;
    if not (rvhtmloIgnorePictures in Options) then begin
      s := gv(Parser.TagParamsX, 'background');
      gr := ReadImage(s, 0, 0, False);
      RVT.BackgroundImage := gr;
      if gr<>nil then begin
        RVT.BackgroundStyle := rvbsTiled;
        FileName := s;
        if rvhtmloBasePathLinks in Options then
          FileName := GetFullFileName(FileName);
        RVT.BackgroundImageFileName := FileName;
      end;
      gr.Free;
    end;
    s := gv(Parser.TagParamsX, 'width');
    if s <> '' then
    begin
      if Pos('%', s) > 0 then
      begin
        Delete(s, Pos('%', s), 1);
        i := -abs(StrToIntDef(s, 100));
      end
      else
        i := RVStyle.PixelsToUnits(abs(StrToIntDef(s, 0)));
      RVT.BestWidth := i;
    end
    else
      RVT.BestWidth := 0;
    (* set table alignment *)
    RVT.ParaNo := CurContainer.FindAlignment(GetAlignParam(Parser.TagParamsX,
      CurContainer.CurAlignment), 0);
    (* new table -> new container, with default TextStyle and alignment *)
    EnterContainer(DefStyle, clTable, rvaLeft);
    s := lowercase(gv(Parser.TagParamsX, 'valign'));
    if s = 'top' then CurContainer.VAlign := rvcTop
    else if s = 'bottom' then CurContainer.VAlign := rvcBottom
    else if s = 'middle' then CurContainer.VAlign := rvcMiddle;
    s := gv(Parser.TagParamsX, 'bgcolor');
    if s <> ''
    then
      CurContainer.BgColor := GetHtmlColor(s)
    else
    begin
      CurContainer.bgColor := clNone;
      s := gv(Parser.TagParamsX, 'style');
      if s <> ''
      then
      begin
        s := StyleValue(s, 'background-color');
        if (s <> '') and (CompareText(s, 'transparent')<>0) then
          CurContainer.BgColor := GetHtmlColor(s);
      end;
    end;
    RVT.Color := CurContainer.BgColor;
    RVData := nil;
    RVT.CurrRow := -1;
    RVT.CurrCol := -1;
  end;
  {..........................................................}
  procedure DoMerge(ColSpan, RowSpan: integer);
  var
    i, colc, j: longint;
  begin
    if ColSpan < 1 then ColSpan := 1;
    if RowSpan < 1 then RowSpan := 1;
    if (ColSpan > 1) or (RowSpan > 1) then
    begin
      while rvt.CurrRow + RowSpan > RVt.Rows.Count do
      begin
        RVt.Rows.Add(Rvt.Rows[0].Count);
      end;
      colc := RVT.CurrCol + ColSpan - RVT.Rows[0].Count;
      for i := 0 to RVT.Rows.Count - 1 do
      begin
        for j := 0 to ColC - 1 do
        begin
          RVT.Rows[i].Add;
        end;
      end;
      RVT.MergeCells(RVT.CurrRow, RVT.CurrCol, ColSpan, rowSpan, True);
    end;
  end;
  {..........................................................}
  procedure CheckForNewLine;            //hrr LineCount, OnNewLine...
  begin
    if FLineCount = Parser.CurrLine then exit;
    FLineCount := Parser.CurrLine;
    if Assigned(FOnNewLine) then
      FOnNewLine(Self);
  end;                          //...hrr LineCount, OnNewLine
  {..........................................................}
var
  w,h : integer;
  line: String;
  LData :TRVRawByteString;
  posi :integer;
  LUnicode: Boolean;
begin
  SetLength(RVFonts, 0);
  SetLength(RVParas, 0);
  FActualEncoding := FEncoding;
  FStartSelNo := -1;
  FEndSelNo := -1;
  FSelRVData := nil;
  if FClearDocument then
    FViewer.Clear;
  RVT := nil;
  CurrListStyle := -1;
  ListLevel := 0;
  FClrListStyle := -1;
  BeforeLinkFont := 0;
  //  PrevFont := TList.Create;
  if FViewer.Color=clNone then
    FViewer.Style.Color := FDefaultBGColor
  else
    FViewer.Color := FDefaultBGColor;
  if not (rvhtmloIgnorePictures in Options) then
    FViewer.BackgroundBitmap := nil;
  FBackgroundRead := False;
  FLinkCol := DefaultLinkCol;
  CF := TFont.Create;
  SSType := rvsssNormal;
  ContainerStack := TCollection.Create(TContainerDataType);
  CurContainer := nil;
  try
    if not FixedCSSStyles then
    begin
      LUnicode := FViewer.Style.TextStyles[0].Unicode;
      if FClearDocument then
      begin
        FViewer.Style.TextStyles.Clear;
        FViewer.Style.ListStyles.Clear;
        FViewer.Style.ParaStyles.Clear;
        with FViewer.Style.ParaStyles.Add do
        begin
        end;
      end;
      with FViewer.Style.TextStyles.Add do
      begin
        FontName := DefaultFontName;
        Size := DefaultFontSize;
        Color := DefaultColor;
        AssignTo(CF);
        SSType := rvsssNormal;
        DefStyle := 0;
        Unicode := LUnicode;
     end;
    end;
    (* create container for main document *)
    EnterContainer(DefStyle, clMain, rvaLeft);
    LData := Data;
    if  (Length(LData)>=3) and (LData[1]=#$EF) and (LData[2]=#$BB) and (LData[3]=#$BF) then begin
      LData := Copy(LData, 4, Length(LData)-3);
      FActualEncoding := rvhtmleUTF8;
    end;
    Parser := THtmlParser.Create(LData);
    Parser.FCodePage := GetCodePage;
    Parser.FEncoding := FActualEncoding;
    Parser.LastWasSpace := True;
    {<dt>}
    CurrStyle := DefStyle;
    try
      RVData := FViewer.RVData;
      CurContainer.RVData := RVData;
      while not Parser.Done do
      begin
        Parser.Next;
        if (FStartSelection>=0) and (FStartSelNo<0) or FNextTimeCloseSelection then
          WriteTSCR;
        CheckSelection(Parser, RVData, RVT);
        CheckForNewLine;                    //hrr OnNewLine
        case Parser.CurrBlock of
          cbComment:                        //hrr OnComment...
            begin
              if Assigned(FOnComment) then
                FOnComment(Self, Parser.TagNameX);
            end;                          //...hrr OnComment
          cbTag:
            begin
              s := LowerCase(Parser.TagNameX);
              if (s = 'title') then
              begin
                FTitle := '';
                Parser.Next;
                CheckForNewLine;                //hrr OnNewLine
                // support for <title></title>
                while ((Parser.CurrBlock <> cbTag) or
                  (LowerCase(Parser.FTagNameX) <> '/title')) and not (Parser.Done) do
                begin
                  FTitle := FTitle + Parser.TagNameX;
                  Parser.Next;
                end;
{
                repeat
                  FTitle := FTitle + Parser.TagName;
						Parser.Next;
                until ((Parser.CurrBlock = cbTag) and (lowercase(Parser.tagName) = '/title')) or (Parser.Done);
}
                Parser.LastWasSpace := True;
              end
              else if (s = 'body') then
              begin
                s := gv(Parser.TagParamsX, 'bgcolor');
                if s <> '' then
                begin
                  FViewer.Color := GetHtmlColor(s);
                end;
                s := gv(Parser.TagParamsX, 'text');
                if s <> '' then
                begin
                  Fviewer.Style.TextStyles[DefStyle].Color := GetHtmlColor(s);
                end;
                s := gv(Parser.TagParamsX, 'link');
                if s <> '' then
                begin
                  FLinkCol := GetHtmlColor(s);
                end;
                if not FBackgroundRead and not (rvhtmloIgnorePictures in Options) then begin
                  s := gv(Parser.TagParamsX, 'background');
                  gr := ReadImage(s, 0, 0, False);
                  bmp := GetBitmap(gr);
                  Fviewer.BackgroundBitmap := bmp;
                  bmp.Free;
                  if gr<>nil then
                    Fviewer.BackgroundStyle := bsTiled;
                  gr.Free;
                end;
              end
              else if (s = '/tr') then
              begin
                (* make sure data is written in case of unbalanced td-tags *)
                WriteTSCR;
                if RVData is TRVTableCellData then
                begin
                  RVT := TRVTableItemInfoH(TRVTableCellData(RVData).GetTable);
                  RVData := nil;
                end;
                (* leaving row -> leave container *)
                LeaveContainer(clRow);
              end
              else if (s = 'tr') then
              begin
                WriteTSCR;
                if RVData is TRVTableCellData then
                begin
                  RVT := TRVTableItemInfoH(TRVTableCellData(RVData).GetTable);
                  RVData := nil;
                end;
                (* close any unbalanced tags *)
                LeaveContainer(clRow);
                if RvData = nil then
                begin
                  inc(RVT.CurrRow);
                  if RVT.CurrRow >= RVT.Rows.Count then
                  begin
                    RVT.Rows.Add(RVT.Rows[0].Count);
                  end;
                  RVT.CurrCol := -1;
                end;
                (* new row -> new container, with default TextStyle and optional align-param *)
                EnterContainer(DefStyle, clRow, GetAlignParam(Parser.TagParamsX, rvaLeft));
                s := LowerCase(gv(Parser.TagParamsX, 'valign'));
                if s = 'top' then CurContainer.VAlign := rvcTop
                else if s = 'bottom' then CurContainer.VAlign := rvcBottom
                else if s = 'middle' then CurContainer.VAlign := rvcMiddle;
                s := gv(Parser.TagParamsX, 'bgcolor');
                if s <> '' then CurContainer.BgColor := GetHtmlColor(s);
//                s := gv(Parser.TagParams, 'background');
//                if s <> '' then CurContainer.BgColor := GetHtmlColor(s);
              end
              else if (s = '/td') or (s = '/th') then
              begin
                WriteTSCR;
                if RVData is TRVTableCellData then
                begin
                  RVT := TRVTableItemInfoH(TRVTableCellData(RVData).GetTable);
                  RVData := nil;
                  (* leaving cell -> leave container *)
                  LeaveContainer(clCol);
                  Parser.LastWasSpace := True;
                  if s = '/th' then
                  begin
                    if CurContainer.PrevFont.Count > 0 then
                    begin
                      CurrStyle := longint(CurContainer.PrevFont[CurContainer.PrevFont.Count
                        - 1]);
                      CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
                      FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
                      SSType := FViewer.Style.TextStyles[CurrStyle].SubSuperScriptType;
                    end;
                  end;
                end;
              end
              else if (s = 'td') or (s = 'th') then
              begin
                WriteTSCR;
                if RVData is TRVTableCellData then
                begin
                  RVT := TRVTableItemInfoH(TRVTableCellData(RVData).GetTable);
                  RVData := nil;
                end;
                (* close any unbalanced tags *)
                LeaveContainer(clCol);
                Parser.LastWasSpace := True;
                ts := '';
                if (RVData = nil) then
                begin
                  (* support for TD without TR tag *)
                  if (RVT.CurrRow < 0) then
                  begin
                    EnterContainer( DefStyle, clRow, rvaLeft );
                    RVT.CurrRow := 0;
                  end;
                  while RVData = nil do
                  begin
                    Inc(RVT.CurrCol);
                    if RVT.CurrCol >= RVT.Rows[RVT.CurrRow].Count then
                      for i := 0 to RVT.Rows.Count - 1 do
                        RVT.Rows[i].Add;
                    RVData := RVT.Cells[RVT.CurrRow, RVT.CurrCol];
                  end;
                  RVData := RVT.Cells[RVT.CurrRow, RVT.CurrCol];
                  RVData.Clear; // remove default item added by RichView
                  (* new cell -> new container, with default TextStyle and optional align-param *)
                  (* CurAlignment is default handles <tr align= <x>> behaviour                  *)
                  EnterContainer(DefStyle, clCol,
                    GetAlignParam(Parser.TagParamsX, CurContainer.CurAlignment));
                  CurContainer.RVData := RVData; // new default RVData
                  if s = 'th' then
                  begin
                    CF.Style := Cf.Style + [fsBold];
                    FindFont(True, CurrLink <> '');
                  end;
                  s := lowercase(gv(Parser.TagParamsX, 'valign'));
                  if s = 'top' then CurContainer.VAlign := rvcTop
                  else if s = 'bottom' then CurContainer.VAlign := rvcBottom
                  else if s = 'middle' then CurContainer.VAlign := rvcMiddle;
                  TRVTableCellData(RVData).VAlign := CurContainer.VAlign;
                  if not (rvhtmloIgnorePictures in Options) then begin
                    gr := ReadImage(gv(Parser.TagParamsX, 'background'), 0, 0, False);
                    TRVTableCellData(RVData).BackgroundImage := gr;
                    if gr<>nil then begin
                      TRVTableCellData(RVData).BackgroundStyle := rvbsTiled;
                      ImageFileName := gv(Parser.TagParamsX, 'background');
                      if rvhtmloBasePathLinks in Options then
                        ImageFileName := GetFullFileName(ImageFileName);
                      TRVTableCellData(RVData).BackgroundImageFileName :=
                        ImageFileName;
                    end;
                    gr.Free;
                  end;
                  DoMerge(StrToIntDef(gv(Parser.TagParamsX, 'colspan'), 1),
                    StrToIntDef(gv(Parser.TagParamsX, 'rowspan'), 1));
                  s := gv(Parser.TagParamsX, 'bordercolor');
                  if s <> '' then
                    TRVTableCellData(RVData).BorderColor := GetHtmlColor(s);
                  s := gv(Parser.TagParamsX, 'bgcolor');
                  if s <> '' then
                    CurContainer.BgColor := GetHtmlColor(s)
                  else begin
                    s := gv(Parser.TagParamsX, 'style');
                    if s <> '' then begin
                      s := StyleValue(s, 'background-color');
                      if (s <> '') and (CompareText(s, 'transparent')<>0) then
                        CurContainer.BgColor := GetHtmlColor(s);
                    end;
                  end;
                  TRVTableCellData(RVData).Color := CurContainer.BgColor;
                  s := gv(Parser.TagParamsX, 'width');
                  if s <> '' then
                  begin
                    if Pos('%', s) > 0 then
                    begin
                      Delete(s, Pos('%', s), 1);
                      i := -abs(StrToIntDef(s, 100));;
                    end
                    else
                      i := RVData.GetRVStyle.PixelsToUnits(abs(StrToIntDef(s, 0)));
                    TRVTableCellData(RVData).BestWidth := i;
                  end;
                  s := gv(Parser.TagParamsX, 'height');
                  if s <> '' then
                    TRVTableCellData(RVData).BestHeight := RVData.GetRVStyle.PixelsToUnits(StrToIntDef(s, 0));
                  s := gv(Parser.TagParamsX, 'style');
                  if s <> '' then
                  begin
                    s := StyleValue(s, 'border-width');
                    if s <> '' then
                      TRVTableCellData(RVData).GetTable.CellBorderWidth := GetCSSValue(s, RVData.GetRVStyle, 0);
                  end;
                end;
              end
              else if (S = '/table') then
              begin
                WriteTSCR;
                LeaveContainer(clTable);
                RVData := CurContainer.RVData;
                (* actually add table to RV *)
                if Assigned(CurContainer.RVT) then
                begin
                  RVData.AddItem('', CurContainer.RVT);
                  CurContainer.RVT := nil;
                end;
                RVT := nil;
                CurParaStyle := -1;
              end
              else if (s = 'table') then
              begin
                WriteTSCR;
                (* add new table *)
                DoTable;
              end
              else if s = 'ol' then
              begin
                WriteTSCR;
                lt := GetListType( gv(Parser.TagParamsX, 'type'), ltNumber );
                CurContainer.EnterIndent( llOL, lt, StrToIntDef(gv(Parser.TagParamsX, 'start'), 1) );
                (* insert extra LF on main level *)
//                if (not (RVData is TRVTableCellData)) and (CurContainer.ListCount = 1) then writeCR;
                CurParaStyle := -1;
              end
              else if (s = '/ol') then
              begin
                WriteTSCR;
                (* insert extra LF on main level *)
//                if (not (RVData is TRVTableCellData)) and (CurContainer.ListCount = 1) then writeCR;
                CurContainer.LeaveIndent( llOL );
                (* next paragraph *)
                CurParaStyle := -1;
              end
              else if s = 'ul' then
              begin
                WriteTSCR;
                case CurContainer.ListCount of
                  0  : lt := ltDisc;
                  1  : lt := ltCircle;
                  else lt := ltSquare;
                end;
                lt := GetListType( gv(Parser.TagParamsX, 'type'), lt );
                CurContainer.EnterIndent( llUL, lt, StrToIntDef(gv(Parser.TagParamsX, 'start'), 1) );
                (* insert extra LF on main level *)
//                if (not (RVData is TRVTableCellData)) and (CurContainer.ListCount = 1) then writeCR;
                CurParaStyle := -1;
              end
              else if (s = '/ul') then
              begin
                WriteTSCR;
                (* insert extra LF on main level *)
//                if (not (RVData is TRVTableCellData)) and (CurContainer.ListCount = 1) then writeCR;
                CurContainer.LeaveIndent( llUL );
                (* next paragraph *)
                CurParaStyle := -1;
              end
              else if (s = 'li') then
              begin
                WriteTSCR;
                with CurContainer do
                begin
                  lt := ltDisc;
                  i  := StrToIntDef( gv(Parser.TagParamsX, 'value'), 0 );
                  if IndentStack.Count > 0 then
                    with TContainerIndentType(IndentStack[IndentStack.Count-1]) do
                    begin
                      lt := ListType;
                      if (i > 0) then
                        ListValue := i
                      else
                        i := ListValue;
                      inc(ListValue);
                    end;
                  CurLIType  := GetListType( gv(Parser.TagParamsX, 'type'), lt );
                  CurLIValue := i;
                  CurHadLI   := True;
                end;
                CurParaStyle := -1;
              end
              else if (s = 'blockquote') then
              begin
                WriteTSCR;
                CurContainer.EnterIndent(llBQ, ltNone, 1);
                CurParaStyle := -1
              end
              else if (s = '/blockquote') then
              begin
                WriteTSCR;
                CurContainer.LeaveIndent(llBQ);
                CurParaStyle := -1;
              end
              else if s = 'pre' then
              begin
                WriteTSCR;
                Parser.RemoveSpaces := False;
                CF.Name := DefaultCFontName;
                CF.Size := DefaultCFontSize;
                FindFont(True, CurrLink <> '');
              end
              else if s = '/pre' then
              begin
                Parser.RemoveSpaces := True;
                WriteTSCR;
                if CurContainer.PrevFont.Count > 0 then
                begin
                  CurrStyle := longint(CurContainer.PrevFont[CurContainer.PrevFont.Count - 1]);
                  CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
                  FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
                end;
              end
              else if s = 'hr' then
              begin
                WriteTSCR;
                if RVData <> nil then
                  RVData.AddBreak;
              end
              else if s = 'img' then
              begin
                WriteTSCR;
                if not (rvhtmloIgnorePictures in Options) then begin
                  gr := ReadImage(gv(Parser.TagParamsX, 'src'),
                    StrToIntDef(gv(Parser.TagParamsX, 'width'), 0),
                    StrToIntDef(gv(Parser.TagParamsX, 'height'), 0), True);
                  if gr <> nil then
                  begin
                    w := -1;
                    h := -1;
                    try
                      s := gv(Parser.TagParamsX, 'width');
                      if s <> '' then // img-tag specifically stated width
                        w := StrtoIntDef(s, gr.Width);
                      s := gv(Parser.TagParamsX, 'height');
                      if s <> '' then // img-tag specifically stated height
                        h := StrtoIntDef(s, gr.Height);
                      if (w<0) and (h>0) and (gr.Height>0) then
                        w := Round(h*gr.Width/gr.Height)
                      else  if (h<0) and (w>0) and (gr.Width>0) then
                        h := Round(w*gr.Height/gr.Width);
                    except
                    end;
                    if (RVData <> nil) then
                    begin
                     //TODO: <li> behaviour ? add marker ?
                      ImageFileName := gv(Parser.TagParamsX, 'src');
                      if (ImageFileName<>'') and (rvhtmloBasePathLinks in Options) then
                        ImageFileName := GetFullFileName(ImageFileName);
                      if rvhtmloImgSrcToImageName in Options then
                        ItemName := TRVAnsiString(ImageFileName)
                      else
                        ItemName := '';
                      TmpStyle := rvsPicture;
                      Link := RVEMPTYTAG;
                      if (CurrLink <> '') then
                      begin
                        RemoveLineBreaks(CurrLink);
                        if rvhtmloBasePathLinks in Options then
                          CurrLink := GetFullFileName(CurrLink);
                        if (rvhtmloAutoHyperlinks in Options)
                          {$IFDEF RVOLDTAGS}and (rvoTagsArePChars in RichView.Options){$ENDIF} then begin
                          Link :=
                            {$IFDEF RVOLDTAGS}integer(StrNew(PChar(CurrLink))){$ELSE}CurrLink{$ENDIF};
                          TmpStyle := rvsHotPicture;
                          end
                        else begin
                          RVData.ReadHyperlink(CurrLink, '', rvlfHTML, TmpStyle,
                            Link, ItemName);
                        end;
                      end;
                      GraphicItem := CreateRichViewItem(TmpStyle, RVData) as TRVGraphicItemInfo;
                      GraphicItem.Tag := Link;
                      GraphicItem.Image := gr;
                      GraphicItem.ParaNo := GetCurParaNo;
                      s := LowerCase(gv(Parser.TagParamsX, 'align'));
                      if s='middle' then
                        GraphicItem.VAlign := rvvaMiddle
                      else if s='abstop' then
                        GraphicItem.VAlign := rvvaAbsTop
                      else if s='absbottom' then
                        GraphicItem.VAlign := rvvaAbsBottom
                      else if s='absmiddle' then
                        GraphicItem.VAlign := rvvaAbsMiddle
                      else if s='left' then
                        GraphicItem.VAlign := rvvaLeft
                      else if s='right' then
                        GraphicItem.VAlign := rvvaRight;                      
                      RVData.AddItemR(ItemName, GraphicItem, True);
                      (* set sizes as specified in img-tag *)
                      if (w > 0) then
                        RVData.SetItemExtraIntProperty( rvData.ItemCount-1,
                          rvepImageWidth, RVData.GetRVStyle.PixelsToUnits(w) );
                      if (h > 0) then
                        RVData.SetItemExtraIntProperty( rvData.ItemCount-1,
                          rvepImageHeight, RVData.GetRVStyle.PixelsToUnits(h) );
                      RVData.SetItemExtraIntProperty( rvData.ItemCount-1,
                        rvepSpacing, 0 );
                      RVData.SetItemExtraStrProperty( rvData.ItemCount-1,
                        rvespAlt, gv(Parser.TagParamsX, 'alt'));
                      RVData.SetItemExtraStrProperty( rvData.ItemCount-1,
                        rvespImageFileName, ImageFileName);
                    end
                    else
                      gr.Free;
                  end;
                end;
              end
              else if s = 'a' then
              begin
                WriteTSCR;
                s := gv(Parser.TagParamsX, 'href');
                if s <> '' then
                begin
                  if CurrLink <> '' then Continue;
                  CurrLink := s;
                  BeforeLinkFont := CurContainer.PrevFont.Count;
                  CF.Color := FLinkCol;
                  CF.Style := DefaultLinkStyle;
                  if not FixedCSSStyles then
                    FindFont(True, CurrLink <> '')
                  else
                  begin
                    CurContainer.PrevFont.Add(Pointer(CurrStyle));
                    with FViewer.Style.TextStyles do
                      for i := 0 to Count - 1 do
                        if Items[i].Jump then
                        begin
                          CurrStyle := I;
                          break;
                        end;
                  end;
                end
                else
                begin
                  ImageFileName := gv(Parser.TagParamsX, 'name');
                  RemoveLineBreaks(ImageFileName);
                  if RVData <> nil then
                    RVData.AddNamedCheckpointExTag(ImageFileName, False, RVEMPTYTAG);
                end;
              end
              else if s = '/a' then
              begin
                if CurrLink <> '' then
                begin
                  WriteTSCR;
                  CurrLink := '';
                  if BeforeLinkFont < CurContainer.PrevFont.Count then
                    CurrStyle := longint(CurContainer.PrevFont[BeforeLinkFont])
                  else
                    CurrStyle := DefStyle;
                  while CurContainer.PrevFont.Count > BeforeLinkFont do
                  begin
                    CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
                  end;
                  BeforeLinkFont := DefStyle;
                  FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
                end;
              end
              else if (s = 'div') then
              begin
                WriteTSCR;
                (* Enter new alignment *)
                //if CurContainer.IndentStack.Count = 0
                //then
                begin
                  if not FixedCSSStyles then
                    CurContainer.EnterAlignment(GetAlignParam(Parser.TagParamsX,
                      CurContainer.CurAlignment), 0);
                  CurParaStyle := -1;  // reset current parastyle
                end;
              end
              else if (s = 'center') then
              begin
                WriteTSCR;
                (* treat as <div align=center> *)
                if not FixedCSSStyles then
                  CurContainer.EnterAlignment(rvaCenter, 0);
                CurParaStyle := -1;  // reset current parastyle
              end
              else if (s = '/div') or (s = '/center') then
              begin
                Parser.LastWasSpace := True;
                WriteTSCR;
                //if CurContainer.IndentStack.Count = 0
                //then
                begin
                  (* Leave alignment *)
                  CurContainer.LeaveAlignment;
                  CurParaStyle := -1;  // reset current parastyle
                end;
              end
              else if (s = 'br') then
              begin
                Parser.LastWasSpace := True;
                WriteTSCR;
                if (CurParaStyle = -1) then   
                  WriteCR;
                CurParaStyle := -1;
              end
              else if s = '/p' then
              begin
                WriteTSCR; // ts suggested to delete this line, but it is incorrect
                CurContainer.LeaveAlignment;
                CurParaStyle := -1;  // reset current parastyle
              end
              else if (length(s) = 3) and (s[1] = '/') and (s[2] = 'h') and CharInSet(s[3], ['1'..'6']) then
              begin
                WriteTSCR;
                //writeCR;
                if not FixedCSSStyles then
                  CurContainer.LeaveAlignment;
                CurParaStyle := -1;
                if CurContainer.PrevFont.Count > 0 then
                begin
                  CurrStyle := longint(CurContainer.PrevFont[CurContainer.PrevFont.Count - 1]);
                  CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
                  FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
                end;
              end
              else if (s='p') or ((length(s) = 2) and (s[1] = 'h') and (CharInSet(s[2],['1'..'6']))) then
              begin
                if Length(s)=2 then
                  OutlineLevel := ord(s[2])-ord('0')
                else
                  OutlineLevel := 0;
//                ParagraphWritten := False;
                Parser.LastWasSpace := True;
                WriteTSCR;
                if FixedCSSStyles then begin
                  s := gv(Parser.TagParamsX, 'class');
                  if Pos('rvps',s)=1 then begin
                    s :=copy(s,5,length(s)-4);
                    i := StrToIntDef(s,0);
                    if i>Fviewer.Style.ParaStyles.Count-1 then
                      i := 0;
                    end
                  else
                    i := 0;
                  CurContainer.Parastyle:=i;
                  end
                else begin
                  // writeCR;
                  s := gv(Parser.TagParamsX, 'class');
                  if (Length(s) > 4) and (Copy(s, 1, 4) = 'rvps') then begin
                    s := Copy(s, 5, Length(s) - 4);
                    i := StrToIntDef(s, -1);
                    if i >= Length(RVParas) then
                      i := -1;
                    if i>=0 then begin
                      if RVParas[i].TAlign = 'left' then
                        CurContainer.EnterAlignment(rvaLeft, OutlineLevel, RVParas[i].ParaStyleNo)
                      else if RVParas[i].TAlign = 'right' then
                        CurContainer.EnterAlignment(rvaRight, OutlineLevel, RVParas[i].ParaStyleNo)
                      else if RVParas[i].TAlign = 'center' then
                        CurContainer.EnterAlignment(rvaCenter, OutlineLevel, RVParas[i].ParaStyleNo)
                      else if RVParas[i].TAlign = 'justify' then
                        CurContainer.EnterAlignment(rvaJustify, OutlineLevel, RVParas[i].ParaStyleNo)
                      else
                        CurContainer.EnterAlignment(CurContainer.CurAlignment, OutlineLevel, RVParas[i].ParaStyleNo);
                    end;
                    end
                  else begin
                    CurContainer.EnterAlignment(GetAlignParam(Parser.TagParamsX,
                      CurContainer.CurAlignment), OutlineLevel);
                  end;
                end;
                CurParaStyle := -1;  // reset current parastyle
                // ts := ts + #13#13;
                if OutlineLevel>0 then begin
                  CF.Size := FontSize[1 + (abs(7 - OutlineLevel) mod 7)];
                  CF.Style := [fsbold];
                  FindFont(True, CurrLink <> '');
                end;
              end
              else if (s = 'b') or (S = 'strong') then
              begin
                WriteTSCR;
                CF.Style := Cf.Style + [fsBold];
                FindFont(True, CurrLink <> '');
              end
              else if (s = '/b') or (s = '/strong') then
              begin
                WriteTSCR;
                CF.Style := Cf.Style - [fsBold];
                FindFont(False, CurrLink <> '');
              end
              else if (s = 'i') or (s = 'em') then
              begin
                WriteTSCR;
                CF.Style := Cf.Style + [fsItalic];
                FindFont(True, CurrLink <> '');
              end
              else if (s = '/i') or (s = '/em') then
              begin
                WriteTSCR;
                CF.Style := Cf.Style - [fsItalic];
                FindFont(False, CurrLink <> '');
              end
              else if s = 'u' then
              begin
                WriteTSCR;
                CF.Style := Cf.Style + [fsUnderline];
                FindFont(True, CurrLink <> '');
              end
              else if s = '/u' then
              begin
                WriteTSCR;
                CF.Style := Cf.Style - [fsUnderline];
                FindFont(False, CurrLink <> '');
              end
              else if (s = 'strike') or (s = 's') or (s = 'del') then
              begin
                WriteTSCR;
                CF.Style := Cf.Style + [fsStrikeout];
                FindFont(True, CurrLink <> '');
              end
              else if (s = '/strike') or (s = '/s') or (s = '/del') then
              begin
                WriteTSCR;
                CF.Style := Cf.Style - [fsStrikeOut];
                FindFont(False, CurrLink <> '');
              end
              else if s = 'sup' then
              begin
                WriteTSCR;
                SSType := rvsssSuperScript;
                //CF.Size := Round(CF.Size/2);
                FindFont(True, CurrLink <> '');
              end
              else if s = '/sup' then
              begin
                WriteTSCR;
                SSType := rvsssNormal;
                //CF.Size := CF.Size*2;
                FindFont(False, CurrLink <> '');
              end
              else if s = 'sub' then
              begin
                WriteTSCR;
                SSType := rvsssSubscript;
                //CF.Size := Round(CF.Size/2);
                FindFont(True, CurrLink <> '');
              end
              else if s = '/sub' then
              begin
                WriteTSCR;
                SSType := rvsssNormal;
                //CF.Size := CF.Size*2;
                FindFont(False, CurrLink <> '');
              end
              else if s = '/font' then
              begin
                WriteTSCR;
                if CurContainer.PrevFont.Count > 0 then
                begin
                  CurrStyle := longint(CurContainer.PrevFont[CurContainer.PrevFont.Count - 1]);
                  CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
                  FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
                end;
              end
              else if s = 'font' then
              begin
                WriteTSCR;
                s := gv(Parser.TagParamsX, 'face');
                if Pos(',', s)>0 then
                  s := Copy(s, 1, Pos(',', s)-1);
                if s <> '' then
                  CF.Name := s;
                s := gv(Parser.TagParamsX, 'size');
                if s <> '' then
                begin
                  if s[1] = '+' then CF.Size := FtoHF(HFtoF(CF.Size) +
                      StrToIntDef(Copy(S, 2, MaxInt), 0))
                  else if s[1] = '-' then CF.Size := FtoHF(HFtoF(CF.Size) - StrToIntDef(Copy(S, 2, MaxInt),
                      0))
                  else
                    CF.Size := FtoHF(StrToIntDef(s, 3));
                end;
                s := gv(Parser.TagParamsX, 'color');
                if s <> '' then CF.Color := GetHtmlColor(s);
                FindFont(True, CurrLink <> '');
              end
              else if s = '/span' then
              begin
                WriteTSCR; // ts suggested to delete this line, but it is incorrect
                if CurContainer.PrevFont.Count > 0 then
                begin
                  CurrStyle := longint(CurContainer.PrevFont[CurContainer.PrevFont.Count - 1]);
                  CurContainer.PrevFont.Delete(CurContainer.PrevFont.Count - 1);
                  FViewer.Style.TextStyles[CurrStyle].AssignTo(CF);
                  SSType := FViewer.Style.TextStyles[CurrStyle].SubSuperScriptType;
                end;
              end
              else if s = 'span' then
              begin
                WriteTSCR;
                s := gv(Parser.TagParamsX, 'class');
                if (Length(s)>4) and (LowerCase(Copy(s, 1, 4))='rvts') then
                begin
                  s := Copy(s, 5, Length(s)-4);
                  i := StrToIntDef(s, -1);
                  if i >= Length(RVFonts) then
                    i := -1;
                  if i >= 0 then
                  begin
                    {if Length(RVFonts) = 0 then begin
                      CF.assign(FViewer.Style.TextStyles[i]);
                      SSType := FViewer.Style.TextStyles[i].SubSuperScriptType;
                      end
                    else }begin
                      CF.assign(FViewer.Style.TextStyles[RVFonts[i].TextStyleNo]);
                      SSType := FViewer.Style.TextStyles[RVFonts[i].TextStyleNo].SubSuperScriptType;
                    end;
                    if RVData <> nil then begin
                      CurContainer.PrevFont.Add(Pointer(CurrStyle));
                      CurrStyle := RVFonts[i].TextStyleNo;
                    end;
                  end;
                end;
              end
              else if (s = 'script') then
              begin
                (* no support for scripts; filter all data within this tag *)
                Parser.Next;
                while ((Parser.CurrBlock <> cbTag) or
                  (lowercase(Parser.tagNameX) <> '/script')) and not (Parser.Done) do
                  Parser.Next;
              end
              else if (s = 'style') then
              begin
                (* no support for style; filter all data within this tag *)
                Parser.Next;
                while ((Parser.CurrBlock <> cbTag) or
                  (lowercase(Parser.tagNameX) <> '/style')) and not (Parser.Done) do
                begin
                  AddRVStyles(LowerCase(Parser.TagNameX), FViewer);
                  if FixedCSSStyles then begin
                    line := lowercase(parser.tagnameX);
                    posi := Pos('background-color:',line);
                    if posi>0 then
                    begin
                      posi:=posi+16;
                      line:=trim(copy(line,posi+1,length(line)-posi));
                      posi:= Pos(';',line);
                      line:=copy(line,1,posi-1);
                      if line<>'' then richview.Style.color:=GetHtmlColor(line);
                    end;
                  end;
                  Parser.Next;
                end;
              end
              else if (s = 'noembed') then
              begin
                (* no support for scripts; filter all data within this tag *)
                Parser.Next;
                while ((Parser.CurrBlock <> cbTag) or
                  (lowercase(Parser.tagNameX) <> '/noembed')) and not (Parser.Done) do
                  Parser.Next;
              end;
            end;
          cbText:
            begin
              ts := ts + Parser.TagNameX;
              if FSeparateLines then WriteTS;    //hrr SeparateLines
            end;
        end;
      end;
    finally
      Parser.Free;
    end;
    WriteTSCR;
    (* support for unbalanced TABLE tag *)
    LeaveContainer(clTable);
    RVData := CurContainer.RVData;
    if Assigned(CurContainer.RVT) then
    begin
      RVData.AddItem('', CurContainer.RVT);
      CurContainer.RVT := nil;
    end;
    // FViewer.Format;
  finally
    ContainerStack.Free;
    //    PrevFont.Free;
    CF.Free;
  end;
end;

{============================= TRVRawByteStringList ===========================}
procedure TRVRawByteStringList.Add(const s: TRVRawByteString);
var NewCapacity: Integer;
begin
  if FCount=FCapacity then begin
    NewCapacity := FCapacity+8;
    ReallocMem(FItems, NewCapacity*sizeof(Pointer));
    FillChar((PRVAnsiChar(FItems)+FCapacity*sizeof(Pointer))^,
      (NewCapacity-FCapacity)*sizeof(Pointer), 0);
    FCapacity := NewCapacity;
  end;
  FItems^[FCount] := s;
  inc(FCount);
end;
{------------------------------------------------------------------------------}
procedure TRVRawByteStringList.Clear;
begin
  if FCount <> 0 then
    Finalize(FItems^[0], FCount);
  if FCapacity<>0 then
    FreeMem(FItems);
  FItems := nil;
  FCapacity := 0;
  FCount := 0;
end;
{------------------------------------------------------------------------------}
destructor TRVRawByteStringList.Destroy;
begin
  Clear;
  inherited;
end;
{------------------------------------------------------------------------------}
function TRVRawByteStringList.GetValue(const Name: TRVRawByteString): TRVRawByteString;
var
  Index: Integer;
begin
  Index := IndexOfName(Name);
  if Index >= 0 then
    Result := Copy(FItems^[Index], Length(Name) + 2, MaxInt)
  else
    Result := '';
end;
{------------------------------------------------------------------------------}
function TRVRawByteStringList.IndexOfName(
  const Name: TRVRawByteString): Integer;
var
  P: Integer;
  S: TRVRawByteString;
begin
  for Result := 0 to FCount - 1 do
  begin
    S := FItems^[Result];
    P := RVPos('=', S);
    if (P <> 0) and
      ({$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}CompareStr(Copy(S, 1, P - 1), Name) = 0) then
      exit;
  end;
  Result := -1;
end;

end.