﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'rvhtmlimport.pas' rev: 27.00 (Windows)

#ifndef RvhtmlimportHPP
#define RvhtmlimportHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvhtmlimport
{
//-- type declarations -------------------------------------------------------
typedef void __fastcall (__closure *TOnComment)(System::TObject* Sender, const System::UnicodeString sComment);

typedef void __fastcall (__closure *TOnImageRequired2)(System::TObject* Sender, const System::UnicodeString src, int Width, int Height, Vcl::Graphics::TGraphic* &Img);

enum DECLSPEC_DENUM TListType : unsigned char { ltNone, ltDisc, ltCircle, ltSquare, ltNumber, ltLowerAlpha, ltUpperAlpha, ltLowerRoman, ltUpperRoman };

enum DECLSPEC_DENUM TRVHTMLOption : unsigned char { rvhtmloAutoHyperlinks, rvhtmloBasePathLinks, rvhtmloImgSrcToImageName, rvhtmloIgnorePictures };

typedef System::Set<TRVHTMLOption, TRVHTMLOption::rvhtmloAutoHyperlinks, TRVHTMLOption::rvhtmloIgnorePictures> TRVHTMLOptions;

enum DECLSPEC_DENUM TRVHTMLEncoding : unsigned char { rvhtmleANSI, rvhtmleUTF8 };

typedef System::StaticArray<Rvtypes::TRVAnsiString, 134217728> TRVRawStringArray;

typedef TRVRawStringArray *PRVRawStringArray;

class DELPHICLASS TRVRawByteStringList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRawByteStringList : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FCount;
	int FCapacity;
	TRVRawStringArray *FItems;
	
public:
	void __fastcall Clear(void);
	void __fastcall Add(const Rvtypes::TRVRawByteString s);
	int __fastcall IndexOfName(const Rvtypes::TRVRawByteString Name);
	Rvtypes::TRVRawByteString __fastcall GetValue(const Rvtypes::TRVRawByteString Name);
	__fastcall virtual ~TRVRawByteStringList(void);
public:
	/* TObject.Create */ inline __fastcall TRVRawByteStringList(void) : System::TObject() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRvHtmlParaStackItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRvHtmlParaStackItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int ParaNo;
	Rvstyle::TRVAlignment Alignment;
	int OutlineLevel;
public:
	/* TObject.Create */ inline __fastcall TRvHtmlParaStackItem(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRvHtmlParaStackItem(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRvHtmlImporter;
class PASCALIMPLEMENTATION TRvHtmlImporter : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	int FCharCount;
	int FLineCount;
	bool FSeparateLines;
	Richview::TCustomRichView* FViewer;
	int FDefaultFontSize;
	int FDefaultCFontSize;
	System::Uitypes::TFontName FDefaultFontName;
	System::Uitypes::TFontName FDefaultCFontName;
	System::Uitypes::TColor FDefaultColor;
	System::Uitypes::TColor FDefaultLinkCol;
	System::Uitypes::TFontStyles FDefaultLinkStyle;
	System::Uitypes::TColor FDefaultBGColor;
	System::UnicodeString FTitle;
	TOnComment FOnComment;
	System::Classes::TNotifyEvent FOnNewLine;
	TOnImageRequired2 FOnImageRequired2;
	int FStartSelection;
	int FEndSelection;
	int FStartSelNo;
	int FEndSelNo;
	bool FNextTimeCloseSelection;
	Crvdata::TCustomRVData* FSelRVData;
	System::UnicodeString FBasePath;
	bool FClearDocument;
	TRVHTMLOptions FOptions;
	bool FBackgroundRead;
	int FClrListStyle;
	bool FEnforceListLevels;
	bool FFixedCSSStyles;
	TRVHTMLEncoding FEncoding;
	TRVHTMLEncoding FActualEncoding;
	void __fastcall LoadHtmlSelection(const Rvtypes::TRVRawByteString Data, int StartSelection, int EndSelection);
	void __fastcall CheckSelection(System::TObject* Parser, Crvdata::TCustomRVData* RVData, Rvtable::TRVTableItemInfo* RVT);
	void __fastcall LoadClipboardHtml(const Rvtypes::TRVRawByteString Data);
	void __fastcall InitListLevel(Rvstyle::TRVListLevel* ALevel, TListType AType, int ADepth);
	Vcl::Graphics::TGraphic* __fastcall ReadImage(const System::UnicodeString src, int Width, int Height, bool NoFail);
	Vcl::Graphics::TBitmap* __fastcall GetBitmap(Vcl::Graphics::TGraphic* gr);
	System::UnicodeString __fastcall GetFullFileName(const System::UnicodeString FileName);
	void __fastcall SetFixedCSSStyles(const bool Value);
	void __fastcall AddRVStyles(System::UnicodeString TagList, Richview::TCustomRichView* FViewer);
	Rvstyle::TRVCodePage __fastcall GetCodePage(void);
	
protected:
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	__property bool FixedCSSStyles = {read=FFixedCSSStyles, write=SetFixedCSSStyles, default=0};
	
public:
	void __fastcall LoadFromClipboard(void);
	void __fastcall LoadHtml(const Rvtypes::TRVRawByteString Data);
	__fastcall virtual TRvHtmlImporter(System::Classes::TComponent* AOwner);
	bool __fastcall GetSelectionBounds(Crvdata::TCustomRVData* &RVData, int &StartNo, int &EndNo);
	__property int CharCount = {read=FCharCount, nodefault};
	__property int LineCount = {read=FLineCount, nodefault};
	__property System::UnicodeString Title = {read=FTitle};
	__property System::UnicodeString BasePath = {read=FBasePath, write=FBasePath};
	
__published:
	__property bool ClearDocument = {read=FClearDocument, write=FClearDocument, default=1};
	__property bool SeparateLines = {read=FSeparateLines, write=FSeparateLines, default=0};
	__property Richview::TCustomRichView* RichView = {read=FViewer, write=FViewer};
	__property System::Uitypes::TFontName DefaultFontName = {read=FDefaultFontName, write=FDefaultFontName};
	__property System::Uitypes::TFontName DefaultCFontName = {read=FDefaultCFontName, write=FDefaultCFontName};
	__property int DefaultFontSize = {read=FDefaultFontSize, write=FDefaultFontSize, default=12};
	__property int DefaultCFontSize = {read=FDefaultCFontSize, write=FDefaultCFontSize, default=10};
	__property System::Uitypes::TColor DefaultColor = {read=FDefaultColor, write=FDefaultColor, default=-16777208};
	__property System::Uitypes::TColor DefaultLinkCol = {read=FDefaultLinkCol, write=FDefaultLinkCol, default=16711680};
	__property System::Uitypes::TFontStyles DefaultLinkStyle = {read=FDefaultLinkStyle, write=FDefaultLinkStyle, default=4};
	__property System::Uitypes::TColor DefaultBGColor = {read=FDefaultBGColor, write=FDefaultBGColor, default=-16777211};
	__property bool EnforceListLevels = {read=FEnforceListLevels, write=FEnforceListLevels, default=1};
	__property TRVHTMLOptions Options = {read=FOptions, write=FOptions, default=3};
	__property TOnComment OnComment = {read=FOnComment, write=FOnComment};
	__property System::Classes::TNotifyEvent OnNewLine = {read=FOnNewLine, write=FOnNewLine};
	__property TOnImageRequired2 OnImageRequired2 = {read=FOnImageRequired2, write=FOnImageRequired2};
	__property TRVHTMLEncoding Encoding = {read=FEncoding, write=FEncoding, default=0};
public:
	/* TComponent.Destroy */ inline __fastcall virtual ~TRvHtmlImporter(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Rvhtmlimport */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVHTMLIMPORT)
using namespace Rvhtmlimport;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvhtmlimportHPP
