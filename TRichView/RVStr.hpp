﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVStr.pas' rev: 27.00 (Windows)

#ifndef RvstrHPP
#define RvstrHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvstr
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _errRVNil;
#define Rvstr_errRVNil System::LoadResourceString(&Rvstr::_errRVNil)
extern DELPHI_PACKAGE System::ResourceString _errRVNotFormatted;
#define Rvstr_errRVNotFormatted System::LoadResourceString(&Rvstr::_errRVNotFormatted)
extern DELPHI_PACKAGE System::ResourceString _errRVNoMemory;
#define Rvstr_errRVNoMemory System::LoadResourceString(&Rvstr::_errRVNoMemory)
extern DELPHI_PACKAGE System::ResourceString _errRVTypesMismatch;
#define Rvstr_errRVTypesMismatch System::LoadResourceString(&Rvstr::_errRVTypesMismatch)
extern DELPHI_PACKAGE System::ResourceString _errRVUnicode;
#define Rvstr_errRVUnicode System::LoadResourceString(&Rvstr::_errRVUnicode)
extern DELPHI_PACKAGE System::ResourceString _errRVCPByCP;
#define Rvstr_errRVCPByCP System::LoadResourceString(&Rvstr::_errRVCPByCP)
extern DELPHI_PACKAGE System::ResourceString _errRVNoSuchCP;
#define Rvstr_errRVNoSuchCP System::LoadResourceString(&Rvstr::_errRVNoSuchCP)
extern DELPHI_PACKAGE System::ResourceString _errRVTagsTypesMismatch;
#define Rvstr_errRVTagsTypesMismatch System::LoadResourceString(&Rvstr::_errRVTagsTypesMismatch)
extern DELPHI_PACKAGE System::ResourceString _errRVNoSuchCP2;
#define Rvstr_errRVNoSuchCP2 System::LoadResourceString(&Rvstr::_errRVNoSuchCP2)
extern DELPHI_PACKAGE System::ResourceString _errRVItemRangeError;
#define Rvstr_errRVItemRangeError System::LoadResourceString(&Rvstr::_errRVItemRangeError)
extern DELPHI_PACKAGE System::ResourceString _errRVCPExists;
#define Rvstr_errRVCPExists System::LoadResourceString(&Rvstr::_errRVCPExists)
extern DELPHI_PACKAGE System::ResourceString _errStyleIsNotAssigned;
#define Rvstr_errStyleIsNotAssigned System::LoadResourceString(&Rvstr::_errStyleIsNotAssigned)
extern DELPHI_PACKAGE System::ResourceString _errInvalidPageNo;
#define Rvstr_errInvalidPageNo System::LoadResourceString(&Rvstr::_errInvalidPageNo)
extern DELPHI_PACKAGE System::ResourceString _errTRVItemFormattedDataNS;
#define Rvstr_errTRVItemFormattedDataNS System::LoadResourceString(&Rvstr::_errTRVItemFormattedDataNS)
extern DELPHI_PACKAGE System::ResourceString _errRViewerOnly;
#define Rvstr_errRViewerOnly System::LoadResourceString(&Rvstr::_errRViewerOnly)
extern DELPHI_PACKAGE System::ResourceString _errRVUndo;
#define Rvstr_errRVUndo System::LoadResourceString(&Rvstr::_errRVUndo)
extern DELPHI_PACKAGE System::ResourceString _errRVCP;
#define Rvstr_errRVCP System::LoadResourceString(&Rvstr::_errRVCP)
extern DELPHI_PACKAGE System::ResourceString _errRVItemReg1;
#define Rvstr_errRVItemReg1 System::LoadResourceString(&Rvstr::_errRVItemReg1)
extern DELPHI_PACKAGE System::ResourceString _errRVItemReg2;
#define Rvstr_errRVItemReg2 System::LoadResourceString(&Rvstr::_errRVItemReg2)
extern DELPHI_PACKAGE System::ResourceString _errRVUndoEmpty;
#define Rvstr_errRVUndoEmpty System::LoadResourceString(&Rvstr::_errRVUndoEmpty)
extern DELPHI_PACKAGE System::ResourceString _errRVUndoAdd;
#define Rvstr_errRVUndoAdd System::LoadResourceString(&Rvstr::_errRVUndoAdd)
extern DELPHI_PACKAGE System::ResourceString _errRVUndoEmptyBuffer;
#define Rvstr_errRVUndoEmptyBuffer System::LoadResourceString(&Rvstr::_errRVUndoEmptyBuffer)
extern DELPHI_PACKAGE System::ResourceString _errRVNegative;
#define Rvstr_errRVNegative System::LoadResourceString(&Rvstr::_errRVNegative)
extern DELPHI_PACKAGE System::ResourceString _errRVFDocProp;
#define Rvstr_errRVFDocProp System::LoadResourceString(&Rvstr::_errRVFDocProp)
extern DELPHI_PACKAGE System::ResourceString _errRVCaretPosition;
#define Rvstr_errRVCaretPosition System::LoadResourceString(&Rvstr::_errRVCaretPosition)
extern DELPHI_PACKAGE System::ResourceString _errRVPrint;
#define Rvstr_errRVPrint System::LoadResourceString(&Rvstr::_errRVPrint)
extern DELPHI_PACKAGE System::ResourceString _errRVCompare;
#define Rvstr_errRVCompare System::LoadResourceString(&Rvstr::_errRVCompare)
extern DELPHI_PACKAGE System::ResourceString _errRVInvProp;
#define Rvstr_errRVInvProp System::LoadResourceString(&Rvstr::_errRVInvProp)
extern DELPHI_PACKAGE System::ResourceString _errRVError;
#define Rvstr_errRVError System::LoadResourceString(&Rvstr::_errRVError)
extern DELPHI_PACKAGE System::ResourceString _errWrongAssign;
#define Rvstr_errWrongAssign System::LoadResourceString(&Rvstr::_errWrongAssign)
extern DELPHI_PACKAGE System::ResourceString _errRVBadStyleTemplateParent;
#define Rvstr_errRVBadStyleTemplateParent System::LoadResourceString(&Rvstr::_errRVBadStyleTemplateParent)
extern DELPHI_PACKAGE System::ResourceString _errRVBadStyleTemplateParent2;
#define Rvstr_errRVBadStyleTemplateParent2 System::LoadResourceString(&Rvstr::_errRVBadStyleTemplateParent2)
extern DELPHI_PACKAGE System::ResourceString _errRVFSubDoc;
#define Rvstr_errRVFSubDoc System::LoadResourceString(&Rvstr::_errRVFSubDoc)
extern DELPHI_PACKAGE System::ResourceString _errRVReportHelperFormat;
#define Rvstr_errRVReportHelperFormat System::LoadResourceString(&Rvstr::_errRVReportHelperFormat)
extern DELPHI_PACKAGE System::ResourceString _errRVInvalidPropertyValue;
#define Rvstr_errRVInvalidPropertyValue System::LoadResourceString(&Rvstr::_errRVInvalidPropertyValue)
extern DELPHI_PACKAGE System::ResourceString _errRVRTFDoubleStyleSheet;
#define Rvstr_errRVRTFDoubleStyleSheet System::LoadResourceString(&Rvstr::_errRVRTFDoubleStyleSheet)
extern DELPHI_PACKAGE System::ResourceString _errRVSTCannotAssign;
#define Rvstr_errRVSTCannotAssign System::LoadResourceString(&Rvstr::_errRVSTCannotAssign)
extern DELPHI_PACKAGE System::ResourceString _errRVSTHFMismatch;
#define Rvstr_errRVSTHFMismatch System::LoadResourceString(&Rvstr::_errRVSTHFMismatch)
extern DELPHI_PACKAGE System::ResourceString _errRVSTHFPropMismatch;
#define Rvstr_errRVSTHFPropMismatch System::LoadResourceString(&Rvstr::_errRVSTHFPropMismatch)
extern DELPHI_PACKAGE System::ResourceString _errRVDocXImgSave;
#define Rvstr_errRVDocXImgSave System::LoadResourceString(&Rvstr::_errRVDocXImgSave)
#define RVRC_ZOOMIN_CURSOR L"RV_ZOOMIN_CURSOR"
#define RVRC_ZOOMOUT_CURSOR L"RV_ZOOMOUT_CURSOR"
#define RVRC_JUMP_CURSOR L"RV_JUMP_CURSOR"
#define RVRC_FLIPARROW_CURSOR L"RV_FLIPARROW_CURSOR"
#define RVNORMALSTYLETEMPLATENAME L"Normal"
#define RVHEADINGSTYLETEMPLATENAME L"heading %d"
#define RVHYPERLINKSTYLETEMPLATENAME L"Hyperlink"
#define RVDEFAULTPARAGRAPHFONTSTYLENAME L"Default Paragraph Font"
#define RVCAPTIONSTYLETEMPLATE L"caption"
#define STYLETEMPLATEINISECTION L"Styles"
#define RVINIFILEYES L"Yes"
#define RVINIFILENO L"No"
#define RVINIFILEYESU L"YES"
#define RVINIFILENOU L"NO"
static const System::WideChar RVINIUNKNOWN = (System::WideChar)(0x3f);
#define RVINI_TEXTSTYLECOUNT L"FontsCount"
#define RVINI_TEXTSTYLEPREFIX L"Font%s"
#define RVINI_PARASTYLECOUNT L"ParasCount"
#define RVINI_PARASTYLEPREFIX L"Para%s"
#define RVINI_LISTSTYLECOUNT L"ListCount"
#define RVINI_LISTSTYLEPREFIX L"List%s"
#define RVINI_STANDARD L"Standard"
#define RVINI_LEFT L"Left"
#define RVINI_RIGHT L"Right"
#define RVINI_TOP L"Top"
#define RVINI_BOTTOM L"Bottom"
#define RVINI_WIDTH L"Width"
#define RVINI_STYLE L"Style"
#define RVINI_INTERNALWIDTH L"InternalWidth"
#define RVINI_BOFFSPREFIX L"Offsets%s"
#define RVINI_VISBPREFIX L"Visible%s"
#define RVINI_SPACEBEFORE L"SpaceBefore"
#define RVINI_SPACEAFTER L"SpaceAfter"
#define RVINI_LEFTINDENT L"LeftIndent"
#define RVINI_RIGHTIDENT L"RightIndent"
#define RVINI_FIRSTINDENT L"FirstIndent"
#define RVINI_LINESPACING L"LineSpacing"
#define RVINI_LINESPACINGTYPE L"LSType"
#define RVINI_NEXTPARANO L"NextParaNo"
#define RVINI_DEFSTYLENO L"DefStyleNo"
#define RVINI_ALIGNMENT L"Alignment"
#define RVINI_NOWRAP L"NoWrap"
#define RVINI_READONLY L"ReadOnly"
#define RVINI_STYLEPROTECT L"StyleProtect"
#define RVINI_DONOTWANTRETURNS L"DoNotWantReturns"
#define RVINI_KEEPLINESTOGETHER L"KeepLinesTogether"
#define RVINI_KEEPWITHNEXT L"KeepWithNext"
#define RVINI_BORDERPREFIX L"Border%s"
#define RVINI_BACKGROUNDPREFIX L"Background%s"
#define RVINI_STYLENAME L"StyleName"
#define RVINI_FONTNAME L"Name"
#define RVINI_JUMP L"Jump"
#define RVINI_SPACESINTAB L"SpacesInTab"
#define RVINI_DEFTABWIDTH L"DefTabWidth"
#define RVINI_JUMPCURSOR L"JumpCursor"
#define RVINI_SIZE L"Size"
#define RVINI_SIZEDOUBLE L"SizeDouble"
#define RVINI_COLOR L"Color"
#define RVINI_BACKCOLOR L"BackColor"
#define RVINI_HOVERBACKCOLOR L"HoverBackColor"
#define RVINI_HOVERCOLOR L"HoverColor"
#define RVINI_HOVERUNDERLINE L"HoverUnderline"
#define RVINI_CURRENTITEMCOLOR L"CurItemColor"
#define RVINI_CHARSET L"Charset"
#define RVINI_CHARSCALE L"CharScale"
#define RVINI_CHARSPACING L"CharSpacing"
#define RVINI_OUTLINELEVEL L"OutlineLevel"
#define RVINI_BIDIMODE L"BiDiMode"
#define RVINI_BOLD L"Bold"
#define RVINI_UNDERLINE L"Underline"
#define RVINI_STRIKEOUT L"StrikeOut"
#define RVINI_ITALIC L"Italic"
#define RVINI_OVERLINE L"Overline"
#define RVINI_ALLCAPS L"Caps"
#define RVINI_PROTECTION L"Protection"
#define RVINI_RTFCODE L"RTFCode"
#define RVINI_HTMLCODE L"HTMLCode"
#define RVINI_DOCXCODE L"DocXCode"
#define RVINI_DOCXINRUNCODE L"DocXInRunCode"
#define RVINI_HIDDEN L"Hidden"
#define RVINI_VSHIFT L"VShift"
#define RVINI_NEXTSTYLENO L"NextStyleNo"
#define RVINI_BASESTYLENO L"BaseStyleNo"
#define RVINI_UNICODE L"Unicode"
#define RVINI_SCRIPT L"Script"
#define RVINI_UNDERLINETYPE L"UnderlineType"
#define RVINI_UNDERLINECOLOR L"UnderlineColor"
#define RVINI_HOVERUNDERLINECOLOR L"HoverUnderlineColor"
#define RVINI_SELECTIONMODE L"SelectionMode"
#define RVINI_SELECTIONSTYLE L"SelectionStyle"
#define RVINI_SELCOLOR L"SelColor"
#define RVINI_SELTEXTCOLOR L"SelTextColor"
#define RVINI_ISELCOLOR L"ISelColor"
#define RVINI_ISELTEXTCOLOR L"ISelTextColor"
#define RVINI_CPCOLOR L"CheckpointColor"
#define RVINI_CPEVCOLOR L"CheckpointEvColor"
#define RVINI_PAGEBREAKCOLOR L"PageBreakColor"
#define RVINI_SOFTPAGEBREAKCOLOR L"SoftPageBreakColor"
#define RVINI_LIVESPELLINGCOLOR L"LiveSpellingColor"
#define RVINI_FLOATLINECOLOR L"FloatLineColor"
#define RVINI_SPECCHARCOLOR L"SpecCharColor"
#define RVINI_USESOUND L"UseSound"
#define RVINI_DEFUNICODESTYLE L"DefUnicodeStyle"
#define RVINI_DEFCODEPAGE L"DefCodePage"
#define RVINI_LINESELECTCURSOR L"LineSelectCursor"
#define RVINI_LINEWRAPMODE L"LineWrapMode"
#define RVINI_UNITS L"Units"
#define RVINI_FONTQUALITY L"FontQuality"
#define RVINI_SELECTIONHANDLES L"SelectionHandles"
#define RVINI_FIELDHIGHLIGHTCOLOR L"FieldHighlightColor"
#define RVINI_FIELDHIGHLIGHTTYPE L"FieldHighlightType"
#define RVINI_DISABLEDFONTCOLOR L"DisabledFontColor"
#define RVINI_FOOTNOTENUMBERING L"FootnoteNumbering"
#define RVINI_FOOTNOTEPAGERESET L"FootnotePageReset"
#define RVINI_ENDNOTENUMBERING L"EndnoteNumbering"
#define RVINI_SIDENOTENUMBERING L"SidenoteNumbering"
#define RVINI_LISTTYPE L"ListType"
#define RVINI_IMAGEINDEX L"ImageIndex"
#define RVINI_FORMATSTRING L"FormatString"
#define RVINI_MARKERINDENT L"MarkerIndent"
#define RVINI_MARKERALIGNMENT L"MarkerAlignment"
#define RVINI_FORMATSTRINGW L"FormatStringW"
#define RVINI_PICTURE L"Picture"
#define RVINI_GRAPHICCLASS L"GraphicClass"
#define RVINI_TABALIGN L"Align"
#define RVINI_TABPOSITION L"Pos"
#define RVINI_TABLEADER L"Leader"
#define RVINI_TABPREFIX L"Tab%s"
#define RVINI_TABCOUNT L"TabCount"
#define RVINI_FONT L"Font"
#define RVINI_LOCONTINUOUS L"Continuous"
#define RVINI_LOLEVELRESET L"LevelReset"
#define RVINI_LEVELSCOUNT L"LevelsCount"
#define RVINI_ONELEVELPREVIEW L"OneLevelPreview"
#define RVINI_LISTID L"ListId"
#define RVINI_LEVELPREFIX L"Lvl%s"
#define RVINI_STYLETEMPLATEID L"StyleTemplateId"
#define RVINI_PARASTYLETEMPLATEID L"ParaStyleTemplateId"
#define RVINI_STCOUNT L"STItemCount"
#define RVINI_STPREFIX L"ST%s"
#define RVINI_ST_ID L"Id"
#define RVINI_ST_NAME L"Name"
#define RVINI_ST_PARENTID L"ParentId"
#define RVINI_ST_NEXTID L"NextId"
#define RVINI_ST_KIND L"Kind"
#define RVINI_ST_QUICKACCESS L"QuickAccess"
#define RVINI_ST_VALIDPARAPROPERTIES L"ValidParaProperties"
#define RVINI_ST_VALIDTEXTPROPERTIES L"ValidTextProperties"
#define RVSTYLE_REG L"RVStyle"
#define RVINI_SINGLESYMBOLS L"SingleSymbols"
#define RVWCEDIT L"E\u0000d\u0000i\u0000t\u0000\u0000\u0000"
#define RVDOCX_XMLSTART L"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\""\
	L"?>"
#define RVDOCX_XMLNS_O L"urn:schemas-microsoft-com:office:office"
#define RVDOCX_XMLNS_M L"http://schemas.openxmlformats.org/officeDocument/2006/math"
#define RVDOCX_XMLNS_R L"http://schemas.openxmlformats.org/officeDocument/2006/rela"\
	L"tionships"
#define RVDOCX_XMLNS_V L"urn:schemas-microsoft-com:vml"
#define RVDOCX_XMLNS_VE L"http://schemas.openxmlformats.org/markup-compatibility/200"\
	L"6"
#define RVDOCX_XMLNS_W L"http://schemas.openxmlformats.org/wordprocessingml/2006/ma"\
	L"in"
#define RVDOCX_XMLNS_WP L"http://schemas.openxmlformats.org/drawingml/2006/wordproce"\
	L"ssingDrawing"
#define RVDOCX_XMLNS_WP14 L"http://schemas.microsoft.com/office/word/2010/wordprocessi"\
	L"ngDrawing"
#define RVDOCX_XMLNS_WPS L"http://schemas.microsoft.com/office/word/2010/wordprocessi"\
	L"ngShape"
#define RVDOCX_XMLNS_W10 L"urn:schemas-microsoft-com:office:word"
#define RVDOCX_XMLNS_WNE L"http://schemas.microsoft.com/office/word/2006/wordml"
#define RVDOCX_XMLNS_CP L"http://schemas.openxmlformats.org/package/2006/metadata/co"\
	L"re-properties"
#define RVDOCX_XMLNS_DC L"http://purl.org/dc/elements/1.1/"
#define RVDOCX_XMLNS_MC L"http://schemas.openxmlformats.org/markup-compatibility/200"\
	L"6"
static const System::WideChar RVCHAR_MIDDLEDOT = (System::WideChar)(0xb7);
static const System::WideChar RVCHAR_DEGREE = (System::WideChar)(0xb0);
static const System::WideChar RVCHAR_NBSP = (System::WideChar)(0xa0);
static const System::WideChar RVCHAR_LQUOTE = (System::WideChar)(0x2018);
static const System::WideChar RVCHAR_RQUOTE = (System::WideChar)(0x2019);
static const System::WideChar RVCHAR_LDQUOTE = (System::WideChar)(0x201c);
static const System::WideChar RVCHAR_RDQUOTE = (System::WideChar)(0x201d);
static const System::WideChar RVCHAR_RDLOWQUOTE = (System::WideChar)(0x201e);
static const System::WideChar RVCHAR_LDANGLEQUOTE = (System::WideChar)(0xab);
static const System::WideChar RVCHAR_RDANGLEQUOTE = (System::WideChar)(0xbb);
static const System::WideChar RVCHAR_ELLIPSIS = (System::WideChar)(0x2026);
#define RVDEFAULTCHECKPOINTPREFIX L"RichViewCheckpoint"
static const System::WideChar RVDEFAULTCHARACTER = (System::WideChar)(0x3f);
#define RVDEFAULTDELIMITERS L" .;,:(){}\"/\\<>!?[]\u2018\u2019\u201c\u201d-+*=\u00a0\u201e"\
	L"\u00ab\u00bb\u2026"
#define RVAddress L"http://www.trichview.com"
#define RVVersion L"v14.15"
#define RVPalettePage L"RichView"
#define RVNOSTYLEMSG L"Style is not defined\rCreate a TRVStyle object and assign "\
	L"it to %s.Style"
static const System::WideChar RVFTagEmptyStr = (System::WideChar)(0x30);
#define RVFORMATNAME L"RichView Format"
#define RTFORMATNAME L"Rich Text Format"
#define URLFORMATNAME L"UniformResourceLocator"
#define HTMLFORMATNAME L"HTML Format"
#define HTMLClipboardSourceURL "SourceURL:"
#define RVDEFSTYLENAME0 L"Normal text"
#define RVDEFSTYLENAME1 L"Heading"
#define RVDEFSTYLENAME2 L"Subheading"
#define RVDEFSTYLENAME3 L"Keywords"
#define RVDEFSTYLENAME4 L"Jump 1"
#define RVDEFSTYLENAME5 L"Jump 2"
#define RVDEFPARASTYLENAME1 L"Centered"
#define RVDEFAULTDESIGNFONT L"MS Sans Serif"
#define RVDEFAULTSTYLEFONT L"Arial"
#define RVDEFAULTTEXTSTYLENAME L"Font Style"
#define RVDEFAULTPARASTYLENAME L"Paragraph Style"
#define RVDEFAULTLISTSTYLENAME L"List Style"
#define RVDEFAULTSTYLETEMPLATENAME L"Style %d"
#define RVLISTLEVELDISPLAYNAME L"%s %d/%d/%d"
#define RVFONT_SYMBOL L"Symbol"
#define RVFONT_WINGDINGS L"Wingdings"
#define RVFONT_ARIAL L"Arial"
#define RVFONT_TIMESNEWROMAN L"Times New Roman"
#define RVFONT_TAHOMA L"Tahoma"
extern DELPHI_PACKAGE System::StaticArray<System::WideChar *, 10> RVListTypeStr;
extern DELPHI_PACKAGE System::StaticArray<System::WideChar *, 3> RVAlignStr;
extern DELPHI_PACKAGE System::Word CFRV_RVF;
extern DELPHI_PACKAGE System::Word CFRV_RTF;
extern DELPHI_PACKAGE System::Word CFRV_HTML;
extern DELPHI_PACKAGE System::Word CFRV_URL;
}	/* namespace Rvstr */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSTR)
using namespace Rvstr;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvstrHPP
