﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVSer.pas' rev: 27.00 (Windows)

#ifndef RvserHPP
#define RvserHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVERVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvser
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVSerEntry;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSerEntry : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Crvdata::TCustomRVData* RVData;
	int AbsFirstItemNo;
	int FirstItemNo;
	bool IsFirst;
	__fastcall TRVSerEntry(Crvdata::TCustomRVData* ARVData);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVSerEntry(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSerializer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSerializer : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
private:
	int FCachedEntryNo;
	Crvfdata::TCustomRVFormattedData* FRootRVData;
	void __fastcall CreateEntries(Crvdata::TCustomRVData* RVData, int &AbsItemNo);
	
public:
	int AbsItemCount;
	__fastcall TRVSerializer(Crvfdata::TCustomRVFormattedData* RVData);
	void __fastcall Rebuild(void);
	void __fastcall AbsoluteToRV(int AbsItemNo, Crvdata::TCustomRVData* &RVData, int &ItemNo, bool &AfterItem);
	void __fastcall RVToAbsolute(Crvdata::TCustomRVData* RVData, int ItemNo, bool AfterItem, int &AbsItemNo);
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVSerializer(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvser */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSER)
using namespace Rvser;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvserHPP
