{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       Item types:                                     }
{       - footnote,                                     }
{       - endnote,                                      }
{       - reference to footnote/endnote.                }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}


unit RVNote;

{$I RV_Defs.inc}

interface

{$IFNDEF RVDONOTUSESEQ}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  Classes, SysUtils, Graphics,
  RVStyle, CRVData, RVItem, DLines, RVClasses, RVUni,
  RVLabelItem, RVSeqItem,
  RVRVData, RVSubData, RichView, RVFMisc, RVScroll,
  {$IFNDEF RVDONOTUSEDOCX}
  RVDocX,
  {$ENDIF}
  RVFuncs, RVStr, RVTypes;

type
  TRVNoteData = class (TRVSubData)
    public
      FNoteTextForPrinting: String;
      function GetNoteText: String; override;
      function GetNoteTextForPrinting: String;
  end;

  TRVNoteFormattingData = class
  public
    procedure SaveToStream(Stream: TStream); dynamic;
    procedure LoadFromStream(Stream: TStream); dynamic;
  end;


  TCustomRVNoteItemInfo = class (TRVSeqItemInfo)
  private
    protected
      FDocument: TRVNoteData;
      procedure SetParentRVData(const Value: TPersistent); override;
      {$IFNDEF RVDONOTUSERVF}
      procedure Do_ChangeDoc(Stream: TStream; ItemNo: Integer);
      {$ENDIF}
      function GetRVFExtraPropertyCount: Integer; override;
      procedure SaveRVFExtraProperties(Stream: TStream); override;
      procedure SetNumberType(const Value: TRVSeqType); override;
      function GetNoteText: String; virtual;
      function DoGetAsText(LineWidth: Integer;
        RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
        TextOnly, Unicode: Boolean; CodePage: TRVCodePage;
        LeadingSpace: Boolean): TRVRawByteString;
    public
      SRVFormattingData: TRVNoteFormattingData;
      constructor CreateEx(RVData: TPersistent;
        ATextStyleNo, AStartFrom: Integer; AReset: Boolean); virtual;
      destructor Destroy; override;
      function SetExtraCustomProperty(const PropName: TRVAnsiString;
        const Value: String): Boolean; override;
      procedure MovingToUndoList(ItemNo: Integer; RVData,
        AContainerUndoItem: TObject); override;
      procedure MovingFromUndoList(ItemNo: Integer; RVData: TObject); override;
      {$IFNDEF RVDONOTUSERVF}
      procedure SaveRVF(Stream: TStream; RVData: TPersistent;
        ItemNo, ParaNo: Integer; const Name: TRVRawByteString; Part: TRVMultiDrawItemPart;
        ForceSameAsPrev: Boolean; PSaD: PRVScreenAndDevice); override;
      function ReadRVFLine(const s: TRVRawByteString; RVData: TPersistent;
        var ReadType: Integer; LineNo, LineCount: Integer; var Name: TRVRawByteString;
        var ReadMode: TRVFReadMode; var ReadState: TRVFReadState;
        UTF8Strings: Boolean; var AssStyleNameUsed: Boolean): Boolean; override;
      {$ENDIF}
      {$IFNDEF RVDONOTUSERTF}
      procedure FillRTFTables(RTFTables: TCustomRVRTFTables;
        ListOverrideCountList: TRVIntegerList; RVData: TPersistent); override;
      {$ENDIF}
      function GetSubDocRTFDocType: TRVRTFDocType; dynamic;
      {$IFNDEF RVDONOTUSEHTML}
      function GetHTMLAnchorName: TRVAnsiString; dynamic;
      procedure SaveToHTML(Stream: TStream; RVData: TPersistent;
        ItemNo: Integer; const Text: TRVRawByteString; const Path,
        imgSavePrefix: String; var imgSaveNo: Integer;
        CurrentFileColor: TColor; SaveOptions: TRVSaveOptions;
        UseCSS: Boolean; Bullets: TRVList
        {$IFNDEF RVDONOTUSELISTS};ListBullets: TRVHTMLListCSSList{$ENDIF}); override;
      {$ENDIF}
      function GetBoolValue(Prop: TRVItemBoolProperty): Boolean; override;
      procedure MarkStylesInUse(Data: TRVDeleteUnusedStylesData); override;
      procedure UpdateStyles(Data: TRVDeleteUnusedStylesData); override;
      procedure ChangeParagraphStyles(ParaMapList, NewIndices,
        OldIndices: TRVIntegerList; var Index: Integer); override;
      procedure ChangeTextStyles(TextMapList, NewIndices,
        OldIndices: TRVIntegerList; var Index: Integer); override;
      procedure Assign(Source: TCustomRVItemInfo); override;
      {$IFNDEF RVDONOTUSERVF}
      procedure ReplaceDocumentEd(Stream: TStream);
      {$ENDIF}
      function IsFixedHeight: Boolean; dynamic;
      function GetSubDocColor: TColor; override;
      procedure SaveFormattingToStream(Stream: TStream); override;
      procedure LoadFormattingFromStream(Stream: TStream); override;
      property Document: TRVNoteData read FDocument;
      property NoteText: String read GetNoteText;
  end;

  TRVFootOrEndnoteItemInfo = class(TCustomRVNoteItemInfo)
    protected
      {$IFNDEF RVDONOTUSERTF}
      function GetRTFDestinationModifier: TRVAnsiString; dynamic;
      {$ENDIF}
    public
      {$IFNDEF RVDONOTUSERTF}
      procedure SaveRTF(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        Level: Integer; var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
        HiddenParent: Boolean); override;
      {$ENDIF}
      {$IFNDEF RVDONOTUSEDOCX}
      procedure SaveOOXMLBeforeRun(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      procedure SaveOOXMLAfterRun(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      {$ENDIF}
  end;

  TRVEndnoteItemInfo = class(TRVFootOrEndnoteItemInfo)
    private
      procedure Init(RVData: TPersistent);
    protected
      function GetNumberType: TRVSeqType; override;
      {$IFNDEF RVDONOTUSERTF}
      function GetRTFDestinationModifier: TRVAnsiString; override;
      {$ENDIF}
    public
      constructor CreateEx(RVData: TPersistent;
        ATextStyleNo, AStartFrom: Integer; AReset: Boolean); override;
      constructor Create(RVData: TPersistent); override;
      {$IFNDEF RVDONOTUSEHTML}
      function GetHTMLAnchorName: TRVAnsiString; override;
      {$ENDIF}
      {$IFNDEF RVDONOTUSEDOCX}
      procedure SaveOOXML(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      {$ENDIF}
      function AsText(LineWidth: Integer;
        RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
        TextOnly, Unicode: Boolean; CodePage: TRVCodePage): TRVRawByteString; override;
  end;

  TRVFootnoteItemInfo = class(TRVFootOrEndnoteItemInfo)
    private
      procedure Init(RVData: TPersistent);
    protected
      function GetNumberType: TRVSeqType; override;
      function GetTextForPrintMeasuring(RVData: TPersistent): String; override;
      function GetTextForPrinting(RVData: TPersistent;
        DrawItem: TRVDrawLineInfo): String; override;
      {$IFNDEF RVDONOTUSERTF}
      function GetRTFDestinationModifier: TRVAnsiString; override;
      {$ENDIF}
    public
      SRVCounter, SRVOldCounter: Integer;
      constructor CreateEx(RVData: TPersistent;
        ATextStyleNo, AStartFrom: Integer; AReset: Boolean); override;
      constructor Create(RVData: TPersistent); override;
      function CreatePrintingDrawItem(RVData, ParaStyle: TObject;
        const sad: TRVScreenAndDevice): TRVDrawLineInfo; override;
      {$IFNDEF RVDONOTUSEHTML}
      function GetHTMLAnchorName: TRVAnsiString; override;
      {$ENDIF}
      {$IFNDEF RVDONOTUSEDOCX}
      procedure SaveOOXML(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      {$ENDIF}
      procedure CalcDisplayString(List: TRVSeqList); override;
      procedure OnDocWidthChange(DocWidth: Integer; dli: TRVDrawLineInfo;
        Printing: Boolean; Canvas: TCanvas; RVData: TPersistent;
        sad: PRVScreenAndDevice; var HShift, Desc: Integer;
        NoCaching, Reformatting, UseFormatCanvas: Boolean); override;
      function AsText(LineWidth: Integer;
        RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
        TextOnly, Unicode: Boolean; CodePage: TRVCodePage): TRVRawByteString; override;
  end;

  TRVNoteReferenceItemInfo = class (TRVLabelItemInfo)
    protected
      function GetTextForPrinting(RVData: TPersistent;
        DrawItem: TRVDrawLineInfo): String; override;
      function FindNote: TCustomRVNoteItemInfo;
    public
      constructor Create(RVData: TPersistent); override;
      constructor CreateEx(RVData: TPersistent; TextStyleNo: Integer);
      {$IFNDEF RVDONOTUSERVF}
      procedure SaveRVF(Stream: TStream; RVData: TPersistent;
        ItemNo, ParaNo: Integer; const Name: TRVRawByteString; Part: TRVMultiDrawItemPart;
        ForceSameAsPrev: Boolean; PSaD: PRVScreenAndDevice); override;
      function ReadRVFLine(const s: TRVRawByteString; RVData: TPersistent;
        var ReadType: Integer; LineNo, LineCount: Integer; var Name: TRVRawByteString;
        var ReadMode: TRVFReadMode; var ReadState: TRVFReadState;
        UTF8Strings: Boolean; var AssStyleNameUsed: Boolean): Boolean; override;
      {$ENDIF}
      procedure OnDocWidthChange(DocWidth: Integer; dli: TRVDrawLineInfo;
        Printing: Boolean; Canvas: TCanvas; RVData: TPersistent;
        sad: PRVScreenAndDevice; var HShift, Desc: Integer;
        NoCaching, Reformatting, UseFormatCanvas: Boolean); override;
      {$IFNDEF RVDONOTUSERTF}
      procedure SaveRTF(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        Level: Integer; var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
        HiddenParent: Boolean); override;
      {$ENDIF}
      {$IFNDEF RVDONOTUSEDOCX}
      procedure SaveOOXMLBeforeRun(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      procedure SaveOOXMLAfterRun(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      procedure SaveOOXML(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      {$ENDIF}
      {$IFNDEF RVDONOTUSEHTML}
      procedure SaveToHTML(Stream: TStream; RVData: TPersistent;
        ItemNo: Integer; const Text: TRVRawByteString; const Path,
        imgSavePrefix: String; var imgSaveNo: Integer;
        CurrentFileColor: TColor; SaveOptions: TRVSaveOptions;
        UseCSS: Boolean; Bullets: TRVList
        {$IFNDEF RVDONOTUSELISTS};ListBullets: TRVHTMLListCSSList{$ENDIF}); override;
      {$ENDIF}
      function AsText(LineWidth: Integer;
        RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
        TextOnly, Unicode: Boolean; CodePage: TRVCodePage): TRVRawByteString; override;
  end;

  TCustomRVNoteItemInfoClass = class of TCustomRVNoteItemInfo;
  TRVEndnoteItemInfoClass = class of TRVEndnoteItemInfo;

const
  RV_FOOTNOTE_SEQNAME = '@footnote@';
  RV_ENDNOTE_SEQNAME  = '@endnote@';
  RV_FOOTNOTE_HTML_ANCHOR = '_footnote%d';
  RV_ENDNOTE_HTML_ANCHOR = '_endnote%d';
  rvsFootnote = -203;
  rvsEndnote  = -204;
  rvsNoteReference = -205;

function GetNextNote(RVData: TCustomRVData; Note: TCustomRVNoteItemInfo;
  NoteClass: TCustomRVNoteItemInfoClass; AllowHidden: Boolean): TCustomRVNoteItemInfo;
function RVGetFirstEndnote(RichView: TCustomRichView; AllowHidden: Boolean): TRVEndnoteItemInfo;
function RVGetNextEndnote(RichView: TCustomRichView;
  Endnote: TRVEndnoteItemInfo; AllowHidden: Boolean): TRVEndnoteItemInfo;
function RVGetFirstEndnoteInRootRVData(RVData: TCustomRVData; AllowHidden: Boolean): TRVEndnoteItemInfo;

function RVGetFirstFootnote(RichView: TCustomRichView; AllowHidden: Boolean): TRVFootnoteItemInfo;
function RVGetNextFootnote(RichView: TCustomRichView;
  Footnote: TRVFootnoteItemInfo; AllowHidden: Boolean): TRVFootnoteItemInfo;
function RVGetFirstFootnoteInRootRVData(RVData: TCustomRVData; AllowHidden: Boolean): TRVFootnoteItemInfo;

function RVGetNoteTextStyleNo(RVStyle: TRVStyle; StyleNo: Integer
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}; StyleTemplateName: TRVStyleTemplateName
    {$IFDEF RICHVIEWDEF4}=''{$ENDIF}{$ENDIF}): Integer;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSESEQ}

uses RVUndo, RVERVData, RVEdit, PtblRV, PtRVData;

function GetStr(const s: TRVAnsiString; CodePage: TRVCodePage; Unicode: Boolean): TRVRawByteString;
begin
  if Unicode then
    Result := RVU_AnsiToUnicode(CodePage, s)
  else
    Result := s;
end;

{================================ TRVNoteFormattingData =======================}
procedure TRVNoteFormattingData.LoadFromStream(Stream: TStream);
begin
  // nothing to do here
end;
{------------------------------------------------------------------------------}
procedure TRVNoteFormattingData.SaveToStream(Stream: TStream);
begin
  // nothing to do here
end;
{================================ TRVUndoChangeDocInfo ========================}
{$IFNDEF RVDONOTUSERVF}
type
  TRVUndoChangeItemDocInfo = class(TRVUndoItemNoInfo)
    private
      Stream: TRVMemoryStream;
    public
      function RequiresFormat: Boolean; override;
      procedure Undo(RVData: TRichViewRVData); override;
      destructor Destroy; override;
  end;
{------------------------------------------------------------------------------}
function TRVUndoChangeItemDocInfo.RequiresFormat: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
procedure TRVUndoChangeItemDocInfo.Undo(RVData: TRichViewRVData);
var Item: TCustomRVNoteItemInfo;
begin
  Item := RVData.GetItem(ItemNo) as TCustomRVNoteItemInfo;
  Item.Do_ChangeDoc(Stream, ItemNo);
end;
{------------------------------------------------------------------------------}
destructor TRVUndoChangeItemDocInfo.Destroy;
begin
  Stream.Free;
  inherited Destroy;
end;
{$ENDIF}
{====================== TCustomRVNoteItemInfo =================================}
constructor TCustomRVNoteItemInfo.CreateEx(RVData: TPersistent;
        ATextStyleNo, AStartFrom: Integer; AReset: Boolean);
begin
  inherited Create(RVData);
  TextStyleNo := ATextStyleNo;
  StartFrom   := AStartFrom;
  Reset       := AReset;
end;
{------------------------------------------------------------------------------}
destructor TCustomRVNoteItemInfo.Destroy;
begin
  RVFreeAndNil(FDocument);
  RVFreeAndNil(SRVFormattingData);
  inherited;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.MovingFromUndoList(ItemNo: Integer;
  RVData: TObject);
begin
  inherited;
  FDocument.MovingFromUndoList;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.MovingToUndoList(ItemNo: Integer; RVData,
  AContainerUndoItem: TObject);
begin
  inherited;
  FDocument.MovingToUndoList(TRVUndoInfo(AContainerUndoItem));
end;
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.GetRVFExtraPropertyCount: Integer;
begin
  Result := inherited GetRVFExtraPropertyCount;
  if FormatString<>'' then
    inc(Result);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.SaveRVFExtraProperties(Stream: TStream);
begin
  inherited SaveRVFExtraProperties(Stream);
  if FormatString<>'' then
    WriteRVFExtraCustomPropertyStr(Stream, 'format', FormatString);
end;
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.SetExtraCustomProperty(const PropName: TRVAnsiString;
  const Value: String): Boolean;
begin
  if PropName='format' then begin
    FormatString := Value;
    Result := True;
    end
  else
    Result := inherited SetExtraCustomProperty(PropName, Value);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
function TCustomRVNoteItemInfo.ReadRVFLine(const s: TRVRawByteString;
  RVData: TPersistent; var ReadType: Integer; LineNo, LineCount: Integer;
  var Name: TRVRawByteString; var ReadMode: TRVFReadMode;
  var ReadState: TRVFReadState;
  UTF8Strings: Boolean; var AssStyleNameUsed: Boolean): Boolean;
var TmpStream: TRVMemoryStream;
  Color: TColor;
  Index, Dummy1: Integer;
  Dummy2, Dummy3: Boolean;
begin
  Result := True;
  case LineNo of
    0..3:
      LoadPropertiesFromRVF(s, LineNo, RVData, UTF8Strings, AssStyleNameUsed);
    4:
      Reset := s<>'0';
    5:
      StartFrom := RVStrToInt(s);
    6:
      begin
        Name := s;
        ParentRVData := RVData;
      end;
    else
      if LineNo=LineCount-1 then begin
        TmpStream := TRVMemoryStream.Create;
        try
          if ReadType=RVF_SAVETYPE_BINARY then begin
            TmpStream.SetSize(Length(s));
            Move(PRVAnsiChar(s)^, TmpStream.Memory^, Length(s));
            end
          else
            Result := RVFTextString2Stream(s, TmpStream);
          TmpStream.Position := 0;
          Index := 0;
          Color := clNone;
          Include(Document.State, rvstLoadingAsSubDoc);
          try
            Result := Document.InsertRVFFromStream_(TmpStream, Index, -1, False,
              False, False, Color, nil, nil, Dummy1, Dummy2, Dummy3, True
              {$IFNDEF RVDONOTUSESTYLETEMPLATES}, nil{$ENDIF});
          finally
            Exclude(Document.State, rvstLoadingAsSubDoc);
          end;
        finally
          TmpStream.Free;
        end;
        end
      else
        SetExtraPropertyFromRVFStr(s, UTF8Strings);
  end;
  if (ReadType=RVF_SAVETYPE_BINARY) and (LineNo=LineCount-2) then
    ReadMode := rmBeforeBinary;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.SaveRVF(Stream: TStream;
  RVData: TPersistent; ItemNo, ParaNo: Integer; const Name: TRVRawByteString;
  Part: TRVMultiDrawItemPart; ForceSameAsPrev: Boolean; PSaD: PRVScreenAndDevice);
var TmpStream: TRVMemoryStream;
    SaveType: Integer;
begin
  if rvfoSaveBinary in TCustomRVData(RVData).RVFOptions then
    SaveType := RVF_SAVETYPE_BINARY
  else
    SaveType := RVF_SAVETYPE_TEXT;
  RVFWriteLine(Stream,
    {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %s %d %d %s %s',
      [StyleNo, 8+GetRVFExtraPropertyCount {Line count after header},
       RVFItemSavePara(ParaNo, TCustomRVData(RVData), ForceSameAsPrev),
       Byte(RVFGetItemOptions(ItemOptions, ForceSameAsPrev)) and RVItemOptionsMask,
       SaveType,
       RVFSaveTag(rvoTagsArePChars in TCustomRVData(RVData).Options,Tag),
       SaveRVFHeaderTail(RVData)]));
  // lines after header
  {0,1,2,3}SavePropertiesToRVF(Stream, RVData);
  {4}RVFWriteLine(Stream, RVIntToStr(ord(Reset)));
  {5}RVFWriteLine(Stream, RVIntToStr(StartFrom));
  {6}RVFWriteLine(Stream, Name);
  SaveRVFExtraProperties(Stream);
  TmpStream := TRVMemoryStream.Create;
  try
    Document.SaveRVFToStream(TmpStream, False);
    if rvfoSaveBinary in TCustomRVData(RVData).RVFOptions then
      RVFSaveStreamToStream(TmpStream, Stream)
    else
      RVFWriteLine(Stream, RVFStream2TextString(TmpStream));
  finally
    TmpStream.Free;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.SetParentRVData(const Value: TPersistent);
begin
  inherited;
  if FDocument<>nil then
    FDocument.MainRVData := TCustomRVData(ParentRVData).GetAbsoluteRootData;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.MarkStylesInUse(
  Data: TRVDeleteUnusedStylesData);
begin
  inherited;
  Document.DoMarkStylesInUse(Data);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.UpdateStyles(
  Data: TRVDeleteUnusedStylesData);
begin
  inherited;
  Document.DoUpdateStyles(Data);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.ChangeParagraphStyles(ParaMapList,
  NewIndices, OldIndices: TRVIntegerList; var Index: Integer);
begin
  inherited ChangeParagraphStyles(ParaMapList, NewIndices, OldIndices, Index);
  Document.ChangeParagraphStyles(ParaMapList, NewIndices, OldIndices, Index);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.ChangeTextStyles(TextMapList, NewIndices,
  OldIndices: TRVIntegerList; var Index: Integer);
begin
  inherited ChangeTextStyles(TextMapList, NewIndices, OldIndices, Index);
  Document.ChangeTextStyles(TextMapList, NewIndices, OldIndices, Index);
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.Assign(Source: TCustomRVItemInfo);
{$IFNDEF RVDONOTUSERVF}
var Stream: TRVMemoryStream;
{$ENDIF}
begin
  {$IFNDEF RVDONOTUSERVF}
  if Source is TCustomRVNoteItemInfo then begin
    Stream := TRVMemoryStream.Create;
    try
      TCustomRVNoteItemInfo(Source).Document.SaveRVFToStream(Stream, False);
      Stream.Position := 0;
      Document.LoadRVFFromStream(Stream);
    finally
      Stream.Free;
    end;
  end;
  {$ENDIF}
  inherited;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
procedure TCustomRVNoteItemInfo.ReplaceDocumentEd(Stream: TStream);
begin
  if TCustomRVData(ParentRVData).GetRVData is TRVEditRVData then
    TRVEditRVData(TCustomRVData(ParentRVData).GetRVData).BeginUndoSequence(
      rvutModifyItem, True);
  Do_ChangeDoc(Stream, -1);
  if TCustomRVData(ParentRVData).GetRVData is TRVEditRVData then
    TRVEditRVData(TCustomRVData(ParentRVData).GetRVData).Change;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.IsFixedHeight: Boolean;
begin
  Result := False;
end;
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.GetSubDocColor: TColor;
begin
  Result := Document.MainRVData.GetColor;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
procedure TCustomRVNoteItemInfo.Do_ChangeDoc(Stream: TStream; ItemNo: Integer);
var ui: TRVUndoChangeItemDocInfo;
    List: TRVUndoList;
    RVFOptions: TRVFOptions;
begin
  ui := nil;
  if TCustomRVData(ParentRVData).GetRVData is TRVEditRVData then begin
    if ItemNo<0 then
      ItemNo := TCustomRVData(ParentRVData).GetRVData.GetItemNo(Self);
    if ItemNo>=0 then begin
      List := TRVEditRVData(TCustomRVData(ParentRVData).GetRVData).GetUndoList;
      if List<>nil then begin
        ui := TRVUndoChangeItemDocInfo.Create;
        ui.Action := rvuModifyItem;
        ui.ItemNo := ItemNo;
        List.AddInfo(ui,
          TCustomRichViewEdit(TRVEditRVData(TCustomRVData(ParentRVData).GetRVData).RichView));
      end;
    end;
  end;
  if (ui<>nil) and (Document.ItemCount>=0) then begin
    ui.Stream := TRVMemoryStream.Create;
    RVFOptions := Document.RVFOptions;
    try
      Document.RVFOptions := Document.RVFOptions-
        [rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveLayout, rvfoSaveBack];
      Document.SaveRVFToStream(ui.Stream, True);
    finally
      Document.RVFOptions := RVFOptions;
    end;
  end;
  Document.Clear;
  if Stream<>nil then begin
    Stream.Position := 0;
    Document.LoadRVFFromStream(Stream);
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTF}
procedure TCustomRVNoteItemInfo.FillRTFTables(RTFTables: TCustomRVRTFTables;
  ListOverrideCountList: TRVIntegerList; RVData: TPersistent);
begin
  inherited;
  Document.FillRTFTables(RTFTables, ListOverrideCountList);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.GetSubDocRTFDocType: TRVRTFDocType;
begin
  Result := rvrtfdtNote;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEHTML}
function TCustomRVNoteItemInfo.GetHTMLAnchorName: TRVAnsiString;
begin
  Result := '';
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.SaveToHTML(Stream: TStream;
  RVData: TPersistent; ItemNo: Integer; const Text: TRVRawByteString; const Path,
  imgSavePrefix: String; var imgSaveNo: Integer; CurrentFileColor: TColor;
  SaveOptions: TRVSaveOptions; UseCSS: Boolean; Bullets: TRVList
  {$IFNDEF RVDONOTUSELISTS};ListBullets: TRVHTMLListCSSList{$ENDIF});
begin
  RVFWrite(Stream, '<a href="#'+GetHTMLAnchorName+'">');
  inherited;
  RVFWrite(Stream, '</a>');
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.GetNoteText: String;
begin
  Result := Text;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.SetNumberType(const Value: TRVSeqType);
begin
  // does nothing
end;
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.GetBoolValue(
  Prop: TRVItemBoolProperty): Boolean;
begin
  case Prop of
    rvbpFloating:
      Result := False;
    rvbpKeepWithPrior:
      Result := True;
    else
      Result := inherited GetBoolValue(Prop);
  end;
end;
{------------------------------------------------------------------------------}
function TCustomRVNoteItemInfo.DoGetAsText(LineWidth: Integer;
  RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
  TextOnly, Unicode: Boolean; CodePage: TRVCodePage;
  LeadingSpace: Boolean): TRVRawByteString;
var Stream: TRVMemoryStream;
    ACodePage: TRVCodePage;
    DocText: TRVRawByteString;
begin
  if TextOnly then begin
    Result := '';
    exit;
  end;
  Result := inherited AsText(LineWidth, RVData, Text, Path, TextOnly, Unicode,
    CodePage);
  ACodePage := TCustomRVData(RVData).GetDefaultCodePage;
  Stream := TRVMemoryStream.Create;
  try
    FDocument.SaveTextToStream(Path, Stream, 0, False, TextOnly, Unicode, False,
      CodePage, False);
    SetLength(DocText, Stream.Size);
    Stream.Position := 0;
    Stream.ReadBuffer(PRVAnsiChar(DocText)^, Length(DocText));
  finally
    Stream.Free;
  end;
  Result := GetStr('[', ACodePage, Unicode)+DocText+
    GetStr(']', ACodePage, Unicode);
  if LeadingSpace then
    Result := GetStr(' ', ACodePage, Unicode) + Result;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.LoadFormattingFromStream(Stream: TStream);
var v: Integer;
begin
  RVFreeAndNil(SRVFormattingData);
  inherited LoadFormattingFromStream(Stream);
  Stream.ReadBuffer(v, sizeof(v));
  if v<>0 then begin
    SRVFormattingData :=
      TRVNoteFormattingData(TCustomRVData(ParentRVData).GetScaleRichViewInterface.CreateNoteSRVData(v));
    SRVFormattingData.LoadFromStream(Stream);
  end;
end;
{------------------------------------------------------------------------------}
procedure TCustomRVNoteItemInfo.SaveFormattingToStream(Stream: TStream);
var v: Integer;
begin
  inherited SaveFormattingToStream(Stream);
  if SRVFormattingData=nil then begin
    v := 0;
    Stream.WriteBuffer(v, sizeof(v));
    end
  else
    SRVFormattingData.SaveToStream(Stream);
end;
{=========================== TRVFootOrEndnoteItemInfo =========================}
{$IFNDEF RVDONOTUSERTF}
function TRVFootOrEndnoteItemInfo.GetRTFDestinationModifier: TRVAnsiString;
begin
  Result := ' ';
end;
{------------------------------------------------------------------------------}
procedure TRVFootOrEndnoteItemInfo.SaveRTF(Stream: TStream; RVData: TPersistent;
  ItemNo, Level: Integer; var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
  HiddenParent: Boolean);
begin
  if DocType<>rvrtfdtMain then
    exit;
  if Hidden or HiddenParent then
    RVFWrite(Stream, '{\v');
  if Length(FormatString)=1 then
    DoSaveRTF(Stream, RVData)
  else
    RVFWrite(Stream, '\chftn');
  RVFWrite(Stream, '{\footnote');
  RVFWrite(Stream, GetRTFDestinationModifier);
  TCustomRVData(Document).SaveRTFToStream(Stream, False, 0, clNone, nil,
    SavingData, GetSubDocRTFDocType, False, False, nil, nil, nil, nil, nil, nil);
  RVFWrite(Stream, '}');
  if Hidden or HiddenParent then
    RVFWrite(Stream, '}');
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCX}
procedure TRVFootOrEndnoteItemInfo.SaveOOXMLBeforeRun(Stream: TStream;
  RVData: TPersistent; ItemNo: Integer; SavingData: TRVDocXSavingData;
  HiddenParent: Boolean);
begin
  // overriding to save nothing
end;
{------------------------------------------------------------------------------}
procedure TRVFootOrEndnoteItemInfo.SaveOOXMLAfterRun(Stream: TStream;
  RVData: TPersistent; ItemNo: Integer; SavingData: TRVDocXSavingData;
  HiddenParent: Boolean);
begin
  // overriding to save nothing
end;
{$ENDIF}
{============================ TRVEndnoteItemInfo ==============================}
constructor TRVEndnoteItemInfo.Create(RVData: TPersistent);
begin
  inherited;
  Init(RVData);
end;
{------------------------------------------------------------------------------}
constructor TRVEndnoteItemInfo.CreateEx(RVData: TPersistent;
  ATextStyleNo, AStartFrom: Integer; AReset: Boolean);
begin
  inherited CreateEx(RVData, ATextStyleNo, AStartFrom, AReset);
  Init(RVData);
end;
{------------------------------------------------------------------------------}
procedure TRVEndnoteItemInfo.Init(RVData: TPersistent);
begin
  FDocument := TRVNoteData.Create(Self, TCustomRVData(RVData));
  FDocument.MainRVData := TCustomRVData(ParentRVData).GetAbsoluteRootData;
  StyleNo := rvsEndnote;
  SeqName := RV_ENDNOTE_SEQNAME;
end;
{------------------------------------------------------------------------------}
function TRVEndnoteItemInfo.GetNumberType: TRVSeqType;
begin
  Result := TCustomRVData(ParentRVData).GetRVStyle.EndnoteNumbering;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTF}
function TRVEndnoteItemInfo.GetRTFDestinationModifier: TRVAnsiString;
begin
  Result := '\ftnalt ';
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEHTML}
function TRVEndnoteItemInfo.GetHTMLAnchorName: TRVAnsiString;
begin
  Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(RV_ENDNOTE_HTML_ANCHOR, [FCachedIndexInList]);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCX}
procedure TRVEndnoteItemInfo.SaveOOXML(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
var CurPart: TRVDocXPart;
    CustomMarkStr: TRVAnsiString;
begin
  if SavingData.EndnotesStream=nil then
    SavingData.EndnotesStream := TRVMemoryStream.Create;
  inc(SavingData.EndnoteIndex);
  if Length(FormatString)=1 then
    CustomMarkStr := ' w:customMarkFollows="1"'
  else
    CustomMarkStr := '';
  RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:endnoteReference%s w:id="%d"/>', [CustomMarkStr, SavingData.EndnoteIndex]));
  if Length(FormatString)=1 then
    RVFWrite(Stream, '<w:t>'+RV_MakeOOXMLStr(FormatString)+'</w:t>');
  RVFWrite(SavingData.EndnotesStream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:endnote w:id="%d">', [SavingData.EndnoteIndex]));
  CurPart := SavingData.CurPart;
  SavingData.CurPart := rvdocxpEndnotes;
  try
    Document.SaveOOXMLDocumentToStream(SavingData.EndnotesStream, '', False, False,
      False, False, clNone, nil, SavingData);
  finally
    SavingData.CurPart := CurPart;
  end;
  RVFWrite(SavingData.EndnotesStream, '</w:endnote>');
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVEndnoteItemInfo.AsText(LineWidth: Integer;
  RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
  TextOnly, Unicode: Boolean; CodePage: TRVCodePage): TRVRawByteString;
var ACodePage: TRVCodePage;
begin
  if TextOnly then
    Result := ''
  else begin
    ACodePage := TCustomRVData(RVData).GetDefaultCodePage;
    Result := inherited AsText(LineWidth, RVData, Text, Path, TextOnly, Unicode,
      CodePage);
    Result := GetStr('[',ACodePage,Unicode)+Result+GetStr(']',ACodePage,Unicode);
  end;
end;
{========================= TRVFootnoteItemInfo ================================}
constructor TRVFootnoteItemInfo.Create(RVData: TPersistent);
begin
  inherited;
  Init(RVData);
end;
{------------------------------------------------------------------------------}
constructor TRVFootnoteItemInfo.CreateEx(RVData: TPersistent; ATextStyleNo,
  AStartFrom: Integer; AReset: Boolean);
begin
  inherited CreateEx(RVData, ATextStyleNo, AStartFrom, AReset);
  Init(RVData);
end;
{------------------------------------------------------------------------------}
function TRVFootnoteItemInfo.GetNumberType: TRVSeqType;
begin
  Result := TCustomRVData(ParentRVData).GetRVStyle.FootnoteNumbering;
end;
{------------------------------------------------------------------------------}
function TRVFootnoteItemInfo.GetTextForPrintMeasuring(RVData: TPersistent): String;
begin
  if TCustomRVData(ParentRVData).GetRVStyle.FootnotePageReset then
    Result := GetDisplayString(TCustomRVData(RVData).GetSeqList(False), 4)
  else
    Result := inherited GetTextForPrintMeasuring(RVData);
end;
{------------------------------------------------------------------------------}
function TRVFootnoteItemInfo.GetTextForPrinting(RVData: TPersistent;
  DrawItem: TRVDrawLineInfo): String;
begin
  if DrawItem is TRVFootnoteDrawItem then
    Result := GetDisplayString(nil, TRVFootnoteDrawItem(DrawItem).DocumentRVData.IndexOnPage)
  else
    Result := inherited GetTextForPrinting(RVData, DrawItem);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTF}
function TRVFootnoteItemInfo.GetRTFDestinationModifier: TRVAnsiString;
begin
  Result := '{}'; // workaround for MS Word glitch
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVFootnoteItemInfo.AsText(LineWidth: Integer; RVData: TPersistent;
  const Text: TRVRawByteString; const Path: String; TextOnly, Unicode: Boolean;
  CodePage: TRVCodePage): TRVRawByteString;
begin
  Result := DoGetAsText(LineWidth, RVData, Text, Path, TextOnly, Unicode, CodePage, True);
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteItemInfo.CalcDisplayString(List: TRVSeqList);
begin
  if SRVCounter>=0 then begin
    Text := GetDisplayString(List, SRVCounter);
    FUpdated := False;
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
function TRVFootnoteItemInfo.CreatePrintingDrawItem(RVData, ParaStyle: TObject;
  const sad: TRVScreenAndDevice): TRVDrawLineInfo;
begin
  if TCustomRVData(ParentRVData).GetRVStyle.FootnotePageReset then
    Result := TRVFootnoteDrawItem.Create
  else
    Result := inherited CreatePrintingDrawItem(RVData, ParaStyle, sad);
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteItemInfo.Init(RVData: TPersistent);
begin
  FDocument := TRVNoteData.Create(Self, TCustomRVData(RVData));
  FDocument.MainRVData := TCustomRVData(ParentRVData).GetAbsoluteRootData;
  StyleNo := rvsFootnote;
  SeqName := RV_FOOTNOTE_SEQNAME;
  SRVCounter := -1;
end;
{------------------------------------------------------------------------------}
procedure TRVFootnoteItemInfo.OnDocWidthChange(DocWidth: Integer; dli: TRVDrawLineInfo;
  Printing: Boolean; Canvas: TCanvas; RVData: TPersistent;
  sad: PRVScreenAndDevice; var HShift, Desc: Integer;
  NoCaching, Reformatting, UseFormatCanvas: Boolean);
begin
  SRVCounter := -1;
  inherited;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEHTML}
function TRVFootnoteItemInfo.GetHTMLAnchorName: TRVAnsiString;
begin
  Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(RV_FOOTNOTE_HTML_ANCHOR, [FCachedIndexInList]);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCX}
procedure TRVFootnoteItemInfo.SaveOOXML(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
var CurPart: TRVDocXPart;
    CustomMarkStr: TRVAnsiString;
begin
  if SavingData.CurInTextBox or
    not (SavingData.CurPart in [rvdocxpMain, rvdocxpHeader, rvdocxpFooter,
    rvdocxpHeader1, rvdocxpFooter1, rvdocxpHeaderEven, rvdocxpFooterEven]) then
    exit;
  if SavingData.FootnotesStream=nil then
    SavingData.FootnotesStream := TRVMemoryStream.Create;
  inc(SavingData.FootnoteIndex);
  if Length(FormatString)=1 then
    CustomMarkStr := ' w:customMarkFollows="1"'
  else
    CustomMarkStr := '';
  RVFWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:footnoteReference%s w:id="%d"/>', [CustomMarkStr, SavingData.FootnoteIndex]));
  if Length(FormatString)=1 then
    RVFWrite(Stream, '<w:t>'+RV_MakeOOXMLStr(FormatString)+'</w:t>');
  RVFWrite(SavingData.FootnotesStream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:footnote w:id="%d">', [SavingData.FootnoteIndex]));
  CurPart := SavingData.CurPart;
  SavingData.CurPart := rvdocxpFootnotes;
  try
    Document.SaveOOXMLDocumentToStream(SavingData.FootnotesStream, '', False, False,
      False, False, clNone, nil, SavingData);
  finally
    SavingData.CurPart := CurPart;
  end;
  RVFWrite(SavingData.FootnotesStream, '</w:footnote>');
end;
{$ENDIF}
{=========================== TRVNoteReferenceItemInfo =========================}
constructor TRVNoteReferenceItemInfo.Create(RVData: TPersistent);
begin
  inherited CreateEx(RVData, TextStyleNo, TCustomRVData(RVData).GetNoteText);
  StyleNo := rvsNoteReference;
end;
{------------------------------------------------------------------------------}
constructor TRVNoteReferenceItemInfo.CreateEx(RVData: TPersistent;
  TextStyleNo: Integer);
begin
  inherited CreateEx(RVData, TextStyleNo, TCustomRVData(RVData).GetNoteText);
  StyleNo := rvsNoteReference;
end;
{------------------------------------------------------------------------------}
procedure TRVNoteReferenceItemInfo.OnDocWidthChange(DocWidth: Integer;
  dli: TRVDrawLineInfo; Printing: Boolean; Canvas: TCanvas;
  RVData: TPersistent; sad: PRVScreenAndDevice; var HShift, Desc: Integer;
  NoCaching, Reformatting, UseFormatCanvas: Boolean);
begin
  if Text<>TCustomRVData(ParentRVData).GetNoteText then
    Text := TCustomRVData(ParentRVData).GetNoteText;
  inherited;
end;
{------------------------------------------------------------------------------}
function TRVNoteReferenceItemInfo.GetTextForPrinting(RVData: TPersistent;
  DrawItem: TRVDrawLineInfo): String;
begin
  if TCustomRVData(ParentRVData).GetRVStyle.FootnotePageReset and
     (RVData is TRVFootnotePtblRVData) then
    Result := TRVFootnotePtblRVData(RVData).Footnote.
      GetDisplayString(nil, TRVFootnotePtblRVData(RVData).IndexOnPage)
  else
    Result := TCustomRVData(ParentRVData).GetNoteText;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERVF}
function TRVNoteReferenceItemInfo.ReadRVFLine(const s: TRVRawByteString;
  RVData: TPersistent; var ReadType: Integer; LineNo, LineCount: Integer;
  var Name: TRVRawByteString; var ReadMode: TRVFReadMode;
  var ReadState: TRVFReadState; UTF8Strings: Boolean;
  var AssStyleNameUsed: Boolean): Boolean;
begin
  case LineNo of
    0..3:
      LoadPropertiesFromRVF(s, LineNo, RVData, UTF8Strings, AssStyleNameUsed);
    4:
      begin
        Name := s;
        ParentRVData := RVData;
        Text := TCustomRVData(RVData).GetNoteText;
      end;
    else
      SetExtraPropertyFromRVFStr(s, UTF8Strings);
  end;
  Result := True;
end;
{------------------------------------------------------------------------------}
procedure TRVNoteReferenceItemInfo.SaveRVF(Stream: TStream;
  RVData: TPersistent; ItemNo, ParaNo: Integer; const Name: TRVRawByteString;
  Part: TRVMultiDrawItemPart; ForceSameAsPrev: Boolean; PSaD: PRVScreenAndDevice);
begin
   RVFWriteLine(Stream,
     {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format('%d %d %s %d %d %s %s',
       [StyleNo, 5+GetRVFExtraPropertyCount {Line count after header},
        RVFItemSavePara(ParaNo, TCustomRVData(RVData), ForceSameAsPrev),
        Byte(RVFGetItemOptions(ItemOptions, ForceSameAsPrev)) and RVItemOptionsMask,
        RVF_SAVETYPE_TEXT,
        RVFSaveTag(rvoTagsArePChars in TCustomRVData(RVData).Options,Tag),
        SaveRVFHeaderTail(RVData)]));
   // lines after header
   {0,1,2,3}SavePropertiesToRVF(Stream, RVData);
   {4}RVFWriteLine(Stream, Name);
   SaveRVFExtraProperties(Stream);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTF}
procedure TRVNoteReferenceItemInfo.SaveRTF(Stream: TStream; RVData: TPersistent;
  ItemNo, Level: Integer; var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
  HiddenParent: Boolean);
var Note: TCustomRVNoteItemInfo;
    SeqList: TRVSeqList;
    Index: Integer;
begin
  case DocType of
    rvrtfdtNote:
      if Hidden or HiddenParent then
        RVFWrite(Stream, '{\v\chftn}')
      else
        RVFWrite(Stream, '\chftn ');
    rvrtfdtSidenote:
      begin
        Note := FindNote;
        if Note=nil then
          exit;
        if Hidden or HiddenParent then
          RVFWrite(Stream, '{\v');
        SeqList := TCustomRVData(Note.ParentRVData).GetSeqList(False);
        Index := Note.GetIndexInList(SeqList);
        RVFWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
          '{\field{\*\fldinst REF _sidenote%d}{\fldrslt %s}}', [Index, Note.NoteText]));
        if Hidden or HiddenParent then
          RVFWrite(Stream, '}');
      end;
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVNoteReferenceItemInfo.AsText(LineWidth: Integer;
  RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
  TextOnly, Unicode: Boolean; CodePage: TRVCodePage): TRVRawByteString;
var Note: TCustomRVNoteItemInfo;
begin
  Result := '';
  Note := FindNote;
  if Note=nil then
    exit;
  Result := GetAsText(RVData, Note.NoteText, Unicode, CodePage);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCX}
procedure TRVNoteReferenceItemInfo.SaveOOXMLBeforeRun(Stream: TStream;
  RVData: TPersistent; ItemNo: Integer; SavingData: TRVDocXSavingData;
  HiddenParent: Boolean);
var Note: TCustomRVNoteItemInfo;
    SeqList: TRVSeqList;
    Index: Integer;
begin
  Note := FindNote;
  if (Note=nil) or (Note.GetSubDocRTFDocType<>rvrtfdtSidenote) then
    exit;
  SeqList := TCustomRVData(Note.ParentRVData).GetSeqList(False);
  Index := Note.GetIndexInList(SeqList);
  RVFWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:fldSimple w:instr=" REF _sidenote%d">', [Index]));
end;
{------------------------------------------------------------------------------}
procedure TRVNoteReferenceItemInfo.SaveOOXMLAfterRun(Stream: TStream;
  RVData: TPersistent; ItemNo: Integer; SavingData: TRVDocXSavingData;
  HiddenParent: Boolean);
var Note: TCustomRVNoteItemInfo;
begin
  Note := FindNote;
  if (Note=nil) or (Note.GetSubDocRTFDocType<>rvrtfdtSidenote) then
    exit;
  RVFWrite(Stream, '</w:fldSimple>');
end;
{------------------------------------------------------------------------------}
procedure TRVNoteReferenceItemInfo.SaveOOXML(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
var Note: TCustomRVNoteItemInfo;
begin
  Note := FindNote;
  if Note=nil then
    exit;
  case Note.GetSubDocRTFDocType of
    rvrtfdtNote:
      case Note.StyleNo of
        rvsFootnote:
          RVFWrite(Stream, '<w:footnoteRef/>');
        rvsEndnote:
          RVFWrite(Stream, '<w:endnoteRef/>');
      end;
    rvrtfdtSidenote:
      RVFWrite(Stream, '<w:t>'+RV_MakeOOXMLStr(Note.NoteText)+'</w:t>');
  end;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEHTML}
procedure TRVNoteReferenceItemInfo.SaveToHTML(Stream: TStream;
  RVData: TPersistent; ItemNo: Integer; const Text: TRVRawByteString; const Path,
  imgSavePrefix: String; var imgSaveNo: Integer; CurrentFileColor: TColor;
  SaveOptions: TRVSaveOptions; UseCSS: Boolean; Bullets: TRVList
  {$IFNDEF RVDONOTUSELISTS};ListBullets: TRVHTMLListCSSList{$ENDIF});
begin
  Self.Text := TCustomRVData(ParentRVData).GetNoteText;
  inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVNoteReferenceItemInfo.FindNote: TCustomRVNoteItemInfo;
var RVData: TCustomRVData;
begin
  Result := nil;
  RVData := TCustomRVData(ParentRVData);
  while (RVData<>nil) and not (RVData is TRVSubData) do
    RVData := RVData.GetAbsoluteParentData;
  if RVData<>nil then
    Result := TRVSubData(RVData).Owner as TCustomRVNoteItemInfo;
end;
{================================ TRVNoteData =================================}
function TRVNoteData.GetNoteText: String;
begin
  Result := TCustomRVNoteItemInfo(Owner).Text;
end;
{------------------------------------------------------------------------------}
function TRVNoteData.GetNoteTextForPrinting: String;
begin
  if FNoteTextForPrinting='' then
    Result := GetNoteText
  else
    Result := FNoteTextForPrinting;
end;
{==============================================================================}
function RVGetNoteTextStyleNo(RVStyle: TRVStyle; StyleNo: Integer
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}; StyleTemplateName: TRVStyleTemplateName{$ENDIF}): Integer;
var TextStyle: TFontInfo;
   {$IFNDEF RVDONOTUSESTYLETEMPLATES}
   OldStyleTemplate, NewStyleTemplate, ParaStyleTemplate: TRVStyleTemplate;
   {$ENDIF}
begin
  TextStyle := DefRVFontInfoClass.Create(nil);
  try
    TextStyle.Assign(RVStyle.TextStyles[StyleNo]);
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    if StyleTemplateName<>'' then begin
      NewStyleTemplate := RVStyle.StyleTemplates.FindItemByName(StyleTemplateName);
      if (NewStyleTemplate<>nil) and (NewStyleTemplate.Kind<>rvstkPara) then begin
        OldStyleTemplate := RVStyle.StyleTemplates.FindItemById(RVStyle.TextStyles[StyleNo].StyleTemplateId);
        ParaStyleTemplate := RVStyle.StyleTemplates.FindItemById(RVStyle.TextStyles[StyleNo].ParaStyleTemplateId);
        OldStyleTemplate.UpdateModifiedTextStyleProperties(TextStyle, ParaStyleTemplate{, False});
        NewStyleTemplate.ApplyToTextStyle(TextStyle, ParaStyleTemplate);
        end
      else
        TextStyle.SubSuperScriptType := rvsssSuperScript;
      end
    else
    {$ENDIF}
      TextStyle.SubSuperScriptType := rvsssSuperScript;
    TextStyle.Protection := [rvprDoNotAutoSwitch];
    Result := RVStyle.TextStyles.FindSuchStyleEx(StyleNo, TextStyle,
      RVAllFontInfoProperties);
  finally
    TextStyle.Free;
  end;
end;
{------------------------------------------------------------------------------}
function GetNextNote(RVData: TCustomRVData; Note: TCustomRVNoteItemInfo;
  NoteClass: TCustomRVNoteItemInfoClass; AllowHidden: Boolean): TCustomRVNoteItemInfo;
var SeqList: TRVSeqList;
  i,j: Integer;
begin
  if rvoShowHiddenText in RVData.Options then
    AllowHidden := True;
  Result := nil;
  SeqList := RVData.GetSeqList(False);
  if SeqList=nil then
    exit;
  if Note<>nil then
    j := Note.GetIndexInList(SeqList)+1
  else
    j := 0;
  for i := j to SeqList.Count-1 do
    if (TRVSeqItemInfo(SeqList.Items[i]) is NoteClass) and
      (AllowHidden or not TRVSeqItemInfo(SeqList.Items[i]).Hidden) then begin
      Result := SeqList.Items[i];
      exit;
    end;
end;
{------------------------------------------------------------------------------}
function RVGetFirstEndnote(RichView: TCustomRichView; AllowHidden: Boolean): TRVEndnoteItemInfo;
begin
  Result := TRVEndnoteItemInfo(GetNextNote(RichView.RVData, nil,
    TRVEndnoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetFirstEndnoteInRootRVData(RVData: TCustomRVData; AllowHidden: Boolean): TRVEndnoteItemInfo;
begin
  Result := TRVEndnoteItemInfo(GetNextNote(RVData, nil,
    TRVEndnoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetNextEndnote(RichView: TCustomRichView;
  Endnote: TRVEndnoteItemInfo; AllowHidden: Boolean): TRVEndnoteItemInfo;
begin
  Result := TRVEndnoteItemInfo(GetNextNote(RichView.RVData, Endnote,
    TRVEndnoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetFirstFootnote(RichView: TCustomRichView; AllowHidden: Boolean): TRVFootnoteItemInfo;
begin
  Result := TRVFootnoteItemInfo(GetNextNote(RichView.RVData, nil,
    TRVFootnoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetNextFootnote(RichView: TCustomRichView;
  Footnote: TRVFootnoteItemInfo; AllowHidden: Boolean): TRVFootnoteItemInfo;
begin
  Result := TRVFootnoteItemInfo(GetNextNote(RichView.RVData, Footnote,
    TRVFootnoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetFirstFootnoteInRootRVData(RVData: TCustomRVData; AllowHidden: Boolean): TRVFootnoteItemInfo;
begin
  Result := TRVFootnoteItemInfo(GetNextNote(RVData, nil,
    TRVFootnoteItemInfo, AllowHidden));
end;


initialization

  RegisterRichViewItemClass(rvsEndnote, TRVEndnoteItemInfo);
  RegisterRichViewItemClass(rvsFootnote, TRVFootnoteItemInfo);
  RegisterRichViewItemClass(rvsNoteReference, TRVNoteReferenceItemInfo);

{$ENDIF}

end.