﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVMapWht.pas' rev: 27.00 (Windows)

#ifndef RvmapwhtHPP
#define RvmapwhtHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvmapwht
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
static const System::Word RVSMW_FONTNAME = System::Word(0x2710);
static const System::Word RVSMW_FONTSIZE = System::Word(0x2710);
static const System::Word RVSMW_FONTCHARSET = System::Word(0xc350);
static const System::Word RVSMW_EACHRGBCOLOR = System::Word(0xd05);
static const System::Word RVSMW_COLORSET = System::Word(0xbb8);
static const System::Word RVSMW_ALLCAPS = System::Word(0xc350);
static const System::Word RVSMW_OVERLINE = System::Word(0xc350);
static const System::Word RVSMW_EACHRGBBCOLOR = System::Word(0x2710);
static const System::Word RVSMW_BCOLORSET = System::Word(0xc350);
static const System::Word RVSMW_FONTEACHSTYLE = System::Word(0xc350);
static const System::Word RVSMW_HOVEREACHEFFECT = System::Word(0x2710);
static const System::Word RVSMW_FONTSTYLESET = System::Word(0xc350);
static const System::Word RVSMW_CHARSCALE = System::Word(0x12c);
static const System::Word RVSMW_CHARSPACING = System::Word(0x3e8);
static const System::Word RVSMW_TEXTBIDIMODE = System::Word(0x3e8);
static const System::Word RVSMW_VSHIFT = System::Word(0xc350);
static const System::Word RVSMW_VSHIFTRATIO = System::Word(0x3e8);
static const System::Word RVSMW_CURSOR = System::Word(0x1388);
static const int RVSMW_PROTECTION = int(0x186a0);
static const System::Word RVSMW_SPECIALCODE = System::Word(0xc350);
static const System::Word RVSMW_HIDDEN = System::Word(0x1388);
static const System::Int8 RVSMW_LANGUAGE = System::Int8(0x64);
static const System::Word RVSMW_SUBSUPERSCRIPTTYPE = System::Word(0xc350);
static const System::Word RVSNW_PROTECTION = System::Word(0xc350);
static const System::Word RVSMW_ALIGNMENT = System::Word(0xc350);
static const System::Word RVSMW_PARABIDIMODE = System::Word(0x2710);
static const System::Word RVSMW_OUTLINELEVEL = System::Word(0xc350);
static const System::Word RVSMW_LINESPACING = System::Word(0x1388);
static const System::Word RVSMW_INDENT = System::Word(0x3e8);
static const System::Word RVSMW_BORDERSIDE = System::Word(0x1388);
static const System::Word RVSMW_BORDERNOSIDE = System::Word(0x4e20);
static const System::Word RVSMW_WIDTH = System::Word(0x3e8);
static const System::Word RVSMW_BORDERSTYLE = System::Word(0x3e8);
static const System::Int8 RVSMW_PADDING = System::Int8(0x64);
static const System::Word RVSMW_NOWRAP = System::Word(0x3e8);
static const int RVSMW_READONLY = int(0x186a0);
static const System::Word RVSMW_STYLEPROTECT = System::Word(0x3e8);
static const System::Word RVSMW_DONOTWANTRETURNS = System::Word(0x2710);
static const System::Word RVSMW_KEEPLINESTOGETHER = System::Word(0x1f4);
static const System::Word RVSMW_KEEPWITHNEXT = System::Word(0x1f4);
static const System::Byte RVSMW_TABPOS = System::Byte(0xc8);
static const System::Int8 RVSMW_TABALIGN = System::Int8(0x64);
static const System::Int8 RVSMW_LEADER = System::Int8(0x32);
static const System::Word RVSMW_NOTAB = System::Word(0x190);
static const System::Word RVMW_LISTTYPE = System::Word(0x1388);
static const System::Int8 RVMW_LISTMISC = System::Int8(0x64);
extern DELPHI_PACKAGE int __fastcall RV_CompareInts(int New, int Old, int Weight);
extern DELPHI_PACKAGE int __fastcall RV_CompareColors(System::Uitypes::TColor Color1, System::Uitypes::TColor Color2, int w1, int w2);
}	/* namespace Rvmapwht */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVMAPWHT)
using namespace Rvmapwht;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvmapwhtHPP
