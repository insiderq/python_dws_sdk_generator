﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVBack.pas' rev: 27.00 (Windows)

#ifndef RvbackHPP
#define RvbackHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvback
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVBackground;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBackground : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Vcl::Graphics::TGraphic* ImageCopy;
	bool FHasThumbnail;
	Vcl::Graphics::TBitmap* __fastcall GetBitmap(void);
	Vcl::Graphics::TBitmap* __fastcall GetThumbnail(void);
	Rvstyle::TRVItemBackgroundStyle __fastcall GetItemBackStyle(void);
	void __fastcall SetItemBackStyle(const Rvstyle::TRVItemBackgroundStyle Value);
	
public:
	Rvscroll::TBackgroundStyle Style;
	Vcl::Graphics::TGraphic* Image;
	__fastcall TRVBackground(bool CreateBitmap);
	__fastcall virtual ~TRVBackground(void);
	bool __fastcall ScrollRequiresFullRedraw(void);
	void __fastcall UpdatePaletted(Rvscroll::TRVPaletteAction PaletteAction, HPALETTE Palette, Winapi::Windows::PLogPalette LogPalette);
	void __fastcall FreeThumbnail(void);
	void __fastcall UpdateThumbnail(System::TObject* RVData, System::TObject* BackgroundOwner, int Width, int Height);
	void __fastcall Draw(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect, int HOffs, int VOffs, int Left, int Top, int Width, int Height, System::Uitypes::TColor Color, bool Clipping, bool PrintSimulation, Rvgrin::TRVGraphicInterface* GraphicInterface, System::TObject* RVData, System::TObject* BackgroundOwner, bool AllowThumbnails);
	void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &ARect, const System::Types::TRect &AFullRect, const Rvstyle::TRVScreenAndDevice &sad, System::Uitypes::TColor Color, bool Preview, Winapi::Windows::PLogPalette LogPalette, System::Classes::TPersistent* PrintingRVData, int ItemBackgroundLayer, Rvgrin::TRVGraphicInterface* GraphicInterface, int PageNo);
	bool __fastcall Empty(void);
	bool __fastcall EmptyImage(void);
	bool __fastcall Visible(void);
	bool __fastcall IsSemitransparent(void);
	void __fastcall FreeImage(void);
	void __fastcall AssignImage(Vcl::Graphics::TGraphic* AImage, System::Classes::TPersistent* ARVData, bool Copy, System::TObject* BackgroundOwner);
	__property Vcl::Graphics::TBitmap* Bitmap = {read=GetBitmap};
	__property Vcl::Graphics::TBitmap* Thumbnail = {read=GetThumbnail};
	__property Rvstyle::TRVItemBackgroundStyle ItemBackStyle = {read=GetItemBackStyle, write=SetItemBackStyle, nodefault};
	__property bool HasThumbnail = {read=FHasThumbnail, nodefault};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvback */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVBACK)
using namespace Rvback;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvbackHPP
