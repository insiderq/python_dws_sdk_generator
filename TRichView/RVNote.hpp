﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVNote.pas' rev: 27.00 (Windows)

#ifndef RvnoteHPP
#define RvnoteHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVSubData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvnote
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVNoteData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVNoteData : public Rvsubdata::TRVSubData
{
	typedef Rvsubdata::TRVSubData inherited;
	
public:
	System::UnicodeString FNoteTextForPrinting;
	DYNAMIC System::UnicodeString __fastcall GetNoteText(void);
	System::UnicodeString __fastcall GetNoteTextForPrinting(void);
public:
	/* TRVSubData.Create */ inline __fastcall TRVNoteData(Rvitem::TCustomRVItemInfo* AOwner, Crvdata::TCustomRVData* AMainRVData) : Rvsubdata::TRVSubData(AOwner, AMainRVData) { }
	
public:
	/* TCustomRVData.Destroy */ inline __fastcall virtual ~TRVNoteData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVNoteFormattingData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVNoteFormattingData : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	DYNAMIC void __fastcall SaveToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFromStream(System::Classes::TStream* Stream);
public:
	/* TObject.Create */ inline __fastcall TRVNoteFormattingData(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVNoteFormattingData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TCustomRVNoteItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVNoteItemInfo : public Rvseqitem::TRVSeqItemInfo
{
	typedef Rvseqitem::TRVSeqItemInfo inherited;
	
protected:
	TRVNoteData* FDocument;
	virtual void __fastcall SetParentRVData(System::Classes::TPersistent* const Value);
	void __fastcall Do_ChangeDoc(System::Classes::TStream* Stream, int ItemNo);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	virtual void __fastcall SetNumberType(const Rvstyle::TRVSeqType Value);
	virtual System::UnicodeString __fastcall GetNoteText(void);
	Rvtypes::TRVRawByteString __fastcall DoGetAsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage, bool LeadingSpace);
	
public:
	TRVNoteFormattingData* SRVFormattingData;
	__fastcall virtual TCustomRVNoteItemInfo(System::Classes::TPersistent* RVData, int ATextStyleNo, int AStartFrom, bool AReset);
	__fastcall virtual ~TCustomRVNoteItemInfo(void);
	DYNAMIC bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, Rvitem::TRVFReadMode &ReadMode, Rvitem::TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall FillRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList, System::Classes::TPersistent* RVData);
	DYNAMIC Rvitem::TRVRTFDocType __fastcall GetSubDocRTFDocType(void);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetHTMLAnchorName(void);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	virtual bool __fastcall GetBoolValue(Rvitem::TRVItemBoolProperty Prop);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall ChangeParagraphStyles(Rvclasses::TRVIntegerList* ParaMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall ChangeTextStyles(Rvclasses::TRVIntegerList* TextMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall Assign(Rvitem::TCustomRVItemInfo* Source);
	void __fastcall ReplaceDocumentEd(System::Classes::TStream* Stream);
	DYNAMIC bool __fastcall IsFixedHeight(void);
	DYNAMIC System::Uitypes::TColor __fastcall GetSubDocColor(void);
	DYNAMIC void __fastcall SaveFormattingToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFormattingFromStream(System::Classes::TStream* Stream);
	__property TRVNoteData* Document = {read=FDocument};
	__property System::UnicodeString NoteText = {read=GetNoteText};
public:
	/* TRVSeqItemInfo.Create */ inline __fastcall virtual TCustomRVNoteItemInfo(System::Classes::TPersistent* RVData) : Rvseqitem::TRVSeqItemInfo(RVData) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFootOrEndnoteItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFootOrEndnoteItemInfo : public TCustomRVNoteItemInfo
{
	typedef TCustomRVNoteItemInfo inherited;
	
protected:
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetRTFDestinationModifier(void);
	
public:
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLBeforeRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLAfterRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
public:
	/* TCustomRVNoteItemInfo.CreateEx */ inline __fastcall virtual TRVFootOrEndnoteItemInfo(System::Classes::TPersistent* RVData, int ATextStyleNo, int AStartFrom, bool AReset) : TCustomRVNoteItemInfo(RVData, ATextStyleNo, AStartFrom, AReset) { }
	/* TCustomRVNoteItemInfo.Destroy */ inline __fastcall virtual ~TRVFootOrEndnoteItemInfo(void) { }
	
public:
	/* TRVSeqItemInfo.Create */ inline __fastcall virtual TRVFootOrEndnoteItemInfo(System::Classes::TPersistent* RVData) : TCustomRVNoteItemInfo(RVData) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVEndnoteItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEndnoteItemInfo : public TRVFootOrEndnoteItemInfo
{
	typedef TRVFootOrEndnoteItemInfo inherited;
	
private:
	void __fastcall Init(System::Classes::TPersistent* RVData);
	
protected:
	virtual Rvstyle::TRVSeqType __fastcall GetNumberType(void);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetRTFDestinationModifier(void);
	
public:
	__fastcall virtual TRVEndnoteItemInfo(System::Classes::TPersistent* RVData, int ATextStyleNo, int AStartFrom, bool AReset);
	__fastcall virtual TRVEndnoteItemInfo(System::Classes::TPersistent* RVData);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetHTMLAnchorName(void);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
public:
	/* TCustomRVNoteItemInfo.Destroy */ inline __fastcall virtual ~TRVEndnoteItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFootnoteItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFootnoteItemInfo : public TRVFootOrEndnoteItemInfo
{
	typedef TRVFootOrEndnoteItemInfo inherited;
	
private:
	void __fastcall Init(System::Classes::TPersistent* RVData);
	
protected:
	virtual Rvstyle::TRVSeqType __fastcall GetNumberType(void);
	DYNAMIC System::UnicodeString __fastcall GetTextForPrintMeasuring(System::Classes::TPersistent* RVData);
	virtual System::UnicodeString __fastcall GetTextForPrinting(System::Classes::TPersistent* RVData, Dlines::TRVDrawLineInfo* DrawItem);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetRTFDestinationModifier(void);
	
public:
	int SRVCounter;
	int SRVOldCounter;
	__fastcall virtual TRVFootnoteItemInfo(System::Classes::TPersistent* RVData, int ATextStyleNo, int AStartFrom, bool AReset);
	__fastcall virtual TRVFootnoteItemInfo(System::Classes::TPersistent* RVData);
	virtual Dlines::TRVDrawLineInfo* __fastcall CreatePrintingDrawItem(System::TObject* RVData, System::TObject* ParaStyle, const Rvstyle::TRVScreenAndDevice &sad);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetHTMLAnchorName(void);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall CalcDisplayString(Rvseqitem::TRVSeqList* List);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
public:
	/* TCustomRVNoteItemInfo.Destroy */ inline __fastcall virtual ~TRVFootnoteItemInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVNoteReferenceItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVNoteReferenceItemInfo : public Rvlabelitem::TRVLabelItemInfo
{
	typedef Rvlabelitem::TRVLabelItemInfo inherited;
	
protected:
	virtual System::UnicodeString __fastcall GetTextForPrinting(System::Classes::TPersistent* RVData, Dlines::TRVDrawLineInfo* DrawItem);
	TCustomRVNoteItemInfo* __fastcall FindNote(void);
	
public:
	__fastcall virtual TRVNoteReferenceItemInfo(System::Classes::TPersistent* RVData);
	__fastcall TRVNoteReferenceItemInfo(System::Classes::TPersistent* RVData, int TextStyleNo);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, Rvitem::TRVFReadMode &ReadMode, Rvitem::TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLBeforeRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLAfterRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVNoteReferenceItemInfo(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TCustomRVNoteItemInfoClass;

typedef System::TMetaClass* TRVEndnoteItemInfoClass;

//-- var, const, procedure ---------------------------------------------------
#define RV_FOOTNOTE_SEQNAME L"@footnote@"
#define RV_ENDNOTE_SEQNAME L"@endnote@"
#define RV_FOOTNOTE_HTML_ANCHOR L"_footnote%d"
#define RV_ENDNOTE_HTML_ANCHOR L"_endnote%d"
static const short rvsFootnote = short(-203);
static const short rvsEndnote = short(-204);
static const short rvsNoteReference = short(-205);
extern DELPHI_PACKAGE int __fastcall RVGetNoteTextStyleNo(Rvstyle::TRVStyle* RVStyle, int StyleNo, Rvstyle::TRVStyleTemplateName StyleTemplateName = Rvstyle::TRVStyleTemplateName());
extern DELPHI_PACKAGE TCustomRVNoteItemInfo* __fastcall GetNextNote(Crvdata::TCustomRVData* RVData, TCustomRVNoteItemInfo* Note, TCustomRVNoteItemInfoClass NoteClass, bool AllowHidden);
extern DELPHI_PACKAGE TRVEndnoteItemInfo* __fastcall RVGetFirstEndnote(Richview::TCustomRichView* RichView, bool AllowHidden);
extern DELPHI_PACKAGE TRVEndnoteItemInfo* __fastcall RVGetFirstEndnoteInRootRVData(Crvdata::TCustomRVData* RVData, bool AllowHidden);
extern DELPHI_PACKAGE TRVEndnoteItemInfo* __fastcall RVGetNextEndnote(Richview::TCustomRichView* RichView, TRVEndnoteItemInfo* Endnote, bool AllowHidden);
extern DELPHI_PACKAGE TRVFootnoteItemInfo* __fastcall RVGetFirstFootnote(Richview::TCustomRichView* RichView, bool AllowHidden);
extern DELPHI_PACKAGE TRVFootnoteItemInfo* __fastcall RVGetNextFootnote(Richview::TCustomRichView* RichView, TRVFootnoteItemInfo* Footnote, bool AllowHidden);
extern DELPHI_PACKAGE TRVFootnoteItemInfo* __fastcall RVGetFirstFootnoteInRootRVData(Crvdata::TCustomRVData* RVData, bool AllowHidden);
}	/* namespace Rvnote */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVNOTE)
using namespace Rvnote;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvnoteHPP
