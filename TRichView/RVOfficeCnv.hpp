﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVOfficeCnv.pas' rev: 27.00 (Windows)

#ifndef RvofficecnvHPP
#define RvofficecnvHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvofficecnv
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVOfficeConverterInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVOfficeConverterInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Rvtypes::TRVAnsiString ClsName;
	System::UnicodeString Name;
	System::UnicodeString Path;
	System::UnicodeString Filter;
public:
	/* TObject.Create */ inline __fastcall TRVOfficeConverterInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVOfficeConverterInfo(void) { }
	
};

#pragma pack(pop)

typedef int __stdcall (*TInitConverter32)(HWND hwndWord, char * szModule);

typedef void __stdcall (*TUninitConverter)(void);

typedef int __stdcall (*TForeignToRtf32Callback)(int cchBuff, int nPercent);

typedef int __stdcall (*TRtfToForeign32Callback)(int rgfOptions, int nReserved);

typedef short __stdcall (*TForeignToRtf32)(NativeUInt ghszFile, void * pstgForeign, NativeUInt ghBuff, NativeUInt ghszClass, NativeUInt ghszSubset, TForeignToRtf32Callback lpfnOut);

typedef short __stdcall (*TRtfToForeign32)(NativeUInt ghszFile, void * pstgForeign, NativeUInt ghBuff, NativeUInt ghszClass, TRtfToForeign32Callback lpfnIn);

typedef NativeUInt __stdcall (*TRegisterApp)(int lFlags, void * lpRegApp);

typedef short __stdcall (*TIsFormatCorrect32)(NativeUInt ghszFile, NativeUInt ghszClass);

typedef void __fastcall (__closure *TConvertingEvent)(System::TObject* Sender, int Percent);

class DELPHICLASS TRVOfficeCnvList;
class DELPHICLASS TRVOfficeConverter;
class PASCALIMPLEMENTATION TRVOfficeCnvList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVOfficeConverterInfo* operator[](int Index) { return Items[Index]; }
	
private:
	TRVOfficeConverter* FOwner;
	NativeUInt hBuffer;
	TConvertingEvent FOnConverting;
	Richview::TCustomRichView* FRichView;
	Rvclasses::TRVMemoryStream* FStream;
	int FStep;
	int FStart;
	int FSize;
	HIDESBASE void __fastcall Put(int Index, TRVOfficeConverterInfo* Value);
	HIDESBASE TRVOfficeConverterInfo* __fastcall Get(int Index);
	void __fastcall LoadList(const System::UnicodeString RegPath, bool ExcludeHTML, bool ExcludeDocX);
	
public:
	__fastcall TRVOfficeCnvList(const System::UnicodeString RegPath, TRVOfficeConverter* Owner, bool ExcludeHTML, bool ExcludeDocX);
	System::UnicodeString __fastcall GetFilter(bool IncludeExtensions);
	void __fastcall ImportRTF(const System::UnicodeString FileName, int Index);
	void __fastcall ExportRTF(const System::UnicodeString FileName, int Index);
	bool __fastcall IsFormatCorrect(const System::UnicodeString FileName, int Index);
	__property TRVOfficeConverterInfo* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVOfficeCnvList(void) { }
	
};


class PASCALIMPLEMENTATION TRVOfficeConverter : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	TRVOfficeCnvList* FImportConverters;
	TRVOfficeCnvList* FExportConverters;
	TConvertingEvent FOnConverting;
	Rvclasses::TRVMemoryStream* FStream;
	bool FExcludeHTMLImportConverter;
	bool FExcludeHTMLExportConverter;
	bool FExcludeDocXExportConverter;
	bool FExcludeDocXImportConverter;
	bool FPreviewMode;
	int FErrorCode;
	bool FExtensionsInFilter;
	TRVOfficeCnvList* __fastcall GetExportConverters(void);
	TRVOfficeCnvList* __fastcall GetImportConverters(void);
	void __fastcall SetExcludeHTMLExportConverter(const bool Value);
	void __fastcall SetExcludeHTMLImportConverter(const bool Value);
	void __fastcall SetExcludeDocXExportConverter(const bool Value);
	void __fastcall SetExcludeDocXImportConverter(const bool Value);
	
public:
	__fastcall virtual TRVOfficeConverter(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVOfficeConverter(void);
	bool __fastcall ImportRTF(const System::UnicodeString FileName, int ConverterIndex);
	bool __fastcall ExportRTF(const System::UnicodeString FileName, int ConverterIndex);
	bool __fastcall ImportRV(const System::UnicodeString FileName, Richview::TCustomRichView* rv, int ConverterIndex);
	bool __fastcall ExportRV(const System::UnicodeString FileName, Richview::TCustomRichView* rv, int ConverterIndex);
	System::UnicodeString __fastcall GetImportFilter(void);
	System::UnicodeString __fastcall GetExportFilter(void);
	bool __fastcall IsValidImporter(const System::UnicodeString FileName, int Index);
	__property TRVOfficeCnvList* ImportConverters = {read=GetImportConverters};
	__property TRVOfficeCnvList* ExportConverters = {read=GetExportConverters};
	__property Rvclasses::TRVMemoryStream* Stream = {read=FStream};
	__property int ErrorCode = {read=FErrorCode, nodefault};
	
__published:
	__property bool ExcludeHTMLImportConverter = {read=FExcludeHTMLImportConverter, write=SetExcludeHTMLImportConverter, default=0};
	__property bool ExcludeHTMLExportConverter = {read=FExcludeHTMLExportConverter, write=SetExcludeHTMLExportConverter, default=0};
	__property bool ExcludeDocXImportConverter = {read=FExcludeDocXImportConverter, write=SetExcludeDocXImportConverter, default=0};
	__property bool ExcludeDocXExportConverter = {read=FExcludeDocXExportConverter, write=SetExcludeDocXExportConverter, default=0};
	__property bool PreviewMode = {read=FPreviewMode, write=FPreviewMode, default=0};
	__property bool ExtensionsInFilter = {read=FExtensionsInFilter, write=FExtensionsInFilter, default=0};
	__property TConvertingEvent OnConverting = {read=FOnConverting, write=FOnConverting};
};


//-- var, const, procedure ---------------------------------------------------
static const System::Int8 rvceCnvLoadError = System::Int8(0x1);
static const System::Int8 rvceFuncError = System::Int8(0x2);
static const System::Int8 rvceInitError = System::Int8(0x3);
static const System::Int8 rvceOpenInFileErr = System::Int8(-1);
static const System::Int8 rvceReadErr = System::Int8(-2);
static const System::Int8 rvceOpenConvErr = System::Int8(-3);
static const System::Int8 rvceWriteErr = System::Int8(-4);
static const System::Int8 rvceInvalidFile = System::Int8(-5);
static const System::Int8 rvceOpenExceptErr = System::Int8(-6);
static const System::Int8 rvceWriteExceptErr = System::Int8(-7);
static const System::Int8 rvceNoMemory = System::Int8(-8);
static const System::Int8 rvceInvalidDoc = System::Int8(-9);
static const System::Int8 rvceDiskFull = System::Int8(-10);
static const System::Int8 rvceDocTooLarge = System::Int8(-11);
static const System::Int8 rvceOpenOutFileErr = System::Int8(-12);
static const System::Int8 rvceUserCancel = System::Int8(-13);
static const System::Int8 rvceWrongFileType = System::Int8(-14);
}	/* namespace Rvofficecnv */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVOFFICECNV)
using namespace Rvofficecnv;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvofficecnvHPP
