﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVReport.pas' rev: 27.00 (Windows)

#ifndef RvreportHPP
#define RvreportHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <PtRVData.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <RVCtrlData.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <RVRTFProps.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvreport
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVReportHelper;
typedef void __fastcall (__closure *TRVDrawHyperlinkEvent)(TRVReportHelper* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);

typedef void __fastcall (__closure *TRVDrawCheckpointEvent)(TRVReportHelper* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, int X, int Y);

class DELPHICLASS TReportRVData;
class PASCALIMPLEMENTATION TReportRVData : public Ptrvdata::TCustomMainPtblRVData
{
	typedef Ptrvdata::TCustomMainPtblRVData inherited;
	
private:
	int StartY;
	int StartAt;
	int Y;
	int DrawItemNo;
	int CurHeight;
	bool Splitting;
	bool FNoMetafiles;
	void __fastcall StoreMargins(void);
	void __fastcall RestoreMargins(void);
	void __fastcall Init(Vcl::Graphics::TCanvas* ACanvas, int APageWidth);
	HIDESBASE bool __fastcall FormatNextPage(int AMaxHeight);
	bool __fastcall Finished(void);
	void __fastcall Reset(void);
	
protected:
	DYNAMIC bool __fastcall ShareItems(void);
	virtual void __fastcall DoOnHyperlink(Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);
	virtual void __fastcall DoOnCheckpoint(Crvdata::TCustomRVData* RVData, int ItemNo, int X, int Y);
	DYNAMIC bool __fastcall NoMetafiles(void);
	
public:
	virtual int __fastcall GetHeight(void);
	DYNAMIC int __fastcall GetPageHeight(void);
	DYNAMIC int __fastcall GetPageWidth(void);
	virtual int __fastcall GetContentHeightForPage(int PageNo);
public:
	/* TCustomMainPtblRVData.Create */ inline __fastcall virtual TReportRVData(Rvscroll::TRVScroller* RichView) : Ptrvdata::TCustomMainPtblRVData(RichView) { }
	
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TReportRVData(void) { }
	
};


class DELPHICLASS TReportRichView;
class PASCALIMPLEMENTATION TReportRichView : public Ptblrv::TCustomPrintableRV
{
	typedef Ptblrv::TCustomPrintableRV inherited;
	
private:
	int __fastcall GetHeight(void);
	int __fastcall GetWidth(void);
	int __fastcall GetLeft(void);
	int __fastcall GetTop(void);
	
protected:
	virtual Rvrvdata::TRichViewRVDataClass __fastcall GetDataClass(void);
	virtual void __fastcall SetBiDiModeRV(const Rvscroll::TRVBiDiMode Value);
	
public:
	__fastcall virtual TReportRichView(System::Classes::TComponent* AOwner);
	virtual bool __fastcall CanUseCustomPPI(void);
	HIDESBASE void __fastcall Format(void);
	
__published:
	__property Color = {default=536870911};
	__property BackgroundBitmap;
	__property BackgroundStyle;
	__property BiDiMode = {default=0};
	__property BottomMargin = {default=5};
	__property Delimiters = {default=0};
	__property LeftMargin = {default=5};
	__property Options = {default=1702949};
	__property RightMargin = {default=5};
	__property RTFOptions = {default=30};
	__property RTFReadProperties;
	__property RVFOptions = {default=98435};
	__property RVFParaStylesReadMode = {default=2};
	__property RVFTextStylesReadMode = {default=2};
	__property Style;
	__property TopMargin = {default=5};
	__property OnControlAction;
	__property OnHTMLSaveImage;
	__property OnRVFImageListNeeded;
	__property OnRVFControlNeeded;
	__property OnRVFPictureNeeded;
	__property OnSaveComponentToFile;
	__property OnURLNeeded;
	__property OnReadHyperlink;
	__property OnWriteHyperlink;
	__property int Width = {read=GetWidth, nodefault};
	__property int Height = {read=GetHeight, nodefault};
	__property int Left = {read=GetLeft, nodefault};
	__property int Top = {read=GetTop, nodefault};
public:
	/* TCustomPrintableRV.Destroy */ inline __fastcall virtual ~TReportRichView(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TReportRichView(HWND ParentWindow) : Ptblrv::TCustomPrintableRV(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TRVReportHelper : public Ptblrv::TCustomRVPrint
{
	typedef Ptblrv::TCustomRVPrint inherited;
	
private:
	TRVDrawHyperlinkEvent FOnDrawHyperlink;
	TRVDrawCheckpointEvent FOnDrawCheckpoint;
	TReportRichView* __fastcall GetRichView(void);
	bool __fastcall GetNoMetafiles(void);
	void __fastcall SetNoMetafiles(const bool Value);
	
protected:
	DYNAMIC Ptblrv::TCustomPrintableRV* __fastcall CreateRichView(void);
	
public:
	__fastcall virtual TRVReportHelper(System::Classes::TComponent* AOwner);
	void __fastcall Init(Vcl::Graphics::TCanvas* ACanvas, int APageWidth);
	bool __fastcall FormatNextPage(int AMaxHeight);
	void __fastcall Reset(void);
	void __fastcall DrawPage(int APageNo, Vcl::Graphics::TCanvas* ACanvas, bool APreview, int AHeight);
	void __fastcall DrawPageAt(int Left, int Top, int APageNo, Vcl::Graphics::TCanvas* ACanvas, bool APreview, int AHeight);
	bool __fastcall Finished(void);
	int __fastcall GetLastPageHeight(void);
	
__published:
	__property TReportRichView* RichView = {read=GetRichView};
	__property ColorMode = {default=0};
	__property bool NoMetafiles = {read=GetNoMetafiles, write=SetNoMetafiles, nodefault};
	__property TRVDrawHyperlinkEvent OnDrawHyperlink = {read=FOnDrawHyperlink, write=FOnDrawHyperlink};
	__property TRVDrawCheckpointEvent OnDrawCheckpoint = {read=FOnDrawCheckpoint, write=FOnDrawCheckpoint};
public:
	/* TComponent.Destroy */ inline __fastcall virtual ~TRVReportHelper(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvreport */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVREPORT)
using namespace Rvreport;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvreportHPP
