﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVPopup.pas' rev: 27.00 (Windows)

#ifndef RvpopupHPP
#define RvpopupHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvpopup
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVSmartPopupProperties;
class DELPHICLASS TRVSmartPopupButton;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSmartPopupProperties : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	int FImageIndex;
	Vcl::Imglist::TCustomImageList* FImageList;
	System::Uitypes::TColor FColor;
	System::Uitypes::TColor FHoverLineColor;
	System::Uitypes::TColor FLineColor;
	System::Uitypes::TColor FHoverColor;
	Vcl::Menus::TPopupMenu* FMenu;
	System::UnicodeString FHint;
	System::Classes::TShortCut FShortCut;
	Rvscroll::TRVSmartPopupType FButtonType;
	TRVSmartPopupButton* FButton;
	Rvscroll::TRVSmartPopupPosition FPosition;
	void __fastcall SetImageIndex(const int Value);
	void __fastcall SetImageList(Vcl::Imglist::TCustomImageList* const Value);
	void __fastcall SetColor(const System::Uitypes::TColor Value);
	void __fastcall SetHoverColor(const System::Uitypes::TColor Value);
	void __fastcall SetHoverLineColor(const System::Uitypes::TColor Value);
	void __fastcall SetLineColor(const System::Uitypes::TColor Value);
	void __fastcall SetHint(const System::UnicodeString Value);
	bool __fastcall StoreHint(void);
	
public:
	Rvscroll::TRVScroller* RichView;
	__fastcall TRVSmartPopupProperties(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall SetButtonState(bool Hot);
	
__published:
	__property int ImageIndex = {read=FImageIndex, write=SetImageIndex, default=0};
	__property Vcl::Imglist::TCustomImageList* ImageList = {read=FImageList, write=SetImageList};
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=-16777211};
	__property System::Uitypes::TColor HoverColor = {read=FHoverColor, write=SetHoverColor, default=-16777192};
	__property System::Uitypes::TColor LineColor = {read=FLineColor, write=SetLineColor, default=-16777203};
	__property System::Uitypes::TColor HoverLineColor = {read=FHoverLineColor, write=SetHoverLineColor, default=-16777193};
	__property Vcl::Menus::TPopupMenu* Menu = {read=FMenu, write=FMenu};
	__property System::UnicodeString Hint = {read=FHint, write=SetHint, stored=StoreHint};
	__property System::Classes::TShortCut ShortCut = {read=FShortCut, write=FShortCut, default=24616};
	__property Rvscroll::TRVSmartPopupType ButtonType = {read=FButtonType, write=FButtonType, default=0};
	__property Rvscroll::TRVSmartPopupPosition Position = {read=FPosition, write=FPosition, default=2};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVSmartPopupProperties(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TRVSmartPopupButton : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
private:
	bool FHot;
	bool FAlwaysHot;
	TRVSmartPopupProperties* FSmartPopupProperties;
	void __fastcall SetSmartPopupProperties(TRVSmartPopupProperties* const Value);
	HIDESBASE MESSAGE void __fastcall CMMouseEnter(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall CMMouseLeave(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	MESSAGE void __fastcall WMSPRun(Winapi::Messages::TMessage &Msg);
	
protected:
	virtual void __fastcall Paint(void);
	
public:
	Rvitem::TCustomRVItemInfo* Item;
	Crvdata::TCustomRVData* RVData;
	int ItemNo;
	__fastcall virtual ~TRVSmartPopupButton(void);
	DYNAMIC void __fastcall Click(void);
	__property TRVSmartPopupProperties* SmartPopupProperties = {read=FSmartPopupProperties, write=SetSmartPopupProperties};
public:
	/* TCustomControl.Create */ inline __fastcall virtual TRVSmartPopupButton(System::Classes::TComponent* AOwner) : Vcl::Controls::TCustomControl(AOwner) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVSmartPopupButton(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word WM_SP_RUN = System::Word(0x464);
}	/* namespace Rvpopup */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVPOPUP)
using namespace Rvpopup;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvpopupHPP
