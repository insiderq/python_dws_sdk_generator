
{*******************************************************}
{                                                       }
{       RichView                                        }
{       Design-time support.                            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVSEdit;

interface
{$I RV_Defs.inc}
uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  {$IFDEF RICHVIEWDEF6}
  DesignIntf, DesignEditors, ColnEdit, VCLEditors,
  {$ELSE}
  DsgnIntf,
  {$ENDIF}
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  RVDocParams,
  {$ENDIF}
  TypInfo, ShellApi, Graphics, Dialogs,
  {$IFDEF RICHVIEWDEFXE2}
  UITypes,
  {$ENDIF}
  RVStyle, RichView, RVEdit, RVCodePages, RVReport, PtblRV;
{$IFDEF RICHVIEWCBDEF3}
type
{$IFDEF RICHVIEWDEF6}
  TRVComponentEditor = TDefaultEditor;
{$ELSE}
  TRVComponentEditor = TComponentEditor;
{$ENDIF}
  {----------------------------------------------------------}
  TRVSEditor = class(TRVComponentEditor)
  private
    {$IFNDEF RVDONOTUSEINI}
    procedure LoadStylesFromINIFile;
    procedure SaveStylesToINIFile;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    procedure LoadRVST;
    procedure SaveRVST;
    {$ENDIF}
    {$ENDIF}
  protected
    {$IFDEF RICHVIEWDEF6}
    VerbIndex: Integer;
    procedure EditProperty(const PropertyEditor: IProperty;
      var Continue: Boolean);override;
  private
    {$ELSE}
    FContinue : Boolean;
    procedure CheckEditF(PropertyEditor: TPropertyEditor);
    procedure CheckEditP(PropertyEditor: TPropertyEditor);
    procedure CheckEditL(PropertyEditor: TPropertyEditor);
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    procedure CheckEditS(PropertyEditor: TPropertyEditor);
    {$ENDIF}
    {$ENDIF}
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

  TRVEEditor = class(TRVComponentEditor)
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

  TRVCodePageProperty = class(TIntegerProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    function GetValue: string; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  {$IFDEF RICHVIEWDEF5}
  TRVGetUnitNameFunc = function (Obj: TPersistent; const PropName: String): String;

  TRVStyleLengthProperty = class(TIntegerProperty
    {$IFDEF RICHVIEWDEF6}, ICustomPropertyDrawing{$ENDIF}
    {$IFDEF RICHVIEWDEF2010}, ICustomPropertyDrawing80{$ENDIF}
    )
  protected
    function GetUnits: String;
    function GetUnitName(Obj: TPersistent; const PropName: String): String; dynamic;
  public
    {$IFDEF RICHVIEWDEF6}
    procedure PropDrawName(ACanvas: TCanvas; const ARect: TRect;
      ASelected: Boolean);
    {$ENDIF}
    {$IFDEF RICHVIEWDEF2010}
    function PropDrawNameRect(const ARect: TRect): TRect;
    function PropDrawValueRect(const ARect: TRect): TRect;
    {$ENDIF}
    procedure PropDrawValue(ACanvas: TCanvas; const ARect: TRect;
      ASelected: Boolean); {$IFNDEF RICHVIEWDEF6}override;{$ENDIF}
  end;

  TRVLineSpacingValueProperty = class (TRVStyleLengthProperty)
  protected
    function GetUnitName(Obj: TPersistent; const PropName: String): String; override;
  end;

  TRVLengthProperty = class(TFloatProperty
    {$IFDEF RICHVIEWDEF6}, ICustomPropertyDrawing{$ENDIF}
    {$IFDEF RICHVIEWDEF2010}, ICustomPropertyDrawing80{$ENDIF})
  protected
    function GetUnits: String;
    function GetUnitName(Obj: TPersistent; const PropName: String): String; dynamic;
  public
    {$IFDEF RICHVIEWDEF6}
    procedure PropDrawName(ACanvas: TCanvas; const ARect: TRect;
      ASelected: Boolean);
    {$ENDIF}
    {$IFDEF RICHVIEWDEF2010}
    function PropDrawNameRect(const ARect: TRect): TRect;
    function PropDrawValueRect(const ARect: TRect): TRect;
    {$ENDIF}
    procedure PropDrawValue(ACanvas: TCanvas; const ARect: TRect;
      ASelected: Boolean); {$IFNDEF RICHVIEWDEF6}override;{$ENDIF}
  end;
  {$ENDIF}

  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  TRVStyleTemplateIdProperty = class(TIntegerProperty)
  private
    function GetCollection: TRVStyleTemplateCollection;
  public
    function GetAttributes: TPropertyAttributes; override;
    function GetValue: string; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  TRVStyleTemplateNameProperty = class (TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  {$IFDEF RICHVIEWDEF6}
  TRVStyleTemplateCollectionProperty = class(TCollectionProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
  end;

  TRVLargeSetEditProperty = class (TPropertyEditor)
  public
    function GetAttributes: TPropertyAttributes; override;
  end;

  TRVValidParaPropertiesEditProperty = class (TRVLargeSetEditProperty)
  public
    procedure GetProperties(Proc: TGetPropProc); override;
    function GetValue: string; override;
  end;

  TRVValidTextPropertiesEditProperty = class (TRVLargeSetEditProperty)
  public
    procedure GetProperties(Proc: TGetPropProc); override;
    function GetValue: string; override;
  end;

  TRVLargeSetElementProperty = class (TNestedProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TRVValidParaPropertiesElementProperty = class (TRVLargeSetElementProperty)
  private
    FElement: TRVParaInfoProperty;
  protected
    constructor Create(Parent: TPropertyEditor; AElement: TRVParaInfoProperty); reintroduce;
    property Element: TRVParaInfoProperty read FElement;
    {$IFDEF RICHVIEWDEF7}
    function GetIsDefault: Boolean; override;
    {$ENDIF}
  public
    function AllEqual: Boolean; override;
    function GetName: string; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    function GetRealValueAt(Index: Integer): TRVParaInfoProperties;
    procedure SetRealValue(Value: TRVParaInfoProperties);
   end;

  TRVValidTextPropertiesElementProperty = class (TRVLargeSetElementProperty)
  private
    FElement: TRVFontInfoProperty;
  protected
    constructor Create(Parent: TPropertyEditor; AElement: TRVFontInfoProperty); reintroduce;
    property Element: TRVFontInfoProperty read FElement;
    {$IFDEF RICHVIEWDEF7}
    function GetIsDefault: Boolean; override;
    {$ENDIF}
  public
    function AllEqual: Boolean; override;
    function GetName: string; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    function GetRealValueAt(Index: Integer): TRVFontInfoProperties;
    procedure SetRealValue(Value: TRVFontInfoProperties);
   end;
  {$ENDIF}
  {$ENDIF}

  procedure Register;

  {$IFDEF RICHVIEWDEF5}
  var
  RVGetUnitNameFunc: TRVGetUnitNameFunc = nil;
  RVGetRVUnitNameFunc: TRVGetUnitNameFunc = nil;
  function GetRVUnitsName(Units: TRVUnits): String;
  {$ENDIF}

{$ENDIF}
implementation
uses RVDsgn;
{$IFDEF RICHVIEWCBDEF3}
const
  CMD_TEXTSTYLES = 0;
  CMD_PARASTYLES = 1;
  CMD_LISTSTYLES = 2;
  CMD_SEPAFTERSTYLES = 3;
  CMD_CONVERT = 4;
  CMD_SEPAFTERCONVERT = 5;
  {$IFNDEF RVDONOTUSEINI}
   CMD_SAVEINI = 6;
   CMD_LOADINI = 7;
   CMD_SEPAFTERINI = 8;
   {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    CMD_STYLETEMPLATES = 9;
    CMD_STYLETEMPLATESSAVE = 10;
    CMD_STYLETEMPLATESLOAD = 11;
    CMD_SEPAFTERTEMPL  = 12;
    CMD_WEB = 13;
   {$ELSE}
    CMD_WEB = 9;
   {$ENDIF}
  {$ELSE}
   {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    CMD_STYLETEMPLATES = 6;
    CMD_SEPAFTERTEMPL  = 7;
    CMD_WEB = 8;
   {$ELSE}
   CMD_WEB = 6;
   {$ENDIF}
  {$ENDIF}
{-----------------------------------------------------------------------}
function TRVSEditor.GetVerbCount: Integer;
begin
  Result := CMD_WEB+1;
end;
{-----------------------------------------------------------------------}
function TRVSEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    CMD_TEXTSTYLES:
      Result := 'Edit Text Styles';
    CMD_PARASTYLES:
      Result := 'Edit Paragraph Styles';
    CMD_LISTSTYLES:
      Result := 'Edit List Styles';
    CMD_SEPAFTERSTYLES, CMD_SEPAFTERCONVERT:
      Result := '-';
    CMD_CONVERT:
      if TRVStyle(Component).Units=rvstuPixels then
        Result := 'Convert Lengths to Twips'
      else
        Result := 'Convert Lengths to Pixels';
    {$IFNDEF RVDONOTUSEINI}
    CMD_SAVEINI:
      Result := 'Save...';
    CMD_LOADINI:
      Result := 'Load...';
    CMD_SEPAFTERINI:
      Result := '-';
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    CMD_STYLETEMPLATESSAVE:
      Result := 'Save StyleTemplates...';
    CMD_STYLETEMPLATESLOAD :
      Result := 'Load StyleTemplates...';
    {$ENDIF}
    {$ENDIF}
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    CMD_STYLETEMPLATES:
      Result := 'Edit Style Templates';
    CMD_SEPAFTERTEMPL:
      Result := '-';
    {$ENDIF}
    CMD_WEB:
      {$IFDEF RVDEBUG}
      Result := 'Register TRichView Online';
      {$ELSE}
      Result := 'TRichView Homepage';
      {$ENDIF}
    else
      Result := '';
  end;
end;
{-----------------------------------------------------------------------}
procedure TRVSEditor.ExecuteVerb(Index: Integer);
{$IFNDEF RICHVIEWDEF6}
var
{$IFDEF RICHVIEWDEF5}
  Components: TDesignerSelectionList;
{$ELSE}
  Components: TComponentList;
{$ENDIF}
{$ENDIF}
begin
  if Index>CMD_WEB then
    exit;
  case Index of
    {$IFNDEF RVDONOTUSEINI}
    CMD_SAVEINI:
      begin
        SaveStylesToINIFile;
        exit;
      end;
    CMD_LOADINI:
      begin
        LoadStylesFromINIFile;
        exit;
      end;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    CMD_STYLETEMPLATESSAVE:
      begin
        SaveRVST;
        exit;
      end;
    CMD_STYLETEMPLATESLOAD:
      begin
        LoadRVST;
        exit;
      end;
    {$ENDIF}
    {$ENDIF}
    CMD_CONVERT:
      begin
        if TRVStyle(Component).Units=rvstuPixels then
          TRVStyle(Component).ConvertToTwips
        else
          TRVStyle(Component).ConvertToPixels;
        Designer.Modified;
      end;
    CMD_WEB:
      begin
        {$IFDEF RVDEBUG}
        ShellExecute(0, 'open', 'http://www.trichview.com/order/', nil, nil, SW_NORMAL);
        {$ELSE}
        ShellExecute(0, 'open', 'http://www.trichview.com', nil, nil, SW_NORMAL);
        {$ENDIF}
        exit;
      end;
  end;
  {$IFDEF RICHVIEWDEF6}
  VerbIndex := Index;
  Edit;
  {$ELSE}
  {$IFDEF RICHVIEWDEF5}
  Components := TDesignerSelectionList.Create;
  {$ELSE}
  Components := TComponentList.Create;
  {$ENDIF}
  try
    FContinue := True;
    Components.Add(Component);
    case Index of
     CMD_TEXTSTYLES:
      GetComponentProperties(Components, tkAny, Designer, CheckEditF);
     CMD_PARASTYLES:
      GetComponentProperties(Components, tkAny, Designer, CheckEditP);
     CMD_LISTSTYLES:
      GetComponentProperties(Components, tkAny, Designer, CheckEditL);
     {$IFNDEF RVDONOTUSESTYLETEMPLATES}
     CMD_STYLETEMPLATES:
      GetComponentProperties(Components, tkAny, Designer, CheckEditS);
     {$ENDIF}
    end;
  finally
    Components.Free;
  end;
  {$ENDIF}
end;
{-----------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEINI}
procedure TRVSEditor.LoadStylesFromINIFile;
var
  odStylesINI: TOpenDialog;
begin
  odStylesINI := TOpenDialog.Create(TRVStyle(Component));
  try
    odStylesINI.DefaultExt := '.ini';
    odStylesINI.Options := odStylesINI.Options+[ofFileMustExist];
    odStylesINI.Filter := 'INI Files (*.ini)|*.ini|All Files (*.*)|*.*';
    if odStylesINI.Execute then
      TRVStyle(Component).LoadINI(odStylesINI.FileName,'Style');
  finally
    OdStylesINI.Free;
  end;
end;
{-----------------------------------------------------------------------}
procedure TRVSEditor.SaveStylesToINIFile;
var
  sdStylesINI: TSaveDialog;
begin
  sdStylesINI := TSaveDialog.Create(TRVStyle(Component));
  try
    sdStylesINI.DefaultExt := '.ini';
    sdStylesINI.Options := sdStylesINI.Options+[ofOverwritePrompt, ofPathMustExist];
    sdStylesINI.Filter := 'INI files (*.ini)|*.ini|All files (*.*)|*.*';
    if sdStylesINI.Execute then
      TRVStyle(Component).SaveINI(sdStylesINI.FileName,'Style');
  finally
    sdStylesINI.Free;
  end;
end;
{-----------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure TRVSEditor.LoadRVST;
var
  odStylesINI: TOpenDialog;
begin
  odStylesINI := TOpenDialog.Create(TRVStyle(Component));
  try
    odStylesINI.DefaultExt := '.rvst';
    odStylesINI.Options := odStylesINI.Options+[ofFileMustExist];
    odStylesINI.Filter := 'RichView Styles (*.rvst)|*.rvst|All Files (*.*)|*.*';
    if odStylesINI.Execute then
      TRVStyle(Component).StyleTemplates.LoadFromRVST(odStylesINI.FileName,
        TRVStyle(Component).Units);
  finally
    OdStylesINI.Free;
  end;
end;
{-----------------------------------------------------------------------}
procedure TRVSEditor.SaveRVST;
var
  sdStylesINI: TSaveDialog;
begin
  sdStylesINI := TSaveDialog.Create(TRVStyle(Component));
  try
    sdStylesINI.DefaultExt := '.rvst';
    sdStylesINI.Options := sdStylesINI.Options+[ofOverwritePrompt, ofPathMustExist];
    sdStylesINI.Filter := 'RichView Styles (*.rvst)|*.rvst|All Files (*.*)|*.*';
    if sdStylesINI.Execute then
      TRVStyle(Component).StyleTemplates.SaveToRVST(sdStylesINI.FileName,
        TRVStyle(Component).Units);
  finally
    sdStylesINI.Free;
  end;
end;
{$ENDIF}
{$ENDIF}
{-----------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF6}
procedure TRVSEditor.EditProperty(const PropertyEditor: IProperty;
                      var Continue: Boolean);
var
  PropName: string;
begin
  PropName := PropertyEditor.GetName;
  if ((VerbIndex=CMD_TEXTSTYLES) and (CompareText(PropertyEditor.GetName, 'TextStyles')=0)) or
     ((VerbIndex=CMD_PARASTYLES) and (CompareText(PropertyEditor.GetName, 'ParaStyles')=0)) or
     ((VerbIndex=CMD_LISTSTYLES) and (CompareText(PropertyEditor.GetName, 'ListStyles')=0))
     {$IFNDEF RVDONOTUSESTYLETEMPLATES}
     or
     ((VerbIndex=CMD_STYLETEMPLATES) and (CompareText(PropertyEditor.GetName, 'StyleTemplates')=0))
     {$ENDIF}
     then
  begin
    PropertyEditor.Edit;
    Continue := False;
    VerbIndex := 0;
  end;
end;
{-----------------------------------------------------------------------}
{$ELSE}
procedure TRVSEditor.CheckEditF(PropertyEditor: TPropertyEditor);
begin
  try
    if FContinue and (CompareText(PropertyEditor.GetName, 'TextStyles') = 0) then
    begin
      PropertyEditor.Edit;
      FContinue := False;
    end;
  finally
    PropertyEditor.Free;
  end;
end;
{-----------------------------------------------------------------------}
procedure TRVSEditor.CheckEditP(PropertyEditor: TPropertyEditor);
begin
  try
    if FContinue and (CompareText(PropertyEditor.GetName, 'ParaStyles') = 0) then
    begin
      PropertyEditor.Edit;
      FContinue := False;
    end;
  finally
    PropertyEditor.Free;
  end;
end;
{-----------------------------------------------------------------------}
procedure TRVSEditor.CheckEditL(PropertyEditor: TPropertyEditor);
begin
  try
    if FContinue and (CompareText(PropertyEditor.GetName, 'ListStyles') = 0) then
    begin
      PropertyEditor.Edit;
      FContinue := False;
    end;
  finally
    PropertyEditor.Free;
  end;
end;
{-----------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure TRVSEditor.CheckEditS(PropertyEditor: TPropertyEditor);
begin
  try
    if FContinue and (CompareText(PropertyEditor.GetName, 'StyleTemplates') = 0) then
    begin
      PropertyEditor.Edit;
      FContinue := False;
    end;
  finally
    PropertyEditor.Free;
  end;
end;
{$ENDIF}
{$ENDIF}
{============================== TRVEEditor ====================================}
procedure TRVEEditor.ExecuteVerb(Index: Integer);
var frm:TfrmRVDesign;
begin
  case Index of
   0:
     begin
       frm := TfrmRVDesign.Create(Application);
       try
         frm.SetRichView(Component as TCustomRichView);
         if frm.ShowModal=mrOk then begin
           Designer.Modified;
         end;
       finally
         frm.Free;
       end;
     end;
   1:
     begin
       {$IFDEF RVDEBUG}
       ShellExecute(0, 'open', 'http://www.trichview.com/order/', nil, nil, SW_NORMAL);
       {$ELSE}
       ShellExecute(0, 'open', 'http://www.trichview.com', nil, nil, SW_NORMAL);
       {$ENDIF}
     end;
  end;
end;
{------------------------------------------------------------------------------}
function TRVEEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0:
      Result := 'Settings...';
    1:
      {$IFDEF RVDEBUG}
      Result := 'Register TRichView Online';
      {$ELSE}
      Result := 'TRichView Homepage';
      {$ENDIF}
    else
      Result := '';
  end;
end;
{------------------------------------------------------------------------------}
function TRVEEditor.GetVerbCount: Integer;
begin
  Result := 2;
end;
{=============================== TRVCodePageProperty ==========================}
function TRVCodePageProperty.GetAttributes: TPropertyAttributes;
begin
 Result := [paMultiSelect, paValueList, paRevertable];
end;
{------------------------------------------------------------------------------}
function TRVCodePageProperty.GetValue: string;
begin
  Result := CodePageToIdent(GetOrdValue);
end;
{------------------------------------------------------------------------------}
procedure TRVCodePageProperty.GetValues(Proc: TGetStrProc);
begin
  GetCodePageValues(Proc);
end;
{------------------------------------------------------------------------------}
procedure TRVCodePageProperty.SetValue(const Value: string);
var NewValue: TRVCodePage;
begin
  if IdentToCodePage(Value,NewValue) then
    SetOrdValue(NewValue)
  else
    inherited SetValue(Value);
end;
{========================== TRVStyleLengthProperty ============================}
{$IFDEF RICHVIEWDEF5}
function TRVStyleLengthProperty.GetUnitName(Obj: TPersistent; const PropName: String): String;
var Collection: TCollection;
    RVStyle: TPersistent;
begin
  Result := '';
  if Obj=nil then
    exit;
  if Assigned(RVGetUnitNameFunc) then begin
    Result := RVGetUnitNameFunc(Obj, PropName);
    if Result<>'' then
      exit;
  end;
  if Obj is TRVStyle then begin
    if TRVStyle(Obj).Units=rvstuPixels then
      Result := 'px'
    else
      Result := 'tw';
    end
  else if Obj is TCustomRichView then
    Result := GetUnitName(TCustomRichView(Obj).Style, PropName)
  else if Obj is TCustomRVInfo then begin
    RVStyle := TCustomRVInfo(Obj).GetRVStyle;
    if RVStyle<>nil then
      Result := GetUnitName(RVStyle, PropName)
    else begin
      {$IFNDEF RVDONOTUSESTYLETEMPLATES}
      if (TCustomRVInfo(Obj).GetOwner is TRVStyleTemplate) and
        (TRVStyleTemplate(TCustomRVInfo(Obj).GetOwner).Collection<>nil) then
      Result := GetUnitName(TRVStyleTemplateCollection(TRVStyleTemplate(TCustomRVInfo(Obj).GetOwner).Collection).GetOwner, PropName);
      {$ENDIF}
    end
    end
  {$IFNDEF RVDONOTUSELISTS}
  else if Obj is TRVListLevel then begin
    Collection := TRVListLevel(Obj).Collection;
    if (Collection=nil) or not (Collection is TRVListLevelCollection) then
      exit;
    Result := GetUnitName(TRVListLevelCollection(Collection).GetOwner, PropName);
    end
  {$ENDIF}
  {$IFNDEF RVDONOTUSETABS}
  else if Obj is TRVTabInfo then begin
    Collection := TRVTabInfo(Obj).Collection;
    if (Collection=nil) or not (Collection is TRVTabInfos) then
      exit;
    Result := GetUnitName(TRVTabInfos(Collection).GetOwner, PropName);
    end
  {$ENDIF}
  else if Obj is TRVBorder then
    Result := GetUnitName(TRVBorder(Obj).Owner, PropName)
  else if Obj is TRVBackgroundRect then
    Result := GetUnitName(TRVBackgroundRect(Obj).Owner, PropName)
  else if Obj is TRVRect then
    Result := GetUnitName(TRVRect(Obj).Owner, PropName)
end;
{------------------------------------------------------------------------------}
function TRVStyleLengthProperty.GetUnits: String;
var i: Integer;
    s: String;
begin
  Result := '';
  for i := 0 to PropCount-1 do begin
    s := GetUnitName(GetComponent(i), GetName);
    if (s='') or ((Result<>'') and (Result<>s)) then begin
      Result := '';
      exit;
    end;
    if Result='' then
      Result := s;
  end;
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF6}
procedure TRVStyleLengthProperty.PropDrawName(ACanvas: TCanvas;
  const ARect: TRect; ASelected: Boolean);
begin
  DefaultPropertyDrawName(Self, ACanvas, ARect);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF2010}
function TRVStyleLengthProperty.PropDrawNameRect(const ARect: TRect): TRect;
begin
  Result := ARect;
end;
{------------------------------------------------------------------------------}
function TRVStyleLengthProperty.PropDrawValueRect(const ARect: TRect): TRect;
begin
  Result := ARect;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVStyleLengthProperty.PropDrawValue(ACanvas: TCanvas;
  const ARect: TRect; ASelected: Boolean);
var Value, Units: String;
begin
{
  ACanvas.Brush.Color := clRed;
  ACanvas.Brush.Style := bsSolid;
  ACanvas.FillRect(ARect);
  ACanvas.Brush.Color := clYellow;
  ACanvas.TextRect(ARect, ARect.Left + 1, ARect.Top + 1, 'test');
  exit;
}
  Value := GetVisualValue;
  Units := GetUnits;
  if (Value<>'') and (Units<>'') then
    ACanvas.TextRect(ARect, ARect.Left + 1, ARect.Top + 1, Value+' '+Units)
  else
    {$IFDEF RICHVIEWDEF6}
    DefaultPropertyDrawValue(Self, ACanvas, ARect);
    {$ELSE}
    inherited PropDrawValue(ACanvas, ARect, ASelected);
    {$ENDIF}
end;
{========================== TRVLineSpacingValueProperty =======================}
function TRVLineSpacingValueProperty.GetUnitName(Obj: TPersistent;
   const PropName: String): String;
begin
  if (Obj is TCustomRVParaInfo) and (TCustomRVParaInfo(Obj).LineSpacingType=rvlsPercent) then
    Result := '%'
  else
    Result := inherited GetUnitName(Obj, PropName);
end;
{============================= TRVLengthProperty ==============================}
function TRVLengthProperty.GetUnitName(Obj: TPersistent; const PropName: String): String;
begin
  Result := '';
  if Assigned(RVGetRVUnitNameFunc) then begin
    Result := RVGetRVUnitNameFunc(Obj, PropName);
    if Result<>'' then
      exit;
  end;
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  if Obj is TRVDocParameters then
    Result := GetRVUnitsName(TRVDocParameters(Obj).Units)
  else
  {$ENDIF}
  if Obj is TRVUnitsRect then
    Result := GetUnitName(TRVUnitsRect(Obj).Owner, PropName)
  else if Obj is TPrintableRV then
    Result := GetRVUnitsName(TPrintableRV(Obj).FUnits)
  else if Obj is TRVPrint then
    Result := GetRVUnitsName(TRVPrint(Obj).Units);
end;
{------------------------------------------------------------------------------}
function TRVLengthProperty.GetUnits: String;
var i: Integer;
begin
  Result := '';
  for i := 0 to PropCount-1 do begin
    if (Result<>'') and (Result<>GetUnitName(GetComponent(i), GetName)) then begin
      Result := '';
      exit;
    end;
    if Result='' then
      Result := GetUnitName(GetComponent(i), GetName)
  end;
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF2010}
function TRVLengthProperty.PropDrawNameRect(const ARect: TRect): TRect;
begin
  Result := ARect;
end;
{------------------------------------------------------------------------------}
function TRVLengthProperty.PropDrawValueRect(const ARect: TRect): TRect;
begin
  Result := ARect;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF6}
procedure TRVLengthProperty.PropDrawName(ACanvas: TCanvas;
  const ARect: TRect; ASelected: Boolean);
begin
  DefaultPropertyDrawName(Self, ACanvas, ARect);
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRVLengthProperty.PropDrawValue(ACanvas: TCanvas;
  const ARect: TRect; ASelected: Boolean);
var Value, Units: String;
begin
  Value := GetVisualValue;
  Units := GetUnits;
  if (Value<>'') and (Units<>'') then
    ACanvas.TextRect(ARect, ARect.Left + 1, ARect.Top + 1, Value+Units)
  else
    {$IFDEF RICHVIEWDEF6}
    DefaultPropertyDrawValue(Self, ACanvas, ARect);
    {$ELSE}
    inherited PropDrawValue(ACanvas, ARect, ASelected);
    {$ENDIF}
end;

function GetRVUnitsName(Units: TRVUnits): String;
begin
  case Units of
    rvuInches: Result := '''';
    rvuCentimeters: Result := ' cm';
    rvuMillimeters: Result := ' mm';
    rvuPicas: Result := ' pc';
    rvuPixels: Result := ' px';
    rvuPoints: Result := ' pt';
    else Result := '';
  end;
end;
{$ENDIF}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
{=========================== TRVStyleTemplateNameProperty =====================}
function TRVStyleTemplateNameProperty.GetAttributes: TPropertyAttributes;
begin
  Result := (inherited GetAttributes)+[paValueList]-[paMultiSelect, paRevertable];
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateNameProperty.GetValues(Proc: TGetStrProc);
var i: Integer;
begin
  Proc('Normal');
  Proc('Normal Indent');
  for i := 1 to 9 do
    Proc(Format('heading %d', [i]));
  Proc('List Bullet');
  for i := 2 to 5 do
    Proc(Format('List Bullet %d', [i]));
  Proc('List Number');
  for i := 2 to 5 do
    Proc(Format('List Number %d', [i]));
  Proc('List');
  for i := 2 to 5 do
    Proc(Format('List %d', [i]));
  Proc('List Paragraph');
  Proc('Block Text');
  Proc('Body Text');
  for i := 2 to 5 do
    Proc(Format('Body Text %d', [i]));
  Proc('Body Text Indent');
  for i := 2 to 5 do
    Proc(Format('Body Text Indent %d', [i]));
  Proc('Body Text First Indent');
  for i := 2 to 5 do
    Proc(Format('Body Text First Indent %d', [i]));
  Proc('Hyperlink');
  Proc('FollowedHyperlink');
  Proc('Plain Text');
  Proc('Title');
  Proc('Normal (Web)');
  Proc('HTML Code');
  Proc('HTML Acronym');
  Proc('HTML Definition');
  Proc('HTML Keyboard');
  Proc('HTML Sample');
  Proc('HTML Variable');
  Proc('HTML Typewriter');
  Proc('HTML Preformatted');
  Proc('HTML Cite');
  Proc('Subtitle');
  Proc('Emphasis');
  Proc('Subtle Emphasis');
  Proc('Intense Emphasis');
  Proc('Strong');
  Proc('Quote');
  Proc('Intense Quote');
  Proc('Subtle Reference');
  Proc('Intense Reference');
  Proc('Book Title');
  Proc('caption');
  Proc('Bibliography');
  Proc('header');
  Proc('footer');
  Proc('endnote reference');
  Proc('footnote reference');
  Proc('Sidenote Reference');
  Proc('annotation reference');
  Proc('endnote text');
  Proc('footnote text');
  Proc('Sidenote Text');  
  Proc('page number');
  Proc('line number');
end;
{============================= TRVStyleTemplateIdProperty =====================}
function TRVStyleTemplateIdProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList];
end;
{------------------------------------------------------------------------------}
function TRVStyleTemplateIdProperty.GetCollection: TRVStyleTemplateCollection;
var Cmp: TPersistent;
begin
  Cmp := GetComponent(0);
  if Cmp is TRVStyleTemplate then
    Result := (Cmp as TRVStyleTemplate).Collection as TRVStyleTemplateCollection
  else if Cmp is TCustomRVInfo then
    Result := (((Cmp as TCustomRVInfo).Collection as TCustomRVInfos).GetOwner as TRVStyle).StyleTemplates
  else
    Result := nil;
end;
{------------------------------------------------------------------------------}
function TRVStyleTemplateIdProperty.GetValue: string;
var Coll: TRVStyleTemplateCollection;
    Val, i: Integer;
begin
  Val := GetOrdValue;
  if Val<=0 then begin
    Result := '(none)';
    exit;
  end;
  if PropCount=0 then begin
    Result := '(error)';
    exit;
  end;
  Coll := GetCollection;
  for i := 0 to Coll.Count-1 do
    if Coll[i].Id=Val then begin
      Result := Format('%s (id=%d)', [Coll[i].Name, Coll[i].Id]);
      exit;
    end;
  Result := '(unknown)';
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateIdProperty.GetValues(Proc: TGetStrProc);
var Coll: TRVStyleTemplateCollection;
    i: Integer;
    StyleTemplate: TRVStyleTemplate;
begin
  if PropCount=0 then
    exit;
  Proc('(none)');
  Coll := GetCollection;
  if GetComponent(0) is TRVStyleTemplate then begin
    StyleTemplate := TRVStyleTemplate(GetComponent(0));
    for i := 0 to Coll.Count-1 do
      if not StyleTemplate.IsAncestorFor(Coll[i]) then
        Proc(Format('%s (id=%d)', [Coll[i].Name, Coll[i].Id]));
    end
  else
    for i := 0 to Coll.Count-1 do
      Proc(Format('%s (id=%d)', [Coll[i].Name, Coll[i].Id]));
end;
{------------------------------------------------------------------------------}
procedure TRVStyleTemplateIdProperty.SetValue(const Value: string);
var s: String;
    i : Integer;
    found: Boolean;
begin
  if (Value='(none)') or (Value='(error)') or (Value='(unknown)') or
     (Value='') then begin
    SetOrdValue(-1);
    exit;
  end;
  s := '';
  if Value[Length(Value)]=')' then begin
    found := False;
    for i := Length(Value)-1 downto 1 do
      if (Value[i]>='0') and (Value[i]<='9') then
        s := Value[i]+s
      else if Value[i]='=' then begin
        found := True;
        break;
        end
      else
        break;
    if not found then begin
      inherited SetValue(Value);
      exit;
    end;
    inherited SetValue(s);
    exit;
  end;
  inherited SetValue(Value);
end;
{======================= TRVStyleTemplateCollectionProperty=================== }
{$IFDEF RICHVIEWDEF6}
function TRVStyleTemplateCollectionProperty.GetAttributes: TPropertyAttributes;
begin
  Result := (inherited GetAttributes)+[paSubProperties];
end;
{============================== TRVLargeSetEditProperty =======================}
function TRVLargeSetEditProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paSubProperties, paReadOnly, paRevertable];
end;
{======================== TRVValidParaPropertiesEditProperty ==================}
procedure TRVValidParaPropertiesEditProperty.GetProperties(
  Proc: TGetPropProc);
var
  I: TRVParaInfoProperty;
begin
  for I := Low(TRVParaInfoProperty) to High(TRVParaInfoProperty) do
      Proc(TRVValidParaPropertiesElementProperty.Create(Self, I));
end;
{------------------------------------------------------------------------------}
function TRVValidParaPropertiesEditProperty.GetValue: string;
var
  S: TRVParaInfoProperties;
  I: TRVParaInfoProperty;
begin
  S := (GetComponent(0) as TRVStyleTemplate).ValidParaProperties;
  Result := '[';
  for I := Low(TRVParaInfoProperty) to High(TRVParaInfoProperty) do
    if I in S then
    begin
      if Length(Result) <> 1 then Result := Result + ',';
      Result := Result + GetEnumName(TypeInfo(TRVParaInfoProperty), ord(I));
    end;
  Result := Result + ']';
end;
{===================== TRVValidTextPropertiesEditProperty =====================}
procedure TRVValidTextPropertiesEditProperty.GetProperties(
  Proc: TGetPropProc);
var
  I: TRVFontInfoProperty;
begin
  for I := Low(TRVFontInfoProperty) to High(TRVFontInfoProperty) do
      Proc(TRVValidTextPropertiesElementProperty.Create(Self, I));
end;
{------------------------------------------------------------------------------}
function TRVValidTextPropertiesEditProperty.GetValue: string;
var
  S: TRVFontInfoProperties;
  I: TRVFontInfoProperty;
begin
  S := (GetComponent(0) as TRVStyleTemplate).ValidTextProperties;
  Result := '[';
  for I := Low(TRVFontInfoProperty) to High(TRVFontInfoProperty) do
    if I in S then
    begin
      if Length(Result) <> 1 then Result := Result + ',';
      Result := Result + GetEnumName(TypeInfo(TRVFontInfoProperty), ord(I));
    end;
  Result := Result + ']';
end;
{========================== TRVLargeSetElementProperty ========================}
function TRVLargeSetElementProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList];
end;
{------------------------------------------------------------------------------}
procedure TRVLargeSetElementProperty.GetValues(Proc: TGetStrProc);
begin
  Proc(BooleanIdents[False]);
  Proc(BooleanIdents[True]);
end;
{====================== TRVValidParaPropertiesElementProperty =================}
constructor TRVValidParaPropertiesElementProperty.Create(
  Parent: TPropertyEditor; AElement: TRVParaInfoProperty);
begin
  inherited Create(Parent);
  FElement := AElement;
end;
{------------------------------------------------------------------------------}
function TRVValidParaPropertiesElementProperty.AllEqual: Boolean;
var
  I: Integer;
  S: TRVParaInfoProperties;
  V: Boolean;
begin
  Result := False;
  if PropCount > 1 then
  begin
    S := GetRealValueAt(0);
    V := FElement in S;
    for I := 1 to PropCount - 1 do
    begin
      S := GetRealValueAt(I);
      if (FElement in S) <> V then Exit;
    end;
  end;
  Result := True;
end;
{------------------------------------------------------------------------------}
function TRVValidParaPropertiesElementProperty.GetName: string;
begin
  Result := GetEnumName(TypeInfo(TRVParaInfoProperty), ord(FElement));
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF7}
function TRVValidParaPropertiesElementProperty.GetIsDefault: Boolean;
begin
    Result := not (FElement in GetRealValueAt(0));
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVValidParaPropertiesElementProperty.GetRealValueAt(
  Index: Integer): TRVParaInfoProperties;
var Obj: TRVStyleTemplate;
begin
  Obj := GetComponent(Index) as TRVStyleTemplate;
  Result := Obj.ValidParaProperties;
end;
{------------------------------------------------------------------------------}
function TRVValidParaPropertiesElementProperty.GetValue: string;
var
  S: TRVParaInfoProperties;
begin
  S := GetRealValueAt(0);
  Result := BooleanIdents[FElement in S];
end;
{------------------------------------------------------------------------------}
procedure TRVValidParaPropertiesElementProperty.SetValue(
  const Value: string);
var
  S: TRVParaInfoProperties;
begin
  S := GetRealValueAt(0);
  if CompareText(Value, BooleanIdents[True]) = 0 then
    Include(S, FElement)
  else
    Exclude(S, FElement);
  SetRealValue(S);
end;
{------------------------------------------------------------------------------}
procedure TRVValidParaPropertiesElementProperty.SetRealValue(
  Value: TRVParaInfoProperties);
var
  I: Integer;
  Obj: TRVStyleTemplate;
begin
  for I := 0 to PropCount - 1 do begin
    Obj := GetComponent(I) as TRVStyleTemplate;
    Obj.ValidParaProperties := Value;
  end;
  Modified;
end;
{======================== TRVValidTextPropertiesElementProperty ===============}
constructor TRVValidTextPropertiesElementProperty.Create(
  Parent: TPropertyEditor; AElement: TRVFontInfoProperty);
begin
  inherited Create(Parent);
  FElement := AElement;
end;
{------------------------------------------------------------------------------}
function TRVValidTextPropertiesElementProperty.AllEqual: Boolean;
var
  I: Integer;
  S: TRVFontInfoProperties;
  V: Boolean;
begin
  Result := False;
  if PropCount > 1 then
  begin
    S := GetRealValueAt(0);
    V := FElement in S;
    for I := 1 to PropCount - 1 do
    begin
      S := GetRealValueAt(I);
      if (FElement in S) <> V then Exit;
    end;
  end;
  Result := True;
end;
{------------------------------------------------------------------------------}
function TRVValidTextPropertiesElementProperty.GetName: string;
begin
  Result := GetEnumName(TypeInfo(TRVFontInfoProperty), ord(FElement));
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF7}
function TRVValidTextPropertiesElementProperty.GetIsDefault: Boolean;
begin
    Result := not (FElement in GetRealValueAt(0));
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVValidTextPropertiesElementProperty.GetRealValueAt(
  Index: Integer): TRVFontInfoProperties;
var Obj: TRVStyleTemplate;
begin
  Obj := GetComponent(Index) as TRVStyleTemplate;
  Result := Obj.ValidTextProperties;
end;
{------------------------------------------------------------------------------}
function TRVValidTextPropertiesElementProperty.GetValue: string;
var
  S: TRVFontInfoProperties;
begin
  S := GetRealValueAt(0);
  Result := BooleanIdents[FElement in S];
end;
{------------------------------------------------------------------------------}
procedure TRVValidTextPropertiesElementProperty.SetValue(
  const Value: string);
var
  S: TRVFontInfoProperties;
begin
  S := GetRealValueAt(0);
  if CompareText(Value, BooleanIdents[True]) = 0 then
    Include(S, FElement)
  else
    Exclude(S, FElement);
  SetRealValue(S);
end;
{------------------------------------------------------------------------------}
procedure TRVValidTextPropertiesElementProperty.SetRealValue(
  Value: TRVFontInfoProperties);
var
  I: Integer;
  Obj: TRVStyleTemplate;
begin
  for I := 0 to PropCount - 1 do begin
    Obj := GetComponent(I) as TRVStyleTemplate;
    Obj.ValidTextProperties := Value;
  end;
  Modified;
end;
{$ENDIF}
{$ENDIF}
{==============================================================================}
{$IFDEF RICHVIEWDEF6}
const TRVIOCategory = 'Import/Export';
      TRVRVFCategory = 'RVF';
      TRVCPCategory ='Checkpoints';
      TRVHypertextCategory = 'Hypertext';
      TRVStyleNameCategory = 'Style Name';
{$ELSE}
{$IFDEF RICHVIEWDEF5}
type
 TRVStyleNameCategory = class(TPropertyCategory)
  public
    class function Name: string; override;
    class function Description: string; override;
  end;
 TRVHypertextCategory = class(TPropertyCategory)
  public
    class function Name: string; override;
    class function Description: string; override;
  end;
 TRVCPCategory = class(TPropertyCategory)
  public
    class function Name: string; override;
    class function Description: string; override;
  end;
 TRVRVFCategory = class(TPropertyCategory)
  public
    class function Name: string; override;
    class function Description: string; override;
  end;
 TRVIOCategory = class(TPropertyCategory)
  public
    class function Name: string; override;
    class function Description: string; override;
  end;

class function TRVStyleNameCategory.Description: string;
begin
  Result := 'Style Name';
end;
class function TRVStyleNameCategory.Name: string;
begin
  Result := 'Style Name';
end;

class function TRVHypertextCategory.Description: string;
begin
  Result := 'Hypertext related properties';
end;
class function TRVHypertextCategory.Name: string;
begin
  Result := 'Hypertext';
end;

class function TRVCPCategory.Description: string;
begin
  Result := 'Checkpoint related properties';
end;
class function TRVCPCategory.Name: string;
begin
  Result := 'Checkpoints';
end;

class function TRVRVFCategory.Description: string;
begin
  Result := 'RichView Format related properties'
end;
class function TRVRVFCategory.Name: string;
begin
  Result := 'RVF'
end;

class function TRVIOCategory.Description: string;
begin
  Result := 'Import/Export';
end;
class function TRVIOCategory.Name: string;
begin
  Result := 'Import/Export';
end;

{$ENDIF}
{$ENDIF}
{-----------------------------------------------------------------------}
procedure Register;
{$IFDEF RICHVIEWDEF6}
const
  TLocalizableCategory: String = sLocalizableCategoryName;
  TInputCategory:       String = sInputCategoryName;
  TVisualCategory:      String = sVisualCategoryName;
  TLegacyCategory:      String = sLegacyCategoryName;
  THelpCategory:        String = sHelpCategoryName;
  TDragDropCategory:    String = sDragNDropCategoryName;
{$ENDIF}
begin
  RegisterComponentEditor(TRVStyle, TRVSEditor);
  RegisterComponentEditor(TCustomRichView, TRVEEditor);

  {$IFDEF RICHVIEWDEFXE}
  RegisterPropertyEditor(TypeInfo(TFontName), TCustomRVFontInfo,'',  TFontNameProperty);
  {$IFNDEF RVDONOTUSELISTS}
  RegisterPropertyEditor(TypeInfo(TFontName), TRVMarkerFont,'',  TFontNameProperty);
  {$ENDIF}
  {$ENDIF}

  RegisterPropertyEditor(TypeInfo(TRVCodePage), nil,'',  TRVCodePageProperty);
  {$IFDEF RICHVIEWDEF5}
  RegisterPropertyEditor(TypeInfo(TRVStyleLength), nil,'',  TRVStyleLengthProperty);
  RegisterPropertyEditor(TypeInfo(TRVLineSpacingValue), TCustomRVParaInfo, '',  TRVLineSpacingValueProperty);
  {$IFNDEF RVDONOTUSEDOCPARAMS}
  RegisterPropertyEditor(TypeInfo(TRVLength), TRVDocParameters, '',  TRVLengthProperty);
  {$ENDIF}
  RegisterPropertyEditor(TypeInfo(TRVLength), TRVUnitsRect, '',  TRVLengthProperty);
  RegisterPropertyEditor(TypeInfo(TRVLength), TRVPrint, '',  TRVLengthProperty);
  {$ENDIF}  
  {$IFNDEF RVDONOTUSESTYLETEMPLATES}
  RegisterPropertyEditor(TypeInfo(TRVStyleTemplateName), nil, '',
    TRVStyleTemplateNameProperty);
  {$IFDEF RICHVIEWDEF6}
  RegisterPropertyEditor(TypeInfo(String), TRVStyleTemplate, 'ValidParaPropertiesEditor',
    TRVValidParaPropertiesEditProperty);
  RegisterPropertyEditor(TypeInfo(String), TRVStyleTemplate, 'ValidTextPropertiesEditor',
    TRVValidTextPropertiesEditProperty);
  RegisterPropertyEditor(TypeInfo(TRVStyleTemplateCollection), nil,'',
    TRVStyleTemplateCollectionProperty);
  {$ENDIF}
  RegisterPropertyEditor(TypeInfo(TRVStyleTemplateId), TRVStyleTemplate,
    'ParentId',  TRVStyleTemplateIdProperty);
  RegisterPropertyEditor(TypeInfo(TRVStyleTemplateId), TRVStyleTemplate,
    'NextId',  TRVStyleTemplateIdProperty);    
  RegisterPropertyEditor(TypeInfo(TRVStyleTemplateId), TCustomRVInfo,
    'StyleTemplateId',  TRVStyleTemplateIdProperty);
  {$ENDIF}
  {$IFDEF RICHVIEWDEF5}
  RegisterPropertiesInCategory(TLocalizableCategory, TFontInfo,
    ['StyleName','Charset','FontName','Size', 'Unicode']);
  RegisterPropertiesInCategory(TInputCategory, TFontInfo,
    ['NextStyleNo', 'Protection']);
  RegisterPropertiesInCategory(TVisualCategory, TFontInfo,
    ['Charset','FontName','Size','Style','StyleEx','VShift','Unicode',
     'CharScale', 'CharSpacing', 'UnderlineType', 'SubSuperScriptType']);
  RegisterPropertiesInCategory(TRVStyleNameCategory, TFontInfo,
    ['StyleName']);
  RegisterPropertiesInCategory(TRVHypertextCategory, TFontInfo,
    ['Jump', 'JumpCursor', 'HoverColor', 'HoverBackColor', 'HoverEffects',
     'HoverUnderlineColor']);
  RegisterPropertiesInCategory(TRVStyleNameCategory, TFontInfo,
    ['StyleName']);

  RegisterPropertiesInCategory(TLocalizableCategory, TParaInfo,
    ['StyleName','FirstIndent','LeftIndent','RightIndent', 'Alignment',
     'SpaceAfter', 'SpaceBefore', 'Tabs']);
  RegisterPropertiesInCategory(TVisualCategory, TParaInfo,
    ['FirstIndent','LeftIndent','RightIndent', 'Alignment',
    'SpaceAfter', 'SpaceBefore', 'Border', 'Background',
    'LineSpacing', 'LineSpacingType', 'Tabs']);
  RegisterPropertiesInCategory(TRVStyleNameCategory, TParaInfo,
    ['StyleName']);
  RegisterPropertiesInCategory(TInputCategory, TParaInfo,
    ['NextParaNo']);

  {$IFNDEF RVDONOTUSELISTS}
  RegisterPropertiesInCategory(TRVStyleNameCategory, TRVListInfo,
    ['StyleName']);
  RegisterPropertiesInCategory(TVisualCategory, TRVListInfo,
    ['OneLevelPreview']);
  RegisterPropertiesInCategory(TVisualCategory, TRVListLevel,
    ['ImageIndex', 'ImageList', 'Picture', 'FirstIndent',
     'FormatString', 'FormatStringW', 'LeftIndent', 'ListType',
     'MarkerAlignment', 'MarkerIndent']);
  {$ENDIF}

  RegisterPropertiesInCategory(TLocalizableCategory, TRVStyle,
    ['DefCodePage']);
  RegisterPropertiesInCategory(TRVHypertextCategory, TRVStyle,
    ['JumpCursor', 'HoverColor']);
  RegisterPropertiesInCategory(TRVCPCategory, TRVStyle,
    ['CheckpointColor', 'CheckpointEvColor']);
  RegisterPropertiesInCategory(TVisualCategory, TRVStyle,
    ['SelectionStyle', 'SelectionMode']);

  RegisterPropertiesInCategory(TLocalizableCategory, TCustomRichView,
    ['Delimiters']);
  RegisterPropertiesInCategory(TInputCategory, TCustomRichView,
    ['OnRVDblClick','OnRVMouseDown', 'OnRVMouseUp','OnRVRightClick',
     'WheelStep']);
  RegisterPropertiesInCategory(TRVRVFCategory, TCustomRichView,
    ['RVFOptions', 'RVFTextStylesReadMode', 'RVFParaStylesReadMode',
     'OnRVFControlNeeded','OnRVFImageListNeeded', 'OnRVFPictureNeeded']);
  RegisterPropertiesInCategory(TVisualCategory, TCustomRichView,
    ['BackgroundStyle',
      'LeftMargin','RightMargin','TopMargin','BottomMargin',
      'MinTextWidth','MaxTextWidth',
      'Tracking', 'VScrollVisible', 'HScrollVisible',
      'DoInPaletteMode', 'UseXPThemes', 'AnimationMode', 'MaxLength',
      'WordWrap', 'VAlign'
      ]);
  RegisterPropertiesInCategory(TRVCPCategory, TCustomRichView,
    ['CPEventKind','OnCheckpointVisible']);
  RegisterPropertiesInCategory(TRVHypertextCategory, TCustomRichView,
    ['FirstJumpNo','OnJump','OnRVMouseMove', 'OnURLNeeded', 'OnReadHyperlink',
     'OnWriteHyperlink']);
  RegisterPropertiesInCategory(TLegacyCategory, TCustomRichView,
    ['AllowSelection', 'SingleClick', 'OnURLNeeded']);
  RegisterPropertiesInCategory(TRVIOCategory, TCustomRichView,
    ['RTFOptions', 'RTFReadProperties', 'OnSaveComponentToFile', 'OnURLNeeded',
     'OnReadHyperlink', 'OnWriteHyperlink',
     'OnHTMLSaveImage', 'OnSaveImage2', 'OnImportPicture',
     'OnSaveRTFExtra', 'OnSaveHTMLExtra', 'OnProgress',
     'OnSaveItemToFile']);

  RegisterPropertiesInCategory(TInputCategory, TCustomRichView,
    ['OnCopy', 'OnSelect', 'OnVScrolled', 'OnHScrolled']);

  RegisterPropertiesInCategory(TVisualCategory, TCustomRichView,
    ['OnGetItemCursor']);

  RegisterPropertiesInCategory(THelpCategory, TCustomRichView,
    ['OnItemHint']);

  RegisterPropertiesInCategory(TInputCategory, TCustomRichViewEdit,
    ['AcceptDragDropFormats', 'AcceptPasteFormats',
     'DefaultPictureVAlign',
    'EditorOptions', 'UndoLimit', 'OnCaretGetOut', 'OnCaretMove',
     'OnChange', 'OnChanging', 'OnStyleConversion', 'OnParaStyleConversion',
     'OnCheckStickingItems', 'OnCurTextStyleChanged', 'OnCurParaStyleChanged',
     'OnDropFiles', 'OnItemResize', 'OnItemTextEdit',
     'OnOleDragEnter', 'OnOleDragLeave', 'OnOleDragOver',
     'OnOleDrop', 'OnPaste']);

  RegisterPropertiesInCategory(TVisualCategory, TCustomRichViewEdit,
    ['CustomCaretInterval', 'OnDrawCustomCaret', 'OnMeasureCustomCaret',
     'DefaultPictureVAlign']);
  {$ENDIF}
  {$IFDEF RICHVIEWDEF6}
  RegisterPropertiesInCategory(TDragDropCategory, TCustomRichViewEdit,
    ['AcceptDragDropFormats',
    'OnDropFiles', 'OnOleDragEnter', 'OnOleDragLeave', 'OnOleDragOver',
     'OnOleDrop']);

  {$ENDIF}
end;
{$ENDIF}


end.
