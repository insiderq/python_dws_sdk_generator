﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRVData.pas' rev: 27.00 (Windows)

#ifndef CrvdataHPP
#define CrvdataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Crvdata
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVRepaintOperation : unsigned char { rvrrInvalidate, rvrrInvalidateLiveSpell, rvrrUpdate };

__interface IRVScaleRichViewInterface;
typedef System::DelphiInterface<IRVScaleRichViewInterface> _di_IRVScaleRichViewInterface;
class DELPHICLASS TCustomRVData;
__interface IRVScaleRichViewInterface  : public System::IInterface 
{
	
public:
	virtual Vcl::Graphics::TCanvas* __fastcall GetFormatCanvas(void) = 0 ;
	virtual void __fastcall DrawControl(int Left, int Top, Vcl::Graphics::TCanvas* Canvas, TCustomRVData* RVData, int DrawItemNo) = 0 ;
	virtual void __fastcall GetMouseClientCoords(Vcl::Controls::TWinControl* RichView, int &X, int &Y) = 0 ;
	virtual void __fastcall DoOnBiDiModeChange(Vcl::Controls::TWinControl* RichView) = 0 ;
	virtual void __fastcall DoAfterReadNote(TCustomRVData* RVData, int ItemNo) = 0 ;
	virtual System::TObject* __fastcall CreateNoteSRVData(int StreamingId) = 0 ;
	virtual void __fastcall FloatingBoxChanged(TCustomRVData* RVData, int ItemNo, bool OnlyRedrawing) = 0 ;
	virtual Vcl::Controls::TCustomControl* __fastcall GetSRichViewEdit(void) = 0 ;
	virtual void __fastcall DoBeforeSaving(Vcl::Controls::TWinControl* RichView) = 0 ;
	virtual void __fastcall DoOnSetHint(Vcl::Controls::TWinControl* RichView) = 0 ;
	virtual void __fastcall DoOnRepaint(Vcl::Controls::TWinControl* RichView, TRVRepaintOperation Operation, System::Types::PRect Rect) = 0 ;
	virtual void __fastcall DoOnFormat(TCustomRVData* RVData) = 0 ;
	virtual void __fastcall DoOnClear(TCustomRVData* RVData) = 0 ;
	virtual bool __fastcall IsAniRectVisible(Vcl::Controls::TWinControl* RichView, System::Types::TRect &ClientR, float &ZoomPercent) = 0 ;
	virtual System::Types::TRect __fastcall GetClientRect(void) = 0 ;
	virtual System::TObject* __fastcall GetSmartPopupProperties(void) = 0 ;
	virtual float __fastcall GetCurrentPageZoomPercent(void) = 0 ;
	virtual void __fastcall ConvertRVToSRV(System::Types::TPoint &ClientPt) = 0 ;
	virtual float __fastcall GetDrawnPageZoomPercent(void) = 0 ;
	virtual bool __fastcall IsOnImportPictureAssigned(void) = 0 ;
	virtual bool __fastcall IsOnReadHyperlinkAssigned(void) = 0 ;
	virtual bool __fastcall IsOnWriteHyperlinkAssigned(void) = 0 ;
	virtual System::TClass __fastcall GetRTFReadPropertiesClass(void) = 0 ;
	virtual Vcl::Controls::TWinControl* __fastcall GetActiveEditor(bool MainEditor) = 0 ;
	virtual void __fastcall Format(void) = 0 ;
	virtual void __fastcall Paginate(void) = 0 ;
	virtual void __fastcall StoreSubDocChanges(void) = 0 ;
	virtual void __fastcall DoOnAddUndo(Vcl::Controls::TWinControl* RichView) = 0 ;
	virtual void __fastcall DoOnDeleteLastUndo(Vcl::Controls::TWinControl* RichView) = 0 ;
	virtual void __fastcall DoOnChange(Vcl::Controls::TWinControl* RichView, bool ClearRedo) = 0 ;
};

enum DECLSPEC_DENUM TRVState : unsigned char { rvstMakingSelection, rvstLineSelection, rvstDrawHover, rvstSkipFormatting, rvstFormatting, rvstFormattingPart, rvstChangingBkPalette, rvstCompletelySelected, rvstClearing, rvstDoNotMoveChildren, rvstForceStyleChangeEvent, rvstIgnoreNextChar, rvstDoNotTab, rvstDeselecting, rvstUnAssigningChosen, rvstNoScroll, rvstFinalizingUndo, rvstRTFSkipPar, rvstLoadingAsPartOfItem, rvstLoadingAsSubDoc, rvstNoKillFocusEvents, rvstEditorUnformatted, rvstNameSet, rvstFirstParaAborted, rvstLastParaAborted, rvstInvalidSelection, rvstDoNotClearCurTag, rvstStartingDragDrop, rvstCanDragDropDeleteSelection, rvstKeyPress, rvstDoNotSaveContent, rvstPreserveSoftPageBreaks, rvstNoDBExitUpdate, rvstSavingPage, rvstCreatingInplace, 
	rvstNoSpecialCharacters, rvstJumpsJustBuilt, rvstNoStyleTemplateAdustment, rvstTouchMouseDown };

typedef System::Set<TRVState, TRVState::rvstMakingSelection, TRVState::rvstTouchMouseDown> TRVStates;

enum DECLSPEC_DENUM TRVFlag : unsigned char { rvflUseJumps, rvflIgnoreMouseLeave, rvflTrim, rvflShareContents, rvflUseExternalLeading, rvflMouseXYAlwaysCorrect, rvflAllowCustomDrawItems, rvflPrinting, rvflRootEditor, rvflRoot, rvflDBRichViewEdit, rvflCanUseCustomPPI, rvflCanProcessGetText, rvflProcessedWidthHeight };

typedef System::Set<TRVFlag, TRVFlag::rvflUseJumps, TRVFlag::rvflProcessedWidthHeight> TRVFlags;

enum DECLSPEC_DENUM TRVFSaveScope : unsigned char { rvfss_Full, rvfss_Selection, rvfss_Page, rvfss_FromPage, rvfss_FullInPage };

typedef Rvclasses::TRVIntegerList* *PRVIntegerList;

typedef void __fastcall (__closure *TRVEnumItemsProc)(TCustomRVData* RVData, int ItemNo, int &UserData1, const System::UnicodeString UserData2, bool &ContinueEnum);

class DELPHICLASS TRVLayoutInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVLayoutInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	bool Loaded;
	int LeftMargin;
	int RightMargin;
	int TopMargin;
	int BottomMargin;
	int MinTextWidth;
	int MaxTextWidth;
	Rvscroll::TRVBiDiMode BiDiMode;
	int LastParaAborted;
	int FirstParaAborted;
	int FirstMarkerListNo;
	int FirstMarkerLevel;
	__fastcall TRVLayoutInfo(void);
	void __fastcall SaveToStream(System::Classes::TStream* Stream, bool IncludeSize, bool OnlyPageInfo);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream, bool IncludeSize);
	void __fastcall SaveTextToStream(System::Classes::TStream* Stream, bool OnlyPageInfo);
	void __fastcall LoadText(const Rvtypes::TRVAnsiString s);
	void __fastcall LoadBinary(const Rvtypes::TRVRawByteString s);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVLayoutInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVHTMLBulletInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVHTMLBulletInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString FileName;
	Vcl::Imglist::TCustomImageList* ImageList;
	int ImageIndex;
	System::Uitypes::TColor BackColor;
	Vcl::Graphics::TGraphic* Graphic;
public:
	/* TObject.Create */ inline __fastcall TRVHTMLBulletInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVHTMLBulletInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFFontTableItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFFontTableItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString FontName;
	System::Uitypes::TFontCharset Charset;
public:
	/* TObject.Create */ inline __fastcall TRVRTFFontTableItem(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFFontTableItem(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFFontTable;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFFontTable : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFFontTableItem* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVRTFFontTableItem* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFFontTableItem* const Value);
	
public:
	HIDESBASE int __fastcall Find(const System::UnicodeString FontName, System::Uitypes::TFontCharset Charset);
	int __fastcall AddUnique(const System::UnicodeString FontName, System::Uitypes::TFontCharset Charset);
	__property TRVRTFFontTableItem* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFFontTable(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFFontTable(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFTables;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFTables : public Rvfuncs::TCustomRVRTFTables
{
	typedef Rvfuncs::TCustomRVRTFTables inherited;
	
private:
	TRVRTFFontTable* FFontTable;
	Rvclasses::TRVColorList* FColorList;
	Rvclasses::TRVIntegerList* FStyleSheetMap;
	
public:
	__fastcall TRVRTFTables(void);
	__fastcall virtual ~TRVRTFTables(void);
	virtual void __fastcall ClearColors(void);
	virtual void __fastcall ClearFonts(void);
	virtual void __fastcall AddColor(System::Uitypes::TColor Color);
	virtual int __fastcall GetColorIndex(System::Uitypes::TColor Color);
	virtual int __fastcall AddFontFromStyle(Rvstyle::TCustomRVFontInfo* TextStyle);
	virtual int __fastcall AddFontFromStyleEx(Rvstyle::TCustomRVFontInfo* TextStyle, const Rvstyle::TRVFontInfoProperties &ValidProperties, bool Root);
	virtual int __fastcall AddFont(Vcl::Graphics::TFont* Font);
	virtual int __fastcall GetFontStyleIndex(Rvstyle::TCustomRVFontInfo* TextStyle);
	virtual int __fastcall GetFontIndex(Vcl::Graphics::TFont* Font);
	virtual void __fastcall ClearStyleSheetMap(void);
	virtual void __fastcall AddStyleSheetMap(int StyleTemplateNo);
	virtual int __fastcall GetStyleSheetIndex(int StyleTemplateNo, bool CharStyle);
};

#pragma pack(pop)

class DELPHICLASS TRVThumbnailCache;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVThumbnailCache : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	int MaxCount;
	__fastcall TRVThumbnailCache(void);
	void __fastcall AddThumbnail(System::TObject* ImageOwner);
	void __fastcall RemoveThumbnail(System::TObject* ImageOwner);
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVThumbnailCache(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVData : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	int FFirstJumpNo;
	Rvitem::TRVItemList* FItems;
	void __fastcall CheckItemIndex(int i);
	bool __fastcall GetPageBreaksBeforeItems(int Index);
	void __fastcall SetPageBreaksBeforeItems(int Index, bool Value);
	bool __fastcall GetClearLeft(int Index);
	bool __fastcall GetClearRight(int Index);
	void __fastcall SetClearLeft(int Index, const bool Value);
	void __fastcall SetClearRight(int Index, const bool Value);
	int __fastcall GetItemCount(void);
	bool __fastcall ShouldSaveTextToFormat(int StyleNo, Rvstyle::TRVTextOption AllowedCode);
	bool __fastcall ShouldSaveTextToHTML(int StyleNo);
	Rvtypes::TRVAnsiString __fastcall GetHTMLATag(int ItemNo, Rvtypes::TRVRawByteString CSS, Rvstyle::TRVSaveOptions Options);
	bool __fastcall ShouldSaveTextToRTF(int StyleNo);
	bool __fastcall ShouldSaveTextToDocX(int StyleNo);
	void __fastcall AddNLRTag_(const Rvtypes::TRVRawByteString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddNLATag_(const Rvtypes::TRVAnsiString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	bool __fastcall AddTextUniversal(const Rvtypes::TRVRawByteString text, int StyleNo, int FirstParaNo, int OtherParaNo, bool AsSingleParagraph, bool CheckUnicode, Rvstyle::TRVCodePage CodePage, const Rvstyle::TRVTag Tag);
	
protected:
	bool FAllowNewPara;
	Rvitem::TRVCPInfo* FirstCP;
	Rvitem::TRVCPInfo* LastCP;
	Rvitem::TRVCPInfo* NotAddedCP;
	virtual bool __fastcall IsWordWrapAllowed(void);
	int __fastcall NextCharStr(const Rvtypes::TRVRawByteString str, int ItemNo, int Index);
	int __fastcall PrevCharStr(const Rvtypes::TRVRawByteString str, int ItemNo, int Index);
	int __fastcall NextChar(int ItemNo, int Index);
	int __fastcall PrevChar(int ItemNo, int Index);
	void __fastcall CheckItemClass(int ItemNo, Rvitem::TCustomRVItemInfoClass RequiredClass);
	DYNAMIC bool __fastcall ShareItems(void);
	DYNAMIC bool __fastcall CanLoadLayout(void);
	DYNAMIC System::UnicodeString __fastcall GetURL(int id) = 0 ;
	virtual Rvscroll::TRVOptions __fastcall GetOptions(void);
	virtual void __fastcall SetOptions(const Rvscroll::TRVOptions Value);
	virtual Rvstyle::TRVFOptions __fastcall GetRVFOptions(void);
	virtual void __fastcall SetRVFOptions(const Rvstyle::TRVFOptions Value);
	virtual Rvstyle::TRVRTFOptions __fastcall GetRTFOptions(void);
	virtual void __fastcall SetRTFOptions(const Rvstyle::TRVRTFOptions Value);
	virtual Rvstyle::TRVFWarnings __fastcall GetRVFWarnings(void);
	virtual void __fastcall SetRVFWarnings(const Rvstyle::TRVFWarnings Value);
	DYNAMIC System::UnicodeString __fastcall GetDelimiters(void);
	virtual Rvstyle::TRVFReaderStyleMode __fastcall GetRVFTextStylesReadMode(void);
	virtual Rvstyle::TRVFReaderStyleMode __fastcall GetRVFParaStylesReadMode(void);
	DYNAMIC void __fastcall RVFGetLimits(TRVFSaveScope SaveScope, int &StartItem, int &EndItem, int &StartOffs, int &EndOffs, Rvitem::TRVMultiDrawItemPart* &StartPart, Rvitem::TRVMultiDrawItemPart* &EndPart, Rvitem::TCustomRVItemInfo* &SelectedItem);
	DYNAMIC System::Classes::TPersistent* __fastcall GetRTFProperties(void);
	void __fastcall DoOnStyleReaderError(System::Classes::TReader* Reader, const System::UnicodeString Message, bool &Handled);
	bool __fastcall InsertRVFFromStream_(System::Classes::TStream* Stream, int &Index, int AParaNo, bool AllowReplaceStyles, bool AppendMode, bool EditFlag, System::Uitypes::TColor &Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout, int &NonFirstItemsAdded, bool &Protect, bool &FullReformat, bool LoadAsSubDoc, Rvstyle::TRVStyleTemplateCollection* ReadStyleTemplates);
	DYNAMIC void __fastcall DataWriter(System::Classes::TStream* Stream);
	void __fastcall DataReader(System::Classes::TStream* Stream);
	void __fastcall ApplyLayoutInfo(TRVLayoutInfo* Layout);
	void __fastcall NormalizeParas(int StartItemNo);
	void __fastcall InsertCheckpoint(int ItemNo, const Rvstyle::TRVTag Tag, const System::UnicodeString Name, bool RaiseEvent);
	void __fastcall UnlinkCheckpoint(Rvitem::TRVCPInfo* cp, bool DecCPCount);
	Rvitem::TRVCPInfo* __fastcall FindCPBeforeItem(int ItemNo);
	void __fastcall UpdateCPItemNo(void);
	virtual void __fastcall InternalFreeItem(Rvitem::TCustomRVItemInfo* item, bool Clearing);
	bool __fastcall IsDelimiter(const Rvtypes::TRVRawByteString s, int Index, Rvitem::TRVItemOptions ItemOptions, Rvstyle::TRVCodePage CodePage);
	void __fastcall Replace0(Rvtypes::TRVRawByteString &s);
	bool __fastcall RV_CanConcateItems(int FirstItemNo, Rvitem::TCustomRVItemInfo* item1, Rvitem::TCustomRVItemInfo* item2, bool IgnorePara, bool AllowConcateNonText);
	void __fastcall SimpleConcate(int FirstItemNo, Rvitem::TCustomRVItemInfo* item1, Rvitem::TCustomRVItemInfo* item2);
	void __fastcall MassSimpleConcate(int FirstItemNo, int LastItemNo);
	void __fastcall SimpleConcateSubitems(int ItemNo);
	DYNAMIC bool __fastcall SupportsPageBreaks(void);
	void __fastcall SaveHTMLCheckpoint(System::Classes::TStream* Stream, Rvitem::TRVCPInfo* Checkpoint, int &cpno, const System::UnicodeString Prefix, bool FromNewLine, Rvstyle::TRVSaveOptions Options);
	Rvtypes::TRVRawByteString __fastcall GetTextForHTML(const System::UnicodeString Path, int ItemNo, bool CSSVersion, Rvstyle::TRVSaveOptions SaveOptions);
	void __fastcall SaveHTMLNotes(const System::UnicodeString Path, const System::UnicodeString ImagesPrefix, const System::UnicodeString CPPrefix, System::Classes::TStream* Stream, bool CSSVersion, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, int &imgSaveNo, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets, Rvitem::TCustomRVItemInfoClass NoteClass, int StyleNo);
	int __fastcall GetFirstParaItem(int ItemNo);
	int __fastcall GetFirstParaSectionItem(int ItemNo);
	Rvitem::TCustomRVItemInfo* __fastcall FindPreviousItem(int ItemNo, Rvitem::TCustomRVItemInfoClass ItemClass);
	int __fastcall FindItemLocalLocationFrom(int StartItemNo, Rvitem::TCustomRVItemInfo* ItemToFind);
	Rvseqitem::TRVSeqItemInfo* __fastcall FindPreviousSeq(int ItemNo);
	DYNAMIC void __fastcall DestroySeqList(void);
	int __fastcall FindLastSeqIndex(int StartAfterMeIndex, System::Classes::TStringList* SeqNames);
	DYNAMIC void __fastcall DestroyMarkers(void);
	Rvmarker::TRVMarkerItemInfo* __fastcall FindPreviousMarker(int ItemNo);
	int __fastcall FindLastMarkerIndex(int StartAfterMeIndex, Rvclasses::TRVIntegerList* ListStyles);
	virtual TRVFlags __fastcall GetFlags(void) = 0 ;
	virtual void __fastcall SetFlags(const TRVFlags Value) = 0 ;
	void __fastcall AddStringFromFile(const Rvtypes::TRVAnsiString s, int StyleNo, int ParaNo, bool FromNewLine, bool AsSingleParagraph, bool &FirstTime, bool &PageBreak);
	DYNAMIC void __fastcall AfterDeleteStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	virtual int __fastcall GetMaxLength(void);
	DYNAMIC void __fastcall ModifyStyleNoByTemplate(Rvitem::TCustomRVItemInfo* Item, int NewStyleNo);
	void __fastcall AdjustItemParaNoBeforeAdding(Rvitem::TCustomRVItemInfo* Item);
	
public:
	TRVStates State;
	int FFirstParaListNo;
	int FFirstParaLevel;
	int CPCount;
	__fastcall TCustomRVData(void);
	__fastcall virtual ~TCustomRVData(void);
	virtual TCustomRVData* __fastcall GetRVData(void);
	virtual TCustomRVData* __fastcall GetSourceRVData(void);
	Rvstyle::TRVCodePage __fastcall GetStyleCodePage(int StyleNo);
	Rvstyle::TRVCodePage __fastcall GetItemCodePage(int ItemNo);
	bool __fastcall IsSymbolCharset(int ItemNo);
	Rvstyle::TRVCodePage __fastcall GetItemCodePage2(Rvitem::TCustomRVItemInfo* Item);
	unsigned __fastcall GetStyleLocale(int StyleNo);
	Rvstyle::TRVCodePage __fastcall GetDefaultCodePage(void);
	virtual Rvstyle::TRVStyle* __fastcall GetRVStyle(void);
	DYNAMIC Vcl::Controls::TWinControl* __fastcall GetParentControl(void);
	DYNAMIC void __fastcall GetParentInfo(int &ParentItemNo, Rvitem::TRVStoreSubRVData* &Location);
	int __fastcall GetAbsoluteRootItemNo(int ItemNo);
	DYNAMIC TCustomRVData* __fastcall GetChosenRVData(void);
	TCustomRVData* __fastcall GetTopLevelChosenRVData(void);
	DYNAMIC Rvitem::TCustomRVItemInfo* __fastcall GetChosenItem(void);
	virtual TCustomRVData* __fastcall GetParentData(void);
	virtual TCustomRVData* __fastcall GetRootData(void);
	virtual TCustomRVData* __fastcall GetAbsoluteParentData(void);
	virtual TCustomRVData* __fastcall GetAbsoluteRootData(void);
	DYNAMIC System::UnicodeString __fastcall GetNoteText(void);
	virtual HPALETTE __fastcall GetRVPalette(void);
	virtual Winapi::Windows::PLogPalette __fastcall GetRVLogPalette(void);
	virtual Rvscroll::TRVPaletteAction __fastcall GetDoInPaletteMode(void);
	void __fastcall UpdateItemsPaletteInfo(void);
	virtual TRVThumbnailCache* __fastcall GetThumbnailCache(void);
	DYNAMIC void __fastcall FreeBackgroundThumbnail(void);
	Rvitem::TRVItemOptions __fastcall GetItemOptions(int ItemNo);
	int __fastcall GetItemNo(Rvitem::TCustomRVItemInfo* Item);
	Rvitem::TCustomRVItemInfo* __fastcall GetItem(int ItemNo);
	bool __fastcall SetItemExtraIntProperty(int ItemNo, Rvitem::TRVExtraItemProperty Prop, int Value);
	bool __fastcall GetItemExtraIntProperty(int ItemNo, Rvitem::TRVExtraItemProperty Prop, int &Value);
	bool __fastcall SetItemExtraIntPropertyEx(int ItemNo, int Prop, int Value);
	bool __fastcall GetItemExtraIntPropertyEx(int ItemNo, int Prop, int &Value);
	bool __fastcall SetItemExtraStrProperty(int ItemNo, Rvitem::TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
	bool __fastcall GetItemExtraStrProperty(int ItemNo, Rvitem::TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	bool __fastcall SetItemExtraStrPropertyEx(int ItemNo, int Prop, const System::UnicodeString Value);
	bool __fastcall GetItemExtraStrPropertyEx(int ItemNo, int Prop, System::UnicodeString &Value);
	Rvstyle::TRVTag __fastcall GetItemTag(int ItemNo);
	Rvstyle::TRVVAlign __fastcall GetItemVAlign(int ItemNo);
	bool __fastcall IsParaStart(int ItemNo);
	int __fastcall GetItemPara(int ItemNo);
	bool __fastcall IsFromNewLine(int ItemNo);
	int __fastcall GetOffsAfterItem(int ItemNo);
	int __fastcall GetOffsBeforeItem(int ItemNo);
	int __fastcall ItemLength(int ItemNo);
	void __fastcall SetItemTag(int ItemNo, const Rvstyle::TRVTag ATag);
	void __fastcall SetItemVAlign(int ItemNo, Rvstyle::TRVVAlign AVAlign);
	int __fastcall GetItemStyle(int ItemNo);
	int __fastcall GetActualStyle(Rvitem::TCustomRVItemInfo* Item);
	int __fastcall GetActualTextStyle(Rvitem::TCustomRVItemInfo* Item);
	int __fastcall GetActualStyleEx(int StyleNo, int ParaNo);
	int __fastcall GetStyleFromStyleTemplate(int StyleNo, int ParaNo);
	DYNAMIC bool __fastcall UseStyleTemplates(void);
	DYNAMIC Rvstyle::TRVStyleTemplateInsertMode __fastcall StyleTemplateInsertMode(void);
	Rvtypes::TRVRawByteString __fastcall GetItemTextR(int ItemNo);
	void __fastcall SetItemTextR(int ItemNo, const Rvtypes::TRVRawByteString s);
	void __fastcall SetItemTextA(int ItemNo, const Rvtypes::TRVAnsiString s);
	void __fastcall SetItemText(int ItemNo, const System::UnicodeString s);
	Rvtypes::TRVAnsiString __fastcall GetItemTextA(int ItemNo);
	Rvtypes::TRVAnsiString __fastcall GetItemTextExA(int ItemNo, Rvstyle::TRVCodePage CodePage);
	System::UnicodeString __fastcall GetItemText(int ItemNo);
	System::UnicodeString __fastcall GetItemTextEx(int ItemNo, Rvstyle::TRVCodePage CodePage);
	Rvtypes::TRVRawByteString __fastcall GetTextInItemFormatW(int ItemNo, const Rvtypes::TRVUnicodeString s);
	Rvtypes::TRVUnicodeString __fastcall GetItemTextW(int ItemNo);
	Rvtypes::TRVUnicodeString __fastcall GetItemTextExW(int ItemNo, const Rvtypes::TRVRawByteString SourceStr);
	void __fastcall SetItemTextW(int ItemNo, const Rvtypes::TRVUnicodeString s);
	Rvtypes::TRVRawByteString __fastcall GetTextInItemFormatA(int ItemNo, const Rvtypes::TRVAnsiString s);
	int __fastcall FindControlItemNo(Vcl::Controls::TControl* actrl);
	bool __fastcall IsHiddenItem(int ItemNo);
	bool __fastcall IsHiddenParaEnd(int FirstItemNo);
	Rvscroll::TRVBiDiMode __fastcall GetItemBiDiMode(int ItemNo);
	Rvscroll::TRVBiDiMode __fastcall GetParaBiDiMode(int ParaNo);
	virtual Rvscroll::TRVBiDiMode __fastcall GetBiDiMode(void);
	void __fastcall FreeItem(int ItemNo, bool Clearing);
	DYNAMIC void __fastcall Clear(void);
	DYNAMIC void __fastcall DeleteItems(int FirstItemNo, int Count);
	void __fastcall DeleteSection(const System::UnicodeString CpName);
	void __fastcall MassSimpleConcateAll(void);
	DYNAMIC bool __fastcall IsAssignedOnProgress(void);
	DYNAMIC void __fastcall DoProgress(Rvstyle::TRVLongOperation Operation, Rvstyle::TRVProgressStage Stage, System::Byte PercentDone);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetExtraRTFCode(Rvstyle::TRVRTFSaveArea Area, System::TObject* Obj, int Index1, int Index2, bool InStyleSheet);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetExtraDocXCode(Rvstyle::TRVDocXSaveArea Area, System::TObject* Obj, int Index1, int Index2);
	DYNAMIC System::UnicodeString __fastcall GetExtraHTMLCode(Rvstyle::TRVHTMLSaveArea Area, bool CSSVersion);
	DYNAMIC System::UnicodeString __fastcall GetParaHTMLCode(TCustomRVData* RVData, int ItemNo, bool ParaStart, bool CSSVersion);
	Rvtypes::TRVRawByteString __fastcall GetParaHTMLCode2(TCustomRVData* RVData, int ItemNo, bool ParaStart, bool CSSVersion, Rvstyle::TRVSaveOptions Options, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall ReadHyperlink(const System::UnicodeString Target, const System::UnicodeString Extras, Rvstyle::TRVLoadFormat DocFormat, int &StyleNo, Rvstyle::TRVTag &ItemTag, Rvtypes::TRVRawByteString &ItemName);
	DYNAMIC void __fastcall WriteHyperlink(int id, TCustomRVData* RVData, int ItemNo, Rvstyle::TRVSaveFormat SaveFormat, System::UnicodeString &Target, System::UnicodeString &Extras);
	virtual bool __fastcall SaveItemToFile(const System::UnicodeString Path, TCustomRVData* RVData, int ItemNo, Rvstyle::TRVSaveFormat SaveFormat, bool Unicode, Rvtypes::TRVRawByteString &Text);
	DYNAMIC Vcl::Graphics::TGraphic* __fastcall ImportPicture(const System::UnicodeString Location, int Width, int Height, bool &Invalid);
	DYNAMIC System::UnicodeString __fastcall GetItemHint(TCustomRVData* RVData, int ItemNo, const System::UnicodeString UpperRVDataHint);
	virtual System::UnicodeString __fastcall DoSavePicture(Rvstyle::TRVSaveFormat DocumentSaveFormat, TCustomRVData* RVData, int ItemNo, const System::UnicodeString imgSavePrefix, const System::UnicodeString Path, int &imgSaveNo, bool OverrideFiles, System::Uitypes::TColor CurrentFileColor, Vcl::Graphics::TGraphic* gr);
	System::UnicodeString __fastcall SavePicture(Rvstyle::TRVSaveFormat DocumentSaveFormat, const System::UnicodeString imgSavePrefix, const System::UnicodeString Path, int &imgSaveNo, bool OverrideFiles, System::Uitypes::TColor CurrentFileColor, Vcl::Graphics::TGraphic* gr);
	DYNAMIC Vcl::Graphics::TGraphic* __fastcall RVFPictureNeeded(const System::UnicodeString ItemName, Rvitem::TRVNonTextItemInfo* Item, int Index1, int Index2, bool PictureInsideRVF);
	DYNAMIC void __fastcall ControlAction(TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Rvitem::TCustomRVItemInfo* Item);
	virtual void __fastcall ItemAction(Rvstyle::TRVItemAction ItemAction, Rvitem::TCustomRVItemInfo* Item, Rvtypes::TRVRawByteString &Text, TCustomRVData* RVData);
	DYNAMIC _di_IRVScaleRichViewInterface __fastcall GetScaleRichViewInterface(void);
	void __fastcall AfterReadNote(TCustomRVData* RVData, int ItemNo);
	DYNAMIC void __fastcall DoBeforeSaving(void);
	DYNAMIC void __fastcall ControlAction2(TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Vcl::Controls::TControl* &Control) = 0 ;
	DYNAMIC Vcl::Controls::TControl* __fastcall RVFControlNeeded(const System::UnicodeString ItemName, const Rvstyle::TRVTag ItemTag);
	DYNAMIC Vcl::Imglist::TCustomImageList* __fastcall RVFImageListNeeded(int ImageListTag);
	DYNAMIC void __fastcall HTMLSaveImage(TCustomRVData* RVData, int ItemNo, const System::UnicodeString Path, System::Uitypes::TColor BackgroundColor, System::UnicodeString &Location, bool &DoDefault);
	DYNAMIC void __fastcall SaveImage2(TCustomRVData* RVData, int ItemNo, Vcl::Graphics::TGraphic* Graphic, Rvstyle::TRVSaveFormat SaveFormat, const System::UnicodeString Path, const System::UnicodeString ImagePrefix, int &ImageSaveNo, System::UnicodeString &Location, bool &DoDefault);
	virtual System::UnicodeString __fastcall SaveComponentToFile(const System::UnicodeString Path, System::Classes::TComponent* SaveMe, Rvstyle::TRVSaveFormat SaveFormat);
	DYNAMIC void __fastcall DoStyleTemplatesChange(void);
	bool __fastcall LoadTextFromStreamW(System::Classes::TStream* Stream, int StyleNo, int ParaNo, bool DefAsSingleParagraph);
	bool __fastcall LoadTextW(const System::UnicodeString FileName, int StyleNo, int ParaNo, bool DefAsSingleParagraph);
	bool __fastcall SaveTextToStream(const System::UnicodeString Path, System::Classes::TStream* Stream, int LineWidth, bool SelectionOnly, bool TextOnly, bool Unicode, bool UnicodeWriteSignature, Rvstyle::TRVCodePage CodePage, bool Root);
	bool __fastcall SaveText(const System::UnicodeString FileName, int LineWidth, bool Unicode, Rvstyle::TRVCodePage CodePage);
	bool __fastcall LoadText(const System::UnicodeString FileName, int StyleNo, int ParaNo, bool AsSingleParagraph, Rvstyle::TRVCodePage CodePage);
	bool __fastcall LoadTextFromStream(System::Classes::TStream* Stream, int StyleNo, int ParaNo, bool AsSingleParagraph, Rvstyle::TRVCodePage CodePage);
	bool __fastcall NextItemFromSpace(int ItemNo);
	System::UnicodeString __fastcall SaveBackgroundToHTML(Vcl::Graphics::TBitmap* bmp, System::Uitypes::TColor Color, const System::UnicodeString Path, const System::UnicodeString ImagesPrefix, int &imgSaveNo, Rvstyle::TRVSaveOptions SaveOptions);
	DYNAMIC bool __fastcall SaveHTMLToStreamEx(System::Classes::TStream* Stream, const System::UnicodeString Path, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, const System::UnicodeString ExtraStyles, const System::UnicodeString ExternalCSS, const System::UnicodeString CPPrefix, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, System::Uitypes::TColor &CurrentFileColor, int &imgSaveNo, int LeftMargin, int TopMargin, int RightMargin, int BottomMargin, Rvback::TRVBackground* Background, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC bool __fastcall SaveHTMLToStream(System::Classes::TStream* Stream, const System::UnicodeString Path, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, int &imgSaveNo, int LeftMargin, int TopMargin, int RightMargin, int BottomMargin, Rvback::TRVBackground* Background, Rvclasses::TRVList* Bullets);
	bool __fastcall SaveHTMLEx(const System::UnicodeString FileName, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, const System::UnicodeString ExtraStyles, const System::UnicodeString ExternalCSS, const System::UnicodeString CPPrefix, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, System::Uitypes::TColor &CurrentFileColor, int &imgSaveNo, int LeftMargin, int TopMargin, int RightMargin, int BottomMargin, Rvback::TRVBackground* Background);
	bool __fastcall SaveHTML(const System::UnicodeString FileName, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, int &imgSaveNo, int LeftMargin, int TopMargin, int RightMargin, int BottomMargin, Rvback::TRVBackground* Background);
	DYNAMIC System::UnicodeString __fastcall GetNextFileName(const System::UnicodeString ImagesPrefix, const System::UnicodeString Path, const System::UnicodeString Ext, int &imgSaveNo, bool OverrideFiles);
	void __fastcall DoOnCtrlReaderError(System::Classes::TReader* Reader, const System::UnicodeString Message, bool &Handled);
	bool __fastcall LoadRVFFromStream(System::Classes::TStream* Stream, System::Uitypes::TColor &Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout, Rvstyle::TRVStyleTemplateCollection* ReadStyleTemplates);
	bool __fastcall InsertRVFFromStream(System::Classes::TStream* Stream, int Index, System::Uitypes::TColor &Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout, bool AllowReplaceStyles, Rvstyle::TRVStyleTemplateCollection* ReadStyleTemplates);
	bool __fastcall AppendRVFFromStream(System::Classes::TStream* Stream, int ParaNo, System::Uitypes::TColor &Color, Rvback::TRVBackground* Background);
	bool __fastcall LoadRVF(const System::UnicodeString FileName, System::Uitypes::TColor &Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout);
	bool __fastcall SaveRVFToStream(System::Classes::TStream* Stream, bool SelectionOnly, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout, bool Root = true);
	bool __fastcall SaveRVFToStreamEx(System::Classes::TStream* Stream, TRVFSaveScope SaveScope, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout, bool Root, Rvstyle::PRVScreenAndDevice sad);
	bool __fastcall SaveRVF(const System::UnicodeString FileName, bool SelectionOnly, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, TRVLayoutInfo* Layout, bool Root);
	DYNAMIC void __fastcall InitStyleMappings(PRVIntegerList &PTextStylesMapping, PRVIntegerList &PParaStylesMapping, PRVIntegerList &PListStylesMapping, Rvstyle::PRVStyleUnits &PUnits);
	DYNAMIC void __fastcall DoneStyleMappings(bool AsSubDoc);
	DYNAMIC bool __fastcall InsertFirstRVFItem(int &Index, Rvtypes::TRVRawByteString &s, Rvitem::TCustomRVItemInfo* &item, bool EditFlag, bool &FullReformat, int &NewListNo);
	TRVFSaveScope __fastcall GetRVFSaveScope(bool SelectionOnly);
	bool __fastcall SaveProgressInit(int &Progress, int Max, Rvstyle::TRVLongOperation Operation);
	void __fastcall SaveProgressRun(int &Progress, int Position, int Max, Rvstyle::TRVLongOperation Operation);
	void __fastcall SaveProgressDone(Rvstyle::TRVLongOperation Operation);
	void __fastcall MakeFontTable(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* StyleToFont, Rvclasses::TRVIntegerList* &StyleTemplateToFont);
	void __fastcall SaveRTFListTable97(System::Classes::TStream* Stream, Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideOffsetsList, TCustomRVData* Header, TCustomRVData* Footer, TCustomRVData* FirstPageHeader, TCustomRVData* FirstPageFooter, TCustomRVData* EvenPagesHeader, TCustomRVData* EvenPagesFooter);
	DYNAMIC bool __fastcall SaveRTFToStream(System::Classes::TStream* Stream, bool SelectionOnly, int Level, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool CompleteDocument, bool HiddenParent, TCustomRVData* Header, TCustomRVData* Footer, TCustomRVData* FirstPageHeader, TCustomRVData* FirstPageFooter, TCustomRVData* EvenPagesHeader, TCustomRVData* EvenPagesFooter);
	bool __fastcall SaveRTF(const System::UnicodeString FileName, bool SelectionOnly, System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	void __fastcall SaveOOXMLFontTableToStream(System::Classes::TStream* Stream);
	void __fastcall SaveOOXMLStylesToStream(System::Classes::TStream* Stream);
	Rvtypes::TRVAnsiString __fastcall GetOOXMLCheckpointStr(Rvitem::TRVCPInfo* CP, Rvdocx::TRVDocXSavingData* SavingData);
	bool __fastcall SaveOOXMLDocumentSettingsToStream(System::Classes::TStream* Stream);
	bool __fastcall SaveOOXMLCorePropertiesToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall SaveOOXMLDocumentToStream(System::Classes::TStream* Stream, const System::UnicodeString Path, bool SelectionOnly, bool RootDocument, bool InCell, bool HiddenParent, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, Rvdocx::TRVDocXSavingData* SavingData);
	void __fastcall SaveOOXMLListTable(System::Classes::TStream* Stream, Rvdocx::TRVDocXSavingData* SavingData, TCustomRVData* Header, TCustomRVData* Footer, TCustomRVData* FirstPageHeader, TCustomRVData* FirstPageFooter, TCustomRVData* EvenPagesHeader, TCustomRVData* EvenPagesFooter);
	Rvrtferr::TRVRTFErrorCode __fastcall LoadRTFFromStream(System::Classes::TStream* Stream);
	Rvrtferr::TRVRTFErrorCode __fastcall LoadRTF(const System::UnicodeString FileName);
	void __fastcall MakeRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList, bool AddDefColors);
	void __fastcall FillRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList);
	void __fastcall AddItemR(const Rvtypes::TRVRawByteString Text, Rvitem::TCustomRVItemInfo* Item, bool AdjustPara);
	void __fastcall AddItem(const System::UnicodeString Text, Rvitem::TCustomRVItemInfo* Item);
	void __fastcall AddItemAsIsR(const Rvtypes::TRVRawByteString Text, Rvitem::TCustomRVItemInfo* Item);
	void __fastcall AddFmt(const System::UnicodeString FormatStr, System::TVarRec const *Args, const int Args_High, int StyleNo, int ParaNo);
	void __fastcall AddNLR(const Rvtypes::TRVRawByteString s, int StyleNo, int ParaNo);
	void __fastcall AddNL(const System::UnicodeString s, int StyleNo, int ParaNo);
	void __fastcall AddNLRTag(const Rvtypes::TRVRawByteString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddNLTag(const System::UnicodeString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddTextNLR(const Rvtypes::TRVRawByteString s, int StyleNo, int FirstParaNo, int OtherParaNo, const Rvstyle::TRVTag Tag = Rvstyle::TRVTag());
	void __fastcall AddTextNL(const System::UnicodeString s, int StyleNo, int FirstParaNo, int OtherParaNo, const Rvstyle::TRVTag Tag = Rvstyle::TRVTag());
	void __fastcall AddTextNLA(const Rvtypes::TRVAnsiString s, int StyleNo, int FirstParaNo, int OtherParaNo, const Rvstyle::TRVTag Tag = Rvstyle::TRVTag());
	void __fastcall AddTextBlockNLA(const Rvtypes::TRVAnsiString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag = Rvstyle::TRVTag());
	void __fastcall AddNLATag(const Rvtypes::TRVAnsiString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddNLWTag(const Rvtypes::TRVUnicodeString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddNLWTagRaw(const Rvtypes::TRVRawByteString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddTextNLWRaw(const Rvtypes::TRVRawByteString s, int StyleNo, int FirstParaNo, int OtherParaNo, bool DefAsSingleParagraph);
	void __fastcall AddTextNLW(const Rvtypes::TRVUnicodeString s, int StyleNo, int FirstParaNo, int OtherParaNo, bool DefAsSingleParagraph);
	void __fastcall AddTab(int TextStyleNo, int ParaNo);
	void __fastcall AddBreakExTag(Rvstyle::TRVStyleLength Width, Rvstyle::TRVBreakStyle Style, System::Uitypes::TColor Color, const Rvstyle::TRVTag Tag);
	void __fastcall AddBreak(void);
	void __fastcall AddBreakEx(Rvstyle::TRVStyleLength Width, Rvstyle::TRVBreakStyle Style, System::Uitypes::TColor Color);
	void __fastcall AddBreakTag(const Rvstyle::TRVTag Tag);
	void __fastcall AddBulletEx(const Rvtypes::TRVAnsiString Name, int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo);
	void __fastcall AddBulletExTag(const Rvtypes::TRVAnsiString Name, int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddHotspotEx(const Rvtypes::TRVAnsiString Name, int ImageIndex, int HotImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo);
	void __fastcall AddHotspotExTag(const Rvtypes::TRVAnsiString Name, int ImageIndex, int HotImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddPictureExTag(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign, const Rvstyle::TRVTag Tag);
	void __fastcall AddControlExTag(const Rvtypes::TRVAnsiString Name, Vcl::Controls::TControl* ctrl, int ParaNo, Rvstyle::TRVVAlign VAlign, const Rvstyle::TRVTag Tag);
	void __fastcall AddPictureEx(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall AddControlEx(const Rvtypes::TRVAnsiString Name, Vcl::Controls::TControl* ctrl, int ParaNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall AddHotPicture(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall AddHotPictureTag(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign, const Rvstyle::TRVTag Tag);
	void __fastcall FreeCheckpoint(Rvitem::TRVCPInfo* &cp, bool AdjustLinks, bool DecCPCount);
	void __fastcall SetCP(Rvitem::TCustomRVItemInfo* Item, Rvitem::TRVCPInfo* &PrevCP, Rvitem::TRVCPInfo* &CP);
	void __fastcall UpdateCPPos(Rvitem::TRVCPInfo* cp, int ItemNo);
	int __fastcall AddNamedCheckpointExTag(const System::UnicodeString CpName, bool RaiseEvent, const Rvstyle::TRVTag Tag);
	void __fastcall SetCheckpointInfo(int ItemNo, const Rvstyle::TRVTag ATag, const System::UnicodeString AName, bool ARaiseEvent);
	bool __fastcall RemoveCheckpoint(int ItemNo);
	Rvstyle::TCheckpointData __fastcall GetFirstCheckpoint(void);
	Rvstyle::TCheckpointData __fastcall GetNextCheckpoint(Rvstyle::TCheckpointData CheckpointData);
	Rvstyle::TCheckpointData __fastcall GetLastCheckpoint(void);
	Rvstyle::TCheckpointData __fastcall GetPrevCheckpoint(Rvstyle::TCheckpointData CheckpointData);
	Rvstyle::TCheckpointData __fastcall GetItemCheckpoint(int ItemNo);
	Rvstyle::TCheckpointData __fastcall FindCheckpointByName(const System::UnicodeString Name);
	Rvstyle::TCheckpointData __fastcall FindCheckpointByTag(const Rvstyle::TRVTag Tag);
	Rvstyle::TCheckpointData __fastcall GetCheckpointByNo(int No);
	int __fastcall GetCheckpointItemNo(Rvstyle::TCheckpointData CheckpointData);
	int __fastcall GetCheckpointNo(Rvstyle::TCheckpointData CheckpointData);
	void __fastcall GetCheckpointInfo(Rvstyle::TCheckpointData CheckpointData, Rvstyle::TRVTag &Tag, System::UnicodeString &Name, bool &RaiseEvent);
	void __fastcall GetBreakInfo(int ItemNo, Rvstyle::TRVStyleLength &AWidth, Rvstyle::TRVBreakStyle &AStyle, System::Uitypes::TColor &AColor, Rvstyle::TRVTag &ATag);
	void __fastcall GetBulletInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, int &AImageIndex, Vcl::Imglist::TCustomImageList* &AImageList, Rvstyle::TRVTag &ATag);
	void __fastcall GetHotspotInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, int &AImageIndex, int &AHotImageIndex, Vcl::Imglist::TCustomImageList* &AImageList, Rvstyle::TRVTag &ATag);
	void __fastcall GetPictureInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, Vcl::Graphics::TGraphic* &Agr, Rvstyle::TRVVAlign &AVAlign, Rvstyle::TRVTag &ATag);
	void __fastcall GetControlInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, Vcl::Controls::TControl* &Actrl, Rvstyle::TRVVAlign &AVAlign, Rvstyle::TRVTag &ATag);
	void __fastcall GetTextInfo(int ItemNo, System::UnicodeString &AText, Rvstyle::TRVTag &ATag);
	void __fastcall SetGrouped(int ItemNo, bool Grouped);
	void __fastcall SetBreakInfo(int ItemNo, Rvstyle::TRVStyleLength AWidth, Rvstyle::TRVBreakStyle AStyle, System::Uitypes::TColor AColor, const Rvstyle::TRVTag ATag);
	void __fastcall SetBulletInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, int AImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	void __fastcall SetHotspotInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, int AImageIndex, int AHotImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	bool __fastcall SetPictureInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, Vcl::Graphics::TGraphic* Agr, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	bool __fastcall SetControlInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	void __fastcall DoMarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	void __fastcall DoUpdateStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	void __fastcall DeleteMarkedStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	void __fastcall DeleteUnusedStyles(bool TextStyles, bool ParaStyles, bool ListStyles);
	DYNAMIC void __fastcall AfterAddStyle(Rvstyle::TCustomRVInfo* StyleInfo);
	int __fastcall GetOrAddTextStyle(Rvstyle::TFontInfo* TextStyle);
	int __fastcall GetOrAddParaStyle(Rvstyle::TParaInfo* ParaStyle);
	DYNAMIC Rvseqitem::TRVSeqList* __fastcall GetSeqList(bool AllowCreate);
	void __fastcall AddSeqInList(int ItemNo);
	void __fastcall DeleteSeqFromList(Rvitem::TCustomRVItemInfo* Item, bool Clearing);
	DYNAMIC Rvmarker::TRVMarkerList* __fastcall GetMarkers(bool AllowCreate);
	DYNAMIC Rvmarker::TRVMarkerList* __fastcall GetPrevMarkers(void);
	int __fastcall SetListMarkerInfo(int AItemNo, int AListNo, int AListLevel, int AStartFrom, int AParaNo, bool AUseStartFrom);
	void __fastcall RecalcMarker(int AItemNo, bool AllowCreateList);
	void __fastcall RemoveListMarker(int ItemNo);
	int __fastcall GetListMarkerInfo(int AItemNo, int &AListNo, int &AListLevel, int &AStartFrom, bool &AUseStartFrom);
	void __fastcall AddMarkerInList(int ItemNo);
	void __fastcall DeleteMarkerFromList(Rvitem::TCustomRVItemInfo* Item, bool Clearing);
	bool __fastcall IsDelimiterA(char ch, Rvstyle::TRVCodePage CodePage);
	bool __fastcall IsDelimiterW(System::WideChar ch);
	bool __fastcall EnumItems(TRVEnumItemsProc Proc, int &UserData1, const System::UnicodeString UserData2);
	void __fastcall ShareItemsFrom(TCustomRVData* Source);
	void __fastcall AssignItemsFrom(TCustomRVData* Source);
	void __fastcall DrainFrom(TCustomRVData* Victim);
	void __fastcall SetParagraphStyleToAll(int ParaNo);
	void __fastcall SetAddParagraphMode(bool AllowNewPara);
	void __fastcall AppendFrom(TCustomRVData* Source);
	void __fastcall Inserting(TCustomRVData* RVData, bool Safe);
	DYNAMIC TCustomRVData* __fastcall Edit(void);
	void __fastcall Beep(void);
	void __fastcall ExpandToParaSection(int ItemNo1, int ItemNo2, int &FirstItemNo, int &LastItemNo);
	void __fastcall ExpandToPara(int ItemNo1, int ItemNo2, int &FirstItemNo, int &LastItemNo);
	Rvtypes::TRVRawByteString __fastcall ReplaceTabs(const Rvtypes::TRVRawByteString s, int StyleNo, bool UnicodeDef);
	void __fastcall AdjustInItemsRange(int &ItemNo);
	virtual System::Uitypes::TColor __fastcall GetColor(void);
	DYNAMIC Rvdocparams::TRVDocParameters* __fastcall GetDocParameters(bool AllowCreate);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool ConvertItems);
	DYNAMIC bool __fastcall KeepTogether(void);
	DYNAMIC int __fastcall GetCurParaStyleNo(void);
	virtual Rvstyle::TRVDocRotation __fastcall GetRotation(void);
	Rvstyle::TRVDocRotation __fastcall GetRealRotation(bool FromAbsRoot);
	bool __fastcall IsVerticalText(void);
	bool __fastcall IsReallyVerticalText(void);
	bool __fastcall IsRotationUsed(void);
	DYNAMIC void __fastcall ChangeParagraphStyles(Rvclasses::TRVIntegerList* ParaMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall ChangeTextStyles(Rvclasses::TRVIntegerList* TextMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC System::Classes::TStringList* __fastcall GetDocProperties(void);
	DYNAMIC System::Classes::TStrings* __fastcall GetExtraDocuments(void);
	__property TRVFlags Flags = {read=GetFlags, write=SetFlags, nodefault};
	__property Rvitem::TRVItemList* Items = {read=FItems};
	__property int ItemCount = {read=GetItemCount, nodefault};
	__property Rvscroll::TRVOptions Options = {read=GetOptions, write=SetOptions, nodefault};
	__property Rvstyle::TRVFOptions RVFOptions = {read=GetRVFOptions, write=SetRVFOptions, nodefault};
	__property Rvstyle::TRVRTFOptions RTFOptions = {read=GetRTFOptions, write=SetRTFOptions, nodefault};
	__property Rvstyle::TRVFWarnings RVFWarnings = {read=GetRVFWarnings, write=SetRVFWarnings, nodefault};
	__property int FirstJumpNo = {read=FFirstJumpNo, write=FFirstJumpNo, nodefault};
	__property bool PageBreaksBeforeItems[int Index] = {read=GetPageBreaksBeforeItems, write=SetPageBreaksBeforeItems};
	__property bool ClearLeft[int Index] = {read=GetClearLeft, write=SetClearLeft};
	__property bool ClearRight[int Index] = {read=GetClearRight, write=SetClearRight};
};

#pragma pack(pop)

class DELPHICLASS TRVItemListHack;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVItemListHack : public Rvitem::TRVItemList
{
	typedef Rvitem::TRVItemList inherited;
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVItemListHack(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVItemListHack(void) : Rvitem::TRVItemList() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool RichViewSavePInHTML;
extern DELPHI_PACKAGE bool RichViewSaveDivInHTMLEx;
extern DELPHI_PACKAGE bool RichViewSavePageBreaksInText;
extern DELPHI_PACKAGE bool RichViewDoNotCheckRVFStyleRefs;
extern DELPHI_PACKAGE bool RichViewAllowCopyTableCells;
extern DELPHI_PACKAGE int RichViewMaxThumbnailCount;
#define cssBKAttStrFixed L"fixed"
#define cssBKAttStrScroll L"scroll"
#define cssBKRepStrRepeat L"repeat"
#define cssBKRepStrNoRepeat L"no-repeat"
extern DELPHI_PACKAGE System::StaticArray<char *, 9> rv_cssBkAttachment;
extern DELPHI_PACKAGE System::StaticArray<char *, 9> rv_cssBkRepeat;
extern DELPHI_PACKAGE void __fastcall RVCheckUni(int Length);
extern DELPHI_PACKAGE void __fastcall RV_RegisterHTMLGraphicFormat _DEPRECATED_ATTRIBUTE1("Use RVGraphicHandler.RegisterHTMLGraphicFormat") (Vcl::Graphics::TGraphicClass ClassType);
extern DELPHI_PACKAGE bool __fastcall RV_IsHTMLGraphicFormat _DEPRECATED_ATTRIBUTE1("Use RVGraphicHandler.IsHTMLGraphic") (Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE void __fastcall RV_RegisterPngGraphic _DEPRECATED_ATTRIBUTE1("Use RVGraphicHandler.RegisterPngGraphic") (Vcl::Graphics::TGraphicClass ClassType);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall StringToHTMLString(const System::UnicodeString s, Rvstyle::TRVSaveOptions Options, Rvstyle::TRVStyle* RVStyle);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall StringToHTMLString2(const System::UnicodeString s, Rvstyle::TRVSaveOptions Options, Rvstyle::TRVCodePage CodePage);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall StringToHTMLString3(const System::UnicodeString s, bool UTF8, Rvstyle::TRVCodePage CodePage);
extern DELPHI_PACKAGE void __fastcall RVSaveFontToRTF(System::Classes::TStream* Stream, Vcl::Graphics::TFont* Font, Rvfuncs::TCustomRVRTFTables* RTFTables, Rvstyle::TRVStyle* RVStyle);
extern DELPHI_PACKAGE int __fastcall RVCompareLocations(TCustomRVData* RVData1, int ItemNo1, TCustomRVData* RVData2, int ItemNo2);
}	/* namespace Crvdata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRVDATA)
using namespace Crvdata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrvdataHPP
