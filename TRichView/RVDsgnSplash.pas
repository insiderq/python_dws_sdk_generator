unit RVDsgnSplash;

interface

{$I RV_Defs.inc}

uses
  Graphics, ToolsAPI, DesignIntf, SysUtils, RVStr;

{$R RVDsgnSplash.res}

procedure Register;


implementation

{$IFDEF VER180}
  {$IFDEF VER185}
  const RVBMPNAME = 'RVBDSSPLASHW'; // Delphi 2007 - white
  {$ELSE}
  const RVBMPNAME = 'RVBDSSPLASHB'; // Delphi 2006 - black
  {$ENDIF}
{$ELSE}
  {$IFDEF VER200}
  const RVBMPNAME = 'RVBDSSPLASHW'; // Delphi 2009 - white
  {$ELSE}
  const RVBMPNAME = 'RVBDSSPLASHB'; // Delphi 2010, XE, XE2 - black
  {$ENDIF}
{$ENDIF}

procedure Register;
begin
  ForceDemandLoadState(dlDisable);
end;

procedure DisplayIcon;
var bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  bmp.LoadFromResourceName(hInstance, RVBMPNAME);
  SplashScreenServices.AddPluginBitmap('TRichView '+RVVersion,
    bmp.Handle, {$IFDEF RVDEBUG}True, 'Unregistered'{$ELSE}False, ''{$ENDIF},
    '');

  bmp.ReleaseHandle;
  bmp.Free;
end;

initialization

  DisplayIcon;
  Sleep(0);



end.

