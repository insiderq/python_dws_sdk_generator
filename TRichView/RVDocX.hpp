﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDocX.pas' rev: 27.00 (Windows)

#ifndef RvdocxHPP
#define RvdocxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVZip.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdocx
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVDocXPart : unsigned char { rvdocxpMain, rvdocxpNumbering, rvdocxpFootnotes, rvdocxpEndnotes, rvdocxpHeader, rvdocxpFooter, rvdocxpHeader1, rvdocxpFooter1, rvdocxpHeaderEven, rvdocxpFooterEven };

typedef System::Set<TRVDocXPart, TRVDocXPart::rvdocxpMain, TRVDocXPart::rvdocxpFooterEven> TRVDocXParts;

class DELPHICLASS TRVDocXPartsList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDocXPartsList : public Rvclasses::TRVIntegerList
{
	typedef Rvclasses::TRVIntegerList inherited;
	
public:
	TRVDocXParts operator[](int Index) { return Items[Index]; }
	
private:
	TRVDocXParts __fastcall GetItems(int Index);
	void __fastcall SetItems(int Index, const TRVDocXParts Value);
	
public:
	void __fastcall IncludePart(int Index, TRVDocXPart Part);
	__property TRVDocXParts Items[int Index] = {read=GetItems, write=SetItems/*, default*/};
public:
	/* TRVIntegerList.CreateEx */ inline __fastcall TRVDocXPartsList(int Count, int Value) : Rvclasses::TRVIntegerList(Count, Value) { }
	/* TRVIntegerList.CreateCopy */ inline __fastcall TRVDocXPartsList(Rvclasses::TRVIntegerList* Source) : Rvclasses::TRVIntegerList(Source) { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVDocXPartsList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVDocXPartsList(void) : Rvclasses::TRVIntegerList() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVDocXImageListPicType : unsigned char { rvdxilptBitmap, rvdxilptIcon, rvdxilptPng };

class DELPHICLASS TRVDocXImageListItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDocXImageListItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Vcl::Imglist::TCustomImageList* ImageList;
	TRVDocXImageListPicType PicsType;
	TRVDocXPartsList* SavedIndices;
	__fastcall TRVDocXImageListItem(Vcl::Imglist::TCustomImageList* AImageList);
	__fastcall virtual ~TRVDocXImageListItem(void);
};

#pragma pack(pop)

class DELPHICLASS TRVDocXRelationCatalog;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDocXRelationCatalog : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Classes::TStringList* FHyperlinks;
	System::Classes::TStringList* FImages;
	System::Classes::TStringList* __fastcall GetHyperlinks(void);
	System::Classes::TStringList* __fastcall GetImages(void);
	
public:
	__property System::Classes::TStringList* Hyperlinks = {read=GetHyperlinks};
	__property System::Classes::TStringList* Images = {read=GetImages};
	bool __fastcall IsEmpty(void);
	__fastcall virtual ~TRVDocXRelationCatalog(void);
public:
	/* TObject.Create */ inline __fastcall TRVDocXRelationCatalog(void) : System::TObject() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVDocXSavingData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDocXSavingData : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVDocXRelationCatalog* __fastcall GetCatalogs(TRVDocXPart Index);
	TRVDocXRelationCatalog* __fastcall GetCurCatalog(void);
	void __fastcall GetImageListImageInfo(Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex, TRVDocXImageListPicType PicType, int &ImageListIndex, bool &IsNew);
	
protected:
	Rvzip::TRVZipPacker* FZip;
	System::StaticArray<TRVDocXRelationCatalog*, 10> FCatalogs;
	Rvclasses::TRVList* FImageLists;
	
public:
	TRVDocXPart CurPart;
	bool HasNumPics;
	bool HasBitmaps;
	bool HasIcons;
	bool HasPng;
	bool CurInTextBox;
	int BookmarkIndex;
	int ImageIndex;
	int TotalImageIndex;
	int BreakIndex;
	int ShapeIndex;
	int NoteBookmarkIndex;
	Rvclasses::TRVIntegerList* ListOverrideOffsetsList1;
	Rvclasses::TRVIntegerList* ListOverrideOffsetsList2;
	int FootnoteIndex;
	int EndnoteIndex;
	Rvclasses::TRVMemoryStream* FootnotesStream;
	Rvclasses::TRVMemoryStream* EndnotesStream;
	Rvtypes::TRVAnsiString SectProps;
	bool CallProgress;
	void __fastcall AddGraphic(Vcl::Graphics::TGraphic* Gr, Rvtypes::TRVAnsiString FileName);
	void __fastcall AddImageListImage(Vcl::Imglist::TCustomImageList* ImageList, int ImageIndex, Rvtypes::TRVAnsiString &FileName, Rvtypes::TRVAnsiString &RelId);
	bool __fastcall HasImageListImagesFor(TRVDocXPart Part);
	__fastcall TRVDocXSavingData(Rvzip::TRVZipPacker* Zip);
	__fastcall virtual ~TRVDocXSavingData(void);
	__property TRVDocXRelationCatalog* Catalogs[TRVDocXPart Index] = {read=GetCatalogs};
	__property TRVDocXRelationCatalog* CurCatalog = {read=GetCurCatalog};
	__property Rvclasses::TRVList* ImageLists = {read=FImageLists};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvdocx */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDOCX)
using namespace Rvdocx;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdocxHPP
