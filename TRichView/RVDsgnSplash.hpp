﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDsgnSplash.pas' rev: 27.00 (Windows)

#ifndef RvdsgnsplashHPP
#define RvdsgnsplashHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <ToolsAPI.hpp>	// Pascal unit
#include <DesignIntf.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdsgnsplash
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Rvdsgnsplash */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDSGNSPLASH)
using namespace Rvdsgnsplash;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdsgnsplashHPP
