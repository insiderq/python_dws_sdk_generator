{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       TRVBoxPosition - class defining position of     }
{       a floating box (used in TRVSidenoteItemInfo)    }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVFloatingPos;

{$I RV_Defs.inc}

interface

{$IFNDEF RVDONOTUSESEQ}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  Classes, SysUtils, Graphics, RVTypes, RVStyle, RVItem;

const
  rveipcFloatHorizontalAnchor       = 500;
  rveipcFloatHorizontalPositionKind = 501;
  rveipcFloatHorizontalOffset       = 502;
  rveipcFloatHorizontalAlign        = 503;
  rveipcFloatVerticalAnchor         = 504;
  rveipcFloatVerticalPositionKind   = 505;
  rveipcFloatVerticalOffset         = 506;
  rveipcFloatVerticalAlign          = 507;
  rveipcFloatRelativeToCell         = 508;
  rveipcFloatPositionInText         = 509;

type
  TRVHorizontalAnchor = (rvhanCharacter, rvhanPage, rvhanMainTextArea,
    rvhanLeftMargin, rvhanRightMargin, rvhanInnerMargin, rvhanOuterMargin);
  TRVVerticalAnchor = (rvvanLine, rvvanParagraph, rvvanPage, rvvanMainTextArea,
    rvvanTopMargin, rvvanBottomMargin);
  TRVFloatPositionKind = (rvfpkAlignment, rvfpkAbsPosition, rvfpkPercentPosition);

  TRVFloatPositionInText = (rvpitAboveText, rvpitBelowText);

  {
  Meaning of TRVFloatPosition values depend on the corresponding position kind:
  rvfpkAlignment:
    value is ignored
  rvfpkAbsPosition:
    value is like TRVStyleLength; it depends in RVStyle.Units - either pixels or twips
  rvfpkPercentPosition:
    value is in 1/1000 of percent; for example, 50000 = 50%
  }
  TRVFloatPosition = type Integer;

  {
  Alignments are used only if the corrsponding position kind is rvfpkAlignment
  }
  TRVFloatHorizontalAlignment = (rvfphalLeft, rvfphalCenter, rvfphalRight);
  TRVFloatVerticalAlignment = (rvfpvalTop, rvfpvalCenter, rvfpvalBottom);

  {
   Horizontal position of the floating box.
     The position is calculated relative to the area on the page
     specified in HorizontalAnchor. Let call it "anchor area".
     There are 3 positioning kinds are possible (defined in HorizontalPositionKind):
     rvfpkAlignment:
       a side of the floating box is positioned at the side of the anchor area;
       this side is defined in HorizontalAlignment (left/center/right)
     rvfpkAbsPosition
       a left side of the floating box is positioned at the left side of
       the anchor area + offset defined in HorizontalOffset;
       this offset is defined in pixels or twips, depending on RVStyle.Units
     rvfpkPercentPosition
       a left side of the floating box is positioned at the left side of
       the anchor area, shifted by the specified percent of the floating area length;
       this shift is defined in HorizontalOffset (as 1/1000 of percent)
       for example:
       0%   -> left side of the floating box = left side of the anchor area
       50%  -> left side of the floating box = center of the anchor area
       100% -> left side of the floating box = right side of the anchor area
       -50% -> left side of the floating box is shifted from the left side
         of the anchor area to the left by 1/2 of the anchor area length

   Vertical position of the floating box:
     The same using VerticalAnchor, VerticalPositionKind, VerticalOffset,
     VerticalAlignment properties
  }

  TRVBoxPosition = class (TPersistent)
    private
      FHorizontalAnchor: TRVHorizontalAnchor;
      FVerticalAnchor  : TRVVerticalAnchor;
      FHorizontalPositionKind, FVerticalPositionKind: TRVFloatPositionKind;
      FHorizontalOffset, FVerticalOffset: TRVFloatPosition;
      FHorizontalAlignment: TRVFloatHorizontalAlignment;
      FVerticalAlignment  : TRVFloatVerticalAlignment;
      FPositionInText : TRVFloatPositionInText;
      FRelativeToCell: Boolean;
      function GetPropertyIdentifier(const PropName: TRVAnsiString): Integer;
    public
      constructor Create;
      procedure Assign(Source: TPersistent); override;
      procedure ConvertToDifferentUnits(NewUnits: TRVStyleUnits; RVStyle: TRVStyle);
      function SetExtraIntPropertyEx(Prop: Integer; Value: Integer): Boolean;
      function GetExtraIntPropertyEx(Prop: Integer; var Value: Integer): Boolean;
      function SetExtraCustomProperty(const PropName: TRVAnsiString; const Value: String): Boolean;
      function GetExtraIntPropertyReformat(Prop: Integer; var ReformatType: TRVReformatType): Boolean;
      function GetChangedPropertyCount: Integer;
      procedure SaveRVFExtraProperties(Stream: TStream);
      property HorizontalAnchor: TRVHorizontalAnchor read FHorizontalAnchor write FHorizontalAnchor default rvhanCharacter;
      property VerticalAnchor: TRVVerticalAnchor read FVerticalAnchor write FVerticalAnchor default rvvanLine;
      property HorizontalPositionKind: TRVFloatPositionKind read FHorizontalPositionKind write FHorizontalPositionKind default rvfpkAbsPosition;
      property VerticalPositionKind: TRVFloatPositionKind read FVerticalPositionKind write FVerticalPositionKind default rvfpkAbsPosition;
      property HorizontalOffset: TRVFloatPosition read FHorizontalOffset write FHorizontalOffset default 0;
      property VerticalOffset: TRVFloatPosition read FVerticalOffset write FVerticalOffset default 0;
      property HorizontalAlignment: TRVFloatHorizontalAlignment read FHorizontalAlignment write FHorizontalAlignment default rvfphalLeft;
      property VerticalAlignment: TRVFloatVerticalAlignment read FVerticalAlignment write FVerticalAlignment default rvfpvalTop;
      property PositionInText: TRVFloatPositionInText read FPositionInText write FPositionInText default rvpitAboveText;
      property RelativeToCell: Boolean read FRelativeToCell write FRelativeToCell default True;
  end;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSESEQ}

const
  rveipcFloat_First = rveipcFloatHorizontalAnchor;
  rveipcFloat_Last  = rveipcFloatPositionInText;


const PropertyNames: array[rveipcFloat_First..rveipcFloat_Last] of PRVAnsiChar =
  ('hanchor', 'hposkind', 'hoffs', 'halign',
   'vanchor', 'vposkind', 'voffs', 'valign', 'reltocell', 'posintext');
const PropertyPrefix = 'box';

function GetFloatPropName(Prop: Integer): TRVAnsiString;
begin
  Result := TRVAnsiString(PropertyPrefix)+PropertyNames[Prop];
end;

{=========================== TRVBoxPosition ===================================}
constructor TRVBoxPosition.Create;
begin
  inherited Create;
  HorizontalPositionKind := rvfpkAbsPosition;
  VerticalPositionKind   := rvfpkAbsPosition;
  RelativeToCell         := True;
end;
{------------------------------------------------------------------------------}
procedure TRVBoxPosition.Assign(Source: TPersistent);
begin
  if Source is TRVBoxPosition then begin
    HorizontalAnchor := TRVBoxPosition(Source).HorizontalAnchor;
    VerticalAnchor   := TRVBoxPosition(Source).VerticalAnchor;
    HorizontalPositionKind := TRVBoxPosition(Source).HorizontalPositionKind;
    VerticalPositionKind   := TRVBoxPosition(Source).VerticalPositionKind;
    HorizontalOffset       := TRVBoxPosition(Source).HorizontalOffset;
    VerticalOffset         := TRVBoxPosition(Source).VerticalOffset;
    HorizontalAlignment    := TRVBoxPosition(Source).HorizontalAlignment;
    VerticalAlignment      := TRVBoxPosition(Source).VerticalAlignment;
    PositionInText         := TRVBoxPosition(Source).PositionInText;
    RelativeToCell         := TRVBoxPosition(Source).RelativeToCell;
    end
  else
    inherited Assign(Source);
end;
{------------------------------------------------------------------------------}
procedure TRVBoxPosition.ConvertToDifferentUnits(NewUnits: TRVStyleUnits;
  RVStyle: TRVStyle);
begin
  if VerticalPositionKind=rvfpkAbsPosition then
    VerticalOffset := RVStyle.GetAsDifferentUnits(VerticalOffset, NewUnits);
  if HorizontalPositionKind=rvfpkAbsPosition then
    HorizontalOffset := RVStyle.GetAsDifferentUnits(HorizontalOffset, NewUnits);
end;
{------------------------------------------------------------------------------}
function TRVBoxPosition.GetExtraIntPropertyEx(Prop: Integer;
  var Value: Integer): Boolean;
begin
  Result := (Prop>=rveipcFloat_First) and (Prop<=rveipcFloat_Last);
  if not Result then
    exit;
  case Prop of
    rveipcFloatHorizontalAnchor:
      Value := Ord(HorizontalAnchor);
    rveipcFloatHorizontalPositionKind:
      Value := Ord(HorizontalPositionKind);
    rveipcFloatHorizontalOffset:
      Value := HorizontalOffset;
    rveipcFloatHorizontalAlign:
      Value := Ord(HorizontalAlignment);
    rveipcFloatVerticalAnchor:
      Value := Ord(VerticalAnchor);
    rveipcFloatVerticalPositionKind:
      Value := Ord(VerticalPositionKind);
    rveipcFloatVerticalOffset:
      Value := VerticalOffset;
    rveipcFloatVerticalAlign:
      Value := Ord(VerticalAlignment);
    rveipcFloatPositionInText:
      Value := Ord(PositionInText);
    rveipcFloatRelativeToCell:
      Value := Ord(RelativeToCell);
  end;
end;
{------------------------------------------------------------------------------}
function TRVBoxPosition.GetExtraIntPropertyReformat(Prop: Integer;
  var ReformatType: TRVReformatType): Boolean;
begin
  case Prop of
    rveipcFloat_First..rveipcFloat_Last:
      begin
        ReformatType := rvrfSRVReformat;
        Result := True;
      end;
    else
      Result := False;
  end;
end;
{------------------------------------------------------------------------------}
function TRVBoxPosition.SetExtraIntPropertyEx(Prop, Value: Integer): Boolean;
begin
  Result := (Prop>=rveipcFloat_First) and (Prop<=rveipcFloat_Last);
  if not Result then
    exit;
  case Prop of
    rveipcFloatHorizontalAnchor:
      HorizontalAnchor := TRVHorizontalAnchor(Value);
    rveipcFloatHorizontalPositionKind:
      HorizontalPositionKind := TRVFloatPositionKind(Value);
    rveipcFloatHorizontalOffset:
      HorizontalOffset := Value;
    rveipcFloatHorizontalAlign:
      HorizontalAlignment := TRVFloatHorizontalAlignment(Value);
    rveipcFloatVerticalAnchor:
      VerticalAnchor := TRVVerticalAnchor(Value);
    rveipcFloatVerticalPositionKind:
      VerticalPositionKind := TRVFloatPositionKind(Value);
    rveipcFloatVerticalOffset:
      VerticalOffset := Value;
    rveipcFloatVerticalAlign:
      VerticalAlignment := TRVFloatVerticalAlignment(Value);
    rveipcFloatPositionInText:
      PositionInText := TRVFloatPositionInText(Value);
    rveipcFloatRelativeToCell:
      RelativeToCell := Value<>0;
  end;
end;
{------------------------------------------------------------------------------}
function TRVBoxPosition.GetPropertyIdentifier(const PropName: TRVAnsiString): Integer;
var i: Integer;
begin
  for i := rveipcFloat_First to rveipcFloat_Last do
    if PropName=GetFloatPropName(i) then begin
      Result := i;
      exit;
    end;
  Result := -1;
end;
{------------------------------------------------------------------------------}
function TRVBoxPosition.SetExtraCustomProperty(const PropName: TRVAnsiString;
  const Value: String): Boolean;
var Prop: Integer;
begin
  Prop := GetPropertyIdentifier(PropName);
  Result := Prop>=0;
  if Result then
    SetExtraIntPropertyEx(Prop, StrToInt(Value));
end;
{------------------------------------------------------------------------------}
function TRVBoxPosition.GetChangedPropertyCount: Integer;
begin
  Result := Ord(HorizontalAnchor<>rvhanCharacter)+
    Ord(HorizontalPositionKind<>rvfpkAbsPosition)+
    Ord(HorizontalAlignment<>rvfphalLeft)+
    Ord(HorizontalOffset<>0)+
    Ord(VerticalAnchor<>rvvanLine)+
    Ord(VerticalPositionKind<>rvfpkAbsPosition)+
    Ord(VerticalAlignment<>rvfpvalTop)+
    Ord(VerticalOffset<>0)+
    Ord(PositionInText<>rvpitAboveText)+
    Ord(not RelativeToCell);
end;
{------------------------------------------------------------------------------}
procedure TRVBoxPosition.SaveRVFExtraProperties(Stream: TStream);

  procedure SaveProp(Prop: Integer; Value, DefValue: Integer);
  begin
    if Value<>DefValue then
      WriteRVFExtraCustomPropertyInt(Stream, GetFloatPropName(Prop), Value);
  end;

begin
  SaveProp(rveipcFloatHorizontalAnchor, ord(HorizontalAnchor), ord(rvhanCharacter));
  SaveProp(rveipcFloatHorizontalPositionKind, Ord(HorizontalPositionKind), ord(rvfpkAbsPosition));
  SaveProp(rveipcFloatHorizontalAlign, Ord(HorizontalAlignment), ord(rvfphalLeft));
  SaveProp(rveipcFloatHorizontalOffset, Ord(HorizontalOffset), 0);
  SaveProp(rveipcFloatVerticalAnchor, ord(VerticalAnchor), ord(rvvanLine));
  SaveProp(rveipcFloatVerticalPositionKind, Ord(VerticalPositionKind), ord(rvfpkAbsPosition));
  SaveProp(rveipcFloatVerticalAlign, Ord(VerticalAlignment), ord(rvfpvalTop));
  SaveProp(rveipcFloatVerticalOffset, Ord(VerticalOffset), 0);
  SaveProp(rveipcFloatPositionInText, Ord(PositionInText), ord(rvpitAboveText));
  SaveProp(rveipcFloatRelativeToCell, Ord(RelativeToCell), ord(True));
end;


{$ENDIF}

end.
