﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVWordPaint.pas' rev: 27.00 (Windows)

#ifndef RvwordpaintHPP
#define RvwordpaintHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvwordpaint
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVWordPainter;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVWordPainter : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int StartOffs;
	int Length;
	virtual void __fastcall Draw(Vcl::Graphics::TCanvas* Canvas, Dlines::TRVDrawLineInfo* ditem, System::Classes::TPersistent* RVData, const System::Types::TRect &r, int Index) = 0 ;
	__fastcall TRVWordPainter(int AStartOffs, int ALength);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVWordPainter(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVWordPainterList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVWordPainterList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVWordPainter* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVWordPainter* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVWordPainter* const Value);
	
public:
	__property TRVWordPainter* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVWordPainterList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVWordPainterList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVWordMisspellPainter;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVWordMisspellPainter : public TRVWordPainter
{
	typedef TRVWordPainter inherited;
	
public:
	virtual void __fastcall Draw(Vcl::Graphics::TCanvas* Canvas, Dlines::TRVDrawLineInfo* ditem, System::Classes::TPersistent* RVData, const System::Types::TRect &r, int Index);
public:
	/* TRVWordPainter.Create */ inline __fastcall TRVWordMisspellPainter(int AStartOffs, int ALength) : TRVWordPainter(AStartOffs, ALength) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVWordMisspellPainter(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvwordpaint */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVWORDPAINT)
using namespace Rvwordpaint;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvwordpaintHPP
