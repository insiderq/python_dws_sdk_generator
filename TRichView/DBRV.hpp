﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'DBRV.pas' rev: 27.00 (Windows)

#ifndef DbrvHPP
#define DbrvHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <Data.DB.hpp>	// Pascal unit
#include <Vcl.DBCtrls.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVRTFProps.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVPopup.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dbrv
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVDBFieldFormat : unsigned char { rvdbRVF, rvdbRTF, rvdbText };

typedef void __fastcall (__closure *TRVCustomFormatEvent)(Richview::TCustomRichView* Sender, System::Classes::TStream* Stream, bool &DoDefault);

class DELPHICLASS TDBRichView;
class PASCALIMPLEMENTATION TDBRichView : public Richview::TCustomRichView
{
	typedef Richview::TCustomRichView inherited;
	
private:
	Vcl::Dbctrls::TFieldDataLink* FDataLink;
	bool FAutoDisplay;
	bool FFocused;
	bool FMemoLoaded;
	System::Classes::TNotifyEvent FOnNewDocument;
	System::Classes::TNotifyEvent FOnLoadDocument;
	TRVCustomFormatEvent FOnLoadCustomFormat;
	void __fastcall DataChange(System::TObject* Sender);
	System::UnicodeString __fastcall GetDataField(void);
	Data::Db::TDataSource* __fastcall GetDataSource(void);
	Data::Db::TField* __fastcall GetField(void);
	void __fastcall SetDataField(const System::UnicodeString Value);
	void __fastcall SetDataSource(Data::Db::TDataSource* Value);
	MESSAGE void __fastcall CMGetDataLink(Winapi::Messages::TMessage &Message);
	void __fastcall SetAutoDisplay(bool Value);
	bool __fastcall IsBackgroundImageStored(void);
	
protected:
	virtual void __fastcall Loaded(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC void __fastcall DblClick(void);
	virtual void __fastcall Paint(void);
	
public:
	__fastcall virtual TDBRichView(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TDBRichView(void);
	void __fastcall LoadField(void);
	__property Data::Db::TField* Field = {read=GetField};
	DYNAMIC bool __fastcall ExecuteAction(System::Classes::TBasicAction* Action);
	virtual bool __fastcall UpdateAction(System::Classes::TBasicAction* Action);
	
__published:
	__property System::UnicodeString DataField = {read=GetDataField, write=SetDataField};
	__property Data::Db::TDataSource* DataSource = {read=GetDataSource, write=SetDataSource};
	__property bool AutoDisplay = {read=FAutoDisplay, write=SetAutoDisplay, default=1};
	__property System::Classes::TNotifyEvent OnLoadDocument = {read=FOnLoadDocument, write=FOnLoadDocument};
	__property System::Classes::TNotifyEvent OnNewDocument = {read=FOnNewDocument, write=FOnNewDocument};
	__property TRVCustomFormatEvent OnLoadCustomFormat = {read=FOnLoadCustomFormat, write=FOnLoadCustomFormat};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Constraints;
	__property Color = {default=536870911};
	__property Ctl3D;
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property HelpContext = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Touch;
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGesture;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseMove;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnResize;
	__property OnStartDrag;
	__property AnimationMode = {default=1};
	__property BackgroundBitmap = {stored=IsBackgroundImageStored};
	__property BackgroundStyle = {default=0};
	__property BiDiMode = {default=0};
	__property BorderStyle = {default=1};
	__property BottomMargin = {default=5};
	__property CPEventKind = {default=0};
	__property Cursor = {default=0};
	__property Delimiters = {default=0};
	__property DocParameters;
	__property DoInPaletteMode;
	__property FirstJumpNo = {default=0};
	__property HScrollVisible = {default=1};
	__property LeftMargin = {default=5};
	__property MaxLength = {default=0};
	__property MaxTextWidth = {default=0};
	__property MinTextWidth = {default=0};
	__property Options = {default=1702949};
	__property RightMargin = {default=5};
	__property RTFOptions = {default=30};
	__property RTFReadProperties;
	__property RVFOptions = {default=98435};
	__property RVFParaStylesReadMode = {default=2};
	__property RVFTextStylesReadMode = {default=2};
	__property Style;
	__property TabNavigation = {default=1};
	__property TopMargin = {default=5};
	__property Tracking = {default=1};
	__property UseStyleTemplates = {default=0};
	__property StyleTemplateInsertMode = {default=0};
	__property UseXPThemes = {default=1};
	__property VAlign = {default=0};
	__property VScrollVisible = {default=1};
	__property WheelStep = {default=2};
	__property WordWrap = {default=1};
	__property OnAddStyle;
	__property OnCheckpointVisible;
	__property OnControlAction;
	__property OnCopy;
	__property OnGetItemCursor;
	__property OnImportPicture;
	__property OnItemAction;
	__property OnItemHint;
	__property OnJump;
	__property OnHScrolled;
	__property OnHTMLSaveImage;
	__property OnPaint;
	__property OnProgress;
	__property OnReadHyperlink;
	__property OnRVDblClick;
	__property OnRVFImageListNeeded;
	__property OnRVFControlNeeded;
	__property OnRVFPictureNeeded;
	__property OnRVMouseDown;
	__property OnRVMouseMove;
	__property OnRVMouseUp;
	__property OnRVRightClick;
	__property OnSaveComponentToFile;
	__property OnSaveDocXExtra;
	__property OnSaveHTMLExtra;
	__property OnSaveImage2;
	__property OnSaveItemToFile;
	__property OnSaveRTFExtra;
	__property OnSelect;
	__property OnSpellingCheck;
	__property OnStyleTemplatesChange;
	__property OnVScrolled;
	__property OnWriteHyperlink;
	__property AllowSelection;
	__property SingleClick;
	__property OnURLNeeded;
public:
	/* TWinControl.CreateParented */ inline __fastcall TDBRichView(HWND ParentWindow) : Richview::TCustomRichView(ParentWindow) { }
	
};


class DELPHICLASS TDBRichViewEdit;
class PASCALIMPLEMENTATION TDBRichViewEdit : public Rvedit::TCustomRichViewEdit
{
	typedef Rvedit::TCustomRichViewEdit inherited;
	
private:
	Vcl::Dbctrls::TFieldDataLink* FDataLink;
	bool FAutoDisplay;
	bool FFocused;
	bool FMemoLoaded;
	Rvclasses::TRVMemoryStream* FDataSaveStream;
	TRVDBFieldFormat FFieldFormat;
	bool FAutoDeleteUnusedStyles;
	System::Classes::TNotifyEvent FOnNewDocument;
	bool FIgnoreEscape;
	System::Classes::TNotifyEvent FOnLoadDocument;
	TRVCustomFormatEvent FOnLoadCustomFormat;
	TRVCustomFormatEvent FOnSaveCustomFormat;
	void __fastcall DataChange(System::TObject* Sender);
	void __fastcall EditingChange(System::TObject* Sender);
	System::UnicodeString __fastcall GetDataField(void);
	Data::Db::TDataSource* __fastcall GetDataSource(void);
	Data::Db::TField* __fastcall GetField(void);
	bool __fastcall DBGetReadOnly(void);
	void __fastcall SetDataField(const System::UnicodeString Value);
	void __fastcall SetDataSource(Data::Db::TDataSource* Value);
	void __fastcall SetFocused(bool Value);
	void __fastcall DBSetReadOnly(bool Value);
	void __fastcall SetAutoDisplay(bool Value);
	void __fastcall UpdateData(System::TObject* Sender);
	HIDESBASE MESSAGE void __fastcall CMEnter(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall CMExit(Winapi::Messages::TWMNoParams &Message);
	MESSAGE void __fastcall CMGetDataLink(Winapi::Messages::TMessage &Message);
	void __fastcall BeginEditing(void);
	void __fastcall DoLoadField(bool Check);
	MESSAGE void __fastcall WMReload(Winapi::Messages::TMessage &Msg);
	HIDESBASE MESSAGE void __fastcall CMWantSpecialKey(Winapi::Messages::TWMKey &Message);
	bool __fastcall IsBackgroundImageStored(void);
	
protected:
	virtual void __fastcall Loaded(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	DYNAMIC void __fastcall DblClick(void);
	DYNAMIC void __fastcall KeyPress(System::WideChar &Key);
	virtual void __fastcall Paint(void);
	DYNAMIC void __fastcall DoFormat(void);
	
public:
	DYNAMIC void __fastcall DoChange(bool ClearRedo);
	virtual bool __fastcall BeforeChange(bool FromOutside);
	__fastcall virtual TDBRichViewEdit(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TDBRichViewEdit(void);
	void __fastcall LoadField(void);
	__property Data::Db::TField* Field = {read=GetField};
	DYNAMIC bool __fastcall ExecuteAction(System::Classes::TBasicAction* Action);
	virtual bool __fastcall UpdateAction(System::Classes::TBasicAction* Action);
	
__published:
	__property bool IgnoreEscape = {read=FIgnoreEscape, write=FIgnoreEscape, default=0};
	__property bool AutoDeleteUnusedStyles = {read=FAutoDeleteUnusedStyles, write=FAutoDeleteUnusedStyles, default=0};
	__property System::UnicodeString DataField = {read=GetDataField, write=SetDataField};
	__property Data::Db::TDataSource* DataSource = {read=GetDataSource, write=SetDataSource};
	__property bool ReadOnly = {read=DBGetReadOnly, write=DBSetReadOnly, nodefault};
	__property bool AutoDisplay = {read=FAutoDisplay, write=SetAutoDisplay, default=1};
	__property TRVDBFieldFormat FieldFormat = {read=FFieldFormat, write=FFieldFormat, default=0};
	__property System::Classes::TNotifyEvent OnLoadDocument = {read=FOnLoadDocument, write=FOnLoadDocument};
	__property System::Classes::TNotifyEvent OnNewDocument = {read=FOnNewDocument, write=FOnNewDocument};
	__property TRVCustomFormatEvent OnLoadCustomFormat = {read=FOnLoadCustomFormat, write=FOnLoadCustomFormat};
	__property TRVCustomFormatEvent OnSaveCustomFormat = {read=FOnSaveCustomFormat, write=FOnSaveCustomFormat};
	__property AcceptDragDropFormats = {default=191};
	__property AcceptPasteFormats = {default=191};
	__property CustomCaretInterval = {default=0};
	__property DefaultPictureVAlign = {default=0};
	__property EditorOptions = {default=18};
	__property UndoLimit = {default=-1};
	__property OnCaretGetOut;
	__property OnCaretMove;
	__property OnChange;
	__property OnChanging;
	__property OnCheckStickingItems;
	__property OnCurParaStyleChanged;
	__property OnCurTextStyleChanged;
	__property OnDrawCustomCaret;
	__property OnMeasureCustomCaret;
	__property OnDropFiles;
	__property OnItemResize;
	__property OnItemTextEdit;
	__property OnOleDragEnter;
	__property OnOleDragLeave;
	__property OnOleDragOver;
	__property OnOleDrop;
	__property OnParaStyleConversion;
	__property OnPaste;
	__property OnStyleConversion;
	__property TabNavigation = {default=0};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Constraints;
	__property Color = {default=536870911};
	__property Ctl3D;
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property HelpContext = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Touch;
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGesture;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseMove;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnResize;
	__property OnStartDrag;
	__property AnimationMode = {default=1};
	__property BackgroundBitmap = {stored=IsBackgroundImageStored};
	__property BackgroundStyle = {default=0};
	__property BiDiMode = {default=0};
	__property BorderStyle = {default=1};
	__property BottomMargin = {default=5};
	__property Cursor = {default=-4};
	__property Delimiters = {default=0};
	__property DocParameters;
	__property DoInPaletteMode;
	__property FirstJumpNo = {default=0};
	__property HScrollVisible = {default=1};
	__property LeftMargin = {default=5};
	__property OnSmartPopupClick;
	__property LiveSpellingMode = {default=1};
	__property MaxLength = {default=0};
	__property MaxTextWidth = {default=0};
	__property MinTextWidth = {default=0};
	__property Options = {default=1702949};
	__property RightMargin = {default=5};
	__property RTFOptions = {default=30};
	__property RTFReadProperties;
	__property RVFOptions = {default=98435};
	__property RVFParaStylesReadMode = {default=2};
	__property RVFTextStylesReadMode = {default=2};
	__property SmartPopupProperties;
	__property Style;
	__property TopMargin = {default=5};
	__property Tracking = {default=1};
	__property UseStyleTemplates = {default=0};
	__property StyleTemplateInsertMode = {default=0};
	__property UseXPThemes = {default=1};
	__property VAlign = {default=0};
	__property VScrollVisible = {default=1};
	__property WheelStep = {default=2};
	__property WordWrap = {default=1};
	__property OnAddStyle;
	__property OnControlAction;
	__property OnCopy;
	__property OnGetItemCursor;
	__property OnImportPicture;
	__property OnItemAction;
	__property OnItemHint;
	__property OnJump;
	__property OnHScrolled;
	__property OnHTMLSaveImage;
	__property OnPaint;
	__property OnProgress;
	__property OnReadHyperlink;
	__property OnRVDblClick;
	__property OnRVFImageListNeeded;
	__property OnRVFControlNeeded;
	__property OnRVFPictureNeeded;
	__property OnRVMouseDown;
	__property OnRVMouseMove;
	__property OnRVMouseUp;
	__property OnRVRightClick;
	__property OnSaveComponentToFile;
	__property OnSaveDocXExtra;
	__property OnSaveHTMLExtra;
	__property OnSaveImage2;
	__property OnSaveItemToFile;
	__property OnSaveRTFExtra;
	__property OnSelect;
	__property OnSpellingCheck;
	__property OnStyleTemplatesChange;
	__property OnVScrolled;
	__property OnWriteHyperlink;
	__property AllowSelection;
	__property SingleClick;
	__property OnURLNeeded;
public:
	/* TWinControl.CreateParented */ inline __fastcall TDBRichViewEdit(HWND ParentWindow) : Rvedit::TCustomRichViewEdit(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Dbrv */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DBRV)
using namespace Dbrv;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DbrvHPP
