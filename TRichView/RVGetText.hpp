﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVGetText.pas' rev: 27.00 (Windows)

#ifndef RvgettextHPP
#define RvgettextHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVERVData.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvgettext
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetCurrentLineText(Rvedit::TCustomRichViewEdit* rve);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetVisibleText(Richview::TCustomRichView* rv);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetRVDataText(Crvdata::TCustomRVData* RVData);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetAllText(Richview::TCustomRichView* rv);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetCurrentParaSectionText(Rvedit::TCustomRichViewEdit* rve);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetCurrentParaText(Rvedit::TCustomRichViewEdit* rve);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetCurrentChar(Rvedit::TCustomRichViewEdit* rve);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall GetCurrentWord(Rvedit::TCustomRichViewEdit* rve);
}	/* namespace Rvgettext */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVGETTEXT)
using namespace Rvgettext;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvgettextHPP
