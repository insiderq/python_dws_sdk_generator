﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVERVData.pas' rev: 27.00 (Windows)

#ifndef RvervdataHPP
#define RvervdataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVUndo.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVRTFProps.hpp>	// Pascal unit
#include <RVResize.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <RVDragDrop.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvervdata
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVCharPos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVCharPos : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
public:
	int X;
	int DrawItemNo;
	int Offset;
	int MoveRightTo;
	__fastcall virtual TRVCharPos(System::Classes::TCollection* Owner);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TRVCharPos(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVCharPosCollection;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVCharPosCollection : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
private:
	HIDESBASE TRVCharPos* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TRVCharPos* const Value);
	
public:
	__property TRVCharPos* Items[int Index] = {read=GetItem, write=SetItem};
public:
	/* TCollection.Create */ inline __fastcall TRVCharPosCollection(System::Classes::TCollectionItemClass ItemClass) : System::Classes::TCollection(ItemClass) { }
	/* TCollection.Destroy */ inline __fastcall virtual ~TRVCharPosCollection(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVParaListOperation : unsigned char { rvplopChange, rvplopRemove, rvplopLevel };

enum DECLSPEC_DENUM TRVUndoDest : unsigned char { udNone, udUndo, udRedo };

class DELPHICLASS TRVEditRVData;
class PASCALIMPLEMENTATION TRVEditRVData : public Rvrvdata::TRichViewRVData
{
	typedef Rvrvdata::TRichViewRVData inherited;
	
private:
	int FPrevTextStyleNo;
	Rvstyle::TRVTag FCurTag;
	int FCurTagStyleNo;
	Rvresize::TRVItemResizer* FResizer;
	Vcl::Controls::TControl* ResizingControl;
	bool FCustomCaretActive;
	System::Types::TRect FCustomCaretRect;
	System::Types::TPoint FCustomCaretOldPos;
	Vcl::Graphics::TBitmap* FCustomCaretBack;
	int FUpDownMovingX;
	bool __fastcall InsSomething(Rvitem::TCustomRVItemInfo* &info, Rvtypes::TRVRawByteString &s, Rvtypes::TRVRawByteString &SplitText, bool AutoTag, bool CallTEEvent, int &InsertPoint, int &ItemsAdded, bool &FullReformat, int &NewListNo, bool NoNewLineReset);
	Rvitem::TCustomRVItemInfo* __fastcall InsEmptyString(int Index, const Rvstyle::TRVTag Tag, int ParaStyle, int FontStyle, bool SameAsPrev, bool BR);
	Rvitem::TCustomRVItemInfo* __fastcall InsString(Rvtypes::TRVRawByteString s, int Index, const Rvstyle::TRVTag Tag, int ParaStyle, int FontStyle, bool SameAsPrev, bool BR, bool &FullReformat);
	Rvitem::TCustomRVItemInfo* __fastcall InsString2(Rvtypes::TRVRawByteString s, int Index, const Rvstyle::TRVTag Tag, Rvitem::TCustomRVItemInfo* Source, bool SameAsPrev, bool BR, bool &FullReformat);
	Rvitem::TCustomRVItemInfo* __fastcall CreateTextItem(const Rvstyle::TRVTag Tag, int ParaStyle, int FontStyle, bool SameAsPrev, bool BR);
	Rvitem::TCustomRVItemInfo* __fastcall CreateTextItem2(const Rvstyle::TRVTag Tag, Rvitem::TCustomRVItemInfo* Source, bool SameAsPrev, bool BR);
	void __fastcall InsertString(Rvtypes::TRVRawByteString &s, Rvtypes::TRVRawByteString &SplitText, int StyleNo, bool AutoTag, bool CaretBefore);
	void __fastcall DoResizeControl(int ItemNo, int OldWidth, int OldHeight, int NewWidth, int NewHeight);
	int __fastcall GetLastParaItemEx(int ItemNo);
	bool __fastcall SetParaStyle(int StartItemNo, int EndItemNo, int ParaNo, bool &FullReformat);
	void __fastcall AfterAddingText(const Rvtypes::TRVRawByteString SplitText, int StartItemNo, int EndItemNo, int ItemsAdded, int DIStartNo, int DIEndNo, bool FullReformat, bool CaretBefore);
	bool __fastcall ItemHasPersistentCheckpoint(int ItemNo);
	bool __fastcall ParaHasPersistentCheckpoint(int ItemNo);
	bool __fastcall MovePersistentCheckpoint(int ItemNo, bool OnlyToPrev);
	void __fastcall ClearCurTag(void);
	bool __fastcall ShouldCopyCurTag(int StyleNo1, int StyleNo2);
	int __fastcall GetOneSelectedItemNo(void);
	bool __fastcall InsertItemFromTextFile(Rvtypes::TRVRawByteString &s, Rvtypes::TRVRawByteString &SplitText, Rvitem::TCustomRVItemInfo* item, bool AutoTag, bool BR, int &FirstIP, int &InsertPoint, int &ItemsAdded, int &MarkerItemNo, bool &FirstItem, bool &PageBreak, bool &FromNewLine, bool &FullReformat);
	bool __fastcall DoItemTextEdit(const Rvtypes::TRVRawByteString OldText, int ItemNo);
	bool __fastcall DoItemTextEditEx(const Rvtypes::TRVRawByteString OldText, int ItemNo, Rvedit::TRVUndoType UndoType);
	
protected:
	DYNAMIC void __fastcall Formatted(int FirstItemNo, int LastItemNo, bool Partial);
	DYNAMIC void __fastcall DeselectPartiallySelectedItem(Rvitem::TCustomRVItemInfo* NewPartiallySelected);
	virtual void __fastcall SetPartialSelectedItem(Rvitem::TCustomRVItemInfo* Item);
	DYNAMIC void __fastcall AfterDeleteStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	bool __fastcall ReplicateMarker(int ReferenceItemNo, int InsertItemNo, bool &FullReformat, bool EditFlag, bool OnlyIfNotHidden);
	void __fastcall AdjustMarkerCaret(bool Right, int &Offs);
	void __fastcall AdjustMarkerPos(int &ItemNo, int &Offs, bool DefRight);
	int __fastcall GetOffsInItem(int CaretOffs);
	bool __fastcall CaretAtTheBeginningOfParaSectionEx(bool OnlyAllowBeginningOfPara);
	bool __fastcall CaretAtTheEndOfParaSectionEx(bool OnlyAllowEndOfPara);
	bool __fastcall CaretInTheLastLine(void);
	bool __fastcall CaretAtTheBeginningOfLine(void);
	bool __fastcall CaretAtTheEndOfLine(void);
	virtual void __fastcall PostPaintTo(Vcl::Graphics::TCanvas* Canvas, int XShift, int YShift, int FirstDrawItemNo, int LastDrawItemNo, bool PrintMode, bool StrictTop, bool StrictBottom);
	bool __fastcall GetResizeHandleAt(int X, int Y, Rvresize::TRVResizeHandleIndex &Index);
	DYNAMIC bool __fastcall InitDragging(Rvdragdrop::TRVDropSource* &DropSource, int &OKEffect);
	DYNAMIC void __fastcall DoneDragging(bool FDeleteSelection);
	DYNAMIC void __fastcall LiveSpellingCheckCurrentItem(void);
	DYNAMIC void __fastcall DoAfterFormat(void);
	
public:
	Crvfdata::TRVDragDropCaretInfo* FDragDropCaretInfo;
	int FCurTextStyleNo;
	int FCurParaStyleNo;
	Rvundo::TRVUndoList* UndoList;
	Rvundo::TRVUndoList* RedoList;
	TRVUndoDest UndoDest;
	int CaretDrawItemNo;
	int CaretOffs;
	TRVCharPosCollection* CharEnds;
	int CaretHeight;
	bool FRVFInserted;
	void __fastcall ActivateCustomCaretTimer(void);
	void __fastcall DeactivateCustomCaretTimer(void);
	void __fastcall DrawCustomCaret(void);
	void __fastcall ClearCustomCaret(void);
	void __fastcall DestroyCustomCaretBack(void);
	virtual void __fastcall PaintBuffered(void);
	void __fastcall LaterSetBackLiveSpellingTo(int ItemNo, int Offs, bool ClearPainters);
	void __fastcall CreateDragDropCaretInfo(void);
	void __fastcall ReleaseDragDropCaretInfo(void);
	DYNAMIC Crvfdata::TRVDragDropCaretInfo* __fastcall GetDragDropCaretInfo(void);
	void __fastcall AssignCurTag(void);
	int __fastcall GetActualCurStyleNo(void);
	DYNAMIC System::Uitypes::TCursor __fastcall GetNormalCursor(void);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC bool __fastcall CancelResize(void);
	DYNAMIC void __fastcall AdjustMouseUpSelection(void);
	void __fastcall ResizeItem(int ItemNo, int Width, int Height);
	virtual void __fastcall XorDrawing(void);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC bool __fastcall InsertFirstRVFItem(int &Index, Rvtypes::TRVRawByteString &s, Rvitem::TCustomRVItemInfo* &li, bool EditFlag, bool &FullReformat, int &NewListNo);
	Rvundo::TRVUndoList* __fastcall GetUndoList(void);
	void __fastcall Change(void);
	void __fastcall ChangeEx(bool ClearRedo);
	DYNAMIC void __fastcall DoOnSelection(bool AllowScrolling);
	DYNAMIC bool __fastcall AllowChanging(void);
	void __fastcall CreateResizer(void);
	void __fastcall DestroyResizer(void);
	void __fastcall UpdateResizer(void);
	DYNAMIC bool __fastcall IsSticking(int FirstItemNo, bool NoSound);
	void __fastcall DoAfterFormatAndCaretPositioning(bool UpdateControl);
	DYNAMIC void __fastcall DoCurrentTextStyleConversion(int &StyleNo, int ParaStyleNo, int ItemNo, int UserData, bool ToWholeParagraphs);
	void __fastcall Do_ChangeSubDoc(Rvclasses::TRVMemoryStream* Stream, Crvdata::TCustomRVData* SubDoc);
	void __fastcall RememberSubDoc(Rvclasses::TRVMemoryStream* Stream, Crvdata::TCustomRVData* SubDoc);
	void __fastcall Do_ReformateRange(int StartNo, int EndNo, bool SuperReformat);
	void __fastcall Do_DeleteItem(int ItemNo, bool &FullReformat);
	void __fastcall Do_InsertItem(int ItemNo, Rvtypes::TRVRawByteString &s, Rvitem::TCustomRVItemInfo* item, bool FromUndo, bool &FullReformat);
	void __fastcall Do_ReplaceItem(int ItemNo, Rvitem::TCustomRVItemInfo* item);
	void __fastcall Do_DeleteItems(int StartItemNo, int EndItemNo, bool &FullReformat);
	void __fastcall Do_InsertItems(int ItemNo, Rvitem::TRVItemList* sl, bool FromUndo, bool &FullReformat);
	void __fastcall Do_ModifyItem(int ItemNo, const Rvtypes::TRVRawByteString s, Rvitem::TCustomRVItemInfo* Item);
	Rvundo::TRVUndoInsertItemsInfo* __fastcall Do_InsertItems_1(int ItemNo, int Count);
	void __fastcall Do_InsertItems_2(int ItemNo, int Count, Rvundo::TRVUndoInsertItemsInfo* ui, bool &FullReformat);
	void __fastcall Do_DeleteSubstring(int ItemNo, int Index, int ALength);
	void __fastcall Do_InsertSubstring(int ItemNo, int Index, const Rvtypes::TRVRawByteString s);
	void __fastcall Do_NewLine(int ItemNo, bool SameAsPrev, int ParaNo, bool &FullReformat);
	void __fastcall Do_BR(int ItemNo, bool BR, bool &FullReformat);
	void __fastcall Do_PageBreak(int ItemNo, bool PageBreak);
	void __fastcall Do_ClearTextFlow(int ItemNo, bool Left, bool Right);
	void __fastcall Do_ExtraIntProperty(int ItemNo, int Prop, int Value);
	void __fastcall Do_ExtraStrProperty(int ItemNo, int Prop, const System::UnicodeString Value);
	void __fastcall Do_Concate(int FirstItemNo);
	void __fastcall Do_MoveCP(int SrcItemNo, int DestItemNo);
	bool __fastcall Do_Para(int FirstItemNo, int EndItemNo, int ParaNo, bool &FullReformat);
	bool __fastcall Do_ParaList(int FirstItemNo, Rvclasses::TRVIntegerList* ParaList, bool &FullReformat);
	void __fastcall Do_ChangeAllParas(Rvclasses::TRVIntegerList* ParaMapList, Crvdata::TCustomRVData* SubDoc, Rvundo::TRVUndoList* List, Rvclasses::TRVIntegerList* NewIndices);
	void __fastcall Do_ChangeAllTextStyles(Rvclasses::TRVIntegerList* StyleMapList, Crvdata::TCustomRVData* SubDoc, Rvundo::TRVUndoList* List, Rvclasses::TRVIntegerList* NewIndices);
	bool __fastcall Do_ChangeStyleTemplates(Rvstyle::TRVStyleTemplateCollection* NewStyleTemplates);
	DYNAMIC void __fastcall ModifyStyleNoByTemplate(Rvitem::TCustomRVItemInfo* Item, int NewStyleNo);
	void __fastcall Do_RememberStyles(Crvdata::TCustomRVData* SubDoc, Rvundo::TRVUndoList* List);
	DYNAMIC void __fastcall ChangeParagraphStyles(Rvclasses::TRVIntegerList* ParaMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall ChangeTextStyles(Rvclasses::TRVIntegerList* TextMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	void __fastcall Do_StyleNo(int ItemNo, int StyleNo);
	void __fastcall Do_AssociatedTextStyleNo(int ItemNo, int TextStyleNo);
	void __fastcall Do_Tag(int ItemNo, const Rvstyle::TRVTag Tag, bool AssignAsIs);
	void __fastcall Do_AddCP(int ItemNo, Rvitem::TRVCPInfo* Checkpoint);
	void __fastcall Do_DeleteCP(int ItemNo);
	void __fastcall Do_ChangeTextR(int ItemNo, const Rvtypes::TRVRawByteString s);
	void __fastcall Do_ChangeVAlign(int ItemNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall Do_Resize(int ItemNo, int Width, int Height, bool Reformat);
	void __fastcall Do_ItemModifyTerminator(int ItemNo, bool Opening);
	Rvundo::TRVUndoModifyItemProps* __fastcall Do_ModifyItemIntProperty(int ItemNo, System::TObject* SubObject, const System::UnicodeString PropertyName, int Value, bool AffectSize, bool AffectWidth, Rvundo::TRVUndoInfoClass UndoInfoClass);
	Rvundo::TRVUndoModifyItemProps* __fastcall Do_ModifyItemStrProperty(int ItemNo, System::TObject* SubObject, const System::UnicodeString PropertyName, const System::UnicodeString Value, Rvundo::TRVUndoInfoClass UndoInfoClass);
	Rvundo::TRVUndoModifyItemProps* __fastcall Do_ModifyItemIntProperties(int ItemNo, System::TObject* SubObject, System::Classes::TStringList* PropList, bool AffectSize, bool AffectWidth, Rvundo::TRVUndoInfoClass UndoInfoClass);
	void __fastcall Do_SetIntProperty(Richview::TRVIntProperty Prop, int Value);
	void __fastcall Do_SetStrProperty(Richview::TRVStrProperty Prop, const System::UnicodeString Value);
	void __fastcall Do_SetFloatProperty(Richview::TRVFloatProperty Prop, Rvstyle::TRVLength Value);
	void __fastcall Do_SetBackgroundImage(Vcl::Graphics::TGraphic* Graphic);
	void __fastcall BeginUndoSequence(Rvedit::TRVUndoType UndoType, bool AllowFinalize);
	void __fastcall SetUndoGroupMode(bool GroupUndo);
	void __fastcall FinalizeUndoGroup(void);
	void __fastcall BeginNamedUndoSequence(Rvedit::TRVUndoType UndoType, const System::UnicodeString Caption, bool AllowFinalize);
	void __fastcall EndUndoSequence(void);
	void __fastcall BeginRedoSequence(Rvedit::TRVUndoType UndoType, const System::UnicodeString Caption);
	DYNAMIC void __fastcall GetSelectionBoundsEx(int &StartItemNo, int &StartItemOffs, int &EndItemNo, int &EndItemOffs, bool Normalize);
	DYNAMIC void __fastcall GetSelBounds(int &StartNo, int &EndNo, int &StartOffs, int &EndOffs, bool Normalize);
	DYNAMIC void __fastcall Clear(void);
	void __fastcall PrepareForEdit(void);
	bool __fastcall DeleteSelection_(void);
	bool __fastcall CanDelete(void);
	void __fastcall InsertTextTyping(Rvtypes::TRVRawByteString text, System::WideChar Key);
	void __fastcall InsertTextA_(const Rvtypes::TRVAnsiString text, bool AutoTag, bool CaretBefore, Rvstyle::TRVCodePage CodePage);
	void __fastcall InsertTextW_(const Rvtypes::TRVRawByteString text, bool AutoTag, bool CaretBefore);
	int __fastcall GetNextParaNo(int ParaNo);
	int __fastcall GetNextStyleNo(int StyleNo, int ParaNo, bool NewLine);
	bool __fastcall OnEnterPress_(bool Shift, bool Recursive);
	bool __fastcall IsCharOffsAtTheLineEndAndItemEnd(int Offs);
	bool __fastcall IsCharOffsAtTheLineStartAndItemStart(int Offs);
	void __fastcall OnDeletePress_(bool Ctrl, bool MovedFromLineEnd);
	void __fastcall OnBackSpacePress_(bool Ctrl, bool MultiDelete, bool FromNextLine);
	void __fastcall SetCurTextStyleNo(int Value);
	void __fastcall SetCurParaStyleNo(int Value);
	DYNAMIC int __fastcall GetCurParaStyleNo(void);
	virtual void __fastcall ClearTemporal(void);
	virtual void __fastcall StartPartialFormatting(void);
	void __fastcall ApplyParaStyle(int ParaStyleNo, Rvitem::TRVEStyleConversionType ConvType, bool Recursive, bool CallOnChange);
	void __fastcall UpdateSelectionEnd(void);
	bool __fastcall OnHomePress(bool Shift, bool Ctrl);
	bool __fastcall OnDownPress(bool Shift, bool Ctrl);
	bool __fastcall OnEndPress(bool Shift, bool Ctrl);
	bool __fastcall OnLeftPress(bool Shift, bool Ctrl);
	bool __fastcall OnPgDownPress(bool Shift);
	bool __fastcall OnPgUpPress(bool Shift);
	bool __fastcall OnRightPress(bool Shift, bool Ctrl);
	bool __fastcall OnUpPress(bool Shift, bool Ctrl);
	void __fastcall MoveCaretToTheBeginningOfThePrevParagraph(bool Select);
	void __fastcall MoveCaretToTheEndOfTheNextParagraph(bool Select);
	void __fastcall ChangeCaret(bool ForceCreate, bool ScrollToCaret, bool DontChangeStyle, bool RefreshBefore);
	DYNAMIC int __fastcall BuildJumpsInfo(bool IgnoreReadOnly);
	DYNAMIC void __fastcall ClearJumpsInfo(void);
	virtual bool __fastcall Format_(bool OnlyResized, bool ForceFormat, bool NoScroll, int depth, Vcl::Graphics::TCanvas* Canvas, bool OnlyTail, bool NoCaching, bool Reformatting, bool CallOnFormat, bool AbsRotation);
	DYNAMIC void __fastcall GetSelStart(int &DINo, int &DIOffs);
	DYNAMIC void __fastcall SrchSelectIt(int StartItemNo, int StartOffs, int EndItemNo, int EndOffs, bool Invert);
	DYNAMIC void __fastcall SrchStart(bool Down, bool FromStart, bool SmartStart, int &strt, int &offs);
	int __fastcall GetCurItemNo(void);
	int __fastcall GetOffsetInCurItem(void);
	bool __fastcall InsertSomething(Rvitem::TCustomRVItemInfo* info, Rvtypes::TRVRawByteString &s, Rvtypes::TRVRawByteString &SplitText, bool AutoTag, bool CaretBefore, bool CallTEEvent);
	bool __fastcall NotFormatted(void);
	void __fastcall StartShiftMoving(void);
	void __fastcall EndShiftMoving(void);
	void __fastcall ApplyStyleConversion_(int UserData, Rvitem::TRVEStyleConversionType ConvType, bool Recursive);
	bool __fastcall InsertRVFFromStreamEd_(System::Classes::TStream* Stream);
	void __fastcall OnChangeCaretLine(int DLOffs);
	void __fastcall ConcateAfterAdding(int &InsertPoint, int &LastInserted, int &ItemsAdded, int &Offs);
	bool __fastcall InsertRTFFromStreamEd_(System::Classes::TStream* Stream);
	DYNAMIC bool __fastcall SaveRTFToStream(System::Classes::TStream* Stream, bool SelectionOnly, int Level, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool CompleteDocument, bool HiddenParent, Crvdata::TCustomRVData* Header, Crvdata::TCustomRVData* Footer, Crvdata::TCustomRVData* FirstPageHeader, Crvdata::TCustomRVData* FirstPageFooter, Crvdata::TCustomRVData* EvenPagesHeader, Crvdata::TCustomRVData* EvenPagesFooter);
	bool __fastcall InsertTextFromStream(System::Classes::TStream* Stream, bool OEM, bool AutoTag, Rvstyle::TRVCodePage CodePage);
	bool __fastcall InsertTextFromStreamW(System::Classes::TStream* Stream, bool AutoTag);
	bool __fastcall InsertTextFromFile(const System::UnicodeString FileName, bool OEM, bool AutoTag, Rvstyle::TRVCodePage CodePage);
	bool __fastcall InsertTextFromFileW(const System::UnicodeString FileName, bool AutoTag);
	void __fastcall KeyPress(System::WideChar &Key);
	void __fastcall AdjustControlPlacement(int ItemNo);
	void __fastcall ResizeControl(int ItemNo, int NewWidth, int NewHeight, bool Reformat);
	void __fastcall Reformat(bool FullFormat, bool ForceFormat, bool NoScroll, int ItemNo, bool UpdateView);
	void __fastcall ReformatEx(bool FullFormat, bool ForceFormat, bool NoScroll, int ItemNo, bool UpdateView, int SelStartNo, int SelStartOffs, int SelEndNo, int SelEndOffs);
	void __fastcall Reformat_(bool FullFormat, int StartDrawItem, int EndDrawItem, int ItemsAdded);
	void __fastcall BeginItemModify(int ItemNo, int &ModifyData);
	void __fastcall EndItemModify(int ItemNo, int ModifyData);
	void __fastcall EndItemModifyEx(int ItemNo, int ModifyData, int SelStartNo, int SelStartOffs, int SelEndNo, int SelEndOffs);
	void __fastcall SelectCurrentWord(void);
	void __fastcall InsertPageBreak(void);
	void __fastcall SplitAtCaret(void);
	DYNAMIC bool __fastcall SaveHTMLToStreamEx(System::Classes::TStream* Stream, const System::UnicodeString Path, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, const System::UnicodeString ExtraStyles, const System::UnicodeString ExternalCSS, const System::UnicodeString CPPrefix, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, System::Uitypes::TColor &CurrentFileColor, int &imgSaveNo, int LeftMargin, int TopMargin, int RightMargin, int BottomMargin, Rvback::TRVBackground* Background, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC bool __fastcall SaveHTMLToStream(System::Classes::TStream* Stream, const System::UnicodeString Path, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, Rvstyle::TRVSaveOptions Options, System::Uitypes::TColor Color, int &imgSaveNo, int LeftMargin, int TopMargin, int RightMargin, int BottomMargin, Rvback::TRVBackground* Background, Rvclasses::TRVList* Bullets);
	System::Types::TPoint __fastcall GetIMEWinCoord(void);
	void __fastcall PrepareForUpdateRangeAfterMarkersOrSeq(int StartNo, int EndNo, bool ForDeletion, int &FirstItemNo, int &LastMarkerIndex, int &LastSeqIndex, Rvclasses::TRVIntegerList* &ListNos, System::Classes::TStringList* &SeqNames);
	void __fastcall UpdateAfterMarkersOrSeq(int FirstItemNo, int LastMarkerIndex, int LastSeqIndex, Rvclasses::TRVIntegerList* ListNos, System::Classes::TStringList* SeqNames);
	void __fastcall UpdateRangeAfterMarkersOrSeq(int StartNo, int EndNo);
	void __fastcall ApplyListStyle_(int AListNo, int AListLevel, int AStartFrom, bool AUseStartFrom, bool ARecursive, TRVParaListOperation Operation, int &ItemsAdded, int &StartNo, int &EndNo, int &SelStartNo, int &SelStartOffs, int &SelEndNo, int &SelEndOffs, Rvclasses::TRVIntegerList* ListNos, int &LastVWMarkerIndex);
	void __fastcall ApplyListStyle(int AListNo, int AListLevel, int AStartFrom, bool AUseStartFrom, bool ARecursive, TRVParaListOperation Operation);
	int __fastcall GetUpDownMovingX(void);
	void __fastcall SetUpDownMovingX(int X);
	void __fastcall ResetUpDownMovingX(void);
	bool __fastcall ApplyChangedStyleTemplatesToParagraphs(Rvstyle::TRVStyleTemplateCollection* NewStyleTemplates, Crvdata::TCustomRVData* SubDoc, Rvundo::TRVUndoList* List);
	bool __fastcall ApplyChangedStyleTemplatesToText(Rvstyle::TRVStyleTemplateCollection* NewStyleTemplates, Crvdata::TCustomRVData* SubDoc, Rvundo::TRVUndoList* List);
	void __fastcall ChangeStyleTemplates(Rvstyle::TRVStyleTemplateCollection* NewStyleTemplates);
	void __fastcall CallStyleChangeEvents(void);
	virtual void __fastcall AfterVScroll(void);
	DYNAMIC bool __fastcall GetForceFieldHighlight(void);
	DYNAMIC void __fastcall LoadFormattingFromStream(System::Classes::TStream* Stream);
	__fastcall virtual TRVEditRVData(Rvscroll::TRVScroller* RichView);
	__fastcall virtual ~TRVEditRVData(void);
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE int RichViewEditCaretWidth;
extern DELPHI_PACKAGE int RichViewEditCaretHeightExtra;
extern DELPHI_PACKAGE int RichViewEditMaxCaretHeight;
extern DELPHI_PACKAGE int RichViewEditMinCaretHeight;
extern DELPHI_PACKAGE bool RichViewEditDefaultProportionalResize;
extern DELPHI_PACKAGE bool RichViewEditEnterAllowsEmptyMarkeredLines;
}	/* namespace Rvervdata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVERVDATA)
using namespace Rvervdata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvervdataHPP
