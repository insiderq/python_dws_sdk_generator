﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RichView.pas' rev: 27.00 (Windows)

#ifndef RichviewHPP
#define RichviewHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <Winapi.RichEdit.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVDragDrop.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <RVWordPaint.hpp>	// Pascal unit
#include <RVThread.hpp>	// Pascal unit
#include <RVPopup.hpp>	// Pascal unit
#include <RVZip.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVSelectionHandles.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <RVRTFProps.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Richview
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVEventType : unsigned char { rvetRVDblClick, rvetJump, rvetRVMouseUp, rvetRVMouseDown, rvetClick, rvetDblClick, rvetTripleClick, rvetMouseMove, rvetDragDrop, rvetEndDrag };

class DELPHICLASS TRVMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMessageData : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	TRVEventType Event;
public:
	/* TObject.Create */ inline __fastcall TRVMessageData(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVClickMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVClickMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	/* TObject.Create */ inline __fastcall TRVClickMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVClickMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVStdDblClickMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStdDblClickMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	/* TObject.Create */ inline __fastcall TRVStdDblClickMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVStdDblClickMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVTripleClickMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTripleClickMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	/* TObject.Create */ inline __fastcall TRVTripleClickMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVTripleClickMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVDblClickMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDblClickMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	Rvtypes::TRVRawByteString ClickedWord;
	int StyleNo;
public:
	/* TObject.Create */ inline __fastcall TRVDblClickMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVDblClickMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVJumpMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVJumpMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	int id;
public:
	/* TObject.Create */ inline __fastcall TRVJumpMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVJumpMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVMouseMoveMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMouseMoveMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	int X;
	int Y;
	int ItemNo;
	System::Classes::TShiftState Shift;
public:
	/* TObject.Create */ inline __fastcall TRVMouseMoveMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVMouseMoveMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVMouseUpDownMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMouseUpDownMessageData : public TRVMouseMoveMessageData
{
	typedef TRVMouseMoveMessageData inherited;
	
public:
	System::Uitypes::TMouseButton Button;
public:
	/* TObject.Create */ inline __fastcall TRVMouseUpDownMessageData(void) : TRVMouseMoveMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVMouseUpDownMessageData(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVDNDMessageData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDNDMessageData : public TRVMessageData
{
	typedef TRVMessageData inherited;
	
public:
	int X;
	int Y;
	System::TObject* Obj;
public:
	/* TObject.Create */ inline __fastcall TRVDNDMessageData(void) : TRVMessageData() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVDNDMessageData(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVPrintingStep : unsigned char { rvpsStarting, rvpsProceeding, rvpsFinished };

enum DECLSPEC_DENUM TRVLiveSpellingMode : unsigned char { rvlspManualStart, rvlspOnChange };

enum DECLSPEC_DENUM TRVAnimationMode : unsigned char { rvaniDisabled, rvaniManualStart, rvaniOnFormat };

enum DECLSPEC_DENUM TRVYesNoAuto : unsigned char { rvynaNo, rvynaYes, rvynaAuto };

enum DECLSPEC_DENUM TRVIntProperty : unsigned char { rvipLeftMargin, rvipRightMargin, rvipTopMargin, rvipBottomMargin, rvipMaxTextWidth, rvipMinTextWidth, rvipBackgroundStyle, rvipColor, rvipBiDiMode, rvipDPUnits, rvipDPOrientation, rvipDPTitlePage, rvipDPFacingPages, rvipDPMirrorMargins };

enum DECLSPEC_DENUM TRVFloatProperty : unsigned char { rvfpDPLeftMargin, rvfpDPRightMargin, rvfpDPTopMargin, rvfpDPBottomMargin, rvfpDPHeaderY, rvfpDPFooterY };

enum DECLSPEC_DENUM TRVStrProperty : unsigned char { rvspDPTitle, rvspDPAuthor, rvspDPComments };

typedef System::TMetaClass* TRVWordEnumThreadClass;

typedef System::TMetaClass* TRVRTFReaderPropertiesClass;

typedef void __fastcall (__closure *TJumpEvent)(System::TObject* Sender, int id);

class DELPHICLASS TCustomRichView;
typedef void __fastcall (__closure *TRVGetItemCursorEvent)(TCustomRichView* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, System::Uitypes::TCursor &Cursor);

typedef void __fastcall (__closure *TRVMouseMoveEvent)(System::TObject* Sender, int id);

typedef void __fastcall (__closure *TRVMouseEvent)(TCustomRichView* Sender, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);

typedef void __fastcall (__closure *TRVSaveComponentToFileEvent)(TCustomRichView* Sender, System::UnicodeString Path, System::Classes::TPersistent* SaveMe, Rvstyle::TRVSaveFormat SaveFormat, System::UnicodeString &OutStr);

typedef void __fastcall (__closure *TRVSaveItemToFileEvent)(TCustomRichView* Sender, const System::UnicodeString Path, Crvdata::TCustomRVData* RVData, int ItemNo, Rvstyle::TRVSaveFormat SaveFormat, bool Unicode, Rvtypes::TRVRawByteString &OutStr, bool &DoDefault);

typedef void __fastcall (__closure *TRVURLNeededEvent)(TCustomRichView* Sender, int id, System::UnicodeString &url);

typedef void __fastcall (__closure *TRVDblClickEvent)(TCustomRichView* Sender, Rvtypes::TRVRawByteString ClickedWord, int Style);

typedef void __fastcall (__closure *TRVRightClickEvent)(TCustomRichView* Sender, Rvtypes::TRVRawByteString ClickedWord, int Style, int X, int Y);

typedef void __fastcall (__closure *TRVFPictureNeededEvent)(TCustomRichView* Sender, const System::UnicodeString ItemName, Rvitem::TRVNonTextItemInfo* Item, int Index1, int Index2, Vcl::Graphics::TGraphic* &gr);

typedef void __fastcall (__closure *TRVFControlNeededEvent)(TCustomRichView* Sender, System::UnicodeString Name, const Rvstyle::TRVTag Tag, Vcl::Controls::TControl* &ctrl);

typedef void __fastcall (__closure *TRVCheckpointVisibleEvent)(TCustomRichView* Sender, Rvstyle::TCheckpointData CheckpointData);

typedef void __fastcall (__closure *TRVControlActionEvent)(TCustomRichView* Sender, Rvstyle::TRVControlAction ControlAction, int ItemNo, Vcl::Controls::TControl* &ctrl);

typedef void __fastcall (__closure *TRVItemActionEvent)(TCustomRichView* Sender, Rvstyle::TRVItemAction ItemAction, Rvitem::TCustomRVItemInfo* Item, Rvtypes::TRVRawByteString &Text, Crvdata::TCustomRVData* RVData);

typedef void __fastcall (__closure *TRVFImageListNeededEvent)(TCustomRichView* Sender, int ImageListTag, Vcl::Imglist::TCustomImageList* &il);

typedef void __fastcall (__closure *TRVHTMLSaveImageEvent)(TCustomRichView* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, const System::UnicodeString Path, System::Uitypes::TColor BackgroundColor, System::UnicodeString &Location, bool &DoDefault);

typedef void __fastcall (__closure *TRVSaveImageEvent2)(TCustomRichView* Sender, Vcl::Graphics::TGraphic* Graphic, Rvstyle::TRVSaveFormat SaveFormat, const System::UnicodeString Path, const System::UnicodeString ImagePrefix, int &ImageSaveNo, System::UnicodeString &Location, bool &DoDefault);

typedef void __fastcall (__closure *TRVReadHyperlink)(TCustomRichView* Sender, const System::UnicodeString Target, const System::UnicodeString Extras, Rvstyle::TRVLoadFormat DocFormat, int &StyleNo, Rvstyle::TRVTag &ItemTag, Rvtypes::TRVRawByteString &ItemName);

typedef void __fastcall (__closure *TRVWriteHyperlink)(TCustomRichView* Sender, int id, Crvdata::TCustomRVData* RVData, int ItemNo, Rvstyle::TRVSaveFormat SaveFormat, System::UnicodeString &Target, System::UnicodeString &Extras);

typedef void __fastcall (__closure *TRVSaveRTFExtraEvent)(TCustomRichView* Sender, Rvstyle::TRVRTFSaveArea Area, System::TObject* Obj, int Index1, int Index2, bool InStyleSheet, Rvtypes::TRVAnsiString &RTFCode);

typedef void __fastcall (__closure *TRVSaveDocXExtraEvent)(TCustomRichView* Sender, Rvstyle::TRVDocXSaveArea Area, System::TObject* Obj, int Index1, int Index2, Rvtypes::TRVAnsiString &OOXMLCode);

typedef void __fastcall (__closure *TRVSaveHTMLExtraEvent)(TCustomRichView* Sender, Rvstyle::TRVHTMLSaveArea Area, bool CSSVersion, System::UnicodeString &HTMLCode);

typedef void __fastcall (__closure *TRVSaveParaToHTMLEvent)(TCustomRichView* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, bool ParaStart, bool CSSVersion, System::UnicodeString &HTMLCode);

typedef void __fastcall (__closure *TRVPaintEvent)(TCustomRichView* Sender, Vcl::Graphics::TCanvas* Canvas, bool Prepaint);

typedef void __fastcall (__closure *TRVImportPictureEvent)(TCustomRichView* Sender, const System::UnicodeString Location, int Width, int Height, Vcl::Graphics::TGraphic* &Graphic);

typedef void __fastcall (__closure *TRVItemHintEvent)(TCustomRichView* Sender, Crvdata::TCustomRVData* RVData, int ItemNo, System::UnicodeString &HintText);

typedef void __fastcall (__closure *TRVProgressEvent)(TCustomRichView* Sender, Rvstyle::TRVLongOperation Operation, Rvstyle::TRVProgressStage Stage, System::Byte PercentDone);

typedef void __fastcall (__closure *TRVSpellingCheckEvent)(TCustomRichView* Sender, const System::UnicodeString AWord, int StyleNo, bool &Misspelled);

typedef void __fastcall (__closure *TRVSpellingCheckExEvent)(TCustomRichView* Sender, const System::UnicodeString AWord, Crvdata::TCustomRVData* RVData, int ItemNo, int Offs, bool &Misspelled);

typedef void __fastcall (__closure *TRVSmartPopupClickEvent)(TCustomRichView* Sender, Vcl::Controls::TCustomControl* Button);

typedef void __fastcall (__closure *TRVAddStyleEvent)(TCustomRichView* Sender, Rvstyle::TCustomRVInfo* StyleInfo);

typedef void __fastcall (__closure *TRVGetSRVInterfaceEvent)(Crvdata::_di_IRVScaleRichViewInterface &Ifc);

class DELPHICLASS TRVEventHandlerItem;
class PASCALIMPLEMENTATION TRVEventHandlerItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::Classes::TComponent* Component;
	System::Classes::TNotifyEvent Event;
public:
	/* TObject.Create */ inline __fastcall TRVEventHandlerItem(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVEventHandlerItem(void) { }
	
};


class DELPHICLASS TRVEventHandlerList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVEventHandlerList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVEventHandlerItem* operator[](int Index) { return Items[Index]; }
	
private:
	TRVEventHandlerItem* __fastcall GetItem(int Index);
	void __fastcall SetItem(int Index, TRVEventHandlerItem* const Value);
	
public:
	void __fastcall RegisterHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall UnregisterHandler(System::Classes::TComponent* Component);
	void __fastcall Execute(System::TObject* Sender);
	HIDESBASE int __fastcall Find(System::Classes::TComponent* Component);
	__property TRVEventHandlerItem* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVEventHandlerList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVEventHandlerList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVDestroyInformer;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDestroyInformer : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int RefCount;
	TCustomRichView* Owner;
	
public:
	bool ControlDestroyed;
	__fastcall TRVDestroyInformer(TCustomRichView* AOwner);
	void __fastcall AddRef(void);
	void __fastcall Release(void);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVDestroyInformer(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TCustomRichView : public Rvscroll::TRVScroller
{
	typedef Rvscroll::TRVScroller inherited;
	
private:
	TRVDestroyInformer* FDestroyInformer;
	System::UnicodeString FNoteText;
	bool FUseStyleTemplates;
	Rvstyle::TRVStyleTemplateInsertMode FStyleTemplateInsertMode;
	System::Uitypes::TCursor FCursor;
	Rvscroll::TRVOptions FOptions;
	Rvstyle::TRVRTFOptions FRTFOptions;
	bool ScrollTimerActive;
	System::UnicodeString FDelimiters;
	int FMaxLength;
	TJumpEvent FOnJump;
	TRVGetItemCursorEvent FOnGetItemCursor;
	TRVMouseMoveEvent FOnRVMouseMove;
	TRVSaveComponentToFileEvent FOnSaveComponentToFile;
	TRVSaveItemToFileEvent FOnSaveItemToFile;
	TRVURLNeededEvent FOnURLNeeded;
	TRVDblClickEvent FOnRVDblClick;
	TRVRightClickEvent FOnRVRightClick;
	TRVMouseEvent FOnRVMouseUp;
	TRVMouseEvent FOnRVMouseDown;
	TRVControlActionEvent FOnControlAction;
	TRVItemActionEvent FOnItemAction;
	Rvscroll::TCPEventKind FCPEventKind;
	TRVFPictureNeededEvent FOnRVFPictureNeeded;
	TRVGetSRVInterfaceEvent FOnGetScaleRichViewInterface;
	TRVFControlNeededEvent FOnRVFControlNeeded;
	TRVFImageListNeededEvent FOnRVFImageListNeeded;
	TRVCheckpointVisibleEvent FOnCheckpointVisible;
	int FMaxTextWidth;
	int FMinTextWidth;
	int FLeftMargin;
	int FRightMargin;
	int FTopMargin;
	int FBottomMargin;
	Rvstyle::TRVFOptions FRVFOptions;
	Rvstyle::TRVFWarnings FRVFWarnings;
	TRVAddStyleEvent FOnAddStyle;
	bool FWordWrap;
	System::Classes::TNotifyEvent FOnCopy;
	TRVHTMLSaveImageEvent FOnHTMLSaveImage;
	TRVSaveImageEvent2 FOnSaveImage2;
	Rvrtfprops::TRVRTFReaderProperties* FRTFReadProperties;
	Rvdocparams::TRVDocParameters* FDocParameters;
	Rvpopup::TRVSmartPopupProperties* FSmartPopupProperties;
	TRVSmartPopupClickEvent FOnSmartPopupClick;
	Rvstyle::TRVFReaderStyleMode FRVFTextStylesReadMode;
	Rvstyle::TRVFReaderStyleMode FRVFParaStylesReadMode;
	TRVReadHyperlink FOnReadHyperlink;
	TRVWriteHyperlink FOnWriteHyperlink;
	TRVSaveRTFExtraEvent FOnSaveRTFExtra;
	TRVSaveDocXExtraEvent FOnSaveDocXExtra;
	TRVSaveHTMLExtraEvent FOnSaveHTMLExtra;
	TRVSaveParaToHTMLEvent FOnSaveParaToHTML;
	TRVPaintEvent FOnPaint;
	TRVImportPictureEvent FOnImportPicture;
	TRVItemHintEvent FOnItemHint;
	System::Classes::TStringList* FDocProperties;
	TRVProgressEvent FOnProgress;
	TRVSpellingCheckEvent FOnSpellingCheck;
	TRVLiveSpellingMode FLiveSpellingMode;
	TRVAnimationMode FAnimationMode;
	Vcl::Stdctrls::TTextLayout FVAlign;
	System::Classes::TNotifyEvent FOnStyleTemplatesChange;
	TRVEventHandlerList* FStyleTemplatesChangeHandlers;
	TRVEventHandlerList* FHScrollHandlers;
	TRVEventHandlerList* FVScrollHandlers;
	TRVEventHandlerList* FSaveHandlers;
	TRVEventHandlerList* FClearHandlers;
	TRVEventHandlerList* FResizeHandlers;
	HIDESBASE MESSAGE void __fastcall WMContextMenu(Winapi::Messages::TWMContextMenu &Message);
	HIDESBASE MESSAGE void __fastcall WMSize(Winapi::Messages::TWMSize &Message);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	HIDESBASE MESSAGE void __fastcall CMMouseLeave(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMCopy(Winapi::Messages::TWMNoParams &Message);
	MESSAGE void __fastcall WMTimer(Winapi::Messages::TWMTimer &Message);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall WMGetDlgCode(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Winapi::Messages::TWMSetFocus &Message);
	HIDESBASE MESSAGE void __fastcall WMKillFocus(Winapi::Messages::TWMKillFocus &Message);
	MESSAGE void __fastcall WMRVEvent(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMRVDragDrop(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMGetSel(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMSetSel(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMGetTextRange(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall CMColorChanged(Winapi::Messages::TMessage &Message);
	int __fastcall GetLineCount(void);
	bool __fastcall GetAllowSelection(void);
	bool __fastcall GetSingleClick(void);
	void __fastcall SetAllowSelection(const bool Value);
	void __fastcall SetSingleClick(const bool Value);
	bool __fastcall GetPageBreaksBeforeItems(int Index);
	void __fastcall SetPageBreaksBeforeItems(int Index, bool Value);
	int __fastcall GetDocumentHeight(void);
	int __fastcall GetFirstJumpNo(void);
	void __fastcall SetFirstJumpNo(int Value);
	void __fastcall SetTabNavigation(const Rvscroll::TRVTabNavigationType Value);
	void __fastcall SetRTFReadProperties(Rvrtfprops::TRVRTFReaderProperties* const Value);
	void __fastcall SetDocParameters(Rvdocparams::TRVDocParameters* const Value);
	bool __fastcall StoreDelimiters(void);
	void __fastcall SetDocProperties(System::Classes::TStringList* const Value);
	void __fastcall DoClearLiveSpellingResults(void);
	void __fastcall ClearItemLiveSpellingResults(Crvdata::TCustomRVData* RVData, int ItemNo, int &UserData1, const System::UnicodeString UserData2, bool &ContinueEnum);
	void __fastcall LiveSpellingValidateWordInItem(Crvdata::TCustomRVData* RVData, int ItemNo, int &UserData1, const System::UnicodeString UserData2, bool &ContinueEnum);
	void __fastcall FullInvalidate(void);
	void __fastcall RecreateAnimatorsProc(Crvdata::TCustomRVData* RVData, int ItemNo, int &UserData1, const System::UnicodeString UserData2, bool &ContinueEnum);
	void __fastcall SetAnimationMode(const TRVAnimationMode Value);
	void __fastcall KillAnimators(void);
	Rvpopup::TRVSmartPopupProperties* __fastcall GetSmartPopupProperties(void);
	void __fastcall SetSmartPopupProperties(Rvpopup::TRVSmartPopupProperties* const Value);
	bool __fastcall GetSmartPopupVisible(void);
	void __fastcall SetSmartPopupVisible(const bool Value);
	bool __fastcall StoreDocParameters(void);
	bool __fastcall GetClearLeft(int Index);
	bool __fastcall GetClearRight(int Index);
	void __fastcall SetClearLeft(int Index, const bool Value);
	void __fastcall SetClearRight(int Index, const bool Value);
	bool __fastcall GetSelectionHandlesVisible(void);
	void __fastcall SetSelectionHandlesVisible(bool Value);
	
protected:
	int VScrollDelta;
	int HScrollDelta;
	System::Classes::TNotifyEvent FOnSelect;
	Rvstyle::TRVStyle* FStyle;
	System::UnicodeString imgSavePrefix;
	Rvstyle::TRVSaveOptions SaveOptions;
	System::Uitypes::TColor CurrentFileColor;
	Rvthread::TRVWordEnumThread* FWordEnumThread;
	int FDblClickTime;
	virtual bool __fastcall RequiresFullRedraw(void);
	virtual void __fastcall WndProc(Winapi::Messages::TMessage &Message);
	DYNAMIC void __fastcall AdjustPopupMenuPos(System::Types::TPoint &pt);
	virtual void __fastcall SetBiDiModeRV(const Rvscroll::TRVBiDiMode Value);
	virtual void __fastcall SetVSmallStep(int Value);
	DYNAMIC System::UnicodeString __fastcall GetDesignTimeText(void);
	virtual void __fastcall Paint(void);
	System::Uitypes::TColor __fastcall GetColor(void);
	System::Uitypes::TColor __fastcall GetHoverColor(System::Uitypes::TColor Color);
	bool __fastcall IsCopyShortcut(System::Classes::TShiftState Shift, System::Word Key);
	bool __fastcall IsCutShortcut(System::Classes::TShiftState Shift, System::Word Key);
	bool __fastcall IsPasteShortcut(System::Classes::TShiftState Shift, System::Word Key);
	DYNAMIC void __fastcall DblClick(void);
	void __fastcall TripleClick(void);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	virtual void __fastcall ClearTemporal(void);
	int __fastcall GetFirstItemVisible(void);
	int __fastcall GetLastItemVisible(void);
	Vcl::Graphics::TBitmap* __fastcall GetBackBitmap(void);
	void __fastcall SetBackBitmap(Vcl::Graphics::TBitmap* Value);
	void __fastcall SetBackgroundStyle(Rvscroll::TBackgroundStyle Value);
	Rvscroll::TBackgroundStyle __fastcall GetBackgroundStyle(void);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Loaded(void);
	bool __fastcall CompareTags(const Rvstyle::TRVTag Tag1, const Rvstyle::TRVTag Tag2);
	virtual void __fastcall SetStyle(Rvstyle::TRVStyle* Value);
	virtual void __fastcall AfterVScroll(void);
	virtual void __fastcall InplaceRedrawing(bool AllowRedrawItself);
	virtual void __fastcall AfterHScroll(void);
	void __fastcall GenerateMouseMove(void);
	void __fastcall Format_(bool OnlyResized, bool ForceFormat, Vcl::Graphics::TCanvas* Canvas, bool OnlyTail, bool NoCaching, bool Reformatting, bool CallOnFormat);
	virtual Rvrvdata::TRichViewRVDataClass __fastcall GetDataClass(void);
	Rvscroll::TRVTabNavigationType __fastcall GetTabNavigation(void);
	virtual Rvrtfprops::TRVRTFReaderProperties* __fastcall GetRTFReadProperties(void);
	virtual TRVRTFReaderPropertiesClass __fastcall GetRTFReadPropertiesClass(void);
	virtual void __fastcall AlignControls(Vcl::Controls::TControl* AControl, System::Types::TRect &Rect);
	virtual Rvdocparams::TRVDocParameters* __fastcall GetDocParameters(void);
	DYNAMIC void __fastcall AfterCreateWnd1(void);
	DYNAMIC void __fastcall AfterCreateWnd2(void);
	virtual void __fastcall SetName(const System::Classes::TComponentName NewName);
	void __fastcall ResumeLiveSpelling(void);
	void __fastcall ShowSmartPopup(void);
	void __fastcall HideSmartPopup(void);
	DYNAMIC void __fastcall SetSmartPopupTarget(void);
	virtual void __fastcall DoGesture(const Vcl::Controls::TGestureEventInfo &EventInfo, bool &Handled);
	void __fastcall RegisterEventHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event, TRVEventHandlerList* &Handlers);
	void __fastcall UnregisterEventHandler(System::Classes::TComponent* Component, TRVEventHandlerList* &Handlers);
	__property bool AllowSelection = {read=GetAllowSelection, write=SetAllowSelection, stored=false, nodefault};
	__property bool SingleClick = {read=GetSingleClick, write=SetSingleClick, stored=false, nodefault};
	__property TRVPaintEvent OnPaint = {read=FOnPaint, write=FOnPaint};
	
public:
	Rvrvdata::TRichViewRVData* RVData;
	Crvdata::TRVFlags Flags;
	Rvback::TRVBackground* Background;
	int imgSaveNo;
	bool CurPictureInvalid;
	__property System::UnicodeString NoteText = {read=FNoteText, write=FNoteText};
	void __fastcall RefreshSequences(void);
	__property Canvas;
	DYNAMIC Vcl::Graphics::TCanvas* __fastcall GetFormatCanvas(Vcl::Graphics::TCanvas* DefCanvas);
	void __fastcall SelectNext_(bool GoForward);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall ActivateScrollTimer(bool Slow);
	void __fastcall DeactivateScrollTimer(void);
	DYNAMIC bool __fastcall RTFReaderAssigned(void);
	DYNAMIC bool __fastcall DocParametersAssigned(void);
	void __fastcall AssignEvents(TCustomRichView* Source);
	__fastcall virtual TCustomRichView(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomRichView(void);
	DYNAMIC void __fastcall GetTabOrderList(System::Classes::TList* List);
	void __fastcall AssignSoftPageBreaks(System::Classes::TComponent* RVPrint);
	void __fastcall ClearSoftPageBreaks(void);
	void __fastcall AddItemR(const Rvtypes::TRVRawByteString Text, Rvitem::TCustomRVItemInfo* Item, bool AdjustPara);
	void __fastcall AddItem(const System::UnicodeString Text, Rvitem::TCustomRVItemInfo* Item);
	void __fastcall AddNLR(const Rvtypes::TRVRawByteString s, int StyleNo, int ParaNo);
	void __fastcall AddNL(const System::UnicodeString s, int StyleNo, int ParaNo);
	void __fastcall AddFmt(const System::UnicodeString FormatStr, System::TVarRec const *Args, const int Args_High, int StyleNo, int ParaNo);
	void __fastcall AddR(const Rvtypes::TRVRawByteString s, int StyleNo);
	void __fastcall Add(const System::UnicodeString s, int StyleNo);
	void __fastcall AddTextNLR(const Rvtypes::TRVRawByteString s, int StyleNo, int FirstParaNo, int OtherParaNo);
	void __fastcall AddTextNL(const System::UnicodeString s, int StyleNo, int FirstParaNo, int OtherParaNo);
	void __fastcall AddTextNLA(const Rvtypes::TRVAnsiString s, int StyleNo, int FirstParaNo, int OtherParaNo);
	void __fastcall AddTextBlockNLA(const Rvtypes::TRVAnsiString s, int StyleNo, int ParaNo);
	void __fastcall AddTab(int TextStyleNo, int ParaNo);
	void __fastcall AddBreak(void);
	int __fastcall AddCheckpoint(void);
	int __fastcall AddNamedCheckpoint(const System::UnicodeString CpName);
	int __fastcall AddNamedCheckpointEx(const System::UnicodeString CpName, bool RaiseEvent);
	void __fastcall AddPictureEx(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall AddHotPicture(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall AddHotspotEx(const Rvtypes::TRVAnsiString Name, int ImageIndex, int HotImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo);
	void __fastcall AddBulletEx(const Rvtypes::TRVAnsiString Name, int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo);
	void __fastcall AddControlEx(const Rvtypes::TRVAnsiString Name, Vcl::Controls::TControl* ctrl, int ParaNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall AddBreakEx(Rvstyle::TRVStyleLength Width, Rvstyle::TRVBreakStyle Style, System::Uitypes::TColor Color);
	void __fastcall AddNLRTag(const Rvtypes::TRVRawByteString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddNLTag(const System::UnicodeString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddRTag(const Rvtypes::TRVRawByteString s, int StyleNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddTag(const System::UnicodeString s, int StyleNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddBreakTag(const Rvstyle::TRVTag Tag);
	int __fastcall AddCheckpointTag(const Rvstyle::TRVTag Tag);
	int __fastcall AddNamedCheckpointExTag(const System::UnicodeString CpName, bool RaiseEvent, const Rvstyle::TRVTag Tag);
	void __fastcall AddPictureExTag(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign, const Rvstyle::TRVTag Tag);
	void __fastcall AddHotPictureTag(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, int ParaNo, Rvstyle::TRVVAlign VAlign, const Rvstyle::TRVTag Tag);
	void __fastcall AddHotspotExTag(const Rvtypes::TRVAnsiString Name, int ImageIndex, int HotImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddBulletExTag(const Rvtypes::TRVAnsiString Name, int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddControlExTag(const Rvtypes::TRVAnsiString Name, Vcl::Controls::TControl* ctrl, int ParaNo, Rvstyle::TRVVAlign VAlign, const Rvstyle::TRVTag Tag);
	void __fastcall AddBreakExTag(Rvstyle::TRVStyleLength Width, Rvstyle::TRVBreakStyle Style, System::Uitypes::TColor Color, const Rvstyle::TRVTag Tag);
	void __fastcall AddFromNewLine _DEPRECATED_ATTRIBUTE0 (const System::UnicodeString s, int StyleNo);
	void __fastcall AddCenterLine _DEPRECATED_ATTRIBUTE0 (const System::UnicodeString s, int StyleNo);
	void __fastcall AddText _DEPRECATED_ATTRIBUTE0 (const System::UnicodeString s, int StyleNo);
	void __fastcall AddTextFromNewLine _DEPRECATED_ATTRIBUTE0 (const System::UnicodeString s, int StyleNo);
	void __fastcall AddPicture _DEPRECATED_ATTRIBUTE0 (Vcl::Graphics::TGraphic* gr);
	void __fastcall AddHotspot _DEPRECATED_ATTRIBUTE0 (int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList, bool fromnewline);
	void __fastcall AddBullet _DEPRECATED_ATTRIBUTE0 (int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList, bool fromnewline);
	void __fastcall AddControl _DEPRECATED_ATTRIBUTE0 (Vcl::Controls::TControl* ctrl, bool center);
	int __fastcall GetCheckpointY(int no);
	Rvstyle::TCheckpointData __fastcall GetFirstCheckpoint(void);
	Rvstyle::TCheckpointData __fastcall GetNextCheckpoint(Rvstyle::TCheckpointData CheckpointData);
	Rvstyle::TCheckpointData __fastcall GetLastCheckpoint(void);
	Rvstyle::TCheckpointData __fastcall GetPrevCheckpoint(Rvstyle::TCheckpointData CheckpointData);
	Rvstyle::TCheckpointData __fastcall GetItemCheckpoint(int ItemNo);
	Rvstyle::TCheckpointData __fastcall FindCheckpointByName(const System::UnicodeString Name);
	Rvstyle::TCheckpointData __fastcall FindCheckpointByTag(const Rvstyle::TRVTag Tag);
	Rvstyle::TCheckpointData __fastcall GetCheckpointByNo(int No);
	void __fastcall GetCheckpointInfo(Rvstyle::TCheckpointData CheckpointData, Rvstyle::TRVTag &Tag, System::UnicodeString &Name, bool &RaiseEvent);
	void __fastcall GetCheckpointXY(Rvstyle::TCheckpointData CheckpointData, int &X, int &Y);
	int __fastcall GetCheckpointYEx(Rvstyle::TCheckpointData CheckpointData);
	int __fastcall GetCheckpointItemNo(Rvstyle::TCheckpointData CheckpointData);
	int __fastcall GetCheckpointNo(Rvstyle::TCheckpointData CheckpointData);
	int __fastcall GetJumpPointY(int id);
	int __fastcall GetJumpPointItemNo _DEPRECATED_ATTRIBUTE1("Use GetJumpPointLocation") (int id);
	void __fastcall GetJumpPointLocation(int id, Crvfdata::TCustomRVFormattedData* &RVData, int &ItemNo);
	bool __fastcall GetItemCoords(int ItemNo, int &Left, int &Top);
	bool __fastcall GetItemClientCoords(int ItemNo, int &Left, int &Top);
	bool __fastcall GetItemCoordsEx(Crvfdata::TCustomRVFormattedData* RVData, int ItemNo, int Part, bool AllowFloating, System::Types::TRect &R);
	void __fastcall Clear(void);
	void __fastcall ClearAll(void);
	void __fastcall Format(void);
	void __fastcall FormatAll(void);
	void __fastcall Reformat(void);
	void __fastcall ReformatAll(void);
	void __fastcall FormatTail(void);
	void __fastcall AppendFrom(TCustomRichView* Source);
	bool __fastcall SaveHTMLToStreamEx(System::Classes::TStream* Stream, const System::UnicodeString Path, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, const System::UnicodeString ExtraStyles, const System::UnicodeString ExternalCSS, const System::UnicodeString CPPrefix, Rvstyle::TRVSaveOptions Options);
	bool __fastcall SaveHTMLToStream(System::Classes::TStream* Stream, const System::UnicodeString Path, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, Rvstyle::TRVSaveOptions Options);
	bool __fastcall SaveHTMLEx(const System::UnicodeString FileName, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, const System::UnicodeString ExtraStyles, const System::UnicodeString ExternalCSS, const System::UnicodeString CPPrefix, Rvstyle::TRVSaveOptions Options);
	bool __fastcall SaveHTML(const System::UnicodeString FileName, const System::UnicodeString Title, const System::UnicodeString ImagesPrefix, Rvstyle::TRVSaveOptions Options);
	bool __fastcall SaveText(const System::UnicodeString FileName, int LineWidth, unsigned CodePage = (unsigned)(0x0));
	bool __fastcall SaveTextToStream(const System::UnicodeString Path, System::Classes::TStream* Stream, int LineWidth, bool SelectionOnly, bool TextOnly, unsigned CodePage = (unsigned)(0x0));
	bool __fastcall LoadText(const System::UnicodeString FileName, int StyleNo, int ParaNo, bool AsSingleParagraph, unsigned CodePage = (unsigned)(0x0));
	bool __fastcall LoadTextFromStream(System::Classes::TStream* Stream, int StyleNo, int ParaNo, bool AsSingleParagraph, unsigned CodePage = (unsigned)(0x0));
	Crvdata::TRVLayoutInfo* __fastcall CreateLayoutInfo(void);
	DYNAMIC void __fastcall ApplyLayoutInfo(Crvdata::TRVLayoutInfo* Layout);
	bool __fastcall LoadRVFFromStream(System::Classes::TStream* Stream);
	bool __fastcall InsertRVFFromStream(System::Classes::TStream* Stream, int Index);
	bool __fastcall AppendRVFFromStream(System::Classes::TStream* Stream, int ParaNo);
	bool __fastcall LoadRVF(const System::UnicodeString FileName);
	bool __fastcall SaveRVFToStream(System::Classes::TStream* Stream, bool SelectionOnly);
	bool __fastcall SaveRVF(const System::UnicodeString FileName, bool SelectionOnly);
	void __fastcall CopyRVF(void);
	bool __fastcall SaveRTFToStream(System::Classes::TStream* Stream, bool SelectionOnly);
	bool __fastcall SaveRTF(const System::UnicodeString FileName, bool SelectionOnly);
	void __fastcall CopyRTF(void);
	bool __fastcall LoadRTFFromStream(System::Classes::TStream* Stream);
	bool __fastcall LoadRTF(const System::UnicodeString FileName);
	DYNAMIC bool __fastcall SaveDocXToStream(System::Classes::TStream* Stream, bool SelectionOnly);
	bool __fastcall SaveDocX(const System::UnicodeString FileName, bool SelectionOnly);
	bool __fastcall LoadFromStream(System::Classes::TStream* Stream, TRVYesNoAuto IsTextUnicode);
	void __fastcall AddNLATag(const Rvtypes::TRVAnsiString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	bool __fastcall SaveTextW(const System::UnicodeString FileName, int LineWidth);
	bool __fastcall SaveTextToStreamW(const System::UnicodeString Path, System::Classes::TStream* Stream, int LineWidth, bool SelectionOnly, bool TextOnly);
	bool __fastcall LoadTextW(const System::UnicodeString FileName, int StyleNo, int ParaNo, bool DefAsSingleParagraph);
	bool __fastcall LoadTextFromStreamW(System::Classes::TStream* Stream, int StyleNo, int ParaNo, bool DefAsSingleParagraph);
	void __fastcall SetItemTextA(int ItemNo, const Rvtypes::TRVAnsiString s);
	void __fastcall AddNLWTag(const Rvtypes::TRVUnicodeString s, int StyleNo, int ParaNo, const Rvstyle::TRVTag Tag);
	void __fastcall AddTextNLW(const Rvtypes::TRVUnicodeString s, int StyleNo, int FirstParaNo, int OtherParaNo, bool DefAsSingleParagraph);
	Rvtypes::TRVUnicodeString __fastcall GetSelTextW(void);
	Rvtypes::TRVUnicodeString __fastcall GetItemTextW(int ItemNo);
	void __fastcall SetItemTextW(int ItemNo, const Rvtypes::TRVUnicodeString s);
	Rvtypes::TRVAnsiString __fastcall GetItemTextA(int ItemNo);
	void __fastcall DeleteSection(const System::UnicodeString CpName);
	void __fastcall DeleteItems(int FirstItemNo, int Count);
	void __fastcall DeleteParas(int FirstItemNo, int LastItemNo);
	void __fastcall CopyTextA(void);
	void __fastcall CopyTextW(void);
	void __fastcall CopyText(void);
	void __fastcall CopyImage(void);
	void __fastcall Copy(void);
	bool __fastcall CopyDef(void);
	Vcl::Graphics::TGraphic* __fastcall GetSelectedImage(void);
	Rvtypes::TRVAnsiString __fastcall GetSelTextA(void);
	System::UnicodeString __fastcall GetSelText(void);
	bool __fastcall SelectionExists(void);
	void __fastcall Deselect(void);
	void __fastcall SelectAll(void);
	bool __fastcall SearchTextA(const Rvtypes::TRVAnsiString s, Rvscroll::TRVSearchOptions SrchOptions);
	bool __fastcall SearchTextW(const Rvtypes::TRVUnicodeString s, Rvscroll::TRVSearchOptions SrchOptions);
	bool __fastcall SearchText(const System::UnicodeString s, Rvscroll::TRVSearchOptions SrchOptions);
	int __fastcall GetItemStyle(int ItemNo);
	void __fastcall GetBreakInfo(int ItemNo, Rvstyle::TRVStyleLength &AWidth, Rvstyle::TRVBreakStyle &AStyle, System::Uitypes::TColor &AColor, Rvstyle::TRVTag &ATag);
	void __fastcall GetBulletInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, int &AImageIndex, Vcl::Imglist::TCustomImageList* &AImageList, Rvstyle::TRVTag &ATag);
	void __fastcall GetHotspotInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, int &AImageIndex, int &AHotImageIndex, Vcl::Imglist::TCustomImageList* &AImageList, Rvstyle::TRVTag &ATag);
	void __fastcall GetPictureInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, Vcl::Graphics::TGraphic* &Agr, Rvstyle::TRVVAlign &AVAlign, Rvstyle::TRVTag &ATag);
	void __fastcall GetControlInfo(int ItemNo, Rvtypes::TRVAnsiString &AName, Vcl::Controls::TControl* &Actrl, Rvstyle::TRVVAlign &AVAlign, Rvstyle::TRVTag &ATag);
	void __fastcall GetTextInfo(int ItemNo, System::UnicodeString &AText, Rvstyle::TRVTag &ATag);
	Rvstyle::TRVTag __fastcall GetItemTag(int ItemNo);
	Rvstyle::TRVVAlign __fastcall GetItemVAlign(int ItemNo);
	void __fastcall SetItemTextR(int ItemNo, const Rvtypes::TRVRawByteString s);
	void __fastcall SetItemText(int ItemNo, const System::UnicodeString s);
	Rvtypes::TRVRawByteString __fastcall GetItemTextR(int ItemNo);
	System::UnicodeString __fastcall GetItemText(int ItemNo);
	bool __fastcall SetItemExtraIntProperty(int ItemNo, Rvitem::TRVExtraItemProperty Prop, int Value);
	bool __fastcall GetItemExtraIntProperty(int ItemNo, Rvitem::TRVExtraItemProperty Prop, int &Value);
	bool __fastcall SetItemExtraIntPropertyEx(int ItemNo, int Prop, int Value);
	bool __fastcall GetItemExtraIntPropertyEx(int ItemNo, int Prop, int &Value);
	bool __fastcall SetItemExtraStrProperty(int ItemNo, Rvitem::TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
	bool __fastcall GetItemExtraStrProperty(int ItemNo, Rvitem::TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	bool __fastcall SetItemExtraStrPropertyEx(int ItemNo, int Prop, const System::UnicodeString Value);
	bool __fastcall GetItemExtraStrPropertyEx(int ItemNo, int Prop, System::UnicodeString &Value);
	bool __fastcall IsParaStart(int ItemNo);
	int __fastcall GetItemPara(int ItemNo);
	bool __fastcall IsFromNewLine(int ItemNo);
	void __fastcall SetBreakInfo(int ItemNo, Rvstyle::TRVStyleLength AWidth, Rvstyle::TRVBreakStyle AStyle, System::Uitypes::TColor AColor, const Rvstyle::TRVTag ATag);
	void __fastcall SetBulletInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, int AImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	void __fastcall SetHotspotInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, int AImageIndex, int AHotImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	bool __fastcall SetPictureInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, Vcl::Graphics::TGraphic* Agr, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	bool __fastcall SetControlInfo(int ItemNo, const Rvtypes::TRVAnsiString AName, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	void __fastcall SetItemTag(int ItemNo, const Rvstyle::TRVTag ATag);
	void __fastcall SetItemVAlign(int ItemNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall SetCheckpointInfo(int ItemNo, const Rvstyle::TRVTag ATag, const System::UnicodeString AName, bool ARaiseEvent);
	bool __fastcall RemoveCheckpoint(int ItemNo);
	int __fastcall FindControlItemNo(Vcl::Controls::TControl* actrl);
	bool __fastcall SelectControl(Vcl::Controls::TControl* actrl);
	void __fastcall GetSelectionBounds(int &StartItemNo, int &StartItemOffs, int &EndItemNo, int &EndItemOffs, bool Normalize);
	void __fastcall SetSelectionBounds(int StartItemNo, int StartItemOffs, int EndItemNo, int EndItemOffs);
	void __fastcall GetWordAt(int X, int Y, Crvfdata::TCustomRVFormattedData* &ARVData, int &AItemNo, System::UnicodeString &AWord)/* overload */;
	void __fastcall GetWordAtR(int X, int Y, Crvfdata::TCustomRVFormattedData* &ARVData, int &AItemNo, Rvtypes::TRVRawByteString &AWord);
	Rvtypes::TRVAnsiString __fastcall GetWordAtA(int X, int Y);
	Rvtypes::TRVUnicodeString __fastcall GetWordAtW(int X, int Y);
	System::UnicodeString __fastcall GetWordAt(int X, int Y)/* overload */;
	void __fastcall SelectWordAt(int X, int Y);
	DYNAMIC void __fastcall UpdatePaletteInfo(void);
	int __fastcall GetOffsBeforeItem(int ItemNo);
	int __fastcall GetOffsAfterItem(int ItemNo);
	void __fastcall SetAddParagraphMode(bool AllowNewPara);
	virtual System::UnicodeString __fastcall SavePicture(Rvstyle::TRVSaveFormat DocumentSaveFormat, const System::UnicodeString Path, Vcl::Graphics::TGraphic* gr);
	System::Types::TRect __fastcall GetSelectionRect(void);
	Rvitem::TCustomRVItemInfo* __fastcall GetItem(int ItemNo);
	int __fastcall GetItemNo(Rvitem::TCustomRVItemInfo* Item);
	void __fastcall GetFocusedItem(Crvfdata::TCustomRVFormattedData* &ARVData, int &AItemNo);
	void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	void __fastcall DeleteMarkedStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	void __fastcall DeleteUnusedStyles(bool TextStyles, bool ParaStyles, bool ListStyles);
	void __fastcall BeginOleDrag(void);
	virtual Rvdragdrop::TRVDropSourceClass __fastcall GetRVDropSourceClass(void);
	int __fastcall SetListMarkerInfo(int AItemNo, int AListNo, int AListLevel, int AStartFrom, int AParaNo, bool AUseStartFrom);
	void __fastcall RemoveListMarker(int ItemNo);
	int __fastcall GetListMarkerInfo(int AItemNo, int &AListNo, int &AListLevel, int &AStartFrom, bool &AUseStartFrom);
	void __fastcall RefreshListMarkers(void);
	int __fastcall GetLineNo(int ItemNo, int ItemOffs);
	bool __fastcall GetItemAt(int X, int Y, Crvfdata::TCustomRVFormattedData* &RVData, int &ItemNo, int &OffsetInItem, bool Strict);
	System::Types::TPoint __fastcall ClientToDocument(const System::Types::TPoint &APoint);
	System::Types::TPoint __fastcall DocumentToClient(const System::Types::TPoint &APoint);
	void __fastcall StartLiveSpelling(void);
	void __fastcall ClearLiveSpellingResults(void);
	void __fastcall LiveSpellingValidateWord(const System::UnicodeString AWord);
	void __fastcall LaterSetBackLiveSpellingTo(Crvdata::TCustomRVData* RVData, int ItemNo, int Offs);
	void __fastcall RemoveRVDataFromLiveSpelling(Crvdata::TCustomRVData* RVData);
	void __fastcall AdjustLiveSpellingOnKeyPress(Crvdata::TCustomRVData* RVData, int ItemNo, int Index, System::WideChar ch);
	void __fastcall AdjustLiveSpellingOnDelete(Crvdata::TCustomRVData* RVData, int ItemNo, int Index, int Count);
	void __fastcall LiveSpellingCheckCurrentItem(Crvdata::TCustomRVData* RVData, int ItemNo);
	void __fastcall StartAnimation(void);
	void __fastcall StopAnimation(void);
	void __fastcall ResetAnimation(void);
	DYNAMIC bool __fastcall ExecuteAction(System::Classes::TBasicAction* Action);
	virtual bool __fastcall UpdateAction(System::Classes::TBasicAction* Action);
	virtual void __fastcall Invalidate(void);
	virtual void __fastcall Update(void);
	void __fastcall DoOnBackBitmapChange(System::TObject* Sender);
	void __fastcall ConvertDocToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits);
	void __fastcall ConvertDocToTwips(void);
	void __fastcall ConvertDocToPixels(void);
	DYNAMIC void __fastcall RegisterStyleTemplatesChangeHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	DYNAMIC void __fastcall UnregisterStyleTemplatesChangeHandler(System::Classes::TComponent* Component);
	DYNAMIC void __fastcall StyleTemplatesChange(void);
	void __fastcall RegisterHScrollHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall RegisterVScrollHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall UnregisterHScrollHandler(System::Classes::TComponent* Component);
	void __fastcall UnregisterVScrollHandler(System::Classes::TComponent* Component);
	void __fastcall RegisterSaveHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall UnRegisterSaveHandler(System::Classes::TComponent* Component);
	void __fastcall RegisterClearHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall UnRegisterClearHandler(System::Classes::TComponent* Component);
	void __fastcall RegisterResizeHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall UnregisterResizeHandler(System::Classes::TComponent* Component);
	DYNAMIC void __fastcall Resize(void);
	void __fastcall DoBeforeSaving(void);
	void __fastcall DoOnClear(void);
	int __fastcall GetIntProperty(TRVIntProperty Prop);
	void __fastcall SetIntProperty(TRVIntProperty Prop, int Value);
	System::UnicodeString __fastcall GetStrProperty(TRVStrProperty Prop);
	void __fastcall SetStrProperty(TRVStrProperty Prop, const System::UnicodeString Value);
	Rvstyle::TRVLength __fastcall GetFloatProperty(TRVFloatProperty Prop);
	void __fastcall SetFloatProperty(TRVFloatProperty Prop, const Rvstyle::TRVLength Value);
	__property int LineCount = {read=GetLineCount, nodefault};
	__property int ItemCount = {read=GetLineCount, nodefault};
	__property int FirstItemVisible = {read=GetFirstItemVisible, nodefault};
	__property int LastItemVisible = {read=GetLastItemVisible, nodefault};
	__property Rvstyle::TRVFWarnings RVFWarnings = {read=FRVFWarnings, write=FRVFWarnings, nodefault};
	__property int DocumentHeight = {read=GetDocumentHeight, nodefault};
	__property bool PageBreaksBeforeItems[int Index] = {read=GetPageBreaksBeforeItems, write=SetPageBreaksBeforeItems};
	__property bool ClearLeft[int Index] = {read=GetClearLeft, write=SetClearLeft};
	__property bool ClearRight[int Index] = {read=GetClearRight, write=SetClearRight};
	__property TRVAnimationMode AnimationMode = {read=FAnimationMode, write=SetAnimationMode, default=1};
	__property Vcl::Graphics::TBitmap* BackgroundBitmap = {read=GetBackBitmap, write=SetBackBitmap};
	__property Rvscroll::TBackgroundStyle BackgroundStyle = {read=GetBackgroundStyle, write=SetBackgroundStyle, nodefault};
	__property int BottomMargin = {read=FBottomMargin, write=FBottomMargin, default=5};
	__property Color = {default=536870911};
	__property Rvscroll::TCPEventKind CPEventKind = {read=FCPEventKind, write=FCPEventKind, nodefault};
	__property System::Uitypes::TCursor Cursor = {read=FCursor, write=FCursor, nodefault};
	__property System::UnicodeString Delimiters = {read=FDelimiters, write=FDelimiters, stored=StoreDelimiters};
	__property DoInPaletteMode;
	__property int FirstJumpNo = {read=GetFirstJumpNo, write=SetFirstJumpNo, default=0};
	__property FullRedraw;
	__property HScrollVisible = {default=1};
	__property HScrollMax;
	__property HScrollPos;
	__property InplaceEditor;
	__property int LeftMargin = {read=FLeftMargin, write=FLeftMargin, default=5};
	__property TRVLiveSpellingMode LiveSpellingMode = {read=FLiveSpellingMode, write=FLiveSpellingMode, default=0};
	__property int MaxLength = {read=FMaxLength, write=FMaxLength, default=0};
	__property int MaxTextWidth = {read=FMaxTextWidth, write=FMaxTextWidth, default=0};
	__property int MinTextWidth = {read=FMinTextWidth, write=FMinTextWidth, default=0};
	__property Rvscroll::TRVOptions Options = {read=FOptions, write=FOptions, default=1702949};
	__property int RightMargin = {read=FRightMargin, write=FRightMargin, default=5};
	__property Rvstyle::TRVRTFOptions RTFOptions = {read=FRTFOptions, write=FRTFOptions, default=30};
	__property Rvrtfprops::TRVRTFReaderProperties* RTFReadProperties = {read=GetRTFReadProperties, write=SetRTFReadProperties};
	__property Rvdocparams::TRVDocParameters* DocParameters = {read=GetDocParameters, write=SetDocParameters, stored=StoreDocParameters};
	__property Rvstyle::TRVFOptions RVFOptions = {read=FRVFOptions, write=FRVFOptions, default=98435};
	__property Rvstyle::TRVFReaderStyleMode RVFParaStylesReadMode = {read=FRVFParaStylesReadMode, write=FRVFParaStylesReadMode, default=2};
	__property Rvstyle::TRVFReaderStyleMode RVFTextStylesReadMode = {read=FRVFTextStylesReadMode, write=FRVFTextStylesReadMode, default=2};
	__property Rvstyle::TRVStyle* Style = {read=FStyle, write=SetStyle};
	__property Rvscroll::TRVTabNavigationType TabNavigation = {read=GetTabNavigation, write=SetTabNavigation, default=1};
	__property int TopMargin = {read=FTopMargin, write=FTopMargin, default=5};
	__property System::Classes::TStringList* DocProperties = {read=FDocProperties, write=SetDocProperties};
	__property VScrollMax;
	__property VScrollPos;
	__property VScrollVisible = {default=1};
	__property VSmallStep;
	__property Rvpopup::TRVSmartPopupProperties* SmartPopupProperties = {read=GetSmartPopupProperties, write=SetSmartPopupProperties};
	__property bool SmartPopupVisible = {read=GetSmartPopupVisible, write=SetSmartPopupVisible, nodefault};
	__property Vcl::Stdctrls::TTextLayout VAlign = {read=FVAlign, write=FVAlign, default=0};
	__property bool WordWrap = {read=FWordWrap, write=FWordWrap, default=1};
	__property bool UseStyleTemplates = {read=FUseStyleTemplates, write=FUseStyleTemplates, default=0};
	__property Rvstyle::TRVStyleTemplateInsertMode StyleTemplateInsertMode = {read=FStyleTemplateInsertMode, write=FStyleTemplateInsertMode, default=0};
	__property bool SelectionHandlesVisible = {read=GetSelectionHandlesVisible, write=SetSelectionHandlesVisible, nodefault};
	__property TRVDblClickEvent OnRVDblClick = {read=FOnRVDblClick, write=FOnRVDblClick};
	__property TRVCheckpointVisibleEvent OnCheckpointVisible = {read=FOnCheckpointVisible, write=FOnCheckpointVisible};
	__property TRVControlActionEvent OnControlAction = {read=FOnControlAction, write=FOnControlAction};
	__property TRVItemActionEvent OnItemAction = {read=FOnItemAction, write=FOnItemAction};
	__property System::Classes::TNotifyEvent OnCopy = {read=FOnCopy, write=FOnCopy};
	__property TRVImportPictureEvent OnImportPicture = {read=FOnImportPicture, write=FOnImportPicture};
	__property TRVItemHintEvent OnItemHint = {read=FOnItemHint, write=FOnItemHint};
	__property TJumpEvent OnJump = {read=FOnJump, write=FOnJump};
	__property TRVGetItemCursorEvent OnGetItemCursor = {read=FOnGetItemCursor, write=FOnGetItemCursor};
	__property TRVHTMLSaveImageEvent OnHTMLSaveImage = {read=FOnHTMLSaveImage, write=FOnHTMLSaveImage};
	__property TRVSaveImageEvent2 OnSaveImage2 = {read=FOnSaveImage2, write=FOnSaveImage2};
	__property TRVReadHyperlink OnReadHyperlink = {read=FOnReadHyperlink, write=FOnReadHyperlink};
	__property TRVWriteHyperlink OnWriteHyperlink = {read=FOnWriteHyperlink, write=FOnWriteHyperlink};
	__property TRVURLNeededEvent OnURLNeeded = {read=FOnURLNeeded, write=FOnURLNeeded};
	__property TRVMouseEvent OnRVMouseDown = {read=FOnRVMouseDown, write=FOnRVMouseDown};
	__property TRVMouseMoveEvent OnRVMouseMove = {read=FOnRVMouseMove, write=FOnRVMouseMove};
	__property TRVMouseEvent OnRVMouseUp = {read=FOnRVMouseUp, write=FOnRVMouseUp};
	__property TRVRightClickEvent OnRVRightClick = {read=FOnRVRightClick, write=FOnRVRightClick};
	__property TRVFControlNeededEvent OnRVFControlNeeded = {read=FOnRVFControlNeeded, write=FOnRVFControlNeeded};
	__property TRVFImageListNeededEvent OnRVFImageListNeeded = {read=FOnRVFImageListNeeded, write=FOnRVFImageListNeeded};
	__property TRVFPictureNeededEvent OnRVFPictureNeeded = {read=FOnRVFPictureNeeded, write=FOnRVFPictureNeeded};
	__property TRVSaveComponentToFileEvent OnSaveComponentToFile = {read=FOnSaveComponentToFile, write=FOnSaveComponentToFile};
	__property TRVSaveItemToFileEvent OnSaveItemToFile = {read=FOnSaveItemToFile, write=FOnSaveItemToFile};
	__property System::Classes::TNotifyEvent OnSelect = {read=FOnSelect, write=FOnSelect};
	__property TRVSaveRTFExtraEvent OnSaveRTFExtra = {read=FOnSaveRTFExtra, write=FOnSaveRTFExtra};
	__property TRVSaveDocXExtraEvent OnSaveDocXExtra = {read=FOnSaveDocXExtra, write=FOnSaveDocXExtra};
	__property TRVSaveHTMLExtraEvent OnSaveHTMLExtra = {read=FOnSaveHTMLExtra, write=FOnSaveHTMLExtra};
	__property TRVSaveParaToHTMLEvent OnSaveParaToHTML = {read=FOnSaveParaToHTML, write=FOnSaveParaToHTML};
	__property System::Classes::TNotifyEvent OnStyleTemplatesChange = {read=FOnStyleTemplatesChange, write=FOnStyleTemplatesChange};
	__property TRVProgressEvent OnProgress = {read=FOnProgress, write=FOnProgress};
	__property TRVSpellingCheckEvent OnSpellingCheck = {read=FOnSpellingCheck, write=FOnSpellingCheck};
	__property TRVSmartPopupClickEvent OnSmartPopupClick = {read=FOnSmartPopupClick, write=FOnSmartPopupClick};
	__property TRVGetSRVInterfaceEvent OnGetScaleRichViewInterface = {read=FOnGetScaleRichViewInterface, write=FOnGetScaleRichViewInterface};
	__property TRVAddStyleEvent OnAddStyle = {read=FOnAddStyle, write=FOnAddStyle};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRichView(HWND ParentWindow) : Rvscroll::TRVScroller(ParentWindow) { }
	
};


class DELPHICLASS TRichView;
class PASCALIMPLEMENTATION TRichView : public TCustomRichView
{
	typedef TCustomRichView inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Constraints;
	__property Color = {default=536870911};
	__property Ctl3D;
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property HelpContext = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Touch;
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGesture;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseMove;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnResize;
	__property OnStartDrag;
	__property AnimationMode = {default=1};
	__property BackgroundBitmap;
	__property BackgroundStyle = {default=0};
	__property BiDiMode = {default=0};
	__property BorderStyle = {default=1};
	__property BottomMargin = {default=5};
	__property CPEventKind = {default=0};
	__property Cursor = {default=0};
	__property Delimiters = {default=0};
	__property DocParameters;
	__property DoInPaletteMode;
	__property FirstJumpNo = {default=0};
	__property HScrollVisible = {default=1};
	__property LeftMargin = {default=5};
	__property MaxLength = {default=0};
	__property MaxTextWidth = {default=0};
	__property MinTextWidth = {default=0};
	__property Options = {default=1702949};
	__property RightMargin = {default=5};
	__property RTFOptions = {default=30};
	__property RTFReadProperties;
	__property RVFOptions = {default=98435};
	__property RVFParaStylesReadMode = {default=2};
	__property RVFTextStylesReadMode = {default=2};
	__property Style;
	__property TabNavigation = {default=1};
	__property TopMargin = {default=5};
	__property Tracking = {default=1};
	__property UseXPThemes = {default=1};
	__property UseStyleTemplates = {default=0};
	__property StyleTemplateInsertMode = {default=0};
	__property VAlign = {default=0};
	__property VScrollVisible = {default=1};
	__property WheelStep = {default=2};
	__property WordWrap = {default=1};
	__property OnAddStyle;
	__property OnCheckpointVisible;
	__property OnControlAction;
	__property OnCopy;
	__property OnGetItemCursor;
	__property OnImportPicture;
	__property OnItemAction;
	__property OnItemHint;
	__property OnJump;
	__property OnHScrolled;
	__property OnHTMLSaveImage;
	__property OnPaint;
	__property OnProgress;
	__property OnReadHyperlink;
	__property OnRVDblClick;
	__property OnRVFImageListNeeded;
	__property OnRVFControlNeeded;
	__property OnRVFPictureNeeded;
	__property OnRVMouseDown;
	__property OnRVMouseMove;
	__property OnRVMouseUp;
	__property OnRVRightClick;
	__property OnSaveComponentToFile;
	__property OnSaveDocXExtra;
	__property OnSaveHTMLExtra;
	__property OnSaveImage2;
	__property OnSaveItemToFile;
	__property OnSaveRTFExtra;
	__property OnSelect;
	__property OnSpellingCheck;
	__property OnStyleTemplatesChange;
	__property OnVScrolled;
	__property OnWriteHyperlink;
	__property AllowSelection;
	__property SingleClick;
	__property OnURLNeeded;
public:
	/* TCustomRichView.Create */ inline __fastcall virtual TRichView(System::Classes::TComponent* AOwner) : TCustomRichView(AOwner) { }
	/* TCustomRichView.Destroy */ inline __fastcall virtual ~TRichView(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRichView(HWND ParentWindow) : TCustomRichView(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word WM_RVDRAGDROP = System::Word(0x410);
static const System::Word WM_RVRELOAD = System::Word(0x411);
static const System::Word WM_RVEVENT = System::Word(0x40f);
static const System::Int8 RV_TIMERID_SCROLLING = System::Int8(0x1);
static const System::Int8 RV_TIMERID_ANIMATION = System::Int8(0x2);
static const System::Int8 RV_TIMERID_CUSTOMCARET = System::Int8(0x3);
extern DELPHI_PACKAGE TRVWordEnumThreadClass RVWordEnumThreadClass;
extern DELPHI_PACKAGE bool RichViewLMouseScroll;
}	/* namespace Richview */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RICHVIEW)
using namespace Richview;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RichviewHPP
