﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVWinGrIn.pas' rev: 27.00 (Windows)

#ifndef RvwingrinHPP
#define RvwingrinHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Winapi.CommCtrl.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvwingrin
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVWinGraphicInterface;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVWinGraphicInterface : public Rvgrin::TRVGraphicInterface
{
	typedef Rvgrin::TRVGraphicInterface inherited;
	
public:
	virtual void __fastcall TextOut_A(Vcl::Graphics::TCanvas* Canvas, int X, int Y, char * Str, int Length);
	virtual void __fastcall TextOut_W(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::WideChar * Str, int Length);
	virtual void __fastcall TextOut_(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::WideChar * Str, int Length);
	virtual void __fastcall ExtTextOut_A(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int Options, System::Types::PRect Rect, char * Str, int Length, System::PInteger Dx);
	virtual void __fastcall ExtTextOut_W(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int Options, System::Types::PRect Rect, System::WideChar * Str, int Length, System::PInteger Dx);
	virtual void __fastcall ExtTextOut_(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int Options, System::Types::PRect Rect, System::WideChar * Str, int Length, System::PInteger Dx);
	virtual void __fastcall DrawText(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, System::Types::TRect &R, System::Classes::TAlignment Alignment);
	virtual void __fastcall GetTextExtentPoint32_A(Vcl::Graphics::TCanvas* Canvas, char * Str, int Length, System::Types::TSize &Size);
	virtual void __fastcall GetTextExtentPoint32_W(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, System::Types::TSize &Size);
	virtual void __fastcall GetTextExtentPoint32_(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, System::Types::TSize &Size);
	virtual void __fastcall GetTextExtentExPoint_A(Vcl::Graphics::TCanvas* Canvas, char * Str, int Length, int MaxExtent, System::PInteger PFit, System::PInteger PDX, System::Types::TSize &Size);
	virtual void __fastcall GetTextExtentExPoint_W(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, int MaxExtent, System::PInteger PFit, System::PInteger PDX, System::Types::TSize &Size);
	virtual unsigned __fastcall GetCharacterPlacement_A(Vcl::Graphics::TCanvas* Canvas, char * Str, int Length, tagGCP_RESULTSA &Results, bool Ligate);
	virtual unsigned __fastcall GetCharacterPlacement_W(Vcl::Graphics::TCanvas* Canvas, System::WideChar * Str, int Length, tagGCP_RESULTSA &Results, bool Ligate);
	virtual unsigned __fastcall GetOutlineTextMetrics(Vcl::Graphics::TCanvas* Canvas, unsigned Size, Winapi::Windows::POutlineTextmetricW POTM);
	virtual bool __fastcall GetTextMetrics(Vcl::Graphics::TCanvas* Canvas, tagTEXTMETRICW &TM);
	virtual bool __fastcall FontHasLigationGlyphs(Vcl::Graphics::TCanvas* Canvas);
	virtual void __fastcall SetTextAlign(Vcl::Graphics::TCanvas* Canvas, unsigned Flags);
	virtual unsigned __fastcall GetTextAlign(Vcl::Graphics::TCanvas* Canvas);
	virtual int __fastcall GetTextCharacterExtra(Vcl::Graphics::TCanvas* Canvas);
	virtual void __fastcall SetTextCharacterExtra(Vcl::Graphics::TCanvas* Canvas, int CharExtra);
	virtual void __fastcall SetLogFont(Vcl::Graphics::TCanvas* Canvas, const tagLOGFONTW &LogFont);
	virtual NativeUInt __fastcall SetFontHandle(Vcl::Graphics::TCanvas* Canvas, const NativeUInt FontHandle);
	virtual void __fastcall SetDiagonalBrush(Vcl::Graphics::TCanvas* Canvas, System::Uitypes::TColor BackColor, System::Uitypes::TColor ForeColor, int XOrg, int YOrg);
	virtual void __fastcall GetViewportExtEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TSize &Size);
	virtual void __fastcall GetViewportOrgEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TPoint &Point);
	virtual void __fastcall GetWindowExtEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TSize &Size);
	virtual void __fastcall GetWindowOrgEx(Vcl::Graphics::TCanvas* Canvas, System::Types::TPoint &Point);
	virtual void __fastcall SetViewportExtEx(Vcl::Graphics::TCanvas* Canvas, int XExt, int YExt, System::Types::PSize Size);
	virtual void __fastcall SetViewportOrgEx(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::Types::PPoint Point);
	virtual void __fastcall SetWindowExtEx(Vcl::Graphics::TCanvas* Canvas, int XExt, int YExt, System::Types::PSize Size);
	virtual void __fastcall SetWindowOrgEx(Vcl::Graphics::TCanvas* Canvas, int X, int Y, System::Types::PPoint Point);
	virtual int __fastcall GetMapMode(Vcl::Graphics::TCanvas* Canvas);
	virtual int __fastcall SetMapMode(Vcl::Graphics::TCanvas* Canvas, int Mode);
	virtual bool __fastcall IsAdvancedGraphicsMode(Vcl::Graphics::TCanvas* Canvas);
	virtual bool __fastcall SetGraphicsMode(Vcl::Graphics::TCanvas* Canvas, bool Advanced);
	virtual bool __fastcall GetWorldTransform(Vcl::Graphics::TCanvas* Canvas, tagXFORM &XForm);
	virtual bool __fastcall SetWorldTransform(Vcl::Graphics::TCanvas* Canvas, const tagXFORM &XForm);
	virtual bool __fastcall ModifyWorldTransform(Vcl::Graphics::TCanvas* Canvas, const tagXFORM &XForm);
	virtual void __fastcall InvalidateRect(Vcl::Controls::TWinControl* Control, System::Types::PRect lpRect);
	virtual void __fastcall IntersectClipRect(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2);
	virtual void __fastcall IntersectClipRectEx(Vcl::Graphics::TCanvas* Canvas, int X1, int Y1, int X2, int Y2, NativeUInt &Rgn, bool &RgnValid);
	virtual void __fastcall RemoveClipRgn(Vcl::Graphics::TCanvas* Canvas, NativeUInt &Rgn, bool &RgnValid);
	virtual void __fastcall RestoreClipRgn(Vcl::Graphics::TCanvas* Canvas, NativeUInt Rgn, bool RgnValid);
	virtual int __fastcall SaveCanvasState(Vcl::Graphics::TCanvas* Canvas);
	virtual void __fastcall RestoreCanvasState(Vcl::Graphics::TCanvas* Canvas, int State);
	virtual void __fastcall FillColorRect(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, System::Uitypes::TColor Color);
	virtual void __fastcall Polyline(Vcl::Graphics::TCanvas* Canvas, void *Points, int Count);
	virtual void __fastcall DrawBitmap(Vcl::Graphics::TCanvas* Canvas, int X, int Y, Vcl::Graphics::TBitmap* bmp);
	virtual void __fastcall StretchDrawBitmap(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, Vcl::Graphics::TBitmap* bmp);
	virtual void __fastcall PrintBitmap(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, Vcl::Graphics::TBitmap* bmp);
	virtual void __fastcall DrawImageList(Vcl::Graphics::TCanvas* Canvas, Vcl::Imglist::TCustomImageList* ImageList, int Index, int X, int Y, System::Uitypes::TColor BlendColor);
	virtual Vcl::Graphics::TCanvas* __fastcall CreateScreenCanvas(void);
	virtual void __fastcall DestroyScreenCanvas(Vcl::Graphics::TCanvas* Canvas);
	virtual Vcl::Graphics::TCanvas* __fastcall CreateCompatibleCanvas(Vcl::Graphics::TCanvas* Canvas);
	virtual void __fastcall DestroyCompatibleCanvas(Vcl::Graphics::TCanvas* Canvas);
	virtual Vcl::Graphics::TCanvas* __fastcall CreatePrinterCanvas(void);
	virtual int __fastcall GetDeviceCaps(Vcl::Graphics::TCanvas* Canvas, int Index);
public:
	/* TObject.Create */ inline __fastcall TRVWinGraphicInterface(void) : Rvgrin::TRVGraphicInterface() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVWinGraphicInterface(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvwingrin */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVWINGRIN)
using namespace Rvwingrin;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvwingrinHPP
