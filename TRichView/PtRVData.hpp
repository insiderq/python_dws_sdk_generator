﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'PtRVData.pas' rev: 27.00 (Windows)

#ifndef PtrvdataHPP
#define PtrvdataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVSidenote.hpp>	// Pascal unit
#include <RVFloatingPos.hpp>	// Pascal unit
#include <RVFloatingBox.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVFontCache.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <RVCtrlData.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Ptrvdata
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVFootnoteRefList;
class DELPHICLASS TRVFootnotePtblRVData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFootnoteRefList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	TRVFootnotePtblRVData* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVFootnotePtblRVData* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVFootnotePtblRVData* const Value);
	
public:
	void __fastcall DeleteByFootnote(Rvnote::TRVFootnoteItemInfo* Footnote);
	HIDESBASE void __fastcall Sort(void);
	int __fastcall GetFootnoteIndex(Rvnote::TRVFootnoteItemInfo* Footnote);
	__property TRVFootnotePtblRVData* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVFootnoteRefList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVFootnoteRefList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSidenoteList;
class DELPHICLASS TRVSidenotePtblRVData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSidenoteList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVSidenotePtblRVData* operator[](int Index) { return Items[Index]; }
	
private:
	TRVSidenotePtblRVData* __fastcall GetItems(int Index);
	void __fastcall SetItems(int Index, TRVSidenotePtblRVData* const Value);
	
public:
	__property TRVSidenotePtblRVData* Items[int Index] = {read=GetItems, write=SetItems/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVSidenoteList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVSidenoteList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVMultiDrawItemPartsList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMultiDrawItemPartsList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	Rvitem::TRVMultiDrawItemPart* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE Rvitem::TRVMultiDrawItemPart* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, Rvitem::TRVMultiDrawItemPart* const Value);
	
public:
	__property Rvitem::TRVMultiDrawItemPart* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVMultiDrawItemPartsList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVMultiDrawItemPartsList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVMultiDrawItemInfo;
class DELPHICLASS TCustomPrintableRVData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVMultiDrawItemInfo : public Dlines::TRVComplexLineHeightDrawLineInfo
{
	typedef Dlines::TRVComplexLineHeightDrawLineInfo inherited;
	
private:
	TRVMultiDrawItemPartsList* FPartsList;
	
public:
	__fastcall TRVMultiDrawItemInfo(void);
	__fastcall virtual ~TRVMultiDrawItemInfo(void);
	DYNAMIC void __fastcall ResetPages(TRVFootnoteRefList* &FootnoteRVDataList, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall UnformatLastPage(TRVFootnoteRefList* &FootnoteRVDataList, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall AddAllFootnotes(TRVFootnoteRefList* &FootnoteRVDataList, int &Height, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall DecHeightByFootnotes(int &Height, bool &ThisPageHasFootnotes);
	DYNAMIC void __fastcall RemoveAllFootnotes(TRVFootnoteRefList* &FootnoteRVDataList, int &Height, bool FootnotesChangeHeight);
	DYNAMIC bool __fastcall GetPartNo(Crvdata::TCustomRVData* RVData, int ItemNo, int OffsInItem, int &PartNo, int &LocalPageNo, int &LocalPageCount, TCustomPrintableRVData* &PtRVData);
	DYNAMIC bool __fastcall GetSubDocCoords(Rvitem::TRVStoreSubRVData* Location, int Part, System::Types::TRect &InnerCoords, System::Types::TRect &OuterCoords, System::Types::TPoint &Origin);
	DYNAMIC int __fastcall GetLocalPageNoFor(Rvitem::TRVStoreSubRVData* Location, int Part);
	DYNAMIC TCustomPrintableRVData* __fastcall GetPrintableRVDataFor(Rvitem::TRVStoreSubRVData* Location);
	__property TRVMultiDrawItemPartsList* PartsList = {read=FPartsList};
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVMultiDrawItemInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : Dlines::TRVComplexLineHeightDrawLineInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVPageInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVPageInfo : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
public:
	int StartY;
	int StartDrawItemNo;
	int StartPart;
	int StartY2;
	int DocumentHeight;
	TRVFootnoteRefList* FootnoteRVDataList;
	TRVSidenoteList* Sidenotes;
	__fastcall virtual ~TRVPageInfo(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
public:
	/* TCollectionItem.Create */ inline __fastcall virtual TRVPageInfo(System::Classes::TCollection* Collection) : System::Classes::TCollectionItem(Collection) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVPageCollection;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVPageCollection : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TRVPageInfo* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVPageInfo* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TRVPageInfo* const Value);
	
public:
	__fastcall TRVPageCollection(void);
	HIDESBASE TRVPageInfo* __fastcall Add(void);
	__property TRVPageInfo* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TRVPageCollection(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TCustomPrintableRVData : public Rvrvdata::TRichViewRVData
{
	typedef Rvrvdata::TRichViewRVData inherited;
	
private:
	Vcl::Graphics::TBitmap* FBackgroundBmp;
	Dlines::TRVDrawLineInfo* FDrawItem;
	int FItemTop;
	
protected:
	virtual Rvback::TRVBackground* __fastcall GetBack(void);
	virtual int __fastcall GetTopCoord(int PageNo, int GlobalPageNo);
	virtual int __fastcall GetTopCoord2(int PageNo, int GlobalPageNo);
	virtual int __fastcall GetPrintableAreaTop(int PageNo);
	virtual int __fastcall GetExtraOffsForBackground(void);
	virtual void __fastcall DoPagePrepaint(Vcl::Graphics::TCanvas* Canvas, int GlobalPageNo, bool Preview, bool Correction, bool InRange);
	virtual void __fastcall DoPagePostpaint(Vcl::Graphics::TCanvas* Canvas, int GlobalPageNo, bool Preview, bool InRange);
	virtual void __fastcall CheckPageNo(int PageNo);
	DYNAMIC bool __fastcall ShareItems(void);
	DYNAMIC Vcl::Graphics::TCanvas* __fastcall InitPrinterCanvas(void);
	DYNAMIC void __fastcall DonePrinterCanvas(Vcl::Graphics::TCanvas* Canvas);
	virtual Rvstyle::TRVColorMode __fastcall GetColorMode(void);
	virtual void __fastcall DoOnHyperlink(Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);
	virtual void __fastcall DoOnCheckpoint(Crvdata::TCustomRVData* RVData, int ItemNo, int X, int Y);
	virtual bool __fastcall PageExists(int PageNo);
	DYNAMIC bool __fastcall IgnoreFootnotes(void);
	void __fastcall CalcFootnoteCoords(TRVFootnoteRefList* References, int PageNo);
	DYNAMIC bool __fastcall NoMetafiles(void);
	
public:
	bool ParentDrawsBack;
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetSourceRVDataForPrinting(void);
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetSourceRVDataForPrintingEx(void);
	virtual void __fastcall GetDrawItemsRange(int PageNo, int &StartNo, int &EndNo, int &Part);
	virtual int __fastcall GetPrintableAreaLeft(int PageNo);
	virtual void __fastcall DrawPage(int PageNo, int GlobalPageNo, Vcl::Graphics::TCanvas* Canvas, bool Preview, bool Correction);
	virtual void __fastcall DrawBackToBitmap(int Left, int Top, Vcl::Graphics::TBitmap* bmp, const Rvstyle::TRVScreenAndDevice &sad, int ItemBackgroundLayer, bool RelativeToItem, bool AllowBitmaps, bool ParentAllowBitmaps, int PageNo);
	DYNAMIC void __fastcall GetPrintableItemsRange(int &MinItemNo, int &MaxItemNo);
	virtual int __fastcall GetContentHeightForPage(int PageNo);
public:
	/* TRichViewRVData.Destroy */ inline __fastcall virtual ~TCustomPrintableRVData(void) { }
	/* TRichViewRVData.Create */ inline __fastcall virtual TCustomPrintableRVData(Rvscroll::TRVScroller* RichView) : Rvrvdata::TRichViewRVData(RichView) { }
	
};


class DELPHICLASS TCustomMultiPagePtblRVData;
class PASCALIMPLEMENTATION TCustomMultiPagePtblRVData : public TCustomPrintableRVData
{
	typedef TCustomPrintableRVData inherited;
	
protected:
	int StreamSavePage;
	DYNAMIC void __fastcall DoFormatting(int PageCompleted, Richview::TRVPrintingStep Step);
	DYNAMIC int __fastcall GetInitialStartAt(void);
	DYNAMIC int __fastcall GetFurtherStartAt(void);
	DYNAMIC void __fastcall SetEndAt(int Value);
	DYNAMIC void __fastcall IncEndAtByStartAt(int PageNo);
	virtual int __fastcall GetTopCoord(int PageNo, int GlobalPageNo);
	virtual int __fastcall GetTopCoord2(int PageNo, int GlobalPageNo);
	DYNAMIC void __fastcall RVFGetLimits(Crvdata::TRVFSaveScope SaveScope, int &StartItem, int &EndItem, int &StartOffs, int &EndOffs, Rvitem::TRVMultiDrawItemPart* &StartPart, Rvitem::TRVMultiDrawItemPart* &EndPart, Rvitem::TCustomRVItemInfo* &SelectedItem);
	virtual bool __fastcall PageExists(int PageNo);
	DYNAMIC bool __fastcall AllowEmptyFirstPage(void);
	TRVFootnotePtblRVData* __fastcall GetFootnoteRVData(Rvnote::TRVFootnoteItemInfo* Footnote);
	void __fastcall DrawSidenotes(Vcl::Graphics::TCanvas* Canvas, int PageNo, bool Preview, Rvfloatingpos::TRVFloatPositionInText PositionInText);
	DYNAMIC TRVSidenoteList* __fastcall GetSidenoteList(int PageNo);
	
public:
	TRVPageCollection* Pages;
	__fastcall virtual ~TCustomMultiPagePtblRVData(void);
	virtual void __fastcall GetDrawItemsRange(int PageNo, int &StartNo, int &EndNo, int &Part);
	void __fastcall FormatNextPage(int &i, int &StartAt, int &StartY, int &Y, bool &Splitting, int &MaxHeight, TRVFootnoteRefList* &FootnoteRVDataList, bool FootnotesChangeHeight);
	bool __fastcall CanPlaceFirstPageHere(int &Height, bool ParentIsFirstItemOnPage, const Rvstyle::TRVScreenAndDevice &sad, bool ThisPageHasFootnotes, bool FootnotesChangeHeight);
	bool __fastcall SavePageAsRVF(System::Classes::TStream* Stream, int PageNo, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, Crvdata::TRVLayoutInfo* Layout);
	bool __fastcall SaveFromPageAsRVF(System::Classes::TStream* Stream, int FirstPageNo, System::Uitypes::TColor Color, Rvback::TRVBackground* Background, Crvdata::TRVLayoutInfo* Layout);
	int __fastcall GetNoteSeparatorHeight(void);
	void __fastcall GetFirstItemOnPageEx(int PageNo, int &ItemNo, int &OffsetInItem, int &ExtraData);
	bool __fastcall IsComplexSoftPageBreak(int PageNo);
	void __fastcall AssignComplexSoftPageBreakToItem(int PageNo, Crvfdata::TCustomRVFormattedData* RVData);
	bool __fastcall GetPageNo(Crvdata::TCustomRVData* RVData, int ItemNo, int OffsetInItem, Crvdata::TCustomRVData* PrintSourceRVData, int &PageNo, int &LocalPageNo, int &LocalPageCount, TCustomPrintableRVData* &PtRVData);
	bool __fastcall GetAnchorObjectCoords(TCustomPrintableRVData* PtRVData, int ItemNo, int CurPageNo, int PageNoInPtRVData, Rvfloatingpos::TRVHorizontalAnchor HorObject, Rvfloatingpos::TRVVerticalAnchor VertObject, bool RelativeToCell, System::Types::TRect &RHorz, System::Types::TRect &RVert, bool &FoundHere);
public:
	/* TRichViewRVData.Create */ inline __fastcall virtual TCustomMultiPagePtblRVData(Rvscroll::TRVScroller* RichView) : TCustomPrintableRVData(RichView) { }
	
};


class DELPHICLASS TCustomMainPtblRVData;
class PASCALIMPLEMENTATION TCustomMainPtblRVData : public TCustomMultiPagePtblRVData
{
	typedef TCustomMultiPagePtblRVData inherited;
	
private:
	Rvmarker::TRVMarkerList* FPrevMarkers;
	
protected:
	DYNAMIC void __fastcall DoFormatting(int PageCompleted, Richview::TRVPrintingStep Step);
	virtual Rvback::TRVBackground* __fastcall GetBack(void);
	virtual int __fastcall GetTopCoord(int PageNo, int GlobalPageNo);
	virtual int __fastcall GetTopCoord2(int PageNo, int GlobalPageNo);
	virtual void __fastcall GetSADForFormatting(Vcl::Graphics::TCanvas* Canvas, Rvstyle::TRVScreenAndDevice &sad);
	virtual int __fastcall GetPrintableAreaTop(int PageNo);
	virtual void __fastcall CheckPageNo(int PageNo);
	DYNAMIC void __fastcall Prepare(void);
	virtual Rvstyle::TRVColorMode __fastcall GetColorMode(void);
	virtual bool __fastcall GetFirstItemMarker(int &ListNo, int &Level);
	DYNAMIC int __fastcall GetInitialStartAt(void);
	DYNAMIC void __fastcall SetEndAt(int Value);
	DYNAMIC void __fastcall IncEndAtByStartAt(int PageNo);
	virtual void __fastcall DoPagePrepaint(Vcl::Graphics::TCanvas* Canvas, int GlobalPageNo, bool Preview, bool Correction, bool InRange);
	virtual void __fastcall DoPagePostpaint(Vcl::Graphics::TCanvas* Canvas, int PageNo, bool Preview, bool InRange);
	
public:
	Vcl::Graphics::TCanvas* PrinterCanvas;
	int TmpTMPix;
	int TmpBMPix;
	Rvstyle::TRVScreenAndDevice PrnSad;
	int FTopMarginPix;
	int FBottomMarginPix;
	bool Transparent;
	System::Types::TRect TmpMNormal;
	System::Types::TRect TmpMFirstPage;
	System::Types::TRect TmpMEvenPages;
	System::Types::TRect PhysM;
	int ContentHeightNormal;
	int ContentHeightFirstPage;
	int ContentHeightEvenPages;
	Rvstyle::TRVColorMode ColorMode;
	bool FIsDestinationReady;
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetSourceRVDataForPrinting(void);
	DYNAMIC void __fastcall Clear(void);
	DYNAMIC int __fastcall GetPageWidth(void);
	DYNAMIC int __fastcall GetPageHeight(void);
	virtual int __fastcall GetPrintableAreaLeft(int PageNo);
	void __fastcall InitFormatPages(void);
	int __fastcall FormatPages(void);
	void __fastcall FinalizeFormatPages(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetCanvas(void);
	virtual void __fastcall DrawPage(int PageNo, int GlobalPageNo, Vcl::Graphics::TCanvas* Canvas, bool Preview, bool Correction);
	virtual System::Uitypes::TColor __fastcall GetColor(void);
	DYNAMIC Rvmarker::TRVMarkerList* __fastcall GetPrevMarkers(void);
	__fastcall virtual TCustomMainPtblRVData(Rvscroll::TRVScroller* RichView);
	DYNAMIC void __fastcall GetPrintableItemsRange(int &MinItemNo, int &MaxItemNo);
	DYNAMIC bool __fastcall IgnorePageBreaks(void);
	System::Types::PRect __fastcall GetTmpM(Rvstyle::TRVHFType HFType);
	System::Types::PRect __fastcall GetTmpMForPage(int PageNo);
	virtual int __fastcall GetContentHeightForPage(int PageNo);
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TCustomMainPtblRVData(void) { }
	
};


class DELPHICLASS TPrintableRVData;
class DELPHICLASS TRVHeaderFooterRVData;
class PASCALIMPLEMENTATION TPrintableRVData : public TCustomMainPtblRVData
{
	typedef TCustomMainPtblRVData inherited;
	
private:
	NativeUInt Rgn;
	bool RgnValid;
	
protected:
	DYNAMIC void __fastcall DoFormatting(int PageCompleted, Richview::TRVPrintingStep Step);
	DYNAMIC Vcl::Graphics::TCanvas* __fastcall InitPrinterCanvas(void);
	DYNAMIC void __fastcall DonePrinterCanvas(Vcl::Graphics::TCanvas* Canvas);
	virtual void __fastcall DoPagePrepaint(Vcl::Graphics::TCanvas* Canvas, int GlobalPageNo, bool Preview, bool Correction, bool InRange);
	virtual void __fastcall DoPagePostpaint(Vcl::Graphics::TCanvas* Canvas, int GlobalPageNo, bool Preview, bool InRange);
	DYNAMIC void __fastcall Prepare(void);
	
public:
	int TmpLMMir;
	TRVHeaderFooterRVData* Header;
	TRVHeaderFooterRVData* Footer;
	TRVHeaderFooterRVData* FirstPageHeader;
	TRVHeaderFooterRVData* FirstPageFooter;
	TRVHeaderFooterRVData* EvenPagesHeader;
	TRVHeaderFooterRVData* EvenPagesFooter;
	Rvstyle::TRVLength HeaderY;
	Rvstyle::TRVLength FooterY;
	TRVHeaderFooterRVData* __fastcall GetHeaderOrFooter(Rvstyle::TRVHFType HFType, bool IsHeader);
	virtual int __fastcall GetPrintableAreaLeft(int PageNo);
	__fastcall virtual TPrintableRVData(Rvscroll::TRVScroller* RichView);
	__fastcall virtual ~TPrintableRVData(void);
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetSourceRVDataForPrintingEx(void);
	DYNAMIC int __fastcall GetPageWidth(void);
	DYNAMIC int __fastcall GetPageHeight(void);
	TRVHeaderFooterRVData* __fastcall GetHeaderForPage(int PageNo);
	TRVHeaderFooterRVData* __fastcall GetFooterForPage(int PageNo);
};


class DELPHICLASS TRectPtblRVData;
class PASCALIMPLEMENTATION TRectPtblRVData : public TCustomMultiPagePtblRVData
{
	typedef TCustomMultiPagePtblRVData inherited;
	
protected:
	virtual void __fastcall GetSADForFormatting(Vcl::Graphics::TCanvas* Canvas, Rvstyle::TRVScreenAndDevice &sad);
	virtual int __fastcall GetPrintableAreaTop(int PageNo);
	virtual int __fastcall GetExtraOffsForBackground(void);
	virtual int __fastcall GetTopCoord(int PageNo, int GlobalPageNo);
	virtual int __fastcall GetTopCoord2(int PageNo, int GlobalPageNo);
	DYNAMIC Vcl::Graphics::TCanvas* __fastcall InitPrinterCanvas(void);
	virtual Rvstyle::TRVColorMode __fastcall GetColorMode(void);
	virtual void __fastcall DoOnHyperlink(Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);
	virtual void __fastcall DoOnCheckpoint(Crvdata::TCustomRVData* RVData, int ItemNo, int X, int Y);
	DYNAMIC bool __fastcall IgnoreFootnotes(void);
	DYNAMIC bool __fastcall NoMetafiles(void);
	
public:
	Crvdata::TCustomRVData* FSourceDataForPrinting;
	TCustomPrintableRVData* FParentPrintData;
	int Left;
	int Top;
	int DX;
	int DY;
	int Width;
	int Height;
	int VOffsInRotatedCell;
	bool Transparent;
	System::Uitypes::TColor FColor;
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetSourceRVDataForPrinting(void);
	DYNAMIC void __fastcall CreateFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas);
	DYNAMIC void __fastcall DestroyFontInfoCache(Rvfontcache::TRVFontInfoCache* &Cache);
	DYNAMIC Rvfontcache::TRVFontInfoCache* __fastcall GetFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas, Crvfdata::TCustomRVFormattedData* RVData);
	virtual int __fastcall GetMaxTextWidth(void);
	virtual int __fastcall GetMinTextWidth(void);
	virtual int __fastcall GetPrintableAreaLeft(int PageNo);
	virtual Crvdata::TCustomRVData* __fastcall GetParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetRootData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteRootData(void);
	__fastcall TRectPtblRVData(Rvscroll::TRVScroller* RichView, Crvdata::TCustomRVData* SourceDataForPrinting, TCustomPrintableRVData* ParentPrintData);
	virtual void __fastcall DrawBackToBitmap(int Left, int Top, Vcl::Graphics::TBitmap* bmp, const Rvstyle::TRVScreenAndDevice &sad, int ItemBackgroundLayer, bool RelativeToItem, bool AllowBitmaps, bool ParentAllowBitmaps, int PageNo);
	virtual int __fastcall GetWidth(void);
	virtual int __fastcall GetHeight(void);
	virtual int __fastcall GetLeftMargin(void);
	virtual int __fastcall GetRightMargin(void);
	virtual int __fastcall GetTopMargin(void);
	virtual int __fastcall GetBottomMargin(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetCanvas(void);
	virtual System::Uitypes::TColor __fastcall GetColor(void);
	virtual Rvstyle::TRVDocRotation __fastcall GetRotation(void);
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TRectPtblRVData(void) { }
	
};


class PASCALIMPLEMENTATION TRVHeaderFooterRVData : public TRectPtblRVData
{
	typedef TRectPtblRVData inherited;
	
protected:
	DYNAMIC bool __fastcall IgnoreFootnotes(void);
	
public:
	TRVSidenoteList* Sidenotes;
	int FLeftMargin;
	int FRightMargin;
	int FTopMargin;
	int FBottomMargin;
	__fastcall TRVHeaderFooterRVData(Rvscroll::TRVScroller* RichView, Crvdata::TCustomRVData* SourceDataForPrinting, TCustomPrintableRVData* ParentPrintData);
	DYNAMIC TRVSidenoteList* __fastcall GetSidenoteList(int PageNo);
	__fastcall virtual ~TRVHeaderFooterRVData(void);
	virtual Rvstyle::TRVStyle* __fastcall GetRVStyle(void);
	DYNAMIC void __fastcall CreateFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas);
	DYNAMIC Rvfontcache::TRVFontInfoCache* __fastcall GetFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas, Crvfdata::TCustomRVFormattedData* RVData);
	virtual int __fastcall GetLeftMargin(void);
	virtual int __fastcall GetRightMargin(void);
	virtual int __fastcall GetTopMargin(void);
	virtual int __fastcall GetBottomMargin(void);
};


class DELPHICLASS TRVEndnotePtblRVData;
class PASCALIMPLEMENTATION TRVEndnotePtblRVData : public TCustomMultiPagePtblRVData
{
	typedef TCustomMultiPagePtblRVData inherited;
	
protected:
	DYNAMIC int __fastcall GetInitialStartAt(void);
	DYNAMIC int __fastcall GetFurtherStartAt(void);
	DYNAMIC void __fastcall SetEndAt(int Value);
	DYNAMIC void __fastcall IncEndAtByStartAt(int PageNo);
	virtual void __fastcall GetSADForFormatting(Vcl::Graphics::TCanvas* Canvas, Rvstyle::TRVScreenAndDevice &sad);
	virtual int __fastcall GetTopCoord(int PageNo, int GlobalPageNo);
	virtual int __fastcall GetTopCoord2(int PageNo, int GlobalPageNo);
	DYNAMIC bool __fastcall AllowEmptyFirstPage(void);
	DYNAMIC bool __fastcall IgnoreFootnotes(void);
	
public:
	Rvnote::TRVEndnoteItemInfo* Endnote;
	int StartAt;
	int NextStartAt;
	int EndAt;
	bool FromNewPage;
	__fastcall virtual TRVEndnotePtblRVData(Rvscroll::TRVScroller* RichView);
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetSourceRVDataForPrinting(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetCanvas(void);
	DYNAMIC System::Classes::TStringList* __fastcall GetDocProperties(void);
	virtual int __fastcall GetPrintableAreaLeft(int PageNo);
	virtual int __fastcall GetPrintableAreaTop(int PageNo);
	virtual Crvdata::TCustomRVData* __fastcall GetParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetRootData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteRootData(void);
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TRVEndnotePtblRVData(void) { }
	
};


class PASCALIMPLEMENTATION TRVFootnotePtblRVData : public TRectPtblRVData
{
	typedef TRectPtblRVData inherited;
	
protected:
	DYNAMIC bool __fastcall IgnoreFootnotes(void);
	
public:
	int IndexOnPage;
	Rvnote::TRVFootnoteItemInfo* Footnote;
	Crvfdata::TCustomRVFormattedData* FootnoteItemRVData;
	int FootnoteDItemNo;
	void __fastcall AdjustFootnoteRefWidths(void);
public:
	/* TRectPtblRVData.Create */ inline __fastcall TRVFootnotePtblRVData(Rvscroll::TRVScroller* RichView, Crvdata::TCustomRVData* SourceDataForPrinting, TCustomPrintableRVData* ParentPrintData) : TRectPtblRVData(RichView, SourceDataForPrinting, ParentPrintData) { }
	
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TRVFootnotePtblRVData(void) { }
	
};


class PASCALIMPLEMENTATION TRVSidenotePtblRVData : public TRectPtblRVData
{
	typedef TRectPtblRVData inherited;
	
protected:
	DYNAMIC bool __fastcall IgnoreFootnotes(void);
	
public:
	System::Types::TRect FullRect;
	Rvsidenote::TRVSidenoteItemInfo* Sidenote;
	Crvfdata::TCustomRVFormattedData* SidenoteItemRVData;
	int SidenoteItemNo;
public:
	/* TRectPtblRVData.Create */ inline __fastcall TRVSidenotePtblRVData(Rvscroll::TRVScroller* RichView, Crvdata::TCustomRVData* SourceDataForPrinting, TCustomPrintableRVData* ParentPrintData) : TRectPtblRVData(RichView, SourceDataForPrinting, ParentPrintData) { }
	
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TRVSidenotePtblRVData(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall RVPrintFormatSidenotes(TCustomMultiPagePtblRVData* OwnerRVData, TPrintableRVData* MainRVData, Rvstyle::TRVHFType HFType);
}	/* namespace Ptrvdata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_PTRVDATA)
using namespace Ptrvdata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PtrvdataHPP
