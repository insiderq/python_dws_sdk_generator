﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVEdit.pas' rev: 27.00 (Windows)

#ifndef RveditHPP
#define RveditHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Winapi.RichEdit.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Winapi.Imm.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Winapi.ShlObj.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <RVDragDrop.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVRTFProps.hpp>	// Pascal unit
#include <RVPopup.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvedit
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVESearchOption : unsigned char { rvseoMatchCase, rvseoDown, rvseoWholeWord, rvseoMultiItem, rvseoSmartStart };

typedef System::Set<TRVESearchOption, TRVESearchOption::rvseoMatchCase, TRVESearchOption::rvseoSmartStart> TRVESearchOptions;

enum DECLSPEC_DENUM TRVEditorOption : unsigned char { rvoClearTagOnStyleApp, rvoCtrlJumps, rvoDoNotWantReturns, rvoDoNotWantShiftReturns, rvoWantTabs, rvoAutoSwitchLang, rvoHideReadOnlyCaret, rvoNoImageResize, rvoNoCaretHighlightJumps, rvoNoReadOnlyJumps };

typedef System::Set<TRVEditorOption, TRVEditorOption::rvoClearTagOnStyleApp, TRVEditorOption::rvoNoReadOnlyJumps> TRVEditorOptions;

enum DECLSPEC_DENUM TRVDragDropFormat : unsigned char { rvddRVF, rvddRTF, rvddText, rvddUnicodeText, rvddBitmap, rvddMetafile, rvddURL, rvddFiles, rvddHTML };

typedef System::Set<TRVDragDropFormat, TRVDragDropFormat::rvddRVF, TRVDragDropFormat::rvddHTML> TRVDragDropFormats;

enum DECLSPEC_DENUM TRVDropFileAction : unsigned char { rvdfNone, rvdfInsert, rvdfLink };

enum DECLSPEC_DENUM TRVUndoType : unsigned char { rvutNone, rvutDelete, rvutInsert, rvutPara, rvutMiscTyping, rvutInsertPageBreak, rvutRemovePageBreak, rvutTyping, rvutTag, rvutStyleNo, rvutAddCheckpoint, rvutRemoveCheckpoint, rvutModifyCheckpoint, rvutModifyItem, rvutToHypertext, rvutFromHypertext, rvutTextFlow, rvutList, rvutProperty, rvutLayout, rvutBackground, rvutStyleTemplate, rvutSubDoc, rvutResize, rvutTableCell, rvutTableRow, rvutTable, rvutTableInsertRows, rvutTableInsertCols, rvutTableDeleteRows, rvutTableDeleteCols, rvutTableMerge, rvutTableSplit, rvutCustom };

enum DECLSPEC_DENUM TRVGetOutDirection : unsigned char { rvdLeft, rvdUp, rvdRight, rvdDown, rvdTop, rvdBottom };

enum DECLSPEC_DENUM TRVOleDropEffect : unsigned char { rvdeNone, rvdeCopy, rvdeMove, rvdeLink };

typedef System::Set<TRVOleDropEffect, TRVOleDropEffect::rvdeNone, TRVOleDropEffect::rvdeLink> TRVOleDropEffects;

class DELPHICLASS TCustomRichViewEdit;
typedef void __fastcall (__closure *TRVStyleConversionEvent)(TCustomRichViewEdit* Sender, int StyleNo, int UserData, bool AppliedToText, int &NewStyleNo);

typedef void __fastcall (__closure *TRVStyleConversionEvent_)(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);

typedef void __fastcall (__closure *TRVPasteEvent)(TCustomRichViewEdit* Sender, bool &DoDefault);

typedef void __fastcall (__closure *TRVOnCaretGetOutEvent)(TCustomRichViewEdit* Sender, TRVGetOutDirection Direction);

typedef void __fastcall (__closure *TRVChangingEvent)(TCustomRichViewEdit* Sender, bool &CanEdit);

typedef void __fastcall (__closure *TRVDropFilesEvent)(TCustomRichViewEdit* Sender, System::Classes::TStrings* Files, TRVDropFileAction &FileAction, bool &DoDefault);

typedef void __fastcall (__closure *TRVOleDragEnterEvent)(Richview::TCustomRichView* Sender, const _di_IDataObject DataObject, System::Classes::TShiftState Shift, int X, int Y, TRVOleDropEffects PossibleDropEffects, TRVOleDropEffect &DropEffect);

typedef void __fastcall (__closure *TRVOleDragOverEvent)(Richview::TCustomRichView* Sender, System::Classes::TShiftState Shift, int X, int Y, TRVOleDropEffects PossibleDropEffects, TRVOleDropEffect &DropEffect);

typedef void __fastcall (__closure *TRVOleDropEvent)(Richview::TCustomRichView* Sender, const _di_IDataObject DataObject, System::Classes::TShiftState Shift, int X, int Y, TRVOleDropEffects PossibleDropEffects, TRVOleDropEffect &DropEffect, bool &DoDefault);

typedef void __fastcall (__closure *TRVItemResizeEvent)(TCustomRichViewEdit* Sender, Crvfdata::TCustomRVFormattedData* RVData, int ItemNo, int Val1, int Val2);

typedef void __fastcall (__closure *TRVItemTextEditEvent)(TCustomRichViewEdit* Sender, const Rvtypes::TRVRawByteString OldText, Crvdata::TCustomRVData* RVData, int ItemNo, Rvstyle::TRVTag &NewTag, int &NewStyleNo);

typedef void __fastcall (__closure *TRVDrawCustomCaretEvent)(TCustomRichViewEdit* Sender, Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect);

typedef void __fastcall (__closure *TRVMeasureCustomCaretEvent)(TCustomRichViewEdit* Sender, System::Types::TRect &Rect);

typedef void __fastcall (__closure *TRVStickingItemsEvent)(TCustomRichViewEdit* Sender, Crvdata::TCustomRVData* RVData, int FirstItemNo, bool &Sticking);

typedef void __fastcall (__closure *TRVDeletingEvent)(TCustomRichViewEdit* Sender, bool &CanDelete);

class PASCALIMPLEMENTATION TCustomRichViewEdit : public Richview::TCustomRichView
{
	typedef Richview::TCustomRichView inherited;
	
private:
	bool FModified;
	int FLockCount;
	bool FReadOnly;
	TRVEditorOptions FEditorOptions;
	TRVDragDropFormats FAcceptDragDropFormats;
	TRVDragDropFormats FAcceptPasteFormats;
	int FCustomCaretInterval;
	System::Classes::TNotifyEvent FOnCurParaStyleChanged;
	System::Classes::TNotifyEvent FOnCurTextStyleChanged;
	System::Classes::TNotifyEvent FOnChange;
	System::Classes::TNotifyEvent FOnCaretMove;
	TRVChangingEvent FOnChanging;
	TRVDeletingEvent FOnDeleting;
	TRVStyleConversionEvent FOnStyleConversion;
	TRVStyleConversionEvent FOnParaStyleConversion;
	TRVPasteEvent FOnPaste;
	TRVDrawCustomCaretEvent FOnDrawCustomCaret;
	TRVMeasureCustomCaretEvent FOnMeasureCustomCaret;
	TRVItemTextEditEvent FOnItemTextEdit;
	TRVItemResizeEvent FOnItemResize;
	TRVStickingItemsEvent FOnCheckStickingItems;
	bool FForceFieldHighlight;
	TRVOnCaretGetOutEvent FOnCaretGetOut;
	TRVDropFilesEvent FOnDropFiles;
	Rvdragdrop::TRVDropTarget* FDropTarget;
	TRVOleDragEnterEvent FOnOleDragEnter;
	TRVOleDropEvent FOnOleDrop;
	TRVOleDragOverEvent FOnOleDragOver;
	System::Classes::TNotifyEvent FOnOleDragLeave;
	Rvstyle::TRVVAlign FDefaultPictureVAlign;
	Richview::TRVEventHandlerList* FCurTextStyleChangeHandlers;
	Richview::TRVEventHandlerList* FCurParaStyleChangeHandlers;
	Richview::TRVEventHandlerList* FCaretMoveHandlers;
	void __fastcall CheckItemClass(int ItemNo, Rvitem::TCustomRVItemInfoClass RequiredClass);
	HIDESBASE MESSAGE void __fastcall WMInputLangChange(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Winapi::Messages::TWMSetFocus &Message);
	HIDESBASE MESSAGE void __fastcall WMKillFocus(Winapi::Messages::TWMKillFocus &Message);
	MESSAGE void __fastcall WMCut(Winapi::Messages::TWMNoParams &Message);
	MESSAGE void __fastcall WMPaste(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall WMGetDlgCode(Winapi::Messages::TWMNoParams &Message);
	MESSAGE void __fastcall CMWantSpecialKey(Winapi::Messages::TWMKey &Message);
	MESSAGE void __fastcall WMSysChar(Winapi::Messages::TWMKey &Message);
	MESSAGE void __fastcall WMUndoFromInplace(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMRedoFromInplace(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMClear(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMUndo(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMUndo(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMRedo(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMCanUndo(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMCanRedo(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall EMCanPaste(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMCreate(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMTimer(Winapi::Messages::TWMTimer &Message);
	void __fastcall UpdateImeWindow(void);
	void __fastcall CompleteImeComposition(void);
	void __fastcall GetCompositionFormParams(tagCOMPOSITIONFORM &CF);
	MESSAGE void __fastcall WMImeStartComposition(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMImeComposition(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMImeChar(Winapi::Messages::TMessage &Message);
	void __fastcall SetCurParaStyleNo(const int Value);
	void __fastcall SetCurTextStyleNo(const int Value);
	int __fastcall GetCurParaStyleNo(void);
	int __fastcall GetCurTextStyleNo(void);
	int __fastcall GetCurItemStyle(void);
	int __fastcall GetCurItemNo(void);
	int __fastcall GetOffsetInCurItem(void);
	void __fastcall ApplyTextStyleConversionProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	int __fastcall DoApplyParaStyleTemplateConversion(int StyleNo, int ParaStyleNo, int StyleTemplateNo, bool ResetAdditionalFormatting);
	void __fastcall ApplyParaStyleTemplateConversionResetProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	void __fastcall ApplyParaStyleTemplateConversionNoResetProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	int __fastcall DoApplyTextStyleTemplateConversion(int StyleNo, int ParaStyleNo, int StyleTemplateNo, bool ResetAdditionalFormatting);
	void __fastcall ApplyTextStyleTemplateConversionResetProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	void __fastcall ApplyTextStyleTemplateConversionNoResetProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	void __fastcall ApplyUserParaStyleConversionProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	void __fastcall ApplyUserTextStyleConversionProc(TCustomRichViewEdit* Sender, int StyleNo, int ParaStyleNo, int UserData, bool AppliedToText, int &NewStyleNo);
	int __fastcall GetUndoLimit(void);
	void __fastcall SetUndoLimit(const int Value);
	bool __fastcall IsUndoShortcut(System::Classes::TShiftState Shift, System::Word Key);
	bool __fastcall IsRedoShortcut(System::Classes::TShiftState Shift, System::Word Key);
	bool __fastcall IsUpDownMoveShortcut(System::Classes::TShiftState Shift, System::Word Key);
	void __fastcall InsertTextW_(const Rvtypes::TRVRawByteString text);
	MESSAGE void __fastcall WMUniChar(Winapi::Messages::TMessage &Message);
	HIDESBASE void __fastcall SetTabNavigation(const Rvscroll::TRVTabNavigationType Value);
	TCustomRichViewEdit* __fastcall GetTopLevelEditor(void);
	int __fastcall GetActualCurTextStyleNo(void);
	void __fastcall SetCustomCaretInterval(const int Value);
	
protected:
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	virtual Rvrvdata::TRichViewRVDataClass __fastcall GetDataClass(void);
	virtual void __fastcall SetReadOnly(const bool Value);
	virtual bool __fastcall GetReadOnly(void);
	virtual void __fastcall SetEnabled(bool Value);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	DYNAMIC void __fastcall KeyUp(System::Word &Key, System::Classes::TShiftState Shift);
	DYNAMIC void __fastcall KeyPress(System::WideChar &Key);
	void __fastcall OnEnterPress(bool Shift);
	void __fastcall OnBackSpacePress(bool Ctrl);
	void __fastcall OnDeletePress(bool Ctrl);
	virtual void __fastcall AfterVScroll(void);
	virtual void __fastcall AfterHScroll(void);
	DYNAMIC bool __fastcall OleDragEnter(int X, int Y);
	DYNAMIC void __fastcall CallOleDragEnterEvent(const _di_IDataObject DataObj, int KeyState, const System::Types::TPoint &pt, int PossibleEffects, int &Effect);
	DYNAMIC void __fastcall OleDragLeave(void);
	DYNAMIC bool __fastcall OleDragOver(int X, int Y);
	DYNAMIC void __fastcall CallOleDragOverEvent(int KeyState, const System::Types::TPoint &pt, int PossibleEffects, int &Effect);
	bool __fastcall InsertTextFileAuto(const System::UnicodeString FileName);
	DYNAMIC int __fastcall OleDrop(const _di_IDataObject DataObj, bool FMove, int KeyState, const System::Types::TPoint &pt, int PossibleEffects);
	DYNAMIC void __fastcall ReleaseOleDropTargetObject(void);
	System::Word __fastcall GetAcceptableRVFormat(void);
	DYNAMIC bool __fastcall OleCanAcceptFormat(System::Word Format);
	DYNAMIC void __fastcall DoAfterDropTargetCreated(_di_IDropTarget aCreatedDropTarget);
	DYNAMIC void __fastcall AdjustPopupMenuPos(System::Types::TPoint &pt);
	System::UnicodeString __fastcall GetBasePathFromHTMLInClipboard(void);
	DYNAMIC void __fastcall SetSmartPopupTarget(void);
	virtual void __fastcall BeforeScroll(void);
	DYNAMIC bool __fastcall DoChanging(void);
	void __fastcall InsertURL(const System::UnicodeString Target, const System::UnicodeString Title);
	__property System::Classes::TNotifyEvent OnChange = {read=FOnChange, write=FOnChange};
	__property System::Classes::TNotifyEvent OnCaretMove = {read=FOnCaretMove, write=FOnCaretMove};
	__property TRVChangingEvent OnChanging = {read=FOnChanging, write=FOnChanging};
	__property TRVPasteEvent OnPaste = {read=FOnPaste, write=FOnPaste};
	__property TabNavigation = {read=GetTabNavigation, write=SetTabNavigation, default=0};
	__property Rvstyle::TRVVAlign DefaultPictureVAlign = {read=FDefaultPictureVAlign, write=FDefaultPictureVAlign, default=0};
	
public:
	TRVStyleConversionEvent_ FCurStyleConversion;
	__fastcall virtual TCustomRichViewEdit(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomRichViewEdit(void);
	void __fastcall SetFReadOnly(bool Value);
	bool __fastcall BeforeInserting(void);
	void __fastcall AfterInserting(void);
	DYNAMIC void __fastcall CurParaStyleChange(void);
	DYNAMIC void __fastcall CurTextStyleChange(void);
	DYNAMIC Rvscroll::TCustomRVControl* __fastcall SRVGetActiveEditor(bool MainDoc);
	virtual bool __fastcall BeforeChange(bool FromOutside);
	DYNAMIC void __fastcall DoChange(bool AClearRedo);
	DYNAMIC void __fastcall Selecting(void);
	void __fastcall AfterCaretMove(void);
	HIDESBASE void __fastcall AssignEvents(Richview::TCustomRichView* Source);
	void __fastcall BeforeUnicode(void);
	TCustomRichViewEdit* __fastcall GetRootEditor(void);
	void __fastcall RefreshAll(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall InsertTextW(const Rvtypes::TRVUnicodeString text, bool CaretBefore = false);
	bool __fastcall InsertStringWTag(const Rvtypes::TRVUnicodeString s, const Rvstyle::TRVTag Tag);
	bool __fastcall InsertItemR(const Rvtypes::TRVRawByteString Name, Rvitem::TCustomRVItemInfo* Item);
	bool __fastcall InsertItem(const System::UnicodeString Name, Rvitem::TCustomRVItemInfo* Item);
	void __fastcall InsertTextA(const Rvtypes::TRVAnsiString text, bool CaretBefore = false);
	void __fastcall InsertText(const System::UnicodeString text, bool CaretBefore = false);
	bool __fastcall InsertStringATag(const Rvtypes::TRVAnsiString s, const Rvstyle::TRVTag Tag);
	bool __fastcall InsertStringTag(const System::UnicodeString s, const Rvstyle::TRVTag Tag);
	bool __fastcall InsertTab(void);
	HIDESBASE bool __fastcall InsertControl(const Rvtypes::TRVAnsiString Name, Vcl::Controls::TControl* ctrl, Rvstyle::TRVVAlign VAlign);
	bool __fastcall InsertPicture(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, Rvstyle::TRVVAlign VAlign);
	bool __fastcall InsertHotPicture(const Rvtypes::TRVAnsiString Name, Vcl::Graphics::TGraphic* gr, Rvstyle::TRVVAlign VAlign);
	bool __fastcall InsertBreak(Rvstyle::TRVStyleLength Width, Rvstyle::TRVBreakStyle Style, System::Uitypes::TColor Color);
	bool __fastcall InsertBullet(int ImageIndex, Vcl::Imglist::TCustomImageList* ImageList);
	bool __fastcall InsertHotspot(int ImageIndex, int HotImageIndex, Vcl::Imglist::TCustomImageList* ImageList);
	bool __fastcall InsertRVFFromStreamEd(System::Classes::TStream* Stream);
	bool __fastcall InsertRVFFromFileEd(const System::UnicodeString FileName);
	bool __fastcall InsertTextFromFile(const System::UnicodeString FileName, unsigned CodePage = (unsigned)(0x0));
	bool __fastcall InsertOEMTextFromFile(const System::UnicodeString FileName);
	bool __fastcall InsertTextFromFileW(const System::UnicodeString FileName);
	bool __fastcall InsertTextFromFileUTF8(const System::UnicodeString FileName);
	bool __fastcall InsertRTFFromStreamEd(System::Classes::TStream* Stream);
	bool __fastcall InsertRTFFromFileEd(const System::UnicodeString FileName);
	DYNAMIC bool __fastcall SaveDocXToStream(System::Classes::TStream* Stream, bool SelectionOnly);
	void __fastcall InsertPageBreak(void);
	void __fastcall RemoveCurrentPageBreak(void);
	void __fastcall ClearTextFlow(bool Left, bool Right);
	void __fastcall ConvertToPicture(int ItemNo);
	void __fastcall ConvertToHotPicture(int ItemNo);
	bool __fastcall CanDelete(void);
	DYNAMIC void __fastcall DeleteSelection(void);
	void __fastcall CutDef(void);
	void __fastcall ApplyParaStyle(int ParaStyleNo);
	void __fastcall ApplyTextStyle(int TextStyleNo);
	void __fastcall ApplyStyleConversion(int UserData, bool Recursive = true);
	void __fastcall ApplyParaStyleConversion(int UserData, bool Recursive = true);
	void __fastcall ChangeStyleTemplates(Rvstyle::TRVStyleTemplateCollection* NewStyleTemplates);
	void __fastcall ApplyParaStyleTemplate(int TemplateNo, bool ResetAdditionalFormatting, bool Recursive = true);
	void __fastcall ApplyTextStyleTemplate(int TemplateNo, bool ResetAdditionalFormatting, bool Recursive = true);
	void __fastcall ApplyStyleTemplate(int TemplateNo, bool Recursive = true);
	bool __fastcall CanPasteRTF(void);
	DYNAMIC bool __fastcall CanPaste(void);
	bool __fastcall CanPasteRVF(void);
	DYNAMIC void __fastcall Paste(void);
	bool __fastcall PasteURL(void);
	bool __fastcall PasteBitmap(bool TextAsName);
	bool __fastcall PasteMetafile(bool TextAsName);
	bool __fastcall PasteGraphicFiles(bool FileNamesAsNames, bool StoreFileNames);
	void __fastcall PasteText(void);
	void __fastcall PasteTextA(void);
	void __fastcall PasteRVF(void);
	bool __fastcall PasteRTF(void);
	void __fastcall PasteTextW(void);
	Rvstyle::TRVTag __fastcall GetCurrentTag(void);
	Rvstyle::TRVVAlign __fastcall GetCurrentItemVAlign(void);
	Rvtypes::TRVRawByteString __fastcall GetCurrentItemTextR(void);
	System::UnicodeString __fastcall GetCurrentItemText(void);
	Rvitem::TCustomRVItemInfo* __fastcall GetCurrentItem(void);
	bool __fastcall GetCurrentItemEx(Rvitem::TCustomRVItemInfoClass RequiredClass, TCustomRichViewEdit* &ItemRichViewEdit, Rvitem::TCustomRVItemInfo* &Item);
	Rvtypes::TRVAnsiString __fastcall GetCurrentItemTextA(void);
	Rvtypes::TRVUnicodeString __fastcall GetCurrentItemTextW(void);
	void __fastcall GetCurrentBreakInfo(Rvstyle::TRVStyleLength &AWidth, Rvstyle::TRVBreakStyle &AStyle, System::Uitypes::TColor &AColor, Rvstyle::TRVTag &ATag);
	void __fastcall GetCurrentBulletInfo(Rvtypes::TRVAnsiString &AName, int &AImageIndex, Vcl::Imglist::TCustomImageList* &AImageList, Rvstyle::TRVTag &ATag);
	void __fastcall GetCurrentHotspotInfo(Rvtypes::TRVAnsiString &AName, int &AImageIndex, int &AHotImageIndex, Vcl::Imglist::TCustomImageList* &AImageList, Rvstyle::TRVTag &ATag);
	void __fastcall GetCurrentPictureInfo(Rvtypes::TRVAnsiString &AName, Vcl::Graphics::TGraphic* &Agr, Rvstyle::TRVVAlign &AVAlign, Rvstyle::TRVTag &ATag);
	void __fastcall GetCurrentControlInfo(Rvtypes::TRVAnsiString &AName, Vcl::Controls::TControl* &Actrl, Rvstyle::TRVVAlign &AVAlign, Rvstyle::TRVTag &ATag);
	void __fastcall GetCurrentTextInfo(System::UnicodeString &AText, Rvstyle::TRVTag &ATag);
	void __fastcall SetItemTextEdR(int ItemNo, const Rvtypes::TRVRawByteString s);
	void __fastcall SetItemTextEd(int ItemNo, const System::UnicodeString s);
	void __fastcall SetItemTagEd(int ItemNo, const Rvstyle::TRVTag ATag);
	void __fastcall SetItemVAlignEd(int ItemNo, Rvstyle::TRVVAlign VAlign);
	void __fastcall ResizeControl(int ItemNo, int NewWidth, int NewHeight);
	void __fastcall SetItemTextEdA(int ItemNo, const Rvtypes::TRVAnsiString s);
	void __fastcall SetItemTextEdW(int ItemNo, const Rvtypes::TRVUnicodeString s);
	void __fastcall SetCurrentItemTextR(const Rvtypes::TRVRawByteString s);
	void __fastcall SetCurrentItemText(const System::UnicodeString s);
	void __fastcall SetCurrentTag(const Rvstyle::TRVTag ATag);
	void __fastcall SetCurrentItemVAlign(Rvstyle::TRVVAlign VAlign);
	void __fastcall SetCurrentItemTextA(const Rvtypes::TRVAnsiString s);
	void __fastcall SetCurrentItemTextW(const Rvtypes::TRVUnicodeString s);
	void __fastcall SetBreakInfoEd(int ItemNo, Rvstyle::TRVStyleLength AWidth, Rvstyle::TRVBreakStyle AStyle, System::Uitypes::TColor AColor, const Rvstyle::TRVTag ATag);
	void __fastcall SetBulletInfoEd(int ItemNo, const Rvtypes::TRVAnsiString AName, int AImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	void __fastcall SetHotspotInfoEd(int ItemNo, const Rvtypes::TRVAnsiString AName, int AImageIndex, int AHotImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	void __fastcall SetPictureInfoEd(int ItemNo, const Rvtypes::TRVAnsiString AName, Vcl::Graphics::TGraphic* Agr, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	void __fastcall SetControlInfoEd(int ItemNo, const Rvtypes::TRVAnsiString AName, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	void __fastcall SetCurrentBreakInfo(Rvstyle::TRVStyleLength AWidth, Rvstyle::TRVBreakStyle AStyle, System::Uitypes::TColor AColor, const Rvstyle::TRVTag ATag);
	void __fastcall SetCurrentBulletInfo(const Rvtypes::TRVAnsiString AName, int AImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	void __fastcall SetCurrentHotspotInfo(const Rvtypes::TRVAnsiString AName, int AImageIndex, int AHotImageIndex, Vcl::Imglist::TCustomImageList* AImageList, const Rvstyle::TRVTag ATag);
	void __fastcall SetCurrentPictureInfo(const Rvtypes::TRVAnsiString AName, Vcl::Graphics::TGraphic* Agr, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	void __fastcall SetCurrentControlInfo(const Rvtypes::TRVAnsiString AName, Rvstyle::TRVVAlign AVAlign, const Rvstyle::TRVTag ATag);
	void __fastcall ResizeCurrentControl(int NewWidth, int NewHeight);
	void __fastcall SplitAtCaret(void);
	void __fastcall AdjustControlPlacement(int ItemNo);
	void __fastcall AdjustControlPlacement2(Vcl::Controls::TControl* Control);
	void __fastcall BeginItemModify(int ItemNo, int &ModifyData);
	void __fastcall EndItemModify(int ItemNo, int ModifyData);
	void __fastcall BeginCurrentItemModify(int &ModifyData);
	void __fastcall EndCurrentItemModify(int ModifyData);
	bool __fastcall GetCurrentItemExtraIntProperty(Rvitem::TRVExtraItemProperty Prop, int &Value);
	void __fastcall SetItemExtraIntPropertyEd(int ItemNo, Rvitem::TRVExtraItemProperty Prop, int Value, bool AutoReformat);
	void __fastcall SetCurrentItemExtraIntProperty(Rvitem::TRVExtraItemProperty Prop, int Value, bool AutoReformat);
	bool __fastcall GetCurrentItemExtraIntPropertyEx(int Prop, int &Value);
	void __fastcall SetItemExtraIntPropertyExEd(int ItemNo, int Prop, int Value, bool AutoReformat);
	void __fastcall SetCurrentItemExtraIntPropertyEx(int Prop, int Value, bool AutoReformat);
	bool __fastcall GetCurrentItemExtraStrProperty(Rvitem::TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	void __fastcall SetItemExtraStrPropertyEd(int ItemNo, Rvitem::TRVExtraItemStrProperty Prop, const System::UnicodeString Value, bool AutoReformat);
	void __fastcall SetCurrentItemExtraStrProperty(Rvitem::TRVExtraItemStrProperty Prop, const System::UnicodeString Value, bool AutoReformat);
	bool __fastcall GetCurrentItemExtraStrPropertyEx(int Prop, System::UnicodeString &Value);
	void __fastcall SetItemExtraStrPropertyExEd(int ItemNo, int Prop, const System::UnicodeString Value, bool AutoReformat);
	void __fastcall SetCurrentItemExtraStrPropertyEx(int Prop, const System::UnicodeString Value, bool AutoReformat);
	Rvstyle::TCheckpointData __fastcall GetCurrentCheckpoint(void);
	Rvstyle::TCheckpointData __fastcall GetCheckpointAtCaret(void);
	void __fastcall SetCheckpointInfoEd(int ItemNo, const Rvstyle::TRVTag ATag, const System::UnicodeString AName, bool ARaiseEvent);
	void __fastcall RemoveCheckpointEd(int ItemNo);
	void __fastcall SetCurrentCheckpointInfo(const Rvstyle::TRVTag ATag, const System::UnicodeString AName, bool ARaiseEvent);
	void __fastcall InsertCheckpoint(const Rvstyle::TRVTag ATag, const System::UnicodeString AName, bool ARaiseEvent);
	void __fastcall RemoveCurrentCheckpoint(void);
	void __fastcall RemoveCheckpointAtCaret(void);
	HIDESBASE bool __fastcall SearchTextA(const Rvtypes::TRVAnsiString s, TRVESearchOptions SrchOptions);
	HIDESBASE bool __fastcall SearchTextW(Rvtypes::TRVUnicodeString s, TRVESearchOptions SrchOptions);
	HIDESBASE bool __fastcall SearchText(System::UnicodeString s, TRVESearchOptions SrchOptions);
	void __fastcall SelectCurrentWord(void);
	void __fastcall SelectCurrentLine(void);
	bool __fastcall CanChange(void);
	void __fastcall Change(void);
	void __fastcall BeginUpdate(void);
	void __fastcall EndUpdate(void);
	DYNAMIC void __fastcall Undo(void);
	DYNAMIC void __fastcall Redo(void);
	DYNAMIC TRVUndoType __fastcall UndoAction(void);
	DYNAMIC TRVUndoType __fastcall RedoAction(void);
	DYNAMIC System::UnicodeString __fastcall UndoName(void);
	DYNAMIC System::UnicodeString __fastcall RedoName(void);
	void __fastcall ClearUndo(void);
	void __fastcall SetUndoGroupMode(bool GroupUndo);
	void __fastcall BeginUndoGroup(TRVUndoType UndoType);
	void __fastcall BeginUndoCustomGroup(const System::UnicodeString Name);
	DYNAMIC void __fastcall ClearRedo(void);
	void __fastcall ApplyListStyle(int AListNo, int AListLevel, int AStartFrom, bool AUseStartFrom, bool ARecursive);
	void __fastcall RemoveLists(bool ARecursive);
	void __fastcall ChangeListLevels(int LevelDelta);
	bool __fastcall GetCurrentMisspelling(bool SelectIt, System::UnicodeString &Word, int &StyleNo);
	DYNAMIC bool __fastcall ExecuteAction(System::Classes::TBasicAction* Action);
	virtual bool __fastcall UpdateAction(System::Classes::TBasicAction* Action);
	void __fastcall SetIntPropertyEd(Richview::TRVIntProperty Prop, int Value, bool AutoReformat);
	void __fastcall SetStrPropertyEd(Richview::TRVStrProperty Prop, const System::UnicodeString Value, bool AutoReformat);
	void __fastcall SetFloatPropertyEd(Richview::TRVFloatProperty Prop, Rvstyle::TRVLength Value, bool AutoReformat);
	void __fastcall SetBackgroundImageEd(Vcl::Graphics::TGraphic* Graphic, bool AutoReformat);
	void __fastcall GetCurrentLineCol(int &Line, int &Column);
	void __fastcall RegisterCurTextStyleChangeHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall RegisterCurParaStyleChangeHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall RegisterCaretMoveHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	void __fastcall UnregisterCurTextStyleChangeHandler(System::Classes::TComponent* Component);
	void __fastcall UnregisterCurParaStyleChangeHandler(System::Classes::TComponent* Component);
	void __fastcall UnregisterCaretMoveHandler(System::Classes::TComponent* Component);
	bool __fastcall IsCaretAtTheBeginningOfLine(void);
	__property bool Modified = {read=FModified, write=FModified, nodefault};
	__property int CurItemNo = {read=GetCurItemNo, nodefault};
	__property int CurItemStyle = {read=GetCurItemStyle, nodefault};
	__property int CurParaStyleNo = {read=GetCurParaStyleNo, write=SetCurParaStyleNo, nodefault};
	__property int CurTextStyleNo = {read=GetCurTextStyleNo, write=SetCurTextStyleNo, nodefault};
	__property int ActualCurTextStyleNo = {read=GetActualCurTextStyleNo, write=SetCurTextStyleNo, nodefault};
	__property int OffsetInCurItem = {read=GetOffsetInCurItem, nodefault};
	__property TRVEditorOptions EditorOptions = {read=FEditorOptions, write=FEditorOptions, default=18};
	__property bool ReadOnly = {read=GetReadOnly, write=SetReadOnly, nodefault};
	__property int UndoLimit = {read=GetUndoLimit, write=SetUndoLimit, default=-1};
	__property TCustomRichViewEdit* TopLevelEditor = {read=GetTopLevelEditor};
	__property TCustomRichViewEdit* RootEditor = {read=GetRootEditor};
	__property bool ForceFieldHighlight = {read=FForceFieldHighlight, write=FForceFieldHighlight, nodefault};
	__property TRVOnCaretGetOutEvent OnCaretGetOut = {read=FOnCaretGetOut, write=FOnCaretGetOut};
	__property TRVDropFilesEvent OnDropFiles = {read=FOnDropFiles, write=FOnDropFiles};
	__property TRVStyleConversionEvent OnParaStyleConversion = {read=FOnParaStyleConversion, write=FOnParaStyleConversion};
	__property TRVStyleConversionEvent OnStyleConversion = {read=FOnStyleConversion, write=FOnStyleConversion};
	__property System::Classes::TNotifyEvent OnCurParaStyleChanged = {read=FOnCurParaStyleChanged, write=FOnCurParaStyleChanged};
	__property System::Classes::TNotifyEvent OnCurTextStyleChanged = {read=FOnCurTextStyleChanged, write=FOnCurTextStyleChanged};
	__property TRVItemTextEditEvent OnItemTextEdit = {read=FOnItemTextEdit, write=FOnItemTextEdit};
	__property OnMouseMove;
	__property OnDragOver;
	__property OnDragDrop;
	__property TRVOleDragEnterEvent OnOleDragEnter = {read=FOnOleDragEnter, write=FOnOleDragEnter};
	__property TRVOleDragOverEvent OnOleDragOver = {read=FOnOleDragOver, write=FOnOleDragOver};
	__property TRVOleDropEvent OnOleDrop = {read=FOnOleDrop, write=FOnOleDrop};
	__property System::Classes::TNotifyEvent OnOleDragLeave = {read=FOnOleDragLeave, write=FOnOleDragLeave};
	__property TRVDragDropFormats AcceptDragDropFormats = {read=FAcceptDragDropFormats, write=FAcceptDragDropFormats, default=191};
	__property TRVDragDropFormats AcceptPasteFormats = {read=FAcceptPasteFormats, write=FAcceptPasteFormats, default=191};
	__property int CustomCaretInterval = {read=FCustomCaretInterval, write=SetCustomCaretInterval, default=0};
	__property TRVDrawCustomCaretEvent OnDrawCustomCaret = {read=FOnDrawCustomCaret, write=FOnDrawCustomCaret};
	__property TRVMeasureCustomCaretEvent OnMeasureCustomCaret = {read=FOnMeasureCustomCaret, write=FOnMeasureCustomCaret};
	__property TRVItemResizeEvent OnItemResize = {read=FOnItemResize, write=FOnItemResize};
	__property TRVStickingItemsEvent OnCheckStickingItems = {read=FOnCheckStickingItems, write=FOnCheckStickingItems};
	__property TRVDeletingEvent OnDeleting = {read=FOnDeleting, write=FOnDeleting};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRichViewEdit(HWND ParentWindow) : Richview::TCustomRichView(ParentWindow) { }
	
};


class DELPHICLASS TRichViewEdit;
class PASCALIMPLEMENTATION TRichViewEdit : public TCustomRichViewEdit
{
	typedef TCustomRichViewEdit inherited;
	
__published:
	__property AcceptDragDropFormats = {default=191};
	__property AcceptPasteFormats = {default=191};
	__property CustomCaretInterval = {default=0};
	__property DefaultPictureVAlign = {default=0};
	__property EditorOptions = {default=18};
	__property ReadOnly;
	__property UndoLimit = {default=-1};
	__property OnCaretGetOut;
	__property OnCaretMove;
	__property OnChange;
	__property OnChanging;
	__property OnCheckStickingItems;
	__property OnCurParaStyleChanged;
	__property OnCurTextStyleChanged;
	__property OnDrawCustomCaret;
	__property OnMeasureCustomCaret;
	__property OnDropFiles;
	__property OnItemResize;
	__property OnItemTextEdit;
	__property OnOleDragEnter;
	__property OnOleDragLeave;
	__property OnOleDragOver;
	__property OnOleDrop;
	__property OnParaStyleConversion;
	__property OnPaste;
	__property OnStyleConversion;
	__property TabNavigation = {default=0};
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Constraints;
	__property Color = {default=536870911};
	__property Ctl3D;
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Enabled = {default=1};
	__property HelpContext = {default=0};
	__property ParentCtl3D = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowHint;
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property Touch;
	__property Visible = {default=1};
	__property OnClick;
	__property OnContextPopup;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnGesture;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseMove;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnResize;
	__property OnStartDrag;
	__property AnimationMode = {default=1};
	__property BackgroundBitmap;
	__property BackgroundStyle = {default=0};
	__property BiDiMode = {default=0};
	__property BorderStyle = {default=1};
	__property BottomMargin = {default=5};
	__property Cursor = {default=-4};
	__property Delimiters = {default=0};
	__property DocParameters;
	__property DoInPaletteMode;
	__property FirstJumpNo = {default=0};
	__property HScrollVisible = {default=1};
	__property LeftMargin = {default=5};
	__property LiveSpellingMode = {default=1};
	__property MaxLength = {default=0};
	__property MaxTextWidth = {default=0};
	__property MinTextWidth = {default=0};
	__property Options = {default=1702949};
	__property RightMargin = {default=5};
	__property RTFOptions = {default=30};
	__property RTFReadProperties;
	__property RVFOptions = {default=98435};
	__property RVFParaStylesReadMode = {default=2};
	__property RVFTextStylesReadMode = {default=2};
	__property SmartPopupProperties;
	__property Style;
	__property TopMargin = {default=5};
	__property Tracking = {default=1};
	__property UseStyleTemplates = {default=0};
	__property StyleTemplateInsertMode = {default=0};
	__property UseXPThemes = {default=1};
	__property VAlign = {default=0};
	__property VScrollVisible = {default=1};
	__property WheelStep = {default=2};
	__property WordWrap = {default=1};
	__property OnAddStyle;
	__property OnControlAction;
	__property OnCopy;
	__property OnGetItemCursor;
	__property OnImportPicture;
	__property OnItemAction;
	__property OnItemHint;
	__property OnJump;
	__property OnHScrolled;
	__property OnHTMLSaveImage;
	__property OnPaint;
	__property OnProgress;
	__property OnReadHyperlink;
	__property OnRVDblClick;
	__property OnRVFImageListNeeded;
	__property OnRVFControlNeeded;
	__property OnRVFPictureNeeded;
	__property OnRVMouseDown;
	__property OnRVMouseMove;
	__property OnRVMouseUp;
	__property OnRVRightClick;
	__property OnSaveComponentToFile;
	__property OnSaveDocXExtra;
	__property OnSaveHTMLExtra;
	__property OnSaveImage2;
	__property OnSaveItemToFile;
	__property OnSaveRTFExtra;
	__property OnSelect;
	__property OnSmartPopupClick;
	__property OnSpellingCheck;
	__property OnStyleTemplatesChange;
	__property OnVScrolled;
	__property OnWriteHyperlink;
	__property AllowSelection;
	__property SingleClick;
	__property OnURLNeeded;
public:
	/* TCustomRichViewEdit.Create */ inline __fastcall virtual TRichViewEdit(System::Classes::TComponent* AOwner) : TCustomRichViewEdit(AOwner) { }
	/* TCustomRichViewEdit.Destroy */ inline __fastcall virtual ~TRichViewEdit(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TRichViewEdit(HWND ParentWindow) : TCustomRichViewEdit(ParentWindow) { }
	
};


enum DECLSPEC_DENUM TRichViewUnicodeInput : unsigned char { rvuiStandard, rvuiAlternative };

//-- var, const, procedure ---------------------------------------------------
static const System::Word WM_RVUNDOFROMINPLACE = System::Word(0x40d);
static const System::Word WM_RVREDOFROMINPLACE = System::Word(0x40e);
extern DELPHI_PACKAGE TRichViewUnicodeInput RichViewUnicodeInput;
}	/* namespace Rvedit */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVEDIT)
using namespace Rvedit;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RveditHPP
