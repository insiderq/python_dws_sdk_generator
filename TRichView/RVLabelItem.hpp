﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVLabelItem.pas' rev: 27.00 (Windows)

#ifndef RvlabelitemHPP
#define RvlabelitemHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvlabelitem
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVLabelItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVLabelItemInfo : public Rvitem::TRVRectItemInfo
{
	typedef Rvitem::TRVRectItemInfo inherited;
	
private:
	int Width;
	int Height;
	int Descend;
	int YOffs;
	Rvstyle::TRVStyleLength FMinWidth;
	System::Classes::TAlignment FAlignment;
	bool FCanUseCustomPPI;
	System::Classes::TPersistent* FParentRVData;
	int FTextStyleNo;
	System::UnicodeString FText;
	void __fastcall SetMinWidth(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetAlignment(const System::Classes::TAlignment Value);
	Rvstyle::TRVStyle* __fastcall GetRVStyle(void);
	void __fastcall SetText(const System::UnicodeString Value);
	
protected:
	bool FUpdated;
	virtual void __fastcall DoPaint(const System::Types::TRect &r, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, Rvstyle::TRVColorMode ColorMode, const System::UnicodeString Text, System::Classes::TPersistent* RVData);
	virtual int __fastcall GetAssociatedTextStyleNo(void);
	virtual void __fastcall SetAssociatedTextStyleNo(int Value);
	virtual void __fastcall SetParentRVData(System::Classes::TPersistent* const Value);
	void __fastcall SavePropertiesToRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData);
	void __fastcall LoadPropertiesFromRVF(const Rvtypes::TRVRawByteString s, int Index, System::Classes::TPersistent* RVData, bool UTF8Strings, bool &TextStyleNameUsed);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	DYNAMIC System::UnicodeString __fastcall GetTextForPrintMeasuring(System::Classes::TPersistent* RVData);
	virtual System::UnicodeString __fastcall GetTextForPrinting(System::Classes::TPersistent* RVData, Dlines::TRVDrawLineInfo* DrawItem);
	virtual int __fastcall GetTextStyleNo(void);
	virtual void __fastcall SetTextStyleNo(const int Value);
	void __fastcall DoUpdateMe(int &Width, int &Height, int &YOffs, int &Descend, bool CanUseCustomPPI, bool CompareMinWidth, System::Classes::TPersistent* RVData, const System::UnicodeString Text, Vcl::Graphics::TCanvas* DefCanvas);
	Rvtypes::TRVRawByteString __fastcall GetAsText(System::Classes::TPersistent* RVData, const System::UnicodeString TextToReturn, bool Unicode, Rvstyle::TRVCodePage CodePage);
	
public:
	bool ProtectTextStyleNo;
	bool RemoveInternalLeading;
	System::Uitypes::TCursor Cursor;
	__fastcall virtual TRVLabelItemInfo(System::Classes::TPersistent* RVData);
	__fastcall TRVLabelItemInfo(System::Classes::TPersistent* RVData, int TextStyleNo, const System::UnicodeString Text);
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	DYNAMIC bool __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	virtual bool __fastcall GetBoolValue(Rvitem::TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(Rvitem::TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	DYNAMIC void __fastcall AfterLoading(Rvstyle::TRVLoadFormat FileFormat);
	DYNAMIC bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, Rvitem::TRVFReadMode &ReadMode, Rvitem::TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall Assign(Rvitem::TCustomRVItemInfo* Source);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall ApplyStyleConversion(System::Classes::TPersistent* RVData, int ItemNo, int UserData, Rvitem::TRVEStyleConversionType ConvType, bool Recursive);
	void __fastcall UpdateMe(void);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	int __fastcall GetFinalPrintingWidth(Vcl::Graphics::TCanvas* Canvas, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall Execute(System::Classes::TPersistent* RVData);
	void __fastcall DoSaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	virtual void __fastcall Inserted(System::TObject* RVData, int ItemNo);
	void __fastcall Changed(void);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraStrPropertyEx(int Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrPropertyEx(int Prop, System::UnicodeString &Value);
	DYNAMIC void __fastcall SaveFormattingToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFormattingFromStream(System::Classes::TStream* Stream);
	__property int TextStyleNo = {read=GetTextStyleNo, write=SetTextStyleNo, nodefault};
	__property System::UnicodeString Text = {read=FText, write=SetText};
	__property Rvstyle::TRVStyleLength MinWidth = {read=FMinWidth, write=SetMinWidth, nodefault};
	__property System::Classes::TAlignment Alignment = {read=FAlignment, write=SetAlignment, nodefault};
	__property Rvstyle::TRVStyle* RVStyle = {read=GetRVStyle};
	__property System::Classes::TPersistent* ParentRVData = {read=FParentRVData, write=SetParentRVData};
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVLabelItemInfo(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const short rvsLabel = short(-200);
static const System::Byte rveipcProtectTextStyleNo = System::Byte(0xc8);
static const System::Byte rveipcRemoveInternalLeading = System::Byte(0xc9);
static const System::Byte rveipcCursor = System::Byte(0xca);
static const System::Byte rveipcMinWidth = System::Byte(0xcb);
static const System::Byte rveipcAlignment = System::Byte(0xcc);
static const System::Byte rvespcText = System::Byte(0xc8);
}	/* namespace Rvlabelitem */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVLABELITEM)
using namespace Rvlabelitem;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvlabelitemHPP
