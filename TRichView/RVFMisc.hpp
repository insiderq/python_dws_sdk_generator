﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVFMisc.pas' rev: 27.00 (Windows)

#ifndef RvfmiscHPP
#define RvfmiscHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvfmisc
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool RichViewWriteControlIgnoreChildren;
extern DELPHI_PACKAGE void __fastcall RVFWrite(System::Classes::TStream* Stream, const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE void __fastcall RVFWriteLine(System::Classes::TStream* Stream, Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE void __fastcall RVFWriteLineX(System::Classes::TStream* Stream, const Rvtypes::TRVRawByteString s, bool Unicode, bool HexUnicode);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVFStream2TextString(Rvclasses::TRVMemoryStream* Stream);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVEncodeString(const Rvtypes::TRVAnsiString str);
extern DELPHI_PACKAGE bool __fastcall RVFTextString2Stream(const Rvtypes::TRVAnsiString str, Rvclasses::TRVMemoryStream* Stream);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVDecodeString(const Rvtypes::TRVAnsiString str);
extern DELPHI_PACKAGE void __fastcall RVFSaveStreamToStream(System::Classes::TStream* SourceStream, System::Classes::TStream* Stream);
extern DELPHI_PACKAGE Rvclasses::TRVMemoryStream* __fastcall RVFStringToStream(const Rvtypes::TRVRawByteString str);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVEncodeWideString(const Rvtypes::TRVUnicodeString str);
extern DELPHI_PACKAGE Rvtypes::TRVUnicodeString __fastcall RVDecodeWideString(const Rvtypes::TRVAnsiString str);
extern DELPHI_PACKAGE bool __fastcall RVFLoadPicture(const Rvtypes::TRVAnsiString s, Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVFSavePicture(Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVFSavePictureBinaryWithoutSize(Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE void __fastcall RVFLoadPictureBinary(const Rvtypes::TRVAnsiString Data, Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE void __fastcall RVFLoadPictureBinary2(System::Classes::TStream* AStream, Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE void __fastcall RVFSavePictureBinary(System::Classes::TStream* Stream, Vcl::Graphics::TGraphic* gr);
extern DELPHI_PACKAGE bool __fastcall RVFLoadControl(const Rvtypes::TRVAnsiString s, System::Classes::TComponent* &ctrl, const System::UnicodeString ClassName, Vcl::Controls::TWinControl* ParentControl, System::Classes::TReaderError OnError);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVFSaveControl(System::Classes::TComponent* ctrl);
extern DELPHI_PACKAGE bool __fastcall RVFLoadControlBinary(const Rvtypes::TRVRawByteString Data, System::Classes::TComponent* &ctrl, const System::UnicodeString ClassName, Vcl::Controls::TWinControl* ParentControl, System::Classes::TReaderError OnError);
extern DELPHI_PACKAGE void __fastcall RVFSaveControlBinary(System::Classes::TStream* Stream, System::Classes::TComponent* ctrl);
extern DELPHI_PACKAGE bool __fastcall RVFReadString(char * &P, Rvtypes::TRVRawByteString &s);
extern DELPHI_PACKAGE bool __fastcall RVFReadInteger(char * &P, int &V);
extern DELPHI_PACKAGE bool __fastcall RVFReadInteger2(System::WideChar * &P, int &V);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFReadText(char * &P);
extern DELPHI_PACKAGE bool __fastcall RVFReadTag(char * &P, bool TagsArePChars, bool Quoted, Rvstyle::TRVTag &Tag, bool UTF8Strings);
extern DELPHI_PACKAGE bool __fastcall RVFReadParaStyle(Rvstyle::TRVStyle* RVStyle, char * &P, int &V, bool UTF8Strings, bool &StyleNameUsed);
extern DELPHI_PACKAGE bool __fastcall RVFReadTextStyle(Rvstyle::TRVStyle* RVStyle, char * &P, int &V, bool UTF8Strings, bool &StyleNameUsed);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFSaveTag(bool TagsArePChars, const Rvstyle::TRVTag Tag);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFSaveText(Rvstyle::TRVStyle* RVStyle, bool UseStyleNames, int TextIdx);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFSavePara(Rvstyle::TRVStyle* RVStyle, bool UseStyleNames, int TextIdx);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFItemSavePara(int ParaNo, System::Classes::TPersistent* RVData, bool ForceSameAsPrev);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFEncodeLineBreaks(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVFDecodeLineBreaks(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall StringToRVFString(const System::UnicodeString s);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVFStringToString(const Rvtypes::TRVRawByteString s, bool UTF8);
}	/* namespace Rvfmisc */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVFMISC)
using namespace Rvfmisc;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvfmiscHPP
