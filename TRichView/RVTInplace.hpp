﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVTInplace.pas' rev: 27.00 (Windows)

#ifndef RvtinplaceHPP
#define RvtinplaceHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVERVData.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVRTFProps.hpp>	// Pascal unit
#include <Winapi.RichEdit.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVTable.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvtinplace
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVTableInplaceRVData;
class PASCALIMPLEMENTATION TRVTableInplaceRVData : public Rvervdata::TRVEditRVData
{
	typedef Rvervdata::TRVEditRVData inherited;
	
private:
	bool resized;
	
protected:
	virtual Crvfdata::TRVSoftPageBreakList* __fastcall GetSoftPageBreaks(void);
	
public:
	bool DrawOnParentMode;
	Rvfuncs::TRVVerticalAlign VAlignment;
	virtual Rvback::TRVBackground* __fastcall GetBackground(void);
	virtual void __fastcall DrawBackground(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &r, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowThumbnails);
	DYNAMIC void __fastcall GetParentInfo(int &ParentItemNo, Rvitem::TRVStoreSubRVData* &Location);
	DYNAMIC bool __fastcall CanLoadLayout(void);
	virtual Crvdata::TCustomRVData* __fastcall GetSourceRVData(void);
	DYNAMIC void __fastcall ShowRectangle(int Left, int Top, int Width, int Height);
	virtual void __fastcall SetDocumentAreaSize(int Width, int Height, bool UpdateH);
	DYNAMIC int __fastcall BuildJumpsInfo(bool IgnoreReadOnly);
	int __fastcall ReallyBuildJumpsInfo(void);
	DYNAMIC void __fastcall ClearJumpsInfo(void);
	DYNAMIC void __fastcall GetOriginEx(int &ALeft, int &ATop);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteParentData(void);
	virtual Crvdata::TCustomRVData* __fastcall GetAbsoluteRootData(void);
	DYNAMIC void __fastcall DoRVDblClick(const Rvtypes::TRVRawByteString ClickedWord, int StyleNo);
	virtual void __fastcall GetItemBackground(int ItemNo, const System::Types::TRect &r, bool MakeImageRect, System::Uitypes::TColor &Color, Vcl::Graphics::TBitmap* &bmp, bool &UseBitmap, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowPictures, bool AllowThumbnails);
	virtual Rvfuncs::TRVVerticalAlign __fastcall GetVerticalAlign(void);
	virtual void __fastcall PaintTo(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &AClipRect, bool StrictTop, bool StrictBottom, bool InplaceOnMainCanvas, bool PrintMode, bool UseWordPainters, int ExtraXOffs, int ExtraYOffs, int MinItemNo, int MaxItemNo, Rvitem::TRVPaintItemPart* MinItemPart, Rvitem::TRVPaintItemPart* MaxItemPart);
	DYNAMIC System::UnicodeString __fastcall GetItemHint(Crvdata::TCustomRVData* RVData, int ItemNo, const System::UnicodeString UpperRVDataHint);
	virtual int __fastcall GetHOffs(void);
	virtual int __fastcall GetVOffs(void);
	virtual Rvstyle::TRVDocRotation __fastcall GetRotation(void);
	virtual int __fastcall GetAreaWidthRot(void);
	virtual int __fastcall GetWidthRot(void);
	virtual int __fastcall GetHeightRot(void);
	DYNAMIC void __fastcall GetPrerotatedSize(int &W, int &H);
public:
	/* TRVEditRVData.Create */ inline __fastcall virtual TRVTableInplaceRVData(Rvscroll::TRVScroller* RichView) : Rvervdata::TRVEditRVData(RichView) { }
	/* TRVEditRVData.Destroy */ inline __fastcall virtual ~TRVTableInplaceRVData(void) { }
	
};


typedef void __fastcall (__closure *TRVTableInplaceChangeEvent)(Rvedit::TCustomRichViewEdit* Sender, bool ClearRedo);

class DELPHICLASS TRVTableInplaceEdit;
class PASCALIMPLEMENTATION TRVTableInplaceEdit : public Rvedit::TRichViewEdit
{
	typedef Rvedit::TRichViewEdit inherited;
	
private:
	Crvfdata::TCustomRVFormattedData* FRVData;
	Rvtable::TRVTableItemInfo* FTable;
	bool FClearing;
	System::Uitypes::TColor FColor;
	TRVTableInplaceChangeEvent FOnChangeEx;
	bool FTransparent;
	MESSAGE void __fastcall CMRelease(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMMoveEditor(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMExpandSel(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMInplaceUndo(Winapi::Messages::TMessage &Message);
	MESSAGE void __fastcall WMInplaceRedo(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMUndo(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall EMUndo(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall EMRedo(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall EMCanUndo(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall EMCanRedo(Winapi::Messages::TMessage &Message);
	
protected:
	virtual Rvrvdata::TRichViewRVDataClass __fastcall GetDataClass(void);
	DYNAMIC void __fastcall KeyPress(System::WideChar &Key);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	virtual Rvrtfprops::TRVRTFReaderProperties* __fastcall GetRTFReadProperties(void);
	virtual Rvdocparams::TRVDocParameters* __fastcall GetDocParameters(void);
	virtual void __fastcall WndProc(Winapi::Messages::TMessage &Message);
	virtual void __fastcall SetReadOnly(const bool Value);
	virtual bool __fastcall GetReadOnly(void);
	virtual void __fastcall InplaceRedrawing(bool AllowRedrawItself);
	virtual void __fastcall Paint(void);
	virtual bool __fastcall RequiresFullRedraw(void);
	
public:
	int FClickTime;
	int FTableItemNo;
	Rvtable::TRVTableCellData* FCell;
	int FRow;
	int FCol;
	bool NormalScrolling;
	__fastcall virtual TRVTableInplaceEdit(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVTableInplaceEdit(void);
	DYNAMIC bool __fastcall RTFReaderAssigned(void);
	DYNAMIC bool __fastcall DocParametersAssigned(void);
	DYNAMIC void __fastcall DoChange(bool ClearRedo);
	void __fastcall SetParentRVData(Crvfdata::TCustomRVFormattedData* RVData);
	void __fastcall SetCell(int Row, int Col, Rvtable::TRVTableItemInfo* Table, int CellHPadding, int CellVPadding);
	void __fastcall SetCellPaddings(int CellHPadding, int CellVPadding);
	void __fastcall UpdateVerticalAlignment(void);
	virtual void __fastcall SetVPos(int p, bool Redraw);
	virtual void __fastcall SetHPos(int p);
	void __fastcall SetClearingState(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall SelectCompletely(bool Select);
	virtual bool __fastcall BeforeChange(bool FromOutside);
	bool __fastcall Resized(void);
	DYNAMIC void __fastcall Undo(void);
	DYNAMIC void __fastcall Redo(void);
	DYNAMIC void __fastcall Click(void);
	DYNAMIC void __fastcall DragDrop(System::TObject* Source, int X, int Y);
	DYNAMIC void __fastcall DoEndDrag(System::TObject* Target, int X, int Y);
	__property TRVTableInplaceChangeEvent OnChangeEx = {read=FOnChangeEx, write=FOnChangeEx};
	__property bool ReadOnly = {read=GetReadOnly, write=SetReadOnly, nodefault};
	__property bool Transparent = {read=FTransparent, write=FTransparent, nodefault};
	__property Rvtable::TRVTableItemInfo* Table = {read=FTable};
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVTableInplaceEdit(HWND ParentWindow) : Rvedit::TRichViewEdit(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Word WM_RVMOVEEDITOR = System::Word(0x40a);
static const System::Word WM_RVINPLACEUNDO = System::Word(0x40b);
static const System::Word WM_RVINPLACEREDO = System::Word(0x40c);
static const System::Word WM_RVEXPANDSEL = System::Word(0x412);
}	/* namespace Rvtinplace */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVTINPLACE)
using namespace Rvtinplace;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvtinplaceHPP
