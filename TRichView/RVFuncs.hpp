﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVFuncs.pas' rev: 27.00 (Windows)

#ifndef RvfuncsHPP
#define RvfuncsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvfuncs
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVVerticalAlign : unsigned char { rvveraTop, rvveraMiddle, rvveraBottom };

class DELPHICLASS TCustomRVRTFTables;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVRTFTables : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	virtual void __fastcall ClearColors(void) = 0 ;
	virtual void __fastcall ClearFonts(void) = 0 ;
	virtual void __fastcall AddColor(System::Uitypes::TColor Color) = 0 ;
	virtual int __fastcall AddFontFromStyle(Rvstyle::TCustomRVFontInfo* TextStyle) = 0 ;
	virtual int __fastcall AddFontFromStyleEx(Rvstyle::TCustomRVFontInfo* TextStyle, const Rvstyle::TRVFontInfoProperties &ValidProperties, bool Root) = 0 ;
	virtual int __fastcall AddFont(Vcl::Graphics::TFont* Font) = 0 ;
	virtual int __fastcall GetColorIndex(System::Uitypes::TColor Color) = 0 ;
	virtual int __fastcall GetFontStyleIndex(Rvstyle::TCustomRVFontInfo* TextStyle) = 0 ;
	virtual int __fastcall GetFontIndex(Vcl::Graphics::TFont* Font) = 0 ;
	virtual void __fastcall ClearStyleSheetMap(void);
	virtual void __fastcall AddStyleSheetMap(int StyleTemplateNo);
	virtual int __fastcall GetStyleSheetIndex(int StyleTemplateNo, bool CharStyle);
public:
	/* TObject.Create */ inline __fastcall TCustomRVRTFTables(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomRVRTFTables(void) { }
	
};

#pragma pack(pop)

typedef bool __fastcall (*TCustomRVIsURLFunction)(const System::UnicodeString Word);

enum DECLSPEC_DENUM TRVGraphicType : unsigned char { rvgtBitmap, rvgtIcon, rvgtMetafile, rvgtJPEG, rvgtPNG, rvgtOther };

enum DECLSPEC_DENUM TRVLineStyle : unsigned char { rvlsNormal, rvlsRoundDotted, rvlsDotted, rvlsDashed, rvlsDashDotted, rvlsDashDotDotted, rvlsHidden };

typedef Vcl::Graphics::TGraphic* __fastcall (*TRV_CreateGraphicsFunction)(Vcl::Graphics::TGraphicClass GraphicClass);

typedef TRVGraphicType __fastcall (*TRV_GetGraphicTypeFunction)(Vcl::Graphics::TGraphic* Graphic);

typedef void __fastcall (*TRV_AfterImportGraphicsProc)(Vcl::Graphics::TGraphic* Graphic);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE int RichViewPixelsPerInch;
static const System::Extended RVEps = 1.000000E-20;
extern DELPHI_PACKAGE TCustomRVIsURLFunction RVIsCustomURL;
extern DELPHI_PACKAGE TRV_CreateGraphicsFunction RV_CreateGraphics _DEPRECATED_ATTRIBUTE1("Use RVGraphicHandler.CreateGraphic") ;
extern DELPHI_PACKAGE TRV_AfterImportGraphicsProc RV_AfterImportGraphic _DEPRECATED_ATTRIBUTE1("Use RVGraphicHandler.AfterImportGraphic") ;
extern DELPHI_PACKAGE bool RichViewAlternativePicPrint;
extern DELPHI_PACKAGE System::Types::TPoint __fastcall RVGetCursorPos(void);
extern DELPHI_PACKAGE int __fastcall RVGetPositiveValue(int a);
extern DELPHI_PACKAGE Winapi::Windows::POutlineTextmetricW __fastcall RV_GetOutlineTextMetrics(Vcl::Graphics::TCanvas* Canvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE Rvstyle::TRVTag __fastcall RV_CopyTag(const Rvstyle::TRVTag SourceTag, bool TagsArePChars);
extern DELPHI_PACKAGE bool __fastcall RV_CompareTags(const Rvstyle::TRVTag Tag1, const Rvstyle::TRVTag Tag2, bool TagsArePChars);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_ReplaceTabsA(const Rvtypes::TRVAnsiString s, int SpacesInTab);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_ReplaceTabsW(const Rvtypes::TRVRawByteString s, int SpacesInTab);
extern DELPHI_PACKAGE void __fastcall RV_ReplaceStr(System::UnicodeString &str, System::UnicodeString oldstr, System::UnicodeString newstr);
extern DELPHI_PACKAGE void __fastcall RV_ReplaceStrA(Rvtypes::TRVAnsiString &str, Rvtypes::TRVAnsiString oldstr, Rvtypes::TRVAnsiString newstr);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_GetHintStr(Rvstyle::TRVSaveFormat DocFormat, const System::UnicodeString Hint);
extern DELPHI_PACKAGE int __fastcall RV_GetDefSubSuperScriptSize(int NormalSize);
extern DELPHI_PACKAGE int __fastcall RV_GetDefSubSuperScriptSizeRev(int ScriptSize);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_IntToRoman(int Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_IntToAlpha(int Value);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetColorHex(System::Uitypes::TColor Color, bool AllowAuto);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetHTMLRGBStr(System::Uitypes::TColor Color, bool Quotes);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_GetHTMLRGBStr2(System::Uitypes::TColor Color, bool Quotes);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetCSSBkColor(System::Uitypes::TColor Color);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_GetHTMLPath(const System::UnicodeString Path);
extern DELPHI_PACKAGE void __fastcall RV_AddStrEx(System::UnicodeString &s1, const System::UnicodeString s2, const System::UnicodeString Delimiter);
extern DELPHI_PACKAGE void __fastcall RV_AddStr(System::UnicodeString &s1, const System::UnicodeString s2);
extern DELPHI_PACKAGE void __fastcall RV_AddStrExA(Rvtypes::TRVAnsiString &s1, const Rvtypes::TRVAnsiString s2, const Rvtypes::TRVAnsiString Delimiter);
extern DELPHI_PACKAGE void __fastcall RV_AddStrA(Rvtypes::TRVAnsiString &s1, const Rvtypes::TRVAnsiString s2);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_GetHTMLFontCSS(Vcl::Graphics::TFont* Font, bool UseFontName);
extern DELPHI_PACKAGE int __fastcall RV_HTMLGetFontSize(int pts);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_HTMLOpenFontTag(Rvstyle::TFontInfo* ts, Rvstyle::TFontInfo* normalts, bool Relative, Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_HTMLOpenFontTag2(Vcl::Graphics::TFont* fnt, Rvstyle::TFontInfo* normalts, bool UseFontName, Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_HTMLCloseFontTag(Rvstyle::TFontInfo* ts, Rvstyle::TFontInfo* normalts, bool Relative);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_HTMLCloseFontTag2(Vcl::Graphics::TFont* fnt, Rvstyle::TFontInfo* normalts, bool UseFontName);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeUniSymbolStrA(const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_SymbolCharsetToAnsiCharset(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeUniSymbolStrRaw(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeUniWingdingsStrA(const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeUniWingdingsStrRaw(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_ProcessOOXMLStrA(const Rvtypes::TRVAnsiString str, bool EncodeQuotes);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_ProcessOOXMLValueStr(const System::UnicodeString str);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeOOXMLStrW(const Rvtypes::TRVUnicodeString str);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeOOXMLStr(const System::UnicodeString str);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeOOXMLValueStr(const System::UnicodeString str);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeOOXMLURLStr(const System::UnicodeString str, bool AlreadyPercentEncoded);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeOOXMLStrEx(const System::UnicodeString str, Rvstyle::TRVCodePage CodePage);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetXMLBoolPropElement(const Rvtypes::TRVAnsiString Name, bool f);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetXMLStrPropElement(const Rvtypes::TRVAnsiString Name, const Rvtypes::TRVAnsiString Val);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetXMLIntPropElement(const Rvtypes::TRVAnsiString Name, int v);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_GetXMLColorPropElement(const Rvtypes::TRVAnsiString Name, System::Uitypes::TColor c);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeXMLNSList(Rvtypes::TRVAnsiString const *Chars, const int Chars_High);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeHTMLSymbolStrA(const Rvtypes::TRVAnsiString s, bool UTF8, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeHTMLSymbolStrRaw(const Rvtypes::TRVRawByteString s, bool UTF8, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeHTMLSymbolStr(const System::UnicodeString s, bool UTF8, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeHTMLWingdingsStrA(const Rvtypes::TRVAnsiString s, bool UTF8, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeHTMLWingdingsStrRaw(const Rvtypes::TRVRawByteString s, bool UTF8, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_MakeHTMLWingdingsStr(const System::UnicodeString s, bool UTF8, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_MakeHTMLStr(const Rvtypes::TRVAnsiString str, bool SpecialCode, bool NextSpace);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_MakeHTMLValueStr(const System::UnicodeString str);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_MakeHTMLURLStr(const System::UnicodeString str);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_CharSet2HTMLLang(System::Uitypes::TFontCharset CharSet);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_DecodeURL(const System::UnicodeString s, bool DecodeLineBreaks);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_EncodeURL(const System::UnicodeString s);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_EncodeSpacesInURL(const System::UnicodeString s);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_HTMLGetEndingSlash(Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_HTMLGetNoValueAttribute(const Rvtypes::TRVAnsiString Attr, Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RV_HTMLGetIntAttrVal2(int Value, Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_HTMLGetIntAttrVal(int Value, Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RV_HTMLGetStrAttrVal(const Rvtypes::TRVAnsiString Value, Rvstyle::TRVSaveOptions SaveOptions);
extern DELPHI_PACKAGE System::UnicodeString __fastcall MakeRTFBookmarkNameStr(const System::UnicodeString s);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVMakeRTFStr(const Rvtypes::TRVAnsiString s, bool SpecialCode, bool UseNamedEntities, bool RTFControlsToCodes);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVMakeRTFStrW(const Rvtypes::TRVUnicodeString s, unsigned CodePage, bool SaveAnsi, bool ForceSaveAnsi, bool SpecialCode, bool RTFControlsToCodes);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVMakeRTFFileNameStr(const System::UnicodeString s, unsigned CodePage, bool SaveAnsi);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall MakeRTFIdentifierStr(const System::UnicodeString s, unsigned CodePage, bool SaveAnsi);
extern DELPHI_PACKAGE void __fastcall RVWriteUnicodeRTFStr(System::Classes::TStream* Stream, const Rvtypes::TRVRawByteString s, unsigned CodePage, bool SaveAnsi, bool ForceSaveAnsi, bool SpecialCode, bool DoubleBSlashes, bool RTFControlsToCodes);
extern DELPHI_PACKAGE bool __fastcall RVIsURL(const System::UnicodeString s);
extern DELPHI_PACKAGE bool __fastcall RVIsEmail(const System::UnicodeString s);
extern DELPHI_PACKAGE int __fastcall RV_XToDevice(int X, const Rvstyle::TRVScreenAndDevice &sad);
extern DELPHI_PACKAGE int __fastcall RV_YToDevice(int Y, const Rvstyle::TRVScreenAndDevice &sad);
extern DELPHI_PACKAGE int __fastcall RV_XToScreen(int X, const Rvstyle::TRVScreenAndDevice &sad);
extern DELPHI_PACKAGE int __fastcall RV_YToScreen(int Y, const Rvstyle::TRVScreenAndDevice &sad);
extern DELPHI_PACKAGE void __fastcall RV_RectToScreen(System::Types::TRect &R, const Rvstyle::TRVScreenAndDevice &sad);
extern DELPHI_PACKAGE void __fastcall RV_InfoAboutSaD(Rvstyle::TRVScreenAndDevice &sad, Vcl::Graphics::TCanvas* Canvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE int __fastcall RV_GetPixelsPerInch(void);
extern DELPHI_PACKAGE bool __fastcall RV_PointInRect(int X, int Y, int Left, int Top, int Width, int Height);
extern DELPHI_PACKAGE void __fastcall RVFillRotationMatrix(tagXFORM &XForm, Rvstyle::TRVDocRotation Rotation, int Left, int Top, int Width, int Height, int DocHeight, TRVVerticalAlign VAlign);
extern DELPHI_PACKAGE Rvstyle::TRVLength __fastcall RV_UnitsPerInch(Rvstyle::TRVUnits Units);
extern DELPHI_PACKAGE int __fastcall RV_UnitsToTwips(Rvstyle::TRVLength Value, Rvstyle::TRVUnits Units);
extern DELPHI_PACKAGE Rvstyle::TRVLength __fastcall RV_TwipsToUnits(int Value, Rvstyle::TRVUnits Units);
extern DELPHI_PACKAGE Rvstyle::TRVLength __fastcall RV_UnitsToUnits(Rvstyle::TRVLength Value, Rvstyle::TRVUnits OldUnits, Rvstyle::TRVUnits NewUnits);
extern DELPHI_PACKAGE int __fastcall RV_UnitsToPixels(Rvstyle::TRVLength Value, Rvstyle::TRVUnits Units, int PixelsPerInch);
extern DELPHI_PACKAGE Vcl::Graphics::TGraphic* __fastcall RV_CreateGraphicsDefault(Vcl::Graphics::TGraphicClass GraphicClass);
extern DELPHI_PACKAGE void __fastcall RV_AfterImportGraphicDefault(Vcl::Graphics::TGraphic* Graphic);
extern DELPHI_PACKAGE int __fastcall RV_GetLuminance(System::Uitypes::TColor Color);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall RV_GetGray(System::Uitypes::TColor Color);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall RV_ChangeColorLuminance(System::Uitypes::TColor Color, float Ratio);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall RV_GetPrnColor(System::Uitypes::TColor Color);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall RV_GetColor(System::Uitypes::TColor Color, Rvstyle::TRVColorMode ColorMode);
extern DELPHI_PACKAGE System::Uitypes::TColor __fastcall RV_GetBackColor(System::Uitypes::TColor Color, Rvstyle::TRVColorMode ColorMode);
extern DELPHI_PACKAGE void __fastcall RVFreeAndNil(void *Obj);
extern DELPHI_PACKAGE void __fastcall RV_BltTBitmapAsDib(HDC DestDc, int X, int Y, int PrintWidth, int PrintHeight, Vcl::Graphics::TBitmap* bm);
extern DELPHI_PACKAGE void __fastcall RV_PictureToDeviceAlt(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int PrintWidth, int PrintHeight, Vcl::Graphics::TBitmap* gr);
extern DELPHI_PACKAGE void __fastcall RV_PictureToDevice(Vcl::Graphics::TCanvas* Canvas, int x, int y, int width, int height, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TGraphic* gr, bool ToScreen, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE void __fastcall ShadeRectangle(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, System::Uitypes::TColor Color);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVExtractDomain(const System::UnicodeString URL);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVExtractDomainOrDrive(const System::UnicodeString Path);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVExtractRelativePath(const System::UnicodeString BaseName, const System::UnicodeString DestName);
extern DELPHI_PACKAGE int __fastcall RV_Sign(int Value);
extern DELPHI_PACKAGE bool __fastcall RVTryOpenClipboard(void);
extern DELPHI_PACKAGE void __fastcall RVDrawCustomHLine(Vcl::Graphics::TCanvas* Canvas, System::Uitypes::TColor Color, TRVLineStyle LineStyle, int LineWidth, int Left, int Right, int Y, int PeriodLength, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE int __fastcall RVGetDefaultUnderlineWidth(int FontSize);
extern DELPHI_PACKAGE void __fastcall RVDrawLineBetweenRects(int Left1, int Top1, int Width1, int Height1, int Left2, int Top2, int Width2, int Height2, Vcl::Graphics::TCanvas* Canvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
}	/* namespace Rvfuncs */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVFUNCS)
using namespace Rvfuncs;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvfuncsHPP
