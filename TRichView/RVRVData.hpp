﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVRVData.pas' rev: 27.00 (Windows)

#ifndef RvrvdataHPP
#define RvrvdataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVCtrlData.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <RVDragDrop.hpp>	// Pascal unit
#include <RVPopup.hpp>	// Pascal unit
#include <RVDocParams.hpp>	// Pascal unit
#include <RVSelectionHandles.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVAnimate.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVFontCache.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvrvdata
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVFGlobalReadData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFGlobalReadData : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Rvclasses::TRVIntegerList* RVFTextStylesMapping;
	Rvclasses::TRVIntegerList* RVFParaStylesMapping;
	Rvclasses::TRVIntegerList* RVFListStylesMapping;
	Rvstyle::TRVStyleUnits Units;
	__fastcall virtual ~TRVFGlobalReadData(void);
public:
	/* TObject.Create */ inline __fastcall TRVFGlobalReadData(void) : System::TObject() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRichViewRVData;
class PASCALIMPLEMENTATION TRichViewRVData : public Rvctrldata::TRVControlData
{
	typedef Rvctrldata::TRVControlData inherited;
	
private:
	System::Classes::TStrings* __fastcall GetExtraDocumentsList(void);
	void __fastcall SetExtraDocumentsList(System::Classes::TStrings* const Value);
	
protected:
	TRVFGlobalReadData* FRVFGlobalReadData;
	System::Classes::TStringList* FExtraDocuments;
	Crvfdata::TRVDataDrawHyperlinkEvent FOnDrawHyperlink;
	Rvscroll::TRVScroller* FRichView;
	Rvfontcache::TRVFontInfoCache* FFontInfoCache;
	Crvdata::TRVThumbnailCache* FThumbnailCache;
	int FClickX;
	int FClickY;
	Rvselectionhandles::TRVSelectionHandles* FSelectionHandles;
	Rvseqitem::TRVSeqList* FSeqList;
	Rvmarker::TRVMarkerList* FMarkers;
	DYNAMIC void __fastcall DestroyMarkers(void);
	DYNAMIC void __fastcall DestroySeqList(void);
	DYNAMIC Vcl::Controls::TControl* __fastcall GetInplaceEditor(void);
	DYNAMIC void __fastcall DestroyInplaceEditor(void);
	DYNAMIC System::Classes::TPersistent* __fastcall GetRTFProperties(void);
	DYNAMIC bool __fastcall CanStartDragging(void);
	DYNAMIC bool __fastcall InitDragging(Rvdragdrop::TRVDropSource* &DropSource, int &OKEffect);
	DYNAMIC void __fastcall DoneDragging(bool FDeleteSelection);
	DYNAMIC void __fastcall SetClickCoords(int X, int Y);
	DYNAMIC bool __fastcall CanStartDragBecauseMouseMoved(int X, int Y);
	Rvfontcache::TRVFontInfoCache* __fastcall DoCreateFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas, Crvfdata::TCustomRVFormattedData* Owner);
	virtual int __fastcall GetMaxLength(void);
	virtual bool __fastcall IsWordWrapAllowed(void);
	
public:
	bool FPlayingAnimation;
	Rvanimate::TRVAnimatorList* FAnimatorList;
	Rvdragdrop::TRVDropSource* FDropSource;
	Rvpopup::TRVSmartPopupButton* FSmartPopupButton;
	DYNAMIC void __fastcall InitStyleMappings(Crvdata::PRVIntegerList &PTextStylesMapping, Crvdata::PRVIntegerList &PParaStylesMapping, Crvdata::PRVIntegerList &PListStylesMapping, Rvstyle::PRVStyleUnits &PUnits);
	DYNAMIC void __fastcall DoneStyleMappings(bool AsSubDoc);
	DYNAMIC void __fastcall CreateFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas);
	DYNAMIC void __fastcall DestroyFontInfoCache(Rvfontcache::TRVFontInfoCache* &Cache);
	DYNAMIC Rvfontcache::TRVFontInfoCache* __fastcall GetFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas, Crvfdata::TCustomRVFormattedData* RVData);
	DYNAMIC void __fastcall AdjustSpecialControlsCoords(Crvfdata::TCustomRVFormattedData* RVData);
	void __fastcall SetSmartPopupCoords(void);
	void __fastcall RepaintSmartPopup(void);
	DYNAMIC void __fastcall Clear(void);
	__fastcall virtual ~TRichViewRVData(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetFormatCanvasEx(Vcl::Graphics::TCanvas* DefCanvas);
	DYNAMIC bool __fastcall CanLoadLayout(void);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetExtraRTFCode(Rvstyle::TRVRTFSaveArea Area, System::TObject* Obj, int Index1, int Index2, bool InStyleSheet);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetExtraDocXCode(Rvstyle::TRVDocXSaveArea Area, System::TObject* Obj, int Index1, int Index2);
	DYNAMIC System::UnicodeString __fastcall GetExtraHTMLCode(Rvstyle::TRVHTMLSaveArea Area, bool CSSVersion);
	DYNAMIC System::UnicodeString __fastcall GetParaHTMLCode(Crvdata::TCustomRVData* RVData, int ItemNo, bool ParaStart, bool CSSVersion);
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetChosenRVData(void);
	DYNAMIC Rvitem::TCustomRVItemInfo* __fastcall GetChosenItem(void);
	DYNAMIC void __fastcall AssignChosenRVData(Crvfdata::TCustomRVFormattedData* RVData, Rvitem::TCustomRVItemInfo* Item);
	DYNAMIC void __fastcall SilentReplaceChosenRVData(Crvfdata::TCustomRVFormattedData* RVData);
	DYNAMIC void __fastcall UnassignChosenRVData(Crvdata::TCustomRVData* RVData);
	DYNAMIC Vcl::Controls::TWinControl* __fastcall GetParentControl(void);
	virtual Rvscroll::TRVPaletteAction __fastcall GetDoInPaletteMode(void);
	virtual HPALETTE __fastcall GetRVPalette(void);
	virtual Rvstyle::TRVStyle* __fastcall GetRVStyle(void);
	virtual Winapi::Windows::PLogPalette __fastcall GetRVLogPalette(void);
	virtual Crvdata::TRVThumbnailCache* __fastcall GetThumbnailCache(void);
	DYNAMIC bool __fastcall UseStyleTemplates(void);
	DYNAMIC Rvstyle::TRVStyleTemplateInsertMode __fastcall StyleTemplateInsertMode(void);
	DYNAMIC System::UnicodeString __fastcall GetURL(int id);
	DYNAMIC void __fastcall ReadHyperlink(const System::UnicodeString Target, const System::UnicodeString Extras, Rvstyle::TRVLoadFormat DocFormat, int &StyleNo, Rvstyle::TRVTag &ItemTag, Rvtypes::TRVRawByteString &ItemName);
	DYNAMIC void __fastcall WriteHyperlink(int id, Crvdata::TCustomRVData* RVData, int ItemNo, Rvstyle::TRVSaveFormat SaveFormat, System::UnicodeString &Target, System::UnicodeString &Extras);
	virtual Rvscroll::TRVOptions __fastcall GetOptions(void);
	virtual void __fastcall SetOptions(const Rvscroll::TRVOptions Value);
	DYNAMIC System::Classes::TStringList* __fastcall GetDocProperties(void);
	virtual Rvstyle::TRVFOptions __fastcall GetRVFOptions(void);
	virtual void __fastcall SetRVFOptions(const Rvstyle::TRVFOptions Value);
	virtual Rvstyle::TRVFWarnings __fastcall GetRVFWarnings(void);
	virtual void __fastcall SetRVFWarnings(const Rvstyle::TRVFWarnings Value);
	virtual Rvstyle::TRVRTFOptions __fastcall GetRTFOptions(void);
	virtual void __fastcall SetRTFOptions(const Rvstyle::TRVRTFOptions Value);
	virtual int __fastcall GetAreaWidth(void);
	virtual int __fastcall GetAreaHeight(void);
	DYNAMIC int __fastcall GetFullDocumentHeight(void);
	virtual void __fastcall GetOrigin(int &ALeft, int &ATop);
	DYNAMIC void __fastcall GetOriginEx(int &ALeft, int &ATop);
	virtual int __fastcall GetMinTextWidth(void);
	virtual int __fastcall GetMaxTextWidth(void);
	virtual int __fastcall GetLeftMargin(void);
	virtual int __fastcall GetRightMargin(void);
	virtual int __fastcall GetTopMargin(void);
	virtual int __fastcall GetBottomMargin(void);
	virtual Crvdata::TRVFlags __fastcall GetFlags(void);
	virtual void __fastcall SetFlags(const Crvdata::TRVFlags Value);
	virtual void __fastcall AdjustVScrollUnits(void);
	virtual void __fastcall SetDocumentAreaSize(int Width, int Height, bool UpdateH);
	virtual void __fastcall ScrollTo(int Y, bool Redraw);
	virtual void __fastcall HScrollTo(int X);
	virtual int __fastcall GetVSmallStep(void);
	virtual Rvback::TRVBackground* __fastcall GetBackground(void);
	DYNAMIC bool __fastcall IsAssignedCopy(void);
	DYNAMIC bool __fastcall IsAssignedRVMouseDown(void);
	DYNAMIC bool __fastcall IsAssignedRVMouseUp(void);
	DYNAMIC bool __fastcall IsAssignedRVRightClick(void);
	DYNAMIC bool __fastcall IsAssignedJump(void);
	DYNAMIC bool __fastcall IsAssignedRVDblClick(void);
	DYNAMIC bool __fastcall IsAssignedCheckpointVisible(void);
	DYNAMIC bool __fastcall IsAssignedOnProgress(void);
	DYNAMIC void __fastcall DoProgress(Rvstyle::TRVLongOperation Operation, Rvstyle::TRVProgressStage Stage, System::Byte PercentDone);
	DYNAMIC void __fastcall DoCopy(void);
	DYNAMIC void __fastcall DoRVMouseMove(int id);
	DYNAMIC void __fastcall DoRVMouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);
	DYNAMIC void __fastcall DoRVMouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);
	DYNAMIC void __fastcall DoRVRightClick(const Rvtypes::TRVRawByteString ClickedWord, int StyleNo, int X, int Y);
	DYNAMIC void __fastcall DoRVDblClick(const Rvtypes::TRVRawByteString ClickedWord, int StyleNo);
	DYNAMIC void __fastcall DoCheckpointVisible(Rvstyle::TCheckpointData CheckpointData);
	DYNAMIC void __fastcall DoDrawHyperlink(Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);
	DYNAMIC void __fastcall DoGetItemCursor(Crvdata::TCustomRVData* RVData, int ItemNo, System::Uitypes::TCursor &Cursor);
	DYNAMIC void __fastcall DoStyleTemplatesChange(void);
	DYNAMIC void __fastcall DoJump(int id);
	DYNAMIC void __fastcall DoSelect(void);
	DYNAMIC System::Uitypes::TCursor __fastcall GetNormalCursor(void);
	DYNAMIC Rvscroll::TCPEventKind __fastcall GetCPEventKind(void);
	DYNAMIC void __fastcall HTMLSaveImage(Crvdata::TCustomRVData* RVData, int ItemNo, const System::UnicodeString Path, System::Uitypes::TColor BackgroundColor, System::UnicodeString &Location, bool &DoDefault);
	DYNAMIC void __fastcall SaveImage2(Crvdata::TCustomRVData* RVData, int ItemNo, Vcl::Graphics::TGraphic* Graphic, Rvstyle::TRVSaveFormat SaveFormat, const System::UnicodeString Path, const System::UnicodeString ImagePrefix, int &ImageSaveNo, System::UnicodeString &Location, bool &DoDefault);
	__fastcall virtual TRichViewRVData(Rvscroll::TRVScroller* RichView);
	virtual int __fastcall GetHOffs(void);
	virtual int __fastcall GetVOffs(void);
	virtual int __fastcall GetRVDataExtraVOffs(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetCanvas(void);
	virtual int __fastcall GetWidth(void);
	virtual int __fastcall GetHeight(void);
	virtual System::Uitypes::TColor __fastcall GetColor(void);
	virtual System::UnicodeString __fastcall SaveComponentToFile(const System::UnicodeString Path, System::Classes::TComponent* SaveMe, Rvstyle::TRVSaveFormat SaveFormat);
	virtual bool __fastcall SaveItemToFile(const System::UnicodeString Path, Crvdata::TCustomRVData* RVData, int ItemNo, Rvstyle::TRVSaveFormat SaveFormat, bool Unicode, Rvtypes::TRVRawByteString &Text);
	DYNAMIC Vcl::Graphics::TGraphic* __fastcall ImportPicture(const System::UnicodeString Location, int Width, int Height, bool &Invalid);
	DYNAMIC System::UnicodeString __fastcall GetItemHint(Crvdata::TCustomRVData* RVData, int ItemNo, const System::UnicodeString UpperRVDataHint);
	DYNAMIC Vcl::Graphics::TGraphic* __fastcall RVFPictureNeeded(const System::UnicodeString ItemName, Rvitem::TRVNonTextItemInfo* Item, int Index1, int Index2, bool PictureInsideRVF);
	DYNAMIC Vcl::Controls::TControl* __fastcall RVFControlNeeded(const System::UnicodeString ItemName, const Rvstyle::TRVTag ItemTag);
	DYNAMIC void __fastcall SetControlHint(const System::UnicodeString Hint);
	DYNAMIC Vcl::Imglist::TCustomImageList* __fastcall RVFImageListNeeded(int ImageListTag);
	DYNAMIC System::UnicodeString __fastcall GetDelimiters(void);
	virtual Rvstyle::TRVFReaderStyleMode __fastcall GetRVFTextStylesReadMode(void);
	virtual Rvstyle::TRVFReaderStyleMode __fastcall GetRVFParaStylesReadMode(void);
	virtual Rvscroll::TRVBiDiMode __fastcall GetBiDiMode(void);
	DYNAMIC void __fastcall ControlAction2(Crvdata::TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Vcl::Controls::TControl* &Control);
	virtual void __fastcall ItemAction(Rvstyle::TRVItemAction ItemAction, Rvitem::TCustomRVItemInfo* Item, Rvtypes::TRVRawByteString &Text, Crvdata::TCustomRVData* RVData);
	DYNAMIC void __fastcall AfterAddStyle(Rvstyle::TCustomRVInfo* StyleInfo);
	DYNAMIC Rvdocparams::TRVDocParameters* __fastcall GetDocParameters(bool AllowCreate);
	DYNAMIC Crvfdata::TRVDragDropCaretInfo* __fastcall GetDragDropCaretInfo(void);
	DYNAMIC bool __fastcall IsDragging(void);
	DYNAMIC Rvselectionhandles::TRVSelectionHandles* __fastcall GetSelectionHandles(void);
	void __fastcall UpdateSelectionHandles(bool EvenForEmptySelection);
	void __fastcall CreateSelectionHandles(bool EvenForEmptySelection);
	DYNAMIC void __fastcall CreateSelectionHandlesAfterTap(void);
	void __fastcall DestroySelectionHandles(void);
	DYNAMIC void __fastcall InsertAnimator(System::TObject* &Animator);
	DYNAMIC bool __fastcall AllowAnimation(void);
	DYNAMIC void __fastcall ResetAniBackground(void);
	DYNAMIC Rvseqitem::TRVSeqList* __fastcall GetSeqList(bool AllowCreate);
	DYNAMIC System::UnicodeString __fastcall GetNoteText(void);
	DYNAMIC Rvmarker::TRVMarkerList* __fastcall GetMarkers(bool AllowCreate);
	DYNAMIC void __fastcall DoBeforeSaving(void);
	DYNAMIC System::Classes::TStrings* __fastcall GetExtraDocuments(void);
	DYNAMIC Crvdata::_di_IRVScaleRichViewInterface __fastcall GetScaleRichViewInterface(void);
	__property Rvscroll::TRVScroller* RichView = {read=FRichView};
	__property Crvfdata::TRVDataDrawHyperlinkEvent OnDrawHyperlink = {read=FOnDrawHyperlink, write=FOnDrawHyperlink};
	__property System::Classes::TStrings* ExtraDocumentsList = {read=GetExtraDocumentsList, write=SetExtraDocumentsList};
};


typedef System::TMetaClass* TRichViewRVDataClass;

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvrvdata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVRVDATA)
using namespace Rvrvdata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvrvdataHPP
