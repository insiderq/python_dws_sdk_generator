RichView 14.15 beta

Full source code (without help and demo projects)
See Install.chm for the instructions.
See "New in version 14" and "New in version 15" in the help file 
------------------------------

Additional files:

Help, demos:
http://www.trichview.com/rvfiles/richviewhelpanddemos.zip
RichViewActions:
http://www.trichview.com/resources/actions/richviewactions.zip

Installing instructions and the license:
Install.chm

FAQ about installing:
http://www.trichview.com/install-faq.html

------------------------------
History

v14.15 (2014-Apr-26)
new: packages for RAD Studio XE6

v14.14 (2014-Apr-8)
new: option rvoPercentEncodedURL for TCustomRichView.Options

v14.13.2 (2014-Mar-21)
v14.13.1 (2014-Feb-05)

v14.13 (2014-Jan-23)
new: item types TRVSideNoteItemInfo, TRVTextBoxItemInfo

v14.12.5 (2014-Jan-22)
v14.12.4 (2013-Dec-12)
v14.12.3 (2013-Dec-3)
v14.12.2 (2013-Dec-2)

v14.12.1 (2013-Nov-12)

v14.12 (2013-Oct-31)
new: new methods for changing and receiving values of integer and string properties
  of item. The most important new methods: 
  SetCurrentItemExtraIntPropertyEx, SetItemExtraIntPropertyExEd,
  SetCurrentItemExtraStrPropertyEx, SetItemExtraStrPropertyExEd.

v14.11.3 (2013-Oct-26)
v14.11.2 (2013-Oct-23)
v14.11.1 (2013-Oct-22)

v14.11 (2013-Oct-21)
new: touchscreen selection handles; new properties:
- TRVStyle.SelectionHandleKind
- TCustomRichView.SelectionHandlesVisible

v14.10 (2013-Oct-15)
new: gestures (Delphi 2010+) - panning in all controls, zooming in TRVPrintPreview

v14.9.4 (2013-Oct-9)
v14.9.3 (2013-Oct-3)
v14.9.2 (2013-Sep-28)
v14.9.1 (2013-Sep-24)

v14.9 (2013-Sep-16)
new: RAD Studio XE5 support

v14.8 (2013-Aug-23)
new: TRVStyle.TextStyles[].SizeDouble - font size in half-points
new: RichViewJustifyBeforeLineBreak global variable
new: support of high fonts (activated in RV_Defs.inc by RVHIGHFONTS in RV_Defs.inc)

v14.7.1 (2013-Aug-8)

v14.7 (2013-Jul-28)
new: new methods in TRichViewEdit: SetIntPropertyEd, SetStrPropertyEd, SetFloatPropertyEd, SetBackgroundImageEd
impr: saving addition picture-item properties to DocX (colors and border)
chg: PageNo parameter for TRVPrint.GetHeaderRect and GetFooterRect
chg: new values in TRVUndoType

v14.6 (2013-Jul-4)
new: sizes in TRVPrint measured in TRVUnits: new Margins, HeaderY, FooterY properties
new: different headers and footers for the first and odd/even pages in TRVPrint:
  new parameter in SetFooter and SetHeader, TitlePage and FacingPages properties.
new: TRVReportHelper.NoMetafiles property

v14.5.2 (2013-Jun-10)

v14.5.1 (2013-May-21)

v14.5 (2013-May-5)
new: RAD Studio XE4 support

v14.4 (2013-Apr-18)
chg: TRVGraphicHandler class

v14.3 (2013-Mar-17)
new: footnotes/endnotes using a fixed text instead of numbers

v14.2.1 (2013-Mar-15)
v14.2 (2013-Mar-14)
new: smooth image scaling
new: text style options for saving codes to DocX (rvteoDocXCode and rvteoDocXInRunCode)

v14.1 (2013-Feb-26)
new: TRichView.SaveDocX and SaveDocXToStream methods allow saving MS Word files
new: TRVOfficeConverter.ExcludeDocXExportConverter and ExcludeDocXImportConverter properties.
new: TRVStyle.FontQuality property

v14.0.3 (2012-Dec-22)
v14.0.2 (2012-Dec-21)
v14.0.1 (2012-Dec-19)

v14.0 (2012-Nov-2)