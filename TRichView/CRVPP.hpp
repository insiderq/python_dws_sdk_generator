﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRVPP.pas' rev: 27.00 (Windows)

#ifndef CrvppHPP
#define CrvppHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Crvpp
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVClickMode : unsigned char { rvcmNone, rvcmSwitchZoom };

class DELPHICLASS TRVMarginsPen;
class PASCALIMPLEMENTATION TRVMarginsPen : public Vcl::Graphics::TPen
{
	typedef Vcl::Graphics::TPen inherited;
	
__published:
	__property Style = {default=5};
	__property Color = {default=12632256};
public:
	/* TPen.Create */ inline __fastcall TRVMarginsPen(void) : Vcl::Graphics::TPen() { }
	/* TPen.Destroy */ inline __fastcall virtual ~TRVMarginsPen(void) { }
	
};


class DELPHICLASS TRVPAPen;
class PASCALIMPLEMENTATION TRVPAPen : public Vcl::Graphics::TPen
{
	typedef Vcl::Graphics::TPen inherited;
	
__published:
	__property Style = {default=5};
	__property Color = {default=255};
public:
	/* TPen.Create */ inline __fastcall TRVPAPen(void) : Vcl::Graphics::TPen() { }
	/* TPen.Destroy */ inline __fastcall virtual ~TRVPAPen(void) { }
	
};


class DELPHICLASS TCustomRVPrintPreview;
class PASCALIMPLEMENTATION TCustomRVPrintPreview : public Rvscroll::TRVScroller
{
	typedef Rvscroll::TRVScroller inherited;
	
private:
	int SavedZoomPercent;
	int FPageNo;
	int FZoomPercent;
	Rvscroll::TRVZoomMode FZoomMode;
	int FPageWidth;
	int FPageHeight;
	int FPhysPageWidth;
	int FPhysPageHeight;
	System::Types::TRect FPhysMargins;
	System::Uitypes::TCursor FZoomInCursor;
	System::Uitypes::TCursor FZoomOutCursor;
	System::Classes::TNotifyEvent FZoomChanged;
	TRVMarginsPen* FMarginsPen;
	TRVPAPen* FPrintableAreaPen;
	TRVClickMode FClickMode;
	System::Uitypes::TColor FPageBorderColor;
	System::Uitypes::TColor FShadowColor;
	int FShadowWidth;
	int FPageBorderWidth;
	int FBackgroundMargin;
	int FPanZoomPercent;
	int FPanDistance;
	System::Types::TPoint FPanPageRectOrigin;
	void __fastcall SetZoomPercent(const int Value);
	void __fastcall SetZoomMode(const Rvscroll::TRVZoomMode Value);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	HIDESBASE MESSAGE void __fastcall WMSize(Winapi::Messages::TWMSize &Message);
	void __fastcall SetZoomInCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetZoomOutCursor(const System::Uitypes::TCursor Value);
	void __fastcall SetMarginsPen(TRVMarginsPen* const Value);
	void __fastcall SetPrintableAreaPen(TRVPAPen* const Value);
	
protected:
	DYNAMIC bool __fastcall CanDrawContents(void);
	DYNAMIC void __fastcall DrawContents(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R);
	virtual void __fastcall DrawMargins(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, int PageNo);
	virtual void __fastcall DrawPrintableAreaBorder(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &R, int PageNo);
	DYNAMIC int __fastcall GetPreview100PercentWidth(void);
	DYNAMIC int __fastcall GetPreview100PercentHeight(void);
	DYNAMIC System::Types::TRect __fastcall GetPhysMargins(void);
	DYNAMIC int __fastcall GetPageCount(void);
	virtual void __fastcall Paint(void);
	virtual void __fastcall Loaded(void);
	DYNAMIC void __fastcall Click(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC int __fastcall GetDefSmallStep(void);
	void __fastcall UpdateCursor(void);
	virtual void __fastcall SetPageNo(const int Value);
	void __fastcall ZoomAt(int X, int Y, int AZoomPercent);
	void __fastcall DoZoomAt(int X, int Y, int AZoomPercent, int OldZoomPercent, int OldHPos, int OldVPos, const System::Types::TPoint &OldPageRectOrigin);
	System::Types::TRect __fastcall GetPageRect(void);
	bool __fastcall IsPointInPage(int X, int Y);
	virtual void __fastcall DoGesture(const Vcl::Controls::TGestureEventInfo &EventInfo, bool &Handled);
	DYNAMIC bool __fastcall IsTouchPropertyStored(Vcl::Controls::TTouchProperty AProperty);
	virtual void __fastcall DoGetGestureOptions(Vcl::Controls::TInteractiveGestures &Gestures, Vcl::Controls::TInteractiveGestureOptions &Options);
	__property System::Uitypes::TCursor ZoomInCursor = {read=FZoomInCursor, write=SetZoomInCursor, default=102};
	__property System::Uitypes::TCursor ZoomOutCursor = {read=FZoomOutCursor, write=SetZoomOutCursor, default=103};
	__property System::Classes::TNotifyEvent OnZoomChanged = {read=FZoomChanged, write=FZoomChanged};
	__property TRVMarginsPen* MarginsPen = {read=FMarginsPen, write=SetMarginsPen};
	__property TRVPAPen* PrintableAreaPen = {read=FPrintableAreaPen, write=SetPrintableAreaPen};
	
public:
	__fastcall virtual TCustomRVPrintPreview(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomRVPrintPreview(void);
	void __fastcall SetZoom(int Percent);
	void __fastcall First(void);
	void __fastcall Last(void);
	void __fastcall Next(void);
	void __fastcall Prev(void);
	void __fastcall UpdateView(void);
	__property int PageNo = {read=FPageNo, write=SetPageNo, nodefault};
	__property int ZoomPercent = {read=FZoomPercent, write=SetZoomPercent, nodefault};
	__property Rvscroll::TRVZoomMode ZoomMode = {read=FZoomMode, write=SetZoomMode, nodefault};
	__property TRVClickMode ClickMode = {read=FClickMode, write=FClickMode, default=1};
	__property System::Uitypes::TColor PageBorderColor = {read=FPageBorderColor, write=FPageBorderColor, default=-16777203};
	__property int PageBorderWidth = {read=FPageBorderWidth, write=FPageBorderWidth, default=2};
	__property System::Uitypes::TColor ShadowColor = {read=FShadowColor, write=FShadowColor, default=-16777195};
	__property int ShadowWidth = {read=FShadowWidth, write=FShadowWidth, default=4};
	__property int BackgroundMargin = {read=FBackgroundMargin, write=FBackgroundMargin, default=20};
	
__published:
	__property Color = {default=-16777200};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRVPrintPreview(HWND ParentWindow) : Rvscroll::TRVScroller(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Int8 crRVZoomIn = System::Int8(0x66);
static const System::Int8 crRVZoomOut = System::Int8(0x67);
}	/* namespace Crvpp */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRVPP)
using namespace Crvpp;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrvppHPP
