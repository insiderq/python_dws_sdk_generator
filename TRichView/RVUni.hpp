﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVUni.pas' rev: 27.00 (Windows)

#ifndef RvuniHPP
#define RvuniHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvuni
{
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<int, 100001> TRVIntegerArray;

typedef TRVIntegerArray *PRVIntegerArray;

typedef System::StaticArray<unsigned, 100001> TRVUnsignedArray;

typedef TRVUnsignedArray *PRVUnsignedArray;

typedef System::StaticArray<System::Word, 100001> TRVWordArray;

typedef TRVWordArray *PRVWordArray;

enum DECLSPEC_DENUM TRVUnicodeTestResult : unsigned char { rvutNo, rvutYes, rvutProbably, rvutEmpty, rvutError };

//-- var, const, procedure ---------------------------------------------------
static const System::Word UNI_LF = System::Word(0xa);
static const System::Word UNI_CR = System::Word(0xd);
static const System::Word UNI_LineSeparator = System::Word(0x2028);
static const System::Word UNI_ParagraphSeparator = System::Word(0x2029);
static const System::Word UNI_Tab = System::Word(0x9);
static const System::Word UNI_VerticalTab = System::Word(0xb);
static const System::Word UNI_FormFeed = System::Word(0xc);
static const System::Word UNI_LSB_FIRST = System::Word(0xfeff);
static const System::Word UNI_MSB_FIRST = System::Word(0xfffe);
static const System::Word UNI_FF = System::Word(0xc);
static const System::Word UNI_HYPHEN = System::Word(0x2d);
static const System::Word UNI_Space = System::Word(0x20);
static const System::Word UNI_ZERO_WIDTH_SPACE = System::Word(0x200b);
static const System::Word UNI_ZERO_WIDTH_JOINER = System::Word(0x200d);
static const System::Word UNI_WORD_JOINER = System::Word(0x2060);
static const System::Word UNI_SOFT_HYPHEN = System::Word(0xad);
static const System::Word UNI_NOT_SIGN = System::Word(0xac);
static const System::Word UNI_NON_BREAKING_HYPHEN = System::Word(0x2011);
static const char UNI_LSB_FIRST1 = '\xff';
static const char UNI_LSB_FIRST2 = '\xfe';
extern DELPHI_PACKAGE bool RVNT;
extern DELPHI_PACKAGE bool RVVista;
extern DELPHI_PACKAGE unsigned __fastcall RVMAKELCID(System::Word plgid);
extern DELPHI_PACKAGE void * __fastcall RVU_FindLineBreak(PRVWordArray Str, int Length, bool FullString, bool IgnoreEndingSpaces);
extern DELPHI_PACKAGE int __fastcall RVU_FindFirstLineBreak(Rvtypes::TRVRawByteString Str, System::Word PrevChar, bool SpacesBefore, System::Word &LastChar);
extern DELPHI_PACKAGE bool __fastcall RVU_GetTextCaretPos(Vcl::Graphics::TCanvas* Canvas, const Rvtypes::TRVRawByteString s, PRVIntegerArray PCP, Rvitem::TRVItemOptions ItemOptions, int Width, int Delta, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE bool __fastcall RVU_GetTextGlyphDX(Vcl::Graphics::TCanvas* Canvas, const Rvtypes::TRVRawByteString s, PRVIntegerArray PDx, PRVWordArray PGlyphs, Rvitem::TRVItemOptions ItemOptions, int Width, int &nGlyphs, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE bool __fastcall RVU_GetTextRangeCoords(Vcl::Graphics::TCanvas* Canvas, const Rvtypes::TRVRawByteString s, int RangeStartOffs, int RangeLength, Rvitem::TRVItemOptions ItemOptions, int Width, int Delta, int &X1, int &X2, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_Copy(const Rvtypes::TRVRawByteString s, int Index, int Count, Rvitem::TRVItemOptions ItemOptions);
extern DELPHI_PACKAGE void __fastcall RVU_GetTextExtentPoint32W(Vcl::Graphics::TCanvas* Canvas, void * str, int Len, System::Types::TSize &sz, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE void __fastcall RVU_GetTextExtentExPoint(Vcl::Graphics::TCanvas* Canvas, const Rvtypes::TRVRawByteString s, int MaxExtent, int &Fit, PRVIntegerArray PDx, Rvitem::TRVItemOptions ItemOptions, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE void __fastcall RVU_GetTextExtentExPointPC(Vcl::Graphics::TCanvas* Canvas, char * pc, int Length, int MaxExtent, int &Fit, PRVIntegerArray PDx, Rvitem::TRVItemOptions ItemOptions, System::Types::TSize &sz, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE int __fastcall RVU_Length(const Rvtypes::TRVRawByteString s, Rvitem::TRVItemOptions ItemOptions);
extern DELPHI_PACKAGE int __fastcall RVU_TextWidth(const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemOptions ItemOptions, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE bool __fastcall RVU_IsSpace(const Rvtypes::TRVRawByteString s, int Index, Rvitem::TRVItemOptions ItemOptions);
extern DELPHI_PACKAGE void __fastcall RVU_Delete(Rvtypes::TRVRawByteString &s, int Index, int Count, Rvitem::TRVItemOptions ItemOptions);
extern DELPHI_PACKAGE void __fastcall RVU_Insert(const Rvtypes::TRVRawByteString Source, Rvtypes::TRVRawByteString &s, int Index, Rvitem::TRVItemOptions ItemOptions);
extern DELPHI_PACKAGE int __fastcall RVU_OffsInPChar(int Offs, Rvitem::TRVItemOptions ItemOptions);
extern DELPHI_PACKAGE bool __fastcall RVU_DrawSelectedTextEx(int Left, int Top, int Width, const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, Vcl::Graphics::TCanvas* RefCanvas, int Index1, int Index2, Rvitem::TRVItemOptions ItemOptions, Rvscroll::TRVBiDiMode BiDiMode, bool UseRefCanvasRes, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE System::Uitypes::TFontCharset __fastcall RVU_CodePage2Charset(unsigned CodePage);
extern DELPHI_PACKAGE unsigned __fastcall RVU_Charset2CodePage(System::Uitypes::TFontCharset Charset);
extern DELPHI_PACKAGE unsigned __fastcall RVU_Charset2Language(System::Uitypes::TFontCharset Charset);
extern DELPHI_PACKAGE Rvtypes::TRVUnicodeString __fastcall RVU_RawUnicodeToWideString(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_GetRawUnicode(const Rvtypes::TRVUnicodeString s);
extern DELPHI_PACKAGE void __fastcall RVU_SwapWordBytes(PWORD arr, int Count);
extern DELPHI_PACKAGE void __fastcall RVU_ProcessByteOrderMark(PWORD &arr, int Count);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_AnsiToUnicode(unsigned CodePage, const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_SymbolCharsetToUnicode(const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_AnsiToUTF8(unsigned CodePage, const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVU_UnicodeToAnsi(unsigned CodePage, const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE bool __fastcall RVU_CanBeConvertedToAnsi(unsigned CodePage, const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE TRVUnicodeTestResult __fastcall RV_TestStreamUnicode(System::Classes::TStream* Stream);
extern DELPHI_PACKAGE TRVUnicodeTestResult __fastcall RV_TestFileUnicode(const System::UnicodeString FileName);
extern DELPHI_PACKAGE TRVUnicodeTestResult __fastcall RV_TestStringUnicode(const Rvtypes::TRVRawByteString s);
extern DELPHI_PACKAGE bool __fastcall RV_TestStreamUTF8(System::Classes::TStream* Stream);
extern DELPHI_PACKAGE bool __fastcall RV_TestFileUTF8(const System::UnicodeString FileName);
extern DELPHI_PACKAGE unsigned __fastcall RVU_GetKeyboardCodePage(void);
extern DELPHI_PACKAGE unsigned __fastcall RVU_GetKeyboardLanguage(void);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_KeyToUnicode(const Rvtypes::TRVAnsiString Key);
extern DELPHI_PACKAGE void * __fastcall RVU_StrScanW(void * Str, System::Word Ch, int Length);
extern DELPHI_PACKAGE unsigned __fastcall RVU_StrLenW(void * Str);
extern DELPHI_PACKAGE void __fastcall RVU_WriteHTMLEncodedUnicode(System::Classes::TStream* Stream, const Rvtypes::TRVRawByteString s, bool NoEmptyLines, bool SpecialCode, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVAnsiString __fastcall RVU_GetHTMLEncodedUnicode(const Rvtypes::TRVRawByteString s, bool SpecialCode, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_GetHTMLUTF8EncodedUnicode(const Rvtypes::TRVRawByteString s, bool SpecialCode, bool NextSpace);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_ReturnProcessedStringEx(const Rvtypes::TRVRawByteString s, Rvstyle::TFontInfo* TextStyle, bool LastOnLine, bool ShowSpecialChars, bool ForDisplay, int &SelOffs1, int &SelOffs2);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RV_ReturnProcessedString(const Rvtypes::TRVRawByteString s, Rvstyle::TFontInfo* TextStyle, bool LastOnLine, bool ShowSpecialChars, bool ForDisplay);
extern DELPHI_PACKAGE void * __fastcall StrPosW(void * Str, void * SubStr);
extern DELPHI_PACKAGE void * __fastcall StrPosLW(void * Str, void * SubStr, int StrLen, int SubStrLen);
extern DELPHI_PACKAGE System::UnicodeString __fastcall RVU_RawByteStringToString(const Rvtypes::TRVRawByteString s, bool RawUnicode, unsigned CodePage);
extern DELPHI_PACKAGE Rvtypes::TRVRawByteString __fastcall RVU_StringToRawByteString(const System::UnicodeString s, bool RawUnicode, unsigned CodePage);
}	/* namespace Rvuni */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVUNI)
using namespace Rvuni;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvuniHPP
