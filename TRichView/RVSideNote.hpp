﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVSidenote.pas' rev: 27.00 (Windows)

#ifndef RvsidenoteHPP
#define RvsidenoteHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVFloatingBox.hpp>	// Pascal unit
#include <RVFloatingPos.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvsidenote
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVSidenoteItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSidenoteItemInfo : public Rvnote::TCustomRVNoteItemInfo
{
	typedef Rvnote::TCustomRVNoteItemInfo inherited;
	
private:
	Rvfloatingbox::TRVBoxProperties* FBoxProperties;
	Rvfloatingpos::TRVBoxPosition* FBoxPosition;
	void __fastcall Init(Crvdata::TCustomRVData* RVData);
	void __fastcall SetBoxProperties(Rvfloatingbox::TRVBoxProperties* const Value);
	void __fastcall SetBoxPosition(Rvfloatingpos::TRVBoxPosition* const Value);
	
protected:
	int __fastcall RTF_GetBelowText(void);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	virtual Rvstyle::TRVSeqType __fastcall GetNumberType(void);
	void __fastcall SaveOOXMLShape(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	void __fastcall SaveOOXMLDrawing(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	
public:
	__fastcall virtual TRVSidenoteItemInfo(System::Classes::TPersistent* RVData);
	__fastcall virtual TRVSidenoteItemInfo(System::Classes::TPersistent* RVData, int ATextStyleNo, int AStartFrom, bool AReset);
	__fastcall virtual ~TRVSidenoteItemInfo(void);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	DYNAMIC void __fastcall Assign(Rvitem::TCustomRVItemInfo* Source);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetHTMLAnchorName(void);
	DYNAMIC bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC Rvitem::TRVRTFDocType __fastcall GetSubDocRTFDocType(void);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLBeforeRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLAfterRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC bool __fastcall IsFixedHeight(void);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	DYNAMIC Rvitem::TRVReformatType __fastcall GetExtraIntPropertyReformat(int Prop);
	DYNAMIC System::Uitypes::TColor __fastcall GetSubDocColor(void);
	__property Rvfloatingbox::TRVBoxProperties* BoxProperties = {read=FBoxProperties, write=SetBoxProperties};
	__property Rvfloatingpos::TRVBoxPosition* BoxPosition = {read=FBoxPosition, write=SetBoxPosition};
};

#pragma pack(pop)

class DELPHICLASS TRVTextBoxItemInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTextBoxItemInfo : public TRVSidenoteItemInfo
{
	typedef TRVSidenoteItemInfo inherited;
	
private:
	HIDESBASE void __fastcall Init(Crvdata::TCustomRVData* RVData);
	
protected:
	virtual int __fastcall GetAssociatedTextStyleNo(void);
	virtual void __fastcall SetAssociatedTextStyleNo(int Value);
	
public:
	__fastcall virtual TRVTextBoxItemInfo(System::Classes::TPersistent* RVData);
	__fastcall virtual TRVTextBoxItemInfo(System::Classes::TPersistent* RVData, int ATextStyleNo, int AStartFrom, bool AReset);
	virtual System::UnicodeString __fastcall GetNoteText(void);
	virtual bool __fastcall GetBoolValue(Rvitem::TRVItemBoolProperty Prop);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	virtual void __fastcall Paint(int x, int y, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Dlines::TRVDrawLineInfo* dli, System::Classes::TPersistent* RVData);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall ApplyStyleConversion(System::Classes::TPersistent* RVData, int ItemNo, int UserData, Rvitem::TRVEStyleConversionType ConvType, bool Recursive);
	DYNAMIC Rvitem::TRVRTFDocType __fastcall GetSubDocRTFDocType(void);
public:
	/* TRVSidenoteItemInfo.Destroy */ inline __fastcall virtual ~TRVTextBoxItemInfo(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define RV_SIDENOTE_SEQNAME L"@Sidenote@"
#define RV_TEXTBOX_SEQNAME L"@TextBox@"
#define RV_SIDENOTE_HTML_ANCHOR L"_sidenote%d"
static const short rvsSidenote = short(-206);
static const short rvsTextBox = short(-207);
extern DELPHI_PACKAGE TRVSidenoteItemInfo* __fastcall RVGetFirstSidenote(Richview::TCustomRichView* RichView, bool AllowHidden);
extern DELPHI_PACKAGE TRVSidenoteItemInfo* __fastcall RVGetNextSidenote(Richview::TCustomRichView* RichView, TRVSidenoteItemInfo* Sidenote, bool AllowHidden);
extern DELPHI_PACKAGE TRVSidenoteItemInfo* __fastcall RVGetFirstSidenoteInRootRVData(Crvdata::TCustomRVData* RVData, bool AllowHidden);
extern DELPHI_PACKAGE TRVSidenoteItemInfo* __fastcall RVGetNextSidenoteInRootRVData(Crvdata::TCustomRVData* RVData, TRVSidenoteItemInfo* Sidenote, bool AllowHidden);
}	/* namespace Rvsidenote */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSIDENOTE)
using namespace Rvsidenote;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvsidenoteHPP
