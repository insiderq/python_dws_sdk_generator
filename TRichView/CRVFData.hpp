﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRVFData.pas' rev: 27.00 (Windows)

#ifndef CrvfdataHPP
#define CrvfdataHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <RVDragDrop.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVSelectionHandles.hpp>	// Pascal unit
#include <RVFontCache.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Crvfdata
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVDrawItemPosition : unsigned char { rvdpStrict, rvdpBetterAbove, rvdpBetterBelow };

class DELPHICLASS TRVSoftPageBreakInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSoftPageBreakInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int ItemNo;
	int Offset;
	int ExtraData;
public:
	/* TObject.Create */ inline __fastcall TRVSoftPageBreakInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVSoftPageBreakInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSoftPageBreakList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSoftPageBreakList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVSoftPageBreakList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVSoftPageBreakList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

typedef void __fastcall (__closure *TRVDataDrawHyperlinkEvent)(Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);

struct DECLSPEC_DRECORD TRVStoreDocTransform
{
public:
	bool Used;
	bool AdvancedMode;
	tagXFORM XForm;
};


enum DECLSPEC_DENUM TRVSelectingState : unsigned char { rvsesInWord, rvsesOutsideWord, rvsesFreeMode, rvsesParaMode };

class DELPHICLASS TRVSelectingInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSelectingInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int DrawItemSOffs;
	int DrawItemEOffs;
	int DrawItemSWordOffs1;
	int DrawItemSWordOffs2;
	int DrawItemEWordOffs1;
	int DrawItemEWordOffs2;
	int DrawItemSNo;
	int DrawItemENo;
	TRVSelectingState SWordState;
	TRVSelectingState EWordState;
	void __fastcall InitE(TRVSelectingState ASWordState);
	bool __fastcall IsAboveSWord(int ADrawItemNo, int ADrawItemOffs);
	bool __fastcall IsBelowSWord(int ADrawItemNo, int ADrawItemOffs);
	bool __fastcall IsInSWord(int ADrawItemNo, int ADrawItemOffs);
	bool __fastcall AreWordsEqual(void);
	bool __fastcall IsEWord(int ADrawItemNo, int ADrawItemWordOffs1, int ADrawItemWordOffs2);
	bool __fastcall IsEFreeStateNeeded(int ADrawItemNo, int ADrawItemOffs);
public:
	/* TObject.Create */ inline __fastcall TRVSelectingInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVSelectingInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVDragDropCaretInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDragDropCaretInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int X;
	int Y;
	int Height;
	int ItemNo;
	int ItemOffs;
	Crvdata::TCustomRVData* RVData;
	int RefCount;
public:
	/* TObject.Create */ inline __fastcall TRVDragDropCaretInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVDragDropCaretInfo(void) { }
	
};

#pragma pack(pop)

struct DECLSPEC_DRECORD TRVFormatParams
{
public:
	int x;
	int baseline;
	int prevdesc;
	int prevabove;
	int ParaNo;
	bool IsParaSectionStart;
	int LineWidth;
	int LineStartX;
	int LineStartY;
	int LastDrawItem;
	int VerticalOffs;
	int LeftIndent;
	int RightIndent;
	int FirstIndent;
	int LastTextStyle;
	int UnadjustedLeftIndent;
	int UnadjustedRightSide;
	int FirstParaItemNo;
	int LineHeightAbove;
	int LineHeightBelow;
	int TextLength;
	int MaxLength;
	bool NoCaching;
	bool Reformatting;
	bool DontFSL;
	bool NeedRecalcY;
	bool LineHasFloating;
	bool LineHasPlaceholders;
	int LastTabDrawItemNo;
	Rvstyle::TRVTabAlign LastTabAlign;
	int LastTabPosition;
	bool SpaceEaten;
	Rvfontcache::TRVFontInfoCache* FontInfoCache;
	Vcl::Graphics::TCanvas* FormatCanvas;
	tagTEXTMETRICW TextMetric;
	bool AllowWordWrap;
	Dlines::TRVFloatingDrawItems* FloatingDrawItems;
	System::Classes::TList* FloatingDrawItemsInThisParaSection;
	bool CanBeFloatingAtTheBeginning;
	bool MovedDownBecauseOfFloating;
	bool UseFormatCanvas;
};


enum DECLSPEC_DENUM TRVTransformBase : unsigned char { rvtrbLocal, rvtrbRoot, rvtrbAbsRoot, rvtrbRecursive, rvtrbInplaceBackground };

class DELPHICLASS TCustomRVFormattedData;
class PASCALIMPLEMENTATION TCustomRVFormattedData : public Crvdata::TCustomRVData
{
	typedef Crvdata::TCustomRVData inherited;
	
private:
	Rvitem::TRVCPInfo* LastRaisedCP;
	Vcl::Controls::TMouseMoveEvent FXORDrawing;
	Rvitem::TCustomRVItemInfo* FCaptureMouseItem;
	TRVSelectingInfo* FSelectingInfo;
	int __fastcall FindDrawItemAtPos(int X, int Y);
	void __fastcall CopyTextA_(void);
	void __fastcall CopyTextW_(void);
	void __fastcall CopyImage_(void);
	void __fastcall CopyRVF_(System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	void __fastcall CopyRTF_(System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	void __fastcall StartFormatting(void);
	void __fastcall EndFormatting(void);
	void __fastcall SearchHotItem(int X, int Y, int HOffs, int VOffs);
	void __fastcall AdjustSelection(void);
	
protected:
	bool XorImageDrawn;
	int MouseX;
	int MouseY;
	int nJmps;
	int LastItemFormatted;
	int LastJumpDIMovedAboveFirst;
	int LastJumpDIMovedAboveLast;
	int LastDIMovedAbove;
	int LastJumpDIDowned;
	Rvitem::TCustomRVItemInfo* FPartialSelectedItem;
	bool AlreadyFormatted;
	TRVSoftPageBreakList* FSoftPageBreaks;
	int FClickedDrawItemNo;
	virtual TRVSoftPageBreakList* __fastcall GetSoftPageBreaks(void);
	DYNAMIC Vcl::Controls::TControl* __fastcall GetInplaceEditor(void);
	DYNAMIC void __fastcall DestroyInplaceEditor(void);
	void __fastcall ClearLastJump(bool CallEvent);
	int __fastcall GetItemParaEx(int ItemNo);
	int __fastcall GetItemParaEx2(int ItemNo, int PrevParaNo);
	bool __fastcall IsParaStartEx(int ItemNo);
	bool __fastcall IsFromNewLineEx(int ItemNo);
	bool __fastcall IsLastParaSectionItemEx(int ItemNo);
	bool __fastcall IsLastParaItemEx(int ItemNo);
	int __fastcall GetFirstParaItemEx(int ItemNo);
	int __fastcall GetFirstParaSectionItemEx(int ItemNo);
	int __fastcall GetFirstNonHiddenItemNo(void);
	int __fastcall GetLastNonHiddenItemNo(void);
	void __fastcall ExpandToParaEx(int ItemNo1, int ItemNo2, int &FirstItemNo, int &LastItemNo);
	void __fastcall ExpandToParaSectionEx(int ItemNo1, int ItemNo2, int &FirstItemNo, int &LastItemNo);
	int __fastcall GetPrevNonHiddenItemNo(int ItemNo);
	int __fastcall GetNextNonHiddenItemNo(int ItemNo);
	int __fastcall GetDrawItemStyle(int DrawItemNo);
	bool __fastcall IsSpaceBetweenDrawItems(int DrawItemNo);
	virtual void __fastcall PostPaintTo(Vcl::Graphics::TCanvas* Canvas, int HOffs, int VOffs, int FirstDrawItemNo, int LastDrawItemNo, bool PrintMode, bool StrictTop, bool StrictBottom);
	int __fastcall FindFloatingAt(int DItemNo, int X, int Y);
	bool __fastcall AdjustSelectionByMode(int X, int Y);
	bool __fastcall AdjustLineSelection(int X, int Y);
	void __fastcall ExpandSelectionToLines(bool OneLine);
	DYNAMIC void __fastcall DeselectPartiallySelectedItem(Rvitem::TCustomRVItemInfo* NewPartiallySelected);
	virtual void __fastcall SetPartialSelectedItem(Rvitem::TCustomRVItemInfo* Item);
	bool __fastcall IsSelectionTopDown(void);
	bool __fastcall DItem_InsideSelection(int DItemNo, int DItemOffs, bool IncludeBorders);
	System::Types::TRect __fastcall GetClientSelectionRect(void);
	void __fastcall DrawDragDropCaret(Vcl::Graphics::TCanvas* Canvas, bool OnlyForSelf);
	DYNAMIC bool __fastcall CanStartDragging(void);
	DYNAMIC bool __fastcall InitDragging(Rvdragdrop::TRVDropSource* &DropSource, int &OKEffect);
	DYNAMIC void __fastcall DoneDragging(bool FDeleteSelection);
	DYNAMIC void __fastcall SetClickCoords(int X, int Y);
	DYNAMIC bool __fastcall CanStartDragBecauseMouseMoved(int X, int Y);
	void __fastcall FinishScreenLine(const Rvstyle::TRVScreenAndDevice &sad, bool IsParaSectionFinished, int &ExtraSpace, TRVFormatParams &Params);
	void __fastcall UpdateLastTab(TRVFormatParams &Params);
	void __fastcall FormatAddFloating(TRVFormatParams &Params, const Rvstyle::TRVScreenAndDevice &sad, int DrawItemNo, int &Y);
	void __fastcall FormatLine(const Rvtypes::TRVRawByteString Text, const Rvtypes::TRVRawByteString OrigText, int Len, int ItemNo, Vcl::Graphics::TCanvas* Canvas, Rvstyle::TRVScreenAndDevice &sad, TRVFormatParams &Params);
	virtual void __fastcall GetSADForFormatting(Vcl::Graphics::TCanvas* Canvas, Rvstyle::TRVScreenAndDevice &sad);
	DYNAMIC void __fastcall Formatted(int FirstItemNo, int LastItemNo, bool Partial);
	DYNAMIC void __fastcall DoAfterFormat(void);
	void __fastcall GetWrapPlaceAfterItem(int &ItemNo, int &Offs);
	void __fastcall ExpandLastJumpDIMovedAbove(void);
	bool __fastcall IsHidden(int ItemNo);
	HIDESBASE bool __fastcall IsHiddenItem(Rvitem::TCustomRVItemInfo* Item);
	bool __fastcall IsParaSectionHidden(int FirstItemNo);
	bool __fastcall CanShowSpecialCharacters(void);
	void __fastcall ConcateItems(int FirstItemNo);
	bool __fastcall InsideWord(int DrawItemNo, int DrawItemOffs);
	void __fastcall GetWordBounds(int DrawItemNo, int DrawItemOffs, int &DrawItemWordOffs1, int &DrawItemWordOffs2);
	void __fastcall GetScreenLineBounds(int DrawItemNo, int &First, int &Last);
	DYNAMIC void __fastcall AfterDeleteStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	virtual void __fastcall InternalFreeItem(Rvitem::TCustomRVItemInfo* item, bool Clearing);
	virtual bool __fastcall GetFirstItemMarker(int &ListNo, int &Level);
	void __fastcall GetIndents(int ItemNo, bool IsParaSectionStart, int &FirstParaItemNo, int &LeftIndent, int &RightIndent, int &FirstIndent);
	Rvstyle::TRVListLevel* __fastcall GetListLevelForItem(int ItemNo);
	int __fastcall GetMaxIndent(int ItemNo, int &FirstParaItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, bool EvenNegativeFirstIndent);
	DYNAMIC void __fastcall RVFGetLimits(Crvdata::TRVFSaveScope SaveScope, int &StartItem, int &EndItem, int &StartOffs, int &EndOffs, Rvitem::TRVMultiDrawItemPart* &StartPart, Rvitem::TRVMultiDrawItemPart* &EndPart, Rvitem::TCustomRVItemInfo* &SelectedItem);
	DYNAMIC void __fastcall LiveSpellingCheckCurrentItem(void);
	void __fastcall AdjustItemNoAfterDeletion(int &ItemNo, int FirstDelItemNo, int LastDelItemNo);
	
public:
	Rvitem::TCustomRVItemInfo* FActiveItem;
	int DocumentWidth;
	int DocumentHeight;
	int TextWidth;
	int FocusedItemNo;
	Dlines::TRVDrawLines* DrawItems;
	int FSelStartNo;
	int FSelEndNo;
	int FSelStartOffs;
	int FSelEndOffs;
	DYNAMIC void __fastcall CreateFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas);
	DYNAMIC void __fastcall DestroyFontInfoCache(Rvfontcache::TRVFontInfoCache* &Cache);
	DYNAMIC Rvfontcache::TRVFontInfoCache* __fastcall GetFontInfoCache(Vcl::Graphics::TCanvas* ACanvas, Vcl::Graphics::TCanvas* AFormatCanvas, TCustomRVFormattedData* RVData);
	__fastcall TCustomRVFormattedData(void);
	__fastcall virtual ~TCustomRVFormattedData(void);
	virtual void __fastcall ClearTemporal(void);
	DYNAMIC void __fastcall Clear(void);
	void __fastcall AssignSoftPageBreaks(System::Classes::TComponent* RVPrint);
	void __fastcall AssignSoftPageBreak(TCustomRVFormattedData* CustomMultiPagePtblRVData, int PageNo);
	bool __fastcall ClearSoftPageBreaks(void);
	int __fastcall GetNextFocusedItem(int ItemNo, bool GoForward, TCustomRVFormattedData* &TopLevelRVData, int &TopLevelItemNo);
	void __fastcall ClearFocus(void);
	DYNAMIC void __fastcall AdjustFocus(int NewFocusedItemNo, System::Classes::TPersistent* TopLevelRVData, int TopLevelItemNo);
	DYNAMIC bool __fastcall AllowAnimation(void);
	DYNAMIC void __fastcall InsertAnimator(System::TObject* &Animator);
	DYNAMIC void __fastcall ResetAniBackground(void);
	bool __fastcall IsDrawItemParaStart(int DrawItemNo);
	bool __fastcall IsDrawItemItemStart(int DrawItemNo);
	bool __fastcall IsDrawItemParaSectionStart(int DrawItemNo);
	bool __fastcall IsDrawItemParaEnd(int DrawItemNo);
	bool __fastcall IsDrawItemFromNewLine(int DrawItemNo);
	bool __fastcall IsDrawItemLastOnWrappedLine(int DrawItemNo);
	void __fastcall RotateRectFromDocToScreen(System::Types::TRect &R, TRVTransformBase Base);
	void __fastcall RotateCoordsFromScreenToDoc(int &X, int &Y, bool AlwaysLocal, bool ParentInplace);
	void __fastcall RotateCoordsFromScreenToDocAbs(int &X, int &Y);
	void __fastcall RotateCoordsFromDocToScreen(int &X, int &Y, TRVTransformBase Base);
	virtual void __fastcall GetOrigin(int &ALeft, int &ATop);
	DYNAMIC void __fastcall GetOriginEx(int &ALeft, int &ATop);
	virtual int __fastcall GetAreaWidthRot(void);
	virtual int __fastcall GetAreaWidth(void) = 0 ;
	virtual int __fastcall GetAreaHeight(void) = 0 ;
	virtual int __fastcall GetLeftMargin(void) = 0 ;
	virtual int __fastcall GetRightMargin(void) = 0 ;
	virtual int __fastcall GetTopMargin(void) = 0 ;
	virtual int __fastcall GetBottomMargin(void) = 0 ;
	virtual int __fastcall GetMinTextWidth(void) = 0 ;
	virtual int __fastcall GetMaxTextWidth(void) = 0 ;
	virtual int __fastcall GetWidth(void) = 0 ;
	virtual int __fastcall GetHeight(void) = 0 ;
	DYNAMIC int __fastcall GetFullDocumentHeight(void);
	virtual int __fastcall GetWidthRot(void);
	virtual int __fastcall GetHeightRot(void);
	DYNAMIC void __fastcall GetPrerotatedSize(int &W, int &H);
	virtual void __fastcall SetDocumentAreaSize(int Width, int Height, bool UpdateH) = 0 ;
	int __fastcall CalculateEmptyParaSectionWidth(int ItemNo, int &FirstParaItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, bool EventNegativeFirstIndent);
	int __fastcall CalculateParaSectionMinWidth(int StartItemNo, int &FirstParaItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	int __fastcall CalculateMinItemsWidthNoWrap(int StartItemNo, int EndItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, bool IgnoreFloating);
	int __fastcall CalculateParaSectionMinWidthDef(int StartItemNo);
	int __fastcall CalculateParaSectionsMinWidth(int StartItemNo, int EndItemNo, int &FirstParaItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	int __fastcall CalculateParaSectionsMinWidthDef(int StartItemNo, int EndItemNo);
	int __fastcall CalculateMinItemWidthWrap(int ItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, bool IgnoreFloating);
	int __fastcall CalculateMinItemsWidthPlusWrap(int StartItemNo, int EndItemNo, int &FirstParaItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	int __fastcall CalculateMinItemWidthPlusEx(int ItemNo);
	int __fastcall CalculateMinItemsWidthPlusEx(int StartItemNo, int EndItemNo);
	int __fastcall CalculateMinDocWidthPlus(int FirstItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	int __fastcall CalculateMinWidthAfterInsert(Rvitem::TCustomRVItemInfo* item, int InsertItemNo);
	int __fastcall CalculateMinLineWidth(int FirstItemNo, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	DYNAMIC void __fastcall ResetSubCoords(void);
	int __fastcall GetDrawItemNo(int BoundLine, int Option);
	bool __fastcall GetItemCoords(int ItemNo, int &Left, int &Top);
	bool __fastcall GetItemClientCoords(int ItemNo, int &Left, int &Top);
	bool __fastcall GetItemCoordsAbsRoot(int ItemNo, int Part, bool AllowFloating, System::Types::TRect &R);
	DYNAMIC System::Types::TPoint __fastcall ClientToScreen(const System::Types::TPoint &p);
	DYNAMIC System::Types::TPoint __fastcall ScreenToClient(const System::Types::TPoint &p);
	virtual Rvfuncs::TRVVerticalAlign __fastcall GetVerticalAlign(void);
	void __fastcall DoPrepareTransform(Vcl::Graphics::TCanvas* Canvas, TRVStoreDocTransform &OldState, int ExtraX, int ExtraY, int HOffs, int VOffs, int Width, int Height, int DocHeight, Rvfuncs::TRVVerticalAlign VAlign, TRVTransformBase Base);
	void __fastcall PrepareTransform(Vcl::Graphics::TCanvas* Canvas, TRVStoreDocTransform &OldState, int ExtraX, int ExtraY, TRVTransformBase Base, int DocHeight);
	void __fastcall RestoreTransform(Vcl::Graphics::TCanvas* Canvas, const TRVStoreDocTransform &OldState);
	void __fastcall GetCoordsOf(int ItemNo, int Offs, int &X, int &Top, int &Bottom);
	void __fastcall GetCoordsOfDrawItem(int DrawItemNo, int DrawItemOffs, int &X, int &Top, int &Bottom);
	DYNAMIC void __fastcall AdjustSpecialControlsCoords(TCustomRVFormattedData* RVData);
	void __fastcall AdjustChildrenCoords(void);
	void __fastcall GetWordAtR(int X, int Y, TCustomRVFormattedData* &RVData, int &ItemNo, Rvtypes::TRVRawByteString &Word);
	bool __fastcall FindWordAtR(Rvtypes::TRVRawByteString &Word, int X, int Y, int &StyleNo, int &ItemNo, int &Offs, TCustomRVFormattedData* &RVData);
	bool __fastcall FindWordAtR_(Rvtypes::TRVRawByteString &Word, int X, int Y, int &StyleNo, int &ItemNo, int &Offs, TCustomRVFormattedData* &RVData, bool CoordRelativeToControl);
	void __fastcall GetItemAt(int X, int Y, int &ItemNo, int &OffsetInItem);
	void __fastcall GetItemAtEx(int X, int Y, TCustomRVFormattedData* &RVData, int &ItemNo, int &OffsetInItem, bool FStrict, bool ClosestSubRVData, bool &InSubRVDataOwnerItem, bool IgnoreFloating, bool CoordRelativeToControl);
	DYNAMIC void __fastcall ShowRectangle(int Left, int Top, int Width, int Height);
	virtual void __fastcall AdjustVScrollUnits(void) = 0 ;
	virtual int __fastcall GetHOffs(void);
	virtual int __fastcall GetVOffs(void);
	virtual int __fastcall GetRVDataExtraVOffs(void);
	virtual void __fastcall ScrollTo(int Y, bool Redraw) = 0 ;
	virtual void __fastcall HScrollTo(int X) = 0 ;
	virtual int __fastcall GetVSmallStep(void) = 0 ;
	virtual void __fastcall AfterVScroll(void);
	void __fastcall OnTimerScroll(void);
	DYNAMIC void __fastcall DoCopy(void);
	DYNAMIC bool __fastcall IsAssignedCopy(void);
	DYNAMIC bool __fastcall IsAssignedRVMouseUp(void) = 0 ;
	DYNAMIC bool __fastcall IsAssignedRVMouseDown(void) = 0 ;
	DYNAMIC bool __fastcall IsAssignedRVRightClick(void);
	DYNAMIC bool __fastcall IsAssignedJump(void) = 0 ;
	DYNAMIC bool __fastcall IsAssignedRVDblClick(void);
	DYNAMIC bool __fastcall IsAssignedCheckpointVisible(void) = 0 ;
	DYNAMIC void __fastcall DoRVMouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y) = 0 ;
	DYNAMIC void __fastcall DoRVMouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y) = 0 ;
	DYNAMIC void __fastcall DoRVRightClick(const Rvtypes::TRVRawByteString ClickedWord, int StyleNo, int X, int Y);
	DYNAMIC void __fastcall DoJump(int id) = 0 ;
	DYNAMIC void __fastcall DoGetItemCursor(Crvdata::TCustomRVData* RVData, int ItemNo, System::Uitypes::TCursor &Cursor);
	DYNAMIC void __fastcall DoRVMouseMove(int id) = 0 ;
	DYNAMIC void __fastcall DoRVDblClick(const Rvtypes::TRVRawByteString ClickedWord, int StyleNo);
	DYNAMIC void __fastcall DoSelect(void);
	DYNAMIC void __fastcall DoCheckpointVisible(Rvstyle::TCheckpointData CheckpointData) = 0 ;
	DYNAMIC void __fastcall DoCurrentTextStyleConversion(int &StyleNo, int ParaStyleNo, int ItemNo, int UserData, bool ToWholeParagraphs);
	DYNAMIC void __fastcall DoDrawHyperlink(Crvdata::TCustomRVData* RVData, int ItemNo, const System::Types::TRect &R);
	DYNAMIC System::Uitypes::TCursor __fastcall GetNormalCursor(void) = 0 ;
	DYNAMIC Rvscroll::TCPEventKind __fastcall GetCPEventKind(void);
	virtual Rvback::TRVBackground* __fastcall GetBackground(void) = 0 ;
	DYNAMIC void __fastcall FreeBackgroundThumbnail(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetCanvas(void);
	DYNAMIC void __fastcall SetCursor(System::Uitypes::TCursor Cursor);
	DYNAMIC Vcl::Controls::TWinControl* __fastcall GetEditor(void);
	DYNAMIC bool __fastcall GetForceFieldHighlight(void);
	void __fastcall MouseLeave(void);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall DblClick(void);
	void __fastcall DblClickAt(const System::Types::TPoint &p);
	void __fastcall TripleClick(void);
	DYNAMIC void __fastcall AdjustMouseUpSelection(void);
	bool __fastcall IsAboveLineSelectMargin(int X, int Y);
	void __fastcall SetMouseCapture(Rvitem::TCustomRVItemInfo* Item, int &Left, int &Top);
	void __fastcall ReleaseMouseCapture(Rvitem::TCustomRVItemInfo* Item);
	DYNAMIC TRVDragDropCaretInfo* __fastcall GetDragDropCaretInfo(void);
	void __fastcall SetDragDropCaretTo(int X, int Y);
	void __fastcall RemoveDragDropCaret(void);
	void __fastcall SetDragDropCaret(int ItemNo, int Offs);
	void __fastcall DoDrag(void);
	DYNAMIC bool __fastcall IsDragging(void);
	DYNAMIC Rvselectionhandles::TRVSelectionHandles* __fastcall GetSelectionHandles(void);
	DYNAMIC void __fastcall CreateSelectionHandlesAfterTap(void);
	bool __fastcall CanInsertHere(int ItemNo, int Offs);
	bool __fastcall IsProtected(int ItemNo, Rvstyle::TRVProtectOption Option);
	bool __fastcall IsParaProtected(int ParaNo, Rvstyle::TRVParaOption Option);
	DYNAMIC bool __fastcall IsSticking(int FirstItemNo, bool NoSound);
	Vcl::Graphics::TCanvas* __fastcall GetFormatCanvas(void);
	virtual Vcl::Graphics::TCanvas* __fastcall GetFormatCanvasEx(Vcl::Graphics::TCanvas* DefCanvas);
	virtual void __fastcall DrawBackground(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &r, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowThumbnails);
	virtual void __fastcall PaintTo(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &AClipRect, bool StrictTop, bool StrictBottom, bool InplaceOnMainCanvas, bool PrintMode, bool UseWordPainters, int ExtraXOffs, int ExtraYOffs, int MinItemNo, int MaxItemNo, Rvitem::TRVPaintItemPart* MinItemPart, Rvitem::TRVPaintItemPart* MaxItemPart);
	virtual void __fastcall GetItemBackground(int ItemNo, const System::Types::TRect &r, bool MakeImageRect, System::Uitypes::TColor &Color, Vcl::Graphics::TBitmap* &bmp, bool &UseBitmap, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowPictures, bool AllowThumbnails);
	void __fastcall Refresh(void);
	void __fastcall UpdateView(void);
	void __fastcall Invalidate(void);
	void __fastcall InvalidateRect(const System::Types::TRect &r, bool FromLiveSpelling);
	void __fastcall UpdateBackgroundPaletteInfo(Rvback::TRVBackground* Background);
	bool __fastcall UpdatingBackgroundPalette(void);
	virtual void __fastcall XorDrawing(void);
	void __fastcall XorDrawingEx(int X, int Y);
	bool __fastcall ClearXorDrawing(void);
	void __fastcall AssignXorDrawing(Vcl::Controls::TMouseMoveEvent P);
	bool __fastcall UsingThisXorDrawing(Vcl::Controls::TMouseMoveEvent P);
	void __fastcall UnAssignXorDrawing(Vcl::Controls::TMouseMoveEvent P);
	DYNAMIC bool __fastcall CancelResize(void);
	Dlines::TRVDrawLineInfoClass __fastcall GetStandardDrawLineInfoClass(Rvitem::TCustomRVItemInfo* item, Rvstyle::TParaInfo* para);
	void __fastcall AdjustDrawItemsForFormatParas(int &StartDrawItemNo, int &EndDrawItemNo, int ItemsInserted);
	void __fastcall FormatParas(int StartDrawItemNo, int EndDrawItemNo, int ItemsInserted);
	virtual void __fastcall StartPartialFormatting(void);
	void __fastcall FormatParasExact(int StartDrawItemNo, int EndDrawItemNo, int ItemsInserted, bool NoCaching);
	virtual bool __fastcall Format_(bool OnlyResized, bool ForceFormat, bool NoScroll, int depth, Vcl::Graphics::TCanvas* Canvas, bool OnlyTail, bool NoCaching, bool Reformatting, bool CallOnFormat, bool AbsRotation);
	void __fastcall Format(bool NoCaching, bool AbsRotation);
	void __fastcall ChangeDItemWidth(int DItemNo, int NewWidth);
	void __fastcall Hiding(void);
	void __fastcall SaveFormattingToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFormattingFromStream(System::Classes::TStream* Stream);
	void __fastcall FindDrawItemForSel(int X, int Y, int &No, int &Offs, bool FStrict, bool IgnoreFloating, bool XYAlreadyRotated, bool XYLocalRotation);
	void __fastcall DoSetSelectionBounds(int StartItemNo, int StartItemOffs, int EndItemNo, int EndItemOffs);
	void __fastcall SetSelectionBounds(int StartItemNo, int StartItemOffs, int EndItemNo, int EndItemOffs);
	bool __fastcall SearchTextR(bool Down, bool MatchCase, bool WholeWord, bool FromStart, bool Unicode, bool MultiItem, bool SmartStart, Rvtypes::TRVRawByteString s);
	void __fastcall SelectWordAt(int X, int Y);
	bool __fastcall SelectControl(Vcl::Controls::TControl* AControl);
	DYNAMIC void __fastcall Deselect(Rvitem::TCustomRVItemInfo* NewPartiallySelected, bool MakeEvent);
	void __fastcall SelectAll(void);
	void __fastcall SelectLine(int ItemNo, int Offs);
	void __fastcall RestoreSelBounds(int StartNo, int EndNo, int StartOffs, int EndOffs);
	DYNAMIC void __fastcall SrchSelectIt(int StartItemNo, int StartOffs, int EndItemNo, int EndOffs, bool Invert);
	DYNAMIC void __fastcall SrchStart(bool Down, bool FromStart, bool SmartStart, int &strt, int &offs);
	DYNAMIC void __fastcall AssignChosenRVData(TCustomRVFormattedData* RVData, Rvitem::TCustomRVItemInfo* Item);
	DYNAMIC void __fastcall SilentReplaceChosenRVData(TCustomRVFormattedData* RVData);
	DYNAMIC void __fastcall UnassignChosenRVData(Crvdata::TCustomRVData* RVData);
	void __fastcall ChooseMe(void);
	bool __fastcall SelectionExists(bool AllowReset, bool UsePartialSelected);
	void __fastcall GetSelectionBounds(int &StartItemNo, int &StartItemOffs, int &EndItemNo, int &EndItemOffs, bool Normalize);
	DYNAMIC void __fastcall GetSelBounds(int &StartNo, int &EndNo, int &StartOffs, int &EndOffs, bool Normalize);
	void __fastcall StoreSelBounds(int &StartNo, int &EndNo, int &StartOffs, int &EndOffs, bool Normalize);
	DYNAMIC void __fastcall GetSelectionBoundsEx(int &StartItemNo, int &StartItemOffs, int &EndItemNo, int &EndItemOffs, bool Normalize);
	DYNAMIC void __fastcall GetSelStart(int &DINo, int &DIOffs);
	Rvtypes::TRVRawByteString __fastcall GetSelTextR(bool Unicode);
	Vcl::Graphics::TGraphic* __fastcall GetSelectedImage(void);
	bool __fastcall GetSingleSelectedItem(TCustomRVFormattedData* &RVData, int &ItemNo);
	System::Types::TRect __fastcall GetSelectionRect(void);
	bool __fastcall GetSelectionBoundsCoords(int &X1, int &Top1, int &Bottom1, int &X2, int &Top2, int &Bottom2);
	bool __fastcall ExpandSelectionToParagraph(bool OnlyIfMultiple);
	DYNAMIC void __fastcall DoOnSelection(bool AllowScrolling);
	bool __fastcall Item_InsideSelection(int ItemNo, int ItemOffs, bool IncludeBorders);
	void __fastcall GetCheckpointXY(Rvstyle::TCheckpointData CheckpointData, int &X, int &Y);
	int __fastcall GetCheckpointYEx(Rvstyle::TCheckpointData CheckpointData);
	void __fastcall BuildJumps(int &StartJumpNo);
	void __fastcall ClearJumps(bool CallEvent);
	int __fastcall GetJumpPointY(int id);
	int __fastcall GetJumpPointItemNo(int id);
	void __fastcall GetJumpPointLocation(int id, TCustomRVFormattedData* &RVData, int &ItemNo);
	void __fastcall CopyRVF(System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	void __fastcall CopyRTF(System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	void __fastcall CopyTextA(void);
	void __fastcall CopyTextW(void);
	void __fastcall CopyText(void);
	void __fastcall CopyImage(void);
	void __fastcall Copy(System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	bool __fastcall CopyDef(System::Uitypes::TColor Color, Rvback::TRVBackground* Background);
	int __fastcall GetLineNo(int ItemNo, int ItemOffs);
	void __fastcall GetParaBoundsEx(int DINo1, int DINo2, int &ParaStart, int &ParaEnd, bool IncludeAdjacent);
	int __fastcall GetFirstVisible(int TopLine);
	DYNAMIC int __fastcall GetFirstItemVisible(void);
	DYNAMIC int __fastcall GetLastItemVisible(void);
	int __fastcall GetOffsBeforeDrawItem(int DrawItemNo);
	int __fastcall GetOffsAfterDrawItem(int DrawItemNo);
	void __fastcall DrawItem2Item(int DrawItemNo, int DrawItemOffs, int &ItemNo, int &ItemOffs);
	void __fastcall Item2DrawItem(int ItemNo, int ItemOffs, int &DrawItemNo, int &DrawItemOffs, TRVDrawItemPosition PosIfHidden = (TRVDrawItemPosition)(0x2));
	void __fastcall Item2FirstDrawItem(int ItemNo, int &DrawItemNo);
	void __fastcall Item2LastDrawItem(int ItemNo, int &DrawItemNo);
	int __fastcall FindDrawItemByItem(int ItemNo);
	void __fastcall InvalidateDrawItem(int DrawItemNo, int Spacing, bool FromLiveSpelling);
	void __fastcall InvalidateHighlightedJump(void);
	DYNAMIC void __fastcall DeleteItems(int FirstItemNo, int Count);
	DYNAMIC void __fastcall DeleteParas(int FirstItemNo, int LastItemNo);
	DYNAMIC bool __fastcall AllowChanging(void);
	void __fastcall Normalize(void);
	DYNAMIC void __fastcall SetControlHint(const System::UnicodeString Hint);
	int __fastcall GetItemPart(int ItemNo, int OffsetInItem, bool PreferPartAfter);
	void __fastcall GetMouseClientCoords(Vcl::Controls::TWinControl* Control, int &X, int &Y);
	DYNAMIC bool __fastcall IgnorePageBreaks(void);
	bool __fastcall IsItemParaProtected(int ItemNo);
	__property Rvitem::TCustomRVItemInfo* PartialSelectedItem = {read=FPartialSelectedItem, write=SetPartialSelectedItem};
	__property Rvitem::TCustomRVItemInfo* CaptureMouseItem = {read=FCaptureMouseItem};
	__property TRVSoftPageBreakList* SoftPageBreaks = {read=GetSoftPageBreaks};
	bool __fastcall IsMultiParagraphSelection(void);
	bool __fastcall IsWholeParagraphSelection(void);
};


//-- var, const, procedure ---------------------------------------------------
static const System::Int8 MINEXACTLINESPACING = System::Int8(0x4);
extern DELPHI_PACKAGE bool RichViewSafeFormatting;
extern DELPHI_PACKAGE bool RichViewShowGhostSpaces;
extern DELPHI_PACKAGE bool RichViewJustifyBeforeLineBreak;
extern DELPHI_PACKAGE bool SRVPrintSpecialCharacters;
}	/* namespace Crvfdata */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRVFDATA)
using namespace Crvfdata;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrvfdataHPP
