﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVXPTheme.pas' rev: 27.00 (Windows)

#ifndef RvxpthemeHPP
#define RvxpthemeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvxptheme
{
//-- type declarations -------------------------------------------------------
typedef unsigned HTheme;

enum DECLSPEC_DENUM TThemeSize : unsigned char { TS_MIN, TS_TRUE, TS_DRAW };

typedef bool __stdcall (*TRV_IsThemeActiveProc)(void);

typedef bool __stdcall (*TRV_IsAppThemedProc)(void);

typedef unsigned __stdcall (*TRV_OpenThemeDataProc)(HWND hwnd, System::WideChar * pszClassList);

typedef HRESULT __stdcall (*TRV_CloseThemeDataProc)(unsigned Theme);

typedef HRESULT __stdcall (*TRV_DrawThemeParentBackgroundProc)(HWND hwnd, HDC hdc, System::Types::PRect Rect);

typedef HRESULT __stdcall (*TRV_DrawThemeEdgeProc)(unsigned Theme, HDC hdc, int iPartId, int iStateId, const System::Types::TRect &pDestRect, unsigned uEdge, unsigned uFlags, System::Types::PRect pContentRect);

typedef HRESULT __stdcall (*TRV_DrawThemeBackgroundProc)(unsigned Theme, HDC hdc, int iPartId, int iStateId, const System::Types::TRect &Rect, System::Types::PRect pClipRect);

typedef HRESULT __stdcall (*TRV_DrawThemeTextProc)(unsigned Theme, HDC hdc, int iPartId, int iStateId, System::WideChar * pszText, int iCharCount, unsigned dwTextFlags, unsigned dwTextFlags2, System::Types::TRect &Rect);

typedef bool __stdcall (*TRV_IsThemeBackgroundPartiallyTransparentProc)(unsigned hTheme, int iPartId, int iStateId);

typedef HRESULT __stdcall (*TRV_GetThemeRectProc)(unsigned Theme, int iPartId, int iStateId, int iPropId, System::Types::TRect &pRect);

typedef HRESULT __stdcall (*TRV_GetThemePartSizeProc)(unsigned hTheme, HDC hdc, int iPartId, int iStateId, System::Types::PRect prc, TThemeSize eSize, System::Types::TSize &psz);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRV_IsThemeActiveProc RV_IsThemeActive;
extern DELPHI_PACKAGE TRV_IsAppThemedProc RV_IsAppThemed;
extern DELPHI_PACKAGE TRV_OpenThemeDataProc RV_OpenThemeData;
extern DELPHI_PACKAGE TRV_CloseThemeDataProc RV_CloseThemeData;
extern DELPHI_PACKAGE TRV_DrawThemeParentBackgroundProc RV_DrawThemeParentBackground;
extern DELPHI_PACKAGE TRV_DrawThemeEdgeProc RV_DrawThemeEdge;
extern DELPHI_PACKAGE TRV_DrawThemeBackgroundProc RV_DrawThemeBackground;
extern DELPHI_PACKAGE TRV_DrawThemeTextProc RV_DrawThemeText;
extern DELPHI_PACKAGE TRV_IsThemeBackgroundPartiallyTransparentProc RV_IsThemeBackgroundPartiallyTransparent;
extern DELPHI_PACKAGE TRV_GetThemeRectProc RV_GetThemeRect;
extern DELPHI_PACKAGE TRV_GetThemePartSizeProc RV_GetThemePartSize;
static const System::Int8 EP_EDITTEXT = System::Int8(0x1);
static const System::Int8 ETS_NORMAL = System::Int8(0x1);
static const System::Int8 ETS_HOT = System::Int8(0x2);
static const System::Int8 ETS_SELECTED = System::Int8(0x3);
static const System::Int8 ETS_DISABLED = System::Int8(0x4);
static const System::Int8 ETS_FOCUSED = System::Int8(0x5);
static const System::Int8 ETS_READONLY = System::Int8(0x6);
static const System::Int8 ETS_ASSIST = System::Int8(0x7);
static const System::Int8 BP_RADIOBUTTON = System::Int8(0x2);
static const System::Int8 RBS_UNCHECKEDNORMAL = System::Int8(0x1);
static const System::Int8 RBS_UNCHECKEDHOT = System::Int8(0x2);
static const System::Int8 RBS_UNCHECKEDPRESSED = System::Int8(0x3);
static const System::Int8 RBS_UNCHECKEDDISABLED = System::Int8(0x4);
static const System::Int8 RBS_CHECKEDNORMAL = System::Int8(0x5);
static const System::Int8 RBS_CHECKEDHOT = System::Int8(0x6);
static const System::Int8 RBS_CHECKEDPRESSED = System::Int8(0x7);
static const System::Int8 RBS_CHECKEDDISABLED = System::Int8(0x8);
static const System::Int8 BP_CHECKBOX = System::Int8(0x3);
static const System::Int8 CBS_UNCHECKEDNORMAL = System::Int8(0x1);
static const System::Int8 CBS_UNCHECKEDHOT = System::Int8(0x2);
static const System::Int8 CBS_UNCHECKEDPRESSED = System::Int8(0x3);
static const System::Int8 CBS_UNCHECKEDDISABLED = System::Int8(0x4);
static const System::Int8 CBS_CHECKEDNORMAL = System::Int8(0x5);
static const System::Int8 CBS_CHECKEDHOT = System::Int8(0x6);
static const System::Int8 CBS_CHECKEDPRESSED = System::Int8(0x7);
static const System::Int8 CBS_CHECKEDDISABLED = System::Int8(0x8);
static const System::Int8 CBS_MIXEDNORMAL = System::Int8(0x9);
static const System::Int8 CBS_MIXEDHOT = System::Int8(0xa);
static const System::Int8 CBS_MIXEDPRESSED = System::Int8(0xb);
static const System::Int8 CBS_MIXEDDISABLED = System::Int8(0xc);
static const System::Int8 BP_GROUPBOX = System::Int8(0x4);
static const System::Int8 GBS_NORMAL = System::Int8(0x1);
static const System::Int8 GBS_DISABLED = System::Int8(0x2);
static const System::Int8 CP_DROPDOWNBUTTON = System::Int8(0x1);
static const System::Int8 CBXS_NORMAL = System::Int8(0x1);
static const System::Int8 CBXS_HOT = System::Int8(0x2);
static const System::Int8 CBXS_PRESSED = System::Int8(0x3);
static const System::Int8 CBXS_DISABLED = System::Int8(0x4);
static const System::Int8 SBP_ARROWBTN = System::Int8(0x1);
static const System::Int8 SBP_THUMBBTNHORZ = System::Int8(0x2);
static const System::Int8 SBP_THUMBBTNVERT = System::Int8(0x3);
static const System::Int8 SBP_LOWERTRACKHORZ = System::Int8(0x4);
static const System::Int8 SBP_UPPERTRACKHORZ = System::Int8(0x5);
static const System::Int8 SBP_LOWERTRACKVERT = System::Int8(0x6);
static const System::Int8 SBP_UPPERTRACKVERT = System::Int8(0x7);
static const System::Int8 SBP_GRIPPERHORZ = System::Int8(0x8);
static const System::Int8 SBP_GRIPPERVERT = System::Int8(0x9);
static const System::Int8 SBP_SIZEBOX = System::Int8(0xa);
static const System::Int8 ABS_UPNORMAL = System::Int8(0x1);
static const System::Int8 ABS_UPHOT = System::Int8(0x2);
static const System::Int8 ABS_UPPRESSED = System::Int8(0x3);
static const System::Int8 ABS_UPDISABLED = System::Int8(0x4);
static const System::Int8 ABS_DOWNNORMAL = System::Int8(0x5);
static const System::Int8 ABS_DOWNHOT = System::Int8(0x6);
static const System::Int8 ABS_DOWNPRESSED = System::Int8(0x7);
static const System::Int8 ABS_DOWNDISABLED = System::Int8(0x8);
static const System::Int8 ABS_LEFTNORMAL = System::Int8(0x9);
static const System::Int8 ABS_LEFTHOT = System::Int8(0xa);
static const System::Int8 ABS_LEFTPRESSED = System::Int8(0xb);
static const System::Int8 ABS_LEFTDISABLED = System::Int8(0xc);
static const System::Int8 ABS_RIGHTNORMAL = System::Int8(0xd);
static const System::Int8 ABS_RIGHTHOT = System::Int8(0xe);
static const System::Int8 ABS_RIGHTPRESSED = System::Int8(0xf);
static const System::Int8 ABS_RIGHTDISABLED = System::Int8(0x10);
static const System::Int8 ABS_UPHOVER = System::Int8(0x11);
static const System::Int8 ABS_DOWNHOVER = System::Int8(0x12);
static const System::Int8 ABS_LEFTHOVER = System::Int8(0x13);
static const System::Int8 ABS_RIGHTHOVER = System::Int8(0x14);
static const System::Int8 SCRBS_NORMAL = System::Int8(0x1);
static const System::Int8 SCRBS_HOT = System::Int8(0x2);
static const System::Int8 SCRBS_PRESSED = System::Int8(0x3);
static const System::Int8 SCRBS_DISABLED = System::Int8(0x4);
static const System::Int8 SCRBS_HOVER = System::Int8(0x5);
static const System::Word TMT_DEFAULTPANESIZE = System::Word(0x138a);
}	/* namespace Rvxptheme */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVXPTHEME)
using namespace Rvxptheme;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvxpthemeHPP
