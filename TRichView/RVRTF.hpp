﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVRTF.pas' rev: 27.00 (Windows)

#ifndef RvrtfHPP
#define RvrtfHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVGrHandler.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
#include <RVRTFErr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvrtf
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVRTFHighlightConvert : unsigned char { rtf_hl_Ignore, rtf_hl_FixedColors, rtf_hl_ColorTable };

enum DECLSPEC_DENUM TRVRTFTabAlign : unsigned char { rtf_tab_Left, rtf_tab_Right, rtf_tab_Center, rtf_tab_Decimal };

enum DECLSPEC_DENUM TRVRTFTabLeader : unsigned char { rtf_tl_None, rtf_tl_Dot, rtf_tl_MiddleDot, rtf_tl_Hyphen, rtf_tl_Underline, rtf_tl_EqualSign };

enum DECLSPEC_DENUM TRVRTFSeqType : unsigned char { rtf_seqDecimal, rtf_seqLowerAlpha, rtf_seqUpperAlpha, rtf_seqLowerRoman, rtf_seqUpperRoman };

enum DECLSPEC_DENUM TRVRTFPosition : unsigned char { rtf_ts_ContinuePara, rtf_ts_NewLine, rtf_ts_NewPara };

class DELPHICLASS TRVRTFReader;
typedef void __fastcall (__closure *TRVRTFNewTextEvent)(TRVRTFReader* Sender, const Rvtypes::TRVAnsiString Text, TRVRTFPosition Position);

enum DECLSPEC_DENUM TRVRTFHeaderFooterType : unsigned char { rtf_hf_MainText, rtf_hf_Header, rtf_hf_Footer, rtf_hf_HeaderL, rtf_hf_FooterL, rtf_hf_HeaderF, rtf_hf_FooterF };

typedef void __fastcall (__closure *TRVRTFHeaderFooterEvent)(TRVRTFReader* Sender, TRVRTFHeaderFooterType HFType, bool Starting, bool &Supported);

enum DECLSPEC_DENUM TRVRTFNoteEventType : unsigned char { rtf_ne_Character, rtf_ne_Start, rtf_ne_EndNote, rtf_ne_End };

typedef void __fastcall (__closure *TRVRTFNoteEvent)(TRVRTFReader* Sender, TRVRTFNoteEventType What, TRVRTFPosition Position);

typedef void __fastcall (__closure *TRVRTFImportPictureEvent)(TRVRTFReader* Sender, const System::UnicodeString Location, Vcl::Graphics::TGraphic* &Graphic, bool &Invalid);

typedef void __fastcall (__closure *TRVRTFNewUnicodeTextEvent)(TRVRTFReader* Sender, const Rvtypes::TRVUnicodeString Text, TRVRTFPosition Position);

typedef void __fastcall (__closure *TRVRTFTranslateKeywordEvent)(TRVRTFReader* Sender, const Rvtypes::TRVAnsiString Keyword, int Param, bool fParam, Rvtypes::TRVAnsiString &Text, bool &DoDefault);

typedef void __fastcall (__closure *TRVBookmarkEvent)(TRVRTFReader* Sender, const System::UnicodeString BookmarkName);

enum DECLSPEC_DENUM TRVRTFProgressStage : unsigned char { rvprtfprStarting, rvprtfprRunning, rvprtfprEnding };

typedef void __fastcall (__closure *TRVRTFProgressEvent)(TRVRTFReader* Sender, TRVRTFProgressStage Stage, System::Byte PercentDone);

enum DECLSPEC_DENUM TRVRTFUnderlineType : unsigned char { rtf_rvut_Normal, rtf_rvut_Thick, rtf_rvut_Double, rtf_rvut_Dotted, rtf_rvut_ThickDotted, rtf_rvut_Dashed, rtf_rvut_ThickDashed, rtf_rvut_LongDashed, rtf_rvut_ThickLongDashed, rtf_rvut_DashDotted, rtf_rvut_ThickDashDotted, rtf_rvut_DashDotDotted, rtf_rvut_ThickDashDotDotted };

enum DECLSPEC_DENUM TRVRTFParaListType : unsigned char { rtf_pn_Default, rtf_pn_Decimal, rtf_pn_LowerLetter, rtf_pn_UpperLetter, rtf_pn_LowerRoman, rtf_pn_UpperRoman, rtf_pn_Bullet };

enum DECLSPEC_DENUM TRVRTFPictureType : unsigned char { rtf_pict_EMF, rtf_pict_PNG, rtf_pict_JPEG, rtf_pict_MacPict, rtf_pict_PmMetafile, rtf_pict_WMF, rtf_pict_DIB, rtf_pict_DDB };

class DELPHICLASS TRVRTFPicture;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFPicture : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVRTFPictureType FType;
	int FMetafileMapMode;
	int FPicW;
	int FPicH;
	int FPicWGoalTw;
	int FPicHGoalTw;
	int FPicScaleX;
	int FPicScaleY;
	int FWBMWidthBytes;
	int FWBMBitsPixel;
	int FWBMPlanes;
	bool FMetafileWithBitmap;
	bool FShpPict;
	
public:
	System::Classes::TMemoryStream* FData;
	int SuggestedWidthTw;
	int SuggestedHeightTw;
	__fastcall TRVRTFPicture(void);
	__fastcall virtual ~TRVRTFPicture(void);
	__property int PicW = {read=FPicW, nodefault};
	__property int PicH = {read=FPicH, nodefault};
	__property int PicWGoalTw = {read=FPicWGoalTw, nodefault};
	__property int PicHGoalTw = {read=FPicHGoalTw, nodefault};
	__property int PicScaleX = {read=FPicScaleX, nodefault};
	__property int PicScaleY = {read=FPicScaleY, nodefault};
	__property TRVRTFPictureType PicType = {read=FType, nodefault};
	__property bool MetafileWithBitmap = {read=FMetafileWithBitmap, nodefault};
	__property bool ShpPict = {read=FShpPict, nodefault};
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFObjectType : unsigned char { rtf_obj_Emb, rtf_obj_Link, rtf_obj_AutLink, rtf_obj_Sub, rtf_obj_Pub, rtf_obj_ICEmb, rtf_obj_HTML, rtf_obj_OCX };

class DELPHICLASS TRVRTFObject;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFObject : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVRTFObjectType FType;
	System::Classes::TMemoryStream* FData;
	int FWidthTw;
	int FHeightTw;
	
public:
	__fastcall TRVRTFObject(void);
	__fastcall virtual ~TRVRTFObject(void);
	__property TRVRTFObjectType ObjType = {read=FType, nodefault};
	__property System::Classes::TMemoryStream* Data = {read=FData};
	__property int WidthTw = {read=FWidthTw, nodefault};
	__property int HeightTw = {read=FHeightTw, nodefault};
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFTableEventKind : unsigned char { rvf_tbl_TableStart, rvf_tbl_TableEnd, rvf_tbl_TableForcedEnd, rvf_tbl_RowEnd, rvf_tbl_CellEnd };

typedef void __fastcall (__closure *TRVRTFTableEvent)(TRVRTFReader* Sender, TRVRTFTableEventKind WhatHappens, bool &Processed);

typedef void __fastcall (__closure *TRVRTFNewPictureEvent)(TRVRTFReader* Sender, TRVRTFPicture* RTFPicture, Vcl::Graphics::TGraphic* Graphic, TRVRTFPosition Position, const System::UnicodeString FileName, bool &Inserted);

typedef void __fastcall (__closure *TRVRTFNewObjectEvent)(TRVRTFReader* Sender, TRVRTFObject* RTFObject, TRVRTFPosition Position, bool &Inserted);

typedef void __fastcall (__closure *TRVRTFNewSeqEvent)(TRVRTFReader* Sender, TRVRTFPosition Position, const System::UnicodeString SeqName, TRVRTFSeqType NumberingType, bool Reset, int StartFrom);

enum DECLSPEC_DENUM TRVRTFAlignment : unsigned char { rtf_al_Left, rtf_al_Right, rtf_al_Center, rtf_al_Justify };

enum DECLSPEC_DENUM TRVRTFBiDiMode : unsigned char { rtf_bidi_Unspecified, rtf_bidi_LTR, rtf_bidi_RTL };

enum DECLSPEC_DENUM TRVRTFVAlign : unsigned char { rtf_val_Top, rtf_val_Bottom, rtf_val_Center };

enum DECLSPEC_DENUM TRVRTFSScriptType : unsigned char { rtf_ss_Normal, rtf_ss_Subscript, rtf_ss_Superscript };

enum DECLSPEC_DENUM TRVRTFFontStyleEx : unsigned char { rtf_fs_AllCaps };

typedef System::Set<TRVRTFFontStyleEx, TRVRTFFontStyleEx::rtf_fs_AllCaps, TRVRTFFontStyleEx::rtf_fs_AllCaps> TRVRTFFontStylesEx;

class DELPHICLASS TRVRTFCharProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFCharProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	bool FHidden;
	TRVRTFBiDiMode FBiDiMode;
	int FSizeDouble;
	System::Uitypes::TColor FColor;
	System::Uitypes::TColor FBackColor;
	System::Uitypes::TColor FUnderlineColor;
	int FFontIndex;
	System::Uitypes::TFontStyles FStyle;
	TRVRTFFontStylesEx FStyleEx;
	int FCharScaleX;
	TRVRTFSScriptType FSScriptType;
	int FVShiftPt;
	int FCharSpacingTw;
	unsigned FLanguage;
	System::UnicodeString FFontName;
	TRVRTFUnderlineType FUnderlineType;
	int FHighlight;
	int FSkipAnsiCount;
	int FStyleSheetIndex;
	void __fastcall ChangeFontStyle(System::Uitypes::TFontStyle fs, int Val);
	void __fastcall ChangeFontStyleEx(TRVRTFFontStyleEx fs, int Val);
	
public:
	__fastcall TRVRTFCharProperties(void);
	virtual void __fastcall Reset(unsigned DefLanguage, int DefFontIndex);
	virtual void __fastcall Assign(TRVRTFCharProperties* Source);
	virtual bool __fastcall CanParseKeyword(const Rvtypes::TRVAnsiString Keyword);
	virtual void __fastcall ParseKeyword(const Rvtypes::TRVAnsiString Keyword, int param, bool fParam);
	__property int SizeDouble = {read=FSizeDouble, nodefault};
	__property System::Uitypes::TColor Color = {read=FColor, nodefault};
	__property System::Uitypes::TColor BackColor = {read=FBackColor, nodefault};
	__property int FontIndex = {read=FFontIndex, nodefault};
	__property System::Uitypes::TFontStyles Style = {read=FStyle, nodefault};
	__property TRVRTFFontStylesEx StyleEx = {read=FStyleEx, nodefault};
	__property int CharScaleX = {read=FCharScaleX, nodefault};
	__property TRVRTFSScriptType SScriptType = {read=FSScriptType, nodefault};
	__property int VShiftPt = {read=FVShiftPt, nodefault};
	__property int CharSpacingTw = {read=FCharSpacingTw, nodefault};
	__property bool Hidden = {read=FHidden, nodefault};
	__property System::UnicodeString FontName = {read=FFontName};
	__property unsigned Language = {read=FLanguage, nodefault};
	__property TRVRTFUnderlineType UnderlineType = {read=FUnderlineType, nodefault};
	__property System::Uitypes::TColor UnderlineColor = {read=FUnderlineColor, nodefault};
	__property TRVRTFBiDiMode BiDiMode = {read=FBiDiMode, nodefault};
	__property int StyleSheetIndex = {read=FStyleSheetIndex, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFCharProperties(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TRVRTFCharPropertiesClass;

enum DECLSPEC_DENUM TRVRTFBorderType : unsigned char { rtf_brdr_None, rtf_brdr_SingleThickness, rtf_brdr_DoubleThickness, rtf_brdr_Shadow, rtf_brdr_Double, rtf_brdr_Dot, rtf_brdr_Dash, rtf_brdr_Hairline, rtf_brdr_DashSmall, rtf_brdr_DotDash, rtf_brdr_DotDotDash, rtf_brdr_Triple, rtf_brdr_ThickThinSmall, rtf_brdr_ThinThickSmall, rtf_brdr_ThinThickThinSmall, rtf_brdr_ThickThinMed, rtf_brdr_ThinThickMed, rtf_brdr_ThinThickThinMed, rtf_brdr_ThickThinLarge, rtf_brdr_ThinThickLarge, rtf_brdr_ThinThickThinLarge, rtf_brdr_Wavy, rtf_brdr_DoubleWavy, rtf_brdr_Striped, rtf_brdr_Emboss, rtf_brdr_Engrave, rtf_brdr_Inset, rtf_brdr_Outset };

enum DECLSPEC_DENUM TRVRTFSide : unsigned char { rtf_side_Left, rtf_side_Top, rtf_side_Right, rtf_side_Bottom };

class DELPHICLASS TRVRTFBorderSide;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFBorderSide : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVRTFBorderType FBorderType;
	int FWidthTw;
	System::Uitypes::TColor FColor;
	int FSpaceTw;
	
public:
	void __fastcall Assign(TRVRTFBorderSide* Source);
	__property TRVRTFBorderType BorderType = {read=FBorderType, nodefault};
	void __fastcall Reset(void);
	__property int WidthTw = {read=FWidthTw, nodefault};
	__property System::Uitypes::TColor Color = {read=FColor, nodefault};
	__property int SpaceTw = {read=FSpaceTw, nodefault};
public:
	/* TObject.Create */ inline __fastcall TRVRTFBorderSide(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFBorderSide(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFParaBorder;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFParaBorder : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::StaticArray<TRVRTFBorderSide*, 4> FSides;
	TRVRTFBorderSide* __fastcall GetSides(TRVRTFSide Index);
	
public:
	void __fastcall Assign(TRVRTFParaBorder* Source);
	__fastcall virtual ~TRVRTFParaBorder(void);
	void __fastcall Reset(void);
	__property TRVRTFBorderSide* Sides[TRVRTFSide Index] = {read=GetSides};
public:
	/* TObject.Create */ inline __fastcall TRVRTFParaBorder(void) : System::TObject() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFMarkerProp : unsigned char { rtfmp_FontIndex, rtfmp_Size, rtfmp_Color, rtfmp_Bold, rtfmp_Italic, rtfmp_Underline, rtfmp_StrikeOut };

typedef System::Set<TRVRTFMarkerProp, TRVRTFMarkerProp::rtfmp_FontIndex, TRVRTFMarkerProp::rtfmp_StrikeOut> TRVRTFMarkerProps;

class DELPHICLASS TRVRTFCustomMarkerProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFCustomMarkerProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVRTFParaListType FListType;
	int FFontIndex;
	int FIndentTw;
	int FSpaceTw;
	int FStart;
	System::Uitypes::TFontStyles FFontStyle;
	System::Uitypes::TColor FColor;
	TRVRTFAlignment FAlignment;
	TRVRTFMarkerProps FFixedProperties;
	void __fastcall ChangeFontStyle(System::Uitypes::TFontStyle fs, int Val);
	
public:
	int FFontSize;
	__fastcall TRVRTFCustomMarkerProperties(void);
	DYNAMIC void __fastcall Reset(void);
	void __fastcall Assign(TRVRTFCustomMarkerProperties* Source, bool FromDefaults);
	void __fastcall UpdateFrom(TRVRTFCharProperties* CharProps);
	__property TRVRTFParaListType ListType = {read=FListType, nodefault};
	__property int FontIndex = {read=FFontIndex, nodefault};
	__property int FontSize = {read=FFontSize, nodefault};
	__property int IndentTw = {read=FIndentTw, nodefault};
	__property int SpaceTw = {read=FSpaceTw, nodefault};
	__property int Start = {read=FStart, nodefault};
	__property System::Uitypes::TFontStyles FontStyle = {read=FFontStyle, nodefault};
	__property System::Uitypes::TColor Color = {read=FColor, nodefault};
	__property TRVRTFAlignment Alignment = {read=FAlignment, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFCustomMarkerProperties(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFMarkerProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFMarkerProperties : public TRVRTFCustomMarkerProperties
{
	typedef TRVRTFCustomMarkerProperties inherited;
	
private:
	int FLevel;
	bool FHanging;
	Rvtypes::TRVAnsiString FTextAfter;
	Rvtypes::TRVAnsiString FTextBefore;
	
public:
	DYNAMIC void __fastcall Reset(void);
	HIDESBASE void __fastcall Assign(TRVRTFMarkerProperties* Source, bool FromDefaults);
	__property int Level = {read=FLevel, nodefault};
	__property Rvtypes::TRVAnsiString TextAfter = {read=FTextAfter};
	__property Rvtypes::TRVAnsiString TextBefore = {read=FTextBefore};
	__property bool Hanging = {read=FHanging, nodefault};
public:
	/* TRVRTFCustomMarkerProperties.Create */ inline __fastcall TRVRTFMarkerProperties(void) : TRVRTFCustomMarkerProperties() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFMarkerProperties(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFLevelFollow97 : unsigned char { rtf_lf_Tab, rtf_lf_Space, rtf_lf_None };

class DELPHICLASS TRVRTFListLevel97;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFListLevel97 : public TRVRTFCustomMarkerProperties
{
	typedef TRVRTFCustomMarkerProperties inherited;
	
private:
	bool FOldStyle;
	bool FLegal;
	bool FNoRestart;
	System::UnicodeString FText;
	Rvtypes::TRVAnsiString FNumbers;
	Rvtypes::TRVUnicodeString FTextW;
	TRVRTFLevelFollow97 FFollow;
	int FLeftIndentTw;
	int FFirstIndentTw;
	int FTabPosTw;
	
public:
	bool FFontSizeDefined;
	bool FIndentsUpdated;
	DYNAMIC void __fastcall Reset(void);
	HIDESBASE void __fastcall Assign(TRVRTFListLevel97* Source);
	__property bool OldStyle = {read=FOldStyle, nodefault};
	__property bool Legal = {read=FLegal, nodefault};
	__property bool NoRestart = {read=FNoRestart, nodefault};
	__property System::UnicodeString Text = {read=FText};
	__property Rvtypes::TRVUnicodeString TextW = {read=FTextW};
	__property Rvtypes::TRVAnsiString Numbers = {read=FNumbers};
	__property TRVRTFLevelFollow97 Follow = {read=FFollow, nodefault};
	__property int LeftIndentTw = {read=FLeftIndentTw, nodefault};
	__property int FirstIndentTw = {read=FFirstIndentTw, nodefault};
	__property int TabPosTw = {read=FTabPosTw, nodefault};
public:
	/* TRVRTFCustomMarkerProperties.Create */ inline __fastcall TRVRTFListLevel97(void) : TRVRTFCustomMarkerProperties() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFListLevel97(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFList97;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFList97 : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFListLevel97* operator[](int Index) { return Items[Index]; }
	
private:
	int FId;
	int FTemplateId;
	bool FSimple;
	Rvtypes::TRVAnsiString FName;
	HIDESBASE TRVRTFListLevel97* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFListLevel97* const Value);
	
protected:
	TRVRTFListLevel97* __fastcall GetLastLevel(void);
	void __fastcall AddNew(void);
	
public:
	__property TRVRTFListLevel97* Items[int Index] = {read=Get, write=Put/*, default*/};
	__property int Id = {read=FId, nodefault};
	__property int TemplateId = {read=FTemplateId, nodefault};
	__property bool Simple = {read=FSimple, nodefault};
	__property Rvtypes::TRVAnsiString Name = {read=FName};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFList97(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFList97(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFListTable97;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFListTable97 : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFList97* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVRTFList97* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFList97* const Value);
	
protected:
	TRVRTFList97* __fastcall GetLastList(void);
	int __fastcall FindList(int ID);
	void __fastcall AddNew(void);
	
public:
	__property TRVRTFList97* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFListTable97(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFListTable97(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFListOverrideLevel97;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFListOverrideLevel97 : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	bool FUseStart;
	int FStart;
	
public:
	bool Used;
	__fastcall TRVRTFListOverrideLevel97(void);
	__property int Start = {read=FStart, nodefault};
	__property bool UseStart = {read=FUseStart, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFListOverrideLevel97(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFListOverride97;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFListOverride97 : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFListOverrideLevel97* operator[](int Index) { return Items[Index]; }
	
private:
	int FListIndex;
	int FNumber;
	int FOverrideCount;
	HIDESBASE TRVRTFListOverrideLevel97* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFListOverrideLevel97* const Value);
	
public:
	TRVRTFListOverrideLevel97* __fastcall GetLastLevel(void);
	void __fastcall AddNew(void);
	__property int ListIndex = {read=FListIndex, nodefault};
	__property int Number = {read=FNumber, nodefault};
	__property int OverrideCount = {read=FOverrideCount, nodefault};
	__property TRVRTFListOverrideLevel97* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFListOverride97(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFListOverride97(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFListOverrideTable97;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFListOverrideTable97 : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFListOverride97* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVRTFListOverride97* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFListOverride97* const Value);
	
protected:
	TRVRTFListOverride97* __fastcall GetLastListOverride(void);
	int __fastcall FindListOverride(int Number);
	void __fastcall AddNew(void);
	
public:
	__property TRVRTFListOverride97* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFListOverrideTable97(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFListOverrideTable97(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFTab;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFTab : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FPositionTW;
	TRVRTFTabAlign FAlign;
	TRVRTFTabLeader FLeader;
	bool FIsListTab;
	
public:
	void __fastcall Assign(TRVRTFTab* Source);
	__property int PositionTW = {read=FPositionTW, nodefault};
	__property TRVRTFTabAlign Align = {read=FAlign, nodefault};
	__property TRVRTFTabLeader Leader = {read=FLeader, nodefault};
	__property bool IsListTab = {read=FIsListTab, nodefault};
public:
	/* TObject.Create */ inline __fastcall TRVRTFTab(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFTab(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFTabList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFTabList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFTab* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVRTFTab* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFTab* const Value);
	
protected:
	void __fastcall AddNew(void);
	TRVRTFTab* __fastcall GetLastTab(void);
	
public:
	HIDESBASE void __fastcall Assign(TRVRTFTabList* Source);
	__property TRVRTFTab* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFTabList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFTabList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFParaProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFParaProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FLeftIndentTw;
	int FRightIndentTw;
	int FFirstIndentTw;
	int FMarkerTabTw;
	int FOutlineLevel;
	int FSpaceBeforeTw;
	int FSpaceAfterTw;
	TRVRTFAlignment FAlignment;
	TRVRTFBiDiMode FBiDiMode;
	TRVRTFParaBorder* FBorder;
	TRVRTFMarkerProperties* FMarkerProps;
	TRVRTFSide FCurBorderSide;
	int FLineSpacing;
	bool FLineSpacingMulti;
	System::Uitypes::TColor FColor;
	int FNestingLevel;
	bool FInTable;
	int Shading;
	System::Uitypes::TColor ForeColor;
	int FListOverrideIndex;
	int FListLevel;
	int FStyleSheetIndex;
	bool FKeepLinesTogether;
	bool FKeepWithNext;
	bool NoTableEv;
	bool NoResetLev;
	TRVRTFTabList* FTabs;
	bool FTabsReady;
	TRVRTFTabList* __fastcall GetTabs(void);
	TRVRTFParaBorder* __fastcall GetBorder(void);
	TRVRTFMarkerProperties* __fastcall GetMarkerProps(void);
	
public:
	__fastcall TRVRTFParaProperties(void);
	__fastcall virtual ~TRVRTFParaProperties(void);
	void __fastcall Finalize(void);
	void __fastcall Reset(void);
	void __fastcall Assign(TRVRTFParaProperties* Source);
	bool __fastcall HasBorder(void);
	bool __fastcall HasMarker(void);
	bool __fastcall HasTabs(void);
	__property TRVRTFTabList* Tabs = {read=GetTabs};
	__property int LeftIndentTw = {read=FLeftIndentTw, nodefault};
	__property int RightIndentTw = {read=FRightIndentTw, nodefault};
	__property int FirstIndentTw = {read=FFirstIndentTw, nodefault};
	__property int SpaceBeforeTw = {read=FSpaceBeforeTw, nodefault};
	__property int SpaceAfterTw = {read=FSpaceAfterTw, nodefault};
	__property int MarkerTabTw = {read=FMarkerTabTw, write=FMarkerTabTw, nodefault};
	__property TRVRTFAlignment Alignment = {read=FAlignment, nodefault};
	__property TRVRTFParaBorder* Border = {read=GetBorder};
	__property TRVRTFMarkerProperties* MarkerProps = {read=GetMarkerProps};
	__property System::Uitypes::TColor Color = {read=FColor, nodefault};
	__property int LineSpacing = {read=FLineSpacing, nodefault};
	__property bool LineSpacingMulti = {read=FLineSpacingMulti, nodefault};
	__property int NestingLevel = {read=FNestingLevel, nodefault};
	__property bool InTable = {read=FInTable, nodefault};
	__property int ListOverrideIndex = {read=FListOverrideIndex, nodefault};
	__property int ListLevel = {read=FListLevel, nodefault};
	__property bool KeepLinesTogether = {read=FKeepLinesTogether, nodefault};
	__property bool KeepWithNext = {read=FKeepWithNext, nodefault};
	__property TRVRTFBiDiMode BiDiMode = {read=FBiDiMode, nodefault};
	__property int OutlineLevel = {read=FOutlineLevel, nodefault};
	__property int StyleSheetIndex = {read=FStyleSheetIndex, nodefault};
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFCellMerge : unsigned char { rtf_cm_None, rtf_cm_First, rtf_cm_Merged };

enum DECLSPEC_DENUM TRVRTFCellTextFlow : unsigned char { rtf_ctf_Normal, rtf_ctf_TopDown, rtf_ctf_BottomUp };

class DELPHICLASS TRVRTFCellProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFCellProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVRTFCellMerge FHMerge;
	TRVRTFCellMerge FVMerge;
	bool InvertWidth;
	int FBestWidth;
	System::Uitypes::TColor FColor;
	TRVRTFVAlign FVAlign;
	TRVRTFParaBorder* FBorder;
	TRVRTFSide FCurBorderSide;
	int FRightBoundaryTw;
	System::Uitypes::TColor ForeColor;
	TRVRTFCellTextFlow FTextFlow;
	int Shading;
	void __fastcall Finalize(void);
	
public:
	int BestHeightTw;
	__fastcall TRVRTFCellProperties(void);
	__fastcall virtual ~TRVRTFCellProperties(void);
	void __fastcall Reset(void);
	void __fastcall Assign(TRVRTFCellProperties* Source);
	__property TRVRTFCellMerge HMerge = {read=FHMerge, nodefault};
	__property TRVRTFCellMerge VMerge = {read=FVMerge, nodefault};
	__property int BestWidth = {read=FBestWidth, nodefault};
	__property System::Uitypes::TColor Color = {read=FColor, nodefault};
	__property TRVRTFVAlign VAlign = {read=FVAlign, nodefault};
	__property TRVRTFParaBorder* Border = {read=FBorder};
	__property int RightBoundaryTw = {read=FRightBoundaryTw, nodefault};
	__property TRVRTFCellTextFlow TextFlow = {read=FTextFlow, nodefault};
};

#pragma pack(pop)

class DELPHICLASS TRVRTFCellPropsList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFCellPropsList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFCellProperties* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVRTFCellProperties* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFCellProperties* const Value);
	
public:
	void __fastcall AddNew(void);
	void __fastcall AssignItems(TRVRTFCellPropsList* Source);
	__property TRVRTFCellProperties* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFCellPropsList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFCellPropsList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFRowProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFRowProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	bool InvertWidth;
	bool FHeading;
	bool FLastRow;
	int FGapHTw;
	int FLeftTw;
	int FHeightTw;
	int FBestWidth;
	TRVRTFParaBorder* FBorder;
	TRVRTFSide FCurBorderSide;
	TRVRTFCellPropsList* FCellProps;
	bool NewCellProps;
	bool AssumedLastCell;
	TRVRTFAlignment FAlignment;
	bool FAlignmentDefined;
	bool FKeepTogether;
	int __fastcall GetPaddingTw(TRVRTFSide Index);
	int __fastcall GetSpacingTw(TRVRTFSide Index);
	bool __fastcall GetUsePadding(TRVRTFSide Index);
	bool __fastcall GetUseSpacing(TRVRTFSide Index);
	TRVRTFCellProperties* __fastcall GetLastCellProp(void);
	void __fastcall Finalize(void);
	
public:
	bool RichViewSpecial;
	System::StaticArray<int, 4> FPaddingTw;
	System::StaticArray<int, 4> FSpacingTw;
	System::StaticArray<bool, 4> FUsePadding;
	System::StaticArray<bool, 4> FUseSpacing;
	__fastcall TRVRTFRowProperties(void);
	__fastcall virtual ~TRVRTFRowProperties(void);
	void __fastcall Reset(void);
	void __fastcall Assign(TRVRTFRowProperties* Source);
	__property TRVRTFCellPropsList* CellProps = {read=FCellProps};
	__property int GapHTw = {read=FGapHTw, nodefault};
	__property int LeftTw = {read=FLeftTw, write=FLeftTw, nodefault};
	__property int HeightTw = {read=FHeightTw, nodefault};
	__property int BestWidth = {read=FBestWidth, nodefault};
	__property int PaddingTw[TRVRTFSide Index] = {read=GetPaddingTw};
	__property int SpacingTw[TRVRTFSide Index] = {read=GetSpacingTw};
	__property bool UsePadding[TRVRTFSide Index] = {read=GetUsePadding};
	__property bool UseSpacing[TRVRTFSide Index] = {read=GetUseSpacing};
	__property TRVRTFParaBorder* Border = {read=FBorder};
	__property bool Heading = {read=FHeading, nodefault};
	__property bool LastRow = {read=FLastRow, nodefault};
	__property TRVRTFAlignment Alignment = {read=FAlignment, nodefault};
	__property bool AlignmentDefined = {read=FAlignmentDefined, nodefault};
	__property bool KeepTogether = {read=FKeepTogether, nodefault};
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFSectionBreakType : unsigned char { rtf_sbk_None, rtf_sbk_Column, rtf_sbk_Even, rtf_sbk_Odd, rtf_sbk_Page };

enum DECLSPEC_DENUM TRVRTFPageNumberFormat : unsigned char { rtf_pg_Decimal, rtf_pg_UpperRoman, rtf_pg_LowerRoman, rtf_pg_UpperLetter, rtf_pg_LowerLetter };

class DELPHICLASS TRVRTFSectionProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFSectionProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FColumnCount;
	TRVRTFSectionBreakType FSectionBreakType;
	int FPageNumberXTw;
	int FPageNumberYTw;
	TRVRTFPageNumberFormat FPageNumberFormat;
	int FHeaderYTw;
	int FFooterYTw;
	Rvclasses::TRVList* FDefMarkerPropsList;
	bool FHasTitlePage;
	void __fastcall InitListDefaults(void);
	
public:
	__fastcall TRVRTFSectionProperties(void);
	void __fastcall Reset(void);
	__fastcall virtual ~TRVRTFSectionProperties(void);
	void __fastcall Assign(TRVRTFSectionProperties* Source);
	__property int ColumnCount = {read=FColumnCount, nodefault};
	__property TRVRTFSectionBreakType SectionBreakType = {read=FSectionBreakType, nodefault};
	__property int PageNumberXTw = {read=FPageNumberXTw, nodefault};
	__property int PageNumberYTw = {read=FPageNumberYTw, nodefault};
	__property TRVRTFPageNumberFormat PageNumberFormat = {read=FPageNumberFormat, nodefault};
	__property int HeaderYTw = {read=FHeaderYTw, nodefault};
	__property int FooterYTw = {read=FFooterYTw, nodefault};
	__property bool HasTitlePage = {read=FHasTitlePage, nodefault};
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFZoomKind : unsigned char { rtf_zk_None, rtf_zk_FullPage, rtf_zk_BestFit };

class DELPHICLASS TRVRTFDocProperties;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFDocProperties : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FPaperWidthTw;
	int FPaperHeightTw;
	int FLeftMarginTw;
	int FTopMarginTw;
	int FRightMarginTw;
	int FBottomMarginTw;
	int FPageNumberStart;
	bool FFacingPages;
	bool FMirrorMargins;
	bool FLandscape;
	int FViewScale;
	TRVRTFZoomKind FZoomKind;
	bool FFootnotePageRestart;
	System::UnicodeString FAuthor;
	System::UnicodeString FComments;
	System::UnicodeString FTitle;
	
public:
	__fastcall TRVRTFDocProperties(void);
	void __fastcall Reset(void);
	void __fastcall Assign(TRVRTFDocProperties* Source);
	__property int PaperWidthTw = {read=FPaperWidthTw, nodefault};
	__property int PaperHeightTw = {read=FPaperHeightTw, nodefault};
	__property int LeftMarginTw = {read=FLeftMarginTw, nodefault};
	__property int TopMarginTw = {read=FTopMarginTw, nodefault};
	__property int RightMarginTw = {read=FRightMarginTw, nodefault};
	__property int BottomMarginTw = {read=FBottomMarginTw, nodefault};
	__property int PageNumberStart = {read=FPageNumberStart, nodefault};
	__property bool FacingPages = {read=FFacingPages, nodefault};
	__property bool MirrorMargins = {read=FMirrorMargins, nodefault};
	__property bool Landscape = {read=FLandscape, nodefault};
	__property int ViewScale = {read=FViewScale, nodefault};
	__property TRVRTFZoomKind ZoomKind = {read=FZoomKind, nodefault};
	__property bool FootnotePageRestart = {read=FFootnotePageRestart, nodefault};
	__property System::UnicodeString Author = {read=FAuthor};
	__property System::UnicodeString Title = {read=FTitle};
	__property System::UnicodeString Comments = {read=FComments};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFDocProperties(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRTFrds : unsigned char { rdsNorm, rdsSkip, rdsFontTable, rdsStyleSheet, rdsColorTable, rdsStyleSheetEntry, rdsPict, rdsShpPict, rdsObject, rdsObjData, rdsBmkStart, rdsInfo, rdsAuthor, rdsTitle, rdsDocComm, rdsField, rdsFldInst, rdsPN, rdsPNTextAfter, rdsPNTextBefore, rdsPNSecLvl, rdsListTable, rdsList, rdsListLevel, rdsListName, rdsListLevelText, rdsListLevelNumbers, rdsLOTable, rdsLO, rdsLOLevel, rdsUpr };

enum DECLSPEC_DENUM TRTFris : unsigned char { risNorm, risBin, risHex };

enum DECLSPEC_DENUM TRTFIPROP : unsigned char { ipropBold, ipropItalic, ipropStrike, ipropUnderline, ipropUnderlineType, ipropUnderlineColor, ipropTextBiDi, ipropCharScaleX, ipropCharSpacing, ipropSScript, ipropVShiftUp, ipropVShiftDown, ipropAllCaps, ipropFontSize, ipropTextColor, ipropTextBackColor, ipropHighlight, ipropLanguage, ipropDefLanguage, ipropSL, ipropSLMult, ipropLeftInd, ipropRightInd, ipropFirstInd, ipropCols, ipropPgnX, ipropPgnY, ipropHeaderY, ipropFooterY, ipropTitlePg, ipropXaPage, ipropYaPage, ipropXaLeft, ipropXaRight, ipropYaTop, ipropYaBottom, ipropPgnStart, ipropSbk, ipropPgnFormat, ipropFacingp, ipropMirrorMargins, ipropLandscape, ipropViewScale, ipropViewZoomKind, ipropJust, ipropParaBiDi, ipropParD, ipropPlain, ipropSectd, ipropF, 
	ipropAF, ipropDefF, ipropSpaceBefore, ipropSpaceAfter, ipropCharBorderSide, ipropParaBorderType, ipropParaBorderSide, ipropParaBorderWidth, ipropParaBorderColor, ipropParaBorderSpace, ipropParaColor, ipropParaFColor, ipropParaShading, ipropOutLineLevel, ipropAnsiCodePage, ipropU, ipropUC, ipropPage, ipropField, ipropFldInst, ipropFldRslt, ipropHidden, ipropKeepLinesTogether, ipropKeepWithNext, ipropTX, ipropTabAlign, ipropTabLeader, ipropListTab, ipropRed, ipropGreen, ipropBlue, ipropFontFamily, ipropFCharset, ipropS, ipropCS, ipropTS, ipropDS, ipropSBasedOn, ipropSLink, ipropSNext, ipropAdditive, ipropSHidden, ipropSQuickAccess, ipropPicW, ipropPicH, ipropPicScaleX, ipropPicScaleY, ipropPicWGoal, ipropPicHGoal, ipropWBMWidthBytes, ipropWBMBitsPixel, 
	ipropWBMPlanes, ipropPictureType, ipropMetafile, ipropPicBmp, ipropObjType, ipropObjWidth, ipropObjHeight, ipropPNLevel, ipropPNHanging, ipropPNType, ipropPNBold, ipropPNItalic, ipropPNUnderline, ipropPNStrike, ipropPNColor, ipropPNF, ipropPNFontSize, ipropPNIndent, ipropPNSp, ipropPNAlign, ipropPNStart, ipropPNSecLvl, ipropListId, ipropListTemplateId, ipropListSimple, ipropListName, ipropLevelStartAt, ipropLevelNumberType, ipropLevelJustify, ipropLevelOld, ipropLevelIndent, ipropLevelSpace, ipropLevelFollow, ipropLevelLegal, ipropLevelNoRestart, ipropLOCount, ipropLONumber, ipropLOStart, ipropLevel, ipropThisIsEndnote, ipropNoteCharacter, ipropFootNoteRestart, ipropRowEnd, ipropCellEnd, ipropInTbl, ipropItap, ipropLastRow, ipropTRowD, ipropRowAlign, 
	ipropTRGapH, ipropTRLeft, ipropTRRowHeight, ipropTRHeader, ipropTRPaddL, ipropTRPaddR, ipropTRPaddT, ipropTRPaddB, ipropTRPaddFL, ipropTRPaddFR, ipropTRPaddFT, ipropTRPaddFB, ipropTRSpdL, ipropTRSpdR, ipropTRSpdT, ipropTRSpdB, ipropTRSpdFL, ipropTRSpdFR, ipropTRSpdFT, ipropTRSpdFB, ipropTRwWidth, ipropTRftsWidth, ipropTRKeepTogether, ipropCLVMerge, ipropCLHMerge, ipropCLTextFlow, ipropCLwWidth, ipropCLftsWidth, ipropCLColor, ipropCLFColor, ipropCLShading, ipropCLVertAl, ipropCellX, ipropTRBorderSide, ipropBorderSideUnknown, ipropCLBorderSide, ipropNoTableEv, ipropNoResetLev, ipropRVCellBestWidth, ipropRVCellBestHeight, ipropRVTableBestWidth, ipropRVCellBestWidthTw, ipropRVCellBestHeightTw, ipropRVTableBestWidthTw, ipropMax };

enum DECLSPEC_DENUM TRTFACTN : unsigned char { actnSpec, actnByte, actnWord };

enum DECLSPEC_DENUM TRTFPROPTYPE : unsigned char { propChp, propPap, propSep, propDop };

struct DECLSPEC_DRECORD TRTFpropmod
{
public:
	TRTFACTN actn;
	TRTFPROPTYPE prop;
	int offset;
};


enum DECLSPEC_DENUM TRTFIPFN : unsigned char { ipfnBin, ipfnHex, ipfnSkipDest };

enum DECLSPEC_DENUM TRTFIDEST : unsigned char { idestNormal, idestShpPict, idestNonShpPict, idestPict, idestSkip, idestFontTable, idestStyleSheet, idestColorTable, idestStyleSheetParaStyle, idestStyleSheetCharStyle, idestStyleSheetSectStyle, idestStyleSheetTableStyle, idestBmkStart, idestInfo, idestAuthor, idestTitle, idestDocComm, idestField, idestFldInst, idestFldRslt, idestNestedTableProps, idestHeader, idestFooter, idestHeaderL, idestFooterL, idestHeaderR, idestFooterR, idestHeaderF, idestFooterF, idestNote, idestPNTextAfter, idestPNTextBefore, idestPN, idestPNSecLvl, idestListTable, idestList, idestListName, idestListLevel, idestLevelText, idestLevelNumbers, idestLOTable, idestLO, idestLOLevel, idestObject, idestObjData, idestObjResult, idestUpr, 
	idestUd };

enum DECLSPEC_DENUM TRVRTFKeywordType : unsigned char { rtf_kwd_Char, rtf_kwd_WideChar, rtf_kwd_Dest, rtf_kwd_Prop, rtf_kwd_Spec };

enum DECLSPEC_DENUM TRVRTFKeywordAffect : unsigned char { rtf_af_None, rtf_af_CharProp, rtf_af_ParaProp, rtf_af_BothProp };

struct DECLSPEC_DRECORD TRVRTFsymbol
{
public:
	Rvtypes::TRVAnsiString Keyword;
	int DefValue;
	bool UseDef;
	TRVRTFKeywordType Kwd;
	int Idx;
	TRVRTFKeywordAffect AffectTo;
};


typedef TRVRTFsymbol *PRVRTFsymbol;

enum DECLSPEC_DENUM TRVRTFStyleSheetType : unsigned char { rtf_sst_Char, rtf_sst_Par, rtf_sst_Sect, rtf_sst_Table };

class DELPHICLASS TRVRTFStyleSheetEntry;
class DELPHICLASS TRVRTFReaderState;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFStyleSheetEntry : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TRVRTFParaProperties* FParaProps;
	TRVRTFCharProperties* FCharProps;
	bool FAdditive;
	bool FHidden;
	int FNumber;
	TRVRTFStyleSheetType FStyleType;
	int FBasedOnIndex;
	int FNextIndex;
	int FLinkIndex;
	int FBasedOnNumber;
	int FNextNumber;
	int FLinkNumber;
	System::UnicodeString FName;
	bool FQuickAccess;
	
public:
	__fastcall TRVRTFStyleSheetEntry(void);
	__fastcall virtual ~TRVRTFStyleSheetEntry(void);
	void __fastcall Assign(TRVRTFReaderState* Source);
	__property TRVRTFParaProperties* ParaProps = {read=FParaProps};
	__property TRVRTFCharProperties* CharProps = {read=FCharProps};
	__property bool Additive = {read=FAdditive, nodefault};
	__property bool Hidden = {read=FHidden, nodefault};
	__property int Number = {read=FNumber, nodefault};
	__property TRVRTFStyleSheetType StyleType = {read=FStyleType, nodefault};
	__property int BasedOnIndex = {read=FBasedOnIndex, nodefault};
	__property int LinkIndex = {read=FLinkIndex, nodefault};
	__property int NextIndex = {read=FNextIndex, nodefault};
	__property System::UnicodeString Name = {read=FName};
	__property bool QuickAccess = {read=FQuickAccess, nodefault};
};

#pragma pack(pop)

class DELPHICLASS TRVRTFStyleSheet;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFStyleSheet : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFStyleSheetEntry* operator[](int Index) { return Items[Index]; }
	
private:
	int FLastParaIndex;
	int FLastCharIndex;
	HIDESBASE TRVRTFStyleSheetEntry* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFStyleSheetEntry* const Value);
	void __fastcall AddPara(int Number);
	
public:
	int __fastcall GetEntry(int Number);
	int __fastcall GetCharEntry(int Number);
	int __fastcall GetParaEntry(int Number);
	void __fastcall CalculateLinks(void);
	__property TRVRTFStyleSheetEntry* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFStyleSheet(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFStyleSheet(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFFontFamily : unsigned char { rtf_ff_Default, rtf_ff_Roman, rtf_ff_Swiss, rtf_ff_Modern, rtf_ff_Script, rtf_ff_Decorative, rtf_ff_Symbol, rtf_ff_BiDi };

class DELPHICLASS TRVRTFFont;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFFont : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int Number;
	System::UnicodeString Name;
	TRVRTFFontFamily Family;
	System::Uitypes::TFontCharset Charset;
	__fastcall TRVRTFFont(void);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVRTFFont(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFFontList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFFontList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVRTFFont* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVRTFFont* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVRTFFont* const Value);
	void __fastcall RemoveChasetFromNames(void);
	
public:
	int __fastcall GetFontIndex(int Number, int Default);
	HIDESBASE void __fastcall Add(int Number);
	__property TRVRTFFont* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVRTFFontList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFFontList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRTFColorList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFColorList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	System::Uitypes::TColor operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE System::Uitypes::TColor __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, const System::Uitypes::TColor Value);
	void __fastcall ResetLast(void);
	void __fastcall SetLastRed(int Value);
	void __fastcall SetLastGreen(int Value);
	void __fastcall SetLastBlue(int Value);
	void __fastcall Finalize(void);
	
public:
	HIDESBASE void __fastcall Add(void);
	__property System::Uitypes::TColor Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVRTFColorList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRTFColorList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVRTFCurrentBorderType : unsigned char { rtf_bt_Char, rtf_bt_Para, rtf_bt_Row, rtf_bt_Cell, rtf_bt_Other };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRTFReaderState : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	unsigned FDefLanguage;
	TRVRTFCharProperties* FCharProps;
	TRVRTFParaProperties* FParaProps;
	TRVRTFSectionProperties* FSectProps;
	TRVRTFRowProperties* FRowProps;
	bool FInNote;
	TRVRTFCurrentBorderType FCurrentBorderType;
	TRTFrds rds;
	TRTFris ris;
	System::UnicodeString FFieldCode;
	System::UnicodeString FBmkStartName;
	bool FFieldPictureIncluded;
	bool FFieldInserted;
	Vcl::Graphics::TGraphic* FInvalidFieldPicture;
	int DefFontNumber;
	int DefFontIndex;
	TRVRTFHeaderFooterType FHFType;
	TRVRTFRowProperties* __fastcall GetRowProps(void);
	TRVRTFBorderSide* __fastcall GetCurrentBorderSide(void);
	
public:
	__fastcall TRVRTFReaderState(void);
	__fastcall virtual ~TRVRTFReaderState(void);
	void __fastcall Assign(TRVRTFReaderState* Source);
	void __fastcall Reset(void);
	__property TRVRTFParaProperties* ParaProps = {read=FParaProps};
	__property TRVRTFCharProperties* CharProps = {read=FCharProps};
	__property TRVRTFSectionProperties* SectProps = {read=FSectProps};
	__property TRVRTFRowProperties* RowProps = {read=GetRowProps};
	__property System::UnicodeString FieldCode = {read=FFieldCode};
	__property unsigned DefLanguage = {read=FDefLanguage, nodefault};
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TRVRTFReader : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	System::Classes::TStream* Stream;
	int StreamSize;
	int InputStringIndex;
	int InputStringLength;
	Rvtypes::TRVAnsiString Text;
	Rvtypes::TRVAnsiString InputString;
	Rvtypes::TRVUnicodeString TextW;
	char LastChar;
	bool UseLastChar;
	TRVRTFNewTextEvent FOnNewText;
	TRVRTFHeaderFooterEvent FOnHeaderFooter;
	TRVRTFNoteEvent FOnNote;
	TRVRTFImportPictureEvent FOnImportPicture;
	TRVRTFNewUnicodeTextEvent FOnNewUnicodeText;
	TRVRTFTranslateKeywordEvent FOnTranslateKeyword;
	TRVRTFNewPictureEvent FOnNewPicture;
	TRVRTFNewObjectEvent FOnNewObject;
	TRVRTFNewSeqEvent FOnNewSeq;
	TRVRTFTableEvent FOnTable;
	TRVRTFStyleSheet* FStyleSheet;
	TRVRTFFontList* FFontTable;
	Rvtypes::TRVRawByteString FTmpString;
	TRVRTFColorList* FColorTable;
	TRVRTFListTable97* FListTable;
	TRVRTFListOverrideTable97* FListOverrideTable;
	TRVRTFReaderState* FRTFState;
	TRVRTFDocProperties* FDocProps;
	TRVRTFPicture* FPicture;
	TRVRTFObject* FObject;
	TRVRTFMarkerProperties* FMarkerProps;
	unsigned FDefCodePage;
	unsigned FCodePage;
	int SkipNext;
	System::Classes::TNotifyEvent FOnRequiredPageBreak;
	int CurrentNestingLevel;
	TRVRTFHighlightConvert FConvertHighlight;
	System::Classes::TNotifyEvent FOnUpdateMarker;
	int FCurPNSecLvl;
	System::UnicodeString FBasePath;
	TRVRTFProgressEvent FOnProgress;
	bool FCallProgress;
	int FProgress;
	int FProgressCounter;
	bool ShpPictInserted;
	bool ObjectInserted;
	bool FExtractMetafileBitmaps;
	System::Classes::TNotifyEvent FOnEndParsing;
	int FPixelsPerInch;
	bool FTabAsSeparateChar;
	TRVBookmarkEvent FOnBookmarkStart;
	System::Classes::TNotifyEvent FOnStyleSheet;
	bool FProcessFinalPar;
	System::StaticArray<bool, 6> FHFRead;
	int __fastcall FindKeyword(const Rvtypes::TRVAnsiString Keyword);
	Rvrtferr::TRVRTFErrorCode __fastcall AddPictureFast(const char AChar);
	
protected:
	bool ForceEvenEmptyNewLine;
	bool AfterTableRow;
	TRVRTFPosition Position;
	int cGroup;
	int cbBin;
	int lParam;
	System::Byte PicHexVal;
	bool PicHexStrt;
	bool fSkipDestIfUnk;
	System::TextFile fpIn;
	bool FEndingCell;
	Rvrtferr::TRVRTFErrorCode __fastcall EndGroupAction(TRTFrds rds, TRVRTFReaderState* SaveItem);
	Rvrtferr::TRVRTFErrorCode __fastcall TranslateKeyword(const Rvtypes::TRVAnsiString Keyword, int param, bool fParam);
	Rvrtferr::TRVRTFErrorCode __fastcall ParseSpecialProperty(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ChangeDest(TRTFIDEST idest, int Val);
	Rvrtferr::TRVRTFErrorCode __fastcall ParseSpecialKeyword(TRTFIPFN ipfn);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_SSEntry(TRTFIPROP iprop, int val, bool &Handled);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_Picture(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_Object(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_PN(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_List(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_ListLevel(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_LO(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall ApplyPropChange_LOLevel(TRTFIPROP iprop, int val);
	Rvrtferr::TRVRTFErrorCode __fastcall Parse(void);
	Rvrtferr::TRVRTFErrorCode __fastcall PushRtfState(void);
	Rvrtferr::TRVRTFErrorCode __fastcall PopRtfState(void);
	Rvrtferr::TRVRTFErrorCode __fastcall ParseRtfKeyword(void);
	Rvrtferr::TRVRTFErrorCode __fastcall ParseChar(char ch);
	void __fastcall UpdateMarker(void);
	Rvrtferr::TRVRTFErrorCode __fastcall FlushOutput(TRVRTFPosition &NextPosition);
	Rvrtferr::TRVRTFErrorCode __fastcall OutputChar(char ch, bool ACheckTableEnd, bool ACheckTable);
	Rvrtferr::TRVRTFErrorCode __fastcall InsertExternalPicture(void);
	Rvrtferr::TRVRTFErrorCode __fastcall InsertSymbol(void);
	Rvrtferr::TRVRTFErrorCode __fastcall InsertSeq(void);
	Rvrtferr::TRVRTFErrorCode __fastcall OutputWideChar(System::WideChar ch);
	void __fastcall UngetC(void);
	char __fastcall GetC(void);
	bool __fastcall IsEOF(void);
	Rvrtferr::TRVRTFErrorCode __fastcall DoNewText(TRVRTFPosition Position, TRVRTFPosition &NextPosition);
	Rvrtferr::TRVRTFErrorCode __fastcall DoNewPicture(Vcl::Graphics::TGraphic* gr, bool &Inserted);
	Rvrtferr::TRVRTFErrorCode __fastcall DoNewSeq(const System::UnicodeString SeqName, TRVRTFSeqType NumberingType, bool Reset, int StartFrom, bool &Inserted);
	Rvrtferr::TRVRTFErrorCode __fastcall DoNewObject(void);
	bool __fastcall DoTable(TRVRTFTableEventKind WhatHappens);
	void __fastcall CheckTable(bool AllowEnd);
	void __fastcall CheckTableAfterNote(void);
	void __fastcall CheckTableAfterHF(void);
	
public:
	Rvclasses::TRVList* SaveList;
	__fastcall virtual TRVRTFReader(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVRTFReader(void);
	System::UnicodeString __fastcall GetFieldCommandValue(const System::UnicodeString s);
	System::UnicodeString __fastcall GetFieldCommandValueEx(const System::UnicodeString s, int &StartIndex, int &Len, bool IgnorePrefixes);
	void __fastcall Clear(void);
	Rvrtferr::TRVRTFErrorCode __fastcall ReadFromStream(System::Classes::TStream* AStream);
	Rvrtferr::TRVRTFErrorCode __fastcall ReadFromFile(const System::UnicodeString AFileName);
	Rvtypes::TRVUnicodeString __fastcall AnsiToUnicode(const Rvtypes::TRVAnsiString s, unsigned CodePage);
	Rvtypes::TRVAnsiString __fastcall UnicodeToAnsi(const Rvtypes::TRVUnicodeString s);
	unsigned __fastcall GetCurrentCodePage(void);
	__property TRVRTFStyleSheet* StyleSheet = {read=FStyleSheet};
	__property TRVRTFFontList* FontTable = {read=FFontTable};
	__property TRVRTFColorList* ColorTable = {read=FColorTable};
	__property TRVRTFListTable97* ListTable = {read=FListTable};
	__property TRVRTFListOverrideTable97* ListOverrideTable = {read=FListOverrideTable};
	__property TRVRTFReaderState* RTFState = {read=FRTFState};
	__property TRVRTFDocProperties* DocProps = {read=FDocProps};
	__property System::UnicodeString BasePath = {read=FBasePath, write=FBasePath};
	__property bool TabAsSeparateChar = {read=FTabAsSeparateChar, write=FTabAsSeparateChar, nodefault};
	__property unsigned CodePage = {read=FCodePage, nodefault};
	__property bool EndingCell = {read=FEndingCell, nodefault};
	
__published:
	__property TRVRTFNewTextEvent OnNewText = {read=FOnNewText, write=FOnNewText};
	__property TRVRTFNewUnicodeTextEvent OnNewUnicodeText = {read=FOnNewUnicodeText, write=FOnNewUnicodeText};
	__property TRVRTFNewPictureEvent OnNewPicture = {read=FOnNewPicture, write=FOnNewPicture};
	__property TRVRTFNewObjectEvent OnNewObject = {read=FOnNewObject, write=FOnNewObject};
	__property System::Classes::TNotifyEvent OnUpdateMarker = {read=FOnUpdateMarker, write=FOnUpdateMarker};
	__property TRVRTFTableEvent OnTable = {read=FOnTable, write=FOnTable};
	__property System::Classes::TNotifyEvent OnRequiredPageBreak = {read=FOnRequiredPageBreak, write=FOnRequiredPageBreak};
	__property TRVRTFHeaderFooterEvent OnHeaderFooter = {read=FOnHeaderFooter, write=FOnHeaderFooter};
	__property TRVRTFNoteEvent OnNote = {read=FOnNote, write=FOnNote};
	__property TRVRTFImportPictureEvent OnImportPicture = {read=FOnImportPicture, write=FOnImportPicture};
	__property TRVRTFTranslateKeywordEvent OnTranslateKeyword = {read=FOnTranslateKeyword, write=FOnTranslateKeyword};
	__property unsigned DefCodePage = {read=FDefCodePage, write=FDefCodePage, default=0};
	__property TRVRTFHighlightConvert ConvertHighlight = {read=FConvertHighlight, write=FConvertHighlight, default=1};
	__property TRVRTFProgressEvent OnProgress = {read=FOnProgress, write=FOnProgress};
	__property bool ExtractMetafileBitmaps = {read=FExtractMetafileBitmaps, write=FExtractMetafileBitmaps, default=1};
	__property int PixelsPerInch = {read=FPixelsPerInch, write=FPixelsPerInch, default=0};
	__property System::Classes::TNotifyEvent OnEndParsing = {read=FOnEndParsing, write=FOnEndParsing};
	__property TRVBookmarkEvent OnBookmarkStart = {read=FOnBookmarkStart, write=FOnBookmarkStart};
	__property TRVRTFNewSeqEvent OnNewSeq = {read=FOnNewSeq, write=FOnNewSeq};
	__property System::Classes::TNotifyEvent OnStyleSheet = {read=FOnStyleSheet, write=FOnStyleSheet};
	__property bool ProcessFinalPar = {read=FProcessFinalPar, write=FProcessFinalPar, default=0};
};


//-- var, const, procedure ---------------------------------------------------
static const short RV_UNDEFINED_TAB_POS = short(-1000);
extern DELPHI_PACKAGE TRVRTFCharPropertiesClass RTFCharPropertiesClass;
}	/* namespace Rvrtf */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVRTF)
using namespace Rvrtf;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvrtfHPP
