unit RVZip;

interface

{$I RV_Defs.inc}

{$IFNDEF RVDONOTUSEDOCX}
uses Windows, Classes, SysUtils, RVTypes, ZLib, RVClasses;

type
  TRVZipCommonFileHeader = packed record
    VersionNeededToExtract: Word;
    GeneralPurposeBitFlag: Word;
    CompressionMethod: Word;
    LastModFileTimeDate: Integer;
    Crc32: Cardinal;
    CompressedSize: Cardinal;
    UncompressedSize: Cardinal;
    FileNameLength: Word;
    ExtraFieldLength: Word;
  end;

  TRVZipLocalFileHeader = packed record
    LocalFileHeaderSignature: Cardinal;
    CommonFileHeader: TRVZipCommonFileHeader;
    // file name, extra field, compressed data (variable size)
  end;

  TRVZipFileHeader = packed record
    CentralFileHeaderSignature: Cardinal;
    VersionMadeBy: Word;
    CommonFileHeader: TRVZipCommonFileHeader;
    FileCommentLength: Word;
    DiskNumberStart: Word;
    InternalFileAttributes: Word;
    ExternalFileAttributes: Cardinal;
    RelativeOffsetOfLocalHeader: Cardinal;
    // file name, extra field, file comment (variable size)
  end;

  TRVZipEndOfCentralDir = packed record
    EndOfCentralDirSignature: Cardinal;
    NumberOfThisDisk: Word;
    NumberOfTheDiskWTSoTCD: Word;
    TotalNumberOfEntriesOnThisDisk: Word;
    TotalNumberOfEntries: Word;
    SizeOfTheCentralDirectory: Cardinal;
    OffsetOfStartOfCentralDirectory: Cardinal;
    ZipFileCommentLength: Word;
    // zip file comment (variable size)
  end;

  TRVZipPacker = class
  private
    FZipStream: TStream;
    Failed: Boolean;
    FCentralDirectory: array of TRVZipFileHeader;
    FFileNames: array of TRVAnsiString;
    FZipFileName: String;
  public
    function CreateZipFile(const FileName: String): Boolean;
    procedure CreateZipInStream(Stream: TStream);
    function Close: Boolean;
    function Add(const FileName: TRVAnsiString;
      Content: PRVAnsiChar; Size: Cardinal): Boolean;
  end;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSEDOCX}

function GetCrc32(buf : PRVAnsiChar; len : Cardinal) : Cardinal;
const Crc32Table : array [0..255] of Cardinal = (
    $000000000, $077073096, $0EE0E612C, $0990951BA,
    $0076DC419, $0706AF48F, $0E963A535, $09E6495A3,
    $00EDB8832, $079DCB8A4, $0E0D5E91E, $097D2D988,
    $009B64C2B, $07EB17CBD, $0E7B82D07, $090BF1D91,
    $01DB71064, $06AB020F2, $0F3B97148, $084BE41DE,
    $01ADAD47D, $06DDDE4EB, $0F4D4B551, $083D385C7,
    $0136C9856, $0646BA8C0, $0FD62F97A, $08A65C9EC,
    $014015C4F, $063066CD9, $0FA0F3D63, $08D080DF5,
    $03B6E20C8, $04C69105E, $0D56041E4, $0A2677172,
    $03C03E4D1, $04B04D447, $0D20D85FD, $0A50AB56B,
    $035B5A8FA, $042B2986C, $0DBBBC9D6, $0ACBCF940,
    $032D86CE3, $045DF5C75, $0DCD60DCF, $0ABD13D59,
    $026D930AC, $051DE003A, $0C8D75180, $0BFD06116,
    $021B4F4B5, $056B3C423, $0CFBA9599, $0B8BDA50F,
    $02802B89E, $05F058808, $0C60CD9B2, $0B10BE924,
    $02F6F7C87, $058684C11, $0C1611DAB, $0B6662D3D,
    $076DC4190, $001DB7106, $098D220BC, $0EFD5102A,
    $071B18589, $006B6B51F, $09FBFE4A5, $0E8B8D433,
    $07807C9A2, $00F00F934, $09609A88E, $0E10E9818,
    $07F6A0DBB, $0086D3D2D, $091646C97, $0E6635C01,
    $06B6B51F4, $01C6C6162, $0856530D8, $0F262004E,
    $06C0695ED, $01B01A57B, $08208F4C1, $0F50FC457,
    $065B0D9C6, $012B7E950, $08BBEB8EA, $0FCB9887C,
    $062DD1DDF, $015DA2D49, $08CD37CF3, $0FBD44C65,
    $04DB26158, $03AB551CE, $0A3BC0074, $0D4BB30E2,
    $04ADFA541, $03DD895D7, $0A4D1C46D, $0D3D6F4FB,
    $04369E96A, $0346ED9FC, $0AD678846, $0DA60B8D0,
    $044042D73, $033031DE5, $0AA0A4C5F, $0DD0D7CC9,
    $05005713C, $0270241AA, $0BE0B1010, $0C90C2086,
    $05768B525, $0206F85B3, $0B966D409, $0CE61E49F,
    $05EDEF90E, $029D9C998, $0B0D09822, $0C7D7A8B4,
    $059B33D17, $02EB40D81, $0B7BD5C3B, $0C0BA6CAD,
    $0EDB88320, $09ABFB3B6, $003B6E20C, $074B1D29A,
    $0EAD54739, $09DD277AF, $004DB2615, $073DC1683,
    $0E3630B12, $094643B84, $00D6D6A3E, $07A6A5AA8,
    $0E40ECF0B, $09309FF9D, $00A00AE27, $07D079EB1,
    $0F00F9344, $08708A3D2, $01E01F268, $06906C2FE,
    $0F762575D, $0806567CB, $0196C3671, $06E6B06E7,
    $0FED41B76, $089D32BE0, $010DA7A5A, $067DD4ACC,
    $0F9B9DF6F, $08EBEEFF9, $017B7BE43, $060B08ED5,
    $0D6D6A3E8, $0A1D1937E, $038D8C2C4, $04FDFF252,
    $0D1BB67F1, $0A6BC5767, $03FB506DD, $048B2364B,
    $0D80D2BDA, $0AF0A1B4C, $036034AF6, $041047A60,
    $0DF60EFC3, $0A867DF55, $0316E8EEF, $04669BE79,
    $0CB61B38C, $0BC66831A, $0256FD2A0, $05268E236,
    $0CC0C7795, $0BB0B4703, $0220216B9, $05505262F,
    $0C5BA3BBE, $0B2BD0B28, $02BB45A92, $05CB36A04,
    $0C2D7FFA7, $0B5D0CF31, $02CD99E8B, $05BDEAE1D,
    $09B64C2B0, $0EC63F226, $0756AA39C, $0026D930A,
    $09C0906A9, $0EB0E363F, $072076785, $005005713,
    $095BF4A82, $0E2B87A14, $07BB12BAE, $00CB61B38,
    $092D28E9B, $0E5D5BE0D, $07CDCEFB7, $00BDBDF21,
    $086D3D2D4, $0F1D4E242, $068DDB3F8, $01FDA836E,
    $081BE16CD, $0F6B9265B, $06FB077E1, $018B74777,
    $088085AE6, $0FF0F6A70, $066063BCA, $011010B5C,
    $08F659EFF, $0F862AE69, $0616BFFD3, $0166CCF45,
    $0A00AE278, $0D70DD2EE, $04E048354, $03903B3C2,
    $0A7672661, $0D06016F7, $04969474D, $03E6E77DB,
    $0AED16A4A, $0D9D65ADC, $040DF0B66, $037D83BF0,
    $0A9BCAE53, $0DEBB9EC5, $047B2CF7F, $030B5FFE9,
    $0BDBDF21C, $0CABAC28A, $053B39330, $024B4A3A6,
    $0BAD03605, $0CDD70693, $054DE5729, $023D967BF,
    $0B3667A2E, $0C4614AB8, $05D681B02, $02A6F2B94,
    $0B40BBE37, $0C30C8EA1, $05A05DF1B, $02D02EF8D
);
begin
  Result := $FFFFFFFF;
  while len > 0 do begin
    Result := (Result shr 8) xor Crc32Table[(Result xor byte(buf^)) and $0FF];
    inc(buf);
    dec(len);
  end;
  Result := Result xor $0FFFFFFFF;
end;

function TRVZipPacker.Add(const FileName: TRVAnsiString;
  Content: PRVAnsiChar; Size: Cardinal): Boolean;
var LocalFileHeader: TRVZipLocalFileHeader;
  Compressor: TCompressionStream;
  Pos: Cardinal;
  DestStream: TRVMemoryStream;
begin
  Result := False;
  if Failed then
    exit;
  try
    Pos := FZipStream.Position;
    FillChar(LocalFileHeader, sizeof(LocalFileHeader), 0);
    LocalFileHeader.LocalFileHeaderSignature := $04034B50;
    with LocalFileHeader.CommonFileHeader do begin
      VersionNeededToExtract := 20;
      CompressionMethod := 8;
      LastModFileTimeDate := DateTimeToFileDate(Now);
      UncompressedSize := Size;
      FileNameLength := Length(Filename);
    end;

    DestStream := TRVMemoryStream.Create;
    try
      Compressor := TCompressionStream.Create(clDefault, DestStream);
      try
        Compressor.Write(Content^, Size);
      finally
        Compressor.Free;
      end;
      // we need to skip 2 bytes before data and 4 bytes after
      with LocalFileHeader.CommonFileHeader do begin
        CompressedSize := DestStream.Size-6;
        //Crc32 := GetCrc32(PRVAnsiChar(DestStream.Memory)+2, CompressedSize);
        Crc32 := GetCrc32(Content, Size);
      end;
      FZipStream.WriteBuffer(LocalFileHeader, sizeof(LocalFileHeader));
      FZipStream.WriteBuffer(PRVAnsiChar(FileName)^, Length(FileName));
      FZipStream.WriteBuffer((PRVAnsiChar(DestStream.Memory)+2)^,
        DestStream.Size-6);
    finally
      DestStream.Free;
    end;
    SetLength(FCentralDirectory, Length(FCentralDirectory)+1);
    SetLength(FFileNames, Length(FFileNames)+1);
    FillChar(FCentralDirectory[High(FCentralDirectory)], sizeof(TRVZipFileHeader), 0);
    FFileNames[High(FFileNames)] := FileName;
    with FCentralDirectory[High(FCentralDirectory)] do begin
      CentralFileHeaderSignature := $02014B50;
      RelativeOffsetOfLocalHeader := Pos;
      VersionMadeBy := 20;
      CommonFileHeader := LocalFileHeader.CommonFileHeader;
    end;
    Result := True;
  except
    Failed := True;
  end;
end;

function TRVZipPacker.Close: Boolean;
var EndOfCentralDir: TRVZipEndOfCentralDir;
    i, Pos: Integer;
begin
  Result := False;
  if FZipStream=nil then
    exit;
  if Failed then begin
    if FZipFileName<>'' then begin
      FZipStream.Free;
      DeleteFile(FZipFileName);
    end;
    exit;
  end;
  try
    Pos := FZipStream.Position;
    for i := 0 to High(FCentralDirectory) do begin
      FZipStream.WriteBuffer(FCentralDirectory[i], sizeof(TRVZipFileHeader));
      FZipStream.WriteBuffer(PRVAnsiChar(FFileNames[i])^, Length(FFileNames[i]));
    end;
    FillChar(EndOfCentralDir, sizeof(EndOfCentralDir), 0);
    with EndOfCentralDir do begin
      OffsetOfStartOfCentralDirectory := Pos;
      EndOfCentralDirSignature := $06054B50;
      SizeOfTheCentralDirectory := FZipStream.Position-Pos;
      TotalNumberOfEntries := Length(FCentralDirectory);
      TotalNumberOfEntriesOnThisDisk := TotalNumberOfEntries;
    end;
    FZipStream.WriteBuffer(EndOfCentralDir, sizeof(EndOfCentralDir));
    Result := True;
  finally
    if FZipFileName<>'' then
      FZipStream.Free;
  end;
end;

function TRVZipPacker.CreateZipFile(const FileName: String): Boolean;
begin
  Result := True;
  try
    FZipStream := TFileStream.Create(FileName, fmCreate);
    FZipFileName := FileName;
  except
    FZipStream := nil;
    Result := False;
  end;
end;

procedure TRVZipPacker.CreateZipinStream(Stream: TStream);
begin
  FZipStream := Stream;
end;

{$ENDIF}

end.
