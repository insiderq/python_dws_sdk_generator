﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVSeqItem.pas' rev: 27.00 (Windows)

#ifndef RvseqitemHPP
#define RvseqitemHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVLabelItem.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvseqitem
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVSeqItemInfo;
class DELPHICLASS TRVSeqList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSeqItemInfo : public Rvlabelitem::TRVLabelItemInfo
{
	typedef Rvlabelitem::TRVLabelItemInfo inherited;
	
private:
	Rvstyle::TRVSeqType FNumberType;
	
protected:
	int FCachedIndexInList;
	virtual Rvstyle::TRVSeqType __fastcall GetNumberType(void);
	virtual void __fastcall SetNumberType(const Rvstyle::TRVSeqType Value);
	
public:
	int Counter;
	int StartFrom;
	bool Reset;
	System::UnicodeString SeqName;
	System::UnicodeString FormatString;
	__fastcall TRVSeqItemInfo(System::Classes::TPersistent* RVData, const System::UnicodeString ASeqName, Rvstyle::TRVSeqType ANumberType, int ATextStyleNo, int AStartFrom, bool AReset);
	__fastcall virtual TRVSeqItemInfo(System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall CalcDisplayString(TRVSeqList* List);
	int __fastcall GetIndexInList(System::Classes::TList* List);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall Assign(Rvitem::TCustomRVItemInfo* Source);
	System::UnicodeString __fastcall GetDisplayString(TRVSeqList* List, int ACounter);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLBeforeRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveOOXMLAfterRun(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, Rvitem::TRVFReadMode &ReadMode, Rvitem::TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC bool __fastcall SetExtraIntPropertyEx(int Prop, int Value);
	DYNAMIC bool __fastcall GetExtraIntPropertyEx(int Prop, int &Value);
	DYNAMIC bool __fastcall SetExtraStrPropertyEx(int Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrPropertyEx(int Prop, System::UnicodeString &Value);
	DYNAMIC Rvitem::TRVReformatType __fastcall GetExtraIntPropertyReformat(int Prop);
	DYNAMIC Rvitem::TRVReformatType __fastcall GetExtraStrPropertyReformat(int Prop);
	__property Rvstyle::TRVSeqType NumberType = {read=GetNumberType, write=SetNumberType, nodefault};
public:
	/* TCustomRVItemInfo.Destroy */ inline __fastcall virtual ~TRVSeqItemInfo(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSeqList : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	int __fastcall InsertAfter(TRVSeqItemInfo* InsertMe, TRVSeqItemInfo* AfterMe);
	void __fastcall RecalcCounters(int StartFrom, Rvstyle::TRVStyle* RVStyle, bool ForAllNames);
	void __fastcall RecalcDisplayStrings(Rvstyle::TRVStyle* RVStyle);
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVSeqList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVSeqList(void) : System::Classes::TList() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const short rvsSequence = short(-202);
static const System::Word rveipcSeqStartFrom = System::Word(0x12c);
static const System::Word rveipcSeqReset = System::Word(0x12d);
static const System::Word rveipcSeqNumberType = System::Word(0x12e);
static const System::Word rvespcSeqName = System::Word(0x12c);
static const System::Word rvespcSeqFormatString = System::Word(0x12d);
}	/* namespace Rvseqitem */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSEQITEM)
using namespace Rvseqitem;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvseqitemHPP
