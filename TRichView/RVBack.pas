
{*******************************************************}
{                                                       }
{       RichView                                        }
{       TRVBackground: background for RichView,         }
{       table, or table cell.                           }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVBack;

{$I RV_Defs.inc}

interface
uses SysUtils, Windows, Classes, Graphics,
     {$IFNDEF RVDONOTUSEJPEGIMAGE}
     Jpeg,
     {$ENDIF}
     RVStyle, RVScroll, RVFuncs, RVGrIn;

type
  TRVBackground = class
    private
      ImageCopy: TGraphic;
      FHasThumbnail: Boolean;
      function GetBitmap: TBitmap;
      function GetThumbnail: TBitmap;
      function GetItemBackStyle: TRVItemBackgroundStyle;
      procedure SetItemBackStyle(const Value: TRVItemBackgroundStyle);
    public
      Style: TBackgroundStyle;
      Image: TGraphic;
      constructor Create(CreateBitmap: Boolean);
      destructor Destroy; override;
      function ScrollRequiresFullRedraw: Boolean;
      procedure UpdatePaletted(PaletteAction: TRVPaletteAction;Palette: HPALETTE; LogPalette: PLogPalette);
      procedure FreeThumbnail;
      procedure UpdateThumbnail(RVData, BackgroundOwner: TObject; Width, Height: Integer);
      procedure Draw(Canvas: TCanvas; Rect: TRect;
        HOffs, VOffs, Left, Top, Width,Height: Integer; Color: TColor;
        Clipping, PrintSimulation: Boolean; GraphicInterface: TRVGraphicInterface;
        RVData, BackgroundOwner: TObject; AllowThumbnails: Boolean);
      procedure Print(Canvas: TCanvas; ARect, AFullRect: TRect; const sad: TRVScreenAndDevice;
        Color: TColor; Preview: Boolean; LogPalette: PLogPalette;
        PrintingRVData: TPersistent; ItemBackgroundLayer: Integer;
        GraphicInterface: TRVGraphicInterface; PageNo: Integer);
      function Empty: Boolean;
      function EmptyImage: Boolean;
      function Visible: Boolean;
      function IsSemitransparent: Boolean;
      procedure FreeImage;
      procedure AssignImage(AImage: TGraphic; ARVData: TPersistent; Copy: Boolean;
        BackgroundOwner: TObject);
      property Bitmap: TBitmap read GetBitmap;
      property Thumbnail: TBitmap read GetThumbnail;
      property ItemBackStyle: TRVItemBackgroundStyle read GetItemBackStyle write SetItemBackStyle;
      property HasThumbnail: Boolean read FHasThumbnail;
  end;

implementation

uses CRVData, PtRVData, RVGrHandler, RVThumbMaker;

{============================== TRVBackground =================================}
constructor TRVBackground.Create(CreateBitmap: Boolean);
begin
  inherited Create;
  if CreateBitmap then
    Image := TBitmap.Create;
  Style  := bsNoBitmap;
end;
{------------------------------------------------------------------------------}
destructor TRVBackground.Destroy;
begin
  Image.Free;
  ImageCopy.Free;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
function TRVBackground.ScrollRequiresFullRedraw: Boolean;
begin
  if Image.Empty then
    Result := False
  else begin
    case Style of
      bsNoBitmap, bsTiledAndScrolled:
        Result := False;
      //bsStretched, bsTiled, bsCentered, corners:
      else
        Result := True;
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.UpdatePaletted(PaletteAction: TRVPaletteAction;
                             Palette: HPALETTE; LogPalette: PLogPalette);
begin
  RVFreeAndNil(ImageCopy);
  FHasThumbnail := False;
  if Image=nil then
    exit;
  case PaletteAction of
    rvpaAssignPalette:
      if (LogPalette<>nil) and not Image.Empty then
        RVGraphicHandler.SetPalette(Image, LogPalette);
    rvpaCreateCopies,rvpaCreateCopiesEx:
      if (LogPalette<>nil) and not Image.Empty then begin
        if (PaletteAction=rvpaCreateCopiesEx) and
          (RVGraphicHandler.GetGraphicType(Image)=rvgtJPEG) then
          ImageCopy := TBitmap.Create
        else
          ImageCopy := RVGraphicHandler.CreateGraphic(TGraphicClass(Image.ClassType));
        ImageCopy.Assign(Image);
        RVGraphicHandler.SetPalette(ImageCopy, LogPalette);
        if ImageCopy is TBitmap then
          TBitmap(ImageCopy).IgnorePalette := True;
      end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.FreeThumbnail;
begin
  RVFreeAndNil(ImageCopy);
  FHasThumbnail := False;
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.Print(Canvas: TCanvas; ARect, AFullRect: TRect;
  const sad: TRVScreenAndDevice; Color: TColor; Preview: Boolean; LogPalette: PLogPalette;
  PrintingRVData: TPersistent; ItemBackgroundLayer: Integer;
  GraphicInterface: TRVGraphicInterface; PageNo: Integer);
var i, j: Integer;
    OffsRect: TRect;
    bmp: TBitmap;
    gr: TGraphic;
    DCIdx, BmpWidth, BmpHeight, DX, DY: Integer;
    //pt: TPoint;
    RVData: TCustomPrintableRVData;

    procedure DrawBitmapAt(Left, Top: Integer);
    var bmp2: TBitmap;
        w, h, x, y: Integer;
    begin
      if RVGraphicHandler.IsTransparent(gr) and (Color=clNone) then begin
        RVData.DrawBackToBitmap( RV_XToScreen(Left+DX, sad), RV_YToScreen(Top+DY, sad),
          bmp, sad, ItemBackgroundLayer, True, True, True, PageNo);
        try
          GraphicInterface.DrawGraphic(bmp.Canvas, 0, 0, gr);
        except
        end;
      end;
      bmp2 := bmp;
      try
        if (Left<ARect.Left) or (Top<ARect.Top) or
           (Left+RV_XToDevice(bmp.Width, sad)>ARect.Right) or
           (Top+RV_YToDevice(bmp.Height, sad)>ARect.Bottom) then begin
          x := 0;
          y := 0;
          bmp2 := TBitmap.Create;
          w := bmp.Width;
          if Left<ARect.Left then begin
            dec(w, RV_XToScreen(ARect.Left-Left, sad));
            dec(x, RV_XToScreen(ARect.Left-Left, sad));
          end;
          if RV_XToScreen(Left, sad)+bmp.Width>RV_XToScreen(ARect.Right,sad) then
            dec(w, RV_XToScreen(Left, sad)+bmp.Width-RV_XToScreen(ARect.Right,sad));
          h := bmp.Height;
          if Top<RV_YToScreen(ARect.Top, sad) then begin
            dec(h, RV_YToScreen(ARect.Top-Top, sad));
            dec(y, RV_YToScreen(ARect.Top-Top, sad));
          end;
          if RV_YToScreen(Top, sad)+bmp.Height>RV_YToScreen(ARect.Bottom, sad) then
            dec(h, RV_YToScreen(Top, sad)+bmp.Height-RV_YToScreen(ARect.Bottom, sad));
          if (w>0) and (h>0) then begin
            bmp2.Width := w;
            bmp2.Height := h;
            try
              GraphicInterface.DrawGraphic(bmp2.Canvas, x, y, bmp);
            except
            end;
            if Left<ARect.Left then
              Left := ARect.Left;
            if Top<ARect.Top then
              Top := ARect.Top;
            end
          else begin
            RVFreeAndNil(bmp2);
          end;
        end;
        if bmp2<>nil then
          RV_PictureToDevice(Canvas, Left, Top, bmp2.Width, bmp2.Height, @sad,
            bmp2, Preview, GraphicInterface);
      finally
        if bmp2<>bmp then
          bmp2.Free;
      end;
    end;

    procedure DrawBitmapAtEx(Left, Top, Width, Height: Integer);
    begin
      if RVGraphicHandler.IsTransparent(gr) and (Color=clNone) then begin
        bmp.Width := RV_XToScreen(Width, sad);
        bmp.Height := RV_YToScreen(Height, sad);
        RVData.DrawBackToBitmap(
          RV_XToScreen(Left+DX, sad), RV_YToScreen(Top+DY, sad),
          bmp, sad, ItemBackgroundLayer, True, True, True, PageNo);
        try
          GraphicInterface.StretchDrawGraphic(bmp.Canvas,
            Rect(0,0,bmp.Width,bmp.Height),gr);
        except
        end;
        RV_PictureToDevice(Canvas, Left, Top, bmp.Width, bmp.Height, @sad, bmp,
          Preview, GraphicInterface);
        end
      else
        RV_PictureToDevice(Canvas, Left, Top,
          RV_XToScreen(Width, sad), RV_YToScreen(Height, sad), @sad, bmp, Preview,
          GraphicInterface);
    end;

begin
  DCIdx := GraphicInterface.SaveCanvasState(Canvas);
  DX := ARect.Left-AFullRect.Left;
  DY := ARect.Top -AFullRect.Top;
  //GraphicInterface.GetWindowOrgEx(Canvas, pt);
  //GraphicInterface.SetWindowOrgEx(Canvas, pt.x-ARect.Left, pt.y-ARect.Top, @pt);
  GraphicInterface.MoveWindowOrgAdvanced(Canvas, ARect.Left, ARect.Top);
  OffsetRect(ARect, -ARect.Left, -ARect.Top);
  try
    if (ImageCopy=nil) or HasThumbnail then
      gr := Image
    else
      gr := ImageCopy;
    RVData := PrintingRVData as TCustomPrintableRVData;
    if (Color<>clNone) and
      ((gr=nil) or gr.Empty or (Style in [bsNoBitmap, bsCentered, bsTopLeft,
        bsTopRight, bsBottomLeft, bsBottomRight])) then begin
       OffsRect := ARect;
       OffsetRect(OffsRect, -ARect.Left, -ARect.Top);
       GraphicInterface.FillColorRect(Canvas, OffsRect, Color);
    end;
    if (gr<>nil) and not gr.Empty then begin
      bmp := TBitmap.Create;
      try
        bmp.Width := gr.Width;
        bmp.Height := gr.Height;
        BmpWidth  := RV_XToDevice(bmp.Width, sad);
        BmpHeight := RV_YToDevice(bmp.Height, sad);
        if LogPalette<>nil then
          bmp.Palette := CreatePalette(LogPalette^);
        if Color<>clNone then begin
          bmp.Canvas.Brush.Color := Color;
          GraphicInterface.FillRect(bmp.Canvas, Rect(0,0,bmp.Width,bmp.Height));
        end;
        if not RVGraphicHandler.IsTransparent(gr) or (Color<>clNone) then
          try
            GraphicInterface.DrawGraphic(bmp.Canvas, 0,0, gr);
          except
          end;
        case Style of
          bsCentered:
            DrawBitmapAt(-ARect.Left+((AFullRect.Right-AFullRect.Left)-bmpWidth) div 2,
                         -ARect.Top+((AFullRect.Bottom-AFullRect.Top)-bmpHeight) div 2);
          bsTopLeft:
            DrawBitmapAt(-ARect.Left+AFullRect.Left, -ARect.Top+AFullRect.Top);
          bsTopRight:
            DrawBitmapAt(-ARect.Left+AFullRect.Right-bmpWidth, -ARect.Top+AFullRect.Top);
          bsBottomLeft:
            DrawBitmapAt(-ARect.Left+AFullRect.Left, -ARect.Top+AFullRect.Bottom-bmpHeight);
          bsBottomRight:
            DrawBitmapAt(-ARect.Left+AFullRect.Right-bmpWidth,
                         -ARect.Top+AFullRect.Bottom-bmpHeight);
          bsTiled, bsTiledAndScrolled:
            for i:= ARect.Top div bmpHeight to ARect.Bottom div bmpHeight do
              for j:= ARect.Left div bmpWidth to ARect.Right div bmpWidth do
                DrawBitmapAt(j*bmpWidth-ARect.Left,i*bmpHeight-ARect.Top);
          bsStretched:
            DrawBitmapAtEx(-ARect.Left, -ARect.Top,
              AFullRect.Right-AFullRect.Left, AFullRect.Bottom-AFullRect.Top);
        end;
      finally
        bmp.Free;
      end;
    end;
  finally
    //GraphicInterface.SetWindowOrgEx(Canvas, pt.x, pt.y, nil);
    GraphicInterface.RestoreCanvasState(Canvas, DCIdx);
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.UpdateThumbnail(RVData, BackgroundOwner: TObject; Width, Height: Integer);
var wScaled, hScaled: Integer;
  {...............................................}
  function NeedsNewThumbnail: Boolean;
  var TnWidth, TnHeight: Integer;
  begin
    Result := TCustomRVData(RVData).GetThumbnailCache.MaxCount<>0;
    if not Result then
      exit;
    Result := ImageCopy=nil;
    if Result then
      exit;
    Result := FHasThumbnail;
    if not Result then
      exit;
    Result := (wScaled<>Image.Width) or (hScaled<>Image.Height);
    if not Result then
      exit;
    TnWidth := wScaled;
    TnHeight := hScaled;
    RVThumbnailMaker.AdjustThumbnailSize(TnWidth, TnHeight);
    Result := (TnWidth<>ImageCopy.Width) or (TnHeight<>ImageCopy.Height);
  end;
  {...............................................}
var Ifc: IRVScaleRichViewInterface;
    Ratio: Single;
begin
  if (Image<>nil) and (Style<>bsNoBitmap) and
    (RVThumbnailMaker.IsAllowedFor(TGraphicClass(Image.ClassType))) then begin
    if RVData<>nil then
      Ifc := TCustomRVData(RVData).GetScaleRichViewInterface
    else
      Ifc := nil;
   if Style=bsStretched then begin
     wScaled := Width;
     hScaled := Height;
     end
   else begin
     wScaled := Image.Width;
     hScaled := Image.Height;
   end;
   if Ifc<>nil then begin
     Ratio := Ifc.GetDrawnPageZoomPercent;
     wScaled := Round(wScaled*Ratio);
     hScaled := Round(hScaled*Ratio);
   end;
   if NeedsNewThumbnail then begin
     ImageCopy.Free;
     ImageCopy := RVThumbnailMaker.MakeThumbnail(Image, wScaled, hScaled);
     FHasThumbnail := ImageCopy<>nil;
   end;
  end;
  if (ImageCopy<>nil) and FHasThumbnail and
    ((Image=nil) or (Style=bsNoBitmap) or ((wScaled=Image.Width) and (hScaled=Image.Height))) then begin
    RVFreeAndNil(ImageCopy);
    FHasThumbnail := False;
    if (RVData<>nil) and (BackgroundOwner<>nil) then
      TCustomRVData(RVData).GetThumbnailCache.RemoveThumbnail(BackgroundOwner);
  end;
  if (ImageCopy<>nil) and FHasThumbnail and (RVData<>nil) and (BackgroundOwner<>nil) then
    TCustomRVData(RVData).GetThumbnailCache.AddThumbnail(BackgroundOwner);
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.Draw(Canvas: TCanvas; Rect: TRect;
  HOffs, VOffs, Left, Top, Width, Height: Integer;
  Color: TColor; Clipping, PrintSimulation: Boolean;
  GraphicInterface: TRVGraphicInterface; RVData, BackgroundOwner: TObject;
  AllowThumbnails: Boolean);
var i, j, wOrig, hOrig: Integer;
    OffsRect: TRect;
    bmp: TBitmap;
    gr: TGraphic;
    DCIdx: Integer;
    ScaledBitmap: Boolean;
    {...............................................................}
    procedure DrawGraphic(X, Y, Width, Height: Integer; gr: TGraphic);
    var bmp: TBitmap;
    begin
      if not PrintSimulation then
        try
          if Width<0 then
            GraphicInterface.DrawGraphic(Canvas, X, Y, gr)
          else
            GraphicInterface.StretchDrawGraphic(Canvas, Bounds(X, Y, Width, Height), gr)
        except
        end
      else begin
        if gr is TBitmap then
          bmp := TBitmap(gr)
        else begin
          bmp := TBitmap.Create;
          try
            bmp.Assign(gr);
          except
            bmp.Width := gr.Width;
            bmp.Height := gr.Height;
            try
              GraphicInterface.DrawGraphic(bmp.Canvas, X, Y, gr);
            except
            end;
          end;
        end;
        try
          RV_PictureToDevice(Canvas, X, Y, Width, Height, nil, bmp, False,
            GraphicInterface);
        finally
          if not (gr is TBitmap) then
            bmp.Free;
        end;
      end;
    end;
  {...............................................}
  procedure DrawBitmap(bmp: TBitmap; x, y: Integer);
  begin
    if ScaledBitmap then
      GraphicInterface.StretchDrawBitmap(Canvas, Bounds(x, y, wOrig, hOrig), bmp)
    else
      GraphicInterface.DrawBitmap(Canvas, x, y, bmp);
  end;
  {...............................................}
begin
 if (Image<>nil) and (Style<>bsNoBitmap) then begin
   wOrig := Image.Width;
   hOrig := Image.Height;
   end
 else begin
   wOrig := 0;
   hOrig := 0;
 end;
 if AllowThumbnails then begin
   UpdateThumbnail(RVData, BackgroundOwner, Width, Height);
   if ImageCopy=nil then
     gr := Image
   else
     gr := ImageCopy;
   end
 else
   gr := Image;
 if Clipping then begin
   DCIdx := GraphicInterface.SaveCanvasState(Canvas);
   with Rect do
     GraphicInterface.IntersectClipRect(Canvas, Left, Top, Right, Bottom);
   //GraphicInterface.SetWindowOrgEx(Canvas, -Rect.Left, -Rect.Top, @pt);
   GraphicInterface.MoveWindowOrgAdvanced(Canvas, Rect.Left, Rect.Top);
   end
 else
   DCIdx := 0;
 try
   OffsetRect(Rect, -Left, -Top);
   if (Color<>clNone) and
     ((gr=nil) or gr.Empty or RVGraphicHandler.IsTransparent(gr) or
     (Style in [bsNoBitmap, bsCentered, bsTopLeft, bsTopRight,
      bsBottomLeft, bsBottomRight])) then begin
     OffsRect := Rect;
     OffsetRect(OffsRect, -Rect.Left, -Rect.Top);
     GraphicInterface.FillColorRect(Canvas, OffsRect, Color);
   end;
   if (gr<>nil) and not gr.Empty then begin
     if ((Style<>bsStretched) or FHasThumbnail) and (gr is TBitmap) and not PrintSimulation then begin
       bmp := TBitmap(gr);
       ScaledBitmap := (bmp.Width<>wOrig) or (bmp.Height<>hOrig);
       case Style of
         bsCentered:
           DrawBitmap(bmp, -Rect.Left+(Width-wOrig) div 2, -Rect.Top+(Height-hOrig) div 2);
         bsTopLeft:
           DrawBitmap(bmp, -Rect.Left, -Rect.Top);
         bsTopRight:
           DrawBitmap(bmp, -Rect.Left+(Width-wOrig), -Rect.Top);
         bsBottomLeft:
           DrawBitmap(bmp, -Rect.Left, -Rect.Top+(Height-hOrig));
         bsBottomRight:
           DrawBitmap(bmp, -Rect.Left+(Width-wOrig), -Rect.Top+(Height-hOrig));
         bsTiled:
          for i:= Rect.Top div hOrig to Rect.Bottom div hOrig do
            for j:= Rect.Left div wOrig to Rect.Right div wOrig do
              DrawBitmap(bmp,
                j*wOrig-Rect.Left,i*hOrig-Rect.Top);
         bsStretched:
            // called only for thumbnails; otherwise it's bad, because it does not set SetStretchBltMode
              if (gr.Width=Width) and (gr.Height=Height) then
                GraphicInterface.DrawBitmap(Canvas,
                  -Rect.Left, -Rect.Top, bmp)
              else
                GraphicInterface.StretchDrawBitmap(Canvas,
                  Bounds(-Rect.Left, -Rect.Top, Width, Height), bmp);
         bsTiledAndScrolled:
          for i:= (Rect.Top+VOffs) div hOrig to
                  (Rect.Bottom+VOffs) div hOrig do
            for j:= (Rect.Left+HOffs) div wOrig to
                    (Rect.Right+HOffs) div wOrig do
              DrawBitmap(bmp,
                j*wOrig-HOffs-Rect.Left,i*hOrig-VOffs-Rect.Top);
       end
       end
     else begin
       case Style of
         bsCentered:
           DrawGraphic(-Rect.Left+(Width-gr.Width) div 2,
           -Rect.Top+(Height-gr.Height) div 2, -1, -1, gr);
         bsTopLeft:
           DrawGraphic(-Rect.Left, -Rect.Top, -1, -1, gr);
         bsTopRight:
           DrawGraphic(-Rect.Left+(Width-gr.Width), -Rect.Top, -1, -1, gr);
         bsBottomLeft:
           DrawGraphic(-Rect.Left, -Rect.Top+(Height-gr.Height), -1, -1, gr);
         bsBottomRight:
           DrawGraphic(-Rect.Left+(Width-gr.Width), -Rect.Top+(Height-gr.Height),
             -1, -1, gr);
         bsTiled:
          for i:= Rect.Top div gr.Height to Rect.Bottom div gr.Height do
            for j:= Rect.Left div gr.Width to Rect.Right div gr.Width do
              DrawGraphic(j*gr.Width-Rect.Left,i*gr.Height-Rect.Top, -1, -1, gr);
         bsStretched:
           DrawGraphic(-Rect.Left, -Rect.Top, Width, Height, gr);
         bsTiledAndScrolled:
          for i:= (Rect.Top+VOffs) div gr.Height to
                  (Rect.Bottom+VOffs) div gr.Height do
            for j:= (Rect.Left+HOffs) div gr.Width to
                    (Rect.Right+HOffs) div gr.Width do
              DrawGraphic(j*gr.Width-HOffs-Rect.Left,i*gr.Height-VOffs-Rect.Top,
                -1, -1, gr);
       end;
     end;
   end;
 finally
   if Clipping then begin
     //GraphicInterface.SetWindowOrgEx(Canvas, pt.x, pt.y, nil);
     GraphicInterface.RestoreCanvasState(Canvas, DCIdx);
   end;
 end;
end;
{------------------------------------------------------------------------------}
function TRVBackground.GetBitmap: TBitmap;
begin
  Result := TBitmap(Image);
end;
{------------------------------------------------------------------------------}
function TRVBackground.GetThumbnail: TBitmap;
begin
  Result := TBitmap(ImageCopy);
end;
{------------------------------------------------------------------------------}
function TRVBackground.GetItemBackStyle: TRVItemBackgroundStyle;
begin
  case Style of
    bsNoBitmap:
      Result := rvbsColor;
    bsStretched:
      Result := rvbsStretched;
    bsCentered, bsTopLeft, bsTopRight, bsBottomLeft, bsBottomRight:
      Result := rvbsCentered;
    else
      Result := rvbsTiled;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.SetItemBackStyle(
  const Value: TRVItemBackgroundStyle);
begin
  case Value of
    rvbsColor:
      Style := bsNoBitmap;
    rvbsStretched:
      Style := bsStretched;
    rvbsCentered:
      Style := bsCentered;
    else
      Style := bsTiled;
  end;
end;
{------------------------------------------------------------------------------}
function TRVBackground.Empty: Boolean;
begin
  Result := (Style=bsNoBitmap) and ((Image=nil) or Image.Empty);
end;
{------------------------------------------------------------------------------}
function TRVBackground.EmptyImage: Boolean;
begin
  Result := (Image=nil) or Image.Empty;
end;
{------------------------------------------------------------------------------}
function TRVBackground.Visible: Boolean;
begin
  Result := (Style<>bsNoBitmap) and (Image<>nil) and not Image.Empty;
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.FreeImage;
begin
  RVFreeAndNil(Image);
  RVFreeAndNil(ImageCopy);
end;
{------------------------------------------------------------------------------}
procedure TRVBackground.AssignImage(AImage: TGraphic; ARVData: TPersistent;
  Copy: Boolean; BackgroundOwner: TObject);
var RVData: TCustomRVData;
begin
  if AImage=Image then
    exit;
  if HasThumbnail and (BackgroundOwner<>nil) then
    TCustomRVData(ARVData).GetThumbnailCache.RemoveThumbnail(BackgroundOwner);
  if Copy then begin
    FreeImage;
    if AImage<>nil then begin
      Image := RVGraphicHandler.CreateGraphic(TGraphicClass(AImage.ClassType));
      Image.Assign(AImage);
      Image.Transparent := AImage.Transparent;
    end;
    end
  else begin
    Image := AImage;
  end;
  RVData := TCustomRVData(ARVData);
  UpdatePaletted(RVData.GetDoInPaletteMode, RVData.GetRVPalette,
    RVData.GetRVLogPalette);
end;
{------------------------------------------------------------------------------}
function TRVBackground.IsSemitransparent: Boolean;
begin
  // assuming that Color=clNone
  Result := (Image<>nil) and not Image.Empty and
    (RVGraphicHandler.IsTransparent(Image) or (Style in [bsCentered, bsTopLeft,
      bsTopRight, bsBottomLeft, bsBottomRight]));
end;
{------------------------------------------------------------------------------}

end.

