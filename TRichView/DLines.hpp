﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'DLines.pas' rev: 27.00 (Windows)

#ifndef DlinesHPP
#define DlinesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dlines
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVDrawLineStreamingId : unsigned char { rvdlsiTRVDrawLineInfo, rvdlsiTRVJustifyDrawLineInfo, rvdlsiTRVSimpleLineHeightDrawLineInfo, rvdlsiTRVComplexLineHeightDrawLineInfo, rvdlsiTRVFloatingDrawLineInfo };

class DELPHICLASS TRVDrawLineInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDrawLineInfo : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	void __fastcall RaiseWrongAssignException(const int Value);
	
protected:
	virtual int __fastcall GetSpaceAtLeft(void);
	virtual void __fastcall SetSpaceAtLeft(const int Value);
	virtual int __fastcall GetExtraSpaceAbove(void);
	virtual void __fastcall SetExtraSpaceAbove(const int Value);
	virtual int __fastcall GetExtraSpaceBelow(void);
	virtual void __fastcall SetExtraSpaceBelow(const int Value);
	virtual int __fastcall GetObjectHeight(void);
	virtual int __fastcall GetObjectLeft(void);
	virtual int __fastcall GetObjectTop(void);
	virtual int __fastcall GetObjectWidth(void);
	DYNAMIC TRVDrawLineStreamingId __fastcall GetStreamingId(void);
	
public:
	int Left;
	int Top;
	int Width;
	int Height;
	int ItemNo;
	int Offs;
	int Length;
	System::ByteBool FromNewLine;
	System::ByteBool FromNewPara;
	System::ByteBool FloatingAboveUsed;
	__fastcall TRVDrawLineInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine);
	virtual void __fastcall SetData(int ALeft, int ATop, int AItemNo, System::ByteBool AFromNewLine);
	virtual void __fastcall SetSize(int AWidth, int AHeight);
	DYNAMIC bool __fastcall InitSplit(const Rvstyle::TRVScreenAndDevice &SaD, bool IgnorePageBreaks);
	DYNAMIC bool __fastcall CanSplitFirst(int Y, const Rvstyle::TRVScreenAndDevice &SaD, bool FirstOnPage, bool PageHasFootnotes, bool FootnotesChangeHeight, bool IgnorePageBreaks);
	DYNAMIC bool __fastcall SplitAt(int Y, const Rvstyle::TRVScreenAndDevice &SaD, bool FirstOnPage, System::Classes::TList* &FootnoteRVDataList, int &MaxHeight, bool FootnotesChangeHeight, bool IgnorePageBreaks);
	DYNAMIC void __fastcall SaveToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	__property int SpaceAtLeft = {read=GetSpaceAtLeft, write=SetSpaceAtLeft, nodefault};
	__property int ExtraSpaceBelow = {read=GetExtraSpaceBelow, write=SetExtraSpaceBelow, nodefault};
	__property int ExtraSpaceAbove = {read=GetExtraSpaceAbove, write=SetExtraSpaceAbove, nodefault};
	__property int ObjectWidth = {read=GetObjectWidth, nodefault};
	__property int ObjectHeight = {read=GetObjectHeight, nodefault};
	__property int ObjectLeft = {read=GetObjectLeft, nodefault};
	__property int ObjectTop = {read=GetObjectTop, nodefault};
public:
	/* TObject.Create */ inline __fastcall TRVDrawLineInfo(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVDrawLineInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVJustifyDrawLineInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVJustifyDrawLineInfo : public TRVDrawLineInfo
{
	typedef TRVDrawLineInfo inherited;
	
private:
	int FSpaceAtLeft;
	
protected:
	virtual int __fastcall GetSpaceAtLeft(void);
	virtual void __fastcall SetSpaceAtLeft(const int Value);
	DYNAMIC TRVDrawLineStreamingId __fastcall GetStreamingId(void);
	
public:
	DYNAMIC void __fastcall SaveToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFromStream(System::Classes::TStream* Stream);
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVJustifyDrawLineInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : TRVDrawLineInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVJustifyDrawLineInfo(void) : TRVDrawLineInfo() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVJustifyDrawLineInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSimpleLineHeightDrawLineInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSimpleLineHeightDrawLineInfo : public TRVJustifyDrawLineInfo
{
	typedef TRVJustifyDrawLineInfo inherited;
	
private:
	int FExtraSpaceBelow;
	
protected:
	virtual int __fastcall GetExtraSpaceBelow(void);
	virtual void __fastcall SetExtraSpaceBelow(const int Value);
	DYNAMIC TRVDrawLineStreamingId __fastcall GetStreamingId(void);
	
public:
	DYNAMIC void __fastcall SaveToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFromStream(System::Classes::TStream* Stream);
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVSimpleLineHeightDrawLineInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : TRVJustifyDrawLineInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVSimpleLineHeightDrawLineInfo(void) : TRVJustifyDrawLineInfo() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVSimpleLineHeightDrawLineInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVComplexLineHeightDrawLineInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVComplexLineHeightDrawLineInfo : public TRVSimpleLineHeightDrawLineInfo
{
	typedef TRVSimpleLineHeightDrawLineInfo inherited;
	
private:
	int FExtraSpaceAbove;
	
protected:
	virtual int __fastcall GetExtraSpaceAbove(void);
	virtual void __fastcall SetExtraSpaceAbove(const int Value);
	DYNAMIC TRVDrawLineStreamingId __fastcall GetStreamingId(void);
	
public:
	DYNAMIC void __fastcall SaveToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFromStream(System::Classes::TStream* Stream);
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVComplexLineHeightDrawLineInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : TRVSimpleLineHeightDrawLineInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVComplexLineHeightDrawLineInfo(void) : TRVSimpleLineHeightDrawLineInfo() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVComplexLineHeightDrawLineInfo(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVDItemFloatingType : unsigned char { rvdifLeft, rvdifRight };

class DELPHICLASS TRVFloatingDrawLineInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFloatingDrawLineInfo : public TRVComplexLineHeightDrawLineInfo
{
	typedef TRVComplexLineHeightDrawLineInfo inherited;
	
private:
	int __fastcall GetFloatBottom(void);
	int __fastcall GetFloatRight(void);
	
protected:
	virtual int __fastcall GetObjectHeight(void);
	virtual int __fastcall GetObjectLeft(void);
	virtual int __fastcall GetObjectTop(void);
	virtual int __fastcall GetObjectWidth(void);
	DYNAMIC TRVDrawLineStreamingId __fastcall GetStreamingId(void);
	
public:
	int FloatLeft;
	int FloatTop;
	int FloatWidth;
	int FloatHeight;
	TRVDItemFloatingType FloatType;
	DYNAMIC void __fastcall SaveToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	__property int FloatRight = {read=GetFloatRight, nodefault};
	__property int FloatBottom = {read=GetFloatBottom, nodefault};
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVFloatingDrawLineInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : TRVComplexLineHeightDrawLineInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVFloatingDrawLineInfo(void) : TRVComplexLineHeightDrawLineInfo() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVFloatingDrawLineInfo(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TRVDrawLineInfoClass;

class DELPHICLASS TRVDrawLines;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVDrawLines : public System::Classes::TList
{
	typedef System::Classes::TList inherited;
	
public:
	TRVDrawLineInfo* operator[](int Index) { return Items[Index]; }
	
private:
	int FStartDeletedIndex;
	int FDeletedCount;
	HIDESBASE TRVDrawLineInfo* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVDrawLineInfo* const Value);
	void __fastcall DeleteRange(int Index1, int Index2);
	
public:
	__fastcall TRVDrawLines(void);
	void __fastcall MarkForDelete(int Index1, int Index2);
	void __fastcall DeleteMarked(void);
	HIDESBASE void __fastcall Insert(int Index, void * Item);
	HIDESBASE void __fastcall Delete(int Index);
	void __fastcall SaveToStream(System::Classes::TStream* Stream, System::Classes::TList* AItems);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream, System::Classes::TList* AItems);
	Rvtypes::TRVRawByteString __fastcall GetString(int Index, System::Classes::TList* AItems);
	Rvtypes::TRVRawByteString __fastcall GetSubString(int Index, System::Classes::TList* AItems, int AStartIndex, int ALength);
	Rvtypes::TRVRawByteString __fastcall GetRightString(int Index, System::Classes::TList* AItems, int AStartIndex);
	int __fastcall FindFirstFloatingAboveDrawItem(int Index, int MinIndex, int Bottom, bool UseBottom);
	void __fastcall AssignFloatingAboveDrawItem(int DItemNo, Rvclasses::TRVIntegerList* ResultList);
	void __fastcall AssignFloatingCrossingY(int Y, int DItemNo, Rvclasses::TRVIntegerList* ResultList);
	__property TRVDrawLineInfo* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVDrawLines(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVFloatingDrawItems;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVFloatingDrawItems : public TRVDrawLines
{
	typedef TRVDrawLines inherited;
	
private:
	bool __fastcall MarkedHaveFloatType(Rvclasses::TRVIntegerList* Marks, TRVDItemFloatingType FloatType);
	
public:
	void __fastcall FillFloatingAboveDrawItem(int DItemNo, TRVDrawLines* Source);
	void __fastcall FillFloatingCrossingY(int Y, int DItemNo, TRVDrawLines* Source);
	void __fastcall RemoveNonOverlapping(int Y);
	void __fastcall MarkNonOverlapping(int Y, Rvclasses::TRVIntegerList* Marks);
	void __fastcall AdjustLineSize(int MinLeftIndent, int MinWidth, int MaxRightSide, int &Y, int &LeftIndent, int &LineWidth, bool AllowDelete, bool ClearLeft, bool ClearRight, System::Classes::TList* ClearIgnore);
	int __fastcall GetFloatBottom(void);
public:
	/* TRVDrawLines.Create */ inline __fastcall TRVFloatingDrawItems(void) : TRVDrawLines() { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TRVFloatingDrawItems(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Dlines */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DLINES)
using namespace Dlines;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DlinesHPP
