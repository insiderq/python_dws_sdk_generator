{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       Item types:                                     }
{       - a note shown as a floating box                }
{       - floating box without a note                   }
{       (boxes are displayed only on a page)            }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVSidenote;

{$I RV_Defs.inc}

interface

{$IFNDEF RVDONOTUSESEQ}

uses {$IFDEF RICHVIEWDEF2009}AnsiStrings,{$ENDIF}
  Windows, Graphics, Classes, SysUtils, StdCtrls,
  RVTypes, RVStyle, CRVData, RVItem, RVNote, RVScroll,
  RVFuncs, RichView, RVStr, RVClasses, RVSeqItem,
  {$IFNDEF RVDONOTUSEDOCX}
  RVDocX,
  {$ENDIF}
  RVFloatingBox, RVFloatingPos, DLines;

type


  TRVSidenoteItemInfo = class (TCustomRVNoteItemInfo)
    private
      FBoxProperties: TRVBoxProperties;
      FBoxPosition  : TRVBoxPosition;
      procedure Init(RVData: TCustomRVData);
      procedure SetBoxProperties(const Value: TRVBoxProperties);
      procedure SetBoxPosition(const Value: TRVBoxPosition);
    protected
      function RTF_GetBelowText: Integer;
      function GetRVFExtraPropertyCount: Integer; override;
      procedure SaveRVFExtraProperties(Stream: TStream); override;
      function GetNumberType: TRVSeqType; override;
      {$IFNDEF RVDONOTUSEDOCX}
      procedure SaveOOXMLShape(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean);
      procedure SaveOOXMLDrawing(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean);
      {$ENDIF}
    public
      constructor Create(RVData: TPersistent); override;
      constructor CreateEx(RVData: TPersistent;
        ATextStyleNo, AStartFrom: Integer; AReset: Boolean); override;
      destructor Destroy; override;
      procedure ConvertToDifferentUnits(NewUnits: TRVStyleUnits;
        RVStyle: TRVStyle; Recursive: Boolean); override;
      procedure Assign(Source: TCustomRVItemInfo); override;
      {$IFNDEF RVDONOTUSEHTML}
      function GetHTMLAnchorName: TRVAnsiString; override;
      {$ENDIF}
      function SetExtraCustomProperty(const PropName: TRVAnsiString;
        const Value: String): Boolean; override;
      {$IFNDEF RVDONOTUSERTF}
      procedure SaveRTF(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        Level: Integer; var SavingData: TRVRTFSavingData; DocType: TRVRTFDocType;
        HiddenParent: Boolean); override;
      {$ENDIF}
      function GetSubDocRTFDocType: TRVRTFDocType; override;      
      {$IFNDEF RVDONOTUSEDOCX}
      procedure SaveOOXML(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      procedure SaveOOXMLBeforeRun(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      procedure SaveOOXMLAfterRun(Stream: TStream; RVData: TPersistent; ItemNo: Integer;
        SavingData: TRVDocXSavingData; HiddenParent: Boolean); override;
      {$ENDIF}
      function AsText(LineWidth: Integer;
        RVData: TPersistent; const Text: TRVRawByteString; const Path: String;
        TextOnly, Unicode: Boolean; CodePage: TRVCodePage): TRVRawByteString; override;
      function IsFixedHeight: Boolean; override;
      function SetExtraIntPropertyEx(Prop: Integer; Value: Integer): Boolean; override;
      function GetExtraIntPropertyEx(Prop: Integer; var Value: Integer): Boolean; override;
      function GetExtraIntPropertyReformat(Prop: Integer): TRVReformatType; override;
      function GetSubDocColor: TColor; override;
      property BoxProperties: TRVBoxProperties read FBoxProperties write SetBoxProperties;
      property BoxPosition:   TRVBoxPosition read FBoxPosition write SetBoxPosition;
  end;

  TRVTextBoxItemInfo = class (TRVSidenoteItemInfo)
  private
    procedure Init(RVData: TCustomRVData);
  protected
    function GetAssociatedTextStyleNo: Integer; override;
    procedure SetAssociatedTextStyleNo(Value: Integer); override;
  public
    constructor Create(RVData: TPersistent); override;
    constructor CreateEx(RVData: TPersistent;
      ATextStyleNo, AStartFrom: Integer; AReset: Boolean); override;
    function GetNoteText: String; override;
    function GetBoolValue(Prop: TRVItemBoolProperty): Boolean; override;
    procedure OnDocWidthChange(DocWidth: Integer; dli: TRVDrawLineInfo;
      Printing: Boolean; Canvas: TCanvas; RVData: TPersistent;
      sad: PRVScreenAndDevice; var HShift, Desc: Integer;
      NoCaching, Reformatting, UseFormatCanvas: Boolean); override;
    procedure Paint(x,y: Integer; Canvas: TCanvas; State: TRVItemDrawStates;
      dli: TRVDrawLineInfo; RVData: TPersistent); override;
    procedure Print(Canvas: TCanvas; x,y,x2: Integer;
      Preview, Correction: Boolean; const sad: TRVScreenAndDevice;
      RichView: TRVScroller;
      dli: TRVDrawLineInfo; Part: Integer;
      ColorMode: TRVColorMode; RVData: TPersistent; PageNo: Integer); override;
    procedure MarkStylesInUse(Data: TRVDeleteUnusedStylesData); override;
    procedure UpdateStyles(Data: TRVDeleteUnusedStylesData); override;
    procedure ApplyStyleConversion(RVData: TPersistent;
      ItemNo, UserData: Integer;
      ConvType: TRVEStyleConversionType; Recursive: Boolean); override;
    function GetSubDocRTFDocType: TRVRTFDocType; override;
  end;

const
  RV_SIDENOTE_SEQNAME = '@Sidenote@';
  RV_TEXTBOX_SEQNAME = '@TextBox@';
  RV_SIDENOTE_HTML_ANCHOR = '_sidenote%d';
  rvsSidenote = -206;
  rvsTextBox = -207;

function RVGetFirstSidenote(RichView: TCustomRichView; AllowHidden: Boolean): TRVSidenoteItemInfo;
function RVGetNextSidenote(RichView: TCustomRichView;
  Sidenote: TRVSidenoteItemInfo; AllowHidden: Boolean): TRVSidenoteItemInfo;
function RVGetFirstSidenoteInRootRVData(RVData: TCustomRVData; AllowHidden: Boolean): TRVSidenoteItemInfo;
function RVGetNextSidenoteInRootRVData(RVData: TCustomRVData;
  Sidenote: TRVSidenoteItemInfo; AllowHidden: Boolean): TRVSidenoteItemInfo;

{$ENDIF}

implementation

{$IFNDEF RVDONOTUSESEQ}

{==============================================================================}
function RVGetFirstSidenote(RichView: TCustomRichView; AllowHidden: Boolean): TRVSidenoteItemInfo;
begin
  Result := TRVSidenoteItemInfo(GetNextNote(RichView.RVData, nil,
    TRVSidenoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetNextSidenote(RichView: TCustomRichView;
  Sidenote: TRVSidenoteItemInfo; AllowHidden: Boolean): TRVSidenoteItemInfo;
begin
  Result := TRVSidenoteItemInfo(GetNextNote(RichView.RVData, Sidenote,
    TRVSidenoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetFirstSidenoteInRootRVData(RVData: TCustomRVData; AllowHidden: Boolean): TRVSidenoteItemInfo;
begin
  Result := TRVSidenoteItemInfo(GetNextNote(RVData, nil,
    TRVSidenoteItemInfo, AllowHidden));
end;
{------------------------------------------------------------------------------}
function RVGetNextSidenoteInRootRVData(RVData: TCustomRVData;
  Sidenote: TRVSidenoteItemInfo; AllowHidden: Boolean): TRVSidenoteItemInfo;
begin
  Result := TRVSidenoteItemInfo(GetNextNote(RVData, Sidenote,
    TRVSidenoteItemInfo, AllowHidden));
end;
{========================== TRVSidenoteItemInfo ===============================}
constructor TRVSidenoteItemInfo.Create(RVData: TPersistent);
begin
  inherited Create(RVData);
  Init(TCustomRVData(RVData));
end;
{------------------------------------------------------------------------------}
constructor TRVSidenoteItemInfo.CreateEx(RVData: TPersistent;
  ATextStyleNo, AStartFrom: Integer; AReset: Boolean);
begin
  inherited CreateEx(RVData, ATextStyleNo, AStartFrom, AReset);
  Init(TCustomRVData(RVData));
end;
{------------------------------------------------------------------------------}
destructor TRVSidenoteItemInfo.Destroy;
begin
  RVFreeAndNil(FBoxProperties);
  RVFreeAndNil(FBoxPosition);
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.Init(RVData: TCustomRVData);
begin
  FBoxProperties := TRVBoxProperties.Create;
  FBoxPosition   := TRVBoxPosition.Create;
  FDocument := TRVNoteData.Create(Self, RVData);
  FDocument.MainRVData := TCustomRVData(ParentRVData).GetAbsoluteRootData;
  StyleNo := rvsSidenote;
  SeqName := RV_SIDENOTE_SEQNAME;
end;
function TRVSidenoteItemInfo.IsFixedHeight: Boolean;
begin
  Result := BoxProperties.HeightType<>rvbhtAuto;
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.Assign(Source: TCustomRVItemInfo);
begin
  if Source is TRVSidenoteItemInfo then begin
    BoxProperties := TRVSidenoteItemInfo(Source).BoxProperties;
    BoxPosition   := TRVSidenoteItemInfo(Source).BoxPosition;
  end;
  inherited;
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.AsText(LineWidth: Integer; RVData: TPersistent;
  const Text: TRVRawByteString; const Path: String; TextOnly, Unicode: Boolean;
  CodePage: TRVCodePage): TRVRawByteString;
begin
  Result := DoGetAsText(LineWidth, RVData, Text, Path, TextOnly, Unicode, CodePage,
    GetSubDocRTFDocType=rvrtfdtSidenote);
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.ConvertToDifferentUnits(NewUnits: TRVStyleUnits;
  RVStyle: TRVStyle; Recursive: Boolean);
begin
  inherited;
  BoxProperties.ConvertToDifferentUnits(NewUnits, RVStyle);
  BoxPosition.ConvertToDifferentUnits(NewUnits, RVStyle);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEHTML}
function TRVSidenoteItemInfo.GetHTMLAnchorName: TRVAnsiString;
begin
  Result := RV_SIDENOTE_HTML_ANCHOR;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.GetNumberType: TRVSeqType;
begin
  Result := TCustomRVData(ParentRVData).GetRVStyle.SidenoteNumbering;
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.GetRVFExtraPropertyCount: Integer;
begin
  Result := inherited GetRVFExtraPropertyCount +
   BoxProperties.GetChangedPropertyCount +
   BoxPosition.GetChangedPropertyCount;
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.SaveRVFExtraProperties(Stream: TStream);
begin
  inherited;
  FBoxProperties.SaveRVFExtraProperties(Stream);
  FBoxPosition.SaveRVFExtraProperties(Stream);
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.SetBoxProperties(const Value: TRVBoxProperties);
begin
  FBoxProperties.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.SetBoxPosition(const Value: TRVBoxPosition);
begin
  FBoxPosition.Assign(Value);
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.SetExtraCustomProperty(
  const PropName: TRVAnsiString; const Value: String): Boolean;
begin
  Result := FBoxProperties.SetExtraCustomProperty(PropName, Value) or
    FBoxPosition.SetExtraCustomProperty(PropName, Value) or
    inherited SetExtraCustomProperty(PropName, Value);
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.GetExtraIntPropertyEx(Prop: Integer;
  var Value: Integer): Boolean;
begin
  Result := FBoxProperties.GetExtraIntPropertyEx(Prop, Value) or
    FBoxPosition.GetExtraIntPropertyEx(Prop, Value) or
    inherited GetExtraIntPropertyEx(Prop, Value);
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.SetExtraIntPropertyEx(Prop, Value: Integer): Boolean;
begin
  Result := FBoxProperties.SetExtraIntPropertyEx(Prop, Value) or
    FBoxPosition.SetExtraIntPropertyEx(Prop, Value) or
    inherited SetExtraIntPropertyEx(Prop, Value);
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.GetExtraIntPropertyReformat(Prop: Integer): TRVReformatType;
begin
  if not BoxPosition.GetExtraIntPropertyReformat(Prop, Result) and
     not BoxProperties.GetExtraIntPropertyReformat(Prop, Result) then
    Result := inherited GetExtraIntPropertyReformat(Prop);
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.GetSubDocColor: TColor;
begin
  Result := FBoxProperties.Background.Color;
  if Result=clNone then
    Result := inherited GetSubDocColor;
end;
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.RTF_GetBelowText: Integer;
begin
  case BoxPosition.PositionInText of
    rvpitBelowText:
      Result := 1;
    else
      Result := 0;
  end;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSERTF}
procedure TRVSidenoteItemInfo.SaveRTF(Stream: TStream; RVData: TPersistent;
  ItemNo, Level: Integer; var SavingData: TRVRTFSavingData;
  DocType: TRVRTFDocType; HiddenParent: Boolean);

var RVStyle: TRVStyle;
  {...................................................................}
  function GetInHeaderKeyword: TRVAnsiString;
  begin
    if DocType=rvrtfdtHeaderFooter then
      Result := '\shpfhdr1'
    else
      Result := '\shpfhdr0';
  end;
  {...................................................................}
  function GetLeftTw: Integer;
  begin
    case BoxPosition.HorizontalPositionKind of
      rvfpkAbsPosition:
        Result := RVStyle.GetAsTwips(BoxPosition.HorizontalOffset);
      else
        Result := 0;
    end;
  end;
  {...................................................................}
  function GetRightOffsTw: Integer;
  begin
    case BoxProperties.WidthType of
      rvbwtAbsolute:
        Result := RVStyle.GetAsTwips(BoxProperties.Width);
      else
        Result := 0;
    end;
  end;
  {...................................................................}
  function GetTopTw: Integer;
  begin
    case BoxPosition.VerticalPositionKind of
      rvfpkAbsPosition:
        Result := RVStyle.GetAsTwips(BoxPosition.VerticalOffset);
      else
        Result := 0;
    end;
  end;
  {...................................................................}
  function GetBottomOffsTw: Integer;
  begin
    case BoxProperties.HeightType of
      rvbhtAbsolute:
        Result := RVStyle.GetAsTwips(BoxProperties.Height);
      rvbhtAuto:
        Result := RVStyle.PixelsToTwips(100);
      else
        Result := 0;
    end;
  end;
  {...................................................................}
  function GetPositionKeywords: TRVAnsiString;
  var v1, v2: Integer;
  begin
    v1 := GetLeftTw;
    v2 := GetRightOffsTw;
    Result := '\shpleft'+RVIntToStr(v1);
    if v2>0 then
      Result := Result+'\shpright'+RVIntToStr(v1+v2);
    v1 := GetTopTw;
    v2 := GetBottomOffsTw;
    Result := Result+'\shptop'+RVIntToStr(v1);
    if v2>0 then
      Result := Result + '\shpbottom'+RVIntToStr(v1+v2);
  end;
  {...................................................................}
  function GetAnchorKeywords: TRVAnsiString;
  begin
    case BoxPosition.HorizontalAnchor of
      rvhanPage, rvhanLeftMargin:
        Result := '\shpbxpage';
      else
        Result := '\shpbxcolumn';
    end;
    Result := Result + '\shpbxignore';
    case BoxPosition.VerticalAnchor of
      rvvanLine, rvvanParagraph:
        Result := Result + '\shpbypara';
      rvvanPage, rvvanTopMargin, rvvanBottomMargin:
        Result := Result + '\shpbypage';
      else
        Result := Result + '\shpbymargin';
    end;
    Result := Result + '\shpbyignore';
  end;
  {...................................................................}
  function GetPaddingProps: TRVAnsiString;
  begin
    Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '{\sp{\sn dxTextLeft}{\sv %d}}{\sp{\sn dyTextTop}{\sv %d}}'+
    '{\sp{\sn dxTextRight}{\sv %d}}{\sp{\sn dyTextBottom}{\sv %d}}',
    [RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Left),
     RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Top),
     RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Right),
     RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Bottom)]);
  end;
  {...................................................................}
  function GetSizeRelH: Integer;
  begin
    case BoxProperties.WidthType of
      rvbwtRelPage:
        Result := 1;
      rvbwtRelMainTextArea:
        Result := 0;
      rvbwtRelLeftMargin:
        Result := 2;
      rvbwtRelRightMargin:
        Result := 3;
      rvbwtRelInnerMargin:
        Result := 4;
      rvbwtRelOuterMargin:
        Result := 5;
      else
        Result := 0;
    end;
  end;
  {...................................................................}
  function GetSizeRelV: Integer;
  begin
    case BoxProperties.HeightType of
      rvbhtRelPage:
        Result := 1;
      rvbhtRelMainTextArea:
        Result := 0;
      rvbhtRelTopMargin:
        Result := 2;
      rvbhtRelBottomMargin:
        Result := 3;
      else
        Result := 0;
    end;
  end;
  {...................................................................}
  function GetRelSizeProps: TRVAnsiString;
  begin
    Result := '';
    if BoxProperties.WidthType<>rvbwtAbsolute then
      Result := Result+{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn sizerelh}{\sv %d}}{\sp{\sn pctHoriz}{\sv %d}}',
        [GetSizeRelH, Round(BoxProperties.Width/100)]);
    case BoxProperties.HeightType of
      rvbhtAbsolute: ;
      rvbhtAuto:
        Result := Result+'{\sp{\sn fFitShapeToText}{\sv 1}}';
      else
        Result := Result+{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
          '{\sp{\sn sizerelv}{\sv %d}}{\sp{\sn pctVert}{\sv %d}}',
          [GetSizeRelV, Round(BoxProperties.Height/100)])
    end;

  end;
  {..................................................................}
  function GetAnchorH: Integer;
  begin
    case BoxPosition.HorizontalAnchor of
      rvhanCharacter:
        Result := 3;
      rvhanPage:
        Result := 1;
      rvhanMainTextArea:
        Result := 0;
      rvhanLeftMargin:
        Result := 4;
      rvhanRightMargin:
        Result := 5;
      rvhanInnerMargin:
        Result := 6;
      else //rvhanOuterMargin:
        Result := 7;
    end;
  end;
  {..................................................................}
  function GetAnchorV: Integer;
  begin
    case BoxPosition.VerticalAnchor of
      rvvanLine:
        Result := 3;
      rvvanParagraph:
        if BoxPosition.VerticalPositionKind<>rvfpkAlignment then
          Result := 2
        else
          Result := 3; // alignment to paragraph is not supported by MS Word
      rvvanPage:
        Result := 1;
      rvvanMainTextArea:
        Result := 0;
      rvvanTopMargin:
        Result := 4;
      else //rvvanBottomMargin:
        Result := 5;
    end;
  end;
  {..................................................................}
  function GetAlignmentH: Integer;
  begin
    Result := -1;
    case BoxPosition.HorizontalPositionKind of
      rvfpkAlignment:
        case BoxPosition.HorizontalAlignment of
          rvfphalLeft:   Result := 1;
          rvfphalCenter: Result := 2;
          rvfphalRight:  Result := 3;
        end;
      rvfpkAbsPosition:
        Result := 0;
    end;
  end;
  {..................................................................}
  function GetAlignmentV: Integer;
  begin
    Result := -1;
    case BoxPosition.VerticalPositionKind of
      rvfpkAlignment:
        case BoxPosition.VerticalAlignment of
          rvfpvalTop:     Result := 1;
          rvfpvalCenter:  Result := 2;
          rvfpvalBottom:  Result := 3;
        end;
      rvfpkAbsPosition:
        Result := 0;
    end;
  end;
  {..................................................................}
  function GetPositionProps: TRVAnsiString;
  var v: Integer;
  begin
    Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      '{\sp{\sn fLayoutInCell}{\sv %d}}{\sp{\sn fBehindDocument}{\sv %d}}'+
      '{\sp{\sn posrelh}{\sv %d}}{\sp{\sn posrelv}{\sv %d}}',
      [ord(BoxPosition.RelativeToCell), RTF_GetBelowText, GetAnchorH, GetAnchorV]);
    v := GetAlignmentH;
    if v>=0 then
      Result := Result+{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn posh}{\sv %d}}', [v])
    else if BoxPosition.HorizontalAnchor<>rvhanCharacter then // MS Word does not support % position relative to character
      Result := Result+{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn pctHorizPos}{\sv %d}}', [Round(BoxPosition.HorizontalOffset/100)]);
    v := GetAlignmentV;
    if v>=0 then
      Result := Result+{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn posv}{\sv %d}}', [v])
    else if not (BoxPosition.VerticalAnchor in [rvvanLine, rvvanParagraph]) then // MS Word does not support % position relative to line or para
      Result := Result+{$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn pctVertPos}{\sv %d}}', [Round(BoxPosition.VerticalOffset/100)])
  end;
  {..................................................................}
  function GetBackgroundProps: TRVAnsiString;
  begin
    if BoxProperties.Background.Color=clNone then
      Result := '{\sp{\sn fFilled}{\sv 0}}'
    else
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fillColor}{\sv %d}}',
        [ColorToRGB(BoxProperties.Background.Color)]);
  end;
  {..................................................................}
  function GetLineStyle: Integer;
  begin
    case BoxProperties.Border.Style of
      rvbSingle:       Result := 0;
      rvbDouble:       Result := 1;
      rvbTriple:       Result := 4;
      rvbThickInside:  Result := 3;
      rvbThickOutside: Result := 2;
      else             Result := -1;
    end;
  end;
  {..................................................................}
  function GetBorderProps: TRVAnsiString;
  begin
    if (BoxProperties.Border.Style=rvbNone) or (BoxProperties.Border.Width=0) or
      (BoxProperties.Border.Color=clNone) then
      Result := '{\sp{\sn fLine}{\sv 0}}'
    else begin
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        '{\sp{\sn fLine}{\sv 1}}{\sp{\sn lineStyle}{\sv %d}}'+
        '{\sp{\sn lineColor}{\sv %d}}{\sp{\sn lineWidth}{\sv %d}}',
        [GetLineStyle, ColorToRGB(BoxProperties.Border.Color),
        RVStyle.GetAsEMU(BoxProperties.Border.GetTotalWidth)]);
      (*
      // does not work in MS Word
      if not BoxProperties.Border.VisibleBorders.Left then
        Result := Result + '{\sp{\sn fLeftLine}{\sv 0}}';
      if not BoxProperties.Border.VisibleBorders.Top then
        Result := Result + '{\sp{\sn fTopLine}{\sv 0}}';
      if not BoxProperties.Border.VisibleBorders.Right then
        Result := Result + '{\sp{\sn fRightLine}{\sv 0}}';
      if not BoxProperties.Border.VisibleBorders.Bottom then
        Result := Result + '{\sp{\sn fBottomLine}{\sv 0}}';
      *)
    end;
  end;
  {..................................................................}
var SeqList: TRVSeqList;
    Index: Integer;
begin
  if not (DocType in [rvrtfdtMain, rvrtfdtHeaderFooter]) then
    exit;
  if GetSubDocRTFDocType=rvrtfdtSidenote then begin
    SeqList := TCustomRVData(RVData).GetSeqList(False);
    Index := GetIndexInList(SeqList);
    RVWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '{\*\bkmkstart _sidenote%d}', [Index]));
    inherited;
    RVWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '{\*\bkmkend _sidenote%d}', [Index]));
  end;
  RVStyle := TCustomRVData(RVData).GetRVStyle;
  //if Hidden or HiddenParent then // does not work
  //  RVWrite(Stream, '{\v');
  RVWrite(Stream, '{\shp{\*\shpinst'+GetPositionKeywords+GetInHeaderKeyword+
    GetAnchorKeywords+'\shpwr3\shpfblwtxt'+RVIntToStr(RTF_GetBelowText));
  RVWrite(Stream,
    '\shpfblwtxt0{\sp{\sn shapeType}{\sv 202}}'+
    GetPaddingProps+GetRelSizeProps+GetPositionProps+GetBackgroundProps+
    GetBorderProps);
  RVWrite(Stream, '{\shptxt');
  TCustomRVData(Document).SaveRTFToStream(Stream, False, 0, clNone, nil,
    SavingData, GetSubDocRTFDocType, False, False, nil, nil, nil, nil, nil, nil);
  RVWrite(Stream, '\par}}{\shprslt}}');
  // RVWrite(Stream, '}'); // does not work
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVSidenoteItemInfo.GetSubDocRTFDocType: TRVRTFDocType;
begin
  Result := rvrtfdtSidenote;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCX}
procedure TRVSidenoteItemInfo.SaveOOXMLBeforeRun(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
var SeqList: TRVSeqList;
    Index: Integer;
begin
  if SavingData.CurInTextBox or
    not (SavingData.CurPart in [rvdocxpMain, rvdocxpHeader, rvdocxpFooter,
    rvdocxpHeader1, rvdocxpFooter1, rvdocxpHeaderEven, rvdocxpFooterEven]) then
    exit;
  if GetSubDocRTFDocType=rvrtfdtSidenote then begin
    SeqList := TCustomRVData(RVData).GetSeqList(False);
    Index := GetIndexInList(SeqList);
    RVWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      '<w:bookmarkStart w:id="%d" w:name="_sidenote%d"/>', [SavingData.BookmarkIndex, Index]));
    SavingData.NoteBookmarkIndex := SavingData.BookmarkIndex;
    inc(SavingData.BookmarkIndex);
    inherited;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.SaveOOXMLAfterRun(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
begin
  if SavingData.CurInTextBox or
    not (SavingData.CurPart in [rvdocxpMain, rvdocxpHeader, rvdocxpFooter,
    rvdocxpHeader1, rvdocxpFooter1, rvdocxpHeaderEven, rvdocxpFooterEven]) then
    exit;
  if GetSubDocRTFDocType=rvrtfdtSidenote then begin
    inherited;
    RVWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      '<w:bookmarkEnd w:id="%d"/>', [SavingData.NoteBookmarkIndex]));
  end;
  inc(SavingData.ShapeIndex);
  RVWrite(Stream, '<w:r>');
  if Hidden or HiddenParent then
    RVWrite(Stream, '<w:rPr><w:vanish/></w:rPr>');
  SavingData.CurInTextBox := True;
  try
    RVWrite(Stream, '<mc:AlternateContent><mc:Choice Requires="wps">');
    SaveOOXMLDrawing(Stream, RVData, ItemNo, SavingData, HiddenParent);
    RVWrite(Stream, '</mc:Choice><mc:Fallback>');
    SaveOOXMLShape(Stream, RVData, ItemNo, SavingData, HiddenParent);
    RVWrite(Stream, '</mc:Fallback></mc:AlternateContent></w:r>');
  finally
    SavingData.CurInTextBox := False;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRVSidenoteItemInfo.SaveOOXML(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
begin
  if SavingData.CurInTextBox or
    not (SavingData.CurPart in [rvdocxpMain, rvdocxpHeader, rvdocxpFooter,
    rvdocxpHeader1, rvdocxpFooter1, rvdocxpHeaderEven, rvdocxpFooterEven]) then
    exit;
  if GetSubDocRTFDocType=rvrtfdtSidenote then
    inherited;
end;
{------------------------------------------------------------------------------}
type
  TRVBoxCSSProperty = (rvbcssLeftMargin, rvbcssTopMargin, rvbcssWidth, rvbcssHeight,
    rvbcssMSOWidthPercent, rvbcssMSOHeightPercent,
    rvbcssMSOWidthRelative_Full, rvbcssMSOHeightRelative_Full,
    rvbcssMSOPosHorizontal_Full, rvbcssMSOPosVertical_Full,
    rvbcssMSOPosHorizontalRelative, rvbcssMSOPosVerticalRelative,
    rvbcssVTextAnchor, rvbcssZIndex);
procedure TRVSidenoteItemInfo.SaveOOXMLShape(Stream: TStream; RVData: TPersistent;
  ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
var RVStyle: TRVStyle;
  {.......................................................................}
  function GetCSSValue(Prop: TRVBoxCSSProperty): TRVAnsiString;
  begin
    case Prop of
      rvbcssLeftMargin:
        if BoxPosition.HorizontalPositionKind=rvfpkAbsPosition then
          Result := RVStyle.GetCSSSizeInPoints(BoxPosition.HorizontalOffset)
        else
          Result := '0';
      rvbcssTopMargin:
        if BoxPosition.VerticalPositionKind=rvfpkAbsPosition then
          Result := RVStyle.GetCSSSizeInPoints(BoxPosition.VerticalOffset)
        else
          Result := '0';
      rvbcssWidth:
        if BoxProperties.WidthType=rvbwtAbsolute then
          Result := RVStyle.GetCSSSizeInPoints(BoxProperties.Width)
        else
          Result := RVStyle.GetCSSSizeInPoints(RVStyle.PixelsToUnits(100));
      rvbcssHeight:
        if BoxProperties.HeightType=rvbhtAbsolute then
          Result := RVStyle.GetCSSSizeInPoints(BoxProperties.Height)
        else
          Result := RVStyle.GetCSSSizeInPoints(RVStyle.PixelsToUnits(100));
      rvbcssMSOWidthPercent:
        if BoxProperties.WidthType<>rvbwtAbsolute then
          Result := RVIntToStr(Round(BoxProperties.Width/100))
        else
          Result := '0';
      rvbcssMSOHeightPercent:
        if not (BoxProperties.HeightType in [rvbhtAbsolute, rvbhtAuto]) then
          Result := RVIntToStr(Round(BoxProperties.Height/100))
        else
          Result := '0';
      rvbcssMSOWidthRelative_Full:
        begin
          case BoxProperties.WidthType of
            rvbwtRelPage:         Result := 'page';
            rvbwtRelMainTextArea: Result := 'margin';
            rvbwtRelLeftMargin:   Result := 'left-margin-area';
            rvbwtRelRightMargin:  Result := 'right-margin-area';
            rvbwtRelInnerMargin:  Result := 'inner-margin-area';
            rvbwtRelOuterMargin:  Result := 'outer-margin-area';
            else Result := '';
          end;
          if Result<>'' then
            Result := 'mso-width-relative:'+Result+';';
        end;
      rvbcssMSOHeightRelative_Full:
        begin
          case BoxProperties.HeightType of
            rvbhtRelPage:         Result := 'page';
            rvbhtRelMainTextArea: Result := 'margin';
            rvbhtRelTopMargin:    Result := 'top-margin-area';
            rvbhtRelBottomMargin: Result := 'bottom-margin-area';
            else Result := '';
          end;
          if Result<>'' then
            Result := 'mso-height-relative:'+Result+';';
        end;
      rvbcssMSOPosHorizontal_Full:
        begin
          case BoxPosition.HorizontalPositionKind of
            rvfpkAlignment:
              case BoxPosition.HorizontalAlignment of
                rvfphalLeft: Result := 'left';
                rvfphalCenter: Result := 'center';
                else {rvfphalRight:} Result := 'right';
              end;
            rvfpkAbsPosition:
              Result := 'absolute';
            rvfpkPercentPosition:
              if BoxPosition.HorizontalAnchor<>rvhanCharacter then
                Result := ''
              else
                Result := 'absolute'; // MS Word does not support % position relative to character
          end;
          if Result<>'' then
            Result := 'mso-position-horizontal:'+Result+';'
          else
            Result := 'mso-left-percent:'+RVIntToStr(Round(BoxPosition.HorizontalOffset/100))+';';
        end;
      rvbcssMSOPosVertical_Full:
        begin
          case BoxPosition.VerticalPositionKind of
            rvfpkAlignment:
              case BoxPosition.VerticalAlignment of
                rvfpvalTop: Result := 'top';
                rvfpvalCenter: Result := 'center';
                else {rvfpvalBottom:} Result := 'bottom';
              end;
            rvfpkAbsPosition:
              Result := 'absolute';
            rvfpkPercentPosition:
              if not (BoxPosition.VerticalAnchor in [rvvanLine,rvvanParagraph]) then
                Result := ''
              else
                Result := 'absolute';  // MS Word does not support % position relative to line or para
          end;
          if Result<>'' then
            Result := 'mso-position-vertical:'+Result+';'
          else
            Result := 'mso-top-percent:'+RVIntToStr(Round(BoxPosition.VerticalOffset/100))+';';
        end;
      rvbcssMSOPosHorizontalRelative:
        case BoxPosition.HorizontalAnchor of
          rvhanPage:         Result := 'page';
          rvhanMainTextArea: Result := 'margin';
          rvhanLeftMargin:   Result := 'left-margin-area';
          rvhanRightMargin:  Result := 'right-margin-area';
          rvhanInnerMargin:  Result := 'inner-margin-area';
          rvhanOuterMargin:  Result := 'outer-margin-area';
          else {rvhanCharacter:} Result := 'char';
        end;
      rvbcssMSOPosVerticalRelative:
        case BoxPosition.VerticalAnchor of
          rvvanLine:         Result := 'line';
          rvvanPage:         Result := 'page';
          rvvanMainTextArea: Result := 'margin';
          rvvanTopMargin:    Result := 'top-margin-area';
          rvvanBottomMargin: Result := 'bottom-margin-area';
          else {rvvanParagraph:}
            if BoxPosition.VerticalPositionKind<>rvfpkAlignment then
              Result := 'text'
            else
              Result := 'line'; // alignment to paragraph is not supported by MS Word
        end;
      rvbcssVTextAnchor:
        case BoxProperties.VAlign of
          tlCenter:  Result := 'middle';
          tlBottom:  Result := 'bottom';
          else {tlTop:} Result := 'top';
        end;
      rvbcssZIndex:
        if BoxPosition.PositionInText=rvpitAboveText then
          Result := '251659264'
        else
          Result := '-251659264';
    end;
  end;
  {.......................................................................}
  function GetShapeCSS: TRVAnsiString;
  begin
    Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      'position:absolute;margin-left:%s;margin-top:%s;width:%s;height:%s;z-index:%s;visibility:visible;'+
      'mso-wrap-style:square;mso-width-percent:%s;mso-height-percent:%s;%s%s'+
      'mso-wrap-distance-left:9pt;mso-wrap-distance-top:3.75pt;mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:3.75pt;'+
      '%smso-position-horizontal-relative:%s;%smso-position-vertical-relative:%s;'+
      'v-text-anchor:%s',
      [GetCSSValue(rvbcssLeftMargin), GetCSSValue(rvbcssTopMargin),
      GetCSSValue(rvbcssWidth), GetCSSValue(rvbcssHeight),
      GetCSSValue(rvbcssZIndex),
      GetCSSValue(rvbcssMSOWidthPercent), GetCSSValue(rvbcssMSOHeightPercent),
      GetCSSValue(rvbcssMSOWidthRelative_Full), GetCSSValue(rvbcssMSOHeightRelative_Full),
      GetCSSValue(rvbcssMSOPosHorizontal_Full), GetCSSValue(rvbcssMSOPosHorizontalRelative),
      GetCSSValue(rvbcssMSOPosVertical_Full), GetCSSValue(rvbcssMSOPosVerticalRelative),
      GetCSSValue(rvbcssVTextAnchor)]);
  end;
  {.......................................................................}
  function GetFillStr: TRVAnsiString;
  begin
    if BoxProperties.Background.Color=clNone then
      Result := 'filled="f"'
    else
      Result := 'fillcolor='+RV_GetHTMLRGBStr(BoxProperties.Background.Color, True);
  end;
  {.......................................................................}
  function GetBorderStr: TRVAnsiString;
  begin
    if (BoxProperties.Border.Style=rvbNone) or (BoxProperties.Border.Width=0) or
       (BoxProperties.Border.Color=clNone) then
      Result := 'stroked="f"'
    else begin
      Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
        'strokecolor=%s strokeweight="%s"',
        [RV_GetHTMLRGBStr(BoxProperties.Border.Color, True),
         RVStyle.GetCSSSizeInPoints(BoxProperties.Border.GetTotalWidth)]);
    end;
  end;
  {.......................................................................}
  function GetBorderElStr: TRVAnsiString;
  begin
    case BoxProperties.Border.Style of
      rvbDouble: Result := 'thinThin';
      rvbTriple: Result := 'thickBetweenThin';
      rvbThickInside: Result := 'thinThick';
      rvbThickOutside: Result := 'thickThin'
      else Result := '';
    end;
    if Result<>'' then
      Result := '<v:stroke linestyle="'+Result+'"/>';
  end;
  {.......................................................................}
  function GetAllowInCellStr: TRVAnsiString;
  begin
    if not BoxPosition.RelativeToCell then
      Result := ' o:allowincell="f"'
    else
      Result := '';
  end;
  {.......................................................................}
  function GetInsetStr: TRVAnsiString;
  begin

    Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
      'inset="%s,%s,%s,%s"',
      [RVStyle.GetCSSSizeInPoints(BoxProperties.Background.BorderOffsets.Left),
       RVStyle.GetCSSSizeInPoints(BoxProperties.Background.BorderOffsets.Top),
       RVStyle.GetCSSSizeInPoints(BoxProperties.Background.BorderOffsets.Right),
       RVStyle.GetCSSSizeInPoints(BoxProperties.Background.BorderOffsets.Bottom)]);
  end;
  {.......................................................................}


begin
  RVStyle := TCustomRVData(RVData).GetRVStyle;
  RVWrite(Stream, '<w:pict>');
  //if SavingData.ShapeIndex=0 then
    RVWrite(Stream, '<v:shapetype id="_x0000_t202" coordsize="21600,21600" '+
    'o:spt="202" path="m,l,21600r21600,l21600,xe"><v:stroke joinstyle="miter"/>'+
    '<v:path gradientshapeok="t" o:connecttype="rect"/></v:shapetype>');
  RVWrite(Stream,  {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<v:shape id="TextBox %d" type="#_x0000_t202" style="%s"%s %s %s>%s',
      [SavingData.ShapeIndex, GetShapeCSS, GetAllowInCellStr, GetFillStr, GetBorderStr,
      GetBorderElStr]));
  RVWrite(Stream, '<v:textbox ');
  if BoxProperties.HeightType=rvbhtAuto then
    RVWrite(Stream, 'style="mso-fit-shape-to-text:t" ');
  RVWrite(Stream, GetInsetStr+'><w:txbxContent>');
  Document.SaveOOXMLDocumentToStream(Stream, '', False, False,
      False, Hidden or HiddenParent, clNone, nil, SavingData);
  RVWrite(Stream, '</w:txbxContent></v:textbox></v:shape></w:pict>');
end;
{------------------------------------------------------------------------------}
type
  TRVBoxXMLProperty = (rvbxmlAnchorH, rvbxmlAlignH, rvbxmlAnchorV, rvbxmlAlignV,
    rvbxmlWidthEMU, rvbxmlHeightEMU, rvbxmlFill, rvbxmlCmpd, rvbxmlBorder, rvbxmlAutoFit,
    rvbxmlSizeRelH, rvbxmlSizeRelV, rvbxmlContentAnchor);

procedure TRVSidenoteItemInfo.SaveOOXMLDrawing(Stream: TStream; RVData:
  TPersistent; ItemNo: Integer; SavingData: TRVDocXSavingData; HiddenParent: Boolean);
var RVStyle: TRVStyle;
  {..................................................................}
  function GetPropValue(Prop: TRVBoxXMLProperty): TRVAnsiString;
  begin
    case Prop of
      rvbxmlAnchorH:
        case BoxPosition.HorizontalAnchor of
          rvhanCharacter:    Result := 'character';
          rvhanPage:         Result := 'page';
          rvhanMainTextArea: Result := 'margin';
          rvhanLeftMargin:   Result := 'leftMargin';
          rvhanRightMargin:  Result := 'rightMargin';
          rvhanInnerMargin:  Result := 'insideMargin';
          else {rvhanOuterMargin:} Result := 'outsideMargin';
        end;
      rvbxmlAlignH:
        case BoxPosition.HorizontalAlignment of
          rvfphalLeft:   Result := 'left';
          rvfphalCenter: Result := 'center';
          else {rvfphalRight:} Result := 'right';
        end;
      rvbxmlAnchorV:
        case BoxPosition.VerticalAnchor of
          rvvanLine:         Result := 'line';
          rvvanParagraph:
            if BoxPosition.VerticalPositionKind<>rvfpkAlignment then
              Result := 'paragraph'
            else
              Result := 'line'; // alignment to paragraph is not supported by MS Word
          rvvanPage:         Result := 'page';
          rvvanMainTextArea: Result := 'margin';
          rvvanTopMargin:    Result := 'topMargin';
          else {rvvanBottomMargin:} Result := 'bottomMargin';
        end;
      rvbxmlAlignV:
        case BoxPosition.VerticalAlignment of
          rvfpvalTop:    Result := 'top';
          rvfpvalCenter: Result := 'center';
          else {rvfpvalBottom:} Result := 'bottom';
        end;
      rvbxmlWidthEMU:
        if BoxProperties.WidthType=rvbwtAbsolute then
          Result := RVIntToStr(RVStyle.GetAsEMU(BoxProperties.Width))
        else
          Result := RVIntToStr(RVStyle.GetAsEMU(RVStyle.PixelsToUnits(100)));
      rvbxmlHeightEMU:
        if BoxProperties.HeightType=rvbhtAbsolute then
          Result := RVIntToStr(RVStyle.GetAsEMU(BoxProperties.Height))
        else
          Result := RVIntToStr(RVStyle.GetAsEMU(RVStyle.PixelsToUnits(100)));
      rvbxmlFill:
        if BoxProperties.Background.Color=clNone then
          Result := '<a:noFill/>'
        else
          Result := '<a:solidFill><a:srgbClr val="'+RV_GetColorHex(BoxProperties.Background.Color, False)+'"/></a:solidFill>';
      rvbxmlCmpd:
        case BoxProperties.Border.Style of
          rvbDouble: Result := 'dbl';
          rvbTriple: Result := 'tri';
          rvbThickInside: Result := 'thinThick';
          rvbThickOutside: Result := 'thickThin'
          else Result := 'sng';
        end;
      rvbxmlBorder:
        if (BoxProperties.Border.Style=rvbNone) or (BoxProperties.Border.Width=0) or
          (BoxProperties.Border.Color=clNone) then
          Result := '<a:ln><a:noFill/></a:ln>'
        else
          Result := {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
            '<a:ln w="%d" cmpd="%s"><a:solidFill><a:srgbClr val="%s"/></a:solidFill>'+
            '<a:miter lim="800000"/><a:headEnd/><a:tailEnd/></a:ln>',
            [RVStyle.GetAsEMU(BoxProperties.Border.GetTotalWidth),
             GetPropValue(rvbxmlCmpd),
             RV_GetColorHex(BoxProperties.Border.Color, False)]);
      rvbxmlAutoFit:
        if BoxProperties.HeightType=rvbhtAuto then
          Result := '<a:spAutoFit/>'
        else
          Result := '';
      rvbxmlSizeRelH:
        begin
          case BoxProperties.WidthType of
            rvbwtRelPage:         Result := 'page';
            rvbwtRelMainTextArea: Result := 'margin';
            rvbwtRelLeftMargin:   Result := 'leftMargin';
            rvbwtRelRightMargin:  Result := 'rightMargin';
            rvbwtRelInnerMargin:  Result := 'insideMargin';
            rvbwtRelOuterMargin:  Result := 'outsideMargin';
            else Result := '';
          end;
          if Result<>'' then
            Result := '<wp14:sizeRelH relativeFrom="'+Result+'"><wp14:pctWidth>'+
              RVIntToStr(BoxProperties.Width)+'</wp14:pctWidth></wp14:sizeRelH>';
        end;
      rvbxmlSizeRelV:
        begin
          case BoxProperties.HeightType of
            rvbhtRelPage:         Result := 'page';
            rvbhtRelMainTextArea: Result := 'margin';
            rvbhtRelTopMargin:    Result := 'topMargin';
            rvbhtRelBottomMargin: Result := 'bottomMargin';
            else Result := '';
          end;
          if Result<>'' then
            Result := '<wp14:sizeRelV relativeFrom="'+Result+'"><wp14:pctHeight>'+
              RVIntToStr(BoxProperties.Height)+'</wp14:pctHeight></wp14:sizeRelV>';
        end;
      rvbxmlContentAnchor:
        case BoxProperties.VAlign of
          tlCenter: Result := 'ctr';
          tlBottom: Result := 'b';
          else {tlTop:} Result := 't';
        end;
    end;
  end;
  {..................................................................}
  function GetPositionHStr: TRVAnsiString;
  var IsPercent: Boolean;
  begin
    IsPercent := (BoxPosition.HorizontalPositionKind=rvfpkPercentPosition) and
      (BoxPosition.HorizontalAnchor<>rvhanCharacter); // MS Word does not support % position relative to character
    if IsPercent then
      Result := '<mc:AlternateContent><mc:Choice Requires="wp14">'
    else
      Result := '';
    Result := Result + '<wp:positionH relativeFrom="'+GetPropValue(rvbxmlAnchorH)+'">';
    case BoxPosition.HorizontalPositionKind of
      rvfpkAlignment:
        Result := Result + '<wp:align>'+GetPropValue(rvbxmlAlignH)+'</wp:align>';
      rvfpkAbsPosition:
        Result := Result + '<wp:posOffset>'+RVIntToStr(RVStyle.GetAsEMU(BoxPosition.HorizontalOffset))+'</wp:posOffset>';
      rvfpkPercentPosition:
        if IsPercent then
          Result := Result + '<wp14:pctPosHOffset>'+RVIntToStr(BoxPosition.HorizontalOffset)+'</wp14:pctPosHOffset>';
        else
          Result := Result + '<wp:posOffset>0</wp:posOffset>';
    end;
    Result := Result + '</wp:positionH>';
    if IsPercent then
      Result := Result+ '</mc:Choice><mc:Fallback><wp:positionH relativeFrom="'+
       GetPropValue(rvbxmlAnchorH)+'"><wp:posOffset>0</wp:posOffset></wp:positionH>'+
       '</mc:Fallback></mc:AlternateContent>';
  end;
  {..................................................................}
  function GetPositionVStr: TRVAnsiString;
  var IsPercent: Boolean;
  begin
    IsPercent := (BoxPosition.VerticalPositionKind=rvfpkPercentPosition) and
      not (BoxPosition.VerticalAnchor in [rvvanLine,rvvanParagraph]); // MS Word does not support % position relative to line or para
    if IsPercent then
      Result := '<mc:AlternateContent><mc:Choice Requires="wp14">'
    else
      Result := '';
    Result := Result + '<wp:positionV relativeFrom="'+GetPropValue(rvbxmlAnchorV)+'">';
    case BoxPosition.VerticalPositionKind of
      rvfpkAlignment:
        Result := Result + '<wp:align>'+GetPropValue(rvbxmlAlignV)+'</wp:align>';
      rvfpkAbsPosition:
        Result := Result + '<wp:posOffset>'+RVIntToStr(RVStyle.GetAsEMU(BoxPosition.VerticalOffset))+'</wp:posOffset>';
      rvfpkPercentPosition:
        if IsPercent then
          Result := Result + '<wp14:pctPosVOffset>'+RVIntToStr(BoxPosition.VerticalOffset)+'</wp14:pctPosVOffset>'
        else
          Result := Result + '<wp:posOffset>0</wp:posOffset>';
    end;
    Result := Result + '</wp:positionV>';
    if IsPercent then
      Result := Result+ '</mc:Choice><mc:Fallback><wp:positionV relativeFrom="'+
       GetPropValue(rvbxmlAnchorV)+'"><wp:posOffset>0</wp:posOffset></wp:positionV>'+
       '</mc:Fallback></mc:AlternateContent>';
  end;
  {..................................................................}
begin
  RVStyle := TCustomRVData(RVData).GetRVStyle;
  RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<w:drawing><wp:anchor distT="45720" distB="45720" distL="114300" distR="114300" '+
    'simplePos="0" relativeHeight="251659264" behindDoc="%d" locked="0" layoutInCell="%d" allowOverlap="1">'+
    '<wp:simplePos x="0" y="0"/>%s%s<wp:extent cx="%s" cy="%s"/>'+
    '<wp:effectExtent l="0" t="0" r="0" b="0"/><wp:wrapNone/>'+
    '<wp:docPr id="%d" name="TextBox %d"/>',
    [RTF_GetBelowText, ord(BoxPosition.RelativeToCell),
     GetPositionHStr, GetPositionVStr,
     GetPropValue(rvbxmlWidthEMU), GetPropValue(rvbxmlHeightEMU),
     SavingData.ShapeIndex, SavingData.ShapeIndex]));
  RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '<a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">'+
    '<a:graphicData uri="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">'+
    '<wps:wsp><wps:cNvSpPr txBox="1"><a:spLocks noChangeArrowheads="1"/></wps:cNvSpPr>'+
    '<wps:spPr bwMode="auto"><a:xfrm><a:off x="0" y="0"/><a:ext cx="%s" cy="%s"/></a:xfrm>'+
    '<a:prstGeom prst="rect"><a:avLst/></a:prstGeom>%s%s</wps:spPr>'+
    '<wps:txbx><w:txbxContent>',
    [GetPropValue(rvbxmlWidthEMU), GetPropValue(rvbxmlHeightEMU),
     GetPropValue(rvbxmlFill), GetPropValue(rvbxmlBorder)]));
  Document.SaveOOXMLDocumentToStream( Stream, '', False, False,
      False, Hidden or HiddenParent, clNone, nil, SavingData);
  RVWrite(Stream, {$IFDEF RVUNICODESTR}AnsiStrings.{$ENDIF}Format(
    '</w:txbxContent></wps:txbx><wps:bodyPr rot="0" vert="horz" wrap="square" '+
    'lIns="%d" tIns="%d" rIns="%d" bIns="%d" anchor="%s" anchorCtr="0">%s</wps:bodyPr>'+
    '</wps:wsp></a:graphicData></a:graphic>',
    [RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Left),
     RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Top),
     RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Right),
     RVStyle.GetAsEMU(BoxProperties.Background.BorderOffsets.Bottom),
     GetPropValue(rvbxmlContentAnchor), GetPropValue(rvbxmlAutoFit)]));
  RVWrite(Stream, GetPropValue(rvbxmlSizeRelH)+GetPropValue(rvbxmlSizeRelV));
  RVWrite(Stream, '</wp:anchor></w:drawing>');
end;
{$ENDIF}
{================================ TRVFloatingBoxItemInfo =====================}
constructor TRVTextBoxItemInfo.Create(RVData: TPersistent);
begin
  inherited Create(RVData);
  Init(TCustomRVData(RVData));
end;
{------------------------------------------------------------------------------}
constructor TRVTextBoxItemInfo.CreateEx(RVData: TPersistent; ATextStyleNo,
  AStartFrom: Integer; AReset: Boolean);
begin
  inherited CreateEx(RVData, 0, AStartFrom, AReset);
  Init(TCustomRVData(RVData));
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.Init(RVData: TCustomRVData);
begin
  StyleNo := rvsTextBox;
  SeqName := RV_TEXTBOX_SEQNAME;
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.OnDocWidthChange(DocWidth: Integer;
  dli: TRVDrawLineInfo; Printing: Boolean; Canvas: TCanvas; RVData: TPersistent;
  sad: PRVScreenAndDevice; var HShift, Desc: Integer; NoCaching, Reformatting,
  UseFormatCanvas: Boolean);
begin
  dli.Width := 0;
  dli.Height := 1;
  HShift := 0;
  Desc := 0;
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.Paint(x, y: Integer; Canvas: TCanvas;
  State: TRVItemDrawStates; dli: TRVDrawLineInfo; RVData: TPersistent);
var RVStyle: TRVStyle;
   Color : TColor;
begin
  RVStyle := TCustomRVData(RVData).GetRVStyle;
  if RVStyle.SpecialCharactersColor<>clNone then
    Color := RVStyle.SpecialCharactersColor
  else if RVStyle.FloatingLineColor<>clNone then
    Color := RVStyle.FloatingLineColor
  else
    Color := clBtnShadow;
  if (rvidsCurrent in State) and (RVStyle.CurrentItemColor<>clNone) then
    Color := RVStyle.CurrentItemColor;

  Canvas.Brush.Color := Color;
  Canvas.Brush.Style := bsSolid;
  Canvas.Pen.Color := Color;
  Canvas.Pen.Style := psSolid;
  RVStyle.GraphicInterface.Line(Canvas, X, Y-5, X, Y+dli.Height);
  RVStyle.GraphicInterface.Rectangle(Canvas, X-5, Y-5, X, Y);
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.Print(Canvas: TCanvas; x, y, x2: Integer;
  Preview, Correction: Boolean; const sad: TRVScreenAndDevice;
  RichView: TRVScroller; dli: TRVDrawLineInfo; Part: Integer;
  ColorMode: TRVColorMode; RVData: TPersistent; PageNo: Integer);
begin
  // no printing
end;
{------------------------------------------------------------------------------}
function TRVTextBoxItemInfo.GetBoolValue(Prop: TRVItemBoolProperty): Boolean;
begin
  case Prop of
    rvbpPlaceholder, rvbpDocXOnlyBeforeAfter:
      Result := True;
    else
      Result := inherited GetBoolValue(Prop);
  end;
end;
{------------------------------------------------------------------------------}
function TRVTextBoxItemInfo.GetNoteText: String;
begin
  Result := '?';
end;
{------------------------------------------------------------------------------}
function TRVTextBoxItemInfo.GetAssociatedTextStyleNo: Integer;
begin
  Result := -1;
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.SetAssociatedTextStyleNo(Value: Integer);
begin

end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.UpdateStyles(Data: TRVDeleteUnusedStylesData);
begin
  if (ParaNo>=0) and (ParaNo<Data.UsedParaStyles.Count) then
    dec(ParaNo, Data.UsedParaStyles[ParaNo]-1);
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.MarkStylesInUse(Data: TRVDeleteUnusedStylesData);
begin
  if (ParaNo>=0) and (ParaNo<Data.UsedParaStyles.Count) then
    Data.UsedParaStyles[ParaNo] := 1;
end;
{------------------------------------------------------------------------------}
procedure TRVTextBoxItemInfo.ApplyStyleConversion(RVData: TPersistent; ItemNo,
  UserData: Integer; ConvType: TRVEStyleConversionType; Recursive: Boolean);
begin

end;
{------------------------------------------------------------------------------}
function TRVTextBoxItemInfo.GetSubDocRTFDocType: TRVRTFDocType;
begin
  Result := rvrtfdtTextBox;
end;

initialization

  RegisterRichViewItemClass(rvsSidenote, TRVSidenoteItemInfo);
  RegisterRichViewItemClass(rvsTextBox, TRVTextBoxItemInfo);

{$ENDIF}

end.
