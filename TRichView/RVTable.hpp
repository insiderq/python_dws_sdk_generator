﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVTable.pas' rev: 27.00 (Windows)

#ifndef RvtableHPP
#define RvtableHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVItem.hpp>	// Pascal unit
#include <RVDataList.hpp>	// Pascal unit
#include <CRVData.hpp>	// Pascal unit
#include <CRVFData.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <RVFuncs.hpp>	// Pascal unit
#include <RVRVData.hpp>	// Pascal unit
#include <RVERVData.hpp>	// Pascal unit
#include <DLines.hpp>	// Pascal unit
#include <RVBack.hpp>	// Pascal unit
#include <RVMarker.hpp>	// Pascal unit
#include <RVSeqItem.hpp>	// Pascal unit
#include <RVNote.hpp>	// Pascal unit
#include <RVDocX.hpp>	// Pascal unit
#include <RVSelectionHandles.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVEdit.hpp>	// Pascal unit
#include <RVFMisc.hpp>	// Pascal unit
#include <PtblRV.hpp>	// Pascal unit
#include <PtRVData.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <RVUni.hpp>	// Pascal unit
#include <RVUndo.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit
#include <RVCtrlData.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvtable
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVCellHAlign : unsigned char { rvcLeft, rvcCenter, rvcRight };

enum DECLSPEC_DENUM TRVCellVAlign : unsigned char { rvcTop, rvcMiddle, rvcBottom, rvcVDefault };

enum DECLSPEC_DENUM TRVTableOption : unsigned char { rvtoEditing, rvtoRowSizing, rvtoColSizing, rvtoRowSelect, rvtoColSelect, rvtoNoCellSelect, rvtoRTFSaveCellPixelBestWidth, rvtoRTFAllowAutofit, rvtoHideGridLines, rvtoOverlappingCorners, rvtoCellBelowBorders, rvtoIgnoreContentWidth, rvtoIgnoreContentHeight };

typedef System::Set<TRVTableOption, TRVTableOption::rvtoEditing, TRVTableOption::rvtoIgnoreContentHeight> TRVTableOptions;

enum DECLSPEC_DENUM TRVTablePrintOption : unsigned char { rvtoHalftoneBorders, rvtoRowsSplit, rvtoWhiteBackground, rvtoContinue };

typedef System::Set<TRVTablePrintOption, TRVTablePrintOption::rvtoHalftoneBorders, TRVTablePrintOption::rvtoContinue> TRVTablePrintOptions;

enum DECLSPEC_DENUM TRVTableBorderStyle : unsigned char { rvtbRaised, rvtbLowered, rvtbColor, rvtbRaisedColor, rvtbLoweredColor };

typedef int TRVHTMLLength;

class DELPHICLASS TRVTableItemInfo;
typedef void __fastcall (__closure *TRVCellEditingEvent)(TRVTableItemInfo* Sender, int Row, int Col, bool Automatic, bool &AllowEdit);

typedef void __fastcall (__closure *TRVCellEndEditEvent)(TRVTableItemInfo* Sender, int Row, int Col, bool Clearing);

class DELPHICLASS TRVTablePaintItemPart;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTablePaintItemPart : public Rvitem::TRVPaintItemPart
{
	typedef Rvitem::TRVPaintItemPart inherited;
	
public:
	int StartRow;
	int LastRow;
public:
	/* TObject.Create */ inline __fastcall TRVTablePaintItemPart(void) : Rvitem::TRVPaintItemPart() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVTablePaintItemPart(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVTableCellData;
class DELPHICLASS TRVTableRow;
class PASCALIMPLEMENTATION TRVTableCellData : public Rvdatalist::TRVItemFormattedData
{
	typedef Rvdatalist::TRVItemFormattedData inherited;
	
private:
	System::Uitypes::TColor FColor;
	TRVCellVAlign FVAlign;
	TRVHTMLLength FBestWidth;
	Rvstyle::TRVStyleLength FBestHeight;
	int FRowSpan;
	int FColSpan;
	int FLeft;
	int FTop;
	int FWidth;
	int FHeight;
	Rvstyle::TRVBooleanRect* FVisibleBorders;
	Crvfdata::TCustomRVFormattedData* FChosenRVData;
	Rvitem::TCustomRVItemInfo* FChosenItem;
	Rvundo::TRVUndoInfo* ContainerUndoItem;
	System::Uitypes::TColor FBorderColor;
	System::Uitypes::TColor FBorderLightColor;
	Rvback::TRVBackground* FBackground;
	System::UnicodeString FBackgroundImageFileName;
	Rvstyle::TRVDocRotation FRotation;
	System::UnicodeString FHint;
	Rvstyle::TRVTag FTag;
	void __fastcall SetBestHeight(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetBestWidth(const TRVHTMLLength Value);
	void __fastcall SetVisibleBorders(Rvstyle::TRVBooleanRect* const Value);
	bool __fastcall CanClear(void);
	void __fastcall SetColor(const System::Uitypes::TColor Value);
	Vcl::Graphics::TGraphic* __fastcall GetBackgroundImage(void);
	Rvstyle::TRVItemBackgroundStyle __fastcall GetBackgroundStyle(void);
	void __fastcall SetBackgroundImage_(Vcl::Graphics::TGraphic* const Value, bool Copy);
	void __fastcall SetBackgroundImage(Vcl::Graphics::TGraphic* const Value);
	void __fastcall SetBackgroundStyle(const Rvstyle::TRVItemBackgroundStyle Value);
	void __fastcall BackgroundImageWriter(System::Classes::TStream* Stream);
	void __fastcall BackgroundImageReader(System::Classes::TStream* Stream);
	bool __fastcall StoreVisibleBorders(void);
	bool __fastcall StoreTag(void);
	
protected:
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall AssignSizeFrom(TRVTableCellData* Cell);
	DYNAMIC bool __fastcall SupportsPageBreaks(void);
	virtual Rvscroll::TRVOptions __fastcall GetOptions(void);
	DYNAMIC void __fastcall DataWriter(System::Classes::TStream* Stream);
	bool __fastcall ContentAffectsWidth(void);
	bool __fastcall ContentAffectsHeight(void);
	
public:
	DYNAMIC bool __fastcall KeepTogether(void);
	virtual Rvstyle::TRVDocRotation __fastcall GetRotation(void);
	TRVCellVAlign __fastcall GetRealVAlign(void);
	virtual Rvfuncs::TRVVerticalAlign __fastcall GetVerticalAlign(void);
	virtual void __fastcall GetItemBackground(int ItemNo, const System::Types::TRect &r, bool MakeImageRect, System::Uitypes::TColor &Color, Vcl::Graphics::TBitmap* &bmp, bool &UseBitmap, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowPictures, bool AllowThumbnails);
	DYNAMIC void __fastcall ResetSubCoords(void);
	virtual Rvback::TRVBackground* __fastcall GetBackground(void);
	int __fastcall GetExtraVOffs(void);
	bool __fastcall IsTransparent(void);
	DYNAMIC void __fastcall GetParentInfo(int &ParentItemNo, Rvitem::TRVStoreSubRVData* &Location);
	TRVTableItemInfo* __fastcall GetTable(void);
	DYNAMIC System::UnicodeString __fastcall GetItemHint(Crvdata::TCustomRVData* RVData, int ItemNo, const System::UnicodeString UpperRVDataHint);
	void __fastcall AssignAttributesFrom(TRVTableCellData* Cell, bool IncludeSize, int DivColSpan, int DivRowSpan);
	DYNAMIC void __fastcall Deselect(Rvitem::TCustomRVItemInfo* NewPartiallySelected, bool MakeEvent);
	DYNAMIC Vcl::Controls::TWinControl* __fastcall GetEditor(void);
	virtual void __fastcall GetOrigin(int &ALeft, int &ATop);
	DYNAMIC void __fastcall GetOriginEx(int &ALeft, int &ATop);
	virtual int __fastcall GetWidth(void);
	virtual int __fastcall GetHeight(void);
	virtual System::Uitypes::TColor __fastcall GetColor(void);
	virtual int __fastcall GetHOffs(void);
	virtual int __fastcall GetVOffs(void);
	virtual int __fastcall GetAreaWidth(void);
	virtual int __fastcall GetAreaHeight(void);
	DYNAMIC void __fastcall AssignChosenRVData(Crvfdata::TCustomRVFormattedData* RVData, Rvitem::TCustomRVItemInfo* Item);
	DYNAMIC void __fastcall UnassignChosenRVData(Crvdata::TCustomRVData* RVData);
	DYNAMIC Crvdata::TCustomRVData* __fastcall GetChosenRVData(void);
	DYNAMIC Rvitem::TCustomRVItemInfo* __fastcall GetChosenItem(void);
	void __fastcall MovingToUndoList(Rvundo::TRVUndoInfo* AContainerUndoItem);
	void __fastcall MovingFromUndoList(void);
	int __fastcall GetCellHeight(void);
	int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	bool __fastcall HasData(bool CheckStyles);
	virtual Crvdata::TCustomRVData* __fastcall GetRVData(void);
	DYNAMIC void __fastcall DoSelect(void);
	DYNAMIC void __fastcall ControlAction(Crvdata::TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Rvitem::TCustomRVItemInfo* Item);
	DYNAMIC void __fastcall ControlAction2(Crvdata::TCustomRVData* RVData, Rvstyle::TRVControlAction ControlAction, int ItemNo, Vcl::Controls::TControl* &Control);
	virtual void __fastcall ItemAction(Rvstyle::TRVItemAction ItemAction, Rvitem::TCustomRVItemInfo* Item, Rvtypes::TRVRawByteString &Text, Crvdata::TCustomRVData* RVData);
	DYNAMIC void __fastcall AdjustFocus(int NewFocusedItemNo, System::Classes::TPersistent* TopLevelRVData, int TopLevelItemNo);
	__fastcall TRVTableCellData(TRVTableRow* ARow);
	__fastcall virtual ~TRVTableCellData(void);
	DYNAMIC Rvmarker::TRVMarkerList* __fastcall GetMarkers(bool AllowCreate);
	DYNAMIC Rvseqitem::TRVSeqList* __fastcall GetSeqList(bool AllowCreate);
	DYNAMIC Crvdata::TCustomRVData* __fastcall Edit(void);
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool ConvertItems);
	__property int Left = {read=FLeft, nodefault};
	__property int Top = {read=FTop, nodefault};
	__property int Height = {read=FHeight, nodefault};
	__property int Width = {read=FWidth, nodefault};
	__property int ColSpan = {read=FColSpan, nodefault};
	__property int RowSpan = {read=FRowSpan, nodefault};
	
__published:
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=536870911};
	__property System::Uitypes::TColor BorderColor = {read=FBorderColor, write=FBorderColor, default=536870911};
	__property System::Uitypes::TColor BorderLightColor = {read=FBorderLightColor, write=FBorderLightColor, default=536870911};
	__property TRVHTMLLength BestWidth = {read=FBestWidth, write=SetBestWidth, default=0};
	__property Rvstyle::TRVStyleLength BestHeight = {read=FBestHeight, write=SetBestHeight, default=0};
	__property Rvstyle::TRVBooleanRect* VisibleBorders = {read=FVisibleBorders, write=SetVisibleBorders, stored=StoreVisibleBorders};
	__property TRVCellVAlign VAlign = {read=FVAlign, write=FVAlign, default=3};
	__property Vcl::Graphics::TGraphic* BackgroundImage = {read=GetBackgroundImage, write=SetBackgroundImage, stored=false};
	__property Rvstyle::TRVItemBackgroundStyle BackgroundStyle = {read=GetBackgroundStyle, write=SetBackgroundStyle, default=0};
	__property System::UnicodeString BackgroundImageFileName = {read=FBackgroundImageFileName, write=FBackgroundImageFileName};
	__property System::UnicodeString Hint = {read=FHint, write=FHint};
	__property Rvstyle::TRVDocRotation Rotation = {read=FRotation, write=FRotation, default=0};
	__property Rvstyle::TRVTag Tag = {read=FTag, write=FTag, stored=StoreTag};
};


class DELPHICLASS TRVTableRows;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTableRow : public Rvdatalist::TRVDataList
{
	typedef Rvdatalist::TRVDataList inherited;
	
public:
	TRVTableCellData* operator[](int Index) { return Items[Index]; }
	
private:
	TRVCellVAlign FVAlign;
	bool FPageBreakBefore;
	bool FKeepTogether;
	HIDESBASE TRVTableCellData* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVTableCellData* const Value);
	void __fastcall InsertEmpty(int Index);
	void __fastcall InsertPointer(int Index, TRVTableCellData* Item);
	
protected:
	TRVTableRows* FRows;
	virtual Crvdata::TCustomRVData* __fastcall GetParentRVData(void);
	bool __fastcall HasCellsInRange(int Index, int RangeStart, int Count);
	
public:
	__fastcall TRVTableRow(int nCols, TRVTableRows* ARows, Crvdata::TCustomRVData* MainRVData);
	HIDESBASE TRVTableCellData* __fastcall Add(void);
	HIDESBASE TRVTableCellData* __fastcall Insert(int Index);
	int __fastcall GetHeight(void);
	Rvstyle::TRVStyleLength __fastcall GetBestHeight(void);
	__property TRVCellVAlign VAlign = {read=FVAlign, write=FVAlign, default=0};
	__property bool PageBreakBefore = {read=FPageBreakBefore, write=FPageBreakBefore, default=0};
	__property bool KeepTogether = {read=FKeepTogether, write=FKeepTogether, default=0};
	__property TRVTableCellData* Items[int Index] = {read=Get, write=Put/*, default*/};
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVTableRow(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTableRows : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	TRVTableRow* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVTableRow* __fastcall Get(int Index);
	HIDESBASE void __fastcall Put(int Index, TRVTableRow* const Value);
	int __fastcall GetMinColWidth(int Col, Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas);
	int __fastcall GetPercentColWidth(int Col, int TableWidth);
	bool __fastcall IsPercentWidthColumn(int Col);
	Rvstyle::TRVStyleLength __fastcall GetRVSLengthColWidth(int Col);
	bool __fastcall StartMergeCells(int TopRow, int LeftCol, int &ColSpan, int &RowSpan);
	
protected:
	Crvdata::TCustomRVData* FMainRVData;
	TRVTableItemInfo* FTable;
	int __fastcall GetColCount(void);
	bool __fastcall IsEmptyRows(int TopRow, int LeftCol, int ColSpan, int RowSpan, int TopRow2, int RowSpan2);
	bool __fastcall IsEmptyCols(int TopRow, int LeftCol, int ColSpan, int RowSpan, int LeftCol2, int ColSpan2);
	int __fastcall GetBestWidth(int TopRow, int LeftCol, int ColSpan, int RowSpan);
	void __fastcall UnmergeCell(int Row, int Col, bool UnmergeRows, bool UnmergeCols);
	void __fastcall UnmergeCells(int TopRow, int LeftCol, int ColSpan, int RowSpan, bool UnmergeRows, bool UnmergeCols);
	void __fastcall InsertRows(int Index, int Count, int CopyIndex, bool DivideHeights);
	void __fastcall InsertCols(int Index, int Count, int CopyIndex, bool DivideWidths);
	void __fastcall DeleteRows(int Index, int Count, bool DecreaseHeight);
	void __fastcall DeleteCols(int Index, int Count, bool DecreaseWidth);
	int __fastcall SplitCellVertically(int Row, int Col, int ColCount);
	int __fastcall SplitCellsVertically(int TopRow, int LeftCol, int ColSpan, int RowSpan, int ColCount);
	int __fastcall SplitCellHorizontally(int Row, int Col, int RowCount);
	int __fastcall SplitCellsHorizontally(int TopRow, int LeftCol, int ColSpan, int RowSpan, int RowCount);
	void __fastcall MovingToUndoList(int Row, int Col, int ColSpan, int RowSpan, Rvundo::TRVUndoInfo* AContainerUndoItem);
	void __fastcall MovingFromUndoList(int Row, int Col, int ColSpan, int RowSpan);
	void __fastcall Do_BeforeInsertRows(int ItemNo, int Row, int Count);
	void __fastcall Do_InsertRows(int Row, int Count);
	void __fastcall Do_UnInsertRows(int Row, int Count);
	void __fastcall Do_BeforeInsertCell(int ItemNo, int Row, int Col);
	void __fastcall Do_BeforeSpreadOverEmptyCells(int ItemNo, int Row, int Col, int ColSpan);
	void __fastcall Do_SpreadOverEmptyCells(int Row, int Col, int ColSpan);
	void __fastcall Do_UnSpreadOverEmptyCells(int Row, int Col, int ColSpan);
	void __fastcall Do_SetSpan(int ItemNo, int Row, int Col, int Span, bool IsColSpan);
	void __fastcall Do_BeforeFreeEmptyCells(int ItemNo, int Row, int Col, int ColSpan, int RowSpan);
	void __fastcall Do_FreeEmptyCells(int Row, int Col, int ColSpan, int RowSpan);
	void __fastcall Do_UnFreeEmptyCells(int Row, int Col, int ColSpan, int RowSpan);
	void __fastcall Do_ChangeEmptyCellStyles(int ItemNo, int Row, int Col, int StyleNo, int ParaNo);
	void __fastcall Do_BeforeInsertEmptyCells(int ItemNo, int Row, int Col, int ColCount, int RowCount);
	void __fastcall Do_InsertEmptyCells(int Row, int Col, int ColCount, int RowCount);
	void __fastcall Do_UnInsertEmptyCells(int Row, int Col, int ColCount, int RowCount);
	void __fastcall Do_BeforeSplitCellHorz(int ItemNo, int Row, int Col, int Row2, bool DecreaseHeight);
	void __fastcall Do_SplitCellHorz(int Row, int Col, int Row2, bool DecreaseHeight);
	void __fastcall Do_UnSplitCellHorz(int Row, int Col, int Row2, int OldBestHeight);
	void __fastcall Do_BeforeSplitCellVert(int ItemNo, int Row, int Col, int Col2, bool DecreaseWidth);
	void __fastcall Do_SplitCellVert(int Row, int Col, int Col2, bool DecreaseWidth);
	void __fastcall Do_UnSplitCellVert(int Row, int Col, int Col2, int OldBestWidth);
	Rvundo::TRVUndoInfo* __fastcall Do_BeforeDeleteRows(int ItemNo, int Row, int Count);
	void __fastcall Do_DeleteRows(int ItemNo, int Row, int Count, Rvundo::TRVUndoInfo* ui);
	void __fastcall Do_UnDeleteRows(int Row, System::Classes::TList* RowList);
	Rvundo::TRVUndoInfo* __fastcall Do_BeforeDeleteCols(int ItemNo, int Col, int Count);
	void __fastcall Do_DeleteCols(int ItemNo, int Col, int Count, Rvundo::TRVUndoInfo* ui);
	void __fastcall Do_UnDeleteCols(int Col, System::Classes::TList* CellList);
	Rvundo::TRVUndoInfo* __fastcall Do_BeforeMergeCells(int ItemNo, int Row, int Col, int ColSpan, int RowSpan);
	void __fastcall Do_MergeCells(int ItemNo, int Row, int Col, int ColSpan, int RowSpan, Rvundo::TRVUndoInfo* ui, bool ChangeBestWidth);
	void __fastcall Do_UndoMergeCells(int ItemNo, int Row, int Col, int OldColSpan, int OldRowSpan, Rvclasses::TRVList* MergedItemsList, TRVHTMLLength OldBestWidth);
	void __fastcall Do_BeforeUnmergeCell(int ItemNo, int Row, int Col, bool UnmergeRows, bool UnmergeCols);
	void __fastcall Do_UnmergeCell(int ItemNo, int Row, int Col, bool UnmergeRows, bool UnmergeCols);
	void __fastcall Do_UndoUnmergeCell(int ItemNo, int Row, int Col, int OldColSpan, int OldRowSpan, TRVHTMLLength OldBestWidth, int OldBestHeight);
	Rvundo::TRVUndoInfo* __fastcall Do_BeforeClearCells(int ItemNo, Rvclasses::TRVIntegerList* RowList, Rvclasses::TRVIntegerList* ColList, System::Classes::TList* &CellsList);
	void __fastcall Do_AfterFillingCells(System::Classes::TList* CellsList, Rvclasses::TRVIntegerList* RowList, Rvclasses::TRVIntegerList* ColList, Rvundo::TRVUndoInfo* ui);
	void __fastcall Do_ClearCells(System::Classes::TList* CellsList, Rvclasses::TRVIntegerList* RowList, Rvclasses::TRVIntegerList* ColList, Rvundo::TRVUndoInfo* ui);
	void __fastcall Do_UnClearCells(System::Classes::TList* CellsList, Rvclasses::TRVIntegerList* RowList, Rvclasses::TRVIntegerList* ColList);
	void __fastcall InsertPointer(int Index, TRVTableRow* Item);
	
public:
	__fastcall TRVTableRows(int nRows, int nCols, Crvdata::TCustomRVData* AMainRVData, TRVTableItemInfo* ATable);
	__fastcall virtual ~TRVTableRows(void);
	void __fastcall MergeCells(int TopRow, int LeftCol, int ColSpan, int RowSpan, bool AllowMergeRC, bool ChangeBestWidth);
	bool __fastcall Empty(void);
	HIDESBASE TRVTableRow* __fastcall Add(int nCols);
	TRVTableCellData* __fastcall GetMainCell(int ARow, int ACol, int &MRow, int &MCol);
	HIDESBASE TRVTableRow* __fastcall Insert(int Index, int nCols);
	void __fastcall Reset(int nRows, int nCols);
	bool __fastcall CanMergeCells(int TopRow, int LeftCol, int ColSpan, int RowSpan, bool AllowMergeRC);
	__property TRVTableRow* Items[int Index] = {read=Get, write=Put/*, default*/};
};

#pragma pack(pop)

struct DECLSPEC_DRECORD TRVTableInplaceParamStorage
{
public:
	bool Stored;
	int StartNo;
	int EndNo;
	int StartOffs;
	int EndOffs;
	int Row;
	int Col;
	Rvitem::TCustomRVItemInfo* PartialSelected;
};


class DELPHICLASS TRVTableItemFormattingInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTableItemFormattingInfo : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Rvclasses::TRVIntegerList* ColWidths;
	Rvclasses::TRVIntegerList* RowHeights;
	Rvclasses::TRVIntegerList* ColStarts;
	Rvclasses::TRVIntegerList* RowStarts;
	int FWidth;
	int FHeight;
	
public:
	Rvclasses::TRVList* Rows;
	__fastcall TRVTableItemFormattingInfo(bool CreateRows);
	__fastcall virtual ~TRVTableItemFormattingInfo(void);
	void __fastcall Clear(void);
	void __fastcall QuickClear(void);
};

#pragma pack(pop)

enum DECLSPEC_DENUM TRVCellDirection : unsigned char { rvcdLeft, rvcdUp, rvcdRight, rvcdDown, rvcdDocTop, rvcdDocBottom, rvcdNext, rvcdPrev };

typedef void __fastcall (__closure *TRVTableDrawBorderEvent)(TRVTableItemInfo* Sender, Vcl::Graphics::TCanvas* Canvas, int Left, int Top, int Right, int Bottom, Rvstyle::TRVStyleLength Width, System::Uitypes::TColor LightColor, System::Uitypes::TColor Color, System::Uitypes::TColor BackgroundColor, TRVTableBorderStyle Style, bool Printing, Rvstyle::TRVBooleanRect* VisibleBorders, int Row, int Col, bool &DoDefault);

enum DECLSPEC_DENUM TRVTableState : unsigned char { rvtsInserted, rvtsEditMode, rvtsModified, rvtsFormatInplace, rvtsVerticalDraggedRule, rvtsDRChangeTableWidth, rvtsJustCreated, rvtsInplaceIsReformatting, rvtsSelExists };

typedef System::Set<TRVTableState, TRVTableState::rvtsInserted, TRVTableState::rvtsSelExists> TRVTableStates;

class DELPHICLASS TRVTableStreamSaveInfo;
class DELPHICLASS TRVTablePrintPart;
class DELPHICLASS TCellPtblRVData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTableStreamSaveInfo : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	TRVTablePrintPart* Part;
	int CellPage;
	TCellPtblRVData* CellPtblRVData;
	int TopRow;
	int LeftCol;
	int RowCount;
	int ColCount;
	bool SaveHeadingRows;
	bool SelectionOnly;
	bool SavePictures;
	bool LoadResult;
	__fastcall TRVTableStreamSaveInfo(TRVTableItemInfo* table);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVTableStreamSaveInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVTableStoredColWidths;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTableStoredColWidths : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	int ppxDevice;
	int TableWidth;
	Rvclasses::TRVIntegerList* ColWidths;
	__fastcall TRVTableStoredColWidths(void);
	__fastcall virtual ~TRVTableStoredColWidths(void);
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TRVTableItemInfo : public Rvitem::TRVFullLineItemInfo
{
	typedef Rvitem::TRVFullLineItemInfo inherited;
	
private:
	TRVTableCellData* FPrintCell;
	TRVTableCellData* FLastCellMovedAbove;
	System::Types::TRect FPrintCellRect;
	TRVCellEditingEvent FOnCellEditing;
	TRVCellEndEditEvent FOnCellEndEdit;
	TRVTableStates FState;
	int CachedItemNo;
	int MyTop;
	int MyLeft;
	int MyClientTop;
	int MyClientLeft;
	int MouseRow;
	int MouseCol;
	int DRMin;
	int DRMax;
	int DRNo;
	int DRCoord;
	int DRDelta;
	TRVTableRows* FRows;
	Rvstyle::TRVStyleLength FCellVSpacing;
	Rvstyle::TRVStyleLength FCellHSpacing;
	Rvstyle::TRVStyleLength FCellHPadding;
	Rvstyle::TRVStyleLength FCellVPadding;
	TRVHTMLLength FBestWidth;
	Rvstyle::TRVStyleLength FBorderWidth;
	System::Uitypes::TColor FBorderColor;
	System::Uitypes::TColor FColor;
	Rvstyle::TRVStyleLength FCellBorderWidth;
	Rvstyle::TRVStyleLength FHRuleWidth;
	Rvstyle::TRVStyleLength FVRuleWidth;
	System::Uitypes::TColor FHRuleColor;
	System::Uitypes::TColor FCellBorderColor;
	System::Uitypes::TColor FVRuleColor;
	TRVTableBorderStyle FBorderStyle;
	TRVTableBorderStyle FCellBorderStyle;
	Rvstyle::TRVStyleLength FBorderHSpacing;
	Rvstyle::TRVStyleLength FBorderVSpacing;
	bool FHOutermostRule;
	bool FVOutermostRule;
	System::Uitypes::TColor FCellBorderLightColor;
	System::Uitypes::TColor FBorderLightColor;
	int FSelStartCol;
	int FSelStartRow;
	int FSelColOffs;
	int FSelRowOffs;
	int BusyCount;
	TRVTableOptions FOptions;
	TRVTablePrintOptions FPrintOptions;
	TRVTableInplaceParamStorage FStoredInplace;
	int FMinWidthPlus;
	int FInplaceMinWidthPlus;
	System::UnicodeString FTextRowSeparator;
	System::UnicodeString FTextColSeparator;
	int FocusedCellRow;
	int FocusedCellCol;
	int ChosenCellRow;
	int ChosenCellCol;
	TRVTableDrawBorderEvent FOnDrawBorder;
	int FHeadingRowCount;
	Rvback::TRVBackground* FBackground;
	System::UnicodeString FBackgroundImageFileName;
	Rvstyle::TRVBooleanRect* FVisibleBorders;
	TRVTableStreamSaveInfo* FStreamSaveInfo;
	TRVTableStoredColWidths* FStoredColWidths;
	bool __fastcall StoreCellPadding(void);
	bool __fastcall StoreCellHPadding(void);
	bool __fastcall StoreCellVPadding(void);
	int __fastcall GetItemNoInRootDocument(void);
	TRVTableCellData* __fastcall GetCells(int Row, int Col);
	void __fastcall SetCells(int Row, int Col, TRVTableCellData* const Value);
	void __fastcall SetBestWidth(const TRVHTMLLength Value);
	int __fastcall GetColNo(int X);
	int __fastcall GetRowNo(int Y);
	int __fastcall GetCrossed(int Coord, Rvclasses::TRVIntegerList* List);
	void __fastcall UpdateCellXCoords(TRVTableItemFormattingInfo* Fmt, bool NoCaching, bool Reformatting, bool &HasRotatedCells);
	void __fastcall FormatVerticalCells(TRVTableItemFormattingInfo* Fmt, bool NoCaching, bool Reformatting);
	void __fastcall UpdateCellYCoords(TRVTableItemFormattingInfo* Fmt);
	int __fastcall GetHorzExtraPix(void);
	void __fastcall InplaceEditorChange(Rvedit::TCustomRichViewEdit* Sender, bool ClearRedo);
	void __fastcall InplaceEditorCaretGetout(Rvedit::TCustomRichViewEdit* Sender, Rvedit::TRVGetOutDirection Direction);
	void __fastcall InplaceEditorMouseDown(Richview::TCustomRichView* Sender, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);
	void __fastcall InplaceEditorMouseUp(Richview::TCustomRichView* Sender, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int ItemNo, int X, int Y);
	void __fastcall InplaceEditorMouseMove(System::TObject* Sender, System::Classes::TShiftState Shift, int X, int Y);
	void __fastcall InplaceEditorDragOver(System::TObject* Sender, System::TObject* Source, int X, int Y, System::Uitypes::TDragState State, bool &Accept);
	void __fastcall InplaceEditorDragDrop(System::TObject* Sender, System::TObject* Source, int X, int Y);
	void __fastcall UpdateInplaceMargins(void);
	void __fastcall UpdateInplaceVerticalAlignment(void);
	void __fastcall DestroyInplace(bool ReformatCell);
	void __fastcall StoreRVSelection(Crvfdata::TCustomRVFormattedData* RVData, TRVTableInplaceParamStorage &storage);
	void __fastcall RestoreRVSelection(Crvfdata::TCustomRVFormattedData* RVData, const TRVTableInplaceParamStorage &storage);
	void __fastcall Init(int nRows, int nCols, Crvdata::TCustomRVData* AMainRVData);
	void __fastcall CellsWriter(System::Classes::TWriter* Writer);
	void __fastcall CellsReader(System::Classes::TReader* Reader);
	void __fastcall ClearTemporal(void);
	void __fastcall SetBorderColor(const System::Uitypes::TColor Value);
	void __fastcall SetBorderHSpacing(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetBorderLightColor(const System::Uitypes::TColor Value);
	void __fastcall SetBorderStyle(const TRVTableBorderStyle Value);
	void __fastcall SetBorderVSpacing(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetBorderWidth(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellBorderColorProp(const System::Uitypes::TColor Value);
	void __fastcall SetCellBorderLightColorProp(const System::Uitypes::TColor Value);
	void __fastcall SetCellBorderWidth(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellHSpacing(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellHPadding(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellVPadding(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellPadding(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellVSpacing(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetColor(const System::Uitypes::TColor Value);
	void __fastcall SetHOutermostRule(const bool Value);
	void __fastcall SetHRuleColor(const System::Uitypes::TColor Value);
	void __fastcall SetHRuleWidth(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetVOutermostRule(const bool Value);
	void __fastcall SetVRuleColor(const System::Uitypes::TColor Value);
	void __fastcall SetVRuleWidth(const Rvstyle::TRVStyleLength Value);
	void __fastcall SetCellBorderStyle(const TRVTableBorderStyle Value);
	void __fastcall SetIntProperty(const System::UnicodeString PropertyName, int Value, bool AffectSize, bool AffectWidth);
	void __fastcall SetCellIntProperty(int ItemNo, const System::UnicodeString PropertyName, int Value, int Row, int Col, bool AffectSize, bool AffectWidth);
	void __fastcall SetCellStrProperty(int ItemNo, const System::UnicodeString PropertyName, const System::UnicodeString Value, int Row, int Col);
	bool __fastcall IsFixedWidthTable(void);
	bool __fastcall CompletelySelected(void);
	void __fastcall UnAssignActiveCell(void);
	bool __fastcall DoOnCellEditing(int Row, int Col, bool Automatic);
	bool __fastcall IsInEditor(void);
	void __fastcall SetHeadingRowCount(const int Value);
	Vcl::Graphics::TGraphic* __fastcall GetBackgroundImage(void);
	void __fastcall SetBackgroundImage_(Vcl::Graphics::TGraphic* const Value, bool Copy);
	void __fastcall SetBackgroundImage(Vcl::Graphics::TGraphic* const Value);
	Rvstyle::TRVItemBackgroundStyle __fastcall GetBackgroundStyle(void);
	void __fastcall SetBackgroundStyle(const Rvstyle::TRVItemBackgroundStyle Value);
	void __fastcall BackgroundImageWriter(System::Classes::TStream* Stream);
	void __fastcall BackgroundImageReader(System::Classes::TStream* Stream);
	void __fastcall ResetLiveSpell(void);
	void __fastcall SetVisibleBorders(Rvstyle::TRVBooleanRect* const Value);
	bool __fastcall StoreVisibleBorders(void);
	void __fastcall DoSaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, bool SelectionOnly, Rvstyle::PRVScreenAndDevice PSaD);
	int __fastcall GetColCount(void);
	int __fastcall GetRowCount(void);
	Rvstyle::TRVStyleLength __fastcall GetCellPadding(void);
	void __fastcall ExpandColsForMinWidthConstrained(int Row, int Col, int Width, Rvclasses::TRVIntegerList* ColWidths, Rvclasses::TRVIntegerList* &PixColWidths, int &MinColsWidth);
	
protected:
	TRVTableItemFormattingInfo* Fmt;
	Rvstyle::TRVScreenAndDevice *cursad;
	void __fastcall XorDrawing(System::TObject* Sender, System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall DrawBorder(Vcl::Graphics::TCanvas* Canvas, int Left, int Top, int Right, int Bottom, Rvstyle::TRVStyleLength Width, System::Uitypes::TColor LightColor, System::Uitypes::TColor Color, System::Uitypes::TColor BackgroundColor, TRVTableBorderStyle Style, bool DrawEvenEmptyBorder, bool Editing, bool Printing, const System::Types::TRect &ClipRect, Rvstyle::TRVBooleanRect* VisibleBorders, int r, int c, Rvstyle::TRVColorMode ColorMode, Rvstyle::TRVStyle* RVStyle);
	bool __fastcall GetCellAt_(int X, int Y, int &Row, int &Col, bool IgnoreBorders);
	void __fastcall UpdateCellSel(void);
	void __fastcall PaintTo(int Left, int Right, int Top, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Rvstyle::TRVStyle* Style, TRVTableItemFormattingInfo* Fmt, bool UseHeadingRowCount, const System::Types::TRect &ClipRect, Rvstyle::TRVColorMode ColorMode, Ptrvdata::TCustomPrintableRVData* RVData, TRVTablePrintPart* Part, Rvitem::TRVPaintItemPart* PaintPart, int ExtraX, int ExtraY, int PageNo);
	int __fastcall GetDevX(int x);
	int __fastcall GetDevY(int y);
	void __fastcall InternalOnDocWidthChange(int DocWidth, TRVTableItemFormattingInfo* Fmt, Vcl::Graphics::TCanvas* Canvas, bool NoCaching, bool Reformatting, bool UseFormatCanvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
	void __fastcall Change(void);
	void __fastcall ChangeEx(bool ClearRedo);
	int __fastcall BeginModify(int ItemNo);
	void __fastcall EndModify(int ItemNo, int Data);
	System::Uitypes::TColor __fastcall GetTableColor(bool UseParentBackground);
	System::Uitypes::TColor __fastcall GetCellColor(TRVTableCellData* Cell);
	System::Uitypes::TColor __fastcall GetCellColor2(TRVTableCellData* Cell);
	bool __fastcall CanSeeBackgroundThroughCell(TRVTableCellData* Cell);
	bool __fastcall CanSplitAtRow(int Row);
	int __fastcall GetSplitRowBelow(int Row);
	int __fastcall GetSplitRowAbove(int Row);
	bool __fastcall BeforeChange(void);
	bool __fastcall CanChange(void);
	bool __fastcall CanChangeEx(void);
	void __fastcall InitUndo(Rvedit::TRVUndoType UndoType);
	void __fastcall DoneUndo(void);
	void __fastcall AssignCellAttributes(int ItemNo, int Row, int Col, TRVTableCellData* SourceCell, bool IncludeSize, int DivColSpan, int DivRowSpan);
	void __fastcall SetCellBestWidth_(int ItemNo, TRVHTMLLength Value, int Row, int Col);
	void __fastcall SetCellBestHeight_(int ItemNo, Rvstyle::TRVStyleLength Value, int Row, int Col);
	void __fastcall SetCellRotation_(int ItemNo, Rvstyle::TRVDocRotation Value, int Row, int Col);
	void __fastcall SetCellColor_(int ItemNo, System::Uitypes::TColor Value, int Row, int Col);
	void __fastcall SetCellTag_(int ItemNo, const Rvstyle::TRVTag Value, int Row, int Col);
	void __fastcall SetCellBackgroundStyle_(int ItemNo, Rvstyle::TRVItemBackgroundStyle Value, int Row, int Col);
	void __fastcall SetCellBackgroundImageFileName_(int ItemNo, const System::UnicodeString Value, int Row, int Col);
	void __fastcall SetCellHint_(int ItemNo, const System::UnicodeString Value, int Row, int Col);
	void __fastcall SetCellVisibleBorders_(int ItemNo, bool Left, bool Top, bool Right, bool Bottom, int Row, int Col);
	void __fastcall SetCellBorderColor_(int ItemNo, System::Uitypes::TColor Value, int Row, int Col);
	void __fastcall SetCellBorderLightColor_(int ItemNo, System::Uitypes::TColor Value, int Row, int Col);
	void __fastcall SetCellVAlign_(int ItemNo, TRVCellVAlign Value, int Row, int Col);
	void __fastcall SetRowVAlign_(int ItemNo, TRVCellVAlign Value, int Row);
	void __fastcall SetRowPageBreakBefore_(int ItemNo, bool Value, int Row);
	void __fastcall SetRowKeepTogether_(int ItemNo, bool Value, int Row);
	int __fastcall GetEditorItemNoForUndo(void);
	Rvedit::TCustomRichViewEdit* __fastcall CreateTemporalEditor(void);
	void __fastcall ApplyToCells(Rvitem::TRVEStyleConversionType ConvType, int UserData, bool SelectedOnly);
	void __fastcall ValidateFocused(void);
	void __fastcall ValidateChosen(void);
	bool __fastcall CellIsChosen(void);
	void __fastcall AdjustFocus(int Row, int Col, System::Classes::TPersistent* TopLevelRVData, int TopLevelItemNo);
	bool __fastcall UndoEnabled(void);
	void __fastcall ChooseSubRVData_(int r, int c);
	void __fastcall EditCell_(int Row, int Col, bool Unquestioning);
	DYNAMIC int __fastcall GetRVFExtraPropertyCount(void);
	DYNAMIC void __fastcall SaveRVFExtraProperties(System::Classes::TStream* Stream);
	bool __fastcall RowsHavePageBreaksBefore(int StartRow, int EndRow);
	bool __fastcall GetNormalizedSelectionBoundsEx(bool IncludeEditedCell, int &TopRow, int &LeftCol, int &ColSpan, int &RowSpan);
	int __fastcall DoGetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, bool UseTableWidth);
	
public:
	bool FMakingSelection;
	Rvedit::TCustomRichViewEdit* FInplaceEditor;
	virtual int __fastcall GetHeight(Rvstyle::TRVStyle* RVStyle);
	virtual int __fastcall GetWidth(Rvstyle::TRVStyle* RVStyle);
	bool __fastcall CanUseHeadingRowCount(void);
	void __fastcall DrawBackgroundUnderCell(Vcl::Graphics::TCanvas* Canvas, TRVTableCellData* Cell, const System::Types::TRect &Rect, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowThumbnails);
	DYNAMIC void __fastcall FreeThumbnail(void);
	virtual bool __fastcall HasThumbnail(void);
	bool __fastcall IsSemiTransparentBackground(void);
	DYNAMIC void __fastcall ClearSoftPageBreaks(void);
	DYNAMIC int __fastcall GetSoftPageBreakDY(int Data);
	int __fastcall GetMyItemNo(void);
	void __fastcall SaveRowsToStream(System::Classes::TStream* Stream, int Index, int Count);
	void __fastcall SaveRectangleToStream(System::Classes::TStream* Stream, int TopRow, int LeftCol, int RowCount, int ColCount, bool SelectionOnly);
	DYNAMIC void __fastcall ResetSubCoords(void);
	DYNAMIC System::Classes::TPersistent* __fastcall GetSubRVDataAt(int X, int Y, bool ReturnClosest);
	bool __fastcall GetCellWhichOwnsControl(Vcl::Controls::TControl* AControl, int &ARow, int &ACol, int &AItemNo);
	DYNAMIC bool __fastcall AdjustFocusToControl(Vcl::Controls::TControl* Control, System::Classes::TPersistent* &TopLevelRVData, int &TopLevelItemNo);
	virtual void __fastcall Print(Vcl::Graphics::TCanvas* Canvas, int x, int y, int x2, bool Preview, bool Correction, const Rvstyle::TRVScreenAndDevice &sad, Rvscroll::TRVScroller* RichView, Dlines::TRVDrawLineInfo* dli, int Part, Rvstyle::TRVColorMode ColorMode, System::Classes::TPersistent* RVData, int PageNo);
	void __fastcall CreateInplace(int ItemNo, int Row, int Col, bool BuildJumps, bool CaretAtStart, bool CaretAtEnd, bool SetTime, bool Unquestioning);
	void __fastcall SetInplaceBounds(int Left, int Top, int Width, int Height);
	bool __fastcall StartSelecting(int Row, int Col, bool FromKeyboard);
	DYNAMIC void __fastcall MovingToUndoList(int ItemNo, System::TObject* RVData, System::TObject* AContainerUndoItem);
	DYNAMIC void __fastcall MovingFromUndoList(int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall Hiding(void);
	DYNAMIC void __fastcall FinalizeUndoGroup(void);
	virtual void __fastcall AdjustInserted(int x, int y, bool adjusty, System::Classes::TPersistent* RVData);
	DYNAMIC bool __fastcall OwnsControl(Vcl::Controls::TControl* AControl);
	DYNAMIC bool __fastcall OwnsInplaceEditor(Vcl::Controls::TControl* AEditor);
	virtual int __fastcall GetMinWidth(Rvstyle::PRVScreenAndDevice sad, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData);
	virtual void __fastcall PaintFullWidth(int Left, int Right, int Top, Vcl::Graphics::TCanvas* Canvas, Rvitem::TRVItemDrawStates State, Rvstyle::TRVStyle* Style, const System::Types::TRect &ClipRect, Dlines::TRVDrawLineInfo* dli, int ExtraX, int ExtraY, Rvitem::TRVPaintItemPart* PaintPart);
	virtual void __fastcall OnDocWidthChange(int DocWidth, Dlines::TRVDrawLineInfo* dli, bool Printing, Vcl::Graphics::TCanvas* Canvas, System::Classes::TPersistent* RVData, Rvstyle::PRVScreenAndDevice sad, int &HShift, int &Desc, bool NoCaching, bool Reformatting, bool UseFormatCanvas);
	void __fastcall ResizeRow(int Index, int Height);
	void __fastcall ResizeCol(int Index, int Width, bool Shift);
	DYNAMIC bool __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	DYNAMIC bool __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	DYNAMIC bool __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y, int ItemNo, System::TObject* RVData);
	DYNAMIC void __fastcall MouseLeave(void);
	DYNAMIC void __fastcall DeselectPartial(void);
	void __fastcall MergeInplaceUndo(bool DestroyLists);
	void __fastcall InplaceDeleted(bool Clearing);
	DYNAMIC bool __fastcall PartiallySelected(void);
	DYNAMIC bool __fastcall CanDeletePartiallySelected(void);
	void __fastcall DoAfterFillingRows(int Row, int Count);
	DYNAMIC void __fastcall DeletePartiallySelected(void);
	virtual bool __fastcall GetBoolValue(Rvitem::TRVItemBoolProperty Prop);
	virtual bool __fastcall GetBoolValueEx(Rvitem::TRVItemBoolPropertyEx Prop, Rvstyle::TRVStyle* RVStyle);
	DYNAMIC bool __fastcall SetExtraCustomProperty(const Rvtypes::TRVAnsiString PropName, const System::UnicodeString Value);
	DYNAMIC void __fastcall SaveRVF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo, const Rvtypes::TRVRawByteString Name, Rvitem::TRVMultiDrawItemPart* Part, bool ForceSameAsPrev, Rvstyle::PRVScreenAndDevice PSaD);
	DYNAMIC void __fastcall SaveRVFSelection(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int ParaNo);
	DYNAMIC bool __fastcall ReadRVFLine(const Rvtypes::TRVRawByteString s, System::Classes::TPersistent* RVData, int &ReadType, int LineNo, int LineCount, Rvtypes::TRVRawByteString &Name, Rvitem::TRVFReadMode &ReadMode, Rvitem::TRVFReadState &ReadState, bool UTF8Strings, bool &AssStyleNameUsed);
	DYNAMIC void __fastcall SaveTextSelection(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int LineWidth, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC void __fastcall BeforeLoading(Rvstyle::TRVLoadFormat FileFormat);
	DYNAMIC void __fastcall AfterLoading(Rvstyle::TRVLoadFormat FileFormat);
	virtual Dlines::TRVDrawLineInfo* __fastcall CreatePrintingDrawItem(System::TObject* RVData, System::TObject* ParaStyle, const Rvstyle::TRVScreenAndDevice &sad);
	virtual void __fastcall DrawBackgroundForPrinting(Vcl::Graphics::TCanvas* Canvas, const System::Types::TRect &Rect, const System::Types::TRect &FullRect, Rvstyle::TRVColorMode ColorMode, int ItemBackgroundLayer, Rvgrin::TRVGraphicInterface* GraphicInterface, bool AllowBitmaps);
	void __fastcall SaveInplace(void);
	void __fastcall RestoreInplace(void);
	void __fastcall UpdateStoredInplaceSelection(void);
	DYNAMIC void __fastcall StartExport(void);
	DYNAMIC void __fastcall EndExport(void);
	DYNAMIC void __fastcall MarkStylesInUse(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall UpdateStyles(Rvitem::TRVDeleteUnusedStylesData* Data);
	DYNAMIC void __fastcall ChangeParagraphStyles(Rvclasses::TRVIntegerList* ParaMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	DYNAMIC void __fastcall ChangeTextStyles(Rvclasses::TRVIntegerList* TextMapList, Rvclasses::TRVIntegerList* NewIndices, Rvclasses::TRVIntegerList* OldIndices, int &Index);
	virtual void __fastcall Inserting(System::TObject* RVData, Rvtypes::TRVRawByteString &Text, bool Safe);
	virtual void __fastcall Inserted(System::TObject* RVData, int ItemNo);
	DYNAMIC void __fastcall BeforeUndoChangeProperty(void);
	DYNAMIC void __fastcall AfterUndoChangeProperty(void);
	DYNAMIC void __fastcall ApplyStyleConversionToSubRVDatas(int UserData, bool SelectedOnly, Rvitem::TRVEStyleConversionType ConvType);
	bool __fastcall GetCellTo(int Row, int Col, TRVCellDirection Dir, int &NewRow, int &NewCol, bool Quiet, bool CanAddRow);
	DYNAMIC void __fastcall SaveRTF(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, int Level, Rvitem::TRVRTFSavingData &SavingData, Rvitem::TRVRTFDocType DocType, bool HiddenParent);
	DYNAMIC void __fastcall FillRTFTables(Rvfuncs::TCustomRVRTFTables* RTFTables, Rvclasses::TRVIntegerList* ListOverrideCountList, System::Classes::TPersistent* RVData);
	DYNAMIC void __fastcall SaveOOXML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, Rvdocx::TRVDocXSavingData* SavingData, bool HiddenParent);
	DYNAMIC Rvtypes::TRVRawByteString __fastcall AsText(int LineWidth, System::Classes::TPersistent* RVData, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, bool TextOnly, bool Unicode, Rvstyle::TRVCodePage CodePage);
	DYNAMIC void __fastcall SaveToHTML(System::Classes::TStream* Stream, System::Classes::TPersistent* RVData, int ItemNo, const Rvtypes::TRVRawByteString Text, const System::UnicodeString Path, const System::UnicodeString imgSavePrefix, int &imgSaveNo, System::Uitypes::TColor CurrentFileColor, Rvstyle::TRVSaveOptions SaveOptions, bool UseCSS, Rvclasses::TRVList* Bullets, Rvstyle::TRVHTMLListCSSList* ListBullets);
	DYNAMIC bool __fastcall EnterItem(Rvitem::TRVEnterDirection From, int Coord);
	DYNAMIC void __fastcall BuildJumps(int &StartJumpNo);
	DYNAMIC System::Classes::TPersistent* __fastcall GetSubRVData(Rvitem::TRVStoreSubRVData* &StoreState, Rvitem::TRVSubRVDataPos Position);
	DYNAMIC void __fastcall ChooseSubRVData(Rvitem::TRVStoreSubRVData* StoreState);
	DYNAMIC void __fastcall CleanUpChosen(void);
	__fastcall virtual TRVTableItemInfo(System::Classes::TPersistent* RVData);
	__fastcall TRVTableItemInfo(int nRows, int nCols, Crvdata::TCustomRVData* AMainRVData);
	__fastcall virtual ~TRVTableItemInfo(void);
	void __fastcall UnmergeCells(int TopRow, int LeftCol, int ColSpan, int RowSpan, bool UnmergeRows, bool UnmergeCols);
	bool __fastcall CanMergeCells(int TopRow, int LeftCol, int ColSpan, int RowSpan, bool AllowMergeRC);
	void __fastcall MergeCells(int TopRow, int LeftCol, int ColSpan, int RowSpan, bool AllowMergeRC);
	void __fastcall MergeSelectedCells(bool AllowMergeRC);
	bool __fastcall CanMergeSelectedCells(bool AllowMergeRC);
	void __fastcall UnmergeSelectedCells(bool UnmergeRows, bool UnmergeCols);
	void __fastcall SplitSelectedCellsVertically(int ColCount);
	void __fastcall SplitSelectedCellsHorizontally(int RowCount);
	bool __fastcall IsCellSelected(int Row, int Col);
	void __fastcall InsertRows(int Index, int Count, int CopyIndex, bool Select = true);
	void __fastcall InsertCols(int Index, int Count, int CopyIndex, bool Select = true);
	void __fastcall InsertColsLeft(int Count);
	void __fastcall InsertColsRight(int Count);
	void __fastcall InsertRowsAbove(int Count);
	void __fastcall InsertRowsBelow(int Count);
	void __fastcall DeleteRows(int Index, int Count, bool DecreaseHeight);
	void __fastcall DeleteCols(int Index, int Count, bool DecreaseWidth);
	void __fastcall DeleteSelectedRows(void);
	void __fastcall DeleteSelectedCols(void);
	void __fastcall DeleteEmptyRows(void);
	void __fastcall DeleteEmptyCols(void);
	bool __fastcall GetCellAt(int X, int Y, int &Row, int &Col, bool IgnoreBorders = false);
	int __fastcall GetVerticalRuleNo(int X, int &MinX, int &ZeroChangeX);
	int __fastcall GetHorizontalRuleNo(int Y, int &MinY, int &ZeroChangeY);
	void __fastcall Select(int StartRow, int StartCol, int RowOffs, int ColOffs);
	void __fastcall Deselect(void);
	void __fastcall SelectRows(int StartRow, int Count);
	void __fastcall SelectCols(int StartCol, int Count);
	bool __fastcall GetSelectionBounds(int &StartRow, int &StartCol, int &RowOffs, int &ColOffs);
	bool __fastcall GetNormalizedSelectionBounds(bool IncludeEditedCell, int &TopRow, int &LeftCol, int &ColSpan, int &RowSpan);
	Rvedit::TCustomRichViewEdit* __fastcall GetEditedCell(int &Row, int &Col);
	void __fastcall SaveToStream(System::Classes::TStream* Stream);
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	void __fastcall LoadFromStreamEx(System::Classes::TStream* Stream, int StartRow);
	void __fastcall EditCell(int Row, int Col);
	void __fastcall Changed(void);
	__property TRVTableRows* Rows = {read=FRows};
	__property TRVTableCellData* Cells[int Row][int Col] = {read=GetCells, write=SetCells};
	void __fastcall SetCellBestWidth(TRVHTMLLength Value, int Row, int Col);
	void __fastcall SetCellBestHeight(Rvstyle::TRVStyleLength Value, int Row, int Col);
	void __fastcall SetCellRotation(Rvstyle::TRVDocRotation Value, int Row, int Col);
	void __fastcall SetCellTag(const Rvstyle::TRVTag Value, int Row, int Col);
	void __fastcall SetCellColor(System::Uitypes::TColor Value, int Row, int Col);
	void __fastcall SetCellBackgroundStyle(Rvstyle::TRVItemBackgroundStyle Value, int Row, int Col);
	void __fastcall SetCellBackgroundImage(Vcl::Graphics::TGraphic* Value, int Row, int Col);
	void __fastcall SetCellBackgroundImageFileName(const System::UnicodeString Value, int Row, int Col);
	void __fastcall SetCellHint(const System::UnicodeString Value, int Row, int Col);
	void __fastcall SetCellVisibleBorders(bool Left, bool Top, bool Right, bool Bottom, int Row, int Col);
	void __fastcall SetTableVisibleBorders(bool Left, bool Top, bool Right, bool Bottom);
	void __fastcall SetCellBorderColor(System::Uitypes::TColor Value, int Row, int Col);
	void __fastcall SetCellBorderLightColor(System::Uitypes::TColor Value, int Row, int Col);
	void __fastcall SetCellVAlign(TRVCellVAlign Value, int Row, int Col);
	void __fastcall SetRowVAlign(TRVCellVAlign Value, int Row);
	void __fastcall SetRowPageBreakBefore(bool Value, int Row);
	void __fastcall SetRowKeepTogether(bool Value, int Row);
	DYNAMIC bool __fastcall MoveFocus(bool GoForward, System::Classes::TPersistent* &TopLevelRVData, int &TopLevelItemNo);
	DYNAMIC void __fastcall ClearFocus(void);
	void __fastcall GetCellPosition(TRVTableCellData* Cell, int &Row, int &Col);
	void __fastcall AssignProperties(TRVTableItemInfo* Source);
	DYNAMIC bool __fastcall SetExtraStrProperty(Rvitem::TRVExtraItemStrProperty Prop, const System::UnicodeString Value);
	DYNAMIC bool __fastcall GetExtraStrProperty(Rvitem::TRVExtraItemStrProperty Prop, System::UnicodeString &Value);
	DYNAMIC void __fastcall KeyDown(System::Word Key, bool Shift);
	__property System::UnicodeString BackgroundImageFileName = {read=FBackgroundImageFileName, write=FBackgroundImageFileName};
	__property int RowCount = {read=GetRowCount, nodefault};
	__property int ColCount = {read=GetColCount, nodefault};
	__property TRVTableStates State = {read=FState, write=FState, nodefault};
	DYNAMIC void __fastcall ConvertToDifferentUnits(Rvstyle::TRVStyleUnits NewUnits, Rvstyle::TRVStyle* RVStyle, bool Recursive);
	Rvstyle::TRVStyle* __fastcall GetRVStyle(void);
	DYNAMIC void __fastcall SaveFormattingToStream(System::Classes::TStream* Stream);
	DYNAMIC void __fastcall LoadFormattingFromStream(System::Classes::TStream* Stream);
	
__published:
	__property TRVTableOptions Options = {read=FOptions, write=FOptions, default=31};
	__property TRVTablePrintOptions PrintOptions = {read=FPrintOptions, write=FPrintOptions, default=3};
	__property TRVHTMLLength BestWidth = {read=FBestWidth, write=SetBestWidth, default=0};
	__property System::Uitypes::TColor Color = {read=FColor, write=SetColor, default=-16777211};
	__property Vcl::Graphics::TGraphic* BackgroundImage = {read=GetBackgroundImage, write=SetBackgroundImage, stored=false};
	__property Rvstyle::TRVItemBackgroundStyle BackgroundStyle = {read=GetBackgroundStyle, write=SetBackgroundStyle, default=0};
	__property int HeadingRowCount = {read=FHeadingRowCount, write=SetHeadingRowCount, default=0};
	__property System::UnicodeString TextRowSeparator = {read=FTextRowSeparator, write=FTextRowSeparator};
	__property System::UnicodeString TextColSeparator = {read=FTextColSeparator, write=FTextColSeparator};
	__property Rvstyle::TRVStyleLength BorderWidth = {read=FBorderWidth, write=SetBorderWidth, default=0};
	__property System::Uitypes::TColor BorderColor = {read=FBorderColor, write=SetBorderColor, default=-16777208};
	__property System::Uitypes::TColor BorderLightColor = {read=FBorderLightColor, write=SetBorderLightColor, default=-16777196};
	__property TRVTableBorderStyle BorderStyle = {read=FBorderStyle, write=SetBorderStyle, default=0};
	__property Rvstyle::TRVStyleLength BorderVSpacing = {read=FBorderVSpacing, write=SetBorderVSpacing, default=2};
	__property Rvstyle::TRVStyleLength BorderHSpacing = {read=FBorderHSpacing, write=SetBorderHSpacing, default=2};
	__property Rvstyle::TRVBooleanRect* VisibleBorders = {read=FVisibleBorders, write=SetVisibleBorders, stored=StoreVisibleBorders};
	__property Rvstyle::TRVStyleLength CellBorderWidth = {read=FCellBorderWidth, write=SetCellBorderWidth, default=0};
	__property System::Uitypes::TColor CellBorderColor = {read=FCellBorderColor, write=SetCellBorderColorProp, default=-16777208};
	__property System::Uitypes::TColor CellBorderLightColor = {read=FCellBorderLightColor, write=SetCellBorderLightColorProp, default=-16777196};
	__property Rvstyle::TRVStyleLength CellHPadding = {read=FCellHPadding, write=SetCellHPadding, stored=StoreCellHPadding, nodefault};
	__property Rvstyle::TRVStyleLength CellVPadding = {read=FCellVPadding, write=SetCellVPadding, stored=StoreCellVPadding, nodefault};
	__property Rvstyle::TRVStyleLength CellPadding = {read=GetCellPadding, write=SetCellPadding, stored=StoreCellPadding, default=1};
	__property TRVTableBorderStyle CellBorderStyle = {read=FCellBorderStyle, write=SetCellBorderStyle, default=1};
	__property Rvstyle::TRVStyleLength VRuleWidth = {read=FVRuleWidth, write=SetVRuleWidth, default=0};
	__property System::Uitypes::TColor VRuleColor = {read=FVRuleColor, write=SetVRuleColor, default=-16777208};
	__property Rvstyle::TRVStyleLength HRuleWidth = {read=FHRuleWidth, write=SetHRuleWidth, default=0};
	__property System::Uitypes::TColor HRuleColor = {read=FHRuleColor, write=SetHRuleColor, default=-16777208};
	__property Rvstyle::TRVStyleLength CellVSpacing = {read=FCellVSpacing, write=SetCellVSpacing, default=2};
	__property Rvstyle::TRVStyleLength CellHSpacing = {read=FCellHSpacing, write=SetCellHSpacing, default=2};
	__property bool VOutermostRule = {read=FVOutermostRule, write=SetVOutermostRule, default=0};
	__property bool HOutermostRule = {read=FHOutermostRule, write=SetHOutermostRule, default=0};
	__property TRVCellEditingEvent OnCellEditing = {read=FOnCellEditing, write=FOnCellEditing};
	__property TRVCellEndEditEvent OnCellEndEdit = {read=FOnCellEndEdit, write=FOnCellEndEdit};
	__property TRVTableDrawBorderEvent OnDrawBorder = {read=FOnDrawBorder, write=FOnDrawBorder};
};


class DELPHICLASS TRVTableStoreSubRVData;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTableStoreSubRVData : public Rvitem::TRVStoreSubRVData
{
	typedef Rvitem::TRVStoreSubRVData inherited;
	
public:
	int Row;
	int Col;
	__fastcall TRVTableStoreSubRVData(int ARow, int ACol);
	DYNAMIC Rvitem::TRVStoreSubRVData* __fastcall Duplicate(void);
	DYNAMIC int __fastcall Compare(Rvitem::TRVStoreSubRVData* StoreSub);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TRVTableStoreSubRVData(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTablePrintPart : public Rvitem::TRVMultiDrawItemPart
{
	typedef Rvitem::TRVMultiDrawItemPart inherited;
	
public:
	Rvclasses::TRVIntegerList* FTableTopCutLine;
	Rvclasses::TRVIntegerList* FTableBottomCutLine;
	Rvclasses::TRVIntegerList* FRowStarts;
	Rvclasses::TRVIntegerList* FRowIndices;
	bool FRowFinished;
	int FHeadingHeight;
	TRVTableItemFormattingInfo* FFmtRef;
	DYNAMIC int __fastcall GetSoftPageBreakInfo(void);
	DYNAMIC bool __fastcall IsComplexSoftPageBreak(Dlines::TRVDrawLineInfo* DrawItem);
	DYNAMIC void __fastcall AssignSoftPageBreaksToItem(Dlines::TRVDrawLineInfo* DrawItem, Rvitem::TCustomRVItemInfo* Item);
	__fastcall TRVTablePrintPart(TRVTableItemInfo* ATable, Dlines::TRVDrawLineInfo* AOwner);
	__fastcall virtual ~TRVTablePrintPart(void);
	void __fastcall UpdateRowStarts(int Index, int RowSpan, int Height, int &MaxRowStart);
};

#pragma pack(pop)

class DELPHICLASS TRVTablePrintInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTablePrintInfo : public Ptrvdata::TRVMultiDrawItemInfo
{
	typedef Ptrvdata::TRVMultiDrawItemInfo inherited;
	
private:
	Rvstyle::TRVScreenAndDevice sad;
	TRVTableItemInfo* FTable;
	int FHeadingRowCount;
	void __fastcall AddFootnotes(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &Height, bool FootnotesChangeHeight, int RowCount);
	void __fastcall RemoveFootnotes(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &Height, bool FootnotesChangeHeight, int RowCount);
	void __fastcall DecHeightByFootnotes_(int &Height, bool &ThisPageHasFootnotes, int RowCount);
	
public:
	TRVTableItemFormattingInfo* Fmt;
	__fastcall TRVTablePrintInfo(TRVTableItemInfo* ATable);
	virtual void __fastcall SetSize(int AWidth, int AHeight);
	__fastcall virtual ~TRVTablePrintInfo(void);
	DYNAMIC bool __fastcall InitSplit(const Rvstyle::TRVScreenAndDevice &Sad, bool IgnorePageBreaks);
	DYNAMIC bool __fastcall CanSplitFirst(int Y, const Rvstyle::TRVScreenAndDevice &Sad, bool FirstOnPage, bool PageHasFootnotes, bool FootnotesChangeHeight, bool IgnorePageBreaks);
	DYNAMIC bool __fastcall SplitAt(int Y, const Rvstyle::TRVScreenAndDevice &Sad, bool FirstOnPage, System::Classes::TList* &FootnoteRVDataList, int &MaxHeight, bool FootnotesChangeHeight, bool IgnorePageBreaks);
	DYNAMIC void __fastcall ResetPages(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall UnformatLastPage(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall AddAllFootnotes(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &Height, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall RemoveAllFootnotes(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &Height, bool FootnotesChangeHeight);
	DYNAMIC void __fastcall DecHeightByFootnotes(int &Height, bool &ThisPageHasFootnotes);
	DYNAMIC bool __fastcall GetPartNo(Crvdata::TCustomRVData* RVData, int ItemNo, int OffsInItem, int &PartNo, int &LocalPageNo, int &LocalPageCount, Ptrvdata::TCustomPrintableRVData* &PtRVData);
	DYNAMIC bool __fastcall GetSubDocCoords(Rvitem::TRVStoreSubRVData* Location, int Part, System::Types::TRect &InnerCoords, System::Types::TRect &OuterCoords, System::Types::TPoint &Origin);
	DYNAMIC Ptrvdata::TCustomPrintableRVData* __fastcall GetPrintableRVDataFor(Rvitem::TRVStoreSubRVData* Location);
	DYNAMIC int __fastcall GetLocalPageNoFor(Rvitem::TRVStoreSubRVData* Location, int Part);
public:
	/* TRVDrawLineInfo.CreateEx */ inline __fastcall TRVTablePrintInfo(int ALeft, int ATop, int AWidth, int AHeight, int AItemNo, System::ByteBool AFromNewLine) : Ptrvdata::TRVMultiDrawItemInfo(ALeft, ATop, AWidth, AHeight, AItemNo, AFromNewLine) { }
	
};

#pragma pack(pop)

struct DECLSPEC_DRECORD TRVPageFormatSaveRec
{
public:
	int DrawItemNo;
	int StartAt;
	int StartY;
	int Y;
	int EndAt;
	int PagesCount;
	bool Splitting;
};


class PASCALIMPLEMENTATION TCellPtblRVData : public Ptrvdata::TRectPtblRVData
{
	typedef Ptrvdata::TRectPtblRVData inherited;
	
private:
	int DrawItemNo;
	int StartAt;
	int StartY;
	int Y;
	int CurPageNo;
	int EndAt;
	bool Splitting;
	TRVPageFormatSaveRec PrevPageInfo;
	void __fastcall ResetDrawItems(int StartDItemNo, Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, bool ResetAll, bool InHeader, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight);
	
protected:
	DYNAMIC void __fastcall SetEndAt(int Value);
	
public:
	virtual Rvstyle::TRVStyle* __fastcall GetRVStyle(void);
	HIDESBASE bool __fastcall FormatNextPage(int &AMaxHeight, Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, bool FootnotesChangeHeight);
	void __fastcall UnformatLastPage(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight);
	void __fastcall ResetPages(Ptrvdata::TRVFootnoteRefList* &FootnoteRVDataList, int &ReleasedHeightAfterFootnotes, bool FootnotesChangeHeight, bool InHeader);
	bool __fastcall Finished(void);
public:
	/* TRectPtblRVData.Create */ inline __fastcall TCellPtblRVData(Rvscroll::TRVScroller* RichView, Crvdata::TCustomRVData* SourceDataForPrinting, Ptrvdata::TCustomPrintableRVData* ParentPrintData) : Ptrvdata::TRectPtblRVData(RichView, SourceDataForPrinting, ParentPrintData) { }
	
public:
	/* TCustomMultiPagePtblRVData.Destroy */ inline __fastcall virtual ~TCellPtblRVData(void) { }
	
};


class DELPHICLASS ERVTableInplaceError;
#pragma pack(push,4)
class PASCALIMPLEMENTATION ERVTableInplaceError : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ERVTableInplaceError(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ERVTableInplaceError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ERVTableInplaceError(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ERVTableInplaceError(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ERVTableInplaceError(NativeUInt Ident, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ERVTableInplaceError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ERVTableInplaceError(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ERVTableInplaceError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERVTableInplaceError(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERVTableInplaceError(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERVTableInplaceError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERVTableInplaceError(NativeUInt Ident, System::TVarRec const *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ERVTableInplaceError(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 rvsTable = System::Int8(-60);
static const System::Int8 crRVSelectRow = System::Int8(0x68);
static const System::Int8 crRVSelectCol = System::Int8(0x69);
#define RVTABLEDEFAULTOPTIONS (System::Set<TRVTableOption, TRVTableOption::rvtoEditing, TRVTableOption::rvtoIgnoreContentHeight>() << TRVTableOption::rvtoEditing << TRVTableOption::rvtoRowSizing << TRVTableOption::rvtoColSizing << TRVTableOption::rvtoRowSelect << TRVTableOption::rvtoColSelect )
#define RVTABLEDEFAULTPRINTOPTIONS (System::Set<TRVTablePrintOption, TRVTablePrintOption::rvtoHalftoneBorders, TRVTablePrintOption::rvtoContinue>() << TRVTablePrintOption::rvtoHalftoneBorders << TRVTablePrintOption::rvtoRowsSplit )
extern DELPHI_PACKAGE bool RichViewTableDefaultRTFAutofit;
extern DELPHI_PACKAGE bool RichViewTableAutoAddRow;
}	/* namespace Rvtable */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVTABLE)
using namespace Rvtable;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvtableHPP
