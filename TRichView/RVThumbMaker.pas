{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       TRVThumbnailMaker - class for smooth            }
{       resizing of images.                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVThumbMaker;

interface

{$I RV_Defs.inc}
uses Windows, Graphics, Classes;

type

  TRVThumbnailMaker = class
  private
    FClassList: TList;
    FMaxThumbnailSize: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AllowThumbnailsFor(Cls: TGraphicClass);
    procedure DisallowThumbnailsFor(Cls: TGraphicClass);
    function IsAllowedFor(Cls: TGraphicClass): Boolean;
    function MakeThumbnail(Gr: TGraphic; Width, Height: Integer): TBitmap;
    procedure AdjustThumbnailSize(var Width, Height: Integer);
    property MaxThumbnailSize: Integer read FMaxThumbnailSize write FMaxThumbnailSize;
  end;

var RVThumbnailMaker: TRVThumbnailMaker;

implementation

uses {$IFNDEF RVDONOTUSEJPEGIMAGE}
     Jpeg,
     {$ENDIF}
     RVFuncs, RVTypes, Math;

{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF4}
type
  TWeightedPixel = record
    Pixel: Integer;
    Weight: Single;
  end;

  TWPMatrix = array of array of TWeightedPixel;

  TRGBFloat = packed record
    r, g, b	: Single;
  end;

{
function SinC(Value: Single): Single;
begin
  if Value <> 0 then begin
    Value := Value * Pi;
    Result := sin(Value) / Value
    end
  else
    Result := 1;
end;

function Lanczos3Filter(Value: Single): Single;
begin
  if Value < 0 then
    Value := -Value;
  if Value < 3 then
    Result := SinC(Value) * SinC(Value / 3)
  else
    Result := 0;
end;
}

function MitchellFilter(Value: Single): Single; {$IFDEF RICHVIEWDEF2005}inline;{$ENDIF}
begin
  if Value < 0 then
    Value := -Value;
  if Value < 1 then
    Result := ((7*Value-12)*Sqr(Value) + 6-2.0/3) / 6
  else if Value < 2 then
    Result := (((-(1.0/3+2)*Value + 12)*Value - 20)*Value + 8.0/3+8) / 6
  else
    Result := 0;
end;

type TFilterFunction = function(Value: Single): Single;

procedure ResizeBitmap(SrcBitmap, DestBitmap: TBitmap; Eps: Integer);
var
  ScaleX, ScaleY, ScaleXRev, ScaleYRev : Single;
  TempBitmap			: TBitmap;
  WPMatrix: TWPMatrix;
  WPCountArray: array of Integer;
  SrcBits, DestBits, TempBits: PRVAnsiChar;
  SrcDelta, DestDelta, TempDelta: Integer;
  SrcWidth, SrcHeight, DstWidth, DstHeight: Integer;
  //FilterFunc: TFilterFunction;
  {........................................................................}
  procedure HorizontalSubSampling;
  var x, x2, Count, MaxCount, MinX, MaxX: Integer;
      CenterX, Weight: Single;
      ScaleXRevRat: Single;
  begin
    ScaleXRevRat := ScaleXRev * Eps;
    MaxCount := Trunc(ScaleXRevRat * 2 + 1);
    for x := 0 to DstWidth-1 do begin
      SetLength(WPMatrix[x], MaxCount);
      Count := 0;
      CenterX := x * ScaleXRev;
      MinX := Floor(CenterX - ScaleXRevRat);
      MaxX := Ceil(CenterX + ScaleXRevRat);
      for x2 := MinX to MaxX do begin
        Weight := MitchellFilter{FilterFunc}((CenterX - x2) * ScaleX) * ScaleX;
        if Weight <> 0 then begin
          if x2 < 0 then begin
            if -x2<SrcWidth then begin
              WPMatrix[x, Count].Pixel := - x2;
              WPMatrix[x, Count].Weight := Weight;
              inc(Count);
            end
            end
          else if x2 >= SrcWidth then begin
            if SrcWidth - x2 + SrcWidth - 1 >= 0 then begin
              WPMatrix[x, Count].Pixel := SrcWidth - x2 + SrcWidth - 1;
              WPMatrix[x, Count].Weight := Weight;
              inc(Count);
            end
            end
          else begin
            WPMatrix[x, Count].Pixel := x2;
            WPMatrix[x, Count].Weight := Weight;
            inc(Count);
          end;
        end;
      end;
      WPCountArray[x] := Count;
    end;
  end;
  {........................................................................}
  procedure VerticalSubSampling;
  var y, y2, Count, MaxCount, MinY, MaxY: Integer;
      CenterY, Weight: Single;
      ScaleYRevRat: Single;
  begin
    ScaleYRevRat := ScaleYRev * Eps;
    MaxCount := Trunc(ScaleYRevRat * 2 + 1);
    for Y := 0 to DstHeight-1 do begin
      SetLength(WPMatrix[y], MaxCount);
      Count := 0;
      CenterY := y * ScaleYRev;
      MinY := Floor(CenterY - ScaleYRevRat);
      MaxY := Ceil(CenterY + ScaleYRevRat);
      for y2 := MinY to MaxY do begin
        Weight := MitchellFilter{FilterFunc}((CenterY - y2) * ScaleY) * ScaleY;
        if Weight <> 0 then begin
          if y2 < 0 then begin
            if -y < SrcHeight then begin
              WPMatrix[y, Count].Pixel := -y2;
              WPMatrix[y, Count].Weight := Weight;
              inc(Count);
            end
            end
          else if y2 >= SrcHeight then begin
            if SrcHeight-y2+SrcHeight-1>=0 then begin
              WPMatrix[y, Count].Pixel := SrcHeight-y2+SrcHeight-1;
              WPMatrix[y, Count].Weight := Weight;
              inc(Count);
            end
            end
          else begin
            WPMatrix[y, Count].Pixel := y2;
            WPMatrix[y, Count].Weight := Weight;
            inc(Count);
          end;
        end;
      end;
      WPCountArray[y] := Count;
    end;
  end;
  {........................................................................}
  procedure HorizontalSuperSampling;
  var x, x2, Count, MaxCount, MinX, MaxX: Integer;
      CenterX, Weight: Single;
  begin
    MaxCount := Eps * 2 + 1;
    for x := 0 to DstWidth-1 do begin
      SetLength(WPMatrix[x], MaxCount);
      Count := 0;
      CenterX := x * ScaleXRev;
      MinX := Floor(CenterX - Eps);
      MaxX := Ceil(CenterX + Eps);
      for x2 := MinX to MaxX do begin
        Weight := MitchellFilter{FilterFunc}(CenterX - x2);
        if Weight <> 0 then begin
          if x2 < 0 then begin
            if -x2 < SrcWidth then begin
              WPMatrix[x, Count].Pixel := - x2;
              WPMatrix[x, Count].Weight := Weight;
              inc(Count);
            end
            end
          else if x2 >= SrcWidth then begin
            if SrcWidth - x2 + SrcWidth - 1 >= 0 then begin
              WPMatrix[x, Count].Pixel := SrcWidth - x2 + SrcWidth - 1;
              WPMatrix[x, Count].Weight := Weight;
              inc(Count);
            end
            end
          else begin
            WPMatrix[x, Count].Pixel := x2;
            WPMatrix[x, Count].Weight := Weight;
            inc(Count);
          end;
        end;
      end;
      WPCountArray[x] := Count;
    end;
  end;
  {........................................................................}
  procedure VerticalSuperSampling;
  var y, y2, Count, MaxCount, MinY, MaxY: Integer;
      CenterY, Weight: Single;
  begin
    MaxCount := Eps * 2 + 1;
    for y := 0 to DstHeight-1 do begin
      SetLength(WPMatrix[y], MaxCount);
      Count := 0;
      CenterY := y * ScaleYRev;
      MinY := Floor(CenterY - Eps);
      MaxY := Ceil(CenterY + Eps);
      for y2 := MinY to MaxY do begin
        Weight := MitchellFilter{FilterFunc}(CenterY - y2);
        if Weight <> 0 then begin
          if y2 < 0 then begin
            if - y2 < SrcHeight then begin
              WPMatrix[y, Count].Pixel := - y2;
              WPMatrix[y, Count].Weight := Weight;
              inc(Count);
            end
            end
          else if y2 >= SrcHeight then begin
            if SrcHeight - y2 + SrcHeight - 1 >= 0 then begin
              WPMatrix[y, Count].Pixel := SrcHeight - y2 + SrcHeight - 1;
              WPMatrix[y, Count].Weight := Weight;
              inc(Count);
            end
            end
          else begin
            WPMatrix[y, Count].Pixel := y2;
            WPMatrix[y, Count].Weight := Weight;
            inc(Count);
          end;
        end;
      end;
      WPCountArray[y] := Count;
    end;
  end;
  {........................................................................}
  procedure RGBFloatToPixel(const rgb: TRGBFloat; DstPixel: PRGBTriple);
  begin
    if rgb.r > 255 then
      DstPixel^.rgbtRed := 255
    else if rgb.r < 0 then
      DstPixel^.rgbtRed := 0
    else
      DstPixel^.rgbtRed := Round(rgb.r);
    if rgb.g > 255 then
      DstPixel^.rgbtGreen := 255
    else if rgb.g < 0 then
      DstPixel^.rgbtGreen := 0
    else
      DstPixel^.rgbtGreen := Round(rgb.g);
    if rgb.b > 255 then
      DstPixel^.rgbtBlue := 255
    else if rgb.b < 0 then
      DstPixel^.rgbtBlue := 0
    else
      DstPixel^.rgbtBlue := Round(rgb.b);
  end;
  {........................................................................}
  procedure ApplyHorizontally;
  var x, y, i: Integer;
    Weight: Single;
    SrcLine, TempLine: PRVAnsiChar;
    SrcPixel, TempPixel: PRGBTriple;
    rgb: TRGBFloat;
  begin
    SrcLine := SrcBits;
    TempLine := TempBits;
    for y := 0 to SrcHeight-1 do begin
      TempPixel := PRGBTriple(TempLine);
      for x := 0 to DstWidth-1 do begin
        rgb.r := 0;
        rgb.g := 0;
        rgb.b := 0;
        for i := 0 to WPCountArray[x]-1 do begin
          SrcPixel := PRGBTriple(SrcLine + WPMatrix[x, i].Pixel*sizeof(TRGBTriple));
          Weight := WPMatrix[x, i].Weight;
          rgb.r := rgb.r + SrcPixel^.rgbtRed * Weight;
          rgb.g := rgb.g + SrcPixel^.rgbtGreen * Weight;
          rgb.b := rgb.b + SrcPixel^.rgbtBlue * Weight;
        end;
        RGBFloatToPixel(rgb, TempPixel);
        inc(PRVAnsiChar(TempPixel), sizeof(TRGBTriple));
      end;
      inc(SrcLine, SrcDelta);
      inc(TempLine, TempDelta);
    end;
  end;
  {........................................................................}
  procedure ApplyVertically;
  var x, y, i: Integer;
    Weight: Single;
    DestLine, TempLine: PRVAnsiChar;
    DestPixel, TempPixel: PRGBTriple;
    rgb: TRGBFloat;
  begin
    DestLine := DestBits;
    TempLine := TempBits;
    for x := 0 to DstWidth-1 do begin
      DestPixel := PRGBTriple(DestLine);
      for y := 0 to DstHeight-1 do begin
        rgb.r := 0;
        rgb.g := 0;
        rgb.b := 0;
        for i := 0 to WPCountArray[y]-1 do begin
          TempPixel := PRGBTriple(TempLine + WPMatrix[y, i].Pixel*TempDelta);
          Weight := WPMatrix[y, i].Weight;
          rgb.r := rgb.r + TempPixel^.rgbtRed * Weight;
          rgb.g := rgb.g + TempPixel^.rgbtGreen * Weight;
          rgb.b := rgb.b + TempPixel^.rgbtBlue * Weight;
        end;
        RGBFloatToPixel(rgb, DestPixel);
        inc(PRVAnsiChar(DestPixel), DestDelta);
      end;
      inc(DestLine, sizeof(TRGBTriple));
      inc(TempLine, sizeof(TRGBTriple));
    end;
  end;
  {........................................................................}
begin
  {
  if Eps=2 then
    FilterFunc := MitchellFilter
  else
    FilterFunc := Lanczos3Filter;}
  DstWidth := DestBitmap.Width;
  DstHeight := DestBitmap.Height;
  SrcWidth := SrcBitmap.Width;
  SrcHeight := SrcBitmap.Height;
  SrcBits := SrcBitmap.ScanLine[0];
  if SrcHeight > 1 then
    SrcDelta := PRVAnsiChar(SrcBitmap.ScanLine[1])-SrcBits
  else
    SrcDelta := 0;
  DestBits := DestBitmap.ScanLine[0];
  if DstHeight > 1 then
    DestDelta := PRVAnsiChar(DestBitmap.ScanLine[1])-DestBits
  else
    DestDelta := 0;
  if SrcWidth > 1 then
    ScaleX := (DstWidth - 1) / (SrcWidth - 1)
  else
    ScaleX := DstWidth / SrcWidth;
  if SrcHeight > 1 then
    ScaleY := (DstHeight - 1) / (SrcHeight - 1)
  else
    ScaleY := DstHeight / SrcHeight;
  if ScaleX>=RVEps then
    ScaleXRev := 1 / ScaleX
  else
    ScaleXRev := SrcWidth / 2;
  if ScaleY>=RVEps then
    ScaleYRev := 1 / ScaleY
  else
    ScaleYRev := SrcHeight / 2;

  TempBitmap := TBitmap.Create;
  try
    TempBitmap.PixelFormat := pf24Bit;
    TempBitmap.Height := SrcHeight;
    TempBitmap.Width := DstWidth;
    TempBits := TempBitmap.ScanLine[0];
    if SrcHeight > 1 then
      TempDelta := PRVAnsiChar(TempBitmap.ScanLine[1])-TempBits
    else
      TempDelta := 0;


    SetLength(WPMatrix, DstWidth);
    SetLength(WPCountArray, DstWidth);

    if ScaleX < 1 then
      HorizontalSubSampling
    else
      HorizontalSuperSampling;
    ApplyHorizontally;
    //TempBitmap.SaveToFile('d:\test\1.bmp');

    WPMatrix := nil;
    SetLength(WPMatrix, DstHeight);
    SetLength(WPCountArray, DstHeight);

    if ScaleY < 1.0 then
      VerticalSubSampling
    else
      VerticalSuperSampling;
    ApplyVertically;
    //DestBitmap.SaveToFile('d:\test\2.bmp');
  finally
    TempBitmap.Free;
  end;
end;
{$ENDIF}
{=============================== TRVThumbnailMaker ============================}
constructor TRVThumbnailMaker.Create;
begin
  inherited Create;
  FMaxThumbnailSize := 3000;
  FClassList := TList.Create;
  FClassList.Add(TBitmap);
  {$IFNDEF RVDONOTUSEJPEGIMAGE}
  FClassList.Add(TJpegImage);
  {$ENDIF}
  {$IFDEF RICHVIEWDEF2010}
  FClassList.Add(TWicImage);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
destructor TRVThumbnailMaker.Destroy;
begin
  FClassList.Free;
  inherited Destroy;
end;
{------------------------------------------------------------------------------}
{ Allows making scaled images for images of the specified graphic class
  Note: for TBimap, scaled images for transparent bitmaps are never creates }
procedure TRVThumbnailMaker.AllowThumbnailsFor(Cls: TGraphicClass);
begin
  if FClassList.IndexOf(Cls)<0 then
    FClassList.Add(Cls);
end;
{------------------------------------------------------------------------------}
{ Disallows making scaled images for the specified graphic class }
procedure TRVThumbnailMaker.DisallowThumbnailsFor(Cls: TGraphicClass);
var i: Integer;
begin
  i := FClassList.IndexOf(Cls);
  if i>=0 then
    FClassList.Delete(i);
end;
{------------------------------------------------------------------------------}
{ Is making scaled images allowed for this class? }
function TRVThumbnailMaker.IsAllowedFor(Cls: TGraphicClass): Boolean;
begin
  {$IFDEF RICHVIEWDEF4}
  Result := FClassList.IndexOf(Cls)>=0;
  {$ELSE}
  Result := False;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
{ Adjusts the scaled image class: if Width and/or Height is greater then
  MaxThumbnailSize, Width and Height are reduced proportionally }
procedure TRVThumbnailMaker.AdjustThumbnailSize(var Width, Height: Integer);
var MaxSize: Integer;
begin
  MaxSize := Max(Width, Height);
  if MaxSize>MaxThumbnailSize then begin
    Width := Round(Width/MaxSize*MaxThumbnailSize);
    Height := Round(Height/MaxSize*MaxThumbnailSize);
  end;
end;
{------------------------------------------------------------------------------}
{ Creates and returns a scaled bitmap image for Gr.
  If scaled images are not allowed for this type of pictures, or a making a
  scaled image of this size does not make sense, it returns nil.
  Normally, the returned picture is Width x Height. However, sizes may be
  adjusted to prevent exceeding MaxThumbnailSize }
function TRVThumbnailMaker.MakeThumbnail(Gr: TGraphic; Width, Height: Integer): TBitmap;
var bmp: TBitmap;
begin
  Result := nil;
  {$IFDEF RICHVIEWDEF4}
  if not IsAllowedFor(TGraphicClass(Gr.ClassType)) or (Gr.Width+Gr.Height=0) or (Width<=0) or (Height<=0) or
    ((Gr is TBitmap) and Gr.Transparent) or
    ((Width=Gr.Width) and (Height=Gr.Height)) then
    exit;
  if Width<=0 then
    Width := Gr.Width;
  if Height<=0 then
    Height := Gr.Height;
  if ((Gr.Height>MaxThumbnailSize) or (Gr.Width>MaxThumbnailSize)) and
     ((Width>MaxThumbnailSize) or (Height>MaxThumbnailSize)) and
     (Gr.Width<Width) and (Gr.Height<Height) then
    exit; // <-- it does not make sense to create such thumbnails
  AdjustThumbnailSize(Width, Height);
  try
    bmp := TBitmap.Create;
    try
      try
        bmp.Assign(Gr);
        bmp.PixelFormat := pf24bit;
      except
        bmp.PixelFormat := pf24bit;
        bmp.Width := Gr.Width;
        bmp.Height := Gr.Height;
        bmp.Canvas.Draw(0, 0, Gr);
      end;
      Result := TBitmap.Create;
      Result.PixelFormat := pf24bit;
      Result.Width := Width;
      Result.Height := Height;
      ResizeBitmap(bmp, Result, 2);
    finally
      bmp.Free;
    end;
  except
    RVFreeAndNil(Result);
  end;
  {$ENDIF}
end;

initialization

  RVThumbnailMaker := TRVThumbnailMaker.Create;

finalization
  RVFreeAndNil(RVThumbnailMaker);

end.
