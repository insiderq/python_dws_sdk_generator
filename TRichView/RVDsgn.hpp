﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDsgn.pas' rev: 27.00 (Windows)

#ifndef RvdsgnHPP
#define RvdsgnHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <RVStyle.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdsgn
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmRVDesign;
class PASCALIMPLEMENTATION TfrmRVDesign : public Vcl::Forms::TForm
{
	typedef Vcl::Forms::TForm inherited;
	
__published:
	Vcl::Stdctrls::TButton* btnOk;
	Vcl::Stdctrls::TButton* btnCancel;
	Vcl::Comctrls::TPageControl* pc;
	Vcl::Comctrls::TTabSheet* tsStyles;
	Vcl::Stdctrls::TLabel* Label1;
	Vcl::Extctrls::TRadioGroup* rg;
	Vcl::Comctrls::TListView* lv;
	Vcl::Stdctrls::TCheckBox* cbStylesDefault;
	Vcl::Comctrls::TTabSheet* TabSheet2;
	Vcl::Stdctrls::TGroupBox* GroupBox1;
	Vcl::Stdctrls::TCheckBox* cbRVFSaveBackground;
	Vcl::Stdctrls::TCheckBox* cbRVFSaveLayout;
	Vcl::Stdctrls::TGroupBox* GroupBox2;
	Vcl::Stdctrls::TCheckBox* cbRVFDefault;
	Vcl::Extctrls::TImage* Image2;
	Vcl::Stdctrls::TLabel* Label3;
	Vcl::Stdctrls::TCheckBox* cbRVFSaveDocProperties;
	Vcl::Stdctrls::TGroupBox* GroupBox3;
	Vcl::Stdctrls::TCheckBox* cbRVFIgnoreUnknownPictures;
	Vcl::Stdctrls::TCheckBox* cbRVFIgnoreUnknownControls;
	Vcl::Stdctrls::TCheckBox* cbRVFInvStyles;
	Vcl::Stdctrls::TCheckBox* cbRVFInvImageIndices;
	Vcl::Stdctrls::TCheckBox* cbRVFIgnoreUnknownCtrlProps;
	Vcl::Stdctrls::TLabel* Label4;
	Vcl::Stdctrls::TCheckBox* cbRVFLoadBackground;
	Vcl::Stdctrls::TLabel* Label5;
	Vcl::Stdctrls::TCheckBox* cbRVFLoadLayout;
	Vcl::Stdctrls::TLabel* Label6;
	Vcl::Stdctrls::TCheckBox* cbRVFLoadDocProperties;
	Vcl::Stdctrls::TCheckBox* cbRVFBinary;
	Vcl::Stdctrls::TCheckBox* cbRVFSavePictures;
	Vcl::Stdctrls::TCheckBox* cbRVFSaveControls;
	void __fastcall FormActivate(System::TObject* Sender);
	void __fastcall rgClick(System::TObject* Sender);
	void __fastcall btnOkClick(System::TObject* Sender);
	
protected:
	Richview::TCustomRichView* rv;
	bool Initialized;
	void __fastcall SaveReg(void);
	
public:
	void __fastcall SetRichView(Richview::TCustomRichView* rv);
public:
	/* TCustomForm.Create */ inline __fastcall virtual TfrmRVDesign(System::Classes::TComponent* AOwner) : Vcl::Forms::TForm(AOwner) { }
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmRVDesign(System::Classes::TComponent* AOwner, int Dummy) : Vcl::Forms::TForm(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmRVDesign(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmRVDesign(HWND ParentWindow) : Vcl::Forms::TForm(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Rvdsgn */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDSGN)
using namespace Rvdsgn;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdsgnHPP
