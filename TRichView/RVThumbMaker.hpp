﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVThumbMaker.pas' rev: 27.00 (Windows)

#ifndef RvthumbmakerHPP
#define RvthumbmakerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvthumbmaker
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TRVThumbnailMaker;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVThumbnailMaker : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Classes::TList* FClassList;
	int FMaxThumbnailSize;
	
public:
	__fastcall TRVThumbnailMaker(void);
	__fastcall virtual ~TRVThumbnailMaker(void);
	void __fastcall AllowThumbnailsFor(Vcl::Graphics::TGraphicClass Cls);
	void __fastcall DisallowThumbnailsFor(Vcl::Graphics::TGraphicClass Cls);
	bool __fastcall IsAllowedFor(Vcl::Graphics::TGraphicClass Cls);
	Vcl::Graphics::TBitmap* __fastcall MakeThumbnail(Vcl::Graphics::TGraphic* Gr, int Width, int Height);
	void __fastcall AdjustThumbnailSize(int &Width, int &Height);
	__property int MaxThumbnailSize = {read=FMaxThumbnailSize, write=FMaxThumbnailSize, nodefault};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TRVThumbnailMaker* RVThumbnailMaker;
}	/* namespace Rvthumbmaker */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVTHUMBMAKER)
using namespace Rvthumbmaker;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvthumbmakerHPP
