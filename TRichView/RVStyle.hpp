﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVStyle.pas' rev: 27.00 (Windows)

#ifndef RvstyleHPP
#define RvstyleHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.AnsiStrings.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <System.IniFiles.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <System.UITypes.hpp>	// Pascal unit
#include <RVClasses.hpp>	// Pascal unit
#include <RVMapWht.hpp>	// Pascal unit
#include <RVScroll.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <RVGrIn.hpp>	// Pascal unit
#include <RVWinGrIn.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------
typedef Extended TRVLength;

namespace Rvstyle
{
//-- type declarations -------------------------------------------------------
typedef System::UnicodeString TRVTag;

struct DECLSPEC_DRECORD TRVScreenAndDevice
{
public:
	int ppixScreen;
	int ppiyScreen;
	int ppixDevice;
	int ppiyDevice;
	int LeftMargin;
	int RightMargin;
};


typedef TRVScreenAndDevice *PRVScreenAndDevice;

enum DECLSPEC_DENUM TRVHoverEffect : unsigned char { rvheUnderline };

typedef System::Set<TRVHoverEffect, TRVHoverEffect::rvheUnderline, TRVHoverEffect::rvheUnderline> TRVHoverEffects;

enum DECLSPEC_DENUM TRVSaveFormat : unsigned char { rvsfText, rvsfHTML, rvsfRTF, rvsfRVF, rvsfDocX };

enum DECLSPEC_DENUM TRVLoadFormat : unsigned char { rvlfText, rvlfHTML, rvlfRTF, rvlfRVF, rvlfURL, rvlfOther };

enum DECLSPEC_DENUM TRVRTFSaveArea : unsigned char { rv_rtfs_TextStyle, rv_rtfs_ParaStyle, rv_rtfs_ListStyle, rv_rtfs_StyleTemplate, rv_rtfs_StyleTemplateText, rv_rtfs_CellProps, rv_rtfs_RowProps, rv_rtfs_Doc };

enum DECLSPEC_DENUM TRVDocXSaveArea : unsigned char { rv_docxs_TextStyle, rv_docxs_ParaStyle, rv_docxs_ListStyle, rv_docxs_StyleTemplate, rv_docxs_StyleTemplateText, rv_docxs_StyleTemplatePara, rv_docxs_CellProps, rv_docxs_RowProps, rv_docxs_TableProps, rv_docxs_SectProps, rv_docxs_Settings, rv_docxs_CoreProperties };

enum DECLSPEC_DENUM TRVHTMLSaveArea : unsigned char { rv_thms_Head, rv_thms_BodyAttribute, rv_thms_Body, rv_thms_End };

enum DECLSPEC_DENUM TRVReaderUnicode : unsigned char { rvruMixed, rvruNoUnicode, rvruOnlyUnicode };

enum DECLSPEC_DENUM TRVReaderStyleMode : unsigned char { rvrsUseSpecified, rvrsUseClosest, rvrsAddIfNeeded };

enum DECLSPEC_DENUM TRVFReaderStyleMode : unsigned char { rvf_sIgnore, rvf_sInsertMap, rvf_sInsertMerge };

enum DECLSPEC_DENUM TRVColorMode : unsigned char { rvcmColor, rvcmPrinterColor, rvcmGrayScale, rvcmBlackAndWhite, rvcmBlackOnWhite };

typedef unsigned TRVCodePage;

enum DECLSPEC_DENUM TRVItemBackgroundStyle : unsigned char { rvbsColor, rvbsStretched, rvbsTiled, rvbsCentered };

enum DECLSPEC_DENUM TRVSubSuperScriptType : unsigned char { rvsssNormal, rvsssSubscript, rvsssSuperScript };

enum DECLSPEC_DENUM TRVUnderlineType : unsigned char { rvutNormal, rvutThick, rvutDouble, rvutDotted, rvutThickDotted, rvutDashed, rvutThickDashed, rvutLongDashed, rvutThickLongDashed, rvutDashDotted, rvutThickDashDotted, rvutDashDotDotted, rvutThickDashDotDotted };

typedef void *TCheckpointData;

enum DECLSPEC_DENUM TRVTextDrawState : unsigned char { rvtsSelected, rvtsHover, rvtsItemStart, rvtsItemEnd, rvtsDrawItemStart, rvtsDrawItemEnd, rvtsControlFocused, rvtsSpecialCharacters, rvtstControlDisabled };

typedef System::Set<TRVTextDrawState, TRVTextDrawState::rvtsSelected, TRVTextDrawState::rvtstControlDisabled> TRVTextDrawStates;

enum DECLSPEC_DENUM TRVPageBreakType : unsigned char { rvpbSoftPageBreak, rvpbPageBreak };

enum DECLSPEC_DENUM TRVBreakStyle : unsigned char { rvbsLine, rvbsRectangle, rvbs3d, rvbsDotted, rvbsDashed };

enum DECLSPEC_DENUM TRVVAlign : unsigned char { rvvaBaseline, rvvaMiddle, rvvaAbsTop, rvvaAbsBottom, rvvaAbsMiddle, rvvaLeft, rvvaRight };

enum DECLSPEC_DENUM TRVBorderStyle : unsigned char { rvbNone, rvbSingle, rvbDouble, rvbTriple, rvbThickInside, rvbThickOutside };

enum DECLSPEC_DENUM TRVSeqType : unsigned char { rvseqDecimal, rvseqLowerAlpha, rvseqUpperAlpha, rvseqLowerRoman, rvseqUpperRoman };

enum DECLSPEC_DENUM TRVListType : unsigned char { rvlstBullet, rvlstPicture, rvlstImageList, rvlstDecimal, rvlstLowerAlpha, rvlstUpperAlpha, rvlstLowerRoman, rvlstUpperRoman, rvlstImageListCounter, rvlstUnicodeBullet };

enum DECLSPEC_DENUM TRVMarkerAlignment : unsigned char { rvmaLeft, rvmaRight, rvmaCenter };

enum DECLSPEC_DENUM TRVListLevelOption : unsigned char { rvloContinuous, rvloLevelReset, rvloLegalStyleNumbering };

typedef System::Set<TRVListLevelOption, TRVListLevelOption::rvloContinuous, TRVListLevelOption::rvloLegalStyleNumbering> TRVListLevelOptions;

typedef System::UnicodeString TRVMarkerFormatString;

typedef Rvtypes::TRVUnicodeString TRVMarkerFormatStringW;

enum DECLSPEC_DENUM TRVFOption : unsigned char { rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoIgnoreUnknownPicFmt, rvfoIgnoreUnknownCtrls, rvfoIgnoreUnknownCtrlProperties, rvfoConvUnknownStylesToZero, rvfoConvLargeImageIdxToZero, rvfoSaveBinary, rvfoUseStyleNames, rvfoSaveBack, rvfoLoadBack, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveLayout, rvfoLoadLayout, rvfoSaveDocProperties, rvfoLoadDocProperties, rvfoCanChangeUnits };

typedef System::Set<TRVFOption, TRVFOption::rvfoSavePicturesBody, TRVFOption::rvfoCanChangeUnits> TRVFOptions;

enum DECLSPEC_DENUM TRVLongOperation : unsigned char { rvloRTFRead, rvloRTFWrite, rvloRVFRead, rvloRVFWrite, rvloHTMLRead, rvloHTMLWrite, rvloTextRead, rvloTextWrite, rvloConvertImport, rvloConvertExport, rvloXMLRead, rvloXMLWrite, rvloDocXRead, rvloDocXWrite, rvloOtherRead, rvloOtherWrite };

enum DECLSPEC_DENUM TRVProgressStage : unsigned char { rvpstgStarting, rvpstgRunning, rvpstgEnding };

enum DECLSPEC_DENUM TRVFWarning : unsigned char { rvfwUnknownPicFmt, rvfwUnknownCtrls, rvfwConvUnknownStyles, rvfwConvLargeImageIdx, rvfwConvToUnicode, rvfwConvFromUnicode, rvfwInvalidPicture, rvfwUnknownStyleProperties, rvfwUnknownCtrlProperties, rvfwConvUnits };

typedef System::Set<TRVFWarning, TRVFWarning::rvfwUnknownPicFmt, TRVFWarning::rvfwConvUnits> TRVFWarnings;

enum DECLSPEC_DENUM TRVControlAction : unsigned char { rvcaAfterRVFLoad, rvcaDestroy, rvcaMoveToUndoList, rvcaMoveFromUndoList, rvcaDestroyInUndoList, rvcaBeforeRVFSave, rvcaAfterRVFSave };

enum DECLSPEC_DENUM TRVItemAction : unsigned char { rviaInserting, rviaInserted, rviaTextModifying, rviaDestroying, rviaMovingToUndoList };

enum DECLSPEC_DENUM TRVProtectOption : unsigned char { rvprStyleProtect, rvprStyleSplitProtect, rvprModifyProtect, rvprDeleteProtect, rvprConcateProtect, rvprRVFInsertProtect, rvprDoNotAutoSwitch, rvprParaStartProtect, rvprSticking, rvprSticking2, rvprSticking3, rvprStickToTop, rvprStickToBottom };

typedef System::Set<TRVProtectOption, TRVProtectOption::rvprStyleProtect, TRVProtectOption::rvprStickToBottom> TRVProtectOptions;

enum DECLSPEC_DENUM TRVParaOption : unsigned char { rvpaoNoWrap, rvpaoReadOnly, rvpaoStyleProtect, rvpaoDoNotWantReturns, rvpaoKeepLinesTogether, rvpaoKeepWithNext, rvpaoWidowOrphanControl };

typedef System::Set<TRVParaOption, TRVParaOption::rvpaoNoWrap, TRVParaOption::rvpaoWidowOrphanControl> TRVParaOptions;

enum DECLSPEC_DENUM TRVTextOption : unsigned char { rvteoHTMLCode, rvteoRTFCode, rvteoDocXCode, rvteoDocXInRunCode, rvteoHidden };

typedef System::Set<TRVTextOption, TRVTextOption::rvteoHTMLCode, TRVTextOption::rvteoHidden> TRVTextOptions;

enum DECLSPEC_DENUM TRVSaveOption : unsigned char { rvsoOverrideImages, rvsoFirstOnly, rvsoMiddleOnly, rvsoLastOnly, rvsoDefault0Style, rvsoNoHypertextImageBorders, rvsoImageSizes, rvsoForceNonTextCSS, rvsoUseCheckpointsNames, rvsoMarkersAsText, rvsoInlineCSS, rvsoNoDefCSSStyle, rvsoUseItemImageFileNames, rvsoXHTML, rvsoUTF8 };

typedef System::Set<TRVSaveOption, TRVSaveOption::rvsoOverrideImages, TRVSaveOption::rvsoUTF8> TRVSaveOptions;

enum DECLSPEC_DENUM TRVRTFOption : unsigned char { rvrtfSaveStyleSheet, rvrtfDuplicateUnicode, rvrtfSaveEMFAsWMF, rvrtfSaveJpegAsJpeg, rvrtfSavePngAsPng, rvrtfSaveBitmapDefault, rvrtfSaveEMFDefault, rvrtfSavePicturesBinary, rvrtfPNGInsteadOfBitmap, rvrtfSaveDocParameters, rvrtfSaveHeaderFooter };

typedef System::Set<TRVRTFOption, TRVRTFOption::rvrtfSaveStyleSheet, TRVRTFOption::rvrtfSaveHeaderFooter> TRVRTFOptions;

enum DECLSPEC_DENUM TRVHFType : unsigned char { rvhftNormal, rvhftFirstPage, rvhftEvenPages };

enum DECLSPEC_DENUM TRVFontStyle : unsigned char { rvfsOverline, rvfsAllCaps };

typedef System::Set<TRVFontStyle, TRVFontStyle::rvfsOverline, TRVFontStyle::rvfsAllCaps> TRVFontStyles;

enum DECLSPEC_DENUM TRVAlignment : unsigned char { rvaLeft, rvaRight, rvaCenter, rvaJustify };

enum DECLSPEC_DENUM TRVDocRotation : unsigned char { rvrotNone, rvrot90, rvrot180, rvrot270 };

enum DECLSPEC_DENUM TRVUnits : unsigned char { rvuInches, rvuCentimeters, rvuMillimeters, rvuPicas, rvuPixels, rvuPoints };

typedef System::Extended TRVLength;

enum DECLSPEC_DENUM TRVLineWrapMode : unsigned char { rvWrapAnywhere, rvWrapSimple, rvWrapNormal };

struct DECLSPEC_DRECORD TRVExtraFontInfo
{
public:
	int ScriptHeight;
};


typedef TRVExtraFontInfo *PRVExtraFontInfo;

typedef System::Inifiles::TCustomIniFile TRVIniFile;

enum DECLSPEC_DENUM TRVSaveCSSOption : unsigned char { rvcssOnlyDifference, rvcssIgnoreLeftAlignment, rvcssNoDefCSSStyle, rvcssUTF8, rvcssDefault0Style, rvcssMarkersAsText };

typedef System::Set<TRVSaveCSSOption, TRVSaveCSSOption::rvcssOnlyDifference, TRVSaveCSSOption::rvcssMarkersAsText> TRVSaveCSSOptions;

enum DECLSPEC_DENUM TRVFontInfoProperty : unsigned char { rvfiFontName, rvfiSize, rvfiCharset, rvfiUnicode, rvfiBold, rvfiItalic, rvfiUnderline, rvfiStrikeout, rvfiOverline, rvfiAllCaps, rvfiSubSuperScriptType, rvfiVShift, rvfiColor, rvfiBackColor, rvfiJump, rvfiHoverBackColor, rvfiHoverColor, rvfiHoverUnderline, rvfiJumpCursor, rvfiNextStyleNo, rvfiProtection, rvfiCharScale, rvfiBaseStyleNo, rvfiBiDiMode, rvfiCharSpacing, rvfiHTMLCode, rvfiRTFCode, rvfiDocXCode, rvfiHidden, rvfiUnderlineType, rvfiUnderlineColor, rvfiHoverUnderlineColor, rvfiCustom, rvfiCustom2, rvfiCustom3, rvfiCustom4, rvfiCustom5 };

enum DECLSPEC_DENUM TRVParaInfoProperty : unsigned char { rvpiFirstIndent, rvpiLeftIndent, rvpiRightIndent, rvpiSpaceBefore, rvpiSpaceAfter, rvpiAlignment, rvpiNextParaNo, rvpiDefStyleNo, rvpiLineSpacing, rvpiLineSpacingType, rvpiBackground_Color, rvpiBackground_BO_Left, rvpiBackground_BO_Top, rvpiBackground_BO_Right, rvpiBackground_BO_Bottom, rvpiBorder_Color, rvpiBorder_Style, rvpiBorder_Width, rvpiBorder_InternalWidth, rvpiBorder_BO_Left, rvpiBorder_BO_Top, rvpiBorder_BO_Right, rvpiBorder_BO_Bottom, rvpiBorder_Vis_Left, rvpiBorder_Vis_Top, rvpiBorder_Vis_Right, rvpiBorder_Vis_Bottom, rvpiNoWrap, rvpiReadOnly, rvpiStyleProtect, rvpiDoNotWantReturns, rvpiKeepLinesTogether, rvpiKeepWithNext, rvpiTabs, rvpiBiDiMode, rvpiOutlineLevel, rvpiCustom, rvpiCustom2, 
	rvpiCustom3, rvpiCustom4, rvpiCustom5 };

typedef System::Set<TRVFontInfoProperty, TRVFontInfoProperty::rvfiFontName, TRVFontInfoProperty::rvfiCustom5> TRVFontInfoProperties;

typedef System::Set<TRVParaInfoProperty, TRVParaInfoProperty::rvpiFirstIndent, TRVParaInfoProperty::rvpiCustom5> TRVParaInfoProperties;

enum DECLSPEC_DENUM TRVLineSpacingType : unsigned char { rvlsPercent, rvlsSpaceBetween, rvlsLineHeightAtLeast, rvlsLineHeightExact };

typedef int TRVLineSpacingValue;

enum DECLSPEC_DENUM TRVStyleMergeMode : unsigned char { rvs_merge_SmartMerge, rvs_merge_Map, rvs_merge_Append };

enum DECLSPEC_DENUM TRVSelectionMode : unsigned char { rvsmChar, rvsmWord, rvsmParagraph };

enum DECLSPEC_DENUM TRVSelectionStyle : unsigned char { rvssItems, rvssLines };

enum DECLSPEC_DENUM TRVTextBackgroundKind : unsigned char { rvtbkSimple, rvtbkItems, rvtbkLines };

enum DECLSPEC_DENUM TRVTabAlign : unsigned char { rvtaLeft, rvtaRight, rvtaCenter };

typedef int TRVStyleTemplateId;

typedef System::UnicodeString TRVStyleTemplateName;

enum DECLSPEC_DENUM TRVStyleTemplateKind : unsigned char { rvstkParaText, rvstkPara, rvstkText };

typedef System::Set<TRVStyleTemplateKind, TRVStyleTemplateKind::rvstkParaText, TRVStyleTemplateKind::rvstkText> TRVStyleTemplateKinds;

enum DECLSPEC_DENUM TRVSpecialCharacter : unsigned char { rvscSpace, rvscNBSP, rvscTab, rvscParagraph, rvscSoftHyphen, rvscParagraphAttrs, rvscPlaceholders, rvcFloatingLines };

typedef System::Set<TRVSpecialCharacter, TRVSpecialCharacter::rvscSpace, TRVSpecialCharacter::rvcFloatingLines> TRVSpecialCharacters;

enum DECLSPEC_DENUM TRVParagraphMarks : unsigned char { rvpmStandard, rvpmArrows };

enum DECLSPEC_DENUM TRVFieldHighlightType : unsigned char { rvfhNever, rvfhCurrent, rvfhAlways };

enum DECLSPEC_DENUM TRVStyleUnits : unsigned char { rvstuPixels, rvstuTwips };

typedef TRVStyleUnits *PRVStyleUnits;

typedef int TRVStyleLength;

enum DECLSPEC_DENUM TRVColoredPart : unsigned char { rvcpBack, rvcpText };

typedef System::Set<TRVColoredPart, TRVColoredPart::rvcpBack, TRVColoredPart::rvcpText> TRVColoredParts;

enum DECLSPEC_DENUM TRVStyleTemplateInsertMode : unsigned char { rvstimUseTargetFormatting, rvstimUseSourceFormatting, rvstimIgnoreSourceStyleTemplates };

enum DECLSPEC_DENUM TRVSelectionHandleKind : unsigned char { rvshkWedge, rvshkCircle };

class DELPHICLASS TRVStyle;
typedef void __fastcall (__closure *TRVDrawTextBackEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int StyleNo, int Left, int Top, int Width, int Height, TRVTextDrawStates DrawState, bool &DoDefault);

typedef void __fastcall (__closure *TRVApplyStyleEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int StyleNo, bool &DoDefault);

typedef void __fastcall (__closure *TRVAfterApplyStyleEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int StyleNo);

typedef void __fastcall (__closure *TRVApplyStyleColorEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int StyleNo, TRVTextDrawStates DrawState, TRVColoredParts ApplyTo, bool &DoDefault);

typedef void __fastcall (__closure *TRVDrawStyleTextEvent)(TRVStyle* Sender, const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, int StyleNo, int SpaceBefore, int Left, int Top, int Width, int Height, TRVTextDrawStates DrawState, bool &DoDefault);

typedef void __fastcall (__closure *TRVStyleHoverSensitiveEvent)(TRVStyle* Sender, int StyleNo, bool &Sensitive);

typedef void __fastcall (__closure *TRVDrawCheckpointEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int X, int Y, int ItemNo, int XShift, bool RaiseEvent, Vcl::Controls::TControl* Control, bool &DoDefault);

typedef void __fastcall (__closure *TRVDrawPageBreakEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int Y, int XShift, TRVPageBreakType PageBreakType, Vcl::Controls::TControl* Control, bool &DoDefault);

typedef void __fastcall (__closure *TRVDrawParaRectEvent)(TRVStyle* Sender, Vcl::Graphics::TCanvas* Canvas, int ParaNo, const System::Types::TRect &ARect, bool &DoDefault);

typedef void __fastcall (__closure *TRVGetGraphicInterfaceEvent)(TRVStyle* Sender, Rvgrin::TRVGraphicInterface* &GraphicInterface);

class DELPHICLASS TCustomRVInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVInfo : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	int FBaseStyleNo;
	System::UnicodeString FName;
	bool FStandard;
	
protected:
	DYNAMIC bool __fastcall IsSimpleEqual(TCustomRVInfo* Value, bool IgnoreReferences, bool IgnoreID = true);
	DYNAMIC bool __fastcall IsSimpleEqualEx(TCustomRVInfo* Value, Rvclasses::TRVIntegerList* Mapping);
	DYNAMIC int __fastcall SimilarityValue(TCustomRVInfo* Value);
	
public:
	__fastcall virtual TCustomRVInfo(System::Classes::TCollection* Collection);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	DYNAMIC void __fastcall Reset(void);
	virtual System::UnicodeString __fastcall GetDisplayName(void);
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const System::UnicodeString DefName);
	DYNAMIC void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits) = 0 ;
	virtual TRVStyle* __fastcall GetRVStyle(void);
	
__published:
	__property int BaseStyleNo = {read=FBaseStyleNo, write=FBaseStyleNo, default=-1};
	__property System::UnicodeString StyleName = {read=FName, write=FName};
	__property bool Standard = {read=FStandard, write=FStandard, default=1};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TCustomRVInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TCustomFontOrParaRVInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomFontOrParaRVInfo : public TCustomRVInfo
{
	typedef TCustomRVInfo inherited;
	
private:
	TRVStyleTemplateId FStyleTemplateId;
	bool __fastcall StoreStyleTemplateId(void);
	
public:
	__fastcall virtual TCustomFontOrParaRVInfo(System::Classes::TCollection* Collection);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	HIDESBASE void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	HIDESBASE void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const System::UnicodeString DefName);
	
__published:
	__property TRVStyleTemplateId StyleTemplateId = {read=FStyleTemplateId, write=FStyleTemplateId, stored=StoreStyleTemplateId, nodefault};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TCustomFontOrParaRVInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TCustomRVFontInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVFontInfo : public TCustomFontOrParaRVInfo
{
	typedef TCustomFontOrParaRVInfo inherited;
	
private:
	Rvscroll::TRVBiDiMode FBiDiMode;
	System::Uitypes::TCursor FJumpCursor;
	System::Uitypes::TFontName FFontName;
	int FSizeDouble;
	System::Uitypes::TColor FColor;
	System::Uitypes::TColor FBackColor;
	System::Uitypes::TColor FHoverColor;
	System::Uitypes::TColor FHoverBackColor;
	System::Uitypes::TColor FUnderlineColor;
	System::Uitypes::TColor FHoverUnderlineColor;
	TRVUnderlineType FUnderlineType;
	TRVHoverEffects FHoverEffects;
	System::Uitypes::TFontStyles FStyle;
	TRVFontStyles FStyleEx;
	int FVShift;
	System::Uitypes::TFontCharset FCharset;
	TRVProtectOptions FProtection;
	TRVTextOptions FOptions;
	int FCharScale;
	TRVStyleLength FCharSpacing;
	TRVSubSuperScriptType FSubSuperScriptType;
	TRVStyleTemplateId FParaStyleTemplateId;
	void __fastcall SingleSymbolsReader(System::Classes::TReader* Reader);
	void __fastcall SizeReader(System::Classes::TReader* Reader);
	void __fastcall SizeDoubleReader(System::Classes::TReader* Reader);
	void __fastcall SizeWriter(System::Classes::TWriter* Writer);
	void __fastcall SizeDoubleWriter(System::Classes::TWriter* Writer);
	int __fastcall GetSize(void);
	void __fastcall SetSize(const int Value);
	
protected:
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	DYNAMIC bool __fastcall IsSimpleEqual(TCustomRVInfo* Value, bool IgnoreReferences, bool IgnoreID = true);
	DYNAMIC int __fastcall SimilarityValue(TCustomRVInfo* Value);
	int __fastcall GetScriptHeight(Vcl::Graphics::TCanvas* Canvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
	bool __fastcall CanUseTFont(TRVStyle* RVStyle, bool CanUseFontQuality);
	
public:
	__fastcall virtual TCustomRVFontInfo(System::Classes::TCollection* Collection);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall ResetMainProperties(void);
	void __fastcall ResetHoverProperties(void);
	DYNAMIC void __fastcall Reset(void);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	void __fastcall AssignSelectedProperties(TCustomRVFontInfo* Source, const TRVFontInfoProperties &Props);
	void __fastcall AssignToLogFont(tagLOGFONTW &LogFont, Vcl::Graphics::TCanvas* Canvas, bool CanUseCustomPPI, bool CanUseFontQuality, bool ExcludeUnderline, bool ToFormatCanvas, TRVStyle* RVStyle);
	void __fastcall Apply(Vcl::Graphics::TCanvas* Canvas, Rvscroll::TRVBiDiMode DefBiDiMode, bool CanUseCustomPPI, bool CanUseFontQuality, PRVExtraFontInfo ExtraFontInfo, bool IgnoreSubSuperScript, bool ToFormatCanvas, TRVStyle* RVStyle);
	void __fastcall ApplyBiDiMode(Vcl::Graphics::TCanvas* Canvas, Rvscroll::TRVBiDiMode DefBiDiMode, bool ToFormatCanvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
	void __fastcall ApplyColor(Vcl::Graphics::TCanvas* Canvas, TRVStyle* RVStyle, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	void __fastcall ApplyBackColor(Vcl::Graphics::TCanvas* Canvas, TRVStyle* RVStyle, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	void __fastcall ApplyTextColor(Vcl::Graphics::TCanvas* Canvas, TRVStyle* RVStyle, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	DYNAMIC bool __fastcall IsEqual(TCustomRVFontInfo* Value, const TRVFontInfoProperties &IgnoreList);
	HIDESBASEDYNAMIC void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const TRVFontInfoProperties &Properties);
	HIDESBASEDYNAMIC void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, bool JumpByDefault, System::Uitypes::TCursor DefJumpCursor);
	DYNAMIC void __fastcall SaveCSSToStream(System::Classes::TStream* Stream, TCustomRVFontInfo* BaseStyle, bool Multiline, bool UTF8, bool Jump);
	DYNAMIC void __fastcall SaveRTFToStream(System::Classes::TStream* Stream, Rvclasses::TRVIntegerList* StyleToFont, System::TObject* RTFTables, TRVStyle* RVStyle, bool InheritedStyle);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetOOXMLStr(TRVStyle* RVStyle, const TRVFontInfoProperties &Properties);
	DYNAMIC void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits);
	DYNAMIC TRVFontInfoProperties __fastcall CalculateModifiedProperties(TCustomRVFontInfo* Source, const TRVFontInfoProperties &PossibleProps);
	DYNAMIC TRVFontInfoProperties __fastcall CalculateModifiedProperties2(const TRVFontInfoProperties &PossibleProps);
	bool __fastcall IsSymbolCharset(void);
	__property int Size = {read=GetSize, write=SetSize, default=10};
	
__published:
	__property System::Uitypes::TFontCharset Charset = {read=FCharset, write=FCharset, default=1};
	__property int SizeDouble = {read=FSizeDouble, write=FSizeDouble, default=20};
	__property System::Uitypes::TFontName FontName = {read=FFontName, write=FFontName};
	__property System::Uitypes::TFontStyles Style = {read=FStyle, write=FStyle, default=0};
	__property int VShift = {read=FVShift, write=FVShift, default=0};
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=-16777208};
	__property System::Uitypes::TColor BackColor = {read=FBackColor, write=FBackColor, default=536870911};
	__property System::Uitypes::TColor HoverBackColor = {read=FHoverBackColor, write=FHoverBackColor, default=536870911};
	__property System::Uitypes::TColor HoverColor = {read=FHoverColor, write=FHoverColor, default=536870911};
	__property TRVHoverEffects HoverEffects = {read=FHoverEffects, write=FHoverEffects, default=0};
	__property TRVFontStyles StyleEx = {read=FStyleEx, write=FStyleEx, default=0};
	__property System::Uitypes::TCursor JumpCursor = {read=FJumpCursor, write=FJumpCursor, default=101};
	__property int CharScale = {read=FCharScale, write=FCharScale, default=100};
	__property TRVStyleLength CharSpacing = {read=FCharSpacing, write=FCharSpacing, default=0};
	__property Rvscroll::TRVBiDiMode BiDiMode = {read=FBiDiMode, write=FBiDiMode, default=0};
	__property TRVSubSuperScriptType SubSuperScriptType = {read=FSubSuperScriptType, write=FSubSuperScriptType, default=0};
	__property TRVProtectOptions Protection = {read=FProtection, write=FProtection, default=0};
	__property TRVTextOptions Options = {read=FOptions, write=FOptions, default=0};
	__property TRVUnderlineType UnderlineType = {read=FUnderlineType, write=FUnderlineType, default=0};
	__property System::Uitypes::TColor UnderlineColor = {read=FUnderlineColor, write=FUnderlineColor, default=536870911};
	__property System::Uitypes::TColor HoverUnderlineColor = {read=FHoverUnderlineColor, write=FHoverUnderlineColor, default=536870911};
	__property TRVStyleTemplateId ParaStyleTemplateId = {read=FParaStyleTemplateId, write=FParaStyleTemplateId, default=-1};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TCustomRVFontInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TFontInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TFontInfo : public TCustomRVFontInfo
{
	typedef TCustomRVFontInfo inherited;
	
private:
	bool FUnicode;
	bool FJump;
	int FNextStyleNo;
	TRVFontInfoProperties FModifiedProperties;
	void __fastcall ReadModifiedProps(System::Classes::TReader* Reader);
	void __fastcall WriteModifiedProps(System::Classes::TWriter* Writer);
	bool __fastcall StoreModifiedProperties(void);
	
protected:
	DYNAMIC bool __fastcall IsSimpleEqualEx(TCustomRVInfo* Value, Rvclasses::TRVIntegerList* Mapping);
	DYNAMIC bool __fastcall IsSimpleEqual(TCustomRVInfo* Value, bool IgnoreReferences, bool IgnoreID = true);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	DYNAMIC int __fastcall SimilarityValue(TCustomRVInfo* Value);
	
public:
	__fastcall virtual TFontInfo(System::Classes::TCollection* Collection);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	DYNAMIC void __fastcall Reset(void);
	DYNAMIC bool __fastcall IsEqual(TCustomRVFontInfo* Value, const TRVFontInfoProperties &IgnoreList);
	virtual void __fastcall Draw(const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, int ThisStyleNo, int SpaceAtLeft, int Left, int Top, int Width, int Height, TRVStyle* RVStyle, TRVTextDrawStates DrawState, bool Printing, bool PreviewCorrection, bool CanUseFontQuality, TRVColorMode ColorMode, Rvscroll::TRVBiDiMode DefBiDiMode, Vcl::Graphics::TCanvas* RefCanvas, Rvgrin::TRVGraphicInterface* GraphicInterface);
	void __fastcall DrawVertical(const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, int ThisStyleNo, int SpaceBefore, int Left, int Top, int Width, int Height, TRVStyle* RVStyle, TRVTextDrawStates DrawState);
	DYNAMIC void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const TRVFontInfoProperties &Properties);
	DYNAMIC void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, bool JumpByDefault, System::Uitypes::TCursor DefJumpCursor);
	DYNAMIC TRVFontInfoProperties __fastcall CalculateModifiedProperties(TCustomRVFontInfo* Source, const TRVFontInfoProperties &PossibleProps);
	DYNAMIC TRVFontInfoProperties __fastcall CalculateModifiedProperties2(const TRVFontInfoProperties &PossibleProps);
	void __fastcall IncludeModifiedProperties(TCustomRVFontInfo* Source, const TRVFontInfoProperties &PossibleProps);
	void __fastcall IncludeModifiedProperties2(const TRVFontInfoProperties &PossibleProps);
	__property TRVFontInfoProperties ModifiedProperties = {read=FModifiedProperties, write=FModifiedProperties};
	
__published:
	__property int NextStyleNo = {read=FNextStyleNo, write=FNextStyleNo, default=-1};
	__property bool Unicode = {read=FUnicode, write=FUnicode, nodefault};
	__property bool Jump = {read=FJump, write=FJump, default=0};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TFontInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TErrFontInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TErrFontInfo : public TFontInfo
{
	typedef TFontInfo inherited;
	
private:
	TRVStyle* FRVStyle;
	
public:
	__fastcall TErrFontInfo(TRVStyle* RVStyle);
	virtual TRVStyle* __fastcall GetRVStyle(void);
public:
	/* TFontInfo.Create */ inline __fastcall virtual TErrFontInfo(System::Classes::TCollection* Collection) : TFontInfo(Collection) { }
	
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TErrFontInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TCustomRVInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVInfos : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
protected:
	System::Classes::TPersistent* FOwner;
	
public:
	__fastcall TCustomRVInfos(System::Classes::TCollectionItemClass ItemClass, System::Classes::TPersistent* Owner);
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	void __fastcall MergeWith(TCustomRVInfos* Styles, TRVStyleMergeMode Mode, Rvclasses::TRVIntegerList* Mapping, Rvclasses::TRVIntegerList* TextStyleMapping, System::Classes::TPersistent* RVData = (System::Classes::TPersistent*)(0x0));
	void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits);
	int __fastcall FindDuplicate(int Index);
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TCustomRVInfos(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TFontInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TFontInfos : public TCustomRVInfos
{
	typedef TCustomRVInfos inherited;
	
public:
	TFontInfo* operator[](int Index) { return Items[Index]; }
	
private:
	TFontInfo* FInvalidItem;
	HIDESBASE TFontInfo* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TFontInfo* Value);
	TFontInfo* __fastcall GetInvalidItem(void);
	void __fastcall SetInvalidItem(TFontInfo* const Value);
	
public:
	int PixelsPerInch;
	__fastcall virtual ~TFontInfos(void);
	int __fastcall FindStyleWithCharset(int BaseStyle, System::Uitypes::TFontCharset Charset);
	int __fastcall FindStyleWithFontStyle(int BaseStyle, System::Uitypes::TFontStyles Value, System::Uitypes::TFontStyles Mask);
	int __fastcall FindStyleWithFontSize(int BaseStyle, int Size);
	int __fastcall FindStyleWithColor(int BaseStyle, System::Uitypes::TColor Color, System::Uitypes::TColor BackColor);
	int __fastcall FindStyleWithFontName(int BaseStyle, const System::Uitypes::TFontName FontName);
	int __fastcall FindSuchStyle(int BaseStyle, TFontInfo* Style, const TRVFontInfoProperties &Mask);
	int __fastcall FindSuchStyleEx(int BaseStyle, TFontInfo* Style, const TRVFontInfoProperties &Mask);
	int __fastcall FindStyleWithFont(int BaseStyle, Vcl::Graphics::TFont* Font);
	HIDESBASE TFontInfo* __fastcall Add(void);
	TFontInfo* __fastcall AddFont(System::Uitypes::TFontName Name, int Size, System::Uitypes::TColor Color, System::Uitypes::TColor BackColor, System::Uitypes::TFontStyles Style);
	TFontInfo* __fastcall AddFontEx(System::Uitypes::TFontName Name, int Size, System::Uitypes::TColor Color, System::Uitypes::TColor BackColor, System::Uitypes::TFontStyles Style, System::Uitypes::TFontCharset Charset);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, System::Uitypes::TCursor DefJumpCursor);
	__property TFontInfo* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
	__property TFontInfo* InvalidItem = {read=GetInvalidItem, write=SetInvalidItem};
public:
	/* TCustomRVInfos.Create */ inline __fastcall TFontInfos(System::Classes::TCollectionItemClass ItemClass, System::Classes::TPersistent* Owner) : TCustomRVInfos(ItemClass, Owner) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVRect;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVRect : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Classes::TPersistent* FOwner;
	TRVStyleLength FTop;
	TRVStyleLength FLeft;
	TRVStyleLength FRight;
	TRVStyleLength FBottom;
	bool __fastcall IsEqualEx(TRVRect* Value, bool IgnL, bool IgnT, bool IgnR, bool IgnB);
	int __fastcall SimilarityValue(TRVRect* Value, TRVStyleLength Weight, int Wht);
	
public:
	__fastcall TRVRect(System::Classes::TPersistent* AOwner);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall AssignValidProperties(TRVRect* Source, bool ValL, bool ValT, bool ValR, bool ValB);
	void __fastcall SetAll(TRVStyleLength Value);
	void __fastcall InflateRect(System::Types::TRect &Rect, TRVStyle* RVStyle, bool AllowNegativeValues);
	void __fastcall InflateRectSaD(System::Types::TRect &Rect, TRVStyle* RVStyle, const TRVScreenAndDevice &sad, bool AllowNegativeValues);
	void __fastcall AssignToRect(System::Types::TRect &Rect);
	void __fastcall AssignToRectIfGreater(System::Types::TRect &Rect);
	bool __fastcall IsEqual(TRVRect* Value);
	bool __fastcall IsAllEqual(int Value);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, bool L, bool T, bool R, bool B);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall ConvertToDifferentUnits(TRVStyle* RVStyle, TRVStyleUnits NewUnits);
	__property System::Classes::TPersistent* Owner = {read=FOwner};
	
__published:
	__property TRVStyleLength Left = {read=FLeft, write=FLeft, default=0};
	__property TRVStyleLength Right = {read=FRight, write=FRight, default=0};
	__property TRVStyleLength Top = {read=FTop, write=FTop, default=0};
	__property TRVStyleLength Bottom = {read=FBottom, write=FBottom, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVRect(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVRect(void) : System::Classes::TPersistent() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBooleanRect;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBooleanRect : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	bool FTop;
	bool FLeft;
	bool FRight;
	bool FBottom;
	bool __fastcall IsEqualEx(TRVBooleanRect* Value, bool IgnL, bool IgnT, bool IgnR, bool IgnB);
	
public:
	__fastcall TRVBooleanRect(bool DefValue);
	void __fastcall SetAll(bool Value);
	void __fastcall SetValues(bool ALeft, bool ATop, bool ARight, bool ABottom);
	void __fastcall GetValues(bool &ALeft, bool &ATop, bool &ARight, bool &ABottom);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall AssignValidProperties(TRVBooleanRect* Source, bool ValL, bool ValT, bool ValR, bool ValB);
	bool __fastcall IsEqual(TRVBooleanRect* Value);
	bool __fastcall IsEqual2(bool ALeft, bool ATop, bool ARight, bool ABottom);
	bool __fastcall IsAllEqual(bool Value);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, bool L, bool T, bool R, bool B);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	
__published:
	__property bool Left = {read=FLeft, write=FLeft, default=1};
	__property bool Right = {read=FRight, write=FRight, default=1};
	__property bool Top = {read=FTop, write=FTop, default=1};
	__property bool Bottom = {read=FBottom, write=FBottom, default=1};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVBooleanRect(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVBorder;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBorder : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Uitypes::TColor FColor;
	TRVBorderStyle FStyle;
	TRVStyleLength FWidthX;
	TRVStyleLength FInternalWidthX;
	TRVBooleanRect* FVisibleBorders;
	TRVRect* FBorderOffsets;
	System::Classes::TPersistent* FOwner;
	void __fastcall SetBorderOffsets(TRVRect* const Value);
	void __fastcall SetVisibleBorders(TRVBooleanRect* const Value);
	int __fastcall SimilarityValue(TRVBorder* Value, int Wht);
	
protected:
	TRVStyle* __fastcall GetRVStyle(void);
	virtual TRVRect* __fastcall CreateBorderOffsets(void);
	void __fastcall DoDraw(const System::Types::TRect &ARect, Vcl::Graphics::TCanvas* Canvas, int WidthLR, int WidthTB, int InternalWidthLR, int InternalWidthTB, TRVColorMode ColorMode, Rvgrin::TRVGraphicInterface* GraphicInterface);
	DYNAMIC bool __fastcall AllowNegativeOffsets(void);
	
public:
	__fastcall TRVBorder(System::Classes::TPersistent* AOwner);
	__fastcall virtual ~TRVBorder(void);
	void __fastcall Reset(void);
	void __fastcall Draw(const System::Types::TRect &Rect, Vcl::Graphics::TCanvas* Canvas, TRVStyle* RVStyle, Rvgrin::TRVGraphicInterface* GraphicInterface);
	void __fastcall DrawSaD(const System::Types::TRect &Rect, Vcl::Graphics::TCanvas* Canvas, TRVStyle* RVStyle, const TRVScreenAndDevice &sad, TRVColorMode ColorMode, Rvgrin::TRVGraphicInterface* GraphicInterface);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	bool __fastcall IsEqual(TRVBorder* Value);
	bool __fastcall IsEqual_Para(TRVBorder* Value, const TRVParaInfoProperties &IgnoreList);
	void __fastcall AssignValidProperties(TRVBorder* Source, const TRVParaInfoProperties &ValidProperties);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const TRVParaInfoProperties &Properties);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	TRVStyleLength __fastcall GetTotalWidth(void);
	void __fastcall ConvertToDifferentUnits(TRVStyle* RVStyle, TRVStyleUnits NewUnits);
	__property System::Classes::TPersistent* Owner = {read=FOwner};
	
__published:
	__property TRVStyleLength Width = {read=FWidthX, write=FWidthX, default=1};
	__property TRVStyleLength InternalWidth = {read=FInternalWidthX, write=FInternalWidthX, default=1};
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=-16777208};
	__property TRVBorderStyle Style = {read=FStyle, write=FStyle, default=0};
	__property TRVBooleanRect* VisibleBorders = {read=FVisibleBorders, write=SetVisibleBorders};
	__property TRVRect* BorderOffsets = {read=FBorderOffsets, write=SetBorderOffsets};
};

#pragma pack(pop)

class DELPHICLASS TRVBackgroundRect;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVBackgroundRect : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TRVRect* FBorderOffsets;
	System::Uitypes::TColor FColor;
	System::Classes::TPersistent* FOwner;
	void __fastcall SetBorderOffsets(TRVRect* const Value);
	int __fastcall SimilarityValue(TRVBackgroundRect* Value, int Wht);
	
protected:
	virtual TRVRect* __fastcall CreateBorderOffsets(void);
	DYNAMIC bool __fastcall AllowNegativeOffsets(void);
	
public:
	__fastcall TRVBackgroundRect(System::Classes::TPersistent* AOwner);
	__fastcall virtual ~TRVBackgroundRect(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall Reset(void);
	void __fastcall PrepareDraw(System::Types::TRect &Rect, TRVStyle* RVStyle);
	void __fastcall PrepareDrawSaD(System::Types::TRect &Rect, TRVStyle* RVStyle, const TRVScreenAndDevice &sad);
	void __fastcall Draw(const System::Types::TRect &Rect, Vcl::Graphics::TCanvas* Canvas, bool Printing, TRVColorMode ColorMode, Rvgrin::TRVGraphicInterface* GraphicInterface);
	bool __fastcall IsEqual(TRVBackgroundRect* Value);
	bool __fastcall IsEqual_Para(TRVBackgroundRect* Value, const TRVParaInfoProperties &IgnoreList);
	void __fastcall AssignValidProperties(TRVBackgroundRect* Source, const TRVParaInfoProperties &ValidProperties);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const TRVParaInfoProperties &Properties);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	__property System::Classes::TPersistent* Owner = {read=FOwner};
	
__published:
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=536870911};
	__property TRVRect* BorderOffsets = {read=FBorderOffsets, write=SetBorderOffsets};
};

#pragma pack(pop)

class DELPHICLASS TRVTabInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTabInfo : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	TRVStyleLength FPosition;
	System::UnicodeString FLeader;
	TRVTabAlign FAlign;
	bool __fastcall StoreLeader(void);
	void __fastcall SetPosition(const TRVStyleLength Value);
	
protected:
	virtual System::UnicodeString __fastcall GetDisplayName(void);
	
public:
	bool __fastcall IsEqual(TRVTabInfo* Value);
	int __fastcall SimilarityValue(TRVTabInfo* Value, int Wht);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	
__published:
	__property TRVTabAlign Align = {read=FAlign, write=FAlign, default=0};
	__property TRVStyleLength Position = {read=FPosition, write=SetPosition, nodefault};
	__property System::UnicodeString Leader = {read=FLeader, write=FLeader, stored=StoreLeader};
public:
	/* TCollectionItem.Create */ inline __fastcall virtual TRVTabInfo(System::Classes::TCollection* Collection) : System::Classes::TCollectionItem(Collection) { }
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TRVTabInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVTabInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVTabInfos : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TRVTabInfo* operator[](int Index) { return Items[Index]; }
	
private:
	System::Classes::TPersistent* FOwner;
	HIDESBASE TRVTabInfo* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TRVTabInfo* Value);
	
public:
	__fastcall TRVTabInfos(System::Classes::TPersistent* Owner);
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	HIDESBASE TRVTabInfo* __fastcall Add(void);
	void __fastcall SortTabs(void);
	bool __fastcall IsEqual(TRVTabInfos* Value);
	int __fastcall Find(int Position);
	int __fastcall SimilarityValue(TRVTabInfos* Value, int Wht);
	void __fastcall Intersect(TRVTabInfos* Value);
	void __fastcall AddFrom(TRVTabInfos* Source);
	void __fastcall DeleteList(Rvclasses::TRVIntegerList* Positions);
	void __fastcall ConvertToDifferentUnits(TRVStyle* RVStyle, TRVStyleUnits NewUnits);
	__property TRVTabInfo* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TRVTabInfos(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TCustomRVParaInfo;
class DELPHICLASS TParaInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRVParaInfo : public TCustomFontOrParaRVInfo
{
	typedef TCustomFontOrParaRVInfo inherited;
	
private:
	TRVStyleLength FFirstIndent;
	TRVStyleLength FLeftIndent;
	TRVStyleLength FRightIndent;
	TRVStyleLength FSpaceBefore;
	TRVStyleLength FSpaceAfter;
	TRVLineSpacingValue FLineSpacing;
	TRVLineSpacingType FLineSpacingType;
	TRVAlignment FAlignment;
	TRVBorder* FBorder;
	TRVBackgroundRect* FBackground;
	TRVParaOptions FOptions;
	Rvscroll::TRVBiDiMode FBiDiMode;
	int FOutlineLevel;
	TRVTabInfos* FTabs;
	void __fastcall SetBorder(TRVBorder* const Value);
	void __fastcall SetBackground(TRVBackgroundRect* const Value);
	bool __fastcall ExtraLineSpacing(void);
	void __fastcall SetTabs(TRVTabInfos* const Value);
	void __fastcall SetSpaceAfter(const TRVStyleLength Value);
	void __fastcall SetSpaceBefore(const TRVStyleLength Value);
	
protected:
	DYNAMIC bool __fastcall IsSimpleEqual(TCustomRVInfo* Value, bool IgnoreReferences, bool IgnoreID = true);
	DYNAMIC int __fastcall SimilarityValue(TCustomRVInfo* Value);
	
public:
	__fastcall virtual TCustomRVParaInfo(System::Classes::TCollection* Collection);
	__fastcall virtual ~TCustomRVParaInfo(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	DYNAMIC void __fastcall Reset(void);
	void __fastcall ResetMainProperties(void);
	void __fastcall ResetBorderAndBackProperties(void);
	void __fastcall AssignSelectedProperties(TCustomRVParaInfo* Source, const TRVParaInfoProperties &Props);
	bool __fastcall IsHeading(int MaxLevel);
	HIDESBASEDYNAMIC void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const TRVParaInfoProperties &Properties);
	HIDESBASEDYNAMIC void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	DYNAMIC void __fastcall SaveRTFToStream(System::Classes::TStream* Stream, System::TObject* RTFTables, TRVStyleLength MinAllowedTabPos, const Rvtypes::TRVAnsiString FILIText, TRVStyle* RVStyle);
	DYNAMIC Rvtypes::TRVAnsiString __fastcall GetOOXMLStr(TRVStyle* RVStyle, TRVStyleLength MinAllowedTabPos, TRVTabInfos* ParentTabs, const TRVParaInfoProperties &Properties);
	DYNAMIC bool __fastcall IsEqual(TCustomRVParaInfo* Value, const TRVParaInfoProperties &IgnoreList);
	DYNAMIC void __fastcall SaveCSSToStream(System::Classes::TStream* Stream, TParaInfo* BaseStyle, bool Multiline, bool IgnoreLeftAlignment, bool IgnoreLeftIndents);
	DYNAMIC void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits);
	DYNAMIC TRVParaInfoProperties __fastcall CalculateModifiedProperties(TCustomRVParaInfo* Source, const TRVParaInfoProperties &PossibleProps);
	DYNAMIC TRVParaInfoProperties __fastcall CalculateModifiedProperties2(const TRVParaInfoProperties &PossibleProps);
	
__published:
	__property TRVStyleLength FirstIndent = {read=FFirstIndent, write=FFirstIndent, default=0};
	__property TRVStyleLength LeftIndent = {read=FLeftIndent, write=FLeftIndent, default=0};
	__property TRVStyleLength RightIndent = {read=FRightIndent, write=FRightIndent, default=0};
	__property TRVStyleLength SpaceBefore = {read=FSpaceBefore, write=SetSpaceBefore, default=0};
	__property TRVStyleLength SpaceAfter = {read=FSpaceAfter, write=SetSpaceAfter, default=0};
	__property TRVAlignment Alignment = {read=FAlignment, write=FAlignment, default=0};
	__property TRVBorder* Border = {read=FBorder, write=SetBorder};
	__property TRVBackgroundRect* Background = {read=FBackground, write=SetBackground};
	__property TRVLineSpacingValue LineSpacing = {read=FLineSpacing, write=FLineSpacing, default=100};
	__property TRVLineSpacingType LineSpacingType = {read=FLineSpacingType, write=FLineSpacingType, default=0};
	__property TRVParaOptions Options = {read=FOptions, write=FOptions, default=0};
	__property Rvscroll::TRVBiDiMode BiDiMode = {read=FBiDiMode, write=FBiDiMode, default=0};
	__property int OutlineLevel = {read=FOutlineLevel, write=FOutlineLevel, default=0};
	__property TRVTabInfos* Tabs = {read=FTabs, write=SetTabs};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TParaInfo : public TCustomRVParaInfo
{
	typedef TCustomRVParaInfo inherited;
	
private:
	int FNextParaNo;
	int FDefStyleNo;
	TRVParaInfoProperties FModifiedProperties;
	void __fastcall ReadModifiedProps(System::Classes::TReader* Reader);
	void __fastcall WriteModifiedProps(System::Classes::TWriter* Writer);
	bool __fastcall StoreModifiedProperties(void);
	
protected:
	DYNAMIC bool __fastcall IsSimpleEqualEx(TCustomRVInfo* Value, Rvclasses::TRVIntegerList* Mapping);
	DYNAMIC bool __fastcall IsSimpleEqual(TCustomRVInfo* Value, bool IgnoreReferences, bool IgnoreID = true);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	
public:
	__fastcall virtual TParaInfo(System::Classes::TCollection* Collection);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	DYNAMIC void __fastcall Reset(void);
	DYNAMIC bool __fastcall IsEqual(TCustomRVParaInfo* Value, const TRVParaInfoProperties &IgnoreList);
	DYNAMIC void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs, const TRVParaInfoProperties &Properties);
	DYNAMIC void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall IncludeModifiedProperties(TCustomRVParaInfo* Source, const TRVParaInfoProperties &PossibleProps);
	void __fastcall IncludeModifiedProperties2(const TRVParaInfoProperties &PossibleProps);
	__property TRVParaInfoProperties ModifiedProperties = {read=FModifiedProperties, write=FModifiedProperties};
	
__published:
	__property int NextParaNo = {read=FNextParaNo, write=FNextParaNo, default=-1};
	__property int DefStyleNo = {read=FDefStyleNo, write=FDefStyleNo, default=-1};
public:
	/* TCustomRVParaInfo.Destroy */ inline __fastcall virtual ~TParaInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TErrParaInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TErrParaInfo : public TParaInfo
{
	typedef TParaInfo inherited;
	
private:
	TRVStyle* FRVStyle;
	
public:
	__fastcall TErrParaInfo(TRVStyle* RVStyle);
	virtual TRVStyle* __fastcall GetRVStyle(void);
public:
	/* TParaInfo.Create */ inline __fastcall virtual TErrParaInfo(System::Classes::TCollection* Collection) : TParaInfo(Collection) { }
	
public:
	/* TCustomRVParaInfo.Destroy */ inline __fastcall virtual ~TErrParaInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TParaInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TParaInfos : public TCustomRVInfos
{
	typedef TCustomRVInfos inherited;
	
public:
	TParaInfo* operator[](int Index) { return Items[Index]; }
	
private:
	TParaInfo* FInvalidItem;
	HIDESBASE TParaInfo* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TParaInfo* Value);
	TParaInfo* __fastcall GetInvalidItem(void);
	void __fastcall SetInvalidItem(TParaInfo* const Value);
	
public:
	HIDESBASE TParaInfo* __fastcall Add(void);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	__fastcall virtual ~TParaInfos(void);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	int __fastcall FindSuchStyle(int BaseStyle, TParaInfo* Style, const TRVParaInfoProperties &Mask);
	int __fastcall FindSuchStyleEx(int BaseStyle, TParaInfo* Style, const TRVParaInfoProperties &Mask);
	int __fastcall FindStyleWithAlignment(int BaseStyle, TRVAlignment Alignment);
	__property TParaInfo* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
	__property TParaInfo* InvalidItem = {read=GetInvalidItem, write=SetInvalidItem};
public:
	/* TCustomRVInfos.Create */ inline __fastcall TParaInfos(System::Classes::TCollectionItemClass ItemClass, System::Classes::TPersistent* Owner) : TCustomRVInfos(ItemClass, Owner) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVMarkerFont;
class PASCALIMPLEMENTATION TRVMarkerFont : public Vcl::Graphics::TFont
{
	typedef Vcl::Graphics::TFont inherited;
	
private:
	bool __fastcall StoreName(void);
	bool __fastcall StoreHeight(void);
	
public:
	__fastcall TRVMarkerFont(void);
	bool __fastcall IsEqual(Vcl::Graphics::TFont* Font);
	bool __fastcall IsDefault(void);
	
__published:
	__property Charset = {default=1};
	__property Color = {default=-16777208};
	__property Name = {stored=StoreName, default=0};
	__property Style = {default=0};
	__property Height = {stored=StoreHeight};
public:
	/* TFont.Destroy */ inline __fastcall virtual ~TRVMarkerFont(void) { }
	
};


class DELPHICLASS TRVHTMLListCSSItem;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVHTMLListCSSItem : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Rvtypes::TRVAnsiString Text;
public:
	/* TObject.Create */ inline __fastcall TRVHTMLListCSSItem(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TRVHTMLListCSSItem(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVHTMLListCSSList;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVHTMLListCSSList : public Rvclasses::TRVList
{
	typedef Rvclasses::TRVList inherited;
	
public:
	HIDESBASE int __fastcall Find(const Rvtypes::TRVAnsiString Text);
	HIDESBASE bool __fastcall Add(const Rvtypes::TRVAnsiString Text);
public:
	/* TRVList.Destroy */ inline __fastcall virtual ~TRVHTMLListCSSList(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TRVHTMLListCSSList(void) : Rvclasses::TRVList() { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVListLevel;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVListLevel : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	TRVListType FListType;
	Vcl::Graphics::TPicture* FPicture;
	Vcl::Imglist::TCustomImageList* FImageList;
	int FImageIndex;
	TRVMarkerFormatString FFormatString;
	TRVMarkerFormatStringW FFormatStringW;
	TRVStyleLength FLeftIndent;
	TRVStyleLength FFirstIndent;
	TRVStyleLength FMarkerIndent;
	TRVMarkerAlignment FMarkerAlignment;
	TRVMarkerFont* FFont;
	TRVListLevelOptions FOptions;
	int FStartFrom;
	Vcl::Graphics::TPicture* __fastcall GetPicture(void);
	void __fastcall SetPicture(Vcl::Graphics::TPicture* const Value);
	TRVMarkerFont* __fastcall GetFont(void);
	void __fastcall SetFont(TRVMarkerFont* const Value);
	bool __fastcall StoreFont(void);
	bool __fastcall StorePicture(void);
	void __fastcall ImageListTagWriter(System::Classes::TWriter* Writer);
	void __fastcall ImageListTagReader(System::Classes::TReader* Reader);
	void __fastcall FormatStringWCodeWriter(System::Classes::TWriter* Writer);
	void __fastcall FormatStringWCodeReader(System::Classes::TReader* Reader);
	void __fastcall FormatStringCodeWriter(System::Classes::TWriter* Writer);
	void __fastcall FormatStringCodeReader(System::Classes::TReader* Reader);
	void __fastcall FormatStringCodeWReader(System::Classes::TReader* Reader);
	void __fastcall FormatStringCodeWWriter(System::Classes::TWriter* Writer);
	bool __fastcall StoreImageList(void);
	System::Classes::TPersistent* __fastcall GetRVFRVData(void);
	
protected:
	virtual System::UnicodeString __fastcall GetDisplayName(void);
	bool __fastcall IsSimpleEqual(TRVListLevel* Value);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	int __fastcall SimilarityValue(TRVListLevel* Value);
	bool __fastcall IsCertainBullet(System::Word const *WideChars, const int WideChars_High, const Rvtypes::TRVSetOfAnsiChar &SymbolSet, const Rvtypes::TRVSetOfAnsiChar &WingdingsSet);
	bool __fastcall IsCircleBullet(void);
	bool __fastcall IsDiscBullet(void);
	bool __fastcall IsRectBullet(void);
	
public:
	__fastcall virtual TRVListLevel(System::Classes::TCollection* Collection);
	__fastcall virtual ~TRVListLevel(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	System::UnicodeString __fastcall GetHTMLOpenTagForCSS(void);
	Rvtypes::TRVAnsiString __fastcall GetIndentCSSForTextVersion(TRVStyle* RVStyle);
	void __fastcall HTMLOpenTag(System::Classes::TStream* Stream, bool UseCSS, TRVStyle* RVStyle, TRVSaveOptions SaveOptions, TRVHTMLListCSSList* ListBullets);
	Rvtypes::TRVAnsiString __fastcall GetHTMLOpenCSS(TRVStyle* RVStyle);
	void __fastcall HTMLCloseTag(System::Classes::TStream* Stream, bool UseCSS);
	bool __fastcall HasPicture(void);
	bool __fastcall UsesFont(void);
	bool __fastcall HasNumbering(void);
	bool __fastcall HasVariableWidth(void);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits, TRVStyle* RVStyle);
	
__published:
	__property TRVListType ListType = {read=FListType, write=FListType, default=0};
	__property int StartFrom = {read=FStartFrom, write=FStartFrom, default=1};
	__property Vcl::Imglist::TCustomImageList* ImageList = {read=FImageList, write=FImageList, stored=StoreImageList};
	__property int ImageIndex = {read=FImageIndex, write=FImageIndex, default=0};
	__property TRVMarkerFormatString FormatString = {read=FFormatString, write=FFormatString, stored=false};
	__property TRVMarkerFormatStringW FormatStringW = {read=FFormatStringW, write=FFormatStringW, stored=false};
	__property TRVStyleLength LeftIndent = {read=FLeftIndent, write=FLeftIndent, default=0};
	__property TRVStyleLength FirstIndent = {read=FFirstIndent, write=FFirstIndent, default=10};
	__property TRVStyleLength MarkerIndent = {read=FMarkerIndent, write=FMarkerIndent, default=0};
	__property TRVMarkerAlignment MarkerAlignment = {read=FMarkerAlignment, write=FMarkerAlignment, default=0};
	__property Vcl::Graphics::TPicture* Picture = {read=GetPicture, write=SetPicture, stored=StorePicture};
	__property TRVMarkerFont* Font = {read=GetFont, write=SetFont, stored=StoreFont};
	__property TRVListLevelOptions Options = {read=FOptions, write=FOptions, default=3};
};

#pragma pack(pop)

class DELPHICLASS TRVListLevelCollection;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVListLevelCollection : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TRVListLevel* operator[](int Index) { return Items[Index]; }
	
private:
	System::Classes::TPersistent* FOwner;
	HIDESBASE TRVListLevel* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TRVListLevel* const Value);
	
public:
	__fastcall TRVListLevelCollection(System::Classes::TPersistent* Owner);
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	HIDESBASE TRVListLevel* __fastcall Add(void);
	HIDESBASE TRVListLevel* __fastcall Insert(int Index);
	bool __fastcall IsSimpleEqual(TRVListLevelCollection* Value);
	void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits, TRVStyle* RVStyle);
	__property TRVListLevel* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TRVListLevelCollection(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVListInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVListInfo : public TCustomRVInfo
{
	typedef TCustomRVInfo inherited;
	
private:
	TRVListLevelCollection* FLevels;
	bool FOneLevelPreview;
	int FListID;
	void __fastcall SetLevels(TRVListLevelCollection* const Value);
	int __fastcall GetListID(void);
	void __fastcall ReadListID(System::Classes::TReader* Reader);
	void __fastcall WriteListID(System::Classes::TWriter* Writer);
	
protected:
	DYNAMIC int __fastcall SimilarityValue(TCustomRVInfo* Value);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	
public:
	DYNAMIC bool __fastcall IsSimpleEqual(TCustomRVInfo* Value, bool IgnoreReferences, bool IgnoreID = true);
	DYNAMIC bool __fastcall IsSimpleEqualEx(TCustomRVInfo* Value, Rvclasses::TRVIntegerList* Mapping);
	__fastcall virtual TRVListInfo(System::Classes::TCollection* Collection);
	__fastcall virtual ~TRVListInfo(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	HIDESBASEDYNAMIC void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	HIDESBASEDYNAMIC void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall SaveCSSToStream(System::Classes::TStream* Stream, TRVHTMLListCSSList* Bullets, TRVSaveCSSOptions SaveOptions, int &Index, const Rvtypes::TRVAnsiString Title);
	bool __fastcall HasNumbering(void);
	bool __fastcall AllNumbered(void);
	bool __fastcall HasVariableWidth(void);
	__property int ListID = {read=GetListID, nodefault};
	DYNAMIC void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits);
	
__published:
	__property TRVListLevelCollection* Levels = {read=FLevels, write=SetLevels};
	__property bool OneLevelPreview = {read=FOneLevelPreview, write=FOneLevelPreview, default=0};
};

#pragma pack(pop)

class DELPHICLASS TRVListInfos;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVListInfos : public TCustomRVInfos
{
	typedef TCustomRVInfos inherited;
	
public:
	TRVListInfo* operator[](int Index) { return Items[Index]; }
	
private:
	HIDESBASE TRVListInfo* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TRVListInfo* const Value);
	void __fastcall RemoveImageList(Vcl::Imglist::TCustomImageList* ImageList);
	
public:
	System::Classes::TPersistent* FRVData;
	bool FRichViewAllowAssignListID;
	HIDESBASE TRVListInfo* __fastcall Add(void);
	HIDESBASE TRVListInfo* __fastcall Insert(int Index);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	void __fastcall SaveCSSToStream(System::Classes::TStream* Stream, TRVHTMLListCSSList* Bullets, TRVSaveCSSOptions SaveOptions, const Rvtypes::TRVAnsiString Title);
	int __fastcall FindSuchStyle(TRVListInfo* Style, bool AddIfNotFound);
	int __fastcall FindStyleWithLevels(TRVListLevelCollection* Levels, const System::UnicodeString StyleNameForAdding, bool AddIfNotFound);
	__property TRVListInfo* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCustomRVInfos.Create */ inline __fastcall TRVListInfos(System::Classes::TCollectionItemClass ItemClass, System::Classes::TPersistent* Owner) : TCustomRVInfos(ItemClass, Owner) { }
	
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TRVListInfos(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TRVFontInfoClass;

typedef System::TMetaClass* TRVParaInfoClass;

typedef System::TMetaClass* TRVListInfoClass;

class DELPHICLASS TRVSTFontInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSTFontInfo : public TCustomRVFontInfo
{
	typedef TCustomRVFontInfo inherited;
	
private:
	System::Classes::TPersistent* FOwner;
	void __fastcall SetNoProp(const int Value);
	
public:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual TRVStyle* __fastcall GetRVStyle(void);
	
__published:
	__property int StyleTemplateId = {write=SetNoProp, nodefault};
	__property int StyleName = {write=SetNoProp, nodefault};
	__property int Standard = {write=SetNoProp, nodefault};
	__property int BaseStyleNo = {write=SetNoProp, nodefault};
public:
	/* TCustomRVFontInfo.Create */ inline __fastcall virtual TRVSTFontInfo(System::Classes::TCollection* Collection) : TCustomRVFontInfo(Collection) { }
	
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TRVSTFontInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSTParaInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSTParaInfo : public TCustomRVParaInfo
{
	typedef TCustomRVParaInfo inherited;
	
private:
	System::Classes::TPersistent* FOwner;
	void __fastcall SetNoProp(const int Value);
	
public:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual TRVStyle* __fastcall GetRVStyle(void);
	
__published:
	__property int StyleTemplateId = {write=SetNoProp, nodefault};
	__property int StyleName = {write=SetNoProp, nodefault};
	__property int Standard = {write=SetNoProp, nodefault};
	__property int BaseStyleNo = {write=SetNoProp, nodefault};
public:
	/* TCustomRVParaInfo.Create */ inline __fastcall virtual TRVSTParaInfo(System::Classes::TCollection* Collection) : TCustomRVParaInfo(Collection) { }
	/* TCustomRVParaInfo.Destroy */ inline __fastcall virtual ~TRVSTParaInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVSTListInfo;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVSTListInfo : public TRVListInfo
{
	typedef TRVListInfo inherited;
	
private:
	System::Classes::TPersistent* FOwner;
	void __fastcall SetNoProp(const int Value);
	
public:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	virtual TRVStyle* __fastcall GetRVStyle(void);
	
__published:
	__property int StyleTemplateId = {write=SetNoProp, nodefault};
	__property int StyleName = {write=SetNoProp, nodefault};
	__property int Standard = {write=SetNoProp, nodefault};
	__property int BaseStyleNo = {write=SetNoProp, nodefault};
public:
	/* TRVListInfo.Create */ inline __fastcall virtual TRVSTListInfo(System::Classes::TCollection* Collection) : TRVListInfo(Collection) { }
	/* TRVListInfo.Destroy */ inline __fastcall virtual ~TRVSTListInfo(void) { }
	
};

#pragma pack(pop)

class DELPHICLASS TRVStyleTemplate;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStyleTemplate : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	TRVStyleTemplateName FName;
	TRVStyleTemplateId FId;
	TRVStyleTemplateId FParentId;
	bool FQuickAccess;
	TRVStyleTemplateId FNextId;
	TRVSTFontInfo* FTextStyle;
	TRVSTParaInfo* FParaStyle;
	TRVSTListInfo* FListStyle;
	TRVFontInfoProperties FValidTextProperties;
	TRVParaInfoProperties FValidParaProperties;
	TRVStyleTemplate* FParent;
	TRVStyleTemplate* FNext;
	System::Classes::TList* FChildren;
	TRVStyleTemplateKind FKind;
	TRVStyleTemplateId __fastcall GetId(void);
	void __fastcall SetTextStyle(TRVSTFontInfo* const Value);
	void __fastcall SetParaStyle(TRVSTParaInfo* const Value);
	void __fastcall ReadID(System::Classes::TReader* Reader);
	void __fastcall WriteID(System::Classes::TWriter* Writer);
	void __fastcall AddChild(TRVStyleTemplate* Child);
	void __fastcall RemoveChild(TRVStyleTemplate* Child);
	void __fastcall SetParentId(TRVStyleTemplateId Value);
	void __fastcall SetNextId(const TRVStyleTemplateId Value);
	void __fastcall UpdateReferences(void);
	void __fastcall SetName(const TRVStyleTemplateName Value);
	void __fastcall ReadValidParaProps(System::Classes::TReader* Reader);
	void __fastcall WriteValidParaProps(System::Classes::TWriter* Writer);
	void __fastcall ReadValidTextProps(System::Classes::TReader* Reader);
	void __fastcall WriteValidTextProps(System::Classes::TWriter* Writer);
	void __fastcall SetValidParaPropertiesEdit(const System::UnicodeString Value);
	System::UnicodeString __fastcall GetValidParaPropertiesEdit(void);
	System::UnicodeString __fastcall GetValidTextPropertiesEdit(void);
	void __fastcall SetValidTextPropertiesEdit(const System::UnicodeString Value);
	void __fastcall SetKind(const TRVStyleTemplateKind Value);
	void __fastcall AdjustKind(void);
	void __fastcall AdjustParentId(void);
	
protected:
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	TRVFontInfoProperties __fastcall AssignToTextStyle(TCustomRVFontInfo* ATextStyle, const TRVFontInfoProperties &AllowedProps, bool Additive);
	void __fastcall AssignToParaStyle(TCustomRVParaInfo* AParaStyle, const TRVParaInfoProperties &AllowedProps);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TRVStyleTemplate(System::Classes::TCollection* Collection);
	__fastcall virtual ~TRVStyleTemplate(void);
	virtual System::UnicodeString __fastcall GetDisplayName(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	bool __fastcall IsAncestorFor(TRVStyleTemplate* StyleTemplate);
	bool __fastcall IsInheritedFrom(TRVStyleTemplateName Name);
	void __fastcall UpdateModifiedTextStyleProperties(TFontInfo* ATextStyle, TRVStyleTemplate* ParaStyleTemplate);
	void __fastcall UpdateModifiedParaStyleProperties(TParaInfo* AParaStyle);
	void __fastcall ApplyToTextStyle(TCustomRVFontInfo* ATextStyle, TRVStyleTemplate* ParaStyleTemplate);
	void __fastcall ApplyToParaStyle(TCustomRVParaInfo* AParaStyle);
	TRVTabInfos* __fastcall GetTabs(void);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString fs);
	__property System::Classes::TList* Children = {read=FChildren};
	__property TRVStyleTemplate* Parent = {read=FParent};
	__property TRVStyleTemplate* Next = {read=FNext};
	__property TRVFontInfoProperties ValidTextProperties = {read=FValidTextProperties, write=FValidTextProperties};
	__property TRVParaInfoProperties ValidParaProperties = {read=FValidParaProperties, write=FValidParaProperties};
	
__published:
	__property TRVSTFontInfo* TextStyle = {read=FTextStyle, write=SetTextStyle};
	__property TRVSTParaInfo* ParaStyle = {read=FParaStyle, write=SetParaStyle};
	__property TRVStyleTemplateName Name = {read=FName, write=SetName};
	__property TRVStyleTemplateId Id = {read=GetId, nodefault};
	__property TRVStyleTemplateId ParentId = {read=FParentId, write=SetParentId, default=-1};
	__property TRVStyleTemplateId NextId = {read=FNextId, write=SetNextId, default=-1};
	__property TRVStyleTemplateKind Kind = {read=FKind, write=SetKind, default=0};
	__property bool QuickAccess = {read=FQuickAccess, write=FQuickAccess, default=1};
	__property System::UnicodeString ValidParaPropertiesEditor = {read=GetValidParaPropertiesEdit, write=SetValidParaPropertiesEdit, stored=false};
	__property System::UnicodeString ValidTextPropertiesEditor = {read=GetValidTextPropertiesEdit, write=SetValidTextPropertiesEdit, stored=false};
};

#pragma pack(pop)

class DELPHICLASS TRVStyleTemplateCollection;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVStyleTemplateCollection : public System::Classes::TCollection
{
	typedef System::Classes::TCollection inherited;
	
public:
	TRVStyleTemplate* operator[](int Index) { return Items[Index]; }
	
private:
	int FNameCounter;
	System::UnicodeString FDefStyleName;
	TRVStyleTemplate* FNormalStyleTemplate;
	Rvclasses::TRVIntegerList* FForbiddenIds;
	HIDESBASE TRVStyleTemplate* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TRVStyleTemplate* const Value);
	void __fastcall AssignUniqueNameTo(TRVStyleTemplate* Item);
	bool __fastcall StoreDefStyleName(void);
	Rvclasses::TRVIntegerList* __fastcall GetForbiddenIds(void);
	
protected:
	System::Classes::TPersistent* FOwner;
	
public:
	__fastcall TRVStyleTemplateCollection(System::Classes::TPersistent* Owner);
	__fastcall virtual ~TRVStyleTemplateCollection(void);
	void __fastcall ResetNameCounter(void);
	void __fastcall Sort(void);
	HIDESBASE TRVStyleTemplate* __fastcall Add(void);
	int __fastcall FindById(TRVStyleTemplateId Id);
	TRVStyleTemplate* __fastcall FindItemById(TRVStyleTemplateId Id);
	int __fastcall FindByName(const TRVStyleTemplateName Name);
	TRVStyleTemplate* __fastcall FindItemByName(const TRVStyleTemplateName Name);
	void __fastcall AssignToStrings(System::Classes::TStrings* Strings, bool AssignIds, TRVStyleTemplateKinds Kinds, TRVStyleTemplate* ExcludeThisStyle, bool OnlyPotentialParents);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall AssignStyleTemplates(TRVStyleTemplateCollection* Source, bool CopyIds);
	void __fastcall ClearParaFormat(TCustomRVParaInfo* AParaStyle);
	void __fastcall ClearTextFormat(TCustomRVFontInfo* ATextStyle);
	void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits);
	void __fastcall MergeWith(TRVStyleTemplateCollection* Value, Rvclasses::TRVIntegerList* Map);
	void __fastcall UpdateReferences(void);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section);
	bool __fastcall LoadFromRVST(const System::UnicodeString FileName, TRVStyleUnits Units);
	bool __fastcall SaveToRVST(const System::UnicodeString FileName, TRVStyleUnits Units);
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner(void);
	TRVStyleTemplateName __fastcall GetUniqueName(TRVStyleTemplate* IgnoreItem);
	TRVStyleTemplateName __fastcall MakeNameUnique(const TRVStyleTemplateName Name);
	__property Rvclasses::TRVIntegerList* ForbiddenIds = {read=GetForbiddenIds};
	__property TRVStyleTemplate* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
	__property TRVStyleTemplate* NormalStyleTemplate = {read=FNormalStyleTemplate};
	
__published:
	__property System::UnicodeString DefStyleName = {read=FDefStyleName, write=FDefStyleName, stored=StoreDefStyleName};
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TRVStyle : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	Vcl::Graphics::TPicture* FInvalidPicture;
	System::Uitypes::TColor FColor;
	System::Uitypes::TColor FHoverColor;
	System::Uitypes::TColor FCurrentItemColor;
	System::Uitypes::TColor FSelColor;
	System::Uitypes::TColor FSelTextColor;
	System::Uitypes::TColor FInactiveSelColor;
	System::Uitypes::TColor FInactiveSelTextColor;
	System::Uitypes::TColor FCheckpointColor;
	System::Uitypes::TColor FCheckpointEvColor;
	System::Uitypes::TColor FFloatingLineColor;
	System::Uitypes::TColor FSpecialCharactersColor;
	System::Uitypes::TColor FGridColor;
	System::Uitypes::TColor FGridReadOnlyColor;
	System::Uitypes::TColor FPageBreakColor;
	System::Uitypes::TColor FSoftPageBreakColor;
	System::Uitypes::TColor FLiveSpellingColor;
	System::Uitypes::TColor FFieldHighlightColor;
	System::Uitypes::TColor FDisabledFontColor;
	System::Uitypes::TCursor FJumpCursor;
	System::Uitypes::TCursor FLineSelectCursor;
	TFontInfos* FTextStyles;
	TParaInfos* FParaStyles;
	TRVListInfos* FListStyles;
	bool FFullRedraw;
	bool FFootnotePageReset;
	bool FDefaultUnicodeStyles;
	bool FUseSound;
	int FSpacesInTab;
	TRVStyleLength FDefTabWidth;
	TRVApplyStyleColorEvent FOnApplyStyleColor;
	TRVApplyStyleEvent FOnApplyStyle;
	TRVAfterApplyStyleEvent FOnAfterApplyStyle;
	TRVDrawStyleTextEvent FOnDrawStyleText;
	TRVStyleHoverSensitiveEvent FOnStyleHoverSensitive;
	TRVDrawTextBackEvent FOnDrawTextBack;
	TRVDrawCheckpointEvent FOnDrawCheckpoint;
	TRVDrawPageBreakEvent FOnDrawPageBreak;
	TRVDrawParaRectEvent FOnDrawParaBack;
	TRVFieldHighlightType FFieldHighlightType;
	TRVSeqType FFootnoteNumbering;
	TRVSeqType FEndnoteNumbering;
	TRVSeqType FSidenoteNumbering;
	int FDefUnicodeStyle;
	TRVCodePage FDefCodePage;
	TRVSelectionMode FSelectionMode;
	TRVSelectionStyle FSelectionStyle;
	TRVTextBackgroundKind FTextBackgroundKind;
	Vcl::Graphics::TPenStyle FGridStyle;
	Vcl::Graphics::TPenStyle FGridReadOnlyStyle;
	TRVLineWrapMode FLineWrapMode;
	System::Uitypes::TFontQuality FFontQuality;
	TRVStyleTemplateCollection* FStyleTemplates;
	TRVStyle* FMainRVStyle;
	bool FSaveStyleTemplatesForNonMain;
	TRVStyleUnits FUnits;
	TRVSelectionHandleKind FSelectionHandleKind;
	void __fastcall SetTextStyles(TFontInfos* Value);
	void __fastcall SetParaStyles(TParaInfos* Value);
	void __fastcall SetListStyles(TRVListInfos* Value);
	System::Uitypes::TColor __fastcall GetHoverColorByColor(System::Uitypes::TColor Color);
	Vcl::Graphics::TPicture* __fastcall GetInvalidPicture(void);
	void __fastcall SetInvalidPicture(Vcl::Graphics::TPicture* const Value);
	void __fastcall SetStyleTemplates(TRVStyleTemplateCollection* const Value);
	TRVStyleTemplateCollection* __fastcall GetStyleTemplates(void);
	bool __fastcall StoreStyleTemplates(void);
	void __fastcall SetMainRVStyle(TRVStyle* Value);
	
protected:
	Rvgrin::TRVGraphicInterface* FGraphicInterface;
	virtual void __fastcall ReadState(System::Classes::TReader* Reader);
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall Loaded(void);
	DYNAMIC void __fastcall CreateGraphicInterface(void);
	
public:
	int ItemNo;
	int OffsetInItem;
	System::Classes::TPersistent* RVData;
	bool IgnoreStyleTemplates;
	void __fastcall ResetTextStyles(void);
	void __fastcall ResetParaStyles(void);
	__fastcall virtual TRVStyle(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVStyle(void);
	virtual TRVFontInfoClass __fastcall GetTextStyleClass(void);
	virtual TRVParaInfoClass __fastcall GetParaStyleClass(void);
	virtual TRVListInfoClass __fastcall GetListStyleClass(void);
	void __fastcall GetNextTab(int ParaNo, int X, const TRVScreenAndDevice &sad, int &Position, System::UnicodeString &Leader, TRVTabAlign &Align, Rvscroll::TRVBiDiMode DefBiDiMode, int LeftIndent, int RightIndent);
	int __fastcall AddTextStyle _DEPRECATED_ATTRIBUTE0 (void);
	void __fastcall DeleteTextStyle _DEPRECATED_ATTRIBUTE0 (int Index);
	void __fastcall SaveINI(const System::UnicodeString FileName, const System::UnicodeString Section);
	void __fastcall LoadINI(const System::UnicodeString FileName, const System::UnicodeString Section);
	void __fastcall SaveToINI(System::Inifiles::TCustomIniFile* ini, System::UnicodeString Section);
	void __fastcall LoadFromINI(System::Inifiles::TCustomIniFile* ini, System::UnicodeString Section);
	void __fastcall SaveReg(const System::UnicodeString BaseKey);
	void __fastcall LoadReg(const System::UnicodeString BaseKey);
	void __fastcall SaveCSSToStream(System::Classes::TStream* Stream, TRVSaveCSSOptions AOptions);
	bool __fastcall SaveCSS(const System::UnicodeString FileName, TRVSaveCSSOptions AOptions);
	Rvtypes::TRVAnsiString __fastcall GetCSSSize(TRVStyleLength Value);
	Rvtypes::TRVAnsiString __fastcall GetCSSSizeInPoints(TRVStyleLength Value);
	System::Uitypes::TColor __fastcall GetHoverColor(int StyleNo);
	void __fastcall DrawTextBack(Vcl::Graphics::TCanvas* Canvas, int ItemNo, int StyleNo, System::Classes::TPersistent* RVData, int Left, int Top, int Width, int Height, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	void __fastcall ApplyStyle(Vcl::Graphics::TCanvas* Canvas, int StyleNo, Rvscroll::TRVBiDiMode DefBiDiMode, bool CanUseCustomPPI, bool CanUseFontQuality, PRVExtraFontInfo ExtraFontInfo, bool IgnoreSubSuperScript, bool ToFormatCanvas);
	void __fastcall ApplyStyleColor(Vcl::Graphics::TCanvas* Canvas, int StyleNo, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	void __fastcall ApplyStyleBackColor(Vcl::Graphics::TCanvas* Canvas, int StyleNo, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	void __fastcall ApplyStyleTextColor(Vcl::Graphics::TCanvas* Canvas, int StyleNo, TRVTextDrawStates DrawState, bool Printing, TRVColorMode ColorMode);
	void __fastcall DrawStyleText(const Rvtypes::TRVRawByteString s, Vcl::Graphics::TCanvas* Canvas, int ItemNo, int OffsetInItem, int StyleNo, System::Classes::TPersistent* RVData, int SpaceBefore, int Left, int Top, int Width, int Height, TRVTextDrawStates DrawState, bool Printing, bool PreviewCorrection, bool CanUseFontQuality, TRVColorMode ColorMode, Rvscroll::TRVBiDiMode DefBiDiMode, Vcl::Graphics::TCanvas* RefCanvas);
	void __fastcall DrawCheckpoint(Vcl::Graphics::TCanvas* Canvas, int X, int Y, int AreaLeft, int Width, System::Classes::TPersistent* RVData, int ItemNo, int XShift, bool RaiseEvent, Vcl::Controls::TControl* Control);
	void __fastcall DrawPageBreak(Vcl::Graphics::TCanvas* Canvas, int Y, int XShift, TRVPageBreakType PageBreakType, Vcl::Controls::TControl* Control, System::Classes::TPersistent* RVData, int ItemNo);
	void __fastcall DrawParaBack(Vcl::Graphics::TCanvas* Canvas, int ParaNo, const System::Types::TRect &Rect, bool Printing, TRVColorMode ColorMode);
	bool __fastcall StyleHoverSensitive(int StyleNo);
	int __fastcall GetAsPixels(TRVStyleLength Value);
	int __fastcall GetAsDevicePixelsX(TRVStyleLength Value, PRVScreenAndDevice PSaD);
	int __fastcall GetAsDevicePixelsY(TRVStyleLength Value, PRVScreenAndDevice PSaD);
	int __fastcall GetAsTwips(TRVStyleLength Value);
	unsigned __fastcall GetAsEMU(TRVStyleLength Value);
	TRVStyleLength __fastcall GetAsDifferentUnits(TRVStyleLength Value, TRVStyleUnits Units);
	System::Extended __fastcall GetAsPoints(TRVStyleLength Value);
	TRVLength __fastcall GetAsRVUnits(TRVStyleLength Value, TRVUnits RVUnits);
	TRVStyleLength __fastcall TwipsToUnits(int Value);
	TRVStyleLength __fastcall PixelsToUnits(int Value);
	TRVStyleLength __fastcall DifferentUnitsToUnits(TRVStyleLength Value, TRVStyleUnits Units);
	TRVStyleLength __fastcall RVUnitsToUnits(TRVLength Value, TRVUnits RVUnits);
	__classmethod int __fastcall GetAsPixelsEx(TRVStyleLength Value, TRVStyleUnits Units);
	__classmethod int __fastcall GetAsTwipsEx(TRVStyleLength Value, TRVStyleUnits Units);
	__classmethod TRVLength __fastcall GetAsRVUnitsEx(TRVStyleLength Value, TRVStyleUnits Units, TRVUnits RVUnits);
	__classmethod TRVStyleLength __fastcall RVUnitsToUnitsEx(TRVLength Value, TRVUnits RVUnits, TRVStyleUnits Units);
	__classmethod TRVStyleLength __fastcall TwipsToUnitsEx(int Value, TRVStyleUnits Units);
	__classmethod TRVStyleLength __fastcall PixelsToUnitsEx(int Value, TRVStyleUnits Units);
	__classmethod int __fastcall RVUnitsToPixels(TRVLength Value, TRVUnits RVUnits);
	__classmethod TRVLength __fastcall PixelsToRVUnits(int Value, TRVUnits RVUnits);
	__classmethod int __fastcall PixelsToTwips(int Value);
	__classmethod int __fastcall TwipsToPixels(int Value);
	void __fastcall ConvertToDifferentUnits(TRVStyleUnits NewUnits);
	void __fastcall ConvertToTwips(void);
	void __fastcall ConvertToPixels(void);
	void __fastcall UpdateModifiedProperties(void);
	virtual int __fastcall FindTextStyle(TFontInfo* TextStyle);
	virtual int __fastcall FindParaStyle(TParaInfo* ParaStyle);
	__property bool DefaultUnicodeStyles = {read=FDefaultUnicodeStyles, write=FDefaultUnicodeStyles, nodefault};
	__property Rvgrin::TRVGraphicInterface* GraphicInterface = {read=FGraphicInterface};
	__property bool SaveStyleTemplatesForNonMain = {read=FSaveStyleTemplatesForNonMain, write=FSaveStyleTemplatesForNonMain, nodefault};
	
__published:
	__property TFontInfos* TextStyles = {read=FTextStyles, write=SetTextStyles};
	__property TParaInfos* ParaStyles = {read=FParaStyles, write=SetParaStyles};
	__property TRVListInfos* ListStyles = {read=FListStyles, write=SetListStyles};
	__property int SpacesInTab = {read=FSpacesInTab, write=FSpacesInTab, default=0};
	__property TRVStyleLength DefTabWidth = {read=FDefTabWidth, write=FDefTabWidth, default=48};
	__property System::Uitypes::TCursor JumpCursor = {read=FJumpCursor, write=FJumpCursor, default=101};
	__property System::Uitypes::TCursor LineSelectCursor = {read=FLineSelectCursor, write=FLineSelectCursor, default=106};
	__property bool FullRedraw = {read=FFullRedraw, write=FFullRedraw, default=0};
	__property bool UseSound = {read=FUseSound, write=FUseSound, default=1};
	__property System::Uitypes::TColor Color = {read=FColor, write=FColor, default=-16777211};
	__property System::Uitypes::TColor HoverColor = {read=FHoverColor, write=FHoverColor, default=536870911};
	__property System::Uitypes::TColor CurrentItemColor = {read=FCurrentItemColor, write=FCurrentItemColor, default=536870911};
	__property System::Uitypes::TColor SelColor = {read=FSelColor, write=FSelColor, default=-16777203};
	__property System::Uitypes::TColor SelTextColor = {read=FSelTextColor, write=FSelTextColor, default=-16777202};
	__property System::Uitypes::TColor InactiveSelColor = {read=FInactiveSelColor, write=FInactiveSelColor, default=-16777203};
	__property System::Uitypes::TColor InactiveSelTextColor = {read=FInactiveSelTextColor, write=FInactiveSelTextColor, default=-16777202};
	__property System::Uitypes::TColor CheckpointColor = {read=FCheckpointColor, write=FCheckpointColor, default=32768};
	__property System::Uitypes::TColor CheckpointEvColor = {read=FCheckpointEvColor, write=FCheckpointEvColor, default=65280};
	__property System::Uitypes::TColor PageBreakColor = {read=FPageBreakColor, write=FPageBreakColor, default=-16777200};
	__property System::Uitypes::TColor SoftPageBreakColor = {read=FSoftPageBreakColor, write=FSoftPageBreakColor, default=-16777201};
	__property System::Uitypes::TColor LiveSpellingColor = {read=FLiveSpellingColor, write=FLiveSpellingColor, default=255};
	__property System::Uitypes::TColor FloatingLineColor = {read=FFloatingLineColor, write=FFloatingLineColor, default=12427914};
	__property System::Uitypes::TColor SpecialCharactersColor = {read=FSpecialCharactersColor, write=FSpecialCharactersColor, default=536870911};
	__property System::Uitypes::TColor GridColor = {read=FGridColor, write=FGridColor, default=-16777201};
	__property System::Uitypes::TColor GridReadOnlyColor = {read=FGridReadOnlyColor, write=FGridReadOnlyColor, default=-16777201};
	__property Vcl::Graphics::TPenStyle GridStyle = {read=FGridStyle, write=FGridStyle, default=2};
	__property Vcl::Graphics::TPenStyle GridReadOnlyStyle = {read=FGridReadOnlyStyle, write=FGridReadOnlyStyle, default=5};
	__property TRVSelectionMode SelectionMode = {read=FSelectionMode, write=FSelectionMode, default=1};
	__property TRVSelectionStyle SelectionStyle = {read=FSelectionStyle, write=FSelectionStyle, default=0};
	__property TRVTextBackgroundKind TextBackgroundKind = {read=FTextBackgroundKind, write=FTextBackgroundKind, default=1};
	__property System::Uitypes::TColor FieldHighlightColor = {read=FFieldHighlightColor, write=FFieldHighlightColor, default=-16777201};
	__property TRVFieldHighlightType FieldHighlightType = {read=FFieldHighlightType, write=FFieldHighlightType, default=1};
	__property System::Uitypes::TColor DisabledFontColor = {read=FDisabledFontColor, write=FDisabledFontColor, default=536870911};
	__property TRVSeqType FootnoteNumbering = {read=FFootnoteNumbering, write=FFootnoteNumbering, default=0};
	__property TRVSeqType EndnoteNumbering = {read=FEndnoteNumbering, write=FEndnoteNumbering, default=3};
	__property TRVSeqType SidenoteNumbering = {read=FSidenoteNumbering, write=FSidenoteNumbering, default=1};
	__property bool FootnotePageReset = {read=FFootnotePageReset, write=FFootnotePageReset, default=1};
	__property TRVLineWrapMode LineWrapMode = {read=FLineWrapMode, write=FLineWrapMode, default=2};
	__property int DefUnicodeStyle = {read=FDefUnicodeStyle, write=FDefUnicodeStyle, default=-1};
	__property TRVCodePage DefCodePage = {read=FDefCodePage, write=FDefCodePage, default=0};
	__property Vcl::Graphics::TPicture* InvalidPicture = {read=GetInvalidPicture, write=SetInvalidPicture};
	__property TRVStyleUnits Units = {read=FUnits, write=FUnits, default=0};
	__property System::Uitypes::TFontQuality FontQuality = {read=FFontQuality, write=FFontQuality, default=0};
	__property TRVSelectionHandleKind SelectionHandleKind = {read=FSelectionHandleKind, write=FSelectionHandleKind, default=0};
	__property TRVApplyStyleEvent OnApplyStyle = {read=FOnApplyStyle, write=FOnApplyStyle};
	__property TRVAfterApplyStyleEvent OnAfterApplyStyle = {read=FOnAfterApplyStyle, write=FOnAfterApplyStyle};
	__property TRVApplyStyleColorEvent OnApplyStyleColor = {read=FOnApplyStyleColor, write=FOnApplyStyleColor};
	__property TRVDrawStyleTextEvent OnDrawStyleText = {read=FOnDrawStyleText, write=FOnDrawStyleText};
	__property TRVStyleHoverSensitiveEvent OnStyleHoverSensitive = {read=FOnStyleHoverSensitive, write=FOnStyleHoverSensitive};
	__property TRVDrawTextBackEvent OnDrawTextBack = {read=FOnDrawTextBack, write=FOnDrawTextBack};
	__property TRVDrawCheckpointEvent OnDrawCheckpoint = {read=FOnDrawCheckpoint, write=FOnDrawCheckpoint};
	__property TRVDrawPageBreakEvent OnDrawPageBreak = {read=FOnDrawPageBreak, write=FOnDrawPageBreak};
	__property TRVDrawParaRectEvent OnDrawParaBack = {read=FOnDrawParaBack, write=FOnDrawParaBack};
	__property TRVStyleTemplateCollection* StyleTemplates = {read=GetStyleTemplates, write=SetStyleTemplates, stored=StoreStyleTemplates};
	__property TRVStyle* MainRVStyle = {read=FMainRVStyle, write=SetMainRVStyle};
};


class DELPHICLASS TRVUnitsRect;
class PASCALIMPLEMENTATION TRVUnitsRect : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TRVLength FLeft;
	TRVLength FRight;
	TRVLength FTop;
	TRVLength FBottom;
	System::Classes::TPersistent* FOwner;
	
public:
	__fastcall TRVUnitsRect(System::Classes::TPersistent* AOwner);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	void __fastcall SetAll(TRVLength Value);
	void __fastcall ConvertToUnits(TRVUnits OldUnits, TRVUnits NewUnits);
	__property System::Classes::TPersistent* Owner = {read=FOwner};
	
__published:
	__property TRVLength Left = {read=FLeft, write=FLeft};
	__property TRVLength Right = {read=FRight, write=FRight};
	__property TRVLength Top = {read=FTop, write=FTop};
	__property TRVLength Bottom = {read=FBottom, write=FBottom};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TRVUnitsRect(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Int8 crJump = System::Int8(0x65);
static const System::Int8 crRVFlipArrow = System::Int8(0x6a);
static const System::Int8 crRVLayingIBeam = System::Int8(0x6b);
static const System::Int8 rvsNormal = System::Int8(0x0);
static const System::Int8 rvsHeading = System::Int8(0x1);
static const System::Int8 rvsSubheading = System::Int8(0x2);
static const System::Int8 rvsKeyword = System::Int8(0x3);
static const System::Int8 rvsJump1 = System::Int8(0x4);
static const System::Int8 rvsJump2 = System::Int8(0x5);
static const System::Int8 rvsBreak = System::Int8(-1);
static const System::Int8 rvsCheckpoint = System::Int8(-2);
static const System::Int8 rvsPicture = System::Int8(-3);
static const System::Int8 rvsHotspot = System::Int8(-4);
static const System::Int8 rvsComponent = System::Int8(-5);
static const System::Int8 rvsBullet = System::Int8(-6);
static const System::Int8 rvsBack = System::Int8(-7);
static const System::Int8 rvsVersionInfo = System::Int8(-8);
static const System::Int8 rvsDocProperty = System::Int8(-9);
static const System::Int8 rvsHotPicture = System::Int8(-10);
static const System::Int8 rvsListMarker = System::Int8(-11);
static const System::Int8 rvsTab = System::Int8(-12);
static const System::Int8 LAST_DEFAULT_STYLE_NO = System::Int8(0x5);
static const int rvsDefStyle = int(2147483647);
static const int DEFAULT_FLOATINGLINECOLOR = int(0xbda28a);
extern DELPHI_PACKAGE TRVRTFOptions rvrtfDefault;
extern DELPHI_PACKAGE TRVFontInfoProperties RVAllFontInfoProperties;
extern DELPHI_PACKAGE TRVParaInfoProperties RVAllParaInfoProperties;
extern DELPHI_PACKAGE TRVParaInfoProperties RVAllParaBackgroundProperties;
extern DELPHI_PACKAGE TRVParaInfoProperties RVAllParaBorderProperties;
extern DELPHI_PACKAGE bool RichViewResetStandardFlag;
extern DELPHI_PACKAGE bool RVNoLstIDProperty;
extern DELPHI_PACKAGE bool RichViewCompareStyleNames;
extern DELPHI_PACKAGE bool RichViewDoNotMergeNumbering;
extern DELPHI_PACKAGE TRVSpecialCharacters RVVisibleSpecialCharacters;
extern DELPHI_PACKAGE TRVParagraphMarks RVParagraphMarks;
extern DELPHI_PACKAGE TRVFontInfoClass DefRVFontInfoClass;
extern DELPHI_PACKAGE TRVParaInfoClass DefRVParaInfoClass;
extern DELPHI_PACKAGE TRVListInfoClass DefRVListInfoClass;
extern DELPHI_PACKAGE int RVFORMATCANVASFACTOR;
extern DELPHI_PACKAGE int RVFORMATCANVASRESOLUTION;
extern DELPHI_PACKAGE bool ScaleRichViewTextDrawAlwaysUseGlyphs;
extern DELPHI_PACKAGE bool RichViewAutoAssignNormalStyleTemplate;
#define RVEMPTYTAG L""
extern DELPHI_PACKAGE int __fastcall RVConvertFromFormatCanvas(int V, bool UseFormatCanvas);
extern DELPHI_PACKAGE int __fastcall RVConvertToFormatCanvas(int V, bool UseFormatCanvas);
extern DELPHI_PACKAGE void __fastcall WriteIntToIniIfNE(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString Key, int Value, int DefValue);
extern DELPHI_PACKAGE void __fastcall WriteBoolToIniIfNE(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString Key, bool Value, bool DefValue);
extern DELPHI_PACKAGE bool __fastcall IniReadBool(System::Inifiles::TCustomIniFile* ini, const System::UnicodeString Section, const System::UnicodeString Key, bool DefValue);
extern DELPHI_PACKAGE void __fastcall RVDrawUnderline(Vcl::Graphics::TCanvas* Canvas, TRVUnderlineType UnderlineType, System::Uitypes::TColor Color, int Left, int Right, int Y, int BaseLineWidth, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE void __fastcall RVDrawHiddenUnderline(Vcl::Graphics::TCanvas* Canvas, System::Uitypes::TColor Color, int Left, int Right, int Y, Rvgrin::TRVGraphicInterface* GraphicInterface);
extern DELPHI_PACKAGE void __fastcall RVWrite(System::Classes::TStream* Stream, const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE void __fastcall RVWriteLn(System::Classes::TStream* Stream, const Rvtypes::TRVAnsiString s);
extern DELPHI_PACKAGE void __fastcall RVWriteX(System::Classes::TStream* Stream, const Rvtypes::TRVAnsiString s, bool Multiline);
}	/* namespace Rvstyle */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSTYLE)
using namespace Rvstyle;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvstyleHPP
