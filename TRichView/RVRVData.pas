
{*******************************************************}
{                                                       }
{       RichView                                        }
{       TRichViewRVData represents RichView document.   }
{       This is a type of TRichView.RVData.             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVRVData;

interface

{$I RV_Defs.inc}

uses Windows, Classes, Controls, Graphics, Forms, StdCtrls,
     RVItem, RVBack, RVStyle, CRVData, CRVFData, RVCtrlData, RVScroll,
    {$IFDEF RICHVIEWDEF4}
     ImgList,
    {$ENDIF}
    {$IFNDEF RVDONOTUSELISTS}
     RVMarker,
    {$ENDIF}
    {$IFNDEF RVDONOTUSESEQ}
    RVSeqItem,
    {$ENDIF}
    {$IFNDEF RVDONOTUSEDRAGDROP}
     ActiveX, RVDragDrop,
    {$ENDIF}
    {$IFNDEF RVDONOTUSESMARTPOPUP}
    RVPopup,
    {$ENDIF}
    {$IFNDEF RVDONOTUSEDOCPARAMS}
    RVDocParams,
    {$ENDIF}
    {$IFDEF RICHVIEWDEF2010}
    RVSelectionHandles,
    {$ENDIF}
    RVClasses, RVFuncs, RVAnimate, RVTypes, RVGrIn, RVFontCache;

type

  TRVFGlobalReadData = class
    public
      RVFTextStylesMapping, RVFParaStylesMapping, RVFListStylesMapping: TRVIntegerList;
      Units: TRVStyleUnits;
      destructor Destroy; override;
  end;

  TRichViewRVData = class (TRVControlData)
  private
    function GetExtraDocumentsList: TStrings;
    procedure SetExtraDocumentsList(const Value: TStrings);
  protected
    FRVFGlobalReadData: TRVFGlobalReadData;
    FExtraDocuments: TStringList;
    FOnDrawHyperlink: TRVDataDrawHyperlinkEvent;
    FRichView: TRVScroller;
    FFontInfoCache: TRVFontInfoCache;
    FThumbnailCache: TRVThumbnailCache;
    FClickX, FClickY: Integer;
    {$IFDEF RICHVIEWDEF2010}
    FSelectionHandles: TRVSelectionHandles;
    {$ENDIF}
    {$IFNDEF RVDONOTUSESEQ}
    FSeqList: TRVSeqList;
    {$ENDIF}
    {$IFNDEF RVDONOTUSELISTS}
    FMarkers: TRVMarkerList;
    procedure DestroyMarkers; override;
    {$ENDIF}
    {$IFNDEF RVDONOTUSESEQ}
    procedure DestroySeqList; override;
    {$ENDIF}
    function GetInplaceEditor: TControl; override;
    procedure DestroyInplaceEditor; override;
    function GetRTFProperties: TPersistent{TRVRTFReaderProperties}; override;
    {$IFNDEF RVDONOTUSEDRAGDROP}
    { Drag & drop: drop from }
    function CanStartDragging: Boolean; override;
    function InitDragging(var DropSource: TRVDropSource;
      var OKEffect: Integer): Boolean; override;
    procedure DoneDragging(FDeleteSelection: Boolean); override;
    procedure SetClickCoords(X, Y: Integer); override;
    function CanStartDragBecauseMouseMoved(X, Y: Integer): Boolean; override;    
    {$ENDIF}
    function DoCreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
      Owner: TCustomRVFormattedData): TRVFontInfoCache;
    function GetMaxLength: Integer; override;
    function IsWordWrapAllowed: Boolean; override;
  public
    {$IFNDEF RVDONOTUSEANIMATION}
    FPlayingAnimation: Boolean;
    FAnimatorList: TRVAnimatorList;
    {$ENDIF}
    {$IFNDEF RVDONOTUSEDRAGDROP}
    FDropSource: TRVDropSource; // object implemeting IDropSource and IDataObject
    {$ENDIF}
    {$IFNDEF RVDONOTUSESMARTPOPUP}
    FSmartPopupButton: TRVSmartPopupButton;
    {$ENDIF}
    procedure InitStyleMappings(var PTextStylesMapping,
      PParaStylesMapping, PListStylesMapping: PRVIntegerList;
      var PUnits: PRVStyleUnits); override;
    procedure DoneStyleMappings(AsSubDoc: Boolean); override;
    procedure CreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas); override;
    procedure DestroyFontInfoCache(var Cache: TRVFontInfoCache); override;
    function GetFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
      RVData: TCustomRVFormattedData): TRVFontInfoCache; override;
    {$IFNDEF RVDONOTUSESMARTPOPUP}
    procedure AdjustSpecialControlsCoords(RVData: TCustomRVFormattedData); override;
    procedure SetSmartPopupCoords;
    procedure RepaintSmartPopup;
    //procedure InternalFreeItem(item: TCustomRVItemInfo; Clearing: Boolean); override;
    {$ENDIF}
    procedure Clear; override;
    destructor Destroy; override;
    function GetFormatCanvasEx(DefCanvas: TCanvas): TCanvas; override;    
    function CanLoadLayout: Boolean; override;
    function GetExtraRTFCode(Area: TRVRTFSaveArea; Obj: TObject;
      Index1, Index2: Integer; InStyleSheet: Boolean): TRVAnsiString; override;
    function GetExtraDocXCode(Area: TRVDocXSaveArea; Obj: TObject;
      Index1, Index2: Integer): TRVAnsiString; override;
    function GetExtraHTMLCode(Area: TRVHTMLSaveArea; CSSVersion: Boolean): String; override;
    function GetParaHTMLCode(RVData: TCustomRVData; ItemNo: Integer;
      ParaStart, CSSVersion: Boolean): String; override;
    function GetChosenRVData: TCustomRVData; override;
    function GetChosenItem: TCustomRVItemInfo; override;
    procedure AssignChosenRVData(RVData: TCustomRVFormattedData;
                                 Item: TCustomRVItemInfo); override;
    procedure SilentReplaceChosenRVData(RVData: TCustomRVFormattedData); override;
    procedure UnassignChosenRVData(RVData: TCustomRVData); override;
    function GetParentControl: TWinControl; override;
    function GetDoInPaletteMode: TRVPaletteAction; override;
    function GetRVPalette: HPALETTE; override;
    function GetRVStyle: TRVStyle; override;
    function GetRVLogPalette: PLogPalette; override;
    function GetThumbnailCache: TRVThumbnailCache; override;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    function UseStyleTemplates: Boolean; override;
    function StyleTemplateInsertMode: TRVStyleTemplateInsertMode; override;
    {$ENDIF}
    function GetURL(id: Integer): String; override;
    procedure ReadHyperlink(const Target, Extras: String; DocFormat: TRVLoadFormat;
      var StyleNo: Integer; var ItemTag: TRVTag; var ItemName: TRVRawByteString); override;
    procedure WriteHyperlink(id: Integer; RVData: TCustomRVData; ItemNo: Integer;
       SaveFormat: TRVSaveFormat; var Target, Extras: String); override;
    function GetOptions: TRVOptions; override;
    procedure SetOptions(const Value: TRVOptions); override;
    function GetDocProperties: TStringList; override;
    function GetRVFOptions: TRVFOptions; override;
    procedure SetRVFOptions(const Value: TRVFOptions); override;
    function GetRVFWarnings: TRVFWarnings; override;
    procedure SetRVFWarnings(const Value: TRVFWarnings); override;
    function GetRTFOptions: TRVRTFOptions; override;
    procedure SetRTFOptions(const Value: TRVRTFOptions); override;
    function GetAreaWidth: Integer; override;
    function GetAreaHeight: Integer; override;
    function GetFullDocumentHeight: Integer; override;
    procedure GetOrigin(var ALeft, ATop: Integer); override;
    procedure GetOriginEx(var ALeft, ATop: Integer); override;
    function GetMinTextWidth: Integer; override;
    function GetMaxTextWidth: Integer; override;
    function GetLeftMargin: Integer; override;
    function GetRightMargin: Integer; override;
    function GetTopMargin: Integer; override;
    function GetBottomMargin: Integer; override;
    function GetFlags: TRVFlags; override;
    procedure SetFlags(const Value: TRVFlags); override;
    procedure AdjustVScrollUnits; override;
    procedure SetDocumentAreaSize(Width,Height: Integer; UpdateH: Boolean); override;
    procedure ScrollTo(Y: Integer; Redraw: Boolean); override;
    procedure HScrollTo(X: Integer); override;
    function GetVSmallStep: Integer; override;
    function GetBackground: TRVBackground; override;

    function IsAssignedCopy: Boolean; override;
    function IsAssignedRVMouseDown: Boolean; override;
    function IsAssignedRVMouseUp: Boolean; override;
    function IsAssignedRVRightClick: Boolean; override;
    function IsAssignedJump: Boolean; override;
    function IsAssignedRVDblClick: Boolean; override;
    function IsAssignedCheckpointVisible: Boolean; override;
    function IsAssignedOnProgress: Boolean; override;

    procedure DoProgress(Operation: TRVLongOperation; Stage: TRVProgressStage;
      PercentDone: Byte); override;
    procedure DoCopy; override;
    procedure DoRVMouseMove(id: Integer); override;
    procedure DoRVMouseDown(Button: TMouseButton; Shift: TShiftState;
      ItemNo, X, Y: Integer); override;
    procedure DoRVMouseUp(Button: TMouseButton; Shift: TShiftState;
      ItemNo, X, Y: Integer); override;
    procedure DoRVRightClick(const ClickedWord: TRVRawByteString; StyleNo,
      X, Y: Integer); override;
    procedure DoRVDblClick(const ClickedWord: TRVRawByteString; StyleNo: Integer); override;
    procedure DoCheckpointVisible(CheckpointData: TCheckpointData); override;
    procedure DoDrawHyperlink(RVData: TCustomRVData; ItemNo: Integer; R: TRect); override;
    procedure DoGetItemCursor(RVData: TCustomRVData; ItemNo: Integer;
      var Cursor: TCursor); override;
    {$IFNDEF RVDONOTUSESTYLETEMPLATES}
    procedure DoStyleTemplatesChange; override;
    {$ENDIF}

    procedure DoJump(id: Integer); override;
    procedure DoSelect; override;
    function GetNormalCursor: TCursor; override;
    function GetCPEventKind: TCPEventKind; override;
    procedure HTMLSaveImage(RVData: TCustomRVData; ItemNo: Integer;
      const Path: String; BackgroundColor: TColor; var Location: String;
      var DoDefault: Boolean); override;
    procedure SaveImage2(RVData: TCustomRVData; ItemNo: Integer;
      Graphic: TGraphic; SaveFormat: TRVSaveFormat;
      const Path, ImagePrefix: String; var ImageSaveNo: Integer;
      var Location: String; var DoDefault: Boolean); override;
    constructor Create(RichView: TRVScroller); virtual;
    function GetHOffs: Integer; override;
    function GetVOffs: Integer; override;
    function GetRVDataExtraVOffs: Integer; override;    
    function GetCanvas: TCanvas; override;
    function GetWidth: Integer; override;
    function GetHeight: Integer; override;
    function GetColor: TColor; override;
    function SaveComponentToFile(const Path: String; SaveMe: TComponent;
      SaveFormat: TRVSaveFormat): String; override;
    function SaveItemToFile(const Path: String; RVData: TCustomRVData;
      ItemNo: Integer; SaveFormat: TRVSaveFormat; Unicode: Boolean;
      var Text: TRVRawByteString): Boolean; override;
    function ImportPicture(const Location: String;
      Width, Height: Integer; var Invalid: Boolean): TGraphic; override;
    function GetItemHint(RVData: TCustomRVData; ItemNo: Integer;
      const UpperRVDataHint: String): String; override;
    function RVFPictureNeeded(const ItemName: String; Item: TRVNonTextItemInfo;
      Index1, Index2: Integer; PictureInsideRVF: Boolean): TGraphic; override;
    function RVFControlNeeded(const ItemName: String;
      const ItemTag: TRVTag): TControl; override;
    procedure SetControlHint(const Hint: String); override;
    function RVFImageListNeeded(ImageListTag: Integer): TCustomImageList; override;
    function GetDelimiters: String; override;
    function GetRVFTextStylesReadMode: TRVFReaderStyleMode; override;
    function GetRVFParaStylesReadMode: TRVFReaderStyleMode; override;
    function GetBiDiMode: TRVBiDiMode; override;
    procedure ControlAction2(RVData: TCustomRVData; ControlAction: TRVControlAction;
      ItemNo: Integer; var Control:TControl); override;
    procedure ItemAction(ItemAction: TRVItemAction; Item: TCustomRVItemInfo;
      var Text: TRVRawByteString; RVData: TCustomRVData); override;
    procedure AfterAddStyle(StyleInfo: TCustomRVInfo); override;
    {$IFNDEF RVDONOTUSEDOCPARAMS}
    function GetDocParameters(AllowCreate: Boolean): TRVDocParameters; override;
    {$ENDIF}
    {$IFNDEF RVDONOTUSEDRAGDROP}
    { Drag & drop: drop to }
    function GetDragDropCaretInfo: TRVDragDropCaretInfo; override;
    { Drag & drop: drop from }
    function IsDragging: Boolean; override;
    {$ENDIF}
    {$IFDEF RICHVIEWDEF2010}
    { Touch-mode selection handles }
    function GetSelectionHandles: TRVSelectionHandles; override;
    procedure UpdateSelectionHandles(EvenForEmptySelection: Boolean);
    procedure CreateSelectionHandles(EvenForEmptySelection: Boolean);
    procedure CreateSelectionHandlesAfterTap; override;
    procedure DestroySelectionHandles;
    {$ENDIF}
    {$IFNDEF RVDONOTUSEANIMATION}
    procedure InsertAnimator(var Animator: TObject); override;
    function AllowAnimation: Boolean; override;
    procedure ResetAniBackground; override;
    {$ENDIF}
    {$IFNDEF RVDONOTUSESEQ}
    function GetSeqList(AllowCreate: Boolean): TRVSeqList; override;
    function GetNoteText: String; override;
    {$ENDIF}
    {$IFNDEF RVDONOTUSELISTS}
    function GetMarkers(AllowCreate: Boolean): TRVMarkerList; override;
    {$ENDIF}
    procedure DoBeforeSaving; override;
    function GetExtraDocuments: TStrings; override;
    function GetScaleRichViewInterface: IRVScaleRichViewInterface; override;
    property RichView: TRVScroller read FRichView;
    property OnDrawHyperlink: TRVDataDrawHyperlinkEvent read FOnDrawHyperlink write FOnDrawHyperlink;
    property ExtraDocumentsList: TStrings read GetExtraDocumentsList write SetExtraDocumentsList;
  end;

  TRichViewRVDataClass = class of TRichViewRVData;

implementation
uses
  {$IFDEF RICHVIEWDEF10}
  Types,
  {$ENDIF}
  RichView, RVUni;
{============================ TRVFGlobalReadData ==============================}
destructor TRVFGlobalReadData.Destroy;
begin
  RVFTextStylesMapping.Free;
  RVFParaStylesMapping.Free;
  RVFListStylesMapping.Free;
  inherited;
end;
{============================ TRichViewRVData =================================}
constructor TRichViewRVData.Create(RichView: TRVScroller);
begin
  inherited Create;
  FRichView := RichView;
end;
{------------------------------------------------------------------------------}
destructor TRichViewRVData.Destroy;
begin
  {$IFDEF RICHVIEWDEF2010}
  DestroySelectionHandles;
  {$ENDIF}
  {$IFNDEF RVDONOTUSELISTS}
  RVFreeAndNil(FMarkers);
  {$ENDIF}
  {$IFNDEF RVDONOTUSESEQ}
  RVFreeAndNil(FSeqList);
  {$ENDIF}
  {$IFNDEF RVDONOTUSEANIMATION}
  TCustomRichView(RichView).StopAnimation;
  RVFreeAndNil(FAnimatorList);
  {$ENDIF}
  FFontInfoCache.Free;
  FExtraDocuments.Free;
  FRVFGlobalReadData.Free;
  inherited Destroy;
  FThumbnailCache.Free;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetDoInPaletteMode: TRVPaletteAction;
begin
  Result := TCustomRichView(FRichView).DoInPaletteMode;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetOptions: TRVOptions;
begin
  Result := TCustomRichView(FRichView).Options;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetOptions(const Value: TRVOptions);
begin
  TCustomRichView(FRichView).Options := Value;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetParentControl: TWinControl;
begin
  Result := TWinControl(FRichView);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVFOptions: TRVFOptions;
begin
  Result := TCustomRichView(FRichView).RVFOptions;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetRVFOptions(const Value: TRVFOptions);
begin
  TCustomRichView(FRichView).RVFOptions := Value;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRTFOptions: TRVRTFOptions;
begin
  Result := TCustomRichView(FRichView).RTFOptions;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetRTFOptions(const Value: TRVRTFOptions);
begin
  TCustomRichView(FRichView).RTFOptions := Value;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVFWarnings: TRVFWarnings;
begin
  Result := TCustomRichView(FRichView).RVFWarnings;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetRVFWarnings(const Value: TRVFWarnings);
begin
  TCustomRichView(FRichView).RVFWarnings := Value;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVLogPalette: PLogPalette;
begin
  Result := TCustomRichView(FRichView).PRVLogPalette;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVPalette: HPALETTE;
begin
  Result := TCustomRichView(FRichView).RVPalette;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetThumbnailCache: TRVThumbnailCache;
begin
  if rvflRoot in Flags then begin
    if FThumbnailCache=nil then
      FThumbnailCache := TRVThumbnailCache.Create;
    Result := FThumbnailCache;
    end
  else
    Result := inherited GetThumbnailCache;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
function TRichViewRVData.UseStyleTemplates: Boolean;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(FRichView).UseStyleTemplates
  else
    Result := inherited UseStyleTemplates;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.StyleTemplateInsertMode: TRVStyleTemplateInsertMode;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(FRichView).StyleTemplateInsertMode
  else
    Result := inherited StyleTemplateInsertMode;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRichViewRVData.GetURL(id: Integer): String;
begin
  Result := '';
  if Assigned(TCustomRichView(FRichView).OnURLNeeded) then
    TCustomRichView(FRichView).OnURLNeeded(TCustomRichView(FRichView), id, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.CanLoadLayout: Boolean;
begin
  Result := True;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetExtraRTFCode(Area: TRVRTFSaveArea;
  Obj: TObject; Index1, Index2: Integer; InStyleSheet: Boolean): TRVAnsiString;
begin
  Result := '';
  if Assigned(TCustomRichView(FRichView).OnSaveRTFExtra) then
    TCustomRichView(FRichView).OnSaveRTFExtra(TCustomRichView(FRichView),
      Area, Obj, Index1, Index2, InStyleSheet, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetExtraDocXCode(Area: TRVDocXSaveArea; Obj: TObject;
  Index1, Index2: Integer): TRVAnsiString;
begin
  Result := '';
  if Assigned(TCustomRichView(FRichView).OnSaveDocXExtra) then
    TCustomRichView(FRichView).OnSaveDocXExtra(TCustomRichView(FRichView),
      Area, Obj, Index1, Index2, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetExtraHTMLCode(Area: TRVHTMLSaveArea;
  CSSVersion: Boolean): String;
begin
  Result := '';
  if Assigned(TCustomRichView(FRichView).OnSaveHTMLExtra) then
    TCustomRichView(FRichView).OnSaveHTMLExtra(TCustomRichView(FRichView),
      Area, CSSVersion, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetParaHTMLCode(RVData: TCustomRVData;
  ItemNo: Integer; ParaStart, CSSVersion: Boolean): String;
begin
  Result := '';
  if Assigned(TCustomRichView(FRichView).OnSaveParaToHTML) then
    TCustomRichView(FRichView).OnSaveParaToHTML(TCustomRichView(FRichView),
      RVData, ItemNo, ParaStart, CSSVersion, Result);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.ReadHyperlink(const Target, Extras: String;
  DocFormat: TRVLoadFormat; var StyleNo: Integer; var ItemTag: TRVTag;
  var ItemName: TRVRawByteString);
var Ifc: IRVScaleRichViewInterface;
begin
  case StyleNo of
    rvsPicture:
      StyleNo := rvsHotPicture;
    rvsBullet:
      StyleNo := rvsHotspot;
  end;
  Ifc := GetScaleRichViewInterface;
  if Assigned(TCustomRichView(FRichView).OnReadHyperlink) and
    ((Ifc=nil) or Ifc.IsOnReadHyperlinkAssigned) then
    TCustomRichView(FRichView).OnReadHyperlink(TCustomRichView(FRichView),
      Target, Extras, DocFormat, StyleNo, ItemTag, ItemName)
  {$IFNDEF RVOLDTAGS}
  else
    ItemTag := Target;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.WriteHyperlink(id: Integer; RVData: TCustomRVData;
  ItemNo: Integer; SaveFormat: TRVSaveFormat;
  var Target, Extras: String);
var Ifc: IRVScaleRichViewInterface;
begin
  Target := '';
  Extras := '';
  if rvflRoot in Flags then begin
    {$IFNDEF RVDONOTUSEITEMHINTS}
    RVData.GetItem(ItemNo).GetExtraStrProperty(rvespHint, Extras);
    Extras := RV_GetHintStr(SaveFormat, Extras);
    {$ENDIF}
    Ifc := GetScaleRichViewInterface;
    if Assigned(TCustomRichView(FRichView).OnWriteHyperlink) and
      ((Ifc=nil) or Ifc.IsOnWriteHyperlinkAssigned) then
      TCustomRichView(FRichView).OnWriteHyperlink(TCustomRichView(FRichView),
        id, RVData, ItemNo, SaveFormat, Target, Extras)
    else if Assigned(TCustomRichView(FRichView).OnURLNeeded) then
      Target := GetURL(id)
    {$IFNDEF RVOLDTAGS}
    else
      Target := RVData.GetItemTag(ItemNo);
    {$ENDIF}
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.RVFControlNeeded(const ItemName: String;
  const ItemTag: TRVTag): TControl;
begin
  Result := nil;
  if Assigned(  TCustomRichView(FRichView).OnRVFControlNeeded) then
      TCustomRichView(FRichView).OnRVFControlNeeded(TCustomRichView(FRichView),
        ItemName, ItemTag, Result);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetControlHint(const Hint: String);
var OldHint: String;
    Ifc: IRVScaleRichViewInterface;
begin
  if (rvflRoot in Flags) then begin
    OldHint := FRichView.Hint;
    FRichView.Hint := Hint;
    Ifc := GetScaleRichViewInterface;
    if Ifc<>nil then
      Ifc.DoOnSetHint(FRichView)
    else begin
      if Hint='' then begin
        {$IFDEF RICHVIEWDEF2009}
        if FRichView.CustomHint<>nil then
          FRichView.CustomHint.HideHint;
        {$ENDIF}
        Application.CancelHint;
        end
      else if (Hint<>OldHint) then begin
        {$IFDEF RICHVIEWDEF5}
        {$IFDEF RICHVIEWDEF2009}
        if FRichView.CustomHint<>nil then
          FRichView.CustomHint.ShowHint(FRichView)
        else
        {$ENDIF}
        begin
          Application.ActivateHint(RVGetCursorPos);
        end;
        {$ENDIF}
      end;
    end;
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.RVFImageListNeeded(ImageListTag: Integer): TCustomImageList;
begin
  Result := nil;
  if Assigned(TCustomRichView(FRichView).OnRVFImageListNeeded) then
    TCustomRichView(FRichView).OnRVFImageListNeeded(TCustomRichView(FRichView),
      ImageListTag, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.RVFPictureNeeded(const ItemName: String;
  Item: TRVNonTextItemInfo; Index1, Index2: Integer;
  PictureInsideRVF: Boolean): TGraphic;
begin
  Result := nil;
  {$IFDEF RVFPICTURENEEDED2}
  if PictureInsideRVF then begin
    if Assigned(TCustomRichView(FRichView).OnRVFPictureNeeded2) then
      TCustomRichView(FRichView).OnRVFPictureNeeded2(TCustomRichView(FRichView),
        ItemName, Item, Index1, Index2, Result)
    end
  else
  {$ENDIF}
    if Assigned(TCustomRichView(FRichView).OnRVFPictureNeeded) then
      TCustomRichView(FRichView).OnRVFPictureNeeded(TCustomRichView(FRichView),
        ItemName, Item, Index1, Index2, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.SaveComponentToFile(const Path: String;
  SaveMe: TComponent; SaveFormat: TRVSaveFormat): String;
begin
  Result := '';
  if Assigned(TCustomRichView(FRichView).OnSaveComponentToFile) then
    TCustomRichView(FRichView).OnSaveComponentToFile(
      TCustomRichView(FRichView), Path, SaveMe, SaveFormat, Result);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.SaveItemToFile(const Path: String; 
  RVData: TCustomRVData; ItemNo: Integer; SaveFormat: TRVSaveFormat;
  Unicode: Boolean; var Text: TRVRawByteString): Boolean;
begin
  if Assigned(TCustomRichView(FRichView).OnSaveItemToFile) then begin
    Result := True;
    TCustomRichView(FRichView).OnSaveItemToFile(TCustomRichView(FRichView),
      Path, RVData, ItemNo, SaveFormat, Unicode, Text, Result);
    Result := not Result;
    end
  else
    Result := False;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.ImportPicture(const Location: String; Width,
  Height: Integer; var Invalid: Boolean): TGraphic;
begin
  Result := nil;
  if Assigned(TCustomRichView(FRichView).OnImportPicture) then begin
    TCustomRichView(FRichView).CurPictureInvalid := False;
    TCustomRichView(FRichView).OnImportPicture(TCustomRichView(FRichView),
      Location, Width, Height, Result);
    Invalid := TCustomRichView(FRichView).CurPictureInvalid;
    TCustomRichView(FRichView).CurPictureInvalid := False;
  end;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetItemHint(RVData: TCustomRVData;
  ItemNo: Integer; const UpperRVDataHint: String): String;
begin
  if ItemNo<0 then begin
    Result := UpperRVDataHint;
    exit;
  end;
  {$IFNDEF RVDONOTUSEITEMHINTS}
  RVData.GetItem(ItemNo).GetExtraStrProperty(rvespHint, Result);
  {$ELSE}
  Result := '';
  {$ENDIF}
  if Assigned(TCustomRichView(FRichView).OnItemHint) then
    TCustomRichView(FRichView).OnItemHint(TCustomRichView(FRichView),
      RVData, ItemNo, Result);
  if Result='' then
    Result := UpperRVDataHint;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedCopy: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnCopy);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoCopy;
begin
  if Assigned(TCustomRichView(FRichView).OnCopy) then
    TCustomRichView(FRichView).OnCopy(TCustomRichView(FRichView));
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoRVMouseMove(id: Integer);
begin
  if Assigned(TCustomRichView(FRichView).OnRVMouseMove) then
    TCustomRichView(FRichView).OnRVMouseMove(TCustomRichView(FRichView),id);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoRVMouseDown(Button: TMouseButton;
  Shift: TShiftState; ItemNo, X, Y: Integer);
begin
  if Assigned(TCustomRichView(FRichView).OnRVMouseDown) then
    TCustomRichView(FRichView).OnRVMouseDown(TCustomRichView(FRichView),Button,Shift,ItemNo,X,Y);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedRVMouseUp: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnRVMouseUp);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedRVMouseDown: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnRVMouseDown);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedRVRightClick: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnRVRightClick);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedJump: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnJump);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedRVDblClick: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnRVDblClick);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedCheckpointVisible: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnCheckpointVisible);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoRVMouseUp(Button: TMouseButton;
  Shift: TShiftState; ItemNo, X, Y: Integer);
begin
  if IsAssignedRVMouseUp then
    TCustomRichView(FRichView).OnRVMouseUp(TCustomRichView(FRichView),Button,Shift,ItemNo,X,Y);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoRVRightClick(const ClickedWord: TRVRawByteString;
  StyleNo, X, Y: Integer);
begin
  if Assigned(TCustomRichView(FRichView).OnRVRightClick) then
    TCustomRichView(FRichView).OnRVRightClick(TCustomRichView(FRichView),
      ClickedWord, StyleNo, X, Y);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoJump(id: Integer);
var data: TRVJumpMessageData;
begin
  data := TRVJumpMessageData.Create;
  data.Event := rvetJump;
  data.id := id;
  PostMessage(GetAbsoluteRootData.GetParentControl.Handle, WM_RVEVENT,
    0, LParam(Data));
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.HTMLSaveImage(RVData: TCustomRVData; ItemNo: Integer;
  const Path: String; BackgroundColor: TColor;
  var Location: String; var DoDefault: Boolean);
begin
  DoDefault := True;
  if Assigned(TCustomRichView(FRichView).OnHTMLSaveImage) then
    TCustomRichView(FRichView).OnHTMLSaveImage(TCustomRichView(FRichView),
      RVData, ItemNo, Path, BackgroundColor, Location, DoDefault);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SaveImage2(RVData: TCustomRVData; ItemNo: Integer;
  Graphic: TGraphic; SaveFormat: TRVSaveFormat;
  const Path, ImagePrefix: String; var ImageSaveNo: Integer;
  var Location: String; var DoDefault: Boolean);
begin
  DoDefault := True;
  if Assigned(TCustomRichView(FRichView).OnSaveImage2) then begin
    GetRVStyle.RVData := RVData;
    GetRVStyle.ItemNo := ItemNo;
    try
      TCustomRichView(FRichView).OnSaveImage2(TCustomRichView(FRichView),
        Graphic, SaveFormat, Path, ImagePrefix, ImageSaveNo, Location, DoDefault);
    finally
      GetRVStyle.RVData := nil;
      GetRVStyle.ItemNo := -1;
    end;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoRVDblClick(const ClickedWord: TRVRawByteString; StyleNo: Integer);
begin
  if Assigned(TCustomRichView(FRichView).OnRVDblClick) then
    TCustomRichView(FRichView).OnRVDblClick(TCustomRichView(FRichView), ClickedWord, StyleNo);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoSelect;
begin
  if rvflRoot in Flags then begin
    if not (csDestroying in FRichView.ComponentState) and FRichView.HandleAllocated then begin
      {$IFDEF RICHVIEWDEF2010}
      //CreateSelectionHandles(False);
      UpdateSelectionHandles(False);
      {$ENDIF}
      if Assigned(TCustomRichView(FRichView).OnSelect) then
        TCustomRichView(FRichView).OnSelect(TCustomRichView(FRichView));
    end;
    end
  else
    inherited DoSelect;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoCheckpointVisible(CheckpointData: TCheckpointData);
begin
  if Assigned(TCustomRichView(FRichView).OnCheckpointVisible) then
    TCustomRichView(FRichView).OnCheckpointVisible(TCustomRichView(FRichView), CheckpointData);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoDrawHyperlink(RVData: TCustomRVData;
  ItemNo: Integer; R: TRect);
begin
  if rvflRoot in Flags then begin
    if Assigned(FOnDrawHyperlink) then
      FOnDrawHyperlink(RVData, ItemNo, R);
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoGetItemCursor(RVData: TCustomRVData; ItemNo: Integer;
  var Cursor: TCursor);
begin
  if rvflRoot in Flags then begin
    if Assigned(TCustomRichView(FRichView).OnGetItemCursor) then
      TCustomRichView(FRichView).OnGetItemCursor(TCustomRichView(FRichView),
        RVData, ItemNo, Cursor);
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESTYLETEMPLATES}
procedure TRichViewRVData.DoStyleTemplatesChange;
begin
  if rvflRoot in Flags then 
    TCustomRichView(FRichView).StyleTemplatesChange
  else
    inherited;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
procedure TRichViewRVData.ControlAction2(RVData: TCustomRVData;
  ControlAction: TRVControlAction; ItemNo: Integer; var Control: TControl);
begin
  if rvflRoot in Flags then begin
    if Assigned(TCustomRichView(FRichView).OnControlAction) then begin
      GetRVStyle.RVData := RVData;
      TCustomRichView(FRichView).OnControlAction(TCustomRichView(FRichView),
        ControlAction, ItemNo, Control);
    end;
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.ItemAction(ItemAction: TRVItemAction;
  Item: TCustomRVItemInfo; var Text: TRVRawByteString; RVData: TCustomRVData);
begin
  if Assigned(TCustomRichView(FRichView).OnItemAction) then
    TCustomRichView(FRichView).OnItemAction(TCustomRichView(FRichView),
      ItemAction, Item, Text, RVData);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoProgress(Operation: TRVLongOperation;
  Stage: TRVProgressStage; PercentDone: Byte);
begin
  if Assigned(TCustomRichView(FRichView).OnProgress) then
    TCustomRichView(FRichView).OnProgress(TCustomRichView(FRichView),
      Operation, Stage, PercentDone);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsAssignedOnProgress: Boolean;
begin
  Result := Assigned(TCustomRichView(FRichView).OnProgress);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetWidth: Integer;
begin
  Result := FRichView.ClientWidth;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetHeight: Integer;
begin
  Result := FRichView.ClientHeight;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetAreaWidth: Integer;
begin
  Result := FRichView.AreaWidth;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetAreaHeight: Integer;
begin
  Result := FRichView.AreaHeight;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetFullDocumentHeight: Integer;
begin
  if rvflRoot in Flags then
    Result := FRichView.AreaHeight*TCustomRichView(FRichView).VSmallStep
  else
    Result := inherited GetFullDocumentHeight;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetFlags: TRVFlags;
begin
  Result := TCustomRichView(FRichView).Flags;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetFlags(const Value: TRVFlags);
begin
  TCustomRichView(FRichView).Flags := Value;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetHOffs: Integer;
begin
  Result := TCustomRichView(FRichView).HScrollPos;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetVOffs: Integer;
begin
  Result := TCustomRichView(FRichView).VScrollPos*TCustomRichView(FRichView).VSmallStep
    -GetRVDataExtraVOffs;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetLeftMargin: Integer;
begin
  Result := TCustomRichView(FRichView).LeftMargin;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRightMargin: Integer;
begin
  Result := TCustomRichView(FRichView).RightMargin;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetBottomMargin: Integer;
begin
  Result := TCustomRichView(FRichView).BottomMargin;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetTopMargin: Integer;
begin
  Result := TCustomRichView(FRichView).TopMargin;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetMaxTextWidth: Integer;
begin
  Result := TCustomRichView(FRichView).MaxTextWidth;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetMinTextWidth: Integer;
begin
  Result := TCustomRichView(FRichView).MinTextWidth;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.AdjustVScrollUnits;
var v: Integer;
begin
  if  TCustomRichView(FRichView).VScrollVisible and
     (DocumentHeight div TCustomRichView(FRichView).VSmallStep > 32000) then begin
    v := DocumentHeight div 32000;
    if DocumentHeight div v > 32000 then
      inc(v);
    TCustomRichView(FRichView).VSmallStep := v;
    end
  {$IFNDEF DONOTDECVSCROLLUNITS}
  else if (TCustomRichView(FRichView).VSmallStep>10) and
          (DocumentHeight div 10 <= 32000) then
    TCustomRichView(FRichView).VSmallStep := 10;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetDocumentAreaSize(Width, Height: Integer;
  UpdateH: Boolean);
var h: Integer;
begin
  //Width := MulDiv(Width,ZoomPercent,100);
  //Height := MulDiv(Height,ZoomPercent,100);
  {$IFDEF RVDEBUG}{$I Debug\c.inc}{$ENDIF}
  h := Height div TCustomRichView(FRichView).VSmallStep;
  if (Height mod TCustomRichView(FRichView).VSmallStep)>0 then
    inc(h);
  FRichView.UpdateScrollBars(Width, h, UpdateH,True);
  {$IFDEF RV_ODHC}
  if Assigned(TCustomRichView(FRichView).OnDocumentHeightChange) then
    TCustomRichView(FRichView).OnDocumentHeightChange(FRichView);
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.ScrollTo(Y: Integer; Redraw: Boolean);
begin
  if rvstNoScroll in State then
    exit;
  if Redraw then
    FRichView.ScrollTo(Y)
  else
    FRichView.ScrollToNoRedraw(Y)
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.HScrollTo(X: Integer);
begin
  if rvstNoScroll in State then
    exit;
  TCustomRichView(FRichView).HScrollPos := X;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetVSmallStep: Integer;
begin
  Result := TCustomRichView(FRichView).VSmallStep;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetBackground: TRVBackground;
begin
  Result := TCustomRichView(FRichView).Background;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetColor: TColor;
begin
  if TCustomRichView(FRichView).Color<>clNone then
    Result := TCustomRichView(FRichView).Color
  else
    Result :=  TCustomRichView(FRichView).Style.Color;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetNormalCursor: TCursor;
begin
  Result := TCustomRichView(FRichView).Cursor
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetDelimiters: String;
begin
  Result := TCustomRichView(FRichView).Delimiters;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetCPEventKind: TCPEventKind;
begin
  Result := TCustomRichView(FRichView).CPEventKind;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetCanvas: TCanvas;
begin
  Result := TCustomRichView(FRichView).Canvas;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVStyle: TRVStyle;
begin
  Result := TCustomRichView(FRichView).Style;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.AssignChosenRVData(RVData: TCustomRVFormattedData;
                                             Item: TCustomRVItemInfo);
begin
  FRichView.AssignChosenRVData(RVData, Item);
  inherited AssignChosenRVData(RVData, Item);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SilentReplaceChosenRVData(
  RVData: TCustomRVFormattedData);
begin
  FRichView.SilentReplaceChosenRVData(RVData);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.UnassignChosenRVData(RVData: TCustomRVData);
begin
  if rvstUnAssigningChosen in State then
    exit;
  State := State+[rvstUnAssigningChosen];
  try
    inherited UnassignChosenRVData(RVData);
    FRichView.UnassignChosenRVData(RVData);
  finally
    State := State-[rvstUnAssigningChosen]
  end;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetInplaceEditor: TControl;
begin
  Result := TCustomRichView(FRichView).InplaceEditor;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetChosenRVData: TCustomRVData;
begin
  Result := TCustomRichView(FRichView).ChosenRVData as TCustomRVData;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetChosenItem: TCustomRVItemInfo;
begin
  Result := TCustomRichView(FRichView).ChosenItem as TCustomRVItemInfo;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DestroyInplaceEditor;
begin
  FRichView.DestroyInplace;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRTFProperties: TPersistent;
begin
  Result := TCustomRichView(FRichView).RTFReadProperties
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDOCPARAMS}
function TRichViewRVData.GetDocParameters(AllowCreate: Boolean): TRVDocParameters;
begin
  if not AllowCreate and not TCustomRichView(FRichView).DocParametersAssigned then
    Result := nil
  else
    Result := TCustomRichView(FRichView).DocParameters;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVFParaStylesReadMode: TRVFReaderStyleMode;
begin
  Result := TCustomRichView(FRichView).RVFParaStylesReadMode;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVFTextStylesReadMode: TRVFReaderStyleMode;
begin
  Result := TCustomRichView(FRichView).RVFTextStylesReadMode;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetBiDiMode: TRVBiDiMode;
begin
  Result := TCustomRichView(FRichView).BiDiMode;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.InitStyleMappings(var PTextStylesMapping,
  PParaStylesMapping, PListStylesMapping: PRVIntegerList;
  var PUnits: PRVStyleUnits);
begin
  if FRVFGlobalReadData=nil then
    FRVFGlobalReadData := TRVFGlobalReadData.Create;
  PTextStylesMapping := @(FRVFGlobalReadData.RVFTextStylesMapping);
  PParaStylesMapping := @(FRVFGlobalReadData.RVFParaStylesMapping);
  PListStylesMapping := @(FRVFGlobalReadData.RVFListStylesMapping);
  PUnits := @(FRVFGlobalReadData.Units);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoneStyleMappings(AsSubDoc: Boolean);
begin
  RVFreeAndNil(FRVFGlobalReadData);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESEQ}
function TRichViewRVData.GetSeqList(AllowCreate: Boolean): TRVSeqList;
begin
  if rvflRoot in Flags then begin
    if (FSeqList=nil) and AllowCreate then begin
      FSeqList := TRVSeqList.Create;
    end;
    Result := FSeqList;
    end
  else
    Result := inherited GetSeqList(AllowCreate);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetNoteText: String;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(FRichView).NoteText
  else
    Result := inherited GetNoteText;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DestroySeqList;
begin
  if rvflRoot in Flags then
    RVFreeAndNil(FSeqList)
  else
    inherited DestroySeqList;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSELISTS}
function TRichViewRVData.GetMarkers(AllowCreate: Boolean): TRVMarkerList;
begin
  if rvflRoot in Flags then begin
    if (FMarkers=nil) and AllowCreate then begin
      FMarkers := TRVMarkerList.Create;
      FMarkers.PrevMarkerList := GetPrevMarkers;
    end;
    Result := FMarkers;
    end
  else
    Result := inherited GetMarkers(AllowCreate);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DestroyMarkers;
begin
  if rvflRoot in Flags then
    RVFreeAndNil(FMarkers)
  else
    inherited DestroyMarkers;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRichViewRVData.GetDocProperties: TStringList;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(RichView).DocProperties
  else
    Result := nil;
end;
{------------------------------------------------------------------------------}
{$IFDEF RICHVIEWDEF2010}
function TRichViewRVData.GetSelectionHandles: TRVSelectionHandles;
begin
  if rvflRoot in Flags then
    Result := FSelectionHandles
  else
    Result := TCustomRVFormattedData(GetAbsoluteRootData).GetSelectionHandles;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.CreateSelectionHandles(EvenForEmptySelection: Boolean);
begin
  if not (rvflRoot in Flags) or (FSelectionHandles<>nil) then
    exit;
  if EvenForEmptySelection or
    (GetTopLevelChosenRVData.GetRVData as TCustomRVFormattedData).SelectionExists(False, False) then begin
    FSelectionHandles := TRVSelectionHandles.Create;
  end;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.CreateSelectionHandlesAfterTap;
begin
  if rvflRoot in Flags then begin
    if not (rvstTouchMouseDown in State) then
      exit;
    CreateSelectionHandles(False);
    UpdateSelectionHandles(False);
    end
  else
    inherited CreateSelectionHandlesAfterTap;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DestroySelectionHandles;
begin
  RVFreeAndNil(FSelectionHandles);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.UpdateSelectionHandles(EvenForEmptySelection: Boolean);
var RVData: TCustomRVFormattedData;
begin
  if not (rvflRoot in Flags) or (FSelectionHandles=nil) then
    exit;
  RVData := GetTopLevelChosenRVData.GetRVData as TCustomRVFormattedData;
  if not RVData.SelectionExists(False, False) and not EvenForEmptySelection and
   not FSelectionHandles.Dragging then begin
    DestroySelectionHandles;
    exit;
  end;
  FSelectionHandles.RVData := RVData;
  if RVData.GetSelectionBoundsCoords(
    FSelectionHandles.X1, FSelectionHandles.Top1, FSelectionHandles.Bottom1,
    FSelectionHandles.X2, FSelectionHandles.Top2, FSelectionHandles.Bottom2) then
    FSelectionHandles.CalculatePositions
  else
    DestroySelectionHandles;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEDRAGDROP}
{------------------------------------------------------------------------------}
{ Drag&Drop: IDropTarget related                                               }
{------------------------------------------------------------------------------}
{ Returns information about drag&drop caret location.
  D&d caret is not allowed in TRichView.
  This method is overriden in TRVEditRVData.                                   }
function TRichViewRVData.GetDragDropCaretInfo: TRVDragDropCaretInfo;
begin
  Result := nil;
end;
{------------------------------------------------------------------------------}
{ Drag&Drop: IDropSource related                                               }
{------------------------------------------------------------------------------}
{ Can dragging from this TRichView be started?                                 } 
function TRichViewRVData.CanStartDragging: Boolean;
begin
  if not (rvflRoot in Flags) then begin
    Result := inherited CanStartDragging;
    exit;
  end;
  if rvoDisallowDrag in GetOptions then
    Result := False
  else
    Result := FDropSource=nil;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetClickCoords(X, Y: Integer);
begin
  if rvflRoot in Flags then begin
    FClickX := X;
    FClickY := Y;
    end
  else
    inherited SetClickCoords(X, Y);
end;
{------------------------------------------------------------------------------}
{ Is cursor moved far enough from the click point to start dragging? }
function TRichViewRVData.CanStartDragBecauseMouseMoved(X, Y: Integer): Boolean;
var DX, DY: Integer;
begin
  if rvflRoot in Flags then begin
    DX := (GetSystemMetrics(SM_CXDRAG)+1) div 2;
    DY := (GetSystemMetrics(SM_CYDRAG)+1) div 2;
    Result := (DX<=Abs(X-FClickX)) or (DY<=Abs(Y-FClickY))
    end
  else
    Result := inherited CanStartDragBecauseMouseMoved(X, Y);
end;
{------------------------------------------------------------------------------}
{ Is dragging from this TRichView in process? Overriden in TRichViewRVData.    }
function TRichViewRVData.IsDragging: Boolean;
begin
  if not (rvflRoot in Flags) then
    Result := inherited IsDragging
  else
    Result := FDropSource<>nil;
end;
{------------------------------------------------------------------------------}
{ Initializing dragging. Overriden in TRVEditRVData.
  Returns True on success.
  Returns DropSource and OKEffect for call of DoDragDrop.                      }
function TRichViewRVData.InitDragging(var DropSource: TRVDropSource;
  var OKEffect: Integer): Boolean;
begin
  if not (rvflRoot in Flags) then begin
    Result := inherited InitDragging(DropSource, OKEffect);
    exit;
  end;
  if rvoDisallowDrag in GetOptions then
    Result := False
  else begin
    Result := FDropSource=nil;
    if Result then begin
      FDropSource := TCustomRichView(FRichView).GetRVDropSourceClass.Create(FRichView);
      OKEffect := DROPEFFECT_COPY;
    end;
    DropSource := FDropSource;
  end;
end;
{------------------------------------------------------------------------------}
{ Finalizing dragging.
  Note: if FDropSource<>nil, this function loses the object.                   }
procedure TRichViewRVData.DoneDragging(FDeleteSelection: Boolean);
begin
  FDropSource := nil;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRichViewRVData.GetRVDataExtraVOffs: Integer;
begin
  {$IFDEF RICHVIEWDEF3}
  case TCustomRichView(RichView).VAlign of
    tlBottom:
      begin
        Result := GetHeight-DocumentHeight;
        if Result<0 then
          Result := 0;
      end;
    tlCenter:
      begin
        Result := (GetHeight-DocumentHeight) div 2;
        if Result<0 then
          Result := 0;
      end;
    else
      Result := 0;
  end;
  {$ELSE}
  Result := 0;
  {$ENDIF}
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.GetOrigin(var ALeft, ATop: Integer);
begin
  ALeft := 0;
  ATop := 0;//GetRVDataExtraVOffs;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.GetOriginEx(var ALeft, ATop: Integer);
begin
  ALeft := 0;
  ATop := 0;//GetRVDataExtraVOffs;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.Clear;
var Ifc: IRVScaleRichViewInterface;
begin
  {$IFNDEF RVDONOTUSELIVESPELL}
  if RichView<>nil then
    TCustomRichView(RichView).ClearLiveSpellingResults;
  {$ENDIF}
  {$IFNDEF RVDONOTUSEANIMATION}
  if TCustomRichView(RichView).AnimationMode=rvaniOnFormat then
    TCustomRichView(RichView).StopAnimation;
  {$ENDIF}
  {$IFNDEF RVDONOTUSESMARTPOPUP}
  RVFreeAndNil(FSmartPopupButton);
  {$ENDIF}
  if (rvflRoot in Flags) and (FThumbnailCache<>nil) then
    FThumbnailCache.Clear;
  inherited;
  if rvflRoot in Flags then begin
    Ifc := GetScaleRichViewInterface;
    if Ifc<>nil then
      Ifc.DoOnClear(Self);
    TCustomRichView(RichView).DoOnClear;
  end;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSEANIMATION}
procedure TRichViewRVData.InsertAnimator(var Animator: TObject);
begin
  if rvflRoot in Flags then begin
    if FAnimatorList=nil then
      FAnimatorList := TRVAnimatorList.Create;
    FAnimatorList.Add(TRVAnimator(Animator));
    if FPlayingAnimation then
      TCustomRichView(RichView).StartAnimation;
    end
  else
    inherited InsertAnimator(Animator);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.AllowAnimation: Boolean;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(RichView).AnimationMode<>rvaniDisabled
  else
    Result := inherited AllowAnimation;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.ResetAniBackground;
begin
  if rvflRoot in Flags then begin
    if FAnimatorList<>nil then
      FAnimatorList.ResetBackground;
    end
  else
    inherited ResetAniBackground;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRichViewRVData.DoCreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
  Owner: TCustomRVFormattedData): TRVFontInfoCache;
begin
  if (rvoFastFormatting in Options) and RVNT then
    Result := TRVFontInfoCacheFast.Create(Owner, GetRVStyle, AFormatCanvas, ACanvas,
      rvflCanUseCustomPPI in Flags)
  else
    Result := TRVFontInfoCacheLowResource.Create(Owner, GetRVStyle, AFormatCanvas, ACanvas,
      rvflCanUseCustomPPI in Flags);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.CreateFontInfoCache(ACanvas, AFormatCanvas: TCanvas);
begin
//  if rvflRoot in Flags then begin
    FFontInfoCache.Free;
    FFontInfoCache := DoCreateFontInfoCache(ACanvas, AFormatCanvas, Self);
{    end
  else
    TCustomRVFormattedData(GetAbsoluteRootData).CreateFontInfoCache(ACanvas, AFormatCanvas);
}
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DestroyFontInfoCache(var Cache: TRVFontInfoCache);
begin
  if Cache=FFontInfoCache then
    FFontInfoCache := nil;
  inherited DestroyFontInfoCache(Cache);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetFontInfoCache(ACanvas, AFormatCanvas: TCanvas;
  RVData: TCustomRVFormattedData): TRVFontInfoCache;
begin
  Result := FFontInfoCache;
  if Result = nil then
    Result := DoCreateFontInfoCache(ACanvas, AFormatCanvas, RVData);
end;
{------------------------------------------------------------------------------}
{$IFNDEF RVDONOTUSESMARTPOPUP}
procedure TRichViewRVData.AdjustSpecialControlsCoords(RVData: TCustomRVFormattedData);
begin
  if (FSmartPopupButton=nil) or (FSmartPopupButton.RVData<>RVData.GetSourceRVData) then
    exit;
  RVData := TCustomRVFormattedData(RVData.GetRVData);
  if (FSmartPopupButton.ItemNo<0) or (FSmartPopupButton.ItemNo>=RVData.ItemCount) or
    (RVData.GetItem(FSmartPopupButton.ItemNo)<>FSmartPopupButton.Item) then
    FSmartPopupButton.ItemNo := RVData.GetItemNo(FSmartPopupButton.Item);
  SetSmartPopupCoords;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetSmartPopupCoords;
var DItemNo: Integer;
    X, Y, Width, Height: Integer;
    RVData: TCustomRVFormattedData;
    R: TRect;
    Pt: TPoint;
    Ifc: IRVScaleRichViewInterface;
    Zoom: Single;
begin
  if (FSmartPopupButton=nil) then
    exit;
  RVData := TCustomRVFormattedData(FSmartPopupButton.RVData.GetRVData);
  if rvstFormattingPart in RVData.GetAbsoluteRootData.State then
    exit;
  RVData.Item2LastDrawItem(FSmartPopupButton.ItemNo, DItemNo);
  if DItemNo<0 then begin
    RVFreeAndNil(FSmartPopupButton);
    exit;
  end;
  Ifc := GetScaleRichViewInterface;
  with RVData.DrawItems[DItemNo] do
    R := Bounds(Left, Top, Width, Height);
  Width := FSmartPopupButton.Width;
  Height := FSmartPopupButton.Height;
  if (Ifc<>nil) then begin
    Zoom := Ifc.GetCurrentPageZoomPercent/100;
    if Zoom<1.0 then
      Zoom := 1.0;
    Width := Round(Width/Zoom);
    Height := ROund(Height/Zoom);
  end;
  RVData.ResetSubCoords;
  RVData.RotateRectFromDocToScreen(R, rvtrbAbsRoot);
  case FSmartPopupButton.SmartPopupProperties.Position of
    rvsppTopLeft:
      begin
        X := R.Left - Width;
        Y := R.Top  - Height;
      end;
    rvsppTopRight:
      begin
        X := R.Right;
        Y := R.Top - Height;
      end;
    rvsppBottomRight:
      begin
        X := R.Right;
        Y := R.Bottom;
      end;
    else//rvsppBottomLeft:
      begin
        X := R.Left - Width;
        Y := R.Bottom;
      end;
  end;
  Width := GetAreaWidth;
  if GetWidth>Width then
    Width := GetWidth;
  if X+FSmartPopupButton.Width>Width then
    X := Width-FSmartPopupButton.Width;
  if X<0 then
    X := 0;
  Height := GetAreaHeight*TCustomRichView(FRichView).VSmallStep;
  if GetHeight>Height then
    Height := GetHeight;
  if Y+FSmartPopupButton.Height>Height then
    Y := Height-FSmartPopupButton.Height;
  if Y<0 then
    Y := 0;
  Pt :=  TCustomRichView(TRichViewRVData(RVData.GetAbsoluteRootData).RichView).DocumentToClient(Point(X,Y));
  if Ifc<>nil then begin
    Ifc.ConvertRVToSRV(Pt);
    if FSmartPopupButton.Parent<>nil then
      FSmartPopupButton.Parent.DisableAlign;
    try
      FSmartPopupButton.Left := Pt.X;
      FSmartPopupButton.Top := Pt.Y;
    finally
      if FSmartPopupButton.Parent<>nil then begin
        FSmartPopupButton.Parent.ControlState := FSmartPopupButton.Parent.ControlState - [csAlignmentNeeded];
        FSmartPopupButton.Parent.EnableAlign;
      end;
    end;
    end
  else begin
    FSmartPopupButton.Left := Pt.X;
    FSmartPopupButton.Tag  := Pt.Y;
    RV_Tag2Y(FSmartPopupButton);
  end;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.RepaintSmartPopup;
begin
  if (FSmartPopupButton<>nil) then
    FSmartPopupButton.Repaint;
end;
{------------------------------------------------------------------------------}
{
procedure TRichViewRVData.InternalFreeItem(item: TCustomRVItemInfo;
  Clearing: Boolean);
begin
  inherited;
  if not Clearing and (FSmartPopupButton<>nil) and
    (FSmartPopupButton.Item=item) then begin
    FSmartPopupButton.Free;
    FSmartPopupButton := nil;
  end;
end;
}
{$ENDIF}
{------------------------------------------------------------------------------}
function TRichViewRVData.GetMaxLength: Integer;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(FRichView).MaxLength
  else
    Result := inherited GetMaxLength;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.IsWordWrapAllowed: Boolean;
begin
  if rvflRoot in Flags then
    Result := TCustomRichView(FRichView).WordWrap
  else
    Result := inherited IsWordWrapAllowed;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetFormatCanvasEx(DefCanvas: TCanvas): TCanvas;
begin
  Result := TCustomRichView(FRichView).GetFormatCanvas(DefCanvas);
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetExtraDocuments: TStrings;
begin
  Result := FExtraDocuments;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetExtraDocumentsList: TStrings;
begin
  if FExtraDocuments=nil then
    FExtraDocuments := TStringList.Create;
  Result := FExtraDocuments;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.SetExtraDocumentsList(const Value: TStrings);
begin
  if Value=nil then
    ExtraDocumentsList.Clear
  else if Value<>ExtraDocumentsList then
    ExtraDocumentsList.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.AfterAddStyle(StyleInfo: TCustomRVInfo);
begin
  if rvflRoot in Flags then begin
    if Assigned(TCustomRichView(FRichView).OnAddStyle) then
      TCustomRichView(FRichView).OnAddStyle(TCustomRichView(FRichView), StyleInfo);
    end
  else
    inherited;
end;
{------------------------------------------------------------------------------}
function TRichViewRVData.GetScaleRichViewInterface: IRVScaleRichViewInterface;
begin
  if GetAbsoluteParentData=nil then begin
    if (FRichView<>nil) and
      Assigned(TCustomRichView(FRichView).OnGetScaleRichViewInterface) then
      TCustomRichView(FRichView).OnGetScaleRichViewInterface(Result)
    else
      Result := nil;
    end
  else
    Result := inherited GetScaleRichViewInterface;
end;
{------------------------------------------------------------------------------}
procedure TRichViewRVData.DoBeforeSaving;
begin
  inherited DoBeforeSaving;
  if (rvflRoot in Flags) then
    TCustomRichView(FRichView).DoBeforeSaving;
end;

end.

