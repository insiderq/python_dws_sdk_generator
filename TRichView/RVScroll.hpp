﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVScroll.pas' rev: 27.00 (Windows)

#ifndef RvscrollHPP
#define RvscrollHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.ImgList.hpp>	// Pascal unit
#include <Winapi.ActiveX.hpp>	// Pascal unit
#include <Winapi.CommCtrl.hpp>	// Pascal unit
#include <RVXPTheme.hpp>	// Pascal unit
#include <RVStr.hpp>	// Pascal unit
#include <RVTypes.hpp>	// Pascal unit
#include <System.Types.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvscroll
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRVBiDiMode : unsigned char { rvbdUnspecified, rvbdLeftToRight, rvbdRightToLeft };

enum DECLSPEC_DENUM TRVOption : unsigned char { rvoAllowSelection, rvoSingleClick, rvoScrollToEnd, rvoClientTextWidth, rvoShowCheckpoints, rvoShowPageBreaks, rvoShowSpecialCharacters, rvoShowHiddenText, rvoTagsArePChars, rvoAutoCopyText, rvoAutoCopyUnicodeText, rvoAutoCopyRVF, rvoAutoCopyImage, rvoAutoCopyRTF, rvoFormatInvalidate, rvoDblClickSelectsWord, rvoRClickDeselects, rvoDisallowDrag, rvoShowItemHints, rvoShowGridLines, rvoFastFormatting, rvoPercentEncodedURL };

typedef System::Set<TRVOption, TRVOption::rvoAllowSelection, TRVOption::rvoPercentEncodedURL> TRVOptions;

enum DECLSPEC_DENUM TRVTabNavigationType : unsigned char { rvtnNone, rvtnTab, rvtnCtrlTab };

enum DECLSPEC_DENUM TRVPaletteAction : unsigned char { rvpaDoNothing, rvpaAssignPalette, rvpaCreateCopies, rvpaCreateCopiesEx };

enum DECLSPEC_DENUM TBackgroundStyle : unsigned char { bsNoBitmap, bsStretched, bsTiled, bsTiledAndScrolled, bsCentered, bsTopLeft, bsTopRight, bsBottomLeft, bsBottomRight };

enum DECLSPEC_DENUM TRVZoomMode : unsigned char { rvzmFullPage, rvzmPageWidth, rvzmCustom };

enum DECLSPEC_DENUM TRVDisplayOption : unsigned char { rvdoImages, rvdoComponents, rvdoBullets };

typedef System::Set<TRVDisplayOption, TRVDisplayOption::rvdoImages, TRVDisplayOption::rvdoBullets> TRVDisplayOptions;

enum DECLSPEC_DENUM TRVSearchOption : unsigned char { rvsroMatchCase, rvsroDown, rvsroWholeWord, rvsroFromStart, rvsroMultiItem, rvsroSmartStart };

typedef System::Set<TRVSearchOption, TRVSearchOption::rvsroMatchCase, TRVSearchOption::rvsroSmartStart> TRVSearchOptions;

enum DECLSPEC_DENUM TCPEventKind : unsigned char { cpeNone, cpeAsSectionStart, cpeWhenVisible };

enum DECLSPEC_DENUM TRVScrollBarStyle : unsigned char { rvssRegular, rvssFlat, rvssHotTrack };

enum DECLSPEC_DENUM TRVRTFHighlight : unsigned char { rtfhlIgnore, rtfhlFixedColors, rtfhlColorTable };

enum DECLSPEC_DENUM TRVSmartPopupType : unsigned char { rvsptDropDown, rvsptShowDialog, rvsptSimple };

enum DECLSPEC_DENUM TRVSmartPopupPosition : unsigned char { rvsppTopLeft, rvsppTopRight, rvsppBottomRight, rvsppBottomLeft };

class DELPHICLASS TRVScrollerInternalIfcObject;
class DELPHICLASS TRVScroller;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TRVScrollerInternalIfcObject : public System::TInterfacedObject
{
	typedef System::TInterfacedObject inherited;
	
private:
	HRESULT InitRes;
	
protected:
	TRVScroller* FOwner;
	DYNAMIC bool __fastcall OwnerDragEnter(int X, int Y);
	void __fastcall CallOwnerDragEnterEvent(const _di_IDataObject DataObj, int KeyState, const System::Types::TPoint &pt, int PossibleEffects, int &Effect);
	DYNAMIC void __fastcall OwnerDragLeave(void);
	DYNAMIC bool __fastcall OwnerDragOver(int X, int Y);
	void __fastcall CallOwnerDragOverEvent(int KeyState, const System::Types::TPoint &pt, int PossibleEffects, int &Effect);
	virtual void __fastcall OwnerReleaseDropTargetObject(void);
	DYNAMIC int __fastcall OwnerDrop(const _di_IDataObject DataObj, bool FMove, int KeyState, const System::Types::TPoint &pt, int PossibleEffects);
	bool __fastcall OwnerCanAcceptFormat(System::Word Format);
	
public:
	__fastcall virtual TRVScrollerInternalIfcObject(TRVScroller* AOwner);
	__fastcall virtual ~TRVScrollerInternalIfcObject(void);
};

#pragma pack(pop)

class DELPHICLASS TCustomRVControl;
class PASCALIMPLEMENTATION TCustomRVControl : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
public:
	DYNAMIC TCustomRVControl* __fastcall SRVGetActiveEditor(bool MainDoc);
	DYNAMIC void __fastcall RegisterSRVChangeActiveEditorHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	DYNAMIC void __fastcall UnregisterSRVChangeActiveEditorHandler(System::Classes::TComponent* Component);
	DYNAMIC bool __fastcall HasOwnerItemInMainDoc(void);
	DYNAMIC TCustomRVControl* __fastcall GetOwnerItemInMainDocEditor(void);
	DYNAMIC void __fastcall RegisterStyleTemplatesChangeHandler(System::Classes::TComponent* Component, System::Classes::TNotifyEvent Event);
	DYNAMIC void __fastcall UnregisterStyleTemplatesChangeHandler(System::Classes::TComponent* Component);
public:
	/* TCustomControl.Create */ inline __fastcall virtual TCustomRVControl(System::Classes::TComponent* AOwner) : Vcl::Controls::TCustomControl(AOwner) { }
	/* TCustomControl.Destroy */ inline __fastcall virtual ~TCustomRVControl(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomRVControl(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TRVScroller : public TCustomRVControl
{
	typedef TCustomRVControl inherited;
	
private:
	Vcl::Forms::TFormBorderStyle FBorderStyle;
	int FSmallStep;
	bool FTracking;
	bool FFullRedraw;
	bool FVScrollVisible;
	bool FHScrollVisible;
	bool FUpdatingScrollBars;
	int FVScrollMax;
	int FVScrollPage;
	int FHScrollMax;
	int FHScrollPage;
	TRVPaletteAction FDoInPaletteMode;
	bool FUseXPThemes;
	bool FNoVScroll;
	bool FNoHScroll;
	bool FSilentFocusing;
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Winapi::Messages::TWMSetFocus &Message);
	HIDESBASE MESSAGE void __fastcall WMHScroll(Winapi::Messages::TWMScroll &Message);
	HIDESBASE MESSAGE void __fastcall WMVScroll(Winapi::Messages::TWMScroll &Message);
	MESSAGE void __fastcall WMGetDlgCode(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall CMCtl3DChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMQueryNewPalette(Winapi::Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall WMPaletteChanged(Winapi::Messages::TWMPaletteChanged &Message);
	MESSAGE void __fastcall WMThemeChanged(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMNCPaint(Winapi::Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Winapi::Messages::TMessage &Message);
	int __fastcall GetVScrollMax(void);
	int __fastcall GetHScrollMax(void);
	void __fastcall SetVScrollVisible(bool vis);
	void __fastcall SetHScrollVisible(bool vis);
	void __fastcall SetBorderStyle(const Vcl::Forms::TBorderStyle Value);
	void __fastcall SetDoInPaletteMode(TRVPaletteAction Value);
	void __fastcall SetVScrollPos(int Value);
	Vcl::Controls::TWinControl* __fastcall GetInplaceEditor(void);
	System::Classes::TPersistent* __fastcall GetChosenRVData(void);
	virtual void __fastcall CreateThemeHandle(void);
	virtual void __fastcall FreeThemeHandle(void);
	void __fastcall SetUseXPThemes(const bool Value);
	
protected:
	TRVBiDiMode FBiDiMode;
	System::Classes::TNotifyEvent FOnVScrolled;
	System::Classes::TNotifyEvent FOnHScrolled;
	System::ByteBool FVDisableNoScroll;
	int HPos;
	int VPos;
	int XSize;
	int YSize;
	bool KeyboardScroll;
	System::Classes::TPersistent* FChosenItem;
	System::Classes::TPersistent* FChosenRVData;
	unsigned FTheme;
	int FScrollFactor;
	System::Types::TPoint FPanPoint;
	int FPanVPos;
	int FPanHPos;
	int FWheelStep;
	DYNAMIC bool __fastcall DoMouseWheelDown(System::Classes::TShiftState Shift, const System::Types::TPoint &MousePos);
	DYNAMIC bool __fastcall DoMouseWheelUp(System::Classes::TShiftState Shift, const System::Types::TPoint &MousePos);
	virtual void __fastcall SetBiDiModeRV(const TRVBiDiMode Value);
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	virtual void __fastcall CreateWnd(void);
	DYNAMIC void __fastcall AfterCreateWnd1(void);
	DYNAMIC void __fastcall AfterCreateWnd2(void);
	DYNAMIC HPALETTE __fastcall GetPalette(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	virtual void __fastcall SetVPos(int p, bool Redraw);
	virtual void __fastcall SetHPos(int p);
	virtual void __fastcall SetEnabled(bool Value);
	void __fastcall ScrollChildren(int dx, int dy);
	virtual void __fastcall AfterVScroll(void);
	virtual void __fastcall AfterHScroll(void);
	virtual void __fastcall BeforeScroll(void);
	virtual void __fastcall DoAfterResize(void);
	DYNAMIC int __fastcall GetDefSmallStep(void);
	Winapi::Windows::PLogPalette __fastcall AllocLogPalette(int ColorCount);
	void __fastcall FreeLogPalette(Winapi::Windows::PLogPalette &lpLogPal);
	DYNAMIC Winapi::Windows::PLogPalette __fastcall GenerateLogPalette(void);
	DYNAMIC void __fastcall UpdatePaletteInfo(void);
	virtual void __fastcall SetVSmallStep(int Value);
	DYNAMIC bool __fastcall OleDragEnter(int X, int Y);
	DYNAMIC void __fastcall CallOleDragEnterEvent(const _di_IDataObject DataObj, int KeyState, const System::Types::TPoint &pt, int PossibleEffects, int &Effect);
	DYNAMIC void __fastcall OleDragLeave(void);
	DYNAMIC bool __fastcall OleDragOver(int X, int Y);
	DYNAMIC void __fastcall CallOleDragOverEvent(int KeyState, const System::Types::TPoint &pt, int PossibleEffects, int &Effect);
	DYNAMIC void __fastcall ReleaseOleDropTargetObject(void);
	DYNAMIC int __fastcall OleDrop(const _di_IDataObject DataObj, bool FMove, int KeyState, const System::Types::TPoint &pt, int PossibleEffects);
	DYNAMIC bool __fastcall OleCanAcceptFormat(System::Word Format);
	virtual void __fastcall DoGesture(const Vcl::Controls::TGestureEventInfo &EventInfo, bool &Handled);
	virtual void __fastcall DoGetGestureOptions(Vcl::Controls::TInteractiveGestures &Gestures, Vcl::Controls::TInteractiveGestureOptions &Options);
	DYNAMIC bool __fastcall IsTouchPropertyStored(Vcl::Controls::TTouchProperty AProperty);
	__property bool Tracking = {read=FTracking, write=FTracking, default=1};
	__property System::Classes::TNotifyEvent OnVScrolled = {read=FOnVScrolled, write=FOnVScrolled};
	__property System::Classes::TNotifyEvent OnHScrolled = {read=FOnHScrolled, write=FOnHScrolled};
	__property TRVPaletteAction DoInPaletteMode = {read=FDoInPaletteMode, write=SetDoInPaletteMode, nodefault};
	__property int VSmallStep = {read=FSmallStep, write=SetVSmallStep, nodefault};
	__property Vcl::Controls::TWinControl* InplaceEditor = {read=GetInplaceEditor};
	__property Vcl::Forms::TBorderStyle BorderStyle = {read=FBorderStyle, write=SetBorderStyle, nodefault};
	__property int WheelStep = {read=FWheelStep, write=FWheelStep, default=2};
	__property bool FullRedraw = {read=FFullRedraw, write=FFullRedraw, nodefault};
	__property bool VScrollVisible = {read=FVScrollVisible, write=SetVScrollVisible, default=1};
	__property bool HScrollVisible = {read=FHScrollVisible, write=SetHScrollVisible, default=1};
	__property int VScrollPos = {read=VPos, write=SetVScrollPos, nodefault};
	__property int HScrollPos = {read=HPos, write=SetHPos, nodefault};
	__property int VScrollMax = {read=GetVScrollMax, nodefault};
	__property int HScrollMax = {read=GetHScrollMax, nodefault};
	
public:
	HPALETTE RVPalette;
	tagLOGPALETTE *PRVLogPalette;
	__fastcall virtual TRVScroller(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TRVScroller(void);
	void __fastcall UpdateScrollBars(int XS, int YS, bool UpdateH, bool UseDNS);
	void __fastcall ScrollToNoRedraw(int y);
	void __fastcall ScrollTo(int y);
	void __fastcall AssignChosenRVData(System::Classes::TPersistent* RVData, System::Classes::TPersistent* Item);
	void __fastcall SilentReplaceChosenRVData(System::Classes::TPersistent* RVData);
	void __fastcall UnassignChosenRVData(System::Classes::TPersistent* RVData);
	void __fastcall DestroyInplace(void);
	bool __fastcall FocusedEx(void);
	bool __fastcall EnabledEx(void);
	void __fastcall SetFocusSilent(void);
	void __fastcall PaintTo_(HDC DC, int X, int Y);
	__property int AreaWidth = {read=XSize, nodefault};
	__property int AreaHeight = {read=YSize, nodefault};
	__property System::Classes::TPersistent* ChosenRVData = {read=GetChosenRVData};
	__property System::Classes::TPersistent* ChosenItem = {read=FChosenItem};
	__property TRVBiDiMode BiDiMode = {read=FBiDiMode, write=SetBiDiModeRV, default=0};
	__property bool UseXPThemes = {read=FUseXPThemes, write=SetUseXPThemes, default=1};
	__property Canvas;
	__property bool NoVScroll = {read=FNoVScroll, write=FNoVScroll, default=0};
	__property bool NoHScroll = {read=FNoHScroll, write=FNoHScroll, default=0};
public:
	/* TWinControl.CreateParented */ inline __fastcall TRVScroller(HWND ParentWindow) : TCustomRVControl(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
#define rvdoALL (System::Set<TRVDisplayOption, TRVDisplayOption::rvdoImages, TRVDisplayOption::rvdoBullets>() << TRVDisplayOption::rvdoImages << TRVDisplayOption::rvdoComponents << TRVDisplayOption::rvdoBullets )
extern DELPHI_PACKAGE int __fastcall RV_GetYByTag(Vcl::Controls::TControl* AControl);
extern DELPHI_PACKAGE void __fastcall RV_Tag2Y(Vcl::Controls::TControl* AControl);
}	/* namespace Rvscroll */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVSCROLL)
using namespace Rvscroll;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvscrollHPP
