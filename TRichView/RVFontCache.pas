{*******************************************************}
{                                                       }
{       TRichView                                       }
{                                                       }
{       Classes for caching font data for               }
{       TRVStyle.TextStyles                             }
{                                                       }
{       Copyright (c) Sergey Tkachenko                  }
{       svt@trichview.com                               }
{       http://www.trichview.com                        }
{                                                       }
{*******************************************************}

unit RVFontCache;

interface

{$I RV_Defs.inc}
uses Windows, Graphics, Classes, RVScroll, RVStyle, RVClasses;

type
  TRVFontInfoCacheItem = class
  public
    LastBiDiMode: TRVBiDiMode;
    Canvas: TCanvas;
    TextMetric: TTextMetric;
    ExtraFontInfo: TRVExtraFontInfo;
    FontInfo: TFontInfo;
    VerticalOffset: Integer;
    EmptyLineHeight: Integer;
    HyphenWidth: Integer;
    Rotation: TRVDocRotation;
    {$IFDEF RVHIGHFONTS}
    DrawOffsetY: Integer;
    {$ENDIF}
  end;
  PRVFontInfoCacheItem = ^TRVFontInfoCacheItem;

  TRVFontInfoCache = class (TRVList)
  protected
    FCanvas, FDrawCanvas: TCanvas;
    FRVStyle: TRVStyle;
    FCanUseCustomPPI: Boolean;
    FOwner: TObject;
    FInvalidItem: TRVFontInfoCacheItem;
    function GetItems(Index: Integer): TRVFontInfoCacheItem; virtual; abstract;
  public
    CurParaBiDiMode: TRVBiDiMode;
    IgnoreParaBiDiMode: Boolean;
    LastTextStyle: Integer;
    Rotation: TRVDocRotation;
    constructor Create(const AData: TObject;
      const ARVStyle: TRVStyle; const ACanvas, ADrawCanvas: TCanvas;
      ACanUseCustomPPI: Boolean); virtual;
    procedure Clear; {$IFDEF RICHVIEWDEF4}override;{$ENDIF}
    property Items[Index: Integer]: TRVFontInfoCacheItem read GetItems; default;
    property Owner: TObject read FOwner;
  end;

  TRVFontInfoCacheFast = class(TRVFontInfoCache)
  protected
    function GetItems(Index: Integer): TRVFontInfoCacheItem; override;
  public
    procedure Clear; {$IFDEF RICHVIEWDEF4}override;{$ENDIF}
    {$IFNDEF RICHVIEWDEF4}
    destructor Destroy; override;
    {$ENDIF}
  end;

  TRVFontInfoCacheLowResource = class(TRVFontInfoCache)
  protected
    function GetItems(Index: Integer): TRVFontInfoCacheItem; override;
  end;

implementation
uses RVFuncs, RVGrin;
{==============================================================================}
function AssignSSProperties(CacheItem: TRVFontInfoCacheItem;
  UseFormatCanvas: Boolean; GraphicInterface: TRVGraphicInterface): Boolean;
var potm: POutlineTextmetric;
begin
  if CacheItem.FontInfo.SubSuperScriptType=rvsssNormal then begin
    CacheItem.VerticalOffset := 0;
    Result := False;
    exit;
  end;
  potm := RV_GetOutlineTextMetrics(CacheItem.Canvas, GraphicInterface);
  if potm<>nil then
    try
      case CacheItem.FontInfo.SubSuperScriptType of
        rvsssSubscript:
          begin
            CacheItem.ExtraFontInfo.ScriptHeight := potm.otmptSubscriptSize.Y;
            CacheItem.VerticalOffset := -potm.otmptSubscriptOffset.Y;
          end;
        rvsssSuperscript:
          begin
            CacheItem.ExtraFontInfo.ScriptHeight := potm.otmptSuperscriptSize.Y;
            CacheItem.VerticalOffset := potm.otmptSuperscriptOffset.Y;
            if CacheItem.VerticalOffset+CacheItem.ExtraFontInfo.ScriptHeight>potm.otmTextMetrics.tmAscent then
              CacheItem.VerticalOffset := potm.otmTextMetrics.tmAscent-CacheItem.ExtraFontInfo.ScriptHeight;
          end;
      end;
    finally
      FreeMem(potm);
    end
  else
    case CacheItem.FontInfo.SubSuperScriptType of
      rvsssSubscript:
        begin
          CacheItem.ExtraFontInfo.ScriptHeight := Abs(Round(CacheItem.Canvas.Font.Height*2/3));
          CacheItem.VerticalOffset := -Abs(Round(CacheItem.Canvas.Font.Height*0.25));
        end;
      rvsssSuperscript:
        begin
          CacheItem.ExtraFontInfo.ScriptHeight := Abs(Round(CacheItem.Canvas.Font.Height*2/3));
          CacheItem.VerticalOffset := Abs(Round(CacheItem.Canvas.Font.Height*0.45));
        end;
    end;
  if UseFormatCanvas then begin
    CacheItem.ExtraFontInfo.ScriptHeight :=
      RVConvertFromFormatCanvas(CacheItem.ExtraFontInfo.ScriptHeight, True);
    CacheItem.VerticalOffset :=
      RVConvertFromFormatCanvas(CacheItem.VerticalOffset, True);
  end;
  Result := True;
end;
{=========================== TRVFontInfoCache =================================}
constructor TRVFontInfoCache.Create(const AData: TObject;
  const ARVStyle: TRVStyle; const ACanvas, ADrawCanvas: TCanvas;
  ACanUseCustomPPI: Boolean);
var i: Integer;
begin
  FRVStyle := ARVStyle;
  FCanvas := ACanvas;
  FDrawCanvas := ADrawCanvas;
  FCanUseCustomPPI := ACanUseCustomPPI;
  FOwner := AData;
  LastTextStyle := -1;
  Capacity := ARVStyle.TextStyles.Count;
  for i := 0 to ARVStyle.TextStyles.Count-1 do
    Add(nil);
end;
{------------------------------------------------------------------------------}
procedure TRVFontInfoCache.Clear;
begin
  inherited Clear;
  RVFreeAndNil(FInvalidItem);
end;
{========================== TRVFontInfoCacheFast ==============================}
procedure TRVFontInfoCacheFast.Clear;
var
  CacheItem: TRVFontInfoCacheItem;
  i: Integer;
begin
  if (FInvalidItem<>nil) and (FInvalidItem.Canvas<>nil) then
    FRVStyle.GraphicInterface.DestroyCompatibleCanvas(FInvalidItem.Canvas);
  for i := 0 to Count-1 do
    if Get(i) <> nil then begin
      CacheItem := Get(i);
      FRVStyle.GraphicInterface.DestroyCompatibleCanvas(CacheItem.Canvas);
    end;
  inherited Clear;
end;
{------------------------------------------------------------------------------}
{$IFNDEF RICHVIEWDEF4}
destructor TRVFontInfoCacheFast.Destroy;
begin
  Clear;
  inherited Destroy;
end;
{$ENDIF}
{------------------------------------------------------------------------------}
function TRVFontInfoCacheFast.GetItems(Index: Integer): TRVFontInfoCacheItem;
var
  CacheItem: TRVFontInfoCacheItem;
  sz: TSize;
  XForm: TXForm;
  {$IFDEF RVHIGHFONTS}
  POTM: POutlineTextmetric;
  {$ENDIF}
begin
  if (Index>=Count) and (Index<FRVStyle.TextStyles.Count) then
    Count := Index+1;
  if (Index<0) or (Index>=Count) then
    CacheItem := FInvalidItem
  else
    CacheItem := Get(Index);
  if (CacheItem = nil) or (CacheItem.Rotation<>Rotation) then begin
    if CacheItem = nil then begin
      CacheItem := TRVFontInfoCacheItem.Create;
      CacheItem.Canvas := FRVStyle.GraphicInterface.CreateCompatibleCanvas(FCanvas);
      if (FCanvas<>FDrawCanvas) and
        (FRVStyle.GraphicInterface.GetMapMode(FCanvas)=MM_ANISOTROPIC) then begin
        FRVStyle.GraphicInterface.SetMapMode(CacheItem.Canvas, MM_ANISOTROPIC);
        FRVStyle.GraphicInterface.GetWindowExtEx(FCanvas, sz);
        FRVStyle.GraphicInterface.SetWindowExtEx(CacheItem.Canvas, sz.cx, sz.cy, nil);
        FRVStyle.GraphicInterface.GetViewportExtEx(FCanvas, sz);
        FRVStyle.GraphicInterface.SetViewportExtEx(CacheItem.Canvas, sz.cx, sz.cy, nil);
      end;
      CacheItem.FontInfo := FRVStyle.TextStyles[Index];
      FRVStyle.ApplyStyle(CacheItem.Canvas, Index, CurParaBiDiMode,
       FCanUseCustomPPI, FCanUseCustomPPI,
        nil, True, FCanvas<>FDrawCanvas);
      CacheItem.LastBiDiMode := CurParaBiDiMode;
    end;
    if FRVStyle.GraphicInterface.IsAdvancedGraphicsMode(FCanvas) then begin
      FRVStyle.GraphicInterface.GetWorldTransform(FCanvas, XForm);
      FRVStyle.GraphicInterface.SetGraphicsMode(CacheItem.Canvas, True);
      FRVStyle.GraphicInterface.SetWorldTransform(CacheItem.Canvas, XForm);
      end
    else
      FRVStyle.GraphicInterface.SetGraphicsMode(CacheItem.Canvas, False);
    CacheItem.Rotation := Rotation;
    if AssignSSProperties(CacheItem, FCanvas<>FDrawCanvas, FRVStyle.GraphicInterface) then
      FRVStyle.ApplyStyle(CacheItem.Canvas, Index, CurParaBiDiMode,
        FCanUseCustomPPI, FCanUseCustomPPI, nil, False, FCanvas<>FDrawCanvas);
    FRVStyle.GraphicInterface.GetTextMetrics(CacheItem.Canvas, CacheItem.TextMetric);
    FRVStyle.GraphicInterface.GetTextExtentPoint32_A(CacheItem.Canvas, '-', 1, sz);
    {$IFDEF RVHIGHFONTS}
    POTM := RV_GetOutlineTextMetrics(CacheItem.Canvas, FRVStyle.GraphicInterface);
    if (POTM<>nil) and (POTM.otmfsSelection and $80 <> 0) then begin
      CacheItem.DrawOffsetY := CacheItem.TextMetric.tmAscent;
      CacheItem.EmptyLineHeight := POTM.otmAscent - POTM.otmDescent + Integer(POTM.otmLineGap);
      CacheItem.TextMetric.tmAscent := POTM.otmAscent+Integer(POTM.otmLineGap);
      dec(CacheItem.DrawOffsetY, CacheItem.TextMetric.tmAscent);
      CacheItem.TextMetric.tmDescent := -POTM.otmDescent;
      CacheItem.TextMetric.tmHeight := POTM.otmAscent-POTM.otmDescent;
      end
    else
    {$ENDIF}
      CacheItem.EmptyLineHeight := sz.cy;
    {$IFDEF RVHIGHFONTS}
    FreeMem(POTM);
    {$ENDIF}
    CacheItem.HyphenWidth := sz.cx;
    if FCanvas<>FDrawCanvas then begin
      CacheItem.TextMetric.tmAscent :=
        RVConvertFromFormatCanvas(CacheItem.TextMetric.tmAscent, True);
      CacheItem.TextMetric.tmHeight :=
        RVConvertFromFormatCanvas(CacheItem.TextMetric.tmHeight, True);
      CacheItem.EmptyLineHeight :=
        RVConvertFromFormatCanvas(CacheItem.EmptyLineHeight, True);
      CacheItem.HyphenWidth :=
        RVConvertFromFormatCanvas(CacheItem.HyphenWidth, True);
      CacheItem.TextMetric.tmDescent :=
        CacheItem.EmptyLineHeight - CacheItem.TextMetric.tmAscent;
        //RVConvertFromFormatCanvas(CacheItem.TextMetric.tmDescent, True);
      {$IFDEF RVHIGHFONTS}
      CacheItem.DrawOffsetY :=
        RVConvertFromFormatCanvas(CacheItem.DrawOffsetY, True);
      {$ENDIF}
    end;
    inc(CacheItem.VerticalOffset,
      MulDiv(CacheItem.TextMetric.tmHeight, CacheItem.FontInfo.VShift, 100));
    if (Index<0) or (Index>=Count) then
      FInvalidItem := CacheItem
    else
      Put(Index, CacheItem);
  end;
  Result := CacheItem;
  if CacheItem.LastBiDiMode<>CurParaBiDiMode then
    CacheItem.FontInfo.ApplyBiDiMode(CacheItem.Canvas, CurParaBiDiMode,
      FCanvas<>FDrawCanvas, FRVStyle.GraphicInterface);
end;
{======================== TRVFontInfoCacheLowResource =========================}
function TRVFontInfoCacheLowResource.GetItems(Index: Integer): TRVFontInfoCacheItem;
var
  CacheItem, CacheItem2: TRVFontInfoCacheItem;
  sz: TSize;
  {$IFDEF RVHIGHFONTS}
  POTM: POutlineTextmetric;
  {$ENDIF}
begin
  if (Index>=Count) and (Index<FRVStyle.TextStyles.Count) then
    Count := Index+1;
  if (Index<0) or (Index>=Count) then
    CacheItem := FInvalidItem
  else
    CacheItem := Get(Index);
  CacheItem2 := CacheItem;
  if CacheItem = nil then begin
    CacheItem := TRVFontInfoCacheItem.Create;
    CacheItem.Canvas := FCanvas;
    CacheItem.FontInfo := FRVStyle.TextStyles[Index];
    if (Index<0) or (Index>=Count) then
      FInvalidItem := CacheItem
    else
      Put(Index, CacheItem);
  end;
  if (LastTextStyle <> Index) or (CacheItem.LastBiDiMode<>CurParaBiDiMode) then begin
    CacheItem.LastBiDiMode := CurParaBiDiMode;
    FRVStyle.ApplyStyle(CacheItem.Canvas, Index, CurParaBiDiMode,
      FCanUseCustomPPI, FCanUseCustomPPI,
      nil, False, FCanvas<>FDrawCanvas);
  end;
  if (CacheItem2 = nil) or (CacheItem.Rotation<>Rotation) then begin
    CacheItem.Rotation := Rotation;
    if FRVStyle.TextStyles[Index].SubSuperScriptType<>rvsssNormal then begin
      FRVStyle.ApplyStyle(CacheItem.Canvas, Index, CurParaBiDiMode,
        FCanUseCustomPPI, FCanUseCustomPPI,
        nil, True, FCanvas<>FDrawCanvas);
      AssignSSProperties(CacheItem, FCanvas<>FDrawCanvas, FRVStyle.GraphicInterface);
      FRVStyle.ApplyStyle(CacheItem.Canvas, Index, CurParaBiDiMode,
        FCanUseCustomPPI, FCanUseCustomPPI,
        nil, False, FCanvas<>FDrawCanvas);
    end;
    FRVStyle.GraphicInterface.GetTextMetrics(CacheItem.Canvas, CacheItem.TextMetric);
    FRVStyle.GraphicInterface.GetTextExtentPoint32_A(CacheItem.Canvas, '-', 1, sz);
    {$IFDEF RVHIGHFONTS}
    POTM := RV_GetOutlineTextMetrics(CacheItem.Canvas, FRVStyle.GraphicInterface);
    if (POTM<>nil) and (POTM.otmfsSelection and $80 <> 0) then begin
      CacheItem.DrawOffsetY := CacheItem.TextMetric.tmAscent;
      CacheItem.EmptyLineHeight := POTM.otmAscent - POTM.otmDescent + Integer(POTM.otmLineGap);
      CacheItem.TextMetric.tmAscent := POTM.otmAscent+Integer(POTM.otmLineGap);
      dec(CacheItem.DrawOffsetY, CacheItem.TextMetric.tmAscent);
      CacheItem.TextMetric.tmDescent := -POTM.otmDescent;
      CacheItem.TextMetric.tmHeight := POTM.otmAscent-POTM.otmDescent;
      end
    else
    {$ENDIF}
      CacheItem.EmptyLineHeight := sz.cy;
    {$IFDEF RVHIGHFONTS}
    FreeMem(POTM);
    {$ENDIF}
    CacheItem.HyphenWidth := sz.cx;
    if FCanvas<>FDrawCanvas then begin
      CacheItem.TextMetric.tmAscent :=
        RVConvertFromFormatCanvas(CacheItem.TextMetric.tmAscent, True);
      CacheItem.TextMetric.tmHeight :=
        RVConvertFromFormatCanvas(CacheItem.TextMetric.tmHeight, True);
      CacheItem.EmptyLineHeight :=
        RVConvertFromFormatCanvas(CacheItem.EmptyLineHeight, True);
      CacheItem.HyphenWidth :=
        RVConvertFromFormatCanvas(CacheItem.HyphenWidth, True);
      CacheItem.TextMetric.tmDescent :=
        CacheItem.EmptyLineHeight - CacheItem.TextMetric.tmAscent;
        //RVConvertFromFormatCanvas(CacheItem.TextMetric.tmDescent, True);
      {$IFDEF RVHIGHFONTS}
      CacheItem.DrawOffsetY :=
        RVConvertFromFormatCanvas(CacheItem.DrawOffsetY, True);
      {$ENDIF}
    end;
    inc(CacheItem.VerticalOffset,
      MulDiv(CacheItem.TextMetric.tmHeight, CacheItem.FontInfo.VShift, 100));
  end;
  Result := CacheItem;
end;

end.
