﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RVDBDsgn.pas' rev: 27.00 (Windows)

#ifndef RvdbdsgnHPP
#define RvdbdsgnHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <Winapi.Messages.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <DesignIntf.hpp>	// Pascal unit
#include <DesignEditors.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <RVSEdit.hpp>	// Pascal unit
#include <RichView.hpp>	// Pascal unit
#include <DBRV.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <RVDsgn.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rvdbdsgn
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TfrmDBRVDesign;
class PASCALIMPLEMENTATION TfrmDBRVDesign : public Rvdsgn::TfrmRVDesign
{
	typedef Rvdsgn::TfrmRVDesign inherited;
	
__published:
	Vcl::Comctrls::TTabSheet* TabSheet3;
	Vcl::Stdctrls::TGroupBox* GroupBox4;
	Vcl::Stdctrls::TCheckBox* cbDBAutoDeleteUnusedStyles;
	Vcl::Stdctrls::TCheckBox* cbDBReadOnly;
	Vcl::Stdctrls::TCheckBox* cbDBEscape;
	Vcl::Stdctrls::TLabel* Label7;
	Vcl::Stdctrls::TComboBox* cmbDBFieldFormat;
	HIDESBASE void __fastcall FormActivate(System::TObject* Sender);
	HIDESBASE void __fastcall btnOkClick(System::TObject* Sender);
public:
	/* TCustomForm.Create */ inline __fastcall virtual TfrmDBRVDesign(System::Classes::TComponent* AOwner) : Rvdsgn::TfrmRVDesign(AOwner) { }
	/* TCustomForm.CreateNew */ inline __fastcall virtual TfrmDBRVDesign(System::Classes::TComponent* AOwner, int Dummy) : Rvdsgn::TfrmRVDesign(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TfrmDBRVDesign(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TfrmDBRVDesign(HWND ParentWindow) : Rvdsgn::TfrmRVDesign(ParentWindow) { }
	
};


class DELPHICLASS TDBRVEEditor;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TDBRVEEditor : public Rvsedit::TRVEEditor
{
	typedef Rvsedit::TRVEEditor inherited;
	
public:
	virtual void __fastcall ExecuteVerb(int Index);
public:
	/* TComponentEditor.Create */ inline __fastcall virtual TDBRVEEditor(System::Classes::TComponent* AComponent, Designintf::_di_IDesigner ADesigner) : Rvsedit::TRVEEditor(AComponent, ADesigner) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TDBRVEEditor(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Rvdbdsgn */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_RVDBDSGN)
using namespace Rvdbdsgn;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RvdbdsgnHPP
